\documentclass[a5paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{hyperref}
\usepackage{cclicenses}
\bibliographystyle{alpha}

\title{}
\author{Yann Kervran}

\begin{document}

\begin{titlepage}
\center{
\LARGE
L'armée du comté de Tripoli et les pratiques militaires franques à la fin du XII\textsuperscript{e}~siècle\\
\Large
\vspace{1cm}
Yann Kervran\\
\vspace{4,5cm}
\begin{center}  \includegraphics[scale=0.5]{logo.png}  \end{center}
\vspace{0,5cm}

Hexagora\\
\smallskip
\small
https://www.hexagora.fr\\
\bigskip

\cc \ccby \ccsa

}

\end{titlepage}


\section{L'armée du comté de Tripoli}
Les forces totales du comté sont estimées entre 100\footnote{Estimation donnée par R.~C. Smail, extrapolée d'après Rey, \textit{Les colonies franques de Syrie au XIIème et XIIIème siècles}, Paris, 1883, pp.~110-111 et Beyer, \textit{Zeitschrift des deutschen Palästina-Vereins}, LXVII, 201, tous les deux cités dans \cite[p.~90]{SMAIL56}. A comparer avec les 675 chevaliers pour le royaume de Jérusalem et environ 600 pour Antioche au début du XII\textsuperscript{e}.} et 300\footnote{Chiffre estimé pour le début du XII\textsuperscript{e} siècle par Jean Richard dans \cite[p.~52]{RICHARD45}} chevaliers. Il est raisonnable de penser que le comté pourvait alors compter environ entre 800/1000\footnote{Estimation basée sur le rapport qui existe entre ces deux corps dans le royaume de Jérusalem, où Jean d'Ibelin (dans le \textit{Livre des Assises de la Haute Cour}, Recueil des Historiens des Croisades, Lois, I, pp.~422-426) indique 5025 sergents. Ces chiffres sont cités par \cite[p.~91]{SMAIL56}} et 3000\footnote{Rapport de 1 à 10 entre les chevaliers et les hommes de pied indiqué par Jean Richard dans \cite[p.~52]{RICHARD45}, appliqué à la valeur haute de la fourchette des chevaliers estimés au début du XII\textsuperscript{e} siècle} sergents. Ces derniers forment l'essentiel des unités de piétons\footnote{Cette dernière remarque ne se base sur aucune source directe mais sur des preuves indirectes, présentées dans \cite[p.~91]{SMAIL56}}.

Les forces lors d'un appel en masse à l'arrière-ban n'ont pas à être prises en compte dans le cadre d'un rassemblement des forces à Séphorie pour une opération militaire non exceptionnelle. Par contre, des mercenaires peuvent être présents\footnote{Par mercenaire, on entend toutes les personnes rémunérées autrement que par un fief-rente, qui sont payées à la tâche en quelque sorte. C'est une pratique très bien attestée depuis près de 100 ans en Terre Sainte, voir à ce sujet \cite[pp.~93-94]{SMAIL56}.}.

Les Turcoples ne sont pas souvent désignés par des termes spécifiques (inclus avec d'autres sous le vocable \textit{equites levis armaturae}, voir \ref{equiteslevis} page \pageref{equiteslevis}) et semblent avoir été parfaitement intégrés aux troupes franques, sans avoir une spécificité particulière\footnote{Les Turcoples n'étaient certainement pas systématiquement des archers montés mais de simples combattants locaux. Voir à ce sujet \cite[p.~112]{SMAIL56}.} si ce n'est peut-être la reconnaissance. Mais il s'agissait alors d'un emploi logique, que leur statut de natif prédisposait à une plus grande efficacité.

On peut encore ajouter des pélerins (dont beaucoup sont armés, comme les seigneurs occidentaux) qui peuvent se joindre aux forces combattantes locales. Ils arrivent souvent grâce au \textit{passagium vernale} des cités italiennes, qui leur permet d'être en Terre Sainte pour Pâques\footnote{Voir à ce sujet Heyd, \textit{Histoire du commerce du Levant au moyen âge}, éd. française refondue (publ. par F.~Raynaud), 2 vols, Leipzig, 1936, I, p.~180 cité dans \cite[p.~95]{SMAIL56}}. Enfin, les ordres de chevalerie composent souvent une force importante des armées de Terre Sainte\footnote{On leur confie souvent des postes-clés ou les positions les plus difficiles à tenir, certainement en raison de leur discipline et leur connaissance du terrain et des adversaires, voir par exemple à Hattin en 1187 ou Arsuf en 1191, cités dans \cite[p.97]{SMAIL56}.} en particulier les Hospitaliers de Saint Jean en ce qui concerne le comté de Tripoli\footnote{Dès 1142, le comte de Tripoli donne des forteresses à l'Hôpital et leur accorde le droit de traiter directement avec les Türks~: \textit{Regesta regni hierosolymitani}, éd. Röhricht, no.~212, \textit{Cartulaire des Hospitaliers}, ed. Delaville le Roulx, no.~144}. Mais ces derniers montraient souvent une certaine indépendance dans leur commandement par rapport aux seigneurs territoriaux\footnote{Voir à ce sujet \cite[pp.~102-103]{SMAIL56}.}.

Les forces armées envoyées sur les champs de bataille sont généralement les mêmes que celles qui défendent les forteresses et les villes. Leur emploi est double (et donc mixte)\footnote{D'où la difficulté de garder les places fortes après un échec majeur lors d'une bataille, voir \cite[pp.~104-105]{SMAIL56} et en particulier la découverte par Ernoul de seulement deux hommes malades pour garder le château d'al-Fule en avril 1187 (\textit{Chronique d'Ernoul et de Bernard le trésorier}, ed. M.~L. de Mas Lattrie, Paris, 1871, pp.~149-150).}.

\section{L'aspect des unités}
\subsection{Uniformes et couleurs}
Au niveau des petites unités, les vassaux portent les insignes de leur seigneur, s'il a les moyens de les équiper\footnote{Guillaume le Maréchal porte à un moment un écu aux couleurs de son seigneur : \textit{Histoire de Guillaume le Maréchal}, I, v.1478, p.~54. En 1176, Raymond le Gros équipe sa suite de 30 hommes avec des boucliers similaires : Giraldus Cambrensis, \textit{Expugnatio Hibernica}, p.~335. Ces deux ancedotes sont citées par J.F. Verbruggen \cite[p.~75]{VERBRUGGEN77}.}.
\subsection{Les bannières}
Les bannières sont nombreuses\footnote{A noter qu'il existe une hiérarchie dans les enseignes. Voir le chapitre \textit{The place of the Commanders in Battle} dans \cite[p.~105-106]{VERBRUGGEN77}.} et ont un rôle tactique essentiel\footnote{Lors de la bataille de Hattin, Saladin estime que la victoire ne sera acquise que lorsque la bannière du roi de Jérusalem sera abattue : Ibn al-Athir, \textit{Kamel-Altevarykl} (extrait), RHC, Historiens Orientaux, Paris, 1872, I, pp.~658-86 cité dans \cite[p.~89]{VERBRUGGEN77}.}. Elles servent de point de ralliement et de résistance\footnote{Il est d'ailleurs indiqué aux templiers de rallier la plus proche bannière du Temple en cas de destruction de celle de leur unité ou, à défaut, de celle du plus proche seigneur chrétien. Ce n'est qu'en cas de destruction de toutes les bannières qu'ils peuvent envisager de fuir :  articles 167-8 et 421 de la Règle du Temple cité dans \cite[p.~89]{VERBRUGGEN77}.}. Leur défense est donc essentielle au sein de l'unité\footnote{La Règle du temple comprend d'ailleurs plusieurs articles très précis quant à l'utilisation de la bannière et de la lance qui la porte : 164-5, 611 cités dans \cite[p.~89]{VERBRUGGEN77}.}.

La plus importante est celle du commandant en chef. Certains dirigeants prennent donc la précaution de se munir d'une bannière de réserve en plus de celle qu'ils arborent\footnote{Voir à ce sujet le seigneur de Valkenburg lors de la bataille de Woeringen en 1288 : Jan van Heelu (dans sa \textit{Rijmkronijk}, ed.~J.F.~Willems, CRH~in~4\textsuperscript{o}, Brussels, 1836, vv.~5668-740, pp.~211-14) cité dans \cite[p.~90]{VERBRUGGEN77}.}. Sa présence signale le bon déroulement du combat. Elle ne doit jamais être inclinée\footnote{Voir les articles de la Règle du Temple 164-5 et 611 cités dans \cite[p.~89]{VERBRUGGEN77}.}.

La disparition d'une bannière indique la destruction de l'unité correspondante. Si c'est celle du commandant en chef, cela signifie la perte de la bataille et donc souvent le début du sauve-qui-peut général. Un des moyens de garantir la bannière principale est de la fixer sur un chariot à l'arrière des lignes, protégée par la réserve\footnote{Comme le fit par exemple Otton à la bataille de Bouvines en 1214, avec son enseigne avec un aigle et un dragon d'or. Voir \cite[p.~109]{VERBRUGGEN77}.}.

Une seule exception existe à l'aspect unique du chef de bataille, c'est au sein des ordres de chevalerie. Les commandants d'unité possèdent une bannière de réserve au cas où la principale viendrait à être abattue\footnote{Article 165 de la Règle du Temple}~; cela peut s'expliquer par les motivations pour lesquelles ils font la guerre. Ils ne se battent pas pour un seigneur, mais pour leur Foi. La disparition de leur leader sur le champ de bataille ne signifie pas pour eux la fin du combat.

\section{Taille et formation des unités}

\subsection{Les formations possibles}\label{formation}
Héritées directement de Végèce, certaines formations semblent être assez communes\footnote{Voir différents exemples rassemblés par Philippe Richardot dans \cite[p.159]{RICHARDOT98}}, suffisamment du moins pour que les termes en soient traduits\footnote{Ainsi Alphonse X de Castille les traduit en castillan, voir \cite[pp.~158-159]{RICHARDOT98}}~:
\begin{itemize}
\item Ligne de bataille. Végèce~: <<~\textit{acies}~»; Alphonse~X de Castille~:«~\textit{haz}~»
\item Le carré long. Végèce~: «~\textit{agmen quadratus}~»
\item Le coin. Végèce~:«~\textit{cuneus}~»; Alphonse~X de Castille~:«~\textit{cu\~no}~»
\item Le cercle. Végèce~:«~\textit{orbis}~»; Alphonse~X de Castille~:«~\textit{muela}~»
\item Les ciseaux. Végèce~:«~\textit{forfex}~»
\end{itemize}

\subsection{La cavalerie}
Lorsqu'un chevalier peut regrouper sous ses ordres 20~chevaliers, on lui donne le terme de «~banneret~» et il perçoit double solde\footnote{\cite[p.~76, note~293]{VERBRUGGEN77}.} lorsque son service est monnayé.

L'unité ainsi formée est appelée \textit{conroi}, et elle est décrite de forme carrée\footnote{C'est Guillaume Guiart qui le dit en toutes lettres, mais pour la bataille de Courtrai : \textit{Branche des royaux lignages : chronique métrique de Guillaume Guiart} / publiée pour la première fois, d'après les manuscrits de la Bibliothèque du roi par J.A. Buchon. Paris : Verdière, 1828 \cite[p.~76]{VERBRUGGEN77}.}. Ces unités regroupent des hommes habitués à vivre et combattre ensemble, originaires d'un même endroit ou entretenant des liens sociaux forts\footnote{Ambroise, \textit{L'Estoire de la guerre sainte. Histoire en vers de la Troisième Croisade (1190-1192)}, v.6183-6189, éd. G.~Paris, Imprimerie Nationale, Paris; 1897, pp.~399-400, cité dans \cite[pp.162-163]{RICHARDOT98}.}.

Les \textit{batailles} se forment par une série de \textit{conrois} qui se juxtaposent. Cela correspond à l'\textit{acies} (la ligne) de Végèce\footnote{Voir \cite[p.~163]{RICHARDOT98}.}. Le déploiement se fait sur 4 rangs\footnote{Dans le cas où les conrois sont trop petits, ils sont groupés par deux pour obtenir cette profondeur. Ce nombre de rangs est attesté en 1211 \cite[p.~77]{VERBRUGGEN77}.}.

La cavalerie comporte des sergents à cheval, bien que les termes \textit{servientes loricati} ou \textit{serjens à cheval} ne soient pas fréquents. On leur préfère les expressions telles que \textit{milites gregarii}\footnote{Raimundus de Aguiler,\textit{Recueil des Historiens des Croisades, Hist. occ.}, III,  p.~242~; Albert d'Aix,\textit{Recueil des Historiens des Croisades, Hist. occ.}, IV, p.~698}, \textit{milites plebei}\footnote{Raimundus de Aguiler,\textit{Recueil des Historiens des Croisades, Hist. occ.}, III, p.~274}. On emploie même \textit{equites levis armaturae}\label{equiteslevis} ou \textit{classis secundae} pour bien les différencier des \textit{milites}, qui sont les guerriers de premier plan. Le choix de ces termes se base plus sur la différence d'équipement (moins perfectionné) que sur la différence d'usage. Le terme de cavalerie légère n'implique pas un rôle différent dans les combats pour ces hommes\footnote{Le Temple reconnaît tout de même aux hommes moins bien équipés le droit de se retirer plus facilement du combat, ce qui est interdit aux sergents bien équipés, voir l'article 172 de la Règle du Temple}.

Les \textit{armigeri} et \textit{ecuyers} n'étaient pas des combattants mais des auxiliaires servants les \textit{milites}\footnote{Voir par exemple la Règle du Temple, articles 161 et 179 où il est dit qu'un chevalier a deux écuyers qui le servent.}.

\subsection{Les chevaliers démontés}
Il est très fréquent que des chevaliers démontent pour encadrer, renforcer, des unités de piétons\footnote{Les exemples abondent pour le XII\textsuperscript{e} siècle, et pas seulement pour les affrontements navals ou les sièges : Henry~I\textsuperscript{er} et Robert de Normandie à Tinchebray en 1106, le même Henry~I\textsuperscript{er} à Brémule en 1119, les Normands à Bourg-Théroulde en 1124 pour ne relever que ces exemples. Voir \cite[Chapitre \textit{The Knights Fighting on Foot}, pp.~106-108]{VERBRUGGEN77}.}. Ils servent alors naturellement de dirigeants, fortifiant le moral des troupes. Ils peuvent être en seconde ligne\footnote{C'est le cas à la bataille de Lincoln, en 1141.}.

\subsection{Les fantassins}
Il existe plusieurs types de formations~: avec un front large\footnote{Comme le font les Flamands généralement,  \cite[p.~182]{VERBRUGGEN77}.}, en colonne profonde\footnote{Qui est utile lorsque le relief est très encaissé. C'est d'ailleurs la formation préférée des Suisses, qu'ils semblent avoir héritée du haut Moyen Age  \cite[pp.~182-183]{VERBRUGGEN77}.} ou en formation circulaire\footnote{Les Ecossais emploient le fameux \textit{schiltron} mais les Suisses savent également adopter cette formation extrêmement efficace contre un adversaire équestre, dans un but défensif, comme les hommes de Berne en 1271, 1289 ou 1132. La pénétration de quelques adversaires risquait de mettre à mal la formation complète, voir H. Delbrück, \textit{Geschichte der Kriegskunst im Rahmen der politischen Geschichte}, 3, Berlin, 2\textsuperscript{nde} edn, 1923, III, pp. 604-5, 686 cité dans \cite[pp.~183-184]{VERBRUGGEN77}.} (voir \ref{formation} page \pageref{formation}).

Les fantassins sont souvent déployés en deux, trois ou plus d'unités mais qui peuvent être assemblées en une seule le cas échéant\footnote{\cite[p.~183]{VERBRUGGEN77}.}. Une bannière est affectée à chaque formation, qui a la même utilité de transmission des ordres et de ralliement que pour les cavaliers\footnote{\cite[p.~183]{VERBRUGGEN77}.} (voir la partie \ref{roledesbannieres} page \pageref{roledesbannieres}).

\section{Préparation à la bataille}
Dès le lever du soleil, les hommes sont appelés à se préparer. Après avoir démonté le camp (tentes, huttes etc.), ils assistent à la messe et communient\footnote{\cite[p.~187]{VERBRUGGEN77}.}. Il est possible que certains éléments du camp soient mis en place comme points de ralliement ou pour servir aux dirigeants\footnote{Ainsi le roi Guy de Lusignan fait monter sa tente sur une des cornes de Hattin lors de l'affrontement face à Saladin en 1187.}.

\section{L'ordre de marche}
Une force de cavalerie avance en premier, composée de plusieurs \textit{conrois} qui s'égayent en éclaireurs. Ils ont pour rôle d'apprécier l'emplacement des forces ennemies\footnote{En 1328, à la bataille de Cassel, ce sont deux maréchaux qui ont la charge de ces éclaireurs~: \textit{Istore et Croniques}, I, P.~343, cité par \cite[p.~208]{VERBRUGGEN77}.}.
Ensuite, l'armée en elle-même est divisée en 9 sections qui forment par trois l'avant-garde, le corps central et l'arrière-garde.

Lorsque la position ennemie est connue, l'avance doit se faire de préférence en ligne, pour pouvoir se déployer de la façon la plus facile et la plus rapide (le passage de colonnes à des lignes peut s'avérer problématique)\footnote{Cette disposition a été adoptée lors de la bataille d'Ascalon en 1099, décrite par Raymond d'Aguilers, cité dans \cite[p.~208]{VERBRUGGEN77}.}.
 
L'avant-garde est généralement composée uniquement de cavaliers, plus mobiles, le corps principal pouvant être de l'infanterie\footnote{Lors des batailles de Legnano en 1176 et de Cortenuova en 1237, les chevaliers sont en premier et entament l'affrontement tandis que l'infanterie peut se déployer plus tranquillement, voir \cite[p.~209]{VERBRUGGEN77}.} et l'arrière-garde et les flancs composés également de cavaliers.

\section{Le déploiement}
\subsection{Les fantassins}
Le recrutement des fantassins, comme des sergents en général, se fait souvent par le biais des services dûs par les collectivités et les ordres et personnalités religieuses\footnote{Voir pour comparaison l'étude de Edouard Audoin, \cite{audouin13} qui analyse en détail les services militaires rendus par la fourniture de contingents de sergents à l'extrême fin du XII\textsuperscript{e} siècle dans le royaume de France.}.

Lorsque les unités de fantassins se déplacent, cela peut être fait de façon à séparer trois corps qui s'appuieront l'un  l'autre lors de leur mise en place. La première unité pouvant devenir l'aile gauche (ou droite), la seconde le centre et la dernière l'autre aile\footnote{\cite[p.~183]{VERBRUGGEN77}.}. Ainsi, chaque unité déjà en place appuie les autres lors de leur déploiement. Les corps créés se déplacent donc en maintenant une certaine distance entre eux\footnote{\cite[p.~183]{VERBRUGGEN77}.}.

Il est fréquent que l'infanterie cherche à protéger ses flancs par un appui naturel comme un fossé, une rivière, un bois, une colline etc\footnote{Voir à ce sujet \cite[p.~182]{VERBRUGGEN77}.}. Lorsque la bataille est uniquement défensive pour eux, même le front s'abrite derrière une telle protection\footnote{A Courtrai en 1302, les Flamands se servent de fossés \cite[p.~182]{VERBRUGGEN77}.}.

Les fantassins cherchent à avoir la formation la plus dense possible\footnote{Il arrive même que des hommes meurent étouffés par la presse, comme papr exemple Philip van Artevelde à la bataille de Westrozebeke ou de nombreux Ecossais lors de la bataille de Dupplin Moor en 1332, cités dans \cite[p.~189]{VERBRUGGEN77}.}, de façon à s'opposer de façon efficace aux attaques de la cavalerie ennemie. C'est la raison pour laquelle il n'y a pas toujours de réserve d'infanterie en arrière-ligne. Parfois les baggages servent à protéger les arrières\footnote{C'est le cas par exemple pour les forces flamandes à Mons-en-Pévèle en 1304\cite[p.~182]{VERBRUGGEN77}.}.

\subsection{Les archers et arbalétriers}
Leur utilité est surtout lors de la préparation et de l'engagement. Ils se déploient en premier et peuvent garder la première ligne tant que le contact n'a pas été fait. Ils passent alors en second plan, soit pour protéger certains points (comme les flancs) ou en réserve le temps de voir l'évolution de l'affrontement.

\section{L'ordre de bataille}
L'armée adopte un ordre profond, avec 3 rangs d'unités. Le premier rang est formé de cavalerie appuyée par les fantassins, situés au centre de la formation\footnote{Lors de la bataille de Hab, en 1119, le roi Baudoin de Jérusalem adopte cet ordre de bataille, qui est similaire à celui adopté, par exemple, par Henry\textsuperscript{er} de Constantinople en 1208 à Philippopoli, voir \cite[pp.~209-210]{VERBRUGGEN77}.}.

Les deux ailes forment le second rang, déployées à droite et à gauche du corps central, un peu en retrait. Elles sont composées de cavalerie qui peuvent protéger les flancs du corps central, étendre le front de bataille et se rendre rapidement en soutien de la ligne principale.

L'arrière, composé de cavalerie, forme la troisième ligne, située au centre. Il s'agit de la réserve qui est souvent dirigée par le commandant en chef de l'armée\footnote{Ainsi Bohémond, qui commande souvent l'armée croisée lors de la première Croisade, est généralement à la tête de la réserve, le roi Baudoin I\textsuperscript{er} également ou encore Richard I\textsuperscript{er}. Voir par exemple Raymond d'Aguilers, \textit{Historia Francorum qui ceperunt Jherusalem}, RHC, Hist. Occ., 3, pp.~244-5, cité dans \cite[pp.~228-219]{VERBRUGGEN77}. Attention, le commandant en chef de l'armée n'est pas forcément le dirigeant politique. Ainsi frère Guérin à Bouvines dirige l'armée tandis que Philippe Auguste assure un commandement au niveau tactique seulement.}. Elle peut s'appuyer sur un corps de fantassins, voire sur les baggages, où les blessés peuvent être amenés et les réserves de boisson et de nourriture gardés\footnote{Lors de la bataille de Reims en 1124, Louis VI avait protégé cet emplacement par un cercle de chariots, voir Suger, c.~28, pp.~22-6, Lot, I, pp.~121-2 cités dans \cite[p.~211]{VERBRUGGEN77}.}. Les morts peuvent également être amenés en arrière des lignes à cet endroit, de façon à ne pas dévoiler à l'adversaire les pertes\footnote{Ainsi Baudoin III cache les pertes qu'il subit lors de sa marche pour s'emparer de Bosra et Salkahd en 1147, voir \cite[pp.~158-159]{SMAIL56}}.

L'engagement, au niveau tactique, est généralement débuté par la droite\footnote{Voir à ce sujet Philippe Contamine : \cite[p.~381]{CONTAMINE80}}.


\section{La communication des ordres}
\subsection{Les instruments de musique}
Les trompettes et cornes servent à donner énormément d'indications, depuis le fait de s'armer jusqu'au rassemblement, en passant par le regroupement des unités\footnote{Pour ne parler que d'elle, la \textit{Chanson de Roland} parle plusieurs fois de l'utilité des trompettes : vv.~700-703, v.~1454-1455, v.~1796 etc., cité dans \cite[p.~75]{VERBRUGGEN77}.}.

Pour les unités de fantassins, des tambours peuvent aider aux déplacements\footnote{Les formations suisses les utilisent voir H. Delbrück, \textit{Geschichte der Kriegskunst im Rahmen der politischen Geschichte}, 3, Berlin, 2\textsuperscript{nde} edn, 1923, III, pp. 619 cité dans \cite[p.~183]{VERBRUGGEN77}.}.

Au tout début du XIII\textsuperscript{e} siècle, on cite comme instruments utilisés les \textit{olifans}, \textit{grelles}, \textit{chalumiaus}, \textit{buisines}\footnote{Instruments énumérés dans la chanson de Geste \textit{\textbf{Gui de Bourgogne}}, cité dans \cite[p.~94]{contamine1997}.}.

L'utilisation de trois appels permet aux troupes tout d'abord de s'équiper, puis de se rassembler en unités puis de se rendre au point de ralliement de l'armée\footnote{Juste avant la bataille des Champs Sanglants, en juin 1119, Gautier le Chancellier indique un tel emploi de trois sonneries de trompes, \cite[II, iv, 3]{hagenmeyer1896} cité dans \cite[p.186]{asbridge2000}.}.

\subsection{Les instructions visuelles}
\label{roledesbannieres}Les bannières servent de complément aux ordres sonores, permettant aux combattants de repérer où se rendre ou se rassembler\footnote{Voir par exemple l'article 167 de la Règle du Temple}. Le fait d'amener la bannière en avant des rangs indique l'ordre d'avancer\footnote{Ce qui confirme le rôle très important de la bannière au point de vue tactique. Des exemples de cette pratique existent dans la règle 164 du Temple ou également dans G. Villani, \textit{Historie Fiorentine}, ed. L.A. Muratori, Rerum Italicarum Scriptores, 13, p.~387~; dans \textit{Chronographia regum Francorum}, ed. H. Moranvillé, SHF, 3 vols, Paris, 1891-97, I, pp.~107-108 cités dans \cite[p.~85]{VERBRUGGEN77}.}.

\subsection{Le cri de guerre}
Le lancer du cri de guerre peut répondre à un déroulement bien précis. Là encore, les Byzantins ont couché par écrit le formalisme de son lancement. A une portée d'arc de l'ennemi, un des hommes criait «~\textit{Parati~!}~». un autre lui répondait «~\textit{Adiuta}~» et alors tous s'écriaient «~\textit{Deus}~»\footnote{\cite[p.~87]{VERBRUGGEN77}.}. Il existe plusieurs types de cri utilisables.\footnote{La \textit{Chanson de Roland} (v.~3358) indique «~\textit{diex aie}~» comme cri, qui fut employé par les croisés lors de la prise de Jérusalem mais il en existe d'autres.}.

\section{Les ordres}

\subsection{Les ordres de cavalerie}

\subsubsection{Formez les rangs~: «~??~»}
Un ordre est donné pour indiquer que les rangs sont formés et qu'il est interdit d'en sortir, que ce soit pour avancer ou reculer. La cohésion doit être maintenue\footnote{C'est également le cas dans les armées byzantines \cite[p.~86]{VERBRUGGEN77}.}.

\subsubsection{En avant~: «~Mouvez~»}
L'avancée des troupes se fait suite à un ordre du commandant en chef\footnote{A Courtrai, c'est le comte d'Artois qui emploie la phrase «~Mouvez~», cité par Guillaume Guiart, v.15119 \cite[p.~86]{VERBRUGGEN77}}. Le relais peut se faire à l'aide de trompettes, qui indiquent alors aux porteurs des bannières d'avancer. Les unités doivent alors suivre leur bannière\footnote{La Règle du Temple, articles 164 et 164, citée dans\cite[p.~86, note~354]{VERBRUGGEN77}.}.

Pour les cavaliers, l'avance se fait avec la lance verticale, elle ne sera mise en position qu'à distance de charge\footnote{Voir \cite[p.~108]{VERBRUGGEN77}. }.

\subsubsection{Halte~: «~??~»}
L'arrêt des troupes peut se faire par plusieurs ordres, la chaîne de commandement étant la même que pour le départ : ordre du commandant en chef, relais par sonneries, exécution indiquée par les bannières\footnote{L'arrêt par la bannière est indiqué par la \textit{Chanson de Roland}, vv.~707-9, vv.~2949-52. L'ensemble des indications possibles se trouve dans \cite[p.~86]{VERBRUGGEN77}.}.

\subsubsection{Gardez la formation~: «~??~»}
Pour garder les formations en mouvement, il existe un ordre du type «~\textit{Aequaliter ambula}~» des Byzantins, qui est par exemple le «~\textit{ordinata aequaliter acies}~» attesté en 933\footnote{Ordre donné par Henry I\textsuperscript{er} lors d'une marche contre les Hongrois. Voir Liudprand, \textit{Antapodosis}, in \textit{Opera}, ed. J. Becker, MGH, SS, 1915, 1.~II,c.~31, pp.~51-2, cité dans \cite[p.~86]{VERBRUGGEN77}.}.
 
Les maréchaux peuvent néanmoins se déplacer librement, de façon à encadrer au mieux les formations\footnote{Jean le Bel écrit au XIV\textsuperscript{e} siècle (\textit{Chronique} I, ed. J.~Viard et E.~Déprez, SHF, Paris 1904-1905, p.~54) «~personne n'osait, de peur de perdre sa tête, chevaucher en avant des bannières, en dehors des maréchaux~».}.

\subsubsection{Déployez la ligne~: «~??~»}
L'expression «~\textit{Largiter ambula}~» est byzantine\footnote{Les ordres byzantins indiqués ici sont extraits du \textit{Mauricii Strategicon}, ed. G.T. Dennis, 1.~III, c.~5, pp.~152-8; c.~9, p.~172; 1~XII, B, 16, p.~442; B, 24, p.~484 cité dans \cite[pp.~86-89]{VERBRUGGEN77}.}. L'idée est de déployer les hommes de façon à avoir un front plus large\footnote{Cette pratique est attestée à plusieurs reprises : lors de la bataille de Woeringen (en 1288), Raas de Liedekerke donne de telles instructions en réaction à la formation adverse, cité dans \cite[p.~86]{VERBRUGGEN77}.}.

\subsubsection{A droite/ A gauche~: «~??~»}
Les ordres byzantins sont respectivement «~\textit{depone dextra}~» et «~\textit{depone sinistra}~». Ils ont pour rôle d'indiquer à la cavalerie de se rendre sur la droite ou la gauche\footnote{\cite[p.~87]{VERBRUGGEN77}.}.

\subsubsection{A l'arrière~: «~??~»}
Les Byzantins ont un ordre qui indique à la cavalerie d'aller protéger les arrières en ouvrant un nouveau front, sans pour autant abandonner le principal. C'est réalisé de façon à contrer des tentatives d'attaques par les arrières\footnote{\cite[p.~87]{VERBRUGGEN77}.}.

\subsubsection{Seconde ligne~: «~??~»}
La première ligne d'attaque peut se voir ordonner de passer en seconde ligne, voire en réserve sur cet ordre\footnote{Des exemples de telles pratiques sont attestées à Adrianople, lors de la défaite de Baudoin~I\textsuperscript{er} en 1205 ou lors de la bataille d'Arsuf livrée par Richard~I\textsuperscript{er} Coeur de Lion\cite[p.~87]{VERBRUGGEN77}.}.

\subsubsection{Serrez les rangs~: «~??~»}
Cette instruction a pour but de densifier la formation, afin de renforcer l'effet de choc et d'éviter la dilution\footnote{A Woeringen, de tels ordres sont répétés à plusieurs reprises : Jan van Heelu (Jan Van Heelu, \textit{Rijmkronijk}, ed.~J.F.~Willems, CRH~in~4\textsuperscript{o}, Brussels, 1836) en parle à plusieurs reprises \cite[p.~86]{VERBRUGGEN77}.}. Il est possible que cela ait pu comprendre l'ordre donné aux flancs de se serrer équivalent au «~\textit{ad latus stringe}~» des Byzantins, qui ne semble pas avoir été utilisé dans les armées de chevaliers\footnote{\cite[pp.~86-87]{VERBRUGGEN77}.}. D'autre part, les byzantins emploient également l'ordre «~\textit{iunge}~» pour indiquer aux rangs arrières de se resserrer vers les premiers rangs\footnote{\cite[p.~87]{VERBRUGGEN77}.}.

\subsubsection{Ennemi en vue~: «~??~»}
Les Byzantins emploient l'ordre «~\textit{cum ordine seque}~» lorsque les cavaliers éclaireurs indiquent la présence de l'ennemi à moins d'un mile\footnote{\cite[p.~87]{VERBRUGGEN77}.}.

\subsubsection{Revenez dans les rangs~: «~??~»}
Pour indiquer aux éclaireurs de se replier sur les positions, les Byzantins emploient l'ordre «~\textit{cede}~»\footnote{\cite[p.~87]{VERBRUGGEN77}.}.

\subsubsection{Maintenez le contact~: «~??~»}
Pour que le éclaireurs continuent à attaquer l'ennemi, les Byzantins donnent l'ordre «~\textit{torna mina}~»\footnote{\cite[p.~87]{VERBRUGGEN77}.}.

\subsubsection{Au pas~: «~Alés le pas~»}
L'indication est donnée pour un déplacement lent des cavaliers. Il est notamment donné en début de mouvement pour une attaque, pour l'approche et la mise en place avant la charge proprement dite\footnote{Lors de la Quatrième croisade, Robert de Clari indique que cet ordre pouvait être donné par deux hommes désignés dans chaque \textit{bataille} parmi les plus braves et les plus sages : Robert de Clari, \textit{La conquête de Constantinople}, ed. Ph. Lauer, CFMA, Paris, 1924 c.~47, p.~47 cité dans \cite[p.~87]{VERBRUGGEN77}.}.

Un assaut est lancé depuis environ une centaine de mètres des lignes adverses\footnote{Le déplacement se fait d'abord lentement pour les attaques, la rapidité de déplacement des montures même à ces vitesses permettant de ne pas trop recevoir de traits pendant les quelques secondes de l'avancée, voir \cite[p.~187]{VERBRUGGEN77}.}.

\subsubsection{Chargez~: «~Poingniés~»}
L'ordre est donné d'éperonner pour lancer la charge, donné uniquement au dernier moment\footnote{Ce sont les mêmes deux responsables désignés par \textit{bataille} qui peuvent donner cet ordre : Robert de Clari, \textit{ibid.}, \textit{loc. cit.} cité dans \cite[p.~87]{VERBRUGGEN77}.}. L'indication peut se donner par un instrument de musique, comme c'est le cas dans les tournois\footnote{Voir \cite[p.~108]{VERBRUGGEN77}.}.

\subsubsection{Regroupement~: «~??~»}
Après une charge, il est essentiel pour les cavaliers de se rallier, de se regrouper, afin de pouvoir recommencer leur attaque de façon cohérente. L'ordre est sonore car il doit être perceptible par des hommes qui se sont sans doute séparés\footnote{Dans la \textit{Chanson de Rolland} il est par exemple indiqué que la trompette est utilisée pour rallier les hommes aux vers 1319, 1686-1689, 3520-3525, 3546-3555  (cités dans\cite[pp.~92-93]{VERBRUGGEN77}). De très nombreux exemples sont connus également pour la Terre Sainte, cités par Guillaume de Tyr pour le roi Amaury I\textsuperscript{er} : RHC, \textit{Hist. Occ.}, I, p.927 ou Ambroise pour la bataille d'Arsuf : \textit{Estoire de la guerre sainte}, ed. G.~Paris, Coll. de documents inédits sur l'histoire de France, 1897, vv.~6539-42, 6552-6, 6559-62, 6580-1, 6594-614, 7311-25 cités dans \cite[pp.~94-95]{VERBRUGGEN77}}.

\subsubsection{Retraite~: «~??~»}
Il semblerait que ce signal, qui doit être général, soit indiqué par un moyen sonore, les trompes\footnote{Ainsi, à la fin de la bataille d'Hastings, Guillaume empêche le comte Eustache de sonner la retraite : Guillaume de Poitiers, \textit{Gesta Guillelmi ducis Normannorum et regis Anglorum}, ed. R.~Foreville, CHFMA, Paris, 1952, 2, c.~24, p.~202 cité dans \cite[p.~87]{VERBRUGGEN77}.}.


\section{Le rôle de chacune des unités}

\subsection{Les unités à pied}

\subsubsection{Les archers}
Avant le début de l'affrontement proprement dit, il est d'usage de se servir des archers et arbalétriers pour harceler l'adversaire, chercher à lui faire perdre sa cohésion, à le garder à distance\footnote{Nous avons l'exemple de cela à diverses reprises, voir la bataille de Hastings en 1066, avec les archers du duc de Normandie}. Cela permet en outre de déployer correctement les unités sans gêne\footnote{Voir à ce sujet \cite[p.~182]{VERBRUGGEN77}.} avant le début du combat proprement dit.

Les archers servent également à protéger de façon épisodique les flancs\footnote{Voir à ce sujet \cite[p.~182]{VERBRUGGEN77}.}.

Entre les charges de cavalerie, les archers peuvent également être employés de façon plus efficace sur les unités adverses qui ont alors une cohésion moins grande, le temps qu'elles se reforment\footnote{Comme lors de la bataille d'Hastings en 1066, l'exemple est présenté dans \cite[p.~212]{VERBRUGGEN77}} après avoir subi un assaut.

En Terre Sainte, le rôle des archers (et arbalétriers) est de maintenir les archers montés turcs à distance, pour protéger les rangs de l'infanterie et les chevaux des cavaliers entre les charges\footnote{Ce rôle est plusieurs fois décrit, depuis la bataille d'Antioche lors dela Première Croisade jusqu'à la bataille d'Arsuf, où le biographe de Saladin parle de leur grande efficacité. Voir \cite[p.~213]{VERBRUGGEN77}}.

\subsubsection{Les fantassins}
Les fantassins sont plus utiles en terrain accidenté ou boisé, la cavalerie ayant besoin d'un vaste terrain plat pour manoeuvrer correctement et se déployer\footnote{Plusieurs auteurs le remarquent, comme Giraldus Cambrensis, dans l'\textit{Expugnatio Hibernica}, p.~395 ou l'auteur du \textit{Strategikon}, VIII, 2, 20, 21~; XI, 3 (ed. G.T.~Dennis, pp.281-3, 369-71) [tr.\textit{Maurice's Strategikon}, pp.~83-4, 96-100] cités dans \cite[pp.~204-205]{VERBRUGGEN77}.}.

Les fantassins servent de point d'appui pour la cavalerie, comme une forteresse humaine qui permet un repli à l'abri\footnote{Ce rôle relativement passif de l'infanterie est attesté depuis l'Antiquité, voir par exemple Végèce, semble avoir perduré (voir le \textit{Strategikon}) et même se renforcer encore dans des armées où la cavalerie acquiert le rôle de premier plan, voir \cite[p.~211]{VERBRUGGEN77}.}. La cavalerie passe alors sur les côtés de l'unité et pas à travers. Lors de l'assaut, elle peut parfois lancer la charge depuis l'arrière des unités de fantassins, en passant entre les piétons\footnote{Pour ces deux manoeuvres combinant piétons et cavaliers, voir le \textit{Strategikon}, 1.~XII, c.~VIII, p.~416. Lot, I, pp.~52, 54, cités dans \cite[p.~212]{VERBRUGGEN77}.}.

Il est parfois important pour les fantassins d'essayer de suivre les unités de cavaliers, de façon à pouvoir appuyer et renforcer leur attaque. Ils ne doivent pas laisser les cavaliers trop s'avancer seuls. Ils peuvent profiter de la charge pour se rapprocher de l'ennemi, voire même entrer en contact avec les unités adverses, généralement alors désorganisées\footnote{Voir le chapitre \textit{Mutual support of Knights and Foot-Soldiers} dans \cite[pp.~214-215]{VERBRUGGEN77}}.
 
Néanmoins, le rôle des fantassins peut devenir très actif lorsque l'ennemi se débande. Alors, les fantassins légers se lancent à l'assaut des adversaires, en raison de leur grande mobilité et de leur souplesse d'emploi. Par contre, il est plus prudent que les fantassins lourds gardent pendant ce temps leur formation\footnote{Cet emploi des fantassins légers pour assurer la maîtrise de certains éléments lorsque la bataille est remportée, en complément de la cavalerie se trouve dans Végèce, cité dans \cite[p.~211]{VERBRUGGEN77} ou bien lors de la bataille de Steppes en 1213, voir la \textit{Vita Odiliae}, pp.~183-4 cité dans \cite[p.~214]{VERBRUGGEN77}.}.

Le but des fantassins est de tenir le terrain, en offrant une ligne de défense infranchissable. Ils doivent empêcher la percée de la cavalerie adverse grâce à leurs piques\footnote{Les chevaliers adverses chargeaient les unités de fantassins en espérant leur débandade avant le choc mais n'hésitaient pas à tenter la percée de force, essayant d'esquiver les pointes adverses, le choc était terrible et le bruit terrifiant. Voir à ce sujet Guillaume Guiart, vv. 15760-2, pp. 245-6 et le\textit{Chronicon comitum Frandrensium}, p. 169, ou encore \textit{Le siège de Barbastre}, c.~6, vv.~220-8 parmi d'autres cités dans \cite[p.~188-9]{VERBRUGGEN77}.} et s'opposer aux unités de fantassins adverses.

Face à la cavalerie, les premiers rangs offrent un rempart de leurs boucliers et les pointes de leurs armes d'hast, les seconds rangs devant s'occuper des cavaliers ennemis tombés à terre.

Il faut tout de même signaler que l'auteur du \textit{Strategikon} propose de lancer les hostilités par un assaut des piétons ensuite soutenus par la charge de la cavalerie\footnote{Cité dans \cite[p.~212]{VERBRUGGEN77}.}.

Une possibilité de composition d'unité de fantassins est la suivante\footnote{Il s'agit de celle adoptée par les hommes de Richard I\textsuperscript{er} Coeur de Lion lors de la bataille d'Arsuf, décrite par Ambroise, \textit{Estoire de la guerre sainte}, ed. G.~Paris, Coll. de documents inédits sur l'histoire de France, 1897, pp.~307-8 et reprise dans \cite[p.~213]{VERBRUGGEN77}.}. Un premier rang de piquiers agenouillés plante le talon des lances dans le sol. Ils tiennent leur bouclier à leur bras gauche, avec la pointe elle-aussi plantée dans le sol. Tous les deux piquiers, un arbalétrier est disposé, qui travaille de concert avec un homme effectuant le chargement\footnote{A Arsuf, les arbalétriers sont Gênois et Pisans, voir Ambroise, \textit{Estoire de la guerre sainte}, ed. G.~Paris, Coll. de documents inédits sur l'histoire de France, 1897, pp.307-8, cité dans \cite[p.~213]{VERBRUGGEN77}.}. A noter que cette formation est déployée en cercle, et que les cavaliers se protégent en son centre, attendant de pouvoir mener une contre-attaque\footnote{C'est la tactique qu'utilise aussi Renaud de Dammartin lors de la bataille de Bouvines en 1214.}.

Les unités de fantassins peuvent être dirigées par un cavalier\footnote{Voir \cite[p.~214]{VERBRUGGEN77}.}.

\newpage

\bibliography{bibliographie}
\vspace{1cm}

% Licence Creative commons
\bigskip
\hrule
\bigskip
\begin{small}
\noindent Copyright 2018~: Yann \textsc{Kervran}\\
Ce livret est placé sous Licence Creative Commons By-Sa (\url{https://creativecommons.org/licenses/by-sa/3.0/fr/}).
\end{small}

\end{document}