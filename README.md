# Dépôt des éléments servant à générer les documents publiés de l'univers Hexagora

Version 2016.10.31

## Organisation du dépôt

Le dépôt s'organise selon un principe dérivé de [GitLab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html).

Les branches sont utilisées comme suit :

* **WIP/XXX** (anciennement Feature/XXX) : Tout ajout de contenu en cours de travail (y compris traductions et corrections) ;
* **master** : Branche de travail principale ;
* **release/XXX** : Contenu figé pour une validation avant publication, étape nécessaire pour les grosses publications, facultative pour les textes courts ;
* **public** : Contenu final publié, sur lequel des scripts peuvent être appliqués pour générer les différents formats de lecture.

De façon générale, on peut penser à des *rebase* tant qu'on n'a pas pushé sur le dépôt distant de façon à clarifier l'historique des commits.

Les branches *master* et *public* sont protégées et seuls certains collaborateurs peuvent intervenir dessus. Vous êtes donc encouragé à travailler uniquement dans une branche *WIP/…*  
Cela peut éventuellement être une branche *release/…* si vous pensez que cela débouchera sur une nouvelle publication.

### Ajout de contenu

Pour tout ajout (qita, roman, traduction), taper (pour créer le nouveau texte qita_999) :

    $ git branch WIP/qita_999
    $ git checkout WIP/qita_999

On peut envoyer le contenu de sa branche en cours sur le serveur distant en tapant :

    $ git push origin feature/qita_999

On peut récupérer aussi le contenu d'une branche distante précise :

    $ git pull origin feature/qita_999

Une fois un travail en cours (WIP) terminé, on peut le réintégrer à la branche *master* :

    $ git checkout master
    $ git merge WIP/qita_999
    $ git branch -d WIP/qita_999 # Si on veut effacer la branche qui vient d'être mergée dans *master*
    $ git push
    $ git push origin --delete WIP/qita_999 # Si on veut effacer la branche qui vient d'être mergé également sur le serveur distant

Si vous n'avez pas les droits pour pusher sur *master* sur le dépôt distant, vous pouvez faire une *merge-request* indiquant que votre travail est terminé.

## Préparation à la publication

### Faire une release

Ceci est une étape accessoire dans le cas où le travail est validé dès sortie de WIP.

Depuis la branche master, on tape les commandes suivantes pour créer et passer sur la branche de publication:

    $ git branch release/qita_999
    $ git checkout release/qita_999

On fait d'éventuelles modifications finales jusqu'à la validation. 


### Publier la release

On peut être dans deux cas de figure, avec une branche *release/qita_999* ou, si aucun travail de validation n'a été nécessaire, juste avec *master*. On tape alors les commandes :

    $ git checkout public
    $ git merge release/qita_999 # Dans le cas où une branche de release avait été créée
    $ git merge master # Dans le cas où il n'y a pas eu de branche de release

Dans tous les cas, il ne faut pas oublier de reverser dans *master* le contenu modifié/ajouté qui a été validé pour *public* :

    $ git checkout master
    $ git merge public

## Corriger des coquilles publiées

Si on veut corriger une coquille dans un texte publié, on peut le faire directement dans *master* (si on en a les droits, sinon, créer une branche *WIP/…* ou se contenter de créer une *Issue* pour que quelqu'un se charge de la correction). Puis on merge dans *public*, puis on push sur le dépôt distant.

    $ git checkout master
    $ git commit -m"Typos"
    $ git checkout public
    $ git merge master
    $ git push
