 https://commons.wikimedia.org/wiki/File:Irakischer_Maler_um_1280_001.jpg

Artist 	Zakariya Qazwini
Title 	
Deutsch: Die Wunder der Schöpfung des al-Qazwînî, Szene: Der Erzengel Isrâfîl
Date 	1280
Medium 	
Deutsch: Papier
Dimensions 	17.5 × 16.1 cm (6.9 × 6.3 in)
Current location 	
Deutsch: Freer Gallery of Art
Deutsch: Washington (D.C.)
Notes 	
Deutsch: Buchmalerei
Source/Photographer 	The Yorck Project: 10.000 Meisterwerke der Malerei. DVD-ROM, 2002. ISBN 3936122202. Distributed by DIRECTMEDIA Publishing GmbH.
Permission
(Reusing this file) 	[1]

 	The work of art depicted in this image and the reproduction thereof are in the public domain worldwide. The reproduction is part of a collection of reproductions compiled by The Yorck Project. The compilation copyright is held by Zenodot Verlagsgesellschaft mbH and licensed under the GNU Free Documentation License.
