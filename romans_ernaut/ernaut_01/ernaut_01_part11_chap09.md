## Chapitre 9

### Côte crétoise, matin du 14 octobre

Les trois enquêteurs avaient débattu un long moment la veille au soir avant de se décider sur la façon dont ils allaient interroger Enrico Maza. Bien qu'ils aient convenu de l'importance des informations à obtenir auprès de lui, ils en étaient arrivés à la solution la plus modérée. Il ne fallait pas l'aborder de face. Coupable ou innocent, c'était un homme redoutable, violent, et l'accuser directement d'être le complice et l'assassin de Fulco Bota risquait de provoquer une dangereuse réaction. L'interroger à ce propos, sans dévoiler leur hypothèse, ne pouvait demeurer sans conséquence, pour les mêmes raisons.

Ernaut se proposa pour lancer quelques questions, l'air de rien, un jour qu'il s'entraînerait avec le soldat comme il le faisait régulièrement depuis plusieurs semaines. L'adolescent pensait avoir créé une relation de confiance qui lui permettrait d'aller suffisamment loin, de collecter plus d'éléments pour prendre une décision définitive. Mais en disant cela à ses deux compagnons, il savait qu'il marchait sur des œufs, car il n'était en fait sûr de rien. Il avait passé une nuit agitée, angoissé par l'importance et la subtilité de sa mission.

Une fois sur le pont, il se prépara et s'échauffa un peu avant de rejoindre Enrico comme ils étaient convenu après le repas. Le temps était enfin revenu au beau et seul le vent soufflant dans les voiles amoindrissait les morsures du soleil implacable. En de tels cas, les deux hommes se retrouvaient à bâbord, tentant de profiter de l'ombre de la voile pour se protéger des rayons mordants. Prêt à en découdre, Ernaut se rinçait la bouche d'un mélange de vin et d'eau, cette dernière sentant trop le croupi pour être bue nature.

Enrico arriva, habillé seulement de sa chemise et de ses braies. Il avait avec lui des bâtons servant d'armes pour l'apprentissage. La plupart des entraînements se basaient sur la lutte à main nue, la meilleure des méthodes pour apprendre à se battre selon le soldat. Mais il souhaitait aussi montrer à Ernaut les différentes manières de se poster pour un combat à l'épée. Il existait un certain nombre de positions et d'enchaînements à connaître afin de vaincre les adversaires en fonction de l'attitude qu'ils choisissaient. Ernaut avait découvert à sa grande déception que sa force ne suffisait pas et que la rapidité, la dextérité et une pratique longue et fastidieuse, étaient bien plus déterminantes que des coups assénés comme un bœuf.

Malgré la chaleur, les affrontements furent une nouvelle fois intenses. Enrico ne faisait pas de cadeau à son élève, qui n'en attendait aucun. Ernaut poussait son professeur dans ses derniers retranchements dès qu'il voyait une opportunité d'affirmer sa puissance. Entre eux deux il n'y avait pas de faux-semblants : même dans l'apprentissage et les combats courtois, nul n'était là pour perdre. La victoire n'en était que plus méritoire.

La chaleur du jour augmentant régulièrement, ils se retrouvèrent rapidement en sueur, l'haleine courte, la langue sèche et les tempes bourdonnantes. Ils s'essuyèrent le visage avec les pans de leurs chemises et s'assirent à l'ombre de la passerelle du gaillard d'avant. Reprenant leur souffle, ils ne parlaient pas, se contentant de se jauger du regard et de se passer de quoi se désaltérer. Ernaut cherchait l'angle par lequel il allait aborder le sujet de l'Espagne, qu'il faudrait forcément subtil, car il savait le soldat très malin. Il fut donc un peu désarçonné par la question que ce dernier lui posa.

« Tu te décideras quand à t'enquérir auprès de moi de Fulco et du marchand ? »

Ernaut prit un air offusqué.

« Pourquoi ça ? Pour quels motifs devrais-je vous interroger ? »

Enrico fit un de ses sourires mystérieux, répercuté dans tout son visage par ses rides jusqu'à la cicatrice qui démentait tout amusement dans ce rictus.

« Parce que c'est ton rôle. Si ton chevalier, là, te l'a pas encore ordonné, ça ne saurait tarder. Sinon tu es rusé assez pour te dire que ce serait bien que je me livre à toi. Pas vrai ? »

Il avala une gorgée du pichet et le tendit à Ernaut.

«  T'inquiète ! Ça me fait guère souci. Je suis peut-être peu droit, mais j'ai de la moralité. Le vieux Bota était homme de bien. Jamais un mot plus haut que l'autre, et il connaissait sa place. Pas un preux guerrier, comme toi ou moi, mais il menait droitement sa barque, j'avais rien contre lui… »

Le soldat porta son regard au loin comme s'il y cherchait ses souvenirs et impressions. Ernaut avala une gorgée, attendant la suite.

« Le marchand, voilà un fils à putain, de ceux qui ordonnent, mais jamais ne vont planter leur glaive parmi la tripaille. Bota, lui, il en a fait l'essai ; il est revenu, fort tourmenté, vu que c'était pas sa place. Mais il n'est pas resté en retrait. Rien que pour ça, j'aurais plaisir à voir pendre son meurtrier. Alors s'il faut que j'éclaircisse quelques lanternes, j'aime autant que ce soit pour toi, parce que tu me plais, petit. »

Il donna un coup de poing amical sur le bras du géant à ses côtés et reprit le récipient de vin. Devenant rouge de confusion, Ernaut ne savait pas quoi répliquer. Il demeurait perplexe de cet accès de générosité qu'il trouvait inhabituel, même si sur le fond, il comprenait intuitivement Maza. Il essaya de remettre de l'ordre dans ses idées, voulant saisir cette opportunité.

« Au plus simple, ce serait me conter où vous étiez lors des murdriments…

— Pas bête ! Alors, que je m'en souvienne… Quand on a piqué  Embriaco, je faisais le guet. Je n'avais pas pu prendre revanche de toi de la partie de dés de la veille, souviens-toi ! J'ai donc erré à bord un peu partout avec Rufus. Depuis la grand salle, j'ai entr'aperçu les deux valets qui jouaient dans le passage. J'ai eu envie de les ennuyer un peu. On a ouï étrange bruit… Je suis allé quérir Fulco pour forcer la porte. Au retour, tu l'avais déjà fracassée.

— Et pour maître Bota ?

— Là, ça va être moins aisé. Suite à la tempête, j'étais lassé de me tremper et on s'est réfugiés en aucune cale avec quelques compaignons. Et on a percé un tonneau, histoire de ne pas demeurer oisifs ! Après, je ne sais guère. Je me suis éveillé à cause de la tribouillerie sur le pont. Ils larguaient le canot pour te repêcher. Guère complet, je l'avoue volontiers, mais voilà tout ce que je peux te conter… »

Ernaut s'adossa à la cloison et posa son crâne contre les planches de bois, faisant rouler sa tête de droite et de gauche, les yeux fermés pour mettre un peu d'ordre dans son esprit. Apparemment, Enrico ne pouvait avoir commis le premier meurtre. Mais il aurait pu tuer son complice responsable de cet assassinat. Il lui fallait encore voir comment il aborderait une autre question.

« Et les autres Génois, Platealonga, Rustico, Pedegola, ou Embriaco, vous les connaissiez ?

— Quelque peu, de renommée, à part Ansaldi Embriaco, que j'ai découvert sur le *Falconus*. Tous ceux qui ont combattu au ponant de la Méditerranée connaissent cette truandaille. De gros négociants, grenouillant à l'entour des familles consulaires. Maintenant, si tu veux savoir si je les avais encontrés en personne, la réponse est non. »

Enrico lança un fin crachat au loin, avant de tourner son regard vers Ernaut.

« C'est comme Embriaco ou le vieux Spinola, le genre pisseux refusant de crotter ses souliers brodés. On est loin de la vaillance des ancêtres ! Tous ces fils à moine ne sont que couards escouillés. S'il le fallait, je saurais les trucider sans remords, mais à ce jour, nos chemins se sont jamais entrecroisées. Chacun son monde ! »

Il se rapprocha d’Ernaut, le regard étonnamment direct et la voix réduite à un souffle de haine.

« D'ailleurs, te fais pas d'illusion, amour de seigneur n'est pas fief ! Ton chevalier et son compaing le tonsuré, ils ne voient en toi que valetaille. Ils te font sourires par-devant, mais le jour où tu cesseras de leur être utile, ils t'oublieront, comme un simple torchias. »

Il attrapa Ernaut par la nuque et se rapprocha encore de son visage, crachant les mots comme du venin qui lui brûlerait les lèvres.

« Il ne faut certes pas leur accorder confiance, encore moins qu'à un paillard tel que moi ! »

### Côte crétoise, matin du 14 octobre

Le pont supérieur fleurait bon la décontraction. Depuis qu'ils avaient atteint les abords de l'île de Crête, qu'ils voyaient au loin vers le sud, les éléments semblaient vouloir les favoriser. Un vent de travers régulier, bien que modérément puissant, assurait un minimum de travail aux marins et un soleil régnant sans partage sur le ciel réchauffait les passagers malmenés. La faible brise ne formait qu'à peine la mer, accordant un grand confort aux voyageurs, qui ne ressentaient donc que peu le mouvement des vagues. Le *patronus* avait une nouvelle fois obligeamment autorisé les passagers à se rendre aux étages supérieurs, de façon à profiter de ces cieux cléments.

Ja'fa et Sawirus de Ramathes ne s'étaient pas fait prier. Ils s'étaient aménagé un petit coin tranquille, fixant une toile depuis la plate-forme haute du château arrière pour se protéger des ardeurs de l'astre solaire, et ils avaient amené de quoi faire la sieste et se détendre en plein air. Après manger, le jeune négociant avait pris sa flûte et joué des airs qui avaient bercé la plupart des présents. Puis il avait à son tour décidé de fermer les yeux, s'abandonnant aux délices d'un repos calme, rythmé par les bruits du vent dans la voile, le grincement des cordages ou ceux de la coque fendant la houle. Les quelques hommes nécessaires au service veillaient sur une armée de ronfleurs, rêvant  paisiblement que le voyage se déroulerait ainsi jusqu'à la fin.

En milieu d'après-midi, lorsque Ja'fa sortit de son sommeil, Sawirus lui proposa de passer un peu de temps à un jeu de plateau. Ils étaient en train de placer les pions sur le tracé quand Régnier s'approcha d'eux.

« Vous allez commencer une partie d'alquerque ? »

Sawirus s'arrêta un instant de poser ses jetons et leva la tête vers le chevalier, fermant à demi les yeux pour ne pas être ébloui par la luminosité ambiante.

« Si c'est ainsi que vous nommez ce jeu, oui. Mais pour moi, ce n'est qu'*al quirkat*. On encontre tellement de formes ! Je crois que chaque contrée en a une propre. »

Soulevant des pièces qu'il venait de prendre, Sawirus le regarda.

« Vous n'êtes pas ici pour parler jeux de table, je pense…

— Certes non. Cela doit être ce beau soleil. Il éloigne soucis et incite au repos… »

Ja'fa tira un de ses coussins et le disposa au pied de Régnier.

« Prenez place. Dites-nous en quoi nous pouvons vous être utiles. »

Une fois en tailleur, Régnier resta silencieux, comme hypnotisé, étudiant la façon dont les deux adversaires poussaient leurs pions. Au bout d'un long moment, reprenant un air plus concentré, il se résolut à parler.

« J'aurais besoin de savoir si l'un ou l'autre aviez connaissance des affaires d'Ansaldi Embriaco à l'époque de l'assaut sur Damas, lorsque Gênes a armé une flotte pour l'Espaigne. Et si vous aviez contact avec Lafranco Rustico, Jacomo Platealonga ou Oberto Pedegola. »

Sawirus toussa légèrement pour s'éclaircir la voix et répondit le premier.

« Les trois derniers ne sont pas hommes à beaucoup commercer avec nous. Ce sont de puissants négociants, mais surtout avec l'Ifriqiyah[^ifriqiya]. Je n'ai jamais eu d'échanges directs avec eux ni à Jérusalem, ni à Tripoli ni en Égypte. »

Ja'fa semblait vouloir appuyer le vieux marchand et attendit que ce dernier ait fini.

« Je les connaissais de vue, comme beaucoup, pour les avoir entrecroisés à Gênes en lieux publics ou une halle ou l'autre. Mais aucun n'est jamais venu à Gibelet ou à Akka[^akka]. Et à moins qu'ils aient commissionné un des négociants usuels des autres familles, je partage l'opinion de maître Sawirus.

— Le jeune Embriaco, lui, était pur produit des comptoirs syriens. Il avait excellente connaissance de moult marchés de la côte et s'était enfoncé vers Alep, Homs et Damas. Sans compter tous ses voyages à Jérusalem même. En particulier lorsqu'il demeura plusieurs années en Terre sainte. La prime fois que je l'ai vu, il n'était que secrétaire, servant un sien oncle de Gibelet. Je m'ensouviens fort bien parce que le jouvencel s'était emporté de joie à l'annonce de l'arrivée de l'ost[^ost] allemand, au printemps 1148. Cela m'avait fort surpris venant d'un Génois. Parfois on aurait dit qu'il s'enthousiasmait plus pour la guerre que le négoce… »

Régnier se remémora à cette occasion ce qu'Ansaldi lui avait raconté alors qu'eux aussi s'affrontaient autour d'un plateau de jeu voilà plusieurs semaines.

« Il m'a conté que ses parents l'avaient élevé comme soldat, espérant peut-être quelque prestigieux destin. Il avait une foi solide en l'avenir des royaumes de Terre sainte… »

Prenant brutalement conscience de ce que Sawirus venait de lui déclarer, il lui posa la main sur l'avant-bras comme celui-ci s'apprêtait à jouer.

« Attendez ! Ansaldi Embriaco était à Gibelet en 1148 ? »

Le vieux marchand fut surpris de la violence de la question et lança un regard interdit à Régnier. Celui-ci lui lâcha le bras, l'air confus. Se massant symboliquement le poignet, Sawirus répondit.

« Pas seulement. Il hostellait là depuis une année ou deux, il est resté fort longtemps, en partie à cause du manque de nefs génoises. C'est de sûr le moment où il a le plus appris. Il a fait ses premières affaires avec un Sicilien, musulman désormais retiré ou presque, al-Manzil al-Qamah. Je l'avais introduit près certains amis, comme d'autres. Voilà raison de notre déception à le voir nous traiter par la suite comme étrangers. »

Écoutant à peine la suite de la réponse, Régnier était plongé dans un abîme d'interrogations. Il savait désormais, sans l'ombre d'un doute, que le jeune marchand assassiné n'avait aucun lien avec l'Espagne. Il n'avait même pas pu y être mêlé indirectement, étant géographiquement bien trop éloigné, et ceci pour les mêmes raisons qui avaient ruiné la famille Bota et tant d'autres. Toutes les théories savantes qu'il s'était efforcé de construire avec ses deux compagnons tombaient en morceau. Il fallait absolument qu'il remette de l'ordre dans ses idées avant de leur parler, de crainte de les voir complètement abattus.

### Au large de l'île de Dia, matin du 15 octobre

Régnier et Ernaut avaient repris leur place habituelle dans la petite cabine d'Ansaldi, chacun à une extrémité de la banquette faisant lit. Ils étaient venus directement après une messe matinale, attendant l'arrivée d'Herbelot qui avait officié en compagnie d'Aubelet. Un temps magnifique leur avait offert l'opportunité d'une belle cérémonie en plein air, rassemblant l'intégralité des marins et des voyageurs. Mais ils n'avaient pas vraiment l'esprit à cela. Le matin, le chevalier avait rapidement réuni Herbelot, Ganelon et Ernaut, et leur avait brièvement expliqué qu'il était désormais certain qu'Ansaldi n'avait rien à voir avec l'Espagne, qu'il était en Terre sainte lors des combats, et même un peu avant et après. Il fallait qu'ils partent sur d'autres pistes et que chacun y réfléchisse de son côté. Ils étaient convenu de se retrouver pour en discuter après la messe.

En attendant le prêtre, Ganelon et Ernaut avaient allumé les lampes et patientaient en silence, comme gênés. L'adolescent jouait avec la flamme et la mèche situées sur l'étagère un peu au-dessus de lui. Régnier semblait perdu dans ses pensées, suivant du bout de l'index les volutes dessinées par les broderies sur la magnifique cotte de soie qu'il avait revêtue pour la cérémonie. Habituellement il aimait être rasé de frais, habillé de propre, et cela suffisait à le mettre de bonne humeur. Là, il était maussade car il craignait d'avoir fait fausse route depuis deux semaines et que l'affaire demeure à jamais un mystère.

Un bruit de pas, rapides et légers, précéda l'arrivée d'Herbelot accompagnée d'un frottement d'étoffes. Lorsque le clerc apparut dans l'embrasure de la porte, les lumières accrochèrent les plis de son vêtement. Lui aussi avait revêtu sa plus belle tenue. Il eut un sourire un peu crispé en voyant les regards se tourner vers lui, ayant l'impression de déranger. Il attrapa un tabouret et, gêné d'être ainsi le perturbateur, il faillit commettre l'erreur de s'y asseoir immédiatement, sans même l'essuyer. De nouveau,  seuls les craquements du *Falconus*, les voix lointaines des matelots, le murmure étouffé des passagers de la salle commune se firent entendre dans la cabine durant un long moment.

Tout à coup, presque brutalement, Régnier se releva et se rapprocha du bord du lit. Sa tête était éclairée en contrejour et seuls ses yeux brillaient à la lueur de la seconde lampe. Sans prendre le temps de faire une entrée en matière, il s'ouvrit directement à ses compagnons, continuant à voix haute les réflexions qu'il menait jusque-là silencieusement.

« Le chemin le plus droit s'est avéré ne mener nulle part. Et que nous reste-t-il ? Peu ou prou une paire de semaines pour démêler cet écheveau. J'ai donc longuement estimé la meilleure façon d'avancer. Alors, je me suis souvenu comment j'avais retrouvé mon portfolio. »

Ernaut et Herbelot, ignorants de l'anecdote, se regardèrent, dans l'expectative, à deux doigts de se demander si Régnier ne commençait pas à divaguer. Mais le chevalier continuait son monologue.

« Je vais plus ou moins écouter le conseil de Mauro Spinola, qui n'est guère loin des vues de maître Malfigliastro. Il nous faut  considérer les événements encontrés sur la nef, s'efforcer de retracer les ultimes moments du marchand. Le murdrier d'Ansaldi nous mènera à celui du charpentier. Rien n'est fortuit en cette affaire ! Meurtrir quelqu'un à bord d'un navire est fort risqué, car on est captif avec sa victime, les témoins et les poursuivants. Adoncques il fallait qu'Ansaldi meure céant, sans délai. »

Herbelot se gratta la tête, comme s'il énonçait une idée venant de lui apparaître.

« Pour ma part, j'ai pensé tantôt à ce que vous nous avez conté au matin. Et si le meurtre de maître Embriaco était une erreur ? Vu qu'il semble seul sans lien avec l'Espaigne ? Ce qui me pose problème, c'est toute cette préparation que vous évoquez, qui fait du murdrier un fripon posé et intrigant, que je ne vois guère commettre pareille erreur. Ou bien espérait-il juste nous plonger dans le brouillard… Et il aurait meurtri le charpentier du navire pour semblables motifs ? »

Marquant un silence, le petit groupe réfléchit à la façon dont il pouvait reprendre ses recherches. À son tour, Ernaut se redressa comme un ressort, se tenant le front de la main après y avoir claqué la paume d'un mouvement vif.

« Et si, en fait, maître Bota avait tout compris ? Quelques jours avant sa mort, il avait discouru sur l'importance selon lui de voir maître Embriaco vengé. Il a peut-être voulu convaincre le coupable de se livrer, et zac ! Celui-là lui a planté son fer droit au cœur ! »

Régnier opina du chef.

« Possible. Comme il n'a interrogé personne, du moins à ma connaissance, c'est qu'il a dû noter quelque chose et comprendre alors l'histoire.

— Pourquoi ne pas suivre ses traces, en ce cas ? rétorqua Ernaut. Reprendre la piste depuis le début, en partant de choses certaines, à savoir la cabine telle qu'elle était. Nous pourrions ainsi retrouver l'endroit comme le charpentier l'a vu. Il sera plus aisé de mettre nos pas en les siens. »

Ernaut marqua un temps, hésitant à aller plus loin. Il finit par déclarer, d'une voix qui s'excusait presque :

« Depuis le début, je n'arrive à comprendre par quelle magie un larron peut ainsi entrer et sortir sans ouvrir aucune porte. Et pourtant cela ne semble étonner personne outre moi ! »

Comme atteint par une révélation, Régnier se releva brusquement et avisa ses compagnons interloqués.

« De vrai ! Tu as raison, Ernaut, c’est une évidence ! Arrêtons de penser en rond, agissons ! Amenons quantité de lumière ici, employons-nous à recréer les derniers instants du mort. »

Le chevalier se tourna vers l'adolescent et lui demanda :

« Vois si maître Sawirus pourrait nous joindre. »

Puis il lança vers le couloir :

« Ganelon ! Occupe-toi de quérir le jeune esclave de maître Sawirus ! »

Alors que son valet s'éloignait dans le passage, le chevalier se pencha par l'huis, par-dessus Herbelot, et cria à l'intention de son serviteur.

« Et après organise-toi avec Ugolino, que toutes les possessions qui étaient en la cabine d'Ansaldi Embriaco soient ramenées au plus vite ! »

Lorsqu'il se retourna, Ernaut était debout, le visage radieux, heureux de l'estime accordée, impatient d'avoir à faire autre chose que cogiter. Tentant de se faufiler derrière Herbelot, il manqua le renverser dans sa précipitation et partit en galopant dans le couloir.

« Quelle brute ! couina Herbelot.

— Si fait, mon ami, mais certes pas sans cervelle. Il a su voir une lueur en toute cette obscurité où nous étions perdus, vous et moi… »

Légèrement offusqué, le clerc piqua du nez et s'épousseta de la crasse imaginaire que ce frottement intempestif avait déposée sur sa tenue. Puis il se leva et rangea proprement le tabouret sous la table, toujours sans émettre la moindre parole, l'air pincé. Ayant retrouvé une contenance, il s'accorda une moue d'autosatisfaction et prêta de nouveau attention à Régnier. Ce dernier était debout sur le lit, une lampe à huile à la main, examinant chaque recoin du plafond et le son de chaque planche avec le manche de son couteau. D'étonnement, le clerc leva un sourcil.

« Que faites-vous, ami ?

— Je cherche par où le fripon s'est échappé. Ernaut dit le vrai, nous devons trouver la manière dont les choses sont arrivées. Comme le murdrier n'est qu'un homme, il n'a guère pu passer au travers des planchers ou des cloisons. Il a donc forcément laissé trace de son passage. »

Finalement convaincu par ce simple bon sens trop longtemps mis de côté, le clerc sortit un petit couteau qu'il conservait dans un sac toujours à son épaule, et avec lequel il taillait ses plumes. Il allait s'en servir pour sonder les cloisons lorsqu'il avisa le manche de buis ouvragé. Arrêtant son geste, il se morigéna intérieurement, le rangea soigneusement à sa place, puis entreprit de frapper de son index sur les pièces de bois.

### Au large de l'île de Dia, après-midi du 15 octobre

Après une demi-journée d'inspection de la pièce, ainsi que des affaires du marchand, le groupe en était au même point. Aucun passage caché, panneau amovible ou trappe n'avait été découvert. Régnier examinait l'entrée en détail, la lampe à la main. Tout était de chêne, solidement bâti de planches verticales clouées avec des chevilles à des traverses. En haut et en bas, un ergot pénétrait dans l'encadrement pour faire pivot, l'huis rentrant dans une feuillure lorsqu'il était fermé. Un loquet, simple targette de bois, servait à l'origine à l'assujettir, glissant dans deux passants sur la porte et un sur le dormant, tous fermement fixés par des fiches. Le dernier avait été arraché par Ernaut quand il était entré de force dans la cabine. Machinalement, le chevalier essayait de vérifier si les pièces correspondaient bien.

« Dis-moi garçon, es-tu certain que l'ouverture était bloquée de l'intérieur, par ce loquet ?

— Oc, je me suis fait dolor à l'épaule à taper parmi. J'ai dû m'y prendre à deux fois, outre le valet qui avait commencé à l'enfoncer. Sans grand succès il est vrai, elle branlait à peine quand je suis arrivé. »

Régnier se mordit la lèvre, cherchant la faille dans cette apparente suite d'évidences. Il fallait s’efforcer d'être logique. Le meurtrier n'était qu'un homme. Comme il n'y avait aucune autre possibilité de sortie, il avait dû emprunter la porte une fois son forfait accompli. Et pourtant elle était fermée de l'intérieur ! Cela voulait peut-être dire que la victime avait elle-même poussé la clavette avant de succomber à ses blessures. Auquel cas elle ne désirait pas que son assassin fût découvert et acceptait le châtiment. Cette éventualité ébahit le chevalier. Cela pouvait tout à fait correspondre à la mentalité du Génois qui semblait chercher à expier une faute. L'argent y était possiblement lié.

Les témoins les plus directs jouaient dans le couloir ou dormaient à côté. Il allait une nouvelle fois interroger Sawirus de Ramathes, Ugolino et Yazid, de façon à voir si des éléments ne leur seraient pas revenus en tête.

« Vérifiez les endroits remirés par un autre. Il faut s'assurer que le seul accès ici soit cette porte. »

Puis il sortit dans le couloir, où patientaient Ugolino et Yazid, Sawirus étant certainement dans sa chambre. Les deux valets, assis au sol, se levèrent quand il apparut et attendirent, le regard interrogateur. Régnier frappa sur la cloison pour que le marchand les rejoigne.

« Cette fois, nous œuvrons à retracer les faits depuis l'après-midi avant le murdriment d'Ansaldi Embriaco. Maître, vous voulez bien commencer ? »

Le vieil homme se gratta la tête un instant puis ses yeux pétillèrent alors qu'il commençait à égrener ses souvenirs, comme si la remémoration de sa vie l'amusait au plus haut point.

« Volontiers. Je suis revenu à bord vers la fin du jour et suis passé voir le cuisinier pour m'enquérir des plats. J'avais acheté quelques douceurs à terre et je n'avais plus très faim. Mais il n'y avait personne alors je suis retourné à ma cabine, où j'avais demandé à Yazid de demeurer tandis que nous étions amarrés. Avec l'histoire de la veille, je ne voulais pas courir de risque. À partir de là, j'ai pris quelque repos puis j'ai soupé, légèrement. Fatigué par une journée à errer dans Otrante, je me suis vite encouché. Les cris d'Ugolino m'ont éveillé…

— Vous n'avez rien ouï entre votre retour en cabine et votre coucher ?

— Le lieu est fort bruyant. Les pas au-dessus, les passagers revenus de terre, les grincements variés… auxquels on s'habitue et qu'on ne remarque plus guère. En tout cas, je ne m'ensouviens pas de la moindre chose étrange sur le moment. »

Régnier se tourna vers le jeune Yazid, qui commençait visiblement à s'impatienter, levant les yeux au ciel et dansant sur place.

« Et toi, jeune homme, qu'as-tu fait ?

— J'ai patienté tout l'après-midi durant dans la chambre et, au retour du maître, j'ai respiré un peu l'air sur le château de proue. Après quelque temps je suis allé quérir le repas. J'ai discuté un peu avec plusieurs matelots occupés là à manger. Une fois le souper servi, nous avons entrepris de jouer avec Ugolino dans le passage. À un moment, le sergent tricheur est venu nous embêter, et c'est là qu'il y a eu le bruit et qu'on a tenté de forcer la porte.

— Tu n'as rien vu de louche ?

— À part le soudard, non.

— Comment ça ?

— Le *patronus* avait annoncé que les soldats feraient les rondes de paire, mais lui était seul. »

Régnier s'interrogea, savoir si cela n'était pas commandé par le ressentiment du jeune homme. Mais la remarque était néanmoins pertinente, car cela indiquait que Maza n'avait personne pour prouver où il était, contrairement à ce qu'il avait laissé entendre à Ernaut. Il se tourna vers Ugolino.

« Et toi ?

— Maître Embriaco avait requis son souper comme d'usage, en fin d'après-midi. Lorsque j'ai porté son repas, il était couché et m'a dit de repasser pour desservir. Je suis remonté jusqu'au chastel de poupe, d'où j'ai miré la nuit s'installer en la cité. Puis je suis retourné pour débarrasser. Ce qui m'a surpris, c'est que maître Embriaco enfilait une cotte à mon entrée. Il n'a pas voulu mon assistance et m'a donné congé du soir. Adoncques, j'ai rangé les affaires en ma huche et on a commencé à jouer avec Yazid, jusqu'à ce que le sergent d'armes s'en vienne. Alors qu'ils se bestenciaient[^bestencier] tous deux, j'ai ouï comme un léger râle, ou grognement, ensuivi d'un choc, tels deux bâtons heurtés l'un à l'autre. Mais très vivement et peu fort. J'ai eu l'impression que ça sortait de la cabine du maître. J'ai appelé, sans obtenir réponse. Alors, j'ai tenté d'enfoncer l'huis. Le grand gaillard est finalement parvenu à l'ouvrir, avec moult difficultés. Voilà, le reste, vous le savez déjà… »

Saluant les trois hommes d'un signe de tête après les avoir remerciés, Régnier entra de nouveau dans la cabine d'Ansaldi. Là, Ernaut examinait le sol le nez collé au plancher, pendant qu'Herbelot, debout sur la table, vérifiait du bout des doigts la fixation d'une planche donnant sur la cale adjacente. Le chevalier ne put réprimer un sourire amusé à la vision de ces deux fins limiers, dont l'un, tel un gracieux lévrier, auscultait avec une délicatesse pleine de mesure tandis que l'autre reniflait comme un féroce mâtin en chasse. Heureusement qu'il avait de tels compagnons, se dit il, car sans eux et leur entrain il aurait peut-être abandonné depuis longtemps.

### Côte crétoise, midi du 16 octobre

La chaleur était étouffante pour la période de l'année. De plus, ce jour-là, le vent était quasi nul et il semblait à tous que le navire faisait du sur-place. Au zénith, l'astre du jour régnait sans partage, éblouissant sur une mer polie comme un miroir. Au sud, la côte rocheuse qu'ils apercevaient avait perdu toute couleur, étincelant d'un blanc immaculé pratiquement depuis l'aube. Régnier avait préféré supporter les odeurs nauséabondes de l'entrepont plutôt qu'affronter les ponts supérieurs écrasés de soleil. Mais même là, la température commençait à être difficile à endurer, incitant les personnes à bord à rester allongées, à faire la sieste et à se reposer, en attendant que l'air redevienne respirable. Il avait envisagé un temps de proposer à Herbelot de faire une partie d'un jeu quelconque, mais s'était ravisé, incapable qu'il se sentait de réfléchir par une telle chaleur. L'absence de vent, plus que tout, rendait l'atmosphère irrespirable. Il se demandait si cela ressemblait à ça, l'Enfer. À chaque fois qu'il devait supporter de pareilles conditions, assez banales en Terre sainte, il s'interrogeait sur les sensations qu'avaient les damnés dans les abîmes infernaux. Il fallait avoir connu le souffle brûlant des bourrasques de poussière dans les étendues arides de Syrie pour comprendre que la chaleur n'était pas toujours un bienfait, ce dont ne pouvaient se douter à l'origine les gens comme lui, originaires de Picardie et du nord de l'Europe.

Comme faisant écho à ses pensées sur le climat des Enfers, Aubelet avança sa tête à l'angle de la cabine de toile de Régnier, demandant par un sourire à être entendu.

« Que désirez-vous, mon père ?

— J'ai su que vous cherchiez à revoir l'enchaînement des faits en la chambre du malheureux négociant. Je crois avoir connaissance d'utiles éléments pour vous. »

Régnier s'installa de façon moins négligée et l'invita de la main à s'asseoir à ses côtés.

« Je suis tout ouïe.

— Voilà : j'ai pris soin de la dépouille du malheureux. J'ai par malheur habitude de préparer mes ouailles lorsque Dieu les rappelle à lui. C'est pour cela que je n'arrive à comprendre quelques détails… fort étranges.

— De quel ordre ?

— Je ne sais si vous êtes familier de pareilles choses, mais les dépouilles changent une fois l'âme au loin. Le sang se serre dans les parties basses et la peau devient blanche et glacée. Au bout de quelque temps, il devient malaisé de plier un bras. Cela s'évanouit après quelques jours généralement. »

D'un bruit de gorge, le chevalier marqua son approbation et son attention. Le prêtre était visiblement embarrassé et commença à bafouiller avant d'exprimer le fond de sa pensée.

« Ce sont tous ces changements usuels qui me troublent… Plus j'y pense, plus je suis assuré que le pauvre Embriaco était mort depuis longtemps quand je l'ai pris en charge avec maître Gonteux. »

Régnier écarquilla des yeux ronds, stupéfait par la révélation du prêtre.

« Vous croyez qu'il ne venait pas d'être poignardé ?

— Je le crains. Je dirais que cela faisait une demi-journée, au bas mot, qu'il avait quitté ce monde. Il était froid lorsque nous l'avons trouvé, et déjà roide, d'où cette horrible posture. »

N'écoutant plus qu'à demi le prêtre, Régnier remettait soudain plusieurs pièces à leur place dans la partie d'échec qu’il disputait depuis plusieurs semaines contre le mystérieux assassin.

« C'est pour cela qu'il y avait des traces de sang dans le lit. Le cadavre y avait été posé entretemps !

— Pardon ?

— Non, rien, ne prenez garde à mes paroles… Se pourrait-il qu'il ait été occis, disons un jour avant ?

— Oui, bien que je ne puisse en jurer. Pourquoi une journée plutôt qu'une demie, si cela n'est pas trop vous demander ?

— Mais parce que, un jour plus tôt, nous avons eu un matelot aplomé par un inconnu, mon père ! Par le meurtrier cherchant à s'échapper ! Il a dû faillir, vu qu'il lui a fallu poignarder le charpentier quelques jours après… »

Les yeux brillants d'excitation, Régnier trépignait quasiment, heureux d'avoir enfin progressé dans son enquête. Bien que cela ne lui ait pas donné la solution du problème pour autant, il avait l'impression d'avoir fait un énorme bond en avant. Il savait désormais que l'assassinat avait probablement eu lieu la veille, et que le coupable, empêché par Octobono de s'enfuir, avait dû improviser une mise en scène afin de se fabriquer un alibi pour le moment où il ferait croire au meurtre.

« Mon père, il est vital que vous ne répétiez cela à personne, absolument personne ! Votre vie en dépend ! Vous n'avez rien dit à quiconque ?

— Quelques mots à maître Gontheux, qui m'a incité à vous conter mon histoire. Mais, même à lui, je n'ai pas tout éclairci. Lorsqu'il a su que cela concernait la dépouille, il n'a guère tenu à en apprendre plus et m'a conseillé de venir vous voir droitement.

— Parfait ! Je vous rends grâces, votre aide a été maîtresse, soyez-en assuré. »

Rasséréné par ces paroles flatteuses, le prêtre salua Régnier et s'éloigna sans bruit, d'un pas léger, le sourire aux lèvres. Le chevalier, de son côté, était également très satisfait. Pour la première fois depuis le début de son enquête, il avait la sensation de posséder un atout, d'avoir accompli une avancée décisive, à l'insu de l'assassin. Il fallait absolument exploiter son avantage sans rien dévoiler et faire le point avec Herbelot et Ernaut pour établir un emploi du temps des suspects la première nuit à Otrante. Un nouvel entretien avec Ugolino ne serait pas de trop non plus par la suite, pour apporter quelques éclaircissements sur ses déclarations.

### Golfe de Mirabellou, fin de matinée du 17 octobre

Ernaut posa le dernier coffre à sa place, sous la banquette faisant lit. Depuis le déjeuner, ils s'étaient employés, avec Régnier et Herbelot, à réorganiser la cabine d'Ansaldi Embriaco comme ils l'avaient trouvée la nuit du meurtre. Ils avaient passé en revue les différents alibis des personnes éventuellement impliquées et voulaient désormais vérifier quelques hypothèses. Herbelot s'était concentré sur la disposition des lampes à huile, la répartition des lumières étant essentielle pour valider leurs idées. Régnier finit d'étaler les couvertures sur le lit, se recula vers l'entrée et arbora un air satisfait.

« Je crois que nous sommes prêts. Qu'en pensez-vous ? »

Ses deux compagnons prirent un instant pour examiner la pièce sous tous les angles, opinant du chef au fur et à mesure en signe d'approbation.

« Fort bien ! Voyons où nous mène cette idée. »

Régnier sauta dans le lit et se roula dans les draps. Herbelot et Ernaut sortirent et passèrent la tête depuis l'extérieur, inspectant la scène avec attention, en particulier le visage de Régnier. Celui-ci leur parla de dessous les couvertures, d'une voix un peu étouffée.

« Alors ? Si je dis : “Déposez tout cela sur la table et retournez plus tard, je n'aurai pas besoin de vous après”, pourriez-vous croire que c'est Ansaldi ? »

Les deux compères à l'entrée se regardèrent, séduits par la proposition. Ernaut voyait désormais où le chevalier voulait en venir. Herbelot confirma à l'intention de Régnier.

« C'est tout à fait croyable. Il est impossible de reconnaître la personne couchée en le lit, tel que vous êtes, et la voix peut être celle d'aucun. »

Régnier se retourna brusquement, l'air enjoué.

« Voilà comment abuser valet. Et la seconde fois, le meurtrier passait un vêtement pour se masquer, grâce à l'étoffe. »

Ernaut resta un moment le regard fixe, intrigué.

« Mais, en ce cas, où était le corps ? Ugolino aurait dû le voir ! »

— C'est là qu'est vrai sacrilège, garçon. Je suis certain qu'il a été blotti sous la banquette, derrière les coffrets. En tirant la malle pour l'y glisser, quelque sang s'est accroché en l'arête. »

Ernaut fronça les sourcils, et ne put s'empêcher de répondre.

« Mais il n'y en avait pas en cet endroit, vous me l'avez conté vous même.

— Certes, il a pu y être disposé alors qu'il était occis depuis un moment. »

La voix posée d'Herbelot se fit entendre, après un instant de réflexion.

« En ce cas, la flaque devait être sèche lorsque le coffre a été bougé, voyons ! »

Le sourire de Régnier s'effaça soudain de son visage.

« Vous avez raison, cela ne peut être ainsi ! »

Herbelot se frotta le visage, sentant qu'ils étaient proches de la solution.

« Je n'ai pas dit qu'on n'avait pas recelé le corps sous la banquette, seulement que le sang sur ce meuble y a été déposé, encore frais, plus avant dans la journée. Sinon vous n'auriez pas trace. »

Attentif aux hypothèses émises, Ernaut essayait de replacer les événements dans l'ordre au fur et à mesure que les idées étaient avancées, réfutées ou élaborées. Il commençait à s'y perdre et chercha à clarifier le tout.

« Si on reprend tout à rebours, on a de prime le marchand frappé à sa table, ou juste devant, d'un poignard parmi le dos. Sa dépouille est ensuite couchée dans le lit, où du sang court. Puis passée sous cette même couche, pour, de fin, le reposer là où il avait été meurtri, c'est bien ça ?

— Exactement », s'exclama Herbelot, manifestement agacé par cette interruption.

Mais son air renfrogné s'éclaira peu à peu tandis qu'il répétait : « Exactement ! » plusieurs fois. Régnier patienta, désireux que le clerc leur expliquât ce à quoi il était en train de penser.

« Comprenez, si maître Embriaco a été occis de dos, c'est qu'il n'avait méfiance de son meurtrier. Voilà un fait essentiel. Il lui a forcément ouvert, l'espérait peut être. Imaginons qu'il avait quelques monnaies à lui remettre, raison pour laquelle le meurtrier savait où fouiller. Et c'est en hissant le coffret à lui, dans le sang encore frais, que ce maudit en a taché la tranche. »

Régnier s'assit dans le lit, l'air songeur.

« Il est vrai que nous n'avons jamais pensé la façon dont le murdriment lui-même s'est passé. Cela pourrait être quelque négoce mal fini, le contact d'Ansaldi l'occit, cherche à fuir avec les pièces, en est empêché par le matelot. Il prend refuge ici et doit trouver quelque ruse pour ne pas être découvert. Adoncques il fait croire à chacun que le négociant est vif une journée outre, temps pour lui de trouver solution. »

Herbelot avait les yeux d'un hibou, sa bouche pincée semblant minuscule dans son visage rond.

« Quelle atroce monstruosité ! Ce larron n'a-t-il donc aucune âme pour commettre pareils méfaits ! »

Ernaut haussa les épaules, déjà rallié à l'idée du chevalier.

« Et pourquoi pas ? Cela expliquerait qu'il ait meurtri Fulco Bota. Il savait peut-être quelque chose. »

Songeant à ce qu'il venait de dire, Ernaut réfléchit un instant, indécis quant à son hypothèse. Puis la vérité lui apparut soudainement.

« Sur les pièces, tout simplement ! Si elles sont la raison de la meurtrerie de maître Embriaco, le coquin n'avait nul désir de les perdre. »

Herbelot lança un regard soudain admiratif au colosse à ses côtés, souriant amicalement.

« Mais dis-moi garçon, commencerais-tu à employer ton chef pour autre chose qu'y poser chapeaux ? »

Ernaut rougit sous le compliment, peu habitué à entendre le clerc en faire. Régnier fit une moue tout en acquiesçant doucement, semblant adhérer au point de vue.

« Si fait, ce serait naturel. Il nous faudrait soumettre ce brillant échafaut au valet d'Ansaldi, sans néanmoins trop en dévoiler. »

Ernaut fronça les sourcils un instant puis haussa les épaules, comme si l'évidence lui était apparue :

« Attendons l'endemain et faisons dire que c'est pour ranger de nouveau la cabine, lâchant ce jour d'hui la rumeur que nous continuons à chercher, sans parvenir à rien. »

### Entre Crête et Kassos, matin du 18 octobre

Lorsque Ugolino cogna à la porte, il se sentait nerveux. Il n'aimait pas revenir à la cabine, et aujourd'hui encore moins. Ernaut l'avait conduit et lui avait dit de frapper et d'entrer quand on lui ordonnerait de le faire. Dans le couloir, il avisa le clerc indiquant la présence proche de Régnier d'Eaucourt, fermant le passage de l'autre côté. Herbelot fit un sourire amical au valet, mais ne lui parla pas. Hésitant, Ugolino cogna plusieurs fois sur le chambranle, la porte n'étant plus bloquée, avec sa targette cassée. Une voix étouffée lui parvint de l'intérieur, lui disant d'entrer.

Il poussa l'huis et fut un instant comme choqué par ce qu'il voyait. La cabine semblait être de nouveau occupée par son maître. Les affaires étaient toutes à leur place et deux lampes brillaient, comme souvent. Et, le plus surprenant, dans le lit, une forme allongée lui rappelait Ansaldi Embriaco. Lançant un regard apeuré vers les personnes dans le couloir, sans réaction de leur part, il avança une main tremblante vers le dormeur. En lui se mélangeaient de nombreux sentiments confus, mais surtout régnait une grande indécision quant à ce qu'il allait voir. Il osa timidement appeler « Maître ? »

Ce fut le moment que Régnier choisit pour se retourner, en soulevant la couverture. Il arborait un sourire ravi. Ugolino resta bouche bée un instant, sa respiration semblant s'être arrêtée. Régnier eut soudain peur que le choc ait été trop fort pour le vieil homme et héla ses compagnons. Tandis qu'il descendait du lit, Ernaut avait déjà poussé le siège près du serviteur, qui s'y laissa tomber, les jambes tremblantes. L'air victorieux du chevalier s'était mué en un masque d'inquiétude. Ugolino finit par déglutir avec difficulté et se passa la main sur le visage, encore manifestement secoué par l'émotion. Régnier sentait qu'il lui fallait expliquer les raisons de cette macabre mise en scène.

« Je suis désolé de t'avoir ainsi apeuré, mais c'était nécessaire. Tu as été abusé le jour où nous relâchions à Otrante. Ce n'était pas ton maître en la couche, c'était le meurtrier qui te faisait acroire qu'Ansaldi était toujours vif. »

Entretemps, Ernaut avait rempli un gobelet à l'intention du valet et le lui avait tendu. Ugolino avala un peu de vin, à gorgées lentes et mesurées, battant des yeux comme un animal effrayé. Il finit par réagir.

« Je vois. Fort convaincant, en ce cas. J'ai eu l'impression que maître Embriaco ressuscitait d'entre les morts… ou que toute cette histoire n'était que malheureux songe !

— Sois assuré que je ne cherche à te tourmenter. Cela me sert à découvrir la vérité et démasquer le démon. »

Ugolino acquiesça en silence, le nez dans son gobelet. Le chevalier enchaîna.

« En outre, ce n'est pas fini pour toi. Tu n'as rien noté d'étrange en cette pièce ?

— Que vous avez tout replacé comme avant ?

— À part ça, ne vois-tu rien qui te heurte, rapport à la manière dont Ansaldi rangeait sa cabine ? »

Le valet tourna la tête de droite et de gauche, cherchant dans les ombres mouvantes les choses qu'il n'aurait pas remarquées.

« Non, tout est bien en place ! »

Régnier baissa les yeux.

« Alors, c'est bon, tu peux sortir, Ganelon. »

Une voix assourdie s'échappa de sous le lit.

« Merci messire, je commençais à estouffer en cette poussière. »

Et, poussant le coffre devant lui, ainsi que quelques sacs, le serviteur de Régnier apparut, toussant et soufflant tandis qu'il s'extrayait de sa cachette avec l'aide d'Ernaut. Le domestique d'Ansaldi ouvrit des yeux ronds.

« Mais que faisait-il là-dessous ?

— Le fripon, lorsqu'il a singé ton maître, a celé le corps en cet endroit. Habitué à cette pièce et occupé à ton labeur, tu n'as guère prêté attention. Tu n'avais aucune raison de le faire, d'ailleurs. »

Ugolino hocha la tête d'un air las, ces épreuves l'éprouvant visiblement.

« Et tu n'as pas non plus remarqué que le sol avait été escuré, car il est fort mal éclairé. Tu n'as donc peut-être pas vu le sang lorsque tu as porté le repas puis débarrassé la table. »

Le vieil homme se pencha en avant, cherchant à discerner le plancher dans la pénombre grise tracée par les flammes des lampes.

« Vous croyez alors que j'ai en fait servi le meurtrier de mon maître toute cette dernière journée ? Et je n'ai rien vu ? Honte sur moi !

— Ne te fustige pas. Tu n'avais aucune raison de te méfier. Et tes réactions ce jour d'hui m'ont été fort précieuses. Tu n'as rien à te reprocher. »

Il se tourna vers son valet, affairé à tenter de se débarrasser des toiles d'araignées et des poussières qu'il avait ramassées en rampant sous la banquette.

« Occupe-toi de lui. Fais-lui servir bon repas et du vin, de ma réserve. Et veille à ce qu'il ne manque de rien. »

Puis, s'adressant à Ugolino.

« Sache que j'ai intention de te louer. Si tu l'acceptes, tu entreras à mon service dès les affaires d'Ansaldi remises à sa famille à Gibelet. Tu pourras y demeurer le temps de voir ce que tu aspires à faire, t'en retourner à Gênes ou rester au royaume de Jérusalem. D'ici là, veille à ne pas dévoiler nos recherches. Le secret nous est nécessaire allié pour prendre le félon. »

Le vieil homme hocha la tête, accablé, mais il esquissa un timide sourire et ses yeux semblaient avoir retrouvé un semblant de vie lorsqu'il quitta la pièce.
