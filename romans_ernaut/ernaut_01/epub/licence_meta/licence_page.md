
<p class="qita_licence"><span class="qita_scission">❖ ❖ ❖</span></p>

<p class="qita_info_licence2">Plus d'informations sur le cycle de textes courts dans le monde des enquêtes d'Ernaut de Jérusalem sur le site de la série Hexagora :  
<http://www.hexagora.fr> / <http://www.ernautdejerusalem.com></p>

<p class="qita_licence"><span class="qita_scission">❖ ❖ ❖</span></p>

<p class="qita_info_licence2">Texte mis à disposition sous la licence</p>

<p class="qita_info_licence2">[Creative Commons BY - SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.fr)</p>

<p class="qita_info_licence2">![Licence Creative Commons BY - SA 3.0](licence_meta/by-sa.png)</p>

<p class="qita_info_licence2"><b>Attribution</b> -- Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Œuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Œuvre.</p>

<p class="qita_info_licence2"><b>Partage dans les Mêmes Conditions</b> -- Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Œuvre originale, vous devez diffuser l'Œuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'Œuvre originale a été diffusée.</p>
