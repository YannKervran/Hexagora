\select@language {french}
\contentsline {section}{Plan et trajet du Falconus}{v}{figure.caption.2}
\contentsline {section}{Liste des personnages}{ix}{section*.5}
\contentsline {chapter}{\numberline {1}Prologue}{1}{chapter.0.1}
\contentsline {section}{\numberline {1.1}Monast\IeC {\`e}re b\IeC {\'e}n\IeC {\'e}dictin de la Charit\IeC {\'e}-sur-Loire, hiver 1223}{1}{section.0.1.1}
\contentsline {chapter}{\numberline {2}~Chapitre 1}{5}{chapter.0.2}
\contentsline {section}{\numberline {2.1}G\IeC {\^e}nes, soir d'orage, \IeC {\'e}t\IeC {\'e}~1156}{5}{section.0.2.1}
\contentsline {section}{\numberline {2.2}G\IeC {\^e}nes, matin du 31 ao\IeC {\^u}t 1156}{8}{section.0.2.2}
\contentsline {section}{\numberline {2.3}G\IeC {\^e}nes, apr\IeC {\`e}s-midi du 31 ao\IeC {\^u}t}{13}{section.0.2.3}
\contentsline {section}{\numberline {2.4}G\IeC {\^e}nes, d\IeC {\'e}but de soir\IeC {\'e}e du 1\textsuperscript {er} septembre}{17}{section.0.2.4}
\contentsline {section}{\numberline {2.5}G\IeC {\^e}nes, soir\IeC {\'e}e du 1\textsuperscript {er} septembre}{24}{section.0.2.5}
\contentsline {section}{\numberline {2.6}C\IeC {\^o}te ligurienne, matin du 2 septembre}{28}{section.0.2.6}
\contentsline {section}{\numberline {2.7}C\IeC {\^o}te toscane, matin du 3 septembre}{31}{section.0.2.7}
\contentsline {section}{\numberline {2.8}C\IeC {\^o}te du Latium, apr\IeC {\`e}s-midi du 5 septembre}{34}{section.0.2.8}
\contentsline {chapter}{\numberline {3}Chapitre 2}{39}{chapter.0.3}
\contentsline {section}{\numberline {3.1}Port romain d'Ostie, soir du 6 septembre}{39}{section.0.3.1}
\contentsline {section}{\numberline {3.2}C\IeC {\^o}te du Latium, journ\IeC {\'e}e du 8 septembre}{42}{section.0.3.2}
\contentsline {section}{\numberline {3.3}C\IeC {\^o}te campanienne, fin d'apr\IeC {\`e}s-midi du 8 septembre}{46}{section.0.3.3}
\contentsline {section}{\numberline {3.4}C\IeC {\^o}te campanienne, soir\IeC {\'e}e du 9 septembre}{51}{section.0.3.4}
\contentsline {section}{\numberline {3.5}Baie de Naples, soir du 10 septembre}{55}{section.0.3.5}
\contentsline {section}{\numberline {3.6}Port de Naples, matin du 11 septembre}{58}{section.0.3.6}
\contentsline {section}{\numberline {3.7}Port de Naples, fin de journ\IeC {\'e}e du 11 septembre}{62}{section.0.3.7}
\contentsline {chapter}{\numberline {4}~Chapitre 3}{67}{chapter.0.4}
\contentsline {section}{\numberline {4.1}~Golfe de Salerne, journ\IeC {\'e}e du 13 septembre}{67}{section.0.4.1}
\contentsline {section}{\numberline {4.2}C\IeC {\^o}te campanienne, matin du 14 septembre}{71}{section.0.4.2}
\contentsline {section}{\numberline {4.3}C\IeC {\^o}te calabraise, journ\IeC {\'e}e du 15 septembre}{75}{section.0.4.3}
\contentsline {section}{\numberline {4.4}C\IeC {\^o}te calabraise, soir du 15 septembre}{79}{section.0.4.4}
\contentsline {section}{\numberline {4.5}Golfe de Gioia, midi du 16 septembre}{82}{section.0.4.5}
\contentsline {section}{\numberline {4.6}Golfe de Gioia, soir du 16 septembre}{86}{section.0.4.6}
\contentsline {section}{\numberline {4.7}D\IeC {\'e}troit de Messine, fin d'apr\IeC {\`e}s-midi du 17 septembre}{90}{section.0.4.7}
\contentsline {chapter}{\numberline {5}Chapitre 4}{95}{chapter.0.5}
\contentsline {section}{\numberline {5.1}Messine, matin du 18 septembre}{95}{section.0.5.1}
\contentsline {section}{\numberline {5.2}Messine, nuit du 18 septembre}{100}{section.0.5.2}
\contentsline {section}{\numberline {5.3}D\IeC {\'e}troit de Messine, fin d'apr\IeC {\`e}s-midi du 19 septembre}{103}{section.0.5.3}
\contentsline {section}{\numberline {5.4}D\IeC {\'e}troit de Messine, soir\IeC {\'e}e du 19 septembre}{106}{section.0.5.4}
\contentsline {section}{\numberline {5.5}Golfe de Tarente, soir\IeC {\'e}e du 22 septembre}{109}{section.0.5.5}
\contentsline {section}{\numberline {5.6}Golfe de Tarente, matin du 23 septembre}{112}{section.0.5.6}
\contentsline {section}{\numberline {5.7}Otrante, fin d'apr\IeC {\`e}s-midi du 25 septembre}{114}{section.0.5.7}
\contentsline {chapter}{\numberline {6}Chapitre 5}{119}{chapter.0.6}
\contentsline {section}{\numberline {6.1}Otrante, fin d'apr\IeC {\`e}s-midi du 26 septembre}{119}{section.0.6.1}
\contentsline {section}{\numberline {6.2}Otrante, nuit du 26 septembre}{123}{section.0.6.2}
\contentsline {section}{\numberline {6.3}Canal d'Otrante, matin du 27 septembre}{127}{section.0.6.3}
\contentsline {section}{\numberline {6.4}Canal d'Otrante, apr\IeC {\`e}s-midi du 27 septembre}{131}{section.0.6.4}
\contentsline {section}{\numberline {6.5}Canal d'Otrante, fin d'apr\IeC {\`e}s-midi du 27 septembre}{133}{section.0.6.5}
\contentsline {section}{\numberline {6.6}Canal d'Otrante, soir\IeC {\'e}e du 27 septembre}{136}{section.0.6.6}
\contentsline {section}{\numberline {6.7}Canal d'Otrante, nuit du 27 septembre}{140}{section.0.6.7}
\contentsline {chapter}{\numberline {7}~Chapitre 6}{145}{chapter.0.7}
\contentsline {section}{\numberline {7.1}Canal d'Otrante, nuit du 27 septembre}{145}{section.0.7.1}
\contentsline {section}{\numberline {7.2}Canal d'Otrante, matin du 28 septembre}{148}{section.0.7.2}
\contentsline {section}{\numberline {7.3}Canal d'Otrante, apr\IeC {\`e}s-midi du 28 septembre}{151}{section.0.7.3}
\contentsline {section}{\numberline {7.4}C\IeC {\^o}te ionienne, apr\IeC {\`e}s-midi du 29 septembre}{154}{section.0.7.4}
\contentsline {section}{\numberline {7.5}C\IeC {\^o}te de Corfou, 30 septembre}{158}{section.0.7.5}
\contentsline {section}{\numberline {7.6}\IeC {\^I}les ioniennes, midi du 1er octobre}{162}{section.0.7.6}
\contentsline {section}{\numberline {7.7}\IeC {\^I}les ioniennes, soir du 2 octobre}{165}{section.0.7.7}
\contentsline {chapter}{\numberline {8}~Chapitre 7}{171}{chapter.0.8}
\contentsline {section}{\numberline {8.1}\IeC {\^I}les ioniennes, matin du 3 octobre}{171}{section.0.8.1}
\contentsline {section}{\numberline {8.2}\IeC {\^I}les ioniennes, Fin de matin\IeC {\'e}e du 3 octobre}{175}{section.0.8.2}
\contentsline {section}{\numberline {8.3}\IeC {\^I}les ioniennes, apr\IeC {\`e}s-midi du 3 octobre}{178}{section.0.8.3}
\contentsline {section}{\numberline {8.4}Au large du golfe de Patras, matin du 4 octobre}{182}{section.0.8.4}
\contentsline {section}{\numberline {8.5}C\IeC {\^o}te du P\IeC {\'e}loponn\IeC {\`e}se, mi-journ\IeC {\'e}e du 5 octobre}{186}{section.0.8.5}
\contentsline {section}{\numberline {8.6}C\IeC {\^o}te du P\IeC {\'e}loponn\IeC {\`e}se, soir du 6 octobre}{190}{section.0.8.6}
\contentsline {section}{\numberline {8.7}C\IeC {\^o}te du P\IeC {\'e}loponn\IeC {\`e}se, apr\IeC {\`e}s-midi du 7 octobre}{193}{section.0.8.7}
\contentsline {chapter}{\numberline {9}~Chapitre 8}{199}{chapter.0.9}
\contentsline {section}{\numberline {9.1}C\IeC {\^o}te sud du P\IeC {\'e}loponn\IeC {\`e}se, matin du 8 octobre}{199}{section.0.9.1}
\contentsline {section}{\numberline {9.2}C\IeC {\^o}te sud du P\IeC {\'e}loponn\IeC {\`e}se, matin du 9 octobre}{203}{section.0.9.2}
\contentsline {section}{\numberline {9.3}C\IeC {\^o}te sud du P\IeC {\'e}loponn\IeC {\`e}se, midi du 9 octobre}{207}{section.0.9.3}
\contentsline {section}{\numberline {9.4}C\IeC {\^o}te sud du P\IeC {\'e}loponn\IeC {\`e}se, matin du 10 octobre}{210}{section.0.9.4}
\contentsline {section}{\numberline {9.5}Au large de Cyth\IeC {\`e}re, apr\IeC {\`e}s-midi du 11 octobre}{215}{section.0.9.5}
\contentsline {section}{\numberline {9.6}Au large d'Antikythira, matin du 12 octobre}{219}{section.0.9.6}
\contentsline {section}{\numberline {9.7}Abords de la c\IeC {\^o}te cr\IeC {\'e}toise, matin du 13 octobre}{223}{section.0.9.7}
\contentsline {chapter}{\numberline {10}Chapitre 9}{227}{chapter.0.10}
\contentsline {section}{\numberline {10.1}C\IeC {\^o}te cr\IeC {\'e}toise, matin du 14 octobre}{227}{section.0.10.1}
\contentsline {section}{\numberline {10.2}C\IeC {\^o}te cr\IeC {\'e}toise, matin du 14 octobre}{231}{section.0.10.2}
\contentsline {section}{\numberline {10.3}Au large de l'\IeC {\^\i }le de Dia, matin du 15 octobre}{235}{section.0.10.3}
\contentsline {section}{\numberline {10.4}Au large de l'\IeC {\^\i }le de Dia, apr\IeC {\`e}s-midi du 15 octobre}{239}{section.0.10.4}
\contentsline {section}{\numberline {10.5}C\IeC {\^o}te cr\IeC {\'e}toise, midi du 16 octobre}{242}{section.0.10.5}
\contentsline {section}{\numberline {10.6}Golfe de Mirabellou, fin de matin\IeC {\'e}e du 17 octobre}{245}{section.0.10.6}
\contentsline {section}{\numberline {10.7}Entre Cr\IeC {\^e}te et Kassos, matin du 18 octobre}{249}{section.0.10.7}
\contentsline {chapter}{\numberline {11}Chapitre 10}{253}{chapter.0.11}
\contentsline {section}{\numberline {11.1}C\IeC {\^o}te sud de Karpathos, fin d'apr\IeC {\`e}s-midi du 19 octobre}{253}{section.0.11.1}
\contentsline {section}{\numberline {11.2}Sud de l'\IeC {\^\i }le de Rhodes, midi du 20 octobre}{256}{section.0.11.2}
\contentsline {section}{\numberline {11.3}Sud de l'\IeC {\^\i }le de Rhodes, matin du 21 octobre}{260}{section.0.11.3}
\contentsline {section}{\numberline {11.4}Sud de l'\IeC {\^\i }le de Rhodes, apr\IeC {\`e}s-midi du 21 octobre}{263}{section.0.11.4}
\contentsline {section}{\numberline {11.5}C\IeC {\^o}te est de Rhodes, matin du 22 octobre}{267}{section.0.11.5}
\contentsline {section}{\numberline {11.6}Port de Rhodes, soir du 22 octobre}{270}{section.0.11.6}
\contentsline {section}{\numberline {11.7}Port de Rhodes, matin du 23 octobre}{273}{section.0.11.7}
\contentsline {chapter}{\numberline {12}~Chapitre 11}{277}{chapter.0.12}
\contentsline {section}{\numberline {12.1}Port de Rhodes, soir du 24 octobre}{277}{section.0.12.1}
\contentsline {section}{\numberline {12.2}Bassin de Rhodes, matin du 25 octobre}{281}{section.0.12.2}
\contentsline {section}{\numberline {12.3}Bassin de Rhodes, soir du 26 octobre}{285}{section.0.12.3}
\contentsline {section}{\numberline {12.4}Mer M\IeC {\'e}diterran\IeC {\'e}e, matin du 27 octobre}{288}{section.0.12.4}
\contentsline {section}{\numberline {12.5}Mer M\IeC {\'e}diterran\IeC {\'e}e, fin d'apr\IeC {\`e}s-midi du 27 octobre}{292}{section.0.12.5}
\contentsline {section}{\numberline {12.6}Mer M\IeC {\'e}diterran\IeC {\'e}e, apr\IeC {\`e}s-midi du 28 octobre}{296}{section.0.12.6}
\contentsline {chapter}{\numberline {13}Chapitre 12}{301}{chapter.0.13}
\contentsline {section}{\numberline {13.1}Port de Gibelet, soir du 1\textsuperscript {er} novembre}{301}{section.0.13.1}
\contentsline {section}{\numberline {13.2}Matin du 2 novembre, port de Gibelet}{306}{section.0.13.2}
\contentsline {section}{\numberline {13.3}Citadelle de Gibelet, midi du 3 novembre}{310}{section.0.13.3}
\contentsline {section}{\numberline {13.4}Gibelet, apr\IeC {\`e}s-midi du 3 novembre}{315}{section.0.13.4}
\contentsline {section}{\numberline {13.5}Forteresse de Gibelet, soir du 3 novembre}{318}{section.0.13.5}
\contentsline {section}{\numberline {13.6}Forteresse de Gibelet, soir du 3 novembre}{321}{section.0.13.6}
\contentsline {section}{\numberline {13.7}Gibelet, matin du 4 novembre}{324}{section.0.13.7}
\contentsline {chapter}{\numberline {14}Addenda}{329}{chapter.0.14}
\contentsline {section}{\numberline {14.1}Notes de l'auteur}{329}{section.0.14.1}
\contentsline {section}{Le pari de la libert\IeC {\'e}}{332}{section*.7}
\contentsfinish 
