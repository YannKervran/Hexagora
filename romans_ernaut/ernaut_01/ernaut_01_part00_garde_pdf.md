# La nef des loups

Roman historique policier

**Cycle Ernaut de Jérusalem**  
**Tome 1**

![Framabook, le pari du livre libre](logo_framabook_epub.jpg)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d’informations sur Framasoft, consultez [http://www.framasoft.org](http://www.framasoft.org)

Se démarquant de l’édition classique, les Framabooks sont dits « livres libres » parce qu’ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu’un utilisateur de logiciels libres. Les Framabooks s’inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l’appropriation collective de la connaissance.

Pour plus d’informations sur le projet Framabook, consultez [http://framabook.org](http://framabook.org)

Copyright 2016 : Yann Kervran, Framasoft (coll. Framabook)

*La nef des loups* est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

ISBN : 979-10-92674-14-9  
Dépôt légal : décembre 2016  
Couverture : Folio 26 recto d'un manuscrit du XIIe siècle de la vie de Bede de Saint-Cuthbert, Cuthbert navigant. Domaine Public - [commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:BLYatesThompson26LifeCuthbertFol26rCuthSails.jpg)

