## Prologue

### Monastère bénédictin de la Charité-sur-Loire, hiver 1223

La lumière du jour naissant filtrait à peine à travers les nuages bas, le jeune Déodat pouvait voir son haleine se muer en fumée tandis qu'il avançait sans bruit dans le cloître. Il portait son écritoire et une chandelle de bonne cire, le visage baissé et empreint de sérieux comme il seyait à un bénédictin.

Il était pourtant habité d'une fièvre qui lui aurait permis de supporter les frimas les plus intenses et son large sourire aurait pu lui valoir des reproches de ses frères les plus stricts. Heureux moine, il était impatient de s'acquitter de sa tâche, obtenue en récompense de son assiduité aux études et en raison de son habileté à rédiger les lettres et documents du prieur. Pour une fois, ce n'était pas de froides chartes qu'il devait recopier, ni des messages adressés à de lointains contacts de son monastère.

Lorsqu'il poussa la lourde et bruyante porte de l'infirmerie, le frère responsable tourna à peine la tête vers lui. Il était occupé à parler à voix basse avec le chantre, un obèse à la respiration sifflante qui semblait endurer mille maux. Il était pourtant allongé sur sa couche au plus près de la vaste cheminée apportant une douce chaleur dans la pièce.

Déodat attrapa un tabouret au passage et s'installa auprès d'un vieillard édenté et somnolent, assis dans son lit sous une fenêtre au châssis tendu de parchemin translucide. Les yeux emplis de cataracte auraient pu être ceux d'un aveugle et son air béat le rendait semblable à un sot ou un fou. Mais son esprit restait vif et, si son élocution était tremblante, ses paroles étaient encore pleines d'une vie insoupçonnable dans ce corps grabataire. Déodat eut à peine le temps de disposer son écritoire sur ses genoux que le patriarche tournait la tête vers lui, un sourire fatigué illuminant les plis de son visage. Sa voix était épaisse et semblait charrier des graviers.

« Te voilà enfin, garçon. Il est bien heure. Nous avons tant à faire. »

Le jeune moine sentit son cœur s'emballer.

« Avez-vous choisi quels souvenirs nous allons parcourir ?

— Calme, Déodat. Il convient d'accomplir les choses en ordre. Avant d'aborder les errements de l'homme fait, nous devons présenter le passage où il abandonna sa condition d'enfant mal dégrossi. Un modèle de vie s'est imposé à lui comme évidence lors de ce voyage merveilleux, à l'origine de toutes les aventures ultérieures.

— N'allons nous pas parler de tous ces lieux étranges, en les terres plus lointaines ? Jérusalem, Alexandrie, Byzance, Bagdad ? »

Le vieillard toussa, troublé par la mention de ces noms qui évoquaient tant pour lui. Il attrapa la main du jeune moine et la pressa amicalement.

« Ce me semble qu'il faut commencer par plus fabuleux voyage, celui que le pèlerin suit, en sien cœur et outre les eaux, jusqu'à parvenir en Terre Sainte. Si un enfançon s'embarqua à Gênes sur le *Falconus*, ce fut en vérité un homme qui en descendit à Gibelet. Prends ta plume et entends bien. Je vais te conter les merveilleuses errances de celui qu'on nomma Ernaut de Jérusalem… »
