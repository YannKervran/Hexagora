## Chapitre 10

### Côte sud de Karpathos, fin d'après-midi du 19 octobre

Digérant son repas du matin, Ernaut s'était accoudé au bastingage et regardait vers le nord l'île qu'ils contournaient. On apercevait les lignes blanches des plages de sable bordant l'arrière-pays plus vert. Il était difficile d'y discerner des habitations et on lui avait dit que la plupart des sites côtiers ici n'accueillaient de toute façon que d'humbles villages de pêcheurs. Le vent était encore assez fort pour que le *patronus* s'efforce de rester à la voile le plus longtemps possible, tant que le jour était suffisant. Mais comme peu de matelots étaient nécessaires pour l'instant, la plupart s'étaient rassemblés pour avaler leur souper avant de ramener les voiles et jeter les ancres. Ernaut admirait un essaim d'oiseaux de mer virevoltant autour d'une zone, à quelque distance du navire. Il essayait de comprendre leur agitation, tendant le cou comme si cela pouvait le rapprocher suffisamment. Octobono, en train de prendre son repas, s'approcha, l'écuelle à la main. Il tentait de mâcher un biscuit, le faisant craquer à chaque bouchée, plutôt confiant dans ses dents pour se lancer dans si hasardeuse entreprise.

« Ils ont repéré quelque banc de poissons. Pour eux aussi c'est l'heure de gloutonner… »

Ernaut se retourna vers le matelot, qui, voulant lui sourire, lui dévoila en partie le contenu de sa bouche. Avec sa tenue négligée, ses cheveux fous et sa barbe mal taillée, il avait vraiment un air effrayant. Mais son visage paraissait si jovial qu'on en oubliait immédiatement la mauvaise impression ressentie au premier regard. C'était d'ailleurs un des hommes les plus appréciés à bord, par les marins comme par les voyageurs.

« On n’est guère éloignés de Rhodes. Plus qu'une paire de jours et on pourra avaler autre chose que ces lopins de cailloux. »

Ernaut fit une grimace en jetant un coup d'œil au repas du matelot.

« J'ai beau en avaler depuis le début du passage, je n'arrive guère à y trouver quelque saveur.

— Et encore, tu n'es pas comme nous autres les pieds dans l'eau plus souvent qu'à terre. Si j'avais gardé tous mes biscuits au lieu de les manger, j'aurais eu assez pour en bâtir solide forteresse !

— Tu as bien du courage de les croquer ainsi !

— J'aime, ça m'endurcit la mâchoire. »

Marquant une pause, Octobono sourit de nouveau et s'accouda à son tour, le regard au loin.

« Et sinon, vous avez quelque peu avancé ? Vous avez plus claires idées sur le bastard qui aurait occis Fulco ?

— Ça, je ne peux dire, même si je savais.

— Comprends, c'était un proche compaing à moi, ça me désolerait qu'on ne sache jamais. Pas par vengeance,  je ne suis pas ainsi. Juste pour savoir, quoi…

— J'entends bien. Mais j'ai fait serment de rester coi.

— D'accord. Je vois. Tu ne peux accorder confiance à aucun. Dommage. »

Il resta encore un bon moment le regard au loin, un rictus sur le visage tout en avalant des morceaux de biscuit qu'il arrosait de rasades de vin bues à même le pichet. Puis, sans quitter des yeux le paysage, il s'adressa à Ernaut d'une voix lasse.

« Tu vois, j'ai erré en tous les coins de cette mer, depuis si longtemps. Et je ne crois avoir eu plus fidèle ami que Fulco. C'était vraiment un loyal compaing. »

Ernaut se sentit gêné et essaya de s'expliquer sur son refus, mais Octobono lui posa doucement la main sur le bras.

« Non attends, je n'espère te forcer. Mais te montrer, à toi et tes amis, qu'il se trouve d'aucuns pour qui Fulco, ce n'est pas que mystère plaisant à percer. Et ça doit être vrai aussi pour le marchand, du moins j'en ai espoir pour lui. Alors, je veux bien qu'on soit écartés, mais fais-moi serment de résoudre cette histoire, et pas seulement par plaisir. »

Ernaut percevait l'émotion du matelot dont on aurait dit soudain qu'il retenait des sanglots à grand peine.

« De sûr, nous faisons tout… »

La pression de la main du marin se fit plus forte et il agrippait désormais la manche du jeune homme.

« Non, tu n'as pas entendu, fais-moi solennel serment que vous ne laisserez échapper le meurtrier de mon compaing. Il me faut l'entendre, tu saisis ? Alors, fais serment ! »

Ernaut se mordit la lèvre et répondit dans un souffle.

« Je te le jure, par Dieu et tous ses saints ! Nous allons trouver ce meurtrier… »

Octobono le lâcha, la tête fixe et le regard figé dans celui d’Ernaut. Puis il se détourna et recommença lentement à avaler son repas. Après un long moment, il renifla, les yeux fermés, humant l'air avec sérieux.

« Demain ne sera guère joyeux avec le vent. Et ça risque de tourner à la pluie de nouvel. »

Heureux que la discussion change, Ernaut ne se fit pas prier pour enchaîner :

« Désespérant comme on s'ennuie quand on doit demeurer à fond de cale tandis que la tempête souffle.

— Oh, ça dépend, j'en sais qui arrivent à s'occuper d'agréable manière. Durant le terrible grain, juste avant le murdriment de Fulco, plusieurs gars ont lipé vinasse la nuit durant. Rien de grave en soi, mais c'était du vin de Spinola. Il leur a fait passer l'envie : le coût en sera retenu sur leur solde.

— Ils sont parvenus à se soûler malgré le branlement du navire ? Ils avaient le cœur solide !

— Ben, ce sont des matelots ! Mais ils étaient tout de même peu vifs le lendemain. Lors je suis descendu prendre cordages pour le canot, j'ai manqué de choir en glissant dans les vomissures d'Enrico. Il y dormait son vin, la tête presque en dedans. »

Le nom intrigua Ernaut.

« Enrico… Maza ?

— Ouais, lui-même ! Mais bon, j'ai fait escurer la pièce à ces soûlards ! Je veux bien qu'on boive, voire qu'on soit malade, mais en ce cas, on laisse propre derrière soi, c'est pas aux autres de frotter le pont ! »

Ernaut continuait poliment à réagir au récit d'Octobono, mais il était perdu dans ses pensées. Il savait que Régnier pensait à Maza comme le responsable du second meurtre, et certainement au moins comme complice du premier. Mais le meilleur ami de la victime venait de lui fournir le plus solide des alibis. Alors qu'on poignardait et jetait le corps de Fulco Bota par-dessus bord, Maza était en train de cuver son vin aux tréfonds du navire.

### Sud de l'île de Rhodes, midi du 20 octobre

Régnier vérifiait qu'il avait fait prendre suffisamment d'habits sales à Ganelon. La pluie tombait bien depuis le matin et le chevalier en avait profité pour demander à son valet de laver un peu quelques chemises, braies et chausses. Il commençait à venir au bout de ses affaires de rechange et voulait saisir l'opportunité que lui offraient les cieux. De leur côté, sous les ordres du *patronus*, les matelots avaient rapidement disposé les tonneaux vides sur les ponts supérieurs de façon à récupérer également un maximum d'eau. Quelques passagers étaient même restés sous la pluie, la bouche ouverte, pour goûter enfin une boisson fraîche qui n'avait pas fermenté pendant des semaines dans des tonneaux à fond de cale.

Tandis qu'il cherchait dans ses vêtements, il ne put s'empêcher de sortir son bliaud[^bliaud] de cour, offert par Hierge de Manassès[^manasseshierges] lui-même lorsqu'il était à son service. Il appréciait toujours de le regarder, admirant le chatoiement des animaux dans les médaillons de soie. C'était une parure extrêmement luxueuse qu'il ne mettait que pour les événements importants, comme les réunions avec la haute noblesse. Il avait eu la possibilité de la porter jusqu'à plus soif lors de sa tournée auprès des puissances européennes, sans grand succès au final d'ailleurs. Soupirant pour lui-même, il roula avec soin le vêtement et le rangea avec la menthe dans son étoffe de lin, afin de le protéger.

Il s'adossa ensuite contre le mur au fond de son espace, en dehors du cercle de clarté de la lampe suspendue au poteau à l'entrée. Il réfléchissait à la liste des assassins possibles et il était ennuyé de devoir en effacer Maza. Le soldat correspondait bien à l'image qu'il se faisait du larron prêt à trancher une gorge pour quelques pièces, comme l'animal qu'il était. Il en côtoyait régulièrement, de ces soudards prêts à tout pour gagner leur pain, et il ne les aimait guère. Il voyait du potentiel en Ernaut et craignait que le garçon ne tourne simple soldat. Sans s'expliquer pourquoi, il sentait qu'une vie plus honorable lui était accessible, malgré son étourderie et sa maladresse. Il était intelligent et débrouillard, plein d'aplomb et de bon sens. Et avec l'âge viendrait un peu plus de discipline et de rigueur, qui lui manquaient tant. Son expérience dans les flots semblait d'ailleurs l'avoir quelque peu assagi et mûri. On l'aurait été à moins.

Il tourna la tête, cherchant la cruche de vin qu'il avait demandée à son valet lorsqu'il s'aperçut que Sawirus de Ramathes se trouvait devant lui, dans le passage. Aussi silencieux qu'une souris, le vieil homme s'était approché, tout sourire, et attendait que Régnier le remarque. Ce dernier n'aimait pas être surpris ainsi et prit un ton un peu courroucé pour interpeller le marchand.

« Eh bien, que faites-vous à patienter dans le passage ?

— Je ne voulais vous interrompre. Retenu en un bateau quasi immobile, je ne suis guère pressé. »

Les yeux rieurs, il assortit sa dernière remarque d'un sourire dévoilant ses dents. L'évidente bonhommie du vieillard rasséréna un peu le chevalier, qui s'efforça de se montrer plus aimable.

« Prenez siège, maître, je vous prie. Voulez-vous quelque vin ?

— Pas pour l'instant, merci. »

Le marchand prit place délicatement, ramenant sur ses genoux les amples manches de son vêtement, de style syrien.

« J'ai réfléchi à une petite chose que je ne vous ai pas contée lors de nos précédentes discussions. Cela ne concerne pas la meurtrerie de maître Embriaco, mais peut avoir quelque importance, car cela ne semble pas… naturel, si vous m'ensuivez. C'était durant la matinée avant le murdriment. »

Régnier se pencha en avant.

« Allez-y, nous verrons si cela peut avoir quelque utilité…

— Fort bien. Comme vous l'avez noté, j'aime à créer courts textes, poèmes et chants. J'y consacre de plus en plus de temps, surtout en voyage. Ce matin-là, je copiais un mien poème, fruit d'un long labeur lorsque je me suis trouvé à court d'encre. J'ai donc entrepris d'en quérir un peu. Sachant que maître Embriaco ne quittait jamais sa cabine, j'ai frappé à son huis pour lui en demander. »

Régnier était de plus en plus intéressé par le récit et se rapprocha insensiblement de son interlocuteur.

« Je n'ai obtenu aucune réponse, ce qui m'a fort contrarié sur le moment. Je pensais qu'il se trouvait forcément Embriaco ou son valet dans la pièce. Ugolino n'aurait certes pas gardé silence, alors je me suis un peu échauffé les sangs et ai cogné du poing plus violemment sur l'huis qui a légèrement bougé. La targette n'était donc pas poussée. Mais elle n'est pas demeurée entr'ouverte, comme si quelque chose la maintenait close. Je n'ai pas osé insister. Quand j'y ai repensé voilà peu, j'ai estimé que cela pouvait vous aider, de le savoir alors absent sans qu'aucun ne le sache. »

Ou qu'il était déjà meurtri, pensa Régnier.

« Toujours est-il que je n'ai donc pas obtenu d'encre et j'ai dû en emprunter à votre ami maître Gonteux. Je lui ai donné un peu de papier pour le remercier. Un homme fort aimable ! Enfin, voilà mon histoire, en espérant qu'elle vous guidera quelque peu sur le chemin vers la vérité. »

Régnier exultait intérieurement, mais s'efforçait de ne pas trop le manifester. Il ne voulait pas que le bruit se répande qu'il s'intéressait à la journée précédant le meurtre.

« Effectivement, cela pourrait avoir utilité. Je n'ai guère idée de la façon, mais je vous mercie d'être venu me narrer ceci. »

Se reculant de nouveau sur sa couche, Régnier se réfugia dans l'obscurité. Il savait désormais avec certitude qu'Ansaldi était mort le matin, à Otrante, et qu'il avait donc été assassiné par l’agresseur du marin. Si seulement Sawirus de Ramathes ne s'était pas montré si discret et avait poussé la porte ! Ils auraient pu déjouer plus facilement les manigances de l'assassin. En l'occurrence, sa politesse et sa discrétion allaient peut-être permettre à un être malfaisant de s'en tirer impuni.

### Sud de l'île de Rhodes, matin du 21 octobre

Depuis quelques jours, Ernaut essayait de passer un peu de temps avec Ingo, le jeune apprenti charpentier. Il avait envisagé dans un premier temps de s'en rapprocher pour pouvoir lui poser à l'occasion toute question utile. Puis peu à peu, il s'était pris d'amitié pour le garçon, qui avait quasiment le même âge que lui. Par son physique, Ernaut attirait souvent les personnes en difficulté et il se plaisait à jouer le protecteur, autant par pure charité que par estime de lui-même. Il lui arrivait donc fréquemment de l'inviter à manger avec Lambert et lui. Le déjeuner avait été particulièrement drôle ce matin-là, car ils s'étaient remémoré les pitreries de Yazid, alias Vide-Godet, qui les avait encore régalés d'un bon spectacle la veille : son succès était tel que de nombreux voyageurs assistaient désormais à une des institutions des veillées. Après cela, Ernaut était resté à seconder celui qui se trouvait presque promu charpentier de bord, malgré son jeune âge. Alors qu'il aidait au tri de pièces de bois devant remplacer les barreaux d'une rambarde cassée par accident, Ernaut demandait à son compagnon de l'éclairer sur la langue italienne. Il avait toujours été à l'aise avec les différents dialectes et, en bon fils de négociant,  maîtrisait parfaitement plusieurs d'entre eux. Il était malgré tout perpétuellement inquiet de ne pas pouvoir communiquer à son aise. En dépit de ses talents, il lui arrivait souvent de prendre un mot pour un autre ou d'avoir une prononciation approximative, arrachant des sourires amicaux à son professeur improvisé.

Les deux garçons s'étaient installés dans la petite loge, en raison de la pluie. Ernaut n'aimait pas ce temps, particulièrement à bord. Le pont était très glissant et il avait failli se blesser en se rendant jusqu'à l'atelier. Par la porte ouverte, il regardait avec  admiration les marins qui, les pieds souvent nus, se déplaçaient sans aucune gêne. La mer était une nouvelle fois un peu formée et les mouvements du *Falconus* mettaient à la torture les passagers à l'estomac fragile. Assis sur le côté de la loge, Ingo ponctuait le temps de coups de maillet réguliers, taillant habilement une pièce avec un ciseau. Ernaut tourna finalement la tête vers lui pour observer ce à quoi il s'était attelé. Il ne s'intéressait guère au travail du bois en général, mais il avait fini par avoir une vraie curiosité sur ce que faisait Ingo, et les différentes fonctions des composantes du navire. Lorsqu'ils faisaient la visite journalière, pour voir si aucun sabotage de la coque n'était à déplorer, le jeune charpentier lui montrait parfois un élément ou l'autre, en lui expliquant brièvement son intérêt dans la structure du bateau. Mais en vérité, ce qui passionnait Ernaut plus que tout autre chose, c'était de fureter partout, la lampe à la main, de rentrer dans chacune des cales et cabines sous le prétexte de l'inspection pour mettre son nez là où on le lui interdisait habituellement.

« Tu vas faire comment pour le passage à rebours, sans maître ?

— Je ne sais. Maître Malfigliastro m'a dit qu'il ferait en sorte que mes gages soient augmentés pour ce passage, vu que je besogne seul comme artisan. Et il faudra aussi s'inquiéter des outillages de maître Bota. J'aimerais pouvoir les racheter, mais il y en a grand chèreté, vu le nombre. Et si on me loue ensuite de nouvel comme simple apprenti, ça ne vaut pas le coup… »

Ernaut prit machinalement un des ustensiles qui se trouvait à côté de lui et le regarda sous toutes les coutures. Il se demandait quel prix cela pouvait bien avoir. Le bruit de la frappe régulière du marteau cessa brusquement, le tirant de ses rêveries. Il leva la tête, et vit Ingo l'air perplexe, la main à la bouche.

« Que t'arrive-t-il ?

— Il me vient souvenir d'une chose ! À propos des outils !

— En rapport avec les meurtreries ?

— Possiblement ! Le clerc, le jeune, il m'a dit que je me devais de vous conter toute chose surprenante… »

Ernaut posa la râpe qu'il tenait et s'accroupit près du jeune charpentier. Avant de tourner la tête vers ce dernier, il s'assura d'un regard que personne ne se tenait aux alentours.

« Dans le passage d'Otrante, maître Bota m'a fort houspillé ; il affirmait que j'avais égaré un des ciseaux, fort coûteux. Il menaçait d'en garder le prix sur mes gages. J'étais fort marri car j'étais sûr de n'y être pour rien. Et puis, en début de mois, j'ai retrouvé le fameux outil, en sa caisse. Quand j'ai voulu en parler à maître Bota, il m'a dit l'avoir retrouvé et qu'il était désolé de s'être échauffé pour rien. »

Prenant un peu de recul, Ernaut réfléchit un instant, calculant à voix basse.

« Le ciseau a disparu après le murdriment du marchand pour revenir quelques jours avant celui de ton maître, c'est bien ça ?

— Je ne connais pas l'exact moment de sa disparition, seulement qu'il n'était plus là lorsque nous étions au large d'Otrante. »

L'adolescent fouilla dans la boîte et en sortit un large instrument de charpentier, au fer solide et épais, qu'il tendit à son compagnon. Ernaut le prit et le regarda attentivement. Il se demandait quel rôle avait pu jouer ce simple objet, si jamais il en avait eu un, dans le sinistre drame qui s'était déroulé à bord. Il le soupesait, frappant dans l'air comme avec un poignard. Mais ce n'était pas l'arme du crime, puisqu'il n'avait à l'évidence pas servi à tuer le charpentier.

« Est-ce que je peux le prendre ? Je voudrais le montrer à messire d'Eaucourt.

— Tu peux le garder le temps qu'il faudra. Si ça peut aider… »

Ernaut se leva, puis, alors qu'il allait sortir l'outil à la main, il se ravisa. Il chercha un chiffon dans la pièce et enveloppa le ciseau puis le glissa sous sa cotte. Comme Ingo le regardait faire, il lui expliqua.

« Si ce simple oustil a servi dans ces meurtreries, oncque ne doit savoir que je l'ai. Ça pourrait de nouveau coûter une vie. Tu me suis ?

— Je n'en parlerai à personne. Et d'aucunes façons, sauf moi, personne ne peut savoir son importance.

— À part toi, moi et… le meurtrier, Ingo. Alors, prends garde. »

Serrant l'objet sous son vêtement, Ernaut sortit sur le pont, à destination de la cabine de Régnier, bravant la pluie qui tombait en gouttelettes fines sur le navire. Fronçant les sourcils pour ne pas recevoir d'eau dans les yeux, il se frotta le nez d'un air méfiant, s'attendant à voir surgir à tout instant un homme armé d'un poignard. Si jamais l'assassin était assez fou pour s'attaquer à lui, il se dit qu'il lui briserait bien volontiers le cou. Il se sentait l'âme d'un traqueur et certainement pas d'une proie. Il avait fait une promesse et comptait bien s'y tenir, tout en y ajoutant éventuellement le plaisir d'envoyer le malfaisant directement en Enfer, de ses propres mains.

### Sud de l'île de Rhodes, après-midi du 21 octobre

Le ciseau à bois en main, Régnier examinait soigneusement la cabine d'Ansaldi Embriaco, à la recherche d'une trace quelconque de l'ustensile. Il en était arrivé à la conclusion que Bota était mort car il avait compris l'usage de cet instrument, essentiel dans le plan de l'assassin du marchand génois. Ayant rapidement écarté l'idée de tenir l'arme du crime, Régnier et ses compagnons étaient désormais convaincus qu'il avait servi à sortir et peut-être aussi entrer dans la chambre. C'est pourquoi ils inspectaient de nouveau les cloisons, chacun équipé d'une lampe à huile, mais en sachant un peu mieux quoi chercher : les marques récentes d'un outil.

Ernaut avisa des griffures sur le chambranle, près des glissières de maintien de la targette de fermeture. Il lui semblait que des enfoncements légers pouvaient avoir été faits avec un objet de la largeur du ciseau, comme si on avait fait levier sur le plat de la porte et arraché les fixations. Il en éprouva la taille de son pouce, afin de s'assurer de son hypothèse avant de crier victoire. Une fois certain, il appela ses deux compagnons. Ganelon, la lampe à la main, dans le couloir s'approcha également par curiosité. Appliquant l'outil exactement dans les traces, Ernaut ne peina guère à convaincre Herbelot et Régnier. Ce dernier semblait enchanté, au grand plaisir de l'adolescent.

« Pourquoi telles traces sont en dedans ? déclara Herbelot. Comme si la personne avait souhaité s'échapper ? Alors qu'il lui suffisait de tirer le coulissant de bois. »

Régnier éprouva symboliquement la solidité du panneau. Décontenancé par la remarque du prêtre, il se tourna vers Ernaut.

« Es-tu vraiment assuré que la porte était bloquée ?

— Pour sûr, messire ! Je vous l'ai déjà dit, je me suis endolori assez en la défonçant. Je n'aurais pas eu à m'y prendre deux fois si elle n'avait eu la barre engagée. »

Un peu dépité que sa découverte ne soit pas la clé du mystère, Ernaut se grattait le crâne d'un air boudeur. Mais peu à peu se forma dans son esprit une hypothèse qu'il avança lentement, tout en appuyant sa démonstration de gestes explicites sur la menuiserie.

« Il est imaginable que le malfaisant n'ait pas cherché à tout arracher, après tout. Nous savons que l'huis doit être fort plaqué contre le chambranle pour glisser le loquet. Mais avec quelque jeu, il devient possible de passer cordelette pour clore du dehors, non ? Nous aurions ainsi notre mystérieux meurtrier passe-murailles. »

Intrigué, Régnier hocha la tête.

« Oui, pourquoi pas ? Essayons donc chez maître Sawirus, voir si nous parvenons à tel résultat. »

Les trois enquêteurs demandèrent au marchand installé à côté de leur céder sa cabine le temps pour eux de vérifier une hypothèse, sans entrer dans les détails pour ne pas déflorer leurs avancées. Ils parvinrent sans difficulté à décoller un peu la targette, obtenant des traces similaires à celles qu'Ernaut avait remarquées. En outre, cela donnait assez de jeu pour qu'un lacet puisse coulisser depuis l'extérieur et refermer la porte depuis le couloir. Resté dans la chambre, Régnier rouvrit, un large sourire sur les lèvres.

« Je crois que nous venons de faire avancée de géant. Grand merci à toi, Ernaut ! »

Il gratifia l'adolescent d'une accolade virile, arrachant à l'heureux bénéficiaire une exclamation aussi enthousiaste que surprise. Ernaut remarqua également que le chevalier l'avait appelé par son prénom et non plus par l'habituel « jeune homme », ce qui lui fit chaud au cœur.

Revenant chez Ansaldi, ils reprirent leurs places habituelles en silence, chacun tentant de placer cette découverte dans le schéma général des événements. Ganelon avait été envoyé prévenir Sawirus qu'il pouvait réintégrer sa cabine. Régnier jouait sans y réfléchir avec le ciseau à bois, l’air concentré. Le premier, il dévoila à haute voix ce à quoi il pensait.

« Ce qui me chagrine, c'est qu'on peut ainsi clore la pièce une unique fois. Il fallait donc un complice pour tenir le lieu le temps d'aller trouver l'outil. »

Ernaut se mordillait les doigts nerveusement, comme si c'étaient des mets de choix. Il répondit sans même réaliser qu'il coupait pratiquement la parole au chevalier.

« Sauf s'il a prévu et fureté auparavant. »

Cette remarque plongea le groupe dans l'expectative. Après un moment passé à échafauder une hypothèse, Herbelot se mit à soliloquer avec force gestes.

« Prenons cette huche, avec du sang sur la tranche. Et maître Sawirus, qui a eu le sentiment que la porte était mal serrée le matin où, nous le savons, Ansaldi était déjà passé outre. Imaginons qu'il n'y ait qu'un mécréant, ayant occis le marchand nuitamment. Il escomptait s'enfuir juste après son forfait, mais fut empêché par l'arrivée du matelot. Bloqué en la nef, il doit soustraire le ciseau à bois pour clore derrière lui, et  dissimuler son forfait. Mais à l'instant où il s'absente pour ce larcin, il doit abandonner la porte ouverte. Il n'a guère le choix ! »

Alors qu'il détaillait sa théorie, Ernaut et Régnier suivaient des yeux le trajet imaginaire. Leurs regards s'accrochaient aux éléments évoqués ou suivaient les pas de l'assassin au-dehors de la cabine, à travers ponts et cloisons.

« Adoncques il encouche le corps, comme si maître Embriaco dormait, au cas, peu croyable, où d'aucun pénétrerait en la chambre. Puis il pose la huche en devers sur le flanc, contre l'huisserie pour la clore et parfaire l'illusion. Il s'empare de l'outillement, revient séant, prépare la targette comme nous, dispose la dépouille sous la couche afin de tromper Ugolino un jour durant. Puis, une fois assuré de sa sauvegarde, il remet le corps en place et barre la porte après lui. »

Un long silence accueillit la fin de ses explications. Enthousiasmé par son récit, qu'il trouvait convainquant, le regard d'Herbelot allait du chevalier à l'adolescent, les yeux pétillants, attendant un mot de félicitation. Régnier se rapprocha du bord du lit et tapa familièrement sur l'épaule du clerc.

« Fort brillant ! Tout trouve place en vostre raisonnement, ami. Le larron a donc frappé pour quelques pièces, espérant s'enfuir avant le départ d'Otrante. Il avait peut-être prévu de bloquer la porte dès l'origine, pour que la meurtrerie ne soit découverte qu'au large. Voilà bien déshonnête coupeur de bourse ! »

### Côte est de Rhodes, matin du 22 octobre

Régnier était monté sur la plate-forme la plus élevée du château arrière pour être un peu au calme. Le *patronus* n’en laissait généralement pas l'accès, et le chevalier avait besoin de tranquillité pour réfléchir. Il avait grossièrement brossé le tableau à Ribaldo, sans développer outre mesure : il était désormais persuadé de n'affronter qu'un seul et même assassin, certainement motivé par l'argent. Le commandant de bord était inquiet, car on n'était guère éloigné de Rhodes, la dernière escale avant l'arrivée à Gibelet, prévue pour dans une dizaine de jours. Conscient qu'il ne lui restait guère de temps, le chevalier reprenait les différents éléments dont il disposait et tentait de voir si cela pouvait lui désigner un coupable.

Il ne regardait pas vraiment le paysage, mais avait les yeux rivés au loin, suivant la côte, s'attachant indifféremment à un détail des rives rocheuses ou l'autre. Des falaises alternaient avec des plages de sable clair, desquelles partait une végétation rase, très éprouvée par la chaleur. La pierre blafarde reflétait durement la lumière et il était obligé de fermer les yeux à demi. Apercevant parfois de l'activité sur les terres, il jouait à imaginer ce qui s'y déroulait, sans grande conviction toutefois. Pour l'heure, il essayait de comprendre ce que défendaient les murailles parachevant l'éminence qui plongeait dans la mer. Il avait une fois de plus coiffé son chapeau de paille pour se prémunir du soleil, bien sensible malgré le vent. Un grincement derrière lui l'incita à jeter un coup d'œil de côté. Il aperçut la silhouette de Mauro Spinola, suivi d'un valet qui le protégeait de la lumière par une sorte d'ombrelle.

« Le bonjour. J'espère ne pas déranger. J'ai plaisir à venir prendre repos sur ce pont au matin. D'autant plus quand le ciel est au beau comme ce jour d'hui. Vous permettez ? »

Régnier inclina la tête poliment et vit arriver les serviteurs de Spinola qui lui installèrent un siège, fixèrent la protection contre l'ardeur du soleil et disposèrent sur une table basse quelques fruits secs, ainsi qu'un pichet avec un gobelet. Le vieil homme ordonna qu'on en monte un second.

« Vous aimeriez peut-être une goutte de vin ? Il n'est point trop fort et rafraîchit fort bien. Il ne vient pas de Chypre, mais le pourrait, tellement il est goûteux. »

Marquant une pause il huma les vapeurs s'échappant du pot.

« Il est d'ailleurs fâcheux que nous ne stoppions pas en cette île, cela nous aurait justement donné occasion d'en acquérir à bon prix. »

Tout en parlant, il avait servi à boire à Régnier, qui accepta de bonne grâce et s'assit sur un tas de cordage roulé à côté. Le marchand prélevait avec soin des fruits dans les coupelles de céramique décorées.

« J'ai vu que vous admiriez cette forteresse. Nous sommes au large de la baie de Saint-Paul, et voilà la cité de Lindos. On peut y faire visite à d'admirables vestiges des anciens temps. Les empereurs de Constantinople ont bâti ces murailles. Celé en une rade, le port se trouve à l'abri de la fureur des flots. Un endroit vraiment charmant !

— Vous y êtes déjà allé, pour si bien le conter ?

— Oui, maintes fois par le passé.

— Inutile de vous demander en quelle occasion, n'est-ce pas ? »

Le vieux diplomate esquissa un sourire narquois et ne répondit pas. Il avait porté son regard sur les côtes à l'ouest et avalait doucement les fruits qu'il avait sélectionnés. Lorsque son valet eut amené un autre verre, il se servit et se pencha vers le chevalier.

« Dites-moi, ami, êtes-vous satisfait de vos avancées ? Je sais que notre bon Malfigliastro se fait soucis. Mais vous me semblez fort serein. Seriez vous près de la solution de cette vilaine affaire ? »

Bien qu'il se doutât que la présence de Spinola n'était pas fortuite, Régnier fut tout d'abord un peu désarçonné par cette question directe. Puis il convint que le diplomate, pour être retors, n'en était pas moins un haut responsable génois, certainement en mission officielle, même s'il agissait dans l'ombre.

« Je l'espère. J'ai dénoué quelques nœuds et la voie me semble plus claire désormais. Mais je dois avouer que la vision d'ensemble n'est pas encore mienne.

— Vous ne vous dévoilez guère, ami. Auriez-vous crainte que je divulgue de vos secrets ? Je n'ose croire que vous me rangez parmi les possibles meurtriers !

— Cela n'a rien à voir. Je n'aime guère accuser sans preuve. Cela entache un nom à jamais, que la suite nous fasse raison ou tort. Donc tant que je n'ai aucune certitude, je préfère demeurer coi. »

Le regard du vieux diplomate brilla quelques instants, reflets du soleil sur les ondes ou éclair malveillant face à la rebuffade. Mais sa voix demeura calme et onctueuse lorsqu'il poursuivit.

« C'est tout à votre honneur. J'ai également fort réfléchi. À ce jour, je suis convaincu que nous avons là quelque banal larron et nulle conspiration. Si cela avait été, je ne crois que le charpentier aurait été meurtri. Occire un homme de peu serait nuisible à toute cause cherchant à se rallier la populace. L'acheter aurait été fort plus efficace.

— Et s'il avait refusé ?

— Il leur aurait suffi d'améliorer l'offre, jusqu'à ce qu'il agrée. »

Régnier n'appréciait décidément guère la mentalité du vieux Génois, même s'il en reconnaissait l'intelligence et la compétence. Mais il comprenait désormais pourquoi il n'aimait pas être en sa présence. Cet homme souillait tout avec ses concepts mesquins et avilissants. Il enduisait de boue tout ce qu'il effleurait du regard, incapable de voir derrière chaque objet ou chaque être vivant autre chose qu'un montant, une valeur qu'on pouvait additionner dans son petit carnet de cire. Herbelot répétait avec mépris que le Christ avait chassé les marchands du Temple le fouet à la main. À observer Mauro Spinola prendre le frais sous son ombrelle, habillé de sa délicate cotte de soie, Régnier pensa qu'il serait temps de recommencer le ménage avant qu'ils ne s'installent de nouveau pour de bon.

### Port de Rhodes, soir du 22 octobre

Ganelon et Ernaut étaient assis de part et d'autre de la passerelle d'accès au navire. Assistés de Rufus, ils surveillaient les montées et descentes des marins, et s'assuraient que personne ne portait sur lui l'argent disparu d'Ansaldi Embriaco. Le *patronus* avait interdit à quiconque en dehors des hommes autorisés par lui et Régnier de descendre du *Falconus*. De nombreux passagers avaient émis des protestations véhémentes et la grogne s'était installée sur le pont des pèlerins. Pour calmer les récriminations, un certain nombre de matelots de confiance avaient été désignés pour faire des achats, encadrés par Ernaut et Régnier. Mais l'interdiction de se rendre sur la terre ferme était tout de même perçue comme une brimade injuste par la majorité des personnes à bord.

Imperturbable, et heureux détenteur de l'autorité, Ernaut avait pu aller faire un tour en ville et acquérir ce qu'il souhaitait. Il goûtait désormais un peu de repos, assis sur le plat-bord, une jambe pendant dans le vide au-dessus de l'eau. Il regardait avec curiosité ce qui se passait à terre, dans cette cité si exotique : c'était la première ville orientale qu'il découvrait. Ces magnifiques murailles de pierre blanche avaient pourtant triste allure sous un ciel bas et une pluie fine persistante. La température demeurait clémente, mais les averses avaient dissuadé beaucoup de voyageurs de monter sur le pont supérieur. Ils devaient rester deux jours environ, et ils espéraient que les cieux s'ouvriraient les jours suivants, pour décharger le navire dans des conditions agréables.

Bien à l'abri sous sa chape, Ernaut gobait des grains de raisin qu'il venait d'acheter à un bon prix. Il était heureux de pouvoir enfin manger quelque chose de frais. Lassé des biscuits secs, il s'était également empressé d'acquérir du pain encore chaud de sa cuisson. Le tout était agrémenté de petits fromages de chèvre, à propos desquels il ne tarissait pas d'éloge.

Les officiers portuaires avaient été prévenus de l'enquête en cours et de la recherche d'un meurtrier et voleur. Du coup, une patrouille du guet était prévue pour surveiller tout particulièrement cette zone des quais. Le *Falconus* était amarré entre un gros navire grec et un plus modeste, provenant d'Ifriqiyah. Mais les distances étaient suffisantes pour qu'une personne ne puisse facilement passer d'un bord à l'autre. Le port était relativement calme, en raison du temps. La soirée était bien avancée et seuls quelques soiffards erraient encore, ainsi que des ouvriers en retard pour rentrer chez eux. Les portes des quartiers se fermaient peu à peu et les hommes du guet s'organisaient pour leurs rondes nocturnes. La ville se préparait paisiblement à entrer dans la nuit.

« Fichu temps, grommela Ganelon. Je serais mieux à terre à m'envoyer force rasades, le cul au sec, face à une vraie table. Et un bon souper fumant ! Quelque pièce de viande aprêtée !

— Moi aussi, j'ai tel appétit que je serais prêt à gloutonner n'importe quoi ! » approuva Ernaut.

La simple évocation d'un bon repas rappela des souvenirs au valet.

« Les Romains ont fameuse soupe de poisson. J'y ai goûté alors que nous avancions avec le roi Louis parmi leurs contrées. Et les boulettes de viande frites ! Dans du pain, un vrai régal !

— Qu'est ce que je ne donnerai pas pour un rôt ou une poignée de saucisses fraîches ! Les passages en mer, ça permet de se rendre compte comme l'on mange bien à terre. »

Après une petite ronde, Rufus revint vers eux et leur proposa de remonter la planche d'accès à bord, vu que les gens n'allaient désormais plus descendre. Alors qu'ils étaient occupés à la tirer et la reposer le long du plat-bord, la pluie s'arrêta enfin. Ernaut ôta sa capuche et leva la tête vers le ciel.

« On dirait que ça voudrait se dégager.

— Que voilà excellente idée. La lune est quasi ronde, ça nous rendra la besogne plus aisée. Difficile de rester discret si la lumière frappe les abords. »

Le marin dégaina un large couteau de sous sa chape.

« Et si jamais le fils à putain espère nous refaire le coup d'Otrante, je lui ferai goûter de ma lame. »

— Ça m'étonnerait qu'il ait vaillance d'attaquer trois personnes, lui rétorqua Ganelon.

— On ne sait jamais, c'est son ultime chance de fuir avant Gibelet…

— Avant les fourches[^fourches] pour lui ! » ne put se retenir Ernaut, l'air mauvais. Ganelon fit une moue, indiquant par là qu'il envisageait une autre possibilité.

« Pour le moment, il n'a pas été découvert. Adoncques il est peut-être tenté d'aller au bout et ainsi ne jamais se dévoiler. Fuir serait avouer, à ce point. »

Alors qu'ils parlaient, la nuit finissait de s'installer et des étoiles commençaient à s'affranchir des lambeaux de nuage. La lune se dévêtit bientôt des dernières nuées, éclairant d'une lumière bleutée les navires balancés lentement au gré des vagues. Ici et là, quelques veilleuses jaunâtres dessinaient des silhouettes orangées. Une patrouille du guet, déambulant d'une extrémité du port à l'autre, et une deuxième, allant sur les chemins de ronde de la fortification, projetaient sur la muraille de pierre des ombres gigantesques, comme si de terribles monstres veillaient sur la cité endormie.

### Port de Rhodes, matin du 23 octobre

Herbelot dormait à poings fermés lorsqu'il sentit qu'on le secouait doucement. Il se réveilla d'un bond, habitué à se lever rapidement pour assister à des offices nocturnes. C'était Ganelon, le visage fatigué. Le jour était visiblement bien entamé et des rais de lumière vive entraient par la trappe d'accès supérieure.

« Que se passe-t-il ?

— Messire Régnier vous mande au plus vite en la cabine du *patronus*.

— Ce n'est pas encore pour meurtrerie, j'espère ?

— Non, apparemment quelque nouvelté. Je n'en sais guère plus.

— Si c'est pour m'annoncer la fuite du meurtrier, ça aurait pu attendre que je m'éveille de moi-même. »

Herbelot repoussa ses couvertures et enfila rapidement la cotte posée sur son meuble, le long de son lit. Il passa également ses chausses, suspendues à une cheville en bois au-dessus de lui. Puis sortit de la bourse cachée sous son oreiller la clef de son coffre et récupéra sa ceinture. Enfin, il boutonna ses chaussures. Bien qu'encore un peu ensommeillé, il était prêt à affronter une nouvelle journée.

La lumière sur le pont principal l'aveugla presque et il sentit la chaleur agréable du soleil sur ses vêtements légèrement empreints d'humidité. Il ne put se retenir de jeter un coup d'œil alentour, sa curiosité ayant été éveillée par le brouhaha des quais.

Des flots ininterrompus de portefaix allaient et venaient par la grande porte, sous le regard de quelques soldats en faction sur la muraille, négligemment accoudés. Tout un monde bariolé s'activait, en particulier aux abords du principal passage. C'était là que se trouvait l'office des douanes, qu'on repérait bien, entouré qu'il était de marchands en belles tenues qui déambulaient, l'air affairé, souvent suivis de leur serviteur ou d'un clerc porteur de leurs documents officiels.

S'arrachant à la contemplation de cette ruche bourdonnante, Herbelot se rendit au château arrière où il retrouva Ganelon, devant la porte de la cabine. Le valet frappa une fois sur le bois et le fit entrer. À l'intérieur, Ribaldo Malfigliastro et Régnier discutaient autour de la table sur laquelle trônait bizarrement un seau. Il les salua poliment puis s'avança, intrigué par la présence de l'ustensile. Régnier lui donna l'explication.

« Voilà puant cuveau qui met en pièces notre idée d'un voleur, je le crains fort. »

Le clerc ne voyait pas bien en quoi ce simple récipient avait quelque chose à faire avec le meurtre et le vol. Il lança un coup d'œil interrogatif à Régnier, qui inclina de la tête vers le *patronus*. Ce dernier était visiblement contrarié, ses sourcils broussailleux obscurcissant son regard.

« Au matin, j'ai commandé aux mousses d'escurer tous les seaux d'aisance. Et, comme ce sont bons gamins, ils m'ont ramené ça,  trouvé en ce seau. »

Ribaldo fit tinter sur la table une poignée de pièces.

« Collées parmi la merdaille ! On a demandé aux passagers s’ils avaient perdu quelques monnaies… Jusqu'à ce point, aucune réponse. »

Régnier intervint.

« Vous voyez le souci. Si ce sont bien monnaies appartenant à Ansaldi Embriaco, elles ont été jetées en la mer. Le larron n'a pas conservé son butin ! »

Herbelot prit une profonde inspiration, écarquillant les yeux tandis que l'idée faisait son chemin dans sa tête.

« La meurtrerie n'aurait pas eu pour motif le vol, en ce cas ! »

Herbelot s'avança vers le seau et y risqua un œil dégoûté, appuyé par une moue expressive. Puis, un peu abattu, il se laissa tomber sur un escabeau contre le mur. Le commandant lui proposa un verre, qu'il accepta volontiers.

« Si l'envie n'est pas cause, cela peut être la colère. Nous en revenons à la possible vengeance, ou profond désaccord. »

Régnier paraissait aussi assez fatigué, ce dernier incident rendant les probabilités de trouver l'assassin encore plus faibles. Il avala un peu de vin.

« D'aucune façon, nous ne pouvons plus espérer des monnaies pour démasquer le fripon.

— On dirait qu'il se joue de nous, traçant fausses pistes les unes après les autres, comme s'il savait la façon dont on le traquait, renchérit Herbelot.

— Si ce n'est pas son prime forfait, rien de surprenant. Mais comment savoir ? Ansaldi n'avait aucun lien avec les autres négociants meurtris à Gênes. Et si son meurtrier est autre, nous n'avons aucun moyen de le découvrir.

— De plus, il doit deviner qu'il nous faut le débusquer avant de toucher la Terre sainte. Sans quoi, nous ne saurons jamais éclaircir cette histoire. »

Pendant tout ce temps, Ribaldo Malfigliastro était resté muet. Il semblait comme replié sur lui-même, tentant de démêler l'infernal écheveau. Il s'exprima d'une voix étonnamment douce.

« Et si maître Embriaco avait démasqué le mécréant ? Payant cette découverte de sa vie… Cela expliquerait les manières du marchand, à se cloîtrer ainsi. Mais en ce cas, pourquoi aurait-il accueilli quelqu'un qu'il soupçonnait ? Ou alors, il ne savait pas qui c'était, seulement qu'il était à bord. »
