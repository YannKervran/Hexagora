# Instructions de compilation pour epub et pdf A5 pour impression

## Création des fichiers epub

Dans le répertoire epub/destination/stockepub se trouve l'arborescence du contenu de l'epub. On peut y modifier les fichiers finaux puis recréer un nouveau ernaut_01.epub manuellement : se placer dans le répertoire epub/destination/stockepub, y effacer l'actuel fichier ernaut_03.epub puis taper la commande :

    $ zip -X0 ernaut_03.epub mimetype

afin de recréer un fichier avec juste le mimetype en premier, non compressé. Ensuite, on y ajoute tous les autres fichiers, avec leur hiérarchie :

    $ zip -X9Dr ernaut_03.epub META-INF OEBPS

## Mise en forme automatisée de l'epub avec les scripts dude

Se placer dans le répertoire et taper la commande :

    pandoc ernaut03_part00_garde_epub.md ernaut03_part01_introduction_epub.md ernaut03_part02_prologue.md ernaut03_part03_chap01.md ernaut03_part04_chap02.md ernaut03_part05_chap03.md ernaut03_part06_chap04.md ernaut03_part07_chap05.md ernaut03_part08_chap06.md ernaut03_part09_chap07.md ernaut03_part10_chap08.md ernaut03_part11_epilogue_epub.md ernaut03_part12_annexes.md ../../notes/notes.md --wrap=none -o epub/ernaut_03_epub.md

On obtient ainsi un document markdown unifié qui comprend les appels de notes et les bons éléments adaptés au fichier epub.

Ensuite, aller dans le répertoire epub

    cd epub/

Entrer la commande dudeit adaptée (vérifier qu'on a bien installé le script python dans un répertoire d'où il peut être appelé) :

    dudeit -e ernaut_03_epub.md

On obtient alors les différents formats epub dans le répertoire epub/destination/stockepub. Les autres répertoires ne sont utilisés que pour la génération de fichiers dokuwiki pour inclusion dans le site.

## Compilation du pdf pour l'impression A5

Se placer dans le répertoire et taper la commande :

    pandoc ernaut03_part00_garde_pdf.md ernaut03_part01_introduction_pdf.md ernaut03_part02_prologue.md ernaut03_part03_chap01.md ernaut03_part04_chap02.md ernaut03_part05_chap03.md ernaut03_part06_chap04.md ernaut03_part07_chap05.md ernaut03_part08_chap06.md ernaut03_part09_chap07.md ernaut03_part10_chap08.md ernaut03_part11_epilogue.md ernaut03_part12_annexes.md ../../notes/notes.md --wrap=none -o TeX/ernaut03_impress.tex

