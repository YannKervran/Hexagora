## Remerciements

La publication de ce troisième tome des enquêtes d'Ernaut n'aurait pu voir le jour sans la rencontre avec la formidable association Framasoft et sa maison d'édition Framabook et tout particulièrement Christophe Masutti. Grâce à eux, ce livre qui dormait dans mes tiroirs depuis des années va enfin pouvoir trouver son public.

J'ai eu par ailleurs la chance de m'appuyer sur une riche documentation photographique aimablement fournie par Bassam Almohor, me donnant l'occasion d'admirer des paysages dont je n'ai jamais senti le vent sur ma peau.

Enfin, il est un écrivain qui a irrigué tout mon travail préliminaire en plus de mes lectures depuis des années, il ne me semble donc pas déplacé de le remercier lui aussi, en particulier pour son *Hussard sur le toit*, merci donc à Jean Giono.

*En mémoire de mes grands-parents Pierre et Marie-Josée, gens de la terre qui n'en négligeaient pas pour autant ceux qui la parcouraient*

### Plan de la Mahomerie

![Plan de la Mahomerie](ernaut3_plan_reference_mahomeria.jpg)

## Liste des personnages

***Habitants de la Mahomerie***

+ Albert le Fèvre  
+ Aymeric le Grand  
+ Bernard, dit Bourgogne  
+ Gautier le Petit  
+ Godefroy le Borgne  
+ Guillaume de Saint-Gilles  
+ Guillaume le Provençal  
+ Hélias  
+ Hugues de Saint-Élie  
+ Iohannes  
+ Lambert le Bourguignon, frère d’Ernaut  
+ Martin  
+ Mathilde  
+ Ogier  
+ Osanne, fille de Godefroy  
+ Perrotte, épouse de Godefroy  
+ Pisan, intendant du Saint-Sépulcre  
+ Robert Plantevigne  
+ Simon Rufus  
+ Thibault, fils de Bourgogne  
+ Umbert, valet d’Ogier

***Administration royale***

+ Arnulf, vicomte  
+ Baset  
+ Droart  
+ Ernaut  
+ Eudes  
+ Larchier  
+ Raoul  
+ Ucs de Monthels, mathessep

***Cour des Bourgeois de Jérusalem***

+ Albert Lombard  
+ André de Tosetus  
+ Pierre de Périgord  
+ Pierre Salomon  
+ Simon Rufus

***Habitants du casal de Salomé***

+ Abu Mahmud, dit Pied-Tort, shaykh  
+ Abu Qasim al-Dabbi, ra’is  
+ Robert de Retest, seigneur

***Habitants de la Tour-Baudoin***

+ Gosbert  
+ Layla

***Habitants du casal de Saint-Gilles***

+ Aubert, père de Guillaume de Saint-Gilles  
+ Clarembaud
+ Pedro le Catalan

***Habitants du casal de Mahomeriola***

+ Libourc  
+ Mahaut, sa mère  
+ Sanson, son père

***Habitants de Jérusalem***

+ Abdul Yasu  
+ Aloys  
+ Blayves le Boiteux  
+ Clément Ferpieds

***Tribu bédouine des Banu Kamil***

+ Abu Hamza

***Habitante du casal de Seylon***

+ Munya
