## Chapitre 2

### Casal de la Mahomerie, mercredi 11 juin, après-midi

Son frère emmené par les valets de la curie, Ernaut était rentré dans le manse. Le soleil accablait désormais ceux qui s’attardaient dehors et traquaient la moindre parcelle d’ombre. Même les bêtes tentaient de lui échapper, en vain. C’était le moment pour les hommes de s’allonger au pied d’un arbre, dans un cabanon ou un repli de terrain. La chaleur régnait, impériale, et tenait les hommes à sa merci.

Les cigales et les grillons, quelques rapaces affamés, les lézards et les serpents étaient les seuls à oser s’y mesurer. Étendu sur le lit, bras sous la tête, Ernaut préparait ses plans. Il n’était pas certain de ce qu’il allait faire en premier. Ses précédentes expériences lui avaient appris que pour pister un assassin, on pouvait s’intéresser à la façon dont il s’y était pris ou aux motifs qui avaient rendu son bras meurtrier.

Il s’assit sur le bord de la couche, les coudes sur les genoux. Ogier allait être rapidement enterré, maintenant que les jurés avaient vu sa dépouille. Il fallait donc commencer par aller voir les blessures qui lui avaient été infligées, avec quel genre d’arme, si c’était possible. Il espérait aussi, naïvement, que de voir le corps du défunt lui en apprendrait un peu sur l’homme qu’il était. Il ceignit son baudrier, sa masse, vérifia que sa bourse contenait quelques deniers et descendit, affrontant la chaleur crue de la rue.

Il n’avait qu’une poignée de façades à dépasser pour retrouver l’église, passa en une rapide foulée le portail et se réfugia dans la fraîcheur de la nef. Le lieu avait été débarrassé et un groupe de pèlerins discutait près de l’autel. Il s’approcha de la dépouille, toujours couverte de son linceul, une bougie à sa tête. Vérifiant que personne ne lui prêtait attention et qu’aucun serviteur de l’église n’était proche, il souleva le drap.

Les chairs molles, affaissées, du visage lui donnaient un air stupide, de grosses bajoues s’étalant en plis disgracieux. Le crâne à demi-chauve s’ornait d’une maigre toison, soigneusement coiffée. Son nez, large, semblait à toute force vouloir rejoindre le menton replié en un bourrelet de graisse. Les lèvres fendues, éclatées, quasi invisibles, retombaient dans la bouche indiquant l’absence d’un grand nombre de dents. Des traces violacées couvraient tout le bas de son visage. L’homme n’avait guère été séduisant.

Repoussant le drap plus avant, Ernaut constata que le cou, puissant, portait de nombreuses traces, des écorchures désormais livides. La poitrine avait reçu plusieurs coups, d’une lame fine. Certaines blessures étaient superficielles, simples déchirures glissant sur les côtes, mais plusieurs coups avaient pénétré profondément. L’assassin ne s’en était pas contenté : il avait également fouaillé les chairs tendres de l’imposant abdomen. C’était à l’évidence l’œuvre d’un forcené.

Ernaut reposa le drap et examina les mains et les bras. Ogier n’avait pas tenté de se défendre, ses poignets portaient des traces de ligatures serrées, qui avaient rendu ses mains de rude travailleur violacées et gonflées. Vérifiant si les chevilles avaient connu le même sort, il écarta le linceul une nouvelle fois, s’attardant sur ses jambes. Ses yeux s’écarquillèrent d’effroi quand il aperçut ce que le meurtrier avait fait au bas-ventre.

Les parties génitales avaient été déchiquetées avec sauvagerie. Pas simplement tranchées. Ogier était certainement mort de cela, ou du moins y avait perdu tout le sang répandu désormais sur le sol de sa demeure. Il avait dû souffrir horriblement avant de lâcher son dernier souffle. Pas étonnant qu’Aymeric ait eu désir de le venger s’il était son ami. Ernaut eut soudain une bouffée d’estime pour celui que son frère devrait affronter avant la fin de la semaine. Il soupira, réinstallant le drap avec soin.

Il venait juste d’achever sa tâche et se tenait devant le corps, analysant ce qu’il en avait compris quand une petite toux le fit se retourner. Un jeune homme au visage étroit, le front bas, le détaillait avec curiosité, un balai à la main. Ernaut lui fit un sourire évanescent avant de prendre la parole.

« Horrible fin pour ce pauvre Ogier, n’est-ce pas ? »

Le valet haussa les épaules.

« Dieu rappelle à son heure.

— Est-ce toi qui a préparé sa dépouille ?

— Avec un mien camarade. Il pèse son poids et on n’était pas trop de deux pour le porter ici.

— Tu as fait sa toilette ?

— Bien sûr ! Il ne pouvait partir dans l’état où il était. »

Ernaut hocha la tête en assentiment.

« Peux-tu me dire comment il était vêtu ?

— Et pourquoi que je ferai ça ?

— Savoir ce qui lui est arrivé apaiserait les craintes. On pourrait prier pour lui en toute sincérité. »

Le jeune homme ne parut qu’à demi-convaincu, mais il avait plus envie de parler que de s’acquitter de sa tâche. Il s’appuya sur le manche de son outil.

« Il avait que sa chemise et ses braies. On a tout jeté, c’était poisseux de sang et… et d’autres choses…

— Il n’avait pas encore une de ses chausses ?

— Comment que vous savez ça ? Le valet passa une langue sèche sur les lèvres, mal à l’aise. Il l’avait pas enfilée ! On lui avait nouée autour de la bouche, et pis avec des noeuds drôlement serrés. On a dû les trancher avec un canivet. »

Ernaut fit une grimace.

« C’est toi qui as rompu ses liens ? J’ai vu les cordes en son hostel.

— Oui-da. C’était plus facile pour le porter. Pauvre gars. Il était pas bien gentil, mais de là à le traiter comme une bête.

— Tu veux parler de… »

Ernaut désigna subrepticement son bassin, le visage tendu, comme dégoûté. Le valet fit un rictus guère plus engageant et confirma d’un signe de tête.

« Moi j’ai pas pu y toucher, c’est Giraud qu’a tout ramassé comme il a pu. On a mis ça dans une toile, et on l’a posé là où le Bon Dieu l’avait placé à sa naissance. Qu’il parte comme il est venu, sans rien qui manque.

— Vous en avez parlé ?

— Ah certes pas ! C’est pas chose à raconter. Préparer les morts, c’est tâche de confiance. On en apprend plein sur eux, on découvre des secrets. Tout ça, c’est entre Dieu et le moribond, alors Giraud et moi, on dit jamais rien. »

Il renifla avec vigueur, tordant les lèvres.

« On en cause même pas entre nous. Quand c’est fini, on oublie. »

Il jeta un coup d’œil vers le groupe de pèlerins, d’où un cri venait de s’échapper, de douleur ou de joie. Il souffla.

« On va le porter en terre d’ici la nuit, le père a dit qu’il était pas bon de fort tarder, d’autant qu’il a pas pu confesser ses pêchés. Son âme doit espérer du Bon Dieu. »

Ernaut décrocha la bourse de son braiel[^braiel] et en fit tomber quelques pièces argentées, les tendant au valet.

« Tiens, tu lui paieras un ou deux cierges à cette occasion. Garde une monnaie pour ton compère et toi vous payer de quoi boire, en remerciement de vos soins. »

Avec un sourire reconnaissant, le serviteur fit disparaître les pièces dans sa main osseuse.

« Que Dieu vous bénisse, l’ami ! »

Ernaut hocha la tête tristement et s’éloigna. L’odeur du cadavre commençait à s’insinuer en lui, à se dissoudre dans sa tête, à polluer ses pensées. Ogier avait subi d’effroyables tortures. Il avait peut-être un magot après lequel on espérait. Jamais des brigands n’auraient osé s’introduire à la nuit dans la demeure d’un riche bourgeois pour lui faire avouer où il celait ses deniers. D’ailleurs, le casal se trouvait dans un des endroits les plus calmes du royaume. Les voyageurs pouvaient aller et venir sans crainte et le berger esseulé n’avait pas de motif de s’inquiéter des voleurs.

Le défunt était un homme solide, empâté, mais qui avait travaillé toute sa vie. Il n’avait pas dû être facile à maîtriser. Plusieurs agresseurs auraient été nécessaires pour s’emparer de lui, le bâillonner, le ligoter. Si c’étaient des gens du village, ils devaient faire bloc, instiller le doute et la suspicion sur d’autres pour les détourner loin d’eux. Mais il n’existait nul secret si terrible qu’il ne puisse diviser les hommes. Ernaut en avait été plusieurs fois témoin : il demeurait toujours un moyen afin d’inquiéter l’un ou l’autre, l’amener à dénoncer ses complices, parfois en promettant l’amnistie au plus prompt à trahir. Il méprisait ce genre de personnes, il en appréciait juste l’utilité, en tant que sergent du roi.

Lorsqu’il sortit de l’église, une langue de feu lui brûla la peau et la cornée. Il se figea. Un chien vint en frétillant de la queue quémander quelques caresses ou une friandise. Il le chassa d’un revers de la main, agacé.  Sans trop y penser, il dirigea ses pas vers la croix de pierre peu éloignée. Elle était fixée sur une estrade de quelques marches, d’où l’on pouvait voir la Cité sainte à une poignée de lieues vers le sud. Transpirant désormais à grosses gouttes, il gravit les degrés en quelques foulées et porta son regard au loin, la main sur le front pour se protéger de la lumière.

Par-delà les reliefs poussiéreux, gris et cendrés, il apercevait le mont des Oliviers, un peu sur sa gauche, moucheté de vert sombre. Des pentes sur lesquelles Jérusalem était installé, on ne devinait que la pointe occidentale, plus élevée. Ernaut plissa les yeux, devinant, lui semblait-il, la tour de Bohémond, par là où les croisés avaient pris la ville, des années plus tôt. Ils avaient ri comme des enfants insouciants, avec Lambert, la première fois qu’ils avaient aperçu les murailles de la Cité sainte.

Des mois avaient passé et cette terre était devenue la leur, les soucis étaient reparus tandis que les récoltes croissaient. Devenu sergent, Ernaut avait espéré un moment que cela comblerait en lui le vide qu’il avait ressenti après son combat avec Maciot. Finalement, il avait senti les ténèbres le ronger peu à peu et il craignait chaque fois davantage lorsqu’il envisageait de se confesser. Il espérait que sa faute ne retomberait pas sur Lambert. Homme d’action, il ne voulait néanmoins pas se contenter d’espoir. Il lui fallait débusquer la vérité et amener Aymeric à retirer son accusation.

Il se mordit la lèvre, surpris d’une idée qui venait de se former dans sa tête. Si jamais il découvrait que c’était bien Godefroy le coupable, il lui faudrait encore convaincre son frère, ce qui serait tout aussi ardu. Le front plissé de sombres pensées, il dévala l’escalier et retourna à grandes enjambées vers la demeure de Lambert. Il lui fallait un endroit frais pour penser, comprendre. Un endroit frais, et un pichet de vin clair. Si seulement Libourc était là !

### Casal de la mahomerie, mercredi 11 juin, veillée

Ernaut revenait d’un pas lourd de chez Godefroy. Le repas avait été triste, long et pesant. Personne n’avait eu le cœur d’entretenir la conversation, chacun se contentant de noyer son regard dans son écuelle. Après quelques échanges sur le défunt, demeurés sans grand intérêt, le silence s’était installé. Les timides sourires d’Osanne n’avaient pas suffi à réchauffer l’atmosphère et, après un verre de vin miellé rapidement avalé, Ernaut avait préféré les quitter.

La lune, le disque quasi plein, était rongée par les collines, mais répandait néanmoins une lumière bleutée, apaisante. Des portes claquaient de temps à autre, des enfants chantaient au loin, un bébé criait tout à côté. Les cigales continuaient à chanter, indifférentes aux ténèbres. Ernaut obliqua vers un sentier pour rejoindre les jardins, à l’arrière des maisons, afin de vérifier que sa monture allait bien.

Lorsqu’il poussa le portillon qui accédait au courtil, un chat s’enfuit à toute allure depuis les broussailles voisines. Tranquillement installé, le cheval tourna à peine les oreilles lorsqu’il sentit qu’on approchait. Il s’ébroua en guise de salut et fit mine de s’intéresser au foin devant lui. Le jeune homme le caressa, appréciant le contact chaud et vibrant.

Il allait ouvrir la porte située à l’arrière de la maison, qu’il n’avait pas barrée à dessein, lorsqu’on l’interpella depuis l’entrée du jardin. Il se retourna, fronçant les sourcils pour mieux discerner la silhouette. L’homme était de taille moyenne, plutôt mince, habillé à l’occidentale avec une cotte et des chausses. Il avait levé la main en guise de salut. Son visage demeurait impénétrable avec l’obscurité. Instinctivement, Ernaut approcha la main de son ceinturon. Ogier avait peut-être été interpellé ainsi alors qu’il allait aux latrines.

« Mille pardons pour venir à la nuitée, je suis arrivé chez Godefroy quand vous en partiez. On me nomme Iohannes. »

Ernaut grogna, attendant toujours d’en savoir plus. Il se passa un moment avant que l’inconnu ne reprenne.

« Il m’a dit que vous vouliez faire lumière sur cette meurtrerie. Adoncques j’ai désir de vous aider.

— En quoi l’affaire vous concerne ?

— J’ai tout d’abord bonne estime envers Godefroy, et pour d’autres raisons que je serais heureux de vous détailler. »

Ernaut soupira, puis se tourna vers la porte, indiquant d’un geste d’avancer.

« Entrez, je peux bien vous offrir un gobelet des tonnels de mon frère. »

La grande salle était illuminée par une lampe à huile tremblotante laissée là à son départ. Il fit signe à Iohannes de s’asseoir, jeta ses armes sur le lit puis attrapa un pichet et deux gobelets qu’il posa sur la table. Il versa le vin et prit place face à son visiteur, profitant de la lumière pour l’examiner en détail. L’homme était un peu plus âgé que Lambert, des rides prolongeant paupières et lèvres. Son front haut surplombait un nez discret, bosselé au milieu. Sa bouche fine ne semblait guère dessinée pour le sourire ; tout en lui inspirait la tristesse. Iohannes n’était pas un homme heureux.

« Vous devez avoir méfiance de ma venue, mais je me suis dit qu’il valait mieux ne pas tarder. Il n’y a que fort peu de temps pour lever le voile en cette affaire.

— Vous demeurez au casal ?

— Oui-da, depuis plusieurs années. Je n’ai pas grand manse, car on m’emploie souvent comme traducteur, surtout l’intendant. Je loge près de la curie, en bas.

— Dans le quartier du Puits ? »

Iohannes ne réprima pas son sourire narquois.

« Certes, mais je suis surtout un homme de la Mahomerie, je n’ai nulle querelle avec ceux d’en haut.

— Vous dites vouloir m’aider à découvrir ce qui s’est vraiment passé ?

— Je ne crois guère que Godefroy, que je pense bien connaître, ait pu occire si vilainement Ogier. Pourtant, j’aurais grande peine à voir Aymeric pendu parce qu’il a accusé fautivement.

— La faute lui en revient, il n’avait qu’à se taire.

— De certes. Mais s’il est pendu, cela ne résoudra rien. Un murdrier sera toujours parmi nous. »

Ernaut avala une gorgée de vin, circonspect. Si l’homme servait parfois l’intendant du casal, il était peut-être en mission pour le Saint-Sépulcre, toujours inquiet de ce qui se passait sur ses terres. Il se souvint avec angoisse des événements qui s’étaient déroulés autour du tombeau du Christ, près de leur couvent, quelques mois plus tôt. Il pinça les lèvres, attentif.

« J’ai bonne connaissance des lieux et je pense que les habitants ont fiance en moi.

— Auriez-vous idée de qui a pu frapper ?

— Non. On savait bien Ogier un peu querelleur à l’occasion, mais rien de nature à le faire mourir.

— Des querelles avec qui ? demanda Ernaut, se penchant sur la table, accoudé.

— Tout le monde et personne. Il parlait fort, vociférait à diable lorsqu’on le contrariait. Mais offrait tout aussi vite une gorgée de son vin. »

Iohannes leva son gobelet et porta un toast silencieux, avala une petite gorgée. Il se frotta le nez, comme indécis, avant de reprendre.

« On dit que vous êtes de la sergenterie du roi…

— En effet, mais je ne fais pas enquête en tant que tel.

— Vous pouvez vous enquérir de moi auprès de Ucs de Monthels. Le mathessep me connait et il vous donnera mes gages. »

Ernaut continuait à dévisager son visiteur nocturne. Il trouvait l’homme sympathique et lui aurait accordé toute confiance rapidement par le passé. Mais les semaines à traquer les menteurs et les escrocs, le poids de sa propre culpabilité, le rendaient suspicieux.

« Que pouvez-vous me dire du casal, des familles d’importance ?

— Eh bien, il y a les Rufus, toute une fratrie installée ici depuis longtemps. Aymeric, que vous avez vu, et son compère Gautier, sont avec eux les plus éminents du Quartier du Puits.

— Et dans ceux d’en-Haut ?

— Il n’y a pas vraiment de meneur selon moi. Beaucoup sont arrivés depuis peu, au casal et dans le royaume. La plupart se contentent d’œuvrer à leurs champs et gardent la tête basse. Ils ne se mélangent guère, sauf par nation. »

Un court silence s’installa, Ernaut hésitait à poser la question qui lui brûlait les lèvres.

« Des gens comme mon frère et moi ?

— Oui et non. Votre frère est homme paisible. Il n’a pas cet esprit étroit que j’ai si souventes fois encontré chez ceux venus de loin. »

Ernaut fixa son interlocuteur, étonné. Il détailla le visage fin, le regard brun. L’homme parlait très bien sa langue, mais portait un nom grec.

« Vous n’aimez guère les nouveaux on dirait ?

— Je n’ai rien contre les hommes de paix comme Lambert. Seulement, tous ne sont pas ainsi. »

Il fixa à son tour Ernaut dans les yeux, le visage tendu, traversé par une souffrance profonde.

« Je suis né ici, mais mon père était de vos pays. J’ai encontré plus souvent qu’à mon tour hostilité et méfiance de la part des marcheurs de Dieu, des porteurs de la Croix.

— Ogier était-il de ceux-là ?

— Il venait d’un casal plus avant vers Naplouse et vivait dans le royaume depuis long temps. Il parlait l’arabe aussi bien que moi.

— Vous entendez la langue des mahométans ? » demanda Ernaut, surpris.

Iohannes tiqua au nom qu’Ernaut avait utilisé, amusé. Il en oublia instantanément le vouvoiement.

« Tu ne les traites donc pas de païens, toi ?

— J’ai appris qu’ils ne l’étaient pas. Si j’ai bien compris ce sont plutôt des hérétiques, non ?

— Que voilà étrange remarque, s’esclaffa Iohannes. Je ne suis pas clerc du chapitre Notre-Dame pour en discuter. De nombreux paysans des casaux voisins sont serfs, musulmans attachés à leurs terres. La plupart ne parlent pas vos langages, alors je leur traduis les volontés de leurs maîtres. »

Ernaut se mit à jouer avec la mèche de la lampe, faisant danser les ombres sur les murs. Une odeur âcre engendrée par la fumée se répandit dans la pièce. Se léchant les doigts de temps en temps, il semblait indifférent à la chaleur. D’une voix calme, il reprit son interrogatoire, passant également au tutoiement, qu’il préférait.

« Il serait possible que l’un d’eux soit venu porter le fer en ce casal selon toi ?

— Possible, bien sûr. Mais pour quelle raison ? Le village de Salomé, à côté d’ici, est peuplé de musulmans auxquels on ne se mélange pas. On s’aperçoit d’un champ l’autre, sans plus. Chacun ignore ceux qui ne sont pas des siens.

— L’envie aurait pu les guider. Cet Ogier était homme fortuné.

— Certes, on le dit. En ce cas, il aurait fallu qu’un des serfs l’apprenne et ose venir ici. Et puis quoi ? Que faire de sa fortune quand on appartient, de corps et de biens, à un seigneur ? D’autant que Robert de Retest n’est pas le plus doux d’entre eux. »

Ernaut prit le pichet et remplit de nouveau les verres, le front soucieux.

« Des brigands par chez vous ?

— Pas depuis des années. Mais cela ne veut rien dire. Leur propre est de venir quand on les attend le moins.

— Mon frère m’a parlé de pasteurs sur les terres de l’est.

— C’est vrai, le Jourdain ne se trouve pas à plus de dix lieues, on y encontre toujours des nomades.

— On les dit voleurs, l’un d’eux est peut-être aussi murdrier. »

Iohannes fit la moue, dubitatif.

« Je n’y crois guère. Lorsqu’ils rapinent, ils le font à plusieurs, de jour, et emportent vite des bêtes. Ils n’aiment pas tant verser le sang, qui amène toujours des ennuis. On peut aisément rendre une bête volée. Ils ont grand usage de la vengeance et n’en prennent pas le risque sans réfléchir.

— Et un motif pour la meurtrerie ? Il y a toujours noir secret qu’on se chuchote entre voisins.

— Je n’en ai pas connoissance. Les gens n’aiment guère parler des morts, surtout ceux qui ont trépassé par violence. Cela attire le mauvais œil. »

Iohannes se leva, finissant son vin d’un grand geste. Il fit claquer le gobelet de terre et sourit à Ernaut.

« Je vais te laisser pourpenser cela. Demain, j’ai quelques travaux que je ne peux surseoir. Si tu le souhaites, je te rejoindrai dès ceux-ci finis.

— Je ne pense pas quitter le casal de la journée, tu me trouveras ici ou dans les environs. Je suis aise de te savoir à mes côtés, j’espère que cela nous permettra d’éviter la bataille.

— Puisse Dieu t’entendre » lui répondit Iohannes, tendant une main amicale.

### Casal de la Mahomerie, mercredi 11 juin, nuit

La chaleur du jour emplissait toujours l’air et, les pieds poudrés par le chemin, Ernaut déambulait. Il avait besoin de trouver le sentier par lequel aborder cette énigme. La lune étant désormais dissimulée par-delà l’horizon, la nuit devenait très sombre et les étoiles scintillaient dans des ténèbres veloutées. Le jeune homme leva la tête, tentant d’en reconnaître certaines. Il n’avait jamais pu en retenir les noms, et savait à peine pointer le nord. D’autant que les constellations étaient nouvelles en Terre sainte. Même Lambert, qui se targuait d’en avoir quelques rudiments, y perdait son latin. Les savants disaient que ces petites lumières présidaient parfois à la destinée humaine et se faisaient forts d’en prédire ainsi le déroulement.

Ernaut se frotta le crâne. En tout cas, elles avaient vu comment cela s’était fini pour Ogier, témoins muets des exactions barbares. Il avançait vers l’arrière des maisons occidentales, souhaitant étudier de nuit le jardin du défunt. Peut-être le coupable hanterait-il le lieu, rongé par le remord, ou brûlant de l’espoir de recommencer. Incapable de voir les pierres et les cailloux, les trous et les ressauts, Ernaut butait presque à chaque pas. Un petit chat vint se blottir contre sa jambe en ronronnant. Il le ramassa et avança en le caressant, arrachant à l’animal des soupirs de félicité.

« Et toi, l’ami, ne saurais-tu dire qui a frappé ? » chuchota-t-il à la bête. Il était désormais derrière la maison, que rien ne distinguait particulièrement. C’était le quatrième jardin depuis le chemin, après celui de Godefroy. Comme les autres, il était ceint d’un petit muret de pierre, qu’on franchissait par un guichet de branches grossièrement taillées. Des broussailles s’étaient enracinées sur ce relief. Une unique fenêtre perçait la façade arrière. Ernaut y discerna une très faible lueur, sûrement la veilleuse laissée là par le valet.

Par-delà le sentier s’étendaient champs et vignes. Les moissons n’avaient laissé que des tiges rases, grises, qui alternaient avec les ceps torturés. Les oliviers étaient un peu plus loin, sur d’autres coteaux. Plusieurs figuiers avaient été plantés le long du chemin et Ernaut s’assit sous leur couvert, sur une pierre, sans quitter les maisons du regard. Une toux rauque s’éleva, un bébé hurla. La nuit était calme.

Il manquait à Ernaut le son régulier des cloches des couvents, qui appelaient les moines à la prière. Ici, l’église ne servait que pour la paroisse et ne sonnait pas la nuit. La petite boule de poils s’était endormie et Ernaut la contempla un instant, la patte débordant de sa main. Confiante, indifférente, insouciante. Le jeune homme sourit. Un mouvement furtif attira soudain son attention. Une silhouette claire se faufilait le long des jardins plus au nord avec célérité, sans bruit, en direction du sud.

Retenant sa respiration, Ernaut se fit aussi petit qu’il le lui était possible, parmi les branches et les feuilles. L’inconnu passa sans le remarquer, suivant les murets. Après quelques instants de réflexion, le jeune homme se mit en chasse, reposant son compagnon d’un temps parmi les pierres. L’autre ne semblait pas prêter attention derrière lui et avançait aussi vite qu’il le pouvait. Ernaut sentit son cœur s’accélérer. Si jamais c’était le coupable, il pourrait innocenter Godefroy aisément et annuler la bataille. Mais il lui fallait peut-être pour cela attendre que le maudit ait frappé une nouvelle fois, prouvant ainsi sa malignité.

Ernaut fit la grimace, il regrettait de ne pas avoir pris son épée. Il serra les poings, dont il ne doutait pas qu’ils puissent suffire à calmer les ardeurs de tout assaillant. Il maintenait une bonne distance, se pelotonnant derrière les buissons, tendu vers un seul gibier, traquant sa proie comme un fauve attentif. Ils franchirent le chemin qui séparait le Haut du Bas du casal. En quelques foulées, l’espace découvert était derrière eux. L’inconnu continuait à avancer avec détermination jusqu’à stopper net devant un arbre. Il s’accroupit.

Ernaut fit de même, dans des broussailles bordant les champs. Puis ils attendirent. Rien ne bougeait. Ernaut n’osait pas même respirer. Des démangeaisons imaginaires venaient le perturber, mais il était tout entier à sa traque. Des miaulements devinrent feulement puis bagarre non loin d’eux. La forme se remit en mouvement, sautant par-dessus la barrière, disparaissant derrière le mur de séparation.

Ernaut se mit à trottiner, dans l’espoir d’arriver à temps sans pour autant prévenir de sa charge. Lorsqu’il arriva au coin, il écarquilla les yeux : l’inconnu serrait quelqu’un dans ses bras. Ernaut tenait le félon, et il venait à point. Il poussa la barrière d’un mouvement vif tandis qu’un rugissement s’échappait de sa poitrine. Il avançait le bras levé, prêt à l’abattre telle l’épée de l’archange saint Michel.

Un cri étouffé lui parvint aux oreilles : l’assassin était-il en train d’étrangler sa pauvre victime ? Sur les derniers pas, il bondit tel un fauve. Son poing allait s’écraser sur la nuque de l’assassin lorsque les deux corps emmêlés tombèrent au sol. Il fouilla de ses mains pour s’emparer du coupable. L’autre forme était une jeune femme, en chemise. Sa voix perça le silence de la nuit : « Pitié ! Nous ne… »

Ernaut se figea, recevant à cette occasion un coup de pied dans le tibia de la part de l’agresseur, qui lança, le souffle coupé : « Rentre, Mathilde ! »

Espérant attraper un bras, Ernaut l’empoigna si fort que l’autre laissa échapper un cri de douleur. Se tortillant comme un ver, il tentait de frapper, de façon désordonnée. La jeune fille s’efforçait, quant à elle, de cogner de ses petits poings sur le dos d’Ernaut, espérant ainsi le faire lâcher. Il chercha à l’empoigner, tournant sur lui-même. Soudain il comprit la situation et lâcha le jeune homme, se mettant à rire. Les deux silhouettes se rassemblèrent immédiatement, noyées dans les bras l’une de l’autre. Ernaut leva le regard vers la maison.

« Mille pardons, j’ai cru que le murdrier frappait de nouveau. »

Une exclamation indignée, retenue, jaillit alors.

« C’est pas vous ?

— Certes non. Sinon j’aurais pris une lame avec moi. Je voulais seulement attraper le félon… »

Le jeune homme s’approcha alors, tenant sa compagne par l’épaule, parlant tout bas, d’une voix essoufflée, habitée d’émotions.

« Parlez moins fort, messire, nous risquons d’attirer l’attention… »

La jeune fille, couverte de sa chemise, les cheveux tombant en une longue natte peu serrée était agrippée à lui, effrayée. Ernaut se rapprocha à son tour et murmura : « M’est avis que nous ne devrions pas demeurer ici, avec ce raffut, il y a grand risque que quelqu’un vienne, non ? »

Les deux adolescents se dévisagèrent, s’embrassèrent tendrement, sans trop s’attarder. Puis la jeune fille glissa dans la maison, reposant la barre derrière elle. Ernaut, un large sourire sur le visage, attira son désormais compagnon en dehors du courtil, vers le chemin.

« Je me vois bien désolé de t’avoir gâché si amiable rencontre… »

L’autre ne répondit pas, se contentant de hausser les épaules.

« Vos parents savent que vous avez tendre commerce ?

— Par tous les saints, si jamais… s’emporta l’autre.

— Tout doux, l’ami, ce n’est là point menace, mais demande. »

Le jeune homme toussa avant de répondre.

« Mon père ne partagerait pas la lèpre avec le sien s’il était ladre.

— C’est pour ça que tu te faufiles de nuit ?

— À chaque fois qu’on peut. »

Il échappa un soupir. Ernaut lui secoua le bras amicalement.

« Calme, compère, on n’est guère âgés tous deux, tu peux bien me parler comme compaing. »

L’autre le dévisagea, inquiet. Il avait besoin de se confier et un inconnu à peine plus vieux que lui pouvait convenir.

« On va s’enfuir tantôt. Ils cherchent des colons un peu partout. On se mariera et on aura des enfants.

— Tu as du bien pour ça ? Fol celui qui veut prendre épouse sans de quoi la nourrir.

— J’ai pas peur du travail, et j’ai mis de côté presque cent deniers » rétorqua-t-il d’un air bravache.

Ernaut se sentit de l’amitié envers ce garçon et le prit par l’épaule, amusé. Il s’efforça de garder un air sévère, afin de ne pas vexer.

« J’ai nom Ernaut, je suis sergent du roi, et je veux bien contribuer à ton pécule si tu me prêtes assistance, l’ami…

— Martin, fils de Pons. Que dois-je faire ?

— Je voudrais juste savoir si tu as rencontré quelqu’un voilà deux nuits, en allant retrouver ton amiete.

— Tu penses au vieil Ogier, hein ?

— Je vois que je n’ai pas affaire à benêt ! »

Martin fronça les sourcils, indécis quant au sous-entendu de la phrase, mais n’ajouta rien.

« As-tu vu quelque chose d’anormal ? Te souviens-tu de quoi que ce soit ?

— J’aurais bien aimé t’aider, mais je n’ai souvenir de rien. Je n’ai guère la tête à musarder quand je file voir Mathilde.

— Je comprends, mais si jamais quelque chose te revient… »

Ernaut fouilla dans sa bourse et en sortit quelques piécettes qu’il mit dans la main du jeune homme.

« Je t’en donnerai d’autres.

— Mille mercis, mais ce n’est pas question d’argent. Quand je sors à la nuitée, c’est juste pour Mathilde, vraiment. Sans bonne raison, pourquoi cheminer par les sentiers alors que tout le monde dort ? »

Ernaut se figea, écarquillant les yeux, arrondissant la bouche.

« Que t’arrive-t-il ?

— Rien du tout, rien du tout… Grand merci, l’ami Martin. Et n’oublie pas, si tu te souviens de quoi que ce soit… »

Il planta là son compagnon d’un soir et repartit précipitamment en direction de la maison de Lambert. Il venait de comprendre. Sur le Falconus, le chevalier Régnier lui avait sans cesse répété qu’il fallait agir avec méthode et ils avaient dû comprendre comment le meurtre avait pu se faire dans une cabine close. Quelques mois plus tôt, Ernaut avait pisté le malfaiteur qui s’en prenait aux pèlerines, en tâchant de découvrir qui il était. Là, en ce casal, il lui fallait trouver un autre brin de l’écheveau à tirer et ce fil, c’était la raison à l’origine de tout cela. Nul ne serait venu en pleine nuit frapper à la porte d’Ogier sans solide motivation. Il n’aurait pas pris le temps de lui faire subir mille outrages après l’avoir bâillonné et ligoté. Ogier avait un ennemi qui s’était mis en marche, plein de fureur, avec un dessein à accomplir.

Ernaut devait en apprendre plus sur le défunt, trouver là où il avait pu se créer un adversaire, un ennemi qui ne trouverait sa satisfaction que dans le sang versé, la douleur infligée. La réponse au mystère de la mort d’Ogier résidait dans sa vie.

### Casal de la Mahomerie, jeudi 12 juin, matin

Lorsqu’il sortit de la maison au matin, Ernaut était beaucoup moins sombre. Il avait décidé de s’enquérir des femmes qu’Ogier avait pu fréquenter de son vivant. Étant donné la mutilation subie, il se pouvait fort bien que cela soit une punition infligée par un mari, un frère ou un père offensé. Ce qui n’excluait pas Godefroy a priori, mais constituait une piste logique. La première des choses à faire était de se rendre à la maison pour y découvrir les femmes des environs, sur lesquelles la concupiscence aurait pu s’abattre en premier lieu. Il descendait la rue, saluant d’un signe de tête ceux qu’il croisait. Dans la partie haute du village, certains lui rendaient son bonjour, de façon plus ou moins appuyée. On commençait à le reconnaître comme l’un d’eux.

Arrivé devant la maison d’Ogier, il frappa de son poing plusieurs coups violents sur la porte. Au même instant, Osanne et sa mère sortaient de chez elles, une corbeille de linge sous le bras. Ernaut s’approcha et les salua comme il convenait. Osanne lui sourit.

« Le valet est parti vaquer à quelque tâche. Je l’ai vu au matin issir[^issir] hors le courtil.

— Dommage… Je repasserai tantôt en ce cas.

— As-tu vu Lambert ce jour ? A-t-il bien dormi ?

— Je vais m’y rendre, il me faut discuter de cette affaire avec lui. Le temps presse. »

La jeune fille hocha la tête.

« S’il ne m’a pas vue encore, dis-lui que je lui porterai à manger, et passe-lui… toute mon affection. »

Ernaut sourit avec douceur, touché par la délicatesse de l’intention, puis regarda les deux femmes s’éloigner vers la source et le lavoir. Devant la maison voisine de celle d’Ogier, un vieil homme s’employait à tailler des manches dans des perches, une hachette à la main, des fers posés à ses pieds.

« Salut à toi, l’ancien, tu résides ici ? »

L’homme arrêta son geste et toisa le géant un petit moment. Il afficha un sourire où trônaient quelques dents jaunies, s’accordant avec son nez boursouflé et couperosé. Puis il reprit lentement son travail, légèrement tourné vers Ernaut.

« C’est là hostel de mon aîné, Guillaume. Nous venons de Provence.

— Tu travailles avec lui à sa terre ?

— Dieu m’est témoin que j’ai fait ma part, mais j’en demande rien, c’est normal pour un père de s’effacer devant le fils. »

Il ricana, finit par tousser et s’étouffer, la gorge ronflant comme un torrent en crue.

« Et tu connaissais bien ton voisin ? demanda Ernaut, montrant de la main le manse d’Ogier.

— Ce bon à rien ? J’aurais préféré qu’il réside vers ses compaings ! Ou qu’il demeure à Saint-Gilles !

— Il causait soucis ? »

Le vieillard stoppa son geste, comme surpris par l’innocence de la question.

« C’était un souci sur pattes ! Godefroy a bien agi, le casal sera plus calme maintenant que ce démon a rejoint l’diab’.

— Tu penses que c’est le Borgne qui l’a occis ?

— Ce que j’en sais, moi ? On le dit, alors je veux bien le croire. Tu espères que non, vu que ton frère va faire bataille pour lui. Dieu jugera. En tout cas, Ogier restera à bouffer les racines et ça, ça me convient ! »

Il conclut sa dernière phrase d’un hochement de tête satisfait et recommença son écorçage. L’entretien était fini. Ernaut reprit sa route en direction de la curie. Des enfants croisèrent son chemin, occupés à batailler, armés de baliveaux, riant tandis qu’ils s’entretuaient pour de chimériques conquêtes. Devant le grand portail, quelques personnes discutaient, mais le groupe s’égaya avant qu’il n’arrive à sa hauteur. À l’entrée, un valet était assis sur un tabouret, occupé à graisser selles et harnais étalés devant lui. Il leva la tête à l’approche d’Ernaut, les paupières presque closes derrière les mèches folles qui tombaient jusqu’à ses joues.

« Je suis Ernaut, le frère de Lambert, je viens le voir.

— Tu peux entrer à ta guise, il doit être dans la cour, ou bien à se reposer là où on dort, au-dessus des chevaux, dans l’écurie.

— Et l’autre, Aymeric ?

— Il couche aux cuisines. Il s’agirait pas qu’ils s’entrebattent avant samedi, hein ? »

L’homme sourit, appliquant avec soin la pommade sur les cuirs craquelés. La haute tour centrale surplombait le grand bâtiment de l’administration du Saint-Sépulcre où résidait l’intendant, frère Pisan. Lambert était assis sur une des marches, les coudes sur les genoux, le visage las, tourné vers le sol. Son frère l’interpella depuis une bonne distance, se moquant gentiment de son aspect triste. Lambert releva la tête, mais sourit à peine. Ernaut s’efforça de manifester de l’entrain et posa la main sur l’épaule de son frère.

« Osanne te passe le bonjour et m’a dit qu’elle passerait te voir d’ici peu. »

Lambert sourit plus franchement, mais garda le silence.

« D’ailleurs, mon frère, à propos de femme, j’ai grande certitude que la meurtrerie est liée à l’une d’elles.

— Comment ça ? Pourquoi affirmes-tu cela ? s’étonna Lambert, arraché à sa torpeur.

— J’ai mes raisons et elles ne se discutent guère, du moins je n’en ai pas le temps. As-tu ouï parler de commerces charnels à son sujet ?

— Je ne le fréquentais pas et n’écoute guère les commérages, répondit-il, l’air pincé.

— C’est un tort, mon frère, car cela permet de se prémunir contre eux ! tonna Ernaut, agacé. Enfin bref. Selon toi, aucun cornard à cause de lui ?

— Je ne crois pas, souffla Lambert, découragé. »

Ernaut se mordit les lèvres, hésitant à poursuivre sur son idée. Il ne voulait pas heurter son frère, qu’il voyait là déjà bien abattu. Il s’assit à côté de lui, posant un bras protecteur sur ses épaules. Puis il s’approcha encore, parlant à mi-voix.

« Sans offense, crois-tu qu’il aurait pu… manquer de respect à Osanne ? Plus d’un homme envie ta bonne fortune. Il aurait pu…

— Que me souffles-tu là ? lui répliqua Lambert, les yeux enfiévrés. Osanne n’aurait jamais…

— Je n’ai pas dit qu’Osanne aurait fait quoi que ce soit, mais que lui l’aurait fait. Ce serait là puissante raison pour inciter Godefroy à agir, venger l’honneur de sa fille.

— Non, je n’y crois pas un instant. Godefroy ne ferait pas ça… Et puis Osanne n’a jamais été pareillement violentée. Elle me l’aurait dit.

— Tu en es sûr ?

— Je joue ma vie là-dessus, Ernaut, est-ce là gage suffisant pour toi ? » s’emporta Lambert.

Le jeune homme hocha la tête, convaincu. Il soupira, cherchant une piste dans les méandres de son esprit.

« Il devait bien encontrer des femmes et il aurait pu se mal comporter avec une d’elles… Qui lui lave son linge ?

— Aucune idée, sûrement la femme ou la fille d’Aymeric, vu qu’ils vont être de la même parentèle.

— Et il n’aurait pas voulu prendre petit avant-goût de ses épousailles ? »

Lambert sourit sans joie.

« Tu crois qu’il y aurait là offense à réparer l’arme à la main ? Si c’était le cas, on baignerait dans le sang ! »

Ernaut se fendit d’un petit rire discret.

« Je te le concède. Outre, ce serait stupide de la part d’Aymeric de se porter témoin pour Ogier en ce cas. Il aurait plutôt tenté de se faire oublier. »

Lambert entreprit de ramasser des petits cailloux qu’il lançait dans la poussière, d’un geste sans vigueur. Ernaut réfléchissait, puis se leva brutalement.

« Je vais aller voir les femmes au lavoir, il serait bien étonnant qu’elles ne cancanent point ! Osanne et sa mère y sont, elles pourront m’assister.

— Certaines bouches demeureront closes, Godefroy n’est pas partout aimé.

— Ogier non plus et la détestation incite parfois à se répandre en confidence. Je n’ai guère que deux jours pour traquer cette bête, Lambert, et je lui ferai rendre gorge. N’est pas goupil qui veut ! »

Lambert se leva à son tour, époussetant sa cotte. Il fixa son frère, tentant d’arborer un visage serein.

« C’est entre les mains de Dieu.

— Que tu dis ! Je n’ai guère vu l’index du Tout-Puissant pointer les voleurs et les menteurs, quand bien même j’œuvre en la sainte cité !

— Attention frère, tu blasphèmes.

— Je ne crois pas. La volonté de Dieu ne vaut que par les hommes qu’elle met en branle. Attendre et espérer est façon de fol ! »

Lambert se tut, ne sachant que répondre. Son visage exprimait une stupeur inhabituelle.

Ernaut eut une bouffée d’affection et il attira son frère dans ses bras.

« Nul ne touchera aux miens, Lambert. Si je dois aller secouer Dieu ou diable, je n’en ai nulle crainte ! »

### Casal de la Mahomerie, jeudi 12 juin, fin de matinée

Lorsqu’il déboucha du casal, Ernaut aperçut une imposante caravane de mules et d’ânes qui montait la route voisine, en direction de Naplouse. Les balles de marchandises fixées sur les flancs étaient grises de poussière, les muletiers couverts de poudre des pieds à la tête et le nuage que tous soulevaient retombait sur les alentours tel un linceul de cendre. Seules les silhouettes de deux cavaliers émergeaient de cette fumée grise, dont ils avaient bu la couleur.

Alentour, les murets de pierre sèche, les affleurements rocheux traçaient des marches d’escalier que seul un Goliath aurait pu gravir. Un rapace planait dans le ciel pervenche. Sous les oliviers, des hommes sarclaient, traquaient la moindre herbe qui aurait pu voler la précieuse eau. Le vent étirait jusqu’aux oreilles d’Ernaut le chant mélodieux des femmes en train de battre le linge.

Elles frappaient en rythme, accomplissant leur rude labeur à cette cadence par toutes chantée. Ernaut se surprit à avancer au pas, entraîné comme par un refrain de marche.

Une voix le héla depuis les abords des maisons. C’était Iohannes, qui sortait du hameau accroché au sud de la muraille de la curie, modestes demeures des valets, des manouvriers, des journaliers sans le sou. Il s’avançait, la main levée.

« J’allais m’enquérir de toi, tu passes fort à propos !

— Je me rends au bassin au linge, voir si aucune femme ne saurait m’en apprendre sur Ogier.

— Je marcherai dans tes pas comme promis, si tu le veux. »

Ernaut marqua son accord d’un signe de tête, invitant de la main à reprendre la descente. Il se frotta la nuque, déjà brûlante, irritée par le souffle de braise s’abattant du ciel. En s’approchant, il prit conscience qu’il existait deux groupes autour du lavoir, qui tenaient chacun un des côtés. Quelques aventurières se tenaient entre les deux, mais il était visible que chaque quartier, chaque femme, avait sa place.

Des visages rougis par l’effort se tournèrent vers eux, certains amicaux, d’autres renfrognés, juste curieux pour la plupart. Seule Osanne, qui était occupée à tordre le linge avec une compagne, laissa son labeur pour venir leur parler. Elle salua Iohannes d’un signe courtois et fit un sourire poli à l’intention d’Ernaut.

« J’aurais usage que tu me présentes à tes commères, Osanne. J’ai besoin de savoir ce qu’elles pensaient d’Ogier.

— Cela pourrait aider père et Lambert ?

— Je le crois.

— Alors viens-t’en ! »

Elle interpella un des groupes, introduisant Ernaut comme le frère de son futur époux, c’est-à-dire comme sa famille. Il avait des questions à poser, habitué qu’il était à pister les malfaisants, en tant que sergent du roi. Ernaut grimaça à cette mention, mais n’osa la reprendre.Plusieurs femmes lancèrent des plaisanteries sur son physique, et deux ou trois se levèrent, essuyant leurs mains écarlates sur leur robe. L’une d’elle osa le défier du regard, une lueur espiègle dans son visage ingrat, marqué par les ans et les travaux épuisants.

« Que veux-tu, l’homme ? Besoin de faire assouplir tes nippes ? »

Ernaut jeta un œil sur ses vêtements gris de poussière et d’usure.

« Je ne viens mie curer mes linges, mais pour savoir si Ogier était connu de vous. »

Les mines se renfrognèrent et une jeune femme se signa en silence. On ne parlait pas des défunts, et surtout pas de ceux qui étaient morts violemment.

« Mon frère est décidé à affronter Aymeric par bataille et je ne crois pas qu’il soit nécessaire qu’un autre meure. Je veux traquer le murdrier et le faire pendre avant samedi.

— Je te comprends, l’homme, mais qu’en savons-nous. Il n’avait ni fille ni épouse !

— Il était bien trop laid pour ça ! » s’esclaffa une silhouette maigrichonne toute en nez.

Quelques-unes se mirent à rire, mais certaines firent la moue, choquée par l’inconvenance de la remarque. Si on devait absolument parler des morts, c’était toujours avec respect. Une des femmes battant le linge releva le buste, indignée.

« Il n’avait pas visage de jouvencel, mais c’était un des nôtres. Il faisait bon ouvrage, payait bien ses valets et participait aux corvées comme un homme. Allons-nous le blâmer de n’avoir pas visage lisse de damoiseau ?

— Moi je le trouvais pas si laid… , ajouta une autre. C’était pas un de ces godelureaux de la cité. Il avait belle prestance, dans sa cotte, à la messe.

— Tu lui aurais bien fait son affaire, hein, la mère? » se réjouit sa voisine, la voix déformée par la gouaille, attirant des protestations véhémentes et des rires sans malice.

Une matrone à la forte poitrine, les mains larges, profondément ridées d’avoir trop travaillé vint auprès d’Ernaut, sa corbeille coincée contre la hanche. Elle la fit glisser à ses pieds, toisant le géant de bas en haut ainsi qu’elle l’aurait fait d’une pièce de viande qu’elle s’apprêtait à découper.

« Je n’ai jamais goûté de le voir rôder dans les parages. Il avait regard de loup et désir de lièvre.

— Il manquait de respect ?

— Oh, il était bien fort malin, trop pour risquer remontrances en public, mais je connais les hommes. Celui-là avait le démon en ses braies, j’en ai certeté. »

Une de ses voisines se leva à son tour, déroulant ses draps qu’elle pliait grossièrement. Elle hochait la tête, faisant voleter quelques mèches grises devant son visage fatigué malgré le peu d’années qu’elle avait connues.

« Je l’ai surpris plus d’une fois à mirer les jeunes filles par ici, et mon époux dit qu’il payait des femmes pour son plaisir.

— Ici ? demanda Ernaut, interloqué. »

La première matrone le foudroya du regard.

« Pas de femmes ainsi en notre casal ! Il devait aller à la cité. Le vice se cache toujours au près de la vertu. »

Une jeune fille, à peine une femme, encouragée par les confidences entendues, ajouta sa voix au concert de reproches.

« Je l’ai bien vu maintes fois qui lançait regard d’envie vers mes commères et moi tandis que nous allions par le casal.

— Ça, ma fille, j’ai toujours dit qu’il ne fallait pas se montrer ainsi aux hommes, surtout comme lui. Puis elle ajouta, à l’intention d’Ernaut : lors du battage, ou pendant les moissons, certaines se mettent en chemise, parfois bras nus. Alors forcément, ça leur monte à la tête, aux corniauds.

— Je n’ai jamais… rétorqua la jeune fille, vexée.

— Je ne parle pas de toi, mais de certaines qui espèrent caresser l’œil des hommes…

— Quand ce n’est pas autre chose ! » lança une vieille édentée assise sur une pierre, le visage brûlé par le soleil, occupée à vérifier l’état des coutures d’une cotte.

Sa remarque déclencha une envolée de rires gênés et de souffles indignés. Mais cela ne perturba guère la confidente d’Ernaut, qui reprit, un sourire fendant son visage.

« Moi, je comprends Aymeric, mais je serais son épouse, j’aurais eu grande peine à donner mon enfant à cet homme.

— Il avait pourtant bel avoir ce me semble.

— Peut-être, mais cela Dieu le donne et le reprend. Il avait surtout méchant caractère et j’aimais pas ses yeux quand il voyait pucelle. Mon homme l’aurait battu à sang s’il avait regardé ainsi une de mes filles. Et j’y aurais bien ajouté ma tournée, pour faire bonne mesure. C’t homme, il avait un démon caché en ses braies, je le dis et compte dessus. »

Ernaut se tourna vers Iohannes, qui était resté légèrement en retrait, l’air attentif, mais sans expression sur son visage. Il n’avait ni le caractère enthousiaste de son ami, le sergent Eudes Larchier, ni l’esprit acéré du chevalier d’Eaucourt. Ernaut était le banneret en cette affaire. S’il avait réussi à rallier des troupes, il lui revenait de montrer par où le conroi[^conroi] devait s’avancer. La matrone se penchait pour reprendre son panier quand Ernaut l’arrêta.

« Il n’y a jamais eu d’histoire justement ? Un père contrarié, un époux cornard ?

— Rien de ça ici, mon gars. Mais c’était peut-être par manque de temps.

— Il n’était pas à la Mahomerie depuis long temps ?

— Depuis guère après la Noël, il aura pas eu temps de vendanger ses ceps. »

Ernaut prit Iohannes à part, remerciant la femme d’un sourire. Il s’exprima à mi-voix.

« Sais-tu d’où il venait, Ogier ?

— Oui, j’ai eu confirmation de cela au matin. Il était du casal de Saint-Gilles, au nord d’ici, à une dizaine de lieues.

— Il aurait pu s’en venir ici pour fuir quelque vengeance ?

— Je ne sais, en tout cas il avait du bien en arrivant m’a confirmé l’intendant.

— Et la femme a-t-elle dit vrai, à propos des courtisanes ? Aucune en le casal ? »

Iohannes garda le silence, examinant Ernaut tout en réfléchissant à sa réponse.

« Il n’y a pas de bains ici, tu l’as vu. Je ne dis pas que parfois, une veuve ou certaines femmes négligées ne…

— Certes, mais je ne parle pas de cela, qui ne font que l’occasion. Je parle d’un lieu où le bachelier, le soldat, ou le voyageur peut encontrer douce amie, contre quelques deniers.

— Rien de ça ici. Tout le monde sait qu’il faut aller à la ville. »

En répondant, Ernaut aurait juré que le hâle de Iohannes s’était foncé, comme s’il rougissait. Il ne s’était pas posé de question jusque-là sur l’homme qui se tenait face à lui. Pas loin de la quarantaine, il devait être marié, et s’il ne l’était pas, c’était qu’il manquait de bien. Les bains étaient un endroit où les hommes comme lui pouvaient calmer leurs ardeurs, refroidir leurs fièvres.

Ernaut n’y voyait guère de mal, comme la plupart de ceux qui fréquentaient ces lieux. Sans de telles femmes, prêtes à s’abandonner au premier venu, les passions inciteraient à aller voir la voisine, l’épouse du compagnon, ce qui ne serait pas sans péril pour la société. Alors que là, chacun trouvait son équilibre à sa façon. Les hommes payaient des femmes consentantes pour se repaître de leurs chairs, et leurs épouses faisaient mine de regarder ailleurs, soulagées de n’être plus seules à subir les assauts lubriques de leurs compagnons.

### Casal de la Mahomerie, jeudi 12 juin, midi

Lorsqu’ils s’éloignèrent du lavoir, Ernaut et Iohannes perçurent quelques rires échappés de l’autre groupe de femmes. Le passage d’hommes en cet endroit attirait souvent des remarques railleuses de la part de celles qui n’étaient pas concernées par cette visite. Ils ne s’en émurent pas et avançaient en discutant de ce qu’il convenait de faire à présent. Ernaut se demandait pourquoi Aymeric avait si vite accepté Ogier comme l’un des siens, prêt à défendre sa mémoire par bataille. Iohannes lui proposa donc d’aller voir celui qui connaissait le mieux Aymeric, Gautier le Petit.

« Tu crois qu’il acceptera de nous voir ? Il n’était guère amical la dernière fois que je l’ai croisé.

— Ce n’est pas mauvais homme. Aucun ici ne l’est vraiment, d’ailleurs, de ce que j’en sais. »

Un rictus cynique déforma les traits d’Ernaut.

« Au moins un me semble bien vil…

— Tu crois que le mal explique toujours le meurtre, toi ? »

La remarque avait été dite sans violence, doucement, d’une voix régulière, mais elle fit l’effet d’un coup de tonnerre. Ernaut se figea, dévisagea l’homme à ses côtés, se demandant s’il l’avait bien jugé jusque-là. Se pouvait-il que… Il chassa cette idée de sa tête, mais garda le silence et les sourcils froncés jusqu’à ce qu’ils s’arrêtent devant un des manses. Iohannes frappa à plusieurs reprises sur la porte entrouverte. S’annonçant d’une voix forte, un petit homme en sortit, les yeux papillonnants devant la lumière, s’efforçant de reconnaître ses visiteurs. Il se renfrogna immédiatement, lâchant presque à regret un bonjour qui ne s’adressait visiblement qu’à Iohannes. Celui-ci ne se démonta pas et sourit.

« Le bon jour à toi, Gautier. Nous aurions besoin de te parler, si tu le veux bien.

— J’ai rien à dire à çui-là ! répliqua Gautier d’un ton peu amène, indiquant Ernaut du menton.

— Nous ne voulons créer nuisance, Gautier, au contraire. Le jugement par bataille nous semble fort périlleux, et nous apensons qu’il faudrait attraper le coupable avec certeté. N’as-tu pas quelques craintes pour Aymeric ?

— Dieu guidera son bras !

— Et si ce n’était pas Godefroy ? Y as-tu pensé ? Peut-être qu’un diable est celé en un cœur ici et qu’il pourrait frapper de nouvel. »

Gautier fit une grimace, se frottant le nez de sa manche. Puis il les invita à entrer d’un geste brusque, s’effaçant dans son cellier. La pièce était fraîche, remplie de paniers, de tonneaux, de sacs et de jattes. Il était apparemment en train de fabriquer des balais, des branches gisant en vrac autour d’un tabouret. Il sortit deux autres escabeaux et s’assit, toujours sans ouvrir la bouche. Il jouait machinalement avec la serpe qui lui servait à tailler les brins, attendant. Iohannes approcha son siège et parla d’une voix douce.

« Tu es certainement un de ceux qui connaissaient mieux Ogier. Peux-tu nous en dire plus sur lui ?

— Sur quoi ?

— Je ne sais pas. Connaissait-il Aymeric avant de s’en venir chez nous ? Proposer sa fille en épousailles, ce n’est pas rien.

— Non, on l’a rencontré le même jour, vu que nos terres se touchent. Il est d’un casal au nord, Saint-Gilles. Il avait du bien, des idées. C’est pour ça qu’y voulaient unir leurs parentèles, tous les deux.

— Ils avaient des projets ?

— Dame oui ! Un moulin à olives ! Ogier avait des bêtes pour le faire tourner. Ils auraient pu ainsi presser leur huile, et même louer le tout. »

Iohannes hocha la tête, appréciateur. Posséder un tel équipement permettait en effet d’améliorer le rendement de sa terre. Non seulement on était indépendant, mais on pouvait récupérer de l’huile sur le pressage pour les voisins. Les deux hommes auraient pu grandement améliorer leur condition. Contrairement aux meules à grains, il était permis, et même encouragé, de posséder de telles installations. Il fallait écraser et presser une telle quantité d’olives lors de la cueillette que parfois on était obligé de chercher dans les casaux aux alentours.

Ernaut ne perçut pas toutes ses implications, trop récemment arrivé et habitant en ville, aussi coupa-t-il Iohannes pour en venir à ce qui l’intriguait.

« D’aucuns auraient pu vouloir empêcher pareil projet ?

— Bien sûr ! Le borgne ! s’exclama Gautier en postillonnant.

— Pourquoi donc ?

— Il était toujours contre Ogier ! La moindre occasion de lui nuire était comme pain blanc à ses lèvres. Sûr qu’il était jaloux.

— Jaloux d’Ogier ? s’étonna Ernaut, les sourcils soulevés.

— Dame ! En voilà un qui s’en vient ici, arrivé après lui, mais du coin, qui s’abouche avec Aymeric, prud’homme s’il en est. Le vieux a pas aimé. »

Ernaut ne voyait pas en quoi l’arrivée d’un nouveau colon aurait pu perturber autant que cela Godefroy. De ce qu’il en avait vu, d’après l’image que lui en avait dépeinte Lambert, c’était plutôt un homme calme, travailleur et discret. Mais Gautier continuait sur sa lancée, aiguillonné par son ressentiment.

« Comprenez, il faisait son coq parmi nous autres. Il était là depuis quelques années et s’apensait roi du coin. Il tirait fierté à causer avec les Syriens, comme si c’était là grande richesse ! »

L’homme s’interrompit et fit un geste d’excuse à l’intention de Iohannes.

« Ceci dit sans offense, hein. Lui en tirait gloire alors que bon… Et bein Ogier il nous a bien plu, tout de suite. Adoncques le vieux a eu peur qu’on le voie plus comme chef ici. Jalousie que j’vous dis. »

Ernaut s’accouda sur ses genoux, la tête sur les mains croisées. Il était dubitatif.

« Tu crois que le Borgne a tué Ogier pour ça ? Pour une meule et quelque rancune qu’il lui gardait ? »

Gautier fit claquer sa mâchoire, plissant les paupières, inclinant la tête comme un loup prêt à mordre.

« Comment donc ! C’est pas rien d’être parmi les anciens, d’être écouté quand on parle. Aymeric en est, et plus d’un ont tenté de lui acheter sa fille, sans rien obtenir.

— Mais Godefroy n’avait pas de fils à marier de toute façon.

— Certes pas. C’est pour ça qu’il voyait d’un mauvais œil le mariage d’Ogier. Il avait aucun moyen d’offrir mieux. »

Iohannes toussa, attirant l’attention sur lui, espérant ramener un peu de calme. Les trois hommes gardèrent le silence un petit moment, ponctué par les raclements de gorges, les reniflements. Ernaut se leva doucement, bientôt imité par les autres. Il s’approcha imperceptiblement de Gautier, qu’il écrasait de sa masse, le surplombant tel un géant. L’homme ne s’en laissa pas compter et affronta le regard d’un air rogue. Ernaut ouvrit la bouche pour parler, une lueur amusée scintillant un instant dans les yeux.

« Sais-tu seulement comment on a meurtri le vieil Ogier ?

— Euh non, répondit Gautier, à demi balbutiant, surpris par la question. D’une lame, je crois.

— Il y a de ça, mais ce n’est pas ce qui l’a tué, je peux te le garantir.

— C’est quoi donc alors ?

— On l’a escouillé, ainsi qu’on le fait aux taureaux pour en faire des bœufs. »

La stupeur décomposa le visage de Gautier, dont la mâchoire s’affaissa.

« Quoi ?

— Tu m’a bien entendu. Et le châtreur n’a nullement ligaturé les bourses comme il sied. Il a tout coupé, couillons et verge, arrachant plus que découpant. »

Le visage révulsé, Gautier bafouillait. La surprise était de taille pour lui.

« J’ai déjà vu quelques meurtreries, bagarres d’ivrognes, vengeance de familiers… Mais je ne vois pas là travail d’un jaloux. C’est l’œuvre d’un diable, un démon qui en voulait à Ogier, à Ogier et à ses couillons. Rien à voir avec une meule ou des projets d’entasser cliquailles. Celui qui a tenu la lame, c’est loup à visage d’homme. Tu crois qu’il pourrait être le Borgne ? Pas moi. Pour mon frère, et pour ton compère Aymeric, pour la sécurité de tous ici, je suis sur sa piste. Tu peux en être, si tu en as désir. »

Indiquant à Iohannes la sortie d’un signe de tête, il planta là Gautier, les mains sur les hanches, perplexe. Le petit homme regarda les deux visiteurs sortir de chez lui en silence, mordant sa lèvre inférieure avec inquiétude. Puis il renifla et s’élança dans l’escalier qui menait à l’étage.

La chaleur s’abattit comme du plomb fondu sur leurs épaules lorsque Iohannes et Ernaut se retrouvèrent dans la rue. Aucune ombre ne s’offrait à eux pour échapper à la toute-puissance du soleil. Leurs nuques se couvrirent en un instant de sueur, leur front de moiteur. Essuyant l’eau de sa manche, Ernaut invita d’un geste Iohannes à se rendre chez Lambert. Le traducteur attendit qu’ils furent éloignés de la demeure de Gautier pour prendre la parole.

« Comment l’as-tu appris ? Je n’en ai eu aucun écho.

— Le clerc qui l’a nettoyé n’avait nul désir que cela se sache. Quand je veux savoir aucune chose, je ne fais confiance qu’à mes deux yeux. »

Iohannes sourit, amusé par la conviction et la détermination de son compagnon.

« Tu penses vraiment que cela peut mener au murdrier ou c’était juste pour effrayer Gautier ?

— Un peu des deux, admit Ernaut. On ne taille pas ainsi un homme sans raison. Alors son histoire de meule qui contrarierait le vieux Godefroy…

— Il ne mentait pas, Ernaut. Le bien fait l’homme en ce casal.

— Mon père taille la vigne comme son père avant lui. Je sais bien la valeur de la terre, même si je suis de la ville maintenant. Pourtant si on peut s’esbigner pour elle, se pourfendre à l’occasion, je n’ai jamais entendu qu’on escouillait pour elle. »

Iohannes se gratta la tête.

« Je suis assez d’accord avec toi. Il y aurait eu des soldats ou des Turcs dans le coin, j’aurais pu croire à quelque forfait de guerre. Mais là, au sein du casal, cela ne s’est pas fait sans raison. »

Ernaut sentit la réticence et lui demanda ce qui le gênait.

« Je ne comprends pas pourquoi tu laisses assavoir la piste que tu remontes. Il me semble plus prudent d’avancer en toute discrétion, sans dévoiler ses intentions, afin de surprendre.

— Cela peut se faire, tu parles de juste. Il existe plusieurs façons de chasser, à la traque, au filet, au vol… »

Iohannes acquiesça, tandis qu’Ernaut poursuivait.

« Comprends, on n’a guère de temps. Je n’ai qu’un fil qui me ramène au coupable, trop fin à mon goût. Alors je me suis dit que faire assavoir à tous que je sais le bon brin peut amener le félon à se trahir.

— Comment cela ?

— Tu n’as jamais tapé en une fourmilière ? Il faut porter un bon coup si on veut les voir s’agiter. J’espère que d’ici peu tout le casal ne parlera que de ça, des couillons d’Ogier. Je touille en bien vilain purin pour y trouver le joyau, alors il me faut le remuer jusqu’à écœurement. Sans cela, aucun espoir de jamais voir le moindre éclat briller au soleil avant samedi. »

### Casal de la Mahomerie, jeudi 12 juin, après-midi

Ernaut et Iohannes étaient installés dans la grande salle, s’efforçant d’échapper à la chaleur qui s’écoulait parmi la moindre ouverture, inondant le plus sombre des endroits. Ils en étaient à leur second pichet, discutant des hommes du casal et de leurs femmes lorsqu’une voix retentit en bas, depuis le cellier. Bientôt, un visage apparut dans la volée d’escaliers. La tête ronde, les joues comme couvertes de grain, l’homme avait un sourire avenant. Ses yeux bruns quasi fermés étaient noyés sous ses paupières et ses sourcils denses. La chevelure, plus frisée que la toison d’un mouton, lui faisait une touffe buissonneuse sur le haut du crâne tandis que sa nuque et son cou étaient glabres. Le tout était posé sur un tronc massif, aux membres fins.

L’homme souriait de toute sa dentition approximative. Iohannes lui rendit son salut et se leva pour l’accueillir, mais l’autre ne lui laissa pas le temps de parler.

« Salut ! J’ai nom Bernard, ici tout le monde m’appelle Bourgogne ! »

Ernaut sourit à l’évocation de son pays natal et vint à son tour à la rencontre du nouvel arrivant.

« Bien aise de voir un homme de chez nous !

— Et moi donc, on est même pas une poignée au casal ! »

Ils s’installèrent rapidement autour de la table. Ernaut remplit de nouveau la cruche de vin clair, puis les gobelets. Bernard transpirait abondamment, sa cotte grise pleine de poussière du labeur était maculée de gouttes tombées de sa chevelure. Il avala le vin en une grande lampée, l’air appréciateur.

« J’étais venu saluer ton frère quand il a pris ce manse. Même si on est tous d’ici, ça fait chaud au cœur de voir des gens du pays. Il m’a conté vos histoires, et son désir de faire bon vin, comme chez toi. »

Ernaut sourit, comprenant que l’homme était suffisamment bavard pour deux, qu’il en viendrait par lui-même à ce qui l’amenait une fois les potins échangés. Il apprit donc toutes les péripéties qui avaient amené ce fils de tourneur sur bois à prendre la route puis à s’installer dans les monts de Judée. L’énumération des membres de sa famille lui paraissait beaucoup plus floue, étant donné que Bernard semblait décidé à battre les records de fécondité de tous les lapins d’Outremer et de Bourgogne réunis. Il n’était pas peu fier d’énumérer les prénoms de ses sept enfants encore en vie, tous vigoureux, travailleurs et respectueux, du moins selon ses paroles.

Tandis qu’il en dressait un portrait visant à vanter autant ses qualités paternelles que les compétences filiales, sa voix se fit moins braillante et il se rapprocha d’Ernaut, assis face à lui. Sans même y penser, il employait de nouveau un dialecte bourguignon, qu’Ernaut n’avait pas entendu depuis des mois, voire des années.

« C’est d’ailleurs à cause du Thibault que je viens te déranger, rapport à l’histoire que tu sais, avec ton frère, le Grand et le Borgne. »

Ernaut s’appuya sur ses coudes et se rapprocha à son tour, tout ouïe.

« C’est un bon gamin, mon Thithi. Il est pas grand, mais vaillant comme Roland. Pis pas fainéant. Il aide bien, ça oui ! Il… »

Bernard jeta un petit coup d’œil vers Iohannes, comme si sa présence le gênait, ou qu’il tentait de l’inclure dans les confidences qu’il allait faire.

« Il va avec un des chiens, par la campagne, pour me ramener ce qu’il me faut, repérer des bons coins.

— Tu œuvres à quoi ?

— Ah, pardon, je croyais l’avoir dit. J’ai la main sûre à empiler mes pierres pour murets et cabanons. Des pierres bien taillées, ça aide ! »

Il fit une moue, traçant du doigt sur la table une carte imaginaire puis reprit : « Il en trouve dans le coin, ici ou là, tu vois. Des vieux trucs des Romains, ou p’têt bien de l’époque de Jésus, va savoir. Enfin bon, c’est pas le propos. Le truc, c’est qu’il est tombé avec ses copains sur un camp récent.

— Il y a bien des voyageurs ou des nomades qui passent par là, je ne vois rien …

— Je parle pas de tentes de bédouins ou d’une halte de marcheurs de Dieu. Mais d’un petit truc discret, genre secret si tu vois ce que je veux dire. »

Bernard releva le buste, se passant la langue sur les dents, convaincu de la justesse de son idée, et de son importance. Iohannes croisa le regard d’Ernaut avant de parler.

« Tu veux dire qu’il a découvert un coin où aurait pu s’installer…

— Le murdrier d’Ogier. Exactement ! »

Bernard siffla entre deux dents et aussitôt arriva moitié sautant, moitié bondissant un gamin au poil hirsute comme son père. Il n’était vêtu que d’une chemise serrée à la taille par une corde, d’ou pendait un petit canif. Il tenait à la main un chapeau de paille troué. De la sueur coulait régulièrement de son front, qu’il essuyait de la main ou de la manche, avec indifférence.

Lorsqu’il parlait, il semblait gêné par ses incisives, qui tentaient désespérément de sortir de sa bouche, faisant la course avec son nez pointu. Il se mit à sautiller sur place, passant d’un pied sur l’autre, balançant ses bras comme s’ils ne lui appartenaient pas et ne savait qu’en faire. Son père lui passa une main affectueuse sur l’épaule.

« Vas-y, mon Thithi, dis-leur ce que t’as vu dans la vieille tour romaine.

— Bein, y’avait un forain qui y est resté un temps, il avait tout chamboulé mes pierres. »

Bernard sourit, comme s’excusant de cette appropriation. Il encouragea son fils à continuer.

« C’est pas tout, hein ?

— Nan, il y avait aussi le camp dans les grottes.

— Quelles grottes ?

— Les romaines, au couchant du casal. On y va de temps en temps pour… »

Bernard fronça les sourcils et lui coupa la parole.

« Ils aiment jouer là-bas, les gamins. Tu sais bien, Iohannes, les vieux tombeaux creusés dans la roche.

— Je vois, en effet, mais elles ne sont pas sur les terres du casal… »

Le regard dépité de Bernard lui fit comprendre immédiatement que son interlocuteur le savait bien et semblait ennuyé par cela. Ernaut perçut l’échange de regards et intervint.

« Pas de souci, Bourgogne, ce ne sera pas de mes lèvres qu’on médira de toi. Tu viens en compaing prêter assistance, je ne vais pas parler dans ton dos. »

L’homme parut soulagé.

« Merci. C’est les terres de Salomé là-bas, le village des païens musulmans, pis le seigneur de Salomé est pas homme facile. Il aimerait guère savoir qu’on prend des pierres dans ses grottes. Même s’il n’en fait rien. Mais c’est là bonne cachette pour qui veut faire l’espie sur le casal. En grimpant à la vieille tour, on voit tout. Le manse d’Ogier, il est de ce côté, il a son courtil qui donne au couchant. Si tu vois ce que je veux dire. »

Ernaut resservit Bernard en vin, souriant en réponse. Il étudia le visage de Iohannes, dans l’espoir d’y lire une réaction. Mais il demeurait impénétrable, fixant Bourgogne d’un air pénétré.

« Si y’a besoin, le gamin pourra te montrer les lieux, mais…

— Il serait plus sage de demander permission au seigneur de Retest avant tout, le coupa Iohannes.

— Oui, j’allais le dire. Il est pas homme généreux de son bien, et sa terre, il y tient. Ça a été grande peine de voir avec lui pour les pâtures des bêtes.

— On demandera avant d’aller fouler sa terre, n’aie crainte. Et on ne parlera pas de toi ni des quelques caillasses dont il se moque.

— Ne crois pas ça. Il est avare comme un Juif. Pis pas commode. Mais je pense pas lui faire grand tort de ramasser quelques pierres, de sûr. »

Il reposa son gobelet avec cérémonie, souriant un long moment, perdu dans ses pensées. Pendant ce temps, le gamin laissait errer ses yeux d’un homme à l’autre, mal à l’aise, impatient de pouvoir quitter l’endroit. Son père se leva brusquement.

« Enfin, comme je le dis à la mère, y’a des fois où faut dire les choses. Si c’est un brigand qu’a fait le coup, pas de raison pour ton frère de risquer sa peau. Entre Bourguignons, on doit se tenir !

— Merci de ton aide, compaing, j’apprécie fort. Sois acertainé que je ne parlerai en rien des balades du gamin, ça ne regarde personne. »

Bernard sourit, rassuré par l’affirmation, puis salua d’un geste avant de descendre l’escalier, guidant le gamin devant lui d’une main autoritaire. Lorsque le silence se fit, après avoir entendu le grincement de la porte, Ernaut leva les yeux vers Iohannes. Celui-ci inspira bruyamment, réfléchit quelques instants à la question muette puis prit la parole à mi-voix.

« Ce n’est pas homme de ragots, à ce que j’en sais. Venir ici avouer qu’il vole des pierres à Retest montre qu’il a désir d’aider.

— Il est si terrible que ça, le seigneur de Salomé ?

— Ni pire ni meilleur qu’un autre. Il n’aime guère qu’on le vole et il se murmure plein de choses sur ce qu’il fait à ses serfs.

— Ça, c’est autre chose…

— Tu crois ? Quand on s’habitue à traiter ses vilains comme des bêtes, on peut être tenté de faire subir même sort aux hommes libres. »

Sa voix s’éteignit tandis qu’il parlait et il demeura là, à fixer Ernaut en silence. Ce fut le jeune homme qui prit la parole, sortant le premier de ses réflexions.

« Ce ne pourrait être simple voyageur ?

— Tu y crois ? Pourquoi se cacher en vieilles ruines alors que l’hôpital accueille qui veut en notre casal ?

— Certains préfèrent demeurer parmi les collines.

— C’est un sergent qui me dit ça ? Je ne vois pas qui préférerait dormir sur la terre et les cailloux quand on propose une paillasse confortable. Sauf ceux qui préfèrent se cacher du roi et des barons. »

Ernaut toussa, voyant parfaitement là où Iohannes voulait en venir.

« Ce qui me gêne, c’est que ça me paraît un peu trop facile : des larrons cachés dans les environs viennent dépouiller Ogier.

— Et quoi ? Ça arrive !

— Certes. Mais je trouve ça…

— Trop arrangeant ? »

Ernaut hocha la tête.

« D’un coup, toute cette histoire n’aurait rien à voir avec le casal, ni ses femmes ? Les brigands n’escouillent pas celui qu’ils larronnent.

— Peut-être pour lui faire avouer où il celait son magot.

— Alors de cela deux choses : un, ils savaient qu’il avait cliquailles et donc quelqu’un d’ici leur a conté, et deux, c’est fort stupide, car ils l’ont tué.

— Les larrons ne sont pas toujours bien fins.

— Je te l’accorde, s’esclaffa Ernaut. Ils figurent parmi les plus stupides créatures sur terre, mis à part les poulets et les ours. Niais et dangereux. »

Ernaut se leva et commença à faire les cent pas dans la pièce tandis qu’il analysait la situation.

« De toute façon, Bourgogne me paraît homme de bien. Il nous a fait confidence qui lui coûte alors je le crois sincère.

— Tu veux aller faire visite aux grottes ?

— Il le faut bien, ne serait-ce que pour m’assurer que ce n’est que mauvais sentier. »

Iohannes parut gêné.

« Alors nous devrons demander permission au seigneur de Retest. Si jamais on veut pouvoir en parler devant les jurés, il ne faut pas se mettre en délicate posture.

— Je vais en toucher deux mots à mon frère, il me dira ce qu’il apense de tout cela, il est souventes fois de bon conseil.

— Il nous faudra perdre du temps pour obtenir agrément du seigneur de Salomé. Cela veut donc dire que nous ne pourrons étudier d’autres voies. »

Ernaut soupira, tournant le dos à Iohannes. Il s’appuya sur l’encadrement de la fenêtre, la tête penchée. Il resta là, sans bouger, la respiration comme bloquée. Puis se retourna brusquement.

« Nous n’avons guère espoir de toute façon. Si on fait bonne récolte en les grottes, peut-être pourrons-nous convaincre Aymeric.

— En effet, cela pourrait suffire s’il sait que des larrons s’y sont cachés un temps. Nous avons un jour. »

