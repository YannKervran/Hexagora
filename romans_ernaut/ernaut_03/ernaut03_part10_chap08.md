## Chapitre 8

### Casal de la Mahomerie, jeudi 19 juin, matin

La compagnie chevauchait bon train, menée par le vicomte sur un superbe étalon alezan. Avec les sergents et les jurés, cela faisait une bonne douzaine d’hommes en selle, dont le passage bruyant et poussiéreux attirait le regard. Nul ne parlait, ou seulement le temps d’indiquer le bon embranchement, le chemin à prendre.

Ernaut avait pu échanger quelques mots avec Arnulf avant le départ. Celui-ci lui demanda s’il avait trouvé de justes motifs pour empêcher le combat. Le jeune homme avait dû le détromper, le regard affligé. Il lui avait expliqué que l’histoire était bien plus complexe que ce que l’accusateur disait, mais qu’il manquait de témoins. Il n’avait pas eu le cœur de parler des mensonges d’Ogier à propos du faux bornage, ou de son activité de passeur. À quoi bon avancer de telles choses qui ne changeraient rien à la situation et le feraient passer pour un médisant, à souiller la mémoire d’un défunt. Le vicomte s’était contenté de hocher la tête d’un air grave, sans commenter.

Une fois en selle, Ernaut n’avait plus ouvert la bouche, repensant à tout ce qu’il savait et tentant d’y démêler le vrai du faux, et ce qui permettrait d’empêcher le combat. Il espérait en son for intérieur que Iohannes aurait décidé Umbert à témoigner, pour réfuter les accusations d’Aymeric ou au moins l’obliger à se dédire.

Tout au long du trajet il se raccrocha à ce maigre espoir. Ils arrivèrent assez tôt à la Mahomerie, se rendirent directement à la curie, où l’intendant les accueillit au milieu des aboiements de chiens, des hennissements des montures. L’ambiance était électrique. L’odeur du sang à venir, l’attente de la violence qui allait se déchaîner, emplissaient les esprits, aiguisaient la perception des choses. Tout le monde s’affairait, discutait, le geste emporté.

À peine descendus de selle, le vicomte et les jurés disparurent avec Pisan dans la grande salle, laissant les sergents s’occuper des montures. Ernaut s’employa à trouver son frère, demandant après lui aux domestiques, eux aussi bien occupés. On lui répondit évasivement qu’il ne tarderait pas à le voir.

Les habitants du casal commençaient à rejoindre la cour, dans laquelle, ils le savaient, le cérémoniel commencerait. Iohannes fut parmi les premiers à arriver et il se dirigea directement vers Ernaut. Il le salua, l’air grave, et le tira à part.

« Je n’ai pu m’expliquer avec Aymeric hier soir, il me croit son ennemi. J’ai néanmoins pu narrer l’affaire à Lambert, rapidement.

— A-t-il fait commentaire ?

— Non. Il semblait résigné.

— Et Umbert, va-t-il porter témoignage ? »

Iohannes prit un air embarrassé.

« Je n’ai pu le voir. »

L’inquiétude rendit Ernaut livide.

« Il a disparu ?

— Non, rien de cela. Sa porte m’est demeurée scellée. »

Un frisson de colère parcourut Ernaut.

« Que croit-il ? Qu’on va le laisser… Il commença à fulminer sur place. Tu aurais dû forcer le passage.

— Et faire quoi ensuite ? Je me suis dit qu’il sortirait pour la bataille. »

Ernaut soufflait. Il évita de croiser le regard de Iohannes, bien près de lui reprocher de n’avoir pas assez insisté. Lui aurait sans nul doute brisé la porte pour entrer et fait raconter tout ce qu’il fallait au jeune valet. Il serrait et desserrait les poings nerveusement, regardait aux alentours comme s’il cherchait quelque chose ou quelqu’un sur qui taper et déverser son trop-plein de colère. Baset, un des sergents d’armes venus avec lui, eut la mauvaise idée de venir le tancer à ce moment-là. Ils devaient s’assembler en ligne pour délimiter un espace au bas de l’escalier. Ernaut lui répondit d’une voix blanche qu’il prendrait sa place quand il le faudrait. Apercevant son état de fureur, l’autre n’insista pas et se retira, contrarié.

Tournant le regard en tous sens, piétinant pour évacuer sa hargne, Ernaut aperçut Godefroy qui s’avançait, en compagne d’Osanne et de Perrote. Il était accompagné d’un des sergents. Le visage las, la mine grave, on aurait dit qu’il allait dès à présent être pendu. La peur emprisonna le cœur d’Ernaut dans un étau. Il secoua la tête un instant, cherchant à évacuer la colère grondant en lui. De la main, il invita Iohannes à le suivre jusqu’à l’accusé.

Godefroy l’accueillit d’un sourire sans chaleur, sa femme et sa fille d’un signe de tête à peine perceptible. Le vieil homme avait revêtu sa plus belle cotte, d’un orange éclatant, rehaussée de bandes brodées au col et aux manches. Il chercha ses mots un instant, mais ne put émettre un son. Ernaut lui posa amicalement la main sur le bras et garda le silence également. Les femmes se resserrèrent autour d’eux et ils demeurèrent ainsi un long moment, sans parler. Pendant ce temps, la foule s’amassait peu à peu.

Des enfants couraient, mimant le combat à venir à grand renfort de cris joyeux. Les parents s’en amusaient ou s’en courrouçaient, selon, amenant le calme par des remontrances sévères. D’autres encore s’esclaffaient des pitreries d’un grand dadais qui mimait la pendaison, la langue hors de la bouche, les yeux révulsés. Pour peu qu’aucun des leurs n’y soit impliqué, beaucoup considéraient la justice comme un spectacle plaisant, qui avait en outre l’avantage de calmer les esprits et d’effrayer les malfaisants.

Plusieurs appels de cor les firent se tourner vers le grand escalier, au pied de la tour. Là, le vicomte et l’intendant, l’air sérieux et emprunt de gravité attendaient, leur mantel de cérémonie bruissant dans le vent léger. Devant eux, les bourgeois de la cour du Roi, tout aussi hiératiques, avaient rejoint deux jurés de la Mahomerie, et ils attendaient, en demi-cercle face à la foule amassée, contenue par les sergents. On pouvait reconnaître l’impressionnante silhouette de Pierre de Périgord, la mine modeste d’Albert Lombard. André de Tosétus était également de nouveau présent, flanqué des deux frères Rufus, à la chevelure rouquine : Guillaume et Simon. En dernier, le vieil Hugues de Saint-Élie échangeait à voix basse quelques commentaires avec son voisin Pierre Salomon.

Une effervescence naquit vers l’entrée de la curie et les regards se portèrent tous dans cette direction. Aymeric le Grand venait d’apparaître, escorté d’un homme d’armes. Il était vêtu d’une chemise teinte d’écarlate, avec des chausses de même couleur. Rasé de près, il s’était également fait coiffer, donnant à son visage crispé un air de jouvence. Il tenait un genre de masse à la main, dont un des côtés était orné d’une pique. Son autre bras était passé dans les énarmes d’un bouclier rond, peint d’ocre rouge. Il fendit la foule pour se trouver face aux dirigeants. Le silence se fit, à peine perturbé par une toux, un raclement de pieds.

Ernaut et Iohannes s’étaient avancés. La voix d’Aymeric résonna, haute et claire.

« Sire, la cour et le vicomte m’ont ordonné de venir équipé de mes armes au jour d’hui, comme champion, et me voici prêt, comme je l’avais dit avec toutes mes armes contre Godefroy, qui se trouve là, de ce que je me suis plaint des coups et du meurtre qu’il a faits. »

Disant cela, il tendit son arme en direction du vieil homme, qui se trouvait en bordure du cercle, la tête baissée, les épaules affaissées. Le vicomte leva la main solennellement et acquiesça, faisant signe à deux gardes.

« Qu’on le mène au champ de bataille, et que nul ne lui porte le moindre mal. »

Iohannes indiqua à Ernaut à voix basse que la lice avait été tracée devant l’église, en haut du village. Accompagnant Aymeric du regard, le jeune homme cherchait Umbert, qu’il ne voyait pas.

« S’il ne se présente pas ici, nous irons le cueillir en son hostel au passage. »

Disant cela, il aperçut Lambert qui s’avançait dans la cour, pareillement vêtu de rouge des pieds à la tête, ses armes lourdes à la main. Il lui parut soudain bien maigrelet. Lui aussi s’était fait raser et coiffer. Les yeux perdus au loin, il échangea tout de même un regard rapide avec son frère, avant de le porter sur Godefroy et, surtout, Osanne. Il marchait comme un fantôme jusqu’à se retrouver en bordure de la foule aux côtés du vieil homme. Celui-ci s’avança lentement, comme à regret et prit la parole d’une voix chevrotante puis plus assurée.

« Sire, je viens devant vous au jour que la cour et le vicomte m’ont indiqué, avec mon champion équipé de ses armes, pour me défendre contre Aymeric, du meurtre et des coups que je nie, pour le rendre mort ou à ma merci à toute heure du jour. »

Le vicomte hocha la tête et ordonna qu’on mène également les deux hommes à la zone du combat, en haut du casal. Puis il s’entretint un instant avec l’intendant avant de retourner dans la grande salle à ses côtés. Les jurés montèrent les rejoindre. Ils allaient certainement profiter de la venue d’Arnulf pour aborder d’autres points que le combat.

La matinée n’était pas trop avancée et il faudrait attendre un peu que le soleil soit à la verticale, avant que l’affrontement ne puisse commencer. Un de ses collègues interpella Ernaut, lui expliquant qu’ils avaient pour mission de garder la lice pendant le combat. Ernaut hocha le menton, indiquant qu’il se rendait sur place. Il tourna la tête vers Iohannes.

« Nous devons passer devant la maison d’Ogier. Nous allons questionner Umbert et l’obliger à révéler ce qu’il sait. »

Ils suivirent la foule qui bruissait de pronostics sur les chances des deux champions. Des regards hostiles émaillaient les échanges, de sourdes haines trouvaient là l’occasion de se manifester sans trop de retenue. Lorsqu’il s’arrêta devant chez Ogier, Ernaut expliqua à ses collègues qu’il avait une chose urgente à faire. L’un d’eux, un gros joufflu à la moustache épaisse comprit à demi-mot et incita le groupe à continuer sa route. Pendant ce temps, Iohannes frappait à la porte avec insistance. Ernaut se joignit à lui et ils appelèrent de leurs voix mêlées le jeune homme. Peu à peu, le ton se fit plus pressant et impérieux. L’huis tremblait des coups de pied et de poing assénés par Ernaut. Finalement, la porte s’ouvrit, sur un jeune garçon au visage défait, la mine sombre. Les cernes accusaient le manque de sommeil et, à son allure abattue, on aurait pu croire que c’était lui qui allait se battre en champ clos. Ernaut entra sans aucun égard, le bousculant presque et gronda plus qu’il ne parla.

« Il va te falloir nous conter la vérité, ou il pourrait t’en cuire ! »

### Casal de la Mahomerie, jeudi 19 juin, matinée

Sans même demander à Umbert, Ernaut pénétra dans la pièce et se dirigea vers l’escalier. Il se figea au bas des marches, regardant les deux hommes à l’entrée, attendant qu’ils le suivent, puis il monta en quelques sauts. Umbert échangea un regard inquiet avec Iohannes, mais n’obtint guère de soutien. Il baissa la tête et prit à son tour le chemin de l’étage. L’interprète poussa la porte et le suivit, se demandant comment ils allaient s’y prendre pour l’inciter à témoigner.

Apparemment, il n’avait même pas prévu de se rendre au combat. En haut, Ernaut faisait les cent pas, les pouces passés dans le baudrier d’où pendait son épée et où sa masse trônait. On aurait dit un ours en cage.

Umbert ouvrit de grands yeux, effrayé, et s’installa à la table. Il n’était vêtu que d’une chemise, ses affaires gisaient éparpillées sur la banquette où il avait dormi. Il fit mine de chercher à boire dans le pichet sur la table et sursauta quand Ernaut lui adressa la parole avec agressivité.

« Nous savons tout, Umbert ! Tu ne peux demeurer coi ainsi ! »

Le jeune valet lui lança un regard désespéré, mais cela ne calma pas Ernaut pour autant.

« Ogier était fieffé coquin, il faisait bien vil commerce avec les mahométans. Tout vient de là, n’est-ce pas ? »

Umbert lança un regard à Iohannes, comme un appel, mais sans réponse. Il hésita à parler, puis baissa la tête. Pendant ce temps, son accusateur continuait.

« Lors de son dernier voyage quand vous étiez à Saint-Gilles, Ogier a trahi ! Il a tué ou livré les hommes et a caché la femme. Que comptait-il en faire ? L’épouser ? Mais c’est interdit par l’Église, ça, face de singe ! »

Entendant cela, le jeune semblait vouloir s’enfoncer dans le sol, penaud. Ernaut s’approcha.

« Alors, où l’a-t-il cachée ? »

La voix se fit tellement fluette qu’elle mourut, mais Umbert se racla la gorge et répéta.

« Elle est partie… »

Ernaut écarquilla les yeux, étonné, puis les fronça immédiatement, se penchant de nouveau, une main sur la table, vociférant sous le nez du valet.

« Que me chantes-tu là ? Où ça ? Pourquoi ? »

Accable, Umbert ne semblait pas en état de répondre. Il ne réussit à le faire que d’une voix morne, hachée, tremblant sous la menace.

« Elle est partie. Il ne l’a pas épousée… Elle est partie.

— C’est ça que tu faisais ces derniers jours ? Tu l’aidais à fuir ?

— Non.

— Où est-elle partie alors ? »

Le jeune homme leva la tête, les yeux brillants, comme enfiévrés.

« Je ne sais. Elle a dû rejoindre Damas ou Babylone.

— Je t’ai dit que les voyageurs n’avaient pas rejoint les leurs. Ton maître les a tués, livrés à des marchands d’esclaves ou Dieu sait quoi. Tu comprends ce que ça veut dire ? »

Umbert ferma les yeux, consterné. Il glissa enfin dans un souffle « C’est bien cela que les voyageurs cherchaient alors…

— De qui parles-tu ?

— Les marchands qui sont venus. En fait, l’un d’eux était cousin de ceux du dernier passage. Il cherchait maître Ogier.

— Face de Carême ! Tu ne pouvais pas le dire plus tôt ? »

Il se retint de frapper le visage devant lui et se contenta de l’abattre sur la table, faisant tinter les céramiques. Puis il déambula dans la pièce, excédé, ronflant comme soufflet de forge. Iohannes demeurait à l’écart, assis sur une banquette. Le géant revint vers la table, s’y appuyant des deux mains. Il s’efforçait, à grand-peine, de ne pas crier, sa voix blanche encore plus terrifiante que tous les hurlements.

« Il t’a dit quoi ?

— Peu de choses. J’ai cru comprendre qu’il demeurait une partie du voyage à payer. Il m’a dit qu’il repasserait.

— Bougre de crétin ! Ce n’était pas pour verser quelques monnaies. Il venait chercher vengeance, le comprends-tu ? »

Le jeune homme se mit à pleurer.

« Dehors, des hommes vont s’entrebattre à cause de toi. Aucun d’eux n’est coupable, entends-tu ? Il faut leur dire la vérité ! »

Iohannes intervint, plus calme, et ajouta d’une voix néanmoins sans indulgence.

« Ton silence vaut crime, Umbert. Quel besoin de protéger la fame de ton maître ? »

Le valet releva la tête, des pleurs inondant ses joues.

« Il s’est toujours montré si bon avec moi, comment…

— Ce n’est pas le trahir que d’en narrer les travers. N’ajoute donc pas tes péchés aux siens.

— Il te faut venir conter à tous ce que tu sais ! » éructa Ernaut, cherchant à l’empoigner par le bras.

Le jeune s’affala, tentant de résister en geignant, refusant d’une voix craintive. Iohannes s’interposa, faisant lâcher prise à Ernaut qui lui décocha un regard furieux.

« Ne veux-tu pas venger ton maître ? Celui qui l’a tué est au loin et des innocents risquent leur vie par ta faute.

— C’est que, sans moi…

— Tu ne pouvais savoir ton maître si félon. Il n’y avait nulle malice à renseigner un de ceux qui pouvaient avoir à payer Ogier. »

Le jeune homme s’essuya le nez et les yeux de la manche. Iohannes continua d’une voix tranquille tandis qu’Ernaut les surveillait, tel un lion prêt à sauter sur sa proie. Au moindre relâchement, il reprendrait son interrogatoire.

« Il est encore temps de dire qu’Ogier a été tué par mahométan sur la piste de sa parentèle. Nous pourrons témoigner qu’il faisait office de passeur, à de nombreuses reprises. Outre cela, nous savons qu’il n’a pas mené le dernier convoi à bien et les a dépouillés, si ce n’est pire. »

Il fut interrompu par des coups violents sur la porte et une voix qui hélait Ernaut. Ce dernier s’approcha de la fenêtre en deux enjambées, ne perdant Umbert de vue qu’à contrecœur. Baset était devant la maison, l’air peu amène.

« Ernaut, foutre-cul, on t’attend ! Le vicomte et les jurés sont arrivés au champ.

— J’ai fort urgente affaire ici, compère !

— Viens-t’en tout de suite ou on va avoir soucis. Ils se font présenter les armes, là ! »

Ernaut bougonna et rentra le buste avant de déclarer à Iohannes : « Le combat ne va pas tarder, il nous faut aller là-haut, qu’Umbert témoigne ! »

À ces mots, le jeune émit un gémissement plaintif, et s’accrocha à la table.

« Non, je ne pourrai pas !

— Tu vas me suivre, bougre de … »

Ernaut attrapa Umbert par le col de sa chemise, mais celui-ci résista et un craquement sonore accompagna la déchirure. Iohannes posa la main sur le bras de Ernaut, l’invitant d’un regard à plus de retenue. Il s’adressa au valet d’une voix autoritaire.

« Nous en avons convenu, Umbert, tu dois parler !

— Je ne le peux, non. Ils vont tous m’en vouloir et si on apprend… Non, je ne veux… »

Dehors la voix résonna une nouvelle fois, appelant Ernaut. Iohannes se rapprocha de Umbert, parlant désormais ainsi qu’à un jeune enfant.

« Que crains-tu ? Tu ne feras que dire vérité.

— Je sais bien que mauvaises nouvelles font mauvais accueil. Ils penseront que je suis félon valet. C’en sera fini de moi ! »

Ernaut fit mine de se préparer à lui adresser un revers de la main puis s’éloigna, à bout de nerf. Iohannes soupira, un peu agacé avant qu’une lueur rusée n’apparaisse dans son regard.

« Et si on voyait pour faire de toi un client du Saint-Sépulcre ? Tu serais ainsi protégé !

— Oui, je… osa la voix timide.

— Tu en avais désir de toute façon, non ? »

Umbert hocha la tête doucement et répondit, entre deux hoquets : « Si je peux prêter serment, je veux bien tout narrer. Mais il me faut être accepté par sire Pisan avant. Je ne sortirai pas sans cela !

— Ernaut, je ne peux tarder plus ! Si tu ne viens pas céans, il pourrait bien t’en cuire ! Ils vont achever de présenter les armes d’ici peu… » lança la voix depuis la rue.

Le géant maugréa de rage et crispa les mâchoires.

« Reste avec lui, Iohannes. Je vais quérir l’intendant et faire en sorte qu’Aymeric ne prête serment. »

Puis il bondit vers l’escalier, fut dehors en quelques sauts, courant afin de rejoindre son compagnon sergent.

### Casal de la Mahomerie, jeudi 19 juin, fin de matinée

La rue était pleine de colons dont la plupart étaient amassés aux abords de l’église. Cherchant dans la foule le vicomte et l’intendant, Ernaut ne prenait pas garde où il allait et bouscula sans ménagement plusieurs personnes qui, comme lui, montaient la rue. Il fut apostrophé d’une voix forte par Guillaume le Provençal. Il allait en famille assister au combat, discutant avec des voisins.

« Alors, maître, vous n’avez pu débusquer le païen à l’origine de ça ? »

Ernaut secoua la tête, ne souhaitant pas s’attarder auprès d’eux, et continua d’avancer.

Il en fut empêché par une ribambelle de gamins, menés par une femme au long nez, la bouche pincée. Elle était flanquée d’un homme au physique mou, à l’allure pataude. Il leva le menton pour saluer Ernaut.

« Je suis fort désolé de voir que tout ça c’est à cause d’Ogier. Moi j’aime bien le vieux Godefroy. Pis vot’ frère.

— C’est Hélias, mon voisin, précisa le Provençal. »

Ernaut sourit à demi par politesse, mais s’efforçait plutôt d’avancer parmi la foule. L’autre ne s’en préoccupa guère et lui tira le bras.

« C’est vrai ce qu’on dit, que c’est des païens qui l’auraient meurtri ?

— Certes, je dois en informer le sire vicomte, qu’il arrête la bataille avant qu’il ne soit trop tard. »

L’homme se retourna vers son épouse et lui lança, l’air narquois :

« Tu vois bien que ce larron avait du monde en son hostel ! »

Intrigué, Ernaut s’arrêta.

« De quand parlez-vous ?

— Bein quelques soirs avant celui où on lui a fait son affaire. »

Il mima un coup de couteau sur la gorge avec l’index, la langue sortie, l’air quasi amusé.

« Vous avez entendu des voix ?

— Bah, plus que ça, des cris ! »

Sa femme leva les yeux au ciel, mais il continua.

« Je revenais de chez… De chez un ami. On avait fort sué toute la journée, à creuser parmi la caillasse pour lui faire une citerne. Il prévoit quelques fruitiers, alors il est toujours bon d’avoir de l’eau en quantité. Il a d’ailleurs promis de venir m’aider…

— Et donc, en rentrant ? le coupa Ernaut.

— En rentrant ? Ah oui ! Comme je revenais, j’ai eu besoin de me soulager, je suis allé au jardin. On avait bu pour se déssoiffer de toute cette poussière et de la chaleur. C’est qu’on attrape chaud à briser la roche ! La soif du terrassier qu’on dit, j’pissais de la poussière ce soir-là. »

Sa femme se pinça le nez, brûlant d’intervenir, mais elle se concentra sur un des gamins qui embêtait un des petits. Hélias continua.

« Enfin bref, j’étais là, tranquille au courtil, quand j’ai entendu force cris qui venaient d’à côté !

— Quelle sorte de cris ?

— Ça se bestanciait fort ! Et que je te gueule dessus et que je te réponds avec colère. Pour sûr qu’il y avait de l’animation dans le manse du vieil Ogier ! »

Ernaut lui serra le bras, provoquant chez lui un mouvement de recul inquiet.

« Mais pourquoi n’avoir rien dit de cela auparavant ? Vous avez peut-être entendu les murdriers ! »

Sa femme, alors, n’y tint plus et elle s’avança vers eux, les interrompant de sa voix de tête.

« Ne l’écoutez pas, il dit cela pour faire son intéressant. Il était plus saoul que chanoine ce soir- là ! »

Ernaut fit une grimace, un peu dépité, mais Hélias ne s’en laissa pas compter.

« J’avais bu quelques godets, possible, pourtant j’ai bien entendu haut et clair qu’on se chamaillait, que j’te dis !

— Je suis moi-même sortie à ce moment. Une des chattes venait de faire ses petits et on lui met une gamelle de lait. Vous savez, les enfants… »

Disant cela, elle eut un sourire attendri à l’intention de la marmaille remuante à ses côtés, adoucissant fugacement son visage revêche.

« J’ai bien entendu des voix aussi : c’était que le vieux et son valet. Ils parlaient fort, mais sans pour autant hurler comme le croit Hélias.

— Le valet ? Umbert ?

— Certes, le jeune commis qui l’assiste au champ. Nul païen mystérieux. J’ai bien reconnu leurs voix.

— C’est que ça s’était calmé entre-temps ! » insista Hélias.

Sa femme secoua la tête, désolée.

« Il raconte toujours des choses quand il a trop bu avec le Pierre, alors je lui ai dit de ne pas répandre pareilles bêtises.

— Il a peut-être ouï querelles avant que vous n’arriviez.

— Et les voyageurs païens se seraient tus, pour ne laisser parler que le jeune valet ? Non, il a plutôt tout inventé. Pas de quoi déranger le vicomte. »

La femme ajouta, d’une voix autoritaire : « De toute façon, le vieil Ogier ne recevait pas, et n’employait pas de journaliers en dehors du gamin. Avec qui d’autre aurait-il pu s’entretenir à la veillée ?

— Moi je te dis que ça gueulait, comme cochon avant l’hiver quand on va le saigner ! » s’entêta Hélias, bougonnant.

Ernaut se passa une main sur le front, cherchant à comprendre comment tout cela pouvait prendre place dans ce qu’il savait déjà. Il remercia d’un geste et se rapprocha de la lice. À peine arrivé aux abords, il aperçut Baset lui faisant de grands signes de tête. Il lui indiquait un endroit où se tenir, qu’Ernaut rejoignit tel un somnambule. Il avait besoin de temps pour réfléchir.

Une vaste zone circulaire avait été délimitée par une légère barrière. Au centre, le vicomte et les jurés étaient en train d’examiner le bâton de combat d’Aymeric, ainsi que son bouclier. Celui-ci se tenait face à eux, l’air bravache, le rouge de sa tenue tranchant sur les tenues beiges et poussiéreuses de la foule derrière lui.

Pour quelle raison y aurait-il eu dispute entre Umbert et Ogier ? Si le valet devait dévoiler l’histoire, Ernaut ne tenait pas à ce que cela se fasse au détriment de Lambert. Le gamin pouvait très bien garder quelques révélations ennuyeuses et aggraver encore la situation. Pour arrêter le combat, il devait être certain de ce que le jeune allait raconter. Ne pas se faire surprendre.

Un des jurés s’avança devant Aymeric, portant un lourd grimoire de parchemin, à la couverture ornée : un évangéliaire. Il dit alors d’une voix forte, que tous l’entendent :

« Aymeric, jure sur cet évangile, de ton poing droit, comme loyal chrétien, que tu n’as sur toi nul armement avec lequel tu pourrais blesser ou assaillir Lambert qui se trouve là et que tu dois combattre, et que, si tu en avais, tu l’ôterais le temps de l’affrontement. »

Aymeric tendit la main et prêta serment, la voix moins assurée qu’il l’aurait voulu. Disant cela, il lançait un regard de défi à Lambert, face à lui, entouré de Godefroy et d’Osanne.

Après tout, quelques jours avant le meurtre, les voyageurs étaient passés et peut-être qu’Ogier n’avait pas été enchanté qu’Umbert leur ait confirmé qu’il demeurait là. C’était compréhensible, s’il les avait trahis. Pour le valet, le voyage était tel que les précédents. Reconnaître qu’ils s’étaient disputés l’aurait obligé à évoquer l’activité de passeur d’Ogier. Ce devait être la raison pour laquelle il s’activait à l’écart depuis le meurtre, ne sachant comment se comporter. Il fallait lui garantir un sauf-conduit pour être sûr qu’il parle en confiance.

Le vicomte, les jurés s’étaient avancés vers le frère d’Ernaut.

« Lambert, montre-moi le bâton et le bouclier avec lesquels tu comptes affronter Aymeric qui est là et donne-les-moi. »

Le champion s’exécuta, fébrile, tendant ses armes. Les jurés les examinèrent alors attentivement, vérifiant qu’elles étaient loyales et conformes.

Pendant ce temps, Ernaut regardait dans la direction de Pisan qui se tenait en retrait, de l’autre côté du champ clos, suivant le cérémoniel avec attention. Il avait le visage crispé, ennuyé, et ses yeux emplis de pitié s’attardaient souvent vers le sol. Lambert prêta à son tour serment sur les Évangiles et le petit groupe d’officiers rejoignit l’intendant, se disposant en bon ordre face à lui.

Pierre de Périgord déclara alors :

« Nous avons fait ce qu’il convenait avec les champions, ils sont à vos ordres.

— Laissez-les aller l’un à l’autre » répondit-il, d’une voix triste, mais décidée.

Pierre de Périgord se tourna alors vers le vicomte :

« Faites venir le crieur et faites crier : va ici et là, et crie de façon à ce que chacun l’entende : que l’on ne parle ni ne fasse quoi que ce soit à ces deux champions qui sont en ce champ, et que celui qui négligera cet ordre sera à la merci du seigneur, corps et avoir. »

Le vicomte acquiesça et fit signe à un valet, qui n’attendait que cela pour partir en courant, hurlant dans tout le casal, afin d’informer ceux qui ne seraient pas encore présents. La justice devait se faire en public.

L’annonceur parti, Arnulf lança un coup d’œil à un bâton fiché en terre. Ernaut savait qu’il vérifiait l’ombre. Tant qu’il ne serait pas midi au soleil, on ne ferait pas s’affronter les deux combattants. Il avait encore un peu de temps. Négligeant les instructions de Baset, il se rapprocha de Pisan qui échangeait désormais à voix basse avec le vicomte et quelques jurés.

### Casal de la Mahomerie, jeudi 19 juin, midi

Ernaut faisait le tour de la lice de combat, se souciant peu des pieds qu’il écrasait, des personnes qu’il bousculait. Il préféra passer au large de Baset, dans le dos d’Aymeric. Il pouvait donc voir son frère, les armes en main, prêt à lancer l’assaut. Il avait le regard perdu, mais son visage s’adoucit quand il reconnut la silhouette du géant qui se faufilait. Ernaut lui fit un sourire encourageant, plus assuré qu’il ne l’était vraiment.

Quand il parvint enfin aux abords du groupe de jurés où se tenait l’intendant, il tendit la main pour attirer son attention. Ce faisant, tous se tournèrent vers lui. Le vicomte Arnulf dévoila un sourire fugace sur ses lèvres fines, mais ne dit rien. Ce fut Simon Rufus, le front soucieux, qui fut le plus prompt à réagir.

« Qu’y a-t-il, sergent ? Quelque souci en ce jugement ?

— Je ne sais, maître Rufus. Il me faut en deviser avec le sire intendant. »

Sa remarque perturba le petit groupe qui s’agita. Simon se rapprocha de lui.

« Il te faut t’expliquer, sergent. Nous ne pouvons laisser aller les choses si elles ne sont pas correctes.

— Je ne suis pas encore certain, maître Rufus.

— Voyons ! Le soleil est presque à son mitan, nous ne pouvons attendre que tu daignes parler ! »

Le vicomte posa une main apaisante sur l’épaule du juré.

« C’est son frère le champion à senestre, maître Rufus. J’ose croire qu’il ne jetterait le trouble à la légère. N’est-ce pas, sergent ? »

La question avait été formulée sans violence, mais elle contenait une menace très explicite. Ernaut n’avait pas le droit de se tromper. Il en allait de sa carrière au service du roi et peut-être, au-delà, de son avenir tout court. Il avala sa salive, une boule au fond de la gorge.

« Sire vicomte, j’aurais besoin que l’intendant me compaigne. J’ai peut-être témoin qui pourrait convaincre Aymeric d’abandonner ses folles accusations.

— Un témoin, à cette heure ! s’amusa Arnulf, faussement joyeux. En ce cas, qu’il se montre et parle devant la cour, nous l’entendrons, ainsi qu’Aymeric.

— C’est là le souci, sire vicomte. Il lui faut avoir avis du sire intendant avant de se prétendre témoin. Il craint fort pour sa vie…

— S’il fait fausse route ? » demanda Simon Rufus.

Ernaut hésita un instant puis haussa les épaules.

« Il ne m’a confié en détail ce qu’il sait, mais je le crois assez pour savoir que c’est fort important. »

Le vicomte se tourna vers Pisan, une interrogation sur le visage. Le vieil intendant hocha doucement la tête et déclara d’une voix douce.

« Je vais aller voir. S’il est un moyen de se garantir par témoin sans demander à Dieu de trancher par combat, je le préfère.

— Faites diligence, alors, sire Pisan, car je ne saurais surseoir la bataille » trancha Arnulf.

Il s’adressait à l’intendant, mais ses yeux ne quittaient pas Ernaut et la précision fut entendue. Le jeune homme salua respectueusement et montra la voie. Autour d’eux, quelques badauds étonnés commentaient la scène, sans avoir la moindre idée de ce qui se passait. Bien évidemment, cela ne dérangeait en rien leur capacité à divulguer ce qu’ils en savaient, avec une assurance qui en disait long sur leur ignorance. Pisan demanda à Ernaut où ils se rendaient, et ce dernier lui expliqua qu’ils allaient voir Umbert, le jeune valet d’Ogier.

« Il va se porter témoin pour Godefroy ? Que voilà bien étrange affaire. Je l’aurais cru plus volontiers enclin à vouloir venger son maître.

— Je ne sais exactement, sire. Il exige de devenir client du Saint-Sépulcre avant de tout dévoiler. Je pense que si vous l’en assurez, il s’ouvrira alors à temps pour éviter la bataille. »

L’intendant hocha la tête. Lui non plus n’était guère heureux de voir deux hommes tester la miséricorde divine.

« Il s’enfrissonne tant de ce qu’il sait, qu’il lui faut l’Église pour le protéger ? Cela doit être bien terrible secret…

— Ogier n’était pas homme facile, sire.

— Il avait sa part d’ombre comme chacun, de certes. Mais j’ai toujours su qu’il en avait plus que d’aucuns, à se méfier de tous, ainsi, à chaque instant. »

Ernaut se dit par-devers lui que ses crimes devaient lui peser sur la conscience. Savoir que des familiers pouvaient venir crier vengeance, s’attendre à être trahi comme lui le faisait. Il s’était bâti son propre enfer sur terre finalement.

Ernaut s’arrêta brusquement, provoquant la surprise de l’intendant, qui s’enquit de ce qui se passait.

« Il était fort méfiant, dites-vous ?

— Oh oui ! On l’aurait cru pareil au conil en pleine clairière.

— Il a pourtant ouvert à son murdrier !

— Que me contez-vous là ?

— La porte a été ouverte, pas forcée. Il n’aurait pas oublié de la clore bien sûrement, tout de même.

— Certes pas. La nuit tombée, nul ne franchissait son seuil sans qu’il le veuille. »

L’intendant ouvrit des yeux ronds, cherchant à suivre Ernaut dans ses déductions. Celui-ci sentait son cerveau s’emballer. Il murmura :

« Il aurait ouvert à quelqu’un de sa connaissance, jamais à un inconnu. Ce ne peut être étranger venu de loin. Surtout s’il avait tancé Umbert à ce propos.

— De quoi parlez-vous donc ? »

Ernaut reprit son chemin, perdu dans ses pensées. Sachant que la famille des membres du dernier convoi le recherchait, il ne pouvait se fier à quiconque. Une seule personne était digne de confiance, le lui avait prouvé jour après jour. C’était Umbert, son jeune et fidèle commis. Mais pourquoi donc aurait-il décidé de frapper son maître ?

Ernaut repensait soudain à Iohannes, demeuré seul avec celui qui avait peut-être tenu l’arme cette nuit-là. Il lui fallait comprendre comment un jeune homme serviable et attaché à son maître se serait subitement découvert une vocation d’assassin. Il revit le cadavre mutilé, cela devait avoir un sens. La dispute quelques jours auparavant. Juste après la venue de la famille des fuyards. Parmi eux une jeune fille amoureuse…

Il écarquilla les yeux, frappé par la révélation, mais il ne s’arrêta qu’un très court instant. C’était sûrement cela ! Ce n’était pas d’Ogier que la jeune Safiya était amoureuse. Comment l’aurait-elle pu ? Elle n’aurait pas menti pareillement à son amie. Muniya disait qu’elle portait les messages à Umbert et Iohannes et lui pensaient qu’il servait de coursier. Mais il devait en réalité être le destinataire. Il rêvait de sauver la belle musulmane.

Ce mystérieux voyageur ne lui avait peut-être pas dit qu’il souhaitait verser quoi que ce soit, mais avait plutôt proféré des menaces, plus ou moins claires, lui faisant découvrir l’étendue de la duplicité d’Ogier. Apprendre que son maître avait trahi, vendu sa bien-aimée, ou peut-être pire… La mutilation devenait compréhensible, œuvre d’un amoureux désespéré.

Ernaut sentit sa poitrine se comprimer devant l’ignominie de l’homme qui arrivait encore à provoquer des morts après avoir été châtié, en profitant de la lâcheté de son bourreau. S’il est de certains hommes qui savent tirer le meilleur de leurs compagnons, d’autres ne sont doués que pour abaisser, souiller ceux qui les fréquentent. Il fallait absolument briser cette malédiction et permettre à la vérité d’éclater.

Ils arrivèrent devant la maison, que Iohannes avait barrée à la suite d’Ernaut. Le jeune homme frappa avec vigueur. Puis, ne voyant rien venir, il appela plusieurs fois, la voix de plus en plus inquiète. Il dévisagea l’intendant, qui ne comprenait pas ce qui se passait.

« Patientez ici, ce n’est pas normal. »

Puis il s’élança pour rejoindre le passage menant aux jardins, à quelques maisons de là. Il n’arrivait plus à penser. Le soleil, lui cuisant le front, la nuque, se rappelait à lui. La sueur lui coulait dans le dos. Il bondissait, sautait, plus qu’il ne courait, espérant encore, n’osant imaginer ce qui s’était passé dans la maison. Il longea les jardinets, traversa la cour. La porte du bâtiment était grande ouverte.

En quelques enjambées il était dedans et se cogna à un tonneau, aveuglé par les ténèbres après la lumière éblouissante. Il avança comme un taureau furieux, indifférent à ce qu’il renversait, brisait, piétinait, se servant de ses mains pour deviner les obstacles. Apercevant la lueur en haut de l’escalier, il s’y élança, appela Iohannes, toujours sans réponse.

Lorsqu’il arriva dans la pièce, il aperçut le traducteur, allongé sur le sol. Il lança un hurlement de dépit, grognement sourd à peine humain. Il avait compris. Trop tard.

### Casal de la Mahomerie, jeudi 19 juin, midi

Agenouillé à côté de Iohannes, Ernaut vérifia s’il voyait du sang et le retourna avec douceur. L’interprète gémit, faisant naître un sourire sur le visage du jeune homme. Il l’appuya contre le mur, avant de pousser le volet et de se pencher au-dehors. L’intendant était toujours là.

« Umbert s’est enfui. Je crois que c’était lui le murdrier. Il a assommé Iohannes. »

Pisan eut un froncement de sourcil inquiet.

« Il nous faut prévenir les champions avant qu’ils ne prêtent leur serment, sinon tout ceci aura été en vain ! »

Ernaut jeta un coup d’œil à Iohannes, qui dodelinait de la tête, la main sur le crâne. Il semblait reprendre ses esprits et ahana avec peine :

« Va prévenir, Ernaut, je me porte garant de Godefroy, je sais qui a murdri. Nul besoin pour eux de s’entrebattre.

— Je te laisse un instant, le temps d’une poignée de *Pater*. Je reviens vite !

— Fais donc, je ne bougerai pas. »

Le jeune homme descendit les escaliers et alla débloquer la porte, retrouvant Pisan. Ils se hâtèrent de rejoindre la foule, désormais silencieuse. Ernaut vit le vicomte au centre de la lice. Un des jurés, portant l’Évangéliaire, approchait. Ernaut hurla du plus fort qu’il le pouvait:

« Ne prêtez pas serment ! Nous avons débusqué le murdrier. Iohannes en est garant. Ne prêtez pas serment ! »

Il écarta sans ménagement les gens devant lui, traçant le passage pour l’intendant. Ils arrivèrent auprès de la barrière. Le vicomte sembla un court instant amusé, mais sa voix n’avait rien de joyeux lorsqu’il prit la parole.

« Nul ne peut interrompre la bataille sans valable raison, sous peine de se mettre à merci. »

Il dévisagea Pisan, désormais à côté de Ernaut, puis revint au sergent.

« Sire intendant, je vous sais prude homme. Parlez vite, car le soleil n’attendra pas.

— Sire vicomte, il semble que terrible félonie ait eu lieu et Umbert, le valet, est derrière tout cela. »

Il se tourna vers Ernaut, l’invitant à continuer.

« Iohannes le gardait et a été frappé traîtreusement. Il gît dans la demeure d’Ogier, trop faible pour bouger, mais encore bien vif. Il se porte garant de Godefroy et assure que le jeune valet est celui qui a frappé. Si vous venez à lui, nous saurons vous narrer en détail toute l’histoire. »

En parlant, il fixait alternativement Arnulf et Aymeric, espérant que ce dernier se laisserait fléchir. Il sut qu’il avait réussi lorsqu’il le vit baisser la tête, à moitié soulagé, mais aussi fort contrarié de s’être ainsi trompé. Un silence pesant régnait sur la place, à peine entamé par le chant des insectes environnants.

Aymeric s’approcha alors d’Arnulf.

« Sire vicomte, je ne peux jurer de présent que c’est bien Godefroy qui a donné les coups meurtriers.

— Il nous faut certes écouter ces nouveaux garants. Si vous avez accusé faussement, vous pourrez être tenu d’en subir les conséquences.

— Je suis à la merci de la cour. Plutôt recevoir châtiment que batailler dans l’erreur sous l’œil de Dieu.

— Fort bien, conclua Arnulf. Sergent, mène-nous donc à ce nouveau garant. »

Les jurés suivirent et ce fut un véritable cortège qui s’achemina jusqu’à la demeure d’Ogier. Dépouillés de leurs armes, Lambert et Aymeric suivaient, un peu incrédules, tandis que Godefroy marchait, plein d’espoir, serrant les mains de sa femme et de sa fille.

La foule fut bloquée par les sergents aux abords et seuls quelques élus purent pénétrer : les deux champions, l’accusé, les jurés, le vicomte, Pisan et Ernaut qui les guidait. À l’étage, Iohannes attendait sur la banquette, un linge posé sur la nuque. Il grimaçait, mais semblait bien plus vaillant que précédemment. Il fit mine de se lever lorsqu’il vit le vicomte, mais celui-ci, d’un signe, lui indiqua de rester assis. Après s’être assuré que tout le monde était présent, il se tourna vers Ernaut, puis Iohannes.

« Alors, qu’en est-il de ceci ?

— Sire vicomte, le jeune valet Umbert est celui qui a frappé mortellement Ogier, voilà plus d’une semaine, déclara l’interprète.

— Vous êtes prêt à en jurer sur les saints Évangiles ?

— Certes oui. Il m’a presque brisé le crâne avant de s’enfuir, une fois son forfait dévoilé. »

Le vicomte se tourna vers Pisan, lui indiquant qu’il faudrait lancer quelques hommes à la recherche du fugitif. Il lui laisserait deux sergents à cet effet, pendant un jour ou deux. Il chercha également du regard le juré porteur du lourd volume des Évangiles. Il était toujours chargé de son paquet, qu’il tenait avec grand soin sous un linge.

« Maître, avancez-vous donc et présentez ce livre à cet homme que voilà, qu’il fasse jurement. »

Le blessé se redressa avec lenteur, soutenu par Ernaut. Le vicomte, après s’être enquis de son nom, lui demanda alors d’une voix solennelle :

« Iohannes, tu jures sur cet Évangile, de ton poing droit, face à la cour des jurés, le vicomte et le sire intendant, qui sont là présents, que tu accuses le nommé Umbert, valet en fuite, d’avoir donné les coups meurtriers à son maître Ogier ?

— J’en fais serment. »

Arnulf s’esclaffa, les yeux rieurs, avant de prendre l’assemblée à témoin.

« Voilà donc la fin de cette affaire. Il est temps de m’en retourner à la cité. Faites-moi savoir si vous retrouvez ce félon. »

La tension dans la pièce retomba immédiatement, soulagement, mais aussi amère découverte pour certains qui avaient soutenu Aymeric. Lambert s’avança vers son frère et le prit dans ses bras, incapable de dire un mot.

La pièce encombrée accueillit alors un véritable tohu-bohu, chacun commentant l’affaire selon son implication et son intérêt. Le vicomte et l’intendant s’entretenaient à voix basse près de l’escalier. Pierre de Périgord s’était avancé jusqu’à la fenêtre, pour déclarer à la foule que la bataille n’aurait pas lieu, que le coupable était Umbert, désormais fugitif. Une clameur envahit alors la rue, la nouvelle faisant l’objet de commentaires variés, enthousiastes ou dépités, mais toujours bruyants.

La salle se vidait peu à peu et le vicomte appela Ernaut, l’attirant un peu à l’écart en présence de l’intendant. Il lui expliqua qu’il devait se mettre à la disposition du Saint-Sépulcre jusqu’au dimanche suivant, histoire de voir si le fuyard pouvait être attrapé. Il devait retourner à Jérusalem après cela.

Tout en délivrant ses instructions, il toisait le jeune homme avec attention. Au moment où il allait le congédier, il sembla hésiter un court instant, puis demanda finalement :

« Comment as-tu traqué ce misérable ? Tout semblait accuser le vieil homme.

— Je ne pouvais accepter que mon frère risque sa vie par bataille.

— Tu n’as donc pas foi en la justice divine, en la justice royale ? »

La voix était peu amène, mais sans hostilité, plutôt étonnée.

« Je ne saurais préjuger des desseins du Seigneur. C’est juste que l’idée que mon frère risque sa vie m’était insupportable. Je n’ai que lui ici. Alors j’ai tenté de comprendre et de trouver qui avait intérêt à meurtrir Ogier.

— Intéressante approche. Tu as ainsi remonté peu à peu la pente de tes incertitudes, jusqu’à t’exposer à la lumière de la vérité, au sommet.

— C’est plus fait de tâtonnements et de hasards, sire vicomte. Je n’ai compris tout cela qu’au dernier moment, trop tard pour m’assurer du coupable. »

Arnulf hocha la tête, un air indéchiffrable sur le visage. Il plissa les yeux et remercia Ernaut d’un signe de menton. Une fois celui-ci éloigné, le vicomte se tourna vers Pisan et lui confia à mi-voix, amusé :

« Qui aurait cru qu’une cervelle se cache parmi semblable montagne de muscle ? »

Puis il se dirigea vers l’escalier afin de rejoindre la sortie.

Godefroy, qui était resté en retrait jusque-là, un peu sonné par tout ce qui se déroulait, s’avança vers Iohannes et Lambert, qu’Ernaut avait rejoints. Ils demeurèrent un long moment sans parler, goûtant simplement le fait d’être en vie, après avoir craint des jours durant. Autour d’eux la pièce se vidait peu à peu.

« Je ne saurais vous dire combien je suis touché de ce que vous avez fait pour moi et les miens, prononça Godefroy tout à trac, d’une voix fluette, emprunte d’émotion. Vous avez chacun, à votre façon, risqué bien grand péril en mon nom. Ce n’est pas là une dette qu’il est facile d’honorer, mais je m’efforcerai de le faire.

— Nul besoin pour ma part, maître Godefroy. Il est du devoir de sa parentèle d’être là » répondit Lambert, la voix enrouée d’émotion.

Le vieil homme sourit, touché par les paroles. Iohannes, toujours son linge à la main, avait repris un peu de couleurs, et lui répliqua, un large sourire sur les lèvres :

« Pour ma part, je ne serais pas contre un petit quelque chose à boire, maître Godefroy. Cela me fluidifierait fort le sang et apaiserait ma douleur.

— Certes, par semblable chaleur, voilà bien qui me siérait aussi, compère, tu parles de vrai ! » acquiesça Ernaut.

Godefroy se mit à rire nerveusement et leur confirma qu’il avait de quoi tous les abreuver leur content et qu’il sortirait même un vin de Chypre qu’il gardait pour une grande occasion. Il avait espéré le boire le jour du mariage de sa fille, mais il s’en procurerait d’autres d’ici là. Ce n’était pas tous les jours que deux membres de la famille échappaient de peu à la mort et à l’infamie.

Il s’adressa alors plus directement à Ernaut et Iohannes :

« Il vous faudra nous conter en détail ce qui s’est passé, tout de même. Car de ce que j’en savais, Umbert était fidèle servant.

— Il m’est avis qu’à trop avoir côtoyé le démon, son cœur a fini comme charbon, rétorqua Ernaut, la mine sombre. »

### Casal de la Mahomerie, jeudi 19 juin, début d’après-midi

Les volets mi-clos pour contenir la chaleur au-dehors laissaient passer un léger courant d’air. Les raies de lumière crue dessinaient les fentes, les contours des planches de bois. Dehors, les cigales étaient seules à se faire entendre. Aucune voix, aucun cri ne résonnait. Après les derniers jours haletants, on aurait dit que le temps s’était figé.

Installés autour de la table, les convives marquaient une pause, chacun se réfugiant dans ses pensées. Lambert, encore vêtu de ses chausses rouges, avait passé une chemise de coton blanc, désireux d’oublier le combat qu’il avait été bien près de livrer. Il se tenait à côté d’Osanne qui le dévorait des yeux, lui arrachant des sourires tendres. Elle s’affairait autour de lui comme elle l’aurait fait pour un héros, ne ménageant pas les allers et retours afin de satisfaire et même anticiper ses désirs.

Perrote s’agitait également en tout sens, très affable et diserte. Elle ne tarissait pas d’éloges, qu’elle assortissait de courtes prières à tous les saints qui pouvaient lui venir à l’esprit. Iohannes en particulier, encore un peu sonné, faisait l’objet de toutes ses attentions. Elle avait sorti de ses réserves du pain, du beurre, un fromage sec bien fait et un autre frais, du miel, des fruits secs et des noix diverses, un reste de tourte, des tranches de jambon, un pâté délicatement parfumé d’épices lointaines… Tout ce qui pouvait se manger dans sa demeure finissait par se retrouver sur la table.

De son côté, Godefroy n’était pas en reste. Il avait mis en perce le tonneau promis et versé un nectar capiteux de Chypre. Il y avait ajouté une bière légère et un autre vin, plus ordinaire, qui se buvait bien coupé. Il ne parlait pas tant, mais s’empressait auprès de ses invités, les incitant à tout goûter, tout boire. Ernaut ne se souvenait pas la dernière fois qu’il avait fait pareil festin.

Les conversations dérivèrent sur les projets d’avenir, la façon dont ils cultiveraient les terres après le mariage, le nombre de journaliers nécessaires aux vendanges, le prix des futailles pour acheminer le vin, les redevances dont il faudrait s’acquitter. Le monde s’offrait de nouveau à eux, et ils se bâtissaient des avenirs chantants, bénissant de bonne chère leurs propos.

Ernaut leur narra l’assaut dont ils avaient été victimes et la proposition qui s’en était suivi. Il voyait déjà les monnaies dans sa besace et les avait dépensées plusieurs fois avant la fin du repas, en cheval, haubergeon et matériels divers. Il décrivit la maison qu’il comptait offrir à la demoiselle de ses pensées.

Quand ils eurent leur content de bonheur à venir, de tartes et de boissons, l’ambiance retomba, à l’heure où la sieste s’imposait. Lambert se tourna vers Iohannes et Ernaut, un sourire radieux lui illuminant le visage.

« Il faudra tout de même nous conter comment vous avez réussi ce miracle.

— Nul miracle là-dedans, mon frère, rétorqua Ernaut. Juste la volonté de traquer la bonne bête.

— De la chance, aussi, ajouta Iohannes. Sans le secours de Dieu, qui peut espérer parvenir à ses fins ?

— Tout de même, nous aimerions fort savoir ce qui a pu mener Umbert à pareille vilenie. Tuer son propre maître !

— Et laisser deux hommes du casal se battre pour son crime, ne l’oublie pas.

— Certes » confirma Lambert, plongeant le nez dans son verre.

Ernaut leur conta ce qu’ils avaient mis du temps à saisir, la vie d’Ogier qui expliquait sa mort. Il était persuadé que le mal commis tout au long de son existence lui avait valu cette fin atroce. Il avait triché, menti toute sa vie. Non content de mégoter sur ses redevances et ses impôts, il finit par se faire traître aux siens, en aidant des serfs musulmans à fuir. Apparemment, il exigeait d’eux de fortes sommes, n’hésitait pas à dépouiller ces désespérés.

Bientôt cela ne lui suffit plus et il en voulut davantage. Il trahit ceux qu’il était censé libérer et les mena à leur perte, sans qu’il soit évident de savoir ce qu’il leur fit exactement. Cette fois, il était allé trop loin et des ennemis décidés s’étaient levés et mis à sa recherche.

C’était certainement une des raisons qui avait entraîné son départ précipité de Saint-Gilles. Seulement, dans son aveuglement égoïste, il avait oublié Umbert, apparemment attaché à une jeune fille qui devait s’enfuir lors de ce dernier passage. Nul ne savait ce qu’elle était devenue, mais lorsque le jeune valet avait appris ce forfait, il n’avait pu le supporter et les mutilations infligées laissaient envisager le pire.

Pour conclure, Iohannes précisa ce qui s’était passé tandis qu’il était demeuré avec Umbert.

« J’ai voulu savoir le détail de ce que le voyageur musulman lui avait confié.

— Tu as fort bien fait. Il nous fallait un nom, même s’il était faux, s’il avait dû jurer et accuser.

— Je pensais ainsi. Mais il ne répondait pas à mes demandes, me semblait garder quelque mystère célé. Je me suis approché, demandant doucement ce qu’il n’osait nous confier.

— Il t’attirait à lui !

— Non, je ne crois pas. Il était tel un oisillon peureux. J’ai insisté plusieurs fois. Je compris alors qu’il répétait doucement "Une bête, une vile bête", les yeux comme fous, perdu en lui-même. Puis il est demeuré coi un moment, jusqu’à ce que… »

Il s’interrompit, la voix chevrotante.

« Il m’a sauté dessus brusquement et m’a saisi au col. J’en suis tombé à la renverse, cherchant à me libérer. J’avais beau me débattre comme diable en eau bénite, il me tenait ferme et s’est mis à postillonner, baver, en maugréant. J’essayais de me défendre, comprenant qu’un démon s’était emparé de lui. Il s’est mis à rire et m’a susurré à l’oreille que le mal devait être purifié par là où on l’avait commis. Selon lui, Ogier était le démon, qui s’était vanté de ses forfaits. Seulement, il avait réveillé un lion là où il ne voyait qu’agneau. »

Il porta alors la main à sa tête et se frotta douloureusement.

« Il m’a ensuite frappé avec un objet dur. Quand je me suis réveillé, tu étais à mes côtés, Ernaut. »

Perrote se signa douloureusement et prononça quelques paroles dévotes.

« Pauvre gamin, rongé par le mal, conclut Godefroy. Je ne saurais prétendre que je savais le cœur d’Ogier si noir, mais j’ai toujours eu certeté qu’il était vil. D’où ma défiance, pas seulement née de ces combines, aussi besogneuses que futiles.

— Qu’allez-vous faire à ce propos ? s’enquit Lambert.

— Je ne sais, tromper ainsi me semble honteux, mais de là à dénoncer…

— Sire Pisan est homme de droiture et il mérite de savoir la vérité, maître Godefroy, indiqua Ernaut. Il nous a été fort amiable tout au long de cette histoire. Si vous ne le faites, je m’en chargerai. J’aurais mauvaise grâce à lui celer pareille menterie en ce jour. »

Le silence accueillit ses paroles et il avala un peu de vin. Il leva le visage vers les fenêtres, soupirant tandis qu’il succombait à la chaleur.

« Je crois qu’il est temps de m’accorder une petite sieste. Pareille bombance demande à dormir dessus pour pouvoir s’apprécier ! »

Il se leva, bientôt accompagné de Iohannes qui prit congé en même temps que lui. Lambert ne les accompagna pas, faisant une longue accolade à son frère avant de se décider à le lâcher, un sourire reconnaissant en guise de salut.

La chaleur dehors était étouffante et les deux hommes eurent l’impression de se trouver de nouveau à respirer dans le khamaseen, bien que le ciel ait été d’un bleu d’azur immaculé. Un chien grogna à leur passage, mais sans quitter l’ombre de son arbre ni seulement lever la tête. Même les vigies les plus consciencieuses savaient que rien de dangereux ne se montrerait en pareille lumière.

Ernaut s’avançait pour remonter vers la maison de Lambert tandis que Iohannes allait prendre le chemin inverse. Ils s’arrêtèrent sous les frondaisons d’un palmier de petite taille, au plus près du tronc pour profiter de son ombre chiche. Le géant tendit une main amicale à son compagnon.

« Mille grâces d’avoir aidé à sauver mon frère, l’ami.

— Nul besoin de mercier. J’ai fait ce que je pensais juste. »

Ernaut esquissa un sourire en demi-teinte.

« Certes, tu me sembles homme à toujours faire pareils choix.

— Que veux-tu dire ?

— Lorsque je suis arrivé la première fois sans les jurés, je n’ai pas remarqué grand désordre, comme l’auraient fait deux hommes s’entrebattant. »

Une lueur d’inquiétude assombrit le visage de Iohannes, toujours silencieux.

« J’ai pourtant bien vu par la suite les bancs au sol et la table poussée, en présence de la Cour. »

Le jeune homme se détourna un instant. Iohannes hésita un long moment, puis l’interpella doucement de son prénom. Ernaut ne le laissa pas continuer.

« Non, ne dis rien. Je sais que tu l’as laissé fuir, car tu pensais bien agir. »

Il fit face à l’interprète, ses yeux clairs emplis de détermination.

« J’ai pour mission de le traquer. Je le ferai, et si je lui mets la main au col, je passerai volontiers le chanvre autour. Il était prêt à laisser mourir mon frère, comprends-tu ?

— Penses-tu donc faire bonne justice ainsi ?

— La justice ? Ernaut ricana. Je laisse cela à Dieu, compère. Je ne suis qu’un homme. »

Il adressa un sourire désabusé à Iohannes. Tout en parlant, ses pensées volèrent au loin, vers la femme qui lui apporterait un peu de paix. Toute son âme avait soif d’elle.
