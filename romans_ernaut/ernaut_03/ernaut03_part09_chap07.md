## Chapitre 7

### Casal de Seylon, mardi 17 juin, début de soirée

La demeure du shaykh de Seylon était très modeste : une seule grande pièce longée d’un côté par des banquettes et par des réserves de nourriture de l’autre. Au fond, une estrade était fermée par un treillis de bois tendu d’une étoffe. Une fenêtre devait ouvrir derrière, car de la lumière en provenait. Pour tout meuble, quelques nattes défraîchies, des coffres. Par contre, un décor géométrique avait été tracé à l’ocre sur les murs chaulés, ce qui donnait un aspect luxueux à l’endroit. Ceci malgré l’odeur piquante du feu de bouse, qui se mêlait aux relents de nourriture.

Le shaykh lui-même était un homme assez grand, aux longues mains. Vêtu de couleurs sombres, il portait un bonnet à motifs. Son visage maigre était barré d’une imposante moustache, qui recouvrait sa bouche et s’étendait jusqu’au bas de sa mâchoire. Les joues creusées, les yeux ridés, il émanait de lui une allure maladive, bien que ses gestes ne soient en rien maladroits. Il invita les arrivants à entrer et fit amener une collation. Une jeune femme, d’une vingtaine d’années se hâta de déposer de quoi boire et quelques olives, du pain et du fromage avant de s’éclipser aussi discrètement qu’elle était apparue depuis le paravent du fond.

Une fois encore, Ernaut dut en passer par les échanges formels d’approche, discussion sur les récoltes, la météo, les conséquences de la tempête. Se faisant passer pour des acheteurs éventuels d’huile, ils eurent droit à une énumération longue et pourtant apparemment non exhaustive des nombreuses qualités de la production locale. Ils furent invités à assister à la cueillette, plus tard dans l’année. La voix du vieil homme était éraillée et donnait à Ernaut envie de se racler la gorge. Il s’exprimait de façon autoritaire et péremptoire, sans que la moindre nuance amicale vienne égayer son visage austère.

Au bout d’un moment, Iohannes estima qu’il était temps de voir s’il acceptait de leur en dire un peu plus sur Ogier, selon le plan qu’ils avaient conçu.

« Nous voyageons beaucoup pour notre négoce, et c’est un ami, Ogier de Saint-Gilles, qui nous a indiqué votre village.

— Iji Sinjil ? Je le connais. Il est parti.

— En effet, nous l’avons appris aussi. C’est pour ça que nous sommes là. »

Le vieil homme lui lança un regard de travers, méfiant. Il avala quelques olives, faisant danser sa moustache en les dévisageant l’un puis l’autre, les sourcils froncés.

« Nous faisons comme lui, devons souvent aller ici et là. Nous empruntons semblables sentiers, travaillons avec les mêmes Bédouins. Les Banu Kamil, peut-être les connaissez-vous ? »

À cette mention, le shaykh eut une lueur dans le regard et on aurait dit qu’un fugace sourire de malice se tapissait sous ses bacchantes. Il gardait néanmoins toujours le silence.

« Comme Ogier est parti, nous pourrions nous charger de son commerce avec vous.

— Pourquoi feriez-vous cela ? C’est bien dangereux négoce, d’aller par les chemins, avec les soldats.

— Moi pour honorer les miens. Mon compagnon fait ça parce qu’il pense que c’est un commerce fructueux. »

Le vieil homme hocha la tête doucement.

« Vous ne transporteriez pas que de l’huile ?

— Certes pas.

— Pourquoi venir me voir pour cela ? »

Lorsque Iohannes lui traduisit, Ernaut hésita. Il ne savait pas si la nouvelle de la mort d’Ogier était parvenue jusqu’ici. Il préféra ne pas prendre de risque et ne pas faire allusion à l’événement.

« Ogier nous a dit qu’il faisait bon commerce avec vous, mais n’est plus là. Il demeure peut-être encore ouvrage à faire.

— C’est vrai, cette terre est maudite, souillée, et ne convient plus. »

Il fit une grimace, et marmonna quelques mots à voix basse avant de leur faire face de nouveau.

« Je ferai ce que je peux, comme pour Iji Sinjil.

— C’est vous qui prépariez tout ?

— C’était mon rôle. Je ne crains pas la main de l’homme qui se dit mon maître, car je n’ai qu’un seul vrai maître, et il me comble de ses bienfaits. J’ai donc aidé Iji à plusieurs reprises déjà, et cela m’agrée de pouvoir le faire de nouvel. »

Il se tut, mordant sa moustache tandis qu’il réfléchissait. Puis il regarda alternativement les deux visiteurs.

« Combien cela coûterait avec vous ?

— Pas plus qu’Ogier, rassurez-vous. Avait-il coutume de demander beaucoup ?

— Il ne faisait pas ça par charité et exigeait la moitié au départ. Un homme dur en affaires, ça oui. Et vous, quel serait votre prix ? »

Ernaut n’avait pas prévu une question si directe et il n’avait aucune idée de la somme qu’il pouvait demander pour être crédible. Il tenta d’atermoyer.

« Cela dépend du trajet. La dernière fois, d’où êtes-vous partis, et jusqu’où faut-il se rendre ?

— Nous avons retrouvé Iji près d’un oued, à l’est du village. Il se rendait au nord, là où les Banu Kamil le retrouvaient, pour ensuite finir le voyage, à Dimashq.

— Il y avait beaucoup de… Combien de personnes retrouvaient Ogier ? Car cela aussi est important.

— D’usage trois ou quatre, une fois cinq. Jamais plus. »

Le shaykh commençait à répondre avec une certaine réserve, comme s’il sentait que quelque chose n’allait pas.

« Quel serait le prix alors ? » insista-t-il.

Les deux compagnons échangèrent des regards, indécis. Ce fut Ernaut qui brisa le silence.

« Il nous faut vérifier si le chemin que nous souhaitions prendre est sûr avec autant de personnes. Nous croyions qu’il n’y en avait qu’une ou deux.

— Nous ferons comme cela vous convient » indiqua le vieil homme.

Il se tourna vers Iohannes et le scruta d’un regard inquisiteur.

« Les tiens sont des Banû al-Hanbalî, des Banû Qudâma ?

— Non. Ma mère seule était de ces terres. Elle n’est plus là, mais elle a eu le temps de m’enseigner du mieux qu’elle le pouvait.

— C’est bien. Il existe tant de menteurs par le monde. Il est bon de savoir que certains savent vivre selon des préceptes non souillés par les inventions et les élucubrations de pseudo savants. Nul besoin d’inventer, tout est déjà écrit pour qui sait lire. »

Il hocha la tête, se confirmant à lui-même sa propre opinion puis avala un peu de leben[^leben].

« J’attendrai un signe de votre part pour le prochain voyage. Nous nous tiendrons prêts si l’affaire se fait. »

Il s’arrêta comme s’il n’était pas certain de vouloir continuer sa phrase et se gratta le cou avant de le faire.

« Il est dur de donner sa confiance, d’autant plus difficile que nous ne nous connaissons pas encore.

— C’est la raison pour laquelle il nous faut bien penser à tout et faire les choses correctement.

— C’est vrai. Iji n’était pas très vertueux. Même s’il était resté, nous n’aurions plus fait affaire. »

Ernaut bougea légèrement, se penchant en avant, soudain intéressé par ce que le vieil homme avait à dire.

« Nous sommes loin de nos frères et sœurs, mais des messages nous en viennent, par des voyageurs, des pèlerins, des marchands. Tout finit par se savoir.

— Que voulez-vous dire ? demanda Ernaut, les sourcils froncés, fébrile à l’idée que le shaykh était peut-être en train d’avouer le crime.

— Parfois certains voyageurs passent et nous apprennent que le voyage ne s’est pas terminé à l’endroit prévu. »

Il leur adressait un regard lourd de sens, ses yeux noirs fouillaient leur âme tandis qu’il les examinait l’un après l’autre.

Ernaut voulait entendre la confirmation de ce qu’il pressentait.

« Ogier vous a trahi ? Et vous l’avez puni pour cela ? »

Le vieil homme s’esclaffa.

« Non, je ne suis pas celui qui a frappé ce larron. Je laisse la justice à Allah, qui sait frapper vite et justement, comme vous pouvez le voir. »

Il était donc au courant du meurtre et le pensait lié à ce dernier voyage tragique.

« C’était pourtant bien une main d’homme qui a tenu l’arme.

— La volonté d’Allah s’incarnait en lui. La justice pour avoir menti et trahi.

— Qu’a-t-il fait pour ainsi mériter si terrible châtiment ?

— Allah seul le sait ! »

Ernaut secoua la tête, marquant son désaccord.

« Le murdrier le sait bien aussi.

— Il te faudra alors découvrir qui il est pour le lui demander. Moi je n’en sais rien. »

Ernaut croisa les bras, agacé de ne pas aboutir alors qu’il avait entrevu la solution. Le shaykh demeurait impassible, avalant une olive ou un morceau de pain avec lenteur, comme indifférent à leur présence à tous les deux. Il demeurait un point mystérieux, et Ernaut voulut en avoir le cœur net.

« N’avez-vous pas crainte d’être de nouveau trahi ? »

Le vieil homme remua sa moustache tandis qu’il faisait jouer sa langue sur ses dents. Ses yeux voyaient quelque chose au-delà d’Ernaut, au-delà de la porte et peut-être par-delà les montagnes environnantes. Il répondit d’une voix pleine d’assurance.

« Il n’est nul besoin d’avoir confiance pour faire affaire. La nécessité suffit parfois. Il est plus important de vivre parmi ses frères, de s’assurer que ses fils puissent étudier les hadiths avec de bons maîtres. La foi en Allah seule peut nous sauver.

— Vous êtes prêts à prendre n’importe quels risques, vous et les vôtres ? À quitter terres et foyers sans assurance ?

— La terre nourrit mon corps, pas mon âme. Mon foyer est partout, car Allah est mon guide. Vous accordez trop d’importance à vos biens, vous autres polythéistes. Vous adorez des idoles et ne pensez qu’à posséder les choses. »

Il échappa un petit rire méprisant, toisant Ernaut avec un dédain qui s’affichait désormais sans crainte.

« Si Iji Sinjil a trahi, c’était qu’Allah le voulait ainsi et il me met à l’épreuve. Je ne suis pas tel ce voyageur empressé qui cherchait après les siens, ou comme toi, à espérer des réponses à des questions sans intérêt. Le Prophète, qu’Allah l’ait en Sa sainte garde, a donné toutes les réponses, avec ses Compagnons. Je n’en espère aucune autre. Tu me proposes un service que j’espérais, pour le salut des miens, je l’accepte et en paierai le prix. Cela ne fait pas de nous des amis. »

Il conclut sa courte allocution en redressant le torse, les bras croisés. L’entretien était clairement terminé. Lorsqu’ils se levèrent, il les salua poliment, mais sans chaleur et ne se leva que pour fermer la porte derrière eux.

Ils se retrouvèrent dans la rue pentue, bousculés par les chèvres que menaient deux garçonnets d’une dizaine d’années. Le regard fier, ils brandissaient des bâtons avec morgue, habillés de vêtements usés et reprisés, les pieds nus dans la poussière et les graviers.

Iohannes soupira et murmura.

« Prêts à tout pour s’enfuir du royaume. Même au prix de la vie de quelques-uns d’entre eux.

— Je ne suis pas si sûr que toi de cela, compère. As-tu pris garde à ce qu’il a dit ?

— À quoi penses-tu ?

— Il a fait allusion à un voyageur qui voulait en savoir plus sur ceux qui se sont perdus dans ce dernier convoi. Lui au moins attachait de l’importance aux siens. »

Il regarda aux alentours.

« Si nous pouvions marcher dans ses traces, peut-être pourrions-nous apprendre ce qui s’est passé, le débusquer. Voire trouver en sa possession la lame qui a tué. »

### Casal de Seylon, mardi 17 juin, soirée

Ils menaient leurs chevaux par la bride en direction de la sortie lorsqu’ils entendirent des appels discrets, comme des sifflements légers. Tournant la tête en tous sens, ils aperçurent une silhouette dans l’encoignure d’une ruelle. En s’approchant, ils découvrirent une jeune femme, tremblante, le visage caché derrière un voile. Sa mince silhouette était drapée dans d’amples vêtements qui ne laissaient rien voir d’elle, dont finalement seuls les yeux bruns, larges et doux, indiquaient que c’était une femme. Elle les aborda à mi-voix, et Iohannes entreprit de traduire aussitôt à Ernaut.

« Faites comme si vous regardiez les jambes de vos montures, s’il vous plait. On ne doit pas me voir avec vous. »

Ernaut obtempéra, s’affairant autour de la selle et des sangles comme s’il avait un souci avec. Dans l’ombre, la jeune femme continua.

« Je n’ai pas beaucoup de temps, je n’ai pas le droit de demeurer dehors seule. Je voulais savoir si j’ai bien entendu, si vous emmenez les gens loin d’ici ? »

Ernaut se mordit la lèvre. Il hésitait. Mentir à un vieux shaykh amer était une chose, trahir les espoirs d’une jeune femme emplie de désarroi lui parut moins aisé.

« En quoi cela te regarde ?

— J’aimerais tant m’enfuir aussi, quitter ce village maudit !

— Ce n’est pas si simple…

— Je sais bien ! Sinon je l’aurais déjà fait, comme Safiya. Je peux vous donner des bijoux si vous voulez. »

Iohannes se planta contre la façade d’une maison, flattant l’encolure de son cheval tout en faisant mine d’en arranger le filet.

« Quel est ton nom ?

— Je suis… Munya, répondit-elle, presque à regret.

— Où veux-tu aller ? »

— N’importe où ! »

Elle échappa un petit cri de désespoir et semblait sangloter, la voix soudain chevrotante. Iohannes fit mine de s’avancer, instinctivement, mais elle recula, effrayée. Elle le dévisagea.

« Où vous menez les gens comme nous, d’habitude ? »

Ernaut jeta un coup d’œil interrogateur à Iohannes et répondit, sans trop y réfléchir.

« Damas.

— C’est là-bas que Safiya est allée aussi ?

— Qui est Safiya ?

— C’est mon amie, elle est partie il y a quelques mois.

— Je ne sais pas. Sûrement. C’est là-bas que vous partez tous, non ? »

La jeune femme se rapprocha, sa voix n’étant plus qu’un filet à peine audible.

« Tout le monde l’a cru, mais elle partait pour vivre sa vie.

— Elle est partie seule ?

— Non, bien sûr que non. Son père lui avait arrangé un mariage à Dimashq. Un frère et un cousin l’accompagnaient. Ce n’était pas vous qui les guidiez alors ? »

Ernaut fronça les sourcils et se tourna, oubliant de jouer la comédie auprès de son cheval.

« Non, un autre Franc s’occupait d’eux. »

La jeune femme baissa la tête, déçue.

« Vous ne pourrez lui porter un message alors.

— Si elle est à Damas avec son nouvel époux, tu devrais arriver à avoir de ses nouvelles.

— Cela m’étonnerait qu’elle y soit, répondit-elle, les yeux soudain emplis d’espoir.

— Pourquoi donc ?

— Elle m’avait confié qu’en réalité elle allait vivre avec celui que son cœur avait choisi.

— Ce n’était pas son promis ? »

La jeune fille secoua la tête avec vigueur, mais sembla hésiter à répondre, effrayée de ce qu’elle allait dire.

« Non. Elle aimait un des vôtres, un Ifranj, et il devait l’épouser. »

Le visage de Iohannes trahit sa surprise. Son expression indiqua à Ernaut l’énormité de l’affirmation, bien plus encore qu’il ne s’en doutait déjà.

« Comment sais-tu ça ?

— Elle me l’a dit. Il voulait la sauver d’ici, la sauver de son mariage et l’emmener loin. Il a réussi, je le sais, car elle n’est pas arrivée où elle le devait. »

Ernaut repensa aux différents trajets qu’avait effectués Ogier. Il savait qu’après le dernier, il avait revendu des vêtements de femme. Mais ils portaient du sang, ainsi que ceux des hommes.

« Comment l’as-tu appris ?

— Un de ses cousins est venu, ils ne les ont pas vus alors ils s’inquiétaient. Ils auraient dû arriver avant le début de la nouvelle année. Ils étaient partis vers la fin de dhou al qi’da^[Mois du calendrier musulman qui finissait le 27 décembre 1157.].

— Il est passé ici voilà long temps ?

— Un mois à peine, je crois. »

La nervosité la gagnait et sa voix s’étranglait régulièrement.

« J’aimerais aussi m’enfuir, comme elle. Vous ne voulez pas m’emmener ? »

Iohannes soupira, lançant un regard consterné à Ernaut. Celui-ci secoua la tête. Il repensa aux prostituées qu’ils avaient vues avec les soldats croisés, quelques jours plus tôt.

« Si ton amie s’est enfuie, c’est parce qu’elle avait quelqu’un avec qui le faire. Où irais-tu, seule, sans personne pour veiller sur toi ?

— L’un de vous ne me veut pas ? Je suis bonne cuisinière et très obéissante. »

L’entretien commençait à devenir pesant et Iohannes manifestait de plus en plus de désapprobation. La jeune femme était appuyée contre le mur, respirant à grands traits, sanglotant.

« Dis-moi encore une chose, sais-tu le nom de celui avec qui Safiya a disparu ? risqua Ernaut.

— Non. Elle ne voulait pas que le sache, elle craignait qu’on m’oblige à le révéler » glissa-t-elle dans un murmure.

Ernaut retint à grand-peine le juron qui lui était venu naturellement aux lèvres. Il serra les poings sans rien trouver sur quoi les asséner. Munya ajouta d’une voix à peine audible.

« J’ai porté un message une fois, pour lui.

— Tu l’as donc vu ? À quoi ressemblait-il ?

— Non, je ne l’ai pas vu. J’ai remis le message au jeune Ifranj que Safiya m’avait indiqué. Il m’attendait à un endroit convenu.

— Et lui ? Son nom ?

— Je ne sais pas trop… Vous dîtes Khan Ber ?

— Umbert ?

— Oui, un jeune, du casal ifranj voisin. »

Cette fois, le blasphème éclata, et Ernaut grimaça de colère. Le jeune valet continuait donc à mentir et tricher. Iohannes, pour sa part, était penché vers Munya, désormais accroupie au sol, le dos au mur. Elle hoquetait dans ses pleurs. Il lui parla doucement un petit moment, sans plus traduire pour Ernaut, et elle finit par se lever, essuyant ses larmes de sa manche.

Lorsqu’elle disparut dans la ruelle rongée de ténèbres, il se retourna vers Ernaut, l’air piteux.

« Quel espoir pour les humbles ? »

Il semblait affecté par la situation, mais ne s’étendit pas. Après avoir rapidement ressanglé, il monta en selle et s’affaira à régler ses rênes. Tout ce temps, Ernaut l’avait observé, remâchant sa colère. Il s’installa à son tour sur sa monture, la dirigeant avec brusquerie sans même y prêter attention.

Ils quittèrent le casal en silence. Lorsqu’ils dépassèrent un groupe de travailleurs, les bras chargés de leurs outils, Ernaut lança son cheval au galop, dans un nuage de poussière et de graviers. Le vent qui sifflait à ses oreilles le calmait et la sensation grisante de vitesse lui fit du bien. Il ne réfléchissait pas vraiment, se contentant de ressasser ce qu’il avait appris.

Le village était empli de secrets imbéciles, qui lui cachaient la vérité, la seule qui aurait pu sauver Lambert. Chacun gardait pour lui ses petites histoires, ses secrets, comme dans tous les hameaux. Et lorsqu’une tragédie éclatait, telle que l’assassinat d’Ogier, chacun s’effrayait de ce qui pouvait le relier à l’événement, mentant alors comme si sa vie en dépendait. Quand bien même cela n’avait rien à voir.

Si Ogier avait trahi les musulmans de Seylon et avait laissé la jeune fille s’enfuir avec son amoureux, il avait sûrement demandé à être payé pour cela. Ernaut n’osait envisager que c’était lui que la jeune femme ait pu aimer. Pourtant, Munya était prête à fuir avec n’importe qui, tellement elle était désireuse de quitter son casal. Safiya avait-elle fait la même proposition au passeur ? Dans ce cas, qu’était-elle devenue ? Aurait-elle empêché le mariage fructueux avec la fille d’Aymeric ?

Serait-elle allée jusqu’à le tuer et l’émasculer pour se venger de ses vaines promesses ? Terrible geste d’une jeune femme abusée, trompée, bafouée ? Toutes les hypothèses défilaient dans sa tête sans qu’il n’en privilégie aucune. Il les laissait aller et venir comme l’air qui cinglait sur ses joues, sans chercher à les ordonner, les classer, les analyser. Il sentait les muscles de sa monture qui se bandaient à chaque foulée, au moindre saut. Il laissait son être se dissoudre dans l’instant, fatigué d’avoir couru les derniers jours, las de ne rien tenir de concret à présenter pour sauver son frère. Il voulait que cette course n’en finisse plus, qu’il galope à tout jamais, en quête des dernières lueurs du jour, loin à l’ouest. Le soleil mourant incendiait la frange outremer du ciel, n’abandonnait la voûte aux étoiles qu’à regret, après y avoir allumé un foyer rougeoyant.

Les silhouettes du casal de Saint-Gilles apparurent bientôt, et Iohannes interpella Ernaut, l’invitant à réduire l’allure. Il fallut un petit moment avant que ce dernier n’obtempère.

« Nous n’irons guère outre, compère. La nuit est sur nous. »

Ernaut le dévisagea, interdit. Iohannes semblait moins irrité que lui, plus las que déçu.

« L’église de Saint-Gilles accueille les pèlerins venus de Naplouse. Faisons halte là, à la fortune des clercs. Inutile de risquer le voyage de nuit.

— Il nous faut être tôt à la Mahomerie si la bataille est demain. »

Iohannes secoua la tête, secouant la main en déni.

« Quoi qu’il arrive, ils ne sauraient faire combat avant le mitan du jour. Sinon le soleil pourrait avantager l’un d’eux. Laissons les chevaux reprendre des forces.

— Tu parles de raison, compère » reconnut le géant, ressentant soudain la fatigue. Il s’emmura de nouveau dans le silence tandis qu’ils abordaient le village. Un groupe de voyageurs, la mine fatiguée, les vêtements poudreux, était en train de se rafraîchir à l’auge emplie d’eau devant l’église quand ils y parvinrent. Ayant mis pied à terre, caressant son cheval transpirant, Ernaut s’approcha de Iohannes.

« Nous profiterons de la veillée pour aller faire visite à quelques habitants du casal. J’aimerais fort savoir à qui Umbert portait messages ou m’en faire une idée avant d’aller le visiter.

— Tu as crainte qu’il ne mente ?

— N’est-ce pas ce qu’il fait depuis le début ? Ce qu’ils font tous ? Je n’ai jamais vu un tel ramassis de menteurs ! »

Le jeune homme savait qu’il exagérait, mais son exaspération le rendait irritable et il lui fallait l’exprimer. La désapprobation qu’il lisait dans les yeux de Iohannes, qu’il savait légitime, ne faisait que renforcer ce sentiment. Il se sentait inefficace, alors que sa tâche était essentielle. Pour une fois, sa curiosité n’était pas en cause et il ne comprenait pas pourquoi tout semblait se liguer contre lui.

### Casal de Saint-Gilles, mardi 17 juin, soirée

La salle d’accueil des voyageurs était de bonne taille, voûtée d’arêtes blanchies à la chaux, comme les murs. Seul décor, un crucifix ornait une niche en bout de salle, avec une bougie au-devant. De longues tables, flanquées de banc, étaient installées perpendiculairement à l’estrade où étaient installés les clercs. Seuls deux étaient assis, avalant la soupe épaisse en de grandes lampées, tandis que les autres supervisaient la distribution de nourriture. En bout de ce podium, un jeune homme empreint de sérieux, debout devant un lutrin, lisait un texte en latin que personne ne comprenait, peut-être pas même lui, à en juger par son phrasé hésitant et monocorde. Bien qu’il s’efforçât à appeler au silence régulièrement, un brouhaha diffus régnait dans l’endroit.

Ernaut et Iohannes s’étaient retrouvés à table à côté de pèlerins de retour de Nazareth. Ils étaient originaires de la région de Lyon, parlaient fort et riaient d’un rien. Leur joie touchait à la fébrilité. Ils racontaient avec empressement qu’ils espéraient bientôt pouvoir embrasser le tombeau du Christ, y prier pour l’âme des leurs. Ce regain d’optimisme et d’enthousiasme eut un effet salutaire sur les deux compagnons, qui se sentirent ragaillardis par la foi ambiante. Ernaut arrêta un des frères qui distribuait le pain et l’interrogea pour savoir s’il existait une taverne, ou du moins un lieu où les hommes d’ici se retrouvaient le soir. Le moine lança un regard anxieux en direction de l’estrade puis, une fois assuré qu’on ne le regardait pas, se pencha et confia à voix basse :

« Si vous descendez la rue à senestre en sortant, prenez tierce chemin que vous encontrerez. Le vieux Clarembaud ouvre souvent ses caves le soir, pour les hommes qui veulent s’entreparler, voire faire rouler les dés. »

Il avait ajouté la dernière phrase avec un gloussement appréciateur. Ernaut le remercia d’un sourire entendu. Le repas fini, ils se levèrent de table et suivirent les indications. À l’est le ciel se faisait écrin de velours pour les étoiles de diamant. Quelques passereaux chantaient en voletant en groupe. Des cris d’enfant, les pleurs d’un bébé, les cancans de commères affairées à échanger les derniers potins résonnaient entre les murs des cours.

Ils trouvèrent aisément la maison faisant office de salle publique. Plusieurs hommes discutaient, assis sur des bancs placés sous un palmier, devant la porte grande ouverte d’un cellier. Un vieil homme racontait une histoire à une assemblée d’enfants et de femmes, installés autour d’un rocher grossièrement taillé. Sa voix déroulait les récits avec chaleur, ses mains mimant les attitudes des héros tandis que son visage aux traits mobiles en dépeignait la moindre expression. Ernaut reconnut immédiatement le vieil homme qu’ils avaient croisé chez le forgeron.

Ils furent accueillis par un barbu à l’air affable, au corps gras et tordu, Clarembaud. Son bras droit était plié bizarrement et sa main difforme ne comportait pas plus de trois doigts. Il leur servit un pichet d’un vin rouge assez clair, rafraîchissant et suffisamment sucré au goût d’Ernaut.

Voyant qu’ils n’étaient pas du casal, il s’efforça de leur faire la conversation, qui débuta immanquablement sur la tempête. Vint ensuite l’inévitable litanie sur les espoirs en la récolte à venir, puis le couplet à propos des activités des forces damascènes, trop proches pour être jamais bien loin des pensées. Entre-temps, leur hôte allait remplir des pichets qu’on lui portait ou resservait ceux qui l’appelaient. Lorsqu’Ernaut aborda le sujet d’Ogier, le vieil homme fronça le visage.

« Il était pas simple, celui-là, si vous m’en croyez. Vous êtes compères ?

— Il réside désormais dans mon casal, confirma Iohannes.

— Ah, j’espère qu’il saura se montrer plus tranquille qu’ici. Pas le mauvais gars, mais souventes fois à finasser. Passez-lui donc salut de ma part. C’est toujours bon d’avoir des nouvelles de chez soi. »

Iohannes sourit en réponse, sans rien ajouter. Ernaut avait quelques scrupules à maintenir Clarembaud dans le flou. Il précisa donc d’une voix neutre :

« Il est malheureusement passé… Voilà un peu plus d’une semaine.

— Ça alors ! Il était pas si vieux pourtant ! Quelque maladie ?

— Non, un de ses ennemis l’a occis par lame.

— Ça par exemple ! » répéta alors en boucle le vieil homme, s’asseyant face à eux sous le coup de la surprise.

Ernaut lui proposa de son vin, qu’il accepta volontiers, avalant une grande rasade.

« Vous étiez fort amis ?

— Non, certes pas. Mais il était d’ici, en un certain sens. »

Sa voix mourut, il resta un moment à regarder le sol, traçant dans la poussière des dessins ésotériques du bout de la semelle. Ernaut respecta cet instant de silence, examinant les autres consommateurs. Le forgeron avait rejoint le petit groupe d’hommes qui discutaient sous l’arbre. Ils s’entretenaient apparemment de questions agricoles, sans prêter attention à lui. Il revint à Clarembaud.

« Il risque d’y avoir jugement par bataille et j’essaie d’empêcher cela. Il est fort possible que ce soit un mahométan qui ait fait le coup.

— Un de ces pillards turcs ?

— Difficile à dire. »

Le vieil homme secoua la tête avec lassitude.

« Il se mêlait trop à eux, je lui avais dit un jour que ça lui attirerait des ennuis. Sans compter qu’il entrainait le gamin.

— Umbert ? »

Clarembaud soupira, plissant les yeux tandis qu’il faisait appel à ses souvenirs.

« Ogier le considérait tel un fils. Enfin, il le traitait bien, quoi. Le gamin a fini par faire comme lui avec les païens d’à côté.

— C’étaient les seuls à les visiter ?

— Oh non ! contesta-t-il, le visage étonné. Il nous faut bien les voir de temps à autre pour régler des soucis de pâture. Et pis les gamins aussi, ils se mettent de ces peignées !

— Ils se battent ?

— Les garçons, oui. Ils s’affrontent comme au temps du roi Godefroy. Il faut les voir défiler quand ils montent à l’assaut. Et plus encore quand ils ramènent quelques trophées, chaussures ou ceintures. »

Ernaut interrogea silencieusement Iohannes d’un regard étonné. Mais ce dernier se contenta de hausser les épaules.

« Le jeune Umbert en était ?

— Dame non! Il était pas comme les autres gamins d’ici ! Même si ça leur dure pas, en général. »

Ernaut sentit une hésitation dans l’affirmation. Il se pencha en avant, les coudes sur les genoux, dévisageant Clarembaud.

« Ça a été différent pour lui ?

— Il faut dire que son père a quelques cultures en limite du terroir, vers chez les païens. »

Clarembaud se tut un instant, réfléchissant à quelque chose qui venait de lui apparaître.

« C’est peut-être pour ça qu’Ogier a loué le gamin, remarquez. Vu qu’il traitait pas mal avec eux, il a préféré un valet qui ne voyait pas de souci à se trouver en leur présence. »

Il baissa la voix, se rapprochant d’eux.

« Il faut dire que d’aucuns sont guère tendres avec eux et seraient prêts à les écorcher tout vifs juste pour le plaisir. J’ai entendu les hommes du comte de Flandre quand ils sont passés. Je n’aimerais guère voir ce qu’ils feraient à un village de païens. C’est qu’il y a des femmes et des enfants, quand même. Ils sont pas comme nous, mais c’est pas raison pour leur faire si grand mal. Tant qu’ils restent à leur place, moi je dis qu’il faut les laisser tranquilles. Pas vrai ? »

Ernaut hocha la tête, peu désireux de polémiquer et assez d’accord en général. Iohannes acquiesça avec plus de réticence, se réfugiant dans son verre dont il avala quelques gorgées. Clarembaud perçut l’hésitation et fit une grimace, légèrement vexé. Il continua donc.

« Je suis acertainé qu’avec quelques clercs habiles au verbe, il s’en baptiserait pas mal. À partir de là, ils paieraient la dîme tels bons chrétiens et prieraient la Vierge comme nous autres. »

Ernaut n’avait guère envie de voir la discussion aborder des sujets politiques. Sans vraiment écouter, il laissait le vieil homme débiter ses idées sur la façon dont le royaume devait être dirigé, les musulmans convertis et les Turcs combattus.

Le mince croissant de lune n’éclairait que fort peu, des lanternes et des lampes avaient donc été allumées, habillant d’ambre les murs et les personnes. Réalisant que son interlocuteur avait fini par se taire, Ernaut confirma d’un « Certes ! » empli de conviction, sans qu’il ait la moindre idée de ce à quoi il s’appliquait.

La soirée retombait. Les contes étaient finis et les jeunes spectateurs se levaient, revivant en gestes et en paroles les hauts faits des héros dont on venait de leur bercer les oreilles. Clarembaud récupéra ses céramiques, salua quelques personnes, serra des mains. Il ne resta bientôt plus qu’eux trois.

« Il est temps de vous laisser, l’ancien, en vous merciant du bon vin et de l’amiable compagnie.

— On a toujours plaisir à échanger nouvelles avec les étrangers, surtout ceux d’ici. »

Ils saluèrent cordialement le vieil homme et reprirent la direction de l’hôpital. La soirée était belle, pas un souffle de vent, et la température clémente sans être étouffante. L’agacement et la chaleur de la journée semblaient loin derrière eux. Ils déambulèrent un moment tranquillement, perdus dans leurs pensées, prenant leur temps avant d’aller se coucher.

Lorsqu’ils parvinrent devant le bâtiment, ils en découvrirent la porte gardée par un énorme mâtin au poil ras, qui dormait en gémissant. Il sursauta à leur approche et leur renifla la main en la léchant, sans se pousser pour autant. Ernaut se pencha afin de caresser l’animal qui n’en demandait pas tant, se mettant à battre la mesure de la queue.

« J’aurais eu espoir d’en apprendre plus. Le vieux était bavard, mais guère utile.

— Peut-être devrions-nous visiter Aubert au matin, demain, avant de s’en retourner. Nous pourrons jouer franc-jeu avec lui, sans devoir camoufler nos intentions.

— En ce cas, il nous faut nous lever tôt. Je veux être sûr d’arriver à la Mahomerie avant le mitan du jour. »

Iohannes confirma d’un signe de tête.

« Ils ne suivent pas les heures en ce lieu, mais les valets doivent se lever tôt, comme les pèlerins. Nous pouvons leur demander de nous éveiller. »

Il enjamba le gardien peu farouche des lieux et prit à droite l’escalier qui menait à l’étage, vers le dortoir. Des ronflements l’accueillirent.  La lueur de la veilleuse, il rejoignit le lit qu’on lui avait attribué. Il ôta ses souliers, s’allongea sur le matelas de paille, la couverture roulée en boule sous la tête. Quelques respirations plus tard, il était endormi.

Dehors, accroupi à côté de l’animal qu’il continuait à flatter, Ernaut cherchait des tracés pour relier les étoiles dans le ciel. Il savait que certaines constellations avaient une forme précise, un nom attribué. Il y voyait pourtant tout ce que sa fantaisie lui dictait. Pour l’heure, il s’efforçait d’y tracer le visage de Libourc.

### Casal de Saint-Gilles, mercredi 18 juin, tôt le matin

Le soleil avait pointé depuis peu qu’Ernaut et Iohannes parcouraient déjà les rues de Saint-Gilles pour se rendre chez Aubert. La mine fatiguée, les membres las d’avoir chevauché sans répit depuis des jours, le corps raide des coups donnés et reçus, ils avançaient d’un pas traînant, leur monture tenue à la bride. Ils découvrirent le vieil homme dans sa cour, occupé à donner du grain aux volailles. En les apercevant, il leva la main et leur sourit, avant de les rejoindre.

« Vous voilà bien tôt en mon hostel. Vous n’avez pas chevauché de nuit aux fins de porter nouvelles jusqu’ici, j’espère ? Ce ne serait pas bon signe.

— Nous avons dormi à l’hospital » le détrompa Ernaut.

Aubert était vêtu seulement de sa chemise et de ses braies, ses souliers enfilés comme des savates, un curieux bonnet à peine enfoncé sur la tête. Ils le prenaient visiblement au saut du lit.

« Si vous patientez un peu, nous pourrons rompre le jeûne ensemble. Le temps que le feu se fasse, on pourra cuire quelques œufs. »

Tout en parlant, il présenta sa récolte du matin dans un panier de paille.

« Grand merci à vous, maître Aubert, mais nous devons nous mettre en route au plus vite. Le jugement pourrait avoir lieu ce mitan du jour.

— Je vois. Juste quelques dernières questions à poser alors ? »

Il sourit, autant pour lui-même que pour ses visiteurs, attendant la première salve. Un peu décontenancé, Ernaut prit une minute à réfléchir sur ce qu’il avait besoin de savoir sur Umbert. En outre, il n’était pas certain de ce qu’il pouvait dévoiler sans risque de ses hypothèses.

« Le jeune valet d’Ogier, votre fils disait qu’il était fort loyal à son maître. C’était votre impression ?

— Dame, oui ! Il a commencé à travailler chez lui tout gamin, sans jamais décevoir.

— Nul motif à se quereller entre eux ? »

Le vieil homme renifla, cherchant dans ses souvenirs.

« Je ne saurai l’affirmer, car il est parfois utile de sermonner son œuvrier. Mais rien de comparable à ce qu’Umbert recevait chez lui. Son père est plutôt du genre coléreux. Le gamin était la cible facile sur laquelle passer ses humeurs. Je ne dis pas qu’il est inutile de faire entendre raison parfois à la marmaille, seulement… »

Il dodelina de la tête, faisant une moue désapprobatrice.

« Le curé s’en est d’ailleurs ému une fois et a dû aller voir le père d’Umbert. C’est de là qu’il a été employé par Ogier. »

Ernaut hocha la tête en assentiment.

« Il a dû se trouver aise d’être placé chez un maître en effet.

— Difficile à dire, c’est un gamin qu’a toujours été secret. Pas le genre à faire des confidences. Il a trouvé là un maître assez juste. Ogier avait des défauts, mais il traitait bien les siens, à ce que j’en ai vu. »

Ernaut passa la main sur ses joues, grattant la barbe naissante qu’il n’avait pas rasée depuis bientôt plus d’une semaine.

« Selon vous, il aurait fait n’importe quoi pour son maître ? »

Aubert fronça les sourcils, alarmé et réticent. Il promena un regard de biais sur Iohannes, un peu en retrait, avant de l’adresser à Ernaut.

« Vous apensez à quoi ? C’est un bon gamin, l’Umbert.

— Est-ce qu’il aurait aidé Ogier en sachant que c’était dangereux ?

— Vous croyez qu’il est en danger ? »

Ernaut se figea. Il n’avait pas pensé à cela. Mais, en effet, si le jeune valet avait participé aux manigances à l’origine de la mort d’Ogier, il était probable qu’il soit également menacé. Il se fit la réflexion, amère, que cela innocenterait définitivement Godefroy et tirerait Lambert d’embarras. Il chassa l’idée, un peu contrit de se laisser aller à de si peu charitables pensées.

« C’est possible, en effet. Ogier a fait quelques entourloupes aux musulmans de Seylon ou à d’autres. Peut-être assez graves pour qu’on veuille lui en faire payer le prix par le sang.

— Cul-Dieu ! Mais qu’est-ce donc de si important ?

— Il leur a peut-être volé une de leurs filles… »

Aubert releva le menton, étonné et un peu indigné. Il ouvrit la bouche, sans savoir quoi dire, puis la referma, toujours aussi ébahi. Il lui fallut un peu de temps avant d’arriver à formaliser une question.

« Je ne peux croire qu’un gars du casal ait pu ainsi prendre une femme, même païenne, pour en faire… »

Il balança la main d’un geste dédaigneux. Sa pensée n’était que trop explicite. Iohannes se rapprocha et répondit d’une voix douce.

« Nous ne savons, maître Aubert. Il est aussi possible qu’il ait songé à l’épouser.

— Marier une païenne ! s’exclama le vieux, dans une nuée de postillons. Quelle folie ! »

L’idée lui paraissait encore plus invraisemblable et il semblait prendre ses deux visiteurs pour des fous échappés de quelque établissement religieux. Il secouait la tête, regardant le sol comme s’il pouvait y trouver de quoi les détromper.

« Il risquerait son âme. Tout le monde sait bien que les évêques ont interdit cela ! De quoi se retrouver frappé d’excommunication ! »

Le mot le fit tellement trembler qu’il esquissa un rapide signe de croix pour se protéger d’avoir osé le prononcer.

« Pourtant, il est possible qu’il en ait eu le désir et il a peut-être quitté le casal pour cela, pas seulement en raison des nouvelles terres de la Mahomerie.

— Il avait une femme là-bas ? Ou une païenne qu’il disait telle ?

— Non, dut en convenir Ernaut, un peu déçu. Juste le valet. D’où mes questions sur lui. Aurait-il poussé la fidélité à Ogier jusqu’à celer à tous que son maître vivait dans le péché ou avait contrevenu aux lois de l’Église ? »

Aubert semblait ennuyé. Sa bonne humeur du matin s’en était allée, et il affichait désormais le poids des ans sur ses épaules. Il souleva son petit bonnet de toile et se gratta le crâne.

« Cela ne paraît pas sot. Avez-vous solides motifs pour apenser pareille chose ?

— On nous a dit qu’il portait messages entre Seylon et ici.

— Possible. Il est vrai qu’il était amené à voir les païens souventes fois, lorsqu’il compagnait Ogier. Il doit en savoir quelques mots. »

Le visage d’Ernaut se crispa. Le valet connaissait peut-être toute l’affaire depuis le début et il n’avait rien dit. Son sang commençait à s’échauffer. Il remercia le vieil Aubert avec chaleur, en s’excusant de l’avoir dérangé si tôt, puis il invita Iohannes à se mettre en selle au plus vite. Lorsqu’ils quittèrent la rue pour s’engager sur le chemin vers le sud, ils lancèrent leur monture à un trot de bonne allure. Ils dépassèrent rapidement deux hommes qui menaient un âne chargé de baliveaux, sans se soucier du nuage de poussière qu’ils provoquaient, s’attirant quelques jurons.

L’air renfrogné, Ernaut semblait déterminé à rentrer au plus vite à la Mahomerie. Iohannes n’eut guère le courage ni l’envie de perturber son compagnon, apparemment perdu dans ses pensées et ressassant sa colère. Il avait déjà pu assister aux embrasements lorsqu’elle avait besoin de s’exprimer et il ne tenait pas à en faire les frais.

Ils s’arrêtèrent à une auge de pierre grossièrement taillée qui recevait l’eau cristalline d’une source. La chaleur commençait à se répandre, étouffant les hommes, les bêtes et les plantes. Ils se frictionnèrent les bras et le visage tout en buvant abondamment. Un peu au-dessus d’eux, dans la colline, des paysans étaient affairés à édifier une restanque. Alentour, de nombreuses parcelles accueillaient des oliviers, des vignes. Sur un des coteaux les plus arides, parmi les broussailles et les buissons, déambulait, épars, un troupeau de chèvres sous la surveillance de gamins à peine plus haut que leurs bêtes.

Les deux cavaliers n’étaient pas encore à mi-parcours et allaient bientôt bifurquer plein sud vers la Tour Baudoin. Ernaut sortit de son sac un vieux quignon de pain sec qu’il brisa en morceaux pour les proposer aux chevaux. Iohannes inspectait les pieds, toujours malmenés sur les chemins caillouteux de Judée. Il jugea le moment opportun pour vérifier les intentions de son compagnon.

« Que veux-tu faire une fois à la Mahomerie, Ernaut ?

— Je ne l’ai pas encore décidé. Tout dépend si la bataille est aujourd’hui ou pas. De prime, il faut s’assurer d’Umbert.

— Tu veux qu’il témoigne de ce qu’il sait ?

— Ce serait le mieux, oui-da. Pourtant je crois qu’il ne le voudra pas. »

Iohannes acquiesça. Si le jeune valet avait voulu parler, il l’aurait déjà fait. Il ne paraissait pas si mauvais garçon et devait avoir un motif bien fort pour se taire ainsi.

« Tu crois comme moi qu’il protège une femme ?

— C’est possible. Ogier n’aurait pu la mener sans que son valet le sache. S’il avoue tout, on va la ramener à Seylon et je ne sais quel sort lui réserveront les siens après pareille traîtrise.

— Il reste fidèle à Ogier par-delà sa mort.

— Je le comprends, mais je ne le laisserai pas faire au prix du sang des miens. Si on lui présente cette épouse, Aymeric sera bien obligé de reconnaître son erreur, et se montrera sûrement moins prompt à vouloir se porter témoin pour un menteur qui l’aura pareillement déshonoré. »

Iohannes toussa, remuant la tête de façon désapprobatrice.

« Cela me paraît tout de même fort étrange, cette histoire de mariage. Si Ogier avait déjà épouse, comment aurait-il pu prendre la fille d’Aymeric ?

— En gardant le secret. Se rapprocher d’Aymeric lui permettait d’espérer bonne fortune. Quel homme refuserait cela ?

— Il serait devenu bigame ?

— Il s’était déjà renié par ses actes. Il pouvait bien faire comme les mahométans, dont on dit qu’ils ont autant de femmes qu’ils le veulent. Peut-être s’était-il secrètement converti, va savoir… »

Iohannes pinça les lèvres, s’apprêtant à répondre, puis détourna le regard et vérifia le dernier pied. Assuré que tout allait bien, il flatta la croupe du cheval et contrôla machinalement les sangles.

« Tu sais, Ernaut, ma mère, elle… Sa voix se brisa. Elle est née sur les bords de l’Euphrate, un fleuve qui coule bien au nord d’ici. »

Ernaut se figea. Il réalisa soudain que son compagnon n’était pas comme lui un franc, mais un métis, et que ses propos acerbes l’avaient peut-être blessé. Comprenant que la présence généralement silencieuse de son traducteur l’avait soutenu ces derniers jours, il fut envahi d’une bouffée de honte. Il remua la tête, le regard désolé, cherchant à exprimer en silence la douleur de ce qu’il comprenait et sa volonté de s’en excuser. Iohannes continua, la voix toujours hésitante.

« Elle était chrétienne d’Arménie, comme la mère du roi. Une femme de bien, dont la voix a rejoint le chœur des anges désormais. »

Ernaut perçut la douleur dans cet aveu et n’osa rien répondre, voyant que son compagnon avait encore des choses à dire.

« Je comprends ton désir d’aider au mieux les tiens, mais ne sois pas trop prompt à juger l’homme qu’était Ogier. Il était peut-être un peu larron sur les bords et menteur envers l’intendant. Quel laboureur ne l’est pas, lorsqu’est venu le temps des redevances ? Peut-être a-t-il aimé sincèrement cette femme de Seylon, nous n’en savons rien. Il nous faudrait savoir si l’idée du mariage est venue de lui ou d’Aymeric, car peut-être s’est-il trouvé en fâcheuse posture, ne pouvant refuser si belles épousailles sans se trahir ni mettre en danger celle qu’il tentait de libérer. »

Ernaut se mordit la lèvre, hésitant à suivre Iohannes dans son raisonnement. Il ne voyait que trop où cela pouvait le mener.

« Nous ne pouvons arriver et proclamer partout ce que nous avons appris. Je te le demande comme une faveur. Voyons d’abord Umbert, tâchons de lui faire narrer ce secret et avisons alors de ce qu’il conviendra de dévoiler à tous.

— Je ne ferai rien qui puisse mettre Lambert en danger, mais dans la mesure du possible, nous ferons comme tu l’as dit. »

Iohannes sourit et lui posa la main sur l’épaule amicalement, soulevant un fin nuage de poussière.

« Tu es un homme de bien, Ernaut, je suis aise de t’avoir encontré. »

### Casal de la Mahomerie, mercredi 18 juin, matinée

Obliquant sur le chemin qui rejoignait le haut du casal, au niveau de l’église, Ernaut et Iohannes convinrent de se renseigner au plus vite à propos du jugement. Cela se déroulerait vraisemblablement dans l’enceinte de la curie voire près de Notre-Dame. Lorsqu’ils arrivèrent en vue de l’édifice, Ernaut soupira d’aise en ne voyant aucun attroupement. Un des convers était occupé à balayer au-dehors la poussière d’ocre que le khamaseen avait déposé dans la nef. Lorsqu’il les vit approcher, il stoppa sa tâche et entra avec empressement, pour ressortir peu après, suivi d’un des hommes de l’intendant. Un peu inquiet qu’on l’ait ainsi attendu, Ernaut approcha son cheval, sans pour autant mettre pied à terre. L’homme vint à lui, agrippant le filet d’une main.

« Le bon jour, le sire intendant doit vous voir au plus vite. Je vais vous conduire près de lui.

— Quelque nouvelle ? Mauvaise ? Bonne ? »

L’homme fit la moue.

« Aucune idée.

— C’est en rapport avec la bataille, elle a été annoncée ?

— Non, pas que je sache. Vous le saurez bien vite. Suivez-moi. »

Et, joignant le geste à la parole, il les accompagna vers le bas du village. Dans la rue, c’était l’effervescence. La tempête enfin calmée, chacun en profitait pour secouer au-dehors tout ce qui pouvait l’être. Certains n’hésitaient pas à faire un véritable ménage de printemps, sortant tous les meubles, le temps de balayer, frotter, nettoyer. L’air sentait bon le savon et l’agitation joyeuse excitait les enfants qui couraient d’une demeure à l’autre, en poussant des cris d’allégresse.

La curie était plus calme. Plusieurs serviteurs déchargeaient une charrette de foin dans un des feniers et l’odeur de l’herbe sèche emplissait l’endroit. L’intendant, occupé à donner des instructions à un muletier qui s’apprêtait à partir, vint au-devant d’eux lorsqu’il les aperçut. Voyant que son visage ne semblait ni inquiet, ni attristé, Ernaut ne s’affola pas. Il cherchait par contre du regard où son frère pouvait se trouver. Comme Iohannes, une fois à terre, il salua poliment le responsable du casal, qui leur sourit amicalement.

« Venez avec moi dans la salle, nous serons plus aise pour entreparler. »

Puis il prit la direction de l’escalier, sans autres formalités. L’endroit ne semblait pas avoir changé depuis la fois précédente, en dehors d’un déplacement des documents sur la grande table et de la présence de reliefs d’un repas. Deux hommes se tenaient là, occupés à relire des lettres et des tablettes, tout en déplaçant des jetons sur un quadrillage tracé à la craie. Ils se levèrent rapidement à leur entrée.

L’un d’eux arborait une toison couleur de feu qui se prolongeait en une superbe barbe. Le crâne se dégarnissait légèrement, mais il ne paraissait pas très âgé. Son regard pétillait de malice tandis qu’il les dévisageait tout en se frictionnant le nez nerveusement. Il était vêtu d’une cotte de travail de bon prix, mais rien n’indiquait qu’il était particulièrement riche en dehors de cela. Il salua, se présentant à Ernaut comme Guillaume Rufus. À côté de lui, suffisamment âgé pour être son père, se tenait Hugues de Saint-Élie, qui salua en second.

Il arborait le dos voûté des hommes ayant trop forcé leur vie durant, les bras pendants, amnésiques de leur force d’antan. Son crâne chauve était blanc, au contraire de son front brûlé par le soleil. On aurait dit qu’il portait un bonnet de peau, décoré ici et là de maigres touffes de poils. Son visage émacié était illuminé par des yeux sombres, quasi enfiévrés. Sa tenue datait de ses meilleures années et il flottait dedans, comme un squelette qu’on aurait vêtu d’une peau trop ample. Ernaut comprit qu’il y avait là une certaine solennité et vit que Iohannes se demandait également ce dont il pouvait s’agir.

L’intendant ne les laissa pas longtemps dans l’incertitude.

« J’ai mission de vous mander à Jérusalem, maître Ernaut. On vous y attend.

— Il y a quelque souci au niveau du royaume ?

— Non. »

Il prit un air désolé.

« Il semblerait qu’on ait besoin de vous aux fins de préparer la bataille. Elle est prévue demain.

— Besoin de moi en la cité ? Pour quelle raison ? Tout se passera ici.

— J’en ai bien connoissance et le mathessep aussi. Je crois qu’il y a eu quelques reproches à votre encontre » lâcha-t-il, à regret.

Le rouquin prit la parole, toisant avec dédain Ernaut et Iohannes.

« Vous allez et venez partout comme bon vous semble, mais il n’est pas utile de remuer ainsi ciel et terre. Cela gêne d’aucuns.

— Vous ai-je porté préjudice, maître ? s’enquit Ernaut, adoptant une posture volontairement impressionnante.

— Non, pas à moi directement, se défendit Simon avec véhémence. Je suis là en tant que membre du conseil du casal. Il y a eu des plaintes.

— La vérité inquiète-t-elle ? »

En demandant cela, Ernaut hésita à lancer un coup d’oeil vers l’intendant. Qui dans la pièce était au courant de la combine d’Ogier à propos des bornes ? Les deux jurés face à lui étaient-ils là pour défendre leurs intérêts propres ou vraiment ceux de la communauté ?

« La Vérité sera connue à l’issue de cette bataille, jeune homme, répondit le vieux Hugues, d’une voix chevrotante. Nul besoin d’aller perturber plus avant cette communauté par vos questions. »

Ernaut se tourna vers Pisan.

« Est-ce à la demande du conseil du casal que je dois partir ?

— La demande la plus vive est apparemment venue du sire de Retest. On se serait plaint de vous auprès de lui, m’a-t-on confié. »

Ernaut échappa une exclamation surprise. Il ne pouvait s’agir que du ra’is, le vieux shaykh n’ayant nul intérêt à contrarier ainsi ceux qui avaient découvert son secret. L’intendant ne releva pas et continua.

« Il vous faut donc prendre le chemin de la cité et vous présenter au plus vite auprès de la sergenterie. Le sire vicomte viendra demain, de nombreuses affaires demandant sa présence en la cité cette fin de semaine. »

Ernaut accusa le coup. Il était mis à l’écart alors même qu’il touchait au but après avoir erré pendant des jours. Il examina les deux jurés, se demandant dans quelle mesure ils avaient joué contre lui. Il se tourna de nouveau face à l’intendant.

« Aurais-je loisir de m’entretenir avec mon frère ?

— Je crains que non, vous avez ordre de chevaucher dès maintenant pour la cité.

— Cela ne sera pas long.

— Je suis désolé, mon garçon, mais ce n’est pas possible. Outre, Lambert est entre les mains du Seigneur, il fait retraite en l’église et ne souffrirait aucune interruption dans ses prières.

— Et Aymeric ?

— Il en est de même pour lui. La bataille demande de se présenter le cœur pur. »

Ernaut hocha la tête, contrarié. L’intendant lui paraissait obéir à des instructions, mais sans prendre grande joie à tout cela. Il avait pour mission de collecter les redevances, de les acheminer à Jérusalem quand on lui en faisait la demande. La justice n’était pas de son ressort. Il l’aurait bien abandonnée à Dieu, mais en l’occurrence, c’était la main du roi qui s’abattait sur les malfaisants. Il n’avait qu’à obéir et le faisait depuis si longtemps qu’il n’y prêtait plus guère attention.

Ernaut n’était pas pareil, il refusait qu’on érige des obstacles sur sa route et poursuivait avec entêtement. Il lui importait peu de savoir qui de lui ou de l’adversité céderait en premier, il n’abandonnerait jamais sans avoir combattu. Les deux villageois ne lui inspiraient que du dépit. Il fixa le vieil homme qui le dévisageait, l’air sentencieux et autoritaire. D’un plissement des paupières, il lui opposa résistance et fit apparaître un sourire si fugitif sur ses lèvres qu’on n’aurait pu jurer de son apparition.

« Il est possible que le sire de Retest prenne quelque plaisir à m’entendre, d’ici peu. Peut-être a-t-on frappé Ogier pour des affaires le concernant. »

Le vieil Hugues comprit immédiatement là où Ernaut voulait en venir et son visage devint livide. Il toussa, attirant l’attention sur lui.

« Je ne crois pas qu’il soit besoin de mêler le sire Robert à une affaire qui ne regarde que Dieu.

— Et si cela nous désigne le murdrier ?

— Dieu nous l’indiquera sans doute aucun demain, jeune homme. J’ai foi en son jugement. Se croire plus avisé que la justice divine conduit à sa propre perte. »

Disant cela, son regard se fit impérieux. Ce n’était pas qu’une sentence moraliste que le vieil homme édictait. C’était un avertissement, voire une menace. Ernaut soutint son regard un instant, prêt à le défier. Il n’en eut pas le temps, car Pisan avait repris la parole.

« Prenez quelques instants pour vous rafraîchir, ainsi que votre monture, maître Ernaut. Vous ferez ensuite route avec le muletier qui attend dehors, il doit justement rejoindre le Saint-Sépulcre pour y porter du grain. Vous aurez ainsi témoin de votre obéissance. »

Il s’assurait également que les instructions seraient suivies et qu’il lui avait transmis le message, songea Ernaut. L’intendant ne pouvait être blâmé pour cela et Ernaut ne souhaitait pas lui causer des ennuis. Il inclina la tête en acquiescement.

« Je vous rends grâce de m’avoir informé de tout cela, sire intendant. Le temps de préparer ma monture et je prends le chemin de la cité. »

Pisan lui sourit.

« Faites-vous donc servir quelque victuaille aux cuisines, que vous ne parcouriez la campagne le ventre creux. »

Ernaut salua une nouvelle fois, reconnaissant. Il ignora les deux jurés du casal et se retira, invitant Iohannes à le suivre. Celui-ci ne le rejoignit que plus tard, alors qu’il étrillait son cheval avant de lui remettre la selle. Il avait eu le temps de se mettre de nouveau en colère et se vengeait sur les objets, brossant la pauvre bête avec vigueur.

« La chiennaille ! Ils craignent pour leurs terres !

— C’est le sire de Retest qui a dû insister, Ernaut. Ce ne sont pas les plaintes de quelques vilains de la Mahomerie qui feraient ployer ainsi Ucs de Monthels, tu le sais comme moi.

— Peuh ! Ça les arrange bien ! Ils peuvent entasser leurs misérables monnaies sous leur traversin ! Qu’ils empilent leurs deniers jusqu’à en avoir trente, et que le Diable les prenne ! »

Iohannes vint l’aider à assujettir la selle, refaisant soigneusement ce qu’Ernaut bâclait.

« Je vais tâcher de voir si je peux m’entretenir avec Aymeric ou ton frère et nous aviserons demain avant la bataille.

— Vois plutôt Umbert, il détient la clef de ce mystère.

— Je le ferai aussi, sois acertainé. Je n’abandonnerai pas. »

Ernaut se calma un peu, rasséréné par les propos apaisants. Il tourna la tête en tous sens avant de demander à Iohannes : « Bon, sinon les cuisines, c’est par où ? »

### Casal de la Mahomerie, mercredi 18 juin, fin de matinée

Ernaut dévorait une bouillie d’orge agrémentée de miel, engouffrant les cuillers avec enthousiasme. Il s’était installé dans un coin de la grande table sur laquelle se préparaient les repas et avait été servi par une servante à l’air revêche qui n’avait pas lâché plus de trois mots. Elle l’ignorait méthodiquement, occupée à préparer des tourtes qu’elle agrémentait de légumes divers. Un jeune allait et venait, s’occupait de maintenir le feu, emportait les récipients pour les récurer, apportait les ingrédients depuis le cellier.

L’endroit sentait un peu la fumée, qui avait coloré de suie le haut des murs et le plafond. Plusieurs coffres et des niches fermées de volets de bois abritaient herbes et aromates, épices et condiments, dont on pouvait sentir de discrets effluves. Son écuelle vidée, Ernaut s’étira le dos, baillant à s’en faire décrocher la mâchoire, échangeant un regard complice avec le jeune serviteur, qui en aurait volontiers fait de même.

Dans la cour commença à résonner le marteau sur l’enclume. Il était temps de se préparer à remonter en selle, songea Ernaut. Il grimaça à l’idée. Il avait le dos en capilotade, les cuisses douloureuses, la nuque raide. Il était sergent, pas chevalier, et passer son temps à chevaucher par les chemins ne lui était pas familier. Il grogna tandis qu’il faisait une nouvelle fois le dos rond, tout en se dirigeant vers la sortie.

Il hésita à saluer la cuisinière, mais cette dernière avait pris soin de ne pouvoir échanger avec lui le moindre regard. Il se contenta d’un salut de la main au jeune valet et quitta l’atmosphère reposante des cuisines pour la cour écrasée de soleil. Il dut plisser les yeux et remarqua alors la silhouette de l’intendant qui venait dans sa direction. Il s’effaça pour lui laisser le passage, mais Pisan s’arrêta, un sourire sur les lèvres.

« Je suis désolé de vous faire renvoyer ainsi qu’un valet, je comprends combien vous souhaitez aider votre frère.

— Grâces vous soient rendues, sire intendant. Je sais bien errer en fort dangereux territoires.

— Le sire de Retest pense-t-il que vous avez désir de l’impliquer ?

— Je ne crois pas. Outre, je n’en ai pas eu l’intention. »

Il se retint d’ajouter que le seigneur de Salomé était, autant que le Saint-Sépulcre, victime dans cette histoire, et qu’il aurait eu beau jeu de laisser Ernaut poursuivre ses investigations. Mais avouer cela revenait à tout dévoiler et risquer de perdre un moyen de pression, alors même que les circonstances devenaient critiques. Il se mordit la lèvre, avant d’ajouter :

« M’est avis que c’est plus le résultat de manigances du ra’is de Salomé. Nous avons dû voir à moult reprises le vieux Pied-Tort.

— Il est certain qu’Abu Qasim n’est pas homme facile.

— Vous le connaissez fort avant ? s’étonna Ernaut.

— Je ne pourrais dire cela. Il m’arrive de le côtoyer lorsque nous avons quelques chartes à ratifier, discussions avec le sire de Retest de droits ou de partages. Il n’est pourtant nul besoin de long temps pour se faire une idée. »

Il sourit avec chaleur, détournant le regard en direction du forgeron qui s’affairait sur son ouvrage de l’autre côté de la cour, dans la pénombre de son atelier. Ernaut l’examina un instant. Il lui fit l’effet d’un homme fatigué, pas tant physiquement, même s’il accusait son âge, mais plutôt accablé par le poids des ans, de la vie qu’il avait dû affronter. Le jeune homme aurait été curieux de savoir ce qu’avait pu être la vie d’un intendant dont la tâche était désormais de collecter les taxes, d’en entreposer le fruit pour ensuite l’envoyer à ses légitimes propriétaires, les chanoines du Saint-Sépulcre.

Le convoi de mules près d’un magasin face à eux finissait de s’organiser. Le responsable était en tain de nouer les liens sur le dernier animal, Ernaut ne pourrait attendre plus longtemps.

« Avant de faire chemin pour Jérusalem, pourrais-je vous entreparler de quelques points dont vous auriez éventuellement la clé ?

— Certes.

— Il est fort possible que le jeune Umbert en sache plus long sur Ogier qu’il ne l’a avoué jusqu’à présent. Le connaissez-vous bien ?

— Je ne saurais dire. Je l’ai vu à plusieurs reprises, dont récemment, avec cette terrible affaire. Que pourrait-il connaître qu’il nous cèle ?

— Ogier avait des ennemis, et Umbert connaît le pourquoi de cela. »

L’intendant hocha la tête doucement, comme pour se faire à l’idée.

« Vous en avez témoin ou c’est juste hypothèse de votre part ?

— On m’a conté certaines choses sur lui, j’aurais voulu en débattre, voir ce qu’il en répondait. »

Pisan croisa les bras, un peu sur la défensive.

« Il est de fait que j’ai entretenu le jeune Umbert de la possibilité pour lui de se faire frère serviteur du Saint-Sépulcre. »

Ernaut écarquilla les yeux.

« Récemment ?

— Oui. Mais l’idée n’est pas neuve. Il m’avait déjà fait demande voilà quelques mois, peu après leur arrivée au casal. Je ne le croyais pas prêt, alors je lui avais conseillé d’attendre. »

Il s’interrompit, le regard se portant soudain au loin, par-delà les murailles, en direction du sud.

« Désormais, tout est changé, avec la mort de son maître…

— Il ne souhaite pas retourner à Saint-Gilles chez les siens ?

— Non, nul désir de cela. »

Il marqua une pause de nouveau, lançant de furtifs regards à Ernaut, comme s’il cherchait à en sonder les intentions ou à en apprécier l’âme.

« J’en avais parlé avec le prêtre, bien sûr, pour savoir ce qu’il pensait. Il m’en a dépeint un fort loyal portrait. S’il cache certaines choses sur Ogier, c’est soit par fidélité, soit parce qu’il sait que les dévoiler ferait plus de mal que de bien.

— Tout de même, sire intendant, il en va de la vie des hommes qui vont s’affronter en champ clos demain.

— Peut-être, ou peut-être pas. »

Ernaut commençait à s’emporter, craignant que l’intendant n’ait eu aussi des informations cruciales en sa possession et ne se gardât de les partager.

« Pour se mettre à l’épreuve devant Dieu, je suis bien aise de savoir qu’il faut avoir des intentions pures. Mais il me paraît fort grave de tromper ceux qui mettent ainsi leur âme sur la balance en leur cachant la vérité.

— Tout cela n’est pas affaire des hommes, jeune Ernaut.

— Quoi donc ? La vérité ? Nous ne devons pas nous en soucier ?

— Je n’ai rien dit de tel. Parfois, elle est si brûlante qu’il vaut mieux éviter de nous y exposer. »

Ernaut fronça les sourcils, étonné.

« Vous a-t-il confié quelque terrible secret ?

— Non. Il m’a semblé tel qu’il est, loyal serviteur, fidèle dans ses engagements et obéissant.

— Il préfère honorer la mémoire d’un mort, peut-être grand larron, plutôt que détromper deux batailleurs en champ clos ? »

L’intendant ouvrit les mains, en signe d’interrogation, un masque de dépit s’affichant sur son visage.

« Il est homme à choisir de deux maux le moindre, je ne saurais en dire plus. »

Ernaut comprit où voulait en venir le vieux Pisan. Il lui sourit à demi, reconnaissant du réconfort qu’il tentait de lui apporter. Puis, saluant de façon appuyée, il prit congé et rejoignit sa monture. Il y accrocha rapidement sa besace et une outre. Pendant ce temps, le muletier s’était avancé vers la sortie, prêt à partir. Il tenait une longue badine à la main et un fouet était passé dans sa ceinture. Le visage basané, brun de poil, il était vêtu de toile aussi grise que les chemins poudreux. Il salua d’un air affable, dévoilant des canines esseulées.

« Je suis prêt à faire chemin et me semble que vous aussi.

— En effet, mais je vais partir devant. »

Le muletier prit un air sentencieux.

« L’intendant m’a dit que vous deviez aller à Jérusalem et que je devrai témoigner de cela peut-être.

— N’ayez nulle crainte, je vous compaigne jusqu’à la sortie, au puits de la Vierge. Mais de là, je pense talonner pour être au plus vite en la cité. Vous pourrez jurer m’avoir vu prendre le chemin. »

L’homme hocha la tête, heureux de s’en tirer à si bon compte, et de se voir débarrassé au plus tôt d’une corvée. Il cria un grand coup lorsqu’Ernaut se mit en selle et le train de mules se mit en branle. Lui-même était installé sur une de ses bêtes, où il avait juste posé un vieux sac. Il allait et venait pour diriger le convoi, les pieds ballants, les jambes dans le vide, sans autre aide qu’un méchant bout de corde en guise de licol. Voyant le regard impressionné d’Ernaut devant la maîtrise de ses bêtes, il lança d’une voix forte :

« Ce n’est pas tant pour les diriger que pour les protéger que je dois être là. S’il ne se trouvait jamais de larrons sur le chemin, elles sauraient faire le voyage aussi bien sans moi ! »

Puis il rit bruyamment de sa remarque, claquant de la langue pour faire accélérer la file lorsqu’ils tournèrent dans la grande rue.

Indifférentes à ce qui se passait autour d’elles, les mules avançaient, les oreilles attentives, le regard calme, le port tranquille. Lorsqu’ils arrivèrent en vue du fond du vallon, du bassin et des jardins, Ernaut salua de la main tout en lançant sa monture à un petit galop. Il lui fallut insister pour obtenir ce qu’il voulait, à sa grande honte, car il se savait observé par le convoyeur. Il ne parvint à maintenir son allure que péniblement et devait sans cesse stimuler son cheval. Il hésitait à trop le pousser, étant donné la chaleur et la fatigue que la bête devait ressentir, à être ainsi chahutée depuis plusieurs jours.

Tout en laissant le paysage défiler, Ernaut repensa machinalement à toute l’histoire. Une fois à Jérusalem, on allait sûrement lui confier quelques tâches rébarbatives, comme c’était le cas à chaque fois que l’on désirait punir un des hommes. On pouvait ainsi se retrouver à surveiller certaines sections des courtines, inventorier un arsenal poussiéreux ou porter des messages à travers toute la cité. Cela ne le dérangeait guère, tant qu’on le laissait être présent pour le jugement.

Sur ce point il n’avait guère de doute. Même si le seigneur de Retest avait protesté et obtenu gain de cause, le vicomte et le mathessep n’étaient pas hommes à laisser quelqu’un décider pour eux. Les sergents étaient au service du roi et ils aimaient à le faire comprendre à chacun, fût-il un noble chevalier fieffé. La punition serait certainement symbolique, son rappel à Jérusalem constituant déjà une concession. Qui venait fort mal néanmoins, car cela le privait peut-être des dernières confrontations d’où la vérité aurait pu jaillir, du moins suffisamment d’éléments pour convaincre Aymeric d’abandonner ses ridicules accusations. Vu que le lendemain il serait à la Mahomerie, Ernaut espérait trouver le moyen d’aller questionner Umbert, avant le serment solennel dont personne ne pouvait se rétracter sans conséquence.

Mais d’ici là, il avait pensé à d’autres pistes, au cas où ce ne serait pas possible pour lui de s’esquiver, qu’Umbert ne sache pas grand-chose de plus, ou ne veuille toujours attester de rien. Il était encore une possibilité, qu’Ogier ait vendu les derniers fugitifs comme esclaves. Les sachant musulmans, il aurait pu en tirer bon prix. Et savoir s’il se trouvait ou non une femme parmi eux serait un indice de plus.

Les négociants qui achetaient et revendaient les prisonniers n’étaient pas de ceux qu’Ernaut aimait à fréquenter. Mais il en connaissait suffisamment pour obtenir des réponses à ses questions. S’il faisait fausse route et qu’Ogier avait caché la femme, cette Safiya, elle avait peut-être nécessité des soins, si les siens n’avaient pas voulu la laisser partir. Une rapide visite à l’hôpital pourrait s’avérer fructueuse. Avec un peu de chance, il obtiendrait là de quoi décider Aymeric.

### Jérusalem, mercredi 18 juin, soir

L’agitation des rues commençait à retomber et les boutiques fermaient peu à peu, dans un grincement d’éventaire. Des cris d’enfants, l’aboiement d’un chien, les exclamations de fin de journée entre voisins négociants, voyageaient au long des murs de la rue du Temple. Ernaut avait ouvert le volet de la fenêtre qui y donnait directement, se laissant bercer par l’animation de l’artère la plus marchande de Jérusalem.

Il avait quitté depuis quelques mois son logement sur la terrasse, dit au revoir à Saïd, qu’il croisait encore de temps à autre, pour s’installer dans deux pièces plus au cœur de la cité. Il aimait en entendre l’agitation, les clameurs, en sentir les parfums. Il en goûtait la palpitante activité, déclinant avec le jour.

Lui-même avait besoin d’un peu de repos et il s’était installé pour souper, confortablement assis sur une natte. Il avait pris l’habitude de manger à l’orientale, sans table. Il n’avait d’ailleurs pour tout meuble, en dehors d’un gros coffre, que deux banquettes et un tabouret.

Une étagère sur le mur accueillait sa maigre vaisselle : écuelles, pots et pichets. Il dévorait à belles dents la tarte au fromage qu’il avait achetée un peu plus tôt. Elle était désormais froide, mais il en appréciait chaque miette, qu’il arrosait régulièrement d’un vin clair, rafraîchissant, des monts de Judée. Il avait également prévu de l’hoummous, avec des oignons, et du pain plat, ainsi que des fruits.

Un bruit de cavalcade et une vive altercation le firent se lever, pour admirer avec amusement la dispute entre trois cavaliers qui remontaient la rue en direction du Temple et un marchand de plats métalliques, qui obstruait le passage avec ses balles et ses ânes. Bien évidemment, le voisinage s’en était mêlé et s’était ainsi assuré que la situation ne pourrait se résoudre sans hauts cris ni féroces invectives. Plusieurs têtes se montrèrent aux fenêtres voisines, admirant également la scène.

Depuis son retour à Jérusalem, Ernaut n’avait pas eu le temps de souffler, et il avait besoin d’une pause avant de reprendre le fil de son enquête. Un rapide passage au palais lui avait confirmé ce qu’il pensait : on avait obtempéré aux demandes de Robert de Retest sans grandes convictions et il était libre jusqu’au lendemain, à condition pour lui de rester dans les limites de la cité et de se présenter au point du jour à la porte de David avec une monture. Prenant à peine le temps de saluer ses compagnons, il avait donc immédiatement entrepris de poursuivre son enquête. La première personne qu’il avait vue, c’était la sœur qui s’occupait des entrées à l’hôpital des femmes de Saint-Jean.

Elle n’avait pu trouver aucune mention d’une Safiya arrivée là aux environs de Noël. Elle lui avait expliqué qu’on avait pu leur donner un faux nom, peut-être même la faire passer pour l’épouse de cet Ogier, mais rien dans sa mémoire ne lui évoquait ce qu’il recherchait.

Comprenant combien cette recherche lui coûtait, elle avait tenté de le rassurer et avait poussé la gentillesse jusqu’à interroger pour lui d’autres moniales, sans plus de succès. Ernaut s’était promis de faire un don substantiel à Saint-Jean. Il avait ensuite tenté de visiter quelques marchands d’esclaves. Il n’y en avait pas tant que ça, et il ne put en rencontrer que deux disponibles et acceptant de lui parler.

Si le premier était un petit homme bedonnant à l’allure assez sympathique, qui semblait considérer les gens qu’il achetait et revendait ni mieux ni pire que des chevaux ou des vaches, le second s’était montré bien plus retors. Résidant près du quartier des tanneurs, il avait installé ses locaux dans un bâtiment qui avait connu des jours meilleurs, avec plusieurs magasins autour d’une cour. Il avait une cohorte de valets à son service, aussi teigneux et antipathiques que leur maître. Assez grand, l’allure indolente, il semblait se mouvoir sans bouger autre chose que les pieds et parlait d’une voix langoureuse, avec une élocution traînante.

Tout au long de la discussion, Ernaut avait eu envie de le secouer pour le presser un peu. Cependant, il avait été mis mal à l’aise par le regard malsain que l’homme dévoilait lorsqu’il évoquait les femmes qu’il acquérait et revendait, son sourire avachi se redressant alors en un rictus rassasié, comme si chaque nom lui évoquait de délicieux souvenirs. Il se montra néanmoins assez accueillant et parcourut ses tablettes, sans y trouver la moindre mention qui aurait pu concerner une Safiya, ou un contact commercial nommé Ogier. Ernaut le suspectait de mentir, mais il n’aurait su dire sur quoi ni pour quelle raison. Lorsqu’il le quitta, lui serrant la main, il lui sembla que la chaleur en avait été aspirée. Il ne respira à son aise qu’une fois de retour dans la rue.

Alors qu’il achetait son repas du soir, il avait croisé Droart et ils avaient échangé quelques nouvelles. Leur ami commun, Eudes, avait été envoyé en mission sur la côte, porter des messages. Beaucoup espéraient qu’une campagne d’ampleur allait bientôt se mettre en place, comme souvent l’été. Le jeune roi, Baudoin, était un homme d’action, désireux d’agrandir le royaume. Droart s’enquit bien évidemment de Lambert et lui proposa de se renseigner auprès de quelques propriétaires de ses connaissances. Au cas où Ogier aurait loué quelque chose pour y cacher Safiya. Ils convinrent de se retrouver le lendemain matin à la porte de la cité, avant qu’Ernaut n’accompagne le vicomte et les jurés pour la bataille.

Ernaut étendit les jambes, se massant les cuisses endolories par les heures passées en selle. S’il s’était amélioré au point de vue de son assiette, son corps n’avait pas encore la souplesse nécessaire pour supporter de longs trajets. Il grimaça, enfournant de grandes bouchées de tourte qu’il mâchait le visage tendu, tout en appuyant sur les points douloureux. Il avait décidé de se rendre dans des bains, peu éloignés de chez lui. Ce n’était pas ceux qu’il fréquentait habituellement. Il les avait choisis, car il voulait visiter des endroits où l’on pouvait se faire accompagner lors de ses ablutions, obtenir quelques services supplémentaires en plus de l’achat de savon et du prêt de serviettes.

Il comptait inspecter ensuite plusieurs autres établissements qu’il connaissait. Là encore, il était possible qu’Ogier ait vendu la jeune femme et il pourrait donc demander après elle, en se faisant passer pour un habitué de ses services. Il ne croyait guère à cette hypothèse, mais cela lui donnait l’impression de pouvoir continuer à chercher, tout en s’accordant un moyen de souffler un peu. Il était épuisé, physiquement, vermoulu de courbatures, couvert d’ecchymoses et l’échec de ses recherches lui pesait lourdement. Il avait l’impression que le casal tout entier savait la vérité, mais que personne ne souhaitait réagir, satisfait de voir Lambert et Aymeric s’affronter pour des faux-semblants, garants de l’apparent équilibre qui régnait sur place.

Ayant fini son repas, il frotta son écuelle de la main avant de la ranger et alla prendre du feu chez son voisin afin d’allumer une lampe à huile, qu’il laissa sur le tabouret, au centre de la pièce. Puis il verrouilla sa porte et descendit vers la venelle d’accès, en sifflotant comme pour se convaincre d’être de bonne humeur. On accédait à l’établissement de bains par un accès discret dans un renfoncement de la rue. Si chacun s’accordait sur leur utilité et sur les revenus que cela générait, y compris par les redevances, nul ne souhaitait qu’ils soient trop apparents dans la cité qui accueillait le tombeau du Christ.

Certains hypocrites évoquaient parfois du bout des lèvres le nom de Marie-Madeleine pour justifier leur présence, sans grand succès. De toute façon, dès que les hommes s’étaient emparés de la ville, ils avaient pu trouver semblables lieux et nul ne songeait à les interdire, tant que cela restait discret.

À l’entrée, un jeune homme l’accueillit poliment, lui proposant des serviettes, ainsi que de quoi se laver, des huiles parfumées. Le prix était raisonnable, deux deniers, pour le simple usage des bains.

« Si vous avez désir d’un massage, il vous faudra le régler ensuite, bien évidemment. »

La proposition avait été formulée si naturellement qu’Ernaut douta quant à ses renseignements. Il prit de quoi se laver et rejoignit les vestiaires, où il rangea ses affaires dans un sac, sous la surveillance d’un garçonnet accroupi sur le sol. Il était certainement là pour s’assurer que personne ne se trompait de paquet lorsqu’il se rhabillait. Deux hommes discutaient à voix basse de leur commerce de viande, quelques allusions salaces rassurèrent Ernaut.

Une fois revêtu uniquement d’une serviette autour des reins, une autre à la main, avec un gant de crin et du savon, il prit le chemin des salles de vapeur, son soin préféré. La brume était telle qu’il devinait plus qu’il ne voyait les présents. On pouvait s’installer sur des bancs de pierre, installés en gradins sur la périphérie de la pièce plongée dans la pénombre. Un petit groupe parlait par intermittence, usant d’une langue inconnue d’Ernaut. Des Teutons, pensa-t-il, croyant reconnaître les accents gutturaux.

Il sortait régulièrement de l’endroit, pour se faire asperger d’eau froide par un valet. Il aperçut à cette occasion plusieurs jeunes femmes, vêtues de leur seule chemise, l’humidité dévoilant leurs formes. Quelques-unes attendaient dans un angle de la pièce, tandis que d’autres allaient et venaient en direction d’un couloir adjacent. Flatté et amusé, il réalisa que plusieurs d’entre elles portaient sur lui un regard appréciateur tandis qu’il se frictionnait avec vigueur.

Une fois qu’il le faisait à côté d’un gros homme au visage mou, affaissé, aux jambes grêles tels deux piquets sous un amas de graisse, il échangea avec plusieurs d’entre elles des regards amusés. Il n’était pas suffisamment naïf pour penser que c’était une réelle complicité, mais cela le fit néanmoins sourire. Il aimait découvrir chez les autres une admiration pour ce qu’il était : un géant au physique de titan, dont le bras évoquait Samson ou Godefroid de Bouillon.

Lorsqu’il en eut terminé avec ses ablutions, il s’approcha de la zone où se tenaient les femmes. Il s’y trouvait alors une jeune, guère plus âgée qu’Osanne, au sourire sans joie, les cheveux blonds noués en un assemblage savant. Ses formes androgynes et osseuses ne l’inspirèrent guère. À côté d’elle, une brune, le crâne enserré d’une touaille mal ajustée, lui souriait. Au coin de ses yeux, de légères rides accusaient son âge, mais son visage était avenant. Ses formes plantureuses distendaient le tissu par endroit, incitant l’œil et peut-être la main, à se faire caressant. Elle l’interpella, avec un accent chantant, peut-être grec, roulant les r et faisant siffler les s.

« Besoin passer huiles de senteur ? »

Disant cela, elle laissait courir son regard gourmand sur le buste de Ernaut, avec autant de métier que de réel intérêt.

« Certes oui, je ne serais pas contre. Il me faudra peut-être payer cela, non ?

— Pas cher ! Mes mains sont si douces, tu aimeras. »

Ernaut acquiesça d’un signe de tête et la femme se leva, lui servant de guide jusqu’à une alcôve fermée d’une natte. Plusieurs lampes à huile éclairaient l’endroit, dont le mur du fond était chauffé par le hammam contigu. Au milieu de la pièce, une banquette de bois, qu’elle caressa de la main, invitant Ernaut à s’y allonger. Il s’exécuta, tandis qu’elle prenait un pot sur une étagère et s’enduisait les mains d’un liquide épais.

« Quel est ton nom ? » demanda-t-il. Elle lui sourit d’une façon très professionnelle.

« Tu aimes quoi ?

— Non, j’aimerais savoir vraiment ton nom…

— Tu es homme aimer parler, hein ? »

Elle s’esclaffa, intriguée, puis s’avança vers lui d’une démarche chaloupée.

« Au cas où je revienne, si j’ai envie de te revoir.

— Alors demander Irène. »

Elle posa ses mains sur les jambes, appuyant de tout son poids.

Elle savait véritablement masser. Ernaut sentit ses muscles congestionnés s’assouplir peu à peu, grognant de plaisir sous les mouvements experts. Il commençait à se détendre. Lorsqu’Irène entreprit de lui masser les cuisses, il grimaça sous la douleur. Elle échappa un petit rire et échangea un regard complice avec lui, scellant un accord tacite tandis que ses mains remontaient. Il inspira profondément, se disant qu’il lui faudrait penser à demander si elle connaissait une certaine Safiya. Après.

