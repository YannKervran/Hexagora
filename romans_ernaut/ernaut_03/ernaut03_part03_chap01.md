## Chapitre 1

### Jérusalem, mardi 10 juin 1158, fin de matinée

La cohue emplissait l’espace, résonnait sous les arcades du passage. Nul ne pouvait se faire entendre sans crier, en ajoutant sa propre voix au vacarme grondant, écrasé d’un soleil implacable. La poussière grise soulevée par le piétinement des souliers, des pattes et des sabots s’accrochait à la moindre parcelle de peau transpirante, envahissait les gorges, brûlait les yeux. La sueur fauve, les relents des bouses se mêlaient aux effluves résineux des grumes tirées par des chameaux impassibles.

La chaleur envahissait tout, faisait écumer les soldats sous leur casque, rendait les mains moites, les bouches sèches. Des hommes à la démarche empressée s’affairaient depuis leurs montures jusqu’à la petite table installée sous un auvent, cherchant l’ombre. Par moments des éclats de voix surnageaient, attirant le regard des curieux. Indifférents au monde, des marcheurs de Dieu, la croix cousue à l’épaule, avançaient l’air amusé, d’un pas vif, sans être inquiétés par les gardes au regard inquisiteur.

Un troupeau de moutons tentait de se faufiler au-dehors, les chiens mordant les pattes des trainards affolés par le chaos ambiant. Des cavaliers à l’air sévère, le menton audacieux, fendaient la foule d’un air impérieux, montrant bague, sceau ou boîte blasonnée aux vigiles débordés avant de s’engouffrer dans les ruelles de la cité, Jérusalem.

Parmi les hommes casqués qui contrôlaient, inspectaient, endiguaient le flot des marchands et des denrées franchissant la muraille se tenait un colosse, bâti à l’antique, Samson ou Goliath, la chevelure coupée à l’écuelle, la mâchoire prête à mordre, le regard fatigué. Protégé d’un vêtement de toile légèrement rembourré, le jeune homme apparaissait gigantesque, démesuré, et sa voix ronflant par-dessus la clameur effrayait jusqu’au plus fier des nomades. Ses mains puissantes étaient toujours en action et on craignait souvent qu’elles ne cherchassent la petite masse à tête de bronze passée dans le baudrier de son épée.

Dépassant la foule des épaules, il représentait depuis plusieurs mois l’écueil sur lequel venaient se briser les espoirs des commerçants fripons, des négociants astucieux, des trafiquants de toute sorte. Escomptant qu’un tel amas de muscle ne pouvait laisser de place à la cervelle, certains avaient espéré le tromper à ses débuts, en pure perte. Il n’était pas le plus habile pour fouiller les ballots, découvrir les produits relevant du péage cachés parmi d’autres, anodins. Mais lorsqu’il fallait faire entendre raison à un mauvais payeur, un voyageur récalcitrant, il lui suffisait d’approcher sa haute carcasse, en ajoutant parfois un froncement de sourcil, et les récriminations se taisaient. La taxe était payée.

La réputation d’Ernaut de la porte David avait dépassé les villages proches et on murmurait désormais son nom jusqu’aux ports de la côte, non sans un certain effroi. À ses débuts, quelques mois plus tôt, il lui avait fallu se faire connaître comme un sergent capable. Il ne rechignait donc pas à distribuer mandales et taloches avec une application qui finit par inquiéter même le mathessep. On le lui reprocha et il s’en était tenu là. Alors, il avait commencé à déverser sa mauvaise humeur dans l’application qu’il mettait à débusquer les tricheurs, les mauvais payeurs. Depuis toujours, c’était une sorte de jeu tacite entre les hommes chargés d’encaisser les taxes à l’entrée de la ville et les assujettis : ces derniers tentaient de dissimuler plus ou moins pour que les premiers puissent prétendre à des découvertes justifiant leur emploi. Mais Ernaut n’avait guère respecté les formes.

Il fouillait sans mesure, étudiait avec froideur, admonestait avec vigueur. Il avait contrarié un grand nombre de négociants réputés, froissés de se voir traités comme de simples escrocs à la petite semaine. Il n’est aucune vérité bonne à entendre quand elle est dite pour humilier et contraindre. Au final le vicomte était satisfait de ses services : les péages étaient payés avec plus de régularité, le pouvoir du roi était visible dès l’entrée dans la ville. Un garde inflexible, prompt à la colère et au bras puissant veillait.

Pour l’heure, Ernaut rêvassait, appuyé contre la muraille, nonchalant. La caravane de chameaux se mettait en branle pour porter son bois d’ouvrage dans la cité. Les longues poutres, à peine équarries, étaient attendues sur un des nombreux chantiers. Il tendit la main vers un camarade, récupéra une outre à peine ventrue et la pressa au-dessus de sa bouche pour en extraire un vin léger, coupé d’eau tiède, à la saveur amère. Remerciant d’un rictus son compagnon, il s’essuya la bouche du revers de la manche et se frotta le visage. Il était de service depuis la veille au soir et se sentait las. Il avait envie d’un bain, un luxe oriental auquel il avait pris goût. Et surtout il se languissait d’une jeune femme, Libourc. Sa simple évocation suffisait à lui échauffer le sang, à le rendre mélancolique. Elle habitait au nord, dans un nouveau village de colons, avec les siens. Il ne la verrait certainement pas avant le dimanche suivant, comme chaque semaine. La messe. Le repas dominical tous ensemble. Les anecdotes du vieux Sanson, son père. Les regards sévères de Mahaut sa mère. Et les sourires de Libourc, riches de fossettes et de gaieté.

Il sortit de sa rêverie et reprit sa place au milieu du passage, enfin libéré. La fin de matinée était toujours plus calme, la plupart des marchandises entrant en ville tôt, ou alors le soir, peu avant la fermeture nocturne des portes. Il sourit à un groupe d’enfants qui sortaient, menant quelques oies. Un jars moins docile jargonnait bruyamment, exprimant avec vigueur son mécontentement à devoir obéir de la sorte. Un des gamins le frappait nonchalamment sur le dos, indifférent à ses cris stridents. Lorsqu’ils débouchèrent hors du passage, vers l’ouest, ils stoppèrent un couple de chevaux avançant au pas.

Les montures comme les hommes n’étaient pas de première fraîcheur et ils étaient gris d’avoir voyagé sur les chemins du royaume. L’un des hommes avait une posture maladroite, avachie et en déséquilibre, une rêne trop tendue et l’autre fort lâche. Son destrier, un petit alezan à l’arrière-main épaisse et à la crinière hirsute ne semblait pas s’en formaliser et prenait de lui-même l’initiative. Ayant brandi un sceau qui leur laissait libre passage, les messagers pouvaient passer sans autre formalité. Mais Ernaut reconnut le piètre cavalier et lui fit signe de la main, tout en s’effaçant légèrement pour son compagnon.

C’était un des hommes de la Mahomerie, le village où son frère s’était installé l’année précédente.

« La bienvenue en la cité du roi, maître Fèvre ! »

L’autre lui rendit son salut avec hésitation, incertain de ce qu’il devait faire de ses rênes. Ernaut attrapa le filet de la monture et la tint calme, le temps pour eux d’échanger quelques nouvelles.

Albert le Fèvre était un des forgerons du village, un jeune au visage massif directement posé sur les épaules, sans cou. Ses cheveux bruns pointaient en tous sens et il portait une épaisse cotte de laine malgré la chaleur. L’air ennuyé, il se tripotait l’oreille comme s’il espérait en faire jaillir quelque génie oriental.

« Salut Ernaut… Je suis aise de t’encontrer si vite. Je l’espérais bien, ton frère m’a indiqué que tu œuvrais souvent en ce pertuis. 

— Besoin d’aide en la Cité ? J’achève mon tour de garde au mitan du jour.

— Je te porte graves nouvelles du casal. »

Le visage du jeune homme se voila en un instant, ses yeux s’enchâssèrent profondément, attendant d’en savoir plus.

« On a retrouvé maître Ogier occis en hostel, tôt ce matin, je compaigne l’homme du Saint-Sépulcre venu l’annoncer à la cour des Bourgeois, car il y a demande de justice. »

Le sergent coula un regard rapide vers l’autre cavalier, un jeune valet qui semblait fort goûter d’avoir chevauché ainsi la matinée.

« En quoi cela me touche ? Je n’ai pas connoissance de cet Ogier…

— Si fait, mais ton frère le connaît fort bien, car il a eu souventes fois maille à partir avec le père de sa promise.

— Godefroy ?

— Oil. Toujours à se bestancier ces deux-là. »

Albert marqua une pause.

« Godefroy a navré Ogier selon Aimeric le Grand et il veut en demander justice.

— Qu’est-ce que Lambert espère de moi ?

— Il est acertainé que ce ne peut être Godefroy. Il dit que tu saurais en trouver témoignage. »

Ernaut ronfla à la remarque, s’attirant un regard inquiet du cheval. Il soupira, indécis.

« Et te savoir là calmerait les esprits, il en a espoir. Cette meurtrerie est l’étincelle attendue par certains pour enflammer leur courroux.

— Je ne vois nulle raison à accuser ainsi le vieux Godefroy, doux comme agnel, sans une once de malice en lui. »

Le forgeron opina, l’air grave.

« Je dois poursuivre. Mais si tu peux avoir licence, il serait fort prude que tu chevauches au plus vite jusqu’au casal. »

Il s’interrompit un moment et se pencha pour ajouter, à mi-voix :

« N’oublie pas ton harnois de guerre. »

Puis, talonnant sa monture qui n’en avait cure, il déboucha vers l’intérieur de la ville et rejoignit l’autre cheval qui s’était avancé lentement, serpentant ensuite parmi la foule déambulant aux abords des boutiques.

Ernaut leva les yeux, couvant du regard la ville qui s’offrait à lui sous un ciel d’azur. D’où il se tenait, il ne voyait que les façades les plus proches, les hautes maisons dont les toits plats étaient encombrés de cabanes, d’appentis, d’auvents légers en feuille de palme. Il fit signe à un des sergents de prendre sa place au milieu du chemin et se dirigea vers un petit bâtiment accolé à la muraille, dans la chicane qui en compliquait le franchissement.

La fraîcheur s’abattit sur sa nuque, apaisant la tension qui s’y était accumulée depuis la veille. Plusieurs clercs s’activaient autour d’une table couverte de tablettes de cire, de documents comptables, de listes de référence. Occupés à récapituler, additionner, inventorier, ils n’observèrent qu’une indifférence polie envers le garde. Assis sur un escabeau devant eux, un marchand ventru, d’origine orientale, tripotait nerveusement un petit sac de maroquin en attendant qu’ils aient fini leurs estimations.

Ernaut tira une portière fatiguée, aux motifs collés de crasse, de suie et de poussière. Une partie de la pièce servait de salle de repos aux gardes, abritant deux paillasses ainsi qu’un brasero allumé seulement au plus fort de l’hiver. Sur un des lits, un jeune homme dormait sur le flanc, les bras croisés, les jambes repliées. Ses cheveux de feu constituaient la seule note de couleur de sa tenue. Ernaut lui secoua l’épaule en murmurant son nom.

« Eudes, j’ai souci, éveille-toi! »

Le dormeur tourna des yeux fatigués vers le colosse qui l’agitait doucement. Les paupières collées peinaient à s’ouvrir et ses minces lèvres craquelées ne s’écartèrent qu’à regret pour qu’y passe une langue desséchée.

« Mon frère appelle après moi, le sang a coulé en son casal.

— Et ?

— Il se pourrait qu’on en accuse celui qui tantôt sera son père. »

Eudes toussa doucement et s’assit contre le mur, frottant ses cheveux d’une main nerveuse.

« Si tu souhaites lui porter aide, c’est à maître Ucs qu’il te faut demander. Tierce a sonné ?

— Depuis un petit moment, oui-da.

— Alors tu le trouveras au palais, à rendre compte de la nuit. Dis-lui que nous en avons parlé. Je ne pense pas qu’il te retienne, il n’y a nulle tâche que nous ne puissions accomplir sans toi. »

Le géant se redressa, l’air grave.

« Merci compaing, je te revaudrai ça ! »

Le rouquin fit un geste las et tira à lui la couverture, s’alllongeant de nouveau.

« Bonne chance, Ernaut. Donne des nouvelles. »

### Casal de la Mahomerie, mardi 10 juin 1158, fin d’après-midi

La monture d’Ernaut serpentait sur la voie, évitant les pierres branlantes. Chaque pas soulevait un peu de la gluante poussière grise qui recouvrait tout. Les collines environnantes étaient parsemées de taches vertes ponctuant de vie les pentes cendrées écrasées de soleil. La terre avait une couleur crayeuse qui faisait plisser les yeux sous la lumière crue de cette fin d’après-midi. Avec leurs habits poudreux, les rares paysans à l’œuvre se fondaient aux murs en pierres sèches. Le ciel, d’un bleu uniforme inaltéré, triomphant, reposait sur les reliefs tel une majestueuse coupole byzantine sur ses bases de marbre. Pas un souffle de vent, pas une goutte d’humidité dans l’air. Seules les cigales semblaient se réjouir, se répondant d’un fossé à l’autre, d’une terrasse aux collines en face.

Ernaut s’épongea le front de la manche et but à sa gourde les dernières gorgées. La langue épaisse, il espérait apercevoir bientôt la vieille tour qui surplombait le village de la Mahomerie. L’appel d’un rapace le fit lever la tête, s’abritant les yeux de la paume. Les cris se répercutaient dans les vallées. Loin devant lui, un convoi progressait, chameaux et mulets soulevant dans l’air un nuage qui les signalait des lieux à la ronde. La dépression qu’il suivait s’ouvrit enfin et il vit les pentes du casal, droit au nord.

Entourées d’un mur grossier, les maisons s’étendaient le long d’une rue pentue, depuis la source en contrebas. L’entrée du village depuis Jérusalem jouxtait le fort bâtiment de la curie où l’intendant du Saint-Sépulcre était installé à l’ombre de la grande tour. En haut sur la droite, surplombant les maisons au toit plat, l’église Sainte-Marie sanctifiait le lieu. Entre elle et les ruines de la vieille tour, sur une éminence à gauche, la route vers Naplouse et la Samarie disparaissait derrière le ressaut du terrain, parmi les reliefs vallonnés. Sur les pentes alentour, de nombreuses vignes couraient parmi les oliviers et quelques figuiers. De rares chaumes grisâtres accueillaient quelques moutons ainsi que des vaches dans les champs les moins inclinés. Sur les pentes à l’ouest de la source, des hommes s’activaient, arrachant le sésame enfin mûr. Près du bâtiment où l’on puisait l’eau, une assemblée de femmes s’occupait du linge, discutait, venait remplir jattes et brocs au milieu d’une nuée d’enfants rieurs.

Tout en bas, dans la dépression accueillant le ruisseau, des arbres fruitiers présentaient avec fierté un feuillage vert, abritant des jardins soigneusement entretenus. Peu à peu, les bruits de l’activité du casal se firent entendre : une enclume résonnait, des gorets fouaillaient dans un enclos non loin, des chiens aboyaient, excités par les courses des gamins. Il obliqua sur la droite, dépassant le bâtiment d’où la source s’écoulait, en un mince filet jusqu’aux bacs qui formaient réserve. D’un signe de tête répété, il salua les présentes, reconnaissant certains visages, s’attirant de temps en temps des sourires en retour, parfois une complète indifférence.

Tout le monde devait savoir qu’il allait venir ou s’y attendait. Une meute d’enfants se dirigea avec enthousiasme vers la curie, se répandant dans les rues comme un ruisselet sur une terre aride. D’ici peu chacun saurait que le frère de Lambert, le sergent du roi, était arrivé. Il remonta au pas la longue rue, indifférent aux regards curieux et inquisiteurs, qui se détourneraient s’il cherchait à les croiser. Les façades chaulées étaient percées de portes basses et de maigres fenêtres, même à l’étage.

Parfois, un visage inquiet s’avançait avec méfiance, intrigué par la venue d’un cheval. Une fois parvenu sur le méplat au milieu du village, Ernaut ralentit l’allure. Il connaissait à partir de là plusieurs personnes, amis et voisins de Lambert. Rien ne distinguait la maison de son frère des autres. C’était une des dernières sur la droite, non loin de l’église Sainte-Marie. Depuis la terrasse, on avait une jolie vue sur les environs et, plusieurs fois, ils avaient rompu le pain ensemble là-haut, admirant le panorama après une journée de labeur.

Le long bâtiment servait d’entrepôt au rez-de-chaussée et l’on accédait à la pièce de vie par un escalier perçant la voûte. De là, une échelle menait à l’extérieur. On entreposait sur le toit certaines denrées, on y faisait sécher des fruits, on y dormait parfois. Ernaut descendit de sa monture, l’attacha à un piquet branlant devant la porte et cria plusieurs fois à l’intention de son frère. Le visage brun, désormais hâlé, aux joues couturées de cicatrices se montra par une des ouvertures.

« Ernaut ! Sois le bien venu, frère, monte donc. »

Le jeune homme poussa la petite porte et traversa la pièce encombrée de paniers de grain, de tonneaux, de jattes et de pots de conserve. Noyé de ténèbres, le lieu sentait la poussière, l’humidité et le sel. La fraîcheur l’enveloppa comme un linceul et un frisson le parcourut tant il s’était échauffé jusqu’alors. En quelques foulées, il fut sur les marches et déboucha à l’étage. Des poussières volaient dans les rayons de lumière tranchant la pénombre. Debout à côté de la table au centre de la pièce, son frère Lambert, les deux mains sur les hanches, l’attendait. Un sourire inquiet barrait son visage grêlé, souvenirs de sa maladie récente. Ses épaules noueuses ne portaient qu’une chemise et ses chausses roulées au genou avaient glissé. Il s’avança pour embrasser Ernaut avec chaleur.

« Mille pardons de te faire chevaucher par chemins en si forte chaudée, mon frère. Tu es en eau ! »

Ernaut avisa la cruche sur la table, ainsi que les deux gobelets.

« Quelques rasades et il n’y paraîtra plus, frère.

— Prends donc un escabel, je vais te servir. La cuvée ne vaut pas celle de père, mais elle enjoie le gosier du voyageur. »

Les deux hommes s’installèrent. Tandis que Lambert remplissait les godets, Ernaut se frottait la nuque tout en laissant ses yeux parcourir la pièce. Tout était propre, les coffres alignés le long du mur, le lit fait, quelques étoffes pendaient à une patère, des baquets étaient empilés dans un coin, à côté de paniers. Il déboucla son baudrier, posa son fourreau et sa masse sur la table à côté de lui. Puis se racla la gorge avant de lever son verre, en avalant la moitié en une seule longue rasade. Enfin il attendit.

Face à lui, Lambert étudiait la table comme s’il ne l’avait jamais vue, ses mains aux ongles sales et râpeux caressant le fil du bois avec attention. Il renifla puis remonta ses doigts vers son menton. Il gratta sa barbe de plusieurs jours, faisant voler un nuage de fine poussière dans les raies de lumière. Puis il croisa enfin le regard de son frère et lui sourit, tendu.

« Je t’ai fait prévenir par le Fèvre car je crains quelqu’ennui à venir pour les miens. »

Il marqua une pause, se tournant vers la fenêtre comme s’il cherchait à voir dans la rue à travers le volet tiré.

« Tu as déjà ouï parler de maître Ogier de Saint-Gilles ? »

Ernaut fit la moue, secouant la tête sans conviction.

« C’est, enfin, c’était le voisin d’Osanne. Un fâcheux, avide de querelles. »

Sa voix s’éteignit comme s’il renâclait à poursuivre. Il inspira un grand coup puis reprit son récit.

« Ce matin, on l’a trouvé sans vie en son hostel. Meurtrerie. Comme il vivait seul, on ne sait ce qui est arrivé pendant la nuitée.

— Des brigands venus des collines ?

— Peut-être, mais il avait nombreuses et fortes disputes dans le casal, surtout avec Godefroy.

— Le père d’Osanne aurait occis son voisin ?

— Certes pas, sinon je ne t’aurais pas fait appeler. Je me serais contenté de prier pour son âme de pécheur. Non. Ce ne peut être lui, j’en suis acertainé. »

Le silence s’installa, Lambert resservit à boire. Une mouche vrombit entre leurs têtes et vint se poser sur la table.

« Le souci, c’est que certains ici voient les choses autrement…

— Des accusations ont été lancées ?

— Rien de direct, mais au lavoir on ne parle que de ça. »

Ernaut réfléchit, faisant chanter le vin entre ses lèvres. Il tenta d’attraper l’insecte, sans succès, et le chassa d’un revers nonchalant.

« Sa famille a demandé à être entendue par la cour ?

— Il n’en avait pas, du moins pas ici. Peut-être à Saint-Gilles, un village plus au nord. De toute façon, si quelqu’un doit s’avancer et parler, ce sera demain.

— Tu espères quoi de moi, frère ? Je ne suis pas magistrat ni juré de la cour.

— Tu sauras peut-être trouver raisons à cette meurtrerie, l’expliquer demain avant que certaines paroles définitives ne soient dites. »

Le jeune sergent eut un sourire sans joie et hocha la tête doucement, examinant le fond de son verre désormais vide.

« De quoi prouver que Godefroy ne peut être derrière tout ça, quoi ?

— Oui. C’est homme de bien, aussi droit et honnête que le père. Tu l’as déjà vu, tu sais qu’il n’irait pas occire nuitamment son voisin. »

Ernaut pinça les lèvres, reposa son gobelet.

« Ces querelles ? Sur quoi portaient-elles ?

— Tout et rien, répondit Lambert, faisant un geste agacé de la main. Cet Ogier aimait à chicaner sur tout.

— Il a bien des féals, si d’aucuns pensent à demander justice pour lui

— Il n’est nul fâcheux qui ne trouve son complice. Il était lié avec la plupart des feux du quartier du Puits de la Vierge, le bas du casal. »

Ernaut se redressa et s’étira. Puis il posa les coudes sur la table, appuyant son menton volontaire sur ses poings fermés. Il se passa la langue sur les dents et fixa Lambert. Celui-ci reprit d’une voix calme.

« Une fois la saison passée, Osanne sera mon espouse et nous serons tous du même sang. La famille, c’est sacré.

— J’entends bien. Tu m’as appelé et il ne sera pas dit qu’Ernaut est déshonnête frère. Je le ferai pour toi.

— Pour les miens !

— Si tu veux… » souffla le géant.

Il se leva, faisant craquer son dos et remuant les membres engourdis après sa chevauchée. Il s’approcha d’une des fenêtres et regarda par la fente du volet partiellement clos. Un âne attendait sous la houlette d’un garçonnet devant la maison qui abritait le moulin, de l’autre côté de la rue. Au loin un chien aboya. Un groupe de pèlerins, les jambes pressées, s’avançait en murmurant des chants religieux. Un sourire s’esquissa sur le visage du jeune homme. Il fit face à Lambert, qui attendait toujours, accoudé à la table.

« Avant toute chose, il me faut me faire mon idée. Une affaire de meurtrerie n’est jamais aisée. Si ce que je trouve ne me plaît pas, je te le dirai. Au moins tu sauras à quoi t’en tenir.

— Je n’ai nulle crainte, ce ne peut être Godefroy. Il est peut-être rude, mais n’a nulle violence dans le sang.

— Tu serais étonné de ce que certains peuvent faire, Lambert. Sans même que cela ne trouble leur visage.

— Osanne m’a dit qu’ils étaient restés à dormir comme d’usage. J’ai foi en elle. »

Ernaut sourit à son frère.

« C’est bien le moins. »

Attrapant son fourreau, il en laça la ceinture et déclara d’une voix joyeuse :

« Allez, mène-moi donc chez ce fâcheux, que je voie ce qu’il en est de cette histoire. Peut-être y trouverons-nous quelque flèche de brigand turc ! »

Ragaillardi par l’entrain d’Ernaut, Lambert vida son gobelet d’un trait et le reposa sur la table, faisant claquer la céramique. Son cœur s’allégeait. Il était certain que d’ici peu tout serait résolu et oublié, et ne pensait qu’à l’avenir joyeux qui l’attendait avec Osanne. Ernaut saurait démêler l’écheveau. Pour une fois, il loua silencieusement la curiosité que son frère avait toujours cultivée, son vice s’avérant en l’occasion étrangement vertueux.

### Casal de la Mahomerie, mardi 10 juin 1158, début de soirée

La demeure d’Ogier était à quelques dizaines de pas plus bas, de l’autre côté de la rue. Comme ses voisines, elle n’offrait à la vue qu’une façade hâtivement chaulée, percée de rares fenêtres à l’étage.

Lorsque les deux frères tapèrent à la porte, leurs coups résonnèrent et attirèrent alentour les regards. Sous un figuier tordu, une poignée d’enfants interrompit un instant ses jeux, lançant des coups d’œil intrigués. Ils savaient que la maison était celle du mort et s’ils parvenaient à en savoir un peu plus, ils auraient l’occasion de parader auprès de leur famille et de leurs amis. Une tête se pencha hors une des ouvertures au premier. Visage rond, cheveux emmêlés, regard brun. Un jeune homme.

« Que voulez-vous ? »

Lambert leva la main, un sourire engageant sur le visage.

« Salut à toi, mon frère que voilà est sergent du roi. Il serait bon qu’il voie la demeure de maître Ogier. »

Le valet regardait alternativement l’un et l’autre, se mordant la lèvre. Il examina en détail Ernaut, s’attardant un long moment sur l’épée à sa hanche, la masse à sa ceinture. Il disparut dans la maison et quelques instants plus tard, ils entendaient la porte être débloquée. L’huis s’effaça et ils purent entrer. Le valet qui se tenait là était habillé d’une cotte trop grande pour lui, rapiécée en de nombreux endroits, élimée un peu partout. Il repoussa le panneau et assujettit la barre avec soin.

« On m’a dit de bien clore, des fois que certains aient tentation de venir à la chaparde.

— Voilà bon conseil. Que la rapine ne vienne en sus de la meurtrerie. On me nomme Lambert, je suis d’un peu plus haut…

— Oui, le Bourguignon, je vous connais répondit le jeune homme en hochant la tête.

— Je suis désolé, je ne sais plus le tien…

— Umbert, le valet. »

Sa voix s’éteignit. Il se tenait là, indécis, dansant d’un pied sur l’autre. La lueur hésitante qui perçait depuis la trémie d’escalier dessinait des rides sur son visage fatigué. Il semblait avoir été tiré du lit, un paquet de cheveux encore hérissés de sommeil. Autour d’eux, des paniers, du bois et des bouses séchées étaient entassés dans un coin, voisins d’un tas de cordes et de ficelles et d’une bâche soigneusement pliée. Derrière, plusieurs tonneaux et de gros contenants en céramique étaient alignés contre le mur. Enfin, un coffre laissé ouvert était empli d’outils, parmi un fatras de vieux manches. Des instruments agricoles étaient appuyés contre la paroi coupant en deux le soubassement voûté, sous l’escalier qui menait à la pièce de vie. De l’autre côté du passage central, des clayonnages supportaient les réserves de légumes mis à sécher, et plusieurs jattes enterrées ne laissaient voir que leur col, chapeauté d’un couvercle de bois. Une odeur chevaline flottait dans l’air et un léger bruit animal se faisait entendre.

« Vous voulez voir l’hostel en son complet ou juste… là où le maître a été occis ?

— Il ne serait pas inutile que je voie le tout. Tu nous montreras l’arrière quand nous repartirons » répondit Ernaut.

Umbert hocha la tête et les devança sur l’escalier raide. Il portait des chausses à étrier et marchait pieds nus, sans un bruit, glissant comme une ombre. Lorsqu’ils débouchèrent à l’étage, l’endroit leur parut bien clair par rapport à la resserre. Comme chez Lambert, le sol était de terre battue et si deux fenêtres donnaient sur la rue, une autre surplombait le courtil à l’arrière. Dans la vaste pièce, des coffres à serrure longeaient un des murs, une table et deux bancs avaient été tirés contre le mur de façade.

Sur le plateau, parmi des miettes de pain et des mouches, une écuelle sale jouxtait un pot où plongeait une cuillère. Une niche creusée à même la paroi accueillait la collection de pots et de pichets pour les repas. Enfin, plusieurs couvertures et des draps gris étaient grossièrement entassés sur une large banquette maçonnée le long du mur du fond, sous une longue perche horizontale. Ici et là, des escabelles rudimentaires servaient de desserte ou de siège, selon l’occasion. Umbert hésita à s’asseoir puis demeura à dévisager les visiteurs. Ernaut promena longuement son regard, s’avançant dans l’endroit comme s’il était chez lui. Il attrapa un des pots, et l’examina en reniflant. Puis il se tourna vers le valet.

« Est-ce ici qu’on a retrouvé ton maître ?

— Oui. Il était juste là confirma le jeune homme en montrant une tache sombre proche du lit.

— Tu as bougé des choses ?

— J’ai juste nettoyé l’escabel qu’était tout poisseux. »

Ernaut fronça les sourcils et repéra rapidement un siège patiné d’un brun sombre.

« Celui-ci ? Mets-le donc ainsi qu’il était. Ensuite ?

— Y’avait un bout de corde tranchée, pleine de nœuds, roide de sang, je l’ai jetée au fumier. »

Ernaut hocha la tête plusieurs fois, incitant le valet à continuer son inventaire.

« Rien d’autre ?

— J’ai dormi sur le lit, mais le maître repoussait juste les draps au matin, les secouait de temps en temps et les reposait sans les plier plus avant. »

Ernaut réfléchit un instant, parcourant la pièce avec attention.

« Tu n’as pas vidé le pot d’aisance aussi ?

— Le maître n’en avait pas, il disait que ça puait trop. Y’a un appentis à l’arrière, dans le jardin.

— On l’a trouvé au matin, c’est bien ça ? »

Umbert acquiesça en silence, se dirigea vers un des bancs et s’y assit lourdement. En deux enjambées, Ernaut se plaça face à lui, jambes écartées, bras croisés. Lambert vint s’installer contre la façade, accoudé dans l’embrasure d’une des fenêtres.

« Peux-tu me narrer exactement ce que tu sais de cette meurtrerie ? »

Le jeune homme haussa les épaules. Une main solide, marquée de corne entre le pouce et l’index, vint gratter la pommette. Il se frotta le nez et se racla la gorge.

« Quand que vous voulez que je débute ?

— Depuis hier soir.

— On a fait corvée d’eau, avec une des bêtes. Une des grandes jarres en bas sert de citerne. Pis on a raclé un peu l’écurie. Avec ça, le soleil avait passé les collines et je suis allé me coucher aux champs.

— Sans même souper ?

— J’ai mangé à la cabane, dans les terres. Comme on y laisse des outils, le maître craint les voleurs. Alors j’y dors. »

Ernaut commença à faire les cent pas, laissant errer son regard sur des détails insignifiants puis revenant fixer Umbert lorsqu’il le questionnait.

« Et au matin, tu viens ici ?

— Des fois oui, des fois non. Je savais quoi faire, j’ai commencé à épierrer le terrain comme prévu, mettant les beaux moellons de côté pour les murs.

— Tu es rentré quand alors ?

— Un des gamins du Provençal est venu me dire qu’on avait trouvé le maître moribond.

— Le Provençal ?

— Le voisin » indiqua Umbert en montrant du pouce une des maisons adjacentes.

Ernaut s’avança jusqu’à la marque sombre au sol, s’agenouilla et y posa la main, comme si la terre allait lui révéler un secret. Sans se retourner, il demanda :

« Tu n’as rien remarqué de spécial quand tu es entré ? Par rapport à hier soir ?

— Il y avait plein de monde, là-dedans, vous savez. Ça piaillait, pire que poulets en basse-cour. »

Ernaut alla examiner le vêtement posé sur la perche près du lit. Une chausse, fatiguée.

« Elle est à ton maître ?

— Oui-da. C’est celles qu’y portait hier.

— Il n’a donc qu’une guibole ?

— Non, pourquoi ?

— Je ne vois là qu’une des jambes… »

Le sergent revint s’installer à la table et joua machinalement avec les miettes un petit moment.

« Il range où le pain ton maître ? »

Umbert désigna du menton un coffre sans ferrure, qui pouvait aussi servir de pétrin.

« C’est toi qui as rangé la miche ?

— Comment que vous savez ça ?

— Je serais resté toute la journée à attendre ici, j’en aurais mangé. Chez moi aussi, on n’aimait pas que le pain reste dehors. »

Le jeune valet hocha la tête et se plongea dans ses pensées, vite interrompu par Ernaut, montrant une céramique de la niche.

« C’est comme la lampe. Ton maître n’en avait qu’une ?

— Oui.

— Tu l’as prise où ?

— Elle était posée sur l’escabel que j’ai lavé. »

Ernaut toussa, se releva. Il inspira longuement et lança un regard interrogateur à Lambert avant de se tourner une nouvelle fois vers Umbert.

« Montre-nous donc la salle arrière et le jardin. »

Le valet descendit sans bruit l’escalier. Il les mena dans la pièce servant d’écurie et alla droit à la porte, qu’il ouvrit pour faire entrer le jour. Le soleil affleurant les reliefs occidentaux éclairait la pièce d’une lumière rasante. Lambert et Ernaut y découvrirent des stalles, avec trois ânes au pelage clair.

Indifférents aux hommes, ils mangeaient leur foin en se disputant les brins avec vigueur. Près de la porte, un petit foyer était couvert d’une cloche de terre, à côté d’une banquette de pierre où des pots étaient rangés. Face aux stalles des animaux, un peu de paille et de foin était entassé. Ernaut avait remarqué que la porte se fermait, comme devant, d’un chevron.

« Tu sais quelle barre était tirée quand ils ont trouvé ton maître au matin ?

— Celle-ci ne l’était pas.

— Comment le sais-tu ?

— J’ai entendu maître Aymeric dire qu’ils étaient entrés par ici. »

Ils sortirent dans le petit jardin à l’arrière. Ceint d’un muret de trois à quatre pieds de haut, il était assez soigné, quoique des parcelles en demeurassent en friche. Un appentis jouxtait la maison, à côté du tas de fumier et des latrines. Ernaut s’avança, alla jusqu’au portillon en bout et le poussa. Des sentiers en partaient, à destination des cultures. Des hommes, la houe à l’épaule, portant un brancard ou menant une bête s’en revenaient de leur journée de travail, le dos courbé. Le long du chemin, sous un aliboufier[^aliboufier] au tronc noueux orné de mille fleurs blanches, quelques gamins jouaient avec des personnages en bois. Un des petits garçons risqua un œil admiratif vers Ernaut. Le nez collé de morve était maculé de poussière grise, et le regard clair, vif allait de l’épée aux vastes épaules du colosse. Il n’avait pas plus de cinq ans. Ernaut lui sourit, s’attirant instantanément les bonnes grâces de l’enfant, ainsi qu’une timide question.

« Dis, tu es chevalier ?

— Certes pas, je ne suis que sergent du roi, gamin.

— Oh ! T’es là à cause de maître Ogier ? »

Ernaut acquiesça gravement.

« C’est mon père, Guillaume, qu’a prévenu ! déclara fièrement le gamin.

— Voilà homme de raison que ton père ! Tu demeures où ?

— Juste là, montra le garçonnet en pointant la demeure qu’Umbert avait désignée comme étant celle du Provençal.

— Tu étais avec lui ?

— Ah non ! C’est pas affaire d’enfançons qu’y disent. Et pis, maître Ogier, il aimait pas qu’on entre en son courtil.

— Il n’était pas gentil ? »

Le gamin confirma de la tête, heureux d’ainsi défier la mort en critiquant un défunt.

« Y râlait tout le temps ! »

Puis il détala comme un lapin, rejoignant l’abri de son propre jardin et abandonnant les autres enfants au sergent. Ils ne surent rien ajouter, moins curieux et plus effrayés. Une petite fille, sœur du premier confident, lui indiqua néanmoins que la veille, à la nuit, elle avait vu de la lumière par-dessus le mur lorsqu’elle s’était levée pour entrebâiller le volet. Mais c’était l’habitude de maître Ogier de se soulager dans ses latrines. Lorsqu’Ernaut retrouva Lambert et le valet, ils discutaient de travaux agricoles au milieu des parterres de légumes.

Avant même qu’il n’ait le temps de les interrompre, le gamin enthousiaste était revenu et criait depuis le chemin :

« Les Bourgeois de la cour sont là ! Ils vont voir la dépouille de maître Ogier ! »

Sautant et bondissant, il avait déjà porté son message à une demi-douzaine d’autres maisons avant que les trois hommes aient eu le temps de quitter le jardin.

### Casal de la Mahomerie, mardi 10 juin 1158, soirée

Le ciel s’assombrissait rapidement au-dessus de l’attroupement amassé au centre du casal, non loin de l’accès à la curie du Saint-Sépulcre. Les hommes, après leur journée de labeur, s’asseyaient sur les murets délimitant le chemin bordé de filaires[^filaire].

Quelques chiens couraient dans la poussière, amusés de voir un tel attroupement, inhabituel à cette heure. Le sifflet d’un berger résonna tandis qu’apparaissait un troupeau de moutons sur une des crêtes sud.

Fendant la foule, Ernaut et Lambert cherchaient Godefroy, le père d’Osanne. C’était un petit homme aux épaules épaisses, doté d’un cou noueux. Son visage carré n’était plus orné que d’une couronne de cheveux gris prolongée d’une barbe argentée. Le bout de son nez camus surplombait une bouche dont on aurait dit qu’elle n’avait pas de lèvres.

Son œil unique, quasi inexistant à force de demeurer plissé sous le soleil de Palestine, était semblable à un bloc de charbon. L’autre orbite était cachée sous un morceau de cuir brun. Il s’entretenait avec un petit groupe, secouant la tête de façon solennelle aux déclarations d’un de ses compagnons. Son faciès rebutant se détendit en voyant les deux hommes arriver.

« Ah ! Lambert ! J’ai grande aise que ton frère soit des nôtres. »

Ernaut salua d’une rapide inclinaison de la tête, de façon plus marquée à l’intention de Godefroy, qui ferait bientôt partie de sa famille. Lambert afficha un sourire forcé.

« Nous sortons de chez Ogier.

— Tu as déjà idée de qui aurait fait le coup, Ernaut ? demanda le vieil homme, avec un entrain peu naturel.

— Non. Je pense assavoir le moment où la meurtrerie s’est passée, mais il me manquait la maîtresse pièce pour comprendre les choses.

— Quoi donc, sergent ?

— La dépouille. On peut y trouver indices ainsi que d’une bête tuée en plein champ. Pour elle on devine qui, du loup, du renard ou du chien sauvage a rompu la gorge, et comment elle est passée. »

Les présents hochèrent la tête en silence, respectueux du savoir d’un homme qu’ils espéraient d’expérience. Ernaut se tourna vers la curie, gros bâtiment à hauts murs de pierre, où le Saint-Sépulcre stockait le produit de ses dîmes, rentes, loyers et redevances. Symbole de son autorité, une haute et large tour s’élançait depuis la cour. Au sommet, les couleurs du royaume de Jérusalem proclamaient ce territoire sous l’autorité du roi Baudoin.

C’était lui qui possédait le droit de haute justice sur ce lieu, par le biais de la cour des Bourgeois de la cité toute proche. Ernaut appartenait à la même administration.

« Sait-on quels jurés ont fait le voyage ?

— Apparemment ils sont deux : un pas très grand, maigre comme Job, brun de poil, avec une barbe soignée, répondit Godefroy.

— André de Tosétus, certainement. L’autre ?

— Plus grand, et plus gros aussi, avec une barbe digne du grand Charles. Il portait un petit bonnet sur ses cheveux frisés et…

— Maître Pierre de Périgord, il est fort ami de Tosétus. »

Lambert leva le menton en signe d’interrogation.

« C’est bon pour nous, ça ?

— Bon pour quoi ? Ils ne sont là qu’aux fins d’entendre ce que chacun a à dire. Ils ne prendront aucun parti. Le sang a été versé, ce ne peut se juger en audience simple. »

Godefroy grimaça, indiquant d’un rapide coup de tête un gaillard dégingandé, qui s’agitait en tout sens. Autour de lui, de nombreux hommes échappaient de temps à autre un regard empli de défiance vers leur petit groupe.

« J’ai ouï dire que le Grand Aymeric est fort courroucé, il répand son ire comme loup sa rage. »

Un long grincement suspendit toutes les conversations en cours. Le grand portail de la cour s’ouvrait largement. Par-delà le passage, un valet fit signe à l’assemblée d’approcher. La foule se déplaça lentement, les deux groupes dansant l’un autour de l’autre sans jamais se mêler, baignés parmi la masse des inquiets et des curieux.

Devant la tour, deux sergents du roi se tenaient de part et d’autre de l’escalier qui menait à la porte d’accès, au premier. Sur la plateforme, les jurés de la cour, qu’Ernaut confirma être les bourgeois qu’il pensait, attendaient, l’air solennel. Derrière eux se tenait un homme voûté, dont l’appendice nasal était si imposant qu’on en oubliait le reste du visage. Il était habillé d’une cotte longue de qualité, mais fort ancienne, plusieurs fois reprisée. D’en bas, on ne pouvait voir sa tonsure discrète, petit disque de peau hâlée au sommet du crâne pointu.

C’était Pisan, le frère du Saint-Sépulcre chargé du casal. Il n’avait pas à juger en l’affaire, mais s’affichait en vertu de la reconnaissance de son statut. Les sergents au pied de l’escalier sourirent à Ernaut, surpris de le voir là, parmi les justiciables. Lorsque la foule se fut approchée, sans se regrouper pour autant, le plus gros des officiels amena le silence en quelques appels puis prit la parole d’une voix forte, habituée à de telles assemblées.

« Habitants du casal de la Mahomerie ! J’ai nom Pierre de Périgord et voici André de Tosétus. Nous venons en votre lieu pour cause de mort. Est-ce là simple meurtrerie ou y-a-t’il quelque vilenie en l’affaire ? »

Une voix forte monta en réponse :

« Il a été occis nuitamment ! Les membres liés !

— Ah. L’un de vous a-t-il vu son murdrier ? »

Personne n’osa répondre, mais un murmure, vague haineuse, gonflait et roulait dans le public autour d’Aymeric le Grand. Pierre de Périgord se gratta la tête et poursuivit.

« Parmi vous, y en a-t-il qui veulent demander justice pour cet homme ? »

La question était à peine posée que la même voix se fit de nouveau entendre :

« Oui, moi je le veux.

— Avance-toi et nomme-toi.

— Je suis Aymeric le Grand, du quartier du Puits de la Vierge. Je demande justice pour Ogier.

— Es-tu de sa parentèle ? Son frère peut-être ?

— Il m’était attaché ainsi qu’un frère, de certes, mais le sang ne nous liait pas. Nous préparions son mariage avec ma fille et seule sa mort nous en empêchera. »

Le juré se tourna vers son collègue, visiblement ennuyé. Ils palabrèrent un instant à voix basse puis Pierre de Périgord fit de nouveau face à la foule.

« Je ne suis acertainé que tu puisses te faire témoin du moribond, mais considérons que oui pour l’instant. Que demandes-tu pour lui ?

— Je vous demande justice, car je connais la main qui a frappé et je veux que le coupable subisse juste châtiment.

— Tu en seras témoin en la cour du vicomte ?

— Je le serai et, si besoin, d’autres voix s’uniront à la mienne pour en jurer. »

Les deux notables tinrent un rapide conciliabule avec l’intendant Pisan et annoncèrent que la cour de justice serait présente le lendemain matin, et que tous ceux qui voudraient être entendus ou avaient une part dans l’affaire devraient être présents, avec leurs témoins. La foule se désagrégea et ce fut par petits groupes bavards que chacun reprit le chemin vers sa demeure. La soirée allait être animée de conversations passionnées, d’argumentations verbeuses, baignées d’inquiétude sourde. Lambert se rapprocha de Godefroy.

« Êtes-vous acertainé qu’il va porter accusation contre vous, père ? »

Ernaut tiqua au titre que Lambert accordait au vieil homme, comprenant qu’il lui indiquait par là son total soutien.

« Il a vociféré contre moi dès le moment où la dépouille ensanglantée a été trouvée. Je ne vois guère qui il pourrait vouloir accuser d’autre.

— Simple désir ne suffit pour faire pareille chose.

— Mon logis est voisin de celui d’Ogier, il prétendra qu’il m’aura été facile de passer là-bas nuitamment et de lui faire son affaire. Tout le monde sait très bien que nous n’étions d’accord en rien. »

Ernaut toussa pour attirer l’attention sur lui et répliqua: « Tout de même, risquer jugement par bataille, cela ne peut se décider sur une simple idée.

— Nous avions si souventes fois querelle avec Ogier que tout le casal pourrait en témoigner, même ton frère. Ogier avait le sang chaud et je ne peux nier m’être quelques fois emporté à cause de ses manigances.

— Personne ne peut témoigner de ce que vous n’êtes pas sorti de chez vous cette nuit ?

— Ma fille Osanne, mon épouse Perrotte. On ne les écoutera guère » admit le vieil homme, un rictus sans joie plissant ses joues.

Lambert posa une main amicale sur Godefroy, l’air rogue.

« Si Aymeric peut se prétendre témoin d’un homme qui n’était pas encore uni à sa fille, je dois bien pouvoir être celui de qui sera le père de mon épouse. »

Le vieil homme stoppa un instant, le temps de dévisager Lambert, visiblement touché par cette déclaration. Ernaut, pour sa part, arborait un visage contrarié. Seule l’obscurité naissante du crépuscule empêchait d’y lire toute la désapprobation qu’il ressentait. Ils continuèrent à remonter la rue principale, quittant la partie basse du casal pour retrouver le plateau, auprès de l’église. Dans cette zone, plusieurs habitants sourirent ou saluèrent discrètement le trio tandis qu’ils avançaient. Aymeric et Ogier n’étaient pas si populaires autour de Sainte-Marie, là où les colons le plus récemment arrivés dans le village étaient installés.

Arrivé devant chez lui, Godefroy stoppa. Il ne put se retenir de porter l’œil vers la bâtisse voisine. Une lueur tremblotante était visible à l’étage, le jeune valet s’y était installé pour la nuit, de façon à surveiller les affaires de son défunt maître. Godefroy soupira et sourit aux deux frères puis il poussa la porte de sa demeure, qui céda en gémissant.

« Je passerai avec un pichet de vin tantôt, si tu veux bien, Lambert. Osanne aura certainement désir de te voir à la veillée. »

À la mention de ce nom, le frère d’Ernaut ne put réprimer un sourire. Il acquiesça et souhaita un agréable souper au vieil homme qui s’engouffra chez lui. Lambert et Ernaut se retrouvaient seuls dans la rue. La lune se montrait timidement et l’obscurité avait mangé les ombres, recouvrant le hameau de ténèbres, seulement vaincues aux fenêtres par la chaleur ambrée des lampes à huile. Lambert fit signe de la tête à son frère et avança vers son logis, à quelques dizaines de pas de là. Il huma l’air et se sentit obligé de dire quelque chose, de s’expliquer.

« Je vois bien que tu n’apprécies guère que… »

Ernaut lui avait posé la main sur le bras et déclara d’une voix sourde :

« Buisson a oreilles, bois écoute… Il n’est pas bon de pérorer ainsi en pleine rue, Lambert. Attends donc que nous soyons en ton hostel.

— Je n’ai rien à cacher, voyons !

— Si tu savais tout le mal qui peut naître de quelques paroles, tu ne dirais pas cela, frère. »

Il laissait errer ses yeux de droite et de gauche, sans bouger la tête, comme s’il s’attendait à voir jaillir un démon de derrière un buisson, un abreuvoir ou un des arbres de la rue. Il savait que ces hideuses créatures s’embusquaient toujours là où on les attendait le moins. Il le savait pour en tenir un, droitement retenu, en son propre cœur.

### Casal de la Mahomerie, mardi 10 juin 1158, veillée

Les deux hommes demeurèrent silencieux jusqu’à ce que la barre de la maison soit tirée derrière eux. Lambert avait pris soin de laisser sur une banquette de pierre une petite lampe à huile comme veilleuse. Il en répartit la flamme à une autre céramique, de plus grande taille, puis se dirigea vers l’arrière, suivi d’Ernaut. La monture du sergent était installée dans le jardinet prolongeant la maison et un peu de foin lui avait été donné. Ernaut tira la porte et alla flatter l’animal.

Il restait encore un peu de fourrage et quelques broussailles pourraient compléter son repas. Le seau d’eau était à demi-plein. Le jeune homme leva la tête, admirant les reliefs environnants. La lune, plus tout à fait pleine, avait dépassé les monts et éclairait désormais d’une lumière bleutée le paysage. Des flammes se devinaient au loin, camps de nomades ou feux de berger. Des voix étouffées résonnaient dans les maisons alentour et les lueurs tremblotantes de lampes scintillaient aux fenêtres. Un chacal poussa un cri au loin, vite rejoint par plusieurs camarades. À côté, Lambert s’employait à préparer quelques braises pour le repas. D’un mouvement de tête, il montra la lampe.

« Prends donc ce feu et va tirer un pichet du tonneau en perce, dans le coin. »

Tandis que le filet glougloutait en remplissant le broc, Ernaut vit jaillir les flammes. Bientôt, une fumée piquante, âcre, se fit sentir et Lambert entr’ouvrit plus largement la porte pour qu’elle puisse s’échapper. Il disposa quelques bûchettes et mena son frère à l’étage. Là, il sortit d’une maie une grosse miche de pain entamée, du fromage frais, des fruits. Un morceau de lard séché se vit amputé de quelques fines tranches.

« Je vais les griller avec des oignons. Cela te conviendra ?

— Parfait pour moi, frère.

— En ville, tu as coutume de pâtés et chairs cuites, tourtes et beignets, mais ici tu n’auras droit qu’à cuisine de vieux garçon. »

Ernaut sourit, occupé à emplir d’huile une autre lampe.

« Il n’est que temps pour toi de trouver une espousée qui s’occupe de la maison.

— À qui le dis-tu ! Je n’arrive à être aux champs et dans le cellier. Heureusement que les voisins m’assistent. »

Il redescendit préparer le souper, laissant Ernaut accoudé à la table, un peu fatigué. Il avait encore chaud de la journée et regrettait les bains de Jérusalem. C’était une des pratiques orientales à laquelle il s’était le plus vite habitué. Enfant, il avait pu rester des journées sans se laver complètement, se contentant d’une toilette rudimentaire. Mais dans la Cité, il était si facile, et peu cher, de se rendre dans un lieu où l’on pouvait se frictionner, se faire masser, couper les cheveux, raser la barbe, qu’il y avait pris goût. Il était donc bien plus sensible qu’auparavant aux odeurs et celles qu’il dégageait en cette fin de journée ne lui plaisaient guère.

Lambert habitait en haut du casal et il fallait faire plusieurs centaines de pas jusqu’à la source pour emplir les réserves. Son frère prévoyait de se doter d’une citerne, comme certaines maisons. Les fortes pluies de l’hiver pourraient ainsi éviter les corvées d’eau. Mais un bain demeurerait un luxe qu’il ne pourrait s’offrir. Ernaut se leva, alla à la fenêtre, poussa le volet de bois pour laisser entrer la fraîcheur de la nuit. La rue était calme, chacun était chez soi. Des chiens errants vadrouillaient, vérifiaient si des trésors ne se dissimulaient pas dans des tas d’ordures.

L’arrivée de Lambert, accompagné des effluves du repas, le tira de sa rêverie. Ils s’installèrent et, très vite, le battement des mâchoires fut le seul bruit qu’on pouvait entendre. Ernaut souhaitait néanmoins s’entretenir avec son frère avant la veillée, avant que Godefroy ne soit là. Il remplit leurs gobelets de vin et tailla d’épaisses tranches de pain pour s’y resservir de la compote d’oignon et du lard avant de prendre la parole.

« Dis-moi, Lambert, es-tu bien sûr de vouloir être témoin ?

— De certes ! Godefroy sera bientôt mon père. La famille doit se tenir.

— Cela, j’entends bien. Mais le grand, là, semble déterminé à demander jugement par bataille. Ce n’est pas un vieux borgne qui pourra l’affronter. »

Lambert reposa sa cuiller.

« Je le sais, Ernaut, j’y pourpense depuis un moment. Mais je ne peux laisser le père d’Osanne seul.

— Tu n’es pas fort combattant, tu ne l’as jamais été.

— Aymeric non plus. De plus, c’est Dieu qui donne la victoire, pas le bras de l’homme. »

Ernaut fit une moue, avala quelques bouchées, pensif.

« Il désigne l’innocent et le coupable, certes. En ce cas, il faut assavoir qui est qui en cette histoire…

— Godefroy n’est pas murdrier ! répliqua sèchement Lambert.

— Tu es prêt à jouer ta vie sur cela ?

— Sans hésiter un seul instant… Il se mordit la lèvre. Outre, Osanne ne comprendrait pas que je me défie de son père. Elle semble fort l’estimer et je le pense homme de droiture. »

Ernaut avala une large bouchée de pain et d’oignon, la mastiqua longuement, observant tour à tour son frère et la lampe entre eux deux.

« Ce serait peut-être mieux que je me porte témoin. Je pourrais ainsi faire bataille. J’ai bien appris depuis que je suis de la sergenterie…

— En aucune façon, le coupa Lambert. Ce n’est pas à toi d’aller en champ clos. »

Il fronça les sourcils, léchant sa cuiller avec soin, comme si du miel y demeurait collé. Puis souffla avant de reprendre d’une voix hachée, hésitante.

« Celui qui ira à bataille devra demeurer enfermé jusque-là et je pense que tu pourrais mieux t’employer.

— À quoi penses-tu ?

— Je ne crois pas Aymeric mauvais homme. Il est juste empli de colère, car Ogier était son ami et allait devenir son fils. Il faudrait lui montrer qu’il se trompe.

— S’il se fait occire en combat, il saura bien assez sa leçon.

— Justement, si l’on pouvait le convaincre de se dédire avant cela, je préférerais. Je n’ai certes pas désir de finir… Enfin, de perdre. Mais je ne crois pas qu’il serait bon qu’Aymeric encontre la mort non plus.

— Il n’a qu’à réfléchir avant d’agir. »

Lambert esquissa un sourire narquois.

« C’est bien toi, Ernaut, qui dit pareille chose ? Il marqua un temps, puis continua. C’est un homme de valeur, qui parle bien, et qu’on écoute au quartier du Puits de la Vierge. Je n’ai nulle colère contre lui. Je suis acertainé que si on lui montre son erreur, de bonne foi il saura s’amender. Tout le monde y gagnera. La vérité sera connue et nous serons saufs tous les deux. »

Ernaut grogna, refusant d’acquiescer aussi facilement, même s’il reconnaissait la justesse des arguments. Lambert, de plusieurs années son aîné, avait toujours été plus réfléchi. Il n’était pas guidé uniquement par son sens du devoir ou les sentiments qu’il éprouvait pour sa future femme. Ernaut se pourlécha une dernière fois, les yeux penchés sur son écuelle vide et les reliefs de son repas, jouant avec les miettes d’un doigt distrait.

« Tu veux que je piste le tueur tandis que tu attendras le jugement ?

— Oui. Tu as la science de ces choses. Finalement, si Dieu t’a fait si curieux, c’est qu’il devait avoir ses raisons. Emploie tes talents à faire jaillir la vérité et empêche cette bataille d’avoir lieu. »

Ernaut hocha la tête, réfléchissant à la façon dont il allait s’y prendre. Il lui faudrait demander congé au mathessep pour les quelques jours à venir. De toute façon, normalement, d’ici la fin de la semaine, tout serait terminé, en bien ou en mal. Il était rare qu’on attende plus de quelques jours pour régler un tel différend. Lorsqu’il entendrait la messe dominicale, l’histoire serait achevée.

« Je t’ai entendu, frère. Je ferai ce que tu me demandes, si on me relève de mes tâches les jours qui viennent, et je traquerai le félon qui a occis cet Ogier. Tu peux compter sur moi. »

Lambert sourit et leva son gobelet, scellant d’un toast silencieux la promesse de son cadet. Puis il débarrassa la table sans plus prononcer un mot. Plusieurs coups résonnèrent sur la porte d’entrée et il descendit ouvrir. Au retour, il était suivi de Godefroy et d’Osanne, les bras chargés d’un pichet, certainement de vin épicé, et d’une jatte emplie de beignets à l’alléchante odeur sucrée. Ernaut en eut les narines qui frémissaient. La jeune femme affichait une bonne humeur peu naturelle, un peu gauche. Elle se tenait, son plat à la main, ne sachant que faire.

De sa longue natte s’échappaient, comme souvent, quelques cheveux frisés indociles. Son visage massif, orné d’yeux sombres était illuminé par son sourire, à l’intention de Lambert. Sa tenue, une ample cotte de travail, la recouvrait ainsi qu’un sac, sans forme ni coupe la mettant en valeur.

Ernaut s’était longtemps demandé ce que Lambert pouvait lui trouver. Puis il avait fini par apprécier la jeune fille, dont la voix claire et calme correspondait bien au caractère de son frère. Elle prit place sur le banc à côté de lui, laissant Godefroy s’installer près d’Ernaut. Le vieil homme s’efforça de montrer un peu d’entrain et excusa sa femme, fort fatiguée. Après avoir empli les verres, chacun se mit à déguster en silence le vin miellé et épicé, personne n’osant prendre la parole. Ce fut Ernaut qui se décida au final, tourné vers Godefroy.

« Maître, avez-vous idée de ce qui trotte dans la tête de cet Aymeric ? Êtes-vous acertainé qu’il vous portera accusation ?

— Sans doute aucun. Il ne m’estime guère. Et sait qu’Ogier et moi nous opposions souvent.

— On se chicane avec voisins souventes fois, sans pour autant leur porter le fer en la poitrine.

— Tu dis vrai, mais il est connu que je m’étais opposé à Ogier à plusieurs reprises et qu’il enrageait de simplement me voir.

— Cela fait de vous son ennemi, mais était-il le vôtre ? »

Le vieil homme plissa les sourcils, faisant disparaître son œil unique dans une fente de ténèbres. Il réfléchit un long moment.

« Je ne dirais pas cela, non. Je le trouvais cupide et autoritaire et m’opposais à lui, mais je ne lui souhaitais aucun mal.

— Que faisait-il qui vous chagrine ainsi ?

— Oh, moult petites choses. Il organisait les travaux de chacun, supervisait les réfections collectives des murets, distribuait corvées et tâches comme s’il était l’avoué du Saint-Sépulcre. Je ne m’y entends guère avec les fanfarons. »

Il avala un peu de vin, faisant glouglouter la boisson dans sa bouche avant de poursuivre.

« Il avait beau jeu de se faire bien voir au quartier du Puits. Il s’y trouve anciens colons, fils de pèlerins nés ici. Lui venait d’un casal plus au nord. Il se targuait donc d’être homme de sapience, au fait de la terre d’ici. Mais moi aussi je connais les monts de Judée. J’ai œuvré en Samarie, j’ai taillé l’olivier pendant des années. Je n’ai pas leçon à recevoir sur la façon d’œuvrer en mon bien. »

La dernière phrase avait été prononcée de façon si péremptoire, si passionnée, qu’Ernaut n’osa poser une autre question. Ce fut Osanne, cette fois, qui prit la parole.

« Nous sommes si heureux de voir que vous êtes à nos côtés en cette épreuve. Nombreux dans ce casal sont ceux qui savent demander assistance à mon père quand ils ont besoin. Mais aucun n’est venu ce soir. »

Elle tourna son regard vers Lambert.

« Je sais que tu es homme de bien et c’est pour cela que je m’enjoie à l’idée de… Elle rougit légèrement. À l’idée de t’épouser. Ce que tu fais là vaut tous les douaires[^douaire] du royaume. Pourtant j’ai grande crainte face à ce qui nous attend. »

Ernaut fit claquer sa langue en dénégation, examinant le fond de son verre. Son regard était dur et sévère et sa voix se fit autoritaire.

« Nenni. Si quelque personne doit s’enfrissonner ce soir, c’est le mécréant qui a frappé Ogier. Car je vais le traquer, l’empoigner et le faire pendre. »

Puis il montra les dents en un sourire sans joie.

### Casal de la Mahomerie, mercredi 11 juin, matin

Les aboiements excités d’une meute de chiens réveillèrent Ernaut en sursaut. Le soleil était déjà levé et la couche de Lambert, à côté de lui, était déserte. Il s’étira lentement, pestant contre les maudites bêtes qui l’avaient ainsi inquiété. Simplement habillé de sa chemise, il s’avança jusqu’à une des fenêtres et fit entrer le jour. Il cligna des yeux devant la forte luminosité. La rue était bruissante de clameurs. Un groupe de pèlerins s’éloignait en chantant. Un âne leur fit écho depuis une bâtisse proche. Sur le toit en face, deux hommes installaient des claies pour y mettre des fruits ou des légumes à sécher. Du bruit dans la pièce le fit se retourner. De retour, Lambert lui souriait, tout en puisant de la farine depuis une maie.

« Je vais porter cela à la voisine, il va y avoir une chauffe du four et nous avons besoin de pain. Elle nous fera des miches en même temps que les siennes.

— Tu es déjà en train ? s’inquiéta Ernaut, voyant que son frère était revêtu de sa cotte, de ses chausses et portait ses souliers.

— Je suis allé sortir les poules et j’ai encontré la femme du Catalan. Elle me rend souventes fois menus services. »

Sur la table, dans une jatte, une demi-douzaine d’oeufs avaient été posés, encore collés de paille et de crasse. Ernaut se racla la gorge, s’avança pour s’habiller à son tour, empoignant ses braies et ses chausses encore suspendues à la perche au-dessus du lit. Pendant ce temps, Lambert sortit un panier et y disposa un bon morceau de pain, du fromage et des fruits.

« Où comptes-tu aller ?

— Je vais montrer travaux à faire sur ma parcelle au Catalan. Il y a quelques zones à sarcler et un des murs demanderait un peu de renfort. »

Il s’interrompit, hésitant à rappeler à voix haute en ce beau matin ce qui avait été décidé nuitamment.

« Il est coutume de tenir les batailleurs enclos jusqu’au jugement, non ?

— De fait ! confirma Ernaut, les lèvres pincées.

— Il me faut donc apenser à ce que mon bien soit entretenu en ce temps. Le courtil derrière est en jachère, il a déjà donné pois et fèves. La plupart de mes calebasses sont dans le bas courtil irrigué, au près de la source en contrebas du village. Le voisin saura fermer les poules au soir et les déclore au matin.

— Tu es donc toujours décidé ?

— Comment pourrais-je me dédire, Ernaut ? Si tu ne peux compter sur la famille, tu es sans feu et sans lieu. J’ai foi en la Vérité du Seigneur. »

Ernaut se détourna, boutonnant ses souliers sans plus un mot. Voyant que son frère demeurait là, au milieu de la pièce, le sac de farine à la main, une trainée poudreuse sur la joue, il soupira.

« Il me faut mener ma monture à la source, qu’elle boive.

— Nous nous verrons à la curie tantôt ?

— Certes ! Il me faut voir si je peux obtenir congé afin de lancer la chasse. Moi non plus je ne laisse pas la famille. »

Lambert réalisa qu’Ernaut ne désapprouvait pas son geste, qu’il était seulement inquiet. Il tendit la main, comme pour le toucher.

« Ernaut… »

Mais son géant de frère s’était déjà avancé vers l’escalier, la mine sombre, le fourreau de son épée et sa masse en main. La porte sur le jardin claqua quand il la referma derrière lui. Ernaut trouva sa bête les yeux clos, une jambe pliée, en train de se reposer. Il la flatta avec une telle vigueur qu’elle sursauta. Il arracha quelques brins d’herbe jaune et les lui donna. Peu susceptible, l’animal accepta le repas en guise d’excuses et frotta son chanfrein contre l’épaule.

Ernaut détacha la bride et le mena derrière lui jusqu’au passage longeant l’arrière des maisons avant de rejoindre la route principale. Malgré l’heure matinale, le soleil régnait déjà, impitoyable, en son royaume d’azur. Des hommes, les bras chargés d’outils agricoles, déambulaient, protégés par leurs chapeaux de paille. Certains saluèrent Ernaut et l’un d’eux alla jusqu’à caresser son cheval.

« Belle bête, elle a quel âge ?

— Je ne saurais dire, je la loue seulement, mais je pense dans les cinq ou six ans.

— Elle a bonne croupe et jambes solides ! Pareille bête fait la richesse de son maître.

— Je ne suis pas sûr qu’elle sache tirer ou bien porter. Elle m’accepte sur son dos, c’est déjà fort bien. »

L’homme sourit.

« Tout ce qui porte bât soulage le dos ! Tantôt, Pons, le meunier, était venu avec ses ânes pour aider à porter les moellons quand on a maçonné le puits.

— Vous êtes là depuis longtemps ?

— Pas tant. Le casal n’est guère vieux. Même ceux d’En-Bas ne sauraient prétendre le contraire. Je suis arrivé avec le roi Louis pour ma part. Je suis Robert le Francien. »

Il tendit la main à Ernaut, que ce dernier serra chaleureusement. C’était là un signe de soutien manifeste, d’autant qu’il était fait dans la rue, au su et au vu de tous. L’homme continua, avançant tranquillement à côté du sergent et de sa monture.

« J’ai d’abord œuvré un peu comme soldat, mais je n’étais guère doué. Plus utile à porter l’eau et le bois, le foin et le grain. Puis j’ai encontré ma dame et on a décidé de s’installer là, voilà une poignée d’années. On y a retrouvé d’autres Français, ce qui m’a enjoyé le cœur. Vous êtes Bourguignons, ton frère et toi, c’est bien ça ?

— De Vézelay.

— Je connais pas, mais Bourguignon et Français, c’est tout un. Il faut se tenir les coudes. »

En disant cela, Robert lança un regard de défiance vers le bas du casal. Ernaut fronça les sourcils.

« Il y a donc des soucis avec certains ?

— Je dis pas qu’on s’esbigne pas avec les Provençaux, les Normands ou les Lombards. Mais on a tous trop souffert en chemin pour ne pas reconnaître ceux qui ont sué de même. Les gens du Puits de la Vierge, ils se croient meilleurs que nous.

— Pourquoi donc ?

— Pourquoi donc ? reprit Robert, l’air indigné. Mais parce que voilà beau ramassis de fainéants, tout juste bons à accaparer les meilleures terres. Ils sont là depuis si longtemps qu’ils en oublient ce que c’est d’être loyal chrétien. Les pires, c’est ceux nés ici. Le fils d’un goret né en un hostel devient pas enfançon… »

L’homme cligna de l’œil, amusé par sa propre plaisanterie. Ils arrivaient à un embranchement, un des chemins longeant la pente vers l’ouest. Il salua gaiement Ernaut et s’y engagea, sifflotant, sa houe à l’épaule. La pente se faisait plus forte, les regards moins amicaux, Ernaut le sentait bien. Alors que jusque-là les visages étaient au pire polis, il lui semblait ressentir une certaine méfiance, voire de l’hostilité. Un vieux boiteux le croisa, appuyé sur une canne, il était trop lent pour s’esquiver et escomptait sur l’animosité de son regard pour repousser l’intrus loin de lui. Peine perdue.

« Salut l’ancien ! »

L’homme grommela quelques mots, frottant ses gencives édentées contre ses lèvres. Ernaut s’approcha de lui.

« Il n’y a pas souci pour abreuver ma bête à la source ? »

Le visage ridé prit une expression vexée et son regard toisa Ernaut avec dédain.

« Y sera pas dit qu’on laisse mourir de soif une bête…

— Grand bien à vous ! Elle n’est pas mienne et je ne saurais dédommager son maître s’il lui arrivait quoi que ce soit.

— Les sergents du roi n’ont pas monture ? répliqua le vieillard, montrant de sa canne l’épée suspendue à la taille d’Ernaut.

— Je ne suis pas ici pour les affaires du roi, mais en visite chez mon frère.

— Le Bourguignon ?

— Oui, Lambert. Vous le connaissez ?

— Je peux pas dire qu’on ait échangé plus de dix mots, mais il est sérieux à la terre. Il a repris un bien en fort mauvais état.

— Ça ! Il est dur à la tâche, personne peut arguer du contraire. Vous avez vos champs vers les siens ?

— Oh non ! répliqua vivement l’ancien, horrifié. Ma terre est parmi celles du quartier du Puits.

— Ceux d’En-Bas ? »

L’homme hocha la tête.

« Enfin, c’est le bien de mon fils. Moi j’ai eu des terres un peu partout en ce royaume, mais c’est ici le mieux.

— Les cultures y sont belles, ça oui.

— Et les Turcs éloignés, comme les Égyptiens. Ces maudits païens ! On peut dire qu’y m’ont fait souffrir, eux autres, ça oui, par la jambe-Dieu ! On a bien mérité un petit bout de terre à nous, tranquilles. On nous doit bien ça. »

Ernaut acquiesça en silence, caressant la gorge de son cheval.

« Tu m’as l’air d’un bon p’tit gars, même si t’es pas d’ici. Tu sers le roi, et ça c’est bien. Le souci, c’est que tous les nouveaux, ils se croient tout permis. On était là avant eux, cul-Dieu ! La terre a bu not’ sang pendant qu’ils s’en venaient par les chemins. On n’a pas de leçons à entendre, on est maître de not’ bien… »

Une voix forte l’interpella soudain. Un gros homme, une doloire à la main appelait le vieux depuis une des bâtisses. Il salua Ernaut d’un signe de tête et s’éloigna en grommelant.

« T’as l’air d’un bon p’tit gars, pour ça oui. »

Le vieillard se dirigea vers le charpentier d’un pas traînant qui soulevait la poussière. Ernaut reprit son chemin et sortit bientôt du casal, retrouvant une centaine de pas plus bas les bassins de la source. Trois femmes battaient la toile sur un de ses bords, chantant d’une voix entraînante tout en assénant des coups réguliers. Il mena son cheval jusqu’à un abreuvoir sous un trop-plein. Les femmes se turent, continuant à tordre et battre avec vigueur. Appréciant la fraîcheur de l’eau, il s’en passa sur le visage et la nuque avant d’y plonger la tête. Il venait de se relever, soufflant, les yeux clos quand une voix rauque l’interpella.

« On dit que ton frère va se porter témoin pour le Borgne ? »

Ernaut se retourna et découvrit un petit homme au visage carré, le cheveu frisé assez court. Il était habillé d’une cotte de belle facture, mais usée par endroits. Il s’appuyait d’un côté sur le muret et de l’autre sur une hotte emplie de bouses séchées. Sa petite taille accentuait son aspect massif, ses bras musculeux. Il semblait aussi noueux, aussi solide que les troncs des vieux oliviers. Ernaut le dévisagea lentement, s’essuyant du revers du bras.

« En quoi ça te concerne, l’ami ?

— Il devrait pas faire ça, c’est pas son affaire.

— S’il épouse Osanne… »

L’homme fit un rictus vexé.

« Tout le monde sait bien que le vieux Godefroy était jaloux d’Ogier. Nous, on était tous amis avec lui, il était notre compaing.

— Tu veux dire qu’il y aura beaucoup de témoins pour lui ?

— Un seul suffit. Il devait épouser la fille d’Aymeric, il demandera justice pour son défunt parent.

— Godefroy est trop vieux, et avec un seul œil, il ne pourra pas entrer au champ clos, de toute façon.

— Eh bien qu’il se trouve un champion alors, mais je te donne conseil d’ami : dis à ton frère de laisser Godefroy. Il est coupable, et rien pourra le sauver. »

Ernaut fit la moue, esquissa un sourire.

« Tu sais que ce pourrait être moi le champion de Godefroy ? Es-tu bien sûr que ton compaing Aymeric préférerait m’affronter ? »

L’homme souffla, dépité.

« Tu comprends donc rien ? Dieu est avec nous, et lui non plus n’aime pas les corniauds du genre de Godefroy. »

### Casal de la Mahomerie, mercredi 11 juin, matinée

Les deux frères avaient retrouvé Godefroy à sa porte. Sa femme Perrotte et sa fille Osanne l’accompagnaient, le visage fermé et triste. Ernaut se fit la réflexion que le vieil homme avait tout du coupable prêt à affronter son châtiment. Il s’efforça de chasser cette idée noire. Devant l’église, une foule s’était déjà amassée. Chacun y allait de son commentaire sur les événements récents et les développements qu’on était en droit d’en attendre. Néanmoins, les voix se firent moins gaillardes à leur approche, à l’exception de celle d’Aymeric qui continuait à pérorer, en jetant désormais de temps à autre une œillade haineuse dans leur direction. À ses côtés se tenait le petit homme vindicatif qu’Ernaut avait croisé à la source. Il l’indiqua à son frère et lui demanda qui c’était.

« Gautier le Petit, il ne va si loin que son nez ne quitte le cul d’Aymeric.

— Ah ! Je comprends mieux pourquoi il voulait me convaincre de ne pas me mêler de cette histoire, tantôt.

— Ce n’est pas mauvais bougre, mais il ne dit pas plus haut que son compère et le suit en toute chose. À eux deux, ils se croient détenir la vicomté du Puits de la Vierge. »

Ernaut afficha un sourire. Il écartait désormais la foule pour s’approcher des marches d’accès. L’assemblée se faisait dans la nef de Sainte-Marie, seul endroit suffisamment solennel — et vaste — pour l’occasion. Sur le côté, attachée à un des anneaux, Ernaut reconnut une des belles montures du vicomte, qui présiderait en l’affaire. Autour, des amis à lui, sergents du roi, s’étaient installés en attendant la fin de l’audience. Aucun n’était suffisamment proche pour lui faire plus qu’un signe amical de la tête ou de la main.

La fraîcheur du bâtiment les saisit lorsqu’ils y entrèrent. La blancheur de la pierre, récemment taillée et maçonnée rendait l’endroit effrayant, glacial. Quelques décors de couleur étaient en cours, mais les artisans semblaient avoir déserté le chantier avant de l’avoir achevé. Le chœur, au fond, présentait une image impressionnante du Christ. Son regard intense semblait scruter au plus profond de l’âme de chacun. À ses pieds, entre les premiers piliers, des chaises avaient été installées sur une petite estrade de bois, autour d’un tabouret curule muni d’un coussin. Là se tenaient les jurés de la cour des Bourgeois, qu’Ernaut connaissait bien, pour les fréquenter souvent.

Il s’y trouvait Pierre de Périgord, déjà présent la veille, qui discutait à voix basse avec Albert Lombard, petit homme toujours impeccablement habillé, dont le visage, à l’ordonnancement hasardeux, reflétait autant de bienveillance qu’il attirait la sympathie. Plus d’un s’était laissé prendre à cette allure débonnaire recouvrant un esprit brillant. À leur côté se tenait Simon Rufus, dont la fratrie occupait plusieurs manses[^manse] dans le casal. Rien d’étonnant à ce qu’il ait souhaité être présent pour cette pénible affaire. Son profil d’aigle, ses joues maigres, le faisaient ressembler à un volatile, ses cheveux roux toujours ébouriffés évoquant sans nul doute une crête.

Lorsqu’il prenait la parole, il s’emportait facilement, sa langue cherchant à suivre le flux rapide de ses pensées. Il avait néanmoins bonne réputation dans les affaires et Ernaut l’avait plus d’une fois vu prendre parti avec grande justesse. Derrière se profilait la vaste bedaine de Pierre Salomon, le teint rougeaud comme à son habitude. Son visage large, noyé de barbe et de cheveux fous transpirait abondamment et il s’essuyait de la manche régulièrement tandis qu’il opinait aux réflexions de ses confrères. C’était un homme habile, à l’esprit pratique, dont on disait qu’il préférait une pile de besants[^besant] à toute autre chose. Mais personne ne le pensait malhonnête pour autant.

En retrait, occupé à échanger avec l’intendant du Saint-Sépulcre, habillé d’une magnifique cotte d’un jaune profond, ornée de décors brodés au col, aux bras et aux poignets, le vicomte semblait soucieux. Ce n’était certes pas de l’affaire qu’il était ennuyé, ayant habitude de présider de tels jugements par bataille. Il semblait à Ernaut qu’il allait toujours ainsi, les yeux couverts de ses épais sourcils, froncés à l’avance de tout ce qu’il redoutait. La main devant la bouche, on ne pouvait voir s’il parlait ou réfléchissait. À gauche de cette assemblée, sur des tréteaux, recouverte d’un linge, on devinait une silhouette. Il était de coutume de montrer le corps de la victime lors des premières audiences de façon à ce que la cour puisse s’assurer du meurtre. Raoul, le jeune homme qui assistait habituellement l’écrivain de la cour, Guérin de Bethléem, reconnut Ernaut et s’avança vers lui.

« Le bon jour, Ernaut. Je ne savais pas que tu devais être présent.

— Ce n’est pas en tant que sergent que je suis là, j’assiste mon frère, qui a part en l’affaire. »

Raoul ne put retenir une grimace de désapprobation.

« Pour la victime ou le murdrier ?

— Il ne croit pas que celui qu’on accuse ait frappé.

— Oh ! Je vois. Il hocha la tête avec tristesse. L’histoire parait pourtant fort simple et je crains qu’il ne se fourvoie.

— Rien n’est jamais simple par ici et certes pas meurtrerie. »

Le jeune clerc lui sourit amicalement, sans conviction.

« J’espère que tout se finira bien pour les tiens. »

Puis il retourna vers la cour, se préparant à noter sur des tablettes de cire les décisions qui seraient prises, de façon à éventuellement les recopier de façon définitive pour les archives. L’intendant frappa dans ses mains à plusieurs reprises tandis que la cour des Bourgeois et le vicomte prenaient leur place. Lorsqu’il s’assit, ce dernier eut un mouvement de surprise en voyant l’imposante silhouette d’Ernaut parmi les premiers rangs. Il ne s’attarda néanmoins pas et adopta son attitude habituelle, empreinte de sérieux, dont on ne pouvait dire s’il écoutait attentivement, somnolait ou pensait à autre chose. Lorsque le calme s’installa, ce fut Raoul qui prit la parole, un peu hésitant au début, mais s’imposant avec force à la fin.

« Bourgeois du casal de la Mahomerie, vous en avez appelé à la cour des Bourgeois du roi, notre sire Baudoin, en une affaire de meurtrerie. Approchez et vous serez entendus, parlez et justice vous sera rendue. »

Puis il se recula, prêt à noter. Aymeric le Grand hésita un court instant, fit quelques pas en avant, visiblement intimidé de se trouver face au regard inquisiteur de la cour. Il se sentait gauche et pitoyable dans ses vêtements à la couleur passée, aux coudes usés. Il s’inclina légèrement en salut et déclara :

« J’ai pour nom Aymeric, dit le Grand, et suis de ce casal de la Mahomerie. Je viens ici déclarer un cas de féroce meurtrerie, commis à la nuit. Mon compère Ogier a reçu par le corps plusieurs coups qui lui ont ravi son souffle, et il en a rendu son âme à Dieu. »

Il marqua un temps, inquiet de suivre la bonne procédure, d’employer les bonnes formules.

« Qu’il plaise à la cour de mander Godefroy, dit le Borgne, de ce même casal, car je me plains de lui qu’il a meurtri Ogier, dont la dépouille est là sous vos yeux. Entendez donc ce que j’ai à vous dire. »

Les regards de la cour scrutèrent l’assemblée, attendant un mouvement, et s’attachèrent à étudier Godefroy lorsque celui-ci s’avança à son tour. Il tentait de faire bonne figure, mais ses épaules lasses, son pas traînant indiquaient bien toute la frayeur qui l’habitait en cet instant.

« Je suis Godefroy le Borgne et je me présente à vous. »

Aymeric se fendit d’un sourire mauvais et, indiquant la dépouille, continua : « Venez voir ce meurtre. »

Le vicomte et les jurés se levèrent alors de leurs sièges et s’approchèrent du défunt, dont le drap fut tiré. Parmi l’assemblée, les cous se tendirent pour mieux voir. Aymeric montra les blessures béantes qui déchiraient le corps d’albâtre.

« Voyez ces coups portés sur cet homme meurtri. »

Sans rien laisser paraître de leurs sentiments, les bourgeois du roi examinèrent, hochèrent la tête, puis reprirent leur place, aussi hiératiques que des statues au portail d’une église. Aymeric toussa, se racla la gorge, impressionné malgré lui de ce qu’il allait prononcer. Plus aucune fanfaronnade dans son attitude n’était perceptible.

« Sire vicomte, à vous et à la cour je me plains de Godefroy le Borgne, qui est là, car il a donné les coups que la cour et vous avez vus, sur Ogier, l’homme qui fut meurtri. »

Il marqua un temps, vissant son regard à celui de Godefroy.

« S’il ne reconnaît pas cela, je le prouverai de mon corps contre le sien et le tuerai ou le ferai rendre grâce à toute heure du jour. »

Puis il attendit, l’air bravache, toisant Godefroy. Le vieil homme fit un pas en avant, inquiet. Sa voix était hésitante lorsqu’il répondit et il s’y reprit une seconde fois.

« Je nie le meurtre et les coups et suis prêt à me défendre contre Aymeric qui est là, de mon corps contre le sien, et le rendrait mort ou à ma merci à toute heure du jour. »

Le vicomte releva le buste et rétorqua d’une voix claire :

« Tu me parais bien âgé pour aller en champ, sans compter ton œil unique. Il te faut donc un témoin, qui se fera ton champion, et aura son sort lié au tien. »

Godefroy hocha avec douleur le chef et Lambert s’avança alors à ses côtés, posant sa main sur son épaule en soutien.

« Je suis Lambert de Bourgogne, de ce casal et suis témoin de cet homme qui nie le meurtre et les coups. Je serai son champion contre Aymeric, de mon corps contre le sien, et le rendrai mort ou à ma merci à toute heure du jour. »

Ernaut sentit sa glotte se serrer et sa mâchoire se crisper. Il lui sembla un instant que le vicomte avait jeté un coup d’œil dans sa direction. À ses côtés, Osanne se fit plus présente, et il posa un bras protecteur sur son épaule. Il lui sembla qu’elle pleurait, mais il ne la regarda pas. Toute son attention était focalisée sur le drame qui se jouait entre les trois hommes. L’église s’assombrit, les voûtes se tassèrent, les ombres s’épaissirent. Pierre Salomon se leva et tonna de sa voix forte, habituée aux discours :

« Soyez prêts pour le troisième jour d’ici, avec toutes les armes qui conviennent à champion pour défendre ce que chacun de vous a dit. »

Puis il donna quelques rapides instructions à l’intendant pour que des gardiens enferment et surveillent les deux combattants, tout en leur laissant le droit de voir qui le souhaitait. Il devait également s’occuper de les nourrir suffisamment. Comprenant que la première partie de ce jeu terrible était achevée, les curieux s’égayèrent, ne laissant que les principaux intéressés dans la nef. Un groupe de pèlerins se faufila, en s’excusant à chaque pas, en direction du chœur. Ernaut détaillait son frère des pieds à la tête, comme s’il ne parvenait pas à se convaincre de ce qu’il avait sous les yeux.

« Trois jours, je n’ai que trois jours pour trouver… Tu as bien fol espoir, mon frère. »

Osanne pleurait désormais à chaudes larmes, pelotonnée contre celui qu’elle pensait épouser, déchirée à l’idée de perdre peut-être à la fois celui-ci et son père. Elle voulut parler, mais ses hoquets l’en empêchèrent. Lambert, bouleversé, s’efforçait de la consoler. Godefroy, guère plus vaillant, tenait la main de sa femme si serrée que les jointures en blanchissaient. Derrière les deux hommes, des valets du Saint-Sépulcre attendaient. Godefroy leur demanda s’il devait aussi être enfermé jusqu’au combat.

« Nul besoin. Mais il vous faut demeurer en votre hostel. La cour risque de prendre tout voyage en dehors du casal pour une fuite. »

Une main frappa dans le dos d’Ernaut. Il se tourna et découvrit Raoul, l’air sérieux.

« Le vicomte désire te parler avant de s’en retourner ».

Devant un des autels, les mains dans le dos ainsi qu’il l’aurait fait pour admirer un paysage ou tancer un serviteur, Arnulf étudiait un imposant Christ en croix peint, orné de pierres, scintillant à la lumière des cierges. Il ne se retourna pas tout de suite à l’arrivée d’Ernaut, commençant à lui parler sans bouger d’un iota.

« Grande surprise de t’encontrer ici, Ernaut. Tu n’étais pourtant pas de mon escorte. »

Il attendait des explications, mais rien dans sa voix ne permettait de déceler de la colère ou de l’agacement. Il semblait juste curieux.

« J’ai su cette meurtrerie par mon frère et qu’il y aurait part.

— Ton frère est champion du borgne, c’est cela ?

— Oui, messire vicomte.

— Douloureux privilège ! »

Il fit enfin face au géant, qu’il examina un petit moment, les minces lèvres se mouvant à peine.

« Et que viens-tu faire en cette histoire ?

— Mon frère m’a mandé. Il aimerait ne pas avoir à faire bataille contre Aymeric, qu’il estime honnête homme, mais fourvoyé.

— Ah, il pense ça ? s’esclaffa le vicomte. Il ne veut pas que Dieu donne raison de lui ! Pourtant, je ne pense pas qu’il aimerait le contraire ?

— Certes non. Il m’a demandé de trouver qui était le vrai murdrier, car il me sait talent à ça.

— Tu sais certes fureter comme peu de mâtins le peuvent. »

Il plissa les yeux, semblant s’amuser de cette conversation. Il se gratta le nez, laissant le temps s’écouler avec métier.

« Je suis aise de voir que tu te soucies des tiens. Une parentèle soudée est gage de droiture. Je te relève de tes charges jusqu’à la bataille, le temps pour toi de faire lumière sur tout cela. Tu m’en rendras compte, que tu réussisses ou pas.

— Que Dieu vous en mercie, messire vicomte ! Je… »

Arnulf l’interrompit d’une main levée.

« Mais sache bien que tu ne peux te prévaloir d’être sergent du roi en cette affaire. Tu n’es qu’un frère qui veut aider les siens. Nul passe-droit, nul privilège. Et si tu te laisses aller à quelques travers… »

Son visage sévère n’eut pas besoin d’en dire plus. Ernaut l’avait déjà vu se fermer ainsi lorsqu’il châtiait un valet ou sermonnait un incompétent. Il menait ses troupes comme ses chevaux, d’une main sûre, avec un solide chanfrein et n’hésitait pas à éperonner au sang pour ramener dans le droit chemin. Le jeune homme s’inclina, balbutiant des remerciements et s’éloigna. Le temps lui était désormais compté.

