## Prologue

### Monastère bénédictin de la Charité-sur-Loire, hiver 1223

Le jour blafard éclairait chichement la salle voûtée et des lampes projetaient de leurs flammèches des ombres dansantes. Près du vaste foyer, où les braises rougeoyantes réchauffaient autant l’âme que le corps, un jeune novice, la tonsure fraichement tracée, s’appliquait à border le lit d’un vieil homme. Celui-ci se laissait faire, comme un enfant, indifférent à cette dépendance contre laquelle il ne pouvait rien. Avec le temps, il se dépouillait de ses oripeaux terrestres et l’orgueil avait quitté depuis longtemps son corps. Son crâne n’accueillait plus assez de cheveux pour qu’il soit nécessaire de passer le rasoir. Ses yeux vitreux s’attardaient sur les choses, comme s’il voyait au-delà, et sa voix rugueuse, épaisse, remuait l’âme. Il sourit au jeune oblat, accusant plus encore les rides qui striaient son visage las.

« Puisse chacun avoir semblable ami à veiller sa mort, Déodat, glissa-t-il avec chaleur.

— Que dites-vous, mon frère ? J’ai bel espoir de vous voir encore moult saisons. »

Le vieux moine soupira, ému de la chaleur de la réponse, mais également fatigué d’attendre que le Seigneur le rappelât à lui. Tout son être aspirait à retourner à la fontaine qui le désaltérerait enfin. Il avait trop vécu pour en demander plus. Il attendait le repos.

« Outre, il vous faut m’en conter encore sur les errances. Le père prieur a grand désir de tout connaître.

— Tu es bon moine, Déodat. Féal et obéissant. De la graine d’abbé. »

Le novice s’empourpra sous le compliment, finissant de lisser la couverture. Il s’empara ensuite de son écritoire, s’asseyant sur l’escabeau où il avait déjà passé tant de jours à noter les confidences de son aîné. Il ne versa pas d’encre dans la corne, se contentant de lisser les panneaux de cire d’une grande tablette à l’aide de l’extrémité plate d’un stylet de bronze. Voyant les mots qu’il effaçait, il se remémora les derniers échanges et les rappela, autant pour lui-même que pour son confident.

« Nous n’étions guère allés après l’élection du père Amaury comme prieur des chanoines du Saint-Sépulcre.

— J’en ai souvenance. Temps de paix relative malgré assauts ici ou là. Le sultan Noreddin était fort occupé en ses royaumes. Baudoin ne s’était pas encore aventuré dans toutes ses chevauchées en les terres d’Égypte. »

Il toussa légèrement et changea de position dans le lit, révélant un corps maladif, osseux, qui tendait la toile. Son regard changea tandis qu’il chassait les souvenirs, les organisait pour en tirer un récit édifiant.

« En cette année-là, le comte Thierry était de retour en Jérusalem, ajoutant ses lances à celles des bannerets du roi. Le nouveau prince d’Antioche écumait ses terres tel un démon, avilissant les hommes de Dieu.

— Et Ernaut ? glissa Déodat.

— Ernaut ? Il avait trouvé sa place, au service du roi Baudoin. Outre, il s’apprêtait à marier la damoiselle de ses pensées. »

Le jeune novice sourit, amusé par l’idée. Jamais sorti de l’enceinte du monastère, il n’avait que rarement eu affaire à des femmes. La voix éraillée l’arracha à ses pensées.

« Pourtant il encontra de nouvel grand péril et son âme fut une fois encore ébranlée.

— Était-ce lié aux démons qui le tourmentaient depuis les Pâques ?

— Certes, car il avait toujours ce poids en son cœur. Mais il fut aussi en butte à la voracité des hommes. »

Déodat hocha la tête, notant consciencieusement sur sa tablette d’une écriture légère. Le vieil homme brandit une main griffue, ongles et tendons, qu’il serra.

« Vois-tu, garçon, il en est d’aucuns avides de tenir toute chose, persuadés que la terre qu’ils empoignent est leur. »

Il eut un ricanement qui se termina en toux puis s’essuya la bouche avec un mouchoir avant de reprendre, d’une voix fatiguée.

« Tant qu’il est vaillant et empli de force, chacun croit pouvoir tout faire et oublie de mercier Dieu de ses bienfaits. Insensé, il croit son bras puissant et capable de tout moissonner.

— N’est-ce pas vrai des rois et des évêques ?

— Si fait, leur main peut influer le cours des choses plus que celle de vilains et pauvres moines comme nous. Pourtant, si haut peuvent-ils trôner, ils ne saisissent que poussière entre leurs doigts. Le seul vrai pouvoir n’appartient qu’au Très-Haut. »

Voyant que le novice fronçait les sourcils, hésitant, le vieux moine lui sourit avec chaleur et précisa :

« Le pardon, Déodat, le pardon. »
