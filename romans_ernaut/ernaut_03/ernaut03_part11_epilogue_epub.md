## Épilogue

### Gawlân, soir du yawm al-’ahad, 11 dhou al qi’da 552[^Plateau du Golan, soir du 15 décembre 1157.]

Un vent coléreux secouait le plateau, faisant frémir les buissons et les arbres dénudés. Il avait dissipé la brume collante, poisseuse, frigorifiante, mais mordait tout aussi férocement. Des congères éparses de neige grise parsemaient les reliefs et les rochers sous une lune gibbeuse baignant la campagne de sa lueur moribonde, accentuant l’impression de froid. Des chacals au ventre creux hurlaient leur faim à la voûte céleste. Faisant concurrence au bruissement de la végétation, leurs cris lugubres ponctuaient le sifflement du vent aux oreilles rougies.

Une fine brume s’échappait de la bouche ou du nez et la seule oasis de chaleur était circonscrite autour d’un petit feu qui projetait vaillamment ses lueurs ambrées sur les roches et les arbres frigorifiés. Pelotonnées de leur mieux, des silhouettes emmitouflées dans des couvertures tentaient de prendre un peu de repos. Ce n’étaient pas des nomades, mais des voyageurs, dont les trois mules attachées à côté se tenaient tête-bêche.

Un homme au visage harassé contemplait les flammes dansantes, une branche à la main pour jouer avec les braises. Ses joues molles retombaient sur une mâchoire épaisse, terminée d’un menton en galoche. Un nez imposant descendait très bas, presque à toucher la bouche, réduite à une fente sombre. Son regard dissimulé sous des replis adipeux fouillait le foyer, cherchant un secret parmi le crépitement des broussailles humides.

Il portait une chape de voyage, resserrée autour de la tête à l’aide d’un turban qui lui servait également d’écharpe. De l’autre côté du foyer, trois formes humaines étaient allongées en un demi-cercle approximatif, tentant de profiter au mieux de la chaleur. Les visages étaient bien plus jeunes que lui. Mais ils n’étaient pas siens. Enfants de fellahs, ils étaient nés sur cette terre. Pourtant ils la fuyaient désormais. Son rôle était de les mener là où ils seraient libres de vivre selon leur gré.

Il toussa puis cracha. Il regrettait d’avoir organisé un passage en cette saison. Le froid était bien trop vif pour ses vieux os. Il devait apprendre à se ménager. Il regarda ses mains crevassées, à la paume rugueuse. Des mains de travailleur. Toute sa vie, il s’était acharné sur une terre difficile qu’il avait fouillée, remuée, ensemencée, arrosée, sarclée, amendée. La pitance qu’il avait pu en arracher avait le goût de la sueur et du renoncement. Chaque année, depuis sa naissance, dans son pays, par-delà les mers, le cycle recommençait, inlassablement.

Il fit une grimace, bougeant une jambe qui commençait à s’engourdir. Cadet d’une famille peu aisée, il avait cru à sa chance lorsqu’on lui avait parlé de terres à prendre loin à l’est, au centre du monde, dans le royaume de Palestine. La terre de Canaan, la Terre promise où le lait et le vin coulaient d’abondance.

Il renifla, essuyant son nez morveux d’une main agacée. Tout cela n’était que mensonge. Il avait trouvé un pays brûlé de soleil, où les pluies disparaissaient du ciel des mois durant, où on devait surveiller chaque jour l’irrigation de son jardinet sous peine de voir ses plants devenir fétus desséchés. Les Turcs, ces maudits cavaliers, surgissaient à tout moment pour vous dérober les maigres biens que le sang et la sueur avaient arrachés de force à ce pays hostile. Ils venaient, se servaient, puis repartaient en riant, dans la poussière et le chaos des cavalcades. Parfois, ils laissaient des cadavres derrière eux. Rarement des jeunes femmes intactes. Le pays béni du Seigneur.

Il sourit sans joie, laissant son regard parcourir les formes assoupies face à lui. L’une d’elles laissait voir un visage doux que la fatigue, le froid et la faim ne rendaient que plus vulnérable. Les longs cils bruns s’étaient joints et le nez frémissait doucement à chaque respiration. Il baissa la tête, laissa sa baguette dans le feu et plaça ses mains contre son ventre, sous la chape, au chaud. Il n’avait pas sommeil. Devinant un mouvement proche, il observa un moment ses mules, qui semblaient calmes, bougeant de temps à autre la queue ou les oreilles.

Le froid de la terre était tel qu’il finit par transpercer les épaisseurs de vêtement et vint lécher la peau de la jeune fille. Elle gémit doucement, se réveillant sans cesse, mal installée sur ce sol gelé et caillouteux. Elle voulut trouver meilleure position et, ce faisant, sentit la chaleur s’éloigner de son visage. Elle écarta les paupières, ensommeillée. Face à elle, le feu brillait de belles flammes et, parmi les ombres mordorées, elle aperçut le passeur assis face à elle.

Il semblait hypnotisé par les langues écarlates qui sautaient entre eux à l’assaut du ciel. Elle se redressa. Il la dévisagea. Il n’avait pas bougé, même pas la tête, ses yeux seuls s’étaient accrochés à elle. Elle n’était pas habituée à être ainsi regardée par un homme de cet âge. Jusqu'alors, elle n’avait pratiquement connu que sa famille. La promiscuité l’indisposait. Elle baissa les yeux. Un de ses deux compagnons, son frère, remua à ses côtés, marmonnant dans son sommeil.

Rien n’aurait pu empêcher Ahmad de dormir, son caractère affable semblait se jouer de toutes les épreuves et il trouvait le sommeil tel un juste qui n’avait nul souci. Elle ne put réprimer un sourire, réconfortée par sa présence. Cela ne dura pas. Elle sentait le poids du regard de l’homme de l’autre côté du feu. Il n’avait pas encore fait un mouvement, mais ses yeux s’étaient crochetés à elle. Elle pouvait en sentir la lueur sans même se tourner.

Elle hésitait à s’éloigner un peu de leur camp pour se soulager. Peu désireuse de se dévoiler ainsi à cet homme si indiscret, elle se mordit la lèvre, hésitante. Mais elle n’avait pas le choix si elle voulait se rendormir et trouver un peu de repos. Elle se leva doucement, sentant le froid s’immiscer avec célérité dans le moindre repli de chaleur qu’elle s’était ménagé. Elle frissonna, rassemblant ses bras contre son corps, se frottant avec vigueur pour activer sa circulation. Elle enjamba son frère endormi, se dirigeant vers le plus proche bosquet comme une ombre discrète. Elle soupira de soulagement lorsqu’elle trouva le couvert.

L’homme n’avait toujours pas bougé, son regard était enchaîné à la silhouette gracile qui s’éloignait, s’attardait sur les branches derrière lesquelles elle avait disparu. Il renifla une nouvelle fois et s’agita un peu, se frottant le nez. Il sentait un frisson se répandre depuis ses reins, dans tout son dos. Ce n’était pas le froid, il n’y prêtait plus aucune attention.

Sous sa chape, ses mains jouaient avec la lame épaisse, robuste, d’un gros coutelas qu’il conservait toujours sur lui. Son regard coula jusqu’aux deux endormis. Il plissa les yeux, en proie à un doute. Ses doigts se crispèrent sur le manche et il appliqua la pointe dans sa paume, comme s’il voulait la percer.

La douleur accéléra son pouls. Une grande inspiration et il se leva pesamment. Chaque pas lui semblait plus difficile. Le sol, pourtant gelé, cherchait à s’attacher à ses pieds, ses jambes. Il ployait, comme s’il devait traverser une foule hostile, dont il lui paraissait sentir les doigts griffus, les chuchotements furtifs.

Posant péniblement un pied devant l’autre, il contourna le feu, s’approchant du plus grand des hommes. Il se pencha, inclinant la tête pour l’examiner. Il aurait pu être son fils, en pleine force de l’âge, le menton à peine bruni d’une barbe juvénile. Il hésita un instant, s’approchant encore du visage, sentant le souffle sur ses doigts. Sa langue essaya d’humecter ses lèvres gercées, craquelées. Il se mordit la joue. Puis plongea sa lame dans le cou, fermant les yeux, ultime réflexe de compassion. Le gargouillis lui souleva le cœur.

Frigorifiée, tremblante, la jeune femme se hâta de revenir vers le feu d’un pas léger. Elle vit immédiatement la silhouette de leur passeur, ombre sur les flammes. Elle crut tout d’abord qu’il remettait du bois à brûler, puis s’aperçut qu’il était auprès de ses compagnons. Peut-être les réveillait-il déjà.

Elle soupira, épuisée, peu désireuse de reprendre la route en pleine nuit à la lueur de la lune. En s’approchant, elle fronça les sourcils. Quelque chose l’intriguait. Personne ne parlait ni ne bougeait. Elle fit craquer une branche de son pied menu. L’homme sursauta, se tourna vers elle.

Dans l’ombre, elle ne pouvait voir nettement son visage, mais sentait le regard avide, brûlant, qui la parcourait. Elle allait l’interroger lorsqu’elle vit sa main. Puis le couteau poisseux, d’où le sang s’écoulait en gouttelettes sombres. Elle hurla. Les chacals lui firent écho, en un concert de chants aigus. Effleurant le Golan sous la voûte étoilée, le vent glacial emporta le tout au loin. ❧

**À suivre…**

<p class="qita_licence"><span class="qita_scission">❖ ❖ ❖</span></p>
<p class="qita_info_licence2">**Ce roman vous a plu ?**</p>
<p class="qita_info_licence2">N'hésitez pas à en parler, à l'envoyer à vos proches et à me le faire savoir.<br/>Cet ebook étant diffusé gratuitement, vous pouvez m'aider à écrire les prochains en me soutenant.</p>
<p class="qita_info_licence2">Faire un don sur [https://hexagora.fr/fr:donate](https://hexagora.fr/fr:donate)</p>
<p class="qita_info_licence2">Merci,<br/>Yann.<br/></p>
<p class="qita_licence"><span class="qita_scission">❖ ❖ ❖</span></p>

