
<p class="qita_licence">
<span class="qita_scission">❖ ❖ ❖</span>
</p>
<p class="qita_info_licence2">
Plus d’informations sur le cycle de textes courts dans le monde des enquêtes d’Ernaut de Jérusalem sur le site de la série Hexagora&#160;:<br/>
<a href="http://www.hexagora.fr">http://www.hexagora.fr</a> / <a href="http://www.ernautdejerusalem.com">http://www.ernautdejerusalem.com</a>
</p>
<p class="qita_licence">
<span class="qita_scission">❖ ❖ ❖</span>
</p>
<p class="qita_info_licence2">
Texte mis à disposition sous la licence
</p>
<p class="qita_info_licence2">
<a href="https://creativecommons.org/licenses/by-sa/3.0/deed.fr">Creative Commons BY - SA 3.0</a>
</p>
<p class="qita_info_licence2">
![](by-sa.png)
</p>
<dl class="qita_info_licence2">
<dt>Attribution</dt>
<dd> – Vous devez créditer l’Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l’Œuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l’Offrant vous soutient ou soutient la façon dont vous avez utilisé son Œuvre</dd>
<dt>Partage dans les Mêmes Conditions</dt>
<dd> – Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l’Œuvre originale, vous devez diffuser l’Œuvre modifiée dans les même conditions, c’est-à-dire avec la même licence avec laquelle l’Œuvre originale a été diffusée</dd>
</dl>
