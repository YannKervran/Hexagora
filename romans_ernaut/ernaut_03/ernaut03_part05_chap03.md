## Chapitre 3

### Casal de la Mahomerie, jeudi 12 juin, soir

Assis sur un banc devant la petite maison de Iohannes, Ernaut admirait la vue qui s’étendait au sud. Une butte, assez raide, coupait le paysage sur sa gauche, mais on devinait néanmoins les massifs montagneux encadrant la Mer Morte, à des lieues de là. Il avalait sans y penser les pois chiches que son hôte lui avait servis, arrosant chaque bouchée pâteuse de vin aigre coupé d’eau. Les oignons et l’huile étaient savoureux, comme le fromage de brebis.

Dans une des parcelles en face d’eux, un âne brayait, contrarié par la trique trop sévère de son maître qui l’avait chargé d’un imposant chargement de broussailles. À droite, sur le chemin menant à la source, des enfants chantaient en courant autour de poulets affolés. Le jeune homme eut un sourire à se remémorer ses propres exploits dans les frondaisons du bord de la Cure, près de Saint-Pierre. Il eut un pincement au cœur, se demandant où étaient passés ses amis et ennemis d’alors. Cela ne faisait pas dix ans, pourtant il lui semblait qu’une éternité s’était écoulée, le temps nécessaire pour parvenir là, aux portes du Paradis sur terre, dans le creuset de la Foi, devenu Enfer par la malfaisance des hommes.

Il examina son compagnon, installé sur une natte décolorée à même le sol. Il n’avait pas l’air syrien, mais pas français pour autant, ni bourguignon. Son accent était d’ici bien qu’Ernaut n’arrivât pas à le préciser, malgré son don pour les langues. L’homme lui paraissait sympathique, pourtant il avait encore des réticences à s’y fier complètement. Le plus amical des paysans recelait parfois une âme noire comme du charbon, et il s’étonnait de voir des clercs hideux à en faire tourner le lait capables de trésors de douceur. Le plus infâme des salauds ne s’annonçait jamais, préférant ramper, serpent sans écailles, loup sans croc, répandant le mal comme un semeur son blé, attendant que l’épi soit dru pour engranger sa moisson de ténèbres.

Il s’éclaircit la voix d’une gorgée de vin et demanda :

« Tu es installé au casal depuis long temps ?

— Cinq années que je suis là.

— Tu es du royaume ? »

Iohannes sourit, comprenant visiblement qu’il lui fallait donner quelques gages à l’enquêteur.

« Maintenant oui. Je suis d’Édesse, père était un Franc, un croisé.

— Le comté perdu ? Tu y as vécu ?

— Jusqu’à sa chute, oui. »

La voix se fit mourante et le regard baissé n’incita pas Ernaut à continuer dans cette voie. Il sentait que l’homme avait ses douleurs et ne souhaitait pas en parler. Il avala plusieurs cuillerées en silence, hochant la tête comme s’il avait obtenu quelques réponses fondamentales. Puis la curiosité fut la plus forte.

« C’est que tu as l’air de bien connaître le coin… Je dois avouer que je m’attriste de devoir attendre au matin pour aller faire visite à Salomé.

— Le seigneur Robert de Retest est jaloux de son bien. Il ne fait pas bon se l’aliéner.

— Espérons alors que nous récolterons bonnes nouvelles demain, car nous n’aurons guère le temps d’aller plus loin. »

Iohannes hocha la tête, un peu dépité.

« Si nous lui montrons respect, nous obtiendrons peut-être son soutien. Un homme tel que lui qui viendrait affirmer que des larrons ont traversé sa terre à la nuit de la murdrerie ! Cela pourrait convaincre Aymeric.

— Dieu t’entende ! Nous ne pouvons espérer qu’un de ses serfs témoigne.

— Ils ne seraient guère prêts à le faire. Sauf peut-être le ra’is ou le shaykh. Ce dernier n’est pas mauvais homme, malgré son abord difficile. »

Ernaut lécha sa cuiller et dévisagea l’interprète.

« Tu penses qu’ils auraient vu quelque chose ?

— Aucune idée, mais si d’aucuns savent des choses, ils ne diront rien sans que ces deux-là aient donné leur accord, et se cacheront derrière eux quoiqu’il arrive. »

Le géant renifla, une grimace de dédain enlaidissant ses traits. Iohannes comprit que le sergent n’aimait guère ceux qui ne partageaient pas son courage, son habitude de tenir bon ses convictions face à l’adversité.

« Tu sais, Ernaut, ce ne sont que des serfs, qui ne valent pas plus que les bêtes qu’ils mènent ou les outils qu’ils manient pour leur seigneur.

— Et quoi ? Ce ne sont plus des hommes ? À se terrer comme conil en son terrier ! »

Iohannes soupira.

« Le shaykh est prud’homme de leur casal, un vieillard sage et pieux. Ils lui font confiance pour savoir s’il est prudent de faire une chose ou l’autre, face à un étranger ou devant leur seigneur. Le ra’is est le plus aisé, le premier d’entre eux, l’homme à qui Robert de Retest parle quand il vient lever la taille ou percevoir ses droits. Ce n’est qu’à travers lui qu’ils connaissent le sire de Salomé. Et ils en sont bien aises. »

La dernière phrase avait été dite dans un soupir, empli de mélancolie et d’aigreur. Iohannes avala plusieurs bouchées sans plus rien dire, les yeux rivés sur l’horizon, le ciel immaculé d’un bleu encore clair sous un soleil fatigué de sa journée à écraser la terre, les hommes et les bêtes. Un chien au poil hirsute passa, une proie sanglante dans la gueule, poursuivi par une horde d’admirateurs plaintifs aussi intéressés que veules. Iohannes se leva et remplit les verres avant de rentrer chez lui le pichet à la main. Ernaut entendit le glouglou familier du vin qui s’écoulait du tonneau en vagues mousseuses.

Lorsque l’interprète se retrouva dehors, il souriait sans joie.

« Le gamin ne saurait tarder à nous revenir, Salomé n’est pas à plus d’une demi-lieue.

— Si le sire Robert n’est pas là ?

— Alors nous aurons affaire à quelque familier à lui. L’important est de demander licence d’entrer et questionner. Nous devrions pouvoir chevaucher là-bas au matin, n’aie nulle crainte.

— Tu as un cheval ?

— Non, j’ai usage d’en emprunter un à l’intendant du casal. Il est important qu’on nous voie arriver en selle. »

Ernaut fronça les sourcils, posa sa gamelle et tendit son verre pour le faire remplir. En le servant, Iohannes détailla son idée.

« Salomé est un vieux village et les fellahs qui y demeurent sont fils de cette terre depuis des temps très anciens. C’est leur pays, leurs collines.

— Nous sommes sur les terres du roi Baudoin et ils lui appartiennent !

— Certes, je t’entends. Mais ils étaient là avant nous et le seront après. Ils ont connu Romains, Grecs, Égyptiens, Turcs et Francs, et en verront bien d’autres encore à l’avenir. Eux demeurent là, attachés à cette terre comme l’olivier à sa colline. Ils ne nous aiment pas, Ernaut. Ils nous craignent, ils nous obéissent, comme le bœuf entravé craint la baguette du bouvier. Si on se présente simples marcheurs, ils nous tourneront le dos, crachant dans nos pas. Nous n’en apprendrons rien.

— Ils ne comprennent que la force, c’est ça ?

— Non. Ils la respectent, comme chacun. »

Ernaut hocha la tête, dévisageant Iohannes, une lueur de suspicion dans l’œil.

« Tu as été l’un d’eux, c’est ça ?

— Non. J’ai toujours été libre. Ma mère était bonne chrétienne et l’arrivée des Francs fut… On a célébré leur venue chez nous et ils ont bientôt été nos frères. Les paysans musulmans de la terre de Judée n’ont toujours connu que des seigneurs, grattant un sol qui ne leur a jamais appartenu. Certains estiment que les Latins ne sont pas pires que d’autres et leurs redevances point trop lourdes. Tant que le maître est juste, ils courbent le dos et patientent.

— Et s’il ne l’est pas ?

— Ils le font en grognant, attendent l’arrivée d’un autre, moins avide, moins coléreux, moins détesté. »

Ernaut toussa, réfléchissant un petit moment avant de reprendre.

« L’un d’entre eux aurait pu être jaloux d’Ogier, un paysan qui prospérait à côté d’eux ?

— Je n’y crois guère.

— Pourquoi pas ? La jalousie ronge les cœurs sans se soucier du pays ou de la croyance.

— C’est que tu as entendu dire qu’Ogier parlait syrien, c’est ça ? Non, cela ne se peut. »

Il se leva, face aux collines, le bras tendu vers les terrasses bien rangées, les oliviers et les vignes soigneusement entretenus.

« Un fellah n’aurait rien eu à y gagner et tout à y perdre. On ne leur cause aucun tort, ces terres sont au Saint-Sépulcre, pas à Robert de Retest. Notre aisance ne leur fait nul dommage.

— Si leur seigneur voit qu’ils sont moins travailleurs que vous ? »

Un rictus désabusé fendit le visage de Iohannes.

« Crois-tu que des hommes d’Outremer savent mieux travailler la terre d’ici que des fellahs qui l’ont appris de leurs pères, encore enfants ?

— C’est vrai, concéda Ernaut, amusé par sa propre naïveté.

— Parler leur langue n’en fait pas des proches, tu sais. Je me rends juste de temps à autre en un de leur casal, pour aider à l’établissement de quelques redevances. »

Iohannes vint s’asseoir près de Ernaut, se passant la langue sur les lèvres, comme s’il hésitait à confier un secret.

« Nous ne sommes pas voisins de ces hommes, ce ne sont pas des villages comme dans ton pays. Il s’agit de deux mondes qui s’ignorent autant que possible. Chacun chez soi. Nous n’avons nulle fête à partager, aucun mariage à célébrer, pas de morts à honorer ensemble. Ils nous sont aussi étrangers que s’ils vivaient à des milliers de lieues d’ici. Nous foulons la même terre, subissons le même soleil, mais n’appartenons pas au même monde. Nous sommes les maîtres et ils sont nos serfs. »

### Château de Salomé,vendredi 13 juin, fin de matinée

Un valet se porta à la rencontre d’Ernaut et de Iohannes lorsqu’ils s’approchèrent du portail d’entrée du château. Occupé jusque-là à des réparations sur le guichet piéton, il se fit fort de les annoncer dans l’endroit puis de les mener auprès du seigneur du lieu.

La cour était petite, les bâtiments organisés autour d’une esplanade grise. Seul un arbre au tronc tordu étalait son ombre sur les cailloux et les graviers. La haute tour de pierre taillée, de faible dimension s’élançait depuis des bâtiments à l’aspect peu glorieux. Comme dans le village, les murs de moellons n’étaient que grossièrement chaulés et un simple feuillage couvrait souvent les appentis. Depuis l’un d’eux, ils entendaient une voix forte s’exprimer, dans la direction où on les menait. Le serviteur prit les rênes de leurs montures et abandonna les arrivants à l’entrée, non sans avoir averti les occupants, sombres formes mouvantes dans la noirceur intérieure.

Iohannes semblait hésitant à entrer, Ernaut patienta donc à ses côtés. Le seigneur du lieu leur apparut soudain, comme diable jaillissant de son antre. Clignant des yeux pour se préserver de la lumière cruelle, un curieux rictus en guise de sourire lui déformait le visage. On aurait dit que sa silhouette s’était tassée avec le temps passé en selle. Le buste court, compact, était posé sur des jambes arquées et ses bras de lutteur s’ornaient de mains épaisses. Son visage, aussi rond qu’il était possible, était mangé par une barbe grise des ans et de la poussière, d’où émergeait un nez épaté. Ses yeux globuleux, sombres, s’activaient en tous sens sous les broussailles de ses sourcils. Son crâne, enfin, était craquelé comme un vieux parchemin, brûlé par les jours passés au soleil, certainement sous le harnois et seuls quelques poils épars s’entêtaient à tenir. Habillé d’une cotte de laine de belle longueur, mais tachée et déchirée, il avait glissé un pouce dans sa ceinture ornée tandis que de l’autre main il brandissait une baguette souple qu’il faisait tournoyer.

« Tiens donc, mais c’est Iohannes de la Mahomerie… Que me vaut ce plaisir ?

— La bonne journée, sire Robert, je suis venu jusqu’à vos terres pour une affaire pressante qui touche de près mon compaing, Ernaut de Jérusalem. »

Le chevalier détourna son attention vers le géant qui lui faisait de l’ombre. Il gardait son sourire et se fouettait désormais les chausses tout en examinant le jeune homme comme s’il devait l’affronter d’ici peu à la lutte.

« Pressante ? Il s’esclaffa. Nous pouvons quand même boire quelques godets de mon vin pour parler de ça… »

Puis il s’élança vers le bâtiment central, soulevant la poussière de ses pas traînants, rejoint par un chien élégant au pelage ras que tantôt il flattait de la main, tantôt il menait de sa baguette. Il gravit les escaliers pour rejoindre l’étage où s’ouvraient quelques jolies fenêtres, obstruées par des volets à claire-voie. Silencieux, les deux voyageurs montèrent à sa suite. L’intérieur était assez grand, certainement la pièce principale de son domaine, où il rendait la justice, faisait ses affaires et entendait les siens. Les murs étaient peints de décors géométriques et des tapis couvraient le sol presque partout. Au fond, sur une estrade de bois, une table avait été dressée devant un siège peint, à haut dossier.

Elle était encombrée de papiers dans un coin, et une écuelle avec des biscuits, un pichet et un gobelet y attendaient Robert. Des bancs s’empilaient dans un angle, des paillasses roulées derrière. Deux portes étaient visibles au fond. Le valet qui les avait guidés apparut soudain, portant des tabourets qu’il installa devant l’estrade, à quelques pas. Pendant ce temps, Retest avalait plusieurs rasades avec empressement. Bientôt, Ernaut et Iohannes reçurent chacun un verre de vin, et une corbeille de fruits et du pain plat furent installés à leur intention.

Pendant ce temps, le seigneur du lieu faisait mine de s’intéresser à des documents, des tablettes qu’il reposait ensuite avec dédain dans la pile désordonnée. Il leva à peine les yeux lorsqu’il s’adressa de nouveau à Iohannes.

« Alors, que sollicitez-vous de moi, vous deux ? »

Iohannes se leva, inclinant le buste légèrement. Robert fit un signe de la main et l’invita à demeurer assis. L’interprète s’exécuta avant de prendre la parole.

« Voilà, sire Robert, nous aurions besoin d’aller et venir sur vos terres pour une affaire qui touche de près Ernaut, à mes côtés.

— Quelle affaire ?

— Son frère doit faire bataille pour meurtre contre un accusateur et nous apensons qu’il s’agit là d’une erreur. Il est possible que le vrai murdrier ait passé par vos terres aux fins de commettre son forfait. Nous désirons le pister.

— Mes serfs sont loyaux et n’auraient pas commis pareilles choses ! répliqua le chevalier d’un ton sec.

— Nous n’en doutons point, sire. Mais il est possible que des brigands se soient cachés parmi les collines, dans vos terres gastes[^gaste]. »

Robert de Retest avala une gorgée, dodelinant de la tête.

« Cela ne me plaît guère qu’on croie que je tiens mal mon bien… »

Il renifla avant de continuer, ne laissant à aucun des deux hommes le temps de répondre.

« Mais je déteste plus encore qu’on cherche à profiter de moi. Je n’ai nulle amitié pour les larrons qui détroussent et pillent et surtout pas s’ils me moquent en se celant en mes terres. »

Il se tourna vers Ernaut, s’efforçant d’avoir une voix aimable, ce qui le rendait d’autant plus inquiétant.

« Ton frère doit être jugé par bataille ? Comment faire confiance au frère d’un possible murdrier ?

— Lambert n’est pas en cause, il se porte témoin pour l’accusé, qui est borgne et vieux.

— Je vois, c’est donc là homme de parole. Ça me plaît mieux ! »

Il détailla la tenue d’Ernaut, s’attardant sur le baudrier et le fourreau.

« Mais toi-même tu n’as pas vêture de manant ou de paysan. Qui es-tu… Ernaut ?

— Je suis homme du roi, un des sergents à son service. »

Iohannes laissa échapper un soupir, comme s’il désapprouvait cet aveu, ou n’estimait guère cette tâche. Le chevalier se leva et vint se placer devant la table, son verre toujours à la main.

« Ah, si tu es un loyal serviteur du roi Baudoin, je ne vois nul motif de ne pas accéder à ta requête. »

Il prit une voix douce.

« Le vicomte Arnulf ne pourra dire que je traite mal ses hommes, j’en donne mon gage. »

Il se retourna, se versant du vin une nouvelle fois.

« Toi, tu le sais déjà, Iohannes : il est fort malaisé d’obtenir quoi que ce soit des serfs, ici. »

Il leur fit face de nouveau.

« Ils labourent sans trop rechigner, paient redevances comme il se doit, mais ne nous aiment guère, espérant toujours nous desservir. Je n’ai nulle confiance en eux. »

Il tendit le bras, embrassant d’un geste la pièce, le château et certainement son domaine en entier.

« J’ai quelques sergents loyaux, qui viennent du pays, pour les mener. J’ai plus fiance en mes mâtins qu’en aucun de ces païens. Ils ont le regard vicieux… »

Il se tut, regardant les deux hommes devant lui. Ernaut brisa le silence.

« Pensez-vous qu’ils cacheraient la présence de brigands ? Ils n’ont rien à y gagner.

— Ah ! Crois-tu ? Et si c’était quelque espie turque du Soudan ? Nous sommes sur la route de Jérusalem. Damas n’est pas si loin… »

Il fit jouer sa langue sur ses lèvres, se suçota les dents avec bruit puis reprit.

« Le ra’is n’est qu’un veule. Il croit que je ne sais pas renifler serpents et goupils. Il cache une âme roussie sous son turban et je le tiens serré.

— Nous aimerions voir aussi le shaykh, avec votre accord.

— Le vieux Pied-Tort ? Il fit une moue amusée. Pour ce qui me concerne, vous pourrez voir et parler à votre guise. Mais je ne saurais vous garantir que vous récolterez quoi que ce soit. Ils ne sont pas comme nous. J’en ai pris mon parti. Tant qu’ils ne me volent pas, je les laisse tranquilles. Mais je n’aime pas qu’on s’en prenne à mon bien, et ma main s’étend sur eux aussi pour les protéger. »

Son regard se fit lourd de menaces, tandis qu’il retournait à son siège où il se laissa tomber avec violence, faisant crisser le meuble sur le sol en bois.

« Vous avez mon aval aux fins de venir sur mes terres et parler à mes serfs. Je compte sur vous pour le faire dans le calme et sans heurts. »

Il s’appuya des coudes sur la table, dévisageant Ernaut.

« Il ne sera pas dit que Robert de Retest, sire de Salomé, n’apporte pas son aide à un sergent du roi, maître Ernaut de Jérusalem. Nous sommes des Francs, seigneurs de ce royaume. Nous devons nous porter assistance entre frères, sinon ces maudits païens finiraient par nous manger le foie. »

Ernaut acquiesça, souriant en guise de remerciement. Robert de Retest bâilla, les invitant de la main à partir. Alors qu’ils s’exécutaient, il les interpella, au seuil de la pièce.

« Vous les trouverez près de leur temple certainement. Aujourd’hui est jour de leur religion, ils ne travaillent donc pas. »

La chaleur, qui les avait oubliés un moment à l’abri des murs épais, se vengea aussitôt de leur fuite, et ils étaient en eau lorsqu’ils parvinrent au bas des escaliers. Ils marchèrent vers leurs chevaux, attachés à un piquet près de l’entrée. Iohannes parlait à mi-voix.

« Cela ne s’est pas trop mal passé, je pense.

— Il ne m’a pas fait l’air d’être bien terrible chevalier.

— Ne t’y fie pas, sa fame dépasse ses terres. Il a parlé du shaykh, qu’il surnomme le boiteux. Ici on dit parfois *al askal*, ce qui veut dire aussi le sage. Par jeu, les gens de son casal appellent donc Robert *al askaf*, le cruel.

— Il est si dur que ça ? »

Iohannes s’arrêta et soupesa le regard d’Ernaut, indécis. Puis il se remit en marche, parlant encore plus doucement.

« Certains seigneurs francs mutilent leurs serfs pour leur passer l’envie de désobéir ou de fuir. »

Ernaut haussa les épaules, indifférent.

« Mauvais valet se fait percer le poing et larron trancher la main. Ils n’ont pas à avoir meilleure coutume que nous.

— Briser les membres pour obtenir le respect me semble douteux comme pratique. »

Ernaut hocha la tête en accord, ne sachant pas vraiment ce que Iohannes entendait par là.

« Outre, il t’a bien fait comprendre que tu lui devrais service à ton tour et ce n’est pas homme à oublier pareille chose.

— Je serai fidèle à ma parole, je ne vois nulle raison de me dédire. »

Appuyé contre sa monture, Iohannes fixa Ernaut, s’essuyant le front du revers de la main.

« Méfie-toi de lui, Ernaut. Il n’est pas bienveillant, c’est tout ce que je veux te dire. »

Le géant se mit en selle, le visage fermé. Il se tourna vers l’escalier de la grande salle. Là, debout devant sa porte, le seigneur Robert les regardait partir, avalant quelque nourriture dont il jetait les débris à un chien lové à ses pieds. Ernaut talonna et soupira.

« Allons donc reconnaître le camp de ces larrons, nous verrons bien ce que les habitants du casal auront à nous dire après cela. »

### Abords de Salomé,vendredi 13 juin, midi

Le chant des grillons et des cigales instillaient la chaleur jusqu’aux oreilles des deux cavaliers. Malgré leurs chapeaux de paille, ils suaient abondamment et avaient vidé leurs gourdes. Leurs chevaux marchaient lentement, ondulant selon les courbes du terrain, évitant les roches les plus grosses et les cailloux les moins stables. Ils venaient de traverser une zone d’oliviers et grimpaient parmi la broussaille épineuse et les murets écroulés. L’air épais ne parvenait à leurs poumons que pour les réchauffer plus encore. Dans leur dos, la sueur s’égouttait, leur occasionnant de désagréables frissons en parcourant leurs reins. Ernaut arrêta son cheval sur la crête, se protégeant les yeux tandis qu’il regardait derrière eux. Des rochers, des buissons, à perte de vue.

« Je ne vois nul endroit où se cacher sur ces terres.

— Pourtant, il est vrai que la vieille tour offre belle vue sur le casal. Mais nul ne peut s’y établir sans se dévoiler. Si des brigands sont passés, ce doit être forcément par ici.

— N’y a-t-il pas quelques Bédouins qui viennent ?

— Pas tant au couchant. Ils ne s’écartent guère du Jourdain. Ils savent les dangers et ne s’aventureraient pas sur les terres de Retest.

— Ils sont protégés du roi.

— Parles-en à ceux que Baudoin lui-même a détroussés l’an dernier » répliqua Iohannes, contrarié.

Ernaut soupira, trop échauffé pour s’énerver.

« Il n’y a ici que chardons et épines, caillasse et rocaille ! »

Sans rien dire de plus, Iohannes fit avancer son cheval en direction de la vallée où était implanté le casal de Salomé. Accablé par la chaleur, il se tenait courbé, les rênes pendantes, laissant sa monture errer tandis qu’il tournait la tête de droite et de gauche à la recherche des traces d’un campement. Il leva la main soudain, interpellant Ernaut d’un sifflement.

« Écoute à senestre : des enfants semblent s’amuser dans les parages. Je ne les vois pas, mais il ne sont guère loin. »

Ernaut acquiesça et mena son cheval à sa suite. Bientôt, ils aperçurent un groupe d’adolescents parmi les broussailles, criant et bavardant. Ils étaient vêtus de tenues grises, longues robes sans forme, et de petits bonnets pour certains.

Ils étaient pratiquement une quinzaine de jeunes garçons qui s’agitaient malgré la chaleur. Lorsqu’ils aperçurent les cavaliers, le silence se fit brusquement et certains se dissimulèrent parmi les reliefs et les plantes. Seuls deux restèrent bien en vue, bravant fièrement du regard les cavaliers qui s’approchaient, dont l’un était porteur d’une masse et d’une épée. Iohannes s’arrêta à bonne distance et descendit, s’avançant à pied après avoir confié ses rênes à Ernaut.

« Il vaut mieux que je leur parle un peu, voir s’ils acceptent de répondre avant toute chose.

— Je te fais confiance, j’attends ici. »

Levant la main en geste amical, il sourit et reçut des inclinaisons polies de la tête en retour. Puis les discussions commencèrent. Ernaut ne comprenait pas ce qui se disait et entendait à peine. Mal à l’aise, il se réinstalla en selle, étira son dos, flatta sa monture. Autour d’eux, le ciel était devenu pâle à force de chaleur. Celle-ci assommait tous ceux qui osaient s’aventurer dans la campagne aride, brune. Pas un souffle de vent n’agitait les branches des buissons. Le jeune homme remua une langue cartonneuse, tenta d’humecter ses lèvres à sa gourde désormais vide. Iohannes lui fit signe finalement d’approcher.

« Ce sont des jeunes de Salomé, ils gardent les troupeaux par ici et travaillent aux champs dans le coin.

— Ils ont vu quelque chose ?

— Ils disent qu’il existe un endroit qui pourrait nous intéresser pas loin d’ici. »

Ernaut sourit aux jeunes en remerciement.

« S’ils nous aident, je suis prêt à leur donner un denier.

— Ils ne font pas ça pour l’argent.

— Ça peut leur donner du cœur à l’ouvrage.

— Soit, je leur dirai. »

Ils prirent le chemin qui descendait vers la vallée, longeant un creux qui s’y versait. À quelque distance, un décrochement était visible dans la côte, parmi les arbres malingres. Les jeunes se firent plus bruyants, tout le groupe bruissant autour d’eux maintenant qu’ils étaient en confiance. La haute stature d’Ernaut semblait inquiéter les plus petits et impressionner les plus grands. Il souriait, amusé et flatté d’être le centre d’intérêt. Lorsqu’ils entrèrent sur le replat en contrebas, il comprirent que l’endroit n’était pas naturel. Une cavité avait été creusée dans la roche, accueillant une demi-douzaine de niches. Ernaut interrogea Iohannes du regard, qui s’entretint avec les jeunes.

« C’est une grotte de l’époque des Romains selon eux. »

Ernaut mit pied à terre, laissant son cheval arracher les touffes d’herbes qui avaient pu se maintenir dans les environs. Le sol avait été pas mal piétiné et des traces d’un feu de petite taille étaient récentes. À n’en pas douter un excellent endroit pour s’installer à l’abri, discrètement, aux abords de Salomé et de la Mahomerie.

« Ce sont eux qui ont fait du feu ? »

La question amusa plusieurs des jeunes qui secouèrent la tête en dénégation, apportant de longues explications à Iohannes qui venait également de descendre de son cheval. L’interprète finit par résumer à Ernaut.

« Ils disent qu’ils préfèrent dormir chez eux que parmi chacals et serpents. »

Ils avaient vu un voyageur s’installer là récemment. Il devait venir de loin, car il avait une monture. Il était resté quelques jours, dormant là apparemment, mais se rendant vers la Mahomerie en journée.

« Ce pourrait être notre larron ! Était-il là avec l’autorisation du seigneur de Salomé ?

— Ils n’en ont aucune idée, de toute façon ils n’en ont pas parlé, de peur de s’attirer des ennuis. »

Ernaut réfléchit un instant.

« C’était un des leurs, un Syrien, ou un Franc comme moi ?

— Il n’était pas comme toi, mais ce n’était pas un homme d’ici. Il avait la tenue d’un voyageur. »

Tandis qu’Ernaut dévisageait les gamins en se demandant ce qu’il pourrait bien apprendre de plus, Iohannes continuait à s’entretenir avec eux. Finalement, il rapporta à Ernaut ce qu’il avait découvert.

« Il n’était pas très content de les voir et a chassé l’un d’eux en brandissant un bâton.

— Ils auraient pu le dénoncer pour se venger.

— Je te l’ai dit, ils se méfient de nous et préfèrent se tenir à l’écart.

— Ils ont dit quand il est parti ?

— Peu après le jour de prière de la semaine dernière, ils ne sont pas sûrs. Ils évitaient de venir là pour ne pas tomber sur lui. »

Ernaut s’accroupit afin de jeter un coup d’oeil dans les niches. Il posa la main dans les cendres du feu, puis se retourna vers Iohannes, entouré des gamins.

« Et pourquoi ils te disent tout ça ?

— Certains savent qui je suis. Je sers souvent d’interprète. »

Iohannes sourit soudain, une lueur joviale dans le regard.

« Je leur ai parlé de la possibilité de gagner une pièce d’argent, aussi…

— Tu me disais que c’était mauvaise idée.

— Ce ne sont que des gamins, moins sensibles à l’honneur que leurs anciens. »

Ernaut laissa s’échapper un rire bref et tira de la bourse qu’il gardait attachée à son braiel la pièce promise, la plus brillante qu’il put trouver. Il sourit à la cantonade, brandissant la récompense devant lui. Un des jeunes s’avança et la prit, le remerciant d’un signe de tête. Puis la compagnie disparut en un instant, comme un vol d’étourneaux.

Le chant des cigales et des grillons occupa de nouveau tout l’espace.

« Allons à Salomé voir si nous pouvons en apprendre plus sur ce voyageur.

— Si les gamins n’ont rien dit… rétorqua Ernaut

— Rien dit à leur seigneur franc, mais à leurs pères… Et puis d’autres ont pu voir celui qui se cachait ici. »

Iohannes attrapa les rênes de son cheval et l’approcha pour se remettre en selle.

« De toute façon, cela ne pourra que nous aider d’aller visiter le shaykh. Marquer ainsi notre respect déliera peut-être les langues.

— Et le ra’is ? Vu que c’est le chef du village.

— Lui, ce n’est pas pareil, on va aller le voir pour éviter qu’il ne nous cause des ennuis. »

Puis il monta en selle, assujettissant de nouveau son chapeau de paille, réglant ses rênes. Ernaut abandonna son examen du sol et régla la sous-ventrière de sa selle, qu’il estimait trop lâche. Il se tourna vers Iohannes.

« Tu ne l’aimes guère, le chef du casal de Salomé, hein ? »

L’interprète se mordit la lèvre, cherchant ses mots.

« Je peux comprendre un homme comme Retest. Il n’est pas mauvais au fond de lui. Il estime que ces hommes sont siens, tels bêtes ou coffres. C’est un seigneur, qui a vécu l’arme au poing… »

Il marqua un temps, secouant la tête comme s’il menait un dialogue intérieur, cherchant à se convaincre lui-même.

« Mais certains sont veules et prêts à toutes les compromissions, juste pour s’offrir un beau turban ou séduire le regard des femmes. Je n’apprécie pas ce genre d’homme, à cœur de chacal, à l’esprit tortueux comme Dimna. »

Devant le visage interdit d’Ernaut, Iohannes expliqua :

« C’est un animal menteur et trompeur dans la fable. Abu Qasim al-Dabbi doit connaître le conte, mais penser qu’en effet les gouvernants s’attachent non pas aux plus nobles, mais aux plus près de lui. Et il parle votre langue, Ernaut, il te faudra lui parler avec méfiance.

— Si je suis là avec l’aval de Robert de Retest, que peut-il me faire ?

— Ne le sous-estime pas. Il est détesté, mais il dirige ce village et le représente auprès du seigneur, dont il a l’oreille. »

Comprenant qu’il s’aventurait en terrain mouvant, Ernaut entendit le conseil et sourit en guise d’assentiment.

« Très bien, alors allons voir ton shaykh d’abord, vu qu’il est plus ouvert.

— Je n’ai jamais dit ça. Il soulage son peuple et se comporte en homme vertueux. Deux raisons qui font qu’il ne nous porte guère dans son cœur lui non plus, et d’autant moins que nous serons là avec la bénédiction de Retest justement. C’est lui qui lui a brisé la cheville voilà quelques années. »

### Casal de Salomé,vendredi 13 juin, après-midi

Le village de Salomé ne ressemblait pas aux établissements de colons francs. Il s’accrochait à la roche, se noyait dans la pente, agglutinait ses murs, ses terrasses, en entrelacs serrés. Un seul passage sinueux méritait le nom de rue, sinon ce n’étaient que venelles, passages pentus, ruelles étroites, escaliers branlants, qui menaient en une demeure ou l’autre.

Partout l’odeur âcre du feu de bouse séchée s’affrontait aux effluves des tas de fumier, fièrement exhibés dans les courettes aperçues par les portes entrebâillées. Des cris d’enfants, comme partout, résonnaient entre les murs, ainsi que des voix de femmes s’interpellant, riant ou réprimandant. De nombreux groupes d’hommes, vêtus de thwabs fins, de ceintures de tissus, de turbans habilement dressés déambulaient, n’accordant qu’à regret le passage aux deux cavaliers. Pourtant, certains visages osaient se fendre d’un signe de salut, voire d’un sourire en reconnaissant Iohannes. Mais la tension était visible. Lorsqu’ils apparaissaient, la vie se retirait, telle une vague orgueilleuse. Si la tour du château de Robert de Retest s’apercevait au détour d’un passage, ils étaient là chez eux et n’aimaient pas y voir des étrangers, fussent-ils les seigneurs.

Ils mirent pied à terre devant l’entrée d’une petite cour et Iohannes en interpella les habitants, frappant du poing sur les panneaux de bois. Une vieille femme, voûtée et ridée, afficha par l’entrebâillement son visage voilé. Si le corps était brisé par le travail, la volonté qui se lisait dans le regard d’ébène était intacte. Après quelques saluts et échanges de paroles, elle disparut en direction de la maison, poussant l’huis derrière elle.

Durant leur attente silencieuse, un chat sauta depuis un toit voisin sur le haut du mur, étudiant les importuns. Estimant qu’ils ne semblaient pas constituer un danger, il entreprit sa toilette, surveillant de part et d’autre de la clôture. Puis il disparut en deux bonds, peu avant que la porte ne s’ouvre. La vieille femme s’effaça, laissant les visiteurs entrer.

La cour était petite, avec un enclos où des poules tenaient compagnie à un petit âne au pelage poussiéreux. Sur le côté, un auvent accueillait le four à pain, grosse cloche de terre à la partie sommitale tronquée, ainsi qu’une réserve de bois. Au fond, un appentis branlant finissait de s’écrouler doucement, encombré de débris divers et d’objets entassés pêle-mêle.

Après avoir attaché les chevaux à un piquet, ils suivirent la femme devant le bâtiment qui s’étendait à droite. Ils dépassèrent la première petite porte et elle se mit de côté devant la seconde, indiquant du bras qu’ils pouvaient entrer, puis elle retourna à ses tâches, sans plus de cérémonie. En entrant, Iohannes lança un « Salaam aleikum ya Abu » respectueux, inclinant le buste, et Ernaut l’imita aussitôt. Une voix éraillée leur répondit selon l’usage.

Ils aperçurent alors dans la pénombre de l’endroit un homme assis sur des nattes. Il avait des matelas roulés lui servant de dossier. Un coffre en planches grossières accueillait quelques lampes à huile éteintes. S’habituant à l’obscurité, Ernaut put discerner les traits de leur hôte. On ne voyait d’abord de lui que l’imposante barbe, qui tombait sur sa poitrine en un paquet duveteux. Elle semblait faire écho au turban de belle taille, de couleur claire, qui ornait le crâne. Entre les deux, le nez semblait avoir tenté de leur faire concurrence, s’avançant aussi loin que possible au-dessus d’une lèvre supérieure dénuée de moustache. Les yeux, noyés de rides, semblaient attendre quelque proie, embusqués, à l’affût.

Il sourit de façon appuyée, trop peut-être, les invitant à prendre place face à lui. Bientôt, la vieille femme revint, toujours silencieuse, et déposa devant eux un pichet d’une boisson à l’odeur saline, ainsi qu’un petit pot empli d’olives, du fromage frais et du pain plat. Elle laissa la porte entrouverte en repartant.

Le vieil homme toussa et leur fit signe de la main de se servir, puis il s’adressa à Iohannes, qui traduisait tout à Ernaut au fur et à mesure. Il était visiblement habitué à servir d’intermédiaire dans les échanges avec les Syriens. La boisson était du chneiné, à base de lait, expliqua-t-il. Puis il se servit à son tour, dévisageant les arrivants tandis qu’il avalait tranquillement quelques bouchées.

Il s’enquit de la météo, savoir si elle était clémente dans les régions qu’Ernaut fréquentait. Il lui parla des moissons, qui étaient, grâce à Dieu, correctes, et espérait en la clémence du Très-Haut pour la récolte des olives. Iohannes expliqua que ces banalités permettaient de se connaître un peu avant d’en venir au vif du sujet, qui serait abordé une fois la collation terminée. Un coq aventureux vint jusqu’à l’entrée, caquetant d’un air sombre, puis s’éloigna. Le silence avait fini par s’installer, sans que cela semble perturber Abu Mahmud. Finalement il repoussa les pots et se rencogna contre ses dossiers. Alors il posa la question qu’Ernaut attendait.

« Qu’est-ce qui a mené tes pas jusqu’à ma demeure, géant Ifranj[^ifranj] ?

— Je suis à la recherche d’un voyageur qui se serait caché dans les collines. Peut-être en as-tu entendu parler ? »

Le vieil homme demeura muet un instant avant de répondre.

« En quoi cela te concerne, toi qui n’est pas de Salomé, ceux qui vont et viennent sur les terres du sire Robert ?

— Mon frère doit se battre pour prouver l’innocence du père de sa femme. Je crois que si on montre que c’est un autre le coupable, il n’y aura pas de combat.

— J’ai entendu dire que les Francs se battaient pour régler leurs procès, oui. »

Il sourit, amusé par l’idée, se lissa la barbe tout en reprenant son sérieux.

« Mais je respecte celui qui défend les siens, il les honore et s’honore lui-même. »

Il secoua la main comme pour en détacher quelque poussière et reprit.

« Il y a bien des voyageurs qui vont et viennent par ici. Peux-tu m’en dire plus ?

— Il se serait caché dans les tombes romaines au sud d’ici, à une demi-lieue des ruines de la vieille tour.

— Il aurait tué l’un des vôtres ?

— Un nommé Ogier, un paysan du casal de la Mahomerie. Du moins j’essaie de m’en assurer. »

Abu Mahmud secoua la tête, semblant approuver ce qu’Ernaut lui disait. Son regard épiait Iohannes chaque fois que ce dernier traduisait et il se demanda un instant si le vieil homme ne comprenait pas ce qui se disait en fait, et vérifiait que ses paroles n’étaient pas dénaturées.

« Il y a bien eu un voyageur comme tu le dis, oui, mais je n’en sais guère plus. Il n’est pas venu jusqu’à notre village.

— Personne ici ne le connaît ?

— Il aurait été accueilli et hébergé en pareil cas. Nul ne dort parmi les pierres si un foyer ami est proche.

— Celui qui a quelque chose à cacher, peut-être ? »

Le shaykh fit un soupir amusé et bougea un peu, grimaçant comme s’il réveillait d’anciennes douleurs.

« Peut-être as-tu entendu parler d’Ogier alors ? C’était un paysan qui parlait votre langue, arrivé voilà quelques mois.

— Nous ne fréquentons pas les gens des autres villages.

— On le disait assez amical avec vous.

— Je ne crois pas qu’un Ifranj ait jamais été accueilli ici comme un ami, mais je ne prétends pas tout savoir. »

Ernaut se demanda un moment si Iohannes reportait fidèlement les paroles. Il sentait une réticence dans les phrases, une hésitation, ou une subtilité qui lui échappait. Il n’aimait pas devoir recourir à un tiers pour échanger, certain de toujours perdre des nuances essentielles. Il ne pouvait mettre le doigt dessus, mais il lui semblait que le vieil homme avait une attitude moins désinvolte que ses paroles pouvaient le laisser croire. Il craignait peut-être qu’Ernaut ne cherche à reporter la faute sur un des membres de sa communauté et atermoyait pour éviter cela. Ou alors il en savait plus et ne considérait pas Ernaut comme digne de confiance, ce qui vexait le jeune homme et commençait à l’irriter.

« Aucun de vous ne se rend jamais à la Mahomerie, donc ?

— Pourquoi irions-nous ? Le sire de ce lieu est à côté, et nous lui portons nos redevances. Les tiens ne savent pas notre langue, et nous ne connaissons pas la leur. Nos usages sont différents… »

Sa voix se fit plus douce.

« Même notre foi est différente. De quoi parlerions-nous ? Nous ne sommes pas commerçants, juste de pauvres fellahs, qui labourent le sol pour en tirer de quoi vivre. Nul besoin d’aller faire visite à l’étranger qui s’acharne pareillement à côté. C’est parmi les siens qu’on est le mieux.

— Vous pourriez discuter des chemins à refaire, des terres gastes à gérer, du passage des troupeaux…

— Cela n’est pas notre affaire, mais celle des maîtres.

— Chez nous, chacun est maître de son bien.

— Remerciez-en Allah, alors, car c’est de lui que tout vient. »

Ernaut s’agitait, agacé par les réponses évasives.

« Vous ne m’aiderez pas à sauver mon frère ?

— Je ne peux rien pour lui, ni toi, d’ailleurs. Tout ça est la volonté d’Allah. »

Comprenant que son invité n’était pas satisfait de l’entretien, Abu Mahmud sourit amicalement, inclinant la tête tout en caressant sa longue barbe.

« Je ne veux pas te manquer de respect après t’avoir accueilli chez moi. Mais tu poursuis une ombre et tu ne pourras apporter la justice. Si ton frère est honnête en son cœur, il n’a rien à craindre. Si un félon a tué, ou est mort, ce ne peut être contre la volonté d’Allah, tu n’as pas à t’en inquiéter.

— Je suis inquiet, car mon frère doit se battre contre un autre homme, qui n’est peut-être pas méchant lui non plus, mais seulement dans l’erreur.

— C’est là une des choses que je ne comprends pas chez vous. Plutôt que d’éprouver le cœur de l’homme en le confrontant à un sage cadi, vous le faites se battre contre son accusateur.

— Dieu ne laisserait jamais un innocent être vaincu. »

Abu Mahmud prit un air grave et rétorqua sans malice et d’une voix bienveillante :

« Si tu en es si sûr, pourquoi viens-tu me questionner à propos d’un voyageur ? »

Lorsqu’ils saluèrent le vieil homme, Ernaut affichait un air morne. La discussion ne lui avait pas appris grand-chose et, pire encore, elle avait semé le doute en lui. Il avançait la tête dans ses pensées, menant son cheval par la bride à côté de son compagnon. Iohannes le comprit et lui sourit d’un air engageant.

« Sacré gaillard, hein ?

— Il semble glissant comme serpent, mais empli de sagesse. Je ne saurais dire si j’ai envie de le faire taire ou de l’écouter plus encore.

— Il est le cœur de ce village, Ernaut. On le dit shaykh, car sa parole mérite toujours d’être entendue. Il ne nous aime pas, c’est certain, mais il essaie, à sa façon, de se montrer juste.

— Tu penses qu’il nous a tout dit ?

— Lui ? Il n’épuisera jamais aucun sujet, bien plus habitué à faire naître la réponse chez les autres qu’à la fournir. Mais pourtant… »

Ernaut s’arrêta soudain, soulagé de voir qu’il n’était pas seul à tenter de démêler le vrai et l’utile du faux et du mensonger dans ce qu’il venait d’entendre.

« Pourtant ?

— Je sais que certains du casal discutent parfois avec des hommes d’ici, lorsqu’une bête s’enfuit ou pour calmer une dissension naissante. Abu Mahmud ne peut ignorer ce fait et il doit savoir que j’y suis certainement impliqué.

— Il a donc menti ? »

Iohannes haussa les épaules.

« Pas sans bonne raison.

— Laquelle ? Faire mourir Godefroy, mon frère ?

— Je n’y crois guère, il n’est pas si retors. Il se dit que peut-être tout cela se terminera avec le combat.

— Il est au courant ?

— Il a certainement été un des premiers à le savoir ici…

— Avant le ra’is ?

— En même temps au moins, mais lui se sert de ce qu’il sait pour aider les siens.

— Ce n’est pas le rôle de celui que nous allons voir ?

— Tu jugeras. Moi je n’ai pas l’impression qu’il y ait de la place pour un autre que lui-même en son cœur. »

### Casal de Salomé, vendredi 13 juin, après-midi

La demeure du ra’is était située aux portes du village, sur les hauteurs. L’entrée en était encadrée par deux palmiers qui apportaient une ombre bienvenue aux voyageurs. Assis là, tressant de la paille pour en faire de larges paniers, un vieil homme les dévisagea sans leur parler. Il se fendit d’un sourire édenté, levant à peine le nez de son ouvrage. Le portail d’entrée, orné d’une khamsa[^khamsa] brune sur chaque vantail, était en partie ouvert. Ayant attaché leurs montures, les deux hommes pénétrèrent, non sans avoir frappé ostensiblement sur le panneau de bois.

La cour, assez vaste, accueillait un bâtiment bas à leur droite, d’où provenait une forte odeur animale. De l’autre côté, un édifice avec deux portes et des fenêtres de petite taille était bordé d’une zone où des plantes tentaient désespérément de survivre à l’assaut brutal du soleil. Devant une des ouvertures, dont le linteau était souligné d’un décor de bois sculpté, un bel aliboufier étendait ses branches. Là, installé sur une natte, un jeune homme lisait des documents qu’il sortait d’un petit coffre à ses côtés.

Il avait ouvert devant lui un nécessaire à écriture. En les apercevant, il se leva et vint les accueillir. Il n’avait pas plus de vingt ans, le visage glabre, le regard franc surligné d’un sourcil quasi ininterrompu. Ses cheveux étaient couverts d’un petit bonnet de laine à motif, et il ne portait qu’un thwab simple, mais de bonne facture, dont les pans s’envolaient à chacun de ses pas vifs. Il les salua et échangea quelques mots avec Iohannes avant de disparaître par la porte ornée. Les deux compères n’eurent pas le temps d’échanger une parole que déjà leur parvenait une voix de l’endroit.

« Des visiteurs ? Dans mon humble demeure ? Je veux leur tenir la main pour les inviter ici ! »

Jaillit alors du bâtiment un homme d’une cinquantaine d’années. Sa silhouette élancée était cachée dans les replis d’un vaste manteau orné de bandes colorées. Son visage, creusé de profondes rides, semblait n’avoir fait que sourire toute sa vie, et même ses yeux, couleur d’ambre, scintillaient avec joie. La barbe rase était blanche, et les favoris se perdaient sous l’épaisse étoffe d’un turban grossièrement drapé. Ses lèvres fines s’arquaient de façon amicale, tandis qu’il avançait en se frottant les mains.

Il s’inclina et les accueillit avec chaleur, les invitant à le suivre. La pièce, pour n’être pas très spacieuse, s’avérait confortable. Des banquettes en faisaient le tour, parées de couvertures et de tapis ras. Le sol était recouvert de plusieurs épaisseurs de nattes, confortables sous le pied. Habillés des meilleures pièces d’étoffe, les divans du fond étaient également garnis de coussins. Abu Qasim en désigna un à chacun des visiteurs, donna quelques instructions au jeune homme, et, se déchaussant, s’installa sur l’emplacement du fond, ramenant ses pieds sous lui. Dans son dos, un kilim aux motifs géométriques était tendu, faisant de son siège comme un trône avec son dais. Il mit un peu de temps à arranger sa tenue, souriant alternativement à l’un et à l’autre. Ce n’est que lorsque des boissons et quelques morceaux de pain plat, accompagnés d’une sauce verte, furent servis qu’il ouvrit de nouveau la bouche tout en portant la main à son cœur.

« C’est là nourriture de ma terre, mangez, buvez… ».

Lui-même grignota délicatement un peu de pain. Il les entretint de ses moutons et de ses chèvres, qui avaient fourni de quoi faire la boisson qu’il leur offrait. Sa voix douce, modulée avec soin, était plaisante, et l’homme n’était pas sans charme. Ernaut était d’autant plus intrigué par les mises en garde de Iohannes. Il écoutait, acquiesçait poliment tout en mangeant quelques bouchées. Profitant de ce que leur hôte employait sa langue, il fit des compliments sur les mets et montra quelque intérêt à ses remarques sur son exploitation. Ce n’est que lorsqu’il appela son valet pour les resservir de nouveau qu’Abu Qasim posa enfin la question de savoir ce qu’ils lui voulaient. Échangeant un rapide coup d’œil avec Iohannes, Ernaut le laissa débuter l’entretien, préférant ne pas commettre d’impair.

« Nous sommes sur la piste d’un voyageur qui se serait caché dans les terres de Salomé tout récemment, et qui espionnait notre casal depuis la vieille tour.

— Sire Robert m’a fait savoir cela, que je puisse vous aider de mon mieux. Avez-vous quelques détails à son propos ?

— Guère, c’est un Syrien apparemment et il a disparu après plusieurs jours à se cacher dans les collines. »

Le ra’is hocha la tête plusieurs fois.

« Il serait lié à cette affaire de murdrerie de votre casal ?

— Peut-être, c’est ce que nous souhaitons savoir. »

Abu Qasim frappa plusieurs fois dans ses mains pour faire venir le jeune homme. Il s’adressa à lui d’une voix forte. Ernaut, qui ne comprenait pas ce qui se disait, le trouvait un peu théâtral, à appuyer ainsi ses gestes ou saluer l’un ou l’autre d’un signe de tête tout en parlant. Très vite, ils se retrouvèrent de nouveau trois dans la petite pièce, et le ra’is expliqua :

« J’ai fait savoir cela au casal. S’il y a quelque chose à découvrir, je l’apprendrai. À votre intention, bien sûr. Je suis au service des invités du sire Robert. Il n’est nulle chose ici qui se fasse sans que je le sache, c’est mon rôle. La plupart des fellahs ont besoin d’être tenus et les rumeurs courent plus vite que la gazelle !

— Vous connaissiez Ogier, l’homme qui a été poignardé ? demanda Ernaut.

— Pas que je sache. Bien long temps que pas un Ifranj du casal voisin n’est venu ici. À quoi ressemblait-il ? »

Ernaut lui traça rapidement le portrait de la victime, expliquant qu’il n’était arrivé que récemment, mais s’enorgueillissait de parler le syrien.

« Cela ne me dit rien. Il a peut-être eu plus de contacts avec les Bédouins. Plusieurs tribus font paître leurs bêtes non loin d’ici, vers le Wadi Qelt. Ils commercent volontiers avec les vôtres, dit-on. »

Ernaut fronça les sourcils et réfléchit un instant.

« Le voyageur aurait-il pu être un nomade ?

— Le propre du voyageur est de vivre au-dehors, mon ami. Le ciel est son toit et les buissons ses murs. Un fellah préfère la solidité de la pierre, la protection des moellons.

— Les Bédouins voyagent généralement en groupe, non ?

— Celui-là avait peut-être à faire une tâche solitaire. Si c’est bien lui qui a plongé sa lame dans le cœur de votre ami, on le comprend aisément. »

Un sourire ambigu s’esquissa sur son visage, donnant à ses yeux qui se plissaient une lueur rusée.

« Il existe des hommes terribles, dans le nord et l’est, dont le maître obscurcit les pensées par des drogues. Ils vont ensuite au loin et frappent celui qu’on leur a désigné. Ils sont comme des ombres et peuvent tuer n’importe qui, prince, roi ou simple serviteur. Personne n’est à l’abri de leurs lames terribles.

— En quoi un paysan de la Mahomerie aurait pu les gêner ?

— Seul Allah le sait, mon ami. Ils avaient peut-être de vieilles histoires à régler. Le cœur des hommes n’incline pas toujours au pardon, et les vieux contentieux sont souvent ceux payés avec le plus d’intérêts. »

Ernaut soupira, cherchant dans le regard de Iohannes une réaction. Mais celui-ci demeurait imperturbable, avalant par petites gorgées sa boisson, assis en tailleur sur la banquette. Il semblait étudier le ra’is avec grand intérêt. Ernaut mangea un morceau de pain avant de reprendre ses questions.

« Aucun des hommes d’ici n’aurait pu se rendre au casal, avoir des affaires avec Ogier ? »

La question parut contrarier le vieil homme et, un instant, son visage perdit son masque débonnaire pour se faire sévère. Pourtant, ce fut avec un air détaché, presque amical qu’il répondit.

« Cela n’est pas possible. Aucun ici n’a le droit de quitter les terres du sire Robert. L’anneau de fer au poignet est le signe de leur servitude. Moi et quelques autres en sommes dispensés, en tant que serviteurs zélés, mais malheur à celui qui s’éloigne, même pour ses affaires.

— On prête au sire Robert de bien tristes histoires, en effet.

— C’est là récits qu’on ferait mieux de ne pas trop écouter. Nul n’a à se plaindre à Salomé. Les redevances ne sont pas élevées, on nous laisse demeurer dans notre foi et aucun Turc ne vient prendre nos femmes quand cela lui chante. Moi je le dis souvent, rien de meilleur n’est arrivé à cet endroit que d’être dans les mains des Ifranjs. Nous pouvons semer, tailler, récolter et faire grandir nos enfants sans craindre pour nos vies. N’est-ce pas le rêve de chacun ? »

Ernaut acquiesça en silence, tandis que le ra’is continuait.

« Je sais bien que d’aucuns esprits chagrins nous accablent de notre peu de foi, que nous ne devrions accepter cela. Mais Allah a voulu qu’il y ait des maîtres et des valets. Que certains hommes soient faits pour obéir, d’autres pour ordonner. Il vaut mieux servir dans la prospérité que dépérir son propre maître. Nulle honte à être là où Allah nous a fait naître. Certains disent que leurs ancêtres vivaient sous la tente, avec les étoiles seules qui les dominaient. Moi je leur dit : retournez-y. Quand le chacal viendra dévorer votre troupeau ou le lion enlever votre enfant, on verra bien qui est le plus heureux, de vous ou de moi. »

La dernière phrase avait été lancée rageusement, vibrante d’un accent de sincérité renversant toutes ses précautions oratoires précédentes. Il se reprit, se redressant tout en arrangeant sa tenue, marmonnant, peut-être vexé de s’être ainsi emporté. Ou bien jouant talentueusement son rôle jusqu’au bout. Il laissa passer un moment, les yeux mi-clos, ne manifestant un semblant de vie qu’en entendant un aboiement de chien proche. De nouveau, son visage était souriant et avenant, impénétrable face de courtisan.

« Mais je vous ennuie avec mes histoires. Vous avez fort impérieuse tâche et me voilà à vous faire perdre votre temps, comme une vieille femme narrant ses contes pour enfants. »

Iohannes lui répondit d’un sourire et d’un haussement d’épaules puis, cherchant l’accord d’Ernaut d’un regard, prit poliment congé du ra’is. Celui-ci ne les laissa partir qu’une fois généreusement bénis, pour plusieurs générations, et assurés de son amitié et de son soutien. Il les abandonna au portail, les saluant tandis qu’ils s’éloignaient du casal sur leurs montures. Il semblait aux deux hommes que sa main était encore sur leur épaule et son oreille aux aguets alors même qu’ils franchissaient la crête menant au plateau vers la Mahomerie.

Le pas des chevaux claquait sur les rochers, soulevait une poussière qui s’attachait à leurs chevilles, leurs jambes. Le soleil invaincu triomphait, déversant des langues de feu qui enflammaient cette fin de journée. Vers le levant, sur la grande route qui menait à Naplouse, une fumée épaisse aurait pu naître de ce brasier. Ce n’était qu’une caravane ou un large convoi, pèlerins, marchands ou soldats soulevant la poussière grise du chemin à chaque pas. Ernaut les pointa et demanda :

« Ce pourraient être des nomades, nous devrions nous enquérir d’eux. Ils connaissent peut-être notre homme.

— Les Bédouins ne suivent jamais les routes. Ils savent leurs chemins, bien plus anciens, et se targuent d’être habiles à traverser un pays sans aucune voie. »

Iohannes se raidit sur sa selle, huma le temps et fronça les sourcils.

« Du vent s’enfuit du désert…

— En effet, la poussière vient à nous. Cela nous fera du bien que cette brise.

— Ernaut, il doit brûler comme fournaise !

— Je préférerais le moindre souffle d’air, même brûlant, à cette sensation de respirer dans un four. »

Iohannes se tourna vers Ernaut, l’air narquois.

« Tu ne devrais jamais dire de pareilles choses, mon ami.

— Pourquoi ça ?

— Parce que parfois, le Très-Haut t’exauce. »

### Environs du casal de la Mahomerie,vendredi 13 juin, début de soirée

Les pas des chevaux résonnaient, roulaient dans le chemin poussiéreux. Des hommes occupés à tirer une carriole alourdie de pierres saluèrent de loin, s’arrachant un instant à leur harassante besogne. Quelques moutons, surveillés par deux pâtres négligemment installés sous un arbre s’écartèrent à l’approche d’Ernaut et de Iohannes, sautant parmi les reliefs, comme cherchant à fuir un prédateur. Lorsque les deux cavaliers arrivèrent à la vieille tour aux abords du casal, ils ne s’arrêtèrent pas.

La vue était dominante, et les rognons de murs permettaient à qui voulait de se camoufler à la vue de ses habitants. Un excellent point d’observation. Sur la route venant de Jérusalem, qui serpentait ensuite jusqu’à Naplouse au nord, un nuage de poussière s’accrochait aux sabots d’un messager pressant sa monture. Certainement un messager remarqua Iohannes. La vieille reine mère, Mélisende, y résidait, et bien qu’elle se soit retirée du pouvoir, elle avait fait de Naplouse une ville de première importance rien que par sa présence. Ernaut releva les accents de dépit que le Syrien mettait dans ses mots et lui demanda ce qu’il en était.

« La grand-mère du roi était fille de Ghavril Malatyatsi[^ghavril], un grand prince de mon pays. »

Son visage fut parcouru d’une telle douleur à cette évocation qu’Ernaut préféra garder le silence. Mais l’interprète continua.

« La reine Mélisende est comme moi, fille de deux peuples. Et avec ses sœurs, elles ont gouverné toutes les terres des seigneurs croisés. Mais leur temps est passé… »

Il pencha la tête, en proie à ses démons, le visage fermé. Il n’avait pas envie d’en dire plus. La suite du parcours se fit en silence, ponctué par les piaffements. Les bruits du casal les atteignirent lorsqu’ils franchirent la grande route et parvinrent à l’entrée nord, donnant droit sur l’église Sainte-Marie. De temps à autre, un visage s’éclairait, une main se levait. Lorsqu’ils arrivèrent devant la demeure de Lambert, Ernaut tira les rênes et s’apprêtait à descendre. Iohannes l’interrompit dans son geste.

« Descends donc avec moi voir Pisan. Je dois lui ramener son cheval.

— L’intendant ? Tu crois qu’il pourrait nous prêter assistance ?

— Il est de son devoir de connaître les gens du casal et il est homme de confiance, droit et juste. Outre tu pourras voir ton frère. »

Acquiesçant d’une moue, Ernaut remit sa monture en marche, la flattant d’une main distraite. Tandis qu’ils avançaient dans le quartier du Puits de la Vierge, les saluts qui s’adressaient à Iohannes étaient souvent interrompus à la vision d’Ernaut. Nul doute que chacun avait pris fait et cause pour un champion, et Lambert n’était pas des leurs, pas plus que Godefroy.

Comme habituellement, les portes de la curie n’étaient pas closes et un chien allongé faisait office de garde. Il ne daigna pas se pousser, se contentant de remuer la queue en levant la tête à leur passage. Un valet s’approcha néanmoins, pour récupérer la monture de Iohannes et aller attacher celle d’Ernaut. Lambert et Aymeric n’étaient pas visibles, ils venaient d’être menés à l’église pour y prier. Ils seraient de retour à la nuit.

Le domestique leur indiqua ensuite où ils pouvaient trouver Pisan, occupé dans un des celliers. Sa voix forte les accueillit lorsqu’ils s’approchèrent du bâtiment. Il était occupé à trier des tonneaux de vin, dont l’odeur de tannin rappela à Ernaut des souvenirs d’enfance. La fraîcheur qui venait de la cave, à laquelle on accédait par plusieurs marches, les assaillit lorsqu’ils arrivèrent dans la pénombre. Des silhouettes se mouvaient parmi les lueurs des lampes à huile qui fumaient en grésillant. Iohannes salua poliment, accompagné par Ernaut.

L’homme était, comme souvent, habillé d’une longue cotte trop grande pour lui et ses cheveux frisés dépassaient d’un bonnet oriental en pain de sucre. Sa courte barbe était percée d’une bouche toujours souriante, comme ses yeux.

« J’ai quelques tonnels à repérer encore et je suis à vous. Un convoi pour les frères est prévu demain, j’ai loué douze bêtes. Il me faut maintenant trouver de quoi les charger. »

Il continua à indiquer à deux hommes les barriques qu’ils devraient hisser au-dehors pour les installer sur les dos des animaux.

Le vin finirait dans les pichets du réfectoire du Saint-Sépulcre. Pisan sourit ensuite aux deux arrivants, les invitant d’un geste à les suivre au-dehors.

« Il est fort dommage qu’en la sainte Cité, les frères n’aient que petits celliers. Il nous faut leur porter les choses petit à petit, ce qui nous coûte fort cher ! Si j’avais pu joindre ce vin au miel, à l’huile et à la cire de la semaine passée… »

Il ne termina pas sa phrase. Sa démarche était encore alerte, mais son dos légèrement voûté, son accent sifflant en raison du manque de dents, attestaient de son âge. Il se rendit jusqu’à la grande salle à l’étage. Ernaut y fut tout de suite frappé par la différence d’avec celle de Salomé :ici pas de décor sur les murs, simplement badigeonnés de blanc. Des fenêtres y apportaient une douce clarté, à travers des volets de bois percés de croix. Le long d’un mur, une imposante table était encombrée d’un fatras de tissus, de vieux vêtements, de sacs. Deux chats y faisaient la sieste, indifférents à ce qui se passait autour d’eux. En dessous, de vieilles savates, des souliers usés prenaient la poussière.

Sous les fenêtres, un banc longeait un autre plateau, où était posé un nécessaire à écriture, des documents soigneusement empilés, à côté d’un modeste coffre ferré. Au mur, flanqué de deux lampes suspendues en verre, un petit Christ en croix en bois peint semblait veiller sur ce monde poussiéreux. Pisan les invita à s’asseoir sur le banc, prit lui-même place sur un tabouret, après avoir crié qu’on leur apporte de quoi boire. Rangeant les documents dans le coffre qu’il ouvrit d’une clef issue de sa bourse, il s’enquit poliment d’eux. Iohannes le remercia du prêt du cheval, interrompu par l’arrivée de la boisson. Ce fut Ernaut qui reprit la parole ensuite.

« Sire intendant, nous avons découvert qu’un larron est peut-être le murdrier d’Ogier, il n’est nul besoin de faire bataille.

— Vous avez témoins ?

— Des serfs de Salomé ont vu cela, oui.

— Si ce sont des mahométans, ils ne peuvent témoigner en la cour, tu dois bien l’assavoir en tant que sergent du roi. »

Ernaut se renfrogna.

« Peut-être qu’Aymeric accepterait de les entendre et retirerait son accusation.

— Peut-être… » abonda Pisan, sans conviction.

Il se pencha vers la fenêtre et poussa le volet doucement, qui céda en grinçant. Le soleil s’engouffra sur eux, frappant le bois sombre de la table avant de venir s’animer sur les murs chaulés. L’intendant dévisagea un instant Ernaut, puis lui sourit avec aménité.

« Je ne suis guère heureux de ce jugement par bataille moi non plus, mais je n’ai nul pouvoir en cette affaire. La justice du roi doit suivre sa voie.

— Vous ne croyez pas Godefroy coupable non plus ?

— Je ne sais pas et suis heureux de ne pas avoir à le découvrir. Dieu pourvoira à nos inquiétudes le moment venu. Il aurait été plus avisé de faire jurer sur des reliques ou de soumettre au feu ou à l’eau l’accusé.

— Une ordalie ? Juge-t-on ainsi dans les lieux du Saint-Sépulcre ?

— Non, rarement, à mon grand regret. Verser le sang de nouvel ne me paraît guère indiqué aux fins de punir de celui répandu. Il est d’autres voies pour connaître la volonté du Seigneur. »

Il avala un peu de vin, souriant. Ernaut n’en démordait néanmoins pas et il se pencha en avant, les yeux rivés sur l’intendant.

« Ne croyez-vous pas qu’un larron, ou des brigands auraient pu venir piller les biens d’Ogier nuitamment ?

— Tu dis vrai, jeune homme. Ogier avait du bien, c’était connu.

— Il avait grandes parcelles au casal ?

— Pas forcément, mais belles terres, avec des vignes de valeur, à la redevance légère, un cens très léger en grande partie, plus le champart. Il était venu de Saint-Gilles avec de quoi s’installer dans l’aisance. »

Iohannes intervint alors, intrigué.

« À dire le vrai, nombreux ici s’interrogent sur le fait qu’un homme sans épouse ait pu avoir si belle tenure, frère Pisan.

— Je les comprends. Mais nous n’avons pas tant de demandes que ça et il avait signifié son envie de prendre famille ici, d’avoir des enfants. Sans compter qu’il avait belle fortune pour créer moulin, ou tout ce qu’il aurait voulu. Il n’est pas fréquent d’accueillir si riche colon.

— Savez-vous comment il avait amassé tout cela ?

— Je crois qu’il a mené des convois en son temps et il a gardé d’ailleurs quelques bêtes. Vous devriez en parler avec Guillaume. Lui aussi est de Saint-Gilles, et il doit en savoir bien plus long que moi. Il réside à côté de Pierre le Charpentier. »

Les noms semblèrent évoquer quelque chose à Iohannes, aussi Ernaut s’abstint de demander des précisions. Pisan regardait par la fenêtre entrouverte, au loin, comme remâchant une idée agréable, un sourire évanescent sur les lèvres. Il les avait oubliés. Un des chats sauta sur la table et vint se frotter à son bras, ce qui le ramena parmi eux. Il prit l’animal et commença à le flatter, ce qui déclencha un ronronnement de félicité.

Ernaut se leva, souriant en guise de remerciement, Iohannes à ses côtés. Il était abattu, ne sachant que faire pour empêcher que son frère ne se batte le lendemain. Prouver que le crime était le fait d’un rôdeur ne pourrait se faire dans la nuit, il en était conscient. Il était malgré tout décidé à ne pas abandonner, à tenter de convaincre lui-même Aymeric le lendemain matin. L’homme n’était peut-être pas si obtus et se rendrait à l’évidence. L’intendant les accompagna jusqu’à la porte, souriant toujours. Il posa une main amicale sur le bras du géant.

« Ne t’enfrissonne donc pas ainsi, jeune. Dieu entend les cœurs sincères, fie-toi au ciel.

— Que voulez-vous dire ?

— Rien de plus que ces simples mots, répliqua le vieil homme, une lueur dans le regard. Tu es encore jeune et ton sang bouillonne comme torrent, faisant vibrer tout ton corps en grand tumulte. Moi je suis un vieil homme fatigué et guère sage, mais je sais qu’Il ne m’a jamais abandonné. Fie-toi à Lui. »

Il les salua et retourna dans la pièce, câlinant le matou qui s’abandonnait avec délices aux caresses. Tandis qu’ils descendaient l’escalier, Ernaut demanda à Iohannes : « Tu crois qu’il sait des choses que nous ignorons ?

— C’est certain, mais rien qui a trait à notre affaire.

— Pourquoi m’avoir dit cela alors ? »

Iohannes leva le nez, fronça les sourcils en regardant le ciel.

« Je ne suis pas sûr… »

Il tapota l’épaule du géant, tentant de le réconforter d’une voix chaleureuse.

« Allons donc voir l’ami Guillaume. Peut-être saura-t-il mettre un nom sur le larron qui aura pisté Ogier depuis Saint-Gilles pour le détrousser. »

D’un signe de tête, Ernaut accepta. Il leva les yeux à son tour, intrigué. Il ne vit rien de spécial, seulement l’azur et une lune discrète. Aucune réponse ne viendrait de là pour calmer l’angoisse de son coeur.

### Casal de la Mahomerie, vendredi 13 juin, soirée

Le cheval d’Ernaut était énervé et fatigué de sa journée, aussi le jeune sergent le mena-t-il dans le jardin de son frère pour le panser un petit moment. Ils avaient convenu avec Iohannes de se retrouver chez Guillaume. Ernaut avait grande envie se rendre à l’église, mais il craignait de déranger son frère dans ses oraisons. Il lui fallait se présenter au jugement le lendemain le cœur prêt, car Dieu saurait accorder sa faveur uniquement à celui qui serait porteur de la vérité.

Il s’en voulait de n’avoir pas plus d’éléments pour empêcher la bataille. Il se comporta un peu brusquement avec la pauvre bête qui se montra rétive en réponse. Il finit par pester et abandonna l’animal à son sort, un seau d’eau devant lui et une brassée de foin sec à portée.

Traversant la maison, Ernaut monta prendre de quoi grignoter rapidement, afin d’attendre le soir. Il découvrit une miche de pain enveloppée sur la table, qu’il crut apportée là par Osanne. Cela lui arracha un sourire attendri. Il n’était pas seul à s’inquiéter et voyait avec reconnaissance que la jeune femme, bien que discrète, tenait à se montrer à leurs côtés en cette affaire. Cela lui rappela qu’il y avait une autre personne qu’il ne voyait pas tant qu’il l’aurait voulu. Il s’assit, se coupa une généreuse tranche qu’il avala sans accompagnement.

Il pensait soudain à Sanson de Brie, celui qui deviendrait son beau-père un jour prochain. Aurait-il le courage de faire ce que Lambert subissait pour Godefroy ? Ce n’était pas de se prêter à une bataille qui l’angoissait, car il était persuadé que nul ne tiendrait face à lui en pareille circonstance. Sans compter la force d’âme qu’il faudrait pour oser lancer pareil défi à un homme tel que lui. Non, ce qui torturait son âme, c’était l’engagement complet de Lambert, qui avait mis sa vie dans la confiance qu’il avait en cet homme. Il était totalement dévoué, sûr de ses intentions et sans crainte. Ernaut ne pouvait en dire autant, lui qui ne voyait Libourc que certains dimanches, pendant la messe, partageant avec elle le reste de la journée après un repas en présence de ses parents. Il appréciait fort le vieux Sanson, qui le lui rendait bien, mais il s’apercevait qu’il faisait sa vie à côté d’eux.

Ils avaient convenu qu’il lui fallait acquérir un peu de bien avant de pouvoir prétendre à fonder une famille. Mais cela l’avait éloigné d’eux, qui résidaient désormais dans un casal en dehors de la cité. Il prenait goût à sa vie d’aventurier, parfois en mission plusieurs jours, de garde de longues heures avec ses compères. Les soirées entre hommes étaient festives et bien arrosées. S’il avait pas mal d’amis avec qui partager de bons moments, il n’en connaissait guère les familles, tenues à l’écart, faisant partie d’une autre vie. Il commençait à se demander s’il ne trahissait pas Libourc à agir comme il le faisait.

Son frère venait de lui démontrer que les sentiments n’étaient pas que protestations entre deux baisers, mais à la source d’un engagement plus profond. Éloignant ces pensées d’un soupir, il se leva pesamment et ressortit. Il tenait son fourreau et sa masse à la main, fixant le baudrier tout en marchant. Il aperçut de loin Guillaume et Iohannes, assis autour d’un jeu de marelle. Des enfants accroupis à côté d’eux jouaient aux osselets et avec des dinettes, tandis qu’une femme, certainement l’épouse de Guillaume, brodait un petit morceau de toile.

Le couple n’était pas très âgé, à peine vingt-cinq ans pour lui, et guère moins pour elle. Ils avaient les mains usées des travailleurs, mais leurs tenues témoignaient de leur relative prospérité. La vie pouvait se faire douce pour les gens du royaume. L’homme se leva à son approche, tendant la main.

« La bien venue à toi, Ernaut. Prends place avec nous. »

Ernaut sourit de cet accueil chaleureux, qui signifiait que chez eux aussi on avait pris parti, et pas pour Aymeric. Il sourit à la cantonade, saluant à distance la femme, qui semblait plus réservée.

Invité à s’asseoir, il s’installa sur un tabouret. Les deux garçonnets proches regardaient son épée avec envie, ce qui lui arracha un sourire bienveillant. De forte carrure, Guillaume n’était pas grand, mais se tenait droitement. Fier, il dévisageait sans gêne, plongeant son regard clair dans les yeux de ses interlocuteurs. La tignasse brune, emmêlée, accueillait souvent ses doigts, qui y répandaient le chaos. On aurait dit qu’il ne savait pas s’exprimer sans faire errer ses mains à un endroit ou l’autre de son visage, tirant une oreille, grattant une joue mal rasée ou torturant son nez à chaque fois qu’il ouvrait la bouche.

« Iohannes m’a dit que tu es le frère de Lambert. C’est bien de l’aider. Nous sommes heureux de le savoir au casal. »

En une phrase simple, il avait résumé toute l’affaire pour lui, et le parti qu’il tenait. Le regard inquiet de sa femme indiquait bien qu’elle aurait préféré moins d’ostentation. Guillaume était ainsi, d’un seul bloc, aussi subtil qu’un sanglier et cachottier qu’un taureau. Il plut immédiatement à Ernaut.

« Grâces pour ton accueil, Guillaume. Dommage de se voir en si triste occasion.

— Je te comprends ! Si je peux t’aider à trouver la piste qui mène au gredin qui a fait ça, cela m’enjoiera. J’ai grande amitié pour Godefroy, mais ne suis guère enclin à en vouloir la mort d’Aymeric.

— Je n’ai pas ce désir non plus, en effet. »

Guillaume lui offrit un sourire franc.

« Iohannes m’a dit que tu souhaiterais en apprendre moult sur Ogier. Demande et je te dirai. »

Ernaut, décontenancé par cette approche, balbutia un peu avant de réfléchir un instant et de poser une question.

« Avait-il quelque ennemi en son ancien casal ?

— Pas que je sache, non. On peut pas dire qu’on l’aimait, mais de là à lui ouvrir les boyels !

— Pas aimé, pourquoi ça ? On l’enviait ? »

Guillaume s’esclaffa.

« Non, de certes. Je ne le savais pas si riche avant qu’il ne vienne prendre la vigne ici. Il n’avait que peu de terres à Saint-Gilles, toujours à courir les vals et les monts.

— Il voyageait beaucoup ?

— Il avait des bêtes pour porter marchandies, car son bien n’aurait pu le nourrir. Un valet s’en occupait la plupart du temps. »

Ernaut dessina du bout du pied dans la poussière tout en parlant.

« Il transportait quoi en général ?

— Aucune idée ! Il était fort secret là-dessus. D’aucuns disaient qu’il préférait causer aux païens qu’à nous autres.

— Il fréquentait les mahométans ?

— La rumeur le voulait. Il parlait volontiers avec les voyageurs syriens de passage. Peut-être commerçait-il, je ne sais.

— Des bêtes ? risqua Iohannes, sans conviction.

— Il n’avait pas d’oailles qui auraient pu intéresser les nomades. Il faisait ses affaires par-delà le royaume même. Je n’ai jamais vraiment su et pas cherché à savoir. C’était pas le genre d’homme avec qui on aime à vider godet. D’ailleurs, le père m’a dit qu’ils ont soufflé de le voir partir. »

Ernaut fronça le sourcil, intrigué.

« Ils avaient des choses à lui reprocher ?

— Non, rien de précis. Mais c’était pas le genre qu’on aime avoir comme voisin, même si on le voyait guère. »

Il lança un œil à sa femme, qui le couvait d’un regard désapprobateur.

« Je sais qu’y faut pas médire des morts, mais j’avoue que j’ai pas sauté de joie à le voir arriver au casal. J’ai fait comme le père, mes affaires, et puis c’est marre.

— Il avait beaux projets pour ici, pourtant.

— Beaux projets, certes, mais pour ici, vraiment ? »

Il se passa la langue sur les lèvres, hésitant, cherchant ses mots.

« Je dirais plutôt qu’il avait beaux projets pour lui, si tu vois ce que je veux dire. »

Guillaume sourit, heureux de sa formule. Puis il attrapa un de gamins qui se tenait à ses côtés, lui caressant le dos en un geste doux. L’enfant, encore mal assuré sur ses jambes, avait la morve qui lui coulait du nez et les mains grises de poussière.

Guillaume le moucha de ses doigts.

« Tout ça avait un fond de vérité vu qu’on l’a meurtri de bien horrible façon. Il a brassé lui-même sa boisson.

— Mon souci, c’est que si je ne peux trouver quelque nom à donner à Aymeric, il me sera difficile de le convaincre.

— Je comprends, mais je n’en ai aucun à te donner. Le père, qui demeure toujours à Saint-Gilles, pourrait peut-être t’éclairer.

— Je n’ai pas le temps de m’y rendre, le jugement par bataille est pour demain. »

Guillaume fronça le nez, les yeux au loin.

« Je ne pense pas qu’il puisse se faire…

— Et pourquoi donc ? Le sire vicomte en a fait annonce. »

Iohannes intervint, de sa voix posée, regardant par-dessus les épaules de Ernaut.

« Le vicomte n’y pourra rien, Ernaut. Je crois que Dieu est avec nous, et nous donne du temps. »

Déconcerté, Ernaut se leva, et tourna la tête. Une bande rousse au loin bouillonnait au bas du ciel, depuis le levant.

« Qu’est-ce que…

— Le khamaseen, déclara Iohannes.

— Quoi donc ?

— Un vent de poussière venu des terres désertiques. Il sera sur nous d’ici peu. »

Ernaut regarda les deux hommes, sans bien comprendre. Iohannes détacha enfin ses yeux de l’horizon.

« Il sera impossible de faire la bataille demain, Ernaut. Le khamaseen dure plusieurs jours en général.

— Il suffoque les bêtes, les hommes, brûle les plantes… ajouta Guillaume, le visage tendu.

— C’est de cela que…

— Que Pisan a voulu parler, certainement, oui. Ceux qui vivent ici depuis assez longtemps ont senti que ça risquait d’arriver. »

Ernaut hochait la tête, oscillant entre l’espoir qui naissait en lui et la crainte de ce qu’était vraiment cette tempête. La femme de Guillaume faisait ranger leurs affaires aux enfants et s’apprêtait à rentrer. Elle les salua d’une voix fluette. Les trois hommes se retrouvèrent seuls devant la maison. Guillaume scrutait le ciel, les yeux plissés.

« Si la tempête est trop longue, ça va faire grand mal aux ceps, c’est pas bon pour nous… J’ai déjà vu des oliviers sécher sur place à cause de ce vent. Mon vieux disait que c’était le souffle du diable. »

Il n’arracha pas ses yeux du Levant lorsqu’Ernaut le dévisagea. Ce dernier tourna alors la tête vers Iohannes. Il en obtint un rictus inquiet. Apparemment lui non plus n’était pas sûr de ce qu’il fallait penser. Dieu leur accordait-il un répit ou venait-il manifester sa désapprobation ? Ernaut soupira et fit craquer les jointures de ses doigts, se renfrognant. Il avait promis à son frère de tout faire. Il ne manquerait pas à son serment. Demain, tempête ou pas, il prendrait la route de Saint-Gilles et en ramènerait le mécréant ou le justicier qui avait planté son fer dans le bas-ventre d’Ogier.
