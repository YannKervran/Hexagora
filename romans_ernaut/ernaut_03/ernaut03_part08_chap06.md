## Chapitre 6

### Casal de la Mahomerie, lundi 16 juin, après-midi

Fuyant la tempête qui enflait, Ernaut et Iohannes se rendirent à l’église, marchant d’un pas vif et s’abstenant de parler tandis que le vent faisait vrombir la poussière autour d’eux. La porte céda en grinçant, propageant un nuage ocre dans le bâtiment. Leur arrivée attira l’attention d’un groupe de voyageurs installés dans un des bas-côtés tandis que d’autres étaient occupés à leurs dévotions devant le maître autel, les bras levés vers le ciel face à l’image du Christ crucifié. La voûte résonnait de leurs psalmodies, faisant écho aux ronflements réguliers, toux, murmures et crachotements de ceux qui se reposaient.

Un des serviteurs du Saint-Sépulcre, reconnaissable à sa tonsure, était en train de collecter leurs souliers, certainement pour aller les faire réparer par un savetier. Un de ses collègues, habillé malgré la chaleur d’une tenue de bure fatiguée et tachée apportait un panier empli de pains d’où dépassaient une oule, un pichet et des écuelles. Ernaut attira Iohannes à l’écart et vint s’asseoir sur un banc disposé le long du mur. Il s’assit avec lassitude, appuyant ses coudes sur ses cuisses, le regard fatigué, haletant comme un homme épuisé.

Iohannes resta debout face à lui, les mains sur les hanches, étudiant les va-et-vient des gens dans la nef. À un moment, il émit un grognement qui attira l’attention d’Ernaut. D’un regard, il indiqua un homme qui s’approchait. Plutôt maigre, de taille moyenne, celui-ci avançait d’une démarche dansante droit sur eux. Le vent avait malmené sa coiffure, cheveux raides qui lui tombaient sur les oreilles. Les joues et le nez couperosés tranchaient avec le ton clair de sa barbe et de sa moustache. En arrivant, il lâcha sa ceinture pour lever la main en un signe amical. Ernaut perçut immédiatement son embarras et se contenta de le dévisager froidement. Iohannes l’accueillit d’un signe de tête.

« Maître Robert ! Le bon jour à vous. »

Il n’obtint en réponse qu’un sourire embarrassé, accompagné d’une inclinaison du buste. Ernaut ne concéda qu’un grommellement guère amical. L’homme était ennuyé, peu à l’aise. Il se gratta la nuque avant de se décider à prendre la parole.

« Je suis enjoyé de vous encontrer ainsi. Je vous cherchais !

— Pour quelle raison ? »

La question d’Ernaut le fit sursauter comme s’il venait de découvrir un serpent à ses côtés. Ses doigts s’agitèrent un instant et il lui fallut reprendre ses esprits avant de continuer.

« Je suis là pour empêcher les choses d’aller de mal en pis.

— Vous parlez de quoi, maître ?

— Je parle de cet assaut que vous avez subi… » glissa-t-il d’une voix tremblante.

Ernaut se leva en entendant cela, dominant soudain son interlocuteur d’une bonne tête. Il l’écrasait de sa masse, de sa taille, de sa colère que ses yeux froncés laissaient jaillir sans retenue. Si le sol le lui avait permis, Robert s’y serait volontiers enfoncé. Il rentra la tête dans les épaules, prêt à recevoir un coup. Sa voix timide se fit néanmoins entendre.

« Je suis là en messager de paix, maîtres. C’était folie et tous en ont convenu.

— Vous savez donc qui a fait cela ?

— Ce ne sont qu’enfançons, têtes creuses sans cervelle. Quand leurs pères l’ont appris, ils ont ajouté force baffes aux coups que vous leur avez bien justement distribués. »

Ernaut s’esclaffa avec rage, tel un fauve prêt à mordre. Mais il redressa le buste et s’apaisa un peu.

« Votre fils en était ?

— Certes non ! répliqua avec vigueur Robert. Je n’aurais jamais laissé faire pareille chose. Il y a bataille prévue pour rendre justice, cela me convient fort.

— Pourquoi donc ces imbéciles nous ont courus sus ? » demanda Iohannes, d’une voix posée.

Robert dansa d’un pied sur l’autre, renâclant à l’idée de s’expliquer. Il lui fallait pourtant boire le vin jusqu’à la lie.

« D’aucuns n’aiment guère voir qu’on s’intéresse à leurs affaires. Les esprits se sont emportés et les jeunes ont compris de male façon les intentions. »

Il se tut, parcourant du regard les ecchymoses sur les visages des deux hommes.

« On m’a donc demandé de venir vous proposer dédommagement.

— Savez-vous que je pourrais demander à ce qu’on leur coupe le poing pour ce qu’ils ont fait ? indiqua Ernaut, les lèvres pincées.

— Certes, et ce serait bon droit. Je viens à vous pour éviter cela. Outre, vous leur avez infligé cuisante leçon.

— J’aurais pu tout aussi bien les méhaigner encore plus fort ou les occire ! C’était mon droit » ajouta le jeune homme, vindicatif.

Robert hocha la tête tristement, douloureusement.

« Je le sais fort bien, maître, et cela leur a été rappelé. C’était grande stupidité, coupable bêtise. Il est donc juste que vous en soyez dédommagé, que chacun en soit tenu quitte.

— Que nous propose-t-on ?

— À chacun de vous nous pouvons offrir trois vingt et quinze besants pour être tenus quittes. »

Iohannes écarquilla les yeux, sifflant entre ses dents.

« Quinze livres !

— Le montant nous paraît juste, étant donné la sauvagerie de l’assaut, les coups apparents. »

Ernaut fit claquer sa langue plusieurs fois, dodelinant de la tête de façon circonspecte.

« Il est une chose que j’ai grand-peine à comprendre, maître.

— Qu’est-ce donc ?

— Je ne pense pas que l’un de nous puisse connaître male mort suite à cet assaut, et nul ne peut prétendre à plus de cinq livres en tel cas.

— Il y aurait aussi à verser à la cour du vicomte vingt livres, en l’affaire, maître.

— Et donc ? Cela ne fait que vingt et cinq au total.

— Vous l’avez dit, le sang a coulé, et vous pourriez demander à faire trancher le poing. Cela vaut bien. »

Ernaut ne semblait pas convaincu, cherchant à percer les intentions de son interlocuteur. Ce dernier baissa la tête et préféra attendre la réaction de Iohannes qui conservait, comme à son habitude, un visage indéchiffrable.

« Quinze livres pour éviter à coquins de se faire trancher au poignet, pourquoi pas ? J’ai pourtant grande impression qu’on espère acheter autre chose. »

Robert prit un air offusqué, mais avec trop peu de talent pour être crédible.

« Que pourrions-nous espérer de vous ?

— Je ne le sais de certeté. Pourtant, je gage fort avant que certains seraient aises que j’arrête là ma traque, n’est-ce pas ?

— La bataille va bientôt se dérouler et tout sera fini, répondit Robert en haussant les épaules.

— Vraiment ? Je ne le crois guère. Il demeure force mystères. Je renifle odeur de mécréance, je la piste jusqu’à Salomé… »

En entendant ce nom, Robert eut un tressaillement et son regard s’agrandit, inquiet. Ernaut se rapprocha de lui et le frappa au torse d’un index vigoureux.

« Il se trame males fourberies en ces lieux et j’ai fort envie d’y jeter le trouble. J’ai prêté serment au roi et m’est devoir de traquer félons et larrons. Ce n’est pas une poignée de monnaies qui me fera reculer. »

Robert bafouilla, bousculé par les coups impérieux d’Ernaut. Sa détresse devenait panique et ses yeux roulaient, espérant un soutien de Iohannes, qu’il sentait moins agressif. Nulle aide ne lui vint néanmoins et il échappa dans un cri : « Nous n’avons rien fait de si grave ! »

Ernaut en arbora un sourire de victoire carnassier, cherchant à plonger son regard dans celui, fuyant, du petit homme devant lui.

« Vous avez fait quoi, de juste ?

— Nous avons à peine poussé quelques bornes, ahana Robert.

— Bougé des bornes ? se moqua Ernaut, presque déçu. Il va falloir tout m’en dire !

— Certaines terres soumises à cens voisinent celles du seigneur de Retest. En déplaçant les limites, nous pouvons semer plus en payant mêmes taxes.

— Je ne suis pas certain que le sire de Retest ait plaisir à se faire ainsi voler, pas plus que le Saint-Sépulcre.

— Tout le monde s’efforce de rendre profitable son bien ! »

Ernaut s’amusa de la remarque.

« Comment avez-vous procédé ?

— Il fallait préciser les parcelles et poser des croix de bornage neuves. Le sire Robert avait mandé un vieux païen, qui a toujours vécu là, et connaît donc les frontières des villages. Il a menti aux fins de nous laisser plus que nous n’avions droit en théorie.

— Pourquoi cela ? Il serait de vos amis ?

— Certes non, répondit l’autre, horrifié. Je ne sais comment, mais c’est Ogier qui l’a décidé.

— La vieille crapule, sourit Ernaut. Voilà donc la façon dont il comptait faire fortune !

— Il l’a fait pour un certain nombre d’entre nous, pas que pour lui. Il était bon compère.

— Aymeric en a profité ? »

Robert hocha la tête en assentiment. Les choses devenaient claires à présent. Il ne demeurait qu’une ombre dans le tableau qui s’esquissait.

« Pourquoi donc accuser Godefroy ? Vous le pensez capable d’avoir tué Ogier pour cela ?

— Je ne saurais dire, en toute loyauté. Il est un des rares à avoir fait remontrances à Ogier. Il n’a pas voulu profiter d’un bornage avantageux.

— Il avait menacé de vous dénoncer ?

— Je ne l’ai pas entendu dire. Mais c’est certainement ce qu’encroit Aymeric. La menace aura dégénéré… »

Il ne finit pas sa phrase, le sous-entendu étant suffisamment précis. Ernaut se mordit la lèvre. S’il avait découvert un nid de conspirateurs, cela offrait également à Godefroy un mobile pour ce meurtre. Il rumina un instant sa frustration, se demandant si Lambert était au courant. Avec sa droiture, il aurait été plutôt du genre de Godefroy, à refuser. Robert attendait, tel un animal qui ne savait s’il serait abattu à l’instant ou relâché.

Ernaut échangea un regard avec Iohannes. Celui-ci haussa imperceptiblement les yeux au ciel. Le jeune homme posa donc une main ferme sur l’épaule de Robert, appuyant légèrement afin de lui en faire sentir toute la force.

« Je vous ai entendu, maître Robert. Vous avez fort bien fait de venir en émissaire.

— Vous ne soufflerez mot ?

— Je n’ai pas encore pris décision. J’ai prêté serment au roi, pas au Saint-Sépulcre ni au sire de Retest. Je dois pourpenser tout cela. »

Robert acquiesça avec déception. Il semblait encore plus misérable qu’à son arrivée.

« Vous saurez bien assez tôt ce qui sera décidé, vous et les vôtres, soyez assuré.

— Je vous mercie de m’avoir écouté. »

Il salua d’une inclinaison rapide du buste et commença à s’éloigner, la mine piteuse, le pas traînant. Ernaut l’interpella avant qu’il ne soit trop loin.

« Pour cette vilaine affaire de coups apparents, j’accepte les quinze livres, et j’ose croire qu’elles seront bienvenues en la bourse de mon compère Iohannes. De cela vous pouvez vous tenir quittes. »

Robert leva une main en accord, avant de s’éloigner vers la porte, légèrement soulagé. Ernaut sourit à Iohannes, amusé : « Si ces imbéciles m’offrent de quoi m’acheter cheval et haubergeon de mailles, je ne vois nulle raison de refuser.

— Même à plusieurs, ils se ruinent.

— Et après ? Nul ne les a forcés à nous assaillir.

— Ils ne voulaient pas nous occire, juste nous inquiéter.

— Je n’en ai cure. Ils ont agi sottement et veulent payer. Soit ! Je prends leurs monnaies et j’oublie. »

Iohannes inclina la tête, gagné par l’argument.

« Il ne faudrait pas pour autant qu’ils croient qu’ils peuvent acheter ma loyauté de quelques livres.

— Tu vas les dénoncer ? »

Ernaut se racla la gorge, indécis.

« Je ne sais encore. La seule chose certaine, c’est que ma décision ne sera pas influencée par leur offre. »

En entendant cela, Iohannes eut un sourire franc et il serra le bras de son compagnon, hochant la tête gravement. Ernaut ajouta :

« Il est par contre une personne qu’il me faut voir pour discuter de tout cela. »

En disant ces quelques mots, il avait le visage agacé et ses mains se contractèrent sans qu’il n’y prenne garde. En un instant, la colère qui couvait jusque-là s’était réveillée.

### Casal de la Mahomerie, lundi 16 juin, début de soirée

La demeure de Godefroy n’était guère éloignée de l’église et Ernaut s’y rendit directement, suivi de près par Iohannes. Le vent s’était calmé et on voyait les silhouettes des bâtiments de façon plus distincte. Tout en avançant, l’interprète levait la tête, sentait l’air, observait les alentours. Il gardait néanmoins prudemment le silence, ayant réalisé à quel point son compagnon était empli de fureur. Le visage fermé qu’il contemplait ne lui inspirait rien de bon et il préféra demeurer en retrait.

Arrivés devant la maison, Ernaut frappa plusieurs coups vigoureux sur la porte en appelant le maître des lieux, se faisant connaître d’une voix impérieuse. Rapidement, on leur ouvrit et ils se retrouvèrent face à Godefroy, une lampe à la main. Son visage était fatigué et il semblait inquiet. Mal rasé, à peine coiffé, on l’eût volontiers pris pour un fou. Ernaut n’eut aucun égard malgré cet aspect désolé et se planta face à lui, les mains sur les hanches, affichant son intention d’en découdre d’un regard menaçant. Il ne le salua même pas.

« Vous voulez donc la mort de mon frère, maître Godefroy ? »

La stupéfaction remplaça instantanément l’abattement. Ses yeux s’agrandirent d’horreur et il ouvrit la bouche comme pour répondre. Face à lui, aussi raide que saint Michel au Jugement Dernier, Ernaut le défiait, les yeux enflammés par la lueur de la mèche. Godefroy souffla et ferma les paupières lentement, comprenant soudain. Il se tourna vers l’escalier et leur fit signe de le suivre. Sa démarche était celle d’un homme brisé.

En haut, à leur arrivée, Perrote et Osanne se levèrent. Leurs regards trahissaient l’angoisse qui les habitait. Elles ne comprenaient pas l’attitude ni l’aspect des hommes qui arrivaient. Inquiètes, elles reprirent distraitement leur ouvrage d’aiguille, assises sur une banquette après avoir rapidement salué. Elles observaient en silence, tourmentées par avance de ce que cela pouvait trahir des événements futurs.

Godefroy s’assit pesamment sur un banc, à table, et invita d’un signe de tête Ernaut et Iohannes à faire de même face à lui. Il leur versa du vin, en avala une gorgée. Iohannes l’imita, mais Ernaut ne daigna pas jeter un regard vers son gobelet. Il continuait à observer le borgne, tel un prédateur indécis. Il attendait la curée. Dehors le silence s’était installé, nul vent ne faisait plus bruisser les branches, claquer les volets, aboyer les chiens. Godefroy toussa et renifla avant de finalement oser une question.

« La traque du murdrier vous a mené sur la piste des secrets d’Ogier, alors ?

— Les siens et ceux d’autres, en effet, confirma Ernaut sèchement.

— Vous apensez qu’ils ont quelque rapport avec cette affaire ?

— Certes oui. Quand bien même, je ne comprends pas que vous ayez gardé par-devers vous pareille chose ! »

Il frappa du poing sur la table, faisant tinter les céramiques et sursauter les présents. Sa voix monta d’un cran.

« Cul-Dieu ! N’avez-vous pas une once de cervelle ? J’ai perdu une semaine à pister en un chemin connu de vous ! »

Godefroy se ramassa sur lui-même, honteux et contrit. Il secouait la tête comme pour se convaincre qu’Ernaut se trompait.

« Mon frère Lambert va mener bataille en votre nom et vous mentez, je ne sais pourquoi… Quel genre d’homme êtes-vous ? Avez-vous une pierre en la poitrine et du vent entre les oreilles ?

— Je suis innocent de ce meurtre, Dieu le montrera, répliqua doucement le vieil homme.

— À Dieu plaise ! Il n’a pourtant guère d’amour pour menteurs et tricheurs ! »

Ernaut se leva, pointant un index vers Godefroy avant d’en frapper la table à plusieurs reprises.

« Si Lambert ne remporte pas le jugement, vous serez serré par le col à une fourche patibulaire, beau sire menteur ! Jusqu’à ce que la langue bleue jaillisse outre vos lèvres, votre visage bouffi, devenu aussi rouge que gratte-cul ! »

Il se laissa retomber sur le banc, haussant les épaules et levant les bras, dépité. Il secouait la tête, comme s’il cherchait à sortir d’un mauvais rêve. Préférant ne plus regarder celui pour qui Lambert risquait sa vie, il tourna la tête vers la pièce et découvrit Osanne, qui le défiait d’un air mauvais.

Elle avait arrêté son ouvrage et le dévisageait, les joues en feu. Il allait l’invectiver elle aussi, mais il n’en eut pas le temps. Elle se leva, posant son aiguille et le vêtement qu’elle reprisait, venant près d’eux sans hésiter un seul instant.

« Qui êtes-vous donc, *messire* Ernaut, pour venir en nostre hostel crier à hue et à dia ? Vous hurlez comme seigneur en son fief ! Nous ne sommes pas vos liges et n’avons nul compte à vous rendre ! »

La surprise enflamma Ernaut qui se mit à crier encore plus fort.

« Et à mon frère donc ? Peu vous chaut qu’il risque le trépas, on dirait ! »

Osanne chancela sous l’accusation, stupéfaite de se l’entendre dire. Elle en bafouilla un instant, mais se reprit bien vite.

« Père est innocent, et Dieu guidera le bras de Lambert. Ce n’est pas de satisfaire votre curiosité maladive à tout propos qui aidera quiconque !

— Un jugement par bataille finit toujours par la mort de l’un. Il m’en croit plus utile de traquer le murdrier et le punir.

— Êtes-vous saint Michel archange descendu du Ciel pour vous croire colère de Dieu ? Laissez-nous régler nos histoires, et accordez un peu de crédit à votre frère ! »

Ernaut souffla, à court d’arguments. La colère empourprait son visage, les veines saillaient dans son cou et, désormais debout, il surplombait tel un ogre la maigre silhouette d’Osanne. Pourtant celle-ci refusait de plier, aussi emplie de fureur que lui-même. Il sentait des picotements qui jaillissaient de ses reins pour se propager jusqu’à sa nuque. C’eut été un homme face à lui, il aurait peut-être laissé ses poings parler.

Mais c’était une femme, qui l’affrontait pour l’amour des siens, en raison des sentiments qui l’unissaient à Lambert. Son frère. Cette pensée le calma. Il se passa la langue sur les lèvres et desserra doucement les poings, les bras encore crispés. Il prit plusieurs inspirations lentes et vit que la jeune femme se calmait elle aussi.

Il reprit sa place sur le banc, à côté de Iohannes, et empoigna avec violence le gobelet de vin, qu’il vida d’un trait. Puis il resta à examiner le fond du récipient, l’air buté. Osanne s’assit à côté de son père en silence, les mains sur les cuisses, le visage fermé. Au loin, un âne se mit à braire, bientôt suivi par quelques autres. Ce fut Iohannes qui osa briser le silence gênant, d’une voix posée.

« Maître Godefroy, pourquoi êtes-vous demeuré muet de pareilles choses ? Si nous cheminons en mauvaise voie et que vous savez pourquoi, il faut nous le dire. Nous n’avons plus guère de temps. Je suis d’avis comme Ernaut qu’il serait mieux pour le casal que nulle bataille n’ait lieu. Perdre Lambert ou Aymeric n’apporterait rien de bon…

— J’aurais grande surprise que quiconque en veuille à Ogier de l’avoir aidé à s’enrichir. Il s’était fait bons amis avec sa tricherie sur les bornes.

— Vous étiez pourtant en grand désaccord ?

— Si fait. J’étais le seul. Or, comme je me sais innocent de ce crime, je ne vois nul autre qui aurait pu le frapper en rapport avec tout cela.

— N’avait-il pas lésé quelques-uns à agir ainsi ? »

Le borgne haussa les épaules.

« Certes ! Voilà source de mon reproche. Voler le Saint-Sépulcre, c’est rober Dieu et les pauvres, que je lui avais dit. Il en avait ri.

— Il est une autre personne qui s’est fait soustraire de la terre et qui serait plus à même de se faire justice par les armes…

— Vous pensez à sire Robert de Retest ? »

Il pouffa, amer.

« Je suis bien d’accord. Seulement, il n’est pas homme à venir occire nuitamment. Plus volontiers, il serait venu avec soudards pour menacer de pendaison ou couper le poing, tel à un voleur. Sire Robert a de nombreux défauts peut-être, mais je ne saurais le traiter de sournois.

— Le préjudice est-il si grand pour lui ?

— Certains ont doublé leurs terres en prenant sur les siennes. Payant le même loyer au Saint-Sépulcre, cela ne leur coûte rien.

— Comment donc Ogier a-t-il pu convaincre le sire de Salomé et le Saint-Sépulcre que si grandes terres avaient faibles redevances ? »

Godefroy resservit du vin et en prit une gorgée, mal à l’aise. Il se frotta le nez à plusieurs reprises. Son regard hésita un instant vers Ernaut, dont l’attitude passive l’inquiétait tout autant que la colère manifestée quelques instants plus tôt.

« Les loyers étaient connus par la coutume et les terres décrites sans que nulle borne ne soit visible. Il a fallu faire appel à un vieux païen qui se souvenait des anciennes limites, avant même que des colons viennent en ces lieux.

— Ogier l’a convaincu de mentir ? Comment a-t-il fait ?

— Je ne saurais dire. Il est connu qu’il avait des liens avec eux. En tout cas, le vieux boiteux a dit ce qu’il attendait. »

Iohannes se redressa sur son siège, surpris.

« Boiteux ? Vous parlez de Pied-Tort ? Le vieux shaykh ?

— Je ne sais. Un vieil homme avec une longue barbe sans moustache et un grand nez. »

Ernaut hocha la tête en réponse à la question muette de Iohannes, désormais tourné vers lui. Godefroy se râcla la gorge.

« J’ai gardé silence, car je ne voyais comment toute cette histoire pouvait avoir lien avec la murdrerie d’Ogier. Il vivait comme larron et menteur et c’est là signe qui ne trompe pas Dieu. De tels hommes ne s’arrêtent jamais de tricher. Ce n’était certes pas son premier méfait et on aura voulu lui demander justice pour l’un d’eux. »

Ernaut hésita un instant, puis demanda d’une voix enrouée :

« Avez-vous dit tout cela à mon frère ?

— Non. Ses terres sont de l’autre côté, et je ne l’ai su que parce qu’on m’a proposé de profiter du faux bornage. J’ai refusé, et de là tout est parti. »

Il pinça les lèvres, sourit à sa fille avec tendresse.

« Il s’est porté garant uniquement parce qu’il sait au fond de lui que je ne suis pas le murdrier. Il a confiance en la justice divine.

— S’il faut qu’un homme ou l’autre en ce casal se fasse tuer à cause de la meurtrerie d’un coquin qui avait mérité son sort, vous pouvez me dire où elle est la justice divine ? »

Le borgne écarquilla un œil étonné et Osanne foudroya Ernaut du regard, choquée par le blasphème. Ernaut secouait la tête en désaccord, assénant les mots tout en frappant de ses doigts pliés sur la table.

« Dieu n’aime pas le mensonge et la fausseté, de cela je suis d’accord. Certains propagent les ténèbres par tromperie, cupidité ou méchanceté. J’encrois que ceux qui les laissent avancer par lâcheté ou peur de la vérité nue ne sont guère plus aimés. Je vais faire lumière sur toute cette histoire, et gare à ceux qui espéraient lui échapper, ramassés en leur coin d’ombre.

— Êtes-vous si vaillant en votre cœur que vous ne craignez pas le regard de Dieu, maître Ernaut ? » le défia Osanne.

Agacé, il allait répondre vivement, mais il marqua un temps, prit une longue inspiration et la toisa avec lassitude.

« Je m’emploie à vivre ainsi qu’il m’a fait. Ai-je jamais dit que la vérité m’était moins cruelle ? »

### Casal de la Mahomerie, lundi 16 juin, soirée

Lorsqu’ils ressortirent de chez Godefroy, Ernaut lança un regard inquisiteur à Iohannes. Celui-ci comprit immédiatement où son compagnon voulait en venir et leva les deux mains, protestant de son innocence.

« Je n’avais nul entendement de cela, Ernaut ! Ils me savent au service du Saint-Sépulcre, ils ne m’auraient jamais confié pareil secret. »

Ernaut continuait à le fixer sans un mot, fronçant les sourcils, cherchant à jauger la sincérité des affirmations. Il avait pris l’habitude de cela alors qu’il interrogeait les marchands arrivant dans la cité.

« Je n’encrois pas qu’il y ait tant de gens partageant ce secret. Sinon cela risquerait de venir aux oreilles de l’intendant et, plus dangereux encore, à celles du sire de Retest. M’est avis qu’ils ont gardé par-devers eux tout cela, avec sagesse. »

Ernaut fit claquer ses mâchoires plusieurs fois. Il faisait confiance à Iohannes depuis le début de cette histoire et ne voyait nulle raison de s’en défier maintenant. Il réfléchit donc à ce qu’il convenait de faire désormais.

« Si nous allions voir Aymeric, lui contant ce qu’on sait, peut-être pourra-t-on le faire changer d’avis.

— Crois-tu qu’il t’entendra ?

— Je pourrais toujours le menacer de tout dévoiler. »

Iohannes lança un œil noir à Ernaut qui se mit à sourire instantanément.

« Je n’ai pas dit que je le ferai, mais il pourrait l’accroire.

— Traquer la vérité en proférant mensonge et menaces ne m’enjoie guère, je ne t’aurais pas cru ainsi.

— Tout fait feu pour qui a froid, compère. Si je dois mentir ou trahir aux fins de sauver mon frère, je n’hésiterai pas. »

Iohannes se renfrogna, croisant les bras tandis qu’il levait la tête vers le ciel désormais libéré de la brume de sable. Pendant ce temps, Ernaut continuait à réfléchir à voix haute.

« Aymeric ne voudra certes pas nous voir à l’écart et ce serait grand risque que de vouloir l’entretenir dans la curie, au nez et à la barbe de l’intendant du Saint-Sépulcre. »

Il commençait à faire les cent pas dans la rue, sans prendre garde au jour finissant, l’ombre dévorant peu à peu la lumière.

« Nous devrions aller voir son compère, le petit…

— Gautier ? Pourquoi donc ? Tu veux le menacer lui et qu’il passe le message ? rétorqua Iohannes, l’air incrédule.

— Certes non, je ne ferai cela que si je n’ai pas d’autre choix. Par contre, tenter de lui faire entendre raison me semble possible. Nous avons deux sentes qui s’offrent à nous : soit cette meurtrerie est liée aux convois d’Ogier, vieille histoire issue de Saint-Gilles, soit cela a à voir avec ce vol de la terre. En aucun cas Godefroy ne saurait avoir partie liée à l’un ou l’autre. On ne tue pas sans raison, surtout de si ignoble façon. »

Le traducteur hocha la tête, apaisé. Il invita Ernaut à le suivre d’un geste du bras, se dirigeant vers le bas du village.

« En ce cas, nous ne pouvons dire avoir fait grand chemin depuis le début de nos recherches.

— Si ces imbéciles ne gardaient pas leurs petits secrets entre eux ! Je n’arrive pas à croire que Godefroy soit resté muet alors qu’il risque sa vie et celle de mon frère. »

Iohannes haussa les épaules, se tournant à demi vers Ernaut tout en marchant.

« Tu ne crois pas comme lui à la justice divine ? S’il se montre vertueux et que la vérité est avec lui, la bataille lui donnera raison. Du moins est-ce là ce qu’il doit penser.

— Ne te fais pas mon avantparlier, je ne doute pas de Dieu. »

Il inspira longuement, et grimaça tandis qu’il faisait rouler ses épaules douloureuses. Après un long moment à fixer le sol devant lui, il ajouta.

« Seulement, il existera toujours un doute en moi quant à l’innocence de Godefroy. Il n’a peut-être pas tenu la lame, mais aurait pu payer larron pour cela. Celui-ci aura voulu arracher à Ogier le secret de son magot, profitant de l’aubaine.

— Tu crois cela ?

— Non, rien ne l’indique, mais je ne peux en jurer comme l’a fait mon frère. Je vois trop de menteurs à bonne figure, de regrattiers à tête d’ange, prêts à dépouiller plus faibles qu’eux. J’ai confiance en Dieu, mais certes pas en l’homme. »

Tout en devisant, ils arrivèrent au manse de Gautier. Ils frappèrent plusieurs fois à la porte, sans succès, et entreprirent de faire le tour du bâtiment. Gautier se trouvait dans le jardin, à l’arrière, occupé à arroser des courges, un seau à la main. Des enfants l’assistaient, sarclant les plantes, nettoyant les feuilles de la poussière du khamaseen. Apercevant Ernaut et Iohannes, il se figea un instant, non sans saluer l’interprète d’un fugitif signe de tête. Puis il fit mine de reprendre son ouvrage.

S’approchant du mur de clôture, le géant leva la main et salua poliment, n’obtenant qu’un grognement en retour. Gautier confia néanmoins le récipient au plus grand des gamins et vint s’enquérir de ce que voulaient ses visiteurs. Il ne les invita pas à entrer, mais se rapprocha, gardant le muret entre eux.

« Le vent se calme, la bataille ne sera plus guère retardée, de ce jour.

— Si fait, je viens m’entre causer avec vous de celle-ci justement. »

Gautier fit encore un pas, jusqu’à s’accouder aux pierres. Il paraissait aussi inquiet qu’irrité. Derrière lui, ses enfants ne perdaient pas une miette de ce qui se passait.

« J’ai rien à y voir, moi.

— N’êtes-vous pas fort compaing de maître Aymeric ?

— Il aurait eu besoin que je jure de lui, je l’aurais fait avec grand plaisir. Mais ce n’est pas le cas et lui sera en le champ clos.

— Oui, face à mon frère… »

Le petit homme lui adressa un regard sévère.

« Il n’avait qu’à demeurer coi. Il n’est pas là depuis si long temps. Nul ne l’a poussé à se porter garant du borgne.

— Il va épouser sa fille, quand même !

— La belle affaire ! Ils ne sont pas encore l’annel au doigt ! Personne ne lui aurait reproché de se tenir en retrait. »

Ernaut s’esclaffa devant l’énormité de la déclaration. Si l’homme n’était pas stupide, il devait être particulièrement obtus. Face à un sanglier, il lui sembla inutile de s’arc-bouter.

« Osanne lui en aurait peut-être tenu grief, non ? »

L’argument fit mouche, Gautier inclina la tête, faisant une moue, répugnant à admettre l’idée.

« Peut-être… Mais un parti comme lui aurait pu trouver ailleurs bonne fiance et ne pas y perdre.

— Mon frère est de ces hommes fidèles qui se tiennent auprès des leurs.

— Je n’ai nulle intention de dire le contraire. Il agit en homme de bien, c’est grand dommage qu’il soit lié à un coquin, voilà tout. »

Il se mit en devoir d’arranger les pierres sur le sommet de son muret, comme si cela devenait une tâche urgente. Ses mains avaient besoin de s’occuper, peu désireuses de laisser son esprit vagabonder en des terrains aventureux. Ernaut laissa un instant le silence s’installer et reprit d’une voix douce.

« J’en ai tout autant à dire sur maître Aymeric. Ne pourrait-il s’être mis en mauvais chemin ? »

Gautier leva la tête, plissant les yeux avec méfiance. Il jeta un coup d’oeil en biais à Iohannes puis revint sur le géant, sans une once de crainte. On aurait dit qu’il se préparait à bondir. Ernaut tenta de faire bonne figure et expliqua ce qu’il avait en tête.

« Je sais qu’il ne faut médire des morts, mais il devait savoir que maître Ogier n’était pas innocent agnel.

— Qui se prétend tel ?

— Voyons, maître Gautier, Ogier n’était pas tout à fait honnête. Il avait plusieurs méfaits qui lui entachaient l’âme. »

Le petit homme se tassa, le visage crispé.

« Qu’avez-vous en tête ?

— Déjà, toutes ces histoires à Saint-Gilles. Il allait et venait, faisant affaire avec des soldats et entretenant des liens avec les mahométans des alentours.

— C’était son affaire, répliqua l’autre, surpris.

— Vous ne saviez pas ? Maître Aymeric le savait-il, lui ?

— Je ne saurais dire. Par contre, je suis acertainé que Ogier était un des nôtres. Il a donné souventes fois gage de la confiance accordée à lui. »

Ernaut se mordit la lèvre, il hésitait à aller plus loin, son interlocuteur oscillant entre l’hostilité et la méfiance. Il n’avait néanmoins guère le choix, il lui fallait absolument trouver une façon de convaincre l’accusateur de lever ses affirmations. C’était le plus urgent, le plus important.

« J’ai ouï dire de cela, en effet, lâcha-t-il dans un soupir, ce qui lui attira un regard courroucé. Il a rendu fort amiable service à certains ici. Fort amiable, mais aussi bien dangereux !

— De quoi donc ?

— Je vous parle des bornes, maître Gautier, de ces terres que certains cultivent sans y avoir droit. Grâce à lui. »

La colère fit bafouiller le petit homme et il éructa, incapable de parler pendant un court moment. Il cherchait autour de lui des personnes pour les prendre à témoin, sans s’arrêter sur Iohannes, qu’il semblait tenir pour négligeable. Il prit et reposa plusieurs fois la même pierre et Ernaut crut un instant qu’il allait s’en servir comme arme. Il se raidit donc à son tour, prêt à faire face à l’assaut éventuel. Les douleurs du matin s’estompèrent, une chaleur l’envahit depuis les reins, tandis que son corps se préparait à un affrontement.

Rien ne vint. Gautier fulminait, mais son embarras était réel. Ernaut en arrivait à plaindre le pauvre homme, clairement dépassé par la situation. Il leva les mains, en signe d’apaisement.

« Je ne veux pas fouiller en vos affaires, maître, il fallait juste que cela soit dit !

— C’est c’te maudit borgne qu’a tout craché, hein ?

— Non, je sors à l’instant de chez lui pour lui en faire reproche justement. Certains des gamins d’ici se seraient tenus cois, je n’en saurais peut-être rien. »

Entendant cela, Gautier piqua du nez. Les enfants derrière lui étaient trop jeunes pour avoir pris part à l’attaque, mais il était fort possible que leur père en ait été au courant. Ernaut s’appuya sur le muret, se penchant en avant.

« Pourquoi donc maître Aymeric jure-t-il pour Ogier ? Nous assavons l’un et l’autre qu’il n’était pas plus droit que cep de vigne !

— Vous parliez de votre frère, prêt à se tenir fidèle aux siens. Aymeric le vaut bien !

— Comment ça ?

— Il espérait marier sa fille à Ogier. À eux deux, ils comptaient construire un moulin et un pressoir et auraient eu alors beau domaine. Pour vous, Ogier n’était que menteur et larron. Pour lui, il allait être de sa parentèle. Alors il le défend. »

Iohannes se rapprocha d’eux et fit claquer sa langue, attirant leur attention.

« Je sais que maître Aymeric est homme fidèle, ainsi que tu le dis, mais pourquoi donc porter accusation envers Godefroy ? »

Gautier les regarda tous les deux, lentement, étonné, puis franchement hilare.

« Cul-Dieu ! Parce qu’il le croit coupable de ce forfait !

— Ce vieillard borgne serait allé en pleine nuit meurtrir Ogier ? s’étonna Ernaut.

— Peut-être n’y était-il pas rendu pour ça et que la discussion s’est enragée.

— Enragée au point qu’il le traite comme chapon ?

— De quoi ? » s’exclama Gautier, les yeux écarquillés.

« On l’a escouillé avant de le meurtrir, maître Gautier. L’avez-vous déjà oublié ? »

### Casal de la Mahomerie, lundi 16 juin, soirée

Le soleil rougissait le ciel tandis qu’il rejoignait l’horizon derrière les reliefs environnants. L’air était encore empli de poussière et de soudaines rafales soulevaient des volutes colorées, mais la tempête semblait finie. Cette nuit la lune se montrerait peut-être dans le ciel enfin vide de nuages. Ayant fini leur discussion peu fructueuse avec Gautier, les deux compagnons revenaient dans la grande rue et s’apprêtaient à descendre vers la curie lorsqu’un cavalier s’approcha d’eux. Ernaut n’eut aucune peine à le reconnaître, c’était un des valets d’Abdul Yasu, un jeune homme aux cheveux clairs, maigre comme un clou, affublé d’une dentition à rendre jalouses toutes les bêtes qu’il soignait. Il avait oublié son nom par contre.

« Maître Ernaut, je suis aise de vous trouver enfin ! J’ai message de la part d’Eudes. »

Ernaut lui tint les rênes pour l’aider à descendre, mais il sauta avec vigueur sans même se tenir. Il semblait hors d’haleine. Le cheval était encore frémissant de la course, le poil collé de sueur. Il avait apparemment mené grand train.

« Il vous faut vous tenir prêt demain matin à la première heure devant l’église d’ici. Le berger a dit qu’il vous mènerait voir ce que vous vouliez. »

Ernaut écarquilla les yeux, surpris, avant de se tourner vers Iohannes. Le gamin se gratta la tête, inquiet d’avoir mal rempli sa mission. Il chercha dans sa mémoire s’il y avait autre chose ou si sa formulation était inexacte. Faire ainsi fonctionner son intellect lui causa un effort douloureux et visible. Les deux compagnons s’en amusèrent et Ernaut lui fit une bourrade amicale.

« Grand merci à toi. C’est là fort heureux message. »

Il fouilla dans sa bourse et en sortit deux deniers qu’il tendit au jeune. Les pièces disparurent avec célérité et un remerciement. En moins de temps qu’il ne faut pour le dire, il était de nouveau en selle.

« Dois-je porter réponse, maître ?

— Tu repars tantôt ?

— Oui, je devais m’en retourner sitôt le message délivré.

— Les portes seront closes !

— Pas celles de l’écurie. »

Ernaut comprit que le jeune savourait ces moments de liberté à galoper parmi les chemins. Il avait dû sauter sur l’occasion qui lui était donnée de monter une des bêtes. Il lui répondit de faire savoir qu’il serait au rendez-vous, puis le congédia d’une claque sur la croupe de l’animal. En un instant, on ne vit plus de lui qu’un nuage de poussière, accompagné du claquement des sabots. Ernaut sourit, rasséréné par l’enthousiasme du jeune valet. Sans compter que c’était une bonne nouvelle qu’il venait de recevoir.

Blayves le Boiteux devait avoir trouvé une piste, suffisamment intéressante pour qu’on lui dépêche un valet. Il allait proposer à Iohannes d’aller partager la bonne nouvelle avec Lambert et tenter de convaincre Aymeric lorsqu’un gamin s’approcha d’eux. Ernaut le reconnut immédiatement, c’était le petit qu’il avait croisé la semaine précédente, le fils du Provençal qui avait prévenu du meurtre. Les pieds nus, vêtu d’une longue chemise, vêtement d’adulte retaillé, il gambadait avec un petit chat tigré dans les bras, le cheveu hirsute et le visage collé de poussière.

À demi soufflant, dansant, il leur expliqua qu’on l’avait envoyé les chercher, leur père ayant des choses à leur dire. Il les mena donc fièrement aux abords de la maison d’Ogier, leur proposant alternativement de caresser son chaton ronronnant, nommé Crapouille. Il les fit entrer sans plus de cérémonie chez lui, s’élançant alors en criant à tue-tête qu’il avait rempli sa mission. Son père ne tarda pas à descendre l’escalier, en braies et chemise, apportant une lampe à huile. Il leur serra la main.

« J’ai pourpensé à cette histoire, sur les païens et tout ça.

— Quels païens ? s’étonna Ernaut.

— Il se dit que vous pistez un païen, que ce serait lui qui aurait meurtri Ogier. C’est pas ça ? »

Iohannes échappa un soupir las, échangeant un regard mi-agacé, mi-amusé avec Ernaut. Celui-ci tendait plutôt vers l’exaspération.

« Qui vous a donc conté pareilles sornettes ?

— Bein, ça se dit, répondit avec naturel le Provençal, ne comprenant pas où était le souci. Ce n’est pas le cas ?

— Nous ne savons pas de sûr.

— Pourtant, vous vous êtes rendus à Salomé, et c’est fief païen. »

Le géant hocha la tête, sans entrain.

« Le murdrier n’est pas là-bas, du moins je ne le crois guère, se hâta de rétorquer Ernaut, inquiet de voir une flambée de violence à l’encontre des musulmans voisins.

— Je m’en doute bien, jamais on ne les voit par ici, eux autres. Par contre… »

Il se rapprocha d’eux, adopta un air de conspirateur, baissant la voix, jetant de rapides coups d’œil aux alentours.

« Il est fort possible que ce soit l’un des voyageurs venus demander après Ogier tantôt.

— Quels voyageurs ?

— Des païens, de passage voilà peut-être deux dimanches. Ils m’ont accosté alors que je rentrais du moulin moudre mon grain.

— Que voulaient-ils ? »

Le Provençal se mordit la lèvre, plus très sûr de ses souvenirs ou indécis quant à ce qu’il fallait en dire. Il sembla hésiter devant l’air sévère des deux enquêteurs dont les visages inquiétants étaient dessinés d’ombre et de lumière rouge par la lampe.

« Ils m’ont demandé après Ogier, je leur ai montré son manse. Umbert en saura plus, c’est lui qui les a accueillis. »

Ernaut lança un regard vers la maison adjacente, comme s’il pouvait en percer les murs par sa simple volonté, afin de déterminer ce qui s’y était passé.

« Si c’était un de ces païens, je me suis dit que ça pouvait avoir importance. »

Ernaut hocha la tête en accord et remercia le Provençal avant de se retirer, suivi de Iohannes.

La nuit tombait vite et on voyait les lueurs dansantes dessiner des yeux de lumière sur les façades. Le vent calmé, on s’était empressé d’ouvrir les volets pour laisser la fraîcheur nocturne pénétrer dans les maisons. Le village bruissait de cris d’enfants, d’écuelles entrechoquées, de voix se répondant. Une odeur de nourriture se répandait, mélange de pain, de soupe et de viande bouillie. Des cigales et des grillons chantaient.

« Le khamaseen est passé, déclara Iohannes.

— Il nous faut nous hâter en ce cas, le jugement ne saurait plus tarder.

— Crois-tu qu’il pourrait être demain ?

— Non. Il faut le temps de prévenir le sire vicomte. Pas avant mercredi ou jeudi. »

Iohannes passa des doigts las sur son visage douloureux tandis qu’il réfléchissait. Ernaut lui posa une main sur l’épaule.

« Il y a de la lumière chez Ogier, ce doit être Umbert. Allons donc nous enquérir de ces mystérieux voyageurs. »

Ils furent rapidement accueillis par le jeune valet dans la salle commune. Il était apparemment en train de souper, de pain et d’un épais brouet agrémenté de fromage qu’il mangeait à même le pot. Il flottait une odeur poivrée. Les volets grand ouverts laissaient entrer l’air qui jouait avec les flammes des lampes posées sur la table.

Un peu inquiet, il les invita à s’asseoir et leur servit un verre, puis reprit son repas, non sans leur avoir demandé s’ils voulaient le partager. Contre une des banquettes, une hotte en osier abîmée était appuyée, ainsi que des outils pour la réparer et quelques liens de corde. Ernaut grignota un morceau de fromage avant de l’interroger. Il se demandait ce que deviendrait le valet. Vu qu’Ogier n’avait pas de famille, ses biens reviendraient au Saint-Sépulcre, propriétaire des lieux. Le manse et les cultures seraient confiés à un nouveau colon. Aurait-il besoin d’aide sur ses terres ? Le jeune ne semblait pas s’inquiéter pour son avenir.

Il était bien bâti et ses mains puissantes emplies de vigueur. Sa réputation était bonne aux alentours et il ne tarderait pas à trouver de l’embauche. Le Saint-Sépulcre lui demanderait peut-être de s’occuper de la parcelle le temps que le nouvel occupant soit trouvé et s’y installe. La terre d’Outremer offrait toujours de l’ouvrage à ceux qui savaient la travailler. Umbert finissait de récurer le plat d’un croûton qu’il suçait avec délectation quand Ernaut se décida à l’interroger.

« On nous a parlé de voyageurs syriens qui auraient demandé après Ogier voilà peu, cela te dit quelque chose ? »

Le jeune se suçota les dents, avala son quignon et hocha la tête, sans grande conviction.

« C’était fort fréquent qu’on demande après lui, avec son train de mules.

— Ce devait être la semaine avant… C’est toi qui les a reçus.

— Ah oui, je vois. Trois marchands en effet, dont un plus âgé.

— Que voulaient-ils ?

— Ils achetaient l’huile d’olive et avaient entendu parler du projet de maître Ogier de presser lui-même. Dans l’espoir d’affaires futures. »

Iohannes grimaça, déçu de la réponse. Ernaut semblait ne pas s’en émouvoir et poursuivit.

« Ils l’ont encontré ?

— Non, il était dans les terres à ce moment-là. Ils comptaient repasser un jour prochain de toute façon, alors ils sont partis.

— Tu les a revus ?

— Non. Il faut dire que la tempête n’incite guère les voyageurs à prendre les chemins. »

Ernaut hocha la tête en accord et lui demanda de décrire le groupe aussi précisément que possible, de se souvenir d’un détail distinctif s’ils en avaient. Il en obtint une vision aussi précise que possible, sans aucun signe particulier en dehors de leur tenue typiquement arabe. Cela fait, Umbert garda le silence un petit moment avant d’oser demander, d’une voix mal assurée :

« Vous apensez qu’ils sont les assassins de maître Ogier ?

— Je ne peux te répondre encore. C’est possible, car ton maître avait bien dangereux échanges avec les Syriens. Pour autant, ce peut n’avoir été qu’honnêtes négociants.

— Je préférerais ainsi. Savoir que j’ai encontré les meurtriers et les ai peut-être aidés…

— Ne t’inquiète pas de cela. »

Le gamin acquiesça en silence et but un peu de vin, le visage désormais tourné vers l’extérieur. On entendait des gamins brailler et une voix féminine proche houspiller un garnement. Des hommes devisaient fort dans la rue, à propos de toisons de laine. Ernaut se leva et remercia le jeune valet. Ce dernier les accompagna et ferma la porte quand ils retrouvèrent la rue. Un groupe discutait sous un palmier, un peu plus au nord, là ou des chemins venaient couper la route principale tracée par les façades. Leur voix ne formait qu’un brouhaha indistinct.

Ernaut se gratta le menton mal rasé et prit la direction de la curie avant de s’arrêter brusquement. Iohannes l’avait suivi et lui demanda, surpris : « Tu ne vas pas voir ton frère ?

— Si, mais pas avant d’avoir discuté de nouvel avec le vieux Pied-Tort.

— Tu ne veux pas le voir cette veillée quand même ?

— En selle, nous n’en aurons pour guère de temps. »

Iohannes secoua la tête, peu enthousiasmé.

« Aller par les collines à la nuit n’est pas une bonne idée. La lune n’est qu’une lame de faux.

— Justement, il faut nous hâter. Va quérir une monture et retrouve-moi chez mon frère. »

Il lui donna une tape amicale dans le dos, mais l’accompagna d’un regard autoritaire. Pourtant l’interprète résistait.

« Que veux-tu lui demander que nous ne savons déjà ?

— Peut-être pourra-t-il nous dire qui sont ces voyageurs.

— Des marchands, le gamin l’a dit.

— C’est ce qu’ils ont affirmé. Ils ont pu mentir. »

Iohannes ne bougeait pas, les mains sur les hanches, bien déterminé à faire valoir son point de vue.

« Même s’il savait, crois-tu qu’il nous le dirait ? Il ne nous apprécie guère.

— Je l’ai bien vu. Au pire, je saurai lui rappeler que sire Robert n’appréciera certes pas de savoir qu’il a aidé des profiteurs à le voler. »

L’interprète lança un regard aigu à Ernaut. Il n’aimait pas les menaces et la violence et semblait désapprouver la conduite cynique de son compagnon, même s’il en reconnaissait les bonnes intentions. Ernaut insista : « Demain nous voyagerons avec Blayves, et je ne sais quand nous serons de retour. Si je ne vais pas à Salomé ce soir, Dieu seul sait le moment où je le pourrai. »

Iohannes hésita encore un moment puis se rendit aux arguments d’Ernaut. Il partit en trottinant vers la curie. Le regardant s’éloigner, Ernaut finit par lever la tête. Le ciel bleu roi était dévoré d’encre à l’est et de timides lueurs scintillaient parmi la voûte. La première nuit étoilée depuis des jours.

### Casal de Salomé, lundi 16 juin, soir

Après les jours passés dans la tempête de poussière, le monde semblait s’ouvrir de nouveau aux hommes. L’air était frais et l’horizon dégagé. Profitant de l’agréable soirée, Ernaut et Iohannes avaient poussé un peu leurs montures sur les chemins familiers. Ils avaient senti avec délectation le vent siffler à leurs oreilles, respirant joyeusement sans plus avoir l’impression d’être face à un four à pain ou dans une forge. Ils firent fuir un prédateur nocturne, peu enclin à protéger sa proie au milieu du chemin face aux deux furies qui galopaient avec entrain.

Quand ils furent arrivés au-dessus de Salomé, les lueurs du village trouaient les ténèbres de la gorge. Dans la forteresse, la cour elle-même était illuminée. Ils finirent leur parcours au pas, les joues enflammées et les yeux brillants, le cœur allégé par leur course effrénée. Lorsqu’ils pénétrèrent dans le casal, une chèvre au pelage clair vint s’enquérir de leur présence, son attache traînant un piquet qu’elle avait arraché. Tout en mâchant la moindre herbe qu’elle croisait, elle surveillait les deux inconnus, à respectable distance.

En débouchant dans la venelle permettant d’accéder chez Pied-Tort, ils entendirent des voix. Un groupe d’hommes se tenait devant le portail, en pleine discussion. Ils se turent, dévisageant les inconnus qui arrivaient en selle à une pareille heure. On voyait à leurs regards attentifs, voire hostiles, que cela n’était jamais bon signe selon eux.

Malgré tout, quelques-uns inclinèrent la tête de guise de salut, vraisemblablement à l’intention de Iohannes. Devant la porte, le vieil Abu Mahmud finissait de s’entretenir avec un jeune, sans quitter Ernaut du regard. Ce dernier grimaça en voyant l’attroupement. Il avait espéré que cette visite puisse se faire discrètement, à défaut d’être secrète. Il mit pied à terre et salua poliment le vieil homme, qui fit de même.

Iohannes s’avança, s’inclina respectueusement devant le shaykh et commença à traduire, comme les fois précédentes.

« Qu’est-ce qui pousse ainsi des Ifranjs sur les chemins sous la lune ?

— J’ai découvert quelques secrets qui m’indiquent de nouvelles pistes.

— Qui te mènent à moi ? » sembla s’étonner le vieil homme, circonspect.

Son regard était inquiet, et sa main jouait nerveusement tout en caressant sa longue barbe. Il s’effaça et leur fit signe de le suivre à l’intérieur. Il rejoignit la pièce où il les avait reçus les fois précédentes. Quand ils pénétrèrent, la vieille femme sortait de l’endroit, saluant à voix basse, les yeux détournés. Abu Mahmud s'assit à sa place, plusieurs plats contenant de petits gâteaux et des fruits secs étaient posés devant lui. Certaines céramiques étaient quasi vides. Ils n’étaient pas les premiers visiteurs que le shaykh recevait ce soir-là.

Ernaut remarqua d’ailleurs que sa tenue était de meilleur aspect qu’auparavant, et son turban noué de neuf. Une fois tout le monde installé, ils reprirent leurs échanges.

« La tempête est finie et mon frère devra bientôt se battre, voilà pourquoi je suis venu te voir si tard.

— Les gens vont et viennent me consulter à tout moment du jour et de la nuit, je suis là pour ça » répondit le vieil homme, indiquant d’un geste qu’il s’en moquait.

Il était pourtant anxieux, cela se voyait à ses coups d’œil rapides sur l’un et l’autre de ses interlocuteurs, à ses mains qui ne se reposaient jamais, à son assise qu’il corrigeait sans cesse.

« Je sais que des voyageurs ont demandé après Ogier peu avant qu’on le frappe, ils se disaient marchands d’huile. Ils sont peut-être passés ici. »

Ernaut décrivit les individus selon les indications qu’il avait pu glaner auprès de Umbert et du Provençal. C’était maigre, mais suffirait peut-être à réveiller des souvenirs chez le shaykh. Ce dernier écoutait Iohannes, hochant la tête pour marquer son attention. Il réfléchit ensuite un petit moment avant de répondre.

« C’est possible qu’ils soient passés. Nos frères n’hésitent pas à venir nous faire visite, nous porter des nouvelles des amis, de ceux qui vivent au loin.

— Ils étaient là pour préparer un passage ? »

Abu Mahmud secoua la tête, ennuyé. Il se mordit la lèvre supérieure un long moment avant de daigner faire entendre sa voix.

« C’étaient des dimashqi, des hommes libres, pas des fellahs comme nous.

— Tu leur as donc parlé, si tu sais d’où ils venaient.

— Quelques mots à peine. Ils étaient respectueux et sont venus me saluer. »

Ernaut soupira, agacé par les faux-fuyants du vieil homme. Celui-ci le perçut et ajouta sans qu’une autre question lui ait été posée.

« L’un d’eux ne m’était pas inconnu, il habite à Dimashq avec les miens. Un brave garçon.

— Ce n’étaient pas de simples marchands n’est-ce pas ? Que voulaient-ils à Ogier ? »

Le shaykh avala un fruit sec, qu’il mangea lentement. Ernaut insista.

« Je ne veux pas vous causer tort, maître Abu. Mais je ne peux rester sans réponse. Je sais déjà que vous avez menti sur les bornes à la demande d’Ogier. Je connais certains de vos secrets et peut-être plus que vous n’en imaginez. »

Abu Mahmud lui lança un regard noir, mais garda le silence. Circonspect, il examinait le géant assis face à lui et le traducteur à sa gauche. Il semblait se retirer en lui-même ainsi que l’aurait fait un escargot face à un prédateur. Cela enflamma Ernaut, dont la voix se fit plus forte.

« Ne rien dévoiler au sire de Retest est fort dangereux pour moi qui suis loyal serviteur du roi. Ne pas dénoncer la fuite de ses serfs est également grand méfait selon nos lois. N’y a-t-il aucune reconnaissance en vous ? Ne pouvez-vous m’offrir un peu d’aide en remerciement ? »

Il se tassa, les bras croisés, le visage empourpré, tandis que Iohannes traduisait. Le vieil homme inclina la tête, apparemment agacé ou ennuyé par ce qu’il entendait. Cette fois, il répondit presque immédiatement.

« Je comprends ta colère. Imagine quelle peut-être la nôtre qui plions l’échine depuis des années sous les coups, souvent injustement donnés. Que nos maîtres exigent beaucoup a toujours été de la volonté d’Allah. Mais cet Ogier était une brute, un menteur et un voleur. Il a été puni comme il le méritait.

— Laisserez-vous des innocents se faire tuer à cause de lui ? »

Ernaut se tut subitement. Il écarquilla les yeux tandis qu’une idée cheminait en son esprit.

« Cet homme que tu dis avoir connu, c’est lui qui errait parmi les collines, n’est-ce pas ? »

La grimace échappée à Abu Mahmud lui suffit comme réponse. Son esprit continuait d’épier les faits à la recherche de la lueur révélatrice. Le shaykh prit la parole d’une voix lasse.

« Il ne m’a pas tout dit sur ses intentions. Il voulait voir le passeur Ifranj qui était à Sinjil auparavant.

— Il t’a semblé en colère, inquiet ?

— Soucieux. Avec les soldats un peu partout, il est dangereux pour nous de voyager et les hommes qui viennent de vos terres ont usage de nous violenter. Il devait espérer pouvoir faire fuir les siens au plus vite.

— De la famille d’ici ?

— Non, plus au nord. »

Ernaut repensa à la troupe qu’ils avaient rencontrée à la Tour-Baudoin. Nul doute que s’ils avaient croisé un groupe de fugitifs musulmans, reconnaissables à leur anneau de fer, ils auraient appliqué une justice expéditive. La question était de savoir si des hommes auraient été jusqu’à traquer un passeur chez lui afin de l’assassiner après l’avoir torturé. Certains soldats étaient bien assez dépravés, mais l’auraient-ils fait si discrètement ?

Même pour les éléments les plus dissipés des armées, se comporter ainsi aux abords de la cité royale de Jérusalem pouvait être dangereux. Si l’on tolérait certains agissements dans les marches, aux frontières éloignées, la justice royale punissait avec vigueur de tels agissements s’ils se déroulaient sous son nez. Ogier avait quitté Saint-Gilles assez rapidement, peu après son dernier convoi dont il était revenu avec des vêtements sanglants.

Il était peut-être tombé sur des soudards et n’avait dû son salut qu’à la fuite ou, plus sûrement, à la négociation. Dans ce cas, les hommes qui l’avaient attaqué le tenaient à leur merci. Se seraient-ils entendus avec lui pour dépouiller les fugitifs ? Ou lui avaient-ils fait comprendre qu’il devait mettre fin à ses pratiques ignobles de passeur ?

Le lendemain, Iohannes et Ernaut allaient rencontrer le complice bédouin. Ce dernier saurait leur dire si un nouveau passage était prévu. Un voyage dans les villages musulmans aux alentours de Saint-Gilles pourrait s’avérer profitable également, au cas où un nouveau convoi de fugitifs se préparait.

Tandis qu’Ernaut réfléchissait, le shaykh l’étudiait en silence, le visage renfrogné.

« Si tu dévoiles tout cela, nous aurons des problèmes, le sais-tu seulement ?

— Je n’ai pas intention d’en parler à la cour.

— Mais tu le feras si tu en as besoin pour sauver ton frère ? »

Ernaut baissa la tête, sans répondre. Puis il lança un regard désolé au shaykh.

« Je ne peux abandonner un mien frère.

— Pas plus que nous, entends-le bien. »

Le jeune homme acquiesça lentement, dépité. Il lui fallait néanmoins poser encore une question, essentielle.

« Maître Abu, vous n’approuvez pas la meurtrerie ?

— Si d’aucun va en tuer un autre comme un chacal, sans honneur, je ne peux l’approuver. Allah aime que sa justice éclate aux yeux de tous. Elle n’est pas faite parmi les ténèbres. Même les Batinites impies ne se comportent pas pareils à des bêtes, malgré leur esprit de dément. »

Ernaut balbutia un remerciement et sourit sans joie au vieil homme qui demeura impavide, fixant le jeune homme franc comme s’il voyait au travers. Les deux visiteurs saluèrent et remercièrent de l’accueil. Le croissant de lune entamait sa plongée derrière les reliefs lorsqu’ils remontèrent en selle à la sortie du village.

Chemin faisant, Iohannes tira Ernaut de ses réflexions.

« N’en savons-nous pas assez pour convaincre Aymeric ? Nul besoin de pourchasser des fantômes.

— Aymeric est prêt à se battre pour que la terre demeure à eux. Désigner ainsi Godefroy, c’est parce qu’il le sait unique opposant. Il a dû se convaincre que c’était là le seul coupable possible. Je doute qu’il entende rumeurs et idées.

— Tu le penses prêt à mourir aux fins de protéger le bornage ?

— Je le crois prêt à tuer, surtout. Il est grand et bien bâti et cela peut suffire à lui donner grande confiance en lui. »

Ernaut talonna sa monture, occupée à arracher des feuilles à une broussaille appétissante.

« Là où il se trompe, c’est en croyant que ces maudites bornes ont à voir avec la meurtrerie. C’est parce qu’il était passeur qu’Ogier a été tué.

— Menteur, renégat et profiteur, beaucoup de raisons de le meurtrir. »

Ernaut garda un instant le silence.

« Ce mystérieux homme des collines aurait pu frapper, mais le vieux Pied-Tort ne semble pas le croire et ne l’aurait protégé s’il le pensait coupable. Crois-tu qu’il aurait pu être abusé ?

— Sa réputation vient de sa sagesse, Ernaut. On chemine de loin pour avoir son avis sur un mariage, la date des semailles ou le prix auquel céder un agneau. Il faudrait être bien habile pour arriver à le tromper.

— Il est pourtant quelque goupil rusé qui a su éviter toutes les fosses. Et qui nous mène parmi broussailleux chemins. »

Iohannes baissa la tête.

« Je ne saurais prétendre connaître les desseins de Dieu, mais peut-être a-t-il voulu que tout cela soit, qu’Ogier ne puisse continuer ses méfaits.

— J’avoue que plus j’en apprends sur lui, moins je suis peiné de le savoir occis. »

Il marqua un temps, se redressant avant d’ajouter d’une voix plus déterminée.

« Mais je ne laisserai pareil larron emporter mon frère dans la tombe. »

### Collines et oued à l’ouest de Jéricho, mardi 17 juin, midi

Les cris de rapaces résonnaient entre les monts. Les voyageurs cheminaient depuis des heures sous un soleil de plomb. Si la chaleur étouffante de la poussière du khamaseen avait été oppressante, les doigts de feu qui les griffaient depuis le ciel d’azur étaient tout aussi impitoyables. Ils longeaient une faille au fond de laquelle serpentait un mince filet d’eau. Ils marchaient le long du versant sud, espérant ainsi profiter du moindre repli ombragé. Dès que la pente était moins forte, une végétation opiniâtre s’efforçait de s’accrocher au milieu de cette étendue de rocaille beige et grise. Une timide bise avait soufflé un temps, mais désormais ils avançaient penchés, assommés par la chaleur. Leurs montures n’étaient guère plus vaillantes, même le poney au pied sûr de Blayves. Ils avaient croisé quelques groupes d’hommes, ainsi que des pâtres avec leurs troupeaux.

Ils n’auraient plus longtemps à attendre avant de pouvoir parler à cet Abu Hamza. Le camp des Banu Kamil était installé sur un plateau surplombant le cours de l’Oued vers le levant. Ils cheminaient en silence, incapables de faire autre chose que s’efforcer de supporter la chaleur. Seul Ernaut trouvait parfois l’énergie de se tourner pour embrasser le paysage.

Ce n’était que succession de plis et de failles, de collines et de vallées, qu’on aurait modelées de poussière grise. En dehors d’arides broussailles à l’aspect peu engageant, aucune plante n’était visible sur les reliefs, l’herbe jaunie à peine discernable parmi la terre caillouteuse et les rochers. Au fond de la gorge, plus qu’un cours d’eau, c’était un ruisseau, une rigole, un filet qui ne se dévoilait que rarement, inquiet de s’évaporer s’il osait défiait le soleil.

Les camps de nomades se reconnaissaient à leurs toiles sombres, discrets bandeaux de ténèbres jetés dans l’immensité sablonneuse. Ils étaient chaque fois précédés de troupeaux de chèvres, de moutons, gardés par des groupes d’hommes et d’enfants impassibles, appuyés sur un bâton au milieu de nulle part. Desséchés par des années de vie en plein désert, ils semblaient indifférents à tout, au soleil, à la chaleur, à la solitude.

Ils saluaient les voyageurs, répondaient aux questions par monosyllabes d’une voix sifflant entre leurs rares dents puis s’inclinaient pour leur départ avant de retrouver leur position hiératique, ajustant le délicat équilibre de leur foulard sur leur crâne d’un geste nonchalant.

Blayves tendit le doigt en direction d’un contrefort rocheux qui surplombait une large étendue à peu près plate. On y comptait une dizaine de tentes oblongues sous lesquelles des silhouettes tentaient de fuir les rayons du soleil. C’était l’heure de la sieste.

« Le camp des Banu Kamil ! » annonça-t-il fièrement, avant de prendre un peu d’eau à son outre de peau.

Lorsqu’ils s’approchèrent du campement, ils furent entourés d’une horde d’enfants amusés qui les suivaient en sautillant. Certains tendaient la main pour caresser les chevaux, toucher ces voyageurs. Blayves esquissa une grimace.

« Attention à vos affaires, compères. Ils sont mignons, mais naissent avec doigts crochus. Nul espoir de retrouver ce qu’ils vous prendraient. »

Il sourit en faisant de grands signes pour écarter la marmaille, criant quelques imprécations. Devant une des tentes, plusieurs hommes s’étaient regroupés autour d’un doyen, la barbe longue et grise, un foulard à carreaux de couleur indéfinissable maintenu sur la tête par une cordelette. Il leva la main en guise de salut, bien vite relayé par Blayves qui sauta de sa monture rapidement pour aller s’entretenir avec lui. Il leur fit signe peu après de s’approcher.

Avec sa haute taille, Ernaut attirait les regards des présents et les plus jeunes s’amusaient de sa stature. Prudemment à l’écart, certaines filles échappaient des gloussements depuis la zone où elles préparaient le repas, tissaient les étoffes ou réparaient les vêtements. Ernaut s’inclina poliment, attendant de savoir ce qu’il devait faire. Le berger discuta un petit moment sous le regard attentif de Iohannes, puis alors que le groupe face à eux se dissolvait, il expliqua à Ernaut qu’ils avaient l’autorisation de parler à Abu Hamza.

De fait, on les invita à entrer sous la grande tente. Des nattes couvraient le sol et le vieil homme qui les avait accueillis échappa ses savates avant d’aller s’asseoir sur un coussin près du mât central. Il fut imité par deux autres, dont l’un fut désigné par Blayves comme étant Abu Hamza. Le visage parcheminé, mangé par une barbe de plusieurs jours oscillant ente le noir et le gris, il portait son foulard remonté sur les côtés, coincé dans l’anneau d’étoffe qui le maintenait sur sa tête, dévoilant un crâne aux cheveux très courts. Plusieurs épaisseurs de vêtements fluides recouvraient son corps et il était impossible de dire s’il était maigre ou simplement mince. De son visage fin jaillissait un nez épaté surmonté d’yeux noirs attentifs, enchâssés sous des sourcils perpétuellement froncés. Il s’assit face à eux.

Discrètement, des femmes apportèrent du pain plat, du zaatar, un peu de fromage frais agrémenté d’herbes et de quoi boire. Ernaut fut surpris de découvrir du vin dans son gobelet. Iohannes sourit, mais ne répondit pas à la question silencieuse du géant.

Pendant ce temps, Blayves continuait de parler avec le vieil homme qui hochait la tête en assentiment, agrémentant de ses commentaires et questions ce que disait le berger franc. Aucun autre ne parlait. Ernaut laissait sa curiosité naturelle guider son regard et il sourit en voyant que la tenture qui les séparait du quartier des femmes était percée d’un petit volet de toile que les curieuses soulevaient de temps à autre.

Les enfants avaient fini par se lasser des visiteurs et ils couraient aux abords, jouant de baguettes de bois parmi les broussailles. Parfois, un sifflement de berger se faisait entendre, auquel répondait un autre appel. Il réalisa soudain que Blayves parlait désormais avec Abu Hamza et porta son regard sur les deux hommes, tout en avalant du fromage qu’il prélevait en faisant cuiller d’un morceau de pain plat. Iohannes se tourna finalement vers lui, se proposant de faire l’interprète. Que voulait savoir Ernaut ?

« Je suis là, car je crains qu’on en veuille à toi et aux tiens.

— Nous ne nous connaissons pas, pourquoi t’inquiéter de moi ?

— Nos histoires sont liées et t’aider me permettra peut-être d’empêcher mon frère de se battre. »

Il résuma l’histoire du meurtre rapidement, en insistant sur le fait que celui ou ceux qui avaient assassiné Ogier étaient sûrement au courant qu’il avait des contacts bédouins pour aider à la fuite de musulmans. Cela entraîna de longs conciliabules entre les hommes, sous le regard attentif de Blayves qui n’avait pas réagi en entendant Ernaut parler de ces évasions. Finalement, Abu Hamza hocha la tête en souriant.

« Je ne dis pas que je l’ai fait, mais si d’autres le croient, je te remercie de me prévenir. Je saurai me protéger.

— Je suis moi-même un serviteur du roi de Jérusalem et vous parler ainsi avant de signaler ces évasions au mathessep pourrait me causer des ennuis.

— Tu le fais pour ton frère, tu agis bien. »

Ernaut accepta le compliment, conscient que les liens familiaux étaient très importants dans une communauté de pasteurs nomades. Sans référents, sans parents sur qui s’appuyer pour affronter les coups du sort, on mourait. Le désert n’était pas un lieu hospitalier, et nul ne pouvait espérer y survivre seul. Le vieil homme intervint alors, ajoutant de sa voix rauque :

« Nous avons accompagné certains voyageurs porteurs de l’anneau de fer, c’est vrai. Ton sultan n’est pas homme de parole, il ment et vole, nous ne faisons que vivre là où nos pères vivaient. Il est comme le vent qui passe. Il soulève la poussière, fouette le visage des hommes avec fureur et retombe bien vite dans l’oubli. »

Sa main dessina des vagues sur l’horizon puis s’affaissa. Il avala un peu de vin et poursuivit.

« Il croit nous tenir parce qu’il prend quelques bêtes, nous fait payer ses taxes, effraie nos femmes. Il se trompe. La terre n’est pas à lui, elle n’est à personne. Chacun peut s’en nourrir et remercier Allah pour ça. Mes pères vivaient libres avant sa naissance et mes fils feront de même lorsqu’il aura été mangé par les sables de l’oubli. »

Il toisa les trois visiteurs avec défi, le visage fier et le menton haut. Puis il leva son verre qu’il finit d’un trait, avant d’indiquer d’un signe à Abu Hamza qu’il pouvait parler. Comprenant qu’il venait de lui donner l’autorisation d’en dire plus, Ernaut ne se fit pas prier.

« J’ai besoin d’en savoir plus sur les voyages d’Ogier, car je le crois fort mécréant. Si je pouvais découvrir ceux qu’il a volés, je pourrais convaincre l’accusateur de mon frère de changer d’avis.

— Nul ici n’a à se plaindre d’Iji Sinjil, celui qui guidait les fugitifs comme tu dis. Il nous menait les personnes et nous étions payés pour notre labeur à l’arrivée au Bilad al-Sham.

— Pourquoi ne pas les prendre en charge depuis leur casal ?

— C’est trop dangereux pour nous d’approcher des villages, surtout de ceux des serfs. Aucun de nous n’a envie de finir porteur de l’anneau d’esclave. »

Iohannes se permit d’intervenir dans la conversation et demanda à son tour :

« Étiez-vous seuls à œuvrer avec Ogier ? Depuis longtemps ?

— Je ne crois pas qu’il ait eu des contacts avec d’autres familles. Il nous connaissait, car nous avions fait quelques affaires par le passé. Ce n’est pas chose facile à proposer à des inconnus. Je crois que cela a commencé avant que Baldawi n’attaque les nôtres à Baniyas. »

Il s’arrêta et réfléchit un court instant.

« Il a mené trois convois jusqu’à nos tentes. »

Ernaut à son tour compta sur ses doigts et il en dénombrait, pour sa part, au moins quatre. Il s’en ouvrit à Abu Hamza.

« Tu dis vrai, il avait déjà mené quelques personnes des environs de Sinjil par-delà le Gawlan. Trop dangereux pour lui ! Il ne voulait pas cheminer par là deux fois » ajouta-t-il en souriant après une pause où il ingurgita un morceau de pain.

Ernaut se mit à réfléchir. Aux dires de Pied Tort, ce premier voyage avait été payé de bijoux et de monnaies, dont Clément Ferpied n’avait pu être informé. Ce dernier avait ensuite acheté des vêtements syriens à trois reprises, sans que rien ne l’inquiète outre mesure, si ce n’était la grande quantité d’habits. Mais la fois suivante, la présence de sang avait semé le doute dans son esprit.

« Cela ne fait que quatre voyages, donc. Il en manque un…

— Je suis certain que nous n’avons pas mené plus de trois fois des fugitifs, s’amusa Abu Hamza.

— Il ne devait pas y avoir un autre passage, pendant le dernier hiver ?

— Non. Avec tous les soldats dans le coin, ç’aurait été dangereux, en plus. »

Ernaut échangea un regard lourd de sens avec Iohannes. Il baissa la tête, effrayé de comprendre ce que cela signifiait. Abu Hamza s’en inquiéta et demanda ce qui se passait.

« Les derniers fugitifs ont été menés à la mort par Ogier. Il n’avait pas l’intention de les sauver » expliqua l’interprète.

Cela n’attira qu’un haussement d’épaules de la part du Bédouin, ce qui irrita Ernaut.

« Vous vous moquez que les vôtres se soient ainsi fait berner et tuer ?

— Ils n’étaient pas des miens » répondit Abu Hamza, indifférent. Il avala une autre bouchée et sourit avec dédain.

« C’est tous des Hanbalites ! Je n’ai rien à voir avec eux. »

### Environs du Wadi Qelt, mardi 17 juin, début d’après-midi

Le départ du camp s’était fait rapidement, Ernaut étant désireux de ne pas s’attarder. Après des journées de tempête, ils se languissaient du moindre zéphyr. La pierre souvent à nu servait de miroir, accentuant leur éblouissement. Pas un animal ne bougeait alentour et leurs montures avançaient d’un pas lent, l’échine basse. Le firmament d’azur était devenu blanc à force de luminosité. Leur guide les menait au plus court de façon à retrouver les routes à l’écart desquelles les nomades cheminaient et s’installaient systématiquement.

Tandis qu’ils serpentaient sur les flancs du relief pour rejoindre un chemin parmi les buissons, Iohannes approcha sa monture de celle de son compagnon. Devant, Blayves ne leur accordait pas la moindre attention.

« Nous en savons suffisamment, compère.

— Le crois-tu seulement ? Hier soir, Aymeric n’a pas daigné nous entendre, comment espérer le convaincre ? »

Iohannes garda le silence puis reprit :

« Ogier était compaing de la trahison. Il a menti en faisant fuir des musulmans vers Damas à quatre reprises. Puis il a organisé un autre voyage où la liberté n’était pas au bout du chemin. »

Ernaut dodelinait de la tête, semblait refuser de croire une telle hypothèse. Iohannes s’en inquiéta et lui demanda, intrigué : « Cela ne te semble pas clair ?

— Je ne sais. Une de ses victimes est donc venue se venger ?

— Ou l’un des siens. Ogier s’en doutait, il a donc tenté de fuir en partant pour la Mahomerie. »

Ernaut réfléchit un long moment, puis avala un peu d’eau à son outre avant de la proposer à Iohannes. Tandis que son compagnon buvait, il lui fit part de ses réflexions.

« Tout cela semble naturel en effet. Mais est-ce assez pour Aymeric ? Belle histoire à conter, mais comment lui faire montrance de sa réalité, voilà mon souci.

— Ogier craignait peut-être des complices. Des soldats en maraude. Moult sont prêts à frapper contre monnaies. D’autant que là, c’était envers des musulmans, de quoi enjoyer certains croisés. »

Le jeune homme gardait la tête baissée, surveillant le chemin caillouteux de crainte que son cheval ne fasse un faux pas. Faisant cela, il se perdait parfois dans ses pensées. Il se redressa au bout d’un moment et se rapprocha à son tour de l’interprète.

« S’il a quitté si vite Saint-Gilles, peut-être est-ce parce qu’il ne pouvait plus s’y adonner à ses activités passées. Les mahométans de là-bas sauront peut-être l’histoire.

— À quoi penses-tu ?

— Imagine qu’il se soit entendu pour tomber dans une fausse embuscade. Il n’aurait pu faire deux fois le même coup. »

Iohannes acquiesça, l’air contrarié. Il réajusta son foulard, dont il passa un des pans dans son cou.

« Difficile d’en trouver témoignage.

— Nullement, il nous suffit de nous rendre dans le hameau le plus proche de Saint-Gilles et nous verrons bien.

— Il nous faut demander licence à leur maître, Ernaut.

— Pas le temps ! »

Il siffla entre ses dents pour interpeller Blayves. Celui-ci avait désormais une dizaine de longueurs d’avance. Il arrêta sa monture et se retourna, une main sur la croupe de l’animal. Ernaut attendit d’être à sa hauteur pour lui parler.

« Nous allons prendre la route de Naplouse au plus vite. Est-ce là le chemin ?

— Non, je m’avançais vers Ramathes. Vous pouvez aller droit sur Bethel, un peu au-dessus de la Mahomerie. »

Il indiqua une direction au nord-ouest, tendant le bras par-delà les reliefs.

« En continuant outre cette colline, vous trouverez une vallée qui y va directement, vous pourrez longer sa crête nord, on avance bien.

— Grand merci de ton aide, Blayves. Nous allons donc continuer par ce chemin.

— Vous avez eu les réponses espérées ? »

Ernaut fit une moue mi-figue mi-raisin. Il ne voulait pas trop donner l’impression qu’il était redevable au pâtre. Sans qu’il le trouve antipathique, il avait toujours eu des réticences à traiter avec lui. Lui laisser trop d’emprise lui déplaisait au plus haut point. Ils se quittèrent donc sur des saluts amicaux, mais sans enthousiasme.

Iohannes et Ernaut atteignirent ensuite une gorge qu’il leur fallut traverser. Au fond, une mince rigole accueillait une eau claire sur des graviers et les chevaux purent se désaltérer avec bonheur. Ernaut répugnait à les faire boire une eau qu’il craignait trop froide, mais il n’avait guère le choix. Il ne les laissa pas longtemps pour éviter que cela n’entraîne des coliques. Ils remontèrent ensuite parmi une sente obstruée d’éboulis, préférant mener à la longe les bêtes que de s’aventurer en selle dans un si périlleux passage.

Ernaut sentait la transpiration lui couler dans le dos, collant sa chemise à sa peau. La sueur venait régulièrement lui brûler les yeux qu’il essuyait d’une main tout aussi humide. Aucun d’eux n’avait le désir de parler, accablés qu'ils étaient de chaleur et de soleil. La zone pelée qu’ils venaient de quitter leur semblait abandonnée de toute vie et ils firent donc halte auprès du premier arbre qu’ils croisèrent, impatient de profiter de l’ombre.

Assis sur le sol poussiéreux, ils prirent avidement quelques gorgées à leurs outres. Ernaut aperçut des gazelles au loin, un petit groupe occupé à brouter, indifférent à la chaleur. Puis il découvrit parmi des rochers peu éloignés des sortes de gros lapins, avec de courtes oreilles. D’un coup de coude, il les indiqua à Iohannes.

« Des blaireaux des roches[^damans]. Il y en a plein par ici. Ils aiment se mettre au soleil, pas comme nous. »

Il observa un moment les petits animaux étendus puis demanda à Ernaut : « Comment comptes-tu t’y prendre pour interroger les habitants du casal ? Ils seront méfiants.

— Je n’en doute pas. Je me suis dit qu’il pourrait être bon de faire accroire que nous faisons la même chose qu’Ogier. »

Iohannes fronça les sourcils.

« Tu penses qu’il suffira de se prétendre passeur et qu’ils le croiront ?

— On peut toujours essayer, rétorqua Ernaut, badin. Tu peux passer pour un Syrien et nous connaissons le nom d’une tribu de Bédouins ayant déjà aidé au passage.

— Et pour quelle raison le ferais-tu, toi ?

— L’argent, bien sûr. Simple et sûr motif, que chacun peut comprendre. »

Après cette courte pause, ils remontèrent en selle et lancèrent leurs montures à un train plus rapide dès que le terrain leur sembla propice. Ils furent bientôt en vue de Bethel. Une imposante caravane de chameaux y soulevait la poussière, encadrée de cavaliers à l’allure martiale. Ils descendaient vers la cité sainte. Une fois sur la grande route, les deux compagnons se mirent au petit trot, avalant les lieues avec célérité. Ils s’arrêtèrent près d’une source à mi-parcours, abreuvant leurs montures et s’aspergeant le visage et les membres avec délice. Iohannes en profita pour rappeler à Ernaut quelques points incontournables.

« Nous devrons passer voir le ra’is et le shaykh, au moins nous présenter. Il nous sera impossible de parler à qui que ce soit sinon.

— Tu connais l’endroit ?

— Non, mais c’est partout pareil.

— Si c’est comme à Salomé, je n’ai guère envie de m’avouer passeur à leur chef.

— Tous les ra’is ne sont pas tel Abu Qasim. »

Ernaut maugréa, mais ne répondit rien. Il se contenta de jeter quelques cailloux devant lui, l’air renfrogné. Ils ne tardèrent pas à remonter en selle et parvinrent à Saint-Gilles en milieu d’après-midi. Le village était quasi désert, tous les hommes ayant profité de la fin de la tempête pour aller inspecter leurs cultures.

Ils se renseignèrent auprès de gamins qui jouaient aux osselets près d’un bassin. Le plus gros village musulman dans les environs était celui de Seylon, à plusieurs lieues au levant. Ils remontèrent en selle et s’y rendirent rapidement. Dans les champs, parmi les oliviers et les vignes on apercevait des silhouettes occupées à effacer les dégâts de la tempête de sable.

Le hameau était plus petit que celui de Salomé, maigre entassement de maisons collées les unes aux autres. Au milieu des vallons arides, parmi les oliviers, l’endroit paraissait miteux, presque abandonné : des cabanes de pierres sèches, des murets de pisé lépreux, une poignée de volailles et des arbres squelettiques. Nulle part un symbole de l’occupation franque n’était visible.

Leur arrivée ne fut saluée que par une poule curieuse qui grattait en leur jetant un œil intéressé, installée en plein milieu de la rue principale qui serpentait parmi les bâtisses. Ils ne tardèrent néanmoins pas à trouver la demeure du ra’is, la seule à avoir une large cour dans laquelle le maître de céans était justement en train de donner des ordres à trois hommes occupés à refaire le toit d’un des bâtiments. L’endroit était encombré de poutres, de branchages, de feuilles de palmiers, de moellons grossiers et de chaux.

En les apercevant, le ra’is vint à leur rencontre, les saluant poliment. Il était de petite taille, le visage rond caché par une imposante barbe noire qui lui montait presque aux yeux. Sur la tête, il portait un foulard semblable à celui des Bédouins et n’était vêtu que d’un thawb de laine à rayures, de belle allure malgré un tissu peu luxueux. Son apparence était plus celle d’un paysan aisé que d’un riche propriétaire.

Iohannes échangea quelques mots avec lui, et s’attira des sourires engageants. Il hochait la tête en parlant et montra plusieurs fois les environs, invitant les deux hommes à le suivre ici et là dans la cour tandis qu’il discourait. Iohannes rapporta qu’il les avait présentés comme marchands d’huile qui s’intéressaient à la production d’ici. L’endroit étant un peu à l’écart des principales routes, le ra’is n’en était que plus heureux de voir des négociants. Il détaillait à l’envi les terroirs, les expositions et les variétés d’olives qui poussaient sur les monts alentour.

Il expliqua que la majeure partie de leur production était destinée au roi de Jérusalem, mais qu’il leur était possible d’en vendre un certain volume. Il ne doutait pas qu’ils parviendraient à faire affaire. Il les rassura également plusieurs fois au sujet de la tempête passée, assurant que cela ne saurait entamer la qualité et la quantité de l’huile qu’ils produiraient. Comprenant qu’il était cruel de laisser tant d’espoir au village, Ernaut essaya d’abréger l’entretien et dit qu’il serait plus poli pour eux de saluer le shaykh avant de penser à faire affaire avec eux. Cela déclencha un enthousiasme d’autant plus grand chez leur interlocuteur qui abandonna ses ouvriers pour les mener personnellement jusqu’à la demeure du vieil homme.

Chemin faisant, il continua à leur vanter les qualités de leur terroir. Ernaut s’inquiéta un instant qu’il demeurât lors de leurs discussions avec le shaykh, mais il se contenta de leur indiquer la porte et s’excusa, ne pouvant laisser les ouvriers seuls trop longtemps, sous peine de risquer la catastrophe, ajouta-t-il dans un sourire.

Lorsqu’il s’éloigna, la main levée une dernière fois en guise de salut, Ernaut s’en voulut d’avoir joué ainsi la comédie à quelqu’un qui lui faisait bonne impression. La vie était rude pour les fellahs et il se sentait cruel de leur apporter de faux espoirs. Il sentit le regard de Iohannes sur lui et lui confirma d’un signe de tête de frapper. Il n’allait pas hésiter après tous ces efforts.
