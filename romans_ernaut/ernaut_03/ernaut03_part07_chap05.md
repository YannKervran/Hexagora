## Chapitre 5

### Casal de Salomé, dimanche 15 juin, midi

Le trajet avait été particulièrement pénible à Ernaut et lorsqu’il était passé demander à Iohannes de l’accompagner à Salomé, ce dernier l’avait trouvé maussade. Il avait répondu que c’était le temps, qui n’arrangeait rien à l’affaire. Il avait échangé quelques mots avec Lambert tandis que l’interprète préparait son cheval, mais n’en avait tiré nul réconfort. Il maugréait plus qu’il ne parlait, haussait les épaules et gardait le regard au loin. Il prit néanmoins le temps d’expliquer qu’il pensait que la solution à leur affaire se tenait à Salomé, qu’Ogier avait peut-être fréquenté d’un peu trop près une femme de chez eux et qu’un père ou un frère avait dû venir régler cette dette d’honneur.

Iohannes gardait le silence tandis qu’Ernaut lui détaillait son hypothèse. Habitué à retranscrire les paroles des autres sans rien laisser paraître de ses propres pensées, son visage était indéchiffrable. Noyés dans la brume épaissie de poussière ocre, ils ne parlèrent pas du trajet.  l, l’un et l’autre n’étaient pas assez bons cavaliers pour tenir leur monture sans effort. Ils parvinrent avec soulagement au village. Quelques ombres s’y devinaient, mais elles s’évanouirent à leur approche : il n’y avait même pas un chien pour avertir de leur entrée.

Ils se dirigèrent droit vers la demeure du shaykh. La cour était vide lorsqu’ils se présentèrent, ils en poussèrent la porte après avoir frappé plusieurs coups et approchèrent leur monture comme la dernière fois. Puis ils se firent connaître à la porte de la maison. Ce fut la vieille femme qui aventura son visage au-dehors, l’air visiblement mécontent. Elle échangea quelques paroles avec Iohannes dans leur langue rugueuse et referma un instant. Lorsqu’elle ouvrit de nouveau, elle s’effaça, les laissa entrer puis sortit.

Abu Mahmud semblait n’avoir pas bougé depuis leur dernière venue, assis au sol, le dos contre les matelas. Il était en train d’écosser des pois chiches, sans même regarder ce qu’il faisait, jetant les cosses dans un panier vide et les graines dans un pot. Un large sourire fendait son visage, de nouveau, mais cela n’était certainement pas de bonne grâce. L’endroit sentait la sueur, la poussière et l’huile brûlée, et n’était éclairé que par deux lampes dont une était posée devant le vieil homme. Après les saluts d’usage, Ernaut décida d’entrer dans le vif du sujet sans plus attendre. Il n’avait guère de temps à perdre.

« Avez-vous pu apprendre nouvelté sur le voyageur des collines ?

— Rien, désolé.

— De mon côté, j’ai découvert des choses sur Ogier, des choses qu’on m’avait cachées. »

Abu Mahmud releva légèrement un sourcil. Ce fut à cet instant qu’entra la vieille femme avec un bol d’olives et des morceaux de pain. Un pichet accompagnait le tout, sentant le chneiné. Elle fit le service puis repartit, dans un intermède de lumière et de poussière quand elle ouvrit la porte. Ernaut goba une olive avant de reprendre.

« Ogier s’intéressait beaucoup aux femmes. »

Le vieil homme sourit, amusé par la remarque, mais n’interrompit pas Ernaut.

« Ce mystérieux voyageur pourrait être un père ou un frère cherchant celui qui aurait déshonoré une fille ou une sœur.

— Cet Ogier aurait dû faire bien terrible chose pour que sa mort soit la seule façon de laver l’affront.

— Toucher à l’une de vos femmes n’est pas un crime chez vous ?

— C’est plus compliqué que ça. »

Abu Mahmud semblait ennuyé, abandonnant son air placide habituel.

« On ne tue pas ainsi, nous ne sommes pas des bêtes.

— Mais vous ne pouvez obtenir justice en cour, même pas accuser un Franc.

— La vérité sort de ta bouche. Il n’aurait pu le faire venir devant le cadi et aucun des vôtres ne l’aurait entendu.

— C’est pour ça que je pense que c’est une vengeance. »

Abu Mahmud secoua la tête, dépité. Il déchira un petit morceau de pain, qu’il mangea doucement, dévisageant Ernaut comme s’il étudiait une montagne qu’il aurait à franchir. Il en cherchait les sentiers, les pistes, les cols, les faces les plus dangereuses. Il renifla avant de prendre la parole.

« Tu crois que nous serions prêts à tout risquer pour une histoire pareille ?

— Comment ça ?

— Si nous n’avons aucun espoir d’obtenir justice pour nos filles, aller meurtrir un des vôtres revient à attirer le malheur sur nous. Les représailles pourraient être terribles ! »

Ernaut se renfrogna.

« Dans un geste de colère, on peut tuer sans y réfléchir…

— Si tu viens au mitan de la nuit chez ton ennemi avec une arme, ce n’est pas la colère qui te fera frapper, mais la détermination.

— Que voulez-vous me dire ? Que ce ne peut être ce voyageur qui a frappé ? Alors que faisait-il là, caché dans les collines ? »

Abu Mahmud lui signifia d’un geste qu’il n’en savait rien. Ernaut devinait que le vieil homme jouait au chat et à la souris avec lui, mais ne voyait comment l’obliger à parler. Il sentait la duplicité, bien qu’étrangement, elle ne lui parût pas perfide. Il devinait que le secret n’était pas infâme, mais dangereux. Il baissa la tête, but un peu et serra ses mains l’une contre l’autre.

« Abu Mahmud, je ne suis pas là en ennemi, je n’ai nul désir d’accabler un innocent. Mon frère risque sa vie et je souhaite l’aider. Si ce voyageur inconnu n’est pas l’assassin et que tu en as preuve, donne-la-moi et je partirai sur une autre piste.

— Tu dis faire cela pour ton frère, je l’entends. Nous aussi nous avons des familles à protéger.

— Je te fais serment de ne rien faire contre lui s’il est innocent de ce crime. »

Abu Mahmud secoua la tête, comme s’il s’efforçait de se réveiller d’un mauvais rêve.

« Nous étions les derniers à vouloir la mort d’Ogier. Pour les mêmes raisons que tu remues ciel et terre : parce que nous voulons protéger les nôtres.

— Il faisait quoi pour vous ?

— Tu n’as pas encore compris ? Il voyageait beaucoup, n’est-ce pas ? Il se rendait souvent en des terres lointaines… »

Le visage d’Ernaut s’illumina soudain.

« Il servait de passeur, c’est ça ? »

Le vieil homme hocha la tête doucement en silence, à regret.

« Il faisait ça pour…?

— Pas par amitié, c’est certain. Il fallait payer le prix.

— Comment le savez-vous ? On vous l’a dit ? »

Abu Mahmud avala une olive, renâclant à l’idée de poursuivre. Sa voix était moins forte qu’habituellement lorsqu’il répondit.

« Il a guidé les miens hors de vos terres, jusqu’en pays d’Islam.

— Les vôtres ? Pourquoi êtes-vous resté ?

— Je suis vieux et boiteux. Et puis on a encore besoin de moi ici »

Iohannes intervint doucement, dévisageant Ernaut.

« En nous disant cela, il fait de nous ses complices, Ernaut. Tu sais ce que…

— Qu’il vienne, celui qui m’accusera d’aider les païens à fuir. Peu me chaut !

— Ce sont des serfs du roi, Ernaut.

— Et alors, il n’est pas à quelques esclaves près ! »

Il fit craquer ses doigts, revenant au vieil homme, qui avait suivi l’échange sans le comprendre. Il n’arborait plus son visage avenant, paraissait s’être refermé sur lui-même, tassé dans son coin.

« Vous pensez que l’inconnu venait pour un passage ? Ou il vous l’a dit ?

— C’est ce que je crois. »

Ernaut rongeait son frein. Tout ce qu’il avait découvert jusqu’alors semblait prendre place dans sa tête. La victime s’intéressait peut-être aux musulmans parce qu’il aimait les femmes de leur type et il avait fini par leur proposer de s’échapper, moyennant finance. Une activité hautement répréhensible, mais qui expliquait l’origine de sa fortune. Combien pouvait-il demander pour mener des fugitifs ?

« Vous souvenez-vous la somme versée à Ogier ?

— Il nous a pris pratiquement tout ce que nous avions comme monnaies et bijoux. »

Ernaut sentit l’amertume dans la réponse.

« Aviez-vous bonnes relations avec lui ?

— Il en est ainsi de certaines personnes comme de certains outils, répugnants, bien qu’utiles. Il faut savoir y recourir quand cela est nécessaire, mais la joie n’envahit pas le cœur pour autant.

— Il vous menait où ?

— Aux frontières du royaume, où des Bédouins veillaient sur la fin du trajet. »

Se frottant les mains, Ernaut réfléchissait. Il était possible qu’Ogier ait été tué justement parce qu’il aidait des musulmans. En ce cas, le meurtrier pourrait peut-être poursuivre sa mission de justice en traquant l’autre partie de la chaîne.

« Abu Mahmud, connaissez-vous les nomades qui travaillaient avec lui ? »

Le vieil homme ne répondit pas, ses yeux allant de Iohannes à Ernaut, comme s’il hésitait à faire confiance à l’un des deux. Ernaut insista.

« Si Ogier a été tué parce qu’il aidait les vôtres à fuir, les Bédouins risquent leur vie aussi. Outre qu’ils ont peut-être déjà eu à se confronter au véritable murdrier.

— Et comment comptes-tu expliquer tout cela aux tiens ?

— Il me faut juste convaincre l’accusateur, pas toute la cour. Je saurai m’assurer de son silence. »

Le vieil homme n’était pas certain de la conduite à tenir. Il réalisait bien qu’Ernaut avait la persévérance d’un chien de chasse et qu’il n’abandonnerait jamais avant d’avoir obtenu ce qu’il désirait. Il était peut-être profitable d’en faire un allié.

« Je lui avais dit de voir un membre des Banu Kamil, il se nomme Abu Hamza.

— Ils pâturent dans quel endroit ?

— Le long du Jourdain souvent, et plus à l’est. Ça dépend.

— Grâce de ta confiance, je ne te trahirai pas, j’en fais serment. »

Abu Mahmud signifia d’un geste qu’il remerciait Ernaut. Ce dernier échangea quelques coups d’œil avec Iohannes et ils se levèrent finalement de concert. Ils prirent congé et s’empressèrent de retourner à leurs montures. De nouveau la poussière volait, emportée par les rafales qui agitaient les branches des buissons, brûlant les yeux, le nez, la bouche.

Ils tirèrent un foulard sur leur visage pour se protéger et menèrent leurs chevaux jusqu’à la sortie du village. Avant de monter en selle, Iohannes se rapprocha d’Ernaut.

« Que veux-tu faire, céans ?

— Il nous faut trouver ce Bédouin, et je sais à qui demander.

— Tu crois à tout cela ? Qu’on l’aurait occis pour avoir fait s’échapper des serfs ?

— Je ne saurais dire, en toute franchise. Cela me paraît expliquer beaucoup de choses : sa fortune, sa fréquentation des musulmans. Il demeure toujours ce point trouble pourtant.

— Quoi donc ?

— Pourquoi lui avoir déchiré ainsi le bas-ventre ? Pour quelle raison faire si sauvage meurtrerie ?

— Certains cachent une âme de bête sous une chair d’homme. »

Ernaut serra les mâchoires, acquiesçant en silence. Le vent hurlait entre eux, redoublant de violence. Leurs chevaux, les oreilles en arrière, roulaient des yeux terrifiés tandis que les rafales de sable griffaient leurs membres.

« Je m’en retourne à Jérusalem, l’homme qui connaît les nomades réside là-bas.

— Je t’accompagne.

— Tu n’es pas obligé.

— Certes non. Mais je viens tout de même. »

Ernaut examina le regard de Iohannes, qui se détourna. Les derniers jours, certains liens s’étaient tissés, et pourtant, il semblait au jeune homme que le traducteur se sentait concerné personnellement par toute cette histoire. N’était son instinct, il aurait cru qu’il y avait là quelque malveillance à l’œuvre.

### Jérusalem, dimanche 15 juin, fin d’après-midi

Ils pénétrèrent à cheval dans la cité sainte, n’ayant aucune peine à progresser au travers des rues quasi désertes. En dehors des pèlerins, trop émus pour résister à l’envie de voir, toucher, fouler les lieux saints, il n’y avait que quelques rares badauds. Les clochers des églises apparaissaient tels des fantômes jaillis de la brume, fouettés par des embruns d’ocre jaune. Les rues commerçantes étaient désertes en raison du dimanche, mais même de nombreux commerces de bouche étaient clos. On se barricadait, on se calfeutrait.

Poussant des grognements de satisfaction, indifférents à la tempête, un petit groupe de porcs festoyait joyeusement d’immondices entassées dans une venelle. Partout le vent poussait des ondes jaunes, déposait la poussière colorée dans les plus infimes recoins. Les deux cavaliers devaient régulièrement secouer leur foulard pour en détacher la poussière collée par leur respiration. Leurs montures transpirantes, fatiguées, étaient maculées de traînées jaunes. Seules les ruelles les plus étroites échappaient un peu au fléau cinglant, mais elles récoltaient néanmoins leur moisson de fine poussière. Tous les volets étaient tirés, les fenêtres bâclées.

Ernaut n’eut aucun mal à retrouver la demeure de Blayves, l’éleveur de moutons qu’il avait interrogé alors qu’il traquait l’assassin des pèlerines. Il l’avait croisé à de nombreuses reprises, lorsqu’il était de garde à l’entrée de la ville. Ce n’était pas devenu un ami, l’homme était trop bourru pour ça, mais ils se saluaient lorsqu’ils se croisaient. Comme il faisait commerce de moutons, il avait souvent à faire avec les tribus qui nomadisaient sur les terres incultes. Certaines faisaient pâturer ses bêtes, lui en achetaient, lui en vendaient. Il saurait sans nul doute où trouver ce Abu Hamza des Banu Kamil.

La maison était fermée, mais cela était peut-être en raison de la tempête. Ernaut attacha sa monture à un anneau de fer non loin et vint frapper à plusieurs reprises sur les panneaux de bois. Une voix leur parvint depuis le fenestron au-dessus. Quelques instants après, ils se trouvaient face à Blayves, vêtu seulement d’une chemise et de ses braies pendantes. Il tenait à la main une longue corne incurvée, qu’il était en train de poinçonner pour en faire un instrument de musique ou d’appel. La pièce était toujours la même, puant le suint et le mouton. L’enclos était vide, empli de foin ; plusieurs paniers s’entassaient à côté, abritant des toisons à nettoyer.

Sous l’échelle et la plateforme, il y avait désormais une table et des bancs, ainsi qu’un haut coffre. D’un geste, le Boiteux les invita à prendre place. Il frotta son grand nez aquilin, plissant les yeux encore plus que de coutume. Il avait maigri depuis la dernière fois et son regard était fatigué, nota Ernaut. Il s’efforça néanmoins d’afficher un air enjoué et plaisanta tout en s’asseyant.

« Que me veut la cour du roi cette fois ? J’ai payé mes cens et péages !

— Ce n’est pas le sergent qui vient te faire visite, Blayves.

— Et ton compère ?

— Non plus. »

Ernaut prit place et s’accouda sur la table, fixant Blayves dans les yeux.

« Il me faut trouver une tribu rapidement. Je me suis dit que tu pourrais m’y aider.

— Ça se peut. J’en visite pas mal. Son nom ?

— Les Banu Kamil. »

Blayves hocha la tête.

« Je connais, en effet. Ils cheminent souvent en la vallée, mais ne s’aventurent que peu au nord. Ils ont fait quelque vilenie ?

— Pas que je sache. Il me faut voir un des leurs. »

Le Boiteux tendit le menton, circonspect. Puis il se leva et empoigna un pichet sur une étagère, ainsi que trois gobelets. Après avoir fait le service, il fit un toast silencieux et prit lentement une gorgée. Il jouait avec le vin dans ses joues, entre ses dents, avant de l’avaler.

« Je leur ai confié des bêtes déjà. Mais pas en ce moment. Je ne sais même pas s’ils sont dans le coin.

— Si tu les trouves, je peux leur faire visite rapidement. C’est fort urgent.

— De toute façon, avec le khamaseen, ils ne bougeront pas, où qu’ils soient. Mais moi, si je dois m’aventurer dans la tempête… »

Il renifla, s’essuya le nez de sa manche. Il semblait attendre quelque chose. Ernaut toussa pour lui faire lever les yeux.

« Tu connais Abdul Yasu ? Tu pourras lui prendre monture, c’est moi qui paierai.

— Fort aimable à toi. Ça doit être bien pressant que tu me lances ainsi sur les routes en payant mes dettes.

— Un jugement par bataille est en jeu, mon frère y est témoin. »

Blayves fit claquer sa mâchoire. Il dévisagea Ernaut, puis Iohannes, comme s’il était incrédule.

« Je comprends. La parentèle, c’est sacré. Avec ce vent, il ne peut y avoir jugement, c’est prévu quand ?

— Cela devait être hier, mais on attend. Certainement pas plus de quelques jours.

— Fort bien. Je vais voir ce que je peux faire. Demain matin, le vent aura peut-être cessé. Je prétexterai une visite à mes troupeaux. Les tribus savent toujours où sont installées les autres, elles se surveillent sans cesse. Tu auras ta réponse tantôt.

— Je t’en mercie, Blayves.

— Nul besoin de me mercier. Je sais que je n’ai pas affaire à un ingrat. »

Disant cela, il toisa Ernaut, dans l’attente d’un signe de sa part. Celui-ci secoua la tête légèrement, acceptant le marché tacite. S’il s’agissait de compter quelques bêtes de moins au passage de la porte, le trésor royal s’en remettrait. Il n’avait qu’un frère avec lui dans le royaume. Il termina son vin et reposa le gobelet, invitant d’un signe de la tête Iohannes à faire de même. Blayves flattait un chien qui s’était approché, caché jusque-là parmi le foin entassé.

« Tu n’auras qu’à me laisser message auprès d’un des sergents de la cité. Ils sauront me prévenir.

— Tu auras de mes nouvelles, n’aie aucune crainte. »

Ernaut se leva, s’appuya sur la table un instant, comme s’il allait dire autre chose puis s’avança vers la sortie.

Iohannes le suivait, le Boiteux à leur suite. Ils se serrèrent la main en silence et, très vite, les deux enquêteurs se retrouvèrent dans la rue. Ernaut s’appuya contre sa monture et se tourna vers Iohannes.

« Resteras-tu ici pour la nuit ?

— Non, j’ai encore temps de retourner au casal. Le vent faiblit en soirée et je préfère être là-bas. Tu sauras m’y retrouver si tu as besoin. »

Il s’interrompit, car Ernaut regardait par-dessus son épaule : quelqu’un hurlait son nom, tout en s’approchant. C’était Droart, en sueur, le visage enfiévré par une course, les cheveux jaunes de poussière. Il haletait tout en parlant.

« Ah mon vieux ! On m’a narré ton arrivée et ta descente en ce quartier, je cours comme dément depuis lors.

— Que se passe-t-il ? Un souci ?

— Nenni, nenni. J’ai au contraire bonne nouvelle pour toi. J’ai déniché ton colporteur, et peux te montrer son logis. »

Ernaut afficha un sourire ravi, ce qui réchauffa le cœur de son ami.

« Ça n’a pas été bien difficile. Il tient boutique avec sa mère, en une échoppe du Temple de la rue Saint-Étienne.

— Quel type de commerce ?

— Fripe ! J’ai cru comprendre que c’est sa mère qui la tient et lui l’approvisionne en quelque sorte. Il fait commerce en les casaux à l’entour, par la même occasion.

— Tu lui as parlé ?

— Je n’aurais su quoi lui demander et je ne voulais pas effaroucher ton oisel. On m’a dit qu’il était à son hostel néanmoins. »

Ernaut interrogea Iohannes du regard. Ce dernier haussa les épaules :

« J’aimerais bien le voir avant de rentrer à la Mahomerie. Lambert sera sûrement désireux de savoir ce que nous aurons pu en apprendre.

— Je peux vous y conduire à l’instant » confirma Droart, qui suait toujours à grosses gouttes.

Ils décidèrent de mener leurs montures à la bride après les avoir fait boire à une fontaine proche. Le fléau y avait déposé une couche ressemblant à du pollen, donnant à l’eau une apparence boueuse. Les chevaux ne s’en désaltérèrent qu’avec réticence, méfiants de cette mare troublée. Chemin faisant, Ernaut se rapprocha de Droart, qui s’épongeait régulièrement le front de la manche. Son embonpoint lui rendait la chaleur plus insupportable encore qu’aux autres.

« Tu as pu glaner quelque information sur ce Fier-Pied ? »

Droart s’esclaffa.

« Oui, déjà il s’appelle Ferpieds en réalité. »

Ernaut sourit.

« Il semble honnête homme, travailleur et sérieux. Les taxes sont payées quand il le faut et ceux qui me l’ont indiqué n’ont pas eu de reproche à lui faire.

— Aucune rumeur de choses interdites ?

— Ça, c’est pas à moi qu’on l’aurait dévoilé. Il te faudra traquer la bête par toi-même pour t’en rendre compte. Il n’est pas marchand qui ne soit en partie menteur ou voleur, comme on dit.

— Je pense à des choses bien plus graves que de regratter quelques monnaies ou de parler faussement de la qualité d’une marchandie.

— Tu penses qu’il serait le murdrier dans ton histoire ? »

Ernaut se figea. Il n’avait pas pensé à l’éventualité, mais cela se tenait. Le colporteur lui aussi voyageait souvent au loin, il pouvait avoir partie liée au trafic d’Ogier. Dans les entreprises malhonnêtes, il n’était pas rare que les complices finissent par s’entredéchirer et cela pouvait facilement finir les armes à la main. Toujours demeurait inexpliquée la raison des mutilations. Un comparse au sang vif pouvait aisément dégainer un couteau et laisser la violence régler une querelle. Seulement, de là à torturer, il y avait un monde. Par ailleurs, le colporteur n’avait nul motif à embrouiller ainsi l’histoire. Personne ou presque n’aurait pu le relier à la victime, récemment arrivée dans son nouveau casal.

« Aucune idée. Il faut que je le voie avant tout. Sais-tu s’il a de la famille ?

— Sa mère, je t’ai dit. Et un gamin, une fille.

— De quel âge ?

— Aucune idée, on m’a dit jeune donzelle. Peut-être comme les miennes. »

Ernaut soupira. Ogier n’aurait tout de même pas abusé d’une enfant. Il semblait se satisfaire des femmes faites.

« Je te suis redevable de ta découverte, Droart.

— Penses-tu, je me suis juste baladé en posant quelques questions.

— C’est peut-être là la clef qui ouvrira les fers qui retiennent mon frère.

— Dans ce cas, tu me paieras un pichet de bon vin de Chypre un de ces jours ! » rigola le petit homme à l’air replet en lui tapant sur l’épaule.

Il adopta un air sérieux, une voix plus mesurée, lorsqu’il reprit la parole.

« Nous n’avons guère de parentèle avec nous ici, Ernaut. Les compères fidèles se font rares. Alors nous devons faire comme si nous étions frères. Ce qui t’arrive ce jour peut m’arriver demain et je sais que tu seras à mes côtés. Lambert, c’est ma famille aussi, je ne te laisserai pas sans te porter aide. »

En disant cela, il s’arrêta et désigna une boutique à arcade dont les volets de bois étaient fermés. Un des battants servait également de porte, surmontée à l’étage d’une fenêtre obturée d’un parchemin. Au dernier niveau, une ouverture était close d’un large volet de bois, un crochet pour poulie attaché au-dessus. Droart arbora un franc sourire.

« Nous y voilà. C’est la demeure de Ferpieds. »

### Jérusalem, dimanche 15 juin, début de soirée

Droart n’avait pas souhaité rester, et avait invoqué un prétexte pour les laisser interroger le colporteur à leur aise. Il pensait, certainement avec justesse, que la présence de trois hommes serait considérée comme une agression. Ernaut attendit donc qu’il se soit éloigné pour frapper vigoureusement sur les panneaux de la boutique, appelant Clément. La porte s’ouvrit presque aussitôt et ils se trouvèrent face à un visage interrogatif.

L’homme avait les traits massifs, avec une dureté que démentait la douceur de son regard brun. Des cheveux noir coupés courts et une barbe naissante soignée assombrissaient encore son allure. De taille moyenne, il était légèrement voûté, et devait avoir à peine une dizaine d’années de plus qu’Ernaut. Il s’enquit d’eux d’une voix grave.

« J’ai nom Ernaut et suis là, car on vous dit fort ami d’Ogier, de la Mahomerie et avant cela de Saint-Gilles. Il a été meurtri voilà peu et je traque son murdrier. »

Clément lui décocha un regard surpris et se recula pour les laisser entrer, impressionné. La pièce était petite, éclairée d’une seule lampe. Elle sentait la fripe, le vêtement ancien, la poussière. Des tas d’étoffe, soigneusement triés, occupaient plusieurs plateaux. Le marchand lui-même était habillé simplement d’une chemise, en relativement bon état, décorée d’une broderie discrète au col, et ses chausses étaient de qualité, assez moulantes. Il ne respirait pas la richesse, mais une certaine aisance. Il bloqua la porte avec soin.

« Montons à la salle, nous y serons mieux. »

Tout en parlant, il entassa sur son bras les habits d’une des piles et monta au raide escalier de bois desservant l’étage. L’endroit était plus lumineux, avec deux fenêtres tendues de parchemin. Des banquettes longeaient un des murs, sur lesquelles jouait une petite fille de sept ou huit ans, au milieu de poupées de chiffon et d’accessoires en bois. À côté, près de la fenêtre de la rue, une vieille femme dont les traits rappelaient ceux de Clément, mais en plus fins, et surtout dévastés de rides, attendait, un ouvrage de couture à la main. Le colporteur déposa vers elle le tas d’étoffes.

« Tiens la mère, d’autres pièces à réparer. L’une d’elles est trop abîmée, tu peux y tailler touaille ou foulard. »

Elle jeta un coup d’œil rapide aux nouveaux arrivants, avant de fixer son fils, attendant des explications. « Ce sont des amis » indiqua-t-il, laconique. Il se tourna vers Ernaut et Iohannes et les invita de la main à prendre place autour d’une petite table munie de bancs. Il leur proposa de la bière, qu’il versa depuis un pichet moussu. Puis il s’assit face à eux, attentif.

Il tenait son verre à deux mains, comme il l’aurait fait d’une boisson chaude. On entendait seulement le babillement de la petite fille qui s’efforçait de jouer sans trop prêter attention aux adultes. Ernaut but un peu de la bière, inclinant la tête de satisfaction, la lèvre couverte de mousse blanche.

Il aspira cette dernière et prit la parole.

« Vous avez peut-être entendu parler de la meurtrerie de la Mahomerie ?

— Non, j’étais en visite vers le sud ces derniers temps, près Bethléhem. Il m’arrive d’aller jusqu’à Hébron, mais guère au-delà.

— Vous commercez les vêtements ?

— Essentiellement. Je vends aussi dans les casaux boutons, lacets, aiguilles. Tout ce qu’ils ne peuvent trouver sur place. Je rachète les vieilles tenues, mère les ravaude et elles connaissent alors nouvelle vie. »

Iohannes hocha la tête, bientôt imité par Ernaut.

« Vous étiez fort ami avec Ogier ?

— Je ne saurais dire ça. On buvait un godet à l’occasion quand je passais en son casal. Il est… était assez drôle finalement. »

Sa voix se fit mourante.

« Ça me fait bizarre de penser qu’il a été murdri. Il n’y a guère de brigands en ce pays depuis des années.

— Nous ne pensons pas à forfait de larron. Des accusations ont même été lancées et un jugement par bataille est prévu tantôt.

— En ce cas, que cherchez-vous, si les coupables ont été désignés ? »

Ernaut secoua la tête, plissant les lèvres de mécontentement.

« Mon frère est témoin de l’accusé, je ne peux croire qu’il se serait trompé à ce point.

— Je comprends, répondit Clément, plongeant le regard dans son verre. Pourquoi venir me voir ? »

Ernaut se pencha en avant, accoudé sur la table.

« Il me faut en savoir plus sur Ogier. Son murdrier devait le connaître, peut-être est-ce un partenaire en affaires… »

Clément saisit parfaitement le sous-entendu et écarquilla des yeux ronds. Il ouvrit la bouche, sans parler, puis se ressaisit avant de répondre d’une voix qu’il aurait aimée plus assurée.

« Il me vendait toutes sortes de choses quand je passais à Saint-Gilles, c’est comme ça que je l’ai connu.

— Quel genre ?

— Celles que j’achète habituellement : des vêtements, ceintures, bonnets, foulards. Tout ce qui n’est pas en trop mauvais état m’intéresse, tant que ma balle n’est pas pleine.

— Cela faisait longtemps ? »

Clément se mordit la lèvre, hésitant entre le géant inquisiteur face à lui et l’homme brun plus effacé, mais au regard acéré. Il sembla se tasser sur son banc, inquiet.

« Je lui en ai acheté trois… non, quatre fois en tout, pas plus. Écoutez, ça m’a toujours paru bizarre, mais je ne voyais pas de raison de m’inquiéter.

— À quel propos ?

— Ogier me proposait grand volume de marchandie, bien plus qu’un seul homme n’en use, même avec un valet.

— Vous lui avez demandé ?

— Un jour, en plaisantant, je lui ai posé la question. Il m’a répondu qu’on le payait parfois ainsi, quand ses clients n’avaient pas de monnaies. Les soldats étaient souvent entre deux paiements, avec plus de butin que de pièces, et réglaient leurs dettes de leurs pillages. »

Il stoppa, avalant un peu de bière pour adoucir sa gorge serrée.

« Les tenues étaient d’ailleurs des vêtements de Syriens. La première fois, c’était à la Noël, peu avant les combats autour de Panéas, voilà deux Pâques. »

Le silence s’installa, à peine troublé par le chantonnement de la petite fille. Impassible, la vieille femme tirait l’aiguille comme si personne n’était présent dans la pièce. Clément jouait avec son gobelet, évitant de fixer ses visiteurs. Ernaut tendit la main et la posa sur le bras du marchand.

« Il y a autre chose, n’est-ce pas ? »

Clément leva la tête. Il semblait désireux de se confier, mais avait besoin d’y être incité. Ernaut ne le voyait pas se faufiler dans la nuit pour aller châtrer et assassiner quelqu’un. Mais il savait peut-être qui l’avait fait, ou pourquoi.

« Confie-toi, l’ami. »

L’autre hocha la tête, finit sa bière, comme pour se donner du courage.

« La dernière fois que je lui acheté des vêtements, il était encore à Saint-Gilles. Peu après la Noël.

— Une dispute ?

— Pas du tout. Il était fort enjoyé de me voir arriver. Il avait un énorme lot de vêtements. Encore des tenues de Syriens.

— Rien d’étonnant. Depuis l’arrivée du comte de Flandre, nos armées guerroient un peu partout.

— Certes. Ogier m’a conté avoir eu dur labeur, car il espérait pouvoir s’installer sur bonne terre à la Mahomerie. Il avait donc grand besoin de monnaies. Je lui ai acheté le tout.

— Il n’était pas heureux du prix ?

— Si ! Il m’a confié alors qu’il avait enfin assez pour acheter à sa guise. Que je le croiserai la fois suivante à la Mahomerie. Il était très content, au contraire. Jamais vu aussi heureux de son sort. »

Ernaut se gratta le visage, curieux de savoir ce qui troublait le colporteur. Il lui exprima son attente d’un regard. Clément baissa la tête, comme honteux.

« Les tenues étaient pour certaines tâchées de sang.

— Et alors ? S’il les avait obtenues auprès de soldats, on sait bien comment ces derniers les récupèrent.

— Certes, mais là, il s’y trouvait des habits de femme, également tachés. »

Ernaut échappa une exclamation de surprise. Iohannes ne broncha pas.

« Cela ne m’a guère plu. Depuis la mort de mon épouse, alors qu’elle enfantait, je… »

Il ne finit pas sa phrase, ému. Il fit mine de boire un peu de bière, mais son gobelet était vide. Il le reposa avec dépit.

« Je n’ai rien dit à Ogier, sauf que depuis lors je l’ai moins vu. Outre, il ne m’a plus jamais proposé de commercer avec lui. Il avait peut-être vu mes réticences cette fois-là.

— Vous avez encore les vêtements ?

— Non ! Je ne garde jamais très longtemps, sinon je ne pourrai nourrir les miens. Cette fois, j’ai aussi fait une aumône à l’Église. Même si c’étaient des païens, que des messes soient dites pour eux ne saurait leur causer grand mal. »

Iohannes sourit à l’intention de Clément, risquant un regard chaleureux vers sa famille.

« Voilà bien charitable pensée, toute à votre honneur.

— J’espère ne pas avoir mal agi en cette affaire. Les soldats guerroient pour le triomphe du Christ, certes. Mais je suis malaisé de les savoir occire femmes et… Dieu sait quoi ! »

Ernaut acquiesça, jaugeant le marchand d’un regard amical. Celui-ci continua.

« Depuis lors, j’ai dû passer quatre ou cinq fois à la Mahomerie, vu que c’est chemin vers de nombreux casaux. Mais nous n’avons plus fait affaire. Ogier était devenu simple paysan, avec ses oliviers.

— Il ne voyageait plus au loin ?

— Il m’a dit que non, qu’il avait fait cela pour amasser de quoi s’installer sur ces terres. Qu’il y avait réussi, non sans mal, et qu’il en était heureux, même si cela lui avait coûté toutes ses économies.

— Il n’avait plus rien ? On le disait fort riche pourtant.

— Je ne sais que ce qu’il m’a dit, qu’il avait tout dépensé. »

Ernaut décocha un regard déçu à Iohannes puis se leva soudainement.

« Grand merci, maître Clément. Grâce à vous, j’y vois plus clair désormais.

— Heureux de vous avoir aidé en votre quête. »

Ils prirent congé rapidement, déçus de n’avoir pas trouvé là un coupable possible. Tous les deux l’avaient estimé fort inoffensif et ses révélations bouleversaient leur conception du meurtre. Tandis qu’ils détachaient leurs montures de l’anneau, ils échangèrent rapidement leurs points de vue.

« Il a peut-être floué quelques soldats qui seraient venus chercher vengeance, risqua Iohannes.

— Je crois plutôt qu’il avait… »

Ernaut écarquilla les yeux d’horreur.

« Le vieux Pied-Tort disait qu’Ogier guidait les mahométans hors de nos terres. Clément vient de nous dire qu’il revendait des tenues ensanglantées. Ne vois-tu pas le rapport entre les deux ?

— Tu crois qu’il les assassinait ? demanda Iohannes, la bouche déformée par le dégoût.

— Peut-être bien. Avec compaings soldats embusqués sur le chemin. »

Iohannes plissa le visage d’écœurement.

« En ce cas, il aura mérité son sort.

— Certes, mais si je ne peux mettre un nom sur la main vengeresse, cela ne règle pas mon souci. »

Iohannes leva la tête et regarda Ernaut, interloqué.

« Tu parles de raison. Même si Ogier a été frappé pour justes motifs, ton frère n’a pas à en souffrir.

— D’autant que cela ne me déplairait guère d’étaler toute cette histoire aux yeux de tous, si j’arrive à pouvoir en désigner le principal démon. Le monstre n’est pas toujours celui qu’on croit.

— Il est partout, Ernaut. Monstruosité de l’un n’excuse pas celle de l’autre. »

Le géant toisa l’interprète avec surprise, puis se détourna.

« Il nous faudrait retourner dès demain matin à Saint-Gilles. L’histoire est née là-bas, j’en ai désormais certitude.

— Fort bien. Cheminons jusqu’à la Mahomerie ce soir. Les portes de la Cité sont encore ouvertes, nous avons temps.

— Pars sans moi, si tu le souhaites. J’ai besoin de repasser ailleurs. »

Ernaut tapa amicalement sur l’épaule de son compagnon avant de monter en selle.

« Je serai chez toi dès potron-minet. Viens, passons outre l’enceinte ensemble, nous pourrons chevaucher de concert jusqu’à Saint-Étienne. »

### Casal de Mahomeriola, dimanche 15 juin, veillée

Les déclarations et, surtout, l’attitude de Clément Ferpieds, avaient ému Ernaut plus qu’il n’aurait accepté de l’avouer. Il avait réalisé combien cet homme s’efforçait au bien malgré une situation difficile et des épreuves qui le perturbaient. Il se consacrait à sa mère et à sa fille et avait cherché réconfort en Dieu lorsqu’il avait senti le danger se profiler pour son âme.

Ernaut avait cru que finalement, il avait bien agi face à Maciot, et que Dieu ne lui en tenait pas rigueur. Pourtant, la réaction de Clément lui paraissait plus juste. Au lieu de se glorifier comme Ernaut l’avait fait de son crime, il avait cherché à s’amender, à s’en mortifier ou à en faire naître quelque bien. Si jamais Ernaut s’était trompé et avait pris les mauvaises décisions, cela pouvait attirer sur lui et les siens le mauvais œil, la colère de Dieu. Lambert payait peut-être pour les fautes de son frère. Victime expiatoire, il risquait sa vie afin de racheter le péché d’un autre.

C’était une chose qu’Ernaut n’avait jamais bien comprise, mais dont les clercs parlaient souvent. Le Christ n’avait-il pas agi ainsi ? Le trouble qui le gagnait ne pouvait être partagé avec personne. Même Eudes n’était qu’un compagnon de soirées, pour le bon temps, et il n’avait nul désir de s’ouvrir à lui. Il avait besoin de réconfort, de chaleur. Il avait donc décidé de reprendre le chemin de Mahomeriola afin de passer la soirée et la nuit chez Sanson, de façon à voir Libourc.

Toujours amical, le vieil homme l’avait accueilli comme un fils malgré l’heure tardive et ils avaient passé un long moment à parler du jugement. Ernaut leur confia les derniers développements et les pistes que cela traçait pour le lendemain. Il était inquiet à l’idée de ne pas pouvoir empêcher la bataille, mais déterminé à tout faire pour y parvenir. Toute la famille était à ses côtés et ils dédiaient leurs prières à celui qui serait bientôt des leurs.

La soirée s’avançant, Sanson commençait à piquer du nez et il se retira le premier, non sans avoir installé une paillasse à l’intention d’Ernaut dans un coin de la pièce. Il eut un regard lourd de sens vers Mahaut lorsqu’il vit qu’elle demeurait à tirer l’aiguille, reprisant une de ses cottes. Elle soupira, mais rangea son ouvrage soigneusement, avant de le rejoindre. Un drap tendu sur une perche les séparait désormais des jeunes gens.

Ernaut et Libourc étaient chacun d’un côté de la table, se souriant avec complicité lorsqu’ils se retrouvèrent seuls. Ils jouaient aux mérelles sans trop faire attention à qui gagnait, goûtant le simple plaisir d’être ensemble, de s’effleurer les doigts quand ils bougeaient leurs jetons. Libourc semblait pensive et elle fut la première à briser le silence.

« As-tu pourpensé à ce que cet Ogier pourrait avoir fait lors de ses trajets ?

— À quoi penses-tu ?

— On ne voyage pas si aisément, surtout quand des armées sont à l’entour. Il était peut-être plus qu’un simple convoyeur.

— Tu crois qu’il faisait quoi ?

— Je ne sais. L’espie, par exemple. Il parlait la langue des païens, tu l’as dit toi-même. »

Ernaut contempla le doux visage un long moment, indécis.

« Mais pour qui l’aurait-il fait ?

— Loin de moi l’idée de l’affirmer, seulement tu dois t’interroger plus avant, Ernaut. »

Le jeune homme sourit.

« Tu es bien la première à me trouver trop peu curieux !

— Je n’ai jamais dit cela, répliqua-t-elle, dans un sourire qui dessina des fossettes. Tu te laisses fort porter par les choses, par contre. »

Ernaut se rembrunit, surpris et un peu vexé par la remarque. Elle s’en aperçut et lui caressa la main.

« Ne fais pas cette tête, on dirait que je viens de manger ta soupe. »

Elle sourit d’autant plus, les yeux emplis de tendresse.

« Ce me semble que tu agis sans savoir où tes pas te mènent.

— Bien forcé, comment deviner les choses avant de les découvrir ? »

Elle soupira, amusée.

« Crois-tu qu’une épouse devine le temps qu’il fera les mois à venir ? Qu’elle sait qui sera malade parmi les siens et combien d’invités passeront en son hostel ?

— Je ne vois pas… risqua Ernaut, circonspect.

— Voilà chose identique, au contraire. Aucune femme ne prétend savoir de quoi demain sera fait, mais elle doit s’efforcer de prévoir ce qui pourrait être. »

Ernaut haussa les épaules.

« Cela ne sert de rien, vu que les choses ne surviennent jamais comme prévu.

— Certes, mais s’être préparé au mieux garantit qu’on saura y faire face. »

Ernaut grogna, guère convaincu. Elle continua.

« Il faut chaque jour prévoir pour les suivants, chaque saison pour la prochaine, chaque moisson pour les semailles. Une épée graissée sera prête à sortir du fourreau, tandis que la lame oubliée, rouillée, ne sera d’aucune utilité. »

Ernaut se mordit les lèvres, hochant la tête avec réticence. Il demeurait en lui une résistance, qu’elle apercevait, mais ne pouvait identifier. Elle se leva et vint à lui, posant ses bras sur les épaules puissantes, l’attirant à elle avec douceur.

« Ernaut, il demeure quelque souci en ton cœur, je le vois. »

Elle posa l’index sur les lèvres de son fiancé, voyant qu’il s’apprêtait à lui répondre.

« Depuis que tu viens me visiter, je n’ai qu’à me louer de toi et je suis si heureuse que père ait accepté notre mariage prochain. Je sais que toi aussi tu t’enjoies à cette idée, je le lis en toi. »

Elle le fixa un moment, puis ses lèvres papillonnèrent un instant sur celles d’Ernaut. Elle s’écarta finalement, comme à regret.

« Pourtant, il demeure en toi quelque chose qui m’inquiète. Car cela t’effraie. »

Son visage s’attrista.

« Je serai bientôt ton épouse et j’aurais aimé que tu viennes t’en ouvrir à moi, pour alléger ton fardeau en le portant à deux, ainsi que tu l’as fait au matin.

— Je n’ai nul… »

Elle lui sourit avec douleur et posa un baiser très doux sur sa bouche.

« Tu ne le vois pas ainsi, mais je le sens au fond de moi. Je ne suis qu’enfançonne peut-être à tes yeux…

— Pourquoi dis-tu cela ? Ai-je jamais été irrespectueux ?

— Certes non. Pourtant, tu es un homme et tu me traites comme noble dame.

— Tu en as souci ? »

Elle souffla.

« Que nenni, seulement je connais la nature et les besoins qu’ont les hommes, je ne suis pas si naïve. »

Ernaut fronça les sourcils, ce qui amusa Libourc, qui traça du doigt les rides que cela dessinait sur son front. Elle hésitait à répondre et, bien que la chiche lumière de la lampe lui fasse déjà un teint cuivré, Ernaut vit qu’elle rougissait.

« Je me disais que tu te sentais peut-être honteux d’aller voir d’autres femmes. »

Stupéfait, Ernaut en ouvrit la bouche, les yeux écarquillés. Il bafouilla un peu, désarçonné par l’idée. Libourc le regardait, un peu confuse de ce qu’elle avait dit et pourtant emplie de tendresse pour lui. Il ne savait quoi répondre, à la fois amusé par l’idée qu’elle le croie préoccupé par le fait qu’il aille voir des prostituées, mais aussi gêné de ne pas l’être vraiment. Si elle avait bien perçu son trouble, sa candeur l’avait attribuée à une autre cause.

« Pour quelle raison crois-tu que… »

Elle ne le laissa pas finir sa phrase, échappant un petit rire taquin.

« Penses-tu que les damoiselles soient si peu au fait des choses de la vie ?

— Non, bien sûr, mais pourquoi le crois-tu de moi ?

— Parce que tu es un homme, et qu’il te faut assouvir certains besoins. »

Elle se pencha et vint chercher ses lèvres, lui offrant un baiser passionné, tandis qu’elle se collait à lui, l’enserrait de ses bras, ses mains caressantes parcourant son dos, sa nuque, son crâne. Ernaut s’abandonna à l’étreinte et se fit également plus aventureux, parcourant de la main, des doigts, les courbes de sa fiancée. Libourc réagissait avec fougue à ces frôlements et leurs souffles devinrent haletants. La jeune femme finit par se détacher de lui, frémissante, soupirante. Enjouée, elle frotta son nez contre celui d’Ernaut et chuchota :

« Ne me dis pas que cela te suffit, je sens bien que ce n’est pas le cas.

— Je n’ai jamais prétendu le contraire.

— J’en suis fort aise, car il en est de même pour moi. »

Elle risqua un œil vers la tenture qui les séparait de ses parents. Des ronflements réguliers s’en échappaient, auxquels ils n’avaient pas prêté attention jusque-là.

« Mère ne me laisse jamais seule bien longtemps, mais si tu étais plus présent, nous pourrions plus facilement… »

Elle ne finit pas sa phrase, un sourire empli de douceur illuminant ses traits. Elle embrassa le bout du nez de son fiancé et s’échappa lentement de son étreinte. Elle posa une main sur sa joue.

« Si tes errements passés pèsent sur ton âme, tu n’as qu’à te confesser, cela te fera grand bien. Pour l’avenir, nous verrons comment faire au mieux. »

Elle rougit de nouveau à sa dernière phrase, mais tint bon le regard d’Ernaut.

« Me confesser ?

— Certes. Cela apporte grand réconfort de se savoir de nouveau pleinement aimé de Dieu. Tu me reviendras libéré de ces soucis. Je veillerai à ce qu’ils ne renaissent pas en toi. Comme le doit une bonne épouse. »

Ernaut sentit sa poitrine se dilater en voyant la jeune femme devant lui, ses longues nattes brunes lui encadrant le visage. Elle semblait si frêle, si gauche dans sa tenue de laine toute simple. Pourtant elle savait lire en lui, quand bien même elle n’en déchiffrait pas toutes les causes. Elle irradiait de tant d’amour qu’il se sentit fondre. Se tournant sur le banc, il l’entoura de ses jambes et croisa ses mains sur ses reins, la tirant vers lui. Il posa un baiser dans son cou, sur sa joue, vint parcourir ses lèvres avant d’en franchir la barrière. Elle échappa un gémissement, collée à lui dans une étreinte lascive.

Leurs mains hésitaient, se montraient téméraires ou timides, impérieuses ou délicates. Un instant où tout disparut autour d’eux, où leurs âmes, plus encore que leurs corps, étaient en communion. Ils se dévisagèrent un long moment, répugnant à se séparer, désireux de prolonger le délicieux instant.

Un ronflement plus sonore que les autres les fit pouffer et les ramena à la réalité. Libourc murmura : « Il est temps de nous coucher, mon bel ami. Tu as grand labeur qui t’attend demain. »

Ernaut soupira et déposa un baiser tendre sur une de ses fossettes. Elle y fit écho sur son front.

« Que la nuit te soit douce, Ernaut. »

Puis elle s’éloigna vers son lit, simple banquette dans le prolongement de celui de ses parents. Ernaut se leva pesamment et s’assit sur sa paillasse. L’air était moite et lui-même était en sueur, il venait de s’en rendre compte. Il entendit des frottements d’étoffe, Libourc qui se déshabillait derrière le rideau, avant de se coucher. Il tendit la main vers une cruche d’eau. Jamais il n’avait vu muraille plus sordide, enceinte plus haïe que cette simple tenture. Il sentait encore sur ses lèvres la chaleur du corps de sa fiancée. Elle avait raison, il fallait qu’il se confesse, qu’il se libère de ce poids qui lui emplissait le cœur. Et qu’il lui revienne, délivré de tous ses soucis, prêt à l’aimer avec ferveur.

### Casal de Saint-Gilles, lundi 16 juin, fin de matinée

La chevauchée était maussade, le vent soufflait encore plus violemment que d’habitude, frappant la peau de ses myriades d’épingles enflammées. Lorsqu’ils s’étaient retrouvés à la curie de la Mahomerie, les deux hommes avaient hésité à continuer vers Saint-Gilles. Ils avaient également envisagé de voir Umbert, le jeune valet d’Ogier, mais Iohannes y était passé, sans succès. Il était parti réparer un des murs de la propriété, effondré à cause des intempéries. Il avait échangé quelques mots avec son voisin, Guillaume le Provençal, alors qu’il partait, juste avant le regain de tempête. Il espérait mettre en place une consolidation qui attendrait la fin du khamaseen, et revenir rapidement. Ils pourraient donc l’interroger à leur retour.

Les rafales les cinglaient avec frénésie, déversant des braises dans leurs poumons, noyant de poussière leurs yeux et leur nez. Ils ne voyaient à guère plus de quelques pas devant eux et furent même forcés de s’arrêter à plusieurs reprises, incapables de voir au-delà de la tête de leur monture. Le bruit était assourdissant, des légions de démons hurlaient, grondaient, gémissaient autour d’eux. Ils n’avaient jamais eu à subir de tels assauts jusqu’à présent. Ernaut s’en inquiéta, se demanda s’il devait y lire un signe divin ou un courroux du diable. Néanmoins, cela ne faisait que renforcer sa détermination et sa colère. Il pestait, hurlait des paroles que seul le vent entendait, un défi aux éléments, aux événements, à la vie qui ne se déroulait pas comme il l’espérait. Loin de l’abattre, la tempête le galvanisait, l’exaltait, et il se sentait plus fort à chaque rafale, plus vaillant à chaque poussière lui rayant le visage.

Le vent avait faibli lorsqu’ils parvinrent à la Tour Baudoin, mais ils ne s’arrêtèrent pas. Une silhouette se tenait sur le chemin de ronde, qui sembla s’intéresser à eux, mais ils continuèrent, évitant le bourg ramassé contre le flanc de la colline. Tels des fantômes, ils avançaient dans la brume couleur de miel, respirant l’air sec et surchauffé à travers leurs foulards. Un véritable avant-goût de l’Enfer se dit Ernaut. Il se redressa alors, le visage fier, défiant les diables qui les narguaient, qui les ralentissaient. Rien ne le ferait reculer.

Arrivés en vue de Saint-Gilles, ils voyaient enfin au loin, dans un soleil blafard esquissant les maisons à grands traits poussiéreux. Ils se rendirent directement à l’ancien manse d’Ogier, espérant y trouver Pedro. Celui-ci était justement dehors, une poule dans une main et une hache dans l’autre. Il leur sourit en les voyant arriver.

« Toujours en chasse malgré le temps ?

— Il le faut bien. On peut encore vous déranger ?

— Nul dérangement. Laissez-moi juste le temps de trancher le col à c’te volaille pour mon épouse. »

Il tira à lui un billot et y appuya fermement l’animal, qu’il décapita d’un coup sec. Puis il la maintint tête en bas, le temps que le sang s’en écoule. Il avait accompli sa tâche avec habitude.

« Je vous offre un verre de vin, peut-être ? Suivez-moi. »

Secouant la dépouille il en fit tomber les dernières gouttes et s’avança vers la porte de la petite maison. La salle voûtée s’appuyait sur la colline, dont le rocher affleurait au fond. L’endroit était sombre, on ne discernait les formes que grâce à la lumière tombant par la trémie proche. Cela sentait l’étable, mais aucun animal n’était présent.

Pedro posa la poule sur un escabeau et grimpa le raide escalier de bois. Il interpella quelqu’un dans une langue chantante avant de les inviter à le suivre. La pièce à vivre était un peu plus spacieuse, s’avançant plus loin contre le relief. Là aussi, on voyait le rocher, sur lequel des banquettes s’appuyaient. Plusieurs fenêtres perçaient la façade, et deux étaient fermées de volets. La dernière était obturée d’un linge qui laissait passer la lumière.

S’affairant dans un coffre, une jeune femme leur tournait le dos, habillée d’une robe d’étoffe brune, un voile usé lui cachant les cheveux. Elle sortit un pichet et trois gobelets, sans jamais croiser leurs regards. Son visage rond était fatigué et une bosse tendait l’étoffe de son vêtement. Elle échangea encore quelques mots avec Pedro, un sourire évanescent apparaissant sur ses lèvres, puis elle leur abandonna l’étage.

Tout en tirant de quoi boire à un petit tonneau, Pedro les invita à prendre place avec lui sur la natte au centre du lieu. Il n’avait pas de table. Dans un coin, près d’une fenêtre, un petit feu faisait mijoter un pot de terre, la fumée et la suie noircissant le mur aux alentours. Ernaut réalisa combien Ogier avait pu réussir, quand il comparait cette modeste demeure avec celle qu’il avait obtenue à la Mahomerie. Nulle surprise à ce qu’on lui ait prêté une réputation de fortune et lui-même devait s’enorgueillir d’un tel succès. Acquis à quel prix ? se demanda Ernaut.

Lissant ses cheveux machinalement, Pedro s’enquit poliment de leurs avancées, sans vraiment chercher à savoir et, bien vite, il fit dévier la conversation sur la récolte de blé, qui n’avait pas été mauvaise, et celles, d’olives, qu’il espérait satisfaisante. Il était très inquiet du vent de poussière, s’y confrontant pour la première fois.

« Je sais l’olivier fort résistant, mais je n’avais jamais vu pareille tempête. On croirait respirer au-dessus d’un feu, la poussière en sus. Ce matin, il a si fort soufflé qu’il a déchiré deux des fenêtres. »

Il tourna la tête vers les châssis qui étaient désormais obturés de bois, haussant les épaules.

« Vous devez avoir eu bien mauvais voyage jusqu’ici.

— Certes oui, confirma Ernaut. Il nous fallait pourtant revenir, car nous avons besoin d’en savoir plus sur Ogier.

— Je vous ai narré tout ce que je savais.

— Lorsqu’il a quitté le casal, vous étiez là ?

— Oui, j’ai repris le manse tout de suite, il fallait me montrer les semailles faites, les soins à apporter aux terres. »

Il fit une grimace, prit un peu de vin.

« ’Fin, c’est plutôt le gamin qui m’a fait la visite.

— Son valet, Umbert ?

— Oui. Son maître était fort occupé à organiser son voyage. Il était souvent à la Mahomerie, pour préparer son arrivée là-bas. »

Ernaut goûta le vin à son tour, qu’il trouva bien piquant et un peu trop dilué.

« Ogier ne paraissait pas inquiet dans les derniers temps ?

— Non, il était normal. Pressé d’aller en ses nouvelles terres, sur lesquelles il fondait grands espoirs. Ceci dit, je ne le connaissais que peu…

— Le valet semblait perturbé ?

— Non plus. Fatigué, car il avait grand ouvrage à faire, outre me montrer les lieux. Lui aussi semblait satisfait à l’idée de s’installer en une belle propriété. »

Pedro s’appuya contre le mur, les mains derrière la tête.

« Il faut dire qu’ils avaient fort peu de temps pour se préparer. Ogier avait dû trouver ces nouvelles terres récemment.

— Pourquoi ça ?

— Ils n’étaient pas prêts et ont quitté les lieux avant la récolte. En général, ça se fait après. »

Iohannes toussa, échangeant quelques regards intrigués avec Ernaut.

« C’est vrai que vous avez pris les terres peu après la Noël, ce n’est pas d’usage courant.

— Certes pas, mais ça m’arrangeait fort. Il m’a laissé le tout à bons termes, la terre ensemencée pour le prix du grain. Je n’ai pas à me plaindre.

— Vous ne vous en êtes pas étonné ? »

Pedro lâcha un rire franc.

« J’avoue avoir pensé que cela recelait quelque félonie. Une trop belle affaire ne peut être que tromperie. Mais non, c’était juste qu’il ne voulait rater le manse de la Mahomerie.

— Rien d’autre ?

— Je ne crois pas.

— Aucune peur en lui ? »

Le jeune homme se gratta le nez, faisant la moue.

« Pas l’impression, non. Pressé de partir, certes oui, mais savoir pourquoi exactement, je ne saurais dire… »

Ernaut réfléchissait, se passant la main sur le visage, qu’il sentait râpeux.

« À son départ, il n’a rien fait de spécial ?

— Il a chargé ses bêtes, et puis voilà. Il avait déjà fait quelques voyages auparavant. Le gamin le compaignait. Ils ont bu un coup avec plusieurs du casal, dont moi, et on les a salués quand ils ont pris le chemin. »

Ernaut se mordait les joues, cherchait l’élément qui lui manquait dans le mystère qu’il avait devant les yeux. La clef se trouvait ici, à Saint-Gilles, il en était persuadé.

Ogier s’était montré très prudent, pas assez pour échapper à son meurtrier, mais suffisamment pour brouiller les pistes.

« Il n’a rien laissé derrière lui ?

— Non, du tout. Enfin, rien qui n’était prévu entre nous, du bois, du grain, des choses du genre. »

Ernaut finit son vin et reposa son gobelet devant lui, faisant claquer sa langue.

« Grand merci à vous, maître Pedro, de nous avoir si bien reçus ! »

L’homme sourit à la remarque et se leva à leur suite pour les accompagner au rez-de-chaussée. Là, à la lueur d’une lampe, sa femme finissait de plumer la poule, triant le duvet dans un petit panier devant elle. Elle ne leur adressa pas un regard. Ils échangèrent encore quelques paroles polies avant de sortir. Iohannes flatta sa monture, décollant un peu de l’habit de poussière des épaules et du cou de l’animal.

« Veux-tu aller poser questions à Aubert ? Je ne crois pas qu’il soit utile de retourner voir le fèvre.

— Non. Nous avons perdu notre temps ici. Ogier était un homme prudent et il n’a rien laissé voir. J’espérais qu’il se soit trahi d’une façon ou l’autre… »

Il souffla, agacé.

« Le valet en saura peut-être plus ? glissa Iohannes.

— De certes, mais acceptera-t-il de se confier ? Il a peut-être surtout désir que justice soit faite à son maître. Il m’a paru bien effacé l’autre jour.

— Comment savoir si on ne le lui demande pas ? »

Ernaut serra les poings, et cogna doucement contre l’arçon de sa selle.

« La réponse à tout cela est celée en ces collines à l’entour. Je me sens tel le mâtin qui renifle un terrier, mais n’en trouve l’huis.

— Les nomades sauront peut-être.

— Espérons. Ici, les voies se ferment devant nos pieds. »

Il fit claquer plusieurs fois ses dents, contractant les joues, le regard empli de colère.

« Tu as raison, nul besoin de perdre plus de temps ici. Allons secouer un peu le valet et voir quels fruits en tombent. »

Iohannes lui décocha un regard intrigué. Jusqu’alors, Ernaut lui avait paru assez calme et il s’inquiétait de le voir soudain aussi furieux qu’un sanglier blessé. Il s’abstint de tout commentaire, se hissa en selle à son tour et tira le foulard sur sa bouche.

« Le khamaseen, le vent des fous… » se dit-il en son for intérieur.

### Abords de la Mahomerie, lundi 16 juin, début d’après-midi

Le khamaseen ne soufflait plus que par à-coups, mais il noyait néanmoins le paysage dans une brume jaune, poussiéreuse. L’air brûlant les mettait à vif. Arrivés au-dessus de la Mahomerie, ils décidèrent de continuer jusqu’à la source de La Vierge. Ils pourraient s’y rafraîchir un peu et soigner leurs chevaux. La grande route passait à l’ouest du casal, par-delà des champs et des jardins. La brume les empêchait de voir les maisons, ils se contentaient de suivre le chemin, entouré parfois de murets de pierre, de broussailles et de buissons.

Les deux hommes étaient avachis en selle, guère plus vaillants que leurs bêtes épuisées, les flancs couverts de sueur collée de poussière. Ernaut suspectait même Iohannes de s’être endormi à plusieurs reprises tandis qu’ils avançaient dans les vallées d’un pas tranquille et régulier. Il se retournait afin de l’apostropher lorsqu’il devina un mouvement furtif. Il fit front, pour se découvrir face à plusieurs ombres menaçantes qui s’avançaient vers lui.

Des mains s’agrippèrent et, surpris, il fut rapidement mis à bas. Il était secoué en tous sens, incapable de résister aux mains qui le poussaient, le tiraient, le frappaient. Il sentit qu’on essayait de lui lier les poignets et il se débattit d’autant plus vigoureusement. Un autre s’employait à lui enfiler un sac sur la tête. Cela décupla sa fureur. Il se mit à frapper des genoux, des pieds, dans tous les sens, comme un forcené. Il jetait sa masse imposante de droite, de gauche, cherchant à échapper à ceux qui le retenaient, l’enserraient.

Le visage désormais dans le noir il s’en servait comme d’un bélier, tentant de frapper aveuglément. Il hurla un cri de rage quand il entendit un râle de douleur après avoir enfoncé une masse molle, peut-être un ventre. Il s’esclaffa quand son genou heurta apparemment une joue et qu’il sentit les os résonner. Il n’était que fureur, ivresse frénétique et frappait plus violemment tandis qu’il échappait à ses assaillants.

Il réussit à libérer un de ses bras et fit des moulinets, envoyant son coude dans les chairs de ceux qu’il sentait auprès de lui. Il s’efforça d’arracher la cagoule, continuant à se battre encore à demi-aveuglé. Il sentit du sang lui couler dans la bouche, depuis son visage. Cela ne fit que renforcer sa violence, abreuvée de sa propre douleur. Il empoigna des cheveux et fracassa ce qu’il espérait être une tête sur son genou. Il était encore à demi à terre, mais se relevait peu à peu.

Il finit de libérer son bras droit et sa main vint naturellement chercher son épée, sa masse, qu’elle ne trouva pas. Arrachant le sac de sa tête il vit qu’il était aux prises avec plusieurs jeunes hommes, échevelés et hirsutes, dont certains avaient les vêtements déchirés, le visage abîmé. Cela le mit en joie et il propulsa son poing dans la face du plus proche, sentant le nez éclater sous le choc qui lui endolorit les phalanges. L’agresseur se jeta en arrière, les mains cherchant à retenir le sang s’écoulant à gros bouillons. On tenta de le faire tomber, appuyant de tout son poids sur ses épaules.

Ernaut s’efforça de décocher plusieurs coups de tête, sans succès, alors qu’il sentait qu’on lui fauchait les jambes. S’accrochant à l’un d’eux dans sa chute, il s’efforça de s’en faire un matelas et le heurta de tout son poids, l’épaule en avant. L’autre lâcha un râle de douleur puis se mit à gémir. Ernaut en profita pour lui prodiguer quelques coups de genoux maladroits.

Sentant que le dernier assaillant avait lâché prise, il se releva aussi vite qu’il le pouvait, non sans asséner quelques coups de pied rapides dans le ventre et le crâne de sa victime. Il se passa la manche sur le visage, pour la découvrir pleine de sang. Il cracha, appréciant le goût de fer et de poussière salée.

Les chevaux s’étaient enfuis et plusieurs ombres s’affairaient près de là. Il bondit dans leur direction, indifférent aux ecchymoses. Les agresseurs se rendirent compte de son approche et se tournèrent face à lui. Ils tenaient Iohannes au sol, immobile. Ernaut déglutit, craignant le pire pour le traducteur.

Devant ce groupe, un des assaillants avait dégainé son épée, dont il tenait le fourreau de la main gauche. Ils avaient dû lui ôter son baudrier pendant la lutte. Ernaut affirma ses appuis, prêt à recevoir un assaut, les yeux rivés sur ceux de son adversaire.

C’était un jeune, d’une vingtaine d’années comme lui, et son visage ne lui était pas inconnu. Un de ceux du casal, il en était certain. Mais le fils de qui ? Il serra les poings ostensiblement, prêt au combat. L’autre ne semblait plus si assuré, mais lui lança d’un air rogue :

« Si tu approches, je t’embroche telle une volaille.

— C’est une lame de taille, bougre de cul-pelé ! »

Le jeune fronça les sourcils et fit mine de reculer, indécis.

« C’est tout un ! Laisse-nous partir ou je te…

— Tu feras quoi, crâne de puce ? Sans le sac, je sais désormais qui tu es, et tes petits copains aussi. Tu penses pouvoir assaillir un sergent du roi et t’en tirer sans mal ? »

Il se mit à rire, avec malignité et plaisir lorsqu’il vit le doute grandir dans les yeux de son adversaire. Il fit un pas, ouvrant et fermant les mains comme s’il s’apprêtait à bondir pour le saisir.

« Pose mon épée, et je ne te ferai peut-être pas trop mal…

— Peuh ! Nous sommes plus que toi, tu n’as aucune chance !

— De qui parles-tu, petit poulet ? Tes compères se sont enfuis, de crainte que je ne voie leur visage. Tu es seul et à ma pogne ! »

L’autre risqua un coup d’œil rapide par-dessus son épaule. Mal lui en prit. En un éclair, Ernaut fut sur lui et lui asséna sur le poignet un coup violent qui lui fit lâcher l’arme. Puis il lui saisit la gorge dans un étau, cherchant à lui attraper une jambe afin de le mettre à terre. L’homme tourna sur lui-même et atterrit à quatre pattes.

Pendant ce temps, ses compagnons en profitèrent pour se sauver, peu désireux de se confronter à une furie de la taille d’un ours. Ernaut se jeta sur le jeune et lui passa un bras autour du cou. Puis il le frappa de son poing fermé dans les reins, un sourire sadique sur les lèvres, l’insultant à chaque coup.

L’autre se débattait, envoyait ses mains dans l’air vide, tentait de griffer aux yeux le géant qui le martyrisait, en vain. Ernaut le retourna pour lui asséner quelques coups au visage, tout en lui écrasant les jambes de ses genoux. L’agresseur était désormais paniqué, ses yeux roulaient dans tous les sens tandis qu’il hurlait et implorait, recevant les coups sans plus guère offrir de résistance.

Sa lèvre se fendit, sa pommette se colora de sang et une de ses arcades se déchira sous le poing de Ernaut, qui prenait son temps pour matraquer avec précision, tandis qu’il maintenait l’autre par son col. Il entendit qu’on l’appelait par son nom, il se figea, fronça les sourcils.

Iohannes était à côté de lui, le nez en sang, des traces de coups sur tout le visage, les cheveux hérissés. Il respirait difficilement et secouait la tête :

« Non, Ernaut, arrête… »

Le géant avait encore le poing levé, il porta son regard sur le visage tuméfié de celui qui l’avait défié. Il le laissa tomber dans la poussière, reniflant de rage une dernière fois avant de hausser les épaules. Iohannes s’approcha de lui et lui posa une main sur l’épaule.

« Ce n’est qu’un gamin, regarde-le…

— Il a voulu jouer à l’homme ! » répliqua Ernaut, cinglant.

Il se releva, engourdi, sentant les douleurs apparaître maintenant que l’adrénaline retombait. Il lâcha un soupir, repoussant l’autre du bout du pied. Un gémissement lui confirma qu’il était toujours vivant.

Il alla ramasser son épée et la rengaina, puis s’inquiéta de sa masse, qu’il récupéra à son tour. Du sang s’écoulait de son nez, il l’essuya de la main. Il revint vers Iohannes qui s’était assis sur le muret le long des cultures et se tenait les côtes. Il cracha, sang, bile et salive se mêlèrent à l’ocre du chemin.

« Ils ne t’ont pas fait trop mal ?

— Ça ira, grimaça l’interprète. Et toi ?

— Rien que je ne leur ai rendu au centuple. »

Il souffla, se massant la main endolorie par les coups. Il réalisa que sa victime avait profité de l’accalmie pour s’enfuir sans demander son reste. Il esquissa un rictus moqueur.

« Ce sont là des hommes du casal, non ? »

Iohannes acquiesça en silence. Ernaut se passa la langue sur les lèvres, grimaça lorsqu’il sentit une coupure. Il ne comprenait pas, se tourna vers Iohannes.

« Pourquoi donc nous assaillir ? Ils veulent que le jugement ait lieu, ou autre chose ?

— Pour que le jugement se tienne, il suffit qu’Aymeric maintienne ses accusations. Ils avaient autre chose en tête, c’est sûr.

— Il y en avait un petit groupe. Plusieurs parentèles seraient impliquées dans la murdrerie ?

— Une exécution ? C’est à ça que tu penses ? »

Ernaut manifesta son doute d’un geste vague.

« En ce cas, on manipule peut-être aussi Aymeric. On a pu lui faire accroire quelques menteries.

— C’est quand même stupide de nous assaillir maintenant. Nous n’avons rien découvert, ou peu s’en faut !

— Tu le crois et moi aussi. Peut-être avons-nous soulevé quelque roche qu’on espérait oubliée et puis ceux que nous traquons ne savent pas de sûr ce que nous avons découvert. Ils nous pensent près de les démasquer. »

Ernaut se releva doucement et aida Iohannes à faire de même. Il leur fallait retrouver leurs montures apeurées par le combat. Avant cela, ils se nettoyèrent grossièrement de la poussière qui les recouvrait. Sifflant et faisant des appels de langue, ils ne tardèrent pas à découvrir les chevaux, qui paissaient tranquillement, dévorant à belles dents des broussailles.

« Si on voulait nous retarder ou nous faire peur, c’est qu’il y a un gros poisson sous cette roche, déclara Ernaut.

— Je n’ai nulle idée de ce qui pourrait ainsi liguer une partie du casal contre un nouveau venu. Il semblait fort ami avec Aymeric et ceux d’En-Bas.

— Il faudrait alors porter la chasse parmi le Quartier Sainte-Marie. »

Le regard dans le vague, Iohannes semblait contrarié.

« Je n’arrive à croire que parmi ces gens que je côtoie chaque jour puisse se cacher une bande de larrons meurtriers.

— Peut-être ne le sont-ils pas. Pourtant ils cèlent un secret qu’ils n’ont pas envie de nous voir dévoiler, fût-ce au prix de la vie de quelques hommes. »

Ils échangèrent des regards inquiets, consternés de cette conclusion. Ernaut examina Iohannes et se mit à sourire, ce qui surprit ce dernier.

« Que t’arrive-t-il ?

— Nous ferions bien de nous laver un peu avant d’aller questionner le petit valet. Il risque de nous prendre pour des démons de l’enfer si on se présente à lui sanglants et poussiéreux. »

L’interprète avisa l’allure d’Ernaut, dont le sourire jovial le faisait paraître tel un dément, couvert de son propre sang, les vêtements en bataille, les cheveux emplis de saletés. Il ne put retenir un rire qui lui résonna douloureusement dans les côtes.

### Casal de la Mahomerie, lundi 16 juin, après-midi

Une fois changés et débarrassés de la poussière du voyage, leurs plaies nettoyées, les deux enquêteurs se retrouvèrent à la maison d’Ogier. Le vent recommençait à souffler et des gamins qui jouaient devant les maisons furent rapidement appelés à l’abri par d’impérieuses voix féminines. Un cavalier passa au trot, courbé sur l’encolure de sa bête, un chien aboyant à ses côtés. Il ne tourna pas la tête une seule fois tandis qu’il montait la rue.

Ernaut frappa sur la porte de son poing fermé, tout en souriant à Iohannes. Leurs visages étaient encore tuméfiés et portaient les signes visibles de l’agression toute récente. Le traducteur bougeait le torse avec peine, comme si son buste lui était douloureux. Il ne fit néanmoins aucune remarque. Lorsque la porte s’ouvrit, ce fut sur un regard ensommeillé. Umbert avait une mèche de cheveux relevée et les traits un peu bouffis. Ils le tiraient visiblement d’une sieste. Il se poussa pour leur permettre d’entrer.

La seule lueur venait d’un petit fenestron bouché de corne. Il bâilla et les salua, reprenant sans plus de cérémonie le chemin vers l’étage. La pièce était rangée, le ménage toujours fait, à l’évidence. Il avait juste dégagé l’espace central et installé là sa paillasse, sur laquelle il s’assit. Face à lui, sur les nattes, les visiteurs s’installèrent.

Umbert se frottait le visage, encore à demi dans son sommeil. Ernaut lui sourit et s’excusa de venir une nouvelle fois le déranger. Il ne réagit pas.

« Tu ne nous avais pas indiqué que tu étais déjà au service d’Ogier, à Saint-Gilles.

— C’est de bonne fame. Je pensais pas ça utile.

— Il était si bon maître que tu l’as suivi ici ? »

Le jeune haussa les épaules.

« Je n’ai connu que lui. On se convenait, je crois.

— Il t’arrivait de le compaigner en ses errances ? »

Umbert fit une grimace douloureuse.

« Non, je m’occupais du clos, des cultures, des bêtes parfois.

— As-tu idée de ce qu’il acheminait avec son train de bât ?

— Tout ce qui devait être porté par les chemins, par qui pouvait le payer.

— A-t-il suivi un ost ou l’autre ? »

Le jeune toussa, cherchant dans ses souvenirs.

« Ça a dû arriver une poignée de fois, oc. Mais il œuvrait plus souvent à porter grain et paille, foin et vin.

— Il ne commerçait pas à son compte ?

— Pas tant. Un peu à l’occasion, ici ou là, mais ce n’était pas son fort. »

Ernaut soupira ostensiblement.

« Tu dois être bien ennuyé de tout cela, non ? Tu vas retourner à Saint-Gilles ?

— J’aimerais pouvoir demeurer ici en un manse. Voire me donner au Saint-Sépulcre. Ils acceptent volontiers de nouveaux bras pour œuvrer en leurs terres.

— Ses affaires vont revenir au roi et le domaine donné à un nouveau colon. »

Umbert hocha la tête doucement.

« Peut-être que celui qui le reprendra aura besoin d’aide.

— Tu n’es pas inquiet de ce qui va t’arriver ?

— Si, bien sûr, mais je n’ai pas grand choix.

— Tu aurais pu aussi te porter témoin. Cela t’aurait valu quelques amis, dans un quartier ou l’autre. »

Umbert détourna le visage, se gratta la joue, avant de baisser la tête.

« Je n’ai nulle raison à cela. Je ne crois pas maître Godefroy murdrier…

— Pourquoi ne pas l’avoir dit, alors ?s’emporta Ernaut.

— Que vaut mon impression ? Je ne vois nulle personne qui aurait pu vouloir le tuer ici. »

Iohannes fronça les sourcils et intervint.

« Il avait des ennemis à Saint-Gilles ?

— Non plus. Pas que je sache du moins. Un larron aura cru à ces histoires de trésor et sera venu lui extorquer nuitamment.

— Avait-il vraiment quelques richesses secrètes ?

— Si c’était le cas, elles le sont demeurées à mes yeux. Ça me semble guère possible. Il avait ses bêtes, les avait vendues pour venir ici et payer la presse qu’il souhaitait réaliser. »

Iohannes et Ernaut échangèrent un regard complice. Il était temps de savoir ce que le valet savait vraiment, qu’il en dévoile un peu plus sur son défunt maître, malgré tout son dévouement. Ernaut croisa les bras, fronçant les sourcils.

« As-tu idée d’où venait cette renommée de fortune ?

— Racontars de jaloux, je dirais.

— N’est-ce point vérité qu’il allait par les chemins un peu mystérieusement ?

— Il en est ainsi de certains qui s’enfrissonnent à ne plus voir leur clocher et envient ceux qui vont au loin.

— Mais maître Ogier faisait transport de bien dangereuses marchandies, non ? »

Ernaut avait appuyé les derniers termes, dévisageant Umbert durement. Celui-ci piqua du nez, empourpré.

« Je vous ai narré que je n’étais pas avec lui.

— Pourtant tu connais son secret, n’est-ce pas ? »

Umbert secoua la tête en dénégation, mais sans vigueur. Ernaut haussa le ton.

« Il n’y a nulle droiture à mentir pour la fame d’un mort. Des hommes attendent de s’affronter par bataille et si tes menteries mènent au trépas de l’un d’eux, Dieu t’en demandera compte, crois-moi ! »

Le jeune valet affronta Ernaut du regard, ses yeux s’emplissant de larmes. Il ouvrit la bouche, sans qu’aucun son n’en sortît. Iohannes se pencha en avant et parla d’une voix douce, mais déterminée.

« Umbert, nous assavons déjà que maître Ogier faisait cheminer des mahométans hors des terres du roi. Ce qu’il nous faut découvrir, c’est ce que cela lui rapportait et qui aurait pu l’apprendre.

— C’est là bien dangereuse activité, surtout avec un seigneur comme Robert de Retest non loin… » ajouta Ernaut.

Leur discours toucha le jeune homme qui se recroquevilla comme un parchemin sous l’effet de la chaleur. Il serrait les genoux contre son buste et refusait de croiser leurs regards. Lorsqu’il brisa enfin le silence, ce fut avec répugnance, d’une voix sourde.

« Je n’ai jamais su au juste ce qu’il faisait. Il partait et revenait, voilà tout.

— Il servait de passeur, tu as bien dû le voir converser avec des mahométans parfois. Le dernier convoi qu’il a fait, avant la Noël, tu en as souvenir ? »

Umbert secoua la tête, sans arriver à parler.

« Il y a eu quelque chose de spécial cette fois-là ? Qui aurait poussé maître Ogier à partir de Saint-Gilles ? »

Le valet mit un moment avant de répondre.

« Il n’a rien dit, sauf qu’il avait enfin de quoi venir s’installer en plus beau manse.

— Il n’est pas allé en un endroit particulier ? N’a rien confié de ce qui s’était passé ? Il n’était pas effrayé ?

— Non, plutôt content d’enfin pouvoir s’installer en meilleures terres.

— Tu n’as pas remarqué qu’il avait rapporté vêtements et étoffes de ce périple ? »

Umbert se mordit la lèvre, fronçant les sourcils comme s’il ne comprenait pas la question.

« Des vêtements ?

— Oui, il est revenu avec quelques marchandies, non ?

— Bien sûr. Souventes fois, des soldats le payaient de ce qu’ils pillaient.

— Il n’y avait pas là plus qu’habituellement ?

— Je n’y ai pas pris garde. »

Ernaut se passa la main sur le visage, agacé de ne pas récolter d’informations précises. Il inspira avant de demander :

« Il menait un convoi de fugitifs ou portait simplement des denrées ?

— Je crois que c’était les deux, à dire le vrai. Il partait long temps lorsqu’il menait quelques-uns en terre infidèle.

— Ce fut le cas, cette ultime fois ?

— Oui. »

Il tournait la tête tantôt vers l’un tantôt vers l’autre puis ajouta, avec dépit : « Enfin pas tout à fait. Il est rentré plus tôt que je m’y attendais, mais je n’ai pas osé lui en demander la raison. Un soir, il m’a confié qu’il avait encontré le passeur plus tôt qu’habituellement. Il en était fort content, car cela lui avait permis de rentrer vite et donc de commencer à préparer sa venue à la Mahomerie. »

Le silence s’installa un petit moment. On n’entendait que le souffle ronflant d’Ernaut, tel un taureau se préparant à charger.

« As-tu idée de ce que ton maître a fait lors de ce voyage ? » lança-t-il d’une voix sèche.

Comprenant au ton de la question que la réponse ne saurait lui plaire, Umbert baissa la tête en silence, prêt à recevoir le coup.

« Il a occis ceux qu’il menait et les a dépouillés, peut-être avec l’aide de quelques soldats complices. Il était murdrier et larron, ton maître, et certains lui en ont demandé justice. »

Umbert le fixait, hagard, comme s’il ne comprenait pas ce qu’on lui disait. Il bafouilla, sans arriver à articuler une phrase.

« Il me paraît avoir eu grand don pour se créer des ennemis, à ainsi dépouiller plus faible que lui, et je ne saurais jeter la pierre à ceux qui ont cherché vengeance. Seulement, mon frère risque sa vie dans cette histoire ! »

Il se leva, haussant la voix.

« C’était sacrée fripouille ! Tu pensais le bien servir à celer ainsi ses faiblesses et ses mensonges. Au final, tu ne finis qu’à t’en faire complice. »

Umbert avait désormais des larmes qui coulaient sur ses joues. Excédé, Ernaut se tourna d’un geste vif vers Iohannes et lui fit signe de sortir à sa suite. Le jeune valet, effondré, ne se leva pas pour les accompagner. Ce fut de fort mauvaise humeur qu’Ernaut retrouva la rue. Le vent le cingla avec vigueur, ce qui décupla sa rage. Iohannes lui tapa sur l’épaule, cherchant à le calmer.

« La paix, l’ami. Ce n’est que fidèle serviteur.

— Ses cachotteries risquent de mener mon frère tout droit en sa tombe !

— En es-tu assuré ? L’assaut que nous avons subi me fait croire que peut-être tout cela n’a rien à voir avec les convois d’Ogier justement. »

Ernaut se figea, attentif, tandis que Iohannes continuait.

« Si, comme nous le disions, il y a conspiration en ce casal, je ne crois pas que cela puisse avoir aucun lien avec ses affaires à Saint-Gilles.

— Ne pourraient-ils pas être plusieurs à faire semblable trafic ?

— Cela m’étonnerait fort. Les seigneurs de ces casaux se feraient bien vite connaître si trop de musulmans s’enfuyaient. Il n’y a qu’à voir sire Robert donner la chasse lorsqu’un seul tente de lui échapper. »

Ernaut se rendait aux arguments. Quant à l’idée qu’ils auraient pu prendre fait et cause pour les éventuelles victimes du dernier trajet clandestin d’Ogier, l’hypothèse lui parut suffisamment absurde pour qu’il ne l’évoque même pas à Iohannes. Si un colon comme Ogier pouvait à l’occasion se lier avec des musulmans, c’était fort rare, et toujours fortement désapprouvé par les deux communautés. Chacun vivait dans son monde, l’indifférence contenant la haine dans des limites acceptables.

Il n’en savait guère plus qu’au premier jour et, tandis qu’il se tenait dans la rue, le vent qui soufflait à ses oreilles lui semblait porter les rires de démons moqueurs, de ces djinns dont on évoquait avec frayeur le nom dans les légendes locales, le soir à la veillée.
