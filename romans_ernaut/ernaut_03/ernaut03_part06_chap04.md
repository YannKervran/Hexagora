## Chapitre 4

### La Tour Baudoin, samedi 14 juin, matin

Le monde se noyait dans un lac de soufre. Tout était d’ocre : le ciel, la terre, l’air… Le soleil s’était déversé en une épaisse couche de feu brûlant les visages, raclant les poumons. La chaleur, suffocante, desséchait la peau, où s’accrochait une poussière d’ocre. Simples silhouettes, les arbres tremblaient le long du sentier qui ne se dévoilait qu’au dernier instant, pas à pas. S’ils n’avaient pas suivi une route importante, Ernaut et Iohannes n’auraient eu aucune chance d’arriver à destination. Il leur fallut néanmoins rebrousser chemin plusieurs fois, serpenter parfois au long d’une pente sans être sûrs de leur direction. Dans les villages qu’ils apercevaient, nulle vie ne se manifestait. On se rencognait, on se barricadait. Sans succès bien souvent. Le vent poussait la cendre jaune sous les portes, à travers les volets, dans les naseaux des bêtes, les bouches des hommes.

Les deux cavaliers avaient protégé leurs montures de pièces d’étoffes et ramené leurs turbans devant leurs visages. Ernaut avait proposé à Iohannes de se rendre seul à Saint-Gilles, mais l’interprète avait insisté pour venir. Il souhaitait aller au bout de cette histoire et pensait pouvoir être encore utile. De fait, sans lui, Ernaut se serait certainement égaré. Ils avançaient lentement, s’arrêtant souvent afin de contrôler l’état de leurs chevaux, vérifier si le chemin paraissait le bon. Ils suivaient désormais un fond de vallon encaissé. Iohannes cria à Ernaut à travers son linge qu’ils arrivaient à Bourg-Baudoin. Ernaut devina les contours d’un imposant bâtiment qui se dessinait sur sa droite.

« Il garde l’entrée du passage au nord, vers Saint-Gilles et au-delà, la Samarie. Allons y abreuver les chevaux, il demeure bien deux à trois lieues à faire. »

Il guida son cheval vers le sentier, délimité par de gros blocs épars qui avaient été poussés lors de l’édification du bâtiment. Des bâtiments annexes étaient appuyés contre le relief et les murs de la forteresse. La porte étant fermée, ils durent appeler un long moment avant que quelqu’un vienne. Le visage protégé par un voile, un soldat en vêtement rembourré leur ouvrit le portail, non sans se plaindre abondamment de façon imagée. Il leur tint néanmoins les rênes le temps pour eux de démonter. Iohannes tira son foulard et sourit à l’homme.

« Nous aimerions donner à boire à ces pauvres bêtes, nous cheminons depuis un moment…

— Folie par ce temps ! Les ruisseaux ne sont que boue !

— Nous n’avons guère le choix, il nous faut joindre Saint-Gilles au plus tôt. »

Le serviteur avisa les armes au côté de Ernaut et le gabarit imposant du porteur. Couvert de poussière jaune, on aurait dit une créature de l’autre monde, un démon du désert.

« Je m’occupe de vos bêtes, allez-donc en la grand’salle, vous y encontrerez compagnie.

— D’autres voyageurs ?

— Non, des soudards du comte Thierry, à l’héberge ici depuis quelques semaines. »

Iohannes retint la grimace qui commençait à s’afficher sur ses traits. Il soupira. Ernaut, pour sa part, n’avait que faire de rencontrer d’autres sergents. Depuis quelque mois, il avait eu son content de récits de batailles, de faits d’armes et de prouesses. Il savait que la vie du soldat se bornait généralement à attendre, longtemps, puis à mourir, rapidement. Il invita du bras Iohannes à le suivre dans la direction indiquée par l’homme.

Lorsqu’ils poussèrent la porte, cela déclencha une effusion de jurons et d’exclamations indignées. La pièce était sombre comparée au-dehors, malgré un fenestron bloqué d’un fin parchemin. De nombreuses lampes à huile avaient été disposées un peu partout, dessinant sur les visages des tatouages de feu. L’assemblée s’était figée à leur entrée. L’odeur de la sueur, des vêtements sales, des haleines avinées puant l’oignon les frappa tout d’abord. La pièce n’était pas si grande, avec des paillasses le long d’un de ses murs. Plusieurs corps y étaient affalés, parmi les couvertures et les sacs. En face, une table étroite s’étendait, avec un banc partiellement occupé par des hommes accoudés parmi la vaisselle sale, le visage morne.

Quelques silhouettes féminines, les cheveux mal peignés, recouvraient de leurs bras maigres les cous robustes, les épaules nerveuses. Les regards n’étaient guère amicaux, pas plus qu’hostiles. Emplis de mauvais vin. Tout le monde attendait. Un des hommes se leva doucement, amoindri par l’alcool. Il avait le visage rond, les joues pendantes de l’homme bien nourri après de longues privations. Il tenait le poing appuyé sur la table et s’aidait de l’autre bras sur une femme au sourire gouailleur, le regard éteint.

« Bienvenue en terre de Sodome, voyageurs ! »

Quelques visages s’égayèrent et certains osèrent même un rire crispé, attendant de voir si le géant qui venait de faire irruption était un démon, un ange ou juste un homme. Ernaut afficha un sourire amusé.

« On m’avait dit que c’était plus au sud de ma route, la tempête a dû me perdre ! »

L’homme ricana et cracha, amusé, manquant de peu un compagnon endormi sur le plateau.

« Prends donc un godet pour ôter cette poussière de ta gorge, c’est le roi Baudoin qui régale !

— Grâces lui en soient rendues » répliqua Ernaut, s’emparant d’un verre et servant Iohannes.

Il avala plusieurs gorgées, se permettant de regarder la compagnie en détail, tandis que lui-même faisait l’objet d’un examen sinon attentif du moins curieux.

« On m’a dit que vous étiez du comte Thierry, c’est cela ?

— Exactement, revêtus de la sainte Croix pour trancher du païen avec la bénédiction du Pape et tout le saint frusquin… On est des héros de la Foi ! »

La déclaration arracha quelques méchants rires, mais guère d’enthousiasme. Ils avaient trop versé de sang, commis trop d’atrocités pour être capable du moindre élan sincère en dehors de la colère.

L’homme poussa du pied un de ses camarades avinés qui s’affala de son banc dans un grognement qui ne le réveilla pas, et invita les deux voyageurs à s’asseoir près de lui.

« Je me nomme Gosbert, capitaine de cette troupe.

— Je suis Ernaut, de Jérusalem, et voici mon compagnon, Iohannes.

— Un Grec ? demanda le soldat, une lueur malsaine dans le regard.

— Certes pas répliqua Iohannes. Je suis du comté perdu, d’Édesse, mais désormais je vis ici, au royaume. »

Gosbert approuva en silence et avala une gorgée de vin.

« Vous êtes de ces terres, alors ?

— Je viens de Bourgogne répondit Ernaut.

— Ah, beau pays que voilà. Un mien oncle y commerçait quelques laines et y achetait bonne piquette. Je l’ai traversé bien vite en venant, trop à mon goût.

— Tu auras bon temps en retournant au pays. »

Gosbert tourna un regard intrigué à Ernaut.

« Si jamais on quitte cette terre désolée. J’espère bien pouvoir ramener mon butin chez les miens, mais le comte semble aimer l’outremer plus encore que la Flandre[^Flandre].

— Cela fait longtemps que vous êtes ici ?

— Trop ! À attendre en buvant et forniquant ! »

Il caressa voluptueusement la femme qu’il tenait contre lui et finit par lui taper sur les fesses. Elle répondit en adoptant une posture aguicheuse, avec une telle affectation qu’elle semblait innée.

« Parfois, on s’amuse un temps des païens du coin. Si d’aucun s’échappe, on lui fait chasse. Dommage qu’on n’encontre pas tant de nomades dans le coin…

— Prends garde à ne pas t’emparer du bien royal. Les bédouins appartiennent au roi et lui versent tributs. Si tu t’attaques à eux, tu risques des ennuis.

— Peuh ! Ce ne sont pas quelques brebis ou de jouer un peu avec eux qui contrariera le roi. Il ne se gêne pas pour le faire lui-même. S’il me met à l’amende, j’ai de quoi payer ! »

Un de ses compagnons se dressa soudain, son verre tendu à bout de bras, il attrapa une des filles et l’embrassa avec avidité, d’un désir animal, violent. Puis il se renversa le vin sur le visage plus qu’il ne but et cria alors « Dieu lo veult ! » bientôt repris en chœur par tous ses compagnons. Certains, trop saouls ou trop fatigués, se contentaient d’ânonner, mais la plupart braillaient en riant. C’était à celui qui hurlerait le plus, crachant et postillonnant, reniflant et éructant telle une bête.

Ernaut, sourit, amusé de ce regain de vie chez ces hommes voués à la mort. Iohannes se contentait de boire lentement, le visage fermé. Gosbert se leva et vint s’appuyer sur l’épaule d’Ernaut.

« Servez-vous, prenez ! Ici, la terre regorge de vin et de lait et les fruits les plus savoureux vous tombent dans la main, emplis de jus délicieux ! »

Il se saisit d’une jeune femme qui passait et l’approcha d’eux.

« Tiens, je te présente… »

Il la dévisagea un long moment, le regard hébété, abruti de vin. Elle le regardait, indifférente. Pas très grande, elle était indiscutablement syrienne, ses longs cheveux noirs tombant en désordre sur ses épaules frêles. Le menton pointu, dressé, était surmonté d’une bouche moqueuse, provocante. Le nez fin, droit, était encadré d’yeux sombres en amande. En eux Ernaut put lire la détermination et l’indifférence à ce qui l’entourait.

Elle fixait Gosbert ainsi qu’elle l’aurait fait d’un cheval ou d’un paysage. Cela ne la concernait pas. Elle faisait mine de se débattre, ramenant ses vêtements que le soldat s’amusait à désordonner. Mais cela ne la touchait pas, elle était ailleurs. Lorsqu’elle daigna arrêter son regard sur lui, Ernaut se sentit gêné, bizarrement, et il n’insista pas malgré le désir qu’elle provoquait en lui. Elle échappa un sourire et baissa les yeux, comme intimidée.

« … Layla.

— Ah oui, ma princesse ! Comment t’oublier ?

— Je suis unique, il n’en existe pas mille comme moi…

— Tu l’as dit ma beauté ! »

Il se tourna vers Ernaut, continuant à caresser le corps malingre qui se débattait sans conviction.

« Le rêve de tout homme, cette Layla !

— Te nommerais-tu Qaïs ? » demanda Iohannes, dans un sourire. Intriguée, Layla darda sur lui un regard impérieux. Elle le toisa un court instant, puis lui répondit d’un air mutin.

« Seul Majnoun me conviendrait ! »

Iohannes s’esclaffa à demi.

« Tu es bien savante pour une… »

Il n’alla pas plus loin.

« Pour une quoi ? Une femme ? demanda Layla, provocante, soulevant ses maigres seins afin de les monter à tous

— Non, répondit Iohannes, soudain gêné.

— Ah oui, pour une lavandière ! »

Elle échappa à la poigne de Gosbert et vint à califourchon d’un homme assis à terre, lui enfouissant son visage sous sa robe tandis qu’elle lui frictionnait le dos, mimant une lavandière battant le linge. Cela déclencha une nouvelle vague de clameurs et de rires qui se termina par un « Dieu lo veult ! » tonitruant. Finissant son verre, Ernaut se leva et tapa sur le bras de Gosbert, qui était de nouveau à table, le regard versé dans un pichet.

« Grand merci de ton hospitalité, que Dieu te garde, toi et tes compagnons.

— Merci l’ami. Fais bonne route. »

Alors qu’il allait ouvrir la porte, Ernaut fit mine de se retourner pour parler à Iohannes. Il sentit un regard de braise qui s’attachait à lui et jeta un dernier coup d’œil dans la pièce. Parmi les corps avachis se tenait un buste droit, qui allait et venait en cadence. La pénombre dessinait à peine le visage maigre, mais les deux yeux brillaient d’un éclat de feu en l’implorant, en le provoquant, en le défiant. Layla ne souriait plus.

### Casal de Saint-Gilles, samedi 14 juin, fin de matinée

Le casal de Saint-Gilles était plus ancien et moins bien organisé que celui de la Mahomerie. Installé sur une éminence couronnée d’une tour, il était constitué de maisons de pierres agglutinées les unes contre les autres. L’avantage, c’était que les ruelles peu larges serpentant entre les demeures protégeaient un peu du vent chaud, râpeux, qui emplissait le ciel de poussière ocre.

Les chevaux devenus trop rétifs pour être montés suivaient avec réticence, renâclant et secouant la tête afin de se débarrasser de la gangue de poussière qui s’accrochait à leur chanfrein. Ernaut et Iohannes mirent un petit moment à trouver la maison d’Aubert, le père de Guillaume. Elle se trouvait au fond d’une petite cour, avec un poulailler et une bergerie. Comme partout, tout était clos, et seul le souffle du vent se faisait entendre.

Ils attachèrent leurs montures sous un abri à bois en partie vide et allèrent frapper à la porte. Une voix de femme les questionna avant de les laisser entrer. Ridée, courbée, le pas traînant, la vieille leur sourit à l’idée qu’elle aurait des nouvelles de son fils. Un voile de drap gris enserrait son visage de souris. On se demandait comment un gaillard comme Guillaume avait pu naître d’un corps si chétif.

L’endroit était modeste : une salle de terre battue avec des banquettes le long des murs, quelques couvertures, des coffres. Un feu cuisait doucement un pot au centre de l’endroit, apportant une maigre clarté en complément d’une lampe à huile. À côté de cette dernière, des fers autour de lui, une pierre à aiguiser à la main, un vieil homme attendait, circonspect. Habillé seulement de sa chemise, les pieds dans des savates, il avait le cheveu blanc frisé, mais rare, qui se prolongeait par une barbe courte et une moustache imposante. Bossu à force se courber, il avait les mains difformes, les doigts crochus, qui s’activaient sans cesse.

« La mère, va donc tirer un pichet pour nos invités. Ils doivent avoir grand-soif avec la poussière ! »

La vieille s’évanouit par une porte qui donnait dans une resserre en bas de quelques marches. Pendant ce temps, Aubert se leva et vint leur serrer la main, les invitant d’un geste à s’asseoir près de lui, repoussant soigneusement ses outils.

« Comme ça, vous êtes compaings avec le Guillaume ?

— Je suis du casal de la Mahomerie, confirma Iohannes. Et le frère d’Ernaut est aussi de là…

— Sacré temps pour aller sur les chemins. Vous êtes au service de quelque maître impatient ?

— Si on veut, sourit Ernaut. Nous essayons de faire bonne justice. »

Le vieil homme fit une moue amusée, marquant son scepticisme.

« Et cela vous mène sur les routes depuis la Judée jusqu’en Samarie ?

— Nous n’irons pas plus loin, nous avions juste besoin de venir ici, à Saint-Gilles.

— Vous me direz tout ça après dîner, on a bien temps. La Mahomerie n’est pas tant loin, mais on n’a pas souvent des nouvelles du gamin. Vous verrez peut-être mon dernier, il est parti voir si les bêtes supportent la tempête. J’espère qu’il sera rentré tantôt. »

Lorsque son épouse revint, il servit à boire un vin léger, coupé d’eau, qui brûlait la gorge par son acidité. Puis ils s’installèrent pour un rapide repas : une salade de pois chiches avec des oignons, du pain et de la purée d’olives, du fromage de brebis frais. Le tout arrosé de la piquette maison.

Si l’homme était disert, sa femme ne l’était guère, ne risquant quelques questions que lorsqu’il était question de Guillaume et des siens. Son univers semblait se restreindre à sa maison et sa famille. Le reste, météo, nouvelles du royaume, campagnes du roi, lui était indifférent. Elle secouait la tête en souriant aux questions de son époux sur le sujet, mais n’écoutait guère les réponses que faisait Iohannes ou Ernaut. Elle avait attrapé à un moment un petit chat au poil tigré, qu’elle caressait distraitement entre deux tâches ou deux bouchées.

Une fois le repas avalé, Aubert détacha une écharde d’une bûche et commença à se curer les dents. Ils avaient épuisé tous les sujets généraux, donné et pris des nouvelles de leurs connaissances, il était temps de passer au fond du problème.

« Alors comme ça, vous traquez un larron jusqu’au nord des monts ?

— Nous ne savons pas s’il est d’ici, mais la victime en venait, de sûr.

— Mouais, éructa Aubert. Je saurais pas bien dire d’où il était, Ogier. Il a longtemps vécu au casal, je peux pas aller contre. Son pays, personne aurait pu le dire…

— Pas un compère à vous, à ce que je vois, répliqua Ernaut.

— Il était ami des monnaies, ça oui ! »

Il fit claquer son cure-dent contre sa dernière incisive entière.

« Il avait maigre de terres, s’intéressait surtout aux pâtures, pour ses animaux de bât. Quelques pieds de vignes, une poignée d’oliviers, guère de froment à semer.

— S’il était souvent au loin, il valait mieux.

— Oh, il a toujours su y faire, à profiter des autres. Aussi, il avait son valet, le p’tit Umbert.

— Il était avec lui déjà ?

— Oh oui, ses parents sont d’ici. Moi j’ai toujours pensé que c’était pas une bonne place, mais le gamin s’y plaisait. Il payait pas trop mal, qu’ils disaient, alors moi… »

Il leva les bras au ciel, regardant son épouse qui hochait la tête en assentiment.

« Il faut vivre avec les gens si on veut les connaitre. P’t être que l’Ogier était pas mauvais maître. De toute façon, il était pas souvent là, alors le gamin faisait à son idée, de sûr ! »

Il sourit, lançant son cure-dent dans le foyer.

« Il s’était fait ennemis au sein du casal ?

— Pas que je sache. On l’aimait guère, mais de là à aller le pourfendre au loin.

— Chez nous, on le disait ami des mahométans, c’était pareil ici ? »

Aubert soupira, se réinstallant à son aise avant de répondre, les yeux rivés sur le feu végétant.

« Y en a de pleins casaux d’ici Naplouse. Plus que de villages de bons chrétiens. Mais on traîne pas avec eux. Lui, il portait les marchandies d’un lieu à l’autre, alors forcément il devait en connaître. Pour son métier.

— C’est comme ça qu’il a appris leur langue ?

— Je ne saurais dire. Je l’ai entendu la parler de temps à autre, quand des marchands ou des voyageurs passaient par ici. Il en apprenait des nouvelles de Babylone et nous les racontait. »

Aubert se leva et alla à la fenêtre, obstruée d’un vieux linge rapiécé en plus du volet. Il tira le tout et jeta un coup d’œil au-dehors.

« La tempête est calmée, enfin. Le gamin tardera pas j’espère. Vous pourrez lui demander, il a mieux connu Umbert que nous. »

Le vieux reprit sa place et recommença à passer la pierre sur le tranchant des outils. Pendant ce temps, la vieille filait de la laine, marmonnant une chanson enfantine. Ernaut s’installa un peu plus à l’aise et finit par s’assoupir. Il fut réveillé par des bêlements un peu inquiets et les cris d’un pâtre. Peu après, la porte laissa le passage à une vive silhouette recouverte de poussière. Refermant rapidement, le jeune homme salua l’assemblée et se secoua un peu.

« J’ai ramené les bêtes, le père. Si ça dure, c’est pas bon pour elles. On a plein de foin, je préfère les savoir à l’abri.

— C’est toi qui vois, gamin, tu es le chef du foyer, ou presque… »

La remarque fit apparaître un sourire sur le visage de la vieille femme qui s’activait déjà pour porter son repas au dernier arrivant. Une fois débarrassé de l’ocre qui le recouvrait, Ernaut put découvrir un visage d’une vingtaine d’années, pas très gracieux, mais avenant, les yeux vifs et le nez fort. Sa voix grave était faite pour se faire entendre dans les champs et il semblait que s’il devait se mettre à hurler on l’entendrait jusqu’à Jérusalem. En quelques mots, Aubert lui présenta les invités et lui indiqua qu’ils auraient aimé en savoir plus sur Ogier.

« Je l’ai pas trop connu lui, plutôt Umbert, qui traînait avec nous voilà quelques années. Toujours dans nos braies le gamin !

— Il parlait de son maître ?

— Pas tant. On avait autre chose en tête quand on se voyait. »

Il fit un clin d’œil égrillard.

« C’était un des plus jeunots, forcément toujours à la traîne. Au service d’Ogier, il était traité comme un homme, alors ça a dû lui plaire.

— Il aimait bien œuvrer au service d’Ogier ?

— J’irais pas jusqu’à dire ça, mais vu que le maître était rarement là, il œuvrait à son envie, tel un chef de son feu. Rien était à lui, mais il faisait comme si. Sans compter que son père l’avait toujours tabassé. »

Il fit une grimace, indiquant clairement que les punitions allaient au-delà de ce qu’il estimait nécessaire.

« Il a reçu moult coups ?

— Plus qu’à son tour. D’autant que c’est pas le mauvais gamin. Pourtant ça tombait dru comme une averse de printemps. À moins d’être une vraie bête, Ogier n’aurait pu le malmener aussi sévèrement.

— Après ses absences, le retour de son maître ne lui posait pas souci ?

— Non, aucun. Il était le même. Tant que le travail était fait, Ogier ne semblait guère méchant. D’autant qu’Umbert est un gars sérieux, pas le genre à faire mal le boulot. »

Ernaut laissa le jeune homme finir d’avaler son repas tranquille, méditant sur les maigres informations qu’il arrivait à glaner. Il étudiait Iohannes, également plongé dans ses réflexions quand une remarque du jeune homme le fit sortir de sa torpeur.

« De toute façon, si la place lui plaisait pas, il serait pas allé à la Mahomerie. Il aurait pu trouver labeur ici s’il avait voulu.

— Il était heureux de partir ?

— Je dirais pas ça, il semblait bien triste. Dame, de quitter là où on a grandi, ça se comprend ! Mais il était plutôt satisfait de suivre Ogier. Il semblait avoir bonne estime de son maître. »

La remarque fit naître un ricanement moqueur d’Aubert, qui regardait l’assemblée d’un air amusé. Il demeura muet et se pencha de nouveau sur sa tâche, estimant peut-être que cela résumait suffisamment son avis. Le gamin reposa son écuelle et s’essuya la bouche de la manche.

« Vous voudriez peut-être aller voir quelques voisins à lui ? Aucun n’est parti. Je peux vous mener à son ancien manse.

— Bonne idée, abonda le vieil homme. Tu rendras sa hache au Gascon par la même occasion. »

Il tendit l’outil, aiguisé de neuf, à son fils.

« Je vais vérifier si nos chevaux vont bien, avant cela. J’en ai pour le temps de deux-trois pater.

— Tu n’as qu’à venir me chercher quand tu as fini !

— Je t’accompagne, Ernaut, intervint Iohannes. Le cheval n’est pas à moi, j’ai intérêt à en prendre grand soin. »

Les deux compagnons sortirent, se retrouvant dans un paysage nimbé d’un blanc crayeux, sale. Le ciel laiteux, épais, écrasait leurs épaules et l’air étouffant asséchait leur bouche. En quelques pas, ils se trouvèrent en sueur. En s’essuyant le front, Ernaut y attacha une bande de poussière jaune qui y traçait comme une peinture. Il parla à Iohannes tandis qu’ils nettoyaient au mieux la tête de leurs chevaux.

« Il faudra qu’on fasse parler plus avant le gamin. Pourquoi ce benêt n’a rien dit ?

— S’il était vraiment attaché à Ogier, cela se comprend. Il est peut-être d’autant plus triste qu’il semble avoir été un des seuls à l’apprécier.

— Il croit Godefroy coupable ?

— Ou il s’en moque, seulement accablé de chagrin. »

### Casal de Saint-Gilles, samedi 14 juin, midi

La maison occupée précédemment par Ogier était en bas du village, tournée vers le nord, ouvrant sur une des rues principales qui sillonnaient l’agglomération. Un long appentis charpenté courait sur sa face externe où du foin, de la paille et du bois étaient entassés. Une auge en pierre avait dû servir à abreuver les bêtes du temps où le transporteur demeurait là.

Un jeune homme de grande taille extrayait des faisceaux de paille en jurant et pestant entre ses dents. Il portait une chemise parée de nombreuses pièces ainsi que des chausses sans pied, assez lâches, de couleur claire indistincte. Lorsqu’il entendit des voix, il se retourna, les sourcils écrasant son regard méfiant. Son nez, fin et brisé, semblait ridicule au-dessus de sa bouche aux lèvres épaisses. Les cheveux noirs, souples, étaient tirés sur les oreilles et lui tombaient dans le cou. Il se gratta la gorge, circonspect.

Ernaut le salua, suivi de Iohannes, et le fils d’Aubert le présenta : Pedro, qu’on appelait le Catalan. À l’évocation de son nom, un sourire apparut et son abord si austère s’illumina bien vite malgré un air carnassier. Il s’approcha et serra vigoureusement la main aux arrivants, sans pour autant lâcher sa paille.

« Pedro a repris le manse d’Ogier. Je vous laisse, j’ai encore mille tâches à faire ce jour. »

Ernaut et Iohannes remercièrent leur guide et l’accompagnèrent du regard un instant, avant qu’il ne disparaisse dans une courbe de la rue.

« Vous êtes compaing de l’ancien d’ici ? risqua Pedro.

— Pas du tout, nous cherchons à en savoir plus à son sujet. On l’a retrouvé meurtri en son nouvel hostel.

— Madre de Dios ! échappa le jeune homme, se signant avec vigueur. Vous voulez le venger ?

— Déjà savoir qui aurait pu le meurtrir.

— Vous croyez que c’est homme d’ici ? »

Ernaut haussa les épaules, soupirant. Iohannes jeta un coup d’œil autour d’eux.

« Vous n’avez rien remarqué de spécial quand vous avez repris l’endroit ?

— No. C’était propre et vide. Il avait laissé un meuble ou deux, mais cassés.

— Rien dans la maison qui vous a intrigué ?

— No. Il y a la pièce et la réserve. Guère moyen d’y oublier des choses. Je suis obligé d’entasser des affaires ici, où il tenait ses bêtes. Mais avec ce sable… »

Il tapa sur la paille, qui lâcha un nuage de poussière jaunâtre.

« Il va me falloir aller tout rincer après la tresse. Madre de Dios, je n’ai jamais vu si male vent.

— Vous fabriquez quoi de cette paille ? demanda Ernaut, intrigué.

— Des scourtins, pour presser la pulpe d’olive et en tirer l’huile. Notre terre ne nourrit guère une famille, il faut bien trouver autre chose.

— Vous exploitez les terres d’Ogier ? »

Pedro acquiesça.

« Pas de raison que ça change. Mais si ça pouvait suffire à un homme seul, pour une famille, c’est bien maigre. Seulement, je n’avais guère le choix, il me fallait trouver une terre, avec l’enfant qui vient. »

Ernaut sourit au jeune homme, en passe de devenir chef de famille. Il se prit à envier le Catalan, qui continuait son récit.

« D’aucune façon, je ne resterai pas là, c’est bien trop modeste. Juste en attendant de dénicher le bel endroit où je pourrai semer assez et récolter des olives. C’est que je m’y connais dans les olives, vous savez… »

Puis il s’épancha longuement sur sa longue expérience, lors des voyages qui l’avaient mené finalement dans les montagnes de Judée. Il avait cherché un temps à se faire marin, mais il était homme de terre, plus à l’aise dans les restanques arides qu’au milieu des vagues. Il avait rencontré lors de ses voyages celle qui portait leur enfant, une fille venue de Grèce.

Tandis qu’il parlait de sa vie, il s’animait, remuait les mains, échappant des brins de paille, et son regard brillait de mille feux. Il avait des projets en quantité et du courage à revendre. Cette première terre serait pour lui une marche, un tremplin, d’où il s’élancerait dans la vie.

« L’ancien occupant du manse a fait belle fortune dit-on, j’espère avancer en semblable chemin.

— Si la terre est si maigre, comment cela se peut ?

— Je ne sais. Elle porte peut-être chance. Il a trouvé de quoi exploiter domaine de belle taille après celui-ci et il est parti fortune faite. »

Il baissa la voix.

« On dit qu’il ne payait pas toutes ses redevances, trichait sur les champarts ou les quantités récoltées.

— Il faut bien de l’astuce pour faire d’une buse un faucon !

— Oh, il n’était pas avare de ses combines. Personne n’ose le dire, mais il savait bien comment tricher sur une mesure ou déplacer peu à peu une borne. »

Iohannes prit un air offusqué, presque indigné.

« C’est là bien dangereuses pratiques.

— Il y en a tant qui le font ! De toute façon l’intendant triche à rebours, comme ça l’équilibre est tenu. Un coup par ci et un coup par là font que la mesure est juste au final.

— Il aurait pu ainsi amasser sa cliquaille, en aidant les autres à tromper ?

— Certes pas, c’est là simple conseil discret entre voisins, que personne n’admet. »

Ernaut se frotta le menton, indécis. Les terres récupérées par Ogier étaient de bonne taille et auraient demandé une large famille pour l’exploiter ou des journaliers qu’il faudrait payer. Cela demandait une mise de fonds assez importante et pourtant cette précédente propriété était loin de pouvoir rendre riche.

« À quelle date est-il parti pour la Mahomerie?

— En ce début d’année, répondit Pedro.

— C’est ce qui me fait dire qu’il a pu trouver fortune ailleurs, lors de ses voyages. À l’approche de la Noël, le roi et le comte Thierry étaient en campagne vers le Nord.

— Il était parti avec eux ?

— Aucune idée, mais il y a toujours moyen de grappiller lorsque des soldats sont dans les parages. Il a de certes saisi belle occasion à un moment et a fortune faite bien aisément. »

Il semblait parler d’expérience et Ernaut savait qu’il avait raison. Les pillages qui accompagnaient les armées étaient l’occasion d’amasser rapidement une fortune. Fortune qui ne profitait jamais aux guerriers et soldats, mais aux chefs et aux marchands qui gravitaient autour. Si l’audace et la violence permettaient tous les excès, elles ne rendaient que rarement les hommes riches.

« Le fèvre, à côté, l’a bien connu vu qu’il ferrait ses bêtes. Il y a aussi le colporteur qui est passé, avec qui il semblait bien compaing.

— Un marchand de passage ?

— Oui, il a fait halte ici alors que nous venions de prendre le manse. Il a été fort surpris de voir qu’Ogier était parti. Il a dû le retrouver par la suite, car il n’est jamais reparu.

— Vous avez souvenance de son nom, de son allure ?

— Comme tous les colporteurs, il était gris de la poussière des chemins, long tel un jour sans pain. Il s’appelle… »

Pedro chercha dans sa mémoire un moment, hésitant sur plusieurs possibilités jusqu’à ce qu’il arrête son choix :

« Fier-pieds, ou quelque chose du genre, le Fripier peut-être. En tout cas, il se nommait Clément. »

Ernaut se tourna vers Iohannes, qui était en train de hocher la tête.

« Le nom me parle, en effet. De toute façon, il doit faire chemin dans les environs et a dû passer à la Mahomerie. Même si c’était pour visiter son compère, il n’aura pas résisté à l’envie d’ouvrir sa balle, de faire commerce.

— Il faudra que j’en parle à mes compaings de la sergenterie. Il a dû passer sous les portes de la cité et s’acquitter du péage. »

Pedro se passa la langue sur les lèvres, soudain ennuyé.

« Je n’ai pas voulu dire qu’il pourrait l’avoir meurtri. Il paraissait honnête compère, pas le genre à… »

Il ne finit pas sa phrase, se mordant la lèvre, comme hésitant, cherchant à se convaincre lui-même. Ernaut comprit immédiatement.

« Nous ne savons rien de l’un comme de l’autre. Étaient-ils fort liés, ou juste cheminaient-ils de pair sur les routes ? Quant à savoir s’il y avait entre eux deux sujet à dispute… Clément saura apaiser nos inquiétudes. »

Il ajouta, un sourire sévère sur le visage :

« Peut-être même bien malgré lui… »

Pedro paraissait ennuyé d’avoir attiré des ennuis à un inconnu, par sa faconde et son plaisir de parler. Après tout, ces deux-là étaient des inconnus avec qui il n’avait jamais rompu le pain ou trinqué une seule fois. Il s’abandonna donc à un silence repentant, tirant sur ses brins de paille pour les égaliser, attendant qu’ils mettent d’eux-mêmes fin à l’entretien. Ernaut le comprit d’instinct et remercia Pedro de son aide avec chaleur et lui souhaita bonne chance pour tous ses projets, et en particulier la venue de son premier-né. La mention de cet événement heureux mit un peu de baume au cœur du Catalan, qui leur fit promettre de venir boire de son vin une prochaine fois qu’ils passeraient au casal de Saint-Gilles.

En s’éloignant, Ernaut se rapprocha de Iohannes et lui parla à mi-voix.

« M’est avis que cet Ogier était tordu comme cep de vigne. J’en connais de ces larrons qui finassent aux portes.

— Tu penses que sa fortune venait de deshonnête commerce ?

— Nous avons un muletier qui voyageait au loin, ami avec un pieds poudreux, de ceux qui aiment à passer outre les péages. Cela nous donne tout ce qu’il faut pour faire bon trafic.

— Trafic de quoi ?

— Je ne sais. Il me faudra aller à la cité ce soir, demander à des miens amis. »

Iohannes renifla, gêné. Cela intrigua Ernaut, qui, d’un regard, s’enquit de ce que son compagnon avait sur le cœur.

« Je sais qu’en tant que sergent du roi, tu ne peux avoir quelque pardon pour pareils fraudeurs, mais…

— Mais cela n’en fait pas murdriers, c’est ce que tu apenses ?

— Voilà ! Rogner sur la dîme, alléger les paniers du champart, tout le monde le fait. Comme le disait Pedro, l’intendant le sait et va à l’encontre. Pas de quoi aller trancher quelqu’un.

— Tu parles de vrai, Iohannes. Pourtant on a éventré Ogier. Et si larrons s’entendent en foire, je sais qu’ils ne sont guère enclins à s’épauler lorsqu’on leur court sus. Ou que l’un d’eux se croit floué. Sa belle fortune, il l’avait peut-être volée à ses comparses.

— Comment les retrouver ?

— Nul besoin. Si je peux faire entendre à Aymeric qu’il risque sa vie pour larron menteur et sournois, il se dédira peut-être. Peu me chaut qu’Ogier obtienne justice, je veux juste que mon frère se sorte de ce guêpier. »

### Casal de Saint-Gilles, samedi 14 juin, début d’après-midi

Un marteau heurtant l’enclume résonnait non loin de là, depuis un bâtiment auquel on accédait en descendant une courte volée de marches. La porte était poussée, mais pas fermée, et Ernaut frappa sur le panneau avant de la franchir. Leur entrée provoqua l’arrêt des coups et trois paires d’yeux pivotèrent vers eux.

Accroupi au sol, devant un billot de bois accueillant une enclume carrée, le forgeron, son long crâne rasé et la barbe courte, tenait son marteau en l’air, mais avait déjà replongé machinalement sa pièce dans le petit feu creusé à même le sol. Il était entouré de pinces et de marteaux, disposés sur une pièce de cuir. Entre les deux soufflets en accordéon, en train de les actionner alternativement, un garçon d’à peine dix ans les dévisageait, curieux et légèrement apeuré devant la taille d’Ernaut. Enfin, au fond, assis sur un coffre, un vieil homme à la barbe fleurie les regardait avec une curiosité mêlée de méfiance. L’endroit mêlait la fumée de charbon et la senteur saline, évoquant le sang, du métal chauffé.

Iohannes sourit, salua poliment et allait faire les présentations lorsqu’Ernaut le coupa, soucieux de s’annoncer à son envie.

« Nous venons là sur les conseils de Pedro, il disait que vous avez connu Ogier.

— Peut-être, concéda l’artisan, de mauvaise grâce.

— C’est qu’il a rendu son âme à Dieu pas plus tard que mardi.

— On l’a su.

— Les nouvelles vont vite.

— Beaucoup de voyageurs par ici, on n’est pas si loin. »

Le forgeron ne semblait guère décidé à faciliter le dialogue. Il empoigna une paire de pinces et sortit sa pièce du feu, rougeoyante, jaune à sa partie la plus fine. En quelques coups ajustés, il en avait fait une patte avec plusieurs angles. Il la replongea parmi les braises et entreprit de trouver un poinçon, amenant à lui une petite bille de bois constellée de trous brûlés. Personne ne parlait. Lorsqu’il sortit de nouveau son ouvrage, il dirigea son apprenti d’un grognement, lui abandonnant la pince. Puis il se dépêcha de percer deux lumières au travers du métal encore mou. Enfin, il jeta le résultat dans un pot au col cassé, créant un bouillonnement bref et un grésillement. Il daigna alors de nouveau s’intéresser à ses visiteurs.

« Vous avez usage de mes services ?

— Pas en tant que fèvre, non, répondit Ernaut. J’ai besoin d’en apprendre plus sur Ogier pour mon frère.

— En quoi ça vous concerne ? Il est d’ici vot’ frère ?

— Non, mais il risque jugement par bataille, car on accuse son beau-père du forfait. »

La réponse sembla laisser l’homme indifférent. Impassible, il jouait avec ses outils pour en réarranger l’organisation.

« J’l’ai pas si bien connu que ça…

— Vous avez été voisins, vous sauriez peut-être qui a pu le meurtrir.

— Pour ça oui, il n’y a nul doute… »

Ernaut se figea un instant, ne sachant pas comment prendre la nouvelle. Il s’apprêtait à chercher de nouveaux arguments et on lui servait sur un plateau ce après quoi il courait depuis des jours. Il en bafouilla un peu, ce qui fit sourire le vieux au fond.

« Vous connaissez son…

— Le connaître, certes pas, mais j’ai assurance de qui a pu faire le coup.

— C’est qui, alors ?

— Un de ces païens, là. Il les fréquentait moult, je lui en avais fait assez reproche. »

Disant cela, il semblait défier Iohannes du regard, semblant le trouver trop levantin à son goût. Il remit une poignée de charbon sur le feu et l’aplatit à l’aide d’une petite pelle.

« Il leur a toujours fait trop confiance. Un jour, il m’a ramené une bête ferrée chez l’un d’eux, dans les collines de Samarie. Un vrai travail de singe ! J’en ai bavé pour remettre le pied en état. Mais non, il s’entêtait !

— Il était ami avec des mahométans ?

— J’dirais pas ça quand même, c’était bon chrétien et il savait qu’on doit pas avoir pareil commerce avec eux. Il en croisait un peu trop souvent, comme s’il n’y voyait pas d’offense à Dieu.

— Peut-être pour ses affaires.

— De certes, je ne vois guère d’autre motif. »

Il émit plusieurs forts bruits de succion, époussetant les battitures de son enclume d’une main machinale. Il semblait remâcher quelque vieille haine qui hésitait à prendre forme, à s’exprimer clairement.

« Des païens, y’en a bien trop dans l’coin à mon goût. Difficile de ne pas s’arrêter parmi un de leurs villages d’ici Naplouse. Je voudrais pas médire d’un mort, comprenez… »

Retenant un sourire, Ernaut se dit que ce genre d’introduction indiquait immanquablement que ce qui allait suivre était justement ce dont on se défendait. Le forgeron baissa la voix.

« On en voit donc, bien obligé. Certaines de leurs corvées se font le long des chemins et on les aperçoit depuis nos champs, à œuvrer le bien de leur maître. Mais on se mêle pas à *eux*, vous voyez… »

Il avait appuyé fortement sur le « Eux » comme s’il parlait de choses répugnantes, de créatures mythologiques des temps passés. Pourtant, il désignait ses voisins, les fermiers des alentours, des gens qu’il était amené à croiser régulièrement. Et cela lui soulevait visiblement le cœur, lui enrageait l’esprit, créait en lui toute l’amertume que sa bile déversait à chaque parole.

Ernaut ressentit le changement d’attitude de Iohannes, sans qu’il puisse être sûr de ce que c’était : colère ou dépit. Pour lui, c’était égal. Chaque homme pouvait se choisir un ennemi, c’était son affaire. Il avait déjà bien assez à faire avec ses problèmes. Il abonda donc dans le sens de l’homme, veillant à ne pas laisser percevoir l’ironie de ses paroles.

« On m’a dit qu’il parlait leur langue, même.

— Cul-Dieu, ça oui. Il s’en vantait fort. Je sais pas comment il l’avait sue, mais on aurait dit qu’il était un des leurs, parfois. Je peux pas dire que ça me plaisait.

— Il est bien normal que les hommes se parlent, non ? rétorqua Iohannes à mi-voix.

— M’est avis que c’est au valet d’apprendre la langue du maître… »

Le forgeron redressa le buste, les mains sur les genoux.

« On s’est chicanés plus d’une fois à ce sujet, je l’avoue maintenant. Je lui disais que parler le païen, c’était pas bon pour gagner le Paradis. Ça le faisait rire, alors on s’en amusait ensemble. Mais maintenant qu’il a rendu son âme, il va devoir en rendre compte à Dieu et tous les saints. »

Il sortit la pièce de métal du baquet d’eau et la montra au vieil homme, qui n’avait pas encore bougé depuis leur entrée. Il l’examina à la faible lueur venue de la porte, hochant la tête de contentement. Il marmonna quelques mots inintelligibles, mais que le forgeron sembla comprendre. Il attrapa un autre morceau de métal et l’installa dans le foyer, indiquant d’un regard à son apprenti de lancer la chauffe avant de se tourner de nouveau vers Ernaut.

« J’serais seigneur patriarche là-bas, je demanderais au roi de convertir tous ces païens. Et ceux qui refusent… »

Il se passa l’index sous la gorge, lançant quelques gloussements vicieux.

« Ça ferait plus de terres pour les bons chrétiens, et ça éviterait des histoires comme ça…

— Des histoires comme ça ?

— Bein oui, c’est forcément un de ces démons qu’aura planté l’Ogier. Ça lui a porté malheur de trop les côtoyer. »

Il secoua la tête en désaccord.

« J’lui avais dit qu’il ne fallait jamais donner sa confiance à un païen. Jamais. »

Il empoigna une pince et plaça le métal chaud sur l’enclume, commençant à écraser le fer de gestes sûrs. Ernaut le remercia et salua les présents rapidement. Il retrouva la rue avec soulagement, même si la chaleur y était encore oppressante et la lumière blafarde irréelle. Il échangea un sourire dépité avec Iohannes. L’interprète cligna des yeux, morose.

Ils s’avancèrent vers la demeure d’Aubert pour récupérer leurs montures. Ernaut repensa à ce qu’ils avaient découvert, maigre moisson eu égard à leurs efforts. Mais il devinait plus nettement la voie qu’ils devaient suivre désormais.

« Il va nous falloir retourner voir le vieux shayk de Salomé. Si Ogier fréquentait tant que ça les mahométans ici, il n’y a nulle raison qu’il ait arrêté…

— Tu crois vraiment à ce qu’a dit le fèvre ?

— C’est la haine qui guide ses paroles et je veux me fier à ma raison plutôt. Pourtant qu’il dise cela rageusement ne signifie pas qu’il se trompe. Si Ogier faisait affaires avec des gens ici, il a peut-être voulu faire pareil à Salomé et cela se sera mal passé.

— Tu penses toujours à quelque trafic ?

— Je ne saurais dire. Il me semble bien malaisé à des serfs de commercer quoi que ce soit. Si c’est le cas, cela ne pourrait se faire sans que le ra’is ne le sache. Si besoin je préfèrerais en parler de prime au shayk. »

Iohannes ne put retenir un sourire, étonné.

« Tu comprends vite !

— Pour un Ifranjs tu veux dire ? s’amusa le géant. Je me fie à mon instinct. C’est peut-être un mahométan, mais il semble homme de droiture, à sa façon. Il pourrait ne pas approuver certaines choses, m’en confirmer l’existence s’il n’a voulu me l’apprendre. »

Les deux hommes parvinrent à leurs chevaux et les préparèrent au voyage. Aubert vint les saluer tandis qu’ils se préparaient. Il les accompagna jusqu’à la sortie, ne les abandonnant qu’après s’être fait confirmer suffisamment qu’ils passeraient de leurs nouvelles à son fils Guillaume. Le vent qui se levait de nouveau ne semblait guère ternir son enthousiasme et il les bénit bien des fois avant de disparaître dans la brume de cendre jaune. Ernaut préparait son foulard pour se protéger de nouveau le visage, mais il interpella avant cela son compagnon.

« Je ne m’arrêterai pas à la Mahomerie. Pourrais-tu narrer à mon frère ce que nous avons découvert ?

— Bien sûr ! Où comptes-tu aller ?

— À la cité. Il me faut débusquer ce colporteur, Clément. Outre, j’ai une chose qui me paraît bien étrange dans tout cela, et il me faut en savoir plus sur d’autres aspects de la vie d’Ogier.

— Tu ne crois pas la meurtrerie liée à son commerce ? »

Ernaut fit la moue.

« Ce qui me pose souci, c’est qu’on l’a châtré jusqu’à malemort. Bien terrible châtiment ce me semble. J’inclinerais à y voir histoire de femme malgré tout.

— Qui te mène à Jérusalem ?

— C’est là qu’il devait aller aux bains, pour rencontrer putains. L’une d’elles saura peut-être nous mettre sur la voie. »

Iohannes semblait à demi-convaincu par l’hypothèse et il restait, figé, cherchant vraisemblablement un argument contraire. Alors Ernaut dénoua son foulard qu’il venait d’attacher, et ajouta :

« Et demain, je dois aller à la messe, avec celle qui sera mon épouse. Il me faut la prévenir, elle et les siens, de ce qui se passe. Ils seront bientôt de la famille. »

Disant cela, le jeune homme sentit un pincement au cœur. Il ne voyait pas Libourc autant qu’il l’aurait souhaité et ne savait pas toujours comment se comporter avec ses parents. Sanson était généralement accueillant et bonhomme, mais sa femme se montrait plus réservée. Ernaut espérait qu’ils ne seraient pas trop contrariés d’apprendre que leur famille se trouvait impliquée dans un jugement par bataille. De toute façon, il avait besoin de la présence de la jeune femme. Son corps et son âme se languissaient d’elle.

### Jérusalem, samedi 14 juin, fin d’après-midi

Lorsqu’Ernaut parvint enfin dans la cité, il était recouvert d’une fine poussière qui collait à ses vêtements, le rendant semblable à une statue. Il n’aspirait qu’à un bain pour se débarrasser de cette gangue qui le faisait transpirer, plus encore que la chaleur étouffante. Il avait déposé sa monture à l’écurie du loueur, son ami Abdul Yasu. Après avoir salué ses compagnons de faction à la porte occidentale, il se dirigea vers le palais où il espérait trouver Eudes.

La rue de David était calme, la plupart des commerces avaient fermé et même les marchés couverts n’étaient guère animés. Le khamaseen envahissait tout, se faufilait par le moindre interstice et y déposait l’ocre qui colorait le ciel. Les vendeurs tentaient donc de préserver leurs marchandises, quitte à ne pas ouvrir quelques jours. Des voyageurs, la bouche et le nez grossièrement emmitouflés, déambulaient, hagards parmi la cité. Ernaut sourit, se demandant quelle impression garderaient ces pèlerins qui découvraient Jérusalem pour la première fois sous une tempête de poussière.

Il tourna sur sa gauche dans la rue du patriarche, rejoignant les abords du Saint-Sépulcre, là où le palais royal se lovait. On disait qu’il était envisagé de construire un bâtiment plus pratique, vers l’entrée occidentale de la ville, à la porte de David. Cela permettrait de s’appuyer sur les anciennes et imposantes fortifications qui s’y tenaient. En outre, comme cela constituait la porte principale, le pouvoir royal pourrait s’y affirmer de façon éclatante aux nouveaux arrivants.

Il pénétra dans le palais par une des portes annexes, saluant le planton avec lequel il avait effectué pas mal de rondes. À l’intérieur, l’activité était réduite, un calme inhabituel régnait. Là aussi, on fonctionnait au ralenti en raison de la tempête. Il quitta le grand passage et entra dans une pièce où ils avaient coutume de s’installer pour les tâches administratives.

C’était une vaste salle voûtée d’arêtes, dont les faisceaux retombaient sur trois épaisses colonnes. Ses fenêtres donnaient sur une cour intérieure, apportant suffisamment de lumière aux scribes et notaires qui enregistraient, additionnaient et inventoriaient. Les tâches des sergents étaient essentiellement le contrôle des poids et mesures, l’encadrement du commerce et la perception des différents prélèvements. S’ils arboraient une allure toute martiale et accompagnaient parfois l’armée lors de ses déplacements, ils relevaient avant tout de l’administration fiscale, de la Secrète du roi. Le guet et la garde de la cité n’étaient au final que des activités annexes.

L’endroit était calme. Sur une des tables, ornée d’un immense tableau ressemblant à un échiquier, deux hommes plaçaient des jetons tout en pointant des listes. À côté d’eux, le mathessep Ucs de Monthels vérifiait le contenu d’un coffret où s’entassaient des bourses de soie scellées. Chacune d’entre elles contenait une valeur définie de pièces dont le montant était garanti par le sceau, évitant de fastidieux recomptage de piles de monnaies. Il aperçut l’imposante silhouette qui venait d’entrer et dévisagea Ernaut un instant avant de le saluer en retour d’un hochement de tête.

Sur une table au fond de l’endroit, le jeune homme aperçut ce qu’il cherchait : une touffe de cheveux roux, penchée au-dessus d’un tas de pièces à trier. Il fallait les regrouper par origine, valeur et usure, pour ensuite en faciliter l’usage. Tâche fastidieuse, ingrate, qui était l’objet de nombreuses plaisanteries parmi la sergenterie et l’enjeu fréquent des parties de dés afin de refiler la corvée. Eudes avait donc perdu. Son visage fin s’éclaira lorsqu’il reconnut la silhouette imposante qui s’avançait vers lui. Il ne quitta pas son banc et continua à classer avec soin, mais afficha un sourire accueillant.

« Je suis bien aise de te voir, ami. On m’a conté tes ennuis ! J’espère que cela se résout…

— Guère ! répondit Ernaut en se laissant tomber sur le banc, un peu las. J’arrive de Saint-Gilles, et la tempête m’a complètement essoré.

— Tu y as trouvé ce que tu espérais ?

— Pas vraiment. J’avance, mais tel un aveugle, et je n’ai pas de choses fort assurées pour convaincre l’autre batailleur. »

Eudes hocha la tête.

« Tant que le khamaseen soufflera, le jugement ne saura se faire. Je crains que tu n’aies à poursuivre ta traque au parmi de la poussière.

— Cela me va. Ce sera comme le souffle de Dieu qui emporte les mécréants et récompense les Justes. »

Ernaut sourit, amusé de sa propre audace. Il tapa amicalement sur l’épaule de son ami, autant pour se donner du courage que pour remercier de ce soutien. Il se rendit alors compte que le mathessep semblait d’un regard l’inviter à approcher.

« Je reviens, compère, maître Ucs veut me parler. »

Comme toujours, le mathessep était habillé avec soin et la poussière ne s’était que peu accrochée à sa belle cotte de laine couleur brique. Le cheveu propre, la barbe rasée, il inspirait l’autorité naturellement et, bien qu’il lui rende plus d’une tête, Ernaut était toujours impressionné par l’homme. Il lui faisait l’effet d’un lion au repos, d’un sanglier embusqué capable de tout dévaster pour peu qu’on ait pris le risque de l’énerver. Pourtant il ne l’avait jamais vu en colère, à peine réprimander vertement quelques hommes.

« On dirait coursier revenu de la princée d’Antioche ! Quelle allure !

— Je ne viens pas de si loin, seulement des abords de Naplouse.

— Je l’ai appris. J’espère que tu sauras assister ton frère comme tu l’espères. »

Il releva la tête de son ouvrage et sourit sans chaleur, mais son regard était franc.

« J’ai entendu aussi que tu allais par les terres du sire de Retest.

— Oïl, maître Ucs. J’ai eu son accord pour cela. Il est possible que la meurtrerie soit le fait d’un mahométan.

— Sois bien prudent, Ernaut, le sire de Retest n’est pas homme facile.

— J’ai cru voir ça, oui !

— Je suis fort sérieux. Brandir la lance et mener le conroi au parmi de l’ennemi, voilà son plaisir, et il est aussi retors qu’un Grec. »

Ernaut comprit que son supérieur le mettait en garde et il adopta une attitude plus respectueuse, reconnaissant de cette attention.

« Il va me falloir de nouvel me rendre à Salomé, pourtant. La piste mène là-bas et ses serfs ne m’ont pas tout narré la précédente fois.

— Fais attention alors. Le sire Robert n’aime pas qu’on fouine, quand bien même il semble détourner le regard. C’est un homme impitoyable.

— Il préférerait protéger un de ses serfs que voir la justice triompher ?

— Cette dernière ne l’intéresse guère si elle ne sert sa bourse ou ses intérêts. Si tu pistes trop près d’un ours… »

Il laissa sa dernière phrase en suspens, souriant mystérieusement. Puis il tendit la main vers un tas de documents, afin de marquer la fin de l’entretien. Ernaut le remercia de ses conseils et prit congé, retournant vers Eudes qui rangeait ses piles en cochant des listes sur des tablettes de cire. D’un regard il s’enquit de ce qui c’était dit.

« Rien d’important. Il m’a mis en garde contre le seigneur d’un des casaux où je recherche le murdrier.

— Tu as bon espoir de le débusquer alors ? »

Ernaut fit la moue.

« Il me faut désormais remonter plusieurs sentes à la fois. Tu pourrais peut-être me porter aide.

— Tout ce que tu veux ! »

Ernaut hésita à parler des prostituées. Eudes était un homme marié, fidèle à ce qu’il en savait. Jamais il ne s’était joint à eux pour aller faire visite à un bordel ou à des bains aux mœurs licencieuses. Il pouvait se garder cette partie de l’enquête pour lui-même, sans en parler à son ami. Il demeurait la question du colporteur, qui pouvait être centrale.

« La victime était apparemment liée à un pieds poudreux du nom de Clément Fier-Pieds, tu le connais ?

— Ça ne me dit rien. Mais je peux faire passer le mot.

— Le mieux serait pour moi de le rencontrer, de pouvoir l’interroger, en fait.

— En ce cas, je peux te faire savoir quand nous l’aurons attrapé. »

Ernaut fronça les sourcils, hésitant un instant à causer des ennuis à quelqu’un qui n’avait peut-être rien à voir dans tout cela. Il haussa imperceptiblement les épaules. Son frère passait avant ces considérations.

« Je ne sais où je serai dans les jours à venir. Je vais profiter de la tempête pour fouiner où il le faut. Je viendrai en personne m’enquérir de lui quand je le pourrai. Gardez juste un œil sur sa personne.

— Tu repars dès ce soir ?

— Non, je suis épuisé par ma course de la journée. Outre, j’ai quelque chasse à faire en la cité.

— Tu veux de l’aide ? »

Ernaut sourit, mais secoua la tête en dénégation.

« Rien que je ne puisse faire seul, je pense.

— Tu es certain ? Je ne suis pas de garde cette nuit. »

Le jeune homme hésita à se dévoiler, puis finalement s’esclaffa.

« Je pense affronter quelques jouvencelles… »

Eudes sembla ne pas comprendre au début puis son visage s’éclaira, devenant goguenard.

« Je vois ! Tu y vas pour… »

Ernaut s’amusa de la question et laissa sa réponse en suspens, jouant du doigt avec une des piles, souriant malicieusement. Puis il fixa son ami.

« Le pauvre gars meurtri venait sûrement dans quelque établissement de la Cité. Certaines choses me font croire à une histoire de femme, je veux savoir ce que celles qui l’ont connu en pensent. »

Il regarda sa tenue poussiéreuse, avec un dépit peu naturel.

« De toute façon, il me faut bien me rendre aux bains, j’en ai fort besoin.

— Je comprends ça. Et si quelqu’un s’offre de te gratter la couenne, tu ne saurais lui en vouloir de te prêter main secourable.

— Ça…

— Je m’occuperai donc de ton marchand pendant ce temps. Il est fort possible que nous allions boire et jouer un peu ce soir. J’en parlerai à Droart. »

Ernaut se mordit la lèvre. Il avait croisé leur ami commun en venant et s’était déjà entretenu de ses projets avec lui. Lui aussi était marié, mais ne dédaignait pas de faire visite à certains établissements en compagnie d’Ernaut. Il disait en riant qu’il n’y avait nul mal à boire de plusieurs vins, que cela ne causait aucun tort. Il avait donc immédiatement proposé à Ernaut de l’accompagner, connaissant fort bien plusieurs endroits. Eudes fronça les sourcils et perçut la gêne chez son ami.

« Tu as un autre souci ?

— Certes non. Mais j’encroie que Droart ne viendra pas faire rouler les dés ce soir. »

Eudes sembla interloqué, soudain inquiet.

« Je l’ai croisé tantôt, aux abords de la porte de David, en arrivant. Je lui ai exposé mes soucis. »

Le sergent pouffa.

« Je vois ! Il ne s’est pas senti le front de te laisser aller en périlleux endroit sans quelqu’un pour te tenir l’étrier !

— Je dois passer chez lui après souper, en effet.

— Il est toujours bon d’avoir un ami sur qui compter pour dangereuses missions ! s’amusa Eudes.

— C’est vraiment aux fins d’en savoir plus sur Ogier que je fais ça. Mon frère risque sa vie.

— Je ne doute point de tes intentions, mon ami. Mais je connais Droart…

— Et moi je connais son épouse, répliqua Ernaut. J’aurais la même, moi aussi je préférerais passer ma nuit au bordel. »

Les deux hommes se dévisagèrent, puis effacèrent leur gêne dans un éclat de rire commun.

### Jérusalem, samedi 14 juin, nuit

La nuit commençait à se faire longue, Ernaut accusait le poids de la fatigue accumulée. Si le vent ne soufflait plus guère, il avait l’impression de déambuler dans un épais brouillard qui ne se dévoilait fine poussière qu’une fois à la lumière. Sans étoile ni lune comme repère, il était très difficile de marcher ; Droart et lui mirent un temps infini pour aller d’un lieu à l’autre.

Ils avaient choisi de terminer par un établissement réputé au sud de la rue de David. Installé dans un ancien palais dont les parures étaient depuis longtemps oubliées, il attirait des hommes de toute la région, parmi les couches les plus populaires. On y chantait, on y dansait pour amuser les clients et il était fréquent qu’on y croise quelque commerçant assoupi, allongé sur un des matelas une fois tous ses sens satisfaits.

C’était une famille qui tenait l’endroit, dont Ernaut n’avait jamais pu bien dire combien ils étaient. Depuis les aînés, aux visages sillonnés de rides, jusqu’aux gamins qui allaient faire les emplettes, portaient les boissons ou nettoyaient les salles, tous prétendaient être parents. D’une quarantaine d’années chacun, Aloys et Theodoros régnaient sur ce petit monde, d’une main ferme et sans aménité, mais sans aucune violence outrée non plus. Bizarrement on aurait cru du mari qu’il venait tout droit de Normandie ou de France, avec son physique européen et son goût pour les tenues latines. Il était de taille moyenne, affichait une tendance à l’embonpoint et portait le cheveu court, à l’écuelle, comme les soldats. Il parlait d’une voix nasillarde sans jamais avoir besoin de crier.

Son épouse, Aloys, était très petite, et portait un voile lâche sur des cheveux qui faisaient sa réputation, d’un noir de jais, bouclés et comme vivants. Le visage semblable à une princesse grecque, elle avait le regard sombre, la voix grave et un tempérament de feu. Ses traits épaissis par les ans, les soucis et une vie dissolue, s’animaient avec passion dès qu’elle ouvrait la bouche. Généralement habillée de sa robe rouge, elle passait comme une langue de feu parmi les clients, surveillant hommes et femmes d’un regard attentif. Un sergent avait expliqué à Ernaut qu’elle était elle-même une ancienne prostituée et qu’elle accordait parfois ses faveurs à des prétendants à son goût. Cela nourrissait le fantasme de bien des hommes.

Depuis la rue, on entrait par une porte percée d’un guichet. Une cour accueillait quelques arbres, palmiers et aliboufiers, et servait de lieu de spectacle aux beaux jours. Au fond, la grande salle était illuminée de tous ses feux, laissant la musique et le brouhaha de la foule se déverser. Avec le mauvais temps, personne ne musardait au-dehors. Ernaut demanda au jeune qui surveillait les entrées s’il pouvait voir Aloys ou Theodoros. Le gamin avait tout de suite reconnu les deux sergents et les avait traités comme il convenait. Leur commerce était souvent l’objet d’attaques et ils ne pouvaient se permettre d’avoir de mauvaises relations avec l’administration royale. De fait, ils payaient généralement leurs loyers et leurs taxes avec une régularité exemplaire.

Les deux sergents attendaient à l’extérieur, un peu las de l’agitation de tous les endroits qu’ils avaient rapidement visités. Droart jetait un œil intéressé par l’entrebâillement de la porte tout en se curant les dents, mais Ernaut s’était allé à fermer les yeux un instant. La voix chaude, savamment modulée d’Aloys le sortit de sa torpeur.

« Le bon soir à vous deux. Y a-t-il quelque chose que je peux faire pour vous ? »

En disant cela, elle adoptait une posture telle que l’on pouvait se méprendre sur la proposition. Pourtant aucun humour dans son regard ne venait démentir la fausse impression. Séductrice, elle maintenait son pouvoir sur les hommes et les provoquait à chaque phrase, sans jamais offrir ce qu’elle promettait. Ernaut s’éclaircit la voix, veillant à ne pas se laisser charmer.

« La bonne nuit, maîtresse Aloys. Je suis sur la trace d’un nommé Ogier, j’aurais besoin de savoir s’il venait en vostre lieu.

— Cela ne regarde que lui, non ? Il n’est nullement profitable de répandre le nom de ceux qui viennent ici, les clercs leur font trop grand hontage !

— Il n’a plus guère à craindre sur ce point et c’est pour savoir qui aurait pu le frapper de male mort que je chasse.

— Ah ! s’esclaffa-t-elle. Les sergents du roi œuvrant à la justice. En dehors d’hommes à deux têtes, j’aurai donc tout vu… »

Elle réfléchit un instant, détaillant la puissante masse d’Ernaut avec un regard neutre.

« Suis-moi, ce n’est pas l’endroit où parler. »

Droart fit un sourire entendu à Ernaut et indiqua la salle de la tête, se mélangeant rapidement à la foule présente. Pendant ce temps, le géant suivait la petite silhouette jusqu’à une pièce voisine où étaient entreposés des paillasses, quelques couvertures, un banc et un coffre. Aloys y mena une lampe et s’installa sur le siège, invitant Ernaut à s’asseoir sur le tas face à elle. Elle demanda plus d’informations sur Ogier, son physique, son âge et tout ce qui permettrait de l’identifier. Nombreux étaient les hommes qui mentaient sur leur nom ou leur origine, peu désireux qu’on puisse retracer leur existence au-delà des murs de l’endroit.

Elle hochait la tête lorsqu’elle obtenait les réponses et se montrait généralement pertinente dans ses interrogations. À un moment, elle interrompit Ernaut, sortit appeler un gamin et leur fit porter du vin. Une fois munie des renseignements, elle l’abandonna un long moment, afin d’aller poser des questions aux femmes qui travaillaient là. Elle proposa à Ernaut de se joindre aux clients, mais il refusa, trop épuisé par sa journée et la tension accumulée les derniers jours. De fait, il s’endormit presque aussitôt quand elle quitta la pièce.

Une main vigoureuse l’arracha à ses rêves. Il ouvrit les yeux, un peu éberlué, et s’accouda, encore noyé de sommeil. Aloys avait repris son siège sur le banc et lissait sa robe d’une main légère, un demi-sourire sur les lèvres.

« Il est bien possible que ton pèlerin soit venu ici. »

La remarque sonna comme un gong dans l’esprit d’Ernaut. Il s'assit lentement, tout en se frottant le visage. Il dévisagea alors Aloys, qui poursuivit.

« Il n’a jamais caché qui il était, disait venir d’un casal pas loin d’ici.

— Avait-il quelque compaing avec lui, généralement ?

— Il venait seul. Pas vraiment un gai compère cet Ogier. Mais il avait bourse pleine pour filles et vin. »

Elle afficha un sourire impudique, écartant les jambes afin que sa robe légère en dessine les traits, les mains sur les cuisses.

« Il venait depuis assez longtemps, mais plus encore depuis quelque mois. Après les Pâques où la cérémonie fut troublée par une bagarre. »

Ernaut la fixa, se demandant si elle citait cela pour le provoquer ou juste comme indice de la date[^voirt2]. Elle affichait en permanence un air de défi, moitié moqueur, moitié séducteur, sans jamais perdre de son autorité.

« Il était riche ?

— Il payait ce qu’il prenait, mais ne se comportait pas en grand sire. Comme un bon commerçant qui sait payer le prix juste, sans trop en lâcher.

— Jamais eu de problème à ce qu’il paie ?

— Non. Nul ne s’aventure à nous faire pareil tour. »

Fronçant les sourcils, Ernaut se demanda si le meurtre d’Ogier pouvait être lié au commerce pratiqué ici. Aloys comprit la lueur dans son regard et partit d’un rire de gorge, chaud, naturel.

« Je ne fais pas tuer mes clients ! Il existe bien d’autres moyens d’obtenir ce qu’on nous doit. Moins violents pour le corps et plus dangereux pour l’âme. Fort efficaces.

— Mais pas eu besoin avec lui ?

— Non. »

Ernaut soupira. Il lui fallait en venir au fond du problème. Aurait-il pu se confier ? Ou son comportement dévoiler quelque chose de ses rapports avec les femmes ?

« Il se comportait comment ici ? N’a-t-il jamais fait aucune confidence ?

— Les hommes ne viennent pas ici aux fins de parler. S’ils ouvrent la bouche, ce n’est que pour échapper rots et blagues et y fourrer quelque vin ou… Elle sourit de façon fugace. Non, il n’a pas laissé de souvenir particulier.

— Il était… correct ?

— Ni plus ni moins qu’un autre. Il venait faire son affaire, se gorgeait de boisson avant cela et repartait apaisé.

— Pas de violence ou d’exigences particulières ? »

Elle fit la moue, secouant la tête en dénégation.

« Il voyait toujours la même fille ?

— Non, il… »

Elle s’interrompit, fronçant les sourcils. Elle sourit à Ernaut, les yeux brillants de ruse.

« Plus ou moins, maintenant que j’y pense. Il aimait les Syriennes. Jamais une blonde ou une fille au teint clair. Uniquement des Syriennes, ou y ressemblant.

— Depuis toujours ?

— Aussi loin que je m’en souvienne. »

Ernaut hochait la tête, satisfait. Cela confirmait ce qu’il envisageait : Ogier entretenait certainement des liens avec la communauté musulmane. S’il avait fauté avec une de leurs femmes, peut-être qu’un frère ou un père avait jugé nécessaire de venger leur honneur, ce qui expliquait la blessure mortelle. L’histoire n’avait rien à voir avec la Mahomerie.

Il était essentiel qu’il parle de nouveau avec le shaykh. Il les avait probablement menés en bateau et protégeait peut-être le coupable. Il était possible qu’il connaisse la femme à l’origine de tout cela. Ernaut se leva, lissant sa cotte, réajustant sa ceinture. Aloys demeura assise, minuscule silhouette devant lui. Elle admirait sans s’en cacher l’impressionnante stature du jeune homme, ses yeux caressant le torse puissant, les épaules vigoureuses, les bras solides. Elle lui faisait l’effet d’un renard apercevant une poule. Elle se leva brusquement, faisant voler la fine étoffe de sa robe, tout sourire.

« Ton compère a dû finir son affaire. Peut-être veux-tu le rejoindre ?

— Je vais rentrer chez moi, la journée a été rude et je n’ai qu’une envie : aller me coucher. »

Elle lui adressa un regard plein de sous-entendu lorsqu’il prononça ces derniers mots, mais n’ouvrit pas la bouche. Attrapant la lampe, elle sortit de la pièce dans un froufrou savamment maîtrisé. Ils retrouvèrent Droart, une fille assise sur ses genoux, en train de chuchoter avec elle. Apercevant son compère, il prit congé et le rejoignit. Il avait le visage enflammé et les yeux imbibés de vin.

« Je m’en vais, Droart. Tu veux rester là ?

— Nenni, je te suis, l’ami. J’en ai terminé et il est temps pour moi de me rentrer aussi. »

Au visage d’Ernaut, il comprit que la moisson avait été fructueuse. Aloys les accompagna jusqu’à la porte donnant sur la rue, toujours sans un mot. Saluant les deux hommes avec indifférence, presque froideur, elle attacha son regard néanmoins à Ernaut, caressant son avant-bras d’un effleurement à peine perceptible tandis qu’elle prenait congé. Alors qu’elle allait fermer l’huis, Ernaut le retint d’une main.

« Maîtresse Aloys, une dernière question, si tu veux bien… »

Elle l’interrogea du menton, attendant la suite.

« Aurais-tu une fille nommée Layla parmi celles que fréquentait Ogier ?

— Layla ? Non, pourquoi cette question ? »

Ernaut haussa les épaules.

« Une idée comme ça… »

Il remercia encore et salua. Alors qu’ils s’éloignaient dans les ténèbres cireuses de la nuit sans lune, Ernaut sentit qu’un regard s’accrochait encore à sa nuque. Droart l’attrapa par le bras et s’approcha de lui, échauffé par le vin.

« J’ai pu découvrir quelques petites choses pour toi, compère…

— Ah bon ? s’étonna Ernaut, persuadé que son compagnon avait oublié les recherches sitôt une compagne trouvée pour la soirée.

— Oui. J’ai pu parler un peu avec… enfin, une fille. Elle a eu affaire à cet Ogier elle m’a dit.

— Et ?

— Il est venu peu après la Noël, et a fait une fête de tous les diables. Comme jamais avant. Les monnaies lui coulaient des doigts. »

### Casal de Mahomeriola, dimanche 15 juin, matin

Comme chaque dimanche lorsqu’il n’était pas de garde, Ernaut s’était rendu à Mahomeriola où s’étaient installés Sanson de Brie et sa famille. Le vent n’était plus si fort que la veille, mais des bourrasques venaient régulièrement le frapper, comme si on lui jetait le sable à pleins poings au visage. Le monde semblait encore une fois noyé dans une brume d’ocre qui se répandait partout sous forme de poussière.

La messe avait été dite et suivie sans grand entrain dans la nef récemment achevée tandis que la tempête s’enhardissait au-dehors. Après le « *ite missa est* » personne n’avait eu le désir de s’attarder comme habituellement pour échanger les dernières nouvelles et chacun s’était empressé de retourner dans sa maison, à l’abri du vent et de la poussière tournoyante. Comme Ernaut était arrivé alors que l’office était déjà en cours, il n’avait pu saluer poliment ses hôtes, ni s’entretenir avec Libourc.

La jeune femme était, comme à chaque fois, souriante et enjouée lorsqu’elle le voyait, mais elle avait réalisé bien vite que quelque chose le troublait. Elle avait remarqué qu’Ernaut s’assombrissait avec le temps et pensait que cela était lié à ses devoirs de sergent. Elle était généralement attentive et cherchait à le réconforter, mais demeurait toujours un peu en retrait, n’ayant que rarement l’occasion d’avoir un peu d’intimité avec lui. Si le mariage avait été évoqué et envisagé, rien n’était encore fait et les jeunes gens ne se voyaient guère en dehors de la présence de leurs aînés. Ils étaient encore largement des inconnus, même si leurs sentiments les faisaient pencher l’un vers l’autre. Libourc attendait avec impatience le moment où Ernaut serait suffisamment établi pour qu’ils puissent s’installer ensemble et faire bénir leur union.

La maison toute récente qu’occupait Sanson était du même type que celle de Lambert à la Mahomerie : une réserve au rez-de-chaussée, surmontée d’un vaste étage servant de lieu de vie. La seule différence était que le vieil homme avait entrepris d’assembler une impressionnante presse à huile pour y écraser ses olives. Les travaux étaient encore en cours, et rendaient bien compte de l’effervescence qui agitait le village. Depuis quelques années, le Saint-Sépulcre, propriétaire du lieu, multipliait les attentions et les exemptions pour y attirer des colons, avec succès. Depuis la prise d’Ascalon, la région était tranquille, à l’abri des raids des Égyptiens, et les installations se faisaient plus nombreuses.

Avec la tempête qui soufflait au-dehors, les volets restèrent fermés et ce fut à la lueur des lampes que le repas se prépara. Mahaut, l’épouse de Sanson, s’occupa avec sa fille de réchauffer le plat et de monter les différents mets depuis la petite cuisine. Pendant ce temps, les deux hommes s’étaient installés à table, parlant de choses anodines. Ernaut attendait que tout le monde soit présent pour les informer du jugement à venir. Une fois le pain tranché devant eux, les écuelles distribuées et le plat encore fumant posé, Sanson versa un peu de vin coupé à tous puis récita un bref bénédicité dans un latin qui heurta même les oreilles de Ernaut. Ce dernier estima que c’était alors le moment.

« Je dois vous faire savoir des nouvelles de mon frère, car je ne pourrai prendre avec vous ce repas, il me faut retourner au plus vite à la Mahomerie.

— As-tu souci de lui ? demanda Sanson, qui avait immédiatement perçu la tension dans l’affirmation d’Ernaut.

— Certes oui. Il doit affronter par bataille un des hommes de son casal, car il s’est porté témoin du père de sa fiancée. »

Mahaut faillit en lâcher son verre de surprise et Libourc lança un regard empreint de tristesse. Sanson se contenta de froncer les sourcils et demanda à en savoir plus. Ernaut conta donc en détail l’affaire, et expliqua qu’il avait entrepris de découvrir de quoi décourager l’accusateur de maintenir ses affirmations. Il avait eu un répit grâce au khamaseen, mais ne pouvait se permettre de consacrer son temps à autre chose que son enquête.

« Tu parles de vrai, garçon, acquiesça Sanson. Je suis aise de voir que Lambert est homme de droiture, et je n’en doutais guère. Si nous pouvons faire quoi que ce soit, n’hésite pas. Tu es bientôt de notre parentèle et ton frère y entrera un peu à travers toi.

— Tout de même, il s’agit de meutrerie, mon ami, le tança Mahaut. Nous ne connaissons pas ce Godefoy et ne pourrions jurer de lui. »

Sanson soupira et prit la main de son épouse, habitué à ses remontrances sèches. Ernaut inclina la tête, avalant un peu de vin.

« C’est raison, ce que vous dites. Moi non plus je ne peux confesser avoir foi entière en lui. Seulement, mon frère a juré et je me dois de l’aider. »

La femme pinça la bouche, bien qu’elle ne pût trouver un argument contraire. Elle n’avait accepté que de mauvaise grâce le fait que Libourc se promette à un sergent, un homme qui portait l’épée. Qu’il soit au service du roi de Jérusalem l’avait un peu radoucie, mais elle ne se privait pas pour faire de temps à autre des remarques, parfois cinglantes. Sanson s’efforça de sourire :

« Bon, tu dois avoir grand désir de parler un peu avec Libourc, alors, si tu ne peux rester. Prenez donc une lampe pour aller dans la salle de la presse, le temps est trop mauvais dehors. »

Libourc adressa un sourire affectueux et rapide à l’adresse de son père tandis qu’elle se levait, la lampe à la main. Lorsqu’il descendit l’escalier derrière elle, Ernaut entendit les deux époux échanger à voix basse. Nul doute que Mahaut lui reparlerait longtemps de cette affaire.

Libourc posa la lampe sur une des poutres qui attendaient d’être assemblées et se tourna vers lui. La flamme dessinait des mèches orangées sur ses cheveux d’ébène. Elle lui sourit avec douceur et s’approcha, l’enlaçant de ses bras frêles. L’accueillant contre lui, il chercha ses lèvres, les goûta doucement avant de s’abandonner à un baiser plus passionné, tandis que leurs mains se faisaient caressantes. Ce ne fut qu’après un moment qui leur parut une éternité que Libourc se détacha un peu, les yeux emplis de désir et les joues en feu. Elle murmura :

« Ces derniers jours étaient si longs !

— Je suis désolé, il me fallait m’occuper…

— Tu n’as pas à t’excuser, le coupa la jeune femme. La vie de ton frère est en jeu, je comprends fort bien. »

Ernaut lui sourit et vola un baiser.

« Je suis heureuse que tu sois venu nous avertir et partager cette messe avec nous, même si tu ne peux rester.

— J’avais grand désir de te voir et je me devais de faire connaître l’histoire à ta famille. Même si je savais que ta mère ne serait guère enjoyée à l’entendre.

— C’est sa façon à elle de s’inquiéter pour les autres. Cela ne l’empêchera pas de joindre Lambert à ses prières. Je ferai de même d’ailleurs. »

Ernaut souffla et serra Libourc contre lui.

« Il aura besoin de toute l’aide des saints. Je ne sais si mes efforts seront utiles.

— Tu ne penses pas réussir ?

— Je ne sais même pas combien de jours me restent ! Si je ne trouve quelque faille, il y aura combat. En jugement par bataille, c’est Dieu qui décide et cela m’effraie fort.

— Tu n’as donc pas confiance en Dieu ? »

Ernaut haussa les épaules, incapable de répondre. Libourc caressa la joue du géant, le fixant avec tendresse. Il perçut en elle comme une tension qui se relâchait, un abandon qu’il n’avait jamais réalisé. Il se mordit la lèvre, hésitant à briser cet instant de calme. Puis la curiosité fut la plus forte.

« Qu’ai-je dit que tu me regardes ainsi, ma belle ?

— Tu es venu à moi et tu me parles. »

Il fronça les sourcils, incertain du sens à donner à cette réponse.

« Je viens chaque fois que je le peux, je t’assure.

— Je ne parlais pas de cela. Aujourd’hui tu m’ouvres un peu ce cœur que tu m’as offert voilà des mois. »

Son regard commençait à s’emplir de larmes et Ernaut sentit une bouffée d’émotions monter en lui. Il déposa un baiser sur le front, puis un autre sur la joue de la jeune femme, incapable d’aller plus loin tandis que leurs regards se noyaient l’un dans l’autre. Ils scellèrent finalement l’instant en s’embrassant longuement, avec passion et tendresse.

Lorsqu’ils s’écartèrent, ils avaient l’impression d’emporter un peu de l’âme de l’autre et ne le firent qu’à regret. Libourc souriait, malgré ses yeux embrumés. Ernaut essuya les larmes de ses doigts, souriant avec tendresse. Il se sentait libéré d’un grand poids, sans bien s’en expliquer la raison.

« Tu me reviendras pareil qu’en ce moment, toujours, Ernaut ? chuchota la jeune femme.

— J’en fais serment.

— Je saurai te le rappeler, mon beau, sourit-elle.

— J’ai confiance en toi pour ça. C’est que parfois, je me perds en mes propres pensées, mais tu demeures avec moi. »

Elle hocha la tête doucement, le visage rayonnant.

« Les hommes sont bien étranges, mère me l’a toujours dit. »

Elle garda le silence un petit moment, tandis que ses mains s’aventuraient dans les cheveux d’Ernaut.

« J’aimerais que tu sois toujours ainsi à l’avenir, sincère confident, de ton âme à la mienne.

— Je n’ai pas de désir plus cher, Libourc. »

Elle posa un doigt impérieux sur ses lèvres, comme pour l’empêcher de parler trop vite.

« Ce n’est pas facile serment que je te demande, je le sais bien. Pourpenses-y et tu me le feras le jour de notre union, je ne veux te contraindre par avance.

— Il n’y a là nulle obligation, je le vois bien. J’en ai fort désir de même. »

La jeune femme embrassa doucement les lèvres d’Ernaut, se hissant sur la pointe des pieds.

« Je sais que tu es sincère en cet instant, mais souventes fois, tu t’aventures en ténébreux chemins.

— Désapprouves-tu ma tâche de sergent ?

— Je ne parle pas de cela, Ernaut. C’est dans ta nature de vouloir combattre le démon. Prends garde à préserver ton âme. »

Il plissa les yeux, inquiet, mais elle continua.

« Cela n’a aucune importance pour moi, tant que tu me reviens au bout du compte. Le destin du chevalier est d’aller au-devant des périls. Son bras n’est que plus vaillant lorsqu’il sait que sa dame l’attend en son castel. »

Elle s’approcha de son oreille, embrassant sa joue au passage.

« Je n’ai nul désir que tu me contes en détail tes combats, tous les sombres périls que tu as dû affronter, et qui ont entaché ton âme par leurs sortilèges. Tant que c’est auprès de moi que tu reviens à chaque fois faire panser tes blessures et que tu ne m’en cèles aucune. »

Elle revint planter son regard, désormais enflammé, dans celui d’Ernaut. Il ne voyait plus la jeune fille sage, à la mise modeste, mais une femme emplie de passion, de vie, et qui s’offrait à lui, pleinement. Il la souleva à lui pour mêler une fois encore leurs souffles.
