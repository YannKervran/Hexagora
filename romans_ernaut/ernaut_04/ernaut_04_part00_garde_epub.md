
![Framabook, le pari du livre libre](logo_framabook_epub.jpg)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d’informations sur Framasoft, consultez [http://www.framasoft.org](http://www.framasoft.org)

Se démarquant de l’édition classique, les Framabooks sont dits « livres libres » parce qu’ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu’un utilisateur de logiciels libres. Les Framabooks s’inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l’appropriation collective de la connaissance.

Pour plus d’informations sur le projet Framabook, consultez [http://framabook.org](http://framabook.org)

Copyright 2018 : Yann Kervran, Framasoft (coll. Framabook)

*Le souffle du dragon* est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

Ouvrage publié avec le concours du Centre National du Livre

![Centre National du Livre](logo_cnl.png)

ISBN : 979-10-92674-23-1\
Dépôt légal : Décembre 2018\
Couverture : Folio 39 verso d'un ouvrage théologique, détail d’un dragon affrontant un éléphant. British Library Harley MS 3244, vers 1236-1250. Domaine Public\

# Remerciements

En plus de l’habituelle équipe de correcteurs de Framabook, Mireille en tête comme toujours, j’ai eu la chance d’être assisté dans mon travail par Frédéric Urbain, éditeur attentionné. Ses relectures précises et ses étonnements parfois drôles, souvent justifiés, sur certaines de mes formulations ou des parties de la narration m’ont beaucoup aidé à finaliser cet ouvrage.

Qu’il en soit ici remercié.

*Pour toi, Amalia, dont le souffle engendre lumière et chaleur*
