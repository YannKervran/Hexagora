![Framabook, le pari du livre libre](logo_framabook_epub.jpg)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d'informations sur Framasoft, consultez <http://www.framasoft.org>

Se démarquant de l'édition classique, les Framabooks sont dits « livres libres » parce qu'ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu'un utilisateur de logiciels libres. Les Framabooks s'inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l'appropriation collective de la connaissance.

Pour plus d'informations sur le projet Framabook, consultez <http://framabook.org>

Copyright 2018 : Yann Kervran, Framasoft (coll. Framabook)

*Le souffle du dragon* est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

Ouvrage publié avec le concours du Centre National du Livre

![Centre National du Livre](logo_cnl.png)

ISBN : 979-10-92674-23-1\
Dépôt légal : Décembre 2018\
Couverture : Folio 39 verso d'un ouvrage théologique, détail d'un dragon affrontant un éléphant. British Library Harley MS 3244, vers 1236-1250. Domaine Public\

Remerciements
=============

En plus de l'habituelle équipe de correcteurs de Framabook, Mireille en tête comme toujours, j'ai eu la chance d'être assisté dans mon travail par Frédéric Urbain, éditeur attentionné. Ses relectures précises et ses étonnements parfois drôles, souvent justifiés, sur certaines de mes formulations ou des parties de la narration m'ont beaucoup aidé à finaliser cet ouvrage.

Qu'il en soit ici remercié.

*Pour toi, Amalia, dont le souffle engendre lumière et chaleur*

Liste des personnages
---------------------

***Lydda, royaume de Jérusalem***

-   Adam, chanoine\
-   Amos, maître teinturier\
-   Anquetin, artisan membre du guet à Lydda\
-   Ascelin, chanoine\
-   Aymar, sergent\
-   Barsam de Galilée, chef maçon\
-   Bertaut, valet\
-   Clément d'Acre, architecte de la basilique\
-   Constantin, évêque et seigneur de Lydda\
-   Danyel, dit père Breton, chanoine et drapier\
-   Élies, doyen des chanoines\
-   Gembert, chanoine et hospitalier\
-   Gerbaut, manouvrier\
-   Gilbert, clerc de l'administration épiscopale\
-   Hashim, gamin des rues\
-   Heimart, chanoine et trésorier\
-   Léo, presbytre byzantin\
-   Maître Domenico, chef charpentier\
-   Marzawi, valet d'un loueur de chevaux à Lydda\
-   Maugier du Toron, vicomte de Lydda\
-   Oleg, boyard de Staraya Ladoga\
-   Régnier de Waulsort, chanoine\
-   Rolant, sergent\
-   Thibault, valet de Régnier de Waulsort\
-   Umayyah, fabricant de paniers\
-   Willelmo Marzonus, négociant en cordages\
-   Zénon de Morée, maître mosaïste\

***Administration royale***

-   Arnulf, vicomte de Jérusalem\
-   Basequin, sergent\
-   Baset, sergent\
-   Brun, clerc de l'administration des finances royales, la Secrète\
-   Droart, sergent\
-   Ernaut, sergent\
-   Eudes, sergent\
-   Ganelon, sergent de Régnier d'Eaucourt\
-   Gaston, sergent\
-   Guillemot, sergent\
-   Jean d'Acre, sénéchal du roi Baudoin\
-   Joscelin de Courtenay, héritier du comté d'Édesse, maréchal du roi Baudoin III\
-   Raoul, scribe\
-   Régnier d'Eaucourt, chevalier\
-   Ucs de Monthels, mathessep\

***Clergé***

-   Amaury de Nesle, patriarche latin de Jérusalem\
-   Frédéric de la Roche, évêque d'Acre\

***Clergé de Tyr***

-   Germain, valet\
-   Herbelot Gonteux, secrétaire de Pierre de Barcelone\
-   Pierre de Barcelone, archevêque de Tyr\

***Proches d'Ernaut***

-   Lambert, frère d'Ernaut, colon à La Mahomerie\
-   Libourc, promise de Ernaut\
-   Mahaut, mère de Libourc\
-   Osanne, épouse de Lambert\
-   Sanson de Brie, colon à la Petite Mahomerie et père de Libourc\

***Divers***

-   Amaury, comte de Jaffa, frère du roi Baudoin\
-   Baudoin III, roi de Jérusalem\
-   Guy le François, sénéchal du comte de Jaffa\
-   Henri le Buffle, chevalier du roi Baudoin\
-   Hugues d'Ibelin, seigneur de la cité de Rama\
-   Manuel, basileus, ou empereur, byzantin\
-   Philippe de Milly, seigneur de Naplouse\
-   Wibald de Stavelot, abbé et conseiller de Frédéric Barberousse\

Prologue
--------

### Monastère bénédictin de la Charité-sur-Loire, hiver 1223

Le vent faisait craquer les branches givrées du jardin d'agrément jouxtant le chauffoir. Poussé par des rafales cinglantes, Déodat frissonna d'autant plus fort lorsqu'il traversa les allées de gravier crissant sous ses pieds. Au moment où il ouvrit la porte du bâtiment, la chaleur le gifla avec la même violence que les frimas l'avaient griffé à sa sortie. Il tapa ses pieds engourdis malgré les chausses de laine, saluant les autres moines quand il en croisait le regard, tout en se dirigeant droit vers la couche de celui qu'il venait écouter jour après jour.

Comme toujours, on n'entendait que crachotements et ronflements, chuchotements et reniflements , gémissements et toux. Les plus faibles, les malades et les vieillards partageaient leurs miasmes en même temps que les soins de leurs frères et la chaleur de belles flambées. Déodat glissa sans bruit vers le petit lit près duquel il avait ses habitudes, son tabouret n'ayant pas quitté le chevet de l'infirme qu'il visitait quotidiennement.

Le moine grabataire semblait assoupi, la bouche entrouverte. Pourtant ses yeux n'étaient pas clos, même s'ils ne devaient plus voir grand-chose d'autre que des macules neigeuses. Il sut néanmoins reconnaître l'approche discrète de son jeune compagnon, qu'il accueillit d'un sourire avant que le moindre mot ne fût prononcé.

« J'espère que vous avez pris repos depuis matines, on m'a dit que vous avez eu nuit fort agitée.

--- Il est temps que Dieu rappelle mon âme à lui, ce corps ne me vaut plus rien.

--- Ne dites pas cela, nous avons encore tant à apprendre de vous, mon frère. »

Le vieil homme ricana, puis toussa. Une fois la quinte apaisée, il se rencogna sous les couvertures, son visage tavelé disparaissant presque sous les épaisseurs de laine.

« Qui peut apprendre de quiconque, mon jeune ami ? J'ai depuis longtemps perdu si fol espoir !

--- Que voulez-vous dire ? Il convient que le jeune apprenne de l'ancien, c'est là l'ordre naturel des choses voulu par Dieu.

--- Ah... Ça ! Je ne peux nier qu'avec l'âge vient le désir de semer les graines de sagesse qu'on a pu glaner au parmi de toutes ses erreurs. Mais qu'elles se plantent en bon terreau, de cela j'ai doutance. »

Tandis que le vieil homme parlait, Déodat s'installait avec son écritoire et ses plumes, ainsi qu'il en avait l'habitude. Il vérifia la pointe de ses instruments puis se prépara à noter.

« Auriez-vous l'heur de me narrer la suite de vos récits, mon frère ?

--- Avec joie, d'autant que tu y verras combien il n'est de leçons bien sues qu'on ne les ait apprises de son propre chef.

--- Où souhaitez-vous reprendre ?

--- C'est à l'abord de l'hiver, quand les ténèbres se font plus fortes et dans l'attente de la Lumière qui nous sauvera que nous allons retrouver Ernaut. Mais avant qu'il ne soit question de lui, retrouvons donc un de ses compagnons. Le roi Baudoin était au loin, espérant par ses mariages s'aboucher avec les Griffons. Le royaume s'endormait sous les gelées, ignorant du feu secret qui brûlait en lui. »

Chapitre 1
----------

### Lydda, chantier de construction de la cathédrale, matin du mardi 9 décembre 1158

Glissant péniblement dans la fine boue née de la pluie du matin, le petit homme semblait perdu dans le gigantesque amoncellement de pierres taillées et de bois de charpente, parmi les chants des portefaix et les aboiements des charretiers. Il ne vacillait pourtant pas dans son cheminement, une main serrant le bas de son habit de clerc et l'autre papillonnant entre les soutiens. De temps à autre, il lançait un regard courroucé à une vaste flaque ou à une passerelle branlante, mais poursuivait son avancée, avec mille précautions pour ne pas se salir.

Les artisans du chantier étaient habitués à la présence des religieux. Le chœur de nouveau consacré permettait aux chanoines de célébrer les offices de façon régulière, interrompant les travaux autour d'eux, remplaçant le bruissement des bras affairés par l'envolée des psaumes glorifiant la splendeur divine. Le reste du temps, l'endroit était confié aux maçons, charpentiers et couvreurs, plâtriers et peintres, mosaïstes et imagiers. De mois en mois, le vaisseau de pierre s'élançait vers les cieux, hébergeant en son cœur massif les mystères de la foi catholique. L'évêque avait vu les choses en grand, soucieux d'honorer un saint capable de tuer le démon. Un guerrier dont la vaillance et la détermination devaient inspirer à tous le courage en cette terre où pullulaient les ennemis de la vraie religion : saint Georges.

Parvenu dans la zone orientale couverte et en partie dallée, le jeune clerc marqua une pause, impressionné par la majesté de l'ensemble. Il leva son long nez pointu, dressant son bouc vers les voûtes, enchâssant ses yeux plus profondément encore qu'à l'accoutumée tandis qu'il embrassait d'un regard les ambitieux décors déployés sur les hauteurs. Fils de Cluny, il n'aimait rien tant que le faste et la pompe à destination du créateur. Rien n'était trop beau pour le glorifier, pâle reflet de sa magnificence. Lui qui ne savait rien faire de ses mains, à part les tacher d'encre, était ébahi de voir que des plus simples éléments, terre et pigments, pierres et sable, on pût tirer de tels hymnes au Très-Haut. Par la magie de la science de ses semblables.

Il se renfrogna, poussa un long soupir désabusé. Comment une créature si imparfaite que l'homme arrivait-elle à si céleste réalisation ? Depuis qu'il était sorti de son monastère pour venir servir l'archevêque de Tyr, il avait chaque jour un peu plus déploré ce qu'il avait découvert. Aussi habile qu'il fût, aucun mortel n'était de la glaise dont on pouvait bâtir une Église à la hauteur des aspirations du jeune prêtre. Il refusait de verser dans l'acédie, effroyable désespérance qui repoussait l'amour de Dieu et niait sa mansuétude. Pourtant, la noirceur des âmes qu'il rencontrait l'abattait chaque jour un peu plus. Aujourd'hui plus que jamais.

Il sentit qu'il avait besoin de mettre de l'ordre dans ses pensées, ses désirs, ses craintes. Le courage lui manquait à l'idée de la tâche qui l'attendait. Il savait qu'une des chapelles contenait un petit autel dédié à la mère de Dieu. N'ayant jamais connu ses parents, il avait pour elle une affection qui dépassait la foi. Enfant, il avait récité l'*Ave Maria* avec ferveur, espérant chaque jour que cette admirable et sainte femme viendrait le visiter un jour. Il avait depuis longtemps écarté ces chimères, désormais convaincu qu'elle résidait à chaque instant en lui. Lançant un regard rapide aux échafaudages qui grinçaient autour de lui, il vérifia qu'il serait tranquille. Les peintres et mosaïstes avaient l'avantage de ne faire aucun bruit pendant leur travail.

Il s'agenouilla donc face à la statue, sentant le froid de la pierre traverser l'étoffe du vêtement. Il perçut la dureté de la dalle, sourit aux souvenirs que cela faisait remonter en lui. Il leva les deux mains en oraison, baissa la tête humblement, clôt les yeux et commença à réciter.

« *Ave Maria gracia plena...* »

Loin au-dessus de lui, un cordage finissait de s'effilocher, brin à brin, jusqu'à se rompre. Un étai subitement détaché s'inclina peu à peu. La plateforme, déséquilibrée, se mit à tanguer en silence. Un bac d'enduit, des truelles et des marteaux, plusieurs seaux, glissèrent lentement. Heurtant un treillage léger, ils en arrachèrent les attaches. Le tout s'effondra alors en un fracas tumultueux. La passerelle inférieure fut pulvérisée, dispersant les auges de mortier, les baquets d'eau et de tesselles. L'ensemble s'abattit en un bruit de tonnerre, pluie de gravats et de poutraisons.

Tous les présents se figèrent. Il n'était pas rare que les fragiles échafaudages rompent, mais le vacarme avait été amplifié par les voûtes. Quelques ouvriers abandonnèrent leur ouvrage, inquiet de savoir si l'un des leurs avait été emporté. Nulle plainte, aucun râle ne s'échappait des décombres. Ils haussèrent les épaules et s'avancèrent pour nettoyer le lieu. Jusqu'à ce qu'un des gamins qui avait empoigné une planche reculât, les yeux écarquillés.

« Y'a un homme là-dessous, j'ai vu son habit ! »

Les bras s'activèrent d'autant plus vite, mettant rapidement au jour un corps broyé, le cou rompu et les membres disloqués. Un des plus courageux, un sculpteur à la démarche lourde osa s'approcher afin de reconnaître la victime. Il le tourna avec douceur, de ses grosses mains empoussiérées.

« C'est pas l'invité du sire évêque ? »

Ils se signèrent tous.

« Si, je crois qu'on le nommait père Herbelot. Il a visité le chantier tantôt, confirma un des présents.

--- Au moins l'est mort en oraisons. Y filera droit au Ciel. »

### Jérusalem, palais du Patriarche, fin de matinée du vendredi 12 décembre 1158

Le vent froid avait apporté une pluie fine et piquante qui glaçait jusqu'aux os. Bien au chaud dans son cabinet de travail, le patriarche Amaury de Nesle termina sa lecture et regarda les gouttelettes se former sur le verre de la fenêtre. Confortablement installé dans un siège avec une couverture sur les genoux, il conservait en main la lettre qu'on venait de lui remettre. Elle affichait le petit sceau de l'évêque de Lydda. Il sentait sur son visage la chaleur rayonnant du brasero proche. Machinalement, il porta les doigts au crucifix qui ornait sa poitrine. Il le fit tinter de l'anneau de sa bague sacerdotale.

Des pas dans la salle adjacente attirèrent son regard, mais personne ne frappa à l'huis. Ses yeux vagabondèrent ensuite sur les meubles de prix qui l'entouraient, jusqu'à s'arrêter sur Frédéric de la Roche, assis face à lui en silence. L'évêque d'Acre grignotait des rissoles. Son opulent bliaud de soie, à la dernière mode, recevait régulièrement des miettes qu'il époussetait d'un geste énergique. Sans sa tonsure, sa bague et son crucifix, on aurait pu le prendre pour un riche baron.

Les deux clercs, pour différents qu'ils fussent, se considéraient comme des amis. Le jeune patriarche était plus porté sur les affaires de la Foi tandis que Frédéric était un homme du siècle, un ecclésiastique qui n'avait guère connu le cloître. Il n'aimait rien tant qu'accompagner l'ost dans ses campagnes, bénir les lances et les glaives. La Haute Cour de Jérusalem le voyait plus souvent que ses ouailles du littoral et on lui prêtait une ambition sans bornes. Même, de sinistres rumeurs couraient, mais Amaury de Nesle savait qu'il y entrait une large part de jalousie. L'homme n'était pas un contemplatif et cela avait parfois des avantages en ces lieux emplis de périls, pour le corps autant que pour l'âme. Le patriarche était donc heureux de le compter parmi ses fidèles, apte à résoudre le souci soulevé par cette missive.

Voyant qu'Amaury avait fini de réfléchir, l'évêque s'enquit poliment de ce que contenait la lettre dont il avait reconnu le sceau qui l'ornait.

« Funestes nouvelles mon fils, bien peu réjouissantes à l'approche de la Noël. L'évêque Constantin est fort ennuyé, car un des membres de la suite de l'archevêque Pierre a trouvé la mort sur son chantier. Peut-être le connaissez-vous, d'ailleurs ? Un jeune prêtre, Herbelot Gonteux.

--- De certes, clerc érudit et consciencieux. Mon sire Pierre sera fort chagriné de cette perte. »

Il fronça les sourcils, se pencha en avant.

« Mais je ne vois pas là raison de vous informer. Tous les jours les hommes meurent d'accident ou de maladie. Voulez-vous que je fasse assavoir la chose à l'archevêque ?

--- Ce qui remue les sangs de Constantin, c'est qu'il y voit l'intervention du Malin...

--- Allons donc ! Comme il y va ! Vous éclaire-t-il sur ses motifs à cela ? »

Pour toute réponse, le patriarche tendit la lettre. Il en profita pour prendre un verre de vin qu'il sirota, laissant à Frédéric le temps de prendre connaissance de la missive. L'impétueux prélat ponctuait sa lecture à mi-voix d'interjections de surprise, de désapprobation voire d'agacement. Il rendit finalement le document en souriant avec aigreur.

« Un de ses chanoines tué par le feu et un clerc cherchant à en expliquer le mystère enseveli sous de la terre et voilà que l'évêque y voit marque des compagnons de la Bête. J'ai cru un instant que j'allais y voir surgir les prophéties de Jean[^1] ! Va-t-il s'enfrissonner de la pluie ou du vent dès à présent ? Cela n'est pas sérieux !

--- Pour invisibles qu'elles soient, certaines forces n'en sont pas moins puissantes. Je sens la frayeur et l'inquiétude qui s'installent en la congrégation de Lydda. Je ne peux laisser un de mes suffragants en pareil désarroi.

--- Pourquoi ne pas porter l'affaire auprès du frère le roi ? Après tout, il est comte du lieu et sera plus à même de s'occuper de dénouer ces tragiques soucis.

--- Je connais votre proximité avec le jeune Amaury, mais quelle légitimité aurait pareille requête ?

--- Hé bien, de prime, si ce n'est pas là simples accidents, comme le redoute votre suffragant, c'est donc affaire de sang. Son gros vicomte n'est pas de taille, Constantin le confesse quasiment en sa missive. Il convient alors d'en référer au comte. »

Amaury de Nesle n'était pas très à l'aise avec les jeux de pouvoir au sein de la Haute Cour. Il appréciait Frédéric, car il pouvait s'appuyer sur sa connaissance des luttes d'influences qui s'y déroulaient. Néanmoins il n'était jamais favorable à l'idée de trop s'acoquiner avec des laïcs dont il n'appréhendait que fort mal les usages et désapprouvait souvent les méthodes. Il devait son élection à sa pratique rigoureuse et son amitié pour la reine mère et ses sœurs, qui avaient soutenu cet esprit fort et profondément religieux. Il n'était pas homme de faction.

« De toute façon, le sire comte est parti au nord avec le roi Baudoin. Je ne peux attendre son retour pour clore cette affaire. C'est là le genre d'étincelle qui embrase les granges, je le sens bien.

--- Nul besoin d'en appeler à Amaury lui-même. Un des chevaliers de son hostel demeuré ici fera bien l'affaire. Demandons-lui de prendre les choses en main.

--- J'aurais préféré missionner quelque clerc discret plutôt que d'ébruiter l'affaire.

--- Il n'y a certainement nul trouble autre que des manquements dans l'organisation du chantier. Cela arrive tout le temps. Le conseil de quelques hommes de l'art les rassurera sur ce qui s'est passé et sur les façons pour l'éviter à l'avenir. Certains hommes cèdent trop vite à des angoisses irraisonnées. Cela ne leur vaut rien de les conforter en ce sens. »

Le patriarche prit encore quelques instants de réflexion, appuyant son visage massif sur ses mains jointes.

« Voyez-donc si on peut nous trouver quelque chevalier du sire comte Amaury dans le palais du roi, que je puisse prendre décision sans tarder. »

Soulagé que le patriarche acceptât de se débarrasser de ces menus tracas, l'évêque se leva avec vigueur. Il ouvrit la porte et héla le premier valet qu'il aperçut, lui confiant l'urgente tâche.

« Au moins avons-nous l'heur que votre palais et celui du roi partagent nombreux passages. Ce sera rapide » affirma-t-il, enthousiaste, avant de reprendre une rissole.

Il ne fallut en effet que peu de temps pour voir arriver un grand échalas bien mis, avec un bliaud assez resserré sur le buste, à longues manches, orné de nombreux décors. Il semblait avoir érigé la coquetterie en art de vivre : imposante barbe bien entretenue, bijoux et étoffes de laine de première qualité. Si ses cheveux n'avaient déserté son crâne, nul doute qu'il les aurait portés à la dernière mode. Le patriarche déplora en silence cette débauche de luxe dérisoire. L'homme ne lui inspirait guère de confiance. Il se présenta poliment, s'inclinant avec respect devant les deux importants prélats et déclina son nom : Guy le François. Amaury de Nesle connaissait très bien sa famille, mais n'avait jamais eu l'occasion de le rencontrer. Il savait que sa fratrie était proche de la couronne et plus encore du jeune Amaury, comte de Jaffa.

L'évêque Frédéric prit les choses en main et traita Guy comme un familier, le priant de passer à ses frères les plus amicales salutations du clergé. Il invita le chevalier à s'asseoir, car ils avaient besoin de ses vues sur un épineux problème. Il n'en fallut guère pour allumer une lueur de vif intérêt dans les yeux de Guy. En quelques phrases lapidaires, le patriarche expliqua le contenu de la lettre venant de Lydda. Tout en parlant, il revint peu à peu sur sa mauvaise impression : l'homme était clairement réfléchi et posé, pas un de ces godelureaux avides de conquêtes militaires et amoureuses. Guy osa quelques questions pertinentes et, ayant obtenu réponse, prit un long moment avant de proposer son opinion.

« Le sire comte de Jaffa n'a certes nul plus grand désir que de se rendre amiable à l'Église et à son premier représentant ici, sire patriarche. Le seul point qui me chagrine, c'est que je ne voudrais pas froisser un éminent prélat comme le sire évêque Constantin.

--- Il écrit pour demander de l'aide en cette affaire. Il doit bien se douter qu'il n'est pas de mes attributions de m'intéresser à crimes de sang, fussent-ils de clercs.

--- Rien n'est plus vrai. Seulement, comme vous en avez certainement savoir, Hugues de Rama[^2] est actuellement captif des Mahométans, et l'hostel comtal s'efforce de tenir son domaine en son absence. Si la mort de ces deux clercs est bien maligne, ce qui demeure à établir, il ne faut pas oublier que le sire évêque Constantin se bat depuis fort longtemps pour ne pas voir le seigneur de Rama[^3] en ses terres. Je crains qu'il ne voie pas d'un bon œil notre arrivée en son domaine, a fortiori s'il se sent dépassé en cette affaire. Il pourrait y craindre perfide intention.

--- Vous aurez courrier scellé de notre part, cela pourrait apaiser ses craintes. »

Guy le François se redressa dans son siège, lissant sa longue barbe. Il sourit aux deux ecclésiastiques tandis qu'il cherchait une façon de ne pas les décevoir tout en se sortant de ce qu'il pressentait comme un piège politique. Involontaire, mais bien réel. Cela faisait des années que la querelle entre l'évêque de Lydda et le comte de Rama pourrissait l'ambiance à la cour de Jaffa. Il n'avait pas envie d'être à l'origine d'un regain de polémique. Voyant ses réticences, Frédéric de la Roche renchérit.

« Nous pourrions missionner un clerc, de certes, mais aucun n'est familier des affaires de sang. Le vicomte de Lydda pouvait suffire pour le chanoine, mais le second, il s'agit tout de même d'un proche de l'archevêque de Tyr. Pierre de Barcelone n'est pas homme à se fâcher d'une telle histoire, mais je ne doute de son immense peine et de son envie de voir l'histoire élucidée.

--- À son départ pour le nord, le roi Baudoin a laissé plusieurs fidèles de valeur, dont un en particulier dont je sais qu'il est de bonne fame et l'esprit acéré. Un homme de terrain, mais qui a suffisamment de diplomatie pour ne pas se comporter comme un taureau agacé de taons.

--- N'a-t-il pas mission en l'absence du roi ?

--- Pas que je sache. Il a longtemps servi la reine, nous avons quelque usage l'un de l'autre. Je peux lui parler en toute fiance. Et puis Lydda n'est guère éloignée, il lui sera facile de se rendre sur place, de démêler ce qu'il y trouvera. Cela peut être résolu en une journée, voire deux. Dans tous les cas l'évêque Constantin aura des réponses à donner au sire archevêque de Tyr, tout en récoltant baume à répandre sur ses angoisses. »

Enjoué, Frédéric de la Roche hocha la tête avec vigueur et n'attendit pas la réponse du patriarche pour marquer son assentiment. Amaury de Nesle réserva son jugement un petit moment avant d'acquiescer lentement du menton.

« Je vais rédiger une lettre pour cet émissaire, qu'on lui fasse bon accueil.

--- Je la lui porterai moi-même, sire patriarche.

--- Et quel est le nom de ce dernier, d'ailleurs ? s'immisça Frédéric de la Roche.

--- Régnier d'Eaucourt[^4]. »

### Jérusalem, palais royal, matin du dimanche 14 décembre 1158

Petit homme à la bedaine aussi impressionnante que la barbe brune, Ganelon ruminait son mécontentement en finissant de lacer des sacs sur des chevaux. Il s'accommodait fort bien de demeurer à Jérusalem avec la cour au loin et n'appréciait guère de devoir partir vers la côte. Il avait entretenu l'espoir de passer la Noël tranquillement avec son épouse. Fraîchement mariés, ils s'étaient établis dans une modeste maison près de la porte de David et escomptaient bien s'affranchir de leurs services pour rénover le commerce du rez-de-chaussée. Lorsque son maître Régnier était au palais, Ganelon n'avait que peu de tâches. Le chevalier disposait désormais d'un hôtel complet, avec valets et cuisinière, peu loin du domicile royal, et le travail de Ganelon s'en trouvait grandement facilité.

Il avait été touché d'apprendre le décès du jeune Herbelot Gonteux, qu'il avait pu apprécier lors de leur retour d'Europe. Mais il ne s'en sentait pas suffisamment proche pour aller courir par monts et par vaux afin d'élucider le mystère d'une mort qui n'était certainement que simple accident de chantier. Il profitait donc de l'absence de Régnier et des derniers préparatifs pour grommeler à son aise, à l'abri de tout risque de réprimande.

« Alors, mestre Ganelon, tu as donc grande ire à grogner tel l'ours Martin ? »

La voix qui venait de l'apostropher lui arracha un sourire. Il se retourna et salua avec enthousiasme le géant qui s'avançait vers lui. Dépassant d'une bonne tête les gens autour de lui, Ernaut semblait avoir encore grandi et forci depuis la dernière fois qu'il avait vu. Le jeune homme paraissait bien déterminé à battre Samson voire Goliath en taille et en vigueur. Le cheveu blond coupé à l'écuelle, la barbe naissante au menton, il arborait son sourire le plus enjoué, éclairant son visage de guerrier d'une surprenante amabilité. À ses côtés, un compagnon, faisant figure de nain, s'avançait, une large besace de scribe en bandoulière.

Ernaut salua avec chaleur Ganelon et lui présenta Raoul, qui serait chargé de rédiger d'éventuelles missives.

« J'ai plaisir à chevaucher avec vous de nouvel. C'est grande pitié que ce soit pour aller mettre en terre mestre Gonteux. »

Ganelon haussa les épaules.

« *Dieu dispose*.

--- Je l'avais recroisé à l'occasion, quand il venait en compagnie de l'archevêque de Tyr. C'était de la graine de patriarche, c'est vraiment pitié. »

Ernaut fit jouer ses muscles puissants, gonflant le dos et la poitrine tandis qu'il comprimait son poing dans sa main.

« Si jamais je peux grapper celui qui l'aura occis...

--- De ce que mon sire Régnier m'a dit, nul ne sait si c'est accident ou meurtrerie. Le premier serait d'évidence, en un chantier comme celui de la cathédrale.

--- Ça, l'avenir nous le contera, mon ami. »

La cour était relativement calme, la fraîcheur de la saison ayant incité les domestiques à privilégier les corvées en intérieur. On entendait malgré tout le tintement d'une enclume dans les bâtiments annexes. Autour d'eux, quelques montures étaient préparées par les palefreniers. Plusieurs hommes chargeaient aussi sur une imposante charrette la litière usagée issue des écuries, destinée à amender les jardins de la cité. Se voir accorder le droit de prélever le purin de la cavalerie de l'hôtellerie royale était un avantage très envié, financièrement intéressant si on n'était pas délicat de l'odorat. Fronçant le nez, Ernaut se trouvait bien content de sa fonction de sergent.

Attrapant son filet, il héla Ganelon tout en équipant sa monture.

« J'ai ouï-dire que tu as désir de quitter le service du sire d'Eaucourt.

--- Je l’aurais pas dit ainsi, mais ça finira par arriver, de certes. Je me suis marié voilà peu et nous avons espoir de nous établir, soit en louant notre bien, soit en faisant commerce par nous-mêmes.

--- Et que veux-tu donc négocier ? Tu sais que je m'y connais en vin ?

--- C'est aimable à toi, mais je pensais à quelque chose de plus simple pour nous. Élainne est bonne couturière, nous pourrions acheter, ravauder et revendre tenues. Le pérégrin a toujours nécessité de linges, pour porter beau aux sanctuaires ou faire le chemin à rebours jusque chez lui. »

Il flatta le cheval d'une bourrade et se pencha pour en inspecter les sabots.

« De toute façon, elle s'est jurée à l'hostel le roi, il nous faut voir comment elle pourrait achever son service avant tout.

--- Et toi, tu vas faire comment ?

--- Sire Régnier n'a plus tant besoin de moi. C'est surtout lorsqu'il doit compaigner l'ost ou faire chemin que je lui suis utile. Mais cela peut se faire de gré à gré, même avec un homme libre. »

Ernaut était enthousiasmé de voir un de ses amis réussir ainsi. Il escomptait lui-même connaître semblable chemin et se languissait dans l'attente. Il était impatient de s'unir à sa promise Libourc et de s'installer avec elle dans leur demeure. Bien que sa tristesse soit réelle quant aux motifs de ce voyage, il était malgré tout ravi de pouvoir renouer avec Régnier d'Eaucourt. Il espérait lui prouver qu'il avait bien évolué, ces dernières années au service de Baudoin III. Ernaut en était convaincu, il se présentait toujours dans le royaume des opportunités profitables pour qui savait les saisir.

Lorsque Régnier d'Eaucourt s'annonça, tout était prêt pour leur départ. Ils n'auraient guère à chevaucher, à peine plus d'une demi-journée en tenant un bon train et ne se pressaient donc guère. Le chevalier salua les présents et contrôla que tout avait été préparé selon ses instructions. Il avait toute confiance en Ganelon et ne s'attarda pas. Au moment où il allait mettre le pied à l'étrier, une voix tonna depuis le bâtiment d'où il était sorti.

« Qui dispose ainsi de mes chevaliers ? Ne suis-je pas le maréchal ici ? »

Accompagnant l'éclat de voix, Joscelin de Courtenay fit son apparition en haut de l'escalier, mains sur les hanches et le visage envahi de tics désapprobateurs. Sa grosse tête s'agitait, hérissée de ses cheveux mi-longs. Habillé comme toujours d'une tenue impeccable, il arborait un fourreau coûteux et une épée dont la poignée richement ornée d'émaux laissait deviner la qualité. Il dévala les marches rapidement, faisant claquer ses éperons dorés de chevalier.

« Alors ? J'ai posé question ! »

Régnier, soucieux de ne pas se mettre entre deux puissants personnages, préféra demeurer dans le vague.

« C'est là mission pour le frère le roi, aux fins de complaire aux attentes du sire patriarche, mon sire maréchal. J'encroyais que vous aviez donné votre aval.

--- Pour cela, il aurait fallu que l'on me demande avis. C'est tout de même incroyable ! Comment puis-je diriger l'ost si on envoie mes chevaliers faire les coursiers selon son bon plaisir ! »

À son éclat de voix, la plupart des valets s'étaient retirés, ayant trouvé une tâche à faire au loin, aussi apeurés qu'une bande de lapins devant un renard.

« Je ne doute pas qu'on vous ait donné beau motif pour chevaucher je ne sais où...

--- Lydda, sire maréchal. Ce n'est pas tant loin que je ne sois là à la moindre semonce de votre part.

--- De ce dernier point, je n'ai nulle doutance, vu que vous allez demeurer ici.

--- Mais, sire Joscelin, le patriarche compte sur moi et...

--- Il ferait beau jeu qu'un tonsuré ordonne à mes féaux. Ne suis-je pas celui à qui votre foi fut jurée ? »

Régnier, mortifié de se voir ainsi morigéner par un bachelier à peine sorti de l'enfance, approuva en silence, une moue contrariée défigurant ses traits. Il avait juré sa foi au roi, et certainement pas à un autoritaire parvenu.

« Parfait, voilà qui est dit.

--- Sire Joscelin, il faudrait au moins mander quelqu'un à Lydda. L'évêque nous y espère. Le patriarche a certainement envoyé coursier pour prévenir de notre arrivée.

--- Hé bien, la prochaine fois, il me viendra demander. »

Le maréchal se tourna, indifférent, et fit mine de découvrir Ernaut, qu'il considéra ainsi qu'on le fait d'un animal de foire.

« Il n'y a qu'à lui envoyer ce valet vôtre. S'il est un homme qui peut redonner confiance, c'est bien lui !

--- Il n'est pas mien sergent, sire. Il est au service le roi, lui aussi. »

Le jeune maréchal fronça un instant les sourcils, suspectant le sarcasme, mais ne s'y attarda pas.

« Hé bien, si le sénéchal laisse baguenauder ses sergents, je n'en ai cure. Qu'il se rende donc là-bas et nous fasse savoir de quoi il retourne. Nous verrons s'il y a lieu d'y mander plus forte délégation. »

Ayant distribué ses ordres, Joscelin tourna les talons et remonta dans le bâtiment, apaisé d'avoir pu ainsi affirmer son autorité. Il laissait les quatre émissaires dans l'expectative.

« Que faisons-nous, sire Régnier ? s'enquit Ganelon.

--- Tu l'as entendu comme moi, tonna Régnier, agacé. Nous demeurons ici et Ernaut sera l'envoyé royal auprès de l'évêque. »

Le jeune homme, qui avait jusque là affiché un air sombre à voir ainsi humilié un chevalier qu'il estimait fort, ne put cacher sa joie. Il allait être l'envoyé désigné par l'hôtel du roi, à la demande du patriarche, pour aider un haut ecclésiastique à résoudre ses problèmes. C'était possiblement l'occasion de faire valoir ses talents. Régnier comprit en un instant le cheminement intellectuel qu'Ernaut était en train de suivre.

« Garde la mesure, Ernaut. Tu vas simplement aller là-bas constater ce qui s'y est passé et tu nous en feras part rapidement. »

Tout en parlant, il défit une petite boîte métallique qu'il avait à la ceinture. Elle contenait le message du patriarche à destination de l'évêque. Sans plus un mot, il la tendit à Ernaut. Raoul, resté en retrait jusque là, intervint de sa voix claire.

« L'évêque risque d'être fort désappointé en ne voyant que nous. Que devons-nous lui dire ?

--- Il suffira de lui expliquer que le roi, pour complaire à la fraîche épouse de son frère[^5], s'est adjoint un coquelet en guise de maréchal. »

La réponse d'Ernaut fit rire tout le monde, les délivrant du poids de l'intervention désagréable de Joscelin de Courtenay. Étrangement, cela fut accompagné de la réapparition de plusieurs valets. Régnier les abandonna rapidement, désireux d'aller prévenir au plus vite de la décision prise par le maréchal. Avant de partir, Ernaut aida Ganelon à transférer quelques provisions sur leurs montures.

« On disait l'ancien maréchal colérique et violent, mais on dirait que le nouveau n'est guère mieux !

--- De certes, je n'aurais jamais cru que les hommes désespéreraient si tôt de la capture d'Eudes de Saint-Amand, confirma Ganelon. Il pestait et tonnait tant et plus, mais jamais il n'aurait parlé à un homme de valeur de pareille façon. Et devant de simples sergents en plus !

--- M'est avis qu'à se fâcher ainsi avec tous, ce béjaune ne fera pas longue carrière ici.

--- On le dit bien plus pressé de faire valoir son titre de comte.

--- *Asne d'Arcadie, chargé d'or, mange chardons et ortie* lança Raoul. Il n'a plus rien à Édesse, je l'ai entendu maintes fois en la chancellerie. C'est ce qui doit le rendre fort amer.

--- Holà, attention, voilà mestre Raoul qui nous éclaire de sa sapience issue des plus grandes écoles cathédrales, se moqua gentiment Ernaut. »

Voyant bien dans le regard malicieux de son comparse qu'il n'y avait là nulle méchante intention, Raoul sourit et fit mine de le tancer de la main. Ce qui ne fit qu'inciter Ernaut à aller plus loin. Il gratifia Ganelon d'une bourrade, monta en selle et poursuivit, goguenard.

« De vrai, sans rire, Raoul est homme de lettres. Un de ses frères étudie auprès des plus grands maîtres et nous reviendra au moins évêque. Sauf s'ils l'élisent pape entretemps. »

Attendant que son compagnon soit installé en selle à son tour, Ernaut salua les présents autour de lui et lança son cheval en direction de la sortie. Il siffla un des gamins pour qu'on lui tire la porte. Il n'avait pas envie de démonter pour quitter la cour de l'hôtel du roi. Après tout, n'était-il pas officiellement chargé d'une mission d'importance, la boîte à messages battant à sa ceinture ? Lorsqu'il passa l'arcade, il se mit à espérer que Libourc le voyait ainsi qu'il se rêvait : sur son destrier, équipé de son harnois de guerre, casque sur la tête et lance à penon en main. Il en oubliait qu'il était assis, de façon plutôt grossière, sur un vilain roncin.

### Lydda, abords du palais de l'évêque, soir du dimanche 14 décembre 1158

Le soleil avait disparu depuis un moment lorsqu'Ernaut et Raoul parvinrent enfin aux abords de l'impressionnant monastère qui abritait la demeure de l'évêque de Lydda. Le lieu tenait à la fois de la forteresse, du couvent pour les chanoines, du palais épiscopal et de l'église de pèlerinage, le tout en un vaste chantier pour servir un ambitieux programme architectural. Séparé de l'agglomération par une belle muraille, une porte extérieure permettait d'y accéder directement.

Lorsqu'ils longèrent les fortifications de la ville, de rares silhouettes les épièrent depuis le rudimentaire chemin de ronde. Il n'était pas si tard et des bruits d'activité humaine franchissaient les murs pour mourir dans leurs oreilles. Autour d'eux, tout était calme dans les vergers et les jardins. Un camp de nomades installé à l'abri d'une palmeraie bruissait à peine de l'agitation des moutons enserrés dans leur enclos. Ils passèrent au large, obliquant vers l'imposant massif de pierre où brillaient encore des lumières. La lune sur son dernier quartier n'éclairant que d'une lueur blême la voie, Ernaut se félicita d'être dans la plaine, sur un ample et droit chemin de poussière.

La porterie du palais s'appuyait sur une tour où flottait une bannière indéchiffrable dans la nuit. Sans vent, elle pendait mollement comme un chiffon. À leur approche, malgré la boîte de messager royal, l'homme de faction refusa d'ouvrir grand les vantaux et ils durent descendre de leur monture pour les mener à la bride par le petit guichet piétonnier. Après des heures passées en selle, ils sentaient la raideur de leurs membres.

« Vous v'nez de la Cité en réponse de l'appel du sire Constantin ? grogna l'homme, peu convaincu par l'aspect des deux émissaires.

--- De certes, nous sommes porteurs d'un message de la part du patriarche Foucher. Fais donc assavoir à ton maître que nous sommes arrivés. »

Sans se départir de son attitude circonspecte, le soldat envoya un gamin d'un simple mouvement de tête. Puis il expira longuement, avant de proposer d'un ton plus amène :

« Compaignez-moi donc. Le sire évêque est en plein repas. Vous avez bien temps de grignoter un truc et de vous rendre présentables. »

Suivant le vieil homme au pas traînant, ils perdirent rapidement le fil des pièces, cours et escaliers qu'ils parcouraient. Comme beaucoup de palais, il était régulièrement aménagé, amendé, augmenté, complexifiant peu à peu le plan originel, s'adaptant aux velléités des nouveaux possédants. Ils aboutirent bientôt à une petite salle avec deux banquettes en pierre, surmontées chacune d'une niche de couchage creusée à même la maçonnerie. Une minuscule fenêtre donnait sur des jardins, au-dessus d'un coffre adjoint de deux tabourets.

« Vous êtes en l'hostellerie. V'là la clef de votre cellule. Vous pourrez demander après une lampe aux cuisines. On va vous porter les linges de lit. »

Il les laissa poser leurs affaires puis les mena à la salle commune où le personnel partageait ses repas, avec les hôtes les moins prestigieux. Ayant décidé d'autorité que ces deux-là n'étaient sûrement pas des gens de marque, il leur indiqua une place sur un banc et les abandonna, après leur avoir rappelé qu'on viendrait les chercher pour les présenter à l'évêque.

Familiers de ce genre d'endroit, pour prendre régulièrement leurs diners et soupers au palais royal, Ernaut et Raoul s'installèrent à leur aise, profitant d'une cuisine assez honnête, où le pain était moelleux et le brouet pas trop clair. Même le vin ne piquait pas trop, approuva Ernaut. La tablée n'était pas très bavarde et assez disséminée. Le lieu était pensé pour accueillir, au besoin, les pèlerins. Avec la clôture des mers, il ne s'en trouvait guère en cette saison.

Raoul commençait à dodeliner de la tête au-dessus de son écuelle lorsqu'un valet vint les chercher : l'évêque allait les recevoir. Ils frottèrent leurs cottes fatiguées et suivirent le domestique jusqu'à la grande porte qui s'écarta pour eux, ouvrant sur la magnifique salle d'honneur du palais. Un instant, Ernaut se figea, impressionné de se trouver être celui qu'on attendait. Devant lui, la vaste pièce était éclairée de quelques lampes et de plusieurs chandeliers, redonnant vie aux histoires tracées à fresque sur les murs. Tout au fond, un trône garni de coussin accueillait un homme âgé, corpulent, mais au port assez raide. Il était vêtu d'un bliaud richement brodé, aux motifs scintillants. D'une main il caressait une épaisse croix émaillée suspendue à son cou. De l'autre il leur fit signe d'approcher. Son visage était plein, ridé et flanqué de deux oreilles en feuille de chou. Son regard semblait fatigué tandis qu'il dévisageait les deux messagers.

À sa droite, recouvrant une chaise curule de son imposant fessier, un gros homme les jaugeait également tout en s'épongeant le front. Il était vêtu de façon assez commune, mais on ne pouvait manquer de voir les éperons dorés brillants à ses pieds et l'épée au fourreau posée non loin de lui. Il fit un salut de tête à peine perceptible, un fantôme de sourire animant sa moue gourmande.

Le plus étonnant dans la scène était un petit singe vervet qui s'accrochait à un des montants du trône. Il ne bougeait guère que le bout de sa queue, ce qui faisait qu'on ne le remarquait pas immédiatement en entrant. Lorsque les deux messagers s'avancèrent, il vint se réfugier dans les bras du prélat, qui le flatta doucement. Peu à l'aise avec le protocole, Ernaut s'agenouilla devant l'évêque, qui lui tendit sa main baguée. Il la baisa et attendit, tête baissée. Le silence s'installa.

Inquiet d'avoir fait une bourde, Ernaut s'accorda un regard en dessous en direction du gros homme sur le côté. Il lui sembla que celui-ci l'invitait à s'exprimer. Sa voix lui parut comme le tonnerre lorsqu'il osa parler. Il salua poliment au nom du patriarche et du roi, et indiqua qu'il était porteur d'un message de la part du premier. Il le sortit de la boîte fixée à sa ceinture et le présenta avec toute la dignité dont il se sentait capable.

L'évêque s'en saisit et n'y jeta pas même un coup d'œil. Sa voix était amère lorsqu'il s'exprima.

« N'y a-t-il donc que valets et sergents en la sainte Cité pour qu'on ne daigne pas même m'envoyer un chevalier, si ce n'est baron de renom ? Ai-je donc perdu tout crédit ? »

Mortifié, Ernaut ne trouva pas quoi répondre et ce fut Raoul qui osa le faire, de sa voix claire et posée.

« Sire évêque, nous sommes, tout au contraire, là pour témoigner de l'intérêt porté à votre affaire. De façon à ce que nul ne remette en cause votre autorité en ce lieu, il a été décidé de confier vos intérêts à quelqu'un de l'hostel du roi. Las, avec la rencontre dans le Nord, il s'est trouvé difficile de dépêcher un homme de qualité rapidement. Notre présence ne s'explique que par le souci de ne pas vous laisser sans réponse. »

L'évêque Constantin grommela un instant, quelque peu rasséréné par la réponse. Ernaut se rendit compte de l'habileté de son ami, familier qu'il était des puissants. Pour sa part, il n'était guère habitué à faire assaut de tant de diplomatie. Le temps que l'ecclésiastique déplie le message et le parcoure à mi-voix, le gros homme leur lança un sourire aimable, avant de s'emparer d'une poignée d'amandes dont il offrit quelques unes au singe, venu sur ses genoux dans l'intervalle. L'animal ne perdait pas de vue les arrivants, se promenant aux alentours tout en grignotant ses friandises.

La lecture de la missive sembla calmer les aigreurs de Constantin. Il reprit ses aises sur son trône et se composa une attitude moins agressive. Grimpé sur un des montants du siège, le primate penchait comme une gargouille, comme s'il hésitait à prendre connaissance d'un courrier qui ne lui était pas destiné.

« Le patriarche Amaury me parle d'un chevalier parmi vous. S'est-il perdu en chemin ?

--- Las, au moment de partir, le maréchal a dû lui confier une tâche urgente, répondit Ernaut.

--- N'y avait-il nul autre homme que lui pour ce service ?

--- Je ne saurais vous dire, sire évêque. Mais je tiens pour certain que cela lui a de certes fort coûté. Il était bon ami avec Herbelot, le secrétaire de l'archevêque. Il n'aurait pas laissé sa place sans quelque urgence vitale au service du roi.

--- Il avait été désigné pour ces raisons ?

--- J'ose l'encroire. Il en est de même pour moi. Nous avons navigué à bord du Falconus et connu moult péripéties dont nous avons triomphé ensemble. Nul plus que nous au sein de l'hostel du roi n'a plus grand désir d'honorer la mémoire de celui dont vous déplorez la mort.

--- Tu connaissais donc ce jeune prêtre ?

--- Nous étions amis autant que peut l'être un homme de son rang avec un simple sergent comme moi. Et je peux donner gage du lien qui l'unissait à Régnier d'Eaucourt, le chevalier qui nous devait compaigner. »

Tout en échangeant, l'évêque retrouvait son calme. Il avait craint un moment que l'on ne cherche à le rabaisser avec cette histoire. Il s'opposait depuis tant d'années au pouvoir du seigneur de Rama qu'il redoutait que le comte Amaury ne profite de la capture de ce dernier pour annuler toutes ses prétentions. Mais le patriarche avait bien entendu sa détresse et avait fait en sorte de lui envoyer des serviteurs loyaux à un ami, donc peu enclins à se laisser influencer par des jeux politiques. Il désigna le gros homme, resté silencieux jusque là.

« Jusqu'à présent, Maugier du Toron, mon vicomte, s'occupait du décès de Régnier de Waulsort, à l'origine de tous nos malheurs. C'est alors qu'il s'efforçait de l'assister en cela que votre pauvre ami a rendu son âme à Dieu.

--- Comme personne ne pouvait être suspecté d'avoir attenté aux jours du pauvre Régnier, je n'avais nulle raison d'y consacrer trop de temps, indiqua le vicomte.

--- D'autant que certains murmuraient choses impies à ce sujet, approuva l'évêque.

--- Donc quand ce jeune clerc a proposé d'y porter lumière, j'en étais plus que satisfait. Moi je m'occupe des mauvais valets et des marchands escrocs.

--- Tant que cela ne concernait qu'un de mes chanoines, nous pouvions faire cela à notre idée. Mais là, un serviteur de l'archevêque est mort. Il nous faut porter réponse. »

Hésitant sur ce qu'il devait en comprendre, Ernaut se tourna vers le vicomte.

« Vous dites qu'Herbelot s'appliquait à résoudre le mystère d'une mort, c'est bien cela ? »

Le gros homme hocha la tête en silence tandis que l'évêque répondait pour lui.

« Certes, tout vient de là. La mort de Régnier de Waulsort. Tragique disparition qui inquiète toutes les âmes de ce palais.

--- Il y aurait eu meurtrerie ?

--- Voilà bien épineuse question, mon garçon. »

L'évêque marqua une pause, se pencha un peu en avant, renâclant à dire ce qu'il avait sur le cœur. Il ne répondit qu'à mi-voix, inquiet de réveiller des forces qui le dépassaient.

« Il se trouve que Régnier était un des plus doctes d'entre nous. Il supervisait tout le programme décoratif du nouveau sanctuaire. Un grand spécialiste des hauts faits de saint Georges. »

Il se rapprocha encore, baissant la voix tandis que sa silhouette s'inclinait.

« On l'a retrouvé un matin, en sa demeure, le corps brûlé comme charbon. »

Une chape de silence s'abattit sur les présents. Même le petit singe se figea un moment. Ernaut se demandait si l'évêque avait mal formulé sa phrase ou s'il tenait à attirer son attention sur un point particulier.

« Pensez-vous que quelque malfaisant a bouté le feu à sa maison, ce ne serait pas juste une lampe qui aurait versé ?

--- Un tel incident aurait embrasé toute la demeure, ce qui ne fut pas le cas...

--- Comment est-ce possible ?

--- C'est bien là le coeur du mystère, mon garçon, intervint le vicomte. »

Constantin baissa la tête, comme si une grande fatigue s'abattait soudain sur lui.

« Les esprits s'inquiètent de cela, surtout après le second incident. On murmure déjà que le feu et la terre ont tué... Je ne peux tolérer cela. Il me faut réponse à donner aux miens et à l'archevêque. »

Il reprit d'une voix plus assurée tout en accueillant le singe sur ses genoux.

« Demain je ferai savoir à tous que vous êtes ici, car le roi Baudoin a désir de faire une donation au sanctuaire de saint Georges. On ne s'étonnera donc pas que vous alliez et veniez à votre guise, histoire d'apprécier nos besoins. Seuls le vicomte et les chanoines sauront la réalité. Veillez à vous montrer discrets, les rumeurs se propagent plus vite qu'incendie de paille. »

Les deux émissaires hochèrent gravement la tête en silence.

« Vu que l'hostel du roi et le sire patriarche vous ont envoyé à moi, je n'ai d'autre choix que de m'en remettre à vous. Je prierai pour que vous portiez toute la lumière en cette ténébreuse question. »

Il les bénit d'un lent signe de croix puis baissa la tête. L'audience était terminée.

### Lydda, hôtellerie du palais de l'évêque, matin du lundi 15 décembre 1158

Les braiments d'un âne tout proche réveillèrent Ernaut en sursaut. La lumière entrait à flots dans la pièce chaulée. Assis sur sa couche, visiblement depuis peu, Raoul se frottait les yeux. Ernaut se leva, ouvrit la fenêtre pour humer l'air au-dehors. Un ciel lapis-lazuli couvrait la palmeraie qui accueillait les jardins. Il s'étira lentement et prit quelques affaires pour se rendre au bassin qu'il avait repéré la veille. Il poserait aussi la question des bains, dont il était certain qu'il y en avait quelque part. C'était une des commodités d'outremer à laquelle il avait rapidement pris goût.

Il échangea quelques mots polis avec des valets autour de la fontaine. Un petit groupe de notables du Lyonnais avait décidé de passer une année entière en Terre sainte. Pèlerins de qualité, ils profitaient de l'hospitalité des nobles et voyageaient à cheval, accompagnés de domestiques et de soldats. Arrivés la veille, ils faisaient une courte escale avant de reprendre leur route vers Naplouse.

Non loin d'eux, dans la cour, sous un appentis couvert de feuilles de palmier, on s'activait à dépecer un cochon. De nombreux pots fumaient aux alentours, apportant l'eau bouillante nécessaire au travail. Gourmand, Ernaut se demanda s'ils auraient l'occasion de goûter les charcuteries et pâtés fraîchement réalisés. On lui expliqua que cela serait servi au banquet de Noël. Un peu à l'écart, les gamins généralement occupés à surveiller des oies avaient oublié un temps leurs corvées pour s'amuser à se lancer une balle façonnée dans la vessie de l'animal. Cela rappela des souvenirs à Ernaut, qui n'aimait rien tant que grignoter les petites saucisses aux herbes que son père confectionnait pour fêter l'abattage du cochon.

Mis en appétit, il retrouva Raoul dans la grande salle, attablé devant une tranche de pain tartiné de hoummous. Autour d'eux, plusieurs manouvriers venaient prendre une collation après les premiers travaux de la journée. Certains étaient clairement des artisans, les manches salies de mortier ou les mains grises de pierre. Bien que Noël ne soit plus guère éloigné, le temps était beau et les températures assez douces. Ernaut, habitué aux rigueurs des hivers bourguignons, s'émerveillait de cela.

« C'est qu'on est guère loin des côtes, le temps y est plus doux, moins sec aussi, précisa Raoul.

--- Tu es d'où au fait, toi ?

--- Je suis né à Jérusalem. Cela fait des années que ma famille y est installée.

--- Tu n'es jamais allé ailleurs ?

--- Je ne suis jamais sorti du royaume, non. Et guère de la cité, à dire le vrai. J'apprécie ses fortes murailles. Mais comme tu le sais, mon frère Guillaume, lui, est parti au loin.

--- Cela fait long temps ?

--- Des années ! De temps à autre nous recevons de ses nouvelles. Mère aurait été heureuse de savoir qu'il finira peut-être abbé ou évêque. »

Abandonnant la croûte de pain trop dure, qui finirait sûrement aux chiens, Ernaut se leva.

« Je vais m'enquérir de la dépouille d'Herbelot, j'aurais désir de prier pour son âme. On se retrouvera auprès du vicomte ?

--- Il y a une audience ce matin, en une grande salle près de la porterie donnant en ville. »

Approuvant de la tête, Ernaut salua le jeune homme et prit la direction du sanctuaire. Le cimetière devait se situer aux abords, un serviteur de l'archevêque ayant sûrement droit à une sépulture privilégiée. Il traversa les différentes zones de bâtiments, depuis les communs les plus à l'extérieur, où il se trouvait, pour rejoindre la partie noble, où étaient implantés les édifices dévolus aux chanoines en pierre de taille. Le palais de l'évêque se déployait de l'autre côté du sanctuaire, au sud. Au centre, adjoint d'un petit cloître qui desservait tous les lieux de prestige, s'élevait la basilique en pleine reconstruction.

Au moment où il y parvint, Ernaut entendit la cloche sonner. Elle marquait les heures pour les clercs, qui chantaient les louanges du Seigneur plusieurs fois chaque jour. Bien peu de gens du commun accordaient un intérêt religieux à ces appels, en dehors peut-être des vêpres, car elles indiquaient généralement la fin de la journée de travail. Certains parmi les plus pieux ou les plus superstitieux remerciaient d'une courte prière qu'elle se soit bien déroulée.

Lorsqu'il pénétra dans l'église, seuls les outils s'exprimaient : le chœur recevait de nombreux décors tandis que la partie occidentale était encore en cours de construction, en partie à ciel ouvert. Des portefaix chargeaient leurs lourds paniers de mortier dans une fosse creusée à même la nef. Puis ils montaient à l'aide d'échelles jusqu'aux niveaux où les maçons empilaient les moellons, emplissaient le blocage. Les murs résonnaient de coups secs, les tailleurs ajustant les pièces les plus délicates à leur emplacement.

Perdu dans un labyrinthe de charpente soutenant les arcades esquissées, les voûtes inachevées, Ernaut tenta de se faire discret, impressionné par l'activité fébrile du lieu. Dans des loges couvertes de nattes, des sculpteurs dégageaient des motifs floraux, des têtes d'animaux de leur gangue de pierre. Si l'édifice en lui-même était bâti dans un calcaire jaune-brun, les plus belles décorations étaient ciselées dans un marbre blanc parfois veiné de gris. Bien sûr, des peintures, des tesselles de mosaïques rehaussaient progressivement les murs, en particulier au niveau du chœur. Enfin, un mobilier liturgique, soies brodées et bijoux d'orfèvrerie, serait extrait du trésor lors des occasions solennelles de façon à célébrer dignement la grandeur de Dieu.

Il fit quelques pas, le regard rivé au sol. Il ne demeurait rien de visible du lieu où était décédé Herbelot et Ernaut dut se faire indiquer l'endroit, devant une des chapelles latérales. Les passages étaient de nouveau fixés en hauteur, permettant aux ouvriers d'aller et venir sans encombre entre plusieurs niveaux à l'aide d'échelles et de cordages... Ernaut s'agenouilla dans le chœur, où le maître autel accueillait de nombreux cierges et de magnifiques bouquets d'anémones multicolores. Il nota mentalement de se procurer de quoi accompagner une prière pour Herbelot. Il ne connaissait guère le jeune clerc et pourtant sa mort lui semblait un affront à la vie. Il avait toutes les qualités pour devenir un ecclésiastique d'importance, savait parfois se montrer agréable compagnon malgré un abord souvent crispant. Avec son décès, tout cela disparaissait. Il n'en demeurait que les bons souvenirs.

Il sortit par une porte latérale et rejoignit les jardins accolés au sanctuaire. De l'extérieur, l'édifice était vraiment imposant. Il tenait tout autant du donjon que de l'église. Son toit offrirait une large terrasse permettant de surveiller les alentours des lieues à la ronde. Il franchit un petit carré abritant encore de nombreuses fleurs, coiffées de pergolas et de plantes grimpantes. Lorsqu'il traversa les lieux, des gamins s'activaient à remplir une énorme jarre de l'eau qu'ils extrayaient du puits au centre.

Il découvrit le cimetière juste après, au plus près de l'église. La présence du corps d'un martyr incitait les religieux les plus fervents à s'assurer une protection fiable dans l'autre monde en se faisant ensevelir à ses côtés. Il y trouva deux tombes récentes. Il avait oublié qu'un chanoine était mort aussi et hésitait donc sur celle où se recueillir. Estimant que Constantin aurait certainement eu soin de garantir une bonne place à l'émissaire de l'archevêque, il opta pour celle qui était la plus proche du lieu saint. Il récita les quelques prières qu'il connaissait, *Pater*, *Ave* et *Credo*, puis adressa une demande particulière à la Vierge, dont il savait l'oreille attentive aux complaintes du commun. Enfin, il alla arracher d'autorité une corolle de fleur qu'il déposa sur la terre fraîchement remuée.

Peu désireux de retrouver tout de suite l'agitation du chantier qui résonnait par les fenêtres non encore obturées de vitraux, il déambula un temps dans les jardins. Un large verger prolongeait la zone d'agrément et plusieurs valets s'employaient à ramasser ce qui serait vraisemblablement les dernières grenades de l'année. Selon la qualité des fruits, ils étaient soigneusement triés dans différentes corbeilles, sous le contrôle d'un latin tonsuré. Ernaut réalisa alors que les chanoines ne s'étaient pas présentés pour chanter. Peut-être qu'en période de travaux, ils bénéficiaient d'exemption. L'exemple d'Herbelot qui avait reçu un échafaudage sur le crâne pendant ses oraisons ne devait guère les motiver à aller célébrer la grâce de Dieu tandis que s'activaient autour d'eux les ouvriers.

Le clerc le salua d'un signe, sans un mot, mais n'abandonna pas son ouvrage. Ernaut savait qu'en plus d'être délicieux, le fruit servait en teinture et Raoul lui avait même montré qu'on pouvait en faire de l'encre, d'un beau noir profond. Avec l'olive et la datte, c'était un des produits miraculeux de l'Outremer. Il leva la tête et renifla le temps. Une nuée grise s'était répandue depuis l'ouest, apportant des relents marins et propageant une fraîcheur renouvelée.

Il fit demi-tour, traversa les carrés aromatiques et médicinaux puis retrouva le sanctuaire. Dans la nef centrale, devant l'escalier qui menait au sous-sol auprès de la tombe, quelques croyants attendaient qu'un domestique les laisse descendre. Ernaut se souvenait de la fois où il était venu avec son frère Lambert. Cela faisait désormais plusieurs années et il ne reconnaissait pas l'homme de faction. Un peu somnolent, il s'appuyait sur un bâton, baillant régulièrement à s'en décrocher la mâchoire. Ernaut comprit bien vite qu'il s'agissait d'un de ces simplets que l'Église prenait sous son aile, incapables de se débrouiller par eux-mêmes, mais aptes à répéter leur vie durant une tâche circonscrite, quoique rébarbative.

De façon instinctive, Ernaut trouva sa place dans la queue, s'attirant des sourires et des salutations. Il estimait qu'il ne serait pas de trop de confier la mémoire d'Herbelot à saint Georges. Vu le gabarit du clerc, il n'était pas inutile de le recommander à un grand guerrier.

### Lydda, cour des Bourgeois, fin de matinée du lundi 15 décembre 1158

Arrivé alors que la séance battait son plein, Ernaut s'était installé au fond, à côté de Raoul qui était venu plus tôt. Il avait chuchoté à Ernaut qu'il était toujours curieux de voir les différentes façons dont les cours opéraient. Ernaut s'étonna d'un tel goût pour des procédures administratives, intrinsèquement rébarbatives. Il en percevait l'importance sans pour autant y trouver le moindre intérêt. Au moins saurait-il qui consulter le jour où il lui faudrait démêler un épineux cas de bornage douteux ou de vol de monture boiteuse.

La salle n'était pas si imposante que celle de Jérusalem, simple cube aux murs blanchis à la chaux, avec un crucifix accroché derrière le vicomte. Lui non plus n'avait guère de points communs avec Arnulf. Si ce n'était ses éperons dorés et son épée, de bonne facture sans ostentation, on aurait pu le prendre pour un artisan prospère ou un négociant peu regardant sur son apparence. Il passait son temps à se gratter et extrayait de temps à autre de sa manche un impressionnant mouchoir dont il se frottait le visage. Il fit néanmoins preuve de bon sens et de tact, remarqua Ernaut, et ses décisions ou recommandations emportaient souvent l'adhésion des présents.

Lorsqu'enfin la séance s'acheva, ils se dirigèrent vers lui, impatients d'en apprendre plus. Maugier du Toron finissait de saluer les jurés, échangeant avec quelques plaideurs des points de détail des procédures. Ernaut comme Raoul étaient familiers de ces scènes, qu'ils voyaient se répéter sans fin chaque semaine à Jérusalem. Raoul aborda le scribe qui rangeait ses tablettes et feuillets dans une vaste cassette en bois marqueté, sans obtenir rien d'autre en retour qu'un reniflement dédaigneux.

« Je suis à vous, mes amis ! »

Le vicomte leur sourit aimablement en s'approchant d'eux. De près, il exhalait une odeur assez forte qui indisposa un peu Ernaut, désormais habitué aux mœurs orientales où l'on célébrait le bain et le savon.

« Je n'ai guère eu de temps au matin, je pensais aller en ville me chercher de quoi rompre mon jeûne. Nous pourrons discuter en marchant si cela vous convient ? »

Sans même attendre la réponse, il attacha son baudrier, ajusta son épée sur ses hanches et se dirigea vers la sortie. Malgré sa petite taille, il marchait à un bon rythme, sans paraître le moins du monde essoufflé par sa forte panse. À chaque fois qu'il croisait une connaissance, il levait sa toaille[^6] en un salut étrange sans ralentir le pas. Désireux de ne pas parler en public, il proposa à Ernaut de patienter qu'ils soient parvenus à destination, où ils pourraient discuter à leur aise. Le jeune homme se rengorgea, flatté de se voir invité à la table d'un vicomte, fût-il peu notable.

Il en fut pour ses frais. Maugier se contenta de bâtonnets de viande dans du pain, rehaussé d'une sauce très poivrée. Il leur en fit l'article, les incitant à s'en procurer, avant de les guider pour s'installer dans la cour d'une maison abandonnée où un bosquet de tamaris avait pris ses aises. Il alla puiser dans une citerne proche et remplit un pichet ébréché qu'il cala entre ses pieds en s'asseyant sur un rognon de mur. Le vicomte n'était apparemment pas homme de faste. Étrangement, il rappelait à Ernaut son père. Il lui parut d'emblée sympathique.

« Je dois avouer être bien aise que vous puissiez vous occuper de cette affaire. J'ai déjà suffisamment de travail avec les plaintes habituelles et les récriminations. Avec la capture du sire Hugues de Rama, les petits malins tentent de tirer leur avantage. Je dois redoubler de prudence.

--- Vous avez enquêté sur le décès du chanoine, c'est cela ?

--- Que voilà bien grand mot ! J'ai surtout veillé à ce que nulle rumeur n'aille enfler. À dire le vrai, vu que personne ne se plaignait vraiment de sa mort, je n'ai pas tant cherché.

--- Mais là, ça a changé ?

--- Cul-Dieu, ça oui. L'archevêque, malgré toute sa bonté, aura grand désir de savoir ce qui est arrivé à son secrétaire. Et lui répondre qu'il prit une planche sur le crâne ne suffira pas, au vu des circonstances.

--- Quelles sont-elles ? »

Maugier avala une grosse bouchée arrosée d'une belle rasade. Ses yeux noyés de graisse ne perdirent guère de vue la nourriture tandis que son cerveau s'agitait.

« De prime, le jeune clerc était venu dans l'espoir d'encontrer le père Waulsort. Apprenant son décès, il proposa au sire évêque d'essayer d'élucider sa mort étrange. De seconde, après avoir fureté qui ci qui là plusieurs jours durant, il encontre son trépas à son tour. De tierce,... »

Son élocution se fit soudain hésitante.

« De tierce, on a appris que le père abbé Wibald, le conseiller du roi des Alemans Frédéric, est mort voilà peu alors qu'il était en mission chez les Griffons[^7]. »

Ernaut n'avait jamais entendu parler de ce personnage et ne comprenait pas en quoi un important ecclésiastique mort à l'autre bout du monde avait à voir avec deux décès autour de la basilique de Lydda.

« Quel rapport avec ce qui nous occupe ici ?

--- Le chanoine Régnier de Waulsort était très lié au père abbé Wibald, ils s'étaient connus dans leurs jouvences. Ils échangeaient nombreux courriers, comme le font les grands savants.

--- Je ne vois toujours pas...

--- Il faut savoir que Régnier était le plus érudit de tous les hommes de l'évêque. Il parlait plusieurs langues, connaissait fort bien de nombreux auteurs, dont les sentences parsemaient son discours. Il avait en charge de définir les programmes des décors ornant le sanctuaire et se montrait toujours désireux d'accroître ses connaissances, sur tous les sujets. De là à penser qu'il aurait mis la main sur un savoir dangereux, il n'y a qu'un pas que nombreux ont déjà fait. »

Ernaut salivait en voyant le gros homme manger et regrettait finalement d'avoir décliné son invitation. Il s'offrit malgré tout une longue rasade d'eau. À ses côtés, Raoul se tenait impassible, ainsi qu'il l'était lors des audiences. Il semblait bien décidé à ne pas sortir de son rôle habituel de scribe, en retrait des débats.

« Et vous, sire Maugier, qu'en dites-vous ? Aurait-il mis au jour un savoir à l'origine de leur mort à tous trois ?

--- Considérant qu'il ne chérissait rien tant que la sapience et le savoir, j'encrois volontiers que la peur ne l'aurait jamais fait reculer.

--- Un parchemin n'a jamais tué, que je sache.

--- Nul besoin, des hommes s'en chargent : pour lire ou pour empêcher de lire. »

Ernaut était soudain bien embarrassé. Il n'était guère à l'aise avec l'écrit, et sûrement pas assez cultivé pour estimer la valeur et la portée d'un texte. Les disputes et chicaneries sans fin entre érudits étaient sujets de moquerie dans les contes populaires et il n'en savait guère plus.

« Qu'espérez-vous de nous, alors ?

--- Oh, moi je n'attends rien. C'est le sire Constantin qui avait besoin de marquer son intérêt envers cette histoire. Pour montrer à ses suzerains qu'il ne prend pas la chose à la légère, mais aussi de façon à calmer les esprits au sein du couvent. Tout cela est hors de mon ressort, même si Régnier est mort en sa maison de ville.

--- Ils ne couchent donc pas au dortoir, tous ensemble ?

--- Cela arrive, et ils sont sensés le faire, mais dans la réalité, tous ont demeure en la cité et y dorment plus souvent qu'ailleurs.

--- Mais comment chantent-ils les heures ?

--- Ils le font chacun chez soi, dans un oratoire voire leur chambre. »

Ernaut comprenait mieux la désaffection du sanctuaire. Le vicomte avala sa dernière bouchée, s'essuya les lèvres et les mains dans ce mouchoir gigantesque qu'il ne lâchait jamais.

« Tout le mystère vient du fait que le chanoine est mort de bien étrange façon, brûlé avec son valet. Si vous arrivez à montrer qu'il n'y a là ni main humaine ni mystère maléfique, rien de plus aisé que de montrer que le pauvre clerc de l'archevêque a simplement joué de malchance, comme cela arrive sur tous les chantiers.

--- C'est là votre pensée ?

--- Je n'ai guère échangé avec le père Régnier, c'était un homme secret, distant. D'autant que nous n'avions rien en commun. Lui tutoyait les plus hautes sphères, perdu dans son savoir. On venait de loin pour apprendre de lui, ce que fit votre ami, d'ailleurs. Je ne serais guère étonné qu'il ait libéré une force qu'il pensait contrôler et y succomba. Dire ce qu'était cette chose est au-delà de mon pouvoir. Je ne suis qu'un modeste chevalier, plus habitué à percer le poing de larrons ou à faire payer l'amende qu'à étudier des savoirs inconnus.

--- Nul chanoine ne saurait nous épauler en cela ?

--- De ce que j'en sais, aucun ici ne peut rivaliser avec ce grand esprit. Ce qui n'est pas sans entraîner certaines jalousies, comme vous pouvez le deviner.

--- L'un d'eux aurait pu tuer pour cela ?

--- Je ne sais. Ils me semblent tous bien inoffensifs, à défaut d'être sans rancœur. »

Il se leva, épousseta sa cotte froissée, attendit de voir s'il demeurait des questions puis les invita à le suivre. Ils retournèrent en silence en direction de l'imposante forteresse. En comparaison, la ville était modeste, avec des bâtiments de rarement plus d'un ou deux étages. La brique crue y était fréquente, recouverte généralement de chaux. Des poutres grossièrement équarries dépassaient des façades, soutenant le toit léger, souvent de simples feuillages. Partout des palmiers élançaient leur tronc balançant doucement dans la brise. D'innombrables puits et citernes apportaient toute l'eau désirée et de petits jardinets fleurissaient ici et là, tranchant d'émeraude la terre grise des chemins.

Aucune logique ne semblait avoir présidé à l'organisation des quartiers et les rues serpentaient, s'affinant fréquemment en fines venelles avant de déboucher sur des places accueillant les marchands et les artisans. La cité était assez animée, les boutiques proposaient des denrées venues parfois de très loin. La présence de pèlerins était moins prégnante qu'à Jérusalem, la vie était plus tournée vers le commerce. La verdoyante zone de jardins qui s'étendait au sud jusqu'à joindre celle englobant Rama fournissait largement les marchés en légumes et fruits frais. Les ports proches apportaient des poissons de mer qu'Ernaut ne voyait que bien rarement à Jérusalem, et de plus à des prix trop élevés pour lui. L'endroit lui donnait une impression d'opulence et d'affairement qu'il n'avait plus guère éprouvée depuis des mois. Il se prit à envisager son installation ici, une fois marié. Loin des frontières, le lieu offrait toute la sécurité dont on pouvait rêver. Les murailles n'en étaient pas très imposantes, mais suffisantes pour repousser des pillards ou une chevauchée.

Lorsqu'ils retrouvèrent la porterie du palais épiscopal, le vicomte s'arrêta près du bâtiment abritant les différentes administrations civiles.

« Allez donc voir le jeune valet qui accompagnait votre ami. Il saura vous guider sur les différents lieux. Les gens le connaissent déjà. Si jamais vous avez besoin de moi, les sergents d'ici sauront me trouver. »

Quand il se fut éloigné, Raoul lança un regard complice à Ernaut.

« Je ne sais pas toi, mais pour ma part, le voir engloutir son en-cas m'a ouvert l'appétit. Que dirais-tu de passer par le réfectoire ? On y dénichera peut-être quignon à grignoter. »

Chapitre 2
----------

### Lydda, réfectoire du palais, après-midi du lundi 15 décembre 1158

Une fois leur repas avalé, Ernaut et Raoul demandèrent après le valet qui accompagnait Herbelot. On leur indiqua un jeune garçon, encore installé à table, occupé à dévorer avec gourmandise un plat à base de purée de lentilles qu'il étalait sur du pain. À le voir agir ainsi, on aurait pu croire qu'il n'avait pas mangé depuis des jours, ce que démentait sa physionomie replète. Les voyant s'approcher, il ralentit son rythme, accordant à Ernaut des regards un peu inquiets.

« Le bon jour à toi, l'ami. On nous a dit que tu étais valet d'Herbelot. Est-ce vrai ?

--- Je suis au service de l'archevêque, mais je me consacrais tout particulièrement au père Herbelot. »

Ernaut lui adressa un sourire, qu'il espérait avenant, et s'installa à côté de lui, à califourchon sur le banc. Raoul alla faire le tour de la grande table.

« Nous aurions besoin de ton aide. Je suis Ernaut, de Jérusalem et voici Raoul. Nous sommes là pour en savoir un peu plus sur ce qui s'est passé ces jours derniers. »

En entendant Ernaut rompre le voile de secret sur lequel comptait l'évêque, Raoul s'empressa d'ajouter :

« Ne t'avise pas de bavarder à ce sujet, nul ne souhaite que l'inquiétude se propage. Si on te demande, tu diras qu'on t'a questionné sur les avis de l'hostel[^8] du patriarche vis-à-vis du chantier. »

Le jeune homme adoptait de plus en plus l'attitude d'un lapin devant des prédateurs. Il écarquillait de grands yeux, déglutissait et mâchait lentement. Il considéra le réfectoire presque vide autour d'eux. Ernaut attrapa le pichet, en renifla le contenu et le reposa.

« N'aie nulle crainte, je comptais Herbelot comme un mien ami, c'est ce qui motive mes questions. Quel est ton nom ?

--- Germain.

--- Très bien. Alors, Germain, connaissais-tu bien Herbelot ? T'avait-il fait quelque confidence sur ce qu'il faisait ici ?

--- Le père Herbelot était homme de bien. J'avais grande estime pour lui, mais je n'étais que son valet, nullement son confident.

--- Tu n'as donc aucune idée de ses entreprises en ce lieu ?

--- Il était d'une grande érudition, je ne saurais rien dire de ses activités. Il travaillait depuis des mois à une grande œuvre qui l'enthousiasmait et il espérait encontrer un des chanoines ici pour en discuter. Las, celui-ci était mort, de façon bien étrange, alors le père Herbelot a entrepris d'en savoir plus.

--- Tu vois que tu peux nous être de grande utilité, l'encouragea Ernaut aimablement. Saurais-tu nous montrer l'endroit où ce clerc est mort ? On m'a dit qu'il n'était pas dans le dortoir ni même dans le couvent.

--- Si fait, il possédait belle demeure en la ville, nous y sommes allés plusieurs fois avec le père Herbelot. »

Son assurance renaissant, son rythme d'ingestion s'était accéléré et chaque phrase s'accompagnait de flocons de lentilles ou de miettes qui volaient sur la table. Il semblait bien déterminé à finir sa collation avant de décoller du banc.

« Par le plus grand des hasards, tu n'étais pas avec Herbelot quand... l'accident s'est déroulé ? »

Les yeux emplis de crainte, Germain secoua la tête, la bouche trop pleine pour s'exprimer. Il termina rapidement d'avaler ce qui traînait devant lui puis se leva. Il n'était pas très grand et peu vigoureux, plutôt indolent. Il lissa ses cheveux bruns frisés en arrière avant de coiffer un petit bonnet de feutre. Frottant les ultimes miettes de sa cotte, il signifia d'un coup de menton qu'il était prêt à y aller.

Tout en traversant les communs du palais à ses côtés, Ernaut en profita pour l'étudier à la dérobée. Il avait cru un instant que le jeune garçon, qui devait avoir quelques années de moins que lui, était un Syriaque. Il s'exprimait avec un accent, avait le teint mat et les traits assez orientaux. Pourtant il s'habillait à la façon franque, cotte et chausses et portait un prénom indéniablement latin. Cela rappela à Ernaut le valet de l'hôpital de Jérusalem dont la passion consistait à identifier l'origine des gens à leur simple tenue[^9]. Il aurait apprécié de posséder pareil talent en cet instant.

Ils traversèrent quelques quartiers, d'abord artisanaux puis en partie d'habitation. Ils aboutirent dans une ruelle devant une petite porte à guichet qui ouvrait dans une belle demeure à étage. Chaulés peu auparavant, les murs étaient coiffés de décors triangulaires. L'endroit offrait une impression de prospérité. Ernaut interrogea Germain du regard.

« Nous y sommes, c'est la demeure du chanoine.

--- Le lieu ne semble guère avoir connu d'incendie. Est-ce bien là qu'il a rendu son âme à Dieu ?

--- De certes. J'y suis venu quelques fois compaigner le père Herbelot. Mais il n'y a nul dommage.»

Ernaut se gratta la tête. Malgré ce qu'avait dit l'évêque, il s'était attendu à découvrir au moins quelques traces calcinées et il se trouvait devant un beau bâtiment, dont la porte était fermée.

« Où donc le chanoine est-il mort, alors ?

--- Il a brûlé ici, tout comme son valet.

--- Le lieu me semble en bien bel état pour avoir été mortelle fournaise il y a peu. Ils n'ont tout de même pas reconstruit si vite.

--- C'est bien là qu'on les a trouvés, on l'a affirmé sans nul doute au père Herbelot.

--- Comment auraient-ils pu brûler sans qu'une partie de la maison ne s'embrase peu ou prou ? »

Ernaut se gratta le front avec plus de vigueur. Il était absurde que deux hommes puissent se consumer sans que rien d'autre n'ait été touché, même si l'incendie avait été par la suite maîtrisé ou se soit éteint par lui-même.

« Quelqu'un a dû bouger les dépouilles, ce n'est pas possible autrement.

--- De certes, ce n'était pas possible, leurs... dépouilles étaient bien trop abîmées. C'est bien ce qui enfrissonne chacun ici : seuls les deux hommes avaient brûlé. Rien d'autre autour d'eux. »

Raoul et Ernaut le dévisagèrent comme s'il avait trop bu et proféré un juron sacrilège.

« Tu es en train de me dire qu'un feu mortel n'a touchés qu'eux deux et pas le moindre meuble ?

--- Il semblerait. Je n'ai guère usage de pareilles choses. Mais de leur mort par brûlure, je suis acertainé. Nous avons vu les corps avant qu'ils ne soient portés en terre. »

Germain en frissonna d'horreur. Ernaut lança un regard désolé sur la porte. Il commençait à comprendre pourquoi les esprits s'échauffaient à la mention de cette histoire. Quoi qu'il en soit, pour le moment, cette découverte le contrariait plus qu'elle ne l'inquiétait, car au lieu de parcourir des ruines en quête de traces, il se trouvait bloqué par une serrure verrouillée. Il ne lui restait plus qu'à retourner auprès du vicomte, en espérant obtenir la clef de l'endroit.

Ils rebroussèrent chemin rapidement, réussirent à retrouver Maugier, qui les informa que l'intégralité du contenu de la maison avait été confiée au doyen des chanoines. C'était lui l'exécuteur testamentaire du défunt et il avait en conséquence la garde de ses biens le temps de voir si on pouvait identifier des héritiers. Ils repartirent donc en quête de l'ecclésiastique, dont on leur indiqua qu'il était le plus souvent dans la pièce de réunion du chapitre, où il avait installé son bureau. Estimant qu'il n'était pas nécessaire de s'encombrer d'un témoin, Ernaut proposa à Germain de le retrouver plus tard dans la chambre qu'il avait partagée avec Herbelot.

La salle capitulaire s'étendait à l'est du petit cloître, son entrée marquée d'une belle porte bordée d'arcades fines. La pierre récemment taillée avait reçu un décor multicolore de motifs géométriques. Sur le tympan surmontant l'huis, une fresque avait été commencée, avec des visages entourant la scène centrale où, très certainement, le Christ ou un saint d'importance, sûrement saint Georges en l'occurrence, allait être figuré. L'huis s'ouvrit sans bruit sur une pièce lumineuse, avec de nombreuses baies ornées de châssis de verre. Des bancs en occupaient la majeure partie, ainsi qu'un lutrin et un trône aux couleurs vives, sous un crucifix monumental. À l'opposé, un espace avait été aménagé en bureau, avec des braseros pour réchauffer l'atmosphère. Plusieurs malles étaient disposées sous des niches closes de volets, creusées à même les murs en pierre de taille. Un large plateau était encombré de volumes et de rouleaux divers, plusieurs encriers, des plumes et des stylets regroupés dans un coffret.

Les bras croisés sur les documents et la tête appuyée dessus, un vieil homme ronflait tranquillement. Son crâne squelettique ne possédait plus que des souvenirs de sa chevelure et, avec sa peau blanche, donnait l'impression que c'était un œuf abrité en un nid d'étoffes sombres bouillonnant autour de lui. Il ne bougea pas à leur entrée. Ernaut se rapprocha en traînant les pieds, espérant que cela suffirait à tirer le dormeur de ses rêves, mais rien n'y fit. Raoul tenta de tousser à plusieurs reprises, sans plus d'effet. Amusé, Ernaut finit par tendre la main vers un épais volume relié de cuir et de bois, posé sur la table. Il le souleva légèrement et le laissa retomber sur le plateau, non sans faire un grand pas en arrière dans le même mouvement.

Le livre claqua d'un bruit sourd qui résonna dans la haute pièce tout en projetant un fin nuage de poussière. Le vieil homme releva la tête, affolé. L'air innocent, Ernaut fit mine de tousser discrètement. Le doyen tenta d'accommoder les deux ombres devant lui, se racla la gorge et se frotta le nez avant d'arriver à sortir un mot intelligible. Sa voix chevrotante mangeait certains sons, l'absence de nombreuses dents lui faisant avaler ses lèvres à chaque inspiration.

« Qui vient donc m'interrompre dans mes réflexions ?

--- J'ai nom Ernaut, de Jérusalem, et voici Raoul, scribe de la cour. Nous sommes ici à la demande du sire évêque, pour l'affaire qui vous occupe. »

Le vieux chanoine, la joue encore déformée par la sieste, farfouilla parmi les documents devant lui, comme s'il avait espoir d'y trouver réponse à ses interrogations. Ses yeux clignaient, tentant d'éliminer les larmes abondantes qui suintaient sur ses paupières. Il les essuyait d'une main osseuse où un chaton rubis scintillait sur un anneau doré. Il parvint finalement à retrouver une contenance et invita les deux jeunes gens face à lui à s'asseoir.

« Le sire évêque Constantin nous a parlé de vous, au matin. Je pensais vous voir plus tôt.

--- Il nous a fallu savoir où en était rendu le vicomte Maugier avant toute chose. Inutile d'aller cheminer en sentiers qu'il avait déjà battus. Nous sommes d'ailleurs ici pour...

--- Certes. Certes. Cependant, j'encrois qu'il n'aurait guère su prospecter là où il le faudrait. Régnier de Waulsort était homme de grande sapience, de sûr appelé à s'asseoir en mon siège le moment venu. Il se rendait en esprit là où bien peu ont capacité à cheminer.

--- Y voyez-vous un lien avec sa mort ?

--- Comment cela pourrait être autrement ? Son monde était d'images et de symboles qui éveillent en nous bien terribles puissances. Son incommensurable finesse d'esprit le portait à toutes les vanités, je le crains. Il se perdait lui-même, à parcourir des auteurs oubliés à juste titre. Ou s'ouvrait à des traditions qui polluèrent sa vertu, je le perçois désormais. De justes intentions ne suffisent pas toujours à garantir des écueils. »

Ernaut fit la grimace. Le doyen Élies cherchait-il à lui expliquer que le chanoine avait cédé à des influences étrangères, qu'il professait une religion déformée par de fausses croyances ? Herbelot l'aurait suivi en tel chemin ? Les inanes subtilités des théologiens le laissaient de marbre et il n'y voyait que spécieux bavardages, incompréhensibles et inutiles pour le commun, dont il était. Pourtant, si cela était à l'origine des morts, il lui fallait en découvrir le pourquoi, ou du moins avoir des pistes à proposer à l'hôtel du roi. Il comptait bien impressionner ses commanditaires par la pertinence de ses investigations. Il s'abandonna à une certaine effronterie dans sa réplique.

« Je ne vois guère en quoi cela aurait à voir avec sa mort ou celle de son valet, et encore moins avec celle d'Herbelot. Le feu ne prend pas spontanément sur un corps.

--- Dieu peut punir de bien des façons ou les démons se déchaîner à leur gré. Régnier a commis un grand péché en se croyant au-dessus de sa condition. Il en a payé le prix. Pour le jeune secrétaire, je ne saurais dire, mais il m'a semblé lui aussi farci de suffisance intellectuelle. Peut-être a-t-il succombé aux mêmes travers ? Ou alors Dieu y aura mis bon ordre, dans sa grande mansuétude, avant qu'il ne mette son âme en état de péché mortel. »

Tout en parlant, le vieil homme frappait de ses phalanges le plateau de bois, martelant chaque mot de sentencieuse façon. À la fin, il ravala la salive envahissant sa bouche édentée, fixant les deux enquêteurs face à lui.

« Soyez bien prudents, mes enfants. Ne risquez en rien de vous aventurer en funestes lieux. Nous ne sommes rien devant les forces qui s'affrontent autour de nous. »

Il esquissa une timide bénédiction, marquant par là son soutien et son inquiétude tout en invitant peut-être à leur départ.

« J'aurais une ultime requête à vous soumettre, si vous le permettez.

--- Le sire évêque nous a clairement indiqué de vous aider en vos entreprises. Demande donc, mon enfant, je ferai de mon mieux.

--- Pourriez-vous nous confier la clef de la demeure du chanoine ? Nous n'y toucherons rien, j'en fais serment. Je pense malgré tout que cela serait utile. »

Dodelinant de sa tête parcheminée, le doyen acquiesça. Il fouilla dans les cassettes autour de lui et lança sur les documents devant Ernaut une grosse clef en fer, dont l'anneau était formé de deux serpents stylisés se faisant face.

### Lydda, hôtellerie du palais épiscopal, fin d'après-midi du lundi 15 décembre 1158

Ernaut et Raoul n'eurent aucune peine à retrouver Germain dans le bâtiment accueillant les visiteurs. Il n'y avait pas grand monde en cette période de l'année. Cela changerait certainement à l'approche de Noël, qui serait le moment où banquets et célébrations seraient l'occasion de rassemblements dans les lieux de pouvoir. Le roi ayant emmené avec lui la majeure partie de la Haute Cour dans le nord afin de rencontrer le basileus byzantin, les nobles de moindre importance auraient toutes latitudes pour recevoir leurs obligés dans leur propre demeure.

Germain occupait une cellule identique à celle de Raoul et Ernaut. Il avait simplement entassé toutes les affaires dans la niche qui avait dû servir au clerc, et s'était ménagé un nid douillet dans son alcôve, avec le plus de couvertures qu'il avait pu trouver. Il semblait d'ailleurs en train de somnoler quand il accueillit les deux enquêteurs. D'autorité, Ernaut s'assit sur un des petits bancs tandis que Raoul s'installait sur un coffre.

« Alors, mon ami, as-tu pourpensé à des choses qui pourraient nous être d'utilité ?

--- Je ne sais guère...

--- Il est possible que le chanoine ait professé des choses étranges, peut-être était-il détenteur de savoir secret ? Herbelot t'a-t-il parlé de cela ?

--- Nullement. Nous ne parlions guère de choses en dehors de mon service. Mais si quelqu'un pouvait découvrir croyances erronées ou pratique vicieuse, c'était bien le père Herbelot.

--- Était-il là pour ça ?

--- Aucune idée. Il semblait plutôt impatient de pouvoir encontrer le chanoine. Il avait d'ailleurs emporté de nombreux sacs emplis de ses propres recherches. Habituellement il ne prend pas tout cela pour porter missive. »

Ernaut lança un coup d'oeil à l'amoncellement de sacoches et de besaces accumulé sur le matelas. Il y avait là un bon nombre de documents, tablettes, rouleaux et codex. Si le chanoine détenait ou professait un savoir interdit, Ernaut n'arrivait pas à imaginer Herbelot se fourvoyer en pareil chemin. Il était plus probable qu'il aurait cherché à le raisonner, et peut-être était-ce son intention première. Constatant le décès, il aurait pu être tenté de contacter d'autres égarés afin de les admonester. Ernaut se souvenait très bien que cela constituait une des activités favorites du petit homme. De là à imaginer qu'une main malveillante avait dénoué les liens tenant l'échafaudage...

Ernaut se doutait que ces territoires orientaux regorgeaient de pratiques hérétiques, voire païennes. Lui-même était bien incapable d'y reconnaître clairement les différents groupes. Il ne s'identifiait d'ailleurs comme catholique que par le versement de la dîme, la récitation des trois prières qu'il connaissait et sa fréquentation assez régulière de la messe. Où il ne comprenait rien, mais qui lui offrait l'occasion de voir ses amis, de discuter avec eux le temps que les officiants exécutent leur liturgie. Enfin, il honorait un certain nombre de saints, dont il reconnaissait la compétence pour retrouver un objet perdu, soigner un proche ou exaucer un vœu. Avec une efficacité toute relative.

Raoul l'interrompit dans ses réflexions, en se dirigeant vers le tas d'affaires entreposées sur la couche.

« Verrais-tu objection à ce que nous regardions dans les bagages du clerc ?

--- C'est que je ne sais si le père Herbelot aurait accepté...

--- C'est justement pour apaiser sa mémoire que je te le demande. Il est possible qu'il ait noté ici ou là ce qu'il avait en tête. Nous serons alors plus à même de savoir si sa mort est fâcheux accident ou maligne meurtrerie. »

Incapable d'opposer un argument construit, Germain plissa plusieurs fois la bouche sans oser apporter contradiction. Finalement, il haussa les épaules et hocha la tête. Raoul alla jeter un coup d'œil aux différents bagages et en sortit une grande quantité de tablettes, de papiers et livrets. Il soupira en réalisant que lire tout cela prendrait beaucoup de temps, même à un habitué comme lui. Il déroula quelques cylindres, parcourut rapidement des panneaux de cire. Il constata avec soulagement que le clerc ne faisait pas usage d'autre chose que le latin. Il était déjà laborieux pour lui de le déchiffrer et il avait craint de se voir confronté à des langues plus exotiques encore, étant donné l'image érudite qu'il se faisait du personnage.

« Je reviendrai avec une lampe pour étudier cela. Il est fort possible que nous y trouvions les obscurs motifs qui se trament derrière les événements.

--- Je suis bien aise que tu sois là, approuva Ernaut. Je n'ai nul désir de devoir pénétrer les mystères de verbiages en latin. »

Il se tourna de nouveau vers Germain.

« N'as-tu vraiment aucun souvenir sur ce que ton maître voulait découvrir ici ? Quelque remarque qui lui aurait échappé ?

--- Il était impatient de discourir avec le chanoine, mais je ne saurai dire de quoi. La découverte de son trépas l'a fort déçu.

--- A-t-il découvert dans les affaires de ce dernier ce qu'il espérait ? Ou, au contraire, en a-t-il éprouvé contrariété ?

--- Il a passé beaucoup de temps en sa demeure, mais je n'y étais pas. Il a aussi étudié assez longuement son appentis dans la loge.

--- De quoi me parles-tu ?

--- Le chanoine était en charge de définir les modèles de tous les décors du sanctuaire. Il avait donc aménagé un espace dans le chantier où tracer ses cartons, donner ses directives aux ymagiers. »

Ernaut lança un regard agacé vers Raoul.

« Voilà nouvelle que j'aurais bien aimé apprendre plus tôt ! Pourquoi nous l'a-t-on cachée ?

--- C'est de bonne fame, mestre. Personne ici n'ignore cela.

--- Saurais-tu m'indiquer où trouver cette loge ?

--- Vous ne sauriez la manquer, un petit cabanon adjoint à l'atelier des sculpteurs. Vous y trouverez toutes les esquisses. »

Au-dehors le crépuscule approchait et Germain ne semblait guère motivé à l'idée de quitter son petit nid. Sauf peut-être pour se rendre au réfectoire. Ernaut se leva, espérant avoir le temps de reconnaître le lieu avant que l'obscurité ne s'installe tout à fait. Voyant que Raoul rangeait les documents dans les sacs, il l'attendit pour sortir et abandonna le valet à ses préoccupations, ou à sa sieste apéritive.

Lorsqu'ils quittèrent l'hôtellerie pour rejoindre la basilique, les trilles d'un tarier les accompagnèrent un moment. Le chantier était pratiquement désert, offrant aux pèlerins l'occasion d'enfin prier au calme. Le soleil noyé de nuages ne lançait guère de feu et les ombres se propageaient rapidement. Seuls les autels accueillaient une lumière dorée, faisant scintiller les décors récents.

Ernaut et Raoul trouvèrent sans peine la loge des sculpteurs, dont un emplacement accueillait des profils découpés, des esquisses tracées à la craie et des motifs grossièrement ébauchés. On y reconnaissait un visage angélique ou un faciès grotesque, des entrelacs végétaux et des ornements géométriques. Dans un coin étaient empilés des chapiteaux de marbre à volutes d'acanthe, fortement endommagés et de tailles diverses. Un fut de colonne ancien était encombré d'un tas de paniers usagés, emplis de fusains, règles et compas. Il y traînait également une corde à nœuds.

Ernaut prit sur une étagère un des panneaux laissés à plat. Il y découvrit un tracé végétal assez sobre, le charbon repassant proprement par-dessus des ébauches faites à la craie. Tout en faisant une moue appréciatrice, il reposa le modèle. Raoul fouillait parmi les études découpées servant certainement à reporter les décors sur la pierre.

« C'est étrange, il n'y a là que les projets pour les sculptures, dont la plupart semblent sans histoire. Il y a pourtant bien plusieurs récits représentés dans les mosaïques et les fresques. Ne serait-ce que celle de la salle capitulaire.

--- Tu parles de raison. Il devait ranger ailleurs ses projets les plus complexes. Demain nous irons à la première heure voir sa demeure.

--- Je te propose de parcourir les notes du père Herbelot, pour ma part. Chacun de notre côté, nous avancerons plus vite. »

Ernaut acquiesça en silence. Les ténèbres mangeaient peu à peu les formes autour d'eux, noyant de pénombre les interstices avant de se répandre en filet dans toute la loge. Avec la nuit, deux valets vinrent faire le tour des autels, remplissant d'huile les lampes, mouchant les rares cierges pour éviter qu'ils ne fument trop. Puis ils disparurent derrière une petite porte. Peu après, la cloche résonna de l'appel à la prière. Quelques pèlerins se relevèrent de leurs oraisons et s'installèrent dans la nef, désireux de ne pas perturber le recueillement des chanoines. Ernaut et Raoul choisirent de s'esquiver. Ils passèrent dans le cloître, croisant à peine une demi-douzaine de clercs en tenue liturgique. Se penchant vers Raoul, Ernaut remarqua que la discipline était bien relâchée dans cet édifice saint.

« Peut-être qu'avec les travaux, leurs offices en sont chamboulés.

--- Tout de même ! Cela en fait bien peu. Combien sont-ils, comme le chanoine mort, à avoir hostel en ville et, peut-être, occupation plus passionnante que leur devoir de bon moine ?

--- Cela les regarde. Je ne crois pas que tous les chanoines vivent selon la même règle.

--- Je les connais bien, ces oiseaux-là. C'est comme chez moi, à Vézelay. Ils vivent comme des comtes, oublieux de leurs devoirs. »

Ernaut tourna la tête de droite et de gauche, comme s'il cherchait une proie à poursuivre.

« J'ai grand désir de demeurer ici, pour leur retour. Aux fins de questionner les frères. Ceux qui sont là doivent être les plus disciplinés. Peut-être lâcheront-ils quelque ragot sur le défunt ?

--- On ne médit pas des morts, Ernaut, lui rétorqua Raoul, les yeux emplis d'effroi. Ça porte malheur !

--- Je n'en ai nul désir. J'ai juste besoin de savoir qui était ce Régnier, pour pouvoir expliquer sa mort et celle d'Herbelot. Attendons à la sortie de la basilique et voyons si certains bons frères seront loquaces. »

Ils patientèrent en vain, les ecclésiastiques étant peu désireux de bavarder, ils esquivèrent leur approche pour se rendre sans bruit dans la salle capitulaire. Leur réunion quotidienne se déroulait après l'office et avant leur repas, qu'ils prendraient en commun dans leur réfectoire. Son estomac se rappelant à lui à cette occasion, Ernaut se résigna à aller avaler son propre souper quand il apprit que les clercs avaient usage, en fin de journée, de se retrouver dans le cloître pour échanger plus librement pendant la veillée. Il pourrait alors s'entretenir avec eux.

### Lydda, hôtellerie du palais épiscopal, veillée du lundi 15 décembre 1158

La soirée fraîchissait et la pluie ne tarderait certainement pas. Un vent léger, mais régulier, poussait les nuages depuis les rivages, à l'ouest, transformant le ciel en une voûte uniformément noire. Après avoir avalé un épais brouet de légumes, Raoul et Ernaut allèrent chercher une tenue de circonstance, enfilant leur chape de voyage. Ils en profitèrent pour s'enquérir d'une lampe auprès du frère guichetier, qui leur remit de quoi éclairer leur chambre pour la nuit. Une veilleuse, à laquelle ils pourraient prendre leur feu, était maintenue allumée dans le couloir. Au-dehors, quelques lanternes vacillaient dans les mains de ceux qui s'aventuraient dans le froid et les ténèbres.

Au moment où ils franchirent la cour, ils assistèrent au départ de cavaliers chaudement emmitouflés. Le garde de faction maugréait fort d'avoir dû ouvrir la grande porte. Il replaça les barres et verrous avec soin, avant de remettre la clef au chevalier responsable du guet pour la nuit. La scène rappela des souvenirs à Ernaut, qui devait parfois s'acquitter de telles tâches. À Jérusalem, ces corvées de surveillance étaient confiées aux bourgeois de la ville, sous la tutelle d'hommes du roi. Les portes étaient généralement sous la garde de professionnels afin d'éviter la corruption.

Avec le temps maussade, Ernaut ne fut guère étonné de ne pas voir beaucoup d'activité dans le cloître. Dans un des coins, un clerc était entouré de marcheurs de la Foi. Il était certainement en train de leur raconter les légendes qui se rattachaient à saint Georges, dont la dépouille était conservée dans la crypte. Un petit groupe s'entassait autour d'une table de jeu, où deux frères poussaient des pions aux mérelles. Emmitouflés dans leurs épaisses robes de laine sombre, ils formaient un amas indifférencié où seules les taches claires de leurs visages bougeaient, ornées du feu des lampes.

Raoul et Ernaut s'approchèrent sans un mot, faisant mine de s'intéresser à la partie. Un des clercs les reconnut et les salua d'un signe de tête, avant de se décider à s'avancer vers eux. Il s'exprimait doucement, dans un chuintement d'habitué au silence. Le menton massif, les traits grossiers et le nez assez imposant, sa tonsure semblait lui avoir laissé une bande de chaumes raides et hérissés autour du crâne. Tout en parlant, il se grattait sans cesse l'oreille comme si une puce y avait élu domicile.

« Êtes-vous les hommes du patriarche ?

--- Pas exactement, mais nous avons été envoyés à sa demande.

--- Je ne vois nulle tonsure, êtes-vous chevalier ?

--- Il a été retenu au dernier moment, nous avons été mandés pour préparer sa venue. »

Le clerc émit un grognement tout en détaillant les deux plénipotentiaires, des pieds à la tête. Il ne parut guère convaincu de ce qu'il examinait, car il tourna alors la tête, semblant découvrir un regain d'intérêt à la partie. Vexé de se voir ainsi mis à l'écart par un avorton qui lui arrivait à peine à l'épaule, Ernaut se rapprocha de lui.

« J'ai nom Ernaut, de Jérusalem. Puis-je savoir si vous êtes clerc du collège des chanoines ? »

L'autre consentit à le fixer de nouveau, d'un air bien moins aimable. Il hocha la tête et répondit avec un soupçon de préciosité dans la voix, le visage déformé d'une moue pincée.

« Je suis le père Heimart, trésorier.

--- Je suis bien aise de faire votre connaissance, mon père. Vous deviez être fort lié avec le père Régnier, alors ? »

Décontenancé par cette déduction, le clerc ouvrit de grands yeux et bafouilla un peu, abandonnant de sa superbe. Un de ses collègues, à côté de lui, en eut un sourire de plaisir. Ernaut nota mentalement d'interroger ensuite le goguenard.

« Notre frère Régnier de Waulsort n'avait nulle fonction proche de la mienne.

--- N'était-il pas grand érudit, célébré pour son vaste savoir ?

--- Certes.

--- J'ai naturellement pensé que, vu votre charge, vous deviez avoir beaucoup de choses en commun avec lui. Comme chacun sait, votre poste est d'importance. Outre, il devait échanger avec vous des différents objets nécessaires au culte, vu que vous en avez la garde. »

Appréciant qu'on lui accordât de la valeur, le père Heimart se rengorgea, se laissant aller à arborer un sourire. La brute épaisse face à lui savait reconnaître les gens d'importance, ce qui le mit en confiance.

« Nous avions certes parfois des échanges autour de nos activités respectives, mais je ne saurais prétendre que nous étions proches. »

Il se tut un moment, se gratta le menton tandis qu'il s'efforçait de préparer ce qu'il voulait dire. Il mit un peu de temps à trouver.

« Nous n'avions guère en commun. Il ne faut certes pas médire d'un mort, mais Régnier avait parfois bien étranges lubies et des idées définitives qu'il n'acceptait guère de voir questionnées.

--- Tu peux carrément dire qu'il n'en faisait qu'à sa tête, aussi dure qu'une caboche de mule, enchérit un des frères assis devant lui, tout en farfouillant dans son imposante barbe.

--- Tout de même, père Ascelin, je vous trouve bien âpre en vos jugements. »

L'autre gloussa et tapa le plateau de ses phalanges.

« Plus dure que ça, oui. Jamais il n'a changé d'un iota ce qu'on lui demandait.

--- Tout de même ! Comme tu y vas ! Aucun décor n'a été fait sans l'aval du chapitre, tout de même.

--- Ah, parlons-en. Il nous laissait faire nos récriminations, hochait la tête puis poursuivait sans tenir compte d'aucun de nos avis. »

La partie fut rapidement délaissée au profit des échanges autour de Régnier de Waulsort. Le brouhaha monta si vite qu'il attira l'attention des pèlerins à l'autre bout du cloître, surpris de se voir ainsi interrompus par des ecclésiastiques. Ernaut leva les mains en signe d'apaisement. Il ne lui était d'aucune utilité que chacun se dispute avec tous. Il voulait des informations précises, pas des imprécations lancées dans le tumulte.

« Je vous en prie, pères, je ne voulais pas vous ébranler pareillement. Je n'aurais jamais cru que le père Régnier était ainsi discuté.

--- Nul ici ne doute qu'il était homme de bien et de grande sapience. Mais Dieu a oublié de lier cet esprit brillant d'une bonne once de modestie. Il s'ensuivait qu'il avait tendance à faire à son idée, en toutes choses, précisa le père Ascelin.

--- Lorsque les travaux ont été entrepris, il s'est trouvé naturellement qu'il serait en charge de discuter des décors avec les ymagiers. Par la suite, il en a déduit qu'il serait seul à en décider, ajouta Heimart. »

Les autres acquiescèrent bruyamment. Si le défunt avait eu quelque talent, ce n'était certes pas celui de se faire des amis. Aucun de ceux qui se trouvaient là, qui l'avaient fréquenté ces dernières années, ne semblait avoir le désir de prendre sa défense. Ernaut en conçut de la gêne. Cela voulait dire qu'il y avait sûrement beaucoup de gens qui avaient apprécié sa disparition, voire qui l'avaient souhaitée. Autant de suspects en puissance. La petite voix de Raoul le surprit alors qu'il réfléchissait à tout cela.

« Est-ce à dire que vous n'approuvez pas les décors disposés en le sanctuaire ? »

Un silence embarrassé accueillit la question. Ce fut le doyen qui osa répondre de sa voix chevrotante.

« C'est plutôt que nous aurions aimé qu'il soit moins directif dans les choix qu'il nous proposait. Il aimait à chercher des idées neuves, sans nul besoin. Il avait un goût malsain pour des choses... exotiques. »

Plusieurs hochements de têtes approuvèrent la déclaration, laissant les deux enquêteurs dans l'embarras. La piste des croyances hérétiques semblait se dessiner devant eux. Le père Heimart comprit où de telles déclarations pouvaient mener, car il se hâta de reprendre la parole.

« Tout ce qu'il mit en place était malgré tout conforme à nos usages. Rien d'irrévérencieux ou de mal à propos. Le sire évêque ne l'aurait jamais permis, pas plus que le chapitre !

--- Tu parles, si on l'avait écouté, il aurait figuré son satané serpent sur tous nos murs ! En quoi billevesées orientales concernent la vraie foi ?

--- Cela n'a pas été, père Adam. Et cela ne sera jamais ! » rétorqua Heimart, conciliant.

La remarque agaça le vieil homme au visage épais et cireux qui les fixait de ses yeux noyés dans de grasses paupières.

« N'ai-je pas tenté à de multiples occasions de le ramener à la raison ? Il savait bien qu'il heurtait nos plus intimes croyances, mais il n'en avait cure ! »

Revivant les contrariétés dont il avait souffert, le clerc se rembrunit et fit disparaître le bas de son visage dans les replis de son habit, bien décidé à maugréer dans son coin à propos de tous les malheurs qu'il avait subis. Un des joueurs profita du silence pour poser un de ses pions d'un air enthousiaste, attirant de nouveau l'attention du groupe. Le père Heimart invita Ernaut et Raoul à l'accompagner un peu plus loin, puis s'assit sur un des murets soutenant les décors de colonnettes.

« Vous le voyez, de grands maux n'ont pas encore trouvé leur cure, dans cette affaire.

--- Avait-il des pratiques si répréhensibles ?

--- Certes pas, le père Régnier était homme de foi, sa croyance était sincère et véritable. C'était juste qu'il s'enthousiasmait très vite des découvertes qu'il faisait. Il ne prenait guère le temps de voir si cela ne heurtait pas les traditions en vigueur ici.

--- Mais qu'avait-il prévu au juste pour s'attirer pareille réprobation ? »

Le père ferma les yeux un instant, prenant sa respiration comme quelqu'un qui doit s'attaquer à une lourde tâche.

« Le père Régnier voulait représenter plusieurs scènes présentant saint Georges à cheval. Une des scènes devait le voir occire un dragon de sa lance, tandis que l'autre le présentait en jeune homme délivré de ses oppresseurs par la sincérité de ses prières.

--- Ce sont là des histoires qu'on m'a contées, alors que je cheminais porteur de la Croix, en simple pérégrin.

--- C'est là le cœur de la discorde. Certains parmi nous n'aiment pas tant voir glorifiés les chevaliers qu'ils voient sans cesse en prendre à leur aise avec l'Église. Devoir passer chaque jour devant une scène présentant le saint sous leur apparence, casqué et armé comme eux, leur est d'une grande souffrance. Les barons finiront par croire leur pouvoir supérieur à celui de Dieu. Nulle voie n'est plus glorieuse que celle de rejoindre son service, afin de mortifier ses terrestres ambitions. »

Ernaut voyait bien les arguties chicanières des ecclésiastiques, qui n'avaient rien de mieux à faire que de s'entre-déchirer sur des problèmes que personne ne comprenait, et dont la plupart se moquaient. La seule chose sur laquelle ils convergeaient, c'était qu'ils devaient demeurer principaux maîtres à bord. La question était néanmoins de découvrir si cela avait pu mener à la mort de deux personnes.

« Avez-vous connaissance des raisons qui le poussaient à proposer semblables scènes ?

--- Le sire évêque n'est pas contre ce genre de représentation militaire, vous savez. Il est également le seigneur en titre de la cité de Lydda et doit au roi une compagnie de chevaliers et de sergents, qu'on nomme la compagnie saint Georges. »

Ernaut se demandait si Herbelot s'intéressait aux représentations des saints. Peut-être partageait-il la marotte de la découverte avec le défunt chanoine, et avait-il de nouveaux modèles à lui proposer ? Cela l'aurait fort surpris qu'un prêtre à ce point certain de la préséance de la Foi et de l'Église sur toutes choses puisse alimenter une pareille controverse au service d'hommes de guerre. Le plus probable fut qu'il avait en tête de quoi contrecarrer les plans du père Waulsort, des connaissances acquises par des recherches aussi érudites que les siennes. En ce cas, si une main criminelle avait agi, pourquoi aurait-elle supprimé deux adversaires soutenant des thèses opposées ?

Il remercia le chanoine poliment et se retira ostensiblement. En repartant, il partagea ses réflexions avec Raoul, espérant que ce dernier trouverait des pistes dans un sens ou l'autre dès le lendemain dans les documents d'Herbelot.

### Lydda, cour de l'hôtellerie du palais, nuit du lundi 15 décembre 1158

Au sortir du cloître, Ernaut se morigéna intérieurement de ne pas avoir pris d'éclairage. La nuit était d'encre et il fallait garder le contact avec le mur pour ne pas se fourvoyer en avançant. Il bénit donc la personne qui les rejoignit, portant la flamme chancelante d'une lampe. Il reconnut le vieux chanoine à la longue barbe. Il s'était coiffé d'un bonnet informe qui glissait sur son crâne chauve lorsqu'il s'agitait. Régulièrement, il le remettait en place d'un geste agacé.

« Je vous ai ouï parler avec le père Heimart. Il a omis le plus grave en cette affaire, je le crains. »

Ernaut se figea un instant, dévisageant le vieux clerc avec toute l'intensité dont il était capable et l'invitant, par son silence, à continuer.

« Le père Régnier avait la fâcheuse habitude de s'intéresser à toutes sortes de traditions. Il y cheminait sans discernement et en sortait ses découvertes.

--- Serait-ce qu'il n'a pas inventé ses décors ? Il aurait déniché quelque secrète tradition ?

--- Secrète, je ne sais, mais il prétendait que c'étaient là motifs bien usuels par ici, chez nos frères chrétiens non latins, entre autres. Voire chez... les hérétiques mahométans. »

Le prêtre avait lâché ce dernier mot comme on se dégage d'une glaire. Il sembla en garder le goût nauséabond dans la bouche un petit moment, cherchant à l'éliminer avant de reprendre.

« Il n'a guère souffert de leurs attaques, arrivé ici une fois les conquêtes de nos aînés réalisées. Il ne voyait nul mal à cueillir en jardins étrangers...

--- Aurait-il décidé de présenter des traditions mahométanes dans la basilique ? s'horrifia Raoul.

--- Il en aurait été bien capable ! Son coeur avait été souillé par ce Thibault, son valet qui ne le quittait guère. Il disait s'appeler ainsi, mais je sais bien qu'il n'était pas plus français, champenois ou normand que Sanguin[^10] lui-même !

--- Aurait-il de la famille en ces lieux ?

--- Non, il vivait dans l'ombre de Waulsort. Un serviteur dont je ne serais pas étonné qu'il ait eu le démon en lui. »

Il leva un doigt sentencieux, les yeux emplis de colère.

« *C'était un grand dragon rouge, ayant sept têtes et dix cornes, et sur ses têtes sept diadèmes*[^11]. »

Ernaut sentit un frisson le parcourir. Autour d'eux, le vent faisant claquer les bannières, grincer les charpentes. La flamme de la lampe elle-même paraissait moins attrayante. Il se pencha, articulant avec peine, la bouche pâteuse.

« Vous croyez qu'il y aurait eu là quelque dangereux savoir interdit ?

--- Sans nul doute aucun. Pourquoi Dieu les aurait-il effacés de la surface de la Terre ? Ils ont voulu jouer avec des forces qui les dépassaient, ils se sont aventurés là où nul ne devrait aller. »

Il marqua un temps, perdu dans son propre univers intérieur, les yeux s'écarquillant au fur et à mesure qu'il découvrait l'horreur de ce qu'il évoquait.

« Cherchez donc ce que tramait ce maudit Thibault et vous aurez la clef de toute cette histoire, c'est acertainé. »

Il éructa encore quelques sons indistincts et retourna vers le cloître, emportant avec lui la lueur vacillante, les abandonnant dans les ténèbres.

« On ne pourra pas dire qu'il nous a fort éclairés, celui-là » se moqua Ernaut avant d'avancer à tâtons vers la porte de l'hôtellerie.

Ayant rejoint leur chambre et pris du feu, ils s'installèrent rapidement sous les couvertures, disposant la petite lampe entre eux deux, sur le banc. À l'extérieur, le vent semblait se renforcer et l'air sifflait dans les interstices des menuiseries. La tête encore emplie de questions, Raoul se tourna vers Ernaut.

« Tu connaissais bien le secrétaire de l'archevêque ? Tu sembles par moment en faire une histoire personnelle. Était-il de tes amis ?

--- À dire le vrai, je ne saurais l'assurer. Nous avons voyagé de concert sur le *Falconus*[^12] et nous sommes revus de temps à autre. Il n'était guère aimable par moment, mais il était homme de bien. »

Il se tourna également, s'accouda avant de poursuivre.

« Un de mes compères, que tu dois connaître, Droart, m'a fait remarquer qu'ici nous étions tous quasiment sans lieu. Nous devons apprendre à nous soutenir les uns les autres, construire notre parentèle de cœur, en quelque sorte. Je ne saurais expliquer pourquoi, mais Herbelot faisait partie de la mienne. Même ses remarques acerbes vont me manquer. Je veux donc m'assurer que sa meurtrerie ne restera pas impunie.

--- Il est possible que tout ça ne soit qu'accidents...

--- En effet. Auquel cas j'aurais au moins vérifié cela pour un mien ami. Je ne fais pas cela pour l'évêque ou le patriarche, je le fais en mémoire d'un petit homme à la barbichette aussi aiguë que l'esprit. »

Il ménagea un silence puis reprit.

« Et toi, tu es enfant de pérégrins ? Tu as de la famille ?

--- De mes parents, nous possédons une tannerie à Jérusalem. C'est mon aîné qui est le chef de famille, sa femme vient de lui donner enfançon : Romainne.

--- Tu comptes travailler avec lui ? Vous êtes nombreux ?

--- Non, il n'y a que mon frère Emlot qui est demeuré. Mes autres frères et sœurs sont tous partis aussi. L'un d'eux est rendu au sud, espérant trouver place en un casal, un autre s'est juré à la Milice du Temple.

--- Comme combattant ? s'enthousiasma Ernaut.

--- Certes non, s'en amusa Raoul. Renaudin n'est pas du genre à aimer les risques. Il est simple valet et travaille en l'enclos à Jérusalem.

--- Tu as grand bonheur d'avoir les tiens à l'entour. Moi j'en arrive même à regretter la femme de mon frère, une vraie teigne. Ils doivent être entourés d'enfançons désormais, pour la plus grande joie de mon père.

--- Je suis surtout impatient de revoir Guillaume, parti depuis si longtemps étudier auprès des plus grandes universités. Je suis sûr que vous vous plairez, il fait toujours montre de grande curiosité. »

Repassant certains souvenirs dans leur tête, les deux jeunes gens se tinrent cois un petit moment. Jusqu'à ce qu'Ernaut reprit la parole.

« Moi je compte m'établir dès que possible. Je me suis promis à une fille de colons, Libourc. J'ai espoir de la marier dans les mois qui viennent.

--- Elle est de bonne famille ? Tu dois lui assurer important douaire ?

--- Oui et c'est pour cela que les choses tardent. Ses parents ont du bien et n'ont qu'elle, ici. Même si Sanson a bonne estime pour moi, il ne laissera jamais partir sa fille si je n'ai pas bel hostel et de quoi l'y installer. »

Il soupira longuement.

« Je mets de côté tout ce que je peux, mais c'est bien long. Lambert, que tu connais, n'a guère de quoi m'épauler en cela. J'ai fait passer un message à mon père, en Bourgogne. Il est riche vigneron et sera sûrement heureux d'apprendre que je compte prendre femme. Cela m'aiderait grandement qu'il me fasse parvenir quelques monnaies.

--- Je te souhaite d'arriver à tes fins. Moi je crains de ne guère pouvoir marier honnête femme. Avec ma solde de scribe, aucun père ne donnera sa fille sans regimber. »

Ernaut grogna, se relevant dans son lit alors qu'il parlait en s'agitant de plus en plus.

« C'est pour ça que j'espère pouvoir participer à plus de chevauchées le roi. Il n'y a que là qu'on peut espérer dénicher picorée. Large butin rend les barons généreux.

--- Tu peux aussi te faire raccourcir la tête ou y laisser un bras. Très peu pour moi !

--- *Fortune règne* ! Si je veux établir solide parentèle, il me faudra bien prendre quelques risques. Et, aussi, m'inquiéter de mes proches. Finalement, ce qu'on fait ici, c'est tout un avec mes espoirs d'hostel. Il faut semer avant de récolter, comme dirait mon père. »

Raoul opina en silence puis se réinstalla mains sous la tête.

« J'ai espoir que tout cela n'est que malencontreux aléas.

--- Demain, nous y verrons plus clair. J'irai faire visite à la demeure du chanoine et je repasserai à sa loge au chantier. À la lumière, peut-être que certaines choses me seront plus visibles. »

### Lydda, hôtellerie du palais épiscopal, matin du mardi 16 décembre 1158

Un ciel bas, uniforme, avait accueilli l'aube grise. Il ne faisait pas froid, mais la lumière fade rendait l'ambiance morose. Même l'activité semblait moindre dans la ville. Ernaut avait attendu de pouvoir avaler un en-cas au réfectoire, pain et porée[^13] agrémentée de fromage émietté puis, craignant la pluie, était repassé prendre sa chape. Il quitta Raoul en le saluant gaiement, heureux de lui laisser la corvée de déchiffrer les archives d'Herbelot.

La cité de Lydda lui parut toujours aussi accueillante. Il s'y trouvait moins de ruines qu'à Jérusalem et les murs peu élevés rendaient les venelles plus respirables. Les rues principales, dans le prolongement des portes, étaient envahies d'animaux de bât allant et venant entre les différents souks qui ouvraient grand leurs larges vantaux en façade. Des portiers, le regard inquisiteur, vérifiaient que nul larron ou importun ne s'aventurait dans ces espaces de commerce.

Il retrouva sans difficulté la demeure du chanoine décédé et en déverrouilla la serrure. Il pénétra alors dans une modeste pièce sombre, dont la seule lumière filtrait d'une fenêtre sur cour, à travers un treillage de roseaux. Des pots, des sacs entassés sur des étagères et quelques meubles bas, une table et un coffre avec de la vaisselle, c'était très probablement une cuisine. Un panier, suspendu au plafond, répandait une odeur d'oignons qui se mêlait à des relents huileux et poussiéreux. L'endroit était propre, même abandonné depuis plusieurs jours. C'était là une maison bien tenue.

Ernaut débloqua la barre de la porte ouvrant dans une étroite cour intérieure. Il ne s'y trouvait que quelques objets cassés et un peu de bois accumulé contre un four, sous un appentis. On y voyait également la bouche d'un puits obturé de planches. À côté d'un tas de compost, quelques plantes tentaient de pousser dans une plate-bande le long du mur opposé au petit guichet qui donnait dans la rue. Percé d'un fenestron, il était verrouillé de deux bastaings.

La seconde pièce, de taille semblable à la première, était dévolue au stockage. Des rayonnages, vides pour la plupart, et les bouches de vastes céramiques enterrées. Ernaut souleva quelques couvercles, pour n'y rencontrer que du grain, des pois ou des lentilles. C'était là cache fréquente pour les objets de valeur. Il y planta une baguette afin de sonder les contenants, sans succès. Il monta ensuite l'escalier qui se déroulait en deux volées jusqu'à l'étage.

Dallé de terre cuite, le second niveau ne comportait qu'une vaste pièce, avec des ouvertures assez larges sur la courette et une, plus étroite et bloquée d'un volet intérieur, au-dessus de l'entrée. Une échelle de meunier donnait accès à une trappe vers le toit-terrasse. Le tout était spacieux, avec un lit à l'aspect douillet, un coffre à son pied et un second matelas glissé sous le sommier. Plusieurs bancs et tabourets occupaient la périphérie, où quelques niches avec des rayonnages étaient parfois fermées d'une porte. Des étagères et des tables longeaient le moindre pan de mur disponible. On n'y voyait que quelques pots vides et des chiffons sales.

L'endroit ne paraissait pas vraiment luxueux, mais il était de toute évidence fort confortable. Un vaste plateau remplissait une large portion de la zone côté rue. Ernaut s'en approcha, y découvrant quelques livrets, de quoi écrire et des feuilles de papier en quantité. Pas mal de portfolios s'empilaient, sans qu'on y voie un ordre quelconque. Ernaut se serait attendu à trouver plus d'ouvrages chez un tel érudit. Il n'y avait là que quelques notes et des listes gribouillées sans grand intérêt. Il n'apercevait par ailleurs aucune trace de feu nulle part. Le sol de brique ne présentait aucune marque de suie, pas plus que les murs chaulés un peu défraîchis. Il ne discerna qu'un endroit sale qui pouvait correspondre à la place que prendraient deux corps. Sans en être certain, il fit pourtant en sorte de ne pas y marcher.

Il fouilla les lieux sans empressement, accordant une attention toute particulière aux placards et niches, ainsi qu'aux coffres qui auraient pu receler une cache. Il découvrit une petite bourse avec plusieurs pièces, pour une valeur de presque une demi-livre, coincée dans la maçonnerie sous le plateau en bois d'une étagère, mais rien d'autre d'intéressant. Il la mit de côté, estimant que cela devait être confié au doyen pour être intégré aux biens du chanoine.

Assis sur le lit, il contemplait les lieux en vérifiant s'il avait négligé un détail. Repensant aux deniers et dirhems qu'il avait trouvés, il décida de demander au vicomte si le valet avait de la famille, car c'était là plutôt les économies d'un domestique que le trésor d'un clerc richement doté. Il laça le petit sac à son braïel puis se leva pour monter sur le toit. La trappe était elle aussi barrée d'une planche, rendant son ouverture impossible de l'extérieur.

La terrasse n'offrait aucun intérêt : des claies pour faire sécher les légumes y pourrissaient lentement, quelques matériaux de construction, peut-être pour un auvent, y étaient empilés sous une poussière ancienne. Il remua malgré tout le tas, sans conviction, pour s'assurer qu'on n'y avait rien dissimulé. Alors qu'il allait redescendre, il vit qu'un voisin était monté sur son propre toit, accolé au sien. Il s'employait à trier des planches accumulées en un amas branlant. Ernaut se demanda s'il n'était pas venu par simple curiosité. Il décida d'en profiter pour obtenir quelques réponses. Levant la main, il s'approcha, tout sourire et salua poliment. Il se fit passer pour un homme au service de l'évêque, occupé à préparer l'enlèvement des affaires.

Le voisin se présenta comme Umayyah, fabricant de paniers. Le front haut et la barbe rase, il n'était pas très âgé. Il avait le verbe fort et parlait très vite, avec un accent rauque. Il ne connaissait que peu Waulsort, mais croisait souvent Thibault. Il en fit un portrait amical et regrettait fort sa disparition. Ils fréquentaient les mêmes commerces et avaient échangé quelques phrases à l'occasion.

Lorsqu'Ernaut lui demanda comment on avait découvert la mort des deux hommes, Umayyah devint plus sombre.

« C'est le jeune Hashim, un gamin qui traîne beaucoup. Son père est un bon à rien, alors il travaille cy et là, il fait le commissionnaire. Chaque matin il venait prendre ses instructions pour la journée : aller chercher du pain, des légumes, quérir un porteur d'eau... Trouvant porte close, il s'en est ému, et a insisté tant et tant qu'on a fini par s'en inquiéter dans le quartier. On l'a aidé à grimper par-dessus le mur de la cour, il a pu entrer par la porte intérieure et les a découverts.

--- Sauriez-vous où je peux le trouver ?

--- Tous les commerçants du quartier le connaissent. Vous n'aurez aucun mal à ça. »

Ernaut le remercia et redescendit. Il avait fouillé tout ce qu'il y avait à voir. Il verrouilla soigneusement toutes les issues puis sortit de la maison. Alors qu'il attachait la clef à sa ceinture, il eut le sentiment d'être épié, peut-être depuis une des fenêtres grillagées alentour, utilisées justement pour surveiller en toute discrétion. En tournant la tête, il eut l'impression de discerner une silhouette habillée comme un latin qui s'esquivait parmi les thawbs[^14] et les 'aba[^15].

Il erra un petit moment parmi les étals, surtout ceux proposant de la nourriture. Cela le mit en appétit et il s'offrit au passage un bâtonnet de poisson grillé dans du pain. En peu de temps, il réussit en effet à dénicher Hashim, gamin à peine âgé d'une dizaine d'années. Inquiété par l'énorme masse de son interlocuteur, celui-ci dansait d'un pied sur l'autre, les sourcils froncés.

Avisant sa tenue élimée et ses pieds nus, Ernaut promit un denier s'il acceptait de s'entretenir avec lui et lui proposa en outre le même en-cas que lui. Tout en le suivant vers une place où on pouvait s'asseoir sur un muret, à côté d'un bassin où les gens venaient puiser de l'eau, Hashim déchirait la provende à belles dents. Ernaut expliqua qu'il avait besoin d'en savoir plus sur ce qui c'était passé dans la maison. Le gamin ne parut nullement s'en inquiéter, obnubilé par ce qu'il avait à dévorer et les perspectives du gain évoqué.

« Je sais pas ce qu'y s'est passé, résuma le gamin, tout en avalant avec délice une grosse bouchée.

--- Tu es bien entré le premier ? C'est toi qui les a trouvés ?

--- Ça oui. Y sentaient fallait voir comme, on aurait dit qu'on avait brûlé des savates là-dedans !

--- Il y avait quoi de brûlé exactement ? Juste eux, d'autres choses autour ?

--- Y n'étaient pas tout brûlés. Seulement partie. Twabah et son maître.

--- Twabah ?

--- Oui, le valet de l'imam. Et l'imam à ses côtés, j'ai bien reconnu leurs tenues.

--- Ils étaient habillés, pas en tenue pour dormir ?

--- Ça non, habillés comme en journée. Ils avaient dû se brûler à la veillée. Ça sentait encore, mais pas de fumée.

--- Et il n'y avait nul feu ?

--- Même pas une lampe ! Peut-être qu'y s'étaient brûlés avec les élixirs de l'imam... »

Ernaut n'avait aucun souvenir qu'il y ait eu le moindre récipient suspect dans la pièce. Il se rapprocha du gamin.

« Quels élixirs ?

--- Les choses étranges sur la table, comme chez le docteur ou l'apothicaire. Pots et fioles en grande quantité. En grand désordre, d'ailleurs, c'est pour ça que j'ai pensé que le feu était sorti de là. »

Ernaut écarquilla les yeux. Quelqu'un s'était empressé de faire le ménage chez le chanoine, désireux de camoufler tout un pan de l'affaire. Le réflexe premier d'un assassin était de brouiller les pistes pouvant mener jusqu'à lui. Il remercia le gamin et lui lança les trois pièces. Hashim lui offrit en retour un sourire partiellement édenté.

Sans plus attendre, Ernaut se rendit au bâtiment administratif pour y discuter avec le vicomte. Avant d'échafauder des hypothèses, il fallait s'assurer que ce n'était pas lui qui avait fait nettoyer l'endroit, de peur que le feu ne se propage par quelque procédé mystérieux. Il le retrouva attablé dans la grande salle d'audience en train de vérifier avec le scribe local les minutes pour la rédaction d'une charte. Ernaut sourit en voyant que ce dernier avait les mêmes attitudes que Raoul et la semblable discrétion de souris. Il attendit poliment, mais, d'un geste, le vicomte l'invita à s'approcher tout en se redressant. Intimant à l'écrivain de s'éloigner d'un mouvement du menton, il s'épongea le front et se gratta un bras tout en écoutant.

Ernaut lui expliqua la situation et l'interrogea sur la disparition du matériel. Maugier acquiesça avoir souvenir de ce fatras sur la table, codex et rouleaux, flasques et burettes, mais il ne s'en était guère occupé, n'ayant aucune idée de l'intérêt de la chose. Il avait confié la clef au doyen, le laissant en charge de tout cela. Il répéta à peu près la même chose à propos de l'argent découvert.

« Vous savez, quand nul ne vient réclamer justice et que cela n'a guère troublé la vie de la cité, je ne m'intéresse généralement guère aux trépassés.

--- L'évêque Constantin est pourtant fort ému de cela, ce me semble.

--- Bien sûr ! De la mort de votre ami ! Mais s'il était en effet fort troublé de la façon dont Waulsort avait passé, cela ne justifiait pas de mesures particulières. Qui peut tout expliquer ici bas ? Il a commencé à se questionner quand le secrétaire de l'archevêque s'est présenté en demandant après le chanoine alors même qu'on venait d'en faire découverte au matin, horriblement brûlé. Et quand le père Herbelot s'est trouvé mourir à son tour, alors là l'inquiétude a grandi au point qu'il en a écrit une missive au patriarche. Qui vous a envoyé. »

Maugier du Toron semblait se désintéresser de l'affaire maintenant qu'il y avait des personnes en charge. Ernaut comprenait sa position, pour avoir vu régulièrement le vicomte Arnulf faire de même. Leur tâche était ingrate, d'administration besogneuse et routinière. Ils sautaient donc sur la moindre occasion de déléguer les corvées à d'autres.

Il remercia Maugier et l'abandonna à ses devoirs scripturaires. Il hésita un instant à rendre visite au doyen tout de suite, mais il préféra aller fouiller la loge en premier. Il était plus sage de ne pas perdre plus de temps au cas où le ménage n'y aurait pas encore été fait.

Durant tout ce temps, Raoul n'était pas demeuré inactif. Il avait demandé à Germain de pouvoir jeter un coup d'œil dans les affaires d'Herbelot. Alors qu'il commençait à en extraire les feuillets, un jeune clerc se présenta. Doté d'un bel embonpoint, celui-ci, prénommé Gilbert avait une voix légère et agréable, et semblait dans les mêmes âges que Raoul et Ernaut. Il se disait ami d'Herbelot, ce que confirma Germain.

« Je vois que vous faites recherches parmi les notes du père Herbelot. Peut-être que je pourrais vous aider, il nous arrivait de parler de ce qu'il faisait... »

Enthousiasmé par cette proposition, Raoul ne tenta pas de s'expliquer sur sa tâche précise, estimant qu'il n'était d'aucune utilité de répondre à une question qui n'avait pas été posée. Il apprit alors qu'Herbelot travaillait à une grande œuvre personnelle. Il ne souhaitait pas moins que rédiger un texte qui ferait date dans l'édification des puissants, de façon à les rendre aptes à mener le peuple de façon conforme aux desseins de Dieu. Rompu à la lecture et à l'étude des auteurs les plus fameux, il envisageait de s'appuyer sur des romans populaires, comme celui d'Alexandre, mais tout en demeurant dans le plus strict respect des doctrines évangéliques. Le projet semblait faire naître l'enthousiasme chez Gilbert, sans pour autant qu'il soit capable d'en décrire le contenu. En fait, il était venu pour voir s'il ne pouvait pas trouver quelques détails sur une éventuelle version chantée, la musique étant sa vraie passion. Il espérait ainsi perpétuer l'œuvre de son ami.

Un peu circonspect quant aux informations que cela pourrait apporter à leur enquête, Raoul fit malgré tout l'effort de s'y intéresser. Cela pouvait au final lui permettre de mettre de côté rapidement tout ce qui n'avait pas trait au décès du clerc. Il proposa à Gilbert de l'aider à faire le tri en ce sens.

« C'est que le père Herbelot avait voulu s'entretenir de cela avec le père Régnier, justement.

--- Peut-il y avoir un lien entre leurs morts et cette histoire, à votre avis ? »

Le jeune clerc en parut horrifié et mit une main devant sa bouche, hésitant à se signer.

« Je ne vois pas comment cela se pourrait ! C'est pur travail d'écriture ! »

Estimant qu'il ne pouvait guère poser plus de questions sans dévoiler son vrai rôle, Raoul accorda un sourire rassurant à Gilbert et se replongea dans les papiers. Ils entreprirent de classer les différents documents en fonction de ce critère, en espérant dénicher les pièces les plus récentes rapidement. Car s'y trouvaient certainement dissimulées des raisons à ce qui s'était passé. Face à eux, Germain s'installa pour plier avec soin les affaires de son défunt maître. Aucun des deux enquêteurs ne remarqua les larmes qu'il s'efforçait d'effacer avec discrétion.

### Lydda, chantier de la basilique Saint-Georges, fin de matinée du mardi 16 décembre 1158

Quelques gouttes marquèrent la poussière alors qu'Ernaut rejoignait le chantier, sans qu'une véritable averse ne s'affirme. Indifférents, les ouvriers s'activaient autour de la mise en place d'une nouvelle rampe destinée à l'accès des matériaux pour la travée à laquelle ils allaient s'atteler. Un énorme treuil hissant les pierres de taille ou le mortier était fixé à sa partie sommitale, traînant un chariot à deux roues sur le pan incliné. Deux échelles assez raides étaient prévues, de part et d'autre, pour permettre aux portefaix de monter leur part et de redescendre sans se gêner.

Plusieurs ingénieurs supervisaient les installations, criant de leur voix chantantes les instructions aux hommes qui tiraiet sur les cordes ou s'activaient aux abords des grues à échelons qui grinçaient sous la charge. Ernaut reconnut l'accent des cités italiennes, certainement des Génois dans cette partie du royaume si près de Jaffa, où ils possédaient un quartier entier. Leurs connaissances en charpenterie navale les désignaient naturellement pour tout ce qui avait trait au levage et aux échafaudages. Beaucoup œuvraient aussi aux engins d'assaut sur les fortifications.

Autour de lui, ce n'étaient que crissements, halètements et appels. Au fond du sanctuaire, les artistes mettaient la dernière main aux décors de mosaïque, aux peintures. Des passerelles plus légères y courraient le long des parois, accessibles par des échelles et des cordes. Plusieurs artisans étaient assis sur de simples planches suspendues en hauteur. Dans une des absides, un valet tonsuré s'employait à secouer les linges d'autel et à balayer la poussière qui revenait sans cesse. Ernaut traversa en faisant attention de ne déranger personne en cette journée agitée. Il se demanda si le jour de la mort d'Herbelot, l'activité avait été aussi industrieuse.

Les loges des tailleurs de pierre et des sculpteurs flanquaient l'impressionnante forge. Ce n'était pas la petite installation légère qu'on rencontrait généralement dans les villages, parfois le fait d'un artisan itinérant. Là, deux hommes frappaient en cadence sur de lourds tas d'acier, de part et d'autre d'un foyer double, où des apprentis actionnaient des soufflets de gros volume. L'endroit dégageait une familière odeur de fer chaud. Dans l'atelier traînait une quantité énorme de poinçons, haches, marteaux et burins : les outils se faisaient régulièrement refaire une santé, par l'adjonction d'une broche acérée ou la réparation d'une fêlure.

La petite loge du chanoine était demeurée en l'état depuis la veille et les ouvriers occupés à tracer dans la pierre les gabarits fournis ne tournèrent même pas la tête quand Ernaut vint y jeter un œil. La lueur du jour ne lui dévoila rien de plus. Les motifs dessinés étaient juste plus aisés à lire et à déchiffrer. Le seul coffre qu'il découvrit, sous une table, était largement ouvert et ne contenait que des instruments rudimentaires, compas et équerres, corde à nœuds et règles.

Conscient que sa vaste stature gênait, quel que soit le passage où il se tenait, il décida de s'asseoir un moment sur un tabouret un peu à l'écart, admirant le puissant édifice en construction. La perspective qui s'offrait à lui était impressionnante, aucune cloison ne s'opposant à la vue depuis la cour occidentale. On découvrait les murs encore inachevés exposant leurs entrailles de blocage, les lourdes charpentes qui soutenaient les arches de pierre en cours d'installation. Il aurait aimé s'entretenir avec les hommes du chantier, mais il savait qu'il ne faisait pas bon les déranger, surtout quand ils s'attaquaient à une opération complexe comme aujourd'hui.

Ayant aperçu un ouvrier avec le bras en écharpe, qui se contentait de porter de l'eau à boire aux uns et aux autres, il se dit qu'il serait moins perturbant de s'adresser à lui. Le dos voûté malgré une puissante musculature, il avait les mains calleuses de l'habitué aux durs travaux et le regard plissé des manouvriers vivant en extérieur. Lorsqu'il vit Ernaut s'approcher, il lui proposa spontanément à boire. Ernaut accepta volontiers, prit la louche de bois et avala quelques gorgées. Il demanda au porteur d'eau s'il aurait le temps de l'assister pour une tâche simple, en échange de quelques mailles[^16]. Blessé, l'homme était incapable de faire grand chose et acquiesça avec enthousiasme, hochant la tête avant de repousser ses fins cheveux blonds derrière ses oreilles. Il se présenta comme Gerbaut, manouvrier. Ernaut se souvint qu'officiellement il était là pour préparer un don royal à l'évêque.

« Pouvez-vous me parler du chantier ? J'aimerais en savoir plus sur ce qui se passe ici... Je suis de l'hostel le roi et il est possible qu'il fasse un don au sanctuaire pour aider aux travaux.

--- Comme vous le voyez, mestre, la basilique est refaite de la base au toit. Je ne suis pas là depuis fort longtemps, mais je sais qu'il est prévu de fermer à l'ouest après cette dernière travée. Il restera grand œuvre de décoration par la suite. Les peintres en sont à leur début.

--- Connais-tu le travail des ymagiers ?

--- Guère. J'ai usage d'aider plutôt en maçonnerie. Je suis tailleur de profession, mais nulle place n'était libre ici, alors je me loue comme simple portefaix. J'attends la fin de l'hiver puis je joindrai la princée d'Oultre-Jourdain. Il y aura du bel ouvrage à faire là-bas. »

Ernaut grimaça et incita son compagnon à avancer vers la loge des sculpteurs. Ils y ciselaient un marbre très clair, aux veines à peine tissées de gris. Le manutentionnaire salua un des artisans, qui leva la tête de son ouvrage. Il supposa peut-être qu'Ernaut était homme d'importance pour se faire guider dans le chantier. Profitant de la pause, il se désaltéra au seau de Gerbaut tandis qu'Ernaut admirait son travail.

« Il n'y a guère de scènes dans ces décors, seulement des feuillages. Est-ce qu'ils ne prévoyaient pas d'historier les décors ?

--- Il demeurait pas mal de décors de l'ancien temps, et le père chargé de nous donner instruction voulait que tout soit fait dans la même veine.

--- Vous n'avez aucune liberté de tracer les motifs ?

--- Oh certes pas ! Déjà qu'entre clercs ils se bestaignent tant et plus quand ce n'est pas leur idée qui est retenue. Nous ne sommes là que pour mettre dans la pierre ce qu'ils ont conçu en esprit.

--- Le chanoine en charge s'appelait le père Régnier, je crois ? Il est mort voilà peu m'a-t-on dit.

--- Cela même ! Heureusement qu'il avait laissé pas mal d'instructions. Entre ça et les reliefs de l'ancienne église, nous savons comment achever le travail sans devoir en appeler à un nouveau maître ymagier. »

Il allait reprendre son poinçon et son marteau lorsqu'Ernaut l'interrompit.

« Vous ne pouvez jamais en faire à votre fantaisie, tout demeure sous contrôle ?

--- Je ne dis pas qu'il ne nous arrive pas d'en faire à notre tête. Les clercs montent rarement examiner les décors les moins visibles ou les plus élevés. Il nous suffit de les retoucher une fois là-haut. Mais ici, il n'y a guère de quoi pareillement s'épancher. »

Laissant le sculpteur se remettre à l'ouvrage, Ernaut se retourna vers le chantier. Dans la cour, un lourd attelage de bois de charpente était tiré par une file de chameau. Il fixa le portefaix, avec son bras cassé.

« Les accidents sont nombreux, je vois que tu as connu quelque souci ?

--- L'endroit est un peu agité depuis une dizaine de jours. Il a fallu démonter les anciennes charpentes pour préparer la nouvelle travée. Moi, une de mes courroies a lâché alors que je montais. Le poids de ma hotte m'a fait basculer, tout à tas. Je m'en sors bien, finalement. »

Il haussa les épaules, fataliste.

« Pareilles déconvenues sont fréquentes. Il n'est pas une semaine sans qu'une jambe ou un bras ne soit brisé. Les imprudents font aussi parfois de mortelles chutes. Ici il n'y a pas lieu de se plaindre. L'évêque veille à ce que nous puissions travailler en de bonnes conditions. Les cordages sont régulièrement vérifiés et entretenus, les étais assez nombreux et les échelles robustes et en quantité. J'ai connu bien pire comme endroits.

--- Il y a tout de même eu mortel accident voilà peu, m'a-t-on confié. Un clerc en pleines oraisons...

--- Ah, dame oui. Ça a été le drame de la dernière huitaine. On ne fait pas si grand cas lorsqu'un plateau qui lâche jette à bas un tâcheron !

--- Tu as vu ce qui s'est passé ?

--- Pas vraiment. J'étais affecté au cordier, qui avait grand ouvrage à refaire à neuf les liens pour la réinstallation. Et dans la nef, tout était sens dessus dessous : ils démontaient ce que vous voyez en train d'être installé de nouvel. Mais bon, ce n'est même pas le démontage qui a causé l'accident, c'est juste une passerelle de peintre qui a lâché. »

Il pointa les légers assemblages au fond du sanctuaire.

« Vu qu'on y porte moins de matériaux et que les passages y sont moindres, il y a parfois moins de soin à leurs liaisons. Souventes fois, on récupère des câbles impropres au treuillage pour nouer les madriers. Et ça reste en place un long temps. »

Soucieux de ne pas trop attirer l'attention sur l'accident, Ernaut tourna son regard vers le complexe de bâtiments qui prolongeait l'église au sud.

« Cette seconde chapelle ne sera pas refaite ?

--- Pas que je sache, une partie des murs méridionaux de la basilique ont été intégrés à ces bâtiments. Il y a quelques pièces réservées aux chanoines, une sacristie, mais surtout ce sont les chapelles pour les Griffons. Je ne pense pas que le sire évêque ait désir de payer pour un culte forain. Puis il ajouta, en souriant : mais notre sire Baudoin peut avoir envie de plaire à sa reine, auquel cas cela pourrait se faire ! »

Ernaut accorda un rictus amusé, conscient que toute nouvelle manne qui pourrait solder des travailleurs serait la bienvenue. Mais il était peu probable que le roi ait jamais entendu parler de la chapelle grecque de Lydda. Quant à financer sa reconstruction, plus ambitieuse, il y avait fort peu de chance que cela n'arrive jamais, vu que Baudoin était réputé pour toujours manquer d'argent y compris pour ses campagnes militaires.

Prétextant un intérêt pour les œuvres figurées, Ernaut proposa d'aller voir de plus près les fresques en cours de réalisation. Ils contournèrent prudemment la partie centrale, se faufilant le long des bas-côtés. Juste avant de rejoindre les absides, des cabanons s'appuyaient au mur. Gerbaut lui expliqua que c'étaient les loges des décorateurs. Là, les peintres broyaient leurs couleurs, entassaient leurs pigments onéreux, tandis que les mosaïstes découpaient leurs tesselles ou y entreposaient leurs mortiers.

Des gamins, sûrement des apprentis, se consacraient aux tâches les plus rébarbatives de tamisage des poudres, de tri des abacules, sous la férule des plus âgés. Ceux-ci faisaient la navette entre ce lieu de préparation et les échafaudages où les maîtres appliquaient tout cela avec dextérité. Comprenant que les discussions ne se faisaient pas dans sa langue, Ernaut demanda quelle était l'origine de ces artistes. Gerbaut expliqua qu'il y avait là surtout des Byzantins pour la mosaïque et des Syriaques pour les fresques. Le chanoine Régnier échangeait beaucoup avec ces derniers, semblant leur accorder une assez grande liberté, contrairement aux usages.

« Il ne leur donnait pas indication des motifs ? s'étonna Ernaut.

--- Si, de certes. Nul ne peut décorer le sanctuaire sans qu'il n'ait établi le programme. Mais les Syriaques étaient plus libres dans les formes. Je les ai vus maintes fois aller et venir dans sa loge, échanger avec lui de points ou d'autres. Généralement, les clercs se contentent de donner des ordres, ils n'attendent pas de propositions, ou guère. Même le célèbre mosaïste qui dirige le chantier se contente de suivre les tracés prévus, sans rien y modifier.

--- Une raison à cela ? Nulle jalousie n'en ressort ?

--- Aucune idée. Peut-être avait-il meilleure fiance dans les peintres syriaques, je ne sais. Pour le second point, je ne saurais dire. »

Ils stationnèrent sous les décors en cours de réalisation. Les artisans, concentrés sur leur tâche, ne s'intéressaient guère à ce qui se passait en bas. Ils assemblaient une frise géométrique de grecques qui délimitait une zone de texte. Le chatoiement des petits éléments disposés avec adresse attirait immanquablement l'œil. Puisant dans des paniers autour d'eux, les mosaïstes suivaient les motifs tracés au charbon sur le mur apprêté. Ernaut prit conscience du fait qu'il y avait là des matériaux extrêmement coûteux, des émaux et parfois même de l'or. De quoi éveiller la convoitise d'un des ouvriers. Rien que le prix des tesselles assemblées le court moment où ils demeurèrent à admirer la mise en place aurait largement couvert les émoluments de plusieurs manutentionnaires la semaine durant. Il risqua un œil vers Gerbaut, se demandant si ce dernier pensait à ce genre de choses. Le portefaix semblait pour l'instant plus intéressé par le tumulte dans les échafaudages.

Il ne paraissait guère possible à Ernaut d'en apprendre plus. Il remercia chaleureusement Gerbaut, et lui glissa quelques pièces comme convenu. Puis il suivit un petit groupe de pèlerins qui sortait de l'édifice par le cloître. Au moins ne risquait-on pas de recevoir un madrier sur la tête en passant par là, pensa-t-il. Il s'assura néanmoins que l'échafaudage sous lequel il s'engageait ne menaçait pas de s'écrouler. Un peu de superstition aurait peut-être sauvé la vie d'Herbelot, estima-t-il.

### Lydda, cloître du palais, fin d'après-midi du mardi 16 décembre 1158

Le soleil s'était finalement décidé à déchirer les nuages alors qu'il basculait vers l'ouest, bordant de couleurs fauves les derniers reliefs cotonneux. Ernaut et Raoul avaient pris un rapide repas au réfectoire en compagnie de Gilbert. Le jeune clerc s'était montré un compagnon agréable et discret et avait apporté beaucoup d'aide à Raoul tout au long de la journée. Il n'avait malheureusement pas pu demeurer aussi longtemps qu'il l'aurait souhaité, ayant de nombreuses tâches à accomplir au service de l'évêque. Ils s'étaient néanmoins retrouvés pour le souper et avaient discuté de tout et de rien tout en avalant les lentilles agrémentées de rares morceaux de lard.

Ernaut en avait profité pour avoir confirmation de la tombe d'Herbelot, heureux de ne s'être pas trompé. Il s'efforçait de faire parler Gilbert sans trop en dévoiler sur la réelle mission qu'il accomplissait. De toute façon, le jeune clerc ne s'intéressait guère aux événements récents, plus enthousiasmé à la perspective de reprendre le travail d'Herbelot. Il se disait qu'il pourrait peut-être mettre en vers les idées esquissées pour en faire un long chant destiné aux souverains. Il comptait bien demander l'autorisation à l'archevêque Pierre de conserver les écrits du prêtre, voire, au pire, de les copier à son usage.

Après le repas, Raoul et Ernaut se rendirent dans le cloître. C'était le lieu le plus agréable pour passer la soirée, avec quelques éclairages et sans trop de chemin pour retourner dans leur chambre. Les rares pèlerins, quelques ecclésiastiques et une bonne partie des serviteurs étaient là pour la veillée, se rassemblant autour de prières, de récits et de discussions. Comme les clercs précédemment, Raoul avait proposé de jouer aux mérelles un petit moment. Ce serait en outre l'occasion d'évoquer tranquillement leurs avancées respectives. Il ne s'écoulerait guère de temps avant qu'on leur demande des comptes sur ce qu'ils avaient découvert.

Empli de chuchotements et d'échanges modérés, le lieu leur semblait étrangement calme après la frénésie qui avait agité le chantier pendant la journée. Raoul expliqua à Ernaut qu'il avait appris tout ce qu'il savait dans un endroit similaire, au sein du *Templum Domini*. Lorsque l'hiver était vraiment rude, ils se repliaient dans le chauffoir, apportant un peu de vie aux clercs grabataires qui patientaient là sans grand chose à faire.

Ils en étaient à une dizaine de parties lorsque la fatigue commença à se manifester. Ernaut était peu en train, déçu de n'avoir guère à se mettre sous la dent à propos de l'enquête. La combustion des corps de Waulsort et de son valet était la seule chose étrange, mais rien n'interdisait de penser que le maître se soit renversé sur lui un liquide inflammable, le serviteur ayant pris feu à son tour en tentant de porter secours. Il demeurait surprenant que cela se soit circonscrit aux deux hommes et n'ait rien embrasé sur la table toute proche, mais cela ne voulait pas forcément dire que c'était impossible.

Raoul s'éloignait après avoir rangé les jetons dans sa besace lorsqu'un des chanoines surgit tout à coup de l'obscurité aux côtés d'Ernaut. Ses yeux divergents lui donnaient une allure de fou, mais il était vêtu dignement, tonsuré proprement. Il avait en outre avancé une main qui n'avait jamais connu l'effort, espérant attraper en vain le poignet d'Ernaut. Ce dernier avait esquivé en un réflexe.

« Êtes-vous bien à la recherche de la vérité, jeune homme ?

--- Le pardon ?

--- Le sire évêque nous a conté que vous étiez ici pour apporter la lumière là où les ténèbres demeurent. Mais ne craignez-vous pas de découvrir trop effroyable vérité ? »

Ernaut se demanda si le vieil homme n'avait pas perdu l'esprit. Légèrement voûté, il branlait de la tête, pourléchant la salive abondante qui coulait de sa bouche édentée.

« Détiendriez-vous quelque savoir utile en ma quête, mon père ? »

D'autorité, l'homme s'assit face à Ernaut, prenant ses deux mains dans les siennes.

« Quelle que soit ta force, il se trouvera une Dalila, mon garçon. Ne surestime pas ton bras. Préserve ton âme des chemins faux et des promesses honteuses.

--- Faites-vous allusion à ce qui se passe ici ?

--- En partie, oui. Tu suis de bien dangereuses traces... Ne va pas éveiller ce qui te détruira. Régnier avait la faiblesse de sa grandeur. L'orgueil de l'intellect lui a fait croire qu'il portait l'armure des saints. Il s'est perdu en chemin et rien n'a pu le sauver du démon. »

Ernaut se demanda s'il avait affaire à un illuminé comme il s'en trouvait parfois. On en faisait généralement des moines, pour les mettre à l'écart et les surveiller, mais peut-être que celui-ci avait fini chanoine. Il continuait à ressasser entre ses lèvres, défiant le jeune homme de ses yeux fous.

« Régnier a voulu lever le voile sur des choses qui ne lui étaient pas permises. Il a réveillé la tentation païenne, la bête. Et il n'était pas saint Georges, il ne savait pas comment abattre l'hideuse. Le souffle des Enfers l'a consumé, pour punition de sa présomption.

--- Il étudiait des choses interdites ?

--- Des textes hérétiques, païens, rien ne l'effrayait ! Il pensait de certes que ses droites intentions et sa foi le prémuniraient. Fou qu'il était, il n'était pas de ces hommes qui peuvent plonger le regard dans les abymes sans y succomber.

--- Vous en a-t-il parlé ? A-t-il consigné cela ?

--- Il a tenté de nous pervertir, de couvrir nos murs et notre sanctuaire d'impies icônes ! Puisse Dieu lui pardonner pareilles offenses. »

Ernaut sentit qu'il tenait là enfin quelque chose de concret, malgré le babillage abscons du vieil homme. Il se pencha, ne souhaitant pas attirer l'attention à eux.

« Vous croyez qu'il aurait succombé à male influence ? Qu'il tramait noir dessein ?

--- Dieu m'est témoin que je n'encroyais nulle mauvaiseté en sien coeur. C'est la faute de son mécréant séide. Il l'a abreuvé de billevesées jusqu'à écœurement. Lui, je l'espère, sera puni pour si néfastes agissements.

--- De qui parlez-vous, son valet ?

--- Il prétendait se faire appeler Thibault, mais ce m'est certeté qu'il n'était baptisé que du bout des lèvres. Il portait encore en sien cœur toute la noirceur de ses anciennes croyances et veillait à en propager les noirs secrets. »

Désemparé, le vieux chanoine soupira longuement, serrant plus fort encore les mains d'Ernaut.

« Prends bien garde à toi, mon garçon. Si Régnier n'a pas su soumettre le dragon et encore moins l'occire, il est de peu de chance que tu y parviennes. Je prierai pour que Dieu t'épargne cette tragique rencontre. »

Avec vigueur, il traça du pouce un signe de croix sur le front d'Ernaut puis le salua d'un sourire gauche. Ernaut tenta de le retenir d'une question, mais le vieil homme s'éloigna avec agitation, levant la main en une ultime bénédiction.

Perturbé par cette discussion, Ernaut rentra sans tarder dans la petite cellule. Il arriva au moment où Raoul s'apprêtait à se glisser sous la couverture.

« Je viens d'avoir bien étrange confession d'un des chanoines.

--- Il m'avait tout l'air d'avoir la marmite fêlée, non ? J'ai préféré m'esquiver...

--- Fort possible, mais il m'a malgré tout donné de quoi pourpenser... Il semble persuadé que le chanoine versait dans la superstition, l'hérésie ou la magie, je ne saurais dire.

--- Que voilà bien marotte de clerc ! Je suis bien content d'officier à la Cour des Bourgeois et nullement en tribunal ecclésiastique. Seul un érudit comme mon frère peut y comprendre quelque chose. »

Ernaut reconnut que c'était là une évidence : il n'était pas apte à appréhender une éventuelle déviance de la part du clerc défunt. Herbelot l'aurait été, sans nul doute. Il s'agissait peut-être d'un groupe d'hérétiques qui professaient des pratiques interdites et qui auraient pris peur avec les investigations d'un homme tel que le secrétaire de l'archevêque. Le chanoine et son valet auraient pu succomber lors d'une cérémonie secrète, leurs corps abandonnés suite à l'échec, voire pire, au succès de celle-ci.

Il repensa au domestique et au fait que c'était lui qui aurait perverti son maître. Il faudrait, le lendemain, se renseigner sur ce dernier. Il avait peut-être de la famille, des amis, sur lesquels il serait bon d'en apprendre plus. Il y avait de fortes chances pour qu'ils partagent ses croyances, ou en aient une connaissance au moins superficielle. Le vicomte saurait l'orienter en ce sens. Depuis qu'il était arrivé en Terre sainte, il avait été surpris de voir le nombre de pratiques religieuses différentes qui se côtoyaient. Il n'avait jamais envisagé que certaines soient contaminées par cette proximité même.

Chapitre 3
----------

### Lydda, palais épiscopal, matinée du mercredi 17 décembre 1158

Une fois son déjeuner avalé, Ernaut prit de nouveau la direction du bâtiment où le vicomte officiait. Le temps était encore maussade, une brume saline s'efforçant de résister aux assauts d'un soleil mitigé. La température était néanmoins agréable et il s'était contenté de sa cotte. Il arriva au moment où Maugier du Toron s'apprêtait à faire une ronde. Celui-ci lui expliqua qu'une importante caravane en provenance du sud était parvenue en ville la veille et que de nombreux échanges allaient se dérouler aujourd'hui. Il était bon de rappeler aux commerçants la présence du pouvoir local, ce qui améliorait également la confiance dans le calme et la stabilité de la cité.

Maugier semblait dédaigner l'usage du cheval et prit la tête d'une demi-douzaine d'hommes à l'équipement disparate. Il convia aimablement Ernaut à se joindre à eux. Il comptait, pour finir sa ronde, contrôler une section des fortifications, faisant noter à un scribe les éventuels travaux et réparations à effectuer dans la maçonnerie. Son vaste mouchoir à la main, il avançait d'un pas rapide en présentant les différents quartiers à son invité. Il n'y avait guère de souks bien définis, plusieurs zones mêlant différentes fonctions artisanales. Les marchés de fruits et légumes frais étaient moins impressionnants qu'à Jérusalem, un bon nombre d'habitants ayant leur propre jardin dans l'oasis accolée. Il se trouvait par contre plus de marchands de poisson et de produits de la mer, au grand plaisir d'Ernaut.

La ville ne possédait pas vraiment d'activité industrielle dédiée à l'exportation, mais revendait d'importantes quantités de nourriture aux cités à l'agriculture moins prospère. Les caravanes de chameaux qui prenaient la route ployaient sous les chargements de grains, de pois et d'huile.

Alors qu'ils faisaient une pause auprès d'un bassin d'eau fraîche, le vicomte s'enquit de façon discrète sur ce qui amenait Ernaut vers lui. Il avait bien compris que ce n'était pas la découverte de Lydda qui le motivait.

« Que pourriez-vous me dire du valet du chanoine ?

--- Aurait-il à voir en cette affaire ? Lui aussi a perdu la vie !

--- Je me disais juste qu'il faudrait voir auprès de sa famille, de ses amis, s'il n'aurait pas échappé confidence ou secret qui pourrait expliquer leur fin à tous deux. »

Reconnaissant la pertinence de la remarque, Maugier reprit la route tranquillement, cherchant un petit moment dans ses souvenirs avant de répondre.

« Je dois avant tout dire que je ne l'ai guère connu. Je l'ai de certes croisé à de multiples reprises, mais il en était de ce valet comme des autres qui ne sont pas à notre service : on n'y prête pas garde. Il servait le chanoine Waulsort et pas l'évêque. On le voyait donc fort peu en le castel. »

Il se gratta le cou avec insistance, cherchant à en déloger une puce particulièrement vorace.

« Je me suis occupé de ses funérailles, il n'avait nulle famille en la cité. Il était venu de loin, ne se mêlait guère aux gens.

--- De quel pays était-il ?

--- Je ne sais pas exactement. Il avait l'air syriaque, mais il est parfois malaisé d'en juger. En tout cas il n'était pas latin.

--- Savez-vous quelle église il fréquentait ?

--- Il me semble bien qu'il assistait aux offices de Saint-Georges. Je n'ai aucun souvenir de l'avoir vu communier, mais je l'ai encontré dans la nef, de sûr.

--- Il était donc bon chrétien » souffla Ernaut, presque déçu.

Surpris, le vicomte prit acte de cette étrange remarque. Il en résulta un énigmatique sourire, qu'il conserva alors qu'il s'épongeait le cou. Autour d'eux des gamins tournoyèrent un instant, se bombardant les uns les autres de balles de feutre.

« Le doyen saura peut-être vous en dire plus, sans que cela ne soit certain. Mais il se trouve peut-être quelques intéressantes affaires du valet parmi celles qu'il aura fait ramasser.

--- Qu'a-t-il donc pris ?

--- Il est exécuteur testamentaire du chanoine, je vous l'ai dit. Il a donc fait porter à l'abri ce qu'il pensait de valeur en la maison. Ce doit être quelque part au castel.

--- Pourquoi ne m'en a-t-il rien dit ? J'ai fouillé la demeure en pure perte ! »

Le vicomte leva les mains et les yeux au ciel en guise de réponse. Il avait pris son parti de l'indépendance du collège canonial et ne s'en offusquait plus. Profitant de leur autonomie légale, ses membres en faisaient à leur aise vis-à-vis du pouvoir séculier. Par chance, à Lydda, c'était l'évêque qui décidait en dernier ressort dans les deux juridictions. Néanmoins, cela ne facilitait pas les choses. Ernaut commençait à comprendre pourquoi le vicomte n'était guère contrarié de voir que l'enquête revenait à un autre. Il devait être fatigué de se heurter toujours aux mêmes écueils.

« Je vais vous laisser achever votre tour, sire vicomte. Peut-être pourrai-je avoir audience auprès le doyen rapidement.

--- J'entends les cloches qui sonnent sexte. Le temps de retrouver le palais, vous l'encontrerez au sortir du sanctuaire. »

Remerciant chaleureusement le gros vicomte, Ernaut parcourut tranquillement le chemin en sens inverse. L'activité dans les rues était à son comble. Une file de chariots à bœufs s'étendait dans la cour du palais lorsqu'il y parvint. Il obliqua vers le cloître, certain que les officiants y passeraient pour retourner à leurs occupations. Il entendit le chant des prêtres qui s'envolait parmi les grincements des cordages, les appels des hommes et les bruits des outils.

Le lieu était conçu comme une modeste zone d'agrément, l'imposant jardin offrant toute la place nécessaire aux plantes potagères et aux simples destinés à la pharmacopée. Plusieurs massifs de fleurs traçaient des motifs réguliers sous un sycomore harmonieusement taillé et une poignée de palmiers. De petits bancs permettaient d'y goûter l'ombre en été et le calme toute l'année, voire le silence quand les travaux stoppaient. Ernaut s'y installa, le regard vissé vers la porte ouvrant sur la basilique.

Il allait s'assoupir quand il reconnut la silhouette chenue qui avançait péniblement dans la galerie. Le doyen Élies avait toutes les apparences d'un homme usé et malade. Mais il s'en trouvait parfois parmi ceux-ci d'assez alertes pour enterrer tous ceux qui espéraient après leur place. Il marchait les mains dans le dos, tête en avant, et paraissait sur le point de chuter à chaque pas. Voyant qu'il était seul, Ernaut sauta sur l'occasion et s'avança bille en tête dans sa direction.

« Le bon jour, père doyen Élies. J'avais espoir de vous encontrer justement. Auriez-vous un peu de temps à m'accorder ?

--- N'en avons nous pas fini ?

--- Il semblerait que certaines omissions aient été faites. »

Devant l'affirmation laconique et le visage fermé d'Ernaut, le doyen fit un geste de la main pour l'inviter à le suivre dans la salle du chapitre où l'attendait son bureau et un siège. Il s'y laissa choir avec soulagement avant d'attraper un verre émaillé et d'y verser un peu de vin sans même en proposer à Ernaut.

Le lieu semblait n'avoir guère bougé depuis la dernière visite d'Ernaut. Il se demandait si le père Élies ne passait pas son temps à dormir, tout en faisant croire qu'il travaillait. Il prit place à son tour sur un tabouret, qu'il approcha de la table, face au chanoine.

« Vous ne m'aviez pas indiqué avoir retiré des affaires de la demeure de Régnier de Waulsort.

--- Je n'ai nulle souvenance de cela. Mais quelle importance ? Nous faisons toujours ainsi pour préserver des larrons et pillards.

--- Il se trouve parmi ces affaires peut-être le début d'une explication de ce qui c'est passé. »

Le doyen adressa un regard aigu à Ernaut. Il avait l'air d'une bête traquée qui ne sait comment esquiver le coup de grâce.

« Le sire évêque, l'archevêque et même le patriarche espèrent que nous trouverons raison à tout cela. Il n'y a nulle crainte à leur révéler toute la vérité !

--- La vérité ! Ah ! Parlons-en ! Que crois-tu savoir, sergent, à ce propos ? J'ai étudié, longuement, et j'ai aussi prié autant qu'il est possible à un homme. Pourtant je n'ai encore aucune idée de ce que cela pourrait être. »

Il se pencha en avant, posant ses deux mains sur le plateau.

« Ma tâche est de préserver ce sanctuaire, de ne laisser aucune ombre entacher sa splendeur. Je le dis sans crainte, car mon cœur est animé d'honnêtes intentions. Comment puis-je savoir si tu chemines à mes côtés ?

--- Je suis là, car on m'a mandé et je sers fidèlement le roi. Mais je comptais aussi Herbelot comme un ami, avec lequel nous avons affronté grands périls. Accordez-moi la grâce de ne point me croire mécréant. »

Le vieil homme pencha la tête, creusant ses joues plus encore à serrer la mâchoire, avant de fermer ses paupières lourdes, retombant affalé sur son siège.

« Soit, je veux bien t'accorder cela. Veille à pourpenser que tout ce que tu pourrais dire a grand risque d'entacher un saint lieu. Nombreux sont les ennemis de la Foi qui seraient trop heureux de profiter de pareille aubaine. »

La curiosité mise en appétit par un tel préambule, Ernaut hocha la tête avec sérieux, bien que son cœur s'emballât à l'idée de devenir dépositaire d'un secret. Le père Élies se leva et lui fit un signe de la main, se dirigeant de nouveau vers le cloître. Ils empruntèrent un passage avec un escalier qui menait à l'étage. Là, un couloir desservait de petites cellules similaires à celles qu'Ernaut et Raoul occupaient dans l'aile de l'hôtellerie. Les pas traînants du doyen éprouvaient durement la maigre patience d'Ernaut, tenté à plusieurs reprises de prendre le vieil homme dans ses bras afin de gagner du temps. Arrivé devant une des portes, le père Élies vérifia que personne n'était aux alentours puis sortit une clef de ses robes et déverrouilla la serrure avant de pousser l'huis.

La pièce était petite, avec une seule banquette et une fenêtre avec un volet de bois intérieur. Elle était remplie de sacs, de paniers, de coffrets empilés pêle-mêle en un équilibre hasardeux. De fragiles récipients de verre étaient tête bêche dans des étuis de paille et de feuilles de palmiers tressées, partout des besaces ventrues offraient à voir des rouleaux de parchemin, des tranches de reliures de codex. Des pots scellés de bouchons coincés d'étoffe nouée, de lacets, s'amoncelaient en tas précaires, depuis de fines et délicates fioles jusqu'à de véritables jarres destinées au grain.

Ernaut écarquilla des yeux ronds. Il avait l'impression de se trouver devant le contenu d'une boutique d'apothicaire. Ondulant au milieu de ce fatras, le doyen glissa jusqu'au fond et ouvrit le volet, dévoilant une petite fenêtre en claustra.

« Tout est là. Je l'ai fait porter au plus vite, par quelques valets de confiance.

--- Mais qu'est-ce donc ?

--- Les affaires du chanoine. Tout ce qui s'entassait dans son étude... »

Il s'empara d'un frêle flacon de verre au profil élancé, le contemplant sans le voir.

« Le père Régnier était homme de grande sapience, mais aussi fort discret. Je me demandais s'il n'œuvrait pas à découvrir quelques recettes de pigments de qualité pour orner le sanctuaire. Il avait en charge cette tâche, vous savez ? »

Ernaut trouva l'hypothèse intéressante, quoique formulée sans grande conviction. Si c'était bien le cas, pourquoi le doyen aurait-il eu une telle peur qu'il eût tenté d'effacer les traces de tous ces travaux ?

« Père doyen, avez-vous savoir de quelque mystère en tout cela ?

--- Savoir, certes non. Crainte, assurément. »

Il reposa la délicate fiole, parcourant des yeux les objets amoncelés là.

« Je ne voudrais pas que d'aucuns bâtissent sur cela pour porter ombrage à la réputation d'un homme de bien. Ces terres sont tellement propices aux malines influences... »

Il attrapa un des sacs, en délivra un lourd volume relié de cuir, l'entrouvrit sans conviction et le reposa.

« Le savant espère la connaissance comme le voyageur l'oasis. Mais certaines choses n'ont pas été faites pour en voir les secrets violés par les mortels que nous sommes. Pourquoi aller chercher auprès de païens et d'hérétiques des savoirs inutiles ? Nous avons les Saintes Écritures, les commentaires des Pères et les lettres de saints frères. Cela suffit à rejoindre les cortèges de Bienheureux et la Jérusalem Céleste. Les autres voies ne sont qu'impasses stériles. »

Il se tourna vers Ernaut, ses yeux larmoyants brillant d'une énergie, peut-être mâtinée de colère, étrange dans ce corps malingre.

« Pourquoi risquer de se perdre dans les mêmes marigots que ceux qui ont englouti Régnier ? Malgré sa Foi et sa sapience, il n'a su se détourner à temps. Il serait plus sage de ne pas le suivre.

--- Je ne suis pas savant chanoine, et pas même clerc éduqué. Je n'aspire nullement à comprendre ce qu'il espérait accomplir. Je souhaite simplement comprendre ce qui lui est arrivé, à lui et à son valet. Pour apaiser nos craintes en ce qui concerne la mort d'Herbelot. »

Le doyen approuva, rassuré, presque souriant.

« C'était une âme pure que celle du secrétaire de l'archevêque. Et pourtant il n'en a pas réchappé. C'est là ma crainte, le péril est mortel autant pour les corps que pour les âmes.

--- Sauf que si c'est une main mortelle qui est derrière tout cela, il nous faut la saisir et la garroter serré, afin d'apaiser les âmes de nos amis.

--- Les leurs ou les nôtres ? »

Sans plus un mot, le chanoine referma le volet, plongeant de nouveau le bric-à-brac dans l'obscurité. Lorsqu'ils sortirent, il verrouilla la porte avec soin et allait renouer la clef à sa ceinture quand Ernaut l'arrêta d'un geste.

« Père doyen, accepteriez-vous de me confier la clef pour les jours qui viennent ? »

Le vieil homme parut horrifié à l'idée et éloigna spontanément sa main du géant qui lui faisait face.

« C'est ce que j'avais accepté pour votre ami et voyez ce qu'il en est advenu. Je ne serai pas cause d'une autre mort !

--- Je ne suis pas fin lettré comme lui et peut-être serai-je moins doué pour y comprendre quelque chose. Mais ce qui est certain, c'est que l'esprit malin derrière tout cela y regardera certainement à deux fois avant de chercher à s'en prendre à moi. »

Hésitant encore un moment, le père Élies étudia l'énorme masse du colosse qui se tenait face à lui puis déposa finalement la clef dans la large paume offerte. En faisant cela, il sembla s'affranchir d'un poids écrasant qui appuyait sur ses frêles épaules.

### Lydda, cellules du dortoir canonial, après-midi du mercredi 17 décembre 1158

Ernaut était allé directement chercher Raoul afin de lui faire part de sa découverte. Il lui semblait avoir fait une grande avancée dans leurs investigations et il était plein d'optimisme. Par ailleurs, il se sentait bien incapable de s'attaquer à pareil assemblage de documents hétéroclites. Il savait écrire grossièrement, péniblement lire, et certes pas le latin. Ses compétences étaient surtout orales, où il excellait à échanger a minima dans n'importe quelle langue au bout de peu de temps à l'entendre. Mais s'il parvenait désormais à balbutier quelques mots dans pas mal de dialectes locaux, il aurait été bien en peine d'en reconnaître la moindre phrase manuscrite.

Le jeune scribe émit un sifflement en découvrant la cache. Il confia à Ernaut n'avoir jamais rien vu de tel. Ce n'était pas selon lui simple matériel d'apothicaire. Il y avait là des ustensiles de grande rareté. Il proposa à Ernaut de les organiser un peu, de trier ce qui était texte et de le mettre à part, car c'était ce qu'ils devraient fouiller en premier.

« Ce devait être un homme exceptionnel. Même mes maîtres au Temple n'avaient rien de tel ! Autant de livres pour un seul homme !

--- Ce n'est pas pour nous simplifier la tâche, grogna Ernaut. S'il nous faut tous les parcourir, nous serons encore ici pour les Pâques !

--- Bien au-delà, même. Certains sont écrits en griffon. J'en vois aussi en langue des mahométans et d'autres encore dans des graphies qui me sont inconnues. Je n'en viendrai jamais à bout tout seul, Ernaut !

--- Nous n'avons nul besoin de comprendre tout cela. Il nous faut surtout ses tablettes, ses notes. Et voir s'il y a là des sentiers qui nous mèneront vers une explication. »

Tout en parlant, il épluchait un tas de feuilles de papier couvertes d'une écriture fine et régulière. Incapable d'en comprendre le sens, il en brandit une poignée sous le nez de Raoul. Après un rapide survol, celui-ci acquiesça.

« Il s'agit là de lettres qu'il a reçues. Il y a porté quelques commentaires, d'ailleurs ! Voilà ce qu'il nous faut, en effet.

--- Par contre, je ne vois rien qui puisse appartenir à son valet. Ce ne sont là que propriétés d'un riche érudit, pas d'un servant.

--- Pourtant, tu m'as confié qu'on prêtait à son domestique une male influence. Peut-être était-il partie prenante dans tout ceci. »

Il reposa les feuillets parmi un tas sur la banquette puis s'attaqua à déplacer des paniers pour inspecter les corbeilles sous la table. Ernaut, pour sa part avait empoigné le premier codex d'une grande pile et se laissait distraire par les planches illustrées de végétaux. Jamais il n'avait eu l'occasion jusqu'à ce jour de parcourir un pareil chef-d'œuvre. Il tournait les pages de parchemin avec respect, appréciant le craquement autant que le délicat toucher de la peau. Les couleurs vives, les tracés précis y étaient entourés de textes dans une langue inconnue. Cela ne donnait que plus d'attrait à ce qu'il considérait comme un ouvrage magique. Il se demanda s'il pourrait posséder un pareil trésor un jour, et s'il saurait en tirer la quintessence. Il le referma avec soin, caressant la reliure de cuir épais, martelée de décors en relief. Puis passa au suivant.

Comprenant qu'ils pourraient se perdre dans l'admiration des livres, Raoul tenta d'attirer l'attention d'Ernaut en sortant quelques objets de verre des paniers. Il s'y trouvait des cornues, ballons et vases de plusieurs tailles, dont certains avaient le cul brûlé d'avoir été porté au feu. Peu enclin à s'exposer aux émanations possiblement toxiques, Ernaut ne prit pas le risque d'en renifler l'intérieur. Plusieurs supports, de fer et de bronze, permettaient de les maintenir en place. Il y avait des dizaines de contenants dont les formes devaient correspondre à un usage précis. Mais c'était là un puzzle qu'ils étaient incapables d'élucider.

Ernaut découvrit également plusieurs creusets encore imprégnés de matières calcinées, ainsi que des lampes servant de toute évidence à chauffer les verres. Des pilons et mortiers, en pierre et métal, parfois décorés de motifs figurés ou de textes en langue arabe complétaient cet hétéroclite assortiment. Enfin, plusieurs dizaines de récipients en céramique, soigneusement bouchés, semblaient contenir les ingrédients de l'étrange cuisine. Les deux compagnons convinrent rapidement qu'ils n'avaient pas intérêt à les ouvrir. Au mieux ils y découvriraient des produits inconnus dont ils ne sauraient que faire, au pire, il s'en dégagerait quelque poison.

Au bout d'un moment, n'y tenant plus, Ernaut se décida à poser la question qui les taraudait tous les deux.

« À ton avis, à quelle sorte de magie le chanoine pouvait-il s'adonner ? »

Reposant l'ustensile qu'il tenait en main, Raoul haussa les épaules.

« Peut-être se passionnait-il pour la science des plantes, la médecine ou les procédés servant aux arts...

--- Cela n'enfrissonerait pas tant les autres chanoines !

--- Tous ne sont pas hommes de science. Certains sont même à l'opposé.

--- J'en suis bien d'accord. Pourtant, ils ne noirciraient pas la mémoire de l'un des leurs s'ils n'avaient grand-peur. »

Il reposa un autre volume sur la pile, attrapant un portfolio empli de feuilles volantes.

« Tu crois que l'un d'eux aurait pu le tuer et supprimer Herbelot par suite, craignant d'être découvert ?

--- Un des chanoines ? Quelle étrange idée ! De certes ils peuvent avoir grande haine, mais je vois mal un de ces vénérables tonsurés aller occire nuitamment deux hommes. Ce ne serait pas tant l'envie de le faire que les capacités à se risquer à si laborieuse tâche. »

Ernaut repensa aux histoires que son père leur contait à la veillée. Il se souvint des rumeurs et des légendes qu'on se chuchotait entre enfants, afin d'éprouver le courage les uns des autres.

« Chez moi, il y avait un vieux rebouteux qui savait comment guérir le mal de dos par un moyen magique. Il fallait lui amener un petit chien, qu'il préparait de ses charmes et sortilèges. Puis il fallait dormir avec l'animal dans son lit et le chien attrapait alors la douleur.

--- C'est là de bonne fame. Je ne suis pas certain que cela soit magie. Ma mère connaissait une femme qui opérait de même, mais avec des chats. Outre, il n'y a là nul secret.

--- Il se disait qu'on pouvait faire de même avec une ficelle, pourvu qu'elle soit droitement préparée.

--- Là, je veux bien croire à quelque envoûtement. Je ne vois pas comment des brins de chanvre pourraient récupérer une douleur à autrui !

--- C'est peut-être en cela qu'il faut être savant pour opérer. »

Raoul ricana tout en prenant de nouveaux rouleaux.

« Je n'encrois guère que Waulsort faisait cela pour soulager les dos des portefaix œuvrant à la basilique !

--- Va savoir ! gloussa Ernaut. Mon père disait que celui qui saurait libérer l'homme des poux et des puces serait reconnu saint avant sa mort. »

La plaisanterie leur redonna du coeur à l'ouvrage et ils ne tardèrent pas à contempler de larges piles de documents entassées sur la couche. Ils avaient trié les flacons de verre, la céramique et les ustensiles de laboratoire et les avaient regroupés dans de grandes corbeilles le long du mur et sous la table.

« Quelle misère qu'ils n'aient pas tout laissé en place dans la demeure de Waulsort, pesta Raoul. Il aurait été bien plus commode de s'y retrouver. Comment savoir s'il existe quelque logique là-dedans ? J'aurais pu ainsi deviner le contenu des textes que je ne saurai lire. »

Il posa une main sur une des piles de documents.

« Je vais mettre de côté ce qui est écrit en latin, ainsi qu'en griffon. J'entends un peu la lecture de cette langue, péniblement. Pour le reste, on verra plus tard. »

Ernaut approuva et s'employa à lui remettre les feuillets, rouleaux et tomes qui lui semblaient correspondre. La tâche leur prit encore une partie de l'après-midi. Ils ne pouvaient s'empêcher de parcourir les illustrations merveilleuses qu'ils découvraient.

« Il demeurait quelques documents en la demeure de Waulsort. Peut-être devrai-je te les apporter, au cas où ils contiendraient éléments d'importance.

--- Penses-tu qu'ils auraient fait pareille omission ?

--- Les valets n'entendent pas le latin. Peut-être n'avaient-ils plus assez de place et ont décidé de laisser ce qu'ils pensaient mineur. Ce n'était nullement des livres, mais de simples lettres, de ce que je me souviens. »

Raoul opina.

« Tu n'auras qu'à y aller une fois que nous aurons trié tout cela. Je resterai ici, si tu es d'accord pour me laisser la clef.

--- Pour sûr. Il ne faut en aucun cas que ce qui a été celé ici en sorte. Le doyen a été impératif à ce sujet. »

Lorsqu'ils eurent achevé, Raoul commença à empiler sur la table les ouvrages qu'il survolerait plus tard, conservant ceux dont il espérait comprendre le sens. Cela faisait encore une belle quantité, impressionnant Ernaut qui n'avait jamais vu autant de codex et de rouleaux à la fois.

« Comment a-t-il fait pour amasser autant de volumes ? Il fallait avoir vaste réseau de correspondants pour dénicher des ouvrages aussi spécialisés et en obtenir copie.

--- Oh, certains doivent se trouver chez le marchand. Peut-être pas ici, mais Jérusalem, Damas ou Antioche regorgent de négociants spécialisés. Il faut voyager, de certes, ou faire savoir à des commissionnaires ce qu'on désire, mais cela n'est pas si compliqué à obtenir.

--- Peut-être était-ce là l'œuvre de son valet ? Le chanoine m'a dit qu'il était possiblement originaire d'ici. Vu son prénom, Thibault, peut-être était-il un baptisé[^17] ? Il aurait pu avoir grand nombre d'amis ici et là, et connaître moyen pour le chanoine d'obtenir ce que le doyen désigne comme magie païenne. »

Hochant la tête, Raoul souscrivait au point de vue.

« Sais-tu que les mages viennent souvent d'Orient ? Ma mère nous contait leurs histoires emplies d'ifrits[^18] et de djinns alors que nous étions enfants.

--- Il nous faut absolument en apprendre plus sur ce Thibault ! Si c'était un mage au savoir occulte, il aurait pu pervertir le chanoine par sombre maléfice. »

Ayant achevé leur tri préliminaire, Ernaut se leva et s'étira en bâillant. Ce faisant, il apprécia d'un coup d'œil le volume d'affaires entassé dans la cellule.

« Tout de même, cela fait sacré fatras. Je ne sais combien coûte chacun de ces objets, mais il n'y a nul doute que seul un homme fortuné pouvait se les procurer.

--- Les livres ne sont pas tant chers, pour peu que les miniatures n'y soient pas trop présentes ou la calligraphie trop illustrée.

--- Cela représente malgré tout coquette somme, même pour chanoine prébendé. Était-il de bonne famille pour s'offrir tout cela ? »

Ernaut commençait à s'agiter tandis que son cerveau se mettait à bouillonner. Il faisait les cent pas dans le minuscule espace devant la porte qui n'était pas encombré de contenants et de paniers.

« Je questionnerai le doyen à ce sujet. Il doit se charger de trouver l'héritier, il saura me désigner celui qui aurait fortune faite avec la mort de Waulsort.

--- Crois-tu que tout cela ne serait que sombre histoire de fortune ?

--- Si le chevalier Régnier d'Eaucourt avait pu nous compaigner, il t'aurait répondu que, souventes fois, c'est le motif le plus simple qui doit être préféré. »

Sur cette ultime remarque et un sourire, il prit la porte, impatient de mettre sa théorie à l'épreuve. Il passa rapidement dans leur chambre récupérer une besace pour y glisser les documents qui traîneraient encore dans la maison de Waulsort. Puis il alla retrouver le doyen Élies dans la salle du chapitre. Pour une fois bien réveillé, ce dernier dictait à un jeune clerc des courriers relatifs à des questions d'organisation des fêtes de Noël. Rongeant son frein, Ernaut patienta un petit moment. Il n'en pouvait mais de voir le vieil homme ânonner ses phrases avec lenteur, reformulant sans cesse des missives adressées à d'autres ecclésiastiques. Ernaut se préparait à partir pour revenir ultérieurement lorsque le chanoine lui fit signe de s'approcher, tout en congédiant son assistant.

« Mon père, j'aurais besoin de savoir qui sera le destinataire de la fortune du chanoine Waulsort.

--- Je ne le sais encore, mon fils. J'ai écrit plusieurs lettres en ce sens, pour la plupart à destination de terres lointaines.

--- N'a-t-il aucun hoir en les environs ?

--- Pas que je sache. Il venait de la terre des Alemans, et y avait conservé nombreux amis. Mais nulle famille ne l'a suivi quand il vint outre-mer.

--- Tout ce... toutes ces étranges possessions finiront entre quelles mains ?

--- Je ne sais pas encore. Il me semble que le plus sage serait de vendre à l'encan ce qui nous semble ne pas relever du savoir occulte. Pour le reste, nous aviserons avec le sire évêque. Il ne conviendrait pas de propager idées fausses et pratiques malines. »

Ernaut acquiesça en silence. La remarque était pertinente. En outre, cela lui faisait se demander si on n'aurait pas éliminé le père Waulsort pour lui dérober un ouvrage secret, difficile à obtenir. En tout cas, la piste de l'héritier félon semblait avoir fait long feu. Il remercia le doyen et le salua avant de sortir.

Le cloître s'assombrissait déjà et il lui fallait se hâter pour aller à la demeure du chanoine. Les portes de la forteresse fermaient pour complies, il n'était pas certain que le guichetier le laisse entrer après cette heure. Il ne tenait pas à passer la nuit dans la maison où deux hommes avaient été mystérieusement brûlés à mort. Il prit donc la direction de la sortie d'un bon pas.

### Lydda, demeure du chanoine Waulsort, soirée du mercredi 17 décembre 1158

La petite maison du chanoine ne recevait guère de lumière et, avec les cloisons ajourées en guise de fenêtre, Ernaut ne voyait pas très clair. Il se contentait donc de glisser dans sa besace tous les papiers qu'il découvrait, sans prendre le temps d'en vérifier la pertinence. Le doyen ne lui reprocherait certainement pas d'avoir terminé l'ouvrage bâclé par les valets.

Désormais averti sur les mystères qui s'étaient déroulés là, il entreprit de sonder les murs et d'étudier plus en détail les meubles. Seulement, il s'avéra bientôt qu'il n'y voyait plus guère. Il n'avait que peu envie de lancer un feu juste pour quelques mèches afin de s'éclairer. Il se résolut donc à prendre une lampe et à en quémander l'allumage dans le quartier.

Au moment où il sortit, il entendit la cloche sonner les vêpres. Cela lui laissait encore un peu de temps. La petite rue était bien sombre et il dut se rendre à une de ses extrémités pour trouver une porte où frapper. Le voisin qui lui répondit paraissait se méfier d'un tel colosse toquant au coucher du soleil, mais il accepta de bon gré d'aller allumer la mèche à son feu. Ainsi équipé, Ernaut put rebrousser chemin vers la demeure de Waulsort.

Il fronça les sourcils en découvrant plusieurs personnes dans la venelle. Il avait tout d'abord cru que ce n'étaient que des passants, mais ceux-ci, trois hommes, habillés comme des Latins, semblaient attendre. Jusque là, il avait estimé qu'en étant sous la protection de l'évêque, il n'avait nul besoin d'être armé. Il regretta aussitôt sa décision. Même son gros couteau au fond de sa besace ne serait guère aisé à sortir. Son sentiment d'alarme augmenta quand il aperçut une épée sur les hanches de l'un d'eux, un maigrelet au nez imposant, qu'il grattait d'un geste nerveux. Derrière lui, ce ne semblaient être que de simples acolytes sans équipement pour le combat. Ernaut vérifia d'un coup d'œil si l'homme avait des éperons au pied. Il n'en vit pas. De plus, en s'approchant, il découvrit que la tenue n'était pas de la dernière fraîcheur. En fait, ils avaient tous l'air assez las et poussiéreux.

Celui qui paraissait être le meneur faisait mine d'attendre tranquillement, mais ses regards trahissaient sa tension. Il ne semblait pas goûter de se trouver face à un si formidable interlocuteur. Il arrivait à peine à l'épaule d'Ernaut et devait peser la moitié de son poids. Quand le jeune colosse se plaça devant eux, la lampe éclairant son visage de lueurs de braise, il déglutit lentement avant de s'exprimer.

« Le salut à toi, compère. Es-tu valet de l'évêque ?

--- En quoi cela vous concerne ?

--- Calme, l'ami. Je t'ai vu issir de la demeure du chanoine, j'en ai donc pensé que tu œuvrais pour les autorités d'ici. »

Ernaut se dit qu'il ne serait pas mauvais de se prétendre au service de Constantin, ce qui n'était qu'une très légère entorse à la réalité, après tout. Il s'empressa donc de répondre.

« Je suis là en effet en service pour le sire Constantin. Et vous, qui êtes-vous pour surveiller ainsi mes allées et venues en ses terres ?

--- Des amis du père Waulsort.

--- En ce cas, j'ai male nouvelle à vous conter : on l'a porté en terre voilà peu. Si vous vous hâtez, vous aurez temps d'aller prier sur sa tombe, en la nécropole du castel Saint-Georges.

--- On l'a appris, c'est ce qui nous a menés en ces lieux... Pourrions-nous entrer ? »

Ernaut se contracta. Bien que les trois hommes ne se comportâssent nullement comme des brutes, leurs intentions étaient plus que troubles.

« Certes pas. Le sire doyen est exécuteur testamentaire du père Waulsort, et nul ne saurait aller et venir en sa demeure sans autorisation expresse de celui-ci. Il a été bien clair en cette affaire. »

Conscient que sa situation était déjà précaire dans la ruelle, Ernaut estimait qu'il serait folie d'aller s'enfermer dans un lieu clos avec ces trois sbires. L'homme perçut son dilemme et s'exprima alors d'un ton cauteleux.

« Et si je te proposais quelques deniers pour nous déverrouiller la porte et nous laisser entrer un moment ? Tu pourrais aller boire un godet à la plus proche taverne, nous t'y retrouverions affaire faite.

--- Ce serait bien déshonnête de ma part envers mes maîtres. Qu'espérez-vous trouver là-dedans ?

--- Rien qui ne puisse t'intéresser, compère. Prends-donc les quelques monnaies, cela ne lésera pas un mort de nous ouvrir sa porte ! »

Se mordillant la lèvre, Ernaut hésitait à accepter. C'était un bon moyen de régler cet imbroglio. Il avait les derniers papiers et rouleaux dans sa besace et avait déjà retourné les lieux en tout sens. Sauf s'ils savaient où chercher exactement, ils n'avaient que peu de chance de dénicher quoi que ce soit. En outre, il avait une idée en tête qui lui permettrait de ne pas complètement se dévoiler. Il hocha lentement le menton, tendant la main.

« Tu as parlé de combien de deniers, déjà ? »

L'homme dévoila une dentition abîmée et glissa la main sous sa cotte, pour en ressortir une petite bourse dont il fit tomber six pièces dans la paume d'Ernaut. Interrogeant Ernaut du regard, il comprit qu'il en fallait plus, et en versa douze. C'était une coquette somme, ils avaient donc un gros intérêt en jeu.

Prenant l'air aussi flegmatique que possible, il leur déverrouilla la porte puis désigna un quartier non loin où ils pourraient le prévenir dans une taverne quand ils auraient fini leurs affaires, pour qu'il puisse refermer la maison. Affectant un sourire engageant, il leur confia la lampe et s'en fut sans un regard en arrière. À peine eût-il tourné au coin qu'il s'agita, demandant aux échoppes encore ouvertes aux environs s'ils savaient où trouver Hashim.

Il fut orienté vers une boutique en partie écroulée où le gamin demeurait avec les siens. Ils furent réticents à répondre, ayant déjà barricadé pour la nuit, mais ils écoutèrent le tintement des pièces proposées par Ernaut. La vision de la demi-douzaine de disques argentés fit disparaître les dernières réserves. Quelques instants plus tard, une petite silhouette le rejoignait dans la rue, chaperonnée par son père, un homme fluet à l'aspect hirsute. Ernaut lui glissa trois deniers dans la main avant d'expliquer à Hashim ce qu'il attendait de lui. Puis il prit la direction de la taverne la plus proche qu'il pouvait trouver aux abords de la maison de Waulsort. C'était des lieux de veillée fréquents, les gens se dispersant dans les places alentour pour profiter d'un temps à discuter des nouvelles, narrer des histoires aux enfants ou colporter les ragots.

La période ne se prêtait guère aux longues soirées, avec la lune dont le croissant ne se dessinait que peu et la fraîcheur de la nuit hivernale. En tant que Bourguignon, Ernaut goûtait la température plus que clémente pour la saison, mais les locaux y voyaient les semaines les plus froides de l'année. Découvrant une échoppe proposant de la bière, il s'en paya un pichet. Elle était dense et bien moussue, consistante comme il l'aimait. Il envisagea un moment d'aller se chercher du pain, mais il savait d'expérience qu'il avait peu de chance d'en trouver à la mie épaisse comme il en avait le goût. Outremer, les commerces vendaient pour la plupart des pains plats.

Il n'avait pas avalé la moitié de sa boisson que le trio fit son apparition. Le meneur se dirigea vers lui, le visage fermé. Sa voix résonna de colère quand il apostropha Ernaut.

« Tu te moques de nous, l'homme !Il n'y a rien là-dedans. Vous avez tout vidé !

--- Hé quoi, tu ne m'as rien demandé ! Comment je saurais ce que tu espérais ? »

Lui adressant un regard mauvais, l'inconnu s'assit face à lui, sur un ressaut de muret.

« J'ai horreur d'être ainsi moqué ! Où sont ses affaires ?

--- Tout est en le palais de l'évêque, je pensais que vous étiez en quête d'autre chose ! »

L'autre haussa les épaules, désormais plus agacé qu'en colère.

« Et que faisais-tu là, si vous avez déjà tout emporté ?

--- Je devais estimer le travail qui demeure pour finir de vider l'hostel. »

Fulminant, l'homme jetait des regards en tous sens, comme s'il s'attendait à voir surgir un ennemi du moindre recoin qu'il n'aurait pas vérifié.

« Tu as possibilité de nous faire accéder aux paquetages ? Tu n'auras pas affaire à un ingrat.

--- Ça me semble difficile. Il y en a un peu partout, de ce que j'ai compris. Le tout sous clef, à la garde du doyen, du vicomte ou du sire évêque.

--- Et tu ne pourrais chaparder pour nous quelques objets dont je te dirai l'inventaire ?

--- Il demeure toujours un clerc ou l'autre, pour surveiller ses possessions dont ils ont la garde. Mais si vous patientez, ils vendront peut-être le tout. Il ne vous suffira que d'acheter ce que vous désirez. »

« Pour peu que ça soit mis à l'encan » pensa Ernaut par-devers lui. Sa dernière phrase sembla apaiser le longiligne soldat. Il n'était pas vraiment satisfait, mais la colère était retombée et il cherchait la meilleure façon d'appréhender les choses. Ernaut patienta, avalant sa bière à petites gorgées. Il voyait les ténèbres gagner, le palais n'allait pas tarder à être clos pour la nuit.

« Je vais devoir rentrer...

--- Tu demeures en castel saint Georges ?

--- Oïl.

--- Parfait. En ce cas, je ne te retiens pas plus. On reprendra contact si jamais le besoin s'en faisait sentir. Comment t'appelles-tu ?

--- Droin. Droin le Groin, mentit spontanément Ernaut.

--- Fort bien, si jamais des nouvelles te viennent, mon émissaire se dira envoyé de la part d'Aymar. »

Le sbire semblait avoir conclu de cette petite opération de corruption qu'il avait un complice à l'intérieur de l'hôtel de l'évêque. Cela convenait parfaitement à Ernaut, qui se contenta de hocher la tête en guise de salut. Il fit un signe rapide aux deux hommes demeurés en retrait et rendit son pichet à la taverne. Enfin, il alla verrouiller la maison avant de se diriger vers le palais.

Il vérifia discrètement à plusieurs reprises s'il n'était pas suivi. L'idée lui sembla un instant ridicule, étant donné qu'ils pensaient savoir où il habitait. Mais il était aussi possible que cet Aymar ne fût pas tant naïf qu'il l'avait laissé entrevoir. Il n'était en tout cas pas simple sergent sans cervelle. Ernaut croisa mentalement les doigts pour que Hashim accomplisse sa mission sans prendre de risque ni se faire attraper. Peut-être que le trio serait moins timoré face à un gamin que devant un géant aux muscles saillants.

Il arriva au portail au moment où les guetteurs allaient clore pour la nuit. Il avait à peine franchi le seuil que les lourds battants claquèrent. Retrouvant ses supports d'acier, l'imposant bastaing de verrouillage pivota dans un grincement lugubre, alors que le clocher sonnait complies.

### Lydda, hôtellerie du palais épiscopal, nuit du mercredi 17 décembre 1158

Mis en appétit par sa bière, Ernaut se dirigea vers le réfectoire. Il se doutait bien que les feux seraient couverts pour la nuit, mais peut-être pourrait-il quémander un quignon ou des fruits, de quoi satisfaire son estomac. Il n'y avait plus grand monde, la plupart des hôtes s'étant retirés. Un petit groupe d'hommes jouait bruyamment aux dés tandis que deux autres, certainement des marchands, comparaient des tablettes et des documents à la lueur de plusieurs lampes.

À l'extrémité d'une table, Ernaut reconnut Raoul en pleine discussion avec un voyageur fluet, le regard triste malgré des rides rieuses au coin de la bouche. Les oreilles décollées étaient familières, comme le ricanement discret qu'il entendit. En outre, son arrivée dans la pièce n'était pas demeurée inaperçue. Il se laissa donc tomber sur le banc avec enthousiasme.

« Le bon soir, Guillemot ! »

Il saliva en voyant son compagnon sergent du roi en train d'avaler une écuelle emplie de riz parfumé, où surnageaient quelques morceaux de viande.

« Es-tu porteur d'un message annonçant la prise de Babylone qu'on te sert comme prince en son palais ?

--- Rien n'est trop beau pour sergent le roi en mission, sourit Guillemot en avalant une large cuiller. »

Levant un index pour signifier une courte interruption de leurs échanges, Ernaut se releva et alla rapidement vérifier s'il ne restait pas de ce bon plat pour lui. Il ne se trouvait là que des commis, mais l'un d'eux accepta de s'enquérir auprès d'un des chefs de cuisine. La moindre denrée était sous surveillance et il en aurait cuit à quiconque aurait cru pouvoir céder à la gourmandise sans en avoir l'autorisation. Il était en veine, lui indiqua le jeune en revenant avec une réponse affirmative, l'évêque avait prévu un repas trop copieux, espérant des invités qui n'étaient pas venus. Le plat s'était refroidi et il était hors de question de rallumer les feux, mais il servit Ernaut avec une belle portion, accompagnée de pain et d'un pichet de vin.

Entretemps le réfectoire s'était encore vidé et il ne demeurait que les chuchotis des deux négociants, qui faisaient écho aux échanges entre Raoul et Guillemot. Ce dernier était un vétéran dans l'hôtel royal, peu exubérant, mais agréable compagnon. Il était ces temps-ci sujet à la morosité, depuis la mort de son épouse. Il n'en faisait que peu étalage, mais se laissait souvent aller à la rêverie, les yeux dans le vague.

« Alors, compère, es-tu ici pour nous porter aide ?

--- J'ai quelques missives pour Jaffa, le détrompa Guillemot, secouant la tête. Je fais halte ici pour vous signifier que le sire sénéchal veut que vous soyez de retour avant la Noël. Il y a de l'ouvrage qui vous attend et il veut que vous ayez fait rapport au patriarche avant cela.

--- Cela nous laisse moins d'une semaine ! Nous sommes loin d'avoir fini, ici !

--- Si tu le dis... T'auras qu'à le répéter au sire vicomte. »

Ernaut maugréa dans son écuelle, avalant quelques cuillérées en silence. Il fit passer la mauvaise nouvelle d'une large rasade de vin.

« Nous avons important ouvrage ici, la Cité ne peut-elle se passer de nous ?

--- La Noël n'est pas tant loin, il faut vérifier que les cens[^19] vont être versés. Tu sais comment c'est...

--- Moi j'espère bien que nous aurons achevé notre tâche ici pour la Noël, intervint Raoul. Je préférerais être en famille à cette occasion !

--- C'est vrai, ça, enchérit Guillemot. N'as-tu pas ta mignonne dans les environs de la Cité ? Il ferait meilleur la voir que demeurer ici, non ?

--- Je ne dis pas le contraire, mais je n'aime guère abandonner labeur d'importance sans l'avoir achevé. »

Guillemot esquissa un sourire, mi-figue mi-raisin. Toute la sergenterie connaissait bien le caractère irascible du jeune homme, aussi entêté qu'un blaireau, un sanglier et un taureau réunis. Il lui faudrait pourtant bien apprendre à rentrer dans le rang.

« A-t-on nouvelles du Nord ? tenta Raoul, espérant faire bifurquer la discussion.

--- Rien de plus depuis que vous êtes partis. Le Turc se tient bien calme, inquiet de savoir à quelle sauce il va être mangé. Il y aura de certes belles batailles à venir dès avant l'été.

--- Aurons-nous une chance d'en être ?hasarda Ernaut. J'apensais que seuls ceux partis avec le roi iraient porter le fer en campagne.

--- Je ne saurais dire, nous n'avons rien appris à ce sujet. Mais le maréchal tempête encore tant et plus, comme il est de coutume. Je n'ose croire que le roi ne l'appellera pas si une chevauchée se décide.

--- Vivement qu'il reparte au nord, celui-là, grogna Ernaut, contrarié de n'avoir pu profiter de la présence de Régnier d'Eaucourt pour cette mission.

--- Ne te fie pas à son air de jouvence. C'est un lion en bataille. Il est d'une lignée de grands chevaliers et saura sûrement retrouver la place de ses pères. Son aïeul est mort les armes en main, alors qu'il avait été gravement blessé dans l'effondrement d'une mine. Et son père croupit dans les geôles sarrasines depuis... »

Il leva les mains en guise d'illustration puis retomba dans le silence. Face à lui, Ernaut avalait le délicieux plat sans y prendre garde. Son esprit tourbillonnait trop pour qu'il arrive à se focaliser sur ce qu'il était en train de faire. Guillemot essuya son écuelle avec soin, y abandonnant de minuscules miettes de pain. Il se rinça enfin la bouche avec une longue rasade de vin coupé d'eau.

« Tu seras sûrement amusé d'apprendre que Houdard, le petit à Droart, a sourire doublement édenté, désormais. Il est passé l'autre soir, tout fier de ses deux quenottes manquantes.

--- Il les a enterrées comme il se doit ?

--- Je ne sais. Dans ma famille, on avait plutôt usage de les vendre comme porte-bonheur.

--- C'est là puissant objet de magie, confirma Raoul. Nous, on les brûle pour éviter de devoir les rechercher une fois au Paradis.

--- En parlant de sortilèges, tu t'y connais en magie mahométane, Guillemot ? »

Cette simple éventualité amusa le sergent.

« Aucunement. À part celle de leurs maudites flèches, qui savent fort droitement se planter en bien indésirables endroits. Il faudrait demander peut-être à ton compère Abdul Yasu.

--- Abdul n'est mie mahométan, et encore moins magicien ! s'amusa Ernaut.

--- C'est en rapport avec ton affaire ici ?

--- Je ne peux t'en parler outre et de toute façon, je n'en sais rien encore. Il y a tant à faire et si peu de temps pour cela...

--- Pour sûr, on rêve tous de se distinguer. »

Il avala une gorgée, servit ses deux compagnons.

« Pourtant, je serais à ta place, je ne m'en ferais guère. Taillé comme tu l'es, tu as de toute façon bonnes chances de finir près le roi. »

La remarque arracha un large sourire à Ernaut, flatté de se voir reconnu pour son physique. Malgré tout, il aspirait à ne pas être réduit à cela.

« Pour une fois qu'on me confie tâche plus intéressante que de fouiller dans les sacoches ou de ramasser les ivrognes et les bagarreurs ! J'aurais cru service du roi plus prestigieux.

--- Le roi conchie son pot de nuit comme les autres, mon gars. Il faut bien des mains pour le vidanger. Et il a déjà pleintée de chevaliers pour brandir sa bannière. Tiens ta place, ce sera déjà plus qu'on espère de beaucoup.

--- Toi, tu fréquentes trop Baset, se moqua gentiment Ernaut.

--- Peut-être bien. Mais s'il a parfois mauvais caractère, il a bon cœur et l'esprit clair. »

Il se rembrunit, attrapa son gobelet et en regarda le fond.

« D'ailleurs, le pauvre a reçu mauvaises nouvelles des siens. Il est parti quelques jours aux fins d'en savoir plus. »

La remarque toucha Ernaut. Il n'appréciait guère Baset, qu'il avait malgré tout connu parmi les premiers dans la sergenterie du roi. Il le trouvait acariâtre et sentencieux. Pourtant, il ne lui souhaitait nul mal. Il se dit qu'il irait faire une petite prière à son intention, d'autant qu'il savait Guillemot assez ami avec lui.

« Tu veux que je mette chandelle en ton nom dans le sanctuaire de saint Georges ? J'irai à la première heure, demain.

--- Ce sera me faire grande amitié que cela, compère. »

Guillemot avala un dernier morceau de pain et plia le canif avec lequel il avait usage de manger. Puis il s'étira, faisant craquer ses articulations.

« Sur ce, je vais aller retrouver la salle commune. Demain, il me faut aller à Jaffa et rentrer aussitôt porter les réponses.

--- Tu vas avoir le cul tanné en selle à courir ainsi !

--- Au moins je n'aurai pas trop froid. Ici ça va encore, mais dans les monts, il y a de quoi attraper bonne engelée. La Cité ne va tarder à blanchir, il fera mauvais être sur les routes quand la neige sera là. J'aime autant être de retour au plus tôt. »

Il se leva puis récupéra son charvin de cuir graissé, sur lequel il avait posé sa chappe de voyage. Il jeta le tout sur son épaule et salua du menton Raoul et Ernaut avant de sortir par la pièce donnant accès aux escaliers vers le grand dortoir. Entretemps, il ne restait plus qu'eux dans le réfectoire. Raoul aida Ernaut à porter les reliefs de leur repas dans la cuisine, où un marmiton ensommeillé se traîna hors de sa paillasse pour les en débarrasser.

La nuit était calme et froide, la lueur de la lune finissante était diffusée par un mince manteau nuageux, ne dessinant que peu les contours des bâtiments. Les deux amis rejoignirent leur chambre en silence et se glissèrent sous les couvertures sans un mot de plus.

### Lydda, hôtellerie du palais épiscopal, matin du jeudi 18 décembre 1158

Ernaut se réveilla très tôt, à peine les lueurs de l'aube pénétraient-elles dans leur petite pièce. Il demeura un moment allongé, au chaud sous ses couvertures. Il appréciait de ne pas devoir partager les couches de la salle commune comme avait dû le faire Guillemot. C'était souvent des litières infestées de puces et de punaises et on passait plus de temps à se gratter qu'à dormir. Il bénéficiait des privilèges qu'on accordait normalement aux chevaliers et aux clercs et sourit à l'idée qu'il devait cela au sale caractère du maréchal Joscelin. Guillemot avait raison, il allait peut-être commencer à l'apprécier, au final.

Se levant sans bruit, il prit ses vêtements et se rendit rapidement au lavoir. Ils avaient découvert la pièce la veille seulement et Ernaut s'était juré d'en tirer avantage. À côté de latrines où l'on pouvait s'installer commodément pour se soulager sans déranger ses compagnons de chambrée, il y avait des seaux et de petits baquets pour faire sa toilette. Un large bassin permettait aux lavandières d'y battre le linge à l'abri. L'eau n'était pas chauffée, mais cela ne rebutait pas Ernaut. Il appréciait de plus en plus les pratiques hygiéniques orientales, et repensait avec dépit à toutes ces années en Bourgogne où il ne se lavait guère plus d'une fois par semaine. Il nota de se procurer du savon dans une des échoppes en ville, ayant à y finaliser ce qu'il avait commencé la veille.

Une fois ses ablutions terminées, il passa rapidement au réfectoire, mais on lui confirma que Guillemot était déjà parti, en selle à peine les portes étaient-elles ouvertes pour laudes. Il tint sa promesse et acheta un cierge à un des camelots, qu'il plaça dans la chapelle dédiée à la Vierge. Cela lui semblait plus adapté comme protection que d'en appeler à un guerrier intrépide pourfendeur de mahométans. Il lui adressa un *Ave* et y ajouta un *Credo* et un *Pater* pour son frère, sa famille, ainsi que Libourc et les siens.

Le chantier reprenait vie lentement quand il quitta la basilique pour aller chercher de quoi rompre le jeûne dans la salle commune. Il se contenta d'un peu de pain, bien décidé à améliorer cela dès qu'il serait en ville. Il espérait qu'à l'approche de Noël, des beignets et autres oublies commenceraient à s'entasser sur les étals. La veille, il avait cru que des gaufres étaient prévues, en voyant les immenses plaques de fer amassées sur une des tables du réfectoire. Il avait déchanté en apprenant que ce n'étaient que des moules à hosties.

Le ciel était toujours aussi morose, d'un gris clair. Un vent très léger faisait danser doucement les feuilles de palmier. Il salua les hommes de faction à l'entrée de la forteresse et pénétra en ville. À plusieurs endroits, il nota que des éventaires étaient installés, proposant légumes et produits fermiers. Au milieu, commissionnaires et femmes au foyer déambulaient, un panier au bras ou sur la tête, faisant leurs achats.

Il était en train de négocier des graines de courge et de pastèque grillées lorsqu'il sentit qu'on lui tirait la manche. Hashim le salua poliment et lui indiqua qu'il avait mené sa mission à bien. D'un signe de la main, Ernaut lui proposa de se servir une poignée de ce qu'il désirait avant de régler le commerçant. Puis il suivit le gamin nonchalamment, en lui demandant des précisions.

« Ils ne sont pas dans un *khan*, mais dans une petite maison, près de la porte de Samarie.

--- Ils sont nombreux ? La demeure est forte ?

--- C'est juste une maison, au-dessus d'un atelier. Ils ont une écurie en bas, avec un petit jardin. L'un d'eux est parti au matin, à l'ouverture des portes.

--- L'homme au gros nez ?

--- Un autre. Il reste deux chevaux à l'attache. »

Ernaut s'accorda un sourire. Il était content de lui et des informations qu'il collectait par ce biais. On ne prêtait que rarement attention à ces gamins des rues.

« Tu veux prendre de quoi manger pour tes amis ? Si vous devez vous relayer... »

Un sourire gourmand barrant son visage, Hashim ne se fit pas prier. Ils retournèrent près des boutiques et il choisit une collection de douceurs à base de semoule au miel ainsi que des figues et des dattes. Une fois ces emplettes effectuées et après avoir mémorisé comment parvenir dans le quartier sous surveillance, Ernaut le laissa filer. Il comptait se rendre de nouveau vers la maison du chanoine, où le gamin pourrait le retrouver pour le tenir informé de ce qui se passait si c'était nécessaire. Ernaut n'avait plus rien à trouver dans le logement même, il le savait désormais, mais il avait l'intention d'en apprendre plus sur le rôle de ces curieux visiteurs nocturnes.

Il chercha Umayyah, qu'il découvrit en train de travailler, au milieu de paniers et de sacs, dans un réduit en façade de sa demeure. Il salua aimablement Ernaut tout en finissant de tresser une natte. Il expliqua que c'était un nouveau produit qu'il comptait ajouter à ses spécialités, mais qu'il n'était pas encore complètement au point. Comprenant qu'Ernaut était là pour discuter, il l'invita à s'assoir et appela sa femme pour qu'elle leur apporte quelques fruits secs et graines salées.

« Tu vas goûter le shalab de ma femme, tout le monde l'adore ! »

Ernaut ajouta à l'en-cas les graines qui lui restaient et s'accroupit. Il entendait des enfants jouer dans l'arrière-cour et un petit chien au poil gris vint quémander des caresses.

« Il est à demi aveugle et a perdu un croc, mais il est encore assez vaillant pour chasser les rats ! » précisa Umayyah en le flattant de façon assez bourrue.

Son épouse salua rapidement et déposa un plateau avec un pichet et deux gobelets avant de retourner à ses affaires. Elle sembla extrêmement jeune à Ernaut. Comme il savait qu'il était toujours délicat de s'adresser aux femmes, il se contenta de la remercier d'un signe de tête, acceptant le verre d'un sourire. Umayyah dégagea un peu autour de lui, engoncé qu'il était dans les fibres coupées, les brins de feuille et les paniers inachevés. Des lots de contenants s'entassaient sur une banquette derrière lui.

Ils portèrent un toast silencieux puis dégustèrent à petites gorgées. Ernaut découvrit une excellente boisson, de lait sucré épaissi d'une farine. Il lui sembla distinguer un arrière-goût épicé, très léger, de cannelle. Il tenta de se répéter le nom, afin de pouvoir s'en offrir à l'occasion. Ils échangèrent un petit moment autour du temps qu'il faisait et de choses sibyllines. Puis Ernaut en arriva à ce qui l'amenait plus précisément.

« Est-ce que tu as déjà vu ou croisé des hommes, dont l'un a un gros nez, autour de la demeure du chanoine ? Ils étaient là hier soir.

--- Je crois voir ceux dont tu parles. L'un d'eux porte l'épée à la hanche. Ils vont et viennent de temps à autre en effet.

--- Tu ne les as jamais vus en même temps que le père Waulsort ou Thibault ?

--- Pas dans mon souvenir. Ils sont souvent venus là et même avec une mule une fois.

--- Pour apporter ou prendre des choses ?

--- Aucune idée. Je les ai vus passer moult fois, mais jamais je n'ai regardé en détail. »

Ernaut grignota quelques graines, faisant craquer la coque qu'il recrachait dans son poing. Umayyah réfléchit en silence avant de préciser.

« C'est surtout l'homme au gros nez, porteur d'une épée qu'on a vu. Cela faisait des années qu'il fréquentait le coin.

--- Te fait-il bonne impression ? Penses-tu qu'il était ami avec les gens de la maison ?

--- Il ne m'a jamais rien acheté, je ne le connais pas. Et vu qu'il était reçu là depuis fort long temps, il devait y être bien accepté. L'imam Constantin aurait châtié quiconque ennuyait son serviteur et jamais on n'accueille son ennemi plus que nécessaire. »

Ernaut se rendit à l'argument de bon sens. Ceux qu'il avait un moment cru être des voleurs ou des profiteurs devaient donc être partie prenante dans le projet de Waulsort. Était-ce des hérétiques ? Des mages ? Cette dernière idée le fit sourire, il n'imaginait guère Aymar, son interlocuteur de la veille, penché sur de vieux grimoires, en train d'user de sortilèges et d'incantations. Il ressemblait plutôt à un valet, voire un sergent d'armes.

« Thibault ne t'a jamais parlé d'eux ? Ou tu ne l'as jamais croisé alors qu'il discutait avec eux ?

--- Non. Ce sont juste des ombres qui vont et qui viennent. Ce ne sont pas des gens d'ici. Dans mon souvenir, ils sont chaque fois en tenue de voyage. »

Ernaut hocha la tête, il n'en apprendrait guère plus auprès du voisin. Il resta encore un moment à échanger sur le commerce local, tout en finissant sa boisson. Puis Ernaut proposa d'acheter un panier à couvercle à l'artisan. Cela lui ferait un petit cadeau pour Libourc tout en lui offrant l'occasion de remercier du temps passé à répondre à ses questions. Il ne marchanda guère, laissant ainsi Umayyah réaliser un bénéfice correct. L'homme parut enchanté de leur rencontre et lui dit au revoir comme à un ami.

Il fit ensuite le tour du quartier, interrogeant quiconque voulait bien lui parler. Il n'obtint rien de plus. Le sergent au gros nez était familier des lieux depuis des années, escorté de divers compagnons au fil du temps. Il n'était guère causant, ne semblait nullement agressif ou dangereux, et trouvait toujours porte ouverte chez le père Waulsort lorsqu'il se présentait. Jamais Thibault n'y avait fait allusion quand il faisait ses emplettes dans le quartier ou échangeait avec ses voisins.

Ernaut en vint à se demander si cet Aymar n'était pas un domestique du chanoine, qu'il envoyait au loin accomplir des missions pour son compte. Son employeur mort, le valet avait eu désir de se rémunérer d'une façon ou l'autre en piochant dans les affaires de son maître. Ernaut l'exclut d'emblée d'une éventuelle liste d'assassins, dans l'hypothèse d'un meurtre, car il aurait eu tout le temps qu'il voulait pour se servir après son forfait. Sa déception de la veille indiquait qu'il n'avait pas eu l'occasion de pénétrer dans la maison depuis le décès de Waulsort.

### Lydda, quartier de Samarie, matin du jeudi 18 décembre 1158

Proche de la porte septentrionale, la zone était le lieu où les selliers, bâtiers et loueurs d'animaux de transport se côtoyaient, lovés contre les souks des voyageurs, où ils pouvaient présenter leurs balles et acquérir les productions locales. L'endroit était assez animé, bruyant et gris de la poussière des pas des hommes et des bêtes. Une belle fontaine s'appuyait sur le plus large des logis pour négociants.

Ernaut fut rapidement rejoint par un gamin qui le mena jusqu'à Hashim. Ses petits espions avaient établi leur camp de base dans un jardin accueillant quelques estaminets où les artisans et les étrangers pouvaient prendre leurs repas. Accroupis près d'un jujubier, les enfants picoraient dans leurs réserves en jouant avec quelques poupées de bois et de chiffon. L'un d'eux essayait poussivement de jouer correctement d'une flûte à l'aspect tordu.

« Alors, quelles nouvelles ?

--- Un homme est allé chercher de quoi manger, avant que les cloches ne sonnent au matin. Depuis, ils ne bougent pas. »

Ernaut demanda à se faire indiquer le bâtiment. Il était conscient que sa silhouette était reconnaissable entre toutes, mais il avait besoin de voir les choses par lui-même. Heureusement, il put s'abriter derrière un muret protégeant un jardin où trônait un bosquet de grenadiers. Le lieu était bien choisi pour épier les alentours, Aymar et ses compagnons étaient installés dans une pièce à l'étage, accessible depuis une terrasse desservie par un escalier extérieur. En dessous, une boutique débordait dans la rue, proposant des sacs et des étoffes, ainsi que de la corde, afin de confectionner les balles de marchandises. Leur logement disposait de plusieurs fenêtres qui leur permettaient de surveiller de tous les côtés, y compris le passage dans l'artère principale.

Leurs chevaux étaient installés dans une écurie juste au bas de leur escalier. Un coup de chance, se dit Ernaut, ce n'était pas un bâtiment enclos, mais un loueur de montures. Il questionna Hashim à ce propos, afin de savoir s'il pouvait obtenir confirmation qu'il ne restait que deux hommes dans la maison.

« Le vieux Marzawi est pas très amiable. Il a toujours peur qu'on effraie ses bêtes, confia Hashim, clairement inquiet à l'idée d'y aller.

--- Je vais aller voir s'il me fait mauvais accueil à moi aussi. Puis je vais aller secouer ce nid de guêpes, s'ils ne sont que deux. Mais avant cela... »

Il laissa aux gamins quelques graines qui lui restaient, leur demandant de demeurer encore un moment, le temps pour lui de revenir. Puis il fila aussi vite qu'il le pouvait en direction du palais. Les cloches sonnaient sexte alors qu'il entrait dans le couloir de l'hôtellerie. Il hésita à aller voir Raoul, puis se dit qu'il valait mieux profiter de son avantage au plus vite. De toute façon, Raoul était de constitution mince et peu impressionnante. Il n'avait d'ailleurs aucune arme dans ses affaires.

Ernaut sortit son épée, la boucla à ses hanches puis coinça sa masse à tête de bronze dans sa ceinture. Il se sentit tout à coup bien plus sûr de lui. Cela lui rappelait quand il devait garder la porte de la Cité. Il s'amusait parfois à se prendre pour le rempart contre lequel les vagues ennemies venaient se briser. Souriant à ces souvenirs, il retourna rapidement dans le quartier de Samarie et retrouva ses petits espions. Leurs yeux se mirent à briller quand ils le découvrirent ainsi affublé de pareils symboles d'autorité. Ernaut s'en sentit d'autant plus fier. Il expliqua à Hashim ce qu'il allait faire, puis s'avança vivement en direction de la maison.

Il savait qu'il était très visible dans la rue, mais il comptait néanmoins sur l'effet de surprise. Ils ne s'attendraient pas à le voir là et il s'estimait en mesure de faire le poids face à deux hommes, fussent-ils armés. Il grimpa les escaliers quatre à quatre, hésita un instant devant la porte. Devait-il tenter de l'ouvrir sans frapper ? Il était sûr de bénéficier de l'initiative si cela était payant, mais déclencherait l'alarme s'il n'arrivait pas à entrer. Dans l'autre cas, s'il toquait, il n'était pas certain qu'ils se montreraient méfiants.

Il se plaqua donc contre le mur, glissa sa lame hors du fourreau et cogna, de plusieurs coups secs. Une voix se fit entendre rapidement, ce n'était pas Aymar. Ernaut attendit un instant puis frappa de nouveau avec insistance. La serrure joua et la porte s'ouvrit. Avant que l'homme n'ait le temps de s'enquérir de qui les dérangeait, Ernaut était sur lui, l'attrapant par le col de son vêtement. Il le souleva de terre, l'entrainant avec lui dans la petite pièce.

L'autre ne fit même pas mine de se débattre, complètement médusé par l'attaque dont il était victime. L'endroit était sombre, Ernaut dut plisser les yeux pour distinguer les banquettes et quelques meubles répartis contre les murs. Sur un des lits, une forme venait de s'asseoir. Il reconnut Aymar, dont la main se tendait vers son épée, appuyée à un coffre.

« Pas de ça, ou je l'embroche comme un poulet ! »

Du pied, Ernaut repoussa la porte, secouant sa victime avec vigueur. Face à lui, Aymar s'était figé, apparemment calme. Sa voix, moins fanfaronne que la veille, lui parut enrouée quand il s'exprima.

« Droin ? Mais que fais-tu ici ?

--- La paix, je ne suis pas venu pour répondre à tes interrogations. C'est pour des réponses que je suis ici ! »

Il balaya la pièce du regard, ne vit que l'épée d'Aymar comme possible problème.

« Fais glisser ton fourreau jusqu'à moi, et débarrasse-toi de toute lame qui pourrait vous donner de mauvaises idées ! »

L'homme qu'il secouait comme un fruitier se mit à balbutier et Ernaut réalisa que, de panique, il était en train de faire sous lui. Il le lâcha brusquement, satisfait de voir que son entrée avait eu l'effet escompté. Ils ne devaient plus guère le prendre pour un sous-fifre de l'évêque. Il ramassa l'arme d'Aymar, la posa vers lui et s'appuya contre le mur, son épée toujours au poing.

« Tu vas me conter tout ce que tu sais de cette demeure, Aymar.

--- Mais qui donc...

... Cela ne te regarde en rien. Réponds à ma question.

--- Je n'en sais que ce qu'on a bien voulu m'en dire !

--- Servais-tu le chanoine ?

--- Pas directement, non. Mon maître m'envoyait lui porter ce qu'il demandait. Je devais porter et rapporter des messages, aussi. Rien de plus. »

Il désigna de la main son compagnon, désormais affalé sur une des banquettes, avec le regard abattu de celui qui s'est déshonoré.

« Ce pauvre Rolant, ou d'autres, ne sont que valets qui m'accompagnent, pour porter les paquets. »

Ernaut dut reconnaître que le garçon avait bien piètre allure s'il était homme de main. Il s'employait pour l'heure à détacher ses chausses humides, par gestes lents, le regard bas.

« Tu portais quoi au chanoine comme affaires ?

--- Des matériaux divers, des pots et des cornues. Je n'y connais rien en ces choses d'érudit. J'avais chaque fois une liste, je me contentais de la suivre.

--- Et pourquoi il ne chargeait pas son valet de ses courses ?

--- Ce n'est pas à moi qu'il faut le demander ! Je ne suis que sergent, donc j'obéis.

--- À ce propos, qui te donne donc tes ordres ? »

Aymar prit alors un air buté et croisa les bras. Ce n'était pas le genre de chose qu'il semblait prêt à dévoiler aisément.

« Faut-il donc que je te bouscule pour que tu obtempères ?

--- Rien de ce que tu pourrais me... *nous* faire ne saurait desceller ma bouche sur ce point.

--- As-tu crainte de ton maître ou agis-tu par fidélité ? »

Aymar haussa les épaules en réponse. Ernaut regarda Rolant, hésitant à le bousculer. Il avait un peu pitié du valet et saurait bien découvrir qui était derrière eux d'une autre façon. Pour l'heure, il avait besoin d'avoir confirmation de son sentiment et tenta un coup de bluff.

« Pourquoi votre maître a-t-il fait tuer Waulsort ? C'est vous qui vous en êtes chargés ? »

À cette question, Rolant ouvrit des yeux ronds et Aymar se redressa dans son lit, visiblement indigné.

« Nous n'avons rien fait ! Nous ne sommes pas coupe-jarrets tueurs de moines !

--- Vous en avez pourtant toutes les apparences !

--- Nous n'étions même pas là ! Nous venons chaque mois et devions repasser après la Noël. Nous ne sommes revenus plus tôt que parce que nous avons appris... la mort du père Régnier. »

Ernaut nota qu'Aymar marquait du respect dans la façon dont il parlait du chanoine, ce qui plaidait en faveur de son récit.

« Que cherchiez-vous dans sa demeure ?

--- Ses travaux, pardi. Mon maître a payé pour cela et il serait justice que cela lui revienne !

--- Il n'a qu'à se faire connaître à la Cour et il sera entendu » ironisa Ernaut.

Aymar baissa les yeux, morose.

« Es-tu bon chrétien, Aymar ? demanda soudain Ernaut.

--- Hein ? De quoi me parles-tu ? »

Il se redressa sur son lit, se passa une main sur le visage.

« Écoute, compère. J'encrois qu'il y a malentendus à dissiper entre nous. Je ne sais exactement ce que mon maître attendait du chanoine, et je ne sais fichtre rien de ce que ce dernier pouvait faire avec ses fioles et ses creusets. C'est là savoir de clerc, et fort rare, vu que mon maître n'a trouvé personne d'autre qui puisse faire cela. Je me contente de servir, comme toi. Je ne suis pas murdrier ni espie du Soudan.

--- Si tu me disais qui tu sers, cela m'aiderait à te croire.

--- Je ne peux me parjurer ainsi. Je n'ai nulle envie de finir le poing percé, ou bien pire. »

Ernaut rengaina sa lame tranquillement puis croisa ses bras, faisant étalage du sentiment de puissance qu'il aimait à dégager. Il savait que cela n'était certes pas moins impressionnant, mais ses intentions paraissaient moins mortelles. Il vit au regard d'Aymar que celui-ci avait compris.

« Je veux bien t'accorder cela, Aymar. Mais sois acertainé que celui à qui j'ai juré ma foi est homme de grande importance, et dont le bras s'abat avec force sur ceux qui mal agissent. J'ai su vous dénicher, je saurais vous retrouver où que vous alliez. Et si vous m'avez pelé la châtaigne, je saurai vous le faire payer, avec intérêts. »

Aymar acquiesça lentement. Ernaut n'aurait su dire s'il était vraiment intimidé ou s'il donnait simplement le change. Il lui faisait l'impression d'être un homme empli de ressources. De son côté, Rolant fut rapidement classé comme quantité négligeable.

Ernaut se décolla du mur et avança vers la porte. Alors qu'il appuyait sur la clenche, Aymar l'apostropha.

« Nous diras-tu seulement ton nom ? Ou comment te contacter, si jamais...

--- Tu en sais bien assez, cela ne serait de rien d'en apprendre plus. Et c'est moi qui te contacterai si j'ai besoin. »

Puis il sortit sans précipitation. Il avança d'un pas égal en direction de la rue principale, assez content de lui au final. Il avait prévenu Hashim qu'il ne retournerait pas vers eux. Il tenait à ce que ses petits espions demeurent secrets le plus longtemps possible. En outre, il savait désormais dans quel sens orienter les recherches de Raoul : ils devaient trouver l'identité du mystérieux commanditaire. Il ne fit même pas mine de contrôler s'il était suivi, vu qu'il rentra directement au château saint Georges.

### Lydda, palais épiscopal, soirée du jeudi 18 décembre 1158

Les doigts boudinés de l'évêque tournaient les pages avec grande précaution, comme si les feuillets étaient enduits de poison. Avec le temps, sa moue s'accentuait, signe manifeste de la désapprobation qu'il éprouvait à la lecture rapide de certains passages. Il marmonnait d'une voix rauque, en un latin si véloce que ni Raoul ni Ernaut n'y comprenaient rien. Ils se contentaient de siroter discrètement l'excellent vin épicé qu'on leur avait servi. Constantin soupirait longuement en les regardant, d'un air désolé, à chaque fois qu'il s'attaquait à un nouvel ouvrage. Ce qu'il découvrait n'était clairement pas à son goût.

Ernaut admirait le vaste lit, habillé d'épais édredons et de tissus colorés, brillant sous les lueurs ambrées des lampes. Le mobilier aux teintes chamarrées apparaissait plus sombre qu'en journée, accentuant le contraste des décors peints des murs. Ils étaient moins complexes que dans la salle d'audience, mais attestaient du statut de premier plan de l'occupant de la chambre. Une absidiole avait été aménagée en oratoire, orné d'un délicat crucifix avec un Christ de bronze émaillé. Un énorme brasero à roulettes avait été installé près d'eux, améliorant grandement le confort sans pour autant suffire à vraiment réchauffer la grande pièce.

L'évêque parvint au dernier document de la pile et le replia, l'air abattu.

« La plupart de ces écrits me sont inconnus. De nom, j'avais usage de Job d'Édesse[^20] et de son *Livre des trésors*, mais Régnier avait commencé à en donner une traduction, semble-t-il. De la médecine, à ce que j'en vois, avec une approche très... organique. Les médecins mahométans et syriaques en ont bonne connaissance. Tous les autres noms me sont inconnus. Je subodore que ce sont là des ouvrages peu répandus. Je ne connais pas le griffon, malheureusement, pas plus que le syriaque et il semble y avoir beaucoup de références à ces langues dans les notes de Waulsort. »

Il tapa du doigt sur un tome épais, l'air soudain plus inquiet.

« Cet ouvrage me pose vrai souci. Ce serait un écrit d'un certain Apollonios. Il se fait le prophète d'un dieu nommé Hermès, qui lui remit *les secrets de la création et les principales causes de toutes choses*. Il se trouve que le seul Apollonius que je connaisse était un hérétique, défendu par Nicomaque Flavien, un païen fervent adversaire de saint Ambroise. »

Il marqua une pause, fronçant les sourcils et déglutissant avec difficulté, comme s'il répugnait à avaler la salive infectée de ces mots.

« Le propos n'est que salmigondis hérétique sans queue ni tête, formules absconses et recettes simoniaques pour des sortilèges impies, je le crains. »

Il posa une main ferme sur le tas d'ouvrages, agacé.

« Jamais je n'aurais cru qu'il y avait en mon propre collège un nid de mécréance. Ni que je lui avais confié la tâche de décorer le sanctuaire de celui-là même qui avait occis et foulé aux pieds la bête immonde du paganisme. »

Il frappa plusieurs fois du plat de la paume sur l'édifice de savoir maudit posé à ses côtés.

« Il faut remettre tout ceci dans la cellule et l'y enfermer. Je ne sais encore ce que nous en ferons. Il me faut consulter mes pairs et vérifier quoi faire de ces ouvrages impies. »

Ernaut posa son verre, se pencha en avant.

« Le père Waulsort ne pouvait-il pas s'intéresser à tout cela pour le combattre, justement ?

--- Il s'adonnait à ces rites magiques, vous en avez contemplé les reliefs vous-même, mon garçon. Il faisait cela pour un maître qui n'était ni le doyen ni moi...

--- Peut-être aurons-nous bonne surprise lorsque j'aurai dévoilé son nom.

--- Une âme pure ne se camouflerait pas derrière valets de basse besogne. »

Ernaut se demanda un instant s'il était possible qu'un clerc au tempérament plus colérique que l'évêque de Lydda ait pu s'en prendre à Régnier de Waulsort. Si ce dernier professait des pratiques interdites, était tenté d'en illustrer le sanctuaire, c'était envisageable. Ou, alors, encore plus tortueux, c'était un de ses compères hérétiques qui avait utilisé un sortilège basé sur le feu pour les punir d'un manquement à leurs secrets. Waulsort avait-il eu dans l'idée de s'intéresser à cette croyance pour mieux l'éradiquer ?

« Il y a aussi cette mention, fréquente dans les notes du chanoine, à propos d'un secret, le *Secretum secretorum*[^21]. Est-ce là quelque chose de familier pour vous ? demanda Raoul d'une voix timide.

--- Cela ne me dit rien. J'encrois qu'il s'agit là d'un patois désignant leur maléfique liturgie. Que ce soit croyance déviante ou pure affabulation païenne, je ne saurais rien en dire. Peut-être faisait-il allusion à l'un ou l'autre de ses ouvrages en langue syriaque.

--- Vu que son valet a été trouvé mort en même temps, brûlé comme lui, pensez-vous possible qu'il ait participé, peu ou prou, aux pratiques magiques du chanoine ?

--- C'était bien plus qu'un valet. Régnier avait passé un temps comme simple moine à Saint-Saba, dans la vallée du Cédron. Il en avait ramené Thibault. C'était un converti, récemment baptisé, qui aurait aimé se faire clerc. Mais il était bien trop vieux pour entamer les études. Néanmoins, il était lettré, j'en suis acertainé. C'était peut-être même lui qui lisait les textes de langue syriaque à Régnier. »

Il écarquilla des yeux horrifiés.

« Peut-être avaient-ils dessein de traduire en latin ces textes impies, de permettre leur propagation parmi les savants et les prêtres de la vraie Foi ! »

De crainte, il en but à grands traits une longue lampée d'hypocras, grimaçant à devoir avaler une trop large rasade. Ernaut émit une autre hypothèse.

« Il est aussi possible que ce soit un de ses compagnons, jaloux de ses avancées, qui ait mis un terme à cela.

--- En ce cas, pourquoi avoir laissé toutes les affaires en place ?

--- Nous n'en savons rien, au vrai. Peut-être manque-t-il les plus importants de leurs évangiles ? Seul un de leurs initiés pourrait nous éclairer. Nous en connaissons peut-être un, indirectement. Une fois que nous saurons qui il est, ce sera plus aisé.

--- Peut-être devrais-je envoyer mes sergents s'emparer des hommes que vous avez dénichés. On saura bien leur faire avouer qui est leur maître, proposa l'évêque.

--- Vu leurs habitudes de secret, je n'en suis pas certain. Et nous serions coupés de tout lien avec celui qui se cache derrière tout cela. Mieux vaut les garder à l'oeil, sans se faire voir, tant que nous n'avons pas au moins une vague idée de qui ils servent. »

Constantin acquiesça sans un mot, ressassant les mauvaises nouvelles qu'il venait d'apprendre. Il ne sourit qu'à demi en voyant son singe approcher doucement de lui, depuis le lit où il s'était caché jusqu'alors. L'animal grimpa prestement sur ses genoux et s'y blottit, lançant des regards inquiets vers les deux visiteurs. Trempant un doigt dans son verre, l'évêque lui fit goûter le vin qu'il lapa d'une petite langue.

Impressionné par le prélat, Raoul reprit la parole d'une voix si faible qu'il dût répéter pour être entendu de Constantin.

« Comme je vous en ai demandé autorisation, j'ai abandonné à Gilbert, un de vos clercs, le tri des documents du secrétaire de l'archevêque. Il n'a pour l'instant trouvé que peu de choses en rapport avec ce qui nous occupe, de ce qu'il m'a confié, mais nous avons encore espoir d'y trouver quelque piste à suivre. Si cette mort n'est pas un simple accident, il y a peut-être raison à en trouver dans les notes du père Gonteux.

--- Certes, certes. C'était là jeune homme pétri de talents variés et l'esprit acéré. Mais, de ce que j'en sais, il ne connaissait pas le syriaque.

--- Il savait le griffon, m'a confié son valet. Cela lui faisait un avantage sur nous. »

Réconforté par le ton amical et direct de l'évêque, Raoul enchaîna.

« Outre, j'ai découvert parmi les affaires du chanoine de nombreuses lettres reçues d'un abbé dont il semble qu'il est de prime importance, le père Wibald de Stavelot.

--- En effet, voilà grand clerc, conseiller des rois germaniques et proche de la curie. Qu'il soit ami du père Waulsort était de bonne fame. Il est malheureusement décédé voilà peu, durant l'été dernier, en mission diplomatique pour le roi des Alemans auprès des Griffons.

--- Votre vicomte m'en a un peu parlé, confirma Ernaut. Savez-vous de quoi il est mort ?

--- Pas précisément, non. Il n'était pas tant âgé et suffisamment alerte pour beaucoup voyager. Ce fut une surprise pour chacun d'apprendre son décès.

--- Et si la même main invisible avait frappé les deux fois ? Si le chanoine s'efforçait à abattre une déviance, ne se peut-il pas qu'un vieil ami lui ait servi de confident ? Peut-être même qu'il était à l'origine des recherches du clerc et donc il aurait été assassiné en premier.

--- Vous allez bien vite en besogne, mon garçon. Si une tâche avait été confiée à Waulsort par l'abbé Wibald, j'en aurais été le premier averti. »

Ernaut retint de justesse un sourire. La remarque de l'évêque était pertinente, sauf dans le cas où sa réputation n'était pas sans tache. Il en eut le vertige rien qu'à imaginer la portée d'une telle hypothèse. Un nid d'hérétiques au cœur du royaume de Jérusalem, c'était là une crise que nul ne souhaitait voir advenir. Les barons étaient déjà bien assez belliqueux entre eux pour ne pas ajouter au chaos par la guerre des croyances au sein de l'Église.

« J'ai reçu pour instruction de retourner à Jérusalem dès après ce dimanche, sire évêque. Nous avons encore deux jours pour avancer en ce marais. Le temps d'y voir un peu clair.

--- J'ai perdu beaucoup de temps à trier les documents du chanoine et j'en ai presque fini, ajouta Raoul. Je vais pouvoir lire de façon plus cohérente ses notes et, j'espère, en mieux comprendre la logique.

--- Vous en avez fait déjà beaucoup, et plus que je n'aurais espéré étant donné les circonstances. Revenez me voir la veille de partir, que je vous donne message à porter. »

Il tendit sa main en guise de congé. Ernaut s'inclina et baisa la bague de l'ecclésiastique, tout en faisant un clin d'œil au petit singe inquiet. Il récupéra les documents, qu'il fit glisser dans un gros sac de toile tandis que Raoul saluait à son tour. Au moment où ils soulevaient la portière de tissu pour accéder à la sortie, la voix de l'évêque les figea un instant.

« Nous errons en des terres fort périlleuses, mes enfants. Ce combat que saint Georges a livré et remporté, il nous faut chacun le refaire. Souventes fois. »

Chapitre 4
----------

### Lydda, hôtellerie du palais de l'archevêque, fin de matinée du vendredi 19 décembre 1158

Ernaut avait passé la matinée à fouiller dans les corbeilles pour comprendre comment pouvaient s'organiser les différents éléments ramassés chez Waulsort. Il regrettait que le doyen ait tout fait démonter pour le porter ici. L'installation encore en place leur aurait certainement été plus facile à appréhender. Il avait tenté au début de faire part de ses suggestions à Raoul, mais celui-ci préférait pouvoir lire en silence. Il avait confié à Ernaut qu'il pensait tenir une piste. Cela demeurait toutefois nébuleux et il voulait confirmer son idée, asseoir cela sur plus de preuves.

Ernaut avait exhumé un ensemble de petits tubes métalliques qui permettaient de réaliser des assemblages. Ils étaient quasiment neufs et n'avaient pas encore servi. Cela lui rappelait le chalumeau avec lequel les orfèvres soudaient de leur souffle les bijoux, quoique le diamètre en fut trop gros pour cet usage. En outre, leur nombre était trop important pour cela, sans compter les coudes. Le chanoine avait largement de quoi construire un réseau de ces conduites. Peut-être pour mettre en place plusieurs feux sous ses fioles. L'inventivité des savants émerveillait toujours Ernaut. Il aurait aimé pouvoir faire de même, mais sans avoir à supporter les longues années d'étude et les interminables leçons. Au final, c'était de toute façon pour jouer avec de petits tubes de bronze, comme un enfant, nota-t-il cyniquement.

Il mit à bas son fragile assemblage lorsque Raoul sauta soudain sur ses pieds. Ernaut l'interrogea d'un coup de menton. Le jeune scribe, d'habitude si réservé, semblait tout à coup surexcité. Il stoppa de la main la question qui se formait sur les lèvres de son compagnon.

« Il me faut obtenir une information, je crois tenir quelque chose. Je reviens ! »

Et il bondit littéralement sur la porte puis s'engouffra au-dehors. Seul dans la pièce, Ernaut entendit le bruit de ses pas mourir au bout du couloir. Il soupira et reprit son échafaudage. Il était tenté de remonter le laboratoire, voire de refaire les expériences que Raoul pourrait reconstituer. Il suffirait de le faire en un lieu ouvert, sans rien d'inflammable. Et de se tenir au loin.

Lorsque Raoul revint, il se précipita sur les livres, qu'il parcourait désormais rapidement, sans plus aucun respect ainsi qu'il le faisait jusqu'alors. Il tournait les pages avec fébrilité, un sourire gourmand sur les lèvres, glissait des plumes entre les feuillets en guise de marque, notait des extraits sur une de ses tablettes, ricanant pour lui-même. Ce petit jeu aiguisait au plus haut point la curiosité d'Ernaut, autant que cela l'énervait d'être pareillement mis à l'écart. Il aperçut le spectre de son frère Lambert, qui avait passé son enfance à lui répéter d'être plus assidu aux leçons du clerc qui leur enseignait lecture, écriture et calcul. C'était un de ces fatidiques instants où on reconnait le bien-fondé des conseils qu'on s'est entêtés à ne jamais suivre par pur esprit de contradiction. Il haussa les épaules et se remit à l'ouvrage, tirant la langue pour augmenter sa concentration.

Il patienta un long moment et l'estomac d'Ernaut, bien en phase avec la cloche, sonnait l'approche du repas de la mi-journée lorsque Raoul s'annonça enfin prêt à s'expliquer. Le jeune homme était visiblement enthousiaste des perspectives.

« J'ai eu grand souci à comprendre ce qu'il faisait, car tout a été mis en pagaille. Alors plutôt que de tenter d'en reconstruire la chronologie, j'ai voulu suivre une idée. »

Il afficha un grand sourire satisfait de lui-même.

« D'évidence, ce qu'il me fallait pister, c'était *le feu*. Et il se trouve qu'*ignis* et ses déclinaisons sont très fréquents dans les notes et les textes du chanoine. J'ai donc regardé en quel contexte. Ne connaissant pas le mot en griffon, je suis allé demander à un clerc, qui m'a confié que cela s'écrivait ΠΥΡ[^22]. J'ai ensuite pu regarder de quoi il retournait dans d'autres manuscrits.

--- Et qu'as-tu donc trouvé ?

--- Il est maintenant acertainé que le chanoine s'intéressait au feu. Je ne sais si c'était un feu magique, mais il en parle partout et ne fait référence qu'à des extraits qui s'intéressent au sujet.

--- C'est assez logique qu'il soit venu au sanctuaire de saint Georges, alors.

--- Je suis très sérieux, Ernaut. Il semblait focalisé sur la façon dont on peut se faire maître du feu, dont on peut le contrôler. »

Ernaut siffla entre ses dents.

« Cela voudrait dire qu'il aurait fait une erreur dans ses préparations ou dans ses opérations et que le feu aurait échappé à son contrôle. Ce serait tragique accident d'un esprit trop impatient !

--- Il me faudra aller parcourir les écrits du père Gonteux, il était sûrement plus à l'aise que moi pour comprendre ce salmigondis. Mais de ce que j'en comprends, le feu peut se comporter selon lui tel l'eau : on peut le canaliser, le guider et le tenir en réserve.

--- Il suffirait d'ôter la bonde pour le déchaîner à volonté ? Ce serait là pouvoir bien dévastateur !

--- Tu imagines le baron qui aurait un tel pouvoir entre ses mains ?

--- Waulsort écrivait longues lettres à son ami, proche du roi des Alemans. Tu imagines si ce dernier avait une arme pareille ? »

Raoul réfléchit un petit moment, reprit plusieurs des volumes, fronçant les sourcils.

« Que t'arrive-t-il, Raoul ?

--- Je sais que les Griffons ont dans leur arsenal un feu qui brûle sur l'eau et qu'ils projettent au loin ou lancent dans des grenades. Du moins, c'est ce qu'on raconte, je ne l'ai jamais vu.

--- Waulsort avait de nombreux ouvrages en griffon. Peut-être voulait-il percer leur secret ? »

Raoul hocha la tête lentement.

« Les Alemans ont fort souffert de la grande campagne de leur roi quand j'étais enfant[^23]. Ce serait là belle revanche pour eux de pouvoir afficher en leur ost la même arme que le roi des Griffons !

--- Mais comment ce feu peut-il se garder ? Il doit embraser tout contenant. Quelle magie protège celui qui en fait usage ? »

--- Si je le savais, Waulsort l'aurait su et ne serait pas mort en ses travaux. Il doit y avoir sortilèges ou artifice d'engingneur pour contenir son pouvoir de destruction.

--- C'était possiblement là que résidait la faiblesse du chanoine. Il avait trouvé comment invoquer ce feu magique, mais a manqué de savoir d'engins pour le conserver à l'abri ! »

Ernaut se leva, soudain enfiévré par leur découverte.

« Il doit bien se trouver savants engingneurs en la basilique, avec tous ces palans, poulies et grues. Avoir en charge la définition des images à tracer lui permettait de les côtoyer sans que personne ne se doute de rien.

--- Peut-être avait-il même quelque complice en ces recherches, là-bas ?

--- Ou quelqu'un qui aura refusé de l'aider et se sent désormais bien piteux. Nous ne pourrons nous contenter de demander directement. Je vais voir si le manouvrier qui m'a guidé l'autre jour pourrait m'être utile en cela.

--- N'oublie pas que nous ne sommes pas censés parler de cela. Il ne faut pas inquiéter les œuvriers.

--- Je serai discret, rassure-toi. »

Il afficha un sourire plein d'allant et invita Raoul à se lever lui aussi.

« Allons donc prendre repas, je m'emploierai à cela dès après. Continue donc à labourer ce sillon, il semble être fort prometteur ! »

Alors qu'il verrouillait soigneusement la porte avant d'attacher la clef à son braïel, il souffla à Raoul.

« En tout cas, si ce chanoine travaillait secrètement à doter l'armée du roi des Alemans d'un feu magique, il faut absolument que nous le fassions savoir à la Haute Cour. C'est là bien terrible avantage qu'il faudrait solliciter pour les campagnes à venir.

--- Leur nouveau souverain s'appelle Frédéric. On le dit grand batailleur et fort occupé à étendre son royaume au détriment des communes génoises, pisanes, mais, surtout, du pape. À Jérusalem, la chancellerie a bourdonné toute l'année des rumeurs de leur opposition.

--- Tu veux dire qu'il s'en serait servi contre le pape lui-même ?

--- Aucune idée, mais si l'abbé de Stavelot n'avait rien dit à l'évêque Constantin, c'était peut-être à dessein. »

Ernaut avait l'impression d'être au bord d'un gouffre. Il se sentait près d'un gigantesque piège, d'un filet où il tomberait au moindre faux pas. Il se rassura en se remémorant son serment de fidélité au roi. Après tout, il avait juré sur les évangiles et n'avait donc pas de cas de conscience : tout ce qu'il découvrirait serait rapporté au sénéchal, qui en ferait son usage.

« Tu m'étonnes que les deux sbires que j'ai secoués n'ont pas été impressionnés. S'ils travaillent bien pour le roi des Alemans, ce n'est pas moi qui pourrais leur faire peur.

--- Tu as bien fait de ne pas leur dire que tu servais le roi Baudoin, car ils auraient pu s'en inquiéter et appeler à l'aide d'autres commensaux pour te réduire au silence.

--- Ce n'auraient pas été les premiers et j'ai toujours su me sortir de pareilles mésaventures.

--- Ce ne sont pas là enfançons mal dégrossis armés de bâtons, Ernaut[^24]. Si tu t'opposes au roi des Alemans, tu finiras percé par lame !

--- J'ai bien plus grande crainte de ce feu magique, que même un lettré ne sait maintenir confiné. Le fer de mes ennemis, je sais m'en garder. Et le payer de retour ! »

Raoul ne put retenir un sourire moqueur. Il commençait à peine à s'habituer aux fanfaronnades d'Ernaut, mais il savait déjà s'en amuser. Il secoua la tête, goguenard, obtenant une bourrade amicale sur l'épaule de la part du géant.

« En attendant, allons donc prendre des forces pour culbuter tous ces malfaisants. Que cette histoire ne soit que méchant enchaînement d'accidents ne nous exonère pas d'en élucider tous les moments. Pour le roi ! »

### Lydda, chantier de la basilique Saint-Georges, après-midi du vendredi 19 décembre 1158

La panse pleine d'un ragoût de poisson au vinaigre, Ernaut s'installa nonchalamment sur un tas de poutres entreposées non loin du chantier. Il avait depuis cet endroit une vue parfaite sur l'ensemble de la cour principale. De là, il pouvait y observer les voyageurs, négociants et colporteurs, les clercs à destination du palais de l'évêque et les porteurs d'eau courbés sous leurs outres. De timides percées du soleil avaient fini par avoir raison de la chape grise uniforme et seuls de hauts nuages répandaient leur ombre au gré des fantaisies du vent de la côte.

Étudiant de loin les ateliers de bois, il se demandait si l'un d'eux avait pu servir aux expérimentations du chanoine. Il se trouvait peut-être des hommes soudoyés par le roi des Allemands parmi eux, assistant le prêtre dans ses travaux. Les forgerons semblaient tout désignés. Hommes du feu, on les prétendait généralement un peu magiciens. Ernaut ne croyait que peu à la réalité de leurs pouvoirs, bien qu'il ait toujours été admiratif de leur talent à plier la matière à leur volonté. Il y avait aussi une loge de potier, avec un imposant four accolé. Mais les pièces mises à sécher sur les clayonnages ne semblaient guère utiles à des travaux alchimiques. C'étaient surtout des vases de transport, de stockage, ainsi que des éléments pour le bâtiment, tuiles et carreaux essentiellement.

L'atelier des verriers était vide, occupée par les artisans voisins pour y entreposer leurs affaires. Ils ne venaient qu'occasionnellement, préférant généralement leurs boutiques en ville, comme à Tyr où ils étaient nombreux et réputés. Ernaut regrettait cette absence, car il aurait bien vu de tels artisans seconder efficacement Waulsort. Ils employaient toutes sortes de poudres, minéraux et métaux broyés dans leurs assemblages pour obtenir du verre coloré. Il lui faudrait se renseigner si le chanoine n'avait pas été proche des verriers habituellement présents.

Il aperçut Gerbaut, qui traînait encore sa longue carcasse, un seau accroché à son bras valide. Il gardait l'autre en écharpe, mais ne ménageait pas ses allers et retours afin de compenser son incapacité. Malgré la température plutôt fraîche, il n'était vêtu que d'une chemise et de ses braies, protégées par une pièce d'étoffe faisant office de tablier. Voyant Ernaut, il le salua ostensiblement, espérant peut-être une nouvelle tâche peu difficile et aussi bien payée que précédemment.

Ernaut se leva et le rejoignit alors qu'il attendait pour accéder au bac où il puisait l'eau. Différents baquets se déversaient les uns dans les autres, chacun étant réservé à un usage précis afin de ne pas polluer ou salir le précieux fluide. Installé sur un petit escabeau, un vieil homme à l'imposante barbe surveillait que chacun se servait là où il en avait le droit, apostrophant avec vigueur les contrevenants et indiquant de sa baguette le bon bassin aux nouveaux venus. Gerbaut salua poliment Ernaut sans quitter sa place dans la file, demandant s'il avait de nouveau besoin d'un guide sur le chantier.

« C'est fort possible. Le service du roi a toujours grand besoin d'œuvriers habiles et compétents. Surtout d'engingneurs sachant assembler complexes engins.

--- De levage ?

--- Il y a de l'ouvrage pour tous. Entre les forteresses à bâtir, les places ennemies à abattre, les engins de siège à assembler...

--- Et l'eau à acheminer, éructa le vieil homme, soucieux de défendre l'importance du liquide qu'il surveillait tel un dragon son trésor.

--- De certes, sans eau nul casal ne peut prospérer, nulle cité ne peut vivre ! » opina Ernaut.

Gerbaut posa son seau, se frotta le menton. Il espérait qu'en se montrant empressé à satisfaire les demandes du sergent du roi, il en récolterait quelques avantages.

« Le maître engingneur ici est maître Clément d'Acre, indiqua Gerbaut, tout en montrant le bâtiment où le responsable des travaux officiait habituellement.

--- Son nom est déjà fameux par le royaume. J'aurais plutôt besoin d'apprendre les noms de ceux qui l'entourent, qui assemblent échafauds, fabriquent ponts, coffrages et treuils.

--- Je peux vous mener à chacun d'eux si vous en avez le désir... »

Ernaut perçut bien que le service ne serait pas gratuit. L'homme était là pour gagner sa vie et le temps qu'il consacrerait à faire le guide ne lui rapporterait rien. Qu'il soit sur le chantier à se fatiguer à porter des charges alors qu'il avait un bras cassé attestait bien de la situation difficile dans laquelle il se trouvait. Ernaut acquiesça, acceptant de verser la même chose que la fois précédente. Le vieux gardien s'en esclaffa de plaisir pour le manutentionnaire. Peut-être que lui aussi aurait apprécié d'améliorer son ordinaire. Malgré son rôle subalterne, Ernaut leur apparaissait comme un homme important ici. Il appartenait à l'hôtel du toi, était vêtu correctement et semblait gérer son temps à sa guise. Toutes choses qui éveillaient des lueurs de jalousie dans le petit monde des manouvriers, journaliers chichement payés pour de durs labeurs.

Gerbaut lui fit faire un grand tour du chantier, interrompant la plupart des responsables dans leurs travaux afin de leur poser quelques questions sur leur ouvrage. Le portefaix présentait Ernaut comme un représentant de la maison de Baudoin III, de passage pour noter des noms en vue d'une possible embauche pour les projets royaux. Là encore, des lueurs s'allumaient dans les regards quand on évoquait une telle possibilité. Sous Clément d'Acre, que chacun ici appelait simplement le Maître, s'activaient deux grands corps, chacun sous le contrôle d'un homme. Le bâti proprement dit était sous la supervision de Barsam de Galilée, un maçon aux mains rongées par la chaux. Il dirigeait l'édification des murs et organisait entre autres le travail des tailleurs de pierre, mortelliers et plâtriers.

« Ce doit être grande fierté d'accomplir si belle œuvre ! lui lança Ernaut.

--- Certes, c'est là plus intéressant chantier que de bâtir échoppes. »

Pas aussi jeune qu'on aurait pu le croire à sa silhouette et sa démarche alerte, Barsam avait le dos bombé et le visage brûlé des hommes exposés aux pénibles travaux en extérieur. Il avait gagné cette place à force de labeur et sa bouche aux lèvres dures s'employait souvent à lancer sèchement un ordre ou une réprimande. Il fit à Ernaut l'impression d'un maître prompt à brandir la baguette, respecté par la crainte de son bras tout autant que par son savoir.

« Ce ne doit pas être souventes fois en effet qu'on peut ainsi orner des murs. Vous avez donc aussi en charge les ymagiers ?

--- Tailleurs et peintres, oui. Y compris les mosaïstes. »

Ernaut eut la tentation de demander après les hommes en charge de ces décors, qui auraient pu lui en apprendre plus sur l'accident d'Herbelot, mais il craignait que cela ne trahisse sa véritable mission.

« J'espère que la mort du chanoine ne retardera pas les travaux...

--- Il y a assez d'ouvrage pour nous occuper le temps que le collège se décide pour la suite. Il faut du temps pour mettre sur les murs les idées de ces hommes de lettres. »

Barsam haussa les épaules, clairement indifférent au destin de Waulsort. Ernaut voulut néanmoins en avoir le cœur net.

« Vous ne travailliez donc pas tant que ça avec lui ? Je pensais qu'à travers lui l'évêque suivait de près ce qui s'édifiait ici.

--- Je le voyais. Mais il échangeait plus souvent directement avec les ymagiers pour les motifs. Moi je suis là pour que le travail avance au bon rythme. Que ce soit une feuille d'acanthe ou une volute géométrique qui orne telle ou telle tête de colonne m'indiffère. Ce que je veux, c'est qu'elle soit prête lorsque vient le moment de la positionner.

--- Les ymagiers échangent directement avec lui, alors, sur ces sujets ?

--- De certes, il est souventes fois très pointilleux. Le seul à échapper à ses diatribes est le maître mosaïste, suffisamment réputé pour qu'on n'ose lui faire réprimandes. Celui-là, il m'est même interdit de lui faire remarques sur ses travaux, qui avancent à son propre rythme, selon ses désirs. On ne mène pas à la baguette les grands œuvriers comme lui, m'a-t-on dit. »

Afin de ne pas trop se dévoiler, Ernaut ajouta quelques autres questions autour des chantiers que le maître maçon avait réalisés, et son éventuelle expérience militaire. Puis il prit congé, abandonnant la petite loge nichée à l'entrée du futur bâtiment.

« Barsam est un homme très sûr. Il ne se vante guère, mais en son absence les choses n'avanceraient pas si vite. Celui qui besogne avec cœur aura toujours de l'embauche avec lui. »

Gerbaut cherchait peut-être à se concilier les bonnes grâces de ce maître exigeant, en le présentant ainsi sous un jour favorable. Ernaut trouvait que le portefaix faisait parfois montre d'une vivacité d'esprit et d'une élocution qui cadrait mal avec son rôle d'homme de peine. Ses talents semblaient clairement sous-employés. Ou alors il était là sous une fausse raison, et servait Ernaut pour mieux contrôler ce qu'il voyait du chantier.

Ils grimpèrent ensuite les échafaudages jusqu'à une plateforme à mi-hauteur de l'édifice. Le plancher en grinçait insidieusement et les fines cloisons en treillis vaguement couvert de torchis ne devaient guère protéger du vent lorsque celui-ci soufflait. Ils y retrouvèrent le maître charpentier en charge de tous les éléments de poutraison et, en particulier, des assemblages aidant à la construction. Il y avait peu de bois d'œuvre proprement dit dans la structure et on démontait autant qu'on remontait pour en épargner le gaspillage. Maître Domenico était en pleine discussion avec un vieil homme richement habillé. Il avait réussi à grimper là malgré un pilon en guise de jambe droite et se déplaçait avec la nonchalance d'un habitué à de tels endroits.

Ernaut fut rapidement présenté au négociant, car c'était de cela qu'il s'agissait. Willelmo Marzonus faisait commerce de cordages, de courroies et filins. Les chantiers constituaient pour lui un marché colossal, aussi important que l'approvisionnement des navires. Quand il serra la main du jeune sergent, son affabilité se renforça à l'énoncé de son appartenance à l'hôtel royal. Il parlait avec l'accent italien qu'Ernaut avait découvert lors de sa traversée jusqu'à Gibelet[^25]. Bien qu'il n'entretînt aucune prévention contre les Génois, Ernaut se sentit mal à l'aise à l'idée d'interroger le charpentier sous l'attention d'un tiers. Il avait déjà le plus grand mal à ne pas trop en dire à ses interlocuteurs et à noyer le poisson suffisamment afin que Gerbaut ne puisse comprendre ce qu'il cherchait vraiment.

Il s'efforça donc de rester sur des questions relativement vagues, sans jamais aucune référence au chanoine. Il se concentrait sur les mécanismes utilisés ici et là pour le chantier. C'est lorsqu'il évoqua la façon dont une immense roue entraînée par une ribambelle de petits ânes gris puisait l'eau pour la verser dans des conduites que la conversation prit une tournure intéressante pour lui.

« C'était là une des marottes du chanoine en charge des décors. Il avait assailli de questions un voyageur qui se disait très versé dans l'usage des pompes.

--- Celui qui est mort ? Quel rapport avec ses attributions ? fit mine de demander Ernaut avec nonchalance.

--- Oui, le père Waulsort. Aucune idée. Ces hommes de science ont parfois de drôles d'idées. Lui était toujours à l'affût de tous les derniers raffinements techniques. Il avait peut-être été novice chez les pères blancs[^26] ! se moqua-t-il en lâchant un rire gras.

--- Il voulait installer des pompes à la place du moulin ?

--- Aucune idée. Il avait évoqué la question des citernes de Rama. »

Voyant l'incompréhension sur le visage d'Ernaut, Gerbaut se mit en devoir de lui expliquer qu'il s'agissait là d'un fort ancien complexe de vastes citernes souterraines. Pouvoir en extraire l'eau en surface de façon mécanique aurait grandement facilité l'irrigation et même la gestion hydraulique en général.

« Mais Rama n'est pas sur les terres de l'évêque, si ? s'étonna Ernaut.

--- Voilà bonne raison qui explique que le frère n'y a jamais utilisé le savoir acquis auprès de ce voyageur ! se gaussa maître Domenico. Le soudan d'Égypte sera notre frère en Christ avant que de voir un des clercs d'ici aider à la seigneurie voisine. »

Comprenant que le clerc avait éventuellement menti sur ses motivations, Ernaut nota qu'il lui faudrait demander à Raoul d'étudier les mentions à ces appareillages complexes. Les petits tubes en bronze avec lesquels il avait joué avaient peut-être servi à des expériences à échelle réduite. Pouvoir acheminer l'eau à travers des conduites paraissait un peu compliqué comme concept à Ernaut qui ne voyait pas comment elle pouvait remonter une pente. Une autre hypothèse était que ces dispositifs auraient permis de contrôler le fameux feu magique. Il se trouvait là d'étranges savoirs que seuls les plus érudits pouvaient appréhender. Et, parfois, ce n'étaient qu'élucubrations sans aucune réalité pratique.

### Lydda, hôtellerie du palais épiscopal, soirée du vendredi 19 décembre 1158

Raoul et Ernaut étaient tranquillement installés sur leurs couches, profitant du calme après le repas. Des chants provenaient du jardin voisin, certainement des pèlerins qui tenaient à préparer dignement Noël. L'évêque avait fait savoir aux personnes dans le réfectoire que les travaux seraient ajournés pour les célébrations à partir du dimanche qui venait et jusqu'à la fête des Innocents. Les dernières tâches visaient à protéger le sanctuaire pour le temps de cet arrêt et le clergé se chargerait ensuite d'apporter les éléments liturgiques appropriés pour magnifier l'endroit. L'annonce avait été reçue sans surprise, même si de nombreux ouvriers parmi les plus modestes auraient préféré pouvoir continuer à gagner leur pain.

Ernaut et Raoul estimaient avoir fait tout ce qu'il leur était possible. Ils avaient demandé audience auprès de l'évêque, qui les convoquerait certainement dans la soirée. Ils échangeaient donc leurs arguments avant d'en faire le récit détaillé.

« Ce que tu évoques à propos des pompes confirme nos suppositions. Certains croquis auxquels je n'avais guère prêté attention pourraient servir à acheminer un feu liquide, confirma Raoul.

--- Je trouve tout de même que c'est là bien sotte façon de procéder. Le premier benêt venu sait que l'on ne peut manipuler le feu aisément, que les matières brûlent à son contact. Un enfant sait cela !

--- Tu oublies deux choses, à mon avis. D'un, il s'agit d'un homme fort savant, et comme on le sait, certains croient que ce savoir leur permet de s'affranchir de certains principes connus des imbéciles. Mon maître à l'école disait que l'orgueil est la première des vanités du clerc. Outre cela, nous ne parlons pas céans du feu commun, qui sert à griller les rôts ou à chauffer notre soupe aux fèves de ce soir. C'est un feu...

--- Magique ? se gaussa Ernaut.

--- Oui, de fait. Il a certains traits qui le mettent à part et son usage, comme sa création, demandent un savoir tiré d'arcanes secrets. Il en est de même de ce feu grégeois dont je t'ai parlé. Bien peu l'ont vu et encore moins sans en subir l'effet. Je ne sais pas comment les Griffons procèdent, mais il est certain qu'ils doivent obliger à long apprentissage, sous le contrôle de maîtres savants. »

Ernaut se rangeait à l'avis de Raoul. Si les Byzantins tenaient le secret de leur arme si bien caché, il fallait en retrouver les attributs par tâtonnement, expérimentations et observations, sources renouvelées de risques voire de catastrophes. Le chanoine avait pêché par excès de confiance en ses capacités à comprendre un phénomène complexe.

« Si nous admettons que le père Waulsort s'est tué avec son valet en cherchant un secret caché, que pouvons-nous dire de la fin d'Herbelot ?

--- Tout d'abord, j'encroie qu'il se faisait les mêmes réflexions que nous dans ses carnets. Il était plus versé que moi dans la langue des Griffons et en connaissait visiblement d'autres. Il parlait par énigmes et faisait de nombreuses ellipses, mais cela pourrait tout à fait indiquer qu'il en était rendu au même point que nous. Après, pour sa mort... »

Ernaut se rassit sur son lit, les coudes sur les genoux, cherchant du sens dans tout cela, tandis que Raoul persévérait dans ses pensées à voix haute.

« Aurait-il eu désir de reprendre les travaux du père Waulsort ? De ce que tu m'en as dit, et de ce que j'ai pu voir de ses travaux, il était plus porté sur la littérature que sur les sciences militaires...

--- Ça, de sûr, il n'était pas homme à s'intéresser aux façons de prendre forteresse ou de réduire une place ennemie. Il avait grandi hors du siècle, dans un monastère. La violence lui faisait horreur. Et, de toute façon, les circonstances de sa mort n'ont rien à voir. Il n'était pas en train de tenter quelque hasardeuse expérience. Il déambulait dans le chœur.

--- Peut-être quelqu'un souhaitait-il s'emparer de ces travaux ? Le père Gonteux éliminé, il pouvait faire main basse dessus et faire siennes les théories de Waulsort.

--- En ce cas, c'est complètement raté ! »

Ernaut repensait aux hommes qu'il avait surpris dans leur repaire. Ils ne lui avaient pas fait l'effet d'être de sanguinaires coupe-jarrets, mais ils cachaient peut-être leur jeu. Jamais ce genre de personne n'acceptait l'affrontement face à face. La dérobade, les faux-semblants, la manipulation étaient leurs armes favorites. Il était envisageable qu'ils aient choisi de ne pas s'aliéner Ernaut de façon définitive, ne sachant pas exactement comment il s'insérait dans l'histoire.

« Les seuls que je vois à présent intéressés par les travaux du père Waulsort sont la clique d'Aymar, peut-être ont-ils voulu forcer la main à ce pauvre Herbelot...

--- Tu m'as confié qu'ils n'ont jamais fait allusion à lui. Et ils n'auraient pas pris la peine de fouiller la maison si tardivement s'ils savaient les documents étudiés au palais par le père Gonteux.

--- C'est là évidence, je le concède. Et bien que je n'incline guère à leur accorder la moindre créance, ils ne m'ont pas fait si mâle impression. Ils n'agissaient pas comme sournois murdriers. C'est fort dommage que tu n'aies rien trouvé sur leur maître dans les différents papiers.

--- Il y en a tellement ! J'espère toujours trouver une lettre où des noms nous seront révélés.

--- Je n'y crois guère. Les messages envoyés par Waulsort sont bien loin désormais et les réponses qu'il recevait devaient lui être faites oralement par Aymar. Si ce dernier ne veut rien dire, c'est qu'aucune trace ne doit demeurer.

--- Je chercherais à voler le secret du feu grégeois, je serais moi aussi fort prudent.

--- Et tu as intérêt à l'être, désormais. Si une conspiration plus sournoise existe, nous allons bientôt devoir en subir les foudres. Ce ne serait que logique. »

Raoul hocha la tête en silence. La perspective ne le mettait guère en joie. Il savait que le service royal pouvait parfois comporter quelques risques, mais il n'avait jamais envisagé qu'il pourrait tomber sous les coups d'un mystérieux spadassin.

« Je dois t'avouer que je n'y crois guère, pourtant. Si des félons avaient tout manigancé, ils auraient agi entretemps. Il ne semble pas que la mort de ton ami ait changé quoi que ce soit ici, dans les affaires. Et nulle autre catastrophe n'a frappé. »

En pure logique, Ernaut se ralliait à ce point de vue. Il n'arrivait néanmoins pas à s'en contenter. Il estimait insupportable que les choses ne soient que le fruit du destin. Il lui fallait trouver un ennemi, de préférence en chair et en os, à combattre. Il en avait besoin pour lui, pour venger la mémoire d'Herbelot, qui n'en aurait certainement pas tant demandé, mais il le souhaitait également afin de démontrer sa valeur aux yeux du roi. Si le complot était aussi profond, les risques si grands, il tenait une inappréciable occasion de prouver son mérite et de grimper dans la hiérarchie de l'hôtel du souverain. Tous ces sentiments mêlés nourrissaient son désir de ne pas en rester là. Il lui fallait en particulier trouver qui agissait dans l'ombre de Waulsort, de façon à pouvoir le mettre en pleine lumière et le livrer à la justice royale. Voire au connétable, qui saurait faire bon usage des éventuelles découvertes militaires.

### Lydda, chambre de l'évêque, veillée du vendredi 19 décembre 1158

« C'est là bien inquiétantes nouvelles mon fils ! »

L'évêque Constantin s'en était levé de contrariété. Le vicomte, demeuré muet, s'était contenté de passer son mouchoir sur sa nuque, en un geste nerveux.

« Le roi Baudoin est actuellement auprès du basileus romain Manuel, après avoir été uni à une princesse de sa maison. Comment pourrait-il souffrir qu'un modeste clerc de mon collège ait pareillement cherché à leur nuire ? Il va me falloir trouver solides explications. »

Il fixa Ernaut, complètement désemparé, les bras ballants. Le jeune homme n'osait pas bouger, debout face au prélat. Il hésitait à le regarder, surpris de se voir ainsi questionné aussi directement. Il espérait que ce n'était là qu'une interrogation personnelle, qui n'attendait nulle réponse. Par chance, le gros vicomte intervint.

« Mon sire, rien ne dit qu'il ne travaillait pas pour un de ces Griffons. Vous assavez comme moi qu'ils sont plus tors que cep de vigne. Il lui aura bien fallu apprendre tout son savoir de quelqu'un !

--- Tout de même ! Je connais bien assez mes clercs pour savoir que Waulsort était féal enfant de la couronne des Alemans.

--- En quoi cela irait à l'encontre ? Ils partagent un ennemi commun, qui se niche en Sicile, et auraient beau jeu d'enflammer adroitement les navires avec lesquels il sème peur et dévastation ! »

L'idée sembla rasséréner l'ecclésiastique, qui retrouva son siège moelleux après s'être emparé d'un bol de fruits au sirop. Il demeura un moment plongé dans ses pensées. Il avait hérité d'un évêché en délicate posture, en conflit plus ou moins ouvert avec la seigneurie voisine. De plus, celle-ci était désormais dans l'escarcelle du frère du roi, le comte de Jaffa, dont on savait qu'il s'entendait fort bien avec Baudoin. Constantin ne pouvait pas prendre le moindre risque dans ses relations avec la couronne s'il désirait voir ses prétentions d'indépendance acceptées.

D'un autre côté, faire en sorte de livrer ces recherches aux Byzantins serait du plus bel effet auprès de Baudoin. Il semblait bien décidé à faire jeu commun avec l'empereur Manuel et ne ménageait pas ses efforts en ce sens. De nombreuses discussions enflammées à la Haute Cour avaient émaillé les derniers mois à propos de la place prépondérante de l'alliance avec les Byzantins. Beaucoup n'avaient aucune confiance en eux, même si chacun reconnaissait que c'était une formidable puissance militaire et la seule capable d'aider à contenir l'expansion du seigneur musulman d'Alep, le sultan Nūr ad Dīn. En outre, ils avaient une flotte qui pouvait permettre de s'affranchir de la coûteuse assistance des Génois. Pour autant, certains estimaient que c'était là mettre tous ses œufs dans un unique panier et se livrer pieds et poings liés à un pouvoir étranger incontrôlable. Selon eux, la soumission d'Antioche ne serait que la première de nombreuses exigences à venir.

« En tout cas, il me faut avoir solides éléments à présenter si je dois défendre pareille idée. »

Il marqua une pause, s'accorda quelques bouchées, ressassant les éléments d'un air maussade.

« Et le pauvre clerc de mon sire l'archevêque Pierre ? Était-il de la conspiration lui aussi ?

--- Certes pas, s'emporta Ernaut. Jamais Herbelot n'aurait pareillement comploté en si séculières affaires ! Il était homme de foi et de culture. J'en serais garant à jurer sur les Évangiles !

--- Et alors donc ? Comment puis-je expliquer sa mort ? Un des comploteurs aura voulu cacher son secret ?

-- Rien ne permet de le croire. Plus le temps passe et plus j'encroie que ce n'est que malheureux accident. J'ai parlé à vos œuvriers, cela arrive tantôt qu'un lien cède, qu'un poteau lâche ou qu'un panier verse d'une plateforme. Les accidents ne sont pas si rares, j'en ai été témoin. »

Le vicomte abondait en ce sens, hochant sa grosse tête avec entrain. Il ne voyait dans toute cette histoire que risque d'échauffer les esprits. Étant donné qu'il était le pompier qui devait refroidir ces ardeurs, il aimait les explications aisément acceptées par la population, même si ce n'était pas l'entière vérité. Certains savoirs devaient demeurer entre de bonnes mains, pour le plus grand bien de la communauté.

« Mon sire, ajouta-t-il à destination de l'évêque, il faut faire taire les rumeurs au plus vite afin de profiter de l'effet de Noël. Plus tôt nous dirons à chacun qu'il n'y a nulle fourberie, nul démon derrière tout cela, mieux cela sera.

--- Tout de même, que dire à propos du père Waulsort ? Le collège des chanoines saura la vérité, mais je ne me vois guère souiller ainsi son honneur auprès de la populace ! »

Ernaut s'avança d'un pas, une idée audacieuse en tête. Il la remuait en tout sens depuis un moment, conscient qu'on lui demanderait peut-être une version officielle à servir au commun, tout autant que la vérité derrière, réservée aux puissants.

« Sire évêque, j'aurais peut-être idée, un peu folle, mais qui aurait l'avantage de servir le sanctuaire. »

Le prélat l'invita à parler d'un geste, tandis qu'il continuait de déguster ses fruits.

« Parmi les pérégrins, dont j'étais, on parle fort des aventures de saint Georges et de son exploit à tuer le dragon. Dans mes jouvences, on présentait parfois ces créatures comme cracheuses de feu... »

Ses bras s'animaient tandis qu'il parlait, ses traits mobiles se mettaient à vivre ce qu'il évoquait.

« Il faut parler à tous ceux qui passent ici de l'ouverture de la tombe du saint homme, et de la mort qui s'ensuivit pour certains, par un feu magique. Ne précisez rien, quelques anecdotes ici et là, quelques récits droitement instillés. Sans mentir, bien sûr, sans dire où ces malheureux ont brûlé. Avec le temps, on oubliera les détails et ce sera vif rappel des exploits du grand saint Georges et de la déférence qu'il convient de lui témoigner. »

L'évêque en oublia d'avaler sa bouchée, la cuiller emplie de sirop stoppée en plein mouvement tandis que le vicomte souriait d'un air matois. Ils n'avaient jamais cru, et encore moins espéré que tant d'audace et d'ingéniosité puissent exister dans ce massif jeune homme. Qu'il ait échafaudé pareil stratagème était déjà en soi significatif de son habileté, mais qu'il osât la dévoiler aussi benoîtement ne laissait pas de surprendre. Le vicomte sourit largement puis lança un regard à l'évêque pour prendre la température avant de s'exprimer. Constantin articula lentement sa réponse.

« C'est là bien aventureuse proposition ! N'y vois-tu pas les risques de blasphème ? »

Ernaut haussa ses puissantes épaules.

« Les marcheurs de Dieu inventent toutes sortes d'histoire eux-mêmes ! Entre ce qu'ils croient, ce qu'ils inventent, ils aiment à avoir dans leur escarcelle suffisance d'histoires pour les veillées jusqu'à leur mort. Nul ne peut empêcher les rumeurs. Il ne s'agit certes pas de mentir, mais de raconter les faits selon la vision qui portera le moins préjudice à tous. S'ils ont leur content de merveilleux et la place pour y ajouter des détails qui leur siéent... »

L'évêque était ébahi de l'astuce d'Ernaut. Il s'en inquiétait également, peu enclin à voir un homme du peuple se montrer aussi subtil que le plus roublard des ecclésiastiques de la curie romaine. Pourtant, c'était là une proposition habile, qui, aux yeux du public, allait poser un couvercle de plomb sur toute l'affaire. Tout en offrant l'avantage de nouvelles anecdotes propices à attirer les pèlerins dans le lieu. Comme tous les clercs, l'évêque savait que le sanctuaire finirait déserté s'il n'abritait pas son quota de miracles. Une bonne histoire propre à effrayer, voilà ce qui ferait frémir de plaisir masochiste les plus naïfs des fidèles.

« Nous verrons cela en son heure, mon garçon. Je te mercie d'avoir pris tant à cœur les intérêts de ce saint endroit. Veille à collecter les preuves de ce que tu nous as expliqué des... travaux du père Waulsort et fais les moi porter. Tu pourras ensuite vaquer à tes tâches auprès de l'hôtel du roi. »

Ernaut salua respectueusement et se retira, un peu chagrin de n'avoir pas été convié à goûter les fruits au sirop. Il était malgré tout satisfait de l'impression qu'il avait faite. Il savait que c'était audacieux de sa part de montrer autant d'initiative, mais s'il ne prenait pas le risque de leur dévoiler ses talents, jamais ils n'auraient l'idée de lui accorder la place qu'il espérait obtenir.

Lorsque la portière de lourde étoffe retomba derrière Ernaut, l'évêque interrogea du regard Maugier du Toron.

« L'idée est habile, sire. Ses explications semblent frappées au sceau du bon sens, chose dont il a fait preuve tout au long de ses recherches.

--- Est-il fils caché de quelque baron, ou cousin oublié de belle famille ?

--- Je ne saurais vous dire, c'est la première fois que je le vois. Je n'aurais pas oublié pareil Samson !

--- Un esprit si fin en un corps si remarquable. Dieu l'a doté de nombreux bienfaits ! Je ne serais pas surpris que bon sang lui vienne d'un noble ancêtre. Espérons qu'il sache toujours en faire honnête usage et ne succombe pas à l'orgueil ! Je serais fort curieux de savoir d'où il vient...

--- Et ses idées, qu'en pensez-vous ? »

L'évêque Constantin fit la moue. On aurait dit qu'il venait d'avaler un fruit trop amer.

« Il semble avoir bonnes raisons de nous servir cela, et je ne vois rien qui puisse indiquer qu'il n'en a pas été ainsi. Mais j'avoue que le remède est âcre. Quant à sa proposition à servir aux visiteurs, elle frise le blasphème !

--- Vous êtes l'autorité en la matière, sire. Je tiens néanmoins à dire que je la trouve fort pertinente, certainement fondée sur l'observation du peuple. »

Constantin lui adressa une œillade sévère.

« Attention, Maugier, je ne tolérerai pas de mon vicomte qu'il s'exprime comme mécréant. Nous sommes des gardiens, des phares qui doivent indiquer au commun la bonne direction. Nulle place pour la déviance en mon hostel ! »

Le discours semblait s'adresser tout autant à lui-même tant son visage était animé de tensions contradictoires. Il ne pouvait qu'être attiré par l'ingéniosité de la proposition et en était même quasi jaloux. Cependant, son solide sens commun et sa pratique d'une foi scrupuleuse étaient heurtés par la tournure d'esprit lui ayant donné naissance.

« Bon, je verrai demain au chapitre comment les chanoines accueilleront l'idée. D'ici là, j'aurai dormi dessus !

--- Et en ce qui concerne les tribulations du père Waulsort ?

--- S'il n'a pas emporté ses secrets avec lui, espérons que je tienne là beau présent à faire au roi. Cela pourrait faire avancer notre cause pour les territoires disputés avec le sire de Rama. »

Maugier s'accorda un sourire complice. L'évêque savait parfois faire preuve de toute la rouerie qu'on s'attendait à rencontrer chez un habitué des intrigues de palais. Il était servant de Dieu, scrupuleux dans ses préceptes et ses pratiques, mais surnageait de temps à autre l'homme de pouvoir. Il fallait juste l'aiguillonner un peu pour l'aider à sortir de ses sentiers familiers.

### Lydda, magasins du palais épiscopal, matinée du samedi 20 décembre 1158

Ernaut avait suivi un solide valet venu à lui jusqu'à un secteur où il n'avait guère traîné les souliers, coincé entre l'hôtellerie et le réfectoire. Il descendit par une courte rampe dans un soubassement légèrement en sous-sol, ménagé pour y abriter de vastes entrepôts tout en offrant une belle esplanade aux étages supérieurs. De façon irrégulière, de massifs piliers quadrangulaires soutenaient les voûtes d'arêtes empoussiérées. Le lieu rappelait par les odeurs humides la cave paternelle à Vézelay, quoique celle-ci fût de dimensions bien plus modestes. De grandes jarres avaient été installées pour y conserver l'huile, à en juger par les ruissellements graisseux qui accrochaient la lumière autour de l'encolure et sur les trappes de bois qui les coiffaient.

Ils montèrent un escalier qui débouchait dans une salle aux étagères emplies de draps et de couvertures, du sol au plafond. Un chanoine de bonne taille, le visage rond marqué d'une imposante tache de naissance sur la joue, en supervisait le tri effectué par une demi-douzaine de femmes et de filles. Il donnait ses instructions d'une voix étonnamment douce, dans la langue locale, avec une grande familiarité avec elles. On aurait dit un aimable grand-père avec ses petits-enfants, si ce n'était qu'il n'en avait pas l'âge. Il accueillit Ernaut d'un sourire, tout en barrant une ligne sur sa tablette de cire.

« Ah, vous voilà ! Nous sommes en train d'aérer les vieux linges, pour qu'ils ne s'abîment pas. Il ne faudrait pas que nos cadeaux aux valets pour la Noël sentent le moisi ! Je suis Danyel, le drapier. Mais on me nomme père Breton. »

Il serra la main d'Ernaut avec vigueur avant de l'inviter à le suivre. Il marchait rapidement, la tête légèrement courbée, les bras dans le dos, comme s'il cherchait à fracasser du front tout ce qui présentait à lui. Même s'il semblait déborder d'énergie, ses manières étaient affables avec chacun de ceux qui le croisaient. Ils arrivèrent dans un recoin d'une des salles où s'amoncelaient les malles, les paniers et les coffres de pin. Il s'y était installé une table et des tabourets et y traitait apparemment ses affaires courantes. Une étroite fenêtre, barrée pour le moment d'un épais parchemin, y dispensait une lumière ambrée. Il s'assit et, d'un geste, invita Ernaut à faire de même. Il leur versa à chacun un peu de vin, certainement coupé d'eau vu sa couleur claire. D'un œil, il vérifia que personne ne traînait dans les environs avant de prendre la parole de sa voix légère.

« Mon sire Constantin nous a informés de vos... explications sur les évènements qui ont endeuillé les lieux ces dernières semaines. »

Il avala un peu de boisson, comme à la recherche de ses mots.

« J'ai appris que vous étiez compagnon du jeune clerc de l'archevêque. J'en suis désolé pour vous, mais ainsi que vous l'envisagez, un chantier est souventes fois lieu de mortel péril. Ces derniers mois, nous avons eu notre compte de membres brisés, sans même parler des malheureux qui ont glissé d'une passerelle pour rejoindre notre Seigneur.

--- Merci de vos condoléances. Si je suis ici, c'est parce que les circonstances ont paru bien mystérieuses.

--- On ne s'émeut point pareillement jusqu'en l'hôtel du roi quand un charpentier tombe d'un faîtage ou qu'un maçon est emporté par un coffrage mal étançonné. »

Il fit un geste de la main, comme pour se débarrasser de ces funestes pensées.

« Il est néanmoins fort bon de voir que c'est à un proche du malheureux que cette histoire a été confiée. Au moins sa tombe aura été illuminée de quelques ferventes et amicales prières. »

Il se resservit une généreuse portion de vin avant de continuer.

« Si je vous ai invité à me joindre, ce n'est pas pour commenter cet accidentel trépas. C'est celui du père Régnier qui me questionne bien plus avant. Le père évêque nous a conté vos conclusions, mais je n'y peux souscrire. »

Ernaut vit que la main du chanoine s'était mise à trembler tandis que sa voix s'éteignait. Il brûlait de presser le drapier de mille questions, mais avait appris à attendre la fin de tels épanchements pour n'en point briser l'élan.

« De tout ce que faisait Waulsort en sa maisonnette, avec son valet, je n'en disconviens pas. Nul ici ne doutait qu'il œuvrait à quelque secrète et mystérieuse entreprise. Il a toujours été ainsi, désireux de percer des arcanes qui n'appartiennent qu'à Dieu. Le Très-Haut nous dote parfois bien étrangement... »

Il se mit à faire tourner son gobelet, les yeux dans le vague, n'adressant à Ernaut que de timides regards quand il espérait voir poindre un soutien ou une lueur de compréhension.

« Je n'ai nulle idée de ce qu'il tramait avec son compère. Je vous en fais serment. Jamais il n'évoquait cela avec l'un ou l'autre de nous. Par contre, je sais avec assurance qu'il ne cherchait pas à vaincre le mystère du feu grégeois.

--- Et pourquoi ça ? demanda Ernaut, après une longue attente.

--- J'ai constaté ce qu'il demeurait des corps de Régnier et de son valet lorsqu'on les a préparés pour leur dernier voyage. Il n'étaient pas brûlés ainsi que le sont les victimes de cette arme terrible. J'en ai déjà vu, dans mes jeunes années, tandis que j'étais plus souventes fois dans le Nord. Les hommes ont des brûlures qui ne ressortent en rien de l'extraordinaire. On les soigne d'ailleurs de la façon habituelle. »

Ses yeux s'affolèrent tandis qu'il dévoilait la révélation qui l'inquiétait tant.

« Là, on aurait dit que le pauvre Waulsort avait été rongé par le feu, ses chairs non pas carbonisées, mais englouties par une flamme vorace. Ce n'était pas là un incendie habituel. J'incline à croire qu'il a touché là quelque incontrôlable magie.

--- Parlez-vous là de pratiques...

--- Démoniaques ? Je ne saurais dire le vrai, je le confesse volontiers. Mais j'ai bien vu les instruments rapportés de son hostel en ville. Ce n'est pas là habituel fourniment de simple prêtre.

--- On me l'a dépeint comme fort curieux des choses de la nature.

--- On peut être versé en bien des savoirs, selon celle-ci ou à son encontre. On s'en trouve parfois d'autant plus avide de percer des secrets qui ne doivent jamais nous être dévoilés. »

Ernaut n'était pas bien sûr de comprendre. Il savait que les clercs étaient prompts à se jeter l'anathème à la face les uns des autres, et des évêques en désaccord n'avaient aucun scrupule à s'excommunier l'un l'autre. C'étaient là des querelles qui touchaient aux passions usuelles d'hommes de pouvoir, même sacrés, et ne concernaient qu'indirectement le peuple, qui devait se contenter d'en subir, le dos courbé, les conséquences. Malgré tout, cela n'avait rien à voir avec la religion en elle-même. Dieu et tous les saints, surtout, demeuraient les principaux interlocuteurs et destinataires de toutes les pratiques pieuses, voire magiques, lorsque certains démons venaient s'inviter dans la danse.

« Je n'ai aucune certeté de cela, mais en ces territoires d'histoire ancienne, où de nombreuses superstitions côtoient la vraie Foi, d'aucuns peuvent croire qu'on peut franchir des ponts, des passages sans en payer le prix...

--- Si je vous comprends bien, le père Régnier aurait tenté d'exercer quelque magie païenne et en aurait été puni ?

--- Thibault était fort versé dans les langues d'ici, dont il avait une pratique aisée. Surtout des écrits. Ce n'était pas simple sergent tout juste bon à nettoyer et cuisiner. Je ne saurais dire quel était son cursus, vu qu'il n'a jamais appris ni *trivium* ni *quadrivium*, mais il savait ses lettres. Du moins celles de sa nation.

--- D'où était-il ?

--- Je ne sais. Il a compaigné le père Waulsort depuis Saint-Saba, dans la vallée du Cédron. Mais ses origines... »

Il haussa les sourcils en signe d'ignorance, puis se concéda une nouvelle lampée. Ernaut se demanda si cette pépie était habituelle ou si c'était là le signe d'une nervosité inaccoutumée.

« En avez-vous parlé à l'évêque ?

--- Je ne me sens pas assez assuré pour ainsi souiller le souvenir du père Régnier auprès de mes frères...

--- Mais vous avez désir que la vérité se sache ?

--- Toute chose n'est pas forcément bonne à dire et mon sire l'évêque a proposé une idée fort habile pour que les rumeurs se taisent et fassent désormais honneur à saint Georges. Non, si je vous confie cela, sous le sceau du secret, j'en fais la demande, c'est afin de m'assurer que l'âme du pauvre Waulsort connaîtra la paix et le repos. Quels qu'aient été ses erreurs ou ses défauts, il les a abandonnés sur terre et il a droit à la félicité des Cieux. »

Ernaut s'accorda un sourire en découvrant que Constantin avait repris l'idée à son compte. Il en était flatté quoiqu'un peu vexé de ne pouvoir s'en attribuer publiquement le mérite. Il avala une longue gorgée du vin, sucré et gouleyant à souhait. Il comprenait que le frère s'en envoie de si belles rasades.

« Vous attendez quoi de moi, à me révéler cela ?

--- Je ne sais au juste. J'ai juste désir de vous apporter mes lumières sur les points que j'estime obscurs. Il serait dommageable que d'aucuns reprennent les travaux du père Régnier si ce n'est pas le secret du feu byzantin qu'il cherchait à percer, mais toute autre chose, bien plus terrible et impie. Imaginez que le sire évêque en fasse cadeau au roi et que cela libère des forces infernales au sein du royaume. »

La perspective le fit frissonner et l'incita à réguler cette fièvre par une nouvelle lampée. Ernaut espérait que le clerc parlait de façon imagée. Personne ne doutait de l'existence des forces démoniaques, mais elles demeuraient généralement tapies dans les ombres, dans le recoin des âmes des valets à leur service. Se pouvait-il qu'il y ait moyen pour eux de pénétrer, chair et sang, dans le royaume des hommes ? Les prêtres le disaient parfois. Dans un tel cas, c'était peut-être saint Georges lui-même qui avait relâché quelque peu sa poigne de la gorge du dragon par qui les maléfices allaient déferler pour engloutir l'univers.

Soudainement inquiet, Ernaut fronça les sourcils. D'une intrigante affaire de secret militaire où il avait l'occasion de mettre en avant ses capacités, il se voyait plongé dans une ténébreuse histoire où des créatures de l'au-delà intervenaient en personne pour contrecarrer les plans les unes des autres, détruisant des vies humaines au passage. Subitement, les choses lui paraissaient moins attrayantes et il caressa machinalement la petite croix qu'il portait généralement autour du cou. Il fallait demander à Raoul de relire les notes de Waulsort à la lumière de cette hypothèse. Les érudits jaloux de leur savoir étaient coutumiers des écritures codées et symboliques et ils avaient peut-être été menés en bateau depuis le début.

Chapitre 5
----------

### Lydda, quartiers des teinturiers, fin d'après-midi du samedi 20 décembre 1158

Les ruelles bordant le quartier où les artisans s'employaient à colorer les fils et les étoffes n'exhalaient pas une pestilence aussi agressive que celui des tanneurs à Jérusalem, mais une odeur acide, mélange des effluves croisés de tous les produits amassés là. D'autant que nul n'avait planté de menthe le long des murs pour s'en garnir copieusement les narines. Heureusement, l'activité n'était pas très importante et les émanations demeuraient dans les limites du supportable. Ernaut avait juste l'impression que ces gens vivaient dans un égout à ciel ouvert. On s'habituait à tout.

Le père Breton avait proposé à Ernaut de le mener auprès d'Amos, un artisan fort versé dans les traditions ésotériques et les pratiques étranges. Il maîtrisait des savoirs techniques complexes, étant donné son métier de teinturier et avait accès à des connaissances fort peu orthodoxes, par son appartenance à la communauté juive. Le chanoine avait longuement expliqué à Ernaut que malgré ces origines discutables, l'homme était assez plaisant. En outre, il recevait de nombreuses commandes de la part de l'évêché, ce qui le rendrait suffisamment conciliant pour ne pas refuser de prêter assistance.

Lorsqu'ils se présentèrent à l'entrée de son local, il vint rapidement à eux. De petite taille, Amos était brun de peau et de poil, la barbe fournie et le cheveu frisé s'échappant d'un turban à l'aspect négligé. Ses avant-bras maigres se finissaient par des mains tavelées, aux ongles noirs des mélanges qu'il manipulait à longueur de journée. Son attitude revêche à l'égard de l'apprenti qui l'avait prévenu se métamorphosa en un sourire large en voyant la silhouette du père Breton.

« Père Danyel ! Quelle joie de vous voir en mon modeste atelier ! Vous auriez dû faire prévenir, je n'ai rien pour vous accueillir dignement, en ce jour. Nous prenions le frais avec des confrères, vous souhaitez vous joindre à nous ? »

Avisant l'imposant géant à côté du chanoine, armée d'une épée et d'une masse, il marqua une pause, se renfrogna quelque peu avant de reprendre, d'une voix plus mesurée.

« Ou peut-être préférez-vous que nous allions un peu à l'écart ? Nous aurons bien temps de profiter des ma'amouls[^27] d'Esther après avoir traité nos affaires. »

Sur un signe de tête du chanoine, il leur fit traverser un entrepôt d'aspect bancal. Il n'était couvert que de feuilles de palmes, de planches et de tout un fatras entassé sur de trop fragiles étais au goût d'Ernaut. Dans la pénombre, tout ce qui pouvait servir de cuves était empli de liquides mousseux, odorants, bouillonnants et colorés. Des baguettes traînaient partout, ainsi que du bois de chauffe, des supports pour les fils et des présentoirs pour y étendre les toiles. L'ensemble était entreposé en un incroyable capharnaüm. Ernaut craignait qu'un simple éternuement ne fasse s'effondrer tout l'atelier.

En haut d'un petit escalier, ils aboutirent dans une modeste pièce encombrée de pots et de jarres, de sacs poussiéreux et de cordages. Dans un coin, des paillasses devaient servir au personnel. Sans faire plus de manières, Amos les invita à prendre place sur les couches, tandis que lui s'affalait sur un tas de vanneries accumulées contre un des murs.

« Je sais que c'est pour vous jour de repos, Amos, mais nous avons besoin de vos lumières.

--- Discuter entre amis n'est pas travail, en quoi puis-je vous aider ? »

Le père Breton se tourna vers Ernaut, l'invitant à parler. Celui-ci hésitait à trop en dire et ne savait pas jusqu'à quel point l'artisan n'était pas bavard à l'instar d'une pie avec son entourage.

« Je suis à la recherche d'informations sur ce qui peut créer un feu qui... ronge les chairs plus qu'il ne les brûle.

--- Ainsi qu'un vitriol voulez-vous dire ?

--- Peut-être, je n'ai aucune science de ces choses. D'où la proposition du père Breton de vous entretenir de cela. Connaissez-vous ordinaires ou extraordinaires façons d'arriver à pareil résultat ? »

Le petit homme ferma les yeux, se gratta le nez, humant dans l'air tout en réfléchissant. Peut-être connaissait-il déjà la réponse à la question, mais se demandait pourquoi on la lui posait et quels savoirs il pouvait partager. Ernaut avait toujours entendu que les Juifs étaient jaloux de leurs secrets, autant qu'on les disait avares de leurs monnaies.

« Il existe de nombreuses façons d'éliminer les chairs. Les tanneurs en ont meilleur savoir que moi. Que souhaitez-vous nettoyer exactement ?

--- Ce n'est pas pour ôter derniers reliefs d'un quelconque objet, mais pour comprendre ce qui aurait pu ronger tout en offrant l'apparence de brûlures. »

Amos mâchonna ses lèvres, comprenant que les questions étaient loin d'être anodines. Il semblait hésiter entre demander à en savoir plus et redouter les conséquences de ce qu'il apprendrait. Curiosité et prudence s'affrontaient dans son crâne.

« Très bien. Vous devez savoir qu'il existe nombre d'onguents, poudres ou métaux qui n'offrent aucun risque tant qu'ils sont manipulés de la correcte façon. Ils sont rares à être dangereux en soi, mais le deviennent pour peu qu'on les échauffe, qu'on les mouille ou qu'on les mélange avec d'autres. Il y a mille façons de rendre mortelle la plus inoffensive des préparations. Ces régions regorgent de savoirs anciens, méconnus, mais pas oubliés.

--- Vous parlez de pratiques magiques ?

--- Je ne saurais dire, au vrai. Rares sont les initiés qui savent lire et comprendre les textes. Les érudits arabes et persans camouflent la science qu'ils ont extraite des textes des anciens. Ils ne se confient les uns aux autres qu'au sein de secrètes confréries. »

Ernaut se demandait si le frère Waulsort avait pu tromper l'une de ses mystérieuses fraternités. S'il travaillait pour les Allemands, peut-être menait-il double-jeu et avait été éliminé plus ou moins directement par ceux qu'il trahissait pour des intérêts séculiers ou politiques. Ernaut sentait qu'il devait poser la question, même s'il paraissait évident qu'Amos ne saurait répondre positivement. Au moins cela marquerait clairement les choses et offrirait l'occasion de voir la façon dont il réagirait, aussi révélatrice, voire plus, que ses mots.

« Avez-vous connaissance de pareilles fratries dans nos provinces ?

--- Je n'en ai aucune idée, je ne suis que modeste teinturier. Certains dans les grandes villes ont meilleures opportunités de s'intéresser à ces choses. Moi-même je n'en ai qu'une vague idée, car mon maître, à Antioche, était lui-même bien plus versé dans tout cela. Il faut dire qu'il était rabbin, homme de grand savoir. Il correspondait avec des érudits fort lointains. Je tiens mon maigre savoir de ce qu'il m'en a dit. »

Il semblait peu désireux de s'étendre sur la question, s'agita un peu sur son siège improvisé puis reprit.

« Ce que je vous dis n'est que vague souvenir de tout ce que j'avais pu entendre. Comme tous les apprentis, j'avais les oreilles qui traînaient tout le temps et les yeux grands ouverts. Enfin bref... C'est alors que j'ai entendu parler de la poudre de lune, qui peut faire ce que vous évoquez. Elle est fort rare, je n'en ai jamais vu, mais elle est parfois citée, nommée escarboucle par ailleurs.

--- Poudre de lune ? Mais comment l'obtient-on ?

--- Pas la moindre idée de cela ! On la dit fort capricieuse, ainsi que la lune qui agit sur nos humeurs.

--- Quel est donc l'aspect de cette poudre ? En avez-vous déjà vu ?

--- Non. Tout ce que j'en sais, c'est qu'on la nomme ainsi, car, tel l'astre lunaire, elle brille dans la nuit. Je ne connais rien d'autre qui fasse de même.

--- Elle éclaire telle une lampe ? Avec des flammes ?

--- Peut-être. Certaines matières sont si volatiles qu'elles cherchent à s'élever par tous les moyens, y compris la combustion spontanée. C'est peut-être son cas. »

Ernaut n'avait aucun souvenir d'un tel produit dans ce qu'il avait vu des reliefs du laboratoire secret de Waulsort. Il lui faudrait interroger ceux qui s'étaient chargés de faire le déménagement. C'était de toute façon peu probable, si la poudre était si dangereuse qu'un savant comme le chanoine y avait succombé, il y aurait eu une véritable hécatombe parmi les valets et portefaix.

« Savez-vous s'il en existe commerce ?

--- Pas chez les habituels négociants. Les hommes capables de la récolter ou de l'assembler doivent être fort rares. Et aussi peu nombreux ceux qui sauraient quoi en faire. Je n'ai jamais entendu parler d'une éventuelle pratique dans un métier ou l'autre. Seul un érudit aurait idée d'un éventuel usage. »

En un sens, il était plus simple de pister un produit si rare, vu que ceux qui y auraient été confrontés en garderaient certainement un souvenir. Sauf si on pouvait le faire passer pour autre chose. Le teinturier marqua une pause, se réfugiant dans ses pensées. Il semblait prendre plaisir à cet échange qui le sortait de ses habitudes. Le visage soudain animé, il se pencha en avant, plissant les yeux tandis qu'il parcourait en silence le contenu de sa mémoire.

« J'ai gardé quelques notes et recettes de mes apprentissages. Cela fait bien longtemps que je n'y ai pas jeté un regard. Il s'y trouve peut-être quelque utile indication, si cela peut vous aider. »

Ernaut approuva avec vigueur.

« Bien sûr, faites ! Je repasserai à l'occasion ou vous pourrez me porter message par l'intermédiaire du père Breton. »

Le visage barré d'un sourire accommodant, Amos rétorqua d'une petite voix.

« Il n'y a aucun espoir d'en apprendre plus sur l'usage que vous réservez à la poudre ?

--- Il n'est pas de mes attributions d'en dire plus sans autorisation. »

Ernaut espérait qu'il n'aurait pas à nommer le maître dont il dépendait. Il n'était pas certain que l'évêque et le sénéchal apprécieraient qu'on mêle ainsi la personne du roi à cette affaire. Il s'abstint donc, le visage fermé, mais sans forcer sur l'expression de son autorité. Le teinturier pouvait encore lui être utile et se montrait de bonne volonté. Il n'était pas nécessaire d'exercer des pressions sur lui.

« Savez-vous s'il s'en trouve dans la cité ? Ou si l'un quelconque ici aurait capacité à en vendre ?

--- Non, je n'ai même jamais entendu quiconque en parler ici. De ce que j'en sais, à part éclairer la nuit et brûler horriblement en dévorant les choses, elle ne sert de rien. Sans compter que, vu sa rareté, elle doit coûter très cher, si jamais il s'en vend. »

Il marqua une pause puis afficha un sourire qu'il espérait sans doute engageant.

« Quoiqu'il arrive, je vous tiens au courant si je trouve quoi que ce soit qui puisse vous intéresser, j'en fais serment.

--- Il y a autre chose, ajouta Ernaut. Tout ceci doit demeurer discret. Mon maître n'aurait guère de plaisir à entendre des rumeurs autour de poudre de lune rôder aux alentours d'ici. »

Amos hocha la tête avec sérieux, impressionné par l'autorité avec lequel la remarque avait été faite. Il n'était visiblement pas habitué à un tel traitement. Tant mieux, estima Ernaut, il n'en serait que plus enclin à se conformer aux instructions.

Ils déclinèrent ensuite la proposition de rejoindre les quelques personnes conviées par Amos à partager le repas du soir. Le drapier pensait vêpres non loin, et il ne voulait pas arriver en retard. D'autant qu'il y avait très probablement une réunion du chapitre juste après, pendant laquelle il ferait son rapport au doyen, voire à l'évêque. Ernaut avait une autre idée en tête. Il espérait se faire confirmer la filière par laquelle le père Régnier se fournissait en poudre de lune. Peut-être était-ce une voie qu'il explorait pour fabriquer une nouvelle forme de feu de guerre, différent de celui utilisé actuellement, ou dans le but de l'améliorer.

Déterminer cela permettrait à Ernaut de clore la question de façon définitive et cohérente, afin de présenter un compte-rendu final le lendemain à l'évêque Constantin. Après tout, ce n'était pas à lui de juger si les pratiques étaient magiques, voire hérétiques ou politiquement discutables. Le principal était de fournir une explication qui soit admise par tous. Il avait accepté l'accident d'Herbelot, point si étonnant après tout comme l'avait remarqué le père Breton. Il ne restait plus qu'à comprendre celui de Waulsort, sans laisser la moindre parcelle de doute.

### Lydda, quartier de la porte de Samarie, soirée du samedi 20 décembre 1158

Les ombres bleutées commençaient à s'imposer aux dernières lueurs ambrées du soleil lorsqu'Ernaut parvint en vue du repaire d'Aymar. Les boutiquiers avaient pour la plupart rentré leurs marchandises à l'abri et certains avaient même déjà verrouillé leur volet. Quelques portefaix s'activaient à décharger des chameaux de bât arrivés en fin de journée, transportant de lourdes balles jusque dans un entrepôt à proximité de la muraille. La porte principale était fermée et il ne subsistait qu'un modeste guichet par lequel entrer. Les hommes préposés à la garde n'avaient pas tardé à clore pour la nuit, sans même attendre que les cloches sonnent.

Ernaut monta quatre à quatre les marches pour aller frapper avec vigueur à l'huis. Il vérifia sans y penser que son épée coulissait librement dans le fourreau, puis assujettit sa main sur le bronze de sa masse. La voix d'Aymar résonna de derrière le bois peu après. Au moins avaient-ils appris à se méfier.

« C'est Droin ! » tonna Ernaut de l'intonation la plus dure qu'il pouvait se donner.

Il lui fallut attendre un petit moment que la porte se débloque. À l'intérieur se trouvaient toujours Rolant et Aymar. Mais cette fois ils se tenaient en retrait, derrière les meubles, leurs armes proches. Au moins n'avaient-ils pas organisé une vraie embuscade. Rolant ne paraissait d'ailleurs guère certain de ce qu'il devait faire, lançant régulièrement des œillades inquiètes en direction de son compère. Ernaut ouvrit largement ses deux mains en signe de bonne volonté. La situation l'amusait.

« La paix, compères, je ne suis pas ici pour chercher noise.

--- Ouais, bah, estime-toi heureux qu'on accepte encore de te parler, après ton dernier coup.

--- Je n'aurais pas à me conduire ainsi si vous étiez moins cachottiers.

--- Parce que toi tu joues franc-jeu, peut-être ? »

Ernaut haussa les épaules et referma derrière lui. D'autorité, il s'assit sur un tabouret proche de la porte. Il espérait ainsi détendre un peu l'atmosphère. Il n'arrivait pas à considérer les hommes face à lui comme des dangers. Il les avait facilement tenus en respect jusque là et voulait en apprendre plus sur eux et, surtout, sur leur commanditaire.

« J'apense que nous devrions nous dévoiler un peu. Il est clair que vous avez la besace vide, au repartir d'ici. Votre mestre ne saurait apprécier de découvrir qu'il a tant semé pour ne rien récolter. Je peux vous donner quelque grain à lui faire moudre, mais il faudra m'en dire plus en échange.

--- Ce n'est pas moi qui force les portes. Je t'ai graissé le poignet pour accéder à la maison du Père Régnier ! C'est toi qui renardes.

--- Je vais essayer de faire quelques pas dans la bonne direction, alors. Tu m'as dit que tu apportais diverses choses au père Waulsort. Il s'y trouvait peut-être ce qui l'a tué.

--- Comment ça ?

--- As-tu souvenance de lui avoir porté de la poudre de lune ? »

Aymar se frotta le visage, l'air soudain moins buté. Il prenait clairement le temps de la réflexion, cherchant dans sa mémoire et vérifiant peut-être s'il pouvait révéler une telle information.

« Tu disais lui porter selon ses besoins, en a-t-il fait mention ?

--- Je n'ai aucune remembrance d'un tel nom. Ça ressemble à quoi ?

--- On ne me l'a pas dit, c'est produit fort rare et peu connu, qui éclaire la nuit. Peut-être l'auras-tu connu sous un autre nom, l'escarboucle... Ou auras-tu confondu une chose avec l'autre ?

--- Il n'a rien reçu qu'il n'ait demandé de prime, fort clairement. Le père Régnier contrôlait tout, il était homme d'ordre, tout comme le Thibault, d'ailleurs.

--- J'entends bien, mais il a peut-être surestimé ses capacités à utiliser de correcte façon les produits et mixtures de son art. La poudre de lune ne se laisse pas aisément dominer et peut infliger les brûlures qu'il a reçues avec son valet. »

Aymar déglutit lentement. Il cherchait à sonder les intentions de son interlocuteur, se demandant pourquoi on lui faisait autant de révélations.

« Nous n'avons jamais livré que de bien habituelles marchandies, rien de semblable à ce dont tu me parles. Il devait se fournir par d'autres moyens pour ses plus rares nécessités.

--- Rien d'inhabituel selon toi ?

--- Le moins commun fut peut-être l'huile de roche. Sinon ce n'étaient que cornues et flasques, poudres de métaux, de pierres ou d'os. Des choses plus aisées à trouver dans une grande cité qu'ici, sans pour autant être si rares que ce dont tu nous parles. »

Ernaut nota de demander à Raoul de chercher la mention de cette huile. On se servait parfois de tels liquides pour les lampes, peut-être Waulsort espérait-il confectionner ce fameux feu de guerre.

« Le chanoine n'a jamais expliqué ce qu'il faisait ?

--- Non. Il lui arrivait de me donner message, mais je n'en connais pas le contenu. Je me contentais de le porter.

--- Depuis tout ce temps, tu n'as jamais été curieux ?

--- Si tu crois que je n'avais que lui à penser ! J'ai bien d'autres tâches à accomplir ! C'était là mission bien tranquille, porter paquets et ballots à un savant clerc, dont il me remerciait par des listes d'autres choses à lui apporter, plus parfois un pli pour mon maître. Son valet nous payait un godet de vin pas mauvais, on échangeait sur le temps et les dernières nouvelles des environs, puis on repartait, messe dite. »

Ernaut était de plus en plus persuadé que ce n'étaient en effet que d'honnêtes valets d'une maison. Ils n'avaient certainement pas accordé plus d'égards à ce travail qu'aux nombreux autres dont ils s'acquittaient. Lui-même avait une infinité de missions dans ses attributions de sergent du roi et souvent il ne s'y intéressait guère au-delà des nécessités du service. Peut-être avait-il déjà transmis un message d'importance sans le savoir, la banalité des ordres reçus et sa nonchalance à les exécuter assurant le secret contenu dans le document bien plus efficacement que tous les artifices compliqués de certains espions.

Il lui fallait trouver une autre façon d'arriver à ses fins. Si Aymar n'était qu'un domestique, au moins pouvait-il porter une requête, à défaut de prendre la moindre décision qui dépasse le cadre strict de ses instructions.

« Vu que tu ne peux me dévoiler le nom de ton suzerain, je te propose de lui donner de ma part quelques renseignements, accompagnés d'une demande pour lui. Acceptes-tu ?

--- Faire le messager, je peux toujours. À voir ce qu'il m'en coûterait.

--- Rien du tout. Au point où tu en es, tu ne peux rien rapporter du travail du chanoine. Mais je peux te dire qu'il est fort probablement mort suite aux manipulations d'un fort rare, coûteux et dangereux matériau à la recherche d'un feu de guerre, fort savant ou magique, c'est selon. Ton mestre, qui a payé fort cher pour obtenir ce secret, sera fort marri de l'apprendre, de voir ses espoirs ainsi déçus. Mais il sera peut-être heureux de m'interroger plus avant sur ce qui s'est passé.

--- Que veux-tu dire ?

--- Que, s'il consent à me voir face à face, je serais heureux de boucher quelques-uns des trous du récit que tu lui conteras. »

Face à lui, Aymar gardait un visage impassible, les yeux froncés. Il croisa les bras, inclina la tête.

« Tu veux me faire passer pour un imbécile en plus d'un bon à rien, c'est ça ?

--- Je t'offre une solution à ton problème. Comme tu l'as dit, nous sommes tous deux sergents, ayant chacun à servir quelque autorité impérieuse. Sans moi tu aurais juste trouvé porte close et pas la moindre chose à rapporter, en dehors de la mort de Waulsort. En proposant à ton seigneur de m'entretenir, tu lui offres la chance d'en savoir plus, aux fins pour lui de faire ensuite ses affaires.

--- Et toi tu sauras qui payait les recherches du père Régnier, comme ça.

--- Il me faut bien aussi dénicher de quoi contenter celui à qui j'ai prêté serment ! »

L'offre sembla agréer fort Rolant, qui regardait Aymar avec espoir. Celui-ci paraissait plus circonspect, cherchant à voir la chausse-trappe que recelait la proposition. Il lui en fallait visiblement plus. Ernaut décida de tenter le tout pour le tout. Il se dévoilait plus qu'il n'aurait souhaité, mais il sentait qu'il n'obtiendrait rien sans prendre de risques. Sans compter qu'il aimait ça.

« Dis à ton maître que je suis sergent le roi.

--- Si tu penses que cela l'enfrissonera...

--- Je n'ai pas dit cela, mais au moins pourras-tu lui permettre d'envisager à qui il s'affronte... »

Il se releva lentement, un air satisfait sur le visage, puis ouvrit la porte.

« Pas besoin de te dire où me chercher, je pense. Et puis de toute façon, moi aussi je sais où te trouver. Donne des nouvelles sans trop tarder. »

Pas mécontent de sa dernière répartie, assez théâtrale à son goût, il sortit de la pièce et descendit l'escalier d'un air léger. Il veilla à ne pas jeter le moindre coup d'œil vers la cache de ses petits espions tandis qu'il partait en direction du palais. Il était assez satisfait de lui, avec assez d'éléments pour faire un rapport détaillé à l'évêque et suffisamment de pistes pour aller plus loin si l'occasion s'en présentait. Il n'était déjà pas peu fier que son idée fût reprise à son compte par si important prélat, il espérait bien impressionner aussi l'hôtel du roi par la qualité de son travail d'enquête.

### Lydda, palais épiscopal, veillée du samedi 20 décembre 1158

Ernaut avait à peine eu le temps de s'entretenir avec Raoul des dernières avancées qu'un valet venait les quérir pour aller rendre compte à l'évêque. Ernaut grogna de voir ainsi son repas s'éloigner, d'autant qu'il risquait de se contenter de manger froid si la discussion s'éternisait. Tout en suivant le domestique, il prit néanmoins son mal en patience, repassant dans sa tête les différents éléments du récit qu'il pouvait faire.

Lorsqu'ils pénétrèrent dans sa chambre, l'évêque était à table, occupé à déguster une petite volaille dont les senteurs sucrées vinrent danser jusqu'aux narines des deux sergents. Il finissait d'en sucer un pilon, tout en les détaillant de la tête aux pieds, sans mot dire. Puis il s'essuya longuement dans une vaste serviette étalée sur sa bedaine.

« Alors, avant votre départ, avez-vous quelques noveltés à me découvrir en nos affaires ? »

Ernaut acquiesça et s'éclaircit la voix, un sourire sur les lèvres.

« Quelques éléments sont venus préciser certains des points que j'ai évoqués hier, sire évêque. Vous aurez plaisir, je le crois, à les ouïr. Comme vous l'assavez déjà, il y a tout lieu de penser que le sire Waulsort s'est infligé male mort par ses propres recherches. Mais il est aussi fort possible que cela lui soit arrivé alors qu'il travaillait à son propre feu de guerre, sans rien à voir avec celui des Griffons.

--- Que me contes-tu là, garçon ? N'était-il pas à la recherche de leur secret ?

--- Il semble plutôt qu'il espérait trouver sa propre façon d'avoir une telle arme, peut-être aux fins de servir la couronne, je ne saurais dire. Il était fourni par quelque secrète personne en différents matériaux et matériels. Mais en tout cas, il suivait sa propre piste en ce qui concerne ce feu. Ce qui l'a peut-être perdu, ayant eu plus gros yeux que gros ventre, si vous me pardonnez l'expression. »

L'évêque maugréa quoique son visage se fut détendu à l'idée que son chanoine ne serait pas à l'origine d'un scandale diplomatique avec la couronne. Il arracha une aile qu'il commença à grignoter tout en parlant.

« Se pourrait-il que le mystérieux commanditaire fût au service le roi ?

--- Je n'en sais encore rien, sire Constantin. J'ai semé quelques graines, dans l'espoir que je pourrai moissonner le nom. Outre, si c'était le cas, il ne serait d'aucune utilité pour moi de l'apprendre, mon sire le roi ou le sénéchal en sachant déjà tout à loisir ce qu'il en est de ces affaires. »

L'évêque Constantin plissa les paupières, cachant sa moue dans les mastications. Il était moins enthousiaste à l'idée que son clergé put avoir partie liée avec des puissances autres que la sienne, surtout laïques. Il ne voyait certes pas d'un mauvais œil qu'on entretînt les meilleures relations avec la couronne, surtout en ces moments de lutte contre le seigneur de Rama, mais de là à le faire sans l'en informer, il trouvait cela pour le moins cavalier. Il avala une bouchée puis s'accorda une belle rasade de vin.

Il lança un long regard condescendant aux deux garçons face à lui. De jeunes hommes habitués à servir, estima-t-il. Malgré sa stature, le plus impressionnant des deux acceptait son rôle de subalterne et tenait sa place. Quoique ses vertus semblassent indiquer qu'il pourrait se voir promu à de plus hautes sphères, s'il continuait dans cette voie.

« Il vous faudra garder le silence sur tout ce que vous avez appris ici. Je ne sais encore quelles seront les conséquences de tout cela, les travaux de Waulsort, sa mort et celle du pauvre père Gonteux, mais il est acertainé qu'il ne serait d'aucune utilité qu'on jase fort avant de cela. »

Ernaut et Raoul opinèrent en silence tandis que le prélat s'emparait d'un autre morceau de viande qu'il trempa dans la sauce avant de le déguster. Puis, s'essuyant les lèvres, il reprit son discours, tout en pointant du doigt quelques documents scellés au bout de sa table.

« J'ai préparé quelques feuillets pour le sénéchal, vous les lui remettrez de ma part lorsque vous lui conterez ce que vous venez de me dire. J'aurais aussi, outre cela, un petit rollet à vous faire remettre au sire patriarche Amaury de Nesle. Son palais touchant à celui du roi, vous n'aurez guère de chemin à faire en plus. »

Ernaut reçut les documents avec respect et les glissa dans la petite boîte dédiée qu'il fixa derechef à sa ceinture. L'évêque finit son plat et s'essuya consciencieusement les mains dans sa serviette.

« Vous mercierez grandement le sire patriarche d'avoir pris le temps de s'intéresser à notre affaire, malgré son aspect inhabituel. Portez -lui mes vœux de santé la meilleure possible. Puisse le Très-Haut le préserver de ses ennemis.

Puis il leur tendit sa bague à baiser. Ils le saluèrent avec respect et obtinrent congé de lui.

Ernaut avait l'estomac dans les talons, heureux que l'entretien n'ait pas trop duré. Ils allèrent directement au réfectoire pour prendre leur repas, mais ils y furent accueillis par un valet qui leur expliqua qu'ils avaient l'heur de souper ce soir en une autre salle. Il les guida jusqu'à une belle pièce, aux murs décorés de faux appareil, où les places étaient moins nombreuses. Plusieurs braseros dispensaient une chaleur appréciable et une grande quantité de lampes scintillait sur les tables. Il ne s'y trouvaient qu'une demi-douzaine de personnes, occupées à converser en une langue inconnue d'Ernaut. Des Allemands lui précisa Raoul. Ils étaient en tout cas vêtus de fort coûteuses étoffes, hommes comme femmes, même si leur mise était modeste. Tous portaient une croix cousue sur la poitrine.

Après avoir salué d'un signe de tête, Ernaut et Raoul s'installèrent autour d'un plateau, là où aurait été le bas bout si cela avait été un banquet officiel. Mais il ne se trouvait personne d'autre avec eux. Ils avaient des gobelets de verre, et un pichet en céramique vernissée, ornée de décors colorés, était placé à leurs côtés avec les tranchoirs et une salière. Impressionnés, ils trinquèrent en silence. Aucun d'eux n'avait eu l'occasion jusqu'alors d'être traité parmi les hôtes de marque. Malgré son absence de faconde, l'évêque devait être satisfait d'eux, nota Raoul à mi-voix.

« Peut-être s'assure-t-il aussi que nous ne parlons à personne de tout ceci avant de partir » plaisanta à demi Ernaut.

Quelle que soit la motivation du seigneur des lieux, Ernaut s'en félicita lorsqu'il vit arriver à eux les épais tranchoirs et l'écuelle de viande marinée. Sans être le délicat volatile que Constantin avait englouti devant eux, c'était de l'excellente volaille, soigneusement apprêtée, avec la même sauce. À partir de là, ils se contentèrent de manger, comprenant pourquoi l'évêque avait tenu à avaler tout cela tandis que c'était encore chaud. Le rehaut de cannelle et les oignons fondus ravissaient le palais. Ils avaient un peu l'impression de fêter la Noël quelques jours en avance.

Alors qu'ils terminaient de se pourlécher après avoir dégusté chacun leur tranchoir jusqu'à la moindre miette, Ernaut prit un temps pour envisager son compagnon. Il se sentait heureux de cette mission en sa compagnie, même si elle avait si mal commencé. Son frère Lambert serait certainement estomaqué de savoir qu'Ernaut avait échangé avec un évêque et en avait reçu pareil remerciement.

Ernaut ne comprenait pas le manque d'ambition dont sa famille faisait preuve, se contentant pour la plupart de demeurer tels qu'ils étaient, honnêtes cultivateurs et négociants. Son père était un prospère vigneron à Vézelay, mais il n'avait jamais cherché à s'affranchir de sa condition. Il acceptait les prélèvements sans rechigner et tolérait les vexations que les puissants seigneurs ecclésiastiques locaux infligeaient aux habitants.

En un sens, Ernaut estimait ce chanoine, mort à force de poursuivre un rêve. Il était érudit et intelligent, disaient ses pairs, mais lui souhaitait s'avancer plus avant en son savoir et ses talents. Il ne se contentait pas de ressasser et de profiter. Il était comme attiré vers un ailleurs dont il sentait qu'il lui faudrait le bâtir. Dans le royaume de Jérusalem, Ernaut avait l'impression de rencontrer plus de personnes qui avaient ces semblables envies de dépassement, qui avaient quitté sans regret leurs proches et leur terroir, trop vieux, trop étriqués pour leurs aspirations. Ici le possible renaissait. Il aimait à entendre les récits autour du jeune prince d'Antioche, cadet de famille arrivé sans avoir et qui avait fini par devenir un des plus importants barons de Terre sainte. Il ne s'en laissait conter par personne, pas même par les autorités religieuses ou son puissant voisin le basileus byzantin. Renaud de Châtillon était son modèle, celui dont il souhaitait suivre les traces.

Il espérait que le succès de cette première mission lui offrirait l'opportunité de briller auprès du sénéchal. C'était lui qui avait la haute main sur les promotions et avancements dans la sergenterie royale. Le vieux Jean d'Acre n'était qu'un officier en charge des finances et de l'administration, mais il était aussi un chemin vers le vicomte et donc vers les chevaliers qui faisaient tant rêver Ernaut. En outre, il espérait qu'il aurait l'occasion de s'entretenir une nouvelle fois avec Amaury de Nesle, désormais patriarche et homme de tout premier plan. Sans savoir en quoi exactement cela pourrait lui servir, il était persuadé que de s'attirer les bonnes grâces d'un prélat si puissant lui serait sans nul doute essentiel.

« Te voilà bien songeur, mon ami, se moqua gentiment Raoul, qui le voyait perdu dans ses pensées.

--- Oï, j'apensais que cette mission finit fort bien après un départ prou hasardeux.

--- Puisse-t-il toujours en être ainsi ! Je n'aurais jamais cru que les leçons du père Thibault me feraient connaître pareille aventure. J'étais persuadé que je venais ici pour simplement entendre et noter les missives qu'on me dicterait. Et me voilà à tenter de déchiffrer quelques grimoires magiques pour y trouver trace d'un secret de guerre griffon ! »

Il adressa à Ernaut un sourire radieux et, après un toast silencieux, se gratifia d'une longue gorgée de ce vin gouleyant à souhait.

« Si d'aventures tu as besoin d'un compère pour tes voyages, Ernaut, pense à moi. Même si je dois me tanner les fesses en selle, j'en suis fort aise. Je n'ai guère désir de finir comme Brun, dont on ne sait, de lui ou de ses archives, lequel est le plus poussiéreux ! »

Ernaut s'esclaffa à la saillie. Brun, pour être un des clercs compétents de la Secrète, était souvent moqué pour son manque d'énergie et de vigueur. La plaisanterie courait parmi les sergents qu'il gèlerait aux Enfers avant que de voir Brun se hâter.

« Demain, nous devrons justement chevaucher de rebours jusqu'à la Cité. Nous ferons halte pour la repue en le casal de la Mahomerie. »

Ce n'était pas seulement pour le repas qu'Ernaut avait désir de marquer un temps dans ces lieux. Il s'y trouvait une jeune personne dont il escomptait qu'elle serait enthousiasmée d'entendre comme il réussissait bien dans les tâches qu'on lui confiait. Il ne désespérait pas de permettre à Libourc de s'établir en une belle demeure à la hauteur de ses rêves les plus fous. Et le plus rapidement serait le mieux, estimait-il. Plus tôt sa valeur serait reconnue dans l'hôtel du roi, plus vite il aurait de quoi s'installer en ménage et épouser Libourc, la demoiselle de ses pensées.

### Lydda, hôtellerie du palais de l'évêque Constantin, fin de matinée du dimanche 21 décembre 1158

Le départ n'avait pas pu se faire aussi tôt qu'Ernaut l'espérait. Avec l'arrêt des travaux, l'organisation des lieux se trouvait bouleversée. De nombreux pèlerins arrivaient désormais pour célébrer la fête sainte en un sanctuaire d'importance et d'autres, parmi lesquels certains des artisans, le quittaient pour justement retrouver leur famille à cette occasion. La cohue dans le réfectoire, dans les communs, était donc conséquente.

Estimant qu'ils feraient une halte à mi-chemin chez Libourc pour bénéficier de la veillée et de l'héberge, Ernaut avait opté pour un départ différé, profitant de leur présence à Lydda pour assister à la messe avant de prendre la route. Il était également allé se recueillir une nouvelle fois sur la tombe d'Herbelot. Sans vraiment se l'expliquer, il ressentait comme une déchirure à l'idée de ne plus jamais croiser cet étonnant petit clerc parfois hautain et bourré de manières insupportables. Il se demandait par ailleurs qui, en dehors de lui, saurait qu'il était enseveli là, au chevet d'une basilique certes prestigieuse, mais loin de ses proches et de ceux qui l'avaient connu de son vivant.

Alors qu'ils laçaient leurs bagages, le jeune Germain vint les saluer. Il s'apprêtait également à retourner chez lui, à Acre. Ernaut constata que lui aussi emporterait le souvenir d'Herbelot. Il leur apprit que Gilbert avait obtenu l'autorisation de conserver les documents afin de les étudier. Ce dernier avait l'intention de poursuivre le projet littéraire de son ami, dont il espérait peut-être faire une chanson. Ernaut sourit à l'idée, peu convaincu qu'Herbelot aurait apprécié de voir son travail transformé en spectacle pour le vulgaire.

Lorsqu'ils se mirent enfin en selle, ils mangèrent un temps la poussière d'un convoi qui partait, comme eux, en direction de l'est. Ils avaient opté pour la route directe qui filait au nord du Toron, pour prendre le chemin le plus septentrional vers Jérusalem, serpentant parmi les reliefs des monts de Judée. Le ciel était dégagé et la température plutôt fraîche, surtout à l'approche des hauteurs où l'influence de la côte s'amoindrissait tandis que l'on s'engouffrait dans le Bab el-Oued. Ils passèrent à quelque distance du Toron, où les chevaliers de la milice du Temple construisaient une impressionnante forteresse. Elle garantirait les alentours tout en accueillant un vaste dépôt où les récoltes et redevances de la plaine pourraient être collectées avant d'être acheminées par convois jusqu'au siège dans la Cité sainte.

Ils profitèrent des abreuvoirs autour de l'église d'Emmaüs pour mettre le pied à terre et laisser leurs montures souffler un peu. Ils n'étaient guère pressés et, tout en puisant l'eau pour leurs chevaux, ils prirent le temps de discuter avec les membres d'une caravane de marchands à destination de Jaffa. De nombreux Génois les avaient rejoints, artisans et ouvriers qui allaient passer les fêtes avec les leurs, sur la côte, dans les quartiers que la couronne avait concédés à la Compania en remerciement des services rendus. Le petit hameau autour de l'église était habitué au va-et-vient et quelques modestes échoppes proposaient leurs savoirs ou leurs produits pour assister les voyageurs : savetiers et maréchaux ferrants, négociants en nourriture et même un cabaretier qui vendait au gobelet un vin clair, léger et bien frais. Ernaut et Raoul le dégustèrent assis sur un muret longeant des jardins entourés d'épineux.

Lorsqu'ils repartirent, ils dépassèrent rapidement un convoi de mules et de quelques chameaux ondulant avec de lourdes barriques sur leur dos. Ils galopèrent le temps de prendre assez d'avance pour ne pas infliger leur poussière aux caravaniers puis tournèrent afin de rejoindre l'étroite vallée qui filait droit vers l'ouest. Au moment où ils se remirent au pas, Raoul indiqua à Ernaut une fortification qui coiffait un relief à leur droite.

« C'était là une forteresse royale, désormais aux mains de la milice du Temple, elle aussi. Peut-être le roi la voudra-t-il un jour retrouver en son domaine.

--- Et pourquoi donc reprendrait-il cette donation ?

--- Pour te la conférer en fief, pardi. On la nomme Chastel Ernaut ! Il n'est que d'y planter ta bannière pour en faire belle baronnie ! »

Ernaut sourit à la plaisanterie. Mais il ne subsistait nulle dérision dans le regard qu'il tourna vers l'éminence. Il n'estimait pas impossible pour lui de finir seigneur de ses terres, portant éperons d'or et siégeant au haut Conseil des barons.

« Quand mes couleurs flotteront en haut de ces courtines, je t'inviterai à venir goûter le vin de mes futailles et nous boirons tous deux en haut de mon donjon !

--- Je saurais te remembrer cette promesse quand tu porteras bannière ! »

Quand ils obliquèrent franchement en direction du levant, les parois se resserrèrent et le chemin se confondait parfois avec le lit de l'oued, alternant zones parsemées de gros blocs et de pierrailles avec des endroits lisses comme du marbre. Ils se félicitaient de n'avoir pas à traverser ce court goulet sous la pluie. Le cours d'eau pouvait se transformer en furieux torrent lors des fortes averses d'hiver qui ne sauraient tarder. Au-dessus d'eux, de rares broussailles, chênes verts et épineux surveillaient le passage.

Au sortir de ce délicat défilé, ils respirèrent plus à leur aise en débouchant dans une vallée moins étouffante, bien aménagée par de belles terrasses où se répartissaient arbres fruitiers, oliviers et vignes. De nombreuses parcelles devaient aussi servir à faire pousser orge et froment. Ils furent tentés un moment de s'arrêter auprès d'une magnifique fontaine appareillée, mais éperonnèrent quand ils découvrirent un nuage de moustiques. Devant eux, quelques gazelles qui s'étaient enhardies jusqu'à venir sur le chemin prirent la fuite en bonds légers. Sur la pente qu'ils commencèrent alors à gravir, les oliviers et les vignes continuaient de dominer.

Peu à peu ils ralentirent le pas alors qu'ils parvenaient aux abords de jardins installés sous des vergers de grenadiers et de figuiers. Ernaut soupira d'aise en voyant se détacher sur le ciel d'un bleu clair le clocher de l'église de la petite Mahomerie. Ils avancèrent tranquillement parmi les manses où les colons latins profitaient de leur jour de repos pour s'amuser dehors à des jeux collectifs ou simplement discuter entre voisins. Ernaut se rendit directement à la maison de Sanson, impatient d'y retrouver Libourc. Il salua en passant quelques têtes connues. Il était toujours heureux qu'ils le voient ainsi, chevauchant l'épée au côté.

Lorsqu'ils arrivèrent enfin, Ernaut sauta joyeusement de sa selle et frappa à la porte avec entrain. L'après-midi était déjà bien entamé et il était possible que Libourc et ses parents soient absents. Ils ne s'attendaient pas à sa venue. Il patienta en vain quelques instants, puis, ayant laissé les chevaux à la garde de Raoul, entreprit d'aller visiter les endroits où il les savait susceptibles de passer le dimanche. Il avait l'habitude de profiter avec eux de ce jour férié.

Il retrouva la famille assemblée avec des amis, en train d'assister à une partie de soule à la crosse où les jeunes gens faisaient montre de leur vaillance et de leur habileté. Ils utilisaient une vaste esplanade empierrée non loin de la maison forte, qui servait aussi lors des moissons pour y séparer le grain de la paille. Libourc, l'apercevant la première, lui fit un signe enthousiaste. Il la rejoignit en quelques enjambées tandis que Sanson et Mahaut découvraient à leur tour son arrivée. Son beau père lui fit bon accueil, comme habituellement, mais Mahaut, toujours très réservée, se contenta d'un hochement de tête silencieux. Ernaut les salua poliment, s'enhardissant à prendre les mains de sa fiancée tout en la dévorant des yeux.

« Que voilà bonne surprise, Ernaut, déclara Sanson. Nous ne t'espérions pas ce jour, tu disais être de service.

--- Si fait, je le suis, répondait Ernaut en montrant la boîte à message qui ornait sa ceinture. Je suis de retour de Lydda et j'apensais pouvoir faire halte avec mon compère pour la nuitée.

--- Nous serons heureux de rompre le pain avec vous deux pour le souper, confirma le vieil homme. Allons donc voir s'il ne se trouve pas quelque pichet pour désoiffer vos gosiers empoussiérés du voyage ! »

Puis, joignant le geste à la parole, il invita le petit groupe à le suivre jusqu'à leur demeure. Raoul y avait dessellé les montures et s'employait à les frictionner avec un chiffon.

« Je dois avoir brosse de crin, lui proposa Sanson tout en déverrouillant sa porte. »

En peu de temps, Libourc et Ernaut y mettant la main, les bêtes furent correctement étrillées et leurs pieds curés. Puis on les mena dans l'enclos où Sanson tenait ses ânes, où ils se virent accorder un peu de foin. Une fois ces tâches achevées, tout le monde se retrouva autour de la table dans la pièce principale à l'étage. Bien qu'il ait été conscient qu'Ernaut ne pouvait pas toujours donner le détail de ses missions, Sanson ne pouvait empêcher sa curiosité naturelle de s'exprimer et il ne fut pas satisfait avant de savoir ce qui amenait Ernaut sur les chemins de Judée.

« Il y a eu quelques morts mystérieuses autour de la basilique, à Lydda. Le sire évêque en avait appelé à l'aide pour s'assurer qu'il ne s'y trouvait pas quelque maline force.

--- Le sire évêque de Lydda ? C'est toi qu'on a envoyé ? s'exclama Libourc, les yeux brillants.

--- Oï, confirma Ernaut. Quoiqu'au départ il devait se trouver un mien ami chevalier avec nous, sire Régnier, que vous connaissez. Il n'a pu nous joindre au dernier moment. L'hostel du roi ne pouvait demeurer sans donner réponse à un évêque.

--- Et ils t'ont envoyé, toi, avec ton compère ? Que voilà beau présage, *Mestre Ernaut* » s'enthousiasma Sanson.

Ernaut savait que cela ne rendrait pas le mariage plus rapide, Sanson se montrant intraitable quant à la situation qu'il voulait voir proposée à sa fille. Mais il était persuadé que le vieil homme était sensible à son succès et qu'il écarterait les autres demandes qui pourraient lui être faites. Mahaut, plus pragmatique, aurait préféré unir Libourc à un honnête laboureur ou un artisan, de préférence un voisin, qui lui aurait ainsi garanti de visiter ses petits-enfants autant qu'elle le souhaitait, et d'avoir de l'aide en ses vieux jours. Pour elle, qu'Ernaut réside dans Jérusalem constituait un problème. Sans même parler de son manque de foi et son comportement qu'elle trouvait trop extraverti. Pourtant, elle faisait relativement bonne figure à son futur gendre, vu qu'il avait agrément de son époux, mais sans excès d'enthousiasme.

Pour être moins fastueux que la veille, le repas du soir fut tout de même de qualité. Les nouveaux arrivants qu'ils étaient, pour la plupart, appréciaient de voir que leurs jardins pouvaient produire quasiment toute l'année et avec une variété de récoltes plus grande que dans leur pays d'origine. Les menus s'en ressentaient, même si chacun demeurait farouchement attaché à ses habitudes alimentaires. Ernaut aimait aussi les haltes dans le manse de Sanson pour le fait qu'il savait y trouver du pain à la mie épaisse, à la croûte craquante, comme il en avait l'usage depuis son enfance.

Après manger, ils restèrent à discuter un petit moment, les hommes s'adonnant à des jeux de plateau tandis que Libourc et sa mère s'activaient à filer les quelques paniers de laine que la tonte de leurs moutons leur avait donnés. Pour Sanson, c'était plus une habitude de se refuser à tout gâchis qu'une nécessité. Ils revendaient le fil obtenu à maigre prix à un négociant, sans chercher à tisser eux-mêmes. Ils achetaient leurs toiles, généralement lors des grandes foires, directement à Jérusalem.

Incapable de résister à sa curiosité naturelle, Sanson s'enquit de la mission qui avait porté les pas d'Ernaut auprès de la basilique de saint Georges. Il avait le souvenir que le mausolée était fort modeste quand ils l'avaient visité, mais un ambitieux chantier commençait à s'y organiser. Ernaut confirma que les travaux allaient bon train, noyant le poisson du pourquoi de son déplacement dans des anecdotes sur les lieux. Libourc sourit à l'évocation du dragon, interrompant alors le flot des paroles de son fiancé.

« Sais-tu que j'ai eu grande terreur des dragons près Lydda quand nous l'avons visité, mon ami ?

--- Il avait pourtant occis la moindre de ces créatures ! se gaussa gentiment Ernaut.

--- Je ne plaisante pas. Nous avions cheminé par Césarée, au plus fort de l'été, et j'y avais aperçu ces horribles dragons[^28] qui infestent les paluds environnants. Lorsque le clerc nous a conté que saint Georges en avait occis un gigantesque, j'en ai tremblé de terreur à l'idée que les siens puissent rôder aux abords. Il m'a fallu longtemps pour ne plus m'enfrissonner de ces démoniaques créatures.

--- Ce ne sont que gros lézards m'a confié un des hommes de la sergenterie. Ils sont particulièrement retors, se cachant au fil de l'eau pour jaillir sur leur proie avec une vigueur qu'on ne soupçonnerait pas, à les voir alanguis au soleil. Raoul dit que leur peau donne un cuir fort résistant.

--- Tout de même, je suis bien aise qu'il ne s'en trouve aucun dans nos montagnes ! »

Un peu plus tard lors de la veillée, Ernaut prit un moment pour discuter avec Libourc tandis que Raoul affrontait Sanson aux échecs, que ce dernier jouait à l'aide d'un dé selon une règle qu'il prétendait être de Meaux. Les deux jeunes gens s'installèrent en bout de banc, cherchant à créer un peu d'intimité par la moindre lumière de l'emplacement. Ils demeuraient de toute façon bien en vue de Mahaut, qui ne détachait guère son regard des gestes qu'ils faisaient.

« Sot que je suis, j'ai oublié de te faire monstrance du panier que je t'ai rapporté de Lydda. Un artisan qui faisait de la belle ouvrage avec des feuilles de palmier.

--- C'est si gentil à toi, mon beau. Mais ne devrais-tu garder ton bien pour nous installer au plus vite ?

--- Ce n'est pas fort onéreux présent. Il te sera pourtant utile, je pense, avec un petit couvercle pour en protéger le contenu. »

Libourc lui serra la main, un sourire creusant de fossettes son visage lisse. Ernaut se retint de la prendre dans ses bras, sachant que Mahaut n'accepterait un tel geste en sa présence. Il lui était toujours difficile de supporter l'inflexible autorité parentale, surtout quand, comme aujourd'hui, il n'avait aucune occasion de s'isoler au moins partiellement avec sa bien-aimée.

« Je verrai avec Lambert si jamais des occasions d'augmenter mon bien se présentent. Il est fort habile pour faire croître et prospérer ce qui lui tombe entre les mains. Nous avons déjà fait quelques plantations, mais n'en récolterons le fruit que d'ici quelques années. J'ai par contre serré en bonne cache les quelques livres que l'assaut des béjaunes de la Mahomerie m'a rapporté[^29]. Et puis le roi va de certes lancer de grandes campagnes l'été à venir. Il y aura occasion de picorer.

--- Je n'aime guère que tu parles de ces fols errements. Ton âme, tout autant que ta chair, y est en grands périls. Je n'aimerais pas que tu fondes notre famille sur pareille litière.

--- Hé quoi, ma mie, ne fait-on pas plus belle récolte où les animaux se soulagent ? »

Libourc accorda un sourire, mais Ernaut voyait bien qu'elle n'était pas à l'aise avec cette idée. Il lui prit à son tour les deux mains.

« Ne te mets pas en angoisses pour mes fallacies. La guerre n'est pas de mon fait, et le roi ne s'y adonne que contraint par l'ennemi qui souhaiterait nous refouler en la mer. S'il peut en naître quelque bien pour nous, ne repoussons pas cette chance...

--- Tu as raison, bel ami, j'en conviens. Je suis juste toujours peinée de voir tant de bonnes gens s'entrebattre pour un honneur ou l'autre, verser le sang par pure avidité. Ne pourrions-nous vivre en bons chrétiens tous ensemble ?

--- Ma mie, ce serait faire fi des coutumes des Mahométans, des Griffons ou même des Juifs, tant accordés à leurs habitudes qu'ils ne doivent les trouver pas pires que les nôtres. Je ne suis pas acertainé qu'ils les changeraient sans quelque urgence à le faire. »

Libourc en convint silencieusement, puis se contenta un long moment de profiter de ces instants de paix, en présence de l'homme qu'elle avait choisi pour vivre sa vie à ses côtés. Ils reprirent leur conversation, échangeant sur les possibles lieux où ils s'installeraient, se chicanant aimablement de détails dont aucun ne correspondrait à la réalité. Mais cela leur importait peu, ce qui comptait était de bâtir ce futur ensemble.

Chapitre 6
----------

### Jérusalem, porte de David, fin de matinée du lundi 22 décembre 1158

Lorsqu'ils arrivèrent en vue de la porte principale, Ernaut et Raoul ne démontèrent pas. Ils saluèrent les hommes de faction, occupés à surveiller une longue file de chameaux dont les lourds paquets dodelinaient à chaque mouvement. Les pauvres bêtes semblaient peiner à supporter les imposants fardeaux et Ernaut s'étonna qu'elles aient réussi à franchir l'une ou l'autre des vallées de Judée. Quand ils parvinrent au niveau du bâtiment des douanes, ils aperçurent Droart qui vint les accueillir rapidement, saluant brièvement Raoul avant de sourire avec chaleur à Ernaut.

« J'ai appris que tu es allé sur la piste d'un tien compère mortellement navré. Passe donc ce soir chez moi, on boira quelques godets à sa mémoire. »

Ernaut accepta l'aimable proposition, d'autant que l'épouse de Droart, pour n'être pas très joviale, était bonne cuisinière. En outre, il appréciait fort Droart, dont le caractère enjoué recélait des trésors d'amitiés. Il s'était montré d'une hospitalité sans réserve depuis qu'Ernaut l'avait rencontré, quelques mois plus tôt. On lui prêtait inconstance et pusillanimité, mais Ernaut n'avait qu'à se louer de lui.

Les deux cavaliers ne pénétrèrent que peu dans la rue de David, qui fendait la ville en deux vers l'est, préférant obliquer à main gauche afin d'emprunter des voies annexes, un peu au nord. Cela leur permettait d'éviter les quartiers les plus commerçants. Il était généralement difficile, voire impossible d'avancer à cheval, à moins de se faire devancer par quelques hommes aptes à bousculer les chalands récalcitrants, parfois même les produits des boutiquiers, qui en prenaient à leur aise sur la largeur du passage.

Ils n'échappèrent néanmoins pas à la presse, fort agitée en ces jours précédant les fêtes. Ils longeaient d'importants marchés, aux grains et aux bêtes, où chacun venait s'assurer de stocks en quantité pour profiter tranquillement de la période de Noël et, surtout, de l'Épiphanie. Chaque année, il s'ensuivait un renchérissement des prix qui faisait grogner les ménages les plus modestes et sourire les vendeurs les plus aisés. D'autant que les cens étaient généralement à verser dans la même période, aux alentours de Noël. La file des miséreux qui bénéficiaient d'une écuelle à l'hôpital de Saint-Jean était toujours plus conséquente en ces moments, parfois autant que lors de la jointure, quand le grain de l'année n'était pas encore récolté et que les réserves avaient fondu.

Ernaut et Raoul abandonnèrent leurs montures aux mains des valets d'écurie, se contentant de prendre chacun leur portemanteau afin de s'engouffrer vers les bâtiments administratifs. Il ne s'y trouvait que Bertaud, occupé à déplacer des piles de méreaux[^30] sur la table servant à faire les comptes. De nombreuses tablettes et quelques rouleaux trônaient sur des liasses de papier. Il les salua aimablement sans pour autant interrompre son travail. Ils se hâtèrent de déposer leurs affaires et d'ôter leurs chapes de voyage. Lorsque Raoul se laissa tomber sur un des bancs, Ernaut lui montra sa petite boîte de messager.

« Je vais quérir le sire sénéchal, attends-moi ici s'il souhaite t'encontrer aussi. »

Il sortit alors en direction des étages supérieurs. Il était désormais familier des coins et recoins de ce dédale qui formait palais. Ce n'était qu'un enchevêtrement de salles, couloirs obliques, escaliers, jardins, courettes, marches et portes, sans logique ni plan concerté. Apprendre à s'y déplacer efficacement constituait un des talents de la nombreuse domesticité qui s'y côtoyait.

Ernaut parcourut tout l'étage noble. Là se trouvaient les zones de réception et les petites chambres où généralement les officiers d'importance accomplissaient leurs tâches. Avec l'armée au loin, la Cour avait déserté pour la Noël et les lieux lui parurent étrangement calmes. Dans la grande salle d'honneur, utilisée par le roi et la Haute Cour pour leurs conciliabules, il ne se rencontrait qu'une poignée de valets en train de suspendre un nouveau lustre où les lampes se superposaient par dizaines. Partout on lui indiqua que le sénéchal n'avait pas été vu de la journée.

Il retrouva Brun, les bras encombrés d'un épais volume, qui sortait d'une des petites pièces de travail. Il était un des rares à posséder les quelques clefs pour accéder aux archives et chambres où l'on conservait les plus importants documents.

« Mestre Brun, je suis en quête de mon sire Jean d'Acre. Auriez-vous connoissance de sa présence ?

--- Il ne sera pas là d'ici demain. Est-ce urgent ?

--- Pas que je sache. C'est le sire évêque de Lydda qui m'a donné message en réponse. »

Brun ne jeta même pas un coup d'œil à la boîte de messager, habitué qu'il était à voir aller et venir les coursiers.

« En ce cas, je te conseille de venir demain à la première heure. Il a quelques audiences de prévues, tu pourras le lui remettre à cette occasion. »

Ernaut remercia Brun et retourna vers la salle voûtée où les sergents réalisaient la plupart des tâches administratives, que d'aucuns appelaient parfois la sénéchaussée. Il obliqua par les cuisines où il réussit à négocier un peu de pain garni de hoummous. Il savait que des futailles étaient toujours présentes non loin pour se désaltérer sans avoir à baguenauder dans tout le palais. Les officiers avaient trouvé là une façon habile de maintenir les hommes au travail. Il s'enquit également du dîner, qui n'allait pas tarder à être corné lui répondit-on.

Raoul accueillit le pain avec force remerciements et s'employa à en faire disparaître sa part avec appétit. Cela les mit en train pour prendre un vrai repas, qu'ils partagèrent avec Gaston, un autre des sergents, parmi les vétérans. Celui-ci accueillit leur entrain et leur jeunesse avec enthousiasme. Il était encore de corvée d'inventaire, avec un petit clerc qui l'aidait à faire le tour de tous les arsenaux de la cité.

« Je vais pas me plaindre qu'ils envisagent de vérifier le fourniment juste avant que les cliquailles de la Noël aillent garnir les coffres de not'sire Baudoin, mais je commence à croire qu'on me prend pour un clerc de la Secrète.

--- Au moins n'as-tu pas à battre la campagne sous la pluie et le froid. J'ai entendu dire qu'ils avaient sale temps dans le Nord.

--- Je suis pas en sel, je crains pas la pluie. Si j'ai fait jurement, c'était pas pour me retrouver cloîtré comme moine. Dans ces maudites pièces, l'air est pourri, il me gâte les entrailles. »

Il avalait pourtant le brouet poivré avec vigueur et postillonnait avec énergie tout en s'exprimant d'une voix habituée au fracas des batailles, assez forte pour s'imposer à plusieurs tables de distance. Tout le monde savait qu'on le traitait en fait justement parce qu'il n'était plus si vaillant, et on le ménageait, ainsi que la troupe qui aurait dû s'accommoder d'un chef boiteux diminué avant l'âge. Il faisait bonne figure jusque là, mais sa vie aventureuse l'avait vieilli bien vite et il refusait de l'admettre, au moins publiquement. Par contre, chacun s'accordait sur sa compétence à préparer les hommes pour le combat, que ce soient les formations de fantassins ou pour le duel face à face. Même diminué, personne n'aurait eu le courage de l'affronter en champ ouvert.

L'après-midi, Raoul fut affecté au tri des rouleaux de cens qui seraient perçus. Il convenait de les avoir disponibles pour les jours à venir, quand les occupants des maisons verseraient leur écot. Chacun savait normalement par la coutume combien il avait à payer, mais il arrivait parfois que certains tentent d'en abaisser le montant. Les documents établis depuis le début du royaume permettaient d'en attester sans doute aucun, même s'il était fréquent que les plus récalcitrants refusent de se soumettre à des écritures dont ils soutenaient qu'elles n'avaient pas la valeur de témoignages d'anciens habitués aux pratiques ancestrales. Mais l'administration de Baudoin III se voulait moderne et se dotait d'archives scripturaires, sans se contenter de la mémoire des habitants, assujettis et percepteurs.

Pour sa part, au retour du réfectoire, Ernaut passa dans le palais du patriarche, qui partageait de nombreux accès avec celui du roi, et demanda s'il pourrait avoir une entrevue avec Amaury de Nesle, ayant un message à lui remettre. L'ecclésiastique recevait le lendemain en fin de matinée, ayant des audiences publiques de prévues. On lui indiqua de revenir à ce moment-là. De retour à la sénéchaussée, on l'envoya ensuite faire escorte au mathessep. Plusieurs accusations étaient remontées jusqu'à la cour des Bourgeois, à propos d'un marchand de grain qui abuserait ses clients avec l'aide d'un mesureur recourant à des jauges faussées. Ernaut appréciait ces opérations de contrôle et de justice. Il ne s'agissait généralement que de confisquer les objets du délit et de convoquer le fautif à un jugement, et bien peu osaient affronter ouvertement l'autorité royale.

Quelques-uns disparaissaient avant leur procès. L'administration percevait les amendes sur les biens abandonnés, accordant l'excédent aux plaignants pour qu'ils en disposent à leur gré. Le plus important était de toute façon que le trouble ne se perpétuât pas. Le contrôle des prix des aliments était capital dans les grandes cités, surtout en période de moindre production. Même le plus égoïste et irréfléchi des barons savait qu'il était dangereux de ne pas faire en sorte que son peuple ait de quoi se nourrir à des tarifs corrects. Des villes s'étaient livrées à des ennemis pour ce genre d'inconséquence.

Les deux contrevenants firent bonne impression à Ernaut, jurant sur tous les saints qu'ils étaient honnêtes dans leurs pratiques et attestant de l'ancienneté de leur métier. Ils imputaient les allégations à des concurrents jaloux. Il fallut le recours à un maître huchier avec des compas pour établir la fausseté des ustensiles et leur rabattre le caquet. Ils se tinrent alors cois, Noël s'annonçant fort mal pour eux.

Ucs de Monthels se montrait chaque fois assez peu amène avec les fraudeurs et n'hésitait pas à les bousculer à l'occasion. Il fit enclouer l'échoppe du marchand et confisqua tous les instruments de mesure de son comparse. À cela s'ajouta une forte saisie de leurs biens, en garantie de leur venue à la Cour des Bourgeois pour se défendre avec leurs témoins face à leurs accusateurs. Comme il n'y aurait aucune session durant les fêtes, ils allaient devoir patienter jusqu'en janvier.

Alors qu'ils s'en retournaient au palais, Ernaut se rapprocha du mathessep.

« Mestre Ucs, je n'ai pas grand usage des pratiques judiciaires, mais pourquoi ne leur fait-on pas directement payer l'amende ?

--- C'est qu'il n'est pas assuré qu'ils aient fauté, ce sera à la Cour d'en décider.

--- Mais nous avons découvert malhonnêtes mesures en leur possession.

--- Quand bien même ! Peut-être ont-elles été posées là par leurs accusateurs même ! Ces derniers sont nombreux et de bonne fame, ce qui leur donne légitimité. Mais le marchand suspect est connu assez pour fournir des témoins de sa droiture, qui seront prêts à jurer que c'est là tromperie et manipulation. S'il s'en trouve assez, cela pourrait faire incliner le plateau du jugement en sa faveur. »

Ernaut se rendit à la pertinence de la remarque, mais il lui paraissait que Ucs de Monthels ne semblait guère convaincu par son propre discours.

« Cela ne donne-t-il pas moyen aux plus habiles et fortunés de s'entourer de faux témoins et de bons amis jurant contre espèces sonnantes ?

--- Ils encourent forte peine pour témoigner faussement. Mais, de fait, j'ai trop vu de sournois bourgeois le cul cousu d'or qui échappaient aux accusations de plus modestes qu'eux. Selon les jurés présents, la justice balance qui ci qui là. Il s'en trouve d'aucuns plus enclins à chercher le vrai dans toutes choses, tandis que d'autres se contentent de respecter les usages sans s'attarder au fond du problème. J'ose encroire que les deux malandrins que nous venons de dénicher ne s'en tireront pas trop aisément. Je n'aime pas les affameurs et je m'efforcerai de faire en sorte qu'ils soient jugés un jour où siégeront les plus sensibles à ces questions. »

Ernaut comprit que malgré son statut de première importance et son attitude parfois un peu distante, le mathessep demeurait un homme du commun, sensible aux misères des plus impécunieux. Peut-être n'était-il pas issu d'une riche et prestigieuse lignée de négociants. Mais, surtout, il retenait que la justice royale, pour équitable qu'elle se prétendait, pouvait se voir ajuster par quelques poussées adroitement placées. Ce n'était pas là manipulation grossière et corruption active, mais modeste chiquenaude imperceptible. Qui pouvait avoir grandes conséquences.

### Jérusalem, palais du patriarche, fin de matinée du mardi 23 décembre 1158

La salle d'audience du patriarche n'avait que peu à envier à celle du roi. Les symboles du pouvoir, pour n'être pas guerriers, en étaient aussi ostentatoires : étoffes, décors et meubles de prix encombraient la pièce avec une accumulation qui en détruisait l'harmonie. Partout l'œil n'encontrait que coffres illustrés de scènes bibliques, fresques et mosaïques murales racontant les hauts faits de saints, dais sculpté, peint et rehaussé de tentures lourdes de motifs rebrodés, vitraux ajoutant au chaos des formes l'exubérance des couleurs.

Le patriarche, habillé d'une parure chatoyante de passementeries au fil d'or et de pierreries, était installé sur un trône magnifique dont le dossier était orné en partie supérieure d'une mandorle abritant un Christ triomphant. Nul ne pouvait ignorer de qui l'ecclésiastique assis là tenait son autorité, ni de sa toute-puissance dans ce monde et au-delà. Autour de lui, répartis sur des chaires, quelques religieux aux atours aussi dispendieux quoique légèrement plus sobres, compensaient leur moindre statut par des crucifix ouvragés suspendus à leur cou et des attributs, bague ou bâton, indiquant l'abbé, l'évêque ou le doyen d'un collège de chanoine. Enfin, un peu en retrait de part et d'autre, de modestes clercs de la chancellerie étaient présents pour noter les minutes, aller quérir tout document utile ou préciser un point de droit ou une question patrimoniale épineuse.

Ernaut se trouvait parmi les derniers solliciteurs. Il avait dû assister aux audiences du sénéchal avant tout, afin de lui confier en mains propres la missive de Constantin de Lydda. Jean d'Acre, qui avait l'esprit tourné vers les perceptions des jours à venir, ne l'avait écouté que d'une oreille distraite et lui fit remettre le pli à un de ses scribes. Ernaut n'était même pas certain qu'il avait entendu sa demande de se rendre au palais du patriarche avant d'y acquiescer. Pour être un homme compétent et, disait-on, assez intègre, il devenait évident à tous qu'il déclinait assez vite ces derniers mois et ne semblait plus guère capable de mener de front toutes les tâches qui relevaient de son service.

Les audiences d'Amaury de Nesle paraissèrent assez similaires à ce qu'Ernaut voyait régulièrement à la Cour des Bourgeois, avec juste la différence de jargon et une pompe plus solennelle. Les demandes étaient souvent purement territoriales, questions de propriétés, donations faites par des fidèles désireux de sauver leur âme par des messes, mais aussi parfois des sollicitations dont seuls les plus lettrés en l'assemblée devaient comprendre les implications. Les échanges se faisaient alors rapidement en latin, afin d'y apporter toute la précision nécessaire à des points de liturgie, de dogme ou de préséance cléricale.

Lorsque vint son tour, Ernaut s'avança et s'agenouilla respectueusement devant Amaury de Nesle. En relevant la tête, il eut le plaisir de voir le patriarche lui sourire. Il demeurait en lui le visage franc et direct du doyen des chanoines qui l'avait interrogé à plusieurs reprises l'année précédente, illuminé de l'intelligence vive trahie par un regard perçant et affairé. D'un geste de la main, il invita Ernaut à s'exprimer.

« Sire patriarche, je suis céans pour vous porter missive du sire évêque Constantin de Lydda. »

Il ouvrit la petite boîte qui ne contenait plus que ce document et le remit cérémonieusement au clerc qui s'avança vers lui.

« A-t-il transmis un message oralement en plus de ces écrits ?

--- Il prie pour vous et votre sauveté des ennemis qui voudraient s'opposer à vous, et vous sait grâce d'avoir entendu son appel en si étrange affaire. »

Entendant cela, le patriarche se fit remettre le document et le parcourut d'un murmure. Fronçant les sourcils, il donna quelques instructions à l'oreille de son servant avant d'envisager Ernaut de nouveau.

« Je sais gré à Sa Majesté le roi Baudoin d'avoir délégué des gens de son hostel pour assister la Sainte Église en ses inquiétudes. »

Puis il bénit Ernaut, toujours agenouillé, avant de le congédier. L'entrevue était terminée. Ernaut se dirigeait vers la sortie lorsque le clerc qui avait transmis le message vint l'arrêter. Le patriarche souhaitait l'entretenir seul à seul une fois ses audiences achevées. Il invitait donc Ernaut à patienter dans les jardins.

Tandis qu'on le menait, Ernaut s'interrogea sur les raisons de cette discrétion. L'évêque Constantin avait insisté sur le besoin de garder toute l'histoire sous scellés. Peut-être avait-il hésité à en confier la teneur à un écrit, fut-il dans les mains d'un messager royal, et certainement rédigé dans une langue savante comme le latin. Les diplomates avaient l'habitude d'être porteurs dans leur mémoire d'autant d'informations que les documents qu'ils délivraient.

La dernière fois qu'il avait eu à échanger avec Amaury de Nesle, il n'avait été impliqué dans l'histoire que par accident[^31]. De se voir reconnu un rôle officiel dans une intrigue qui touchait aux intérêts de la couronne et du clergé le faisait frissonner de plaisir.

Il dut prendre son mal en patience et il s'entraînait à jouer au palet avec des cailloux dans une travée du modeste cloître où on l'avait abandonné quand enfin on vint le chercher. Il emprunta d'autres passages, inconnus de lui, jusqu'à une petite salle similaire aux pièces de travail des officiers royaux. Bien moins ostentatoire que la halle d'audience, elle était malgré tout richement ornée de céramique bariolée, et les cabinets qui en occupaient le bas des murs étaient tous de qualité, peints et renforcés de fer. Le patriarche était debout devant un lutrin, en train de tourner les pages d'un grand volume à l'épaisse couverture. Il ne portait plus qu'une magnifique tenue de laine pâle, une croix émaillée autour du cou et sa bague pour tout insigne.

C'était là visiblement un lieu de travail où la lumière, filtrée par des carreaux de verre clair, se répandait doucement sur un vaste plateau encombré de chevalets pour codex et de rouleaux. Amaury de Nesle attendit qu'il vînt à lui et lui tendit sa bague à baiser en une génuflexion. Pour être discret, l'entretien n'en était pas moins formel.

« Je suis aise de voir que tes talents ont trouvé excellent maître à servir, mon garçon ! »

Un peu intimidé par l'importance de son interlocuteur, Ernaut en vint à oublier qu'il était difficile de ne pas se rappeler son gabarit et se trouva flatté qu'on se souvînt de lui. Il n'était après tout qu'un sergent parmi tant d'autres. Il bafouilla quelques remerciements, agrémenté d'un salut qu'il espérait respectueux, peu au fait de l'étiquette en pareil cas.

« Peux-tu me conter ce qui s'est passé en les terres de l'évêque de Lydda ? Ce que tu en as compris, du moins.

--- Il semblerait qu'un des chanoines, désireux de créer un feu de guerre similaire à celui des Griffons, n'ait pas réussi à s'en rendre maître après l'avoir créé. Il en est mort.

--- Et le clerc de l'archevêque ?

--- Malencontreux accident comme il s'en trouve si souvent sur les chantiers. Je le déplore grandement, car je comptais le père Gonteux pour un ami, ayant traversé les mers à ses côtés pour venir ici.

--- Rien de plus ?

--- Rien ne permet de le croire. S'il y a eu lien entre eux, c'était parce que le père Waulsort était fort érudit dans les traditions des saints. Le père Gonteux souhaitait l'entretenir de ça pour son propre dessein, un travail qui lui tenait à cœur. Cela n'avait rien à voir avec ces recherches de feu. »

Le patriarche promenait doucement sa lourde silhouette autour de la pièce tout en écoutant avec application, ses yeux clairs parcourant au décor sans y prêter le moindre intérêt. Il n'était qu'attention pour le récit d'Ernaut.

« Tu n'encrois pas à quelque sournoise meurtrerie ou mystère ?

--- Meurtrerie, je ne saurais en jurer, mais rien ne l'indique. Il y a bien quelques ténèbres demeurées en cette histoire, mais elles ne concernent nullement nos morts.

--- Tu parles de ce mystérieux maître qui fournissait le chanoine en onguents, poudres et instruments ?

--- De certes. Mais c'est là mystère qui appartient au mort. Il est de peu de chances qu'il lui doive son trépas, donc je n'ai pas à y chercher plus avant. »

Le prélat esquissa un sourire à la réponse d'Ernaut. Il avait déjà pu constater l'immense curiosité du géant face à lui et doutait de sa capacité à la contraindre bien longtemps. Il appréciait néanmoins ce qu'il entendait. Le plus important pour lui était de ne pas provoquer de problème avec la couronne. Baudoin III était jeune et impétueux et moins porté sur la religion que sa mère Mélisende. Il ne fallait pas qu'un membre de l'église soit à l'origine d'un scandale avec les Byzantins, dont le roi espérait faire des alliés contre le sultan syrien Nūr ad-Dīn.

« Qu'apenses-tu de ces histoires de magie et de démons ?

--- Pas grand-chose, sire patriarche. J'avoue ne pas être connaisseur de tels sujets, qui sont plus familiers aux clercs et savants. Je ne vois néanmoins pas pourquoi aller chercher pareilles réponses alors que les questions qui se posent ne le nécessitent pas.

--- Ne crois-tu pas au merveilleux et à l'existence de pouvoirs surnaturels ?

--- Je ne sais pas trop, à dire le vrai. Je crois ce qu'en ont dit ma parentèle et les curés de chez moi, à Vézelay. Dieu et ses saints, je sais bien qu'ils agissent autour de nous, mais le comment de ça... Outre, cela n'est d'aucune mesure en cette affaire, qui se peut expliquer de façon bien naturelle, ainsi que je vous l'ai narré. »

Le patriarche revint à sa table, s'y appuya avec cérémonie et releva les yeux vers Ernaut. Son ton et sa voix avaient changé, retrouvant soudain le timbre qu'il avait adopté dans la grande salle.

« Tu as bien œuvré en cette affaire, mon garçon. Je le ferai savoir à ton hostel. Il te faut poursuivre en même chemin et garder tes lèvres scellées sur tout ce que tu as appris. »

Ernaut hocha le menton en silence, mal à l'aise devant l'aspect soudain pontifiant d'Amaury de Nesle.

« Il n'est pas de meilleure intention qui, captée par maline intelligence, puisse engendrer de terribles catastrophes. Selon tes mots, les prêtres ont joué de malchance les deux fois. Ne nous aventurons donc pas outre, Dieu nous a fait clairement comprendre que la voie était fermée. N'incitons pas la curiosité en babillant à tort et à travers. »

Puis il tendit sa main à Ernaut pour qu'il baise son anneau, lui signifiant par là son congé. Le jeune homme se retira après un salut respectueux. Lorsqu'il tira la porte, un valet l'attendait pour le guider jusqu'aux passages menant à l'hôtel du roi. Enthousiasmé à l'idée qu'on parlerait de lui en bien aux plus hautes sphères, il regrettait juste que Baudoin ou son frère Amaury fussent absents. Il lui faudrait trouver Régnier d'Eaucourt pour lui faire part de sa réussite. Le chevalier, quoique parfois un peu rude dans ses remarques, l'avait toujours soutenu dans ses velléités d'ascension. Ce fut d'un pas léger qu'il se rendit au réfectoire lorsqu'il entendit sonner la corne du premier appel au repas. Il avait un appétit à engloutir un cheval !

À peine était-il arrivé aux abords des bassins où les domestiques se lavaient les mains avant de manger qu'il fut arrêté par un des petits garçons à tout faire qui portaient messages et instruction d'une aile du palais à l'autre. Le gamin lui indiqua qu'il était convoqué de toute urgence.

« Qui donc m'espère ? J'ai déjà remis message au sénéchal et je sors à peine de l'entretien avec le patriarche.

--- On ne m'a pas dit le quoi du comment, juste de te faire venir ! »

Et sans plus de cérémonie, le gamin s'engouffra dans un des escaliers. Si Ernaut voulait avoir la réponse à sa question, il lui faudrait suivre le mouvement, au risque de rater son dîner.

### Jérusalem, palais royal, mi-journée du mardi 23 décembre 1158

Ernaut connaissait l'endroit où on le menait : une des petites salles de travail de l'étage noble, les murs percés de nombreuses niches et un grand plateau en son centre. Derrière celui-ci, le sénéchal Jean d'Acre discutait à voix basse avec un interlocuteur qu'Ernaut reconnut pour l'apercevoir de temps à autre au palais : un des hommes du frère du roi, le comte Amaury de Jaffa. De faible corpulence, sa taille accentuait son aspect arachnéen et, n'eussent été ses yeux ornés de rides profondes, on aurait pu le croire assez jeune. Le cheveu rare autour du crâne, il le portait court, mais suivait la mode en ce qui concernait la barbe finement coiffée en pointe. Sa tenue était également de la dernière élégance, avec de larges manches et un buste assez serré, en soie. Lorsqu'Ernaut entra, les deux hommes continuèrent un moment à échanger à mi-voix puis Jean d'Acre se leva.

« Mon garçon, voici le sire Guy, sénéchal du comte Amaury. Réponds-lui ainsi que tu le ferais à moi. »

Puis il salua son invité et sortit sans plus un mot. Ernaut attendait, un peu fébrile à l'idée qu'on lui demande plus d'explications. Il n'avait jamais eu affaire à ce sénéchal. Il lui donnait l'impression d'un nobliau de Cour peu familier du harnois et de la selle. Assis à la table, Guy y compulsait quelques documents d'un air détaché, ne se décidant à darder ses yeux sombres sur le sergent qu'après un long moment. Puis il demeura ainsi sans mot dire, comme s'il voulait évaluer un éventuel pouvoir hypnotique sur le jeune homme. Habitué à se faire remettre en place par de plus martiaux que ce coquelicot de Cour, Ernaut se contenta de fixer le mur, résistant à l'envie de le dévisager à son tour.

« Prends donc un siège, sergent. Je ne suis pas chevalier officiant en ton hostel. Tu as nom Ernaut, c'est cela ?

--- Oui, concéda Ernaut, un peu surpris à l'invitation de s'asseoir en présence d'un tel baron. Ernaut de Vézelay.

--- Un Bourguignon ! Peut-être devrais-tu dire de Jérusalem, maintenant que tu t'es juré à notre roi. »

Il assortit sa remarque d'un sourire amical.

« J'ai demandé à te voir, car toute cette enquête se déroule en les terres de mon sire Amaury. Ce qu'en dit le sire évêque me semble pour le moins... tronqué. Il me faut avoir meilleure vision de toute l'affaire. »

Ernaut lui résuma une nouvelle fois l'histoire, telle qu'il la comprenait, en y ajoutant plus de détails sur ses doutes et les manques qu'il voyait dans l'enquête, en particulier la méconnaissance du commanditaire des travaux de Waulsort. Il évoqua aussi plus longuement la possible connexion avec le prélat proche de l'empereur des Allemands. Sans oublier de mentionner son idée de camoufler les événements derrière une miraculeuse découverte. Qu'au moins quelques-uns sachent d'où venait vraiment cette suggestion.

« Tu as la cervelle bien habile pour simple sergent. Es-tu fils caché de quelque baron ?

--- Mon père est important vigneron et il nous a fait instruire, avec mes frères, chez les prêtres de Sainte-Marie.

--- Il serait de certes fier de ce que je vois en ce jour, adoncques. »

Ernaut accepta le compliment de bonne grâce. Guy lui sembla soudain plus sympathique. Un muguet de Cour, mais qui savait reconnaître les gens de valeur.

« En as-tu dit la même râtelée au sire patriarche ou à l'évêque ?

--- Peu ou prou, oui. Il m'avait été indiqué que j'étais là-bas à leur demande, la couronne rendant service à l'Église.

--- Certes, certes. Mais il te faut te ramentever à chaque instant que ta fidélité va avant tout au sire roi, pas à un clerc, fut-il le meilleur ami de Baudoin. »

Ernaut commençait à se demander si Guy était vraiment au service d'Amaury. Tout le monde connaissait la bonne entente entre les deux frères, mais que le sénéchal de l'un sermonne les hommes de l'autre lui semblait étrange.

« Quoi qu'il en soit, il n'y a là rien de bien grave. Tu t'es assez plaisamment sorti de cette histoire, étant donné les circonstances. Régnier ne s'y était pas trompé. »

Il se pencha vers Ernaut, croisant les doigts en baissant la voix.

« Vu que tu es fils de vigneron, tu dois assavoir que le vin tiré doit être bu. Tu vas donc t'en retourner à Lydda et faire en sorte de récupérer ce que tu peux des notes et des affaires du chanoine Waulsort. Ton idée de laisser entendre que l'histoire serait due à quelque démon est excellente, fais en sorte qu'elle se propage. De la façon la plus grossière sera le mieux. Que les pérégrins en aient pour leur argent. Je ferai en sorte aussi de faire connaître l'anecdote. Cela ne pourra que faire affluer les visites et les dons au sanctuaire, ce dont tout le monde bénéficiera au final. C'est donc là bien pieux mensonge... »

Voyant qu'Ernaut se trémoussait sur son siège, il s'interrompit et l'invita à s'exprimer.

« Je ne suis pas certain que je puisse vous rapporter les affaires du chanoine. Tout le monde semble s'entendre là-bas pour dire que nul ne doit les voir, et qu'il serait peut-être profitable, même, de les faire définitivement disparaître.

--- Où les recèlent-ils ?

--- En une petite chambrette, fermée à clef.

--- Alors en ce cas, tu trouveras quelque prétexte pour y retourner et faire copie de tout ce qui te paraîtra pertinent.

--- C'est que je n'entends guère les lettres, sire Guy. Et certes pas les langues dans lesquelles tout ça est rédigé.

--- Nous t'adjoindrons le même scribe que précédemment. Tu n'auras qu'à lui dire qu'il est besoin de ces textes pour le service du roi. Outre, cela évitera à de nouvelles personnes d'y porter les yeux. »

Ernaut acquiesça puis redemanda la parole, que Guy lui accorda.

« L'évêque Constantin voudra de certes en savoir plus sur ce que je fais là-bas. Il m'a semblé assez... sourcilleux sur ses privilèges.

--- Il ne faut certes pas lui faire penser qu'aucun d'eux serait écorné. Tu n'as qu'à lui dire que le roi a besoin de connaître l'homme qui payait Waulsort, qu'un tel espion ne saurait être toléré en le royaume. Mais tu t'emploieras plutôt à collecter le savoir du chanoine.

--- Sans me mettre en chasse de celui qui menait dans l'ombre ?

--- Nul besoin de s'épuiser à cela. Il a sûrement déjà battu le rappel et ses chiens s'emploient de certes à effacer ses traces avec frénésie. En apprendre autant que lui sur cette arme de guerre me paraît plus urgent. N'en parle à personne et ne fais rien qui pourrait éveiller les soupçons quant à l'intérêt que cela représente pour la couronne. Officiellement tu ne fais que continuer l'enquête sur un espion qui aurait partie liée à la mort du chanoine, rien de plus.

--- À qui dois-je en référer, sire ?

--- À moi et uniquement à moi. Si jamais je ne pouvais t'entendre, je te dépêcherai coursier avec un signe valide de reconnaissance. »

Ernaut hocha le menton. Son enthousiasme à se voir confier une vraie mission secrète se teintait d'une légère angoisse à l'idée que tout cela fut justement si discret. Il lui faudrait faire attention à chacune de ses paroles et il lui semblait que face à lui se trouvaient de si formidables puissances qu'elles ne prendraient guère de gant pour traiter avec un simple sergent, aussi colossal que fût son gabarit. Ce fut donc d'une voix bien moins fanfaronne que d'habitude qu'il répondit.

« Quand dois-je me mettre en route ? Ce jour ?

--- Il y a beaucoup d'ouvrages ces temps-ci à la sénéchaussée. Sans compter les fêtes. Si nous courions trop vite à Lydda, cela pourrait mettre la puce à l'oreille que c'est là chose de première importance pour nous. Il faut qu'on ait l'impression que des messages sont allés et venus, et que c'est bien là décision du roi, inquiet d'une espie. Mais si nous tardons trop, nous prenons le risque que tout soit détruit, ainsi que tu l'as dit. »

Le sénéchal réfléchit un petit moment.

« Le mieux serait entre Noël et l'Épiphanie. Je verrai avec le sire Jean d'Acre qu'il te mande d'ici une semaine environ. »

### Jérusalem, demeure de Droart, soirée du jeudi 25 décembre 1158

La maison résonnait des cris des enfants excités par la fête, des voix des hommes s'interpellant tandis qu'ils faisaient rouler les dés, déplaçaient les pions ou narraient leurs anecdotes à quelques compagnons. Droart avait convié des amis pour les célébrations de la Noël. Après s'être tous rendus au Saint-Sépulcre pour y assister à la grande messe solennelle, ils s'étaient retrouvés chez lui, profitant des nombreuses salles, dont la majeure partie nécessiterait encore de longs et coûteux travaux. Ils allaient et venaient entre l'intérieur et l'extérieur au rythme de l'échauffement des danseurs ou des jeux pratiqués.

Plusieurs pièces de viande avaient été apprêtées pour l'occasion, ainsi qu'un large éventail de pâtés commandés aux alentours. Chacun avait porté de quoi agrémenter le festin, depuis les sauces doucereuses, sucrées ou relevées jusqu'aux compotes de fruits ou aux biscuits au miel. Et, bien sûr, de grandes quantités de boissons avaient été préparées, que ce soient des vins trempés d'épices ou de la petite bière brassée chez soi, chacun pouvait trouver de quoi flatter son palais.

Droart profitait de la fête pour faire visiter les ruines et les chantiers, auxquels nombre de ses amis participaient.

« J'avoue que j'ai eu plus gros yeux que gros ventre, mais ce sera bonne rente pour les enfants. Il y aura de quoi louer échoppes et hostels, voire implanter un bel endroit de négoce...

--- Tes fils vont finir à la Cour, mais pas aux ordres du mathessep. Ils seront là-bas jurés de bon rang, s'amusa Eudes.

--- Moque, moque ! Mais le Houdard sait déjà fort bien compter.

--- Il a bon maître, je ne connais guère de sergent plus apte à faire de deux et deux cinq » se gaussa Abdul Yasu, un autre de leurs compères.

Il était fort commun de graisser la main aux hommes du roi pour obtenir de petites dérogations ou facilités, et chacun en retirait de quoi mieux nourrir sa famille ou s'offrir bons repas en quantité. Mais Droart avait érigé cela en art, habile à organiser ses affaires et à rendre service là où il y gagnerait le plus de remerciements. En espèces sonnantes et trébuchantes parfois, mais surtout en amitiés voire en parts dans les transactions qui lui permettaient d'accumuler à grande vitesse. Certains de ses intimes s'inquiétaient qu'un jour on lui en fasse reproche. Il n'accordait jamais aucune importance à ces remarques, confiant dans le fait qu'il ne franchissait aucune limite et, surtout, veillait à ne pas trop s'acoquiner avec des personnes de mauvaise renommée.

Avec un petit groupe, ils visitaient le chantier d'une boutique qui posséderait une pièce de stockage à l'arrière, une salle et une terrasse au-dessus, le tout ouvrant dans un ancien souk, où se tenait un marché vivrier ainsi que quelques artisans potiers. Il y travaillait depuis plusieurs semaines, mais avait ralenti avec l'hiver et le froid. Il lui fallait en outre acheter des poutres, élément de prix et de poids, afin de relever les deux niveaux effondrés. Il avait simplement éliminé les gravats, en avait retiré ce qui pouvait être réutilisé ou revendu et commençait à peine à remonter les parties internes abîmées.

« J'apensais qu'il pourrait s'y installer quelque maître œuvrier, proposant paniers et nattes, filets et sacoches. Il s'en trouve toujours un manque quand on vient en ces quartiers. À voir si je ne pourrais pas m'associer avec lui plutôt que d'en exiger fort loyer.

--- Le cens du roi n'est pas trop élevé ?

--- Certes pas et c'est aussi pourquoi je préfère avoir une part dans le commerce de ceux à qui je loue. Ils n'ont pas tant besoin de courir la piécette pour me payer mon dû. Et il y a toujours espoir d'en gagner plus, ce qui est profitable à tous.

--- Tu pourrais quitter le service de l'hostel du roi, maintenant que te voilà ceinturé de biens comme baron royal, proposa Ernaut.

--- Bien au rebours, mon ami. Je fais ma pelote justement parce que j'y suis. J'en fais figure de graisse sur l'essieu, qui irait fort mal allant en grinçant. »

Il assortit sa réponse d'un clin d'œil et continua à la traversée des zones emplies de décombres. Le quartier avait longtemps hébergé des juifs et la prise de la ville par les Latins en avait fait fuir un grand nombre pour les territoires sous protection musulmane. Divers endroits s'étaient abîmés, d'autres avaient été convertis en jardins. Droart en possédait d'ailleurs un de bonne taille, qu'il arrosait grâce à une large citerne dont on lui avait vanté la contenance lors de l'achat. Il avait le projet de la curer, afin que l'eau s'y trouve moins souillée. Sans compter qu'on y découvrait toujours quelques surprises étonnantes, voire de valeur.

Alors qu'ils revenaient vers le bâtiment principal, dont seuls deux niveaux étaient occupés par Droart et sa famille, le troisième étage et les communs demandant encore des travaux, Ernaut se rapprocha de son ami.

« Si d'aventure tu connais quelques honnêtes et bons négociants qui recherchaient petitime associé, n'hésite pas à m'en parler.

--- Serait-ce là ton labeur secret pour nos maîtres en ces temps de cens ? On murmure fort sur tes allers et venues entre les palais et ton voyage à Lydda. Raoul n'a pas desserré les dents à ce propos, ce qui rend d'autant plus curieux.

--- Il nous en cuirait fort d'en trop parler, mon ami. Mais ce n'est pas de là que vient mon regain. J'ai enfin touché la somme promise pour les coups reçus à l'été, peu après la Toussaint. Me voilà en mesure de faire de quelques livres bons tonneaux[^32] !

--- À la bonne heure, voilà sergent selon mon cœur, qui aspire à plus haut rang.

--- Il en est qui ont le col tranché à trop se le hausser, Droart, objecta Baset, qui venait de les retrouver.

--- C'est le souci avec toi, compère, le gourmanda gentiment Droart. Tu vois en chaque chose la ténèbre avant que d'en voir la lumière.

--- Peut-être parce que Dieu a voulu ma vie ainsi. »

Droart et ses amis lui accordèrent un sourire, sachant que leur collègue traversait une mauvaise passe en cette fin d'année. Il n'était pas si exubérant et chaleureux que chacun l'aurait voulu, mais se montrait de temps à autre assez bon compagnon. Il était sans nul doute un des plus honnêtes hommes de la Cité et pour cela chacun s'accommodait de son caractère souvent austère, voire déprimant. Ernaut ne s'entendait guère avec lui, mais le tolérait, parce que c'était un vieux compère de ses nouveaux familiers. Il reconnaissait toutefois à Baset une certaine rigueur qui faisait qu'il prenait ses tâches très au sérieux et n'hésitait jamais à indiquer comment faire les choses. Avec parfois tant de sérieux et si peu de souplesse dans son enseignement qu'on ne le sollicitait pas tant qu'il serait profitable.

Ils rejoignirent la zone où l'on dansait, quelques visiteurs ayant pris soin de se munir d'instruments de musique. Un branle endiablé résonnait dans la cour, entraînant un bon groupe, adultes et enfants mêlés, ces derniers compensant leur manque de compétence par un enthousiasme redoublé. Ernaut accompagna Droart et Eudes jusqu'à un tonneau mis en perce, où ils se servirent un pichet de bière fraîchement brassée. La mousse, épaisse, en accentuait les arômes. Droart l'avait récupéré des Indes, comme il aimait à le dire, ce qui signifiait que c'était le fruit d'une de ses innombrables tractations.

Droart invita ses deux amis à s'installer auprès de lui, sur un banc branlant qu'il avait tiré des décombres, encore gras de plâtre et de poussière. Ernaut s'y assit avec un brin d'appréhension tellement l'objet lui semblait fragile.

« Quoi qu'il en soit, je voulais te dire, Ernaut, qu'il se dit force bien de tes récents voyages. J'ai ouï parler en termes fort élogieux de toi. Il va te falloir prendre garde à ce que tu ne t'attires pas quelques jalousies en l'hostel, à briller si fort et si vite !

--- Qui donc as-tu entendu ?

--- Le sénéchal, l'autre jour, a reçu sire d'Eaucourt et lui a conté par le menu que son absence n'avait finalement causé nul préjudice. Le chevalier se sentait fort mal à l'aise de n'avoir pas servi comme il était prévu. J'encrois d'ailleurs qu'il en garde quelque mauvaise dent au maréchal. En tout cas, il a convenu que tu étais de l'acier dont on fait les plus belles lames.

--- Pour peu que la trempe ne te voie pas finir tout rompu ! plaisanta Eudes.

--- Voilà beau conte qui sonne gaiement à mes oreilles, compères. Il en est de tout cela comme d'un essaim fort bourdonnant, dont on sait qu'on pourrait tirer quantité de bon miel, mais peut-être aussi y gagner méchantes piqûres. »

Droart opina sans plus rien en dire. Sa curiosité le poussait à ne pas s'en contenter, mais Eudes l'avait déjà repris à plusieurs reprises sur le sujet. C'était faire courir de trop grands risques à Ernaut, estimait Eudes. Il voyait bien que leur jeune ami brûlait d'envie de partager son secret, mais si les compliments pleuvaient pour l'instant, ils tourneraient bien vite à l'aigre s'il faillissait à la confiance qu'on avait placée en lui.

« Je dois d'ailleurs m'en retourner à Lydda dès avant l'Épiphanie, indiqua Ernaut. Il m'y reste broutilles à achever pour dire que tout cela est bien fini.

--- Ne serait-ce pas que tu espères échapper aux corvées de compte et de tri ? railla Droart. Bien pratique ces chevauchées au loin tandis que nous devons lever les cens de la Noël !

--- Je confesse volontiers ne pas regretter de m'absenter en ces affaires ! Je laisse compter les monnaies à qui les aime mieux que moi ! »

Dehors, le soleil commençait à diminuer et, le jour ne pénétrant que faiblement dans la maison, quelques enfants passaient de pièce en pièce afin d'y allumer les nombreuses lampes apportées pour ce jour de liesse. Les danseurs s'étaient calmés et refluaient tous à l'intérieur maintenant que les températures se faisaient plus froides. Rapidement la nuit allait tomber. Quelques hommes montèrent des tables pour s'y installer autour d'un plateau ou de dés, tandis que les plus jeunes prenaient place devant un spectacle improvisé de marionnettes.

En réprimandant les plus impatients qui picoraient dans leurs plats alors qu'elles passaient, plusieurs femmes déposèrent de quoi se restaurer sur une longue planche fixée au mur par une rangée de crochets de métal. De nombreux tranchoirs, parfois du simple pain non levé, étaient proposés dans des paniers afin de s'en faire des assiettes.

« Tu seras revenu pour la distribution de linges de l'Épiphanie, Ernaut ?

--- Je ne sais pas encore. La tâche peut s'achever en une journée ou s'éterniser. Cela ne dépend pas de moi.

--- Tu dois te sentir comme chevalier en son fief, à ainsi paonner sans nul vicomte ni mathessep pour te gourmander.

--- De certes, j'aime à pouvoir ainsi galoper sans frein. Mais j'avoue que l'exercice est fort périlleux et je regrette fort nos veillées. Sans bons compères, nul ne peut tant apprécier son chemin. »

Droart et Eudes acquiescèrent et trinquèrent de leur verre en agrément.

« Nous ne savons rien de ce que tu fais au service le roi, Ernaut, et n'avons aucun besoin d'en apprendre quoi ou qu'est-ce pour te prêter la main si le besoin s'en fait, lui confia Eudes.

--- Oui-da, corne haut et fort tel Roland et nous accourrons pour t'assister » approuva Droart.

Puis il ajouta, une lueur de malice dans l'œil.

« Après, peut-être te faudra-t-il nous en conter plus sur l'histoire, aux fins que nous te soyons bien utiles ! »

### Lydda, palais épiscopal, fin d'après-midi du samedi 27 décembre 1158

Ernaut et Raoul eurent la malchance d'être accueillis à leur arrivée dans la plaine de Jaffa par une brutale averse née de la mer, venue mourir sur les reliefs orientaux. Ils avaient donc terminé leur voyage trempés comme des soupes, refroidis par le vent du large. Lorsqu'ils se présentèrent au palais, on leur indiqua que l'évêque n'était pas dans les murs, mais qu'il les recevraient dès que ça lui serait possible. Ils eurent ainsi tout le temps de se sécher et de changer de vêtements. Signe de la faveur dont ils jouissaient désormais, un jeune serviteur leur proposa d'étendre leurs effets auprès des feux de la cuisine.

Ils étaient donc pleinement remis des aléas de leur chemin quand Constantin les convoqua. On les mena jusqu'à sa chambre, où il se trouvait installé à une table de travail lorsqu'ils pénétrèrent. Constantin était occupé à examiner différents documents, l'air concentré, tandis que son petit singe grignotait quelques amandes et pistaches dans un bol en bordure de plateau. À leur arrivée, celui-ci bondit en direction du lit dans les replis duquel il disparut.

Bien qu'il les ait vus entrer et salués d'un signe de tête, le prélat ne leur lâcha pas un mot. Il paraissait fort absorbé dans sa tâche et notait de temps à autre d'un trait rapide quelques éléments sur une tablette de cire ou un papier. Puis il reposa le tout, apparemment satisfait d'avoir achevé une importante mission. Sa mine se grisa bien vite lorsqu'il s'adressa à eux, impatient de savoir pourquoi ils revenaient, peut-être avec une nouvelle déplaisante.

« Soyez assuré, sire évêque, que nous n'apportons céans rien de neuf, bien le rebours.

--- Que me dépêche-t-on messager sans message ?

--- Il se trouve que l'hostel du roi pense qu'il me faudrait étudier la chose plus avant pour que se fasse jour le nom du mystérieux ordonnateur de tout cela.

--- Cela n'était donc pas le service du roi qui motivait ces travaux ? »

En l'exprimant, l'évêque affichait clairement sa déception à voir ses espoirs contrariés.

« Il n'est que de le confirmer, sire. Rien ne m'a été confié, je ne suis là que pour finir de sentir la trace, tel bon mâtin.

--- Et comment comptes-tu t'y prendre ? J'aspirais à ce que tout cela soit enfoui bien loin en nos cœurs et ne pèse désormais plus sur nos âmes.

--- Il n'y aura guère embarras pour vous, sire Constantin. Le sénéchal a bien insisté sur ce point. Il me faudrait surtout avoir accès de nouvel aux affaires du chanoine, ainsi qu'à celles du père Gonteux, si vous en êtes d'accord.

--- J'espérais me débarrasser au plus vite de ces impies travaux, mais tu as de la chance, je n'en ai pas eu le temps, ayant fort importante tâche en ces jours. »

Il désignait la table encombrée de documents, dont Ernaut se demandait s'ils concernaient son fief ou son évêché, voire les deux. Le jeune sergent se dit qu'il devrait peut-être mettre un peu de baume aux plaies du vieil homme, qui pourrait lui en savoir gré.

« Mon sire le sénéchal a aussi indiqué que nous devions nous assurer que rien du secret du chanoine ne transpire, de veiller à ce que rien n'en demeure.

--- Un autodafé ? Ce n'est guère discret ! Je pensais plutôt enfermer tout sous bonne garde, à charge pour un pieux pasteur d'en connaître le contenu pour ne pas en laisser le mal se répandre.

--- Il se peut que l'hostel le roi puisse disposer de tout cela, si vous y concédez. »

À la proposition, l'évêque souleva un sourcil fort étonné et y réfléchit quelques instants, avant de secouer le menton énergiquement, faisant trembler ses bajoues.

« Il s'agit là de savoir de clerc et il n'en est de meilleur garde que cléricale. Son danger ne touche pas seulement les corps, mais aussi les âmes. Il est de mon devoir de n'en pas disposer avec légèreté. »

Ernaut avait espéré un temps s'acquitter aisément de sa mission de collecte, s'assurant un succès facile qui lui aurait peut-être valu encore plus de louanges. Raoul en serait quitte pour recopier ce qu'il en comprendrait.

« Ne dites rien de ce point à quiconque des chanoines même. Nous évoquerons le fait demain et je vous dirai ce que nous aurons convenu avec le doyen. Tout cela est son affaire, aussi. C'est à lui que revient de s'occuper des affaires du père Waulsort.

--- À ce propos, en sait-on plus sur ses héritiers ?

--- Non, il était arrivé en Outremer jeune clerc, après avoir parcouru de nombreux pays. Il était de bonne famille, mais lointaine. Je n'ai pas connaissance qu'il ait jamais reçu un sien depuis qu'il était chanoine. Mais il écrivait beaucoup.

--- Le clerc des Alemans que vous aviez évoqué, parce que récemment rappelé à Dieu, était-il de sa parentèle ?

--- Certes non ! Le père Wilbald de Stavelot appartient au monde des princes et des barons, tout au rebours de ce pauvre Waulsort, simple clerc sans puissantes attaches. »

Ernaut hésita à poursuivre sur le sujet, puis renonça, craignant qu'il ne soit trop délicat à aborder. Mais comme l'évêque aperçut son atermoiement, ce dernier l'invita à formuler son interrogation à voix haute.

« Je me demandais si vous aviez appris d'autres choses sur cet éminent prélat. Quoi que ce soit qui pourrait avoir partie liée à... l'histoire qui s'est déroulée ici.

--- Le pays des Alemans est bien loin d'ici, il nous faudra des mois avant d'en savoir plus, mon garçon. Et tu l'apprendras peut-être avant que je n'en sache rien, vu que tu appartiens à l'hostel du roi et que Baudoin passe la Noël avec l'empereur des Griffons, dont on sait qu'il est toujours fort sourcilleux des Alemans. »

Découvrant le front perplexe d'Ernaut, l'évêque adopta un ton professoral, familier qu'il était des sermons, leçons et édification des fidèles.

« Manuel, actuellement à la tête des Griffons, a forte partie avec le sire de Sicile, qui se verrait bien tailler nouveaux fiefs en ses rivages voisins de Grèce. Et comme ce dernier a souvent maille à partir avec le roi des Alemans, il n'est pas étonnant qu'il soit toujours à guetter les mouvements et atermoiements du Nord. C'est là connaissance fort élémentaire pour quiconque a eu, comme moi, l'honneur de fréquenter la curie romaine. »

Considérant les deux jeunes sergents face à lui, l'évêque se dit qu'il ne serait pas mauvais qu'ils rapportent de lui et de son pouvoir une image plutôt flatteuse. Cela pourrait servir ses intérêts dans la cause dont il espérait que le roi et la haute Cour trancheraient à son avantage au détriment du seigneur de Rama. Il leur conta donc, de façon assez précise le voyage qu'il avait fait auprès du pape avec le patriarche précédent, Foucher d'Angoulême.

Pour n'en comprendre peut-être pas tous les détails, et assez mal le paysage politique que cela représentait, Ernaut s'y forgea une vision beaucoup moins monolithique des clercs que celle qu'il avait développée jusque là. Il découvrait que, pas plus que chez les barons et les rois, il n'y avait d'unité. Si le Pape estimait qu'il était le plus éminent représentant de Dieu sur terre, ses aspirations étaient régulièrement contestées par d'autres ecclésiastiques. En outre, par le fait qu'il soit aussi un seigneur temporel ayant de nombreux biens et domaines à gérer et défendre, il se trouvait doublement impliqué dans les intrigues européennes au plus haut niveau.

La vision lui donna un peu le tournis, tout en l'assurant de la puissance et de l'importance de l'homme qui lui narrait anecdotes et précisions diplomatiques avec un art consommé de la formule. Pour être astucieux et armé d'une solide répartie, Ernaut ne pouvait nullement rivaliser avec un vieillard expérimenté, formé aux meilleures écoles de rhétorique, habitué à forger des discours dès l'âge où les chevaliers commencent à monter en selle. Il en conçut un peu de dépit, découvrant soudain la béance de son ignorance sur le sujet.

Alors qu'il était parti dans des circonvolutions sans fin, l'évêque fut interrompu par le son clair des cloches. Un peu noyé dans la logorrhée et agacé de voir son inculture pareillement révélée, Ernaut espérait que l'annonce d'un office allait mettre un terme au pénible énoncé. Pourtant Constantin poursuivit encore un moment, n'ayant été que peu troublé par l'appel à la prière et ne manifestant aucune intention de s'y rendre. Lorsqu'il acheva enfin son exposé, il s'offrit une longue rasade. Estimant que l'essentiel était dit, il leur donna congé et se replongea dans l'étude de ses documents.

Lorsqu'ils sortirent du bâtiment, retournant vers la petite cellule qu'ils s'étaient vu affecter, Ernaut se tourna vers Raoul.

« Qu'apenses-tu de tout ce qu'il nous a dit à propos des Alemans et des Griffons ? »

Le jeune scribe se mit à rire.

« J'en dis que comme tous les prélats que l'on encontre à la Haute Cour, il n'a de cesse qu'il n'ait mis dix mots là où un seul aurait suffi.

--- Il nous a brossé fort importante fresque de ce qui pourrait se tramer en derrière de nos affaires ?

--- Je n'en saurais dire, il m'est apparu qu'il se trouve face à épineux problème. Il retrouve alors ses habitudes cléricales, à gloser sans fin sur chaque petite chose. Le discours remplace souventes fois l'action, voire la décision chez ces animaux-là. »

Il se figea, stoppant son compagnon d'un geste sur le bras.

« Par contre, il a semé scintillante gemme en son bourbier. Y as-tu pris garde ?

--- Je t'avoue que je n'en retiens que salmigondis.

--- Haha. Quand tu auras comme moi l'oreille exercée à la parlature des gens de Cour, tu sauras y trier bon grain de l'ivraie.

--- Et en nos histoires, qu'en gardes-tu ?

--- Le grand abbé dont Waulsort était fort ami, est mort alors qu'il s'en revenait de chez les Griffons. Et de si étrange façon qu'on a parlé d'empoisonnement. »

Ernaut écarquilla les yeux.

« Il a dit ça ?

--- Pas en termes aussi clairs, mais on peut en comprendre cela.

--- Mais c'est là information de grande importance. Si on a poisonné l'abbé, on aura peut-être cherché à éliminer son ami pour identiques raisons.

--- Tout à fait. Mais l'évêque ne semble pas croire cela, il en a parlé avec grande légèreté. Et ne s'en soucie guère.

--- Il nous a pourtant narré fort longuement ce qu'il en était entre les royaumes. »

Raoul s'esclaffa et secoua la tête.

« Il a déclamé prou, mais sans vraiment y pourpenser. Tout le temps que nous y étions, il n'avait d'yeux que pour ses tablettes et documents.

--- Il s'y trouve peut-être quelque information d'importance en notre affaire.

--- Absolument pas. J'ai essayé de lire, cela ne concernait que des chameaux !

--- Des chameaux ? De quoi me parles-tu là ?

--- Tu ne sais donc pas ? C'est de bonne fame pourtant : l'évêque de Lydda est fort épris de ces animaux, dont il organise des courses avec les plus belles bêtes qu'il achète aux nomades ou fait venir à prix d'or des sables du sud. »

Ernaut éclata de rire. L'évêque était plus enclin à s'adonner à son inoffensive marotte qu'à se plonger dans leur enquête. Ils pourraient donc fouiller tout à loisir, tant qu'ils ne heurteraient pas son autorité. Il tapa dans le dos de son compagnon, l'invitant à se laver les mains. Avec un peu de chance, le repas du soir n'allait pas tarder à être servi. Il se sentait un appétit d'ogre après cette mauvaise journée à se mouiller en selle.

### Lydda, palais épiscopal, fin de matinée du dimanche 28 décembre 1158

Sachant qu'ils ne pourraient rien faire sans obtenir l'aval préalable du chapitre canonial, Ernaut avait flemmardé au lit, n'en sortant que poussé par la faim. Lorsqu'il avait ouvert les yeux, Raoul était déjà levé et avait déserté la chambre. Il prit malgré tout son temps, profitant de la moindre activité dans les lieux, maintenant que les fêtes étaient passées et avant que le chantier ne redémarre. Il avait retrouvé Raoul pour la messe, à laquelle ils avaient assisté tous les deux. Ils s'étaient ensuite installés dans le cloître, au cas où les chanoines auraient eu le désir de les interroger sur l'enquête en cours.

Ne voyant rien venir, Ernaut proposa à Raoul d'attendre là un moment tandis qu'il allait faire un petit tour. Il n'aimait pas particulièrement rester sans rien faire et préférait sentir ses jambes fonctionner plutôt qu'exercer sa patience. Il se dirigea donc vers le cimetière, pour y faire une courte prière pour Herbelot, puis baguenauda dans les jardins. Nul valet ne s'y activait, à la fois parce que c'était dimanche et parce que, pendant les célébrations, beaucoup avaient obtenu l'autorisation de s'absenter. L'endroit était particulièrement tranquille, une brise légère caressant les branches. Ernaut appréciait le climat très doux du royaume durant l'hiver, en particulier vers la côte.

Lorsqu'il repassa vers les communs, il aperçut de nombreux domestiques qui semblaient revenir de loin, encore équipés de leurs tenues de voyages et, pour certains, une balle ou une hotte sur le dos. Il eut soudain l'idée de s'enquérir de ceux qui avaient débarrassé les affaires de Waulsort. On lui indiqua un des valets, présentement occupé à un petit jardin qu'il entretenait, visiblement par plaisir autant que pour les légumes qu'il en tirait. En fait de lopin, Ernaut découvrit un carré fort bien soigné, même s'il était modeste, calé entre la muraille et les bâtiments de service.

L'homme ne semblait pas très âgé, mais, courbé avant son temps, il avait le visage fatigué et les paumes crevassées du travailleur manuel. Ses cheveux, plus sel que poivre, étaient assez longs et plutôt négligés, comme sa personne. Il avait lacé une vieille pièce d'étoffe en guise de tablier et s'activait à biner la terre d'une petite zone avec une houe de bois. Il suspendit son geste en voyant arriver l'imposant gabarit d'Ernaut, l'accueillit d'un sourire en partie édenté.

« La bonne journée. Tu es bien Bertaut ?

--- Ça s'pourrait bien. T'y veux quoi ?

--- On m'a dit que tu as aidé à vider la maison de Waulsort. Est-ce bien vrai ?

--- P'têt bein, et pouquoi qu'tu veux le savoir, jeune ?

--- Un de mes amis lui avait prêté un ouvrage d'importance et souhaiterait le récupérer...

--- Hou là, moi j'en sais rien, j'ai porté sacs et paniers, j'en sais pas plus. Vois donc avec not' sire Constantin.

--- Justement, il m'a permis de voir ce qui était dans la cellule, je ne l'y ai pas vu. Peut-être l'a-t-on mis ailleurs ? Pourrais-tu m'en conter sur comment ça s'est passé ce jour là ? »

Bertaut s'appuya sur sa houe, se gratta le nez et envisagea le géant face à lui. Peut-être était-il plus finaud qu'il n'en avait l'air, ce qui ne constituait pas un vrai exploit vu sa piètre allure.

« Moi j'y sais rien des livres et rouleaux. J'ai même pas touché à ces affaires-là ! J'ai surtout nettoyé. On m'file toujours les sales besognes. C'est moi qu'ai porté l'eau pour nettoyer le sang...

--- Le sang ? Ne put retenir Ernaut, surpris par la remarque.

--- Oui, et alors que des plus jeunes étaient là, qui...

--- On m'a narré que le pauvre chanoine avait été brûlé !

--- Ça, de sûr ! Mais y avait aussi du sang, tout brûlé qu'on aurait dit du boudin. Je connais bien cette affaire, à avoir prou tué le cochon. »

Ernaut était complètement pris au dépourvu par l'affirmation. Jamais personne n'avait mentionné le fait qu'il y avait du sang.

« Tu es certain de toi ?

--- Un peu, oï ! On a vite frotté, que les clercs qui v'naient voir les écrits ne salissent pas leurs souliers. Tout était rendu propre avant qu'y z'entrent. Mais y'avait du sang, aucun doute ! »

Il prit un air matois, comme s'il avait une remarque d'importance à ajouter, se pencha un peu vers Ernaut et continua, un ton de conspirateur dans la voix.

« Si ça se trouve, le père Waulsort, il aura fait v'nir un dragon, du genre de not bon saint Georges. Et si c'te bête, elle crache du feu, c'est p't'être pour préparer sa mangeaille. En les croquant, elle aura fait jaillir le sang. »

L'information était capitale pour Ernaut. Il ne croyait pas un instant à l'idée formulée par Bertaut, même s'il l'écouta avec un sourire d'agrément. Il demeurait toujours possible que l'accident qui avait gravement brûlé les deux hommes ait par ailleurs causé des blessures, mais si du sang avait coulé en assez grande quantité, cela pouvait aussi indiquer que des violences leur avaient été faites.

Pris de vertige à cette éventualité, Ernaut se mit à échafauder des hypothèses folles. Il lui fallait absolument éclaircir ses idées, peut-être en échangeant avec Raoul. Si Waulsort avait été tué par meurtre, l'incendie avait très bien pu servir à camoufler certaines choses. Il était quasiment certain que ce n'était pas un feu normal. Qui donc aurait intérêt à inventer pareille mise en scène ? S'il n'était si affolé par la moindre agitation dans ses terres, Ernaut aurait bien pensé qu'un ecclésiastique comme Constantin était tout désigné. Pour avoir grandi sous la férule de prêtres assez peu enclins au dialogue, jusqu'à engendrer la violence, Ernaut avait des hommes d'Église une image plutôt négative.

Il demeurait que c'était Constantin qui avait appelé à l'aide et il était catastrophé à l'idée que la mort de Herbelot puisse n'être pas accidentelle. Était-ce la culpabilité d'un intrigant qui voyait les choses lui échapper ? De toute façon, même s'il était à l'origine de l'histoire, ce n'était certainement pas lui qui avait opéré nuitamment. Tout le monde connaissait sa silhouette dans les environs, sans compter qu'il n'était pas du genre à aller faire quoi que ce soit directement. Il était habitué à être servi.

Se pouvait-il qu'Aymar et sa bande soit ceux qui auraient exécuté les desseins d'un prélat ambitieux ? Nul à la cour, à Jérusalem n'ignorait combien Constantin de Lydda était âpre et déterminé dans les questions de préséance qui l'opposaient aux Ibelins, seigneurs de Rama.

Ernaut prit conscience de l'importance de la récolte des informations matérielles sur les circonstances d'une telle mort. Régnier d'Eaucourt lui avait montré la voie, sur le *Falconus*[^33] et cela lui avait alors semblé d'évidence. Mais il avait failli à cette règle de collecter tout ce qui était possible du lieu même où les choses s'étaient déroulées. Il se résolut de ne plus tomber dans ce travers si le cas devait se représenter. Il lui faudrait étudier de ses yeux l'endroit, comme ils l'avaient fait avec Herbelot et Régnier, ou, à défaut se contenter de questionner précisément les personnes qui en avaient bouleversé l'ordonnancement. Il y avait une histoire à reconstruire derrière les vestiges demeurés là et, sans un glanage habile, il n'en pourrait moudre aucune farine.

Voyant son interlocuteur perdu dans ses pensées, Bertaut avait repris sa tâche de désherbage. Ernaut remercia du bout des lèvres le valet et retourna vers le bâtiment où il espérait retrouver Raoul. Il continuait de recenser les choses dont il estimait jusque là qu'elles étaient peu ou prou établies, pour les remettre dans la balance de son jugement. Quoi qu'il en soit, il demeurait une inconnue qu'il faudrait découvrir : la façon dont ce feu était apparu et s'il s'agissait bien d'une mystérieuse substance née de l'imagination d'un érudit inventif. Car cela pouvait tout aussi bien être un banal accident qu'avoir servi à camoufler un sordide assassinat. Et préluder à la mort de Herbelot, qui en devenait suspecte par la même occasion.

Il retrouva Raoul installé sur un des murets du cloître, affairé à aiguiser un petit canif qui ne le quittait jamais. Ernaut se moquait souvent de ne le voir jamais le sortir lorsqu'ils avaient besoin de couper, refusant d'admettre qu'en entamer le fil le rendrait moins efficace pour tailler les plumes avec lesquelles Raoul s'enorgueillissait de noircir les feuilles.

Ernaut l'interrompit, chuchotant plus qu'il n'était nécessaire, inquiet d'éveiller l'intérêt d'éventuels curieux. Personne n'était aux alentours, sauf un jardinier en train de soigner de petites haies de buis.

« Je viens d'apprendre une fort importante chose en notre affaire, Raoul. Le chanoine et son valet ont sûrement été meurtris de violente façon.

--- Que chantes-tu là ? N'avons-nous pas convenu que c'était accident, fort dommageable, mais simple accident néanmoins.

--- Sauf qu'un des valets qui ont vidé la demeure m'a affirmé en avoir nettoyé le sang qui s'y trouvait au sol.

--- C'est peut-être arrivé durant l'accident, qu'en savons-nous ? En as-tu appris des détails, de ce valet ?

--- Je n'ai guère eu le désir de trop attirer sa curiosité sur moi. Nous ne sommes pas censés parler de ceci à quiconque et, outre, l'affaire est entendue, du moins pour l'évêque. S'il apprend que nous musardons en posant délicates questions, son ire risque d'être terrible. Il nous faut demeurer plus cois que pierre en tout cela. »

Raoul hocha le menton. Il était d'une discrétion à toute épreuve, peu habitué à jouer un rôle actif. Il lui convenait parfaitement d'être tenu à la marge, où il savait qu'il était moins remarqué. Se prendre pour un enquêteur l'amusait, mais il ne voulait pas pour autant trop s'exposer et s'estimait pleinement satisfait de voir qu'Ernaut se comportait comme s'il était le meneur de leur petite troupe. Son statut n'était pourtant clairement pas d'importance par rapport à un scribe de la Cour.

« Une fois que le sire évêque nous aura donné son accord pour accomplir nos tâches, je te propose d'aller commencer à recopier tout ce qui te semblera utile dans les documents. Pendant ce temps, je vais aller à la pêche aux nouvelles en ce qui concerne les mauvais garçons qui se terraient dans leur hostel en ville. Il nous faut en savoir plus, sinon nous risquons de prendre folle décision.

--- D'autant que le sénéchal du comte de Jaffa me fait l'effet de ne guère apprécier mauvais valet.

--- Certes. Pas plus que le sire Constantin de voir son autorité prise en défaut. Je crois bien que nous avons été placés entre martel et enclume, mon pauvre. »

Ils sourirent tous deux, sans être vraiment rassurés. De simples sergents comme ils l'étaient ne seraient que de peu d'importance si des décisions devaient être prises d'un côté ou l'autre pour régler les problèmes soulevés par cette histoire. Si le pape, le basileus byzantin, le roi des Allemands et la couronne de Jérusalem devaient s'affronter, ce ne seraient pas barons qui seraient sacrifiés en premier. Aux échecs, le pion ne sert-il pas souvent qu'à se faire tuer pour permettre à son seigneur de triompher ?

« Il va te falloir mettre grande hâte à grapper tout ce que tu pourras dans les textes du chanoine. Au moins ne serons-nous pas bredouilles si d'aventure les choses se gâtaient pour nous. Pensons avant tout à bien servir les intérêts de la couronne. C'est à elle que nous avons juré, après tout. Et prends bien garde à tout ce qui pourrait évoquer poudre de lune ou escarboucle. »

### Lydda, quartier de la porte de Samarie, après-midi du dimanche 28 décembre 1158

Les boutiques étant fermées, la ville semblait assoupie, malgré les voix s'échappant des cours intérieures et des jardins. On ne voyait pas grand monde dans les rues, un temps gris incitant chacun à demeurer chez soi afin d'éviter une éventuelle averse. Bien emmitouflé, Ernaut parcourait les allées et venelles pour tenter de retrouver Hashim. Au moment de quitter Lydda, il l'avait remercié et grassement dédommagé de ses efforts, et il espérait pouvoir de nouveau avoir recours à ses services, ou profiter de sa mémoire. Il avait frappé en vain à l'endroit où Hashim résidait avec sa famille. Il préférait perdre du temps à le dénicher avant de faire quoi que ce soit avec Aymar et ses compagnons, afin d'avoir tous les renseignements utiles. Il n'était même plus sûr que ceux-ci soient encore présents, leur tâche étant probablement terminée.

Malgré son acharnement, il n'arriva à rien et ne parvint qu'à être transpirant à force d'agitation sans aucun résultat. Ce genre de situation lui portait tout particulièrement sur les nerfs et avait tendance à le faire réagir sottement, sautant sur le premier prétexte qui permette de sortir de ce statu quo. Pestant après Hashim qui n'était jamais présent quand on avait besoin de lui, il retourna une nouvelle fois vers la petite place où ses espions avaient coutume de s'installer pour faire la vigie. Et, de là, se rendit finalement vers le bâtiment qu'occupait Aymar.

L'écurie de louage était ouverte et un domestique au turban brinquebalant s'employait à en balayer le passage d'entrée. Il n'y avait nul jour férié pour qui avait des animaux. En les mettant à son service, on devenait aussi leur esclave, enchaîné plus sûrement que par le plus solide des liens. Lorsqu'Ernaut approcha, l'homme le salua poliment, sans interrompre sa tâche. Il était pieds nus, avec un thawb maintes fois reprisé et un morceau d'étoffe serré à la taille. Malgré cette apparence négligée, il s'était rasé peu de jours auparavant et n'avait pas l'air misérable. Peut-être réservait-il de vieilles hardes aux travaux salissants. Prenant Ernaut pour un client, il l'accueillit comme tel et lui conseilla d'attendre le retour de son maître s'il souhaitait une monture ou des animaux de bât. Il avait un accent local terrible, mais s'exprimait sans hésitation dans la langue des nouveaux seigneurs. Ernaut le détrompa sur ses intentions et lui demanda s'il savait si les gens qui demeuraient au-dessus étaient encore là.

« Partis voilà une paire de jours. C'est des compères à vous ?

--- Peu ou prou. Peut-être ferons-nous affaire, mais je préfère en savoir plus dès avant.

--- Je ne saurais dire s'ils ont bonne fame ni me porter garant. Pas plus que d'aucuns à l'entour. J'espère que vous n'aurez pas mauvaise surprise.

--- La merci à toi, compère. En sais-tu long à leur propos ?

--- Ils ne sont pas fort causants, ne sont pas d'ici. Ils viennent qui cy qui là depuis long temps. Peut-être reviendront-ils ? Les gens de cette importance vont et viennent tels des nomades.

--- Tu disais ne les connaître point.

--- Si fait. Mais c'étaient des hommes de qualité ou servant un riche maître. J'ai usage de voir les animaux, leurs montures étaient bien soignées et ils sont repartis avec. Ce n'étaient donc pas de simples louages, auquel cas les gens nous laissent parfois une bête ou l'autre, préférant en changer malgré la dépense en plus. »

Malgré son apparence fruste, l'homme était observateur et s'exprimait de façon bien savante pour un simple commis d'écurie. Cela lui fit penser à Abdul Yasu, son ami loueur de bêtes à Jérusalem. Avoir un ribaud avec les yeux bien ouverts en chacun de ces endroits garantissait de nombreuses informations sur les allées et venues. Comme Hashim, ils étaient discrets et se fondaient dans le paysage, mais surveillaient tout se qui se passait. Sans compter que, parfois, ils étaient dotés d'un esprit vif. Là aussi se trouvait une source utile et profitable à exploiter si, d'aventure, on lui confiait une nouvelle enquête à l'avenir.

« Sais-tu à qui appartient la maison où ils demeuraient ?

--- Oui, à mon maître. Mais il ne t'en dira rien, car il ne s'en occupe guère. C'est moi qui leur baille la clef. »

Remerciant chaleureusement le valet pour son aide, Ernaut le gratifia d'une petite pièce, ce dont il fut repayé par une pluie de bénédictions. Au moins aurait-il un allié dans la place au cas où ces hommes reviendraient.

Il retrouva ensuite le chemin du palais. À moins qu'il ne remette la main sur Hashim et que ce dernier ait continué sa surveillance par curiosité, il n'avait plus guère de moyens pour remonter la piste du seigneur d'Aymar. Du moins pas sans se dévoiler complètement et prendre le risque de contrarier ou d'alerter une des puissances impliquées dans cette histoire. Il lui fallait se fier à Raoul et à sa science des textes pour y pêcher d'utiles indices. Et si jamais certains documents pouvaient être discrètement escamotés pour les délivrer au sénéchal par la même occasion, Ernaut n'hésiterait pas.

Si les secrets sur lesquels Waulsort travaillait étaient d'une telle importance, ils seraient en de bonnes mains si la couronne en disposait. Et ce pourrait être l'occasion d'être évoqué auprès de Baudoin comme le champion qui avait découvert l'histoire, voire de lui être directement présenté. Dans ses rêveries en marchant, Ernaut se voyait accolé par le roi en présence de la Haute Cour. Il se doutait néanmoins que, pour être essentiels, de pareils agissements ne pouvaient être de ceux qu'on célébrait en public avec faste. Il appartenait aux hommes des basses œuvres, qu'on félicite du bout des lèvres sans trop en dire.

Alors qu'il passait le portail donnant accès à la forteresse Saint-Georges, il salua les sergents de faction, le sourire aux lèvres. Il venait de réaliser qu'il lui faudrait remercier le connétable d'avoir manifesté son désaccord à ce qu'on dispose de *ses* chevaliers sans l'avoir consulté au préalable. Il se rendit sans même y penser au cloître. Lorsqu'il vit que que Raoul avait disparu, il le traversa en rapides enjambées pour le retrouver dans la cellule où les documents étaient entassés.

Le scribe l'accueillit d'un sourire, mais sans un mot, plongé dans une pile de feuillets, tablettes et rouleaux. Ernaut eut un petit pincement au cœur d'être ainsi écarté de ces importantes recherches en raison de son illettrisme. Peut-être devrait-il voir avec un clerc de la cité s'il ne pourrait pas reprendre ses études sur ce point ? Sans parler de *quadrivium* ou de *trivium*, savoir manier l'écriture aussi bien que les chiffres lui serait d'un grand avantage. Ce serait en outre l'occasion de briller aux yeux de Libourc, qui compensait son incapacité à lire par une excellente mémoire auditive.

Chapitre 7
----------

### Lydda, palais épiscopal, fin d'après-midi du dimanche 28 décembre 1158

Raoul lui expliqua en quelques mots qu'ils avaient obtenu l'aval des ecclésiastiques pour copier ce qui pouvait être utile au roi, mais sans pouvoir rien emporter. Ernaut ne resta pas longtemps avec lui, comprenant que s'exciter comme un lion en cage ne lui servirait à rien et risquait de déranger son compagnon, déjà inquiet de l'ampleur de la tâche. Il estima qu'il devait profiter de ce délai d'attente forcée pour reprendre les éléments qu'il avait amassés jusque là. Peut-être qu'une trame en naîtrait. Il se connaissait assez pour savoir qu'il ne pourrait digérer l'information en demeurant inactif. Il avait besoin d'air et d'espace pour se sentir à l'aise et laisser son cerveau procéder.

Il traversa de nouveau la cour pour se rendre au bâtiment de l'administration civile. Les portes en étaient fermées, bien sûr, mais le toit-terrasse formant un excellent point de vigie, un sergent y était de faction. Ernaut le héla et lui demanda la permission de monter. L'homme fut tout d'abord réticent, bien qu'il ait vu Ernaut en compagnie du vicomte. La promesse de quelques pièces pour envoyer un gamin leur quérir un pichet si une taverne se trouvait ouverte lui fit bonne impression et le décida à accueillir Ernaut.

Il se nommait Anquetin et faisait commerce de sangles et baudriers de cuir. Il servait là comme membre du guet. Il n'avait guère dépassé la vingtaine, arborait un visage lourd et un regard ébahi qui lui conféraient un air simplet. Vêtu d'une vieille cotte de laine et d'une chape épaisse, il semblait assez prospère comme artisan, ce qui incitait à la confiance. Il accueillit Ernaut aimablement, ainsi qu'il l'aurait fait d'un habituel compagnon. Il était sur le fond assez content de ne pas demeurer seul, ayant la tâche de guetter jusqu'à complies sonnées.

« Je suis bien aise d'avoir compère, pour dire le vrai. Nous avions usage d'être deux à espier ici, mais avec les nouveaux murs qui se montent partout, nous ne suffisons plus guère à la tâche. »

Il fit à Ernaut une visite en règle des environs, qu'ils voyaient jusque fort loin. Au sud, par-delà l'émeraude des jardins, vergers et palmeraies se trouvait la cité de Rama. Anquetin lui expliqua qu'il était fréquent que des festivités prennent place dans l'oasis, mais que les anciennes foires avec les nomades s'y tenaient moins souvent que par le passé. La faute au seigneur de Rama qui en contestait le contrôle à l'évêque.

Il y avait assisté à un tournoi, un jour, organisé par quelques croisés. Il avait entendu parler de cette habitude des barons venus d'outremer, mais il ne comprenait pas bien l'intérêt qu'il y avait à se battre entre chrétiens alors qu'il y demeurait tant à faire pour éradiquer les païens. Il avait néanmoins beaucoup goûté le spectacle, profitant de l'absence de danger à voir ces farouches combattants s'affronter.

« L'un d'eux, seigneur de Miremont je crois, était tellement habile à faire danser sa monture, et si solide qu'il mit bas deux adversaires d'une poingnée, en les prenant par le travers. Il fallait voir comme ils y allaient, pointant leurs hampes de frêne en tout sens. Si des païens étaient là, ils ont dû en avoir grande peur à voir si grande hardiesse. »

Il envisagea Ernaut d'un coup d'œil, sourire aux lèvres.

« Tu aurais beau jeu, vu ta membrature, à t'esbattre avec eux. Une gifle là, un horion ici, la distribution serait aisée !

--- Tous ces beaux barons ont grande sapience de la guerre, je ne suis pas certain que je pourrais les battre » objecta Ernaut, faussement modeste.

Anquetin se contenta de hausser les épaules et se pencha tout soudain du côté de la ville, apostrophant un gamin qu'il connaissait pour quérir de lui s'il pouvait aller chercher cabaretier.

« La ville est toujours fort calme en ces jours. Le frimas y est le plus fort alors chacun s'enferme dans sa chacunière maintenant que labours et semailles sont achevés pour le blanc[^34]. On m'a dit que tu étais céans au service le roi ?

--- Si fait. Il est possible que le roi fasse débours au profit du sanctuaire.

--- Ce serait belle nouvelle pour le sire évêque. Un tel geste du roi marquerait nettement sa préférence dans la querelle contre le sire de Rama.

--- J'ai aussi pour devoir d'encontrer les maîtres de l'œuvre. Le roi a toujours usage d'hommes de talent en ses forteresses et murailles. »

À l'ouest, la route qui tirait presque en ligne droite vers Jaffa était soulignée de murets et de jardins, de cultures et de zones de pâturages. Régulièrement, des hameaux s'agglutinaient autour des points d'eau. C'était une des régions les plus prospères du royaume, où le paysan pouvait devenir aisé. Anquetin lui parla de sa famille, installée à Jaffa, où la plupart vivaient de la mer. Lui préférait avoir un sol solide sous les pieds et avait décidé de venir à Lydda, attiré par les offres de taxation légères qu'on y faisait. Il avait fait ce choix à la fin de son apprentissage et s'était marié deux ans plus tôt.

Le levant était barré d'une houle rocheuse qui se brisait sur les hauts reliefs de Judée, noyés dans des brumes grisées. Anquetin lui expliqua qu'il avait visité la cité sainte à de multiples reprises, mais que le climat y était trop froid pour lui. Il avait la neige en profonde aversion et trouvait stupide d'aller se claquemurer en des endroits où, pour passer l'hiver, il fallait brûler tant et plus de bois. Sans même parler des jardins qui y donnaient moins que vers Lydda.

Ils étaient encore en train d'admirer le panorama, qu'Anquetin, finalement très bavard, rendait vivant par les nombreuses anecdotes de sa vie aux alentours, lorsque les cloches sonnèrent pour l'office.

« Vêpres sonnantes. On n'est plus si loin de s'en retourner ! »

Ils virent les hommes de faction au sol verrouiller les deux grands portails, celui donnant sur la ville et celui ouvrant directement dans la plaine. Puis quelques lanternes scintillèrent dans les ombres des postes de garde. La journée étant assez courte en cette période de l'année et Ernaut commençait à trouver le lieu un peu frisquet. Il redescendit, non sans avoir salué amicalement la vigie, qui voulait l'inviter à rompre le pain chez lui une fois son service achevé. Ernaut déclina, ayant un compère à retrouver, expliqua-t-il.

« En ce cas, n'hésite pas à me faire visitance, je fais toujours bon accueil aux sergents le roi. »

La nuit tombait rapidement, étendant un manteau de ténèbres sur le sanctuaire piqué des mille feux des bougies, lampes et chandelles. Bientôt la noirceur serait totale et il faudrait se déplacer au jugé.

Raoul n'avait quasiment pas bougé et Ernaut aurait pu croire qu'il n'avait rien fait, si les documents n'avaient pas migré d'un endroit à l'autre autour de lui. Les yeux plissés à tenter de lire avec la chiche lumière d'une seule mèche, il reposa sa plume en voyant Ernaut entrer.

« À la bonne heure, j'allais me rendre au réfectoire pour le souper, vêpres viennent à peine de sonner.

--- Allons-y de concert, il ne te sert à rien de t'abîmer les yeux. Demain il fera jour.

--- Si nous voulons être de retour à Jérusalem dès avant les Pâques, il ne me faut pas lambiner. Il nous arrive souvent, à la Secrète, de travailler au noir[^35].

--- Je suis acertainé que d'aucuns auraient grand plaisir à voir les cliquailles du roi proposées à tous les vents !

--- Ils auraient surtout grande lassitude à voir rollets et tablettes. »

Tout en parlant, Raoul répartit scrupuleusement les différents documents autour de lui puis il boucha son encrier de corne et essuya délicatement ses instruments. Il avait un petit écritoire et un plumier, symboles de son office, qui ne le quittaient jamais.

« Les hommes qui venaient porter les affaires au chanoine sont partis. Il va nous falloir arpenter sentiers neufs si nous voulons démasquer la personne qui a payé tout cela.

--- Ne pourraient-ils pas être ceux qui auraient meurtri le clerc une fois son office achevé ? Afin de ne laisser aucune trace...

--- Bien le rebours. Ils ont été fort courroucés de voir que les affaires avaient été emportées de la maison. S'ils avaient été assassins, pourquoi ne pas avoir larronné de concert ? Ce pourrait être plutôt parmi leurs ennemis qu'il faudrait chercher.

--- Et ceux-ci s'en seraient pris au clerc de l'archevêque aussi ? »

Ernaut confirma. Selon lui, Waulsort avait été découvert et ses travaux jugés dangereux. Il avait donc été éliminé.

« Attends, si on a dû aussi dépêcher Herbelot, c'est qu'il demeurait une crainte que les choses n'aient pas été arrêtées... Qu'il comprenne et poursuive ou transmette le point où en était rendu le chanoine.

--- Je suis toute la journée durant le nez dans ses écrits, pourquoi rien n'a été tenté contre moi ?

--- Il est bien moins aisé de venir jusqu'à cette petite cellule qu'en ville ou sur le chantier. Outre, il va en effet te falloir être fort prudent et convenir entre nous de signes secrets pour frapper à l'huis.

--- Tu crois vraiment qu'il demeure un risque ?

--- De deux choses l'une : soit le murdrier a détalé comme Aymar, auquel cas nous ne craignons rien, soit il est toujours à rôder aux alentours et il nous faut faire preuve de prudence. J'avoue que je préférerais ce second cas. Dans tous les cas, un peu de méfiance ne saurait nuire.

--- Tu espères qu'il va s'en prendre à nous ? » s'inquiéta Raoul.

Ernaut lui accorda un sourire carnassier en réponse.

« Je ne suis pas petit clerc amolli par l'étude et j'ai déjà frictionné fort avant des marauds qui s'en voulaient me rosser. Qu'il y vienne donc ! Je me fais fort de l'éreinter pour de bon et de le présenter au sénéchal saucissonné comme il sied à un fripon de son acabit ! »

### Lydda, palais épiscopal, matin du lundi 29 décembre 1158

Ragaillardi à l'idée qu'il puisse se défouler sur le fauteur de troubles s'il lui prenait l'envie saugrenue de s'attaquer à lui, Ernaut se présenta tout guilleret au réfectoire. Il engloutit un bon morceau de pain additionné d'une purée de légumes aux oignons, le tout arrosé d'une bière légèrement sucrée. Raoul avait à peine grignoté avant de détaler pour son lieu d'étude. La veille au soir, il avait expliqué qu'il n'avait trouvé aucune mention de pierre de lune, qu'il se contentait pour l'instant de trier les documents dont il était quasiment certain qu'ils n'avaient rien à voir avec leur histoire. Les valets manutentionnaires avaient tout mélangé, entassant papiers et volumens sans prendre garde à leur classement, dont ils n'avaient que faire. Raoul essayait donc de retrouver la logique des choses et, surtout, d'en extraire ce qui était pertinent.

Une fois rassasié, Ernaut se rendit en ville afin de chercher un barbier. Avec les bains, c'était un luxe qu'il aimait s'offrir depuis qu'il était dans le royaume de Jérusalem. Les praticiens étaient nombreux, beaucoup d'hommes appréciant d'avoir la tête rasée, ce qui leur évitait d'être infesté de poux et de puces. Ernaut veillait à se faire tailler la barbe, qu'il tentait de tenir en un état soigné, le vendredi ou le samedi, de façon à se présenter sous son meilleur jour quand il allait visiter Libourc pour la messe dominicale. Il ne tarda pas à découvrir une échoppe où quelques chalands âgés discutaient, accueillant les clients comme s'ils étaient de leur parentèle. Le petit homme agité qui virevolta autour de lui muni d'un rasoir bavardait dans la langue locale avec de grands hochements de tête. Ernaut subodora que là comme partout, il était question surtout de météo et des histoires de voisinage.

Alors qu'il revenait, il eut soudain une idée en franchissant le portail. Il prit conscience que les soldats de faction connaissaient son visage et ne l'interceptaient plus quand il allait et venait. Si, comme il le croyait désormais, Herbelot avait été assassiné, son meurtrier avait dû passer une des deux entrées. Même si le mur n'en était guère vaillant, délimitant l'espace plus qu'agissant comme une véritable fortification, il était difficile de s'introduire dans le lieu sans être vu par un garde, à moins d'avoir un complice.

Il dirigea ses pas vers le bâtiment de la Cour, où le vicomte donnait séance, lui indiqua-t-on à son arrivée. Lorsqu'Ernaut pénétra dans la salle d'audience, Maugier du Toron plissa les yeux sans mot dire, tout à l'écoute d'une plaidoirie. Deux jurés représentaient les intérêts de commerçants opposés dans une histoire d'accès à un puits, semblait-il. Les affaires habituelles continuaient, nota Ernaut.

Les échanges furent un temps assez vifs lors des cas suivants, où il était question de punir d'un sévice corporel un serviteur qui avait déserté son travail. Affecté entre autres à l'entretien de parcelles d'olivier, il avait disparu au moment de la récolte. Bien qu'il fût revenu de lui-même, son maître exigeait qu'on lui perçât le poing en signe d'infamie. Le contrevenant était accompagné de témoins qui juraient que cela ne s'était pas fait de son chef, mais qu'il avait été envoyé en pèlerinage forcé pour des raisons que son confesseur seul connaissait. Ledit confesseur était absent, ayant estimé qu'il n'avait aucun compte à rendre de sa pratique religieuse à une justice bourgeoise.

Installé au fond, Ernaut n'écoutait que distraitement. Il lui arrivait d'être commis à la salle d'audience à Jérusalem et il avait appris à ne pas se sentir trop concerné par ce qui se disait. Il regrettait juste qu'il n'y ait nul jardin à admirer le temps que les débats cessent, alors il somnola sur son banc, bras croisés et jambes tendues. Il sursauta quasiment quand la voix du vicomte le héla. Il n'avait pas pris conscience que la pièce s'était vidée et qu'il ne demeurait que Maugier et un clerc qui se grattait la tête en comparant différents documents, sur une table vers l'entrée.

Peu désireux de claironner ce qu'il souhaitait dire, Ernaut s'approcha et demanda à discuter de façon plus discrète. Maugier lui proposa de l'accompagner dans une déambulation autour du palais. Ils feraient mine de visiter le chantier afin de décevoir les curieux. À peine eurent-ils mis le pied dehors que, d'un regard, le vicomte invita Ernaut à s'exprimer.

« Le sire évêque vous a sûrement dit que nous devons derechef nous mettre en chasse de ceux qui ont tramé toute cette histoire depuis l'ombre. J'ai pourpensé à cela et je me demandais depuis quand vous avez enclos si fortement le palais.

--- Oh, voilà bien ancienne muraille. Nous l'avons relevée, certes, ces dernières années. Mais nos glorieux prédécesseurs eux-mêmes l'avaient établie quand ils marchaient sur Jérusalem. Pourquoi cette question ?

--- Si personne ne peut entrer fors les portes, je me disais que peut-être un des sergents aurait remarqué choses étranges les jours où le chanoine ou le père Gonteux rendirent leurs âmes à Dieu.

--- Je n'en ai aucun souvenir. Et ce serait délicat de poser la question, ce sont des hommes du guet qui s'acquittent de cette tâche pour la porte de ville. Leur parler reviendrait à crier en place publique ce qui doit être caché. Celle vers l'extérieur est sous la responsabilité de Seguin, le banneret de la milice Saint-Georges et il est absent, ayant mené la troupe près le roi le temps des campagnes au Nord. »

Voyant cette piste se refermer, Ernaut n'avait guère plus d'espoir pour sa question suivante, mais il préférait en avoir le cœur net.

« Pour la ville, rien non plus à signaler de spécial ces jours-là ?

--- Pas que j'en ai souvenance. Je vérifierai tantôt, afin d'en être assuré. »

Ernaut grommela. C'étaient là des informations bien vagues. Il se pouvait parfaitement qu'un intrus ait pénétré à l'insu de tous et perpétré son crime sans se faire remarquer, perdu dans la masse de gens qui allaient et venaient. Mais s'il partait de cette hypothèse, il pouvait suspecter tout le monde et personne, c'était un cul-de-sac. Il ne lui restait donc qu'une autre possibilité à suivre, à savoir que le ou les assassins soient des habitués.

« Savez-vous si d'aucuns familiers des lieux qui étaient là en ces jours ont disparu ? »

Le vicomte s'esclaffa.

« Disparu, pas forcément ! Mais beaucoup sont absents pour les fêtes, de l'hostel de l'évêque et, surtout, du chantier. Les travaux doivent reprendre ces jours, surtout en ce qui concerne la charpente, afin d'être prêts à recommencer les ouvrages de mortier au plus vite quand la froidure aura passé. Je ne parle même pas des pérégrins, toujours d'abondance.

--- Parmi ceux-ci, justement, vous n'avez eu vent d'aucune chose étrange ?

--- Il ne s'en trouve pas tant pour quitter leur manse en ces jours, en dehors de la Noël. Quelques habitués viennent plutôt à Carême, profitant de la pousse pour abandonner leurs champs un temps. Ou, plus encore, après moisson, fenaison et avant la récolte des olives, au plus chaud. »

Après avoir tant espéré de cette idée, Ernaut renâclait à la perspective de l'abandonner. Il se souvenait qu'il lui fallait dévider chaque bobine l'une après l'autre jusqu'à leur dernier brin. Ne passer à la suivante qu'une fois certain qu'on en avait exploré tous les possibles.

« Et parmi ceux-ci, avez-vous connoissance de Griffons ou d'Alemans ?

--- Nous ne demandons pas toujours leur pays aux gens qui passent les murs, fors si nous suspectons quelque vilenie ou entourloupe au péage. Peut-être devriez-vous poser la question aux chanoines, au frère hospitalier particulièrement ? »

Le vicomte s'épongea le visage, s'attardant sur sa bouche et son menton, comme s'il tentait de cacher ce qu'il allait dire.

« Serait-ce qu'il y a certeté que le complot a si lointaines racines ?

--- Certeté, pas du tout, mais doutance. Le chanoine était du royaume des Alemans et semblait s'intéresser aux secrets des Griffons, comme vous le savez. Cela aurait pu attirer l'attention sur lui de la part des uns ou des autres.

--- D'autant que la couronne des Alemans est toujours fort épineuse à porter. Il ne se trouve pas deux évêques ou barons qui n'arrivent à s'entrebattre pour au moins trois raisons.

--- Ils auraient porté leur querelle jusqu'ici ? Vous êtes au fait des menées et diplomaties des Alemans ?

--- Je n'en sais miette. C'était une simple idée, de ce que j'ai pu entendre à l'occasion. Mais je n'ai rien qui puisse établir pareille présomption. »

Ernaut fronça les sourcils et étudia le visage de son interlocuteur. Était-il complètement sincère ou ses paroles avaient-elles dépassé ses intentions ? Maugier du Toron, dirigeant le guet et les sergents de ville, avait tous les avantages pour perpétrer ces forfaits. Et il était en position de force pour faire en sorte que personne n'en sache jamais rien. L'homme lui était sympathique, mais c'était là une possible ruse. Il garda en mémoire l'idée, il lui faudrait vérifier si le vicomte avait eu partie au rapatriement des affaires du père Waulsort. Sans le condamner pour autant, vu que le meurtrier avait eu tout le temps dont il avait besoin pour s'emparer de ce qu'il souhaitait, cela pouvait trahir un oubli ou le désir de s'assurer que tout était selon ses attentes. Il remercia Maugier pour son aide et, suivant son conseil, il prit congé de lui pour aller questionner les chanoines.

Il remarqua que les ateliers et le chantier en général commençaient à s'animer au fur et à mesure de l'arrivée des artisans et manouvriers. Certains portaient encore chape et vêtements de voyage, besace au côté et bâton en main, probablement les plus désargentés qui espéraient trouver de l'embauche à peine débarqués en ville. Rien qui fût suspect parmi eux, néanmoins. Ils s'égayaient dans les différentes zones selon leur compétence et connaissance, réveillant les lieux de leurs appels et du bruit de leurs outils. Une file de bœufs était organisée, certainement dans le but d'aller quérir des pierres depuis un antique édifice des environs, source aisée de moellons de belle taille.

### Lydda, palais épiscopal, fin de matinée du lundi 29 décembre 1158

Désormais familier de l'hôtellerie des chanoines, Ernaut alla directement à la petite cellule d'où le père Gembert régnait sur son univers. Malheureusement, il trouva porte close. Comme à son habitude, le clerc devait visiter son royaume baguette en main, avec laquelle il précisait ses instructions et, parfois, réprimandait ses domestiques. Après avoir erré un temps dans les couloirs, Ernaut le retrouva dans les cuisines, veillant à un inventaire des écuelles et cuillers. La mine fort contrariée, il découvrait de nombreux plats fêlés et en faisait reproche aux gamins de salle responsables de la vaisselle.

Les cheveux ras ne laissaient voir que peu la tonsure coiffant son crâne anguleux, les yeux dissimulés sous d'épais sourcils sombres tranchant dans son visage clair. Les oreilles larges abritaient leur suffisance de poils, ce qui lui donnait un air faussement comique, démenti par les traits autoritaires de sa bouche pincée. Légèrement voûté, ce qui le faisait paraître plus petit qu'il n'était, il se tenait généralement les mains dans le dos, jouant du ballant de sa baguette ainsi qu'un chat de sa queue. Lorsqu'il s'emportait comme à l'instant, sa voix montait désagréablement dans les aigus.

Ernaut patienta, le temps que la crise soit passée, sans entrer dans les cuisines, sachant par avance qu'il y serait mal reçu, surtout à l'approche du repas de la mi-journée. Lorsque le chanoine eût terminé ses reproches et alla pour sortir, il le salua poliment. Il prit grand soin à se montrer respectueux, désireux de ne pas prolonger sa colère. Il fut donc très surpris de l'entendre répondre d'une voix posée, presque amicale.

« Je suis toujours heureux de porter secours aux pérégrins et hôtes en nos lieux. D'autant que vous êtes céans pour bien grave affaire. »

D'un geste de sa badine, il invita Ernaut à le suivre. Il ne souhaitait apparemment pas aborder de si importants sujets dans une salle où chacun aurait pu les entendre.

En fait de modeste cellule monastique, Ernaut découvrit une pièce fort douillette, au sol rehaussé d'un tapis aux complexes motifs géométriques, avec des meubles de qualité, dont un lit aux édredons chamarrés. Si le seul ornement au mur était un crucifix, il était de belle taille, de bois peint et n'aurait pas dépareillé une église. En outre, les parois étaient soulignées d'un décor de faux appareil tracé à l'ocre qui conférait un aspect cossu à l'endroit. Une fenêtre à double arcature, avec des châssis de verre clair, délivrait une importante lumière.

Après avoir clos la porte, le chanoine proposa un tabouret à Ernaut et prit place sur un petit coffre agrémenté d'un coussin. Entre eux, la table était propre, bien cirée, avec quelques rouleaux et tablettes soigneusement rangés dans des casiers. Gembert posa délicatement sa baguette à côté de lui et, joignant les mains, attendit les questions. Son aspect débonnaire si peu après avoir laissé exploser son mécontentement désarçonnait un peu Ernaut. Il lui fallut un peu de temps avant de formuler sa demande.

« Il me faut encore moissonner quelques épis pour mon maître, et j'avais espoir que vous pourriez avoir remembrance de détails d'importance. Auriez-vous souvenir de pérégrins ou voyageurs qui auraient eu suspects agissements, lors des malheureux événements ? »

Le chanoine dévisagea Ernaut comme s'il n'avait pas compris, puis prit une profonde inspiration avant de parcourir ses souvenirs. Tout le temps que dura cette recherche, Ernaut laissa divaguer son regard sur les motifs du tapis et des draps de lit. Lorsqu'il se sentit prêt, Gembert se pencha légèrement en avant, croisant les bras qu'il appuya à demi sur la table.

« Même en cherchant bien, je ne vois rien de bien exceptionnel. Nous avons eu quelques visiteurs de marque, mais aucun d'importance. Des pérégrins venus pour leur majorité de Triple[^36] et des environs de Nazareth, ainsi qu'un groupe venu de Champagne. Aucun n'a fait montre de la moindre malfaisance ou n'a agi de surprenante façon.

--- Aucun n'a eu inhabituelle question ? »

Le chanoine secoua le menton en dénégation.

« Avez-vous souvenir de qui étaient ces notables ?

--- Un châtelain royal, trois barons de moindre statut, de Normandie, d'Auvergne et de Novgorod, un notable Pisan...

--- Nul du royaume des Alemans ou de Byzance ?

--- Pas ces derniers temps. Quoique le boyard soit de certes passé en les territoires du basileus.

--- Qu'est-ce donc qu'un *boyard* ? Un Griffon ?

--- C'est un baron ou chevalier des terres septentrionales, féal du roi de Kiev. On en voit parfois. Ce ne sont pas bons chrétiens comme nous, sans pour autant être hérétiques, partageant la foi des Griffons justement. »

Ernaut se dit qu'un riche seigneur à la solde du pouvoir byzantin aurait tout à fait convenu à la tâche d'éliminer une possible menace.

« Savez-vous s'il se trouve toujours dans le royaume ?

--- Je ne sais. Il avait désir de faire visite au Saint Sépulcre avant de retourner en son pays. Il nous a quittés le jour où nous portions en terre notre frère Waulsort.

--- Il était donc absent quand l'accident du père Herbelot est arrivé ?

--- Absolument. »

Le chanoine plissa les yeux, les faisant entrer sous l'impressionnant abri de ses sourcils.

« Auriez-vous suspicion qu'il aurait à voir en toute cette histoire ?

--- À peine. Disons que je tente de voir possibles pistes à flairer. Que savez-vous de lui ?

--- Il se nommait Oleg, de la ville de Staraya Ladoga. On y bâtit une église en l'honneur de saint Georges et il avait grand désir de se recueillir sur la tombe de notre vénéré martyr. Il m'a donné l'image d'un fort honnête homme, fier et emporté comme le sont parfois ces rudes soldats du nord, mais de bonne compagnie et généreux avec le sanctuaire. Même si je me doute qu'il a versé bien plus généreuse obole à l'autel de sa foi, tenu par le presbytre Léo.

--- Est-ce là chanoine ?

--- Que non pas, s'exclama le prêtre, horrifié. C'est le chef de la croyance des Griffons ici. Ils ont usage d'une partie du sanctuaire, le petit édifice au sud, avec leur propre autel et leurs liturgies. Mais nous n'avons rien à voir les uns avec les autres. »

Ernaut n'avait pas intégré que les Byzantins puissent être aussi bien établis dans les lieux. Un clergé installé à l'année avait toute facilité pour surveiller Waulsort et leur libre accès à la basilique leur aurait permis de préparer l'accident d'Herbelot. Il se morigéna de n'y avoir pas pensé plus tôt. Il lui faudrait un jour mettre le nez dans l'organisation ecclésiastique à laquelle il ne comprenait que peu, découvrant régulièrement qu'il s'agissait d'un monde riche et complexe dont nul parmi les laïcs n'avait idée. Et s'il appartenait à l'administration royale et ne pouvait espérer qu'on y appliquerait sa justice, cela lui serait nécessaire s'il voulait mieux appréhender ce qui se déroulait autour de lui. Voyant que le silence s'installait, il hocha la tête afin de retrouver une contenance avant de reprendre, mais le père Gembert le devança.

« En ce qui concerne ce baron du nord, je ne saurais dire, mais il me serait fort surprenant que le presbytre Léo ait maille à partir avec tout cela. Pour n'être pas honnête chrétien, il n'en est pas moins bonne âme.

--- Pensez-vous qu'il accepterait de parler avec moi ?

--- Il ne sait que peu de toute cette histoire, à dessein. Car ainsi que vous pouvez l'apenser, il rend compte à son évêque et celui-ci à son patriarche, eux-mêmes fort impliqués auprès du pouvoir du basileus. Il serait le premier à les informer des moindres faits qui peuvent en ressortir. Vous seriez de mes gens, je vous interdirais donc de l'aborder, mais je ne sais quelle latitude vous a laissée le sénéchal en cet office. »

Le chanoine marqua un long temps, alternant des moues diverses, soit dans l'espoir qu'Ernaut développe les buts de sa mission, soit pour lui signifier sa réticence à voir un clergé étranger mêlé à ces affaires. Il s'éclaircit la voix avant de reprendre.

« Quoi qu'il en soit, il vous faudra montrer grande prudence, l'homme est savant et subtil. Il règne sur la petite communauté de Griffons de la ville avec ses clercs.

--- Ils sont donc nombreux ?

--- Pas tant. Il a deux diacres qui le servent, que je ne connais guère. Et les familles doivent être au plus une cinquantaine d'âmes dans la cité. Mais moult pérégrins de leur foi viennent se prosterner sur la tombe de saint Georges. Il est fort vénéré chez eux. »

Le prêtre pinça les lèvres, parut chagriné à l'idée qu'il allait énoncer.

« Serait-ce possible qu'il y ait eu quelque querelle savante à l'encontre des travaux du père Waulsort ?

--- Je n'ai pas le savoir qui me permettrait de juger de cela. Je dois simplement tâcher de découvrir qui se cache derrière ces malheurs, s'il y a lieu. Il y a eu ici fort dommageables menées au détriment de la couronne et du sire évêque, il convient donc d'en dévoiler les initiateurs. »

L'explication sembla satisfaire le chanoine, qui hocha la tête en silence. Ernaut le remercia chaleureusement, indiquant qu'il ferait montre de la plus grande prudence dans ses rapports avec le clergé orthodoxe. Gembert parut apprécier cette concession à son autorité et il se montra particulièrement aimable, se levant pour raccompagner Ernaut au-dehors. C'était l'heure du repas, et il suivit son hôte jusqu'au réfectoire. Là, il l'abandonna afin de surveiller la correcte distribution des plats, non sans l'avoir poliment salué.

Ernaut retrouva Raoul, qui avalait les lentilles à grandes cuillérées comme s'il n'avait rien mangé depuis des jours. À la moquerie de son compagnon, le jeune scribe marqua une pause.

« La tâche que tu m'as donnée est fort longue, Ernaut, et je m'y emploie de mon mieux.

--- En as-tu récolté quelques fruits ?

--- J'ai fini de faire mon classement, à vue de nez. Il ne reste pas tant de pièces qui ont possiblement sujet à notre affaire, mais elles sont en latin pour la plupart, et griffon pour quelques-unes. Cela va me demander complexe labeur. »

Ernaut haussa les épaules, attendant que le valet qui lui déposait une écuelle pleine de lentilles au fromage avec une épaisse tranche de pain se fut éloigné.

« De toute façon, moi je n'avance guère, perdu en un brouillard dont on pourrait croire qu'il ne se lèvera jamais.

--- Si le murdrier a fui, emportant avec lui tous les secrets du chanoine, il n'a pu manquer d'effacer ses traces. Nous n'en obtiendrons jamais vengeance ou réparation.

--- Comme le dit mon ami Abdul Yasu, *La vengeance ne répare pas un tort, mais elle en prévient cent autres*. Pour ma part, en comprendre suffisamment peut aider à en prévenir prou. »

Chapitre 8
----------

### Lydda, palais épiscopal, après-midi du lundi 29 décembre 1158

Le ciel était une nouvelle fois assez gris quand Ernaut ressortit après manger, un vent soulevant par rafales la poussière de la cour où s'activaient les ouvriers désormais en assez grand nombre. La majeure partie d'entre eux était affectée au déchargement de pièces de bois de charpente destinées à étendre les infrastructures d'échafaudage sur lesquelles viendraient s'appuyer les voûtes et arcades. Parmi les cris des manutentionnaires, on entendait aussi les ciseaux des tailleurs et sculpteurs, ainsi que le marteau du forgeron. Le chantier n'avait pas mis longtemps à retrouver son rythme.

Ernaut chercha un moment la haute silhouette de Gerbaut, en vain. Peut-être avait-il trouvé meilleure embauche durant la trêve de Noël. Ernaut eut un instant la crainte qu'il n'ait agi comme espion pour le parti d'Aymar. Baguenaudant aux alentours des travaux, il alla se poster à l'abri d'une loge abandonnée, dont le toit de feuilles de palme était en partie effondré.

Tout en faisant semblant de ne regarder que les opérations de construction, il conservait en général les yeux tournés vers la zone sud de l'édifice religieux, où se nichait la modeste église de rite byzantin. Il lui fallut prendre son mal en patience, car il ne s'y présentait guère de monde. Tout le temps où il s'employa à surveiller, il n'aperçut que deux hommes y entrer, dont l'un par une porte annexe. Puis une famille, femme et enfants, disparut à son tour dans la bâtisse, avant de réapparaître afin de quitter les lieux.

Désespérant de tirer quoi que ce soit d'utile, Ernaut se demandait comment il pourrait procéder quand il vit arriver Bertaut, encombré d'une lourde corbeille. Le valet, après avoir frappé au bâtiment qui unissait le sanctuaire chrétien au Byzantin, pénétra par une petite ouverture et y demeura un long moment. Lorsqu'il en ressortit, Ernaut le rejoignit en quelques enjambées. Il lui sourit et se mit à l'accompagner. L'autre salua du bout des lèvres, visiblement occupé à une tâche lui tenant à cœur. La corbeille étant vide, Ernaut lui demanda ce qu'il en était.

« J'dois porter cierges et chandelles, confiés à moi par le père Breton. »

Tout en répondant, il tenta de bomber le torse, ce qui le rendit encore plus difforme qu'à l'ordinaire, en cherchant ainsi à contrarier sa bosse. Ernaut vit qu'il tenait là un sujet pour flatter le valet et le faire parler.

« Mission d'importance en effet. Est-ce pour illuminer autels et chapelles ? Même celle des Griffons ? »

Bertaut lui lança un regard comme s'il avait affaire à un simple d'esprit.

« Cul-Dieu ! Y f'rait beau voir qu'on y donne nos chandelles de bonne cire pour leur mécréance !

--- Ne sont-ils pas ici en toute amitié avec le sire évêque ?

--- J'dis pas que non, mais de là à leur faire aumône. Ils ont bien assez de cliquailles pour orner leurs murs, qu'ils y mettent lampes à leur convenance. D'aucune façon, le vieux n'y voit goutte.

--- Comment ça ?

--- Leur père abbé ou je n'sais comment qu'ils l'appellent, il doit s'faire guider pour marcher tellement y voit rien. Alors que ferait-il de nos bons cierges ? »

Cela augurait mal de la capacité du prêtre à aller nuitamment occire Waulsort ou organiser une chute mortelle dans le chantier.

« Tu parles de raison, compère. Pour ma part, je leur reprendrai bien volontiers cette église pour la rendre à Dieu. Qu'ils aillent faire leurs priements impies en un autre lieu que notre sol sacré !

--- Oh, y sont pas bien gênants ! Et pis y sont discrets, qu'y n'effraieraient pas un agneau. Sans compter qu'on peut aller faire oraisons en leur chapelle, ils n'y voient pas malice. J'y suis été une fois, histoire de me rendre compte de comment qu'c'est.

--- Et alors ?

--- Y'a trop de couleurs partout, que ça embrouille les sens. Pis y mettent de l'encens qu'on dirait que ça leur coûte rien. J'peux dire que j'ai vu et que j'en ai eu ma suffisance. »

Comme ils arrivaient aux abords du cellier, il salua d'un signe de tête et poursuivit son chemin dans le bâtiment. La discussion incita Ernaut à y aller constater de ses propres yeux. S'ils ne refoulaient pas les catholiques, il pourrait ainsi mieux voir ce qui se passait. Et si on l'interrogeait, il avait toujours la fable du chantier possible pour une propriété royale. Il entra donc d'un pas décidé dans l'édifice. Il y fut surpris par la taille modeste, amplifiée par la surcharge picturale des murs et les ténèbres relatives. La majeure partie de la lumière extérieure pénétrait par de chiches baies et on ne discernait l'espace qu'à la lueur de lampes à huile. Des personnages, certainement des saints, ornaient de leur profil hiératique la plupart des surfaces, l'air sévère. Face à l'entrée, un complexe cycle de peintures se déployait de façon colossale, présentant des visages austères et sentencieux. Sentant d'instinct le sacré de l'endroit, il se signa lors d'une brève génuflexion et avança, plissant les paupières le temps que ses yeux voient mieux dans les ténèbres.

Il n'avait pas fait plus de quelques enjambées qu'une voix résonna, dans une langue qu'il ne comprit pas. Il aperçut un minuscule vieil homme, tout de noir vêtu, qui venait vers lui d'un pas hésitant. Recouvert d'une tenue sombre sans ornement et coiffé d'une toque de même couleur, on ne discernait de lui que son impressionnante barbe blanche surplombée d'un nez tout aussi imposant. De ses manches s'échappaient des mains chenues, d'albâtre. Il les portait en avant comme pour se garantir des obstacles et Ernaut comprit que ses yeux, noyés de cataracte n'y voyaient plus guère. Ce devait être le fameux responsable. Il le salua poliment, sans ostentation, et obtint en retour des paroles amicales, l'invitant à se recueillir tout son saoul dans ces lieux.

« La merci de votre amiable accueil. Je ne suis pas de votre foi, je le confesse.

--- Je m'en suis douté, vu que vous n'avez entendu mon premier appel. Vous êtes celte, n'est-ce pas ? Nombreux furent vos prédécesseurs ici, qui ont usé de cette chapelle quand la grande mère ecclésiale était effondrée. Nous avons usage de partager ces lieux. »

Il s'apprêtait à repartir, mais Ernaut voulait en apprendre plus.

« Accueillez-vous souventes fois des gens de ma foi ici ?

--- Pas tant qu'il s'en trouve près le sépulcre du saint, mais il s'en encontre d'aucuns, poussés par la curiosité, peut-être, et le froid, parfois, qui aventurent leurs pas jusqu'ici. Pourquoi cette question ?

--- Je suis de Jérusalem, et je n'ai pas usage de vos églises. Chez moi, on voit certains autels qui vous sont accordés...

--- Vous êtes donc de cette première catégorie, d'autant que le ciel est assez clément aujourd'hui. Disposez de ces lieux comme de votre église. Je vous demanderai juste de demeurer en deçà de l'iconostase, les lieux en delà étant réservés aux officiants tels que moi. »

Il se tourna vers le mur oriental en le désignant de la main, puis appela dans sa langue natale, avant de se retourner une nouvelle fois vers Ernaut.

« Je suis désolé de n'avoir plus guère de temps à vous consacrer, mes diacres reviennent à peine d'un long voyage et nous avons encore labeur pour préparer dignement la Théophanie.

--- Je m'en voudrais de léser les ouailles d'un prêtre. Étaient-ils absents pour quelque religieuse affaire ?

--- Oh non, et il s'en est fallu de peu qu'ils ne soient pas de retour à temps pour la naissance de Christ. »

Il eut un petit sourire, noyé dans sa barbe tandis qu'il envisageait Ernaut de ses yeux en partie aveugle.

« Je le devine, vous êtes bien plus que vous ne le dites. Si le sire évêque vous a mandé pour savoir s'ils avaient bien recueilli notre écot aux nécessaires travaux, rassurez-le. Nous avons reçu toutes les assurances ! Il nous faut quelques jours encore et nous lui ferons porter le tout.

--- De quels travaux parlez-vous ? »

Le vieil homme sourit une nouvelle fois, chassant l'air de la main comme s'il entendait une bonne blague. Il salua Ernaut d'un signe de tête puis s'éloigna de sa démarche lente. Il ne restait plus à Ernaut qu'à ressortir. Il lui faudrait voir à faire surveiller le presbytre et ses acolytes, au cas où ils auraient des activités cachées et que tout ce discours ne fût là que pour l'embrouiller. Le prêtre semblait bien trop mal allant pour nuire à quiconque, mais il avait l'influence suffisante pour pousser un fidèle à passer à l'acte.

Avant tout, Ernaut voulait s'assurer de l'absence des deux assistants. Il se rendit donc à la salle du chapitre où il pensait retrouver le doyen, mais il le rencontra dans le cloître, brandissant d'étranges objets de bronze devant un parterre de jeunes élèves tonsurés. Ernaut s'approcha sans bruit, découvrant que le chanoine était en train d'expliquer le fonctionnement assez complexe de l'appareil qu'il tenait en main. Relativement plat et circulaire, on pouvait le suspendre par un anneau et divers cadrans pivotaient autour d'un axe. Aux dires du vieil homme, cela servait à des calculs astronomiques.

Comprenant qu'Ernaut venait pour l'entretenir, le père Elies invita un de ses étudiants à le remplacer. Il s'avança alors vers une autre aile du cloître, où parler plus à leur aise.

« Je suis désolé de vous interrompre en pleine leçon, père doyen, mais il me faut avoir confirmation de quelques faits.

--- Il n'y a pas offense, nous entamons à peine les premiers degrés de l'astronomie avec ces jeunes gens. Il est temps pour eux d'apprendre à compter dans le temps et l'espace. Le comput demande un esprit habile et bien ciselé.

--- Le comput ?

--- Connaître épacte, indiction et autres éléments essentiels à la détermination des Pâques, par exemple. C'est tâche fort difficile, qui ne se peut qu'une fois maîtrisés tous les autres savoirs, fors la théologie, bien sûr. »

Ernaut se demanda tout à coup si le vieil Élies n'était pas plus savant qu'il ne le laissait croire. Peut-être avait-il eu partie liée avec Waulsort dans ses travaux, ou s'y intéressait, voire les jalousait. Pour être chanoine, il n'en était pas moins homme.

« Je ne suis pas tant raffolé de ces sciences qui n'aident en rien à se rapprocher de Dieu, mais il en est besoin pour respecter les usages du culte. Maintenant que nous avons perdu notre écolâtre, je ne suis pas acertainé que nous continuerons à enseigner ainsi. Il ne me semble de nul usage d'ainsi se farcir le crâne avec ces savoirs superflus. La prière me semble bien plus assuré chemin. »

Il eut un petit soupir et, prenant le bras d'Ernaut, se rapprocha de lui.

« Mais vous n'êtes céans pas pour études. Demeure-t-il quelque ombre que je pourrais dissiper de mes lumières ?

--- Si fait, père doyen. Je me demandais si vous avez bonne fiance avec les pères griffons qui ont leur culte dans la chapelle méridionale. Et si vous pouvez me dire s'ils se sont absentés ces derniers temps.

--- Le vieux Léo aurait-il part à notre affaire, mon garçon ? Il faudrait en parler au sire évêque, qu'on leur retire l'usage de ces saints lieux.

--- Je n'y crois guère, mais pour faire de mon doute une certitude, j'ai besoin de votre aide. »

Le vieux chanoine se renfrogna, peut-être déçu d'abandonner si vite une piste pour se débarrasser d'un culte concurrent.

« Ils sont toujours à chicaner pour tout et rien. Et ne paient pas la dîme, ce qui est grand hontage. Sans compter qu'on croirait traiter avec des Juifs tellement ils se cramponnent à leurs monnaies. Il a fallu que le sire Constantin s'y reprenne à plusieurs fois pour qu'ils daignent verser leur part en ces travaux.

--- C'est justement à ce propos que j'ai besoin de précisions. Les desservants qui assistent le vieux presbytre se sont absentés récemment ?

--- Il était bien temps ! Ils geignaient de ne pouvoir verser monnaie comme on les attendait d'eux. Le père évêque leur a intimé de collecter suffisante somme, faute de quoi ils verraient leur sanctuaire fort diminué. J'aurais préféré qu'il les chasse incontinent, mais il a le naturel amiable et pardonneux...

--- Avez-vous remembrance de quand et quand ils étaient fors Lydda ?

--- Je ne sais plus de juste, mais ils sont partis peu après la Fête de tous les saints[^37], pour ne revenir que ces jours-ci, demandant encore délai alors même que la Noël est passée et qu'ils sont en retard ! »

Ils étaient donc au loin lors des meurtres et de l'accident, comprit Ernaut. Cela ne les dédouanait pas d'une possible participation à un complot, mais en ce cas ils auraient dû recourir à des complices. Ils auraient parfaitement pu orchestrer toute l'affaire, profitant de leur absence pour recruter des séides ou contacter des hommes de main. Il remercia avec chaleur le doyen et le laissa reprendre son cours. Il lui fallait discuter de nouveau avec le teinturier pour en apprendre plus sur cette fameuse poudre de lune. Le recours à une telle préparation trahissait le complot. Si le chanoine et son assistant avaient bien reçu des coups qui avaient répandu le sang, user de ce composant ne servait qu'à cacher le forfait et semer le doute. C'était à la fois une façon de brouiller les pistes et un avertissement envers ceux qui auraient été tentés de poursuivre les mêmes recherches : le danger était gigantesque, les forces adverses terribles et incontrôlables.

### Lydda, quartier des teinturiers, après-midi du lundi 29 décembre 1158

Ernaut retrouva sans mal l'atelier d'Amos, la ville n'étant pas si vaste et sinueuse que Jérusalem. Les portes du lieu étaient largement ouvertes sur la rue où de nombreux fils étaient suspendus entre de provisoires crochets et bâtons. De l'un à l'autre, des gamins allaient et venaient, dévidant des écheveaux enroulés autour de leurs bras maigres. L'endroit sentait toujours aussi mauvais et le sol était constellé de taches brunes diverses qui gouttaient depuis les cuves jusqu'aux fils tendus.

Le maître teinturier ne le laissa pas approcher qu'il s'avançait déjà à sa rencontre, s'inclinant de façon appuyée. Ernaut lui trouva le sourire un peu forcé, mais ne s'en fâcha pas. Il aimait à savoir son interlocuteur mal à l'aise, cela n'en rendait que plus facile son objectif. Il le salua poliment, mais sans se départir d'un air de sérieux qu'il espérait respirer l'autorité. Tout en jetant un œil circonspect aux larbins qui allaient et venaient, Amos invita Ernaut à pénétrer dans son atelier, mais ne se dirigea pas vers le cagibi de la fois précédente. Il conduisit Ernaut à travers un jardin joliment fleuri jusqu'à une petite salle protégée d'une galerie. Une femme aux traits sévères, le visage cerclé d'un foulard chamarré les accueillit d'une voix monocorde puis disparut rapidement dans une autre pièce. Elle en revint avec un pichet de vin et des pains plats aux olives. Amos fit le service avant de prendre la parole.

« Je suis bien aise de vous revoir, mestre Ernaut. J'ai tâché d'en savoir plus auprès de quelques amis à propos de l'escarboucle. J'ai envoyé quelques lettres, mais je n'en espère pas réponse avant long temps.

--- J'espère que ce fut là discrète inquisition.

--- Nul ne s'étonne des curiosités des teinturiers, n'ayez crainte ! Outre cela, j'ai retrouvé de vieilles lettres de mes maîtres et amis. La récolte n'est point tant large, mais de bonne qualité. »

Ménageant ses effets, il s'accorda une longue rasade et quelques bouchées avant de reprendre.

« Peut-être voulez-vous m'enquérir de certains points avant que je ne vous narre mes trouvailles ?

--- Mes questions trouveront peut-être réponse dans votre récit, faites donc.

--- Je ne sais si vous avez souvenance que je pensais l'escarboucle fort dangereuse. Elle l'est bien plus encore : le simple fait de la toucher ou d'en respirer les vapeurs fétides est mortel. Et, comble du danger, il suffit parfois qu'elle soit exposée à l'air pour s'enflammer d'elle-même. C'est ce qui la rend si rare. Il n'existe qu'un seul moyen de la conserver : il faut la maintenir en un endroit frais, noyée dans de l'eau et l'y manipuler avec pincettes. »

Il ajouta, avec un sourire mi-figue mi-raisin :

« On comprend alors pourquoi il ne s'en rencontre que peu. C'est là fort périlleux composé. Outre cela, le point le plus intéressant, c'est qu'il se nourrit de vie et dédaigne les choses mortes.

--- Qu'est-ce à dire ?

--- Ses flammes dévorent les chairs, mais pas les habits. Et ses vapeurs viennent ronger en dedans.

--- Peste ! C'est là composé bien démoniaque !

--- D'autant qu'il n'en faut que très peu. Un mithqal suffit à tuer plusieurs hommes, dit-on. »

Voyant qu'Ernaut ne visualisait pas les quantités, Amos précisa :

« Le poids d'un dinar en or, mon ami. Rien de plus !

--- C'est terrible. Une telle poudre anéantirait bien vite n'importe quel ost !

--- S'il se trouvait quelque ingénieux savant assez fou pour fabriquer l'escarboucle en grande quantité et assez habile pour la faire transporter sans qu'elle ne se retourne contre ses créateurs. Rendons grâce qu'il ne s'en est trouvé aucun jusqu'à ce jour.

--- Est-ce compliqué d'en obtenir ?

--- Si fait. On l'obtient à partir de grandes quantités d'urine, que l'on doit putréfier, selon des procédés complexes dont je n'ai pas le détail. Seuls de zélés et compétents érudits doivent détenir de tels secrets.

--- Nul ne saurait la fabriquer discrètement ?

--- Je ne le pense pas. Il faudrait force ustensiles adaptés et beaucoup d'efforts, sans garantie de réussir. »

Il était possible que le chanoine se soit intéressé à une telle poudre, mais en ce cas, il demeurait étonnant qu'on n'en trouve aucune mention dans ses écrits, du moins jusqu'à présent. Pour Ernaut, il apparaissait clairement que quelqu'un était intervenu, d'une façon ou d'une autre, pour mettre un terme aux recherches et qu'il avait usé de l'escarboucle pour camoufler ses agissements. L'hypothèse des Byzantins jaloux de leur secret du feu de guerre lui semblait la plus pertinente.

Ernaut demeura encore un petit moment avec le teinturier, appréciant fort le vin, puis se résolut à le laisser retourner à ses travaux. Il dirigea alors ses pas vers le quartier où avait résidé Waulsort. Il n'avait pas l'intention de fouiller une nouvelle fois dans la maison, mais espérait y rencontrer son jeune espion, Hashim. Il se rendit d'instinct à l'atelier de Umayyah qui l'accueillit aimablement, occupé qu'il était à refendre des feuilles de palmier afin de les tresser. Par politesse, Ernaut s'enquit de ses affaires et de sa santé avant de demander après Hashim.

« Il traîne dans les environs comme à son habitude. Je l'ai vu passer en clopinant avant le mitan du jour, les bras encombrés de paniers.

--- Clopinant ? Il avait plutôt usage de courir !

--- C'est ce qui m'a surpris, il a un de ses pieds emmitouflé d'un linge et lui en ai demandé la raison, il s'est contenté de hausser les épaules. »

Ernaut remercia l'artisan et reprit ses recherches. Il découvrit finalement Hashim occupé à refaire son bandage improvisé, installé sur le toit-terrasse d'un petit cellier en contrebas de la rue. En le voyant, le gamin se fendit d'un sourire plus commercial qu'enthousiaste.

« Alors, Hashim, j'avais grande crainte, à te voir ainsi déserter les rues ! J'espère que tu vas bien !

--- On s'débrouille, mestre. »

Désignant le pansement rudimentaire, Ernaut s'assit à ses côtés, désireux de ne pas trop l'impressionner.

« J'espère que cela n'est pas trop grave. Que serait Lydda sans le bruit de tes courses en les rues ?

--- Tout ira bien, mestre, n'ayez crainte. »

Ernaut se pencha pour voir de plus près le chiffon servant de pansement.

« As-tu vu chirurgien pour cela ? Que male mort ne vienne pas s'y loger ! »

Haussant les épaules, Hassim déroula un peu son bandage et lui montra les marques bleutées encore légèrement encroutées de sang.

« L'imam m'a dit de nettoyer tous les soirs à l'eau claire. Ça guérit bien, même si ça gêne un peu !

--- Comment as-tu reçu pareille navrure ?

--- Parfois, je fais le muletier jusqu'à la côte. Je n'aime pas tant ça les bêtes, et elles me le rendent bien. Une de ces satanées bourriques m'a marché sur le pied. »

Il rattacha son tissu, puis adressa un sourire enjoué à Ernaut.

« Mais ne craignez rien, je peux encore vous être utile.

--- En ce cas, j'ai en effet ouvrage pour toi. J'aurais besoin que tu gardes les yeux sur quelques drôles.

--- Ceux de l'autre fois ont fini par quitter la ville, par la porte de Samarie.

--- Je l'ai su, maugré ton absence pour m'en rendre compte. »

Désappointé de cette rebuffade lancée sans y penser, Hashim pointa du nez, au grand dam d'Ernaut. Celui-ci souhaitait entretenir l'enthousiasme de son jeune ami, afin qu'il s'acquitte de cette nouvelle mission avec autant de cœur que précédemment.

« Mais c'est sans importance. Il va te falloir venir avec moi jusqu'au sanctuaire Saint-Georges. Je t'y désignerai ceux dont tu dois t'inquiéter. Et il serait bon que tu embauches de nouvel tes petits compagnons.

--- Je ne saurais aller en l'enceinte de la forteresse, l'accès nous en est défendu. À moins de leur graisser la main, nul homme du guet ne nous laisse entrer. Ils nous disent chapardeurs.

--- Je ferai en sorte que tu puisses aller et venir sans t'en inquiéter. »

Tout en se relevant, il demanda à Hashim de le rejoindre vers les écuries de sa précédente mission, avec au moins deux de ses compères. Il souhaitait vérifier une nouvelle fois si les envoyés du commanditaire n'étaient pas revenus. Il eut à peine le temps de s'en enquérir auprès du valet que la petite bande sautait d'un pied sur l'autre sous le bosquet de palmiers de la place adjacente.

Il les mena jusqu'au palais, amusé de voir leur plaisir à l'idée de franchir une des limites de leur territoire. Les hommes de faction ne firent guère d'objections à la demande d'Ernaut de laisser aller et venir les gamins, étant donné qu'ils étaient commissionnés par un sergent du roi. L'un d'eux fit grise mine, mais s'efforça maladroitement de le cacher. Ernaut s'imagina être à la place de la petite bande et se rappelait les nombreuses transgressions avec ses amis d'enfance à Vézelay. Il eut une fugace pensée pour son frère ennemi d'alors, Droin, arrivé à son tour en Terre sainte pour y trouver embauche comme valet de ferme. Il avait gardé l'habitude d'user de son prénom quand il sentait qu'on pouvait lui reprocher ses agissements.

Il mena discrètement ses espions au travers du chantier en pleine activité, afin de leur indiquer les personnes à surveiller. Il lui était facile de décrire le vieux prêtre byzantin, mais les deux servants lui étaient inconnus et il devait s'en remettre à l'astuce des gamins des rues pour en deviner l'apparence. Il donna aussi quelques méreaux à Hashim pour qu'ils se trouvent à manger le temps de leur vigie. Lorsqu'Ernaut désirerait un rapport, il se nouerait son foulard sur la tête en un turban. Ils n'auraient à ce moment qu'à le suivre jusqu'à un endroit discret où ils pourraient discuter à loisir.

Une fois ses instructions délivrées, il les laissa se disperser à leur gré et prit la direction des cellules des chanoines. Il était content de sa journée et souhaitait s'en entretenir avec Raoul. Il ne détestait rien tant que de demeurer inactif. Même s'il n'avait guère appris de choses nouvelles, au moins s'était-il bien occupé depuis le matin, et il en ressentait une grande satisfaction. Avec un peu de chance, les découvertes seraient aussi en bonne voie dans les recherches parmi les écrits de Waulsort.

Raoul ne déverrouilla la serrure qu'une fois qu'Ernaut eut accompli la frappe convenue et se soit annoncé de la voix. Le scribe avait disséminé les documents au travers de toute la pièce et devait sautiller entre les tas pour ne pas perturber son classement. Ernaut opta pour la prudence et se cantonna aux abords de la porte.

« J'ai fait copie de quelques pièces, déjà, mais il me semble fort douteux qu'il n'en naisse rien d'utile. C'est par trop fragmentaire.

--- Combien te reste-t-il de documents à trier ?

--- Aucun, justement. C'est pour cela que j'ai commencé copies. Je note chacune des fois où il semble faire allusion au feu de guerre des Griffons, ou fait référence à un savoir qui pourrait être en rapport. Mais il ne se trouve que bien peu de choses, une fois le reste écarté avec certitude. »

Raoul prit un air contrarié, plissant les sourcils.

« Plus je parcours ces lettres, plus grandit en moi la conviction qu'il y manque l'essentiel.

--- Le sire évêque a pourtant assuré que nous avions tout ici.

--- De certes. Tout ici était mélangé, mais une fois un peu d'ordre apporté, il me semble qu'on a retiré justement des travaux du chanoine tout ce qui aurait été le plus pertinent. Je n'ai là que vagues formulations, notes confuses de lectures et vagues copies de textes d'anciennes langues.

--- Essentiels travaux qu'un murdrier aurait pu emporter une fois son forfait accompli.

--- Exactement. »

### Lydda, jardins de l'évêché, fin d'après-midi du mardi 30 décembre 1158

Estimant qu'il était important pour lui de tenir son rôle, Ernaut s'était efforcé depuis le matin de ne pas sembler se préoccuper des prêtres byzantins, ni n'avait porté ses pas vers la demeure de Waulsort. Il se contentait de passer le temps en échangeant avec les maçons, charpentiers et tailleurs de pierre, se baladant parmi les ouvriers tout en faisant mine de prendre un grand intérêt à ce qu'ils faisaient. Ce n'était d'ailleurs pas complètement feint. Il n'aimait rien tant que laisser libre cours à sa curiosité naturelle et son statut d'envoyé du roi, d'invité de l'évêque, lui donnait toute l'autorité dont il avait toujours rêvé.

Les travaux abordaient une phase délicate, le chantier devait avancer pour élever une nouvelle travée plus à l'ouest. Chaque tranche était ainsi montée jusqu'à être hors d'eau avant de se voir complétée d'une voisine occidentale. Cela avait permis, en commençant par le chœur à l'est, de rendre l'édifice au culte très rapidement, tout en ayant un programme architectural ambitieux. Ernaut était surpris de la quantité de pierres et même de sculptures qui était récupérées sur les ruines environnantes. Les Romains avaient bâti de vastes complexes, abandonnés par la suite et il s'y trouvait de grandioses vestiges dans lesquels puiser de quoi concevoir de nouveaux ouvrages de grande envergure. Il avait appris que la collecte des plus beaux éléments était un privilège que les puissants s'arrachaient, désireux de ne pas voir leur voisin, et certainement pas un bourgeois, s'accaparer les plus magnifiques réalisations du passé.

Il découvrit également qu'au contraire de la Bourgogne où il était né, où les forêts étaient nombreuses et fournies, le royaume de Jérusalem manquait perpétuellement de bois d'œuvre de qualité. Le comté de Triple, au nord, en expédiait d'importantes quantités, et on acheminait même des pièces de charpente depuis l'autre rive de la Méditerranée. Un des chanoines lui avait expliqué que les mahométans avaient abattu la basilique à l'approche de Godefroy de Bouillon afin de l'empêcher d'en utiliser les poutres pour en faire des engins de siège contre Jérusalem. Cette décision impie n'était pas pour rien, selon lui, dans la victoire de leurs glorieux prédécesseurs, saint Georges n'ayant pu laisser pareil affront impuni.

Ernaut avait vu parfois passer Hashim ou l'un de ses commissionnaires, mais il avait veillé à ne pas leur accorder d'attention. Le succès de leur mission dépendait de sa capacité à lui d'en faire peu de cas. Il avait estimé qu'une journée suffirait à avoir une bonne idée des agissements habituels des prêtres byzantins. Il avait hésité à prêter la main à Raoul, mais ce dernier se contentait de lire les documents l'un après l'autre, puis les disposait en tas précis, après en avoir noté éventuellement certaines phrases. Ernaut n'était donc en mesure de lui apporter aucune aide et risquait fort, au contraire, de le retarder, par ses incessantes envies de bavardage.

Le ciel de cette fin de journée se montrait clément, d'évanescents filaments cotonneux s'étirant haut dans l'azur. La température était assez froide, un vent du large délivrant humidité et fraîcheur. Assis sur un petit banc près du chevet de l'église, Ernaut admirait le doux balancement des palmiers. Les valets finissaient d'empierrer une vaste zone avec ce qu'ils avaient récolté dans les parcelles environnantes. Les hommes n'avaient plus guère d'entrain pour leur tâche et attendaient visiblement avec impatience que vêpres sonnent l'arrêt du labeur. Indifférentes aux travailleurs, des volailles picoraient parmi la terre remuée quasiment au milieu de leurs jambes.

Depuis qu'il était revenu, Ernaut venait prier chaque jour sur la tombe d'Herbelot. Sans se l'expliquer vraiment ni chercher à en savoir la raison, il se sentait en devoir de telles actions de grâce quotidiennes. Il n'avait pas été si proche du jeune clerc, mais il lui semblait que partait avec lui quelque chose de son enfance. Étonnamment, le décès de l'agaçant petit bonhomme l'affectait plus que le fait qu'il ne reverrait jamais son père ni la plus grande partie de sa famille en dehors de Lambert.

Lorsque la cloche commença à résonner, Ernaut se leva et sortit son turban de sa sacoche. Il avait appris à le nouer plus adroitement avec le temps et, surtout, il veillait à le poser sur un bonnet, *ma'raqa*, qui permettait de l'ôter et de le remettre commodément. C'était là une pratique plutôt citadine dont Abdul Yasu s'était fait un plaisir de lui montrer l'usage. Il flâna un moment près des loges d'artisans, assistant à leur fermeture l'une après l'autre. Puis il prit la direction d'un quartier en ville où il savait trouver un petit souk qui accueillait tavernes et commerces de bouche. Installé autour d'une belle place où s'activait un moulin entraîné par un âne pour emplir un abreuvoir, il offrait en outre l'avantage de n'avoir qu'une porte d'entrée, férocement gardée par un concierge borgne. Il était donc facile d'en surveiller discrètement les arrivants et de découvrir une filature.

Ernaut acheta quelques beignets de poisson enfilés sur des bâtonnets, avec du pain et une sauce au cumin à base de pois chiches. Il se fit également servir une bière de couleur brune, assez épaisse, avec une mousse compacte. Cela lui constituerait, ainsi qu'à son espion, un souper acceptable. Autour de lui, des bandes de jeunes gens, journaliers sans épouse ni mère pour leur faire à manger, venaient chercher de quoi se restaurer pour le soir et le lendemain. Chez ces manouvriers désargentés, c'était le pain qui était le plus important des achats, parfois agrémenté de purée de pois ou d'olives, voire d'un peu d'huile. En outre, ils allongeaient la ration de vin qu'ils faisaient verser dans une outre ou une calebasse d'une large rasade d'eau prélevée au moulin. Il sourit pour lui-même, heureux d'échapper pour l'heure à une vie aussi laborieuse. Les curés lui avaient enseigné que Dieu avait prévu un rôle à chacun et qu'il ne fallait pas en bousculer les desseins, mais le remercier de ses bienfaits, quelle que soit sa place dans le monde. Pour les fréquenter chaque jour, Ernaut estimait qu'il faisait mieux vivre à être baron richement doté que portefaix ou terrassier et qu'il était alors plus aisé d'entendre les sermons des prêtres.

Il s'installait à peine pour prendre son repas qu'Hashim se découvrit à lui subitement. Le gamin avait réussi à pénétrer dans le souk sans être vu d'Ernaut et arborait cet air satisfait de celui qui sait la récompense proche. D'un geste, Ernaut l'invita à s'assoir à ses côtés et lui tendit de quoi se restaurer.

« Alors, as-tu joli conte à me narrer ? »

La bouche déjà pleine, Hashim se contenta de hocher la tête, avalant à grande vitesse ce qu'on lui avait offert. Ernaut lui fit comprendre qu'ils n'étaient pas pressés et qu'il pouvait donc prendre son temps pour manger, mais le gamin dévora tout ce qui lui avait été attribué avant même qu'Ernaut n'ait eu le loisir de finir la moitié de ses brochettes. Puis il s'éclaircit la voix d'une petite gorgée de bière, pour enfin s'essuyer les lèvres d'un grand mouvement du bras.

« Les imams ne bougent guère en dehors de leur mosquée. Ils vont et viennent parfois jusqu'au palais, où on ne peut entrer. Et à la nuit, s'en retournent chacun en sa demeure.

--- Rien de particulier sur celles-ci ?

--- Non, ils demeurent dans le même quartier, au sud, près d'un petit souk de changeurs. »

Ernaut posa diverses questions sur les agissements des trois prêtres, sans que le moindre élément ne lui parût suspect. Ils s'occupaient du sanctuaire, des quelques fidèles habitant en ville, et rien de spécial ne semblait exister entre eux, la demeure de Waulsort, la cellule où ses notes étaient gardées pour l'instant sous la surveillance de Raoul, ou le repaire d'Aymar et ses hommes. Assombri de voir ses espoirs déçus, Ernaut décida de se réconforter avec des pâtisseries à la pistache et en proposa à Hashim, dont les yeux brillèrent d'excitation en réponse.

Tandis qu'ils dévoraient les sucreries, constatant bien le désappointement de son employeur, le gamin s'efforçait de rapporter les moindres éléments qu'il avait pu observer, même ceux qui lui avaient paru insignifiants. Peut-être aspirait-il à un renouvellement de sa mission ? Mais Ernaut ne releva rien dans la majeure partie de ses déclarations.

« Je ne sais si c'est d'usage pour toi, mais il se trouve aussi un habitué à leur mosquée parmi les gens du palais.

--- Du palais ? Un chanoine ?

--- Non, un des ymagiers qui œuvre en leur temple. Il va et vient assez souventes fois. Au vu des moments, il est possible qu'il prenne ses repas avec les imams des Romains.

--- Cela me semble fort étrange qu'artisans et clercs se mêlent ainsi. Ils n'ont nul hospice pour pérégrins, qui se rendent tous au réfectoire de l'évêque ! »

Hashim haussa les épaules, préférant se cantonner dans un rôle d'observateur.

« As-tu fait le compte de ses venues ?

--- Non pas. Mais cela peut se faire dès à présent ! »

La perpétuation implicite de l'embauche n'échappa pas à Ernaut, qui accorda un sourire à son rusé petit ami. Il acquiesça, tout en tentant d'en savoir plus.

« Quels travaux cet homme fait-il ?

--- Je ne sais pour l'heure, mais cela sera vite découvert. Je pourrai te le montrer demain matin. Souhaites-tu que nous l'ayons à l'œil aussi ? Nous ne suffirons pas à la tâche.

--- Je ne suis pas acertainé que le sire évêque apprécierait que vous soyez trop nombreux à courir en ses lieux. Préférez l'artisan aux clercs si vous devez choisir, mais essayez de les surveiller tous, autant que faire se peut. »

Hashim hocha le menton, déçu de n'avoir pas là l'occasion d'augmenter ses tarifs. Il avait déjà âprement marchandé, expliquant que s'absenter de ses rues pendant plusieurs jours risquait de les voir conquises par de jeunes entreprenants qui n'apprécieraient guère de laisser la place à son retour. Pour les gamins tels que lui, il existait une concurrence d'autant plus féroce qu'elle permettait d'avoir le ventre sinon plein, du moins apaisé de temps à autre.

« Au matin demain, tu n'auras qu'à me surveiller issir de l'hostellerie. J'irai tranquillement en direction du cimetière, ainsi que j'en ai usage. Trouve avec tes camarades une façon de me désigner l'artisan dont tu m'as parlé. »

### Lydda, cloître canonial, fin d'après-midi du mercredi 31 décembre 1158

Après un nouveau rapport de ses espions, Ernaut avait pu identifier celui des artistes qui fréquentait intensément le lieu de culte byzantin. C'était le maître mosaïste, qui supervisait et réalisait des décors tout autour du chœur. Pas très grand, le profil épais et empâté, poilu comme ours, il se déplaçait entre les passerelles et le sol avec une aisance qui rappelait celle du singe de l'évêque. Ernaut n'avait pas trop attardé son regard sur lui, mais il lui avait trouvé aimable faciès. La tête ronde et joufflue, des plis soulignaient ses paupières et ses traits rebondis mangés d'une barbe de plusieurs jours. Il portait sur le crâne un vague bonnet, qui avait dû arborer des décors, depuis des années effacés. Il donnait ses instructions d'une voix forte et mélodieuse, avec un demi-sourire sur le visage. Quoiqu'il semblât atteint d'une mauvaise toux qui l'interrompait souvent, il lui arrivait fréquemment de chantonner tandis qu'il s'adonnait aux tâches les plus exigeantes.

Espérant en apprendre plus sur celui qui devenait pour l'instant son principal suspect, Ernaut était parti en quête du doyen des chanoines. Las, le père Élies lui avait indiqué qu'il ne s'occupait guère des gens du chantier, se contentant d'en supporter les désagréments. Waulsort supervisait les programmes iconographiques et le drapier réglait les factures. Un peu grognon d'être dérangé pour la moindre question, le doyen ne s'était guère montré coopératif, il disait n'en savoir pas plus et ne voulait pas s'y intéresser. Ernaut en fut quitte pour se hâter vers les entrepôts où il pouvait retrouver le père Danyel. Il aurait au moins le plaisir de pouvoir y rencontrer une aimable compagnie.

Il trouva le drapier dans sa petite loge, en pleine discussion avec un groupe de marchands. Ils avaient apporté différentes toiles, que le chanoine palpait avec science. Il échangeait avec ces négociants levantins dans la langue locale avec une aisance qui rendit Ernaut jaloux. Lorsqu'il l'aperçut, le prêtre mit un terme rapidement à son entretien. Atermoyer et reporter faisait partie des négociations commerciales et les visiteurs s'éclipsèrent sans rechigner, jaugeant malgré tout avec dépit ce géant qui n'avait pas su attendre son tour.

« Il m'avait bien semblé apercevoir Sanson déambuler en nos lieux » plaisanta le chanoine, en invitant Ernaut à entrer.

Sans plus de manière, il s'assit, proposant à Ernaut d'un mouvement de la main de faire pareil face à lui.

« Notre sire le père évêque Constantin nous a confié qu'il demeurait quelques mystères à lever.

--- On m'a en effet renvoyé pour glaner plus de détails sur certains points. Et j'aurais besoin de vos lumières. En tant que drapier, vous tenez les comptes, si j'ai bien compris ?

--- Oui, j'ai la charge, avec le frère cellérier et l'hospitalier, de beaucoup de dépenses ici.

--- C'est vous qui payez pour les travaux ?

--- Certes. Cela s'est décidé avant même que je ne sois nommé. Il était convenu que c'était là besogne moins ardue que celle de mes deux frères. Les dépenses du chantier y ont donc été affectées, avec la gestion des pécunes qui va avec.

--- Connaissez-vous tous les hommes qui œuvrent en la basilique ? »

Le père Danyel éclata de rire.

« Certes non ! Il s'en vient tant et plus chaque jour quémander un travail dont il sait qu'il sera payé, et bien payé, par monnaie sonnante et trébuchante. J'ai recours à des maalem[^38] ou commissionnaires qui s'occupent de ces choses. Je vois surtout maître Barsam d'ailleurs. »

Ernaut fit une moue qui n'échappa pas au chanoine, qui s'enquit de sa signification d'un regard.

« Il me faut en apprendre un peu plus sur un des hommes du chantier et je ne veux certes pas que cela se sache. Je n'en connais pas le nom, seulement la pratique.

--- Dites-moi toujours, j'en ai usance, après toutes ces années. Peut-être pourrais-je le nommer ? »

Ernaut eut quelques scrupules à se dévoiler ainsi, d'autant qu'il n'avait rien de tangible pour le moment, mais le père Danyel lui inspirait confiance et il lâcha, dans un long soupir :

« Le maître mosaïste...

--- Mestre Zénon de Morée[^39] ? Que voilà artiste bien facile à nommer. Il est tant et tant connu qu'il ne vous aurait pas fallu demander à beaucoup pour en apprendre le nom ! Qu'en voulez-vous savoir ?

--- Autant que vous pouvez en dire... »

Fronçant le sourcil à la gravité du ton d'Ernaut, le père Danyel prit le temps de rassembler ses idées.

« Je n'ai jamais eu à parler avec lui, mais il est de bonne fame et ne se déplace nulle part sans avoir reçu congé du roi des Griffons. Il est arrivé céans voilà quelques mois, offert par Manuel à nos territoires pour en rehausser la beauté par ses talents.

--- Quelques mois ? Qu'est-ce à dire ?

--- Il est arrivé après l'été, de mémoire, mais je ne saurais en dire plus. J'ai surtout souvenance que le père Waulsort en était fort enthousiaste. Il allait voir ses projets magnifiés par les doigts agiles de ce maître œuvrier.

--- Ils se connaissaient donc ?

--- De prime, je ne sais, mais vu que le père Régnier traçait les cartons pour tous les ymagiers, il échangeait forcément avec mestre Zénon. D'autant que ce dernier, appartenant à une autre Église, était sûrement au fait des innovations voulues par le père.

--- Vous n'étiez pas en accord avec le père Waulsort ?

--- Je ne suis pas tant savant qu'il l'était. Outre, je n'ai pas fréquenté les chœurs de Cluny comme lui et je trouve un peu trop dispendieux de payer à prix d'or un artisan pour parer nos églises. Sans compter que ces simples manouvriers ont caprices de barons.

--- Avez-vous un exemple précis en tête de ces exigences ?

--- Je n'ai que ça, mon pauvre ami. Il faut lui accorder autant d'ouvriers qu'il en fait demande. Il a droit à plus de repos, est logé à nos frais, va et vient à son gré... J'en passe tant et plus. Il a même exigé de ne travailler qu'avec ses propres mortiers, qu'on lui achemine à prix d'or depuis Byzance. Et lorsqu'il s'est blessé voilà quelque temps, il a exigé de se faire soigner par un des siens et pas par notre frère apothicaire comme il est d'usage. »

Voyant l'intérêt d'Ernaut pour un simple artisan, le père drapier se tint coi un moment, clairement circonspect sur ce qu'il devait penser de l'échange. Il semblait en redouter les implications et se résolut donc à ne pas demeurer sur une vague impression.

« Aurait-il quoi que ce soit à voir avec toute cette histoire ?

--- Je ne sais. C'est pour le moment juste une piste que je me dois de flairer. »

Ils furent interrompus par le son clair de la cloche qui appelait chacun à l'arrêt de son travail et les chanoines à la prière. Par réflexe, le père Danyel se leva brusquement, le visage soudain ennuyé. Ernaut ne voulut pas qu'il aille propager ce qui n'était encore qu'une possibilité.

« Ne prenez pas ombrage de ce que je vous ai demandé, mon père. Quand bien même il aurait été mêlé à tous ces incidents, rien n'indique qu'il ait eu part à quelque perfidie. Il pourrait avoir sa place en toute autre partie. »

En disant cela, Ernaut était bien conscient de mentir. Il lui apparaissait que le mosaïste avait toutes les caractéristiques de la proie qu'il pourchassait depuis des jours. Tout semblait le désigner, y compris, et c'est là le plus accablant pour le jeune homme, sa familiarité avec les légers échafaudages du sanctuaire. D'instinct, cela fit naître en lui une colère dont il savait qu'il lui faudrait la laisser s'exprimer.

Accompagnant le père Danyel jusqu'au cloître, il l'y abandonna pour monter rejoindre Raoul. Il se demandait si le scribe avait pu trouver mention de Zénon dans les écrits de Waulsort. Selon Ernaut, s'il pouvait être établi que l'artisan avait prêté la main aux recherches du chanoine, l'affaire serait entendue.

Quand il pénétra dans la pièce, son air sombre n'échappa point à son jeune compagnon, qui choisit prudemment de demeurer loin de l'orage, replongeant le nez dans ses papiers. Il sursauta presque lorsqu'Ernaut brisa le silence d'une voix dure.

« As-tu connoissance d'un certain Zénon dans les écrits de Waulsort ? Ou d'un homme venu de Morée ?

--- Cela ne me dit rien... Est-ce là un ancien savant ? Un ami du chanoine ?

--- Il est vif et bien vif, et a peut-être ourdi toutes ces meurtreries. Tout m'incite à le croire, mais sans rien pour l'accuser, il lui sera facile de se prévaloir de son statut pour m'échapper.

--- L'accuser ? Tu en es déjà là ? N'avons-nous pas pour mission de collecter sans agir ? »

Ernaut lança un regard courroucé à Raoul, mais ne sut lui tenir rigueur de cette remarque si bien tournée. Le visage de son compagnon lui semblait si innocent qu'il ne put s'empêcher de sourire et d'adoucir son ton.

« Tu parles de raison compère. Je vais trop vite en besogne. Disons qu'avoir solides éléments pour démontrer mon idée aiderait à convaincre le sénéchal du comte de Jaffa.

--- Quoique cela ne me dise rien, je ne saurais jurer qu'un tel nom ne m'a pas échappé jusqu'alors, mais j'y porterai attention d'ores en avant. »

Il s'arrêta brusquement, comme s'il hésitait à parler, puis reprit, d'une voix mal assurée.

« S'il a pris part à ce que nous pensons, pourquoi donc le père Waulsort l'aurait-il mentionné ? Ils auraient été adversaires, pas amis...

--- Je ne sais encore quel fut son rôle, ni même s'il en tint l'un quelconque. »

La voix d'Ernaut s'éteignit tandis qu'il parlait tout en réfléchissant à ces derniers développements. Puis il inspira un grand coup avant de reprendre la parole.

« J'entends ce que tu dis, mon ami. Si je peux trouver qu'il dissimule poudre de lune en ses affaires, tout sera simple. Il a peut-être poussé l'impudence jusqu'à s'en faire porter par les frères chanoines, devenus complices sans le savoir de la plus odieuse des machinations.

--- J'ai peur de comprendre...

--- N'aie crainte, j'en fais mon affaire. Il te faut préparer tes paquets et finir au plus vite tes travaux ici. Demain tu sauteras peut-être en selle porter des nouvelles de notre part à l'hostel le roi en Jérusalem et, surtout, au sire sénéchal du comte de Jaffa.

--- Et toi ?

--- Moi, je vais aller nuitamment voir si je ne peux lever un lièvre. De cela dépendra peut-être ton départ dès potron jacquet[^40]. »

Sans préciser plus avant, il laissa là son ami et descendit au réfectoire, où il obtint un peu de pain et du poisson séché en guise de collation. Il se rendit ensuite dans la chambre. Il avait l'intention de s'accorder du repos avant une tâche dont il pensait qu'elle serait plus discrète en pleine nuit. Il avait jusqu'alors grogné de se faire éveiller toutes les nuits par les cloches appelant pour vigiles, mais il comptait cette fois en profiter pour mener à bien une petite incursion, dont il espérait, bien naïvement eu égard à son gabarit, qu'elle serait furtive.

Excité par la situation, il ne réussit pas à vraiment dormir et discuta un peu avec Raoul au moment où celui-ci vint à son tour pour se coucher. De nombreuses hypothèses encombraient son esprit et il se réveilla à plusieurs reprises en sursaut, croyant avoir entendu les cloches. Ce fut donc avec un certain soulagement et un peu de fatigue qu'il se mit en marche au milieu de la nuit, lorsqu'elles résonnèrent pour de bon.

Le sanctuaire restait ouvert afin de permettre à ceux des voyageurs qui le désiraient d'y faire des oraisons nocturnes. Plusieurs valets y participaient à la garde, partageant quelques grabats qu'ils tiraient dans un coin au matin. Tout en veillant à ce qu'aucune dégradation ne soit faite, leur tâche était de surtout de s'assurer que les lampes et bougies ne créassent pas d'incendie. Ils mouchaient aussi régulièrement les cierges payés par de riches donateurs, dont il avait été demandé qu'ils demeurent illuminés de tout temps.

Ernaut se rendit aux latrines, où il s'installa un moment, ne souhaitant pas croiser les ecclésiastiques lors de leur office. Après une attente impatiente qui lui parut durer une éternité, et dont il escomptait qu'elle soit suffisante, il traversa la cour pour accéder à l'église par l'entrée latérale depuis le cloître. Une lune en son premier quartier peignait d'un blanc laiteux les plantes et les décors sculptés des chapiteaux. Il s'approcha de la porte de l'édifice et n'entendit pas de chant. Il poussa l'huis doucement, dans l'espoir de se faire remarquer le moins possible.

Il devait impérativement franchir l'espace nu du transept pour se rendre vers la loge des artisans. Bien qu'ils travaillassent dans le chœur, leurs affaires étaient entreposées dans la partie occidentale du bâtiment. Même si le secteur oriental voyait ses décors en cours de réalisation, on n'y apercevait plus que de légers échafaudages disposés pour ne pas gêner les offices religieux.

Nul pèlerin n'avait souhaité profiter du privilège de dormir au plus près de la tombe de saint Georges, le lieu était calme et seules les chapelles scintillaient d'une chaude lumière. Une toux y résonna sous leur voûte. Ernaut se faufila lentement le long des murs, demeurant dans la nef latérale dans l'espoir d'être moins visible. Il marchait doucement, respirant la bouche grande ouverte. Il entrevit quelques formes allongées au pied d'un des massifs de colonnes, dans la partie méridionale. Il se dépêcha d'avancer jusque dans les ténèbres des ateliers et du chantier.

Il savait que, si des hommes du guet faisaient des rondes à l'extérieur, personne ne venait inspecter la zone durant la nuit. Les artisans gardaient leurs outils avec eux ou les enfermaient dans de solides coffres bardés de fer. Il ne s'offrait là rien d'utile à chaparder. Arrivé dans la partie du bâtiment où peintres et mosaïstes entreposaient des affaires et partageaient une petite loge, il ralentit le pas. L'endroit était particulièrement sombre et il lui fallut un long moment pour que ses yeux commencent à y discerner des formes.

La zone des mosaïques était facile à repérer, avec les casiers de tesselles de différents matériaux préparés par les commis. Les plus beaux, parfois recouverts d'or, étaient enfermés dans des coffres-forts, mais les plus communs étaient ici, triés par teinte ou par usage. Plusieurs jarres contenaient les mortiers fins qui servaient à enduire les murs et à fixer les carrés colorés. Ce qui l'intéressait tout particulièrement, c'était la grande armoire de chêne derrière tout cela. Il avait vérifié qu'elle demeurait close à tout moment et Hashim lui avait confirmé avoir vu Zénon l'ouvrir à l'aide d'une clef qu'il nouait à sa ceinture. C'était l'endroit parfait où conserver de l'escarboucle.

Caressant du bout des doigts la solide plaque d'acier qui protégeait la serrure, Ernaut sortit de sa besace un petit crochet qu'il avait confectionné avec un clou. Adolescent, il avait jalousé Droin qui était parvenu à déverrouiller un cadenas. Il en gardait le souvenir qu'il suffisait de tourner son outil avec douceur. Remuant son ustensile en tous sens sans rien comprendre à ce qui se passait, il découvrit rapidement que ce n'était pas là un exercice aisé. Droin avait dû grandement s'entraîner avant de réaliser son exploit. Il avait feint la facilité, désireux de briller devant eux.

Il tenta chacune des serrures du meuble, l'une après l'autre, sans jamais plus de chance. Il préférait ne pas trop s'acharner chaque fois, dans la crainte de dérégler quelque chose dans les mécanismes qui puissent mettre la puce à l'oreille du maître mosaïste. Pestant entre ses dents, il finit par admettre qu'il n'arriverait à rien et repartit du sanctuaire aussi discrètement qu'il y était entré. Heureusement, le valet qui tenait lieu de vigie était endormi, la tête sur l'autel, et ronflait paisiblement.

Lorsqu'il retrouva sa couche, Ernaut ne put trouver le sommeil rapidement tellement il était agacé. Malgré l'absence de nouveaux éléments, il se convainquit peu à peu de la culpabilité de Zénon de Morée et de l'urgence à en informer le sénéchal Guy. S'ils ne savaient pas qui avait commandité les travaux, au moins pourraient-ils en apprendre plus de celui qui avait tué pour protéger le secret. Encore fallait-il s'en emparer avant qu'il ne disparaisse.

### Lydda, hôtellerie du palais épiscopal, matin du jeudi 1er janvier 1159

La nuit avait paru sans fin pour Ernaut, qui se réveillait sans cesse en se tournant sur sa couche. Alors que l'aube pointait à peine, il s'était rendu aux bacs pour faire un peu de toilette, partageant l'endroit avec les plus matinaux des domestiques. Puis il prit son temps pour avaler une bouillie mêlée de raisins secs. Il vit arriver et repartir plusieurs groupes, de voyageurs et artisans. Quand ces derniers furent tous sortis, il quitta le lieu et avança en direction du sanctuaire. L'église commençait à s'animer de l'activité quotidienne.

Il alla directement vers la zone où travaillaient les mosaïstes et se planta au milieu, poings sur les hanches, pour en admirer les réalisations. Ayant vérifié que Zénon de Morée était dans les environs, il s'enquit de façon bruyante du nom du responsable de ces décors auprès d'un des aides. Lorsqu'il dirigea ses pas vers l'artisan, celui-ci avait entendu qu'il était question de lui. Ernaut le salua poliment et lui expliqua qu'il était là au service du roi afin de déterminer de possibles embauches sur de futurs chantiers de la couronne.

« On me demande si souventes fois. J'ai plutôt usage de décorer églises que palais.

--- Je ne doute que vos accomplissements doivent vous attirer force commandes. Pourrais-je évoquer votre nom auprès le sénéchal si d'aventures l'hostel le roi avait des projets dans vos cordes ?

--- Je serais honoré de me mettre au service de votre roi. Le *basileus* aime à ce que nous partagions nos savoir-faire avec nos amis et frères en religion. »

Tout en parlant, Ernaut se tournait ostensiblement vers les décors, mais du coin de l'œil, il étudiait en détail le mosaïste, espérant découvrir une preuve physique de sa culpabilité. Il continua à alimenter la conversation en banalités, jouant lourdement son rôle de plénipotentiaire au service des projets architecturaux royaux. Sans avoir vraiment trouvé de confirmation satisfaisante de ses soupçons, il se résolut à laisser maître Zénon retourner à ses tesselles. Quand il lui tendit la main pour prendre congé, il remarqua que l'artisan avait celle de droite grossièrement emmaillotée dans des linges. Il crut même discerner un fugace rictus de douleur lorsqu'il la serra avec son énergie coutumière.

Finalement enchanté de cette initiative osée, il se contint jusqu'au cloître, marchant sans donner l'impression qu'il était pressé, puis, une fois hors de vue depuis la nef, il bondit littéralement en direction des cellules où Raoul devait finir de préparer ses affaires.

« J'ai longuement pourpensé à cette histoire au long de la nuit. Tu vas chevaucher aussi vite que tu le peux pour porter message au sire Guy. Je ne peux m'absenter, dans la crainte que celui que je pense coupable ne s'échappe.

--- Tu as trouvé quelque irréfutable élément ?

--- J'ai échangé quelques mots avec lui et je viens de voir qu'il a blessure à la main, peut-être en manipulant la poudre de lune. Le teinturier m'a confié qu'elle était fort friande des chairs.

--- Cela pourrait aussi être anodine blessure récoltée au chantier.

--- Peut-être, je n'ai pas voulu en discuter avec lui de crainte qu'il ne comprenne que mon réel intérêt n'allait pas aux mosaïques. Tout semble indiquer qu'il a commis ces méfaits. Nous trouverons tout ce qu'il faut pour l'en convaincre. Une fois en notre pouvoir, il se trouvera possiblement quelques indécis qui oseront alors jurer de ses coupables agissements. »

Raoul se frotta les yeux, peu désireux de créer une querelle. Malgré toute l'amitié qu'il avait pour Ernaut, il ne voulait néanmoins pas compromettre cette importante mission.

« Avons-nous suffisamment de choses pour accroire cela ? Le sénéchal aura sûrement pressantes questions et je me vois mal lui dire que nous n'avons rien de solide, nul témoin de quoi que ce soit.

--- Je comprends tes craintes et je les partage. Il me tarde certes de rendre justice envers le pauvre Herbelot, mais je n'agis pas sans avoir longuement pourpensé. Nous sommes pour lors à la croisée des chemins. Nous n'avons aucune nouvelle piste à suivre et il se trouve que mestre Zénon a toutes les apparences du goupil que nous chassons. Il faut en rendre compte, car nous n'avons pas gré d'agir, ainsi que tu l'as dit hier. N'oublie pas qu'il a peut-être caché quelque part le fruit de ses rapines. Il pourrait tout à fait partir et s'en serait fini alors de cette histoire. Le sénéchal n'aurait pas ce à quoi il aspire et moi je n'aurai pas eu vengeance pour mon compère. Il nous faut donc obtenir agrément de notre conduite. D'où ton urgent département tandis que moi je ferai en sorte de garder notre suspect à l'œil. »

Voyant que ses explications rassuraient Raoul, Ernaut lui accorda un sourire, qui se teinta de ruse tandis qu'une nouvelle idée se formait dans son esprit.

« Porte tout ce que tu as trouvé, qui n'est pas de grande importance et narre-lui surtout que nous pourrions peut-être récupérer ce que le murdrier aurait robé après avoir occis Waulsort et son valet.

--- Ne crois-tu pas qu'il a tout détruit plutôt ?

--- À dire le vrai, je ne suis certain de rien. Ses maîtres romains pourraient avoir envie de découvrir ce qu'un érudit comme le chanoine aurait inventé, pour possiblement améliorer leur propre feu de guerre. Mais si nous n'offrons nul espoir en cela au sénéchal, il se pourrait qu'il dédaigne l'affaire. Et laisse partir le mosaïste. »

La contrariété que cette simple éventualité faisait naître en Ernaut s'afficha alors sur son visage de façon si nette que Raoul n'eut pas le front de remarquer autrement qu'en pensée que c'était dangereux de jouer au chat et à la souris avec puissant baron. Il se sentit soudain bien penaud. Comprenant le désarroi de son ami, Ernaut s'efforça de faire bonne figure.

« Ne penses-tu pas qu'il serait fort malséant qu'un tel maufaisant soit impuni ? Nous avons les mains liées et devons rendre compte. Faisons en sorte que nous puissions agir en bien une fois nos maîtres éclairés sur la situation.

--- Toi qui es fils de vigneron, n'as tu pas appris que *Trop bas percer rend le vin trouble* ? C'est jouer avec le feu, nul baron n'aime à se savoir trahi par sien valet.

--- Qui te parle de trahir ? J'entends juste lui faire comprendre ce que je crois être vrai, au plus profond de moi. Je n'ai rien à lui offrir si ce n'est la fiance qu'il peut mettre en mes instincts. Mais n'est-ce pas pour cela qu'il m'a mandé ici ? Le chasseur croit le nez de son chien, quand bien même il n'en comprend pas tous les subtils usages. »

Dans son excitation, Ernaut arpentait la petite pièce en s'exprimant. La veille nocturne n'avait pas amélioré son humeur et il était intimement convaincu qu'il tenait le coupable des forfaits.

« Explique au sire Guy tout cela, selon tes mots. Je suis à ses ordres, ainsi que le sénéchal le roi me l'a indiqué et je n'agirai sans. À lui de me dire s'il préfère que nous dénoncions ce murdrier à l'évêque, l'archevêque ou le patriarche, aux fins qu'ils le livrent au bras séculier, vicomte, comte ou roi, pour ses crimes de sang. Il sera alors bien temps de lui arracher tous ses secrets sur le feu de guerre griffon. Puis nous apaiserons les âmes de ses pauvres victimes par juste châtiment.

--- Et s'il n'a rien à voir en tout cela ?

--- Il sera aisé pour lui de donner gage clair de ses agissements devant ses juges. S'il est honnête œuvrier, il en sera quitte pour quelques journées de perdues. »

Comprenant qu'il ne servait à rien de tenter de raisonner son compagnon, Raoul se mit rapidement en chemin. Après l'avoir salué une dernière fois, Ernaut s'activa à rouler son turban sur sa tête. Il devait donner de nouvelles instructions à Hashim pour les jours qui venaient, dans l'attente des directives de Guy le François. Il ne fallait pas perdre le mosaïste des yeux.

Chapitre 9
----------

### Lydda, palais épiscopal, fin d'après-midi du samedi 3 janvier 1159

Ernaut rongeait son frein depuis le départ de son compagnon, tout en s'efforçant de rendre son rôle crédible. Grâce à Hashim, il avait pu établir que Zénon de Morée avait un logement dans le quartier des Byzantins, chez une famille où il habitait dans une petite pièce en fond de cour. En outre, rien ne trahissait en lui l'espion ou le saboteur, il se contentait d'aller et venir sur le chantier, s'occupant avec passion de ses décors.

Ernaut avait vu avec soulagement le retour de Gerbaut, dont le bras semblait guérir. Il n'osa pas trop poser de questions sur le mosaïste, que le manouvrier ne connaissait que peu. Il souhaitait aussi se débarrasser d'une idée qui le taraudait, même si elle lui paraissait absurde : il avait besoin de s'assurer que ce ne pouvait être le portefaix qui ait occis tout le monde. Ils partagèrent donc une bière un soir à une taverne voisine, sans que le discret Gerbaut se dévoile outre mesure. Il disait avoir beaucoup voyagé, travaillant la pierre et ayant à l'occasion dirigé des chantiers, mais il avait connu un drame familial qui l'avait jeté sur les routes. Il se contentait de labeurs annexes, de besognes de journalier afin de conserver sa liberté. Tout en échangeant, Ernaut put s'assurer que sa blessure n'était pas une brûlure, ce qui le soulagea fort pour la suite de l'entretien. Il glissa également quelques remarques de temps à autre dans le but d'étayer les raisons officielles de son retour. Il était persuadé que cela alimenterait les commentaires qui ne manqueraient pas de naître autour de sa présence.

Il était dans sa cellule, attendant la fin de journée pour aller se promener turban sur la tête pour écouter le rapport de Hashim quand on vint le chercher pour se rendre chez l'évêque. Il était étonné de cette convocation, n'ayant rien eu à faire avec Constantin depuis son arrivée. Il fut d'autant plus circonspect qu'il ne fut mené ni dans la grande salle ni dans la chambre, mais dans une petite pièce où de lourds volumes s'entassaient sur des étagères. Au milieu d'eux se tenait Guy le François admirant un imposant codex aux pages décorées en compagnie de l'évêque Constantin. Voyant sa surprise, le sénéchal accueillit Ernaut d'un sourire.

« Voilà notre intrépide limier, qui déniche merles quand on l'envoie quérir moineaux... »

Ernaut s'inclina avec respect et eut soudain l'affreuse impression que Raoul n'avait peut-être pas été assez habile dans son rapport. Le sénéchal du comte Amaury ne semblait pas en colère, mais cela n'augurait pas forcément du bien, vu l'ambiguïté de sa remarque. C'était toutefois l'évêque qui paraissait le plus perturbé. On l'aurait dit ennuyé plus qu'agacé, mais sa face ronde ne trahissait que difficilement ses sentiments. Il n'ouvrit néanmoins pas la bouche, laissant la parole à Guy le François.

« Nous ne pouvons demeurer céans trop longtemps sans attirer l'attention. Alors tu vas ouvrir grand tes oreilles et bien tenir en mémoire ce que je vais te conter. »

Voyant que son numéro autoritaire arrivait à inquiéter l'énorme masse de muscles face à lui, Guy s'accorda un sourire carnassier.

« Tu as, semble-t-il, découvert ce qui pourrait, et j'insiste bien sur ce mot *pourrait*, être une terrible malfaisance en nos provinces. Nul ne saura jamais ce sur quoi travaillait au juste ce pauvre chanoine, mais il est fort possible qu'il se soit aventuré en bien périlleuses recherches. Selon toi, celles-ci auraient déplu au pouvoir de nos frères griffons qui auraient dépêché une de leurs espies pour se garantir que leur secret du feu de guerre ne soit mie découvert par tierce personne. »

Il se tourna vers l'évêque, qui, bien que clairement désemparé, hocha la tête en assentiment.

« Hors cela, nous n'avons nulle certitude que ce possible maufaisant soit le mestre mosaïste Zénon de Morée, quand bien même tu as collecté à ses dépens bien lourds, et je crois, logiques soupçons. Mais logique n'est pas justice. Quand bien même tu aurais toutes les garanties, il ne serait guère profitable ni au roi Baudoin ni à mon maître Amaury que nous fâchions nos naturels alliés griffons alors même que nous ne sommes acertainés de rien. Nous n'allons donc rien faire contre lui qui demeure, pour ce que nous en savons officiellement, un honnête et fort talentueux mosaïste, gracieusement envoyé au roi Baudoin par son désormais cousin le basileus Manuel[^41]. »

Constatant le dépit que cette déclaration faisait apparaître sur les traits d'Ernaut, le sénéchal adopta un ton autoritaire pour continuer.

« Tu as donc tes ordres : tu demeureras céans encore une paire de jours, aux fins que personne ne voie mon passage coïncider avec ton département. Puis tu rentreras en l'hostel le roi à Jérusalem, sans plus parler de toute cette affaire à quiconque sans y avoir été autorisé. M'as-tu bien entendu ? »

La gorge nouée par l'émotion, Ernaut hocha le menton, incapable d'émettre un son. Soucieux de mettre un peu de miel dans le vinaigre de son discours, Guy enchaîna d'une voix plus amène.

« Tu as fort bien œuvré en toute cette affaire. Montre-toi aussi obéissant que tu as été malin jusqu'à présent et nous pourrons tous convenir que tu as bien servi le roi ton maître. Tu peux disposer. »

Incapable d'en endurer plus, Ernaut salua silencieusement et sortit de la pièce comme un somnambule en plein cauchemar. Il avait tant escompté du dénouement de cette affaire, certain d'y avoir fait de son mieux qu'il s'attendait à ce qu'on lui accorde de venger la mémoire de son ami. Jamais il n'aurait cru qu'on lui intimerait de façon aussi injuste de mettre un terme à tout cela. Le ressentiment y naissait de la tristesse qu'il ressentait à une pareille iniquité. Quand il sortit du palais, il serrait les poings et la mâchoire à s'en faire mal, espérant trouver un exutoire à sa rage, fut-ce le plus minable des prétextes. Heureusement pour les présents, sa fureur était si évidente que chacun s'esquivait en le voyant arriver. Il en fut quitte pour aller se défouler sur le matelas de sa couche, frappant l'enveloppe de tissu jusqu'à l'exploser et en faire voler les fétus de paille. Il se jeta alors au sol, grognant tel un ours blessé, inapaisé malgré les coups portés et rumina sa colère un long moment. Sans envisager la nuit qui s'installa peu à peu, pour enfin se retrouver dans le noir complet.

La faim le ramena à la réalité. Il se leva, indifférent au spectacle de sa chambre dévastée et sortit de la pièce. Il était certainement trop tard pour se faire servir au réfectoire, mais il se trouverait possiblement une taverne ouverte quelque part dans la cité. Il noua son épée à sa hanche, enfila une chape et vérifia la bourse à son braïel, puis quitta l'hôtellerie. À son grand désarroi, les hommes de guet à la porte donnant en ville lui expliquèrent qu'ils ne pouvaient le laisser sortir vu l'heure. Inquiets d'opposer un refus définitif à un colosse à l'allure si peu enjouée, ils lui proposèrent de partager un pichet avec eux. Ils n'avaient pas grand-chose à faire, n'ayant pour mission que de garantir que personne n'entrait nuitamment dans les lieux. Comme il était peu probable qu'un assaut soit porté depuis la cité, ils se contentaient de jouer les concierges pour les patrouilles et les importants visiteurs qui auraient besoin d'aller et venir.

Leur accueil simple et amical et leur humour grivois lui permirent de ne pas se complaire seul dans son malheur. Cela lui coûta quelques deniers aux dés, mais au moins finit-il la veillée avec moins d'amertume qu'il ne l'avait commencée. En outre, il évita ainsi la tentation de se rendre jusqu'au logement du mosaïste pour lui faire expier ses péchés manu militari.

Les vigiles avaient sonné depuis un bon moment quand il se décida à regagner son lit, remerciant ses compagnons de la soirée. Lorsqu'il retrouva sa chambre saccagée, son acrimonie revint et il s'allongea sur la couche de Raoul, la tête emplie de pensées chagrines.

Le lendemain, il s'accorda une grasse matinée, ne s'extrayant des draps que pour se faire raser avant la messe. Il prit également du temps pour ses ablutions, goûtant l'eau froide sur sa peau. Voyant un timide soleil s'annoncer pour la journée, il hésita un moment à se sécher au-dehors, mais renonça devant le vent. Sa colère était retombée dans la nuit, mais son aigreur avait cru d'autant. Depuis qu'il avait rejoint le service du roi, c'était la première fois qu'il était ainsi désavoué dans ses ambitions. Il avait pensé que la Terre sainte saurait lui offrir tout ce qu'il espérait et son dépit était à la hauteur de ses expectations. Il se sentait las. Il avait soudain une furieuse envie d'aller retrouver Libourc, de s'établir avec elle en un manse loin de la Cité et de se contenter de travailler la terre. Elle, au moins, ne mentait jamais.

### Lydda, hôtellerie du palais épiscopal, matin du lundi 5 janvier 1159

Ernaut avait rassemblé ses quelques affaires pour reprendre la route de Jérusalem et se rendait à l'écurie pour y préparer sa monture lorsqu'il perçut un regard qui s'accrochait à lui. Il chercha dans la cour sans pourtant identifier personne. Puis ses yeux s'aventurèrent au-delà du portail d'entrée depuis la ville, grand ouvert en ce clair matin. Il y découvrit Aymar appuyé à un mur, grattant son gros nez comme il en avait le tic. Surpris de sa présence, Ernaut alla droit en sa direction, mais celui-ci s'esquiva quand il le vit s'approcher, sans même avoir échangé un mot avec lui. Il déambulait sans se presser, mais ne manifestait nullement l'intention de s'adresser à lui.

Ernaut comprit qu'il lui fallait le suivre ainsi qu'il marchait, sans donner l'impression qu'ils avançaient de concert. Ils pénétrèrent dans le cœur de ville, comme si chacun était occupé à ses affaires propres, tout en s'assurant par de rapides regards qu'ils ne se perdaient pas. Ils aboutirent dans un souk où tailleurs, fripiers et négociants de toile se partageaient les échoppes.

Voyant qu'Aymar faisait mine de s'intéresser aux habits disposés dans des paniers devant un marchand de vêtements d'occasion sans aller plus loin, Ernaut vint à ses côtés.

« Emprunte l'escalier dans mon dos et frappe à la porte une série de coups rapides. Tu y es attendu. »

Sans attendre de réponse ni préciser quoi que ce soit, Aymar reposa le thawb qu'il était en train de regarder et s'éloigna vers une autre boutique. Ernaut prit le temps de discuter un peu avec le vendeur, puis se décida à monter les quelques marches. Tout en espérant que ce n'était pas là un piège, il serra la poignée de son épée peut-être plus que de raison.

À peine eût-il toqué que la porte s'ouvrit pour dévoiler un faciès carré de soldat. Le nez brisé, le regard suspicionneux et le front bas s'harmonisaient avec le gambison de toile fine, sali et bruni par les années et la chevelure à l'écuelle rasée sur l'arrière. Le tout posé sur des jambes qui semblaient rouler à chaque pas tant elles étaient arquées. La voix était par contre étonnamment fluette et aigüe.

Il s'enquit de l'identité du visiteur et, s'étant fait confirmer qu'il s'appelait Droin, le laissa entrer. La pièce était toute en longueur, surplombant la boutique à qui elle avait dû servir de réserve un temps. Un petit volet percé d'une claire-voie apportait la seule lumière. L'homme fit un geste à un comparse du même tonneau, qui attendait à côté d'une porte au fond. Celui-ci disparut en un éclair.

Puis le soldat se mit en devoir de se curer les dents avec soin, sans quitter des yeux Ernaut. Ce dernier faisait mine de ne pas être impressionné, mais la carrure et les attitudes du sergent lui semblaient autrement plus imposantes que tout ce qu'il avait vu des compagnons d'Aymar jusqu'à présent. Il n'eut heureusement pas longtemps à patienter, le second homme vint le chercher sans plus de parole. Bien qu'il fût plus jeune et parût avoir moins blanchi sous le harnois, il portait tout de même épée et tenue rembourrée de guerre, et bougeait avec l'assurance d'un soldat. Il mena Ernaut à travers quelques pièces et courettes jusqu'à une salle de bonne taille.

Percée de plusieurs fenêtres où la lumière pénétrait au travers de décors de bois sculptés, elle était pavée de carreaux vernissés et les murs chaulés s'étaient vu adjoindre quelques peintures géométriques. Le mobilier en était rudimentaire : pour l'essentiel des tapis accumulés sur des nattes de palmier et quelques matelas. Il s'y trouvait un grand coffre bardé de métal sur lequel était assis, de côté, un homme au physique délié. Bien qu'il fût très mince, les joues creuses habillées de barbe flanquant un nez fin légèrement busqué, il fit à Ernaut l'impression de n'être que muscle et tendon, empli d'énergie. Il était vêtu d'un bliaud de très belle qualité et jouait avec une croix qu'il avait suspendue autour du cou. Ses cheveux bruns frisés étaient bien coupés quoiqu'un peu courts pour la mode chez les galants, ce qui pouvait trahir une habitude à porter le heaume et la coiffe de mailles. Il toisa Ernaut un petit moment, un sourire matois dansant sur ses lèvres, sans dire un mot. Puis, voyant un éclair de compréhension dans le regard de son invité, il brisa enfin le silence.

« Je vois que tu as mis un nom sur ma personne, jeune... Droin. Ou devrais-je dire Ernaut ? Penses-tu qu'il y ait tant de jeunes gens de ton gabarit en le royaume que tu ne puisses être aisément reconnu ? Mais tu as bien agi, prouvant que tu n'étais pas muscle sans cervelle. »

Il envisagea en détail Ernaut encore un moment, puis reprit tout à trac.

« Je t'ai mandé aux fins que tu ne fasses pas erreur sur ton office en cette histoire. »

Ernaut se demandait quel rôle avait joué l'important baron devant lui. Il avait suffisamment souvent fréquenté la cour autour de la reine mère Mélisende pour ne pas douter que c'était Philippe de Naplouse, ou de Milly comme on l'appelait parfois. C'était un des principaux seigneurs de Terre sainte, proche du pouvoir royal depuis plusieurs décennies. C'était aussi, selon les rumeurs, un chef de guerre expérimenté et un chevalier de grande bravoure. Jamais Ernaut n'aurait pensé qu'il pouvait prêter la main à de tortueuses menées.

« Peut-être ne le sais-tu pas, mais la couronne de Jérusalem est fort mal allant. Il n'est de jour qui ne voie un de ses ennemis lever des troupes contre elle, ou un de ses alliés négocier son amitié à prix d'or. Le chanoine participait à sa façon à la puissance du bras de notre roi, sans que celui-ci n'en sache rien. »

Il marqua une pause, lâchant sa croix pour se tourner face à Ernaut.

« Il est parfois nécessaire que la main dextre ignore ce que fait la senestre, fort cyniquement nommée. »

Il se tut de nouveau un moment, examinant les réactions du jeune homme face à lui. Son regard avisé, habitué à évaluer les soldats qu'il menait au combat, se plissa de ruse lorsqu'il reprit la parole.

« Ce jour, il est en ton pouvoir de faire rendre gorge au félon qui nous a si traitreusement assailli, faisant en sorte que tout cela ne soit pas qu'inutile gâchis. »

Comprenant que Philippe de Milly lui demandait l'exact inverse de ce qui lui avait été ordonné, Ernaut s'agita un peu et fut invité par un geste à s'exprimer.

« Je ne souhaite certes pas contrarier vos humeurs, sire. Mais on m'a intimé de ne rien faire.

--- Tout à fait, et devant bon témoin, un évêque digne de foi prêt à en jurer de sa main annelée sur les Saintes Évangiles ! N'as-tu point entendu ce que je t'ai exprimé dès avant ? »

Philippe de Milly se leva soudain et s'approcha d'Ernaut. Bien qu'il en fît la moitié du gabarit, il en égalait presque la taille et compensait par une intense énergie qu'il mettait dans chaque geste. Il posa une main amicale sur l'épaule du jeune géant.

« Tu as bonne fame en l'hostel du roi et pourrais fort bien être voué à importantes missions. Mais il va te falloir faire un choix, jeune Ernaut, dont tu ne pourras revenir. Es-tu prêt à servir la couronne à n'importe quel prix, fut-ce celui de ton honneur ? Il est facile de disposer de sa vie ainsi que tu l'as déjà juré. C'est là bien modeste offrande puisqu'elle s'accompagne de la certitude d'éternelles louanges. Autrement rude et absolu est le serment de celui qui n'hésite pas au seuil du pas périlleux de l'âme. Es-tu de cet acier dont on modèle Durandal ou Joyeuse ? Ou simple ferraille destinée au canif dont le vilain tranche son quignon ? »

Ernaut se mordit la lèvre. Il avait envie de croire en l'invitation du puissant baron, mais la méfiance demeurait. Il trouvait cela trop discret, trop sournois, sans éclat ni vaillance. Quoiqu'en dise Philippe de Milly, il se sentait à des lieues des Roland, Olivier et autre preux de Charlemagne.

« Il me serait aisé de te rappeler que tu obtiendrais vengeance pour ton ami, ce jeune clerc. Mais ce n'est pas là vassal dont le royaume a besoin. Ce qu'il lui faut, ce sont des hommes assurés de servir de loyale façon, ayant au plus profond d'eux le désir de bien agir, droitement et sans peur, confiants en une cause qui les dépasse. »

Il retourna s'asseoir et croisa les mains, se penchant en avant.

« Je ne saurais t'ordonner en rien ce qui adviendra. Toi seul peux choisir de sortir de ces méfaits peut-être quelque bien. Et si d'aventures tu obtenais quelque utile information sur un projet de feu de guerre, n'hésite pas à te présenter en mon hostel à Jérusalem, tu y seras entendu discrètement. »

Ernaut n'avait pas bougé, médusé par les déclarations du puissant seigneur de Naplouse. Il déglutit lentement, sentant que les mots s'arrachaient péniblement à sa gorge.

« Je pourrais mener Aymar...

--- N'en dis pas plus, je ne veux rien savoir des détails. Mais quoi qu'il advienne, ce sera de ton fait seul. On prête suffisamment de méshonnêtes agissements à mon hostel pour ne pas apprendre qu'un des miens a créé un différent avec le puissant cousin griffon de notre roi Baudoin. »

Il laissa ses paroles en suspens un moment puis agita la main.

« Nous en avons fini, tu peux te retirer. »

Un peu hébété, Ernaut salua et se retira, guidé de nouveau par les soldats mutiques. Lorsqu'il retrouva le souk, il se sentait désorienté, comme s'il sortait d'un long sommeil ou avait trop bu. Il erra un temps en ville puis repartit vers le palais. Impatient de se mettre en selle, il se hâta de finir de préparer ses affaires. Il lui fallait quitter cet endroit au plus vite. Il hésita un moment à prendre la route du sud, qui passait par la petite Mahomerie, où Libourc saurait l'apaiser, mais il n'eut pas le cœur d'aller la voir dans un tel état de confusion. Il ne souhaitait pas dévoiler son désarroi ni laisser entendre qu'il avait des soucis dans son service. Sa belle-mère n'approuvait déjà pas qu'il porte les armes, elle n'aurait certainement pas manqué de pointer du doigt les nouvelles incommodités qu'elle y suspectait.

Poussant son cheval aussi durement qu'il le pouvait, il galopa dès que les voies n'étaient pas trop étroites ou instables, dépassant plusieurs caravanes dont les guides pestèrent d'être ainsi bousculés par les graviers et la poussière de ce cavalier pressé. Arrivé à Jérusalem, il ne lâcha pas plus de trois mots à ses collègues à la porte de David et, sa monture à peine confiée aux palefreniers, il alla se claquemurer chez lui. Lorsqu'il se laissa tomber sur sa couche, encore gris du chemin, il n'aurait pas su dire s'il était euphorique, en colère ou au bord des larmes. Ce qui était certain, c'est qu'il aurait volontiers passé toute cette énergie à frapper de ses poings nus sur n'importe quoi, ou n'importe qui.

### Jérusalem, palais royal, midi du mardi 6 janvier 1159

Ernaut s'était rendu de bonne heure au palais afin d'y faire son rapport au sénéchal. Il savait que celui-ci écouterait d'une oreille distraite, l'affaire ayant été prise en charge et entendue par les hommes du comte de Jaffa. Il tenait néanmoins à ce que son service soit irréprochable. Tout au long de la journée, il s'efforça également de manifester un peu de rigueur, à défaut d'enthousiasme, dans ce qu'il faisait. Il croisa Raoul à plusieurs occasions et se contenta de sourire sans chercher à échanger avec lui, inquiet de réveiller des blessures trop vives.

Quand il apprit que Droart et Eudes étaient de la patrouille nocturne, il demanda à en faire partie. Il les retrouva avec plaisir lors du repas du soir, qu'ils prirent ensemble dans une des salles servant à l'occasion de réfectoire. La jovialité coutumière de Droart eut raison des idées noires d'Ernaut et celui-ci finit par se mettre à plaisanter avec ses amis. Il accepta de leur narrer sa mission, sans en évoquer les éléments secrets, qui étaient pourtant ceux qui pesaient le plus sur son âme.

Droart tenta de le réconforter, en parlant de ses travaux à lui, dont il espérait qu'ils ne réveilleraient nul dragon, disait-il. En dehors de sa femme, dont il n'était pas certain qu'un saint Georges y suffirait à la tâche.

« Ne prends pas en male part ce que je vais te dire, compère, glissa Eudes, mais si tu la trouves si peu à ton goût, pourquoi ne sévis-tu pas ? Si tu lui abandonnes ainsi les braies, ne t'étonne pas qu'elle s'en empare !

--- *Il n'y a femme, cheval ni vache qui n'ait toujours quelque tache*. Si on savait ce que le mariage apporte, peut-être se ferait-on moine !

--- Toi chez les encapuchonnés escouillés ? Je voudrais bien t'y voir !

--- Tu parles de raison. Disons chez les chanoines. Eux au moins ont à ouïr mignonnes garcelettes à confesse et peuvent en tirer quelque avantage ! Encore qu'avec ma chance, j'aurais mon épouse comme paroissienne ! »

Baset vint mettre fin à leur discussion, le mathessep rassemblait la patrouille pour les dernières instructions. Ernaut retrouva l'atmosphère habituelle des rondes avec un étrange sentiment de distance. Il lui semblait qu'il n'était entouré que de fantômes et que tout se déroulait comme s'il n'était pas du même monde. Il voyait les gestes, entendait et souriait aux plaisanteries échangées, obtempérait aux ordres, mais tout cela arrivait en dehors de lui, sans qu'il n'ait vraiment l'impression d'être là.

Alors qu'ils passaient près de la porte de David, les hommes chargés des courtines signalèrent un émissaire qui demandait à pénétrer en ville. Après vérification, c'était un coureur venu du Nord, porteur de messages royaux. Ernaut et Droart furent désignés pour l'accompagner jusqu'au palais où il serait auditionné le lendemain matin à l'aube. Leur mission achevée, ils firent demi-tour pour rejoindre la patrouille. Chemin faisant, Droart proposa une gorgée d'une petite gourde qui ne le quittait que rarement, surtout les nuits de garde. Après s'en être autorisé une longue lampée, il fit claquer sa langue à plusieurs reprises.

« Je n'avais pas compris que tu étais si fort compère du jeune clerc. Peut-être devrais-tu faire donner quelques messes pour lui, ça ne peut pas faire de mal.

--- Je ne suis pas certain qu'il aurait aimé qu'on nous dise compères, mais aurait sûrement apprécié l'idée des messes. Je vais y songer, merci.

--- Et puis on devrait se faire belle veillée un de ces soirs, qu'en dis-tu ? Fêtons Charnage[^42] avant de subir Carême ! Et nous y causerons de nos projets. Cela fait moment que tu n'as pas évoqué ton mariage avec ta drôlesse.

--- C'est que le temps me paraît bien long avant de m'unir à elle.

--- Et ce sera encore pire une fois que tu l'auras mariée, crois-moi ! »

La face réjouie de Droart sous le clair de lune arracha un éclat de rire à Ernaut.

« Enfin, je ne te le souhaite certes pas. Mais mieux vaut être mal compaigné que pas compaigné du tout. Et si d'aventures tu as projet d'avoir bel hostel, je connais les personnes dont graisser la main pour tout se fasse au mieux.

--- Je n'ai pas encore suffisamment d'avoirs pour m'établir. Encore quelques mois, une ou deux Pâques !

--- Et quoi ? Le béjaune ne veut pas devenir un homme ? Demeurer bachelier est certes fort agréable pour aller muser qui ci qui là, mais il convient à un moment de fonder une famille et de peupler le royaume ! »

Ernaut convint de la justesse de la remarque. Même s'il était impatient de s'unir à Libourc, il se mettait parfois des exigences trop élevées en ce qui concernait leur future installation. Nombre de jeunes mariés n'avaient pas le capital dont lui disposait. En outre, à en croire ce que lui avait confié Philippe de Milly, il n'était pas sans attirer l'attention sur lui et pouvait espérer grimper encore dans la hiérarchie de l'hostel du roi. Sergent monté, voire mathessep lui semblaient des postes assez rapidement envisageables désormais.

Le choix qu'il devait faire en ce qui concernait le mosaïste pouvait aussi modifier la façon dont il concevait l'avenir. Il convenait donc d'intégrer Libourc et leurs possibles enfants à sa réflexion. Il commençait à voir les choses plus posément, avec moins d'angoisse. Mais il n'était pas encore prêt à prendre une décision. Se perdre dans les activités routinières du service lui offrirait un moment de grâce, où il n'aurait guère à user de ses méninges. Il allait se rendre à la Petite Mahomerie le dimanche suivant, pour assister à la messe avec sa future belle-famille. La présence seule de Libourc lui apporterait de la force pour trancher, même s'il ne comptait pas tout lui révéler de son dilemme. Il voulait éviter de l'exposer ainsi à toute la noirceur du monde.

Lorsque primes sonnèrent aux églises de la ville, la patrouille était de retour au palais. Les escouades de jour étaient parties prendre leur service aux portes et les hommes ayant passé la nuit dehors étaient pour la plupart rentrés chez eux. Malgré la fatigue, Ernaut chercha, dans les zones de la Cour et de la sénéchaussée, après Régnier d'Eaucourt. Il avait croisé Ganelon au réfectoire le matin et espérait pouvoir s'entretenir avec le chevalier avant d'aller se coucher.

Il finit par le retrouver dans un des petits jardins, en grande discussion avec quelques soldats, dont deux portaient la cape blanche des miliciens du Christ. Régnier reconnut Ernaut, mais demeura à échanger un long moment jusqu'à ce que chacun s'éloigne. Il fit alors signe au jeune homme d'approcher. Ce dernier s'inclina poliment en arrivant.

« Alors, jeune Ernaut, comment t'es-tu sorti de mission de chevalier ? plaisanta aimablement Régnier.

--- J'espère ne pas avoir gâché trop irrémédiablement les choses, rétorqua Ernaut, mi-figue mi-raisin. Si vous en êtes d'accord, j'aimerais avoir votre sentiment en cette affaire.

--- Je t'encontre bien sombre en cette belle matinée. Est-ce à voir à cause de la mort du père Gonteux ?

--- Disons que c'est lié à cela, sans en être la directe cause. »

Le chevalier invita Ernaut à l'accompagner dans une déambulation parmi les plantes. Avec le froid et l'absence de fleurs, le jardin semblait en sommeil malgré la luxuriance des formes.

« Quelle est donc la ténèbre qui ronge ton cœur ? Peux-tu m'en dire sur les tristes événements de Lydda ?

--- Je ne saurais entrer dans les détails, ordre m'ayant été donné de ne point en parler. Et la question qui m'amène n'en est que la cause, sans en nécessiter l'inventaire. »

Ernaut stoppa, cherchant à formuler au mieux ce qui le poignait ainsi. Le visage attentif du chevalier comprit que la demande dépassait les besoins d'une simple enquête de routine. Il ne le brusqua pas par des questions forcément maladroites par leur méconnaissance.

« Avez-vous éprouvé déjà quelques doutes quant à votre conduite en vos serments ?

--- Qu'est-ce à dire ?

--- J'ai fait solennelle promesse, mais je me demande comment la mieux servir. Et il me semble parfois que je dois en trahir certains termes pour en respecter le sens.

--- Oh, certes, je comprends fort bien le déchirement, pour l'avoir maintes fois éprouvé. C'est là le destin de celui qui cherche une voie honorable, Ernaut. »

Il s'éclaircit la voix, triant parmi ses souvenirs afin d'en tirer un récit élaboré, voire édifiant.

« Ainsi que toi, j'ai juré sur saints livres et il m'est arrivé d'être en l'hostel le roi lors des terribles affrontements entre Baudoin et sa mère Mélisende. Ils en étaient venus à s'assaillir en pleine cité, l'un érigeant pierrières pour déloger l'autre du chastel. À la parfin, Dieu a voulu que le fils l'emporte sur sa mère. Je n'encrois nullement que le connétable Manassès ait failli à son devoir, mais il a pourtant dû laisser sa charge. En toute cette affaire, il en a jugé selon son âme et son honneur et on l'a démis et chassé pour cela. Et moi, ainsi que mon sire Onfroy, j'ai rejoint l'hostel de Baudoin. Ai-je démérité pour cela ? Ai-je trahi mon sire Manassès ? Un autre que moi en jugera. »

Il se tourna vers Ernaut et lui frappa sur la poitrine.

« Il te faut comprendre ce que tu espères en cette vie et en l'autre, et tu en tireras l'aune à laquelle tu établiras toute mesure. Il n'est nul serment dont on n'ait fait l'épreuve du feu de chaque mot en son âme. Laisse donc paroles creuses et promesses vides aux bateleurs en amour de jouvencelles. Si tu as doutances en tes choix, c'est que tu es en bonne voie. La seule certitude que tu dois protéger en ton fors, c'est ta foi en Dieu. »

Ernaut demeura un long moment à éprouver le sens de ces paroles, cherchant à en tirer un enseignement utile à la résolution de son problème. Il remercia chaleureusement le chevalier de lui avoir accordé du temps et s'apprêtait à partir quand ce dernier le retint d'un geste.

« C'est possiblement revenu à tes oreilles qu'on parle assez souventes fois de toi parmi barons et officiers. Tu as attiré l'attention sur toi, Ernaut. Veille à te garder comme en combat et à ne jamais poingner seul. Tout conroi[^43] ne vaut que par les liens qui le tissent. Garde féals compaings près de toi. »

### Petite Mahomerie, demeure de Sanson de Brie, après-midi du dimanche 11 janvier 1159

La fin de la semaine n'avait été que routine du service. La ville était habituellement calme en ces périodes, les températures hivernales dans les montagnes de Judée refroidissant les ardeurs des pèlerins, qui, en cette saison, visitaient plus volontiers les lieux saints proches des côtes. Le commerce lui aussi était ralenti, pour les mêmes raisons. La garde aux portes était donc généralement assez ennuyeuse et les patrouilles lors des longues nuits plutôt tranquilles. Il se trouva juste une querelle entre membres du guet bourgeois qui se disputaient l'honneur de tenir la porte Saint-Étienne l'année suivante. Il était fait reproche aux bouchers de ne guère partager et ces derniers estimaient qu'ils n'avaient pas à le faire, étant donné que c'était là leur privilège depuis des décennies.

La situation ayant dégénéré à la faveur de la nuit et de trop grandes quantités de vin chaud, il avait fallu que la patrouille des sergents intervienne. Il se trouvait néanmoins quelques blessés parmi les bourgeois, dont un assez sérieusement. Le vicomte organisa assez vite une rencontre avec les principales confréries afin de clarifier les choses. Il se moquait des points de préséance et n'avait aucune intention de s'en mêler, mais il était inacceptable que les hommes chargés de tenir les murailles en viennent à se battre entre eux. Quand bien même nul ennemi n'était aux portes, la prochaine fois qu'il serait confronté à pareils agissements, il punirait de façon rapide et, insista-t-il, *définitive*. Puis il laissa la place aux jurés pour que la question qui les avait divisés soit abordée.

Bien qu'il ait terminé son service assez tôt le samedi, Ernaut choisit de ne pas chevaucher le jour même. Il prit le temps de se rendre aux bains, bénéficia d'un barbier adroit et volubile, qui lui farcit la tête de nouvelles aussi variées qu'inintéressantes, puis s'offrit quelques pâtés qu'il engloutit dans sa chambre. Au soir, il dormait si profondément qu'il n'entendit pas sonner complies.

Sans se presser, il était arrivé largement à temps pour l'office à la Petite Mahomerie, profitant d'une météo clémente pour déambuler dans la palmeraie qui abritait quelques jardins près de l'église. Malgré l'envie, il ne se précipita pas chez Libourc, préférant lui faire la surprise, goûtant par avance le plaisir de voir la joie illuminer les traits de sa fiancée. Il ne fut pas déçu et il eut toutes les peines du monde à se discipliner pendant la messe. D'autant que le prêtre, fraîchement nommé par les chanoines du Saint-Sépulcre, s'était mis en tête de séparer hommes et femmes pendant la cérémonie, ce qui faisait grincer les dents des plus conservateurs des paroissiens, pointilleux quant au respect de leurs habitudes. Ernaut se dit que le clerc faisait montre là d'une intransigeance que n'aurait pas reniée Herbelot. Au sortir de la messe, il incita sa belle-famille et leurs proches à faire preuve de mansuétude envers le nouvel officiant, leur narrant ses démêlés avec le père Gonteux lors de la traversée.

Après le repas, comme à leur habitude, Sanson et Mahaut s'allongèrent pour une courte sieste, laissant les jeunes gens faire une promenade. Libourc proposa à Ernaut de rejoindre quelques amis du hameau, qui avaient usage de se retrouver pour partager contes, chants et musique. Ils passèrent ainsi le temps entre jeux et rires, un des participants ayant un talent et une répartie dignes d'un jongleur. Il fut nommé roi de la fête et amusa les convives de ses charades alambiquées et gages inventifs. Lorsqu'ils rentrèrent au logis pour le souper, Ernaut et Libourc avaient le souffle court et le visage si enflammé qu'ils s'attirèrent un froncement de sourcils plein de défiance de la part de Mahaut, qui réquisitionna sèchement sa fille pour finir de préparer le repas.

Comme à son habitude lors des veillées, Sanson sculptait des manches d'outils ou de canifs. Ce n'étaient que des motifs simples et il distribuait son travail sans jamais en demander paiement, car il faisait cela par goût et pour « garder le sens du bois ». Ernaut aimait regarder le vieil homme ciseler adroitement les objets, concentré sur son ouvrage tandis qu'il échangeait nouvelles sur la météo ou discussions sur les récoltes à venir. Au fil du temps, il avait appris à véritablement apprécier Sanson, avec son caractère lunaire et ses yeux souvent dans le vague. Il lui rappelait parfois son propre père, dont la nature débonnaire l'avait protégé durant toute son enfance, jusqu'à son départ même.

« Libourc nous a conté plus avant sur ton importante mission de la Noël. Est-ce signe que tu es promis à meilleure place ?

--- Je ne sais. À dire le vrai, ce n'est que par défaut qu'on m'a confié cette tâche.

--- Tout de même, avoir parlé en égal à évêque et vicomte, ce n'est pas rien ! Ne te rabaisse donc pas. »

Il assortit sa remarque d'un sourire qui en démentait le tranchant.

« Crois-tu qu'il se pourrait qu'on te nomme en charge d'un office ?

--- Pour l'heure, nul n'a à se plaindre du mathessep, et je n'ai guère porté les armes pour servir à cheval.

--- De cela, je préfère. Même si tu ne rêves que de penon et horions comme tous les jeunes, il est plus profitable de faire le sergent ès ville. Même si on le fête, saint Georges a bien mal fini, j'encrois. »

Mahaut les interrompit alors pour passer à table. Ils y déplacèrent les lampes et s'installèrent pour le bénédicité, récité comme chaque fois par Sanson, sauf quand il déléguait le privilège à un invité qu'il souhaitait honorer. La conversation s'éteignit ensuite, le temps qu'ils avalent leur épais potage. Mahaut y avait ajouté du gruau et l'avait accompagné d'une compotée d'oignons. Et, comme à son habitude, Sanson servait un vin clair venu de Samarie qu'il affectionnait tout particulièrement. En guise de dessert, Mahaut avait confectionné des petits fromages de brebis frais, agrémentés de sel et de cébettes. Même s'il en redoutait le caractère acariâtre, Ernaut reconnaissait à sa future belle-mère de réels talents culinaires, dont il espérait que Libourc avait hérité.

À la fin du repas, quand Sanson repliait le canif qui ne le quittait jamais, les échanges reprenaient, sur une question de sa part. Il revint sur le voyage d'Ernaut vers la plaine côtière, s'intéressant cette fois aux pratiques agricoles, au commerce de la cité et à la qualité des produits manufacturés. Il était généralement assez critique sur les usages locaux, trouvant que les Levantins étaient trop indolents. Il leur reconnaissait des compétences pour le négoce, partageant ces dons avec les Juifs, ce qui n'était dans sa bouche qu'un demi-compliment, vu la méfiance qu'il manifestait envers ces derniers. Il ne tarda pas à n'écouter les réponses que d'une oreille, dodelinant du chef un moment avant de se résoudre à aller se coucher. Avec le manque de lumière, Mahaut et Libourc ne pouvaient ni coudre ni repriser et elles se contentaient de filer la laine, faisant tourner leur fuseau de façon hypnotique.

Ils jouèrent un temps aux devinettes, puis Ernaut les amusa du récit des troubles du guet, saupoudrant par endroit des détails inventés pour ajouter du piment à l'histoire. Encouragée par sa fille, Mahaut se laissa aller à leur narrer les exploits d'Ogier, héros local de Meaux.

« Il fut grand guerrier, ainsi que saint Georges, mais il ne mourut pas de sa Foi comme lui. Après avoir féalement servi le grand Charles[^44], il choisit la sainte vie de moine et se retira à Saint-Faron, où ses reliques font grands miracles.

--- N'est-ce pas lui qui avait une courte lame ? demanda Ernaut, moins connaisseur du héros.

--- Si fait, son épée fut brisée lorsqu'on voulut l'éprouver au marbre. Et elle demeura ainsi, nommée dès lors *Courtain*. »

Elle se mit alors à leur narrer les exploits du héros briard, dont le bras occit à lui seul tant de musulmans qu'on se demandait comment il pouvait en demeurer assez pour peupler les régions où ils habitaient désormais. Libourc se souvenait avoir visité, étant petite, le mausolée du saint, dont elle expliqua qu'il sentait le vieux linge poussiéreux, étant recouvert d'une grande quantité d'étoffes de prix. Voyant que sa mère plissait le nez à cette mention, elle se dépêcha d'enchaîner sur un discours dont elle savait qu'il serait plus favorablement accueilli.

« Il y eut si vaillants paladins à servir Charles ! Que n'encontre-t-on plus d'Olivier ou de Roland de nos jours ?

--- Il en est ainsi de toute chose, ma fille. Une fois les plus beaux fruits récoltés, ne demeurent sur l'arbre que les verreux et pourris. Nous n'avons pas eu la chance de connaître ces héroïques périodes, nous devons faire au mieux en ces temps désolés et oublieux. Dieu l'a fait ainsi : après le jour vient la nuit. »

Même s'il partageait l'avis de Libourc sur le manque de grands hommes à leur époque, Ernaut était bien loin du point de vue défaitiste de Mahaut. Il avait admiré le roi à la tête de son armée, il avait applaudi devant les barons avançant au pas, bannières claquant au vent. Il ne pensait absolument pas que ne pouvaient se trouver des modèles comme au temps jadis. Il suffisait de voir Onfroy de Toron mener une charge, ou les couleurs de Philippe de Milly, dont on vantait la vaillance, égale à celle du prestigieux prince d'Antioche, Renaud de Châtillon.

En outre, cette idée lui permettait d'envisager qu'un jour, il pourrait être de ceux dont on enviait les exploits, rejoignant ses propres héros au firmament de leur gloire.

Chapitre 10
-----------

### Jérusalem, palais royal, fin de matinée du lundi 12 janvier 1159

La Petite Mahomerie n'étant guère distante de Jérusalem, Ernaut avait choisi de chevaucher à la fraîche. Mal lui en prit, car une légère, mais persistante, pluie l'accompagna tout au long de son périple, ce qui lui fit commencer son service au matin trempé comme une soupe. Il fut donc soulagé d'apprendre qu'il devait demeurer à l'intérieur, même si c'était pour une tâche habituellement considérée comme une corvée. Il s'agissait de trier les redevances perçues pour la Noël, de regrouper les monnaies identiques, poids et aloi pour qu'elles soient ensuite rangées dans des bourses dont la valeur serait garantie par un sceau apposé par un des clercs de la Grant Secrète.

Il passa ainsi la journée, penché sur une table, les yeux plissés dans la faible lumière de la grande salle voûtée, à rassembler pièces et méreaux. Gaston, qui surveillait les sergents et portait les tas à la pesée, égaya un peu leur ouvrage en leur narrant quelques-unes des batailles auxquelles il avait participé. Il avait été de fréquents coups de main, ne dévoilait parfois que peu de détails sur les circonstances de ses exploits, mais ne manquait jamais d'en faire un récit épique et haut en couleur.

Ayant retrouvé Eudes et Droart en fin de journée, lorsque le vicomte distribua les tâches de la soirée, Ernaut découvrit qu'ils étaient tous les trois désœuvrés pour la veillée et convinrent de se rejoindre à la taverne où de nombreux hommes du roi aimaient à venir tuer le temps. N'ayant nulle femme qui lui préparait le repas, Ernaut passa par Malquisinat pour y quérir de quoi manger, dont une belle fouace de l'échoppe de Margue. Puis il se rendit directement chez le père Josce.

L'endroit était assez calme, n'étant peuplé que des célibataires qui n'avaient personne pour tenir leur feu et leur pot. Il fit quelques parties de table avec Basequin, un compagnon de l'hôtel du roi qui noyait son récent veuvage dans le vin et la bière. Il parvenait ainsi à avoir encore moins d'esprit qu'au naturel, ce qui plaçait la barre à un niveau plus que respectable. Suffisamment du moins pour qu'Ernaut se lasse assez vite de gagner sans avoir à y réfléchir suffisamment pour y prendre plaisir.

Il vit donc arriver Eudes, puis Droart, avec un réel soulagement et, prenant son tabouret, il fit avec eux un petit cercle où ils pouvaient discuter à loisir sans craindre d'être trop entendus. Ernaut avait l'intention de partager avec eux une partie de ses problèmes et, surtout, de leur demander s'ils accepteraient d'aider à leur résolution.

« J'aurais besoin de compagnons pour périlleux projets, compères. Vous m'avez toujours traité en frère, depuis que je suis en la Cité. C'est le pourquoi je vous fais demande. Néanmoins, je ne saurais espérer que vous acceptiez sans y longuement pourpenser.

--- En ce cas, c'est d'accord en ce qui me concerne, ricana Droart.

--- Je suis sérieux, Droart. Tu pourrais y perdre plus que la vie. »

La remarque doucha l'enthousiasme du plaisantin, qui avala une gorgée. Eudes n'avait pas ouvert la bouche, attentif à ce que disait Ernaut.

« Je ne peux vous conter toute l'histoire, mais il me faudrait féals compagnons pour serrer un maufaisant et lui arracher ce qu'il a volé. Le péril en vient qu'il est fort puissamment protégé et que nul ne doit rien savoir de ce que nous ferons.

--- Protégé ? Est-ce à dire qu'il s'agit de baron avec ses hommes ? s'inquiéta Eudes.

--- Pas du tout. Il n'est même pas tant costaud que je ne pourrais m'en emparer seul. Il a puissant parrain qui fait qu'on ne peut lui demander raison devant des juges, c'est tout.

--- Voilà bien rusé coquin dont j'aime à garroter de chanvre le cou » grinça Droart.

Eudes fixait toujours Ernaut. Tout au rebours de Droart, il aimait réfléchir avant de parler. Pourtant, il en partageait souvent les avis, même si ses motivations en étaient moins instinctives.

« Je te connais assez pour croire que ce n'est pas là méshonnête projet, Ernaut. Es-tu bien assuré de ton bon droit au regard de Dieu ?

--- À plus d'un titre. Mais, de toute façon, je n'ai nul désir de me substituer à son jugement. L'intendant Pisan[^45] trouvait qu'il est présomptueux de se penser habile assez pour peser les âmes ou juger de son bon droit. Cette histoire est tellement embrouillée que je ne saurais trancher en conscience. Pourtant, il m'est possible d'en restreindre quelque peu les dommages. Je peux le faire seul, mais à plusieurs, cela sera d'autant plus aisé et moins risqué. Il nous faudra partir plusieurs jours, sans que quiconque soupçonne où nous allons.

--- Quand prévois-tu d'agir ?

--- Ils ne vont pas tarder à labourer pour pois et vesces à la Mahomerie, il est aisé d'obtenir congé du vicomte en ces moments. Quoi qu'il en soit, je ne voudrais pas que ce soit après la fête des chandelles[^46]. »

Droart manifesta son accord d'un claquement de langue.

« Il me sera facile de prétexter quelque tâche urgente pour ma maison. Et de dire à Ysabelon que le service le roi m'envoie au loin.

--- Si tu en es d'accord, Ernaut, je préférerais ne pas mentir à mon épouse. Je dirais juste que je dois m'absenter pour quelque secret ouvrage, sans rien en dire de plus » compléta Eudes.

Ernaut sourit à ses deux amis, n'ayant guère de mot pour exprimer sa reconnaissance. L'un après l'autre, il leur serra l'avant-bras puis se releva pour aller chercher de quoi trinquer au succès de leur entreprise. Il profita d'un passage moins éclairé de la salle pour se frotter discrètement les yeux. Lorsqu'il revint, Droart avait retrouvé sa faconde et allégeait l'ambiance de ses pitreries.

Ernaut leur servit à boire et leur expliqua qu'il dédommagerait chacun des frais qu'ils auraient, mais qu'il serait plus prudent de louer séparément les montures à Abdul Yasu. Il leur dévoila aussi son plan plus en détail, espérant qu'ils pourraient en faire la critique et l'améliorer. Le plus dur serait de patienter jusqu'à ce qu'ils aient l'autorisation de s'absenter.

### Palmeraie de Rama, fin d'après-midi du lundi 26 janvier 1159

Ernaut avait passé le dimanche avec son frère Lambert et sa jeune épouse Osanne, leur laissant entendre qu'il était sur le chemin d'une mission pour le roi. Habitués à ses incessants déplacements, ils n'avaient pas posé de question, heureux de l'accueillir dans leur manse pour une journée. Lambert lui avait fait faire le tour du propriétaire, montrant à Ernaut ce qu'il faisait des sommes à lui concédées. Ernaut savait qu'il était homme de labeur, dur à la tâche, aussi exigeant de son bien que des siens et de lui-même. Jamais Lambert n'avait trahi cette réputation.

Par ailleurs, l'affaire du meurtre d'Ogier était encore fraîche[^47] et le clivage entre le haut et le bas du village demeurait vif. Lambert avait tenté de se rapprocher de ses contradicteurs, mais les ressentiments étaient trop forts. Il regrettait ces dissensions, n'y voyant que de l'énergie perdue pour tous. Certains chantiers ne pouvaient s'accomplir facilement qu'avec beaucoup de main d'œuvre et si les quartiers tiraient à hue et à dia comme alors, tout prendrait beaucoup plus de temps. Il le déplorait, mais sans s'apitoyer. Ernaut trouvait qu'il ne s'en montrait que plus volontaire, bien décidé à arracher à force de travail ce qu'on ne voulait lui accorder aisément.

Lambert était persuadé que tout pouvait se régler à condition d'y consacrer les efforts nécessaires. Cet optimisme forcené fit du bien à Ernaut, qui demeurait circonspect quant au choix qu'il avait fait. D'autant plus qu'il avait convaincu Eudes et Droart de mettre leurs pas dans les siens. Pour une fois, il se trouva bien des discours de son frère. Il s'en amusa, estimant que c'était peut-être de quitter les enfances qui le rendait plus sensible au sérieux outrancier de Lambert. Heureusement qu'il y avait Droart pour équilibrer.

Le vent de la côte faisait danser les palmiers qui abritaient les zones de culture entre Rama et Lydda. Ernaut avait proposé de s'y retrouver à la mi-journée, en deçà de la porte de Rama qui menait au nord, au croisement qui partait à l'ouest vers Jaffa. Il s'y trouvait un grand complexe fort ancien servant de citernes dont Gerbaut, le portefaix, lui avait parlé. On y rencontrait quelques granges ainsi que des bassins pour les bêtes, près d'une zone empierrée où des foires pouvaient s'installer, ou des caravanes s'organiser. Pour le moment, on n'y voyait que deux gamins occupés à brosser un chameau indolent, et un petit groupe de nomades discutant au milieu de moutons grossièrement parqués entre des broussailles.

Ernaut avait hésité à entrer dans Rama pour s'offrir un repas chaud. Puis il avait préféré se faire discret. Il se savait reconnaissable et n'avait pas envie qu'on puisse se souvenir de son passage. Il demeura donc tranquillement en retrait, installé contre un tronc, à cracher les noyaux des dattes qu'il grignotait en somnolant. Droart n'allait plus tarder à revenir de sa mission. Il s'était porté volontaire pour délivrer le message au maître mosaïste, déguisé en un mélange improbable de Latin et de Syrien, attifé comme un fellah à l'aspect étrange.

Une fois sa monture déposée aux écuries, il devait jouer au pèlerin pour entrer dans l'église puis remettre un pli à l'artisan, comme si on le lui avait confié. Ernaut lui avait pour cela donné un petit papier où un écrivain public avait rédigé pour lui « Danger. Urgence » dans la langue grecque. Droart avait pour mission de se faire passer pour un commissionnaire mandaté par un Byzantin. Le but était de donner rendez-vous nuitamment à Zénon près des citernes de la palmeraie.

Ernaut était conscient que son plan était risqué, le mosaïste pouvant prendre peur et préférer s'enfuir. C'est pourquoi Eudes attendait la fermeture des portes au nord de Lydda pour les retrouver. Ils avaient estimé que c'était là le second choix que ferait l'espion pour disparaître au plus vite, le premier étant de se diriger vers Jaffa afin de s'y embarquer, auquel cas il ne manquerait pas de passer vers Ernaut. La cité de Lydda n'était pas si bien fortifiée, mais ses murs et ses tours permettaient de contrôler qui entrait et sortait. Ernaut ne voulait pas y être vu, et n'avait rien trouvé de mieux pour en faire partir le mosaïste de sa propre volonté.

Droart le rejoignit alors que le soleil était encore assez haut. Il semblait assez content de lui et lui expliqua qu'il s'était bien amusé à passer les portes en jouant la comédie. Il devint plus grave quand il évoqua sa rencontre avec Zénon de Morée.

« Il a dit ne rien comprendre de ce que je lui ai dit, qu'il n'était que simple artisan. J'ai insisté, disant qu'on m'avait payé pour remettre ce pli et que je comptais bien m'acquitter de cette tâche. Et quand je lui ai remis ton papier, il a blêmi comme fraudeur à la porte, j'en jurerai. Il n'a plus rien dit après cela et j'ai pris congé sans autre parole.

--- Il n'a pas dit qu'il viendrait ?

--- Il n'a rien dit du tout. »

Ernaut acquiesça, soudain inquiet à l'idée que l'artisan n'ait rien à voir dans tout cela. Il lui semblait avoir tout compris des intentions et projets cachés, mais peut-être avait-il fait fausse route tout ce temps. Il n'aurait la preuve qui lui manquait que lorsque le mosaïste se présenterait au rendez-vous nocturne.

Eudes arriva au moment où le soleil s'évanouissait en langues de feu derrière les nuages bas qui suivaient la côte. Ils s'installèrent pour un rapide en-cas de pain et de poisson séché, qu'ils avalèrent avec un peu de bière. Même Droart ne parlait guère, lui aussi rendu nerveux par la situation. Puis ils prirent leurs aises pour un long moment de veille. La lune, qui débutait à peine son premier quartier leur accordait une chiche lumière suffisante pour discerner ce qui se déroulait aux abords, tout en leur permettant de se camoufler dans l'obscurité des bas-côtés du chemin. Après avoir regroupé leurs montures à l'écart, et leur avoir noué un sac d'avoine à chacune autour du cou, ils s'installèrent à quelque distance l'un de l'autre, emmitouflés dans une couverture pour se prémunir du froid. Puis ils patientèrent.

Ils virent déambuler quelques chiens et entendirent une bataille fugace entre des animaux sauvages. Un chameau, vraisemblablement échappé de son attache, promena sa longue carcasse, arrachant les feuilles aux arbustes environnants. Ils ne discernaient pas les lueurs de la ville non loin, cachées derrière l'épaisse végétation des jardins.

L'approche d'un cheval leur fit tendre l'oreille et le cou, mais il venait de l'ouest, et le voyageur ne s'arrêta pas vers eux, ne ralentissant que peu en parvenant parmi les bosquets sombres. Ernaut commençait à somnoler quand il entendit l'arrivée d'une monture au pas. Il plissa les yeux, cherchant à reconnaître si la silhouette en selle correspondait à celle de Zénon. Il se raidit un peu, tira de côté sa couverture lorsqu'il pensa que c'était le cas.

Le cavalier stoppa, noua ses rênes à une barrière et jeta des regards en tout sens. Ernaut vit une épée à sa hanche, et des paquets en croupe du cheval. C'était bien Zénon, qui s'était apprêté pour un voyage. Il l'entendit appeler doucement, certainement dans la langue des Byzantins, car il n'y comprit rien. Il patienta, le temps de s'assurer que le mosaïste n'était pas tourné vers lui, puis il se leva brusquement et l'interpella d'une voix forte.

« Zénon de Morée, tu ne trouveras mie compère ici ! Tu es en notre pouvoir, délace ton baudrier. »

L'artisan se retourna, tendu comme un arc. Il eut un petit mouvement de recul en découvrant l'impressionnante silhouette d'Ernaut, dont la cotte de mailles brillait légèrement sous la lune. Ce dernier tenait d'une main sa masse et gardait l'autre prête à dégainer. Zénon sursauta en l'entendant siffler, obtenant deux réponses sorties des broussailles.

« Des arbalètes pointent sur toi en cet instant. Sur mon ordre, les traits seront décochés pour te percer. Laisse-toi lier les poignets et tout cela te sera évité. »

Le mosaïste ne pouvait pas ignorer que c'était une arme dont seules les troupes régulières avaient le droit de se doter. Bien qu'il n'ait trouvé aucun moyen de s'en procurer une discrètement, Ernaut tenait à les évoquer, ne serait-ce que pour faire croire qu'il était là en mission officielle. Zénon comprit immédiatement le sous-entendu.

« Vous ne pouvez rien. Si vous me prenez, il vous faudra en rendre compte au basileus. Sa main me protège.

--- On ne t'a pas dit ? Ici c'est le royaume de Jérusalem ! Son bras n'est pas tant long qu'il arrive jusqu'ici. »

Ernaut tentait de s'assurer de son courage par sa raillerie.

« Vous ne pouvez rien contre moi ! Je ne suis sujet de votre roi !

--- Pourtant, il va te falloir rendre compte de tes meurtreries et rendre ce que tu as volé.

Ernaut n'aurait pu en jurer sur l'instant, mais il eut alors l'impression que Zénon avait lancé un rapide coup d'œil à ses bagages. Il s'y trouvait peut-être les documents qu'il avait dérobés au chanoine. Peu désireux de faire durer l'entretien, au risque de voir arriver quelqu'un, Ernaut fit quelques pas, jouant de tous ses muscles et gonflant la poitrine. Zénon cracha d'une voix acide :

« Je te connais, tu es sergent le roi ! Nous avons parlé construction tantôt !

--- Tu sais donc que je ne suis pas simple brigand, mais que je suis là pour plus importante raison. Si tu ne déposes pas ton épée à l'instant, cela va être douloureux pour toi, j'en fais serment. »

Zénon inspira bruyamment, expira lentement, puis, haussant les épaules, il fit jouer la boucle de son ceinturon, laissant tomber son fourreau au sol. Ernaut lança quelques ordres, comme s'ils étaient plus de trois à s'être embusqués, afin qu'un homme vienne s'emparer de l'arme et vérifie que Zénon n'en ait pas d'autres, camouflées sur lui. Lorsque Droart s'exécuta, il fit un signe de la tête à Ernaut, pour qu'il s'approchât : un des poignets de Zénon était particulièrement abîmé, les chairs rongées comme par un feu.

« Voilà bien flétrissure de ton infamie, Zénon. Te reste-t-il de cette poudre démoniaque ? »

Le mosaïste secoua la tête en dénégation, abattu. Ernaut en fut rassuré, car il n'aurait pas su quoi en faire. Il se pencha vers le Byzantin, l'air rogue, grognant plus que parlant, malgré son chuchotement.

« Nous allons nous mettre en chemin dès à présent. Tu n'ouvriras la bouche que je ne t'y ai autorisé. Si tu m'obéis bien, je ne porterai pas la main sur toi. »

Ils se mirent alors en selle, Ernaut tenant la monture de Zénon en longe. Ils n'eurent pas besoin du cheval supplémentaire qu'ils avaient prévu, au cas où le mosaïste serait venu à pied, et Ernaut décida d'y poser les affaires du Byzantin. Cela lui permettait de conserver les éventuels documents qui s'y cachaient, quelle que soit la suite des événements. Ils firent une courte pause quand l'aube commença à pointer, gardant le prisonnier à tour de rôle, tout en ayant pris le soin de le lier à un arbre, en retrait de la voie pour ne pas trop attirer l'attention. Ils filaient bon train et avaient déjà passé Jaffa, abandonnant ses reliefs loin derrière eux au sud. Zénon se laissait mener en silence, vigilant sans être inquiet. Il avait remarqué qu'on le menait au nord et espérait peut-être se voir remis à ses compatriotes.

Eudes, Ernaut et Droart ne parlaient qu'à voix basse devant lui. Ils avaient suffisamment préparé leur voyage pour n'avoir à se soucier que de menus détails : les haltes pour vérifier les bêtes, inspecter les liens du captif ou avaler un peu de pain et des dattes. Lorsqu'un crachin se leva de la mer qu'ils longeaient, ils rentrèrent la tête dans les épaules, mais sans modérer leur chevauchée. Au crépuscule, ils abordaient la zone de marécages autour de Césarée. Peu après, Ernaut ralentit le pas et bifurqua sur un sentier.

Zénon sentait bien que ce n'était plus la voie principale, mais il ne quitta pas son mutisme. Se rapprocher de la côte, c'était envisager l'embarquement dans un navire, peut-être discrètement pour ne pas avoir à se justifier auprès des autorités portuaires. Après un moment, Ernaut démonta. Il avait reconnu la croix qu'il avait plantée quelques jours plus tôt. Il fit glisser Zénon de son cheval et fut rejoint par Eudes et Droart. Aucun d'eux ne délaça ses paquets pour préparer un éventuel campement. Le mosaïste sentit ses entrailles se serrer.

« Il est temps pour toi de révéler tes méfaits, Zénon. Nous avons vu que tu as avec toi bien des rouleaux et pages de codex pour un simple artisan. Sont-ce là les travaux du chanoine ? »

Le prisonnier se mordit les lèvres, le menton tremblant, puis il hocha la tête.

« Tu les as tués pour protéger le secret du feu de guerre ? L'avait-il découvert ?

--- Je ne sais ! Il me fallait prendre tout ce qui me semblait d'importance. Je suis simple mosaïste, pas savant...

--- Savant assez pour user de l'escarboucle pourtant. Pour quelle raison souiller ainsi leurs corps ? Veux-tu que la Résurrection leur soit refusée ?

--- C'était pour instiller la crainte à d'autres de s'aventurer en pareilles recherches ! Je n'ai pas voulu cela, je n'ai fait qu'obéir. »

Il déglutit bruyamment puis ajouta d'une voix pâteuse et hésitante :

« Allez-vous me tuer ?

--- Contrairement à toi, nous ne disposons pas de vie et de mort. Nous laissons cela à Dieu ! »

Il s'approcha du mosaïste, dont le visage était désormais mélange de pleurs, de morve et de salive.

« Pourquoi avoir tué le père Gonteux ? Il n'était mie savant et fort peu désireux d'aider à la guerre. Une âme de paix s'il y en eut jamais.

--- Comment l'aurais-je su ? J'ai appris qu'il savait le grec, qu'il était là en quête du père Waulsort. Je l'ai cru venu pour reprendre les recherches ! »

Ernaut en eut un frisson. Herbelot avait été occis par erreur, sur des faux-semblants et des impressions. Le crime n'en était que plus effroyable à ses yeux.

« Allez-vous me libérer ? Me remettre aux miens ? risqua Zénon d'une voix timide.

--- Je ne sais. Cela ne sera pas de mon fait, lui rétorqua sèchement Ernaut. Tu as la chance de m'encontrer maintenant que je me sais d'un autre métal que toi. Je te l'ai promis, je ne porterai pas la main sur toi. »

Visiblement inquiet du double sens que pouvaient avoir ces paroles, le Byzantin examina Eudes et Droart, ce qu'Ernaut remarqua.

« Tu n'as rien à craindre d'eux non plus ! »

Voyant que l'homme terrifié aurait désormais de la peine à marcher, Ernaut l'attrapa par l'épaule, le tourna et le fit avancer devant lui, jusqu'à l'arrêter sur la berge d'un petit étang encombré de roseaux et de mousse.

« Tu as profané un lieu sanctifié par saint Georges. Il me semble juste de voir si Dieu t'accordera le pardon et te sauvera des dragons. Si tu rebrousses chemin, nous t'accueillerons avec du fer. Traverse jusqu'à sauveté et nous oublierons t'avoir jamais vu. »

Puis il poussa Zénon dans le marais empli de crocodiles assoupis.

Épilogue
--------

### Jérusalem, hôtel de Philippe de Naplouse, soir du mercredi 28 janvier 1159

Le froid semblait s'être installé sur la Cité pour un moment. S'il n'avait pas neigé, il faisait néanmoins une température à pierre fendre et chacun se renfermait chez soi. Philippe de Milly était venu à Jérusalem pour discuter avec son frère Henri le Buffle, dont la présence au sein des territoires latins n'était qu'intermittente. C'était à se demander comment il avait pu avoir tant d'enfants avec son épouse, tant il voyageait, la plupart du temps en Égypte.

La fratrie se partageait un bel emplacement, non loin du palais de Baudoin III, qui n'accueillait leurs familles au complet que lors des principales fêtes, en juillet tout particulièrement, pour l'anniversaire de la prise de la ville. En ces saisons froides, cela n'était guère plus qu'un pied-à-terre pour héberger les frères de passage ou en mission auprès de la Cour.

Pour le moment, Henri sirotait son vin miellé en silence. Ils venaient d'évoquer le décès de Bernard Vacher, porte-bannière du roi qu'Henri appréciait tout particulièrement. C'était de lui qu'il avait appris les rudiments de diplomatie qu'il avait fait ensuite fructifier avant d'en faire quotidiennement usage. Quoique soldat de bon renom, il excellait dans la collecte de renseignements, en particulier dans les terres égyptiennes, où il résidait le plus souvent. De son côté, Philippe s'amusait avec les pièces en ivoire d'un jeu d'échecs où il aimait à affronter ses frères. Il allait en faire proposition à Henri quand un des valets souleva la portière, s'inclinant respectueusement. Il tenait à la main un lourd bagage de selle en cuir.

« Qu'est-ce donc ?

--- On vient de déposer cela à votre intention, sire. Il m'a été indiqué que c'étaient les courriers du chanoine que vous espériez.

--- On n'a rien dit d'autre ? Était-ce un homme d'armes, taillé à chaux et sable tel un géant ?

--- Rien de plus, sire. Le coursier n'était qu'un vieil homme accompagné d'un roquet galeux. »

Philippe hocha la tête et se fit porter le sac, qu'il entreprit de délacer, soudain songeur. Lorsque le valet fut ressorti, Henri s'enquit de ce qu'était cette livraison. Mais Philippe n'eut pas le temps de répondre qu'entrait dans la pièce leur demi-frère, Guy le François, sénéchal du comte de Jaffa. Toujours parfaitement habillé, rasé et coiffé, il était vêtu d'un magnifique bliaud de laine orangé dont les manches brillaient de broderies au fil d'or. Ses deux cadets le saluèrent avec chaleur, l'invitant à partager leur collation. Ils en oublièrent un moment la sacoche de cuir humide.

Au bout d'un moment, Henri s'en souvint et la montra de nouveau de sa main partiellement amputée.

« Alors, mon frère, qu'est-ce donc que tu en fais tel mystère ?

--- Il n'y a là rien qui puisse vous étonner. »

Il se tourna vers Guy, un sourire rusé sur le visage.

« La graine que nous avons semée et arrosée a germé, mon frère. Il ne faudrait tarder pour moissonner bel épi, dont on fera le meilleur des pains !» ❧

*À suivre...*

<p class="qita_licence">
[❖ ❖ ❖]{.qita_scission}
</p>
<p class="qita_info_licence2">
**Ce roman vous a plu ?**
</p>
<p class="qita_info_licence2">
N'hésitez pas à en parler, à l'envoyer à vos proches et à me le faire savoir.<br/>Cet ebook étant diffusé gratuitement, vous pouvez m'aider à écrire les prochains en me soutenant.
</p>
<p class="qita_info_licence2">
Faire un don sur <https://hexagora.fr/fr:donate>
</p>
<p class="qita_info_licence2">
Merci,<br/>Yann.<br/>
</p>
<p class="qita_licence">
[❖ ❖ ❖]{.qita_scission}
</p>
Notes de l'auteur
-----------------

L'idée de ce roman est née de la lecture de l'anecdote de Jean Phocas à propos de l'ouverture de la tombe de saint Georges à Lydda. Cette histoire, qui lui aurait été narrée par des prêtres du lieu lors de son passage en 1185, parle d'un flash qui brûla plusieurs hommes qui avaient tenté d'ouvrir le sépulcre, suite à la demande de l'évêque. J'y ai tout de suite flairé quelques éléments propres à une intrigue mâtinée de fantastique. En travaillant l'idée, je me suis dit que c'était aussi l'occasion de mettre en scène un aspect essentiel de mon rapport à l'histoire. En effet, j'estime toujours cette dernière comme le résultat d'une fabrication. D'où naquit la suggestion d'Ernaut de tordre la réalité pour qu'elle se conforme à une légende mieux adaptée. Cela me permettait en outre de bénéficier de plus de libertés opératoires, le lieu saint et un événement public étant bien évidemment plus difficiles à utiliser comme scène de crime.

Pour le cadre général, je me suis donc largement appuyé encore une fois sur les travaux de Denys Pringle (*Secular Buildings in the Crusader Kingdom of Jerusalem. An archaeological Gazetteer*, Cambridge University Press, 1997 & *The Churches of the Crusader Kingdom of Jerusalem. A corpus*, Deux volumes, Cambridge University Press, 1993 & 1998). Sa longue notice descriptive de l'existant et du connu des lieux m'a permis de brosser un portrait cohérent de ce lieu si particulier, palais forteresse et lieu de pèlerinage accolé à une ville.

J'ai également beaucoup étudié ce que j'ai pu trouver de l'histoire de Philippe de Naplouse, que les habitués des Qit'a connaissent déjà, ainsi que ses deux frères. Cet éminent baron offre beaucoup de prise à l'invention romanesque, ayant un parcours de vie tortueux et peu banal, tout en ne possédant pas une biographie bien précise. J'ai construit surtout autour de ce que j'ai pu trouver dans un article de Malcolm Barber (« The career of Philip of Nablus in the kingdom of Jerusalem » dans Peter Edbury & Jonathan Phillips (dir.), *The Experience of Crusading*, Volume 2, Cambridge University Press : 2003, p. 60--78), tout en étoffant avec quelques notices tirées du projet *Medieval Lands*, dirigé par Charles Cawley (*A prosopography of medieval European noble and royal families* - http://fmg.ac/Projects/MedLands/ ).

Le cheminement de l'intrigue, qui se voulait à la base un peu ésotérique afin de rendre hommage à un des vénérables initiateurs du genre, Umberto Eco avec *Le nom de la rose*, a peu à peu glissé vers la science. J'ai tout d'abord étudié les traditions alchimiques telles que j'ai pu les suivre à l'aide des travaux de Serge Hutin (*Les alchimistes au Moyen Âge*, Paris : Hachette Livre, 1995), complétés par des versions anciennes de *La table d'Émeraude* (*Hermès Trismégiste. La table d'Émeraude. Aux sources de la tradition*, Paris : Les Belles Lettres, 2008). Je me suis aussi pas mal appuyé sur le texte de Françoise Hudry sur le *De secretis nature* (« Le *De Secretis nature* du ps.-Apollonius de Tyane, traduction latine par Hugues de Santalla du Kitāb sirr al-halīqa », dans *Chrysopeia*, Tome VI - *Cinq traités alchimiques médiévaux*, Paris : C.N.R.S. Centre d'Étude des Sciences et des Doctrines, 1997-1999, p.1-154). Bien que tous ces éléments ne soient pas forcément d'importance dans le récit en lui-même, ils m'ont permis d'asseoir solidement le cadre général et de m'appuyer sur des éléments connus en ce qui concerne les savoirs hermétiques, auxquels étaient liées les connaissances scientifiques et leur diffusion.

J'ai bien sûr croisé tout cela avec des éléments d'histoire des sciences proprement dite, à savoir surtout le gros catalogue de l'exposition *L'âge d'or des sciences arabes* (Paris : Actes Sud/Institut du Monde Arabe, 2005), qui offre autant à voir qu'à lire. J'ai par ailleurs parcouru également le troisième tome de *L'histoire des sciences arabes*, sur « Technologie, alchimie et sciences de la vie » ( Collection dirigée par Roshdi Rashed, Paris : Éditions du Seuil, 1997) qui m'a permis de resituer tout cela dans un contexte plus global. C'est ce qui m'a incité à opter pour l'usage du phosphore blanc pour dissimuler les crimes et embrouiller les enquêteurs, même si sa « découverte » officielle est considérée comme plus tardive, en contexte européen. Je n'ai malheureusement pas pu me procurer le travail de Eduard Farber (*History of Phosphorus* , Bull United States National Museum, No. 240, 1965) et j'ai recouru à des auteurs plus anciens, tel que Jean-Chrétien-Ferdinand Hœfer (*Histoire de la Chimie*, vol. 1, Paris : Firmin-Didot, 1843, 339), ce qui m'a permis de retracer des mentions anciennes d'un manuscrit *Ordinatio Alchid Bechil Saraceni Philosophi* d'où je sors les termes d'escarboucle et de pierre ou poudre de lune.

Sur le feu grégeois, je me suis référé à John Haldon, qui a traité plusieurs fois du sujet et en a fait une synthèse récente (« Greek fire revisited: recent and current research » dans Elizabeth Jeffreys (dir.), *Byzantine Style, Religion and Civilization: In Honour of Sir Steven Runciman*, Cambridge University Press, p. 290--325).

Enfin, toutes les histoires autour de saint Georges, qui m'ont permis d'embrouiller plus encore les pistes, reposent pour l'essentiel sur le travail de Michel Van Esbroeck (« L'histoire de l'Église de Lydda dans deux textes géorgiens », dans *Bedi Kartlisa*, 36, 1977, p.109-131) et de Piotr Grotowski (« The Legend of St. George Saving a Youth from Captivity and Its Depiction in Art » dans *Serie Byzantina*, I, Varsovie : 2003, p.27-77). Là encore, ce fut pour moi, sur le fond, l'occasion de montrer combien les traditions culturelles s'entremêlent et se nourrissent, tout en offrant l'occasion de créer de fausses pistes d'un point de vue purement narratif.

À lire tout cet étalage de savantes références, je crains que l'on accorde une forte coloration de véracité à mon travail, qu'il ne gagne une autorité de même nature. Même si cela me semble nécessaire de donner les sources qui ont irrigué mon imagination, il ne s'agit absolument pas de prétendre que ce roman soit vrai. Il fut très certainement nourri également des lectures que j'ai faites alors que je le rédigeais, à savoir pour l'essentiel la saga Vorkosigan de Lois McMaster Bujold (Édition J'ai Lu, 3 volumes, 1997-2003) et *Fortune de France*, de Robert Merle (éditions Plon, 13 volumes, 1977-2003), qu'on ne présente plus, et à qui j'ai emprunté quelques tournures de phrase en hommage.

Si vous avez envie de prolonger l'aventure au-delà de la lecture de mes romans, j'ai mis en place un site Internet. Vous y trouverez toute ma production littéraire sous licence libre en téléchargement gratuit, des liens vers des endroits où vous en procurer des versions papier ainsi que des éléments de réflexion sur mon projet. Vous pourrez y prendre contact avec moi, y récupérer les sources de mes textes, ainsi qu'y trouver un moyen de soutenir mon travail, de devenir un de mes mécènes, en faisant un don ponctuel ou régulier.

[www.hexagora.fr](https://www.hexagora.fr)

[^1]: Allusion à l'Apocalypse de Jean.

[^2]: Hugues d'Ibelin (? - 1170), seigneur d'Ibelin et de Rama.

[^3]: Aujourd'hui Ramla, dans la plaine de Sharon.

[^4]: Voir le premier tome, *La nef des loups*.

[^5]: Joscelin III de Courtenay (1134 - v.1200) ne put jamais récupérer son comté d'Édesse. Sa soeur étant la première épouse d'Amaury de Jérusalem (et mère de Sybille et de Baudoin IV), il fit carrière dans le royaume de Jérusalem.

[^6]: Chiffon, torchon, mouchoir, selon le cas.

[^7]: Terme usuel pour désigner les Grecs, c'est à dire les Byzantins.

[^8]: Le terme peut désigner une famille, une maisonnée, une maison (le bâtiment), avec toutes les polysémies de ces termes.

[^9]: Voir le second tome, *Les Pâques de sang*.

[^10]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^11]: Apocalypse 12:3

[^12]: Voir le premier tome, *La nef des loups*.

[^13]: Purée ou hachis de légume.

[^14]: Sorte de robe à manches longues, vêtement de base des populations moyen-orientales d'alors.

[^15]: Large robe aux manches amples.

[^16]: Petite pièce de monnaie, de faible valeur.

[^17]: Terme désignant un converti, baptisé après avoir abjuré une autre religion.

[^18]: Génie magicien.

[^19]: Genre de loyer.

[^20]: Savant, philosophe (IXe siècle) et, surtout, traducteur depuis le grec vers le syriaque de *Galien*, médecin grec très renommé en Méditerranée orientale.

[^21]: Le *secret des secrets*.

[^22]: *Pyr*, prononcé \[piɾ\].

[^23]: La seconde croisade, qui a échoué devant Damas en 1148 et n'a abouti à rien de vraiment concret.

[^24]: Voir le troisième tome, *La terre des morts*.

[^25]: Voir le premier tome, *La nef des loups*.

[^26]: Les Cisterciens, connus pour leurs talents de gestionnaires et d'innovateurs techniques.

[^27]: Pâtisserie du Levant, à base de pâte farcie généralement de dattes, de pistaches ou de noix. Des versions salées, avec du poisson, existent.

[^28]: Des crocodiles.

[^29]: Voir le troisième tome, *La terre des morts*.

[^30]: Jetons, pion, servant parfois de ticket, voire de monnaie.

[^31]: Voir le second tome, *Les Pâques de sang*.

[^32]: Unité de poids valant approximativement une tonne, soit 20 quintaux médiévaux, soit 2000 livres de poids.

[^33]: Voir le premier tome, *La nef des loups*.

[^34]: Vient du mot arabe qui désigne familièrement le blé et l'orge.

[^35]: L'expression désigne le travail qui ne se fait pas à la lueur du jour, en public. Il est généralement interdit aux artisans, pour éviter qu'ils ne travaillent mal ou de façon malhonnête. De là découle la notion de travail caché.

[^36]: Tripoli, actuellement au Liban.

[^37]: La Toussaint, soit le 1er novembre.

[^38]: « Maître », en l'occurrence, responsable d'un des secteurs du marché.

[^39]: Nom que les Latins donnaient parfois à l'Achaïe, région au nord-ouest du Péloponnèse.

[^40]: Forme médiévale de l'expression *potron-minet*, qui désigne l'aube. Le choix de ce terme ici est en hommage à Robert Merle, qui me le fit découvrir dans son excellente série *Fortune de France*.

[^41]: Allusion au mariage du roi Baudoin avec la jeune princesse byzantine Théodora Comnène, nièce de l'empereur Manuel.

[^42]: Période pendant laquelle on a le droit de manger de la *chair*, de la viande.

[^43]: Unité militaire, de cavalerie généralement, mais qui peut avoir un sens général.

[^44]: Charlemagne était la figure légendaire favorite de beaucoup de traditions, et on lui prêtait des voyages un peu partout.

[^45]: Voir le troisième tome, *La terre des morts*.

[^46]: Chandeleur, quarante jours après Noël, soit le 2 février.

[^47]: Voir le troisième tome, *La terre des morts*.
