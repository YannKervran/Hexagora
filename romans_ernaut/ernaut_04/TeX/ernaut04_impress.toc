\babel@toc {french}{}
\contentsline {section}{Liste des personnages}{{\fontencoding {T1}\selectfont i}}{section*.3}% 
\contentsline {chapter}{\numberline {1}Prologue}{1}{chapter.0.1}% 
\contentsline {section}{\numberline {}Monast\IeC {\`e}re b\IeC {\'e}n\IeC {\'e}dictin de la Charit\IeC {\'e}-sur-Loire, hiver 1223}{1}{section.0.1.1}% 
\contentsline {chapter}{\numberline {2}Chapitre 1}{3}{chapter.0.2}% 
\contentsline {section}{\numberline {}Lydda, chantier de construction de la cath\IeC {\'e}drale,\\ matin du mardi 9 d\IeC {\'e}cembre 1158}{3}{section.0.2.1}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, palais du Patriarche, fin de matin\IeC {\'e}e\\ du vendredi 12 d\IeC {\'e}cembre 1158}{6}{section.0.2.2}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, palais royal, matin\\ du dimanche 14 d\IeC {\'e}cembre 1158}{12}{section.0.2.3}% 
\contentsline {section}{\numberline {}Lydda, abords du palais de l'\IeC {\'e}v\IeC {\^e}que,\\soir du dimanche 14 d\IeC {\'e}cembre 1158}{19}{section.0.2.4}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais de l'\IeC {\'e}v\IeC {\^e}que,\\matin du lundi 15 d\IeC {\'e}cembre 1158}{26}{section.0.2.5}% 
\contentsline {section}{\numberline {}Lydda, cour des Bourgeois, fin de matin\IeC {\'e}e\\du lundi 15 d\IeC {\'e}cembre 1158}{31}{section.0.2.6}% 
\contentsline {chapter}{\numberline {3}Chapitre 2}{39}{chapter.0.3}% 
\contentsline {section}{\numberline {}Lydda, r\IeC {\'e}fectoire du palais, apr\IeC {\`e}s-midi\\du lundi 15 d\IeC {\'e}cembre 1158}{39}{section.0.3.1}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais \IeC {\'e}piscopal,\\fin d'apr\IeC {\`e}s-midi du lundi 15 d\IeC {\'e}cembre 1158}{46}{section.0.3.2}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais \IeC {\'e}piscopal,\\veill\IeC {\'e}e du lundi 15 d\IeC {\'e}cembre 1158}{52}{section.0.3.3}% 
\contentsline {section}{\numberline {}Lydda, cour de l'h\IeC {\^o}tellerie du palais,\\nuit du lundi 15 d\IeC {\'e}cembre 1158}{59}{section.0.3.4}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais \IeC {\'e}piscopal,\\matin du mardi 16 d\IeC {\'e}cembre 1158}{63}{section.0.3.5}% 
\contentsline {section}{\numberline {}Lydda, chantier de la basilique Saint-Georges,\\fin de matin\IeC {\'e}e du mardi 16 d\IeC {\'e}cembre 1158}{71}{section.0.3.6}% 
\contentsline {section}{\numberline {}Lydda, clo\IeC {\^\i }tre du palais, fin d'apr\IeC {\`e}s-midi\\du mardi 16 d\IeC {\'e}cembre 1158}{78}{section.0.3.7}% 
\contentsline {chapter}{\numberline {4}Chapitre 3}{85}{chapter.0.4}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, matin\IeC {\'e}e\\du mercredi 17 d\IeC {\'e}cembre 1158}{85}{section.0.4.1}% 
\contentsline {section}{\numberline {}Lydda, cellules du dortoir canonial,\\apr\IeC {\`e}s-midi du mercredi 17 d\IeC {\'e}cembre 1158}{93}{section.0.4.2}% 
\contentsline {section}{\numberline {}Lydda, demeure du chanoine Waulsort,\\soir\IeC {\'e}e du mercredi 17 d\IeC {\'e}cembre 1158}{100}{section.0.4.3}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais \IeC {\'e}piscopal,\\nuit du mercredi 17 d\IeC {\'e}cembre 1158}{106}{section.0.4.4}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais \IeC {\'e}piscopal,\\matin du jeudi 18 d\IeC {\'e}cembre 1158}{111}{section.0.4.5}% 
\contentsline {section}{\numberline {}Lydda, quartier de Samarie,\\matin du jeudi 18 d\IeC {\'e}cembre 1158}{116}{section.0.4.6}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal,\\soir\IeC {\'e}e du jeudi 18 d\IeC {\'e}cembre 1158}{123}{section.0.4.7}% 
\contentsline {chapter}{\numberline {5}Chapitre 4}{129}{chapter.0.5}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais de l'archev\IeC {\^e}que,\\fin de matin\IeC {\'e}e du vendredi 19 d\IeC {\'e}cembre 1158}{129}{section.0.5.1}% 
\contentsline {section}{\numberline {}Lydda, chantier de la basilique Saint-Georges,\\apr\IeC {\`e}s-midi du vendredi 19 d\IeC {\'e}cembre 1158}{134}{section.0.5.2}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais \IeC {\'e}piscopal,\\soir\IeC {\'e}e du vendredi 19 d\IeC {\'e}cembre 1158}{141}{section.0.5.3}% 
\contentsline {section}{\numberline {}Lydda, chambre de l'\IeC {\'e}v\IeC {\^e}que, veill\IeC {\'e}e\\du vendredi 19 d\IeC {\'e}cembre 1158}{145}{section.0.5.4}% 
\contentsline {section}{\numberline {}Lydda, magasins du palais \IeC {\'e}piscopal,\\matin\IeC {\'e}e du samedi 20 d\IeC {\'e}cembre 1158}{151}{section.0.5.5}% 
\contentsline {chapter}{\numberline {6}Chapitre 5}{159}{chapter.0.6}% 
\contentsline {section}{\numberline {}Lydda, quartiers des teinturiers,\\fin d'apr\IeC {\`e}s-midi du samedi 20 d\IeC {\'e}cembre 1158}{159}{section.0.6.1}% 
\contentsline {section}{\numberline {}Lydda, quartier de la porte de Samarie,\\soir\IeC {\'e}e du samedi 20 d\IeC {\'e}cembre 1158}{166}{section.0.6.2}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, veill\IeC {\'e}e\\du samedi 20 d\IeC {\'e}cembre 1158}{171}{section.0.6.3}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais de l'\IeC {\'e}v\IeC {\^e}que Constantin,\\fin de matin\IeC {\'e}e du dimanche 21 d\IeC {\'e}cembre 1158}{177}{section.0.6.4}% 
\contentsline {chapter}{\numberline {7}Chapitre 6}{187}{chapter.0.7}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, porte de David, fin de matin\IeC {\'e}e\\du lundi 22 d\IeC {\'e}cembre 1158}{187}{section.0.7.1}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, palais du patriarche, fin de matin\IeC {\'e}e\\du mardi 23 d\IeC {\'e}cembre 1158}{194}{section.0.7.2}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, palais royal,\\mi-journ\IeC {\'e}e du mardi 23 d\IeC {\'e}cembre 1158}{200}{section.0.7.3}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, demeure de Droart,\\soir\IeC {\'e}e du jeudi 25 d\IeC {\'e}cembre 1158}{204}{section.0.7.4}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, fin d'apr\IeC {\`e}s-midi\\du samedi 27 d\IeC {\'e}cembre 1158}{210}{section.0.7.5}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, fin de matin\IeC {\'e}e\\du dimanche 28 d\IeC {\'e}cembre 1158}{216}{section.0.7.6}% 
\contentsline {section}{\numberline {}Lydda, quartier de la porte de Samarie,\\apr\IeC {\`e}s-midi du dimanche 28 d\IeC {\'e}cembre 1158}{222}{section.0.7.7}% 
\contentsline {chapter}{\numberline {8}Chapitre 7}{227}{chapter.0.8}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, fin d'apr\IeC {\`e}s-midi\\du dimanche 28 d\IeC {\'e}cembre 1158}{227}{section.0.8.1}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, matin\\du lundi 29 d\IeC {\'e}cembre 1158}{232}{section.0.8.2}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, fin de matin\IeC {\'e}e\\du lundi 29 d\IeC {\'e}cembre 1158}{238}{section.0.8.3}% 
\contentsline {chapter}{\numberline {9}Chapitre 8}{245}{chapter.0.9}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, apr\IeC {\`e}s-midi\\du lundi 29 d\IeC {\'e}cembre 1158}{245}{section.0.9.1}% 
\contentsline {section}{\numberline {}Lydda, quartier des teinturiers,\\apr\IeC {\`e}s-midi du lundi 29 d\IeC {\'e}cembre 1158}{252}{section.0.9.2}% 
\contentsline {section}{\numberline {}Lydda, jardins de l'\IeC {\'e}v\IeC {\^e}ch\IeC {\'e}, fin d'apr\IeC {\`e}s-midi\\du mardi 30 d\IeC {\'e}cembre 1158}{259}{section.0.9.3}% 
\contentsline {section}{\numberline {}Lydda, clo\IeC {\^\i }tre canonial, fin d'apr\IeC {\`e}s-midi\\du mercredi 31 d\IeC {\'e}cembre 1158}{264}{section.0.9.4}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais \IeC {\'e}piscopal,\\matin du jeudi 1er janvier 1159}{272}{section.0.9.5}% 
\contentsline {chapter}{\numberline {10}Chapitre 9}{277}{chapter.0.10}% 
\contentsline {section}{\numberline {}Lydda, palais \IeC {\'e}piscopal, fin d'apr\IeC {\`e}s-midi\\du samedi 3 janvier 1159}{277}{section.0.10.1}% 
\contentsline {section}{\numberline {}Lydda, h\IeC {\^o}tellerie du palais \IeC {\'e}piscopal,\\matin du lundi 5 janvier 1159}{281}{section.0.10.2}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, palais royal, midi\\du mardi 6 janvier 1159}{287}{section.0.10.3}% 
\contentsline {section}{\numberline {}Petite Mahomerie, demeure de Sanson de Brie,\\apr\IeC {\`e}s-midi du dimanche 11 janvier 1159}{293}{section.0.10.4}% 
\contentsline {chapter}{\numberline {11}Chapitre 10}{299}{chapter.0.11}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, palais royal, fin de matin\IeC {\'e}e\\du lundi 12 janvier 1159}{299}{section.0.11.1}% 
\contentsline {section}{\numberline {}Palmeraie de Rama, fin d'apr\IeC {\`e}s-midi\\du lundi 26 janvier 1159}{302}{section.0.11.2}% 
\contentsline {chapter}{\numberline {12}\IeC {\'E}pilogue}{311}{chapter.0.12}% 
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, h\IeC {\^o}tel de Philippe de Naplouse,\\soir du mercredi 28 janvier 1159}{311}{section.0.12.1}% 
\contentsline {chapter}{\numberline {13}Addenda}{315}{chapter.0.13}% 
\contentsline {section}{\numberline {}Notes de l'auteur}{315}{section.0.13.1}% 
\contentsfinish 
