## Liste des personnages

***Lydda, royaume de Jérusalem***  
Adam, chanoine  
Amos, maître teinturier  
Anquetin, artisan membre du guet à Lydda  
Ascelin, chanoine  
Aymar, sergent  
Barsam de Galilée, chef maçon  
Bertaut, valet  
Clément d’Acre, architecte de la basilique  
Constantin, évêque et seigneur de Lydda  
Danyel, dit père Breton, chanoine et drapier  
Élies, doyen des chanoines  
Gembert, chanoine et hospitalier  
Gerbaut, manouvrier  
Gilbert, clerc de l’administration épiscopale  
Hashim, gamin des rues  
Heimart, chanoine et trésorier  
Léo, presbytre byzantin  
Maître Domenico, chef charpentier  
Marzawi, valet d’un loueur de chevaux à Lydda  
Maugier du Toron, vicomte de Lydda  
Oleg, boyard de Staraya Ladoga  
Régnier de Waulsort, chanoine  
Rolant, sergent  
Thibault, valet de Régnier de Waulsort  
Umayyah, fabricant de paniers  
Willelmo Marzonus, négociant en cordages  
Zénon de Morée, maître mosaïste  

***Administration royale***  
Arnulf, vicomte de Jérusalem  
Basequin, sergent  
Baset, sergent  
Baset, sergent  
Brun, clerc de l’administration des finances royales  
Droart, sergent  
Ernaut, sergent  
Eudes, sergent  
Ganelon, sergent de Régnier d’Eaucourt  
Gaston, sergent  
Guillemot, sergent  
Jean d’Acre, sénéchal du roi Baudoin  
Joscelin de Courtenay, maréchal du roi Baudoin III  
Raoul, scribe  
Régnier d’Eaucourt, chevalier  
Ucs de Monthels, mathessep  

***Clergé***  
Amaury de Nesle, patriarche latin de Jérusalem  
Frédéric de la Roche, évêque d’Acre  

***Clergé de Tyr***  
Germain, valet  
Herbelot Gonteux, secrétaire de Pierre de Barcelone  
Pierre de Barcelone, archevêque de Tyr  

***Proches d’Ernaut***  
Lambert, frère d’Ernaut, colon à La Mahomerie  
Libourc, promise de Ernaut  
Mahaut, mère de Libourc  
Osanne, épouse de Lambert  
Sanson de Brie, père de Libourc  

***Divers***  
Amaury, comte de Jaffa, frère du roi Baudoin 
Baudoin III, roi de Jérusalem  
Guy le François, sénéchal du comte de Jaffa  
Henri le Buffle, chevalier du roi Baudoin  
Hugues d’Ibelin, seigneur de la cité de Rama  
Manuel, basileus, ou empereur, byzantin  
Philippe de Milly, seigneur de Naplouse  
Wibald de Stavelot, conseiller de Frédéric Barberousse  
