## Épilogue

### Jérusalem, hôtel de Philippe de Naplouse, soir du mercredi 28 janvier 1159

Le froid semblait s’être installé sur la Cité pour un moment. S’il n’avait pas neigé, il faisait néanmoins une température à pierre fendre et chacun se renfermait chez soi. Philippe de Milly était venu à Jérusalem pour discuter avec son frère Henri le Buffle, dont la présence au sein des territoires latins n’était qu’intermittente. C’était à se demander comment il avait pu avoir tant d’enfants avec son épouse, tant il voyageait, la plupart du temps en Égypte.

La fratrie se partageait un bel emplacement, non loin du palais de Baudoin III, qui n’accueillait leurs familles au complet que lors des principales fêtes, en juillet tout particulièrement, pour l’anniversaire de la prise de la ville. En ces saisons froides, cela n’était guère plus qu’un pied-à-terre pour héberger les frères de passage ou en mission auprès de la Cour.

Pour le moment, Henri sirotait son vin miellé en silence. Ils venaient d’évoquer le décès de Bernard Vacher, porte-bannière du roi qu’Henri appréciait tout particulièrement. C’était de lui qu’il avait appris les rudiments de diplomatie qu’il avait fait ensuite fructifier avant d’en faire quotidiennement usage. Quoique soldat de bon renom, il excellait dans la collecte de renseignements, en particulier dans les terres égyptiennes, où il résidait le plus souvent. De son côté, Philippe s’amusait avec les pièces en ivoire d’un jeu d’échecs où il aimait à affronter ses frères. Il allait en faire proposition à Henri quand un des valets souleva la portière, s’inclinant respectueusement. Il tenait à la main un lourd bagage de selle en cuir.

« Qu’est-ce donc ?

— On vient de déposer cela à votre intention, sire. Il m’a été indiqué que c’étaient les courriers du chanoine que vous espériez.

— On n’a rien dit d’autre ? Était-ce un homme d’armes, taillé à chaux et sable tel un géant ?

— Rien de plus, sire. Le coursier n’était qu’un vieil homme accompagné d’un roquet galeux. »

Philippe hocha la tête et se fit porter le sac, qu’il entreprit de délacer, soudain songeur. Lorsque le valet fut ressorti, Henri s’enquit de ce qu’était cette livraison. Mais Philippe n’eut pas le temps de répondre qu’entrait dans la pièce leur demi-frère, Guy le François, sénéchal du comte de Jaffa. Toujours parfaitement habillé, rasé et coiffé, il était vêtu d’un magnifique bliaud de laine orangé dont les manches brillaient de broderies au fil d’or. Ses deux cadets le saluèrent avec chaleur, l’invitant à partager leur collation. Ils en oublièrent un moment la sacoche de cuir humide.

Au bout d’un moment, Henri s’en souvint et la montra de nouveau de sa main partiellement amputée.

« Alors, mon frère, qu’est-ce donc que tu en fais tel mystère ?

— Il n’y a là rien qui puisse vous étonner. »

Il se tourna vers Guy, un sourire rusé sur le visage.

« La graine que nous avons semée et arrosée a germé, mon frère. Il ne faudrait tarder pour moissonner bel épi, dont on fera le meilleur des pains !» ❧

*À suivre…*
