## Chapitre 1

### Jérusalem, rue de David, matin du dimanche 14 avril 1157

La foule était toujours dense dans la rue de David. Un ciel gris, uniforme, n’apportait que peu de lumière et aucune chaleur. Bousculé par les marchands pressés, les portefaix encombrés, les animaux bâtés, Ernaut avançait doucement, suivant docilement la file qui serpentait entre les étals. Sa haute taille lui permettait d’examiner à son envie ici et là et son imposant gabarit, tout en muscles, empêchait qu’on n’entrave son cheminement, du moins de façon volontaire. Le jeune homme avait revêtu sa tenue de voyage, en laine solide, et arborait un turban plus ou moins correctement enroulé autour de la tête. Son aspect de guingois donnait un côté comique au personnage, malgré la puissance qui émanait de son physique. Les mois passés en mer et sur les routes avaient tanné sa peau, contrastant avec ses cheveux et ses sourcils clairs. Sa barbe, de plusieurs jours, habillait d’un léger hâle brun son menton. Il remonta la rue du Patriarche de façon à rejoindre l’église du Saint-Sépulcre par la chapelle Sainte-Marie : par un escalier, l’accès en était plus direct jusqu’à la tombe du Christ. Tandis qu’il descendait les marches, Ernaut eut un pincement au cœur. Il ne traversait plus le lieu sans se remémorer les tragiques événements auxquels il avait été mêlé. Sans s’y attarder, il laissa ses yeux traîner au sol, à la recherche de quelques traces de ce qui s’était passé. Plusieurs fidèles, visiblement des Arméniens, étaient occupés à prier devant les icônes.

Ernaut les dépassa et entra dans la vaste rotonde. À l’abri de l’imposante coupole, l’édicule renfermant le tombeau du Christ était, comme d’habitude, environné de nombreux pèlerins. Néanmoins, on pouvait désormais se déplacer à son aise, le flot des croyants étant reparti à la fin de la semaine de Pâques. Ici et là, des prêtres encadraient les prières, assistaient les âmes. À l’entrée du passage qui ouvrait vers l’église et le chœur, sous la haute arcade, un chanoine entre deux âges surveillait d’un regard d’aigle les alentours. Le visage plutôt carré, sa tonsure ne laissait qu’une couronne brune parsemée de gris autour de son crâne. Les paupières plissées, la bouche aux lèvres fines pincées, il observait sans bouger la tête, ses yeux vifs se contentant d’aller de droite et de gauche. Sa coule était d’une belle laine noire, ample et souple, accentuant l’impression de gravité s’échappant du personnage. Il n’était pas question pour lui de tolérer un officiant qui ne soit pas autorisé à organiser une messe improvisée ou s’installer sur un autel. Tout était codifié, planifié, chaque congrégation avait ses droits, et privilèges, et comptait bien les voir respectés. Le clerc était là pour cela. Parmi la foule, Ernaut ne pouvait passer inaperçu. Les yeux du chanoine s’arrêtèrent sur le jeune homme et un sourcil fin s’arqua en une grimace de désapprobation. Il fit quelques pas d’une démarche empressée, le délicat lainage de sa longue tenue volant autour de ses jambes. Un observateur attentif aurait pu voir son crâne brillant, fraîchement rasé, osciller doucement de droite et de gauche en signe de désaccord. Il attendit d’être auprès du géant pour faire entendre sa voix, d’un ton sec :

« Mon fils, quelle surprise de vous revoir céans ! Je vous pensais déjà retournant en votre pays. »

À voir l’éclat qui brillait dans ses yeux, il formulait un espoir plus qu’une pensée. Ernaut ne chercha pas à relever et hocha la tête avec humilité.

« Si fait, père, je ne devrais plus être en cette cité, mon frère déjà chemine au nord. »

Étonné devant si rapide reddition, le chanoine attira Ernaut un peu à l’écart, sous les arcades des hauts piliers. Habitué à accueillir et rassurer les pèlerins, il savait reconnaître ceux qui se présentaient en détresse. Il oubliait alors tout, y compris ses griefs, soucieux de ne se consacrer qu’à son devoir de pasteur des âmes.

« Subsiste-t-il encore soucis en vous que vous demeuriez près le sépulcre du Christ ? Ne puis-je apporter quelque baume sur vos plaies ?

— Je ne sais. Malgré toute ma dévotion, je me sens fort égaré depuis ces Pâques. Je pensais trouver là accomplissement de ma Foi, je ne ressens que grand trouble. »

La remarque fit se dessiner un rictus moqueur sur le visage austère.

« Peut-être sentez-vous en votre cœur le malaise que vous avez engendré. Seul bien vilain mécréant saurait vivre en paix après cela. »

Ernaut lança un regard contrarié au petit homme qui osait le provoquer ainsi, puis se calma bien vite.

« Certes oui. Je ne peux me prétendre fier de pareil outrage, mais il me semblait que j’œuvrai sur le chemin du Christ, le prenant en semblance pour mon Salut et celui de faibles.

— Il est aisé de se fourvoyer quand la passion vous habite, garçon ! » ajouta le chanoine, sentencieux.

Le jeune homme inclina la tête, conscient de la vérité de ces paroles. Cela n’était pas une découverte pour lui et ne lui apportait aucun réconfort.

« Mais lorsque les démons s’en prennent à innocents, faibles, que pouvons-nous faire, que devons-nous faire, mon père ? »

Le chanoine examina Ernaut comme il l’aurait fait d’une bête de somme à la foire, prenant quelques instants pour réfléchir à sa réponse.

« Il n’est pas possible pour nous de comprendre les desseins du Seigneur, nous devons bonnement les accepter et nous soumettre.

— Mais si Dieu m’a fait tel que je suis, ne dois-je pas m’efforcer d’user des miens talents pour plus faibles que moi, comme chien protégeant brebis ? »

Le chanoine réprima une exclamation amusée.

« Voilà bien étrange idée pour justifier si fâcheuse aventure. Il n’est guère en nos moyens de changer les choses, garçon. Je serais incliné à croire que vous oubliez devoirs de bon chrétien. Alors même que vous étiez pérégrin. L’intention peut être belle, mais elle doit cheminer en bon sentier, et le chien n’est rien sans la houlette d’un juste pasteur. Sinon l’arrivée se fait en terrible lieu, où les âmes se tourmentent. »

Voyant que son sermon faisait mouche, il conclut sa démonstration d’une voix moins amène.

« C’est ce qui t’est arrivé, garçon. »

Ernaut hocha la tête, contrit. Une angoisse commençait à lui comprimer la poitrine. Comme le chanoine avait aisément résumé ce qu’il ressentait ! Il lui fallait trouver une solution pour alléger son fardeau, retrouver la paix intérieure.

« N’y a-t-il moyen de cheminer à rebours, de racheter pareille faute ?

— La Maison du Seigneur n’a jamais porte close, mais pour si fort outrage, grande pénitence est nécessaire. As-tu vu confesseur qui puisse te guider droitement ? »

Le géant secoua la tête en dénégation. Il n’était pas encore prêt à s’expliquer en détail devant un prêtre, et certainement pas devant Dieu.

« Alors, empresse-toi de le faire. »

Ernaut fouilla dans sa besace, faisant tinter les pièces qu’il resserrait dans sa bourse.

« Serait-il de quelque profit de verser offrandes au sanctuaire, pour messes ou luminaires ? »

Une lueur intéressée brilla furtivement dans les yeux du clerc, vite maîtrisée, tandis que sa voix se faisait condescendante. Des années d’appels aux dons entraînaient chez lui des réflexes dont il n’était certainement même plus conscient.

« Il n’est jamais inutile de le faire, car les prières aident au pardon de l’âme par le Seigneur. Mais, s’il devient alors plus enclin à t’écouter, il lui faut t’entendre pour pouvoir pardonner. Le saint Office ne saurait tarder, nul doute qu’il t’apportera grand réconfort si tu t’ouvres de vrai au Christ. Nous sommes au dimanche du Bon Pasteur, ne peut-on voir là signe qu’il a désir de te ramener au parmi du troupeau ? »

Ernaut acquiesça en silence, remâchant son trouble. Il lança un coup d’œil vers le tombeau du Christ, pour lequel il avait cheminé des mois durant pour son Salut et dans l’espoir d’une vie nouvelle. Malgré toute cette attente, et l’accomplissement de son voeu, il ressentait un immense chagrin en lui, avivé par le souvenir d’un engagement failli.

### Jérusalem, Temple du Seigneur, matin du dimanche 24 mars 1157

Le soleil venait à peine de se montrer derrière le Mont des Oliviers, pourtant la foule était immense, assemblée face à ce qu’on nommait alors le Temple du Seigneur. Les pèlerins étaient si nombreux que certains tentaient de grimper aux arbres qui parsemaient l’esplanade pour ne rien manquer des cérémonies. Comme chacun brandissait qui un rameau d’olivier, qui une branche de palmier, on aurait dit une forêt vivante, ondoyant sous le souffle d’une puissante tempête. Chacun s’efforçait d’être au plus près du petit groupe d’officiants perchés sur le parvis devant le grand bâtiment octogonal. Habillés avec opulence, ils entouraient un évêque, reconnaissable à sa chape rouge, sa haute mitre et sa crosse d’or scintillant dans le soleil du matin. Lorsqu’il s’approcha, encadré d’autres prêtres, chacun comprit qu’il s’apprêtait à bénir les végétaux agités vers lui. Les fidèles criaient des Alleluia sans même entendre ce qui se disait.

Parmi eux, Ernaut, immense gaillard à l’aspect encore juvénile, surpassait tous ses voisins au moins de la tête et le plus souvent des épaules. Il arborait pour l’occasion une coupe à l’écuelle toute fraîche et un rasage récent. Son visage franc, carré, mais affable, surplombait une masse de muscles lui permettant de brandir à lui seul plus de feuillages que n’en auraient eu besoin vingt personnes. Enthousiaste, il criait comme les autres, s’efforçant de répondre par des hurlements fervents aux prières et supplications initiées par l’évêque, dont il n’apercevait que la silhouette. Autour de lui, certains lui lançaient des regards courroucés ou indulgents, parfois admiratifs.

Ernaut ne faisait pas à proprement partie d’un groupe de voyageurs venus jusqu’ici. Son seul compagnon, son frère Lambert, était alité dans le grand hôpital de Saint-Jean, aux prises avec de mauvaises fièvres depuis qu’il avait contracté la vérole à leur retour de Galilée. Il y avait bien quelques passagers du *Falconus*[^voirt1] avec qui il avait plaisir à se retrouver pour les cérémonies, ou partager un repas, sans pour autant prétendre être intégré à leur communauté. Ils ne s’étaient d’ailleurs croisés que rarement lors de leurs visites dans les différents endroits cités par la Bible, et le fil qui les reliait s’était peu à peu distendu.

Lambert et lui n’étaient pas de simples marcheurs de Dieu, ils étaient là pour honorer un vœu, certes, mais ils prévoyaient de s’installer, de profiter des offres alléchantes que les souverains d’Outremer faisaient aux Latins pour venir s’établir. À peine débarqués, ils s’étaient empressés, Lambert surtout, de découvrir tous les lieux saints, en attendant de pouvoir assister aux célébrations de Pâques. Ils se mettraient en quête de quelques arpents à cultiver une fois leurs dévotions accomplies. Pour Ernaut, si le voyage était plaisant, toutes les cérémonies lui paraissaient la plupart du temps ennuyeuses.

Il ne comprenait rien à ce que faisaient les religieux et il participait du bout des lèvres, chantant souvent en retard de sa voix râpeuse, parfois les mauvais hymnes. Il ne partageait pas la foi simple et sincère de son frère et n’avait estimé sa venue outremer que comme une occasion de découvrir des régions inconnues, de nouveaux lieux, voire de nouvelles aventures. Il n’était plus désormais suffisant pour lui d’être maître du Poron[^voirqitaporon], il espérait devenir quelqu’un de plus important, comme son père l’avait toujours pressenti. Et la Terre sainte, le royaume de Jérusalem, représentait l’endroit idéal pour cela.

Pour l’heure, le jeune homme, emporté par l’enthousiasme ambiant, agitait en tout sens ses branches, manquant d’assommer ou d’éborgner une bonne douzaine de personnes à chaque ressaut de la vague. Les palmes et les rameaux constitueraient pour tous ces voyageurs le symbole de leur venue en ces lieux révérés, un souvenir longtemps chéri, qu’ils montreraient avec respect à leurs amis, leurs enfants. Ils tiendraient en main le témoin matériel de l’aventure spirituelle qu’ils étaient pour la plupart en train de vivre.

Les religieux, chanoines du Saint-Sépulcre, moines de Saint-Jean de l’Hôpital, de Sainte-Marie Latine et du Mont Sion, s’organisèrent finalement en procession pour rejoindre, par le nord du Quartier du Temple puis la porte de Josaphat, le cortège solennel du Patriarche qui revenait de Béthanie avec la Sainte Croix. La cohue était immense, pourtant l’avancée se faisait sans heurt, malgré la presse.

Imprégné de l’ambiance fervente, Ernaut attrapa un enfant qui semblait noyé dans la masse, parmi ses frères, sœurs et amis plus grands et le posa un temps sur ses épaules, s’attirant un sourire reconnaissant du père, débordé à gérer sa marmaille. Bien que la tête du cortège chantait à l’unisson avec les religieux, le milieu de colonne où se trouvait le géant se contentait de prières proférées avec plus de piété que de talent, et du brouhaha s’échappait parfois quelques amen ou kyrie, lancés par un groupe de voix plus déterminées que les autres.

Cahin-caha, la procession franchit l’entrée orientale de la ville, et descendit dans la vallée. Le soleil soulignait les contours de la foule qui venait face à eux, depuis l’est. Très tôt, les plus courageux avaient accompagné le patriarche jusqu’à l’endroit où Jésus avait ressuscité Lazare et s’en revenaient par le Mont des Oliviers, galvanisés par la vision de leurs frères et sœurs issus de la cité, avec lesquelles ils allaient pénétrer, sur les traces du Christ, dans Jérusalem, par la Porte Dorée. Elle n’était ouverte que pour cette occasion annuelle, murée le reste du temps. Devant eux, les prélats s’étaient rejoints autour de la sainte Croix, dans son reliquaire d’or, portée par le vieux patriarche Foucher d’Angoulême, assisté de quelques jeunes clercs.

Le soleil faisait scintiller la relique, obligeant ses admirateurs à plisser les yeux, comme s’ils cherchaient à voir une lumière trop brillante pour de simples mortels. Progressivement, un chant parvint aux pèlerins les plus éloignés, « *Ave, Rex noster*[^averexnoster] » initié par les chantres, puis repris par la foule qui s’agenouillait au fur et à mesure. Lorsqu’ils se relevèrent, ce fut pour entonner tous en chœur la même antienne, qu’Ernaut répéta en boucle jusqu’à ce qu’il comprenne que les autres enchaînaient sur « *Fili David*[^averexnosterb] ».

Comme il mettait tout son cœur dans les paroles qu’il déclamait, il rougit face aux quelques rictus amusés ou courroucés de son entourage et, vexé, décida de se contenter de bouger les lèvres sans chercher à en faire sortir le moindre son. Ce n’était pas la première fois qu’il recourait à ce stratagème depuis qu’il était arrivé. Le nombre de cérémonies auxquelles il avait assisté et leur complexité l’avaient dissuadé de tenter d’en comprendre le déroulement, encore moins d’en prévoir les prières. Il souriait benoîtement à une vieille femme, comme pour s’excuser de son erreur lorsqu’il réalisa que ses compagnons étaient tous de nouveau en train de s’agenouiller.

Le temps qu’il s’exécutât, tout le monde se redressait au chant du « *Pueri Hebraerum, portantes ramos olivarum*[^puerihebraerum] ». Et quand il se décida finalement à participer à cette nouvelle antienne qui se répétait, hurlant de sa plus belle voix, « *portantes ramos olivarum* », ce fut pour entendre autour de lui « *vestimenta prosternebant in via*[^vestimenta] ». Cette fois-ci, les visages tournés vers lui furent encore plus nombreux et un vieil homme d’un groupe de pèlerins qu’il connaissait de vue pour les avoir souvent croisés sur des lieux saints, s’approcha de lui en jouant des coudes. Il lui chuchota, l’œil rieur et le sourire aux lèvres :

« Malaisé de s’y retrouver lorsque tu n’as quelque prêtre à l’entour pour te guider, garçon. »

Ernaut acquiesça en silence, hochant du menton. L’ancien lui fit signe de se pencher, n’ayant aucune chance d’atteindre son oreille quand bien même il se serait hissé sur la pointe des pieds. Il lui glissa:

« Moi, je me contente de remuer babines, mais je récite en ma tête le *Credo*, le *Pater* ou l’*Ave*, selon. Ainsi je ne fais pas affront au Seigneur. »

Satisfait de lui, il hocha de la tête à plusieurs reprises, démontrant par l’exemple son astuce, riant pour lui-même de son innocent stratagème. Ernaut fit une moue et se dit qu’il tenterait peut-être cela une prochaine fois. Cela avait au moins le mérite d’être plus respectueux que sa technique personnelle. Son frère l’avait suffisamment sermonné sur l’importance qu’il y avait à participer de tout son cœur aux nombreux offices en ce lieu saint, en cette période sacrée. Il en allait de l’observance de son vœu et, partant de là, de sa rédemption. Malgré toute la forfanterie dont il était capable, le jeune homme n’était pas assez sot pour gâcher les chances qu’il avait d’être purifié de ses fautes. Ne serait-ce que dans le cas où il aurait de nouveau besoin d’un tel recours à l’avenir.

Descendant vers le sud, l’énorme procession suivait la Croix et le clergé aux toilettes colorées, soulevant un nuage de poussière qui faisait tousser la fin de la colonne. Il fallut du temps pour que tous soient réunis à l’entrée de la ville. Bénéfice de sa grande taille, Ernaut ne perdait pas une miette de ce qui se passait. Devant les deux hautes arcades moulurées se tenaient tous les célébrants, en face desquels étaient assemblés de nombreux enfants. Ils chantaient de nouvelles antiennes, reprises par la foule ici et là, mais le jeune homme n’était plus le seul à se tromper et les voix se faisaient moins vaillantes alors que la liturgie se complexifiait.

Ernaut sourit et se demanda ce que signifiait tout cela. Il savait que cette cérémonie faisait écho à l’entrée du Christ dans la ville sainte, le prêtre d’un groupe qu’il côtoyait de temps à autre le lui avait expliqué quelques jours plus tôt. C’était à cette occasion que le Seigneur s’était fait capturer et crucifier avant de ressusciter. Tandis qu’il repensait à cette histoire, Ernaut admirait la pierre jaune inondée de soleil, que le ciel bleu parsemé de nuages cotonneux mettait en valeur. Sur les remparts, un garde était penché vers la relique et semblait participer aux festivités.

Au-dessus d’eux, ondoyant dans le vent léger, en haut d’un mât surplombant la porte, la bannière du roi de Jérusalem flottait paresseusement : une grande croix d’or entourée de quatre plus petites, sur un fond blanc. Ernaut avait pu l’admirer un bon nombre de fois et la trouvait désormais familière. Il était bien loin, le modeste bourg de Vézelay où il était né, entre les vignes et les champs de blé, sur les bords de la Cure. Il tourna la tête, contemplant la combe qui descendait vers le sud, où d’imposantes et mystérieuses tombes avaient été édifiées pour d’anciens rois. Là où il se trouvait avaient été enterrés les braves qui avaient pris la ville, deux générations plus tôt. Ils avaient l’honneur d’être dans la vallée la plus vénérée de la chrétienté, au plus près de quartier du Temple. Pour l’heure, néanmoins, la foule piétinait les dalles de pierre qui recouvraient les héros, leur assénant une nouvelle leçon d’humilité.

Tandis qu’il laissait son esprit vagabonder, il sentit que le cortège reprenait son avance. Il passa sous les arcades, impressionné par la beauté des chapiteaux sculptés rehaussés de couleurs vives. Au moment où il se trouva dans le couloir, bousculé et bousculant, il s’efforça de toucher des doigts l’anneau doré qui s’était ouvert de lui-même à l’approche du Sauveur. Du moins c’est ce qu’il lui semblait avoir compris lorsque le prêtre lui avait fait le récit de l’entrée du Christ. Il n’était d’ailleurs pas le seul, de nombreux pèlerins se signant ensuite de la même main, le visage recueilli. Une volée d’une quinzaine de marches menait à l’esplanade où avait été érigé le Temple du Seigneur.

L’air était vif, le soleil clément, et la foule en liesse déclamait les chants d’allégresse d’un cœur vibrant. Beaucoup se contentaient de lever les bras et leurs rameaux vers les cieux en psalmodiant, d’autres applaudissaient, les larmes aux yeux, tandis que certains embrassaient leurs proches, heureux de partager leur bonheur d’être là en pareil instant. Ils faisaient le tour du parvis en direction du temple de Salomon, l’ancien palais royal, désormais propriété des chevaliers du Temple, les hommes au blanc manteau frappé de la croix rouge. L’immense bâtiment faisait peine à voir, encore partiellement ruiné, encombré d’échafaudages, de piles de moellons et de tas de chaux qui en indiquaient la réfection.

La foule s’assembla au sud, sur les marches, parmi les caroubiers, les aliboufiers et les palmiers. La ferveur remplissait les cœurs d’allégresse et éclairait les visages. Les pèlerins tenaient en leur main le symbole de leur voyage. Ils pourraient retourner heureux dans leurs demeures, par-delà la mer, et conserver jusqu’à leur mort ce témoin de leur dévotion. Ernaut avait pris grand soin de se munir de nombreuses branches, pour son frère Lambert bien sûr, mais pas seulement. Il en distribuait maintenant aux fidèles alentour, les serrant dans ses bras puissants quand ils manifestaient leur reconnaissance.

Beaucoup ne suivaient plus guère le déroulement de la cérémonie, perdus dans leurs prières personnelles ou occupés à se congratuler avec leurs proches. Lorsque le chantre termina l’antienne « *Circumdederunt me gemitus mortis*[^circumdederunt] », le silence se fit peu à peu. Les clercs qui entouraient le patriarche, vêtus de leurs tenues rouges, se réunirent en petits groupes à destination de leurs congrégations respectives pour y assister à la messe dominicale. Lentement le murmure devint brouhaha tandis que les fidèles osaient parler plus fort, se rassemblaient avant de s’égayer dans la cité. Quelques-uns restaient au Temple du Seigneur, mais le plus grand nombre se dirigeait par la Belle Porte vers le cœur de la ville pour rejoindre, par la rue de David et le quartier de l’Hôpital, le Saint-Sépulcre. Ernaut se trouvait parmi ceux-ci, cherchant du regard s’il ne repérait pas quelques connaissances avec qui assister à la messe. L’édifice risquait d’être bondé, certains impotents et malades ayant l’habitude d’y demeurer la plupart du temps. Nul doute qu’ils auraient pris soin de s’installer au plus près de l’autel et du chœur où se tiendraient les chanoines entourant le patriarche.

À un jet de flèche devant lui, Ernaut aperçut le crâne de l’Englois. C’était un ancien contact de son père qu’il avait connu lors de leurs fréquents voyages en Normandie, où ils portaient leur vin, à destination de l’Angleterre. Il était là comme meneur d’un groupe de fidèles normands. Ils n’avaient pas de prêtre avec eux, mais l’un des pèlerins était clerc et suffisamment versé dans les Écritures et la liturgie pour assister et encadrer sa petite troupe.

Bousculant des coudes et des bras, le jeune colosse tenta de se faufiler pour avancer plus vite, aussi efficace et délicat qu’un sanglier à travers blé. Il interpela le vieil homme d’un salut amical. La mine avenante, bien que fatiguée, se tourna vers lui. Jeffroy l’Englois était un poissonnier qui aimait la bonne chère. Son visage portait les stigmates d’une vie à se faire plaisir à manger, mais il était désormais amaigri, ses joues retombant, flasques, sur une mâchoire carrée, surplombant un cou où son goitre n’était plus que rides. Il tentait de cacher sa calvitie, sans grand succès, en remontant ses mèches grises sur le sommet de son crâne.

Son regard malicieux étincela lorsqu’il reconnut la voix qui l’interpellait.

« Or donc, mon p’tit bourguignon ! Comment qu’y va ?

— Fort bien, vous rendez-vous au Sépulcre ?

— N’est-ce pas seul endroit où aller en pareil jour de Pâques Fleuries ?

— Certes. Mais vous auriez pu avoir désir de célébrer messe en autre lieu.

— J’ai parcouru trop de chemins et de mers pour ne pas y prier tout mon soûl avant de m’en départir. »

Fin psychologue, le vieil homme ajouta :

« Goûterais-tu de voir le saint mystère en mienne compagnie ? »

Ayant aperçu le jeune géant qui devisait avec leur compagnon, plusieurs des pèlerins de son groupe saluèrent Ernaut, de la main ou juste d’un sourire. Quelques têtes peu familières s’étaient jointes à eux, mais il en connaissait la plupart. Ils s’étaient croisés la première fois à Nazareth, en début de l’année, puis ils s’étaient retrouvés ici et là, au gré de leurs pérégrinations respectives. Il leur était de temps à autre arrivé de voyager quelques jours ensemble, de partager un hébergement, de rompre le pain de concert. C’était l’usage en Terre sainte. Bien que le flot de pèlerins soit continu, en grand nombre, on finissait toujours par reconnaître des visages, la plupart débarquant et repartant aux mêmes saisons, quand la traversée était possible et de façon à être présents pour Pâques. Parfois, les chevaliers du Temple du Christ rassemblaient en un large groupe beaucoup de marcheurs, les aidant à franchir sous escorte une zone dangereuse. Plus rarement, les frères de Saint-Jean de l’Hôpital faisaient de même. La plupart du temps, chacun était libre de parcourir les lieux où s’étaient déroulés la plupart des vies, des histoires, des miracles, dont ils entendaient parler à la messe ou admiraient le récit sur les décors des plus belles églises.

Régulièrement, on croisait des marcheurs de Dieu venus d’autres contrées, des Allemands, des Provençaux, des Anglais… et même des pèlerins encore plus exotiques, noirs de peau et pourtant bons chrétiens. Alors quand on avait le plaisir de pouvoir échanger avec des personnes de langue proche, on ne se privait pas d’évoquer ensemble le pays, avec nostalgie, tout en faisant défiler les lieues dans ces régions étrangères et en se prosternant dans les édifices saints.

### Matin du lundi 25 mars 1157

Aux abords de la petite église Saint-Martin, Ernaut croisa quelques enfants qui jouaient au milieu de chiens, se passant une balle de bois en riant. Ils soulevaient une grande quantité de poussière en courant, au grand dam des femmes qui allaient et venaient, occupées à gérer l’approvisionnement de leur foyer. Cette vision mit le cœur d’Ernaut en joie et il fut tenté un instant de participer avec eux, mais il savait qu’il n’était plus temps pour lui de se livrer à de semblables amusements. Il souffla, comme pour chasser cette idée de sa tête, puis obliqua en direction de la rue de l’Arche de Judas, remontant vers le nord de la cité de Jérusalem. Il louait avec son frère une pièce dans un immeuble de la partie sud de la ville.

Ce qui lui avait immédiatement plu, c’était la possibilité de se rendre rapidement aux bains, distants d’à peine plus d’une portée d’arbalète. En outre, ils étaient peu éloignés de la porte de Sion, qui permettait de partir vers les régions méridionales du royaume. Surtout, elle ouvrait sur une zone où il faisait bon se reposer, admirer les environs depuis les hauteurs qui accueillaient Sainte-Marie, le mont Sion. Bien que nombreux endroits de la cité fussent plus ou moins à l’abandon, envahis de verdure, voire convertis en jardins, il était toujours agréable de s’en échapper pour prendre l’air depuis la butte au-dessus de la piscine de Siloé. À la fin de l’hiver, beaucoup d’habitants y profitaient du soleil dans l’après-midi, s’appuyant parfois à la muraille, le dos chauffé par la pierre. Des hommes de guerre venaient de temps à autre s’entraîner à l’épée, l’arc ou l’arbalète, le visage sérieux et affairé. Il arrivait même que certains prennent leur activité tellement à cœur que cela dégénérait en pugilat incontrôlé, provoquant l’hilarité ou la crainte dans les environs, selon l’humeur des spectateurs.

Ernaut avait été tenté à plusieurs reprises de rejoindre les soldats, dans l’espoir de faire fructifier les quelques conseils qu’Enrico Maza lui avait donnés[^voirt1]. Mais il s’était abstenu, tout d’abord en raison de la présence de Lambert, qui lui rappelait régulièrement qu’il était là pour accomplir un vœu et pas pour s’amuser. Et depuis que son frère était tombé malade, Ernaut n’arrivait pas à se résoudre à trahir la confiance, ou l’espoir, que son aîné plaçait en lui. Tandis qu’il déambulait vers le nord de la ville, en direction du quartier de l’Hôpital, Ernaut se laissa bercer par l’effervescence des rues. Jérusalem n’était jamais plus peuplée qu’à l’approche de la Semaine sainte. De nombreuses nationalités se retrouvaient là pour célébrer ensemble les mystères divins, dans le lieu le plus sacré de leur foi. La cohue était telle qu’il fallait généralement avancer en file, devant les étals proposant de la nourriture, des vêtements, de la céramique, du mobilier, des pièces d’orfèvrerie, des boissons… Et il était fréquent qu’on soit suivi ou précédé d’une mule ou d’un âne, d’un chariot à bras ou de portefaix, dont les lourds paquets rendaient l’approche de certaines échoppes assez périlleuse.

Au cœur de la ville, à la croisée des rues principales se trouvait le Change latin. Plusieurs hommes s’affichaient là l’air crâne, l’épée au côté et le casque à nasal négligemment posé sur la tête, un haubergeon sur les épaules. Ils ricanaient entre eux en dévisageant les femmes qui passaient alentour mais toisaient également quiconque aurait pu causer un problème. C’était le lieu où les voyageurs pouvaient se procurer les monnaies locales contre leurs pièces étrangères. On y trouvait surtout des pèlerins venus d’Europe, les marchands ayant à faire plus particulièrement avec le change syrien, plus loin au nord, au-delà du Marché couvert, où l’on pouvait dénicher toutes les espèces du Levant, y compris sarrasines.

Ernaut obliqua sur sa gauche et rejoignit la rue de David. Il comptait emprunter une venelle qui se faufilait entre les boutiques alignées du côté droit de la voie et qui permettait de retrouver la place au cœur du quartier de l’hôpital de Saint-Jean. C’était un endroit toujours empli de vie car il n’accueillait pas que les malades et les infirmes, mais aussi beaucoup de voyageurs désargentés. Certains se contentaient de venir quérir leurs repas, préférant s’abriter comme ils le pouvaient ici et là, passant parfois leurs journées dans les lieux saints à prier pour leur âme et celles de leurs proches. Chaque jour, un grand nombre recevait sa pitance en de longues files patientes.

Les besoins en approvisionnement étaient immenses et quotidiennement, de fréquents convois d’animaux de bât, de porteurs, vidaient leur chargement de grain, de légumes, de viande, de vin, dans les celliers des frères de l’hôpital. Ernaut et Lambert avaient choisi de louer leur propre logement, étant suffisamment aisés pour cela. En outre, cela leur donnait l’occasion de mieux découvrir la vie dans la cité outremer, et de se préparer à leur installation future dans le royaume. Lorsqu’il sortit du passage voûté et retrouva le soleil, la douce chaleur se répandit sur la nuque puissante d’Ernaut. Le temps était encore frais, les premiers rayons matinaux faisaient comme une agréable caresse sur sa peau.

Il fut un instant tenté de humer l’air à plein poumon, mais il écarta aussitôt l’idée, inquiet de s’emplir le nez de la puanteur ambiante. Des cuisines approvisionnant l’hôpital s’exhalaient un ensemble de saveurs qui auraient pu flatter ses narines si elles ne mêlaient pas aux relents des caniveaux qui couraient sous ses pieds, à la sueur et la poussière des bêtes qu’on y amenait, ployées sous des chargements aussi divers que des sacs de nourriture, du foin, des outres ou du bois. Quelques tas d’immondices attendant d’être évacués attiraient tous les chiens galeux du quartier, bataillant ferme contre les hordes de chats et de rats qui leur disputaient les détritus. Toute l’agitation empuantie qui régnait là finit par le submerger et il porta machinalement la main à son visage, se frottant le nez comme pour en détacher l’odeur âcre. Fouettant autour de lui avec sa branche de palmier pour en chasser les mouches, il pressa le pas, impatient de rejoindre le grand hôpital de Saint-Jean, laissant néanmoins son regard dériver sur les magnifiques bâtiments de couleur miel qui l’entouraient.

Les cris d’étourneaux affairés résonnaient entre les hauts murs de la vaste place où il se trouvait maintenant. Il était impressionné par l’imbrication des constructions, dont la majeure partie était neuve. Au-dessus des magasins bordant la venelle couverte qu’il avait empruntée s’étendait la zone réservée aux femmes, pour leurs soins ou d’éventuels accouchements. On y gardait aussi les enfants n’ayant plus personne pour s’occuper d’eux, le temps d’en faire des adultes autonomes. Face à lui, vers l’ouest, était implanté un vaste ensemble qui hébergeait les frères de l’Hôpital, leurs officiers et, depuis quelques années, des chevaliers de leur ordre. Séparé de cet imposant complexe qui abritait également l’église conventuelle de Saint-Jean-Baptiste par un passage couvert débouchant dans la rue du Patriarche, se trouvait à sa droite l’impressionnant hospice, établi sur plusieurs niveaux.

Les sous-sols constituaient une partie des celliers où la fortune de l’ordre était entreposée. Ce n’était pas de l’or ni des joyaux, mais des vivres pour les pauvres et les frères, du tissu pour les couchages et la vêture, de l’huile pour éclairer les édifices, toutes les denrées qui leur permettait d’assurer leur mission de charité. Le rez-de-chaussée était occupé par l’immense salle de soin pour les hommes. On disait qu’il s’y dénombrait plus de mille lits, minutieusement organisés, sous la responsabilité d’une armée de valets, serviteurs et de quelques médecins.

L’étage au-dessus accueillait les pèlerins qui n’avaient nulle part où s’abriter, le temps pour eux de s’acquitter de leurs vœux dans la Cité sainte. Ils se trouvaient au plus près du Saint-Sépulcre, qu’ils pouvaient rejoindre par une large porte, au nord. Enfin, surplombant le tout, un imposant clocher qui défiait ostensiblement les chanoines voisins, affirmant haut et clair la puissance des moines de Saint-Jean de l’Hôpital. S’approchant de l’entrée arrière du bâtiment où l’on soignait son frère, il souriait à la cantonade. Il avait l’impression d’être de retour en France car se côtoyaient sur ce vaste perron plus d’Occidentaux que partout ailleurs dans la ville : Français, Provençaux, Aquitains, Bretons, Normands, Flamands, Anglais, Germains, Savoyards… Toutes les nations semblaient s’être retrouvées ici et noyaient dans leur masse les quelques autochtones, dont la plupart s’activaient, une charge dans les bras.

Rien d’anormal à ce que les foules de tous les pays se rassemblassent en un tel lieu. Le centre du monde n’était-il pas à quelques pas, marqué solennellement d’une pierre dans le Saint des Saints, l’église du Saint-Sépulcre ? Il eut tôt fait de franchir le porche d’accueil ouvrant sur la partie dédiée aux malades. Sous le portique d’entrée, il salua d’un signe de tête enjoué le valet affairé à balayer l’accès du bâtiment où les moines, leurs médecins et leurs servants prodiguaient leurs soins aux fiévreux, patients et blessés.

L’air y était frais et le bourdonnement d’une intense activité, amplifiée par l’écho sous les hautes voûtes, renforçait l’idée que les visiteurs n’y étaient pas les bienvenus pour badiner. Malgré son assurance habituelle, bénéfice d’un physique hors du commun, Ernaut se sentait comme un enfant dans la vaste zone emplie de lits et de paravents, de coffres et de tables, où docteurs, chirurgiens, chefs de salles et domestiques soignaient plusieurs centaines de nécessiteux chaque jour. Une ruche ronflante où les abeilles seraient intégralement habillées de noir, marquées d’une croix blanche sur la poitrine. Et pour le coup, il s’estimait aussi utile à l’endroit qu’un gros bourdon esseulé devant des butineuses agitées.

Il venait, comme chaque jour, s’enquérir de son frère. Lambert avait attrapé la vérole peu de temps après leur arrivée dans la ville sainte et il avait été confié dès lors aux bons soins des hospitaliers. Traité comme un prince et soigné par les meilleurs praticiens, il se remettait, quoiqu’avec lenteur, et n’avait pas eu trop à souffrir. Bien que son visage garderait des traces indélébiles de l’affliction, ses yeux n’avaient pas été touchés et les docteurs l’estimaient désormais hors d’affaire, encore que trop faible pour sortir avant quelques jours. S’avançant dans les travées, Ernaut tentait de gêner le moins possible les allées et venues de l’intense domesticité qui vaquait à ses tâches.

Il dépassa deux employés qui enveloppaient un corps dans un linceul sur un brancard, entourés de quelques parents éplorés. Plus loin, un médecin examinait une fiole d’urine comme s’il s’agissait d’une pierre précieuse, sous le regard admiratif des serviteurs de salle attentifs. Un hurlement strident lui fit tourner la tête, et il vit des valets accourir au milieu d’un attroupement autour du lit d’où était venu le cri. Un visage en sueur se releva, puis un bras ensanglanté ; le chirurgien s’essuya le front en jetant un œil désolé vers son patient puis se pencha de nouveau sur sa tâche, après avoir empoigné la scie que lui tendait son assistant.

Frissonnant à l’idée de devoir un jour passer entre les mains d’un tel praticien, Ernaut manqua de renverser un vieillard qui avançait à grand-peine. Glissant dans des savates de fine étoffe en oscillant de droite et de gauche, il fit un bond en arrière perdant en un instant le bénéfice d’un long moment de marche. Un sourire embarrassé sur le visage, le jeune homme se dépêcha de rejoindre une autre rangée, histoire de se faire oublier. Lorsqu’il arriva enfin à la zone où était alité son frère, il vit ce dernier relevé sur sa couche, bien emmitouflé dans ses couvertures, en train d’avaler un épais gruau. Il se laissa tomber plus qu’il ne s’assit sur le coffre au pied du lit, levant la main pour tout salut.

« Alors, comment se porte le malade ce jour d’hui ? »

Le regard toujours un peu enfiévré, Lambert sourit en avalant sa cuillérée puis reposa son écuelle devant lui.

« Plutôt bien, je dirais. La nuit a été fort excellente, si l’on excepte l’agitation toujours présente ici.

— Ils ont fait procession spéciale hier soir ? »

Le malade oscilla de la tête avec lenteur, se dépêchant d’avaler une nouvelle cuillerée avant de répondre.

« Même pas ! Je l’espérais, mais ils se sont contentés de l’office habituel, juste en fin de complies. Ils ne m’ont d’ailleurs pas laissé assister à la messe. »

Ernaut fit une moue, se mordant la lèvre supérieure tout en regardant le visage désormais grêlé de petites cicatrices de son frère. Lambert reposa le récipient vide sur un tabouret à côté de son lit tout en balayant de la main devant lui.

« Mais foin de tout cela ! Conte-moi plutôt ce que tu as fait toi ! J’aurais tant aimé venir à tes côtés pour la procession. »

Ernaut esquissa un demi-sourire, inclinant la tête.

« Bein, il y avait grand monde, de tous les pays. J’ai entendu des langues que je ne connaissais pas, c’est encore plus incroyable qu’à Provins ou lors des foires de Saint-Denis ! Je m’étais levé aux aurores pour être tranquille. On est revenus en grande pompe avec les évêques, les abbés, tout ça. Puis nous sommes rendus au Saint-Sépulcre pour messe. J’étais avec Jeoffroy l’Englois et son groupe. Ils te passent le bon jour d’ailleurs…

— Voir tout cela de mes propres yeux ! Peut-être fois prochaine… Il semblait y avoir si admirable foule !

— Un peu, oui. Tu te serais fait écraser ! J’ai dû user de ma palme pour fouetter un peu à l’entour, histoire de pouvoir respirer ! »

Voyant que son frère se moquait de lui, Lambert ne releva pas.

« Puis il y a eu fort longue messe, *Ave rex noster*, tous ces trucs et puis voilà. Ne t’inquiète pas, j’avais acheté belle palme pour toi aussi. »

Il agita aussitôt la branche qu’il tenait à la main d’un air facétieux. En réaction, Lambert fit une moue contrariée.

« Ne te fais pas plus mécréant que tu n’es. C’est gentil d’avoir fait bénir rameau pour moi, mais il me faudra revenir participer en mon temps.

— Comme tu veux, mon vieux. Pour moi, voilà désormais chose faite. Dans quelques jours mon vœu sera accompli !

— Il ne s’agit pas seulement d’être présent, Ernaut, je te l’ai dit mille fois ! »

Lambert essuya son front, où la sueur collait ses cheveux bruns. Il soupira longuement, l’air contrarié. Ernaut tourna la tête, marquant une pause, faisant mine de s’intéresser à l’activité environnante le temps que la tension entre eux retombe. Lorsqu’il posa de nouveau les yeux sur son frère, il empoigna sa besace et y plongea la main, tout sourire.

« Je t’ai amené aussi force friandises, sucreries au miel, aux amandes et aux pistaches. C’est absolument délicieux, il faut que tu en aies goûtance. J’ai trouvé ça en une petite échoppe au près de la maison. »

Vérifiant qu’il n’y avait aucun responsable aux alentours, il fit glisser un petit baluchon bien garni vers son frère. Celui-ci s’empara du présent sans mot dire et le dissimula sous un pan de couverture.

« C’est gentil, mais tu sais ici la mangeaille est excellente. Ils servent un pain vraiment… su-ccu-lent ! »

Il accompagna sa dernière remarque de gestes démonstratifs de la main et d’un claquement des lèvres. Tandis qu’ils discutaient, un groupe affairé se rapprochait d’eux peu à peu. C’était la visite matinale du médecin, qui vérifiait que ses prescriptions étaient suivies tout en contrôlant l’état des patients sous sa garde. Il était entouré d’une nuée de valets et de domestiques qui se chargeaient de donner les remèdes ou de manipuler les malades. Le frère de l’hôpital de Saint-Jean responsable de la zone en profitait pour faire une inspection. Et malheur aux assistants qui n’avaient pas fait leur travail correctement !

D’ailleurs, Ernaut aussi s’était déjà fait rabrouer plusieurs fois par frère Garin. Le visage autoritaire couronné d’un petit chapeau de feutre blanc, il lançait ses ordres d’une voix sèche, la fente qui lui servait de bouche s’ouvrant à peine. Et il semblait à l’adolescent que ses deux bras ne faisaient qu’un, étant donné qu’il ne paraissait jamais tenir ses mains que serrées l’une dans l’autre contre sa poitrine. Ses yeux globuleux lui donnaient un aspect un peu effrayant, d’autant qu’il avait l’air de tout voir, comme un vautour sur sa branche.

À maintes reprises, il avait repris Ernaut lorsque celui-ci apportait des friandises, interdites, à son frère. Malgré tout, Lambert disait qu’il était un brave homme, toujours prêt à améliorer le confort de ceux dont il avait la charge. Bien que strict dans ses ordres, il savait être obéi sans abuser de son autorité. Tandis qu’il se rapprochait, jetant un regard en biais sur chaque couche, il ne pouvait manquer Ernaut, qui tentait pourtant de se faire oublier en se dissimulant derrière le petit groupe d’employés. Il vint droit sur lui, les pans de son habit à peine dérangés par ses modestes enjambées.

« Eh bien ! Eh bien ! Notre jeune destorbeur est de retour ? »

Le ton amical de sa voix n’était jamais un indicateur de ce qu’il avait en tête, les reproches les plus cinglants et les encouragements et félicitations étant débités d’une façon relativement monocorde. Mal à l’aise, se tortillant un peu, Ernaut lui répondit le sourire aux lèvres :

« Le bon jour, frère Garin. Je suis venu m’enquérir de la santé de Lambert.

— C’est tout à ton honneur, garçon. J’espère que tu n’as pas de nouvel tenté de lui glisser interdites denrées. »

Ernaut lança un regard en coin vers son frère, comme appelant à l’aide, et bredouilla quelque « euh » embarrassés avant de finalement arriver à composer une réponse.

« J’ai bien appris la leçon, soyez-en assuré. Je ne tiens guère à devoir exécuter encore force corvées à votre service. »

Le chef de salle secoua la tête en dénégation et pointa l’index vers le ciel.

« Ce n’est pas pour moi que tu les as fournies. Nous sommes tous ici valets de ton frère et de ses pareils. Les servir, c’est servir Dieu. »

Le rictus de Frère Garin sembla se muer en discret sourire tandis qu’il parlait. Les yeux légèrement plissés, il arborait presque un visage espiègle.

« Quoi qu’il en soit, tu ne peux pas demeurer. Nous avons grand labeur et, même si tu n’étais si imposant, il n’y aurait pas de place pour toi ici. Cette halle de l’hôpital est royaume de malades et de soigneurs. Les bien portants, ouste ! Embrasse ton frère et fais-moi plaisir de débarrasser le plancher. »

Obéissant à cette amicale injonction et trop heureux de s’en tirer à si bon compte, Ernaut se leva d’un bond. Alors qu’il se penchait vers son frère, l’hospitalier ajouta :

« Et profites-en pour récupérer ces petits gâteaux que tu as dû faire choir par mégarde de ton sac. »

### Milieu de matinée du lundi 25 mars 1157

Un peu désappointé, Ernaut se dirigea vers le passage qui donnait sur la rue du Patriarche, à l’ouest. S’arrêtant le temps d’engouffrer les quelques desserts qu’il avait dû récupérer, il réfléchit à ce qu’il allait faire de sa journée : pas de cérémonie particulière aujourd’hui et il avait déjà prié dans la plupart des lieux saints des environs. Il était donc indécis sur la manière dont il pourrait s’occuper, ayant espéré pouvoir rester un peu avec son frère.

Il se sentait perdu dans cette ville animée où il ne connaissait vraiment personne. Il avait bien revu quelques-uns des voyageurs du *Falconus*[^voirt1], mais la plupart ne reviendraient que pour la fin de la Semaine sainte et les célébrations de Pâques. Se pourléchant afin de bien avaler la moindre miette, les pouces coincés dans la ceinture, il commença à avancer sans trop réfléchir où il se rendait, se laissant porter par le mouvement de la foule, toujours dense aux abords du Saint-Sépulcre.

Avec sa haute taille et son gabarit, il ne manquait pas d’attirer l’attention sur lui, des jeunes filles parfois, mais le plus souvent de marchands inquiets et de sergents du guet circonspects. Il voulait remonter tranquillement vers la porte Saint-Étienne, en rejoignant la rue du même nom qu’il retrouverait très vite. Il décida donc de prendre à main droite le chemin vers l’est, descendant les escaliers qui menaient au parvis du Saint-Sépulcre auquel on accédait en franchissant une colonnade au nord, puis il continua vers les marchés aux abords du Change syrien. Sans conviction, il se disait qu’il se baladerait peut-être hors la ville pour aller contempler le paysage depuis le Mont des Oliviers.

Il sourit en passant devant l’entrée principale de l’Hôpital, réalisant qu’il aurait pu sortir directement par là. Tout en marchant, il contemplait les boutiques présentant des produits qui lui paraissaient tellement exotiques jusqu’alors. Les rues étroites étaient littéralement envahies par les denrées qui débordaient des petites échoppes. La plupart ne faisaient que quelques pieds de profondeur et le vendeur devait se balancer sur une corde fixée au plafond pour s’extraire de derrière ses marchandises. On pouvait y admirer des médailles pieuses, de modestes souvenirs de Terre sainte, des palmes pour ceux qui n’osaient en prendre sur un arbre.

Tous les commerces n’étaient pas néanmoins dédiés à la religion et les accessoires pour les élégantes disputaient la place à l’encens et aux parfums. Des marchands d’habits d’occasion étaient nombreux également, achetant, ravaudant et revendant des tenues d’étoffes colorées. Les vêtements étaient si étranges : chapeaux faits d’un interminable foulard enroulé, habits à longues et larges manches, en coton bariolé, et ceux en soie ! Il n’aurait jamais pu rêver s’en procurer quand il vivait à Vézelay avec ses parents. On en trouvait ici, déjà portés par d’autres auparavant, à des prix bien plus raisonnables. De qualité inférieure certes, mais cela demeurait de la soie.

Il s’arrêta devant un étal où était disposé justement tout un assortiment de toilettes de seconde main. Le vendeur, un Syrien au turban imposant, lui sourit d’un air engageant, dévoilant de trop rares dents. Impatient de satisfaire son client, il montrait du doigt une pièce ou l’autre avec entrain, présentant chaque robe, sûrement de façon élogieuse, dans une langue qu’Ernaut ne comprenait pas. Ernaut se gratta le menton, et indiqua au marchand ce qu’il cherchait, pointant son index sur le couvre-chef de l’homme. Ce dernier le regardait d’un air indécis, étalant avec enthousiasme tout ce qui lui tombait sous la main dans sa petite boutique, lançant d’une voix rauque :

« Très bon qualité ! Très bon prix ! », sans vraiment comprendre ce qu’on attendait de lui.

« Non non, compère, je veux chapel comme toi, ce truc, là sur ta tête. »

Le vendeur de la boutique à côté s’esclaffa et pointa la tête hors de son arcature, se penchant par-dessus ses articles.

« Lui pas comprendre, je peux dire si tu veux. »

Heureux de trouver un peu d’aide, le jeune homme put enfin se faire comprendre de son interlocuteur. Après avoir porté son choix sur une belle pièce aux extrémités décorées, il passa quelques monnaies d’argent et repartit avec le turban bien fixé sur la tête, grâce aux vendeurs attentionnés dont il avait égayé quelques instants. S’estimant désormais irrésistible, et bien plus intégré à son nouveau pays, il jetait dorénavant un œil qu’il s’efforçait d’arborer plus connaisseur sur ce qui se présentait à lui, une légère moue indiquant bien aux marchands qu’il était familier de tout cela.

Il traversa ainsi le marché des chandelles, où l’on pouvait acquérir les cierges à brûler devant les autels et les statues. L’endroit sentait particulièrement bon, avec les bains de cire qui servaient à quelques-uns pour tremper leurs mèches de coton. Arrivé à la croisée de la rue aux Herbes, il sentit les odeurs qui montaient depuis Malquisinat, où se trouvaient de nombreuses boutiques vendant de la nourriture, depuis de simples potages jusqu’à des mets délicats et les pâtisseries les plus savantes. Sur sa gauche il entendait les clameurs du marché aux volailles, le caquètement des poules le disputant au cacardement des oies, vaincus parfois par quelques cris de coqs. Bien que la viande fût interdite en ces temps de Carême, au même titre que les œufs, certains étaient dispensés, et quelques habitants ne respectaient pas le calendrier catholique, appartenant à une église orientale.

Ne sachant pas résister à l’appel du ventre et l’appétit aiguisé par un début de repas prometteur, il vira sur sa droite, accomplissant un nouveau quart de tour autour du quartier de l’hôpital. Étant donné l’heure, il y avait beaucoup de personnes qui venaient s’acheter de quoi se restaurer pour le déjeuner. C’était un vrai festival pour les narines, les senteurs épicées se mêlant aux arômes de fruits et aux effluves caramélisés. Partout dans la rue on entendait rissoler, frire, gratter, égoutter, remuer, battre. Ernaut devait véritablement forcer le passage s’il voulait obliger les clients à se détacher des étals ne serait-ce que le temps pour lui de se faufiler. Il finit par se décider pour des boulettes de poisson recouvertes de graines de sésame, enfichées sur une baguette de bois et agrémentées d’un bon morceau de pain plat.

Son festin à la main, il chercha à sortir de la marée humaine pour avaler tranquillement ce qui s’annonçait comme un excellent repas. Il savait que plus bas dans la rue il existait un endroit où il y avait comme une petite cour, au débouché d’une venelle où des ordures étaient généralement entassées. De l’autre côté, faisant face à quelques boutiques de changeurs on trouvait des bancs de pierre dont il espérait, peut-être naïvement, qu’il y dénicherait une place libre.

Mais la foule qu’il y découvrit en se rapprochant le fit soupirer de mécontentement, arrachant d’un coup de dent une boulette de sa pique pour nourrir son désagrément. Il comprit néanmoins bien vite qu’il y avait quelque chose d’anormal, les badauds ne progressaient pas et regardaient tous en direction de l’esplanade au bout de la ruelle, commentant quelque chose entre eux. Il se faufila, de façon souple selon lui, en bousculant tout le monde de l’avis de ceux qu’il écrasa, et parvint à s’avancer suffisamment pour avoir un aperçu de ce qui se passait.

Des sergents du guet étaient occupés à écarter le tas d’immondices sous les ordres d’un homme à la belle tenue. Vêtu d’un bliaud de laine orange brodée et d’un mantel bleu à décor, la quarantaine, les cheveux châtains coupés à l’écuelle et la barbe bien taillée, il arborait une splendide épée au côté. On murmurait autour d’Ernaut qu’il s’agissait du mathessep Ucs de Montelh, le chef des valets d’armes qui servaient la Cour des Bourgeois. Il tenait par les épaules une fillette brune d’à peine dix ans, aux vêtements rapiécés et les yeux emplis de larmes, et se penchait de temps à autre vers elle pour la réconforter.

Ernaut tendit le cou pour mieux voir ce qui se passait, provoquant une nouvelle vague de bousculade ponctuée de cris et de soupirs énervés. Il comprit alors ce que ces hommes dégageaient : le corps d’une femme était maintenant discernable, recouvert jusqu’alors d’ordures. Lorsque les sergents entreprirent de mettre au jour le visage, ils eurent un mouvement de recul et l’un d’eux se releva, la main sur la bouche, le regard affolé. D’où il était Ernaut n’arrivait pas à voir pour quelle raison. Curieux, il se déhancha tellement qu’il manqua de faire tomber ses voisins, attirant un bref instant l’attention du responsable des gardes. Hypnotisé par ce qu’il observait, ce dernier ouvrait des yeux inquiets, un peu hagards.

Ne voulant pas perdre de sa superbe devant la foule, Ucs toussa et donna quelques instructions pour qu’on couvre le corps, en attendant de l’emmener. Puis il éloigna la gamine et demanda à ceux qui s’étaient amassés de reprendre leur chemin, qu’il n’y avait rien d’intéressant à voir. Munis de leur lance qu’ils tenaient à deux mains, à plat, les sergents entreprirent donc de repousser le public agglutiné, qui ne se dispersait que de mauvaise grâce. Profitant de ce que certains des gens alentour parlaient français ou provençal, Ernaut laissa traîner son oreille, l’air détaché, tout en finissant d’avaler sa brochette qu’il avait négligée quelques instants.

« Ce serait pérégrine du Tournaisis on m’a dit !

— Pauvresse, finir pareillement !

— Sûrement un de ces sarrasins païens. Il y en a moult en les rues, ils tiennent toutes les échoppes !

— Certes non, il n’y a que bons chrétiens ici. Les païens ont été boutés hors le royaume.

— Moi, on m’a toujours dit que d’aucuns avaient fait croire qu’ils étaient convertis, mais ce ne sont que menteurs et larrons. D’ailleurs… »

La femme qui parlait alors se pencha auprès de son groupe pour finir sa phrase à voix basse, jetant un œil inquiet à la ronde. Croisant l’air innocent d’Ernaut au passage, cela ne sembla guère abuser la commère. La bande opéra finalement un repli stratégique à l’écart, en direction du nord. Le jeune colosse haussa les épaules et déchira d’un coup de dent un morceau de pain. Quand il baissa les yeux et se tourna, il s’aperçut qu’il se trouvait désormais au premier rang, le manche d’une lance vaillamment brandie devant lui par un petit homme au regard décidé, pour le faire reculer. Indifférent à la demande muette, il s’efforçait de mieux voir le corps, curieux comme à son habitude. Un détail l’intrigua : il lui sembla reconnaître la besace à demi versée sur le sol, en vieille toile décorée d’une croix de galons croisés.

Le mathessep allait repartir après avoir donné discrètement ses instructions lorsqu’il prit conscience du géant qui, seul, demeurait à examiner la scène.

« Dis-moi, aurais-tu problème d’entendement pour demeurer là malgré mes ordres ?

— Le pardon, maître, il me semble bien que… »

Ernaut n’était pas totalement sûr de ce qu’il allait avancer et, surtout, de ce que serait la réaction de son frère quand il apprendrait qu’il s’était encore mêlé de ce qui ne le regardait pas.

« J’écoute, qu’as-tu à dire ?

— Je connais la besace laissée là. La femme qui la tenait ne l’aurait certes pas jetée alors je crains que le corps ne soit sien.

— Tu pourrais m’indiquer son nom, celui de son groupe ? »

Sans même attendre une autorisation, Ernaut s’avança doucement, adressant un sourire au garde qu’il dépassa sans gêne.

« Certes oui, mais il me faudrait voire outre ».

Ucs fit un signe de tête pour qu’on dévoile le corps à Ernaut. Le crâne était méconnaissable, comme s’il avait été écrasé par un cheval fou. La mâchoire disloquée, le front défoncé, n’offraient plus qu’une caricature de visage. De la coiffe maculée de taches brunes et rouges dépassaient de nombreuses touffes de cheveux autrefois gris, frisés, désormais trempés de sang, collés dans les hématomes coagulés et la matière cérébrale répandue.

Le spectacle arracha une grimace à Ernaut. Il s’inquiéta un court instant du destin prochain des boulettes de poisson qu’il venait d’avaler. Le mathessep ne le quittait pas des yeux, attendant une réponse, qui sortit d’une voix blanche.

« Si fait, il me semble bien la reconnaître. On la nomme Phelipote, je n’en sais guère plus. Je l’ai vue à moult reprises avec d’autres tandis que nous rendions grâce en une chapelle ou l’autre.

— A-t-elle quelque parentèle en la Cité ? Est-elle de bonne famille ? Sa mise est fort modeste, mais peut-être est-elle ainsi vêtue par humilité.

— J’ai souvenance qu’elle avait un sien mari qui la compaignait. Et peut-être d’autres familiers, je ne saurais dire. »

Ucs hocha la tête, pensif.

« Il me faudra évoquer l’affaire en la Cour. Il serait bon que tu puisses rendre raison à d’éventuelles questions.

— Je n’en connais guère plus, maître.

— J’ai bien entendu, la Cour aura peut-être désir de t’ouïr à ce sujet, jeune… Quel est ton nom d’ailleurs ?

— Ernaut de Vézelay.

— Un Bourguignon, s’exclama le mathessep, comme si le fait l’amusait. Es-tu seul en la Cité ?

— Non pas, j’ai fait chemin avec un mien frère, Lambert. Les moines de Saint-Jean le soignent pour mauvaises fièvres. »

Ucs de Montelh renifla et prit quelques instants pour mettre de l’ordre dans ses pensées. Il claqua des doigts en direction d’un de ses hommes, qui s’approcha. Il donna des instructions pour qu’Ernaut soit mené à la session de la Cour des Bourgeois qui devait se tenir en fin de matinée.

D’ici là, il ne devait pas être laissé seul. Le sergent responsable de la tâche, Baset, un brun entre deux âges, le nez camus et la peau mate soupira de dépit. Il n’aimait guère s’acquitter de pareilles corvées. D’autant qu’il ne voyait pas en quoi le grand costaud aurait pu avoir besoin de protection. Et si c’était pour l’obliger à ne pas rater la réunion, la mission était tout aussi vaine, il n’était pas de taille à dompter des ours.

### Fin de matinée du lundi 25 mars 1157

La résidence du roi n’était qu’à une courte distance, lovée contre la rotonde du Saint-Sépulcre, coincée entre la rue du même nom et celle du patriarche. L’ancien palais au sud-est de la ville avait été cédé aux chevaliers du Temple peu à peu, quelques années auparavant, et Baudoin de Jérusalem devait s’accommoder d’un conglomérat de bâtiments disparates en guise de demeure. Les rumeurs allaient bon train affirmant que le jeune Baudoin ne se contenterait certainement pas de l’endroit, maintenant qu’il avait établi son pouvoir face à sa mère Mélisende.

Certaines langues acides ajoutaient que l’assaut sur Panéas[^paneas] lui avait apporté la fortune nécessaire. L’assemblée de la Cour des Bourgeois se déroulait ce jour-là dans une petite salle et Ernaut en y pénétrant fut surpris de sa modeste taille. Sur une estrade était disposé un siège curule à tête de lions, devant un tapis oriental aux motifs géométriques. Face à lui, une demi-douzaine de bancs suivaient les murs tandis que quelques tabourets étaient placés à droite et à gauche. Deux fenêtres décorées de colonnettes laissaient entrer la lumière surplombant un jardin en contrebas, dont on apercevait le feuillage tremblant des arbustes par le verre légèrement coloré.

Baset alla s’installer au fond, adoptant une posture indolente. Il renifla un coup, sur le point de cracher, lorsqu’il réalisa où il se trouvait. Ennuyé, il tourna la tête de droite et de gauche avant d’avaler bruyamment d’un air contrarié le projectile si bien préparé. Un sourire sans joie aux lèvres, Ernaut patienta quelques instants à la porte, mécontent d’être tenu enfermé par une belle journée. Il entendit le brouhaha annonciateur de l’arrivée d’un groupe au moment où il s’apprêtait à aller vérifier si les châssis des fenêtres pouvaient pivoter pour admirer la vue au-dehors.

Une dizaine de personnes entra, sans prêter attention à ceux qui se trouvaient là. Ils étaient tous vêtus d’habits révélateurs de leur statut. Ce n’était que souples lainages, amples coupes et décors brodés, le tout en d’éclatantes couleurs. Il ne leur manquait que les éperons d’or pour en faire des chevaliers. Parmi eux trois hommes s’installèrent sur les tabourets, arrangeant les plis de leurs atours d’un geste machinal. Baset lui souffla à l’oreille leurs noms tandis qu’ils prenaient place.

De carrure modeste, le premier, André de Tosetus, avait la peau mate, de hautes pommettes et les yeux presque bridés, d’un noir profond. Sa barbe finement taillée et ses cheveux courts attestaient du soin qu’il portait à sa personne. Des rides avaient inscrit sur son faciès maigre la bonne humeur qu’il manifestait à l’instant même, échangeant quelques propos amusants avec son voisin, Pierre de Périgord. Celui-ci était bien plus impressionnant, avec une bedaine pointant en avant comme l’étrave d’un navire. On le devinait néanmoins puissant, car il se mouvait avec aisance. Une imposante barbe bien entretenue lui mangeait la moitié du visage, dont on apercevait les yeux clairs, vifs et inquisiteurs. Son crâne était orné d’un bonnet de feutre brodé. Le dernier, Rainald Sicherius, échangeait quelques mots avec un écrivain flanqué de son apprenti portant l’écritoire. Très brun de cheveu, il se déplaçait comme un ours, les bras ballants, le regard baissé et les sourcils froncés. Sa pilosité broussailleuse lui remontait vers les paupières et des poils noirs garnissaient une partie de son cou. Lorsqu’il lui arrivait de sourire, on l’aurait cru prêt à mordre. Malgré cela, ou peut-être pour cette raison, Ernaut le trouva immédiatement sympathique. Derrière lui venait le mathessep Ucs de Montelh, toujours équipé comme il l’était le matin, le front soucieux. Il alla se tenir contre les fenêtres, après avoir salué de la tête les présents.

Puis entra le personnage que tous attendaient, qu’Ernaut n’avait jamais vu, mais qu’il reconnut sans aucun doute. Arnulf, vicomte de Jérusalem, n’eut que quelques enjambées à faire pour se trouver sa place sur l’estrade. Il était vêtu d’un magnifique bliaud de soie, aux motifs végétaux jaunes sur fond vert. Le pendant de sa fine ceinture rehaussée de plaques dorées dansait à chaque pas entre ses genoux, attirant l’œil. De taille moyenne, il avait le torse puissant du chevalier habitué au haubert, aux cavalcades et à la vie rude sous le harnois. Sa mâchoire carrée était glabre, avec des lèvres tellement étroites qu’on l’aurait cru sans bouche. Ses yeux sombres étaient enchâssés derrière d’épais sourcils où le gris commençait à manger le noir, comme dans ses cheveux qu’il portait coupés à l’écuelle, comme nombre de soldats.

Il leva une main ornée de nombreuses bagues pour que le silence se fasse. Rapidement on n’entendit plus que quelques toussotements, raclements de gorges et reniflements.

« Le bon jour à ce conseil, nous sommes céans pour tenir audience plénière de la cour des bourgeois du roi en ce… »

Il se tourna vers l’écrivain, qui compléta aussitôt :

« 25e jour de mars.

— Nous devons examiner meurtrerie d’une pérégrine, questions d’usage et plusieurs documents pour la Grand Secrète[^grandsecrete]. Je vous laisse commencer les discussions ? »

Le plus petit des jurés se tourna vers le mathessep.

« Maître Ucs, pouvez-vous faire récit ? Est-celà meurtrerie ou simple homicide ? »

Le sergent s’avança d’un pas :

« De sûr meurtrerie, maître André, une pauvre infortunée qu’on a fort sauvagement occis en une venelle. Rien n’a été robé parmi ses biens et le corps fut celé d’ordures. »

Quelques murmures indignés ponctuèrent la dernière déclaration.

« Nous ne savons pas encore exact moment de l’assaut, mais le jeune homme présent là – il désigna Ernaut du doigt – affirme qu’elle était ici avec son mari. Il nous faut le trouver et nous serons éclairés.

— Se pourrait-il que le méfait soit sien ? Demanda le juré à l’allure martiale.

— Par ma foi, j’aurais peine à croire, maître Sicherius. Tant de sauvagerie qu’on dirait œuvre de bête, le visage a été brisé et le corps lui-même a reçu si grands coups qu’on croirait tous les os rompus. »

Le vicomte se pencha en direction d’Ernaut.

« As-tu bonne connoissance de cette pauvresse ? Avait-elle bonne entente avec son époux ?

— Je ne saurai le jurer sur Évangiles, messire, je n’ai pas souvenance qu’ils se soient bestanciés. Et lui ne m’a pas laissé impression d’être méchant homme.

— Saurais-tu en faire montrance au guet ? A-t-il quelque signe reconnaissable ? S’il est innocent, il va de sûr s’enquérir de sa dame, et peut-être demander justice. Dans le cas contraire, il cherchera à fuir…

— Si ce n’est déjà fait, messire Arnulf », rétorqua le juré à l’imposante barbe.

L’assemblée se tint coite tandis que le vicomte fixait son contradicteur, ne laissant paraître aucune émotion. Au bout de quelques instants, il se tourna de nouveau vers Ernaut, qui n’attendait que ce signe pour répondre.

« Il n’est que pérégrin, sans rien de particulier, mais je saurais le reconnaître si je le voyais. Outre, nous avons quelques communs amis.

— Parfait, en ce cas, tu te tiendras au service du mathessep Ucs et de ses hommes. J’aimerais entendre l’homme dans deux jours, au prochain conseil. »

Après un signe de connivence adressé au chef des sergents, de nouveau appuyé contre les colonnettes de la fenêtre, Ernaut trouva une place sur un des bancs. Pierre de Périgord prit alors la parole, soucieux de soulever un point de détail qui le chiffonnait.

« Ne faudrait-il pas en référer aux frères de Saint-Jean ? Le lieu de découverte est de leur ban[^ban] ce me semble.

— Pas pour crime de sang, maître Rainald. C’est bien à la Cour des Bourgeois du Roi de s’en occuper, répliqua André.

— Je suis d’accord, mais il ne semble pas déshonnête de montrer quelques égards. Les tenir informés. »

Le vicomte soupira, se frotta le menton.

« Les frères de l’hôpital ont déjà leur suffisance de soucis en cette période de Pâques. J’ai grand doute qu’ils voient là pertinente nouvelle.

— Il s’agit tout de même de meurtrerie de pérégrin, messire Arnulf. »

Le vicomte allait répondre lorsque Rainald Sicherius crut bon d’intervenir :

« En ce cas, vu l’endroit et la nature de la victime, je dirais qu’il serait bon de porter aussi message aux chanoines du Saint-Sépulcre »

Pierre de Périgord leva les yeux au ciel, provoquant l’agitation de son confrère qui persista.

« Ils sont concernés par le sort des marcheurs de la Foi autant que les frères de Saint-Jean..

— Cela n’a rien à voir, l’hôpital les accueille, les nourrit, prend soin d’eux. Et surtout, c’est aux marches de leur quartier que tout cela est arrivé.

— Parce que le souci des âmes que manifestent les chanoines vous semble inférieur ?

— Certes non. C’est juste qu’ils n’ont guère à voir en cette affaire selon moi.

— Et par ma foi, moi j’affirme que l’on ne peut s’accorder avec l’un et négliger l’autre.

— Vous dites cela, car vous savez fort bien que tout ce qui entre dans les oreilles du prieur Amaury ne tarde guère à traverser celles du patriarche Foucher ! Nous ne sommes pas là pour jouer ses espies ! »

Rainald fit mine de quitter de son tabouret lorsque le vicomte intervint, la main levée en guise d’apaisement.

« Nous n’avons pas à prendre parti en ces querelles. D’autant que depuis le voyage du patriarche auprès de Sa Sainteté Adrien[^adrien4], la paix semble être de retour entre eux. N’allons pas agiter braises sous la cendre.

— Qu’entendez-vous par cela ?

— Que soit nous donnerons nouvelle à tous, sinon à nul d’entre eux. »

André de Tosetus s’éclaircit la voix pour attirer l’attention sur lui.

« Il ne serait guère prenant de leur porter message à tous. Ne serait-ce que pour avoir quelque savoir sur l’époux, ainsi que pour prévenir les fidèles. Certains pèlerins ne sortent guère hors l’église du Saint-Sépulcre et d’autres, malades, sont tenus en la clôture de l’hôpital. »

Le vicomte hocha le menton, attendant un contradicteur éventuel avant de donner des instructions précises en ce sens à Ucs de Montelhs et à l’écrivain. Puis ils passèrent à des questions financières, fort complexes, dont Ernaut se désintéressa bien vite. D’ailleurs, il voyait que le mathessep lui-même dodelinait parfois de la tête. Au contraire, certains participants dans la salle défendaient avec vigueur leur parti, détaillant les arguments sans fin. Certains d’entre eux semblaient être des professionnels, habitués à des formulations ampoulées et impressionnantes, rapidement ennuyeuses. Baset finit par se rapprocher d’Ernaut et lui fit signe de se pencher vers lui.

« Sais-tu pourquoi saint Yves est seul avocat en Paradis ? »

Ernaut fit signe que non de la tête. Le sergent commençait à rire avant même de souffler la réponse.

« Parce que s’il en était deux, ils ne feraient que parler et nul ne connaitrait plus la paix au Ciel. »

Baset ricana à plusieurs reprises, apparemment enthousiasmé par sa blague. Poli, Ernaut se força à sourire et à approuver de la tête comme s’il trouvait cela drôle. En l’occurrence, il trouvait surtout le temps long. Appuyé contre le mur du fond, il finit par s’assoupir. Le mouvement autour de lui le réveilla alors que la séance était levée.

Tout le monde s’était assemblé en petits comités et les discussions allaient bon train. Baset s’entretenait avec le mathessep et le vicomte quand Ernaut le retrouva. Arnulf le vit approcher et baissa la voix, de sorte que le jeune homme n’entendit que la fin de la conversation.

« … Nous en rediscuterons lorsque Geoffroy sera présent. C’est prud’homme avisé pour pareils soucis. »

Sur cette dernière phrase, le vicomte salua rapidement les présents et sortit de la salle aussi vigoureusement qu’il y était entré. Le mathessep se rapprocha d’Ernaut pour lui parler, mais fut intercepté par l’écrivain qui avait apparemment encore des documents à lui présenter. Voyant que Baset ne manifestait pas l’intention de s’adresser à lui, le jeune homme attendit auprès des fenêtres.

Autour de lui, de nombreux bourgeois évoquaient l’attaque sur Panéas. Si Baudoin avait remporté une victoire dont il avait besoin, financièrement, et pour l’image de la couronne après tous les soucis avec sa mère, ils déploraient le fait que beaucoup de nouveaux venus aient poussé à une pareille opération. Cela risquait de déclencher des hostilités avec les Damascènes, avec lesquels la paix était établie depuis quelques années, à des conditions plutôt avantageuses en plus, avec le versement annuel d’un tribut en bon argent.

Tout à son pèlerinage, Ernaut n’avait pas réalisé que le royaume de Jérusalem avait une telle activité militaire alors même que des milliers de personnes le parcouraient simplement pour accomplir leurs dévotions. Il avait appris qu’au début de février il y avait eu une chevauchée au nord de la Galilée, mais n’en savait guère plus. Un autre groupe vint se placer plus près de lui et il perdit le fil, pour découvrir cette fois que les Égyptiens avaient voilà quelques mois écumé les côtes du pays, remontant assez loin et se livrant à des opérations de piraterie. Ils s’étaient emparés de plusieurs navires et cargaisons. L’un des interlocuteurs disait que maintenant qu’Ascalon avait été prise, il était temps de se doter de nefs pour régler son sort à la flotte infidèle qui menaçait sans cesse les rivages. Ce n’était bon ni pour les affaires ni pour le royaume selon lui, en insistant sur le premier point.

Très vite, ils changèrent de sujet, comparant leurs avanies respectives depuis quelques années, qui avaient entamé leur réussite, les amenant en deça de leurs espérances malgré l’évidente prospérité que leurs tenues proclamaient. Le vainqueur dans ce concours d’apitoiement fut un rouquin, avantagé par une relative surdité, qui racontait d’une voix forte les récents et violents tremblements de terre dans les territoires du nord. Il y avait perdu sa femme et de nombreuses balles de soie, et semblait tout autant désolé par les deux tragédies.

À aucun moment Ernaut n’eut l’impression d’être dans la cité sainte à quelques mètres du Sépulcre du Christ, à l’approche des fêtes de Pâques. Il aurait aussi bien pu se trouver aux foires champenoises ou normandes qu’il avait tant fréquentées avec son père. Lorsque Ucs de Montelh vint chercher Ernaut, ce dernier allait devenir incollable sur les différentes routes qui permettaient de payer le moins de péages entre Damas et Tyr.

« Tu vas me compaigner voir quelques autres sergents. Histoire de leur dépeindre le mari.

— Y aurait-il moyen de s’offrir de quoi manger en chemin ? », risqua Ernaut.

Le mathessep le dévisagea comme s’il avait vu une étrange bête puis sourit amicalement.

« Bien sûr, nous passerons par Malquisinat, cela ne fait pas grand détours pour se rendre à la Porte de David. »

Il asséna à Ernaut une grande tape dans le dos comme si ce dernier venait lui raconter une bonne blague puis l’invita d’un geste à quitter la salle à sa suite. Derrière eux, oublié par le mathessep, trottinait Baset, le regard sombre.

### Après-midi du lundi 25 mars 1157

Les abords de la porte de David étaient très animés lorsqu’ils sortirent de la fortification après avoir entretenu quelques sergents. Principale entrée de la cité, il était toujours difficile de la franchir. Selon le moment de la journée, elle était encombrée d’animaux, de maraîchers portant leurs récoltes au marché, de caravanes marchandes, de groupes de voyageurs à pied, à cheval, en chameau. On s’y retrouvait, on s’y séparait, on y commerçait dans les premières échoppes aux alentours. Le tout sous la surveillance d’une porte fortifiée qui abritait les douanes royales.

Supervisant la cohue depuis leur vigie, les gardes lançaient de temps à autre une corde par-dessus le parapet pour qu’un gamin aille leur remplir une outre de vin contre une piécette pour le service rendu. Plusieurs écuries se trouvaient là également, hébergeant les montures des voyageurs, de ceux qui n’avaient pas la place chez eux, ou pas l’envie de s’en occuper eux-mêmes.

L’accès principal, la rue de David, partait droit en direction du mont du Temple, de l’autre côté de la ville. Très importante artère marchande, elle était encombrée d’étals qui débordaient des produits les plus variés, ne laissant que peu d’espace aux passants. Tandis qu’ils avançaient, de nombreux boutiquiers saluaient Ucs de Montelh, de façon parfois un peu trop empressée, et de temps à autre sans enthousiasme.

Ernaut s’interrogea sur les raisons de cette notoriété.

« Êtes-vous né en la cité pour être si bien connus de tous ?

— Non pas. Je suis arrivé avec l’ost du roi Louis de France, pour le secours d’Édesse[^croisade2a].

— Je vous ai peut-être vu alors. J’ai passé mes enfances à Vézelay, où l’abbé Bernard est venu parler devant les hommes du roi justement.

— Je n’y étais pas, je l’ai rejoint par la suite, lors son voyage vers le sud. »

Ernaut se remémora la vaste assemblée qu’il avait admirée alors qu’il n’était qu’un enfant. Ses souvenirs étaient imprécis, il avait été à l’époque émerveillé par l’apparence des riches barons, des chevaliers qui se pressaient autour du pouvoir royal. Son père lui avait désigné les puissants réunis là, mais il n’avait gardé en tête presque aucun nom.

Des années plus tard, il s’était ému d’apprendre que beaucoup étaient morts en chemin et qu’ils n’avaient abouti à rien. La fière armée du Christ avait été réduite à néant dans les sables du désert devant la ville de Damas. Il en avait rêvé une fois, effrayé par les squelettes en haillons de soie, à demi ensevelis au pied de murailles immenses, du haut desquelles de noirs démons à forme humaine riaient. Il avait été très surpris d’apprendre que la cité victorieuse était désormais en paix avec le royaume de Jérusalem et lui versait même un important tribut chaque année. Un adversaire vainqueur acceptant de payer, quel étrange pays se disait Ernaut !

D’un autre côté, les infidèles de Damas étaient peut-être prêts à sacrifier une partie de leur richesse, qu’on estimait colossale, juste pour être tranquilles. De méchantes rumeurs prétendaient qu’ils avaient bien fait don d’une véritable fortune aux chevaliers du Temple du Christ pour qu’ils se retirent et convainquent l’armée des croisés d’en faire autant. Cette pensée vint à l’esprit d’Ernaut lorsqu’il aperçut deux manteaux blancs frappés de la croix rouge, arrivant dans leur direction. L’un d’eux était en civil, l’autre en tenue de guerre, portant haubert et baudrier. Ucs les remarqua aussi et indiqua au jeune homme :

« Ils possèdent de nombreuses propriétés en ville, et c’est le temps des loyers pour eux aussi. Ils doivent effectuer un contrôle.

— Je pensais qu’ils n’étaient que guerriers voués au martyr.

— Que crois-tu ? La guerre est fort vorace, en soudards, mais aussi en monnaies. Elle rapporte parfois, et coûte toujours. Les frères de Saint-Jean vont bien vite l’apprendre.

— Que voulez-vous dire ?

— Ils avaient pour usage de louer sergents d’armes, comme moi, pour leur protection et celle de leurs biens et propriétés. Désormais ils équipent frères chevaliers, des combattants qui portent le manteau noir de l’ordre.

— Et cela est gênant ? »

Ucs opina silencieusement.

« Il existe déjà les frères du Temple pour batailler encontre les infidèles, il n’est nul besoin de faire soldats des hommes voués à servir pérégrins. On ne peut traire deux vaches de la même main.

— J’aurais cru que chacun ici aurait à cœur de voir des nouveaux guerriers de la Foi prêts à tenir le fer. »

Le mathessep grogna, à demi convaincu.

« La populace jase, tu sais. Personne n’aime que le soigneur se fasse guerrier. Dieu a choisi un rôle pour chacun, et il faut savoir tenir son rang. Sinon c’est perpétuelle fête des fols. »

Il s’arrêta.

« Lorsque frère Gérard tenait l’hospice des pauvres, il n’avait certes pas en tête de devenir fieffé seigneur. Ses héritiers finissent par se croire supérieurs au patriarche. D’aucuns pensent que le vieux Raymond perd le sens avec les ans. Sans compter que le connétable Onfroi lui a fait don de la moitié de Panéas désormais.

— N’est-ce pas normal qu’ils aient si grande renommée ? On m’a dit que la fondation de l’Hôpital remonte au temps des anciens rois romains et que Christ lui-même y retrouvait ses disciples.

— Peut-être bien, moi aussi, j’ai ouï ces récits. Mais je n’y ai guère entendu parler de chevaliers portant harnois ni de combats la lance au poing. Il serait plus avisé de fieffer de vrais hommes de guerre plutôt que des servants de Dieu, voilà tout. »

Sur ce point, Ernaut ne pouvait qu’être d’accord. Comme tous les jeunes gens aventureux, il aurait rêvé de chevaucher l’écu au côté, la lance en main, pour aller conquérir des terres qu’il dirigerait ensuite depuis la forteresse qu’il léguerait à son fils. Il sourit, chaleureusement, bien qu’il soit jaloux du statut de son interlocuteur, un des hommes les plus importants de la sergenterie de la cité de Jérusalem.

Continuant leur avancée parmi la foule dense, Ernaut en profita pour détailler l’aspect de Ucs de Montelh. Il était vêtu d’une longue cotte de laine orange adroitement taillée, dont les bras, les poignets et le col étaient garnis de bandes d’un rouge vif, brodées en de délicats ornements floraux. Des chausses ajustées couvraient ses jambes et, s’il ne portait pas d’éperons dorés, il avait tout de même aux pieds des souliers bas, de peau noire, dont les décors de fil étaient désormais collés de poussière. Il se protégeait la tête d’un bonnet de feutre écarlate agrémenté d’une fibule. Et surtout, il arborait un magnifique baudrier sombre, réhaussé d’une boucle et d’un long mordant ornés d’argent. Ne dépassait que la poignée de son épée, à la fusée de cuir noir et au pommeau d’acier légèrement gravé, dont la sobriété indiquait bien qu’il s’agissait d’un instrument de travail quotidien.

Le mathessep s’arrêta au débouché de la rue du Patriarche qui partait sur leur gauche pour rejoindre le Saint-Sépulcre, vers le palais royal. Il stoppa Ernaut d’un geste et siffla un grand coup, attirant les regards des passants. Parmi eux, un rouquin se retourna et fit un signe à Ucs, révélant ainsi qu’il l’avait bien remarqué et l’attendait.

« Il me faut te laisser, garçon. Je te ferai savoir si je requiers de nouveau tes services. D’ici là, porte-toi bien.

— Grand merci, maître Ucs, j’espère que vous attraperez ce maraud. »

En réponse, le chef des sergents leva les yeux au ciel d’un air dépité en acquiesçant puis s’éloigna rapidement. L’adolescent décida de retourner vers l’hôpital afin de voir son frère. Il en était ennuyé, mais il lui fallait expliquer à Lambert ses dernières occupations. Il en concevait autant d’excitation que d’anxiété. Son aîné n’était en général pas aussi enthousiaste que lui à l’idée de se mêler des affaires des autres. Prudent et circonspect, il rappelait sans cesse à Ernaut ses devoirs, ne lâchant jamais la bride à celui qu’il semblait considérer comme un jeune étalon fougueux. Néanmoins, ses conseils et remarques étaient souvent frappés de bon sens.

La ruelle d’accès à la cour arrière de l’hôpital était bondée. De nombreux pèlerins passaient là une partie de leur journée, prenant l’air tout en s’offrant l’opportunité de rencontrer des compatriotes. Régulièrement, les frères de Saint-Jean étaient obligés de ramener de l’ordre, l’exubérance des attroupements finissant par gêner le service et troubler la paix recherchée. Ernaut avait même entendu parler de combats de coqs qui avaient été organisés quelques mois auparavant. L’absence du grand maître, Raymond du Puy, parti demander le soutien du pape dans son conflit avec le patriarche, avait entraîné un relâchement général.

En cette période de Carême, en pleine Semaine sainte, tout était calme et la seule activité pratiquée était la discussion en petits groupes. Ernaut réalisa que le soleil frappait un angle de bâtiment inoccupé. Il choisit un tronçon de muret qui lui semblait indiqué pour un peu de repos. Bien qu’il en sache l’intérêt, il ne se sentait pas encore prêt à affronter derechef Lambert et ses sermons. Il finit par fermer les yeux, se laissant bercer par le ronronnement ambiant et la douce chaleur qui montait des pierres chauffées. Il sursauta donc quand une main ferme vint lui taper sur l’épaule.

« Je vois que certains sont comme lézard ! »

Le visage souriant de Gringoire la Breite[^voirt1], le cheveu blanc en bataille comme toujours, vint se dessiner sur le ciel. Il empestait le vin et postillonnait comme à son habitude, déclamant comme s’il s’adressait à un pâtre au fin fond de ses champs.

« Alors, garçon, comment te portes-tu depuis le mois dernier ? »

Ernaut se leva et salua amicalement le marchand. Ce dernier avait une outre de vin à la main et la lui proposa tout en s’asseyant sur un bout du muret. Le jeune homme déclina d’un signe de tête et se poussa un peu pour son compagnon, clignant des yeux comme si on l’avait sorti d’un long sommeil.

« Plutôt pas mal. On a eu quelques frayeurs avec Lambert, il a attrapé méchante maladie et les fièvres ne le lâchaient pas. Ça fait presque trois semaines que les frères de l’Hôpital s’occupent de lui. Il est maintenant sauf, il sortira bientôt.

— C’est comme moi, je viens de laisser ma pauvre Ermenjart à l’hôpital à deux pas d’ici. Elle s’est trouvée mal à Jéricho et j’ai dû louer deux mules pour la porter sur un brancard, elle m’arrivait plus à marcher et ne tenait pas en selle. »

Les yeux larmoyants, il avala une grande rasade, s’essuyant la bouche du revers de la main.

« Elle tenait à aller jusqu’à la grotte de saint Jean-Baptiste. Eh bien maintenant, j’espère qu’il saura intercéder en sa faveur ! »

Ernaut tapota amicalement le bras de son interlocuteur et montra d’un mouvement ample les bâtiments monastiques qui les entouraient.

« Ne vous bilez pas, ils sont fort habiles. J’y vais chaque jour et je peux vous assurer qu’ils ont les meilleurs médecins à leur service. »

Voyant que sa remarque ne soulevait pas l’enthousiasme, il enfonça le clou, se frappant sur le ventre d’un geste démonstratif.

« Et certainement aussi la meilleure mangeaille. Pour sûr, ça nous change de ce qu’on devait croquer sur le *Falconus* !

— C’est gentil à toi d’essayer de m’apporter espoir, garçon. Le fait est que je suis trop inquiet pour penser à autre chose.

— Et trop soûl aussi pour chasser tes sombres humeurs » pensa Ernaut par-devers lui.

La dernière remarque de Gringoire avait coupé court à la conversation et les deux compères restaient donc là, assis, les yeux dans le vague. Ils regardaient sans la voir l’effervescence qui régnait sur l’endroit, groupes de voyageurs et de pèlerins qui allaient et venaient dans les hôpitaux, découvrant émerveillés le lieu où ils résideraient quelque temps, assistant parfois un proche pour son entrée ou pleurant entre eux la disparition d’un ami ou d’un parent. Un rassemblement un peu plus large se formait progressivement à quelques mètres. Plusieurs fidèles parlaient de façon agitée et la discussion semblait rapidement s’enflammer. On faisait cercle autour d’un homme tenant un long bourdon[^bourdon] à la main, qui paraissait disproportionné eu égard à sa taille. Ernaut se tourna vers son compagnon.

« Je vous abandonne quelques instants, maître la Breite, si vous le voulez bien. Je voudrais savoir de quoi il retourne là-bas. »

Le marchand hocha la tête en acceptation, sans émettre le moindre son. L’alcool commençait à l’abrutir et il lui fallait déjà beaucoup de concentration pour conserver une station à peu près verticale sur son postérieur. L’adolescent se leva et rejoignit le groupe informel qui s’était constitué. Plusieurs personnes, apparemment tous des voyageurs, confrontaient leurs opinions sur la découverte du matin. L’un d’eux avait entendu des sergents du guet en parler et des curieux s’étaient assemblés autour de lui. Un garçon à l’œil torve était en train d’apporter de l’eau à son moulin, visiblement angoissé de se trouver ainsi propulsé sur le devant de la scène.

« On m’a dit que c’était pauvresse morte de faim ou de maladie. C’est malheureux, mais ça peut s’encontrer aussi céans, dans le royaume de Dieu. »

L’air affecté, un moustachu à l’accent chantant s’avança.

« D’autant plus qu’on sait que moult sarrasins rôdent à l’entour.

— Quel rapport avec le fait qu’une malheureuse passe outre de faim ou de maladie ? »

Le moustachu prit un air mystérieux, levant le menton en un éloquent signe d’incertitude. Tapant du bâton sur le sol empierré dans un mouvement emphatique, une vieille femme au visage avenant intervint à son tour.

« Ce serait tout de même bien triste pour une marcheuse de Dieu de succomber à quelques pas de la plus belle demeure du Seigneur sur terre. »

Elle conclut son intervention par un signe de croix, aussitôt imité par quelques dévots. Celui qui semblait être au centre du cercle la regarda comme si elle venait de proférer un juron en pleine messe.

« Vous croyez qu’une pauvresse comme vous dites, elle aurait vêtements de bonne toile et bourse bien garnie pour un voyage ?

— Elle n’a pas été détroussée ? Vous en êtes acertainé ? » Demanda une grosse femme à face de hibou.

Planté devant elle, le petit homme sec, au visage en lame de couteau, répondit en tapant du poing sur son bourdon, martelant sa déclaration comme s’il voulait la faire pénétrer de force dans les cervelles environnantes.

« Absolument certain ! La gamine avait repéré les souliers, de frais ressemelés. En les tirant, elle a découvert qu’il s’y trouvait encore des pieds ! »

Une bordée de « Mon Dieu », « Sainte Mère de Dieu » ou de plus prosaïques expressions ponctuèrent alors les échanges quelques instants. Le narrateur, pénétré de son importance, attendit que les visages redeviennent attentifs et les bouches silencieuses pour continuer, usant de toute sa science du discours.

« Elle était encore tout habillée, sa besace à l’épaule… intacte ! »

Une voix féminine se fit entendre depuis l’arrière.

« Ne me dites pas qu’on l’a… »

Le conteur écarta d’un geste agacé cette proposition, soucieux de conserver son auditoire concentré sur son propre récit.

« Ça, je ne sais ! Ce qui est bien sûr, c’est que le murdrier est un sacré furieux. »

Il bomba le torse et prit une profonde inspiration, tenant l’assistance suspendue au bout de ses lèvres. Il semblait se repaître avec une délectation morbide tandis qu’il prononça sa dernière phrase d’un air solennel.

« Sa gorge avait été entièrement écrasée, comme sa bouche. Pareil que si on l’avait écrasée à coup de pierres. Tout le bas de son visage n’était que viande et os broyés ! »

Une nouvelle série d’exclamations, plus colorées et imagées que la première fusa soudain, chacun relevant le buste et reprenant son souffle après l’avoir trop longtemps retenu. Un silence respectueux s’installa quelques instants, que seul le moustachu osa finalement briser :

« Cela ne peut pas être l’œuvre d’un bon chrétien. C’est sûrement travail de sarrasin. On dit qu’ils… »

Il hésita un instant, comme si la révélation qu’il allait faire était trop affreuse pour être exprimée ainsi en plein jour.

« … Qu’ils mangent de la chair humaine ! »

Sa remarque souleva un tollé vite suivi d’un tumulte où chacun s’acharnait à convaincre son voisin de la véracité d’un point de vue ou de l’autre. Ernaut avisa la femme à tête ronde à sa droite et un petit groupe de personnes âgées à sa gauche et opta pour un repli stratégique, hochant du chef en assentiment général tout en reculant peu à peu. Pour une fois, la retraite lui semblait le choix le plus pertinent.

Lorsqu’il fut enfin sorti de l’assemblée, il se retourna, pour se retrouver face au moustachu, qui errait justement en quête d’une oreille attentive. Ernaut marqua un temps, puis réalisa soudain que cela pouvait être pris comme une invitation, ce que l’attitude de l’homme lui confirma d’un coup d’œil. Il ne lui laissa pas l’opportunité de commencer.

« Je suis désolé, je dois m’occuper de mon vieux père, qui est là malade. J’avais oublié, je vous demande le pardon. »

Puis il planta là son interlocuteur sans attendre davantage et retourna auprès de Gringoire, qu’il trouva assoupi contre le pilier. Il posa les mains sur ses hanches et jeta un regard alentour.

« Des ivrognes et des excités ! Il est beau le royaume de Dieu sur terre ! »

Réalisant ce qu’il venait de dire, il regarda par-dessus son épaule vers le nord, une lueur d’angoisse sur le visage. Un peu contrit, il se repentit immédiatement de cette pensée blasphématoire, inquiet d’une telle provocation à quelques mètres de l’endroit le plus saint au monde.

### Soirée du lundi 25 mars 1157

Lorsqu’il descendit les marches qui menaient vers le parvis du Saint-Sépulcre, Ernaut arborait sa mine des mauvais jours. Comme il l’avait redouté, Lambert l’avait vivement sermonné sur son attitude, déçu de le voir se lancer une nouvelle fois dans des aventures qui le dépassaient. Il craignait que la fréquentation de Régnier d’Eaucourt ne lui soit montée à la tête[^voirt1]. Ils étaient là d’abord pour accomplir leurs vœux de pèlerins puis pour s’établir comme colons, pas pour se pencher sur des mystères qui en concernaient d’autres.

Ernaut lui avait bien répliqué qu’il trouvait une telle attitude bien peu chrétienne, qu’il ne cherchait qu’à rendre service et qu’il ne comprenait pas en quoi cela pouvait offenser Dieu. L’hospitalier en charge de la travée où était soigné Lambert avait fini par leur demander de parler moins fort, car ils dérangeaient les malades alentour. Excédés, les deux frères avaient échangé quelques paroles apaisantes avant de se séparer encore agacés.

Comme toujours, la rue était pleine de monde et il fallait jouer des coudes pour franchir la colonnade qui fermait au sud la placette donnant accès à l’église. En cette fin de journée, le soleil commençait à déserter l’endroit et les parties basses étaient désormais noyées dans l’ombre. Malgré tout on y rencontrait, comme à toute heure, quelques camelots installés à côté de leur natte, présentant des paniers de breloques et de babioles. On pouvait acheter des cierges, comme dans la voie à côté, des objets pieux, des souvenirs.

Il s’y trouvait également des marchands de victuailles, fruits frais, tourtes et pâtés. Certains pèlerins passaient beaucoup de temps dans le Saint-Sépulcre et n’en sortaient que pour s’approvisionner rapidement. Contre les absides des chapelles à l’ouest s’étaient installés quelques artisans qui proposaient de réparer chaussures et cottes à bas prix. Plusieurs fripiers vantaient les pièces de vêtement fatiguées qu’ils offraient à des prix modiques.

Parmi les petits groupes qui se tenaient là, Ernaut remarqua plusieurs hommes de couleur. Il avait été très surpris la première fois qu’il les avait vus, persuadé qu’ils étaient tous mahométans. On lui avait expliqué qu’il existait un royaume chrétien, par-delà le désert, et qu’il abritait des disciples du Christ. C’était les descendants de la reine de Saba croyait-on, celle qui avait été subjuguée par Salomon. Les dévisageant de loin il s’arrêta un instant pour acheter un beau cierge à un des revendeurs. Il en choisit un assez gros, de bonne cire, qu’il souhaitait déposer pour son frère. Malgré toutes leurs dissensions, Ernaut n’était pas assez coléreux pour lui refuser l’aide de quelques prières pour accélérer sa guérison.

Sur les marches permettant d’accéder directement à la chapelle du Calvaire, en passant au-dessus des celle des Trois Maries, deux gardiens étaient affalés, discutant avec animation tout en surveillant d’un œil distrait le flot des pèlerins qui allaient et venaient. Une famille, certainement arrivée depuis peu était figée devant le bâtiment, admirant les vantaux de bronze, les tympans sculptés et peints qui les surmontaient. Comme ils parlaient le même dialecte français que lui, Ernaut entendit bien ce que le père expliquait aux enfants.

Les scènes faisaient allusion aux événements qu’ils célébraient alors, durant la Semaine sainte. Depuis la porte gauche, la plus à l’ouest, figurait la résurrection de Lazare, Jésus sur son âne, l’entrée à Jérusalem, le dernier repas avec les disciples et l’arrestation suite à la trahison de Judas. Le tympan occidental présentait la Vierge avec l’Enfant tandis que l’oriental était orné du plus beau des miracles, le Christ en majesté, ressuscité, avec Marie-Madeleine prostrée à ses pieds. Lambert avait traduit à Ernaut la phrase latine que Jésus tenait, extraite de l’évangile :

« *Quid, mulier, ploras ? Iam iam quem quaeris adoras. Me dignum recoli iam vivum tangere noli*[^quidmulier] ».

Ernaut connaissait bien l’histoire, toute son enfance ayant été bercée des récits de la vie de Marie-Madeleine, dont le corps était conservé à Vézelay où il avait grandi.

Lorsqu’il pénétra dans l’édifice, la fraîcheur habituelle le saisit. Il bouscula plusieurs personnes, occupées à déchiffrer le texte sur la première tombe qu’ils avaient découverte. Ernaut l’avait fait en son temps, impressionné d’approcher la dernière demeure de ces rois de légende, héroïques devanciers, conquérants des territoires latins d’Orient. Il venait souvent se recueillir sur leurs dépouilles, cherchant l’inspiration, éprouvant aussi du plaisir à se remémorer leurs exploits. Depuis l’entrée on trouvait d’abord Foulque, où se situait le groupe de curieux actuels. Il était le père de Baudoin, l’époux de Mélisende dont la réputation alimentait encore bien des rumeurs, alors même qu’elle vivait désormais retirée de la Cour. Puis Baudoin le second, l’ancien comte d’Édesse, cousin des premiers souverains. Au-delà de la colonnade, on pouvait ensuite admirer la sépulture de Baudoin, le premier monarque latin de Jérusalem, frère du duc Godefroy. Celui-ci venait en dernier, au plus près du chœur. Au-dessus de sa pierre tombale, de petites colonnettes soutenaient un couvercle en gable, surmonté d’une croix.

Depuis qu’il était arrivé, Ernaut avait pu entendre toutes sortes de légendes sur le conquérant de Jérusalem. On le décrivait comme extrêmement pieux, ayant rétabli le service chrétien dans bon nombre d’édifices, ce qui n’intéressait que peu le jeune homme. Par contre, il était passionné par les récits de faits d’armes, les batailles la lance au poing, les forteresses rasées, les villes prises. On disait qu’il était capable de trancher en deux monture et cavalier d’un seul coup d’épée ou qu’il avait assommé un chameau d’une simple gifle. Sans nul doute, il constituait pour Ernaut le genre de modèle auquel il pouvait s’identifier.

Dépassant une nouvelle troupe de croyants habillés comme des Syriens, parlant une langue qui lui était inconnue, il parvint finalement dans le chœur. À sa gauche il voyait la rotonde accueillant le Sépulcre du Christ, qu’il visitait fréquemment. Une foule de petits groupes de fidèles se pressait entre les piliers, le visage tourné vers les hauteurs, parfois plongé vers un recoin, tous s’extasiant devant l’endroit, imprégnés de sa majesté. Certains avançaient à genoux, d’autres sur de pauvres béquilles, de temps à autre aidés par un proche. Là où il se trouvait, Ernaut pouvait admirer la coupole loin au-dessus de sa tête, par la lanterne de laquelle une abondante lumière descendait. Il se dirigea vers la droite, face au chœur des chanoines.

L’abside était évidée par de nombreuses arcatures, surmontées d’une mosaïque saisissante, presque achevée. Le Christ, portant la Croix dans Sa main gauche, menait Adam vers le Paradis, entouré de ses disciples et de sa famille. Une inscription en latin courant sous ce décor lui avait été traduite par un clerc les premiers jours, mais Ernaut n’en savait plus guère la signification, pas plus que de celle qui ornait l’arc fermant le lieu. Il en avait retenu qu’il fallait prier Jésus car Il était ressuscité et, surtout, qu’il faisait des dons aux hommes. Ce dernier point lui avait particulièrement fait chaud au cœur.

Dans l’abside étaient disposés les sièges des chanoines, entourant la chaire du patriarche. Au-dessus de celle-ci une icône de la Vierge était flanquée de celles de saint Jean Baptiste et de l’ange Gabriel. Et devant se trouvait le maître-autel de l’Anastasis. De très nombreux croyants étaient assemblés là, priant à mi-voix ou en commun. Parfois, des clercs de plusieurs groupes organisaient quelques oraisons publiques. Tout cela sous la surveillance attentive de frères lais du Saint-Sépulcre qui veillaient à ce que les prérogatives des chanoines et du patriarche ne soient pas violées.

Toujours prompt à se moquer, Ernaut avait remarqué qu’ils renvoyaient presque systématiquement vers le tronc des offrandes quiconque avait un doute sur ce qu’il convenait de faire. Combien de fois leur avait-on conseillé de brûler quelques cierges, de faire un don à l’autel et, bien sûr, de prier après tout cela. Il salua de la tête quelques personnes qu’il avait croisées lors de ses visites dans les lieux saints. On finissait fréquemment par retrouver quelques-unes de ses connaissances ici ou là. Il essaya de voir si l’époux de la pauvre défunte était là, sans conviction et sans succès.

Après avoir allumé et fixé sa chandelle sur un des candélabres, il s’agenouilla et entreprit de réciter quelques *Ave Maria*. Il savait la Vierge toujours prompte à intercéder en faveur des croyants sincères, ce que Lambert était sans nul doute. Ernaut ne s’aventurait guère dans ses prières en dehors du *Pater*, de l’*Ave* et du *Credo*. Il pouvait suivre des lèvres quelques autres, mais sans l’aide d’un prêtre, il était bien en peine de se les remémorer. Tandis qu’il récitait, concentré sur sa demande, il sentit une présence silencieuse derrière son épaule. Il jeta un coup d’œil et aperçut l’un des chanoines, reconnaissables à leurs tenues de laine d’excellente qualité et à leur tonsure soignée. Celui-ci avait une vingtaine d’années, le regard doux et le sourire chevalin. Voyant qu’Ernaut se signait, il comprit que ses oraisons étaient terminées et le salua d’une inclinaison timide du buste.

« Le bonjour, frère, je suis désolé de vous déranger en pleines oraisons, le père prieur Amaury[^amaurynesle] aurait grand désir de vous encontrer. »

Ernaut écarquilla de grands yeux.

« Pardon ? Vous devez faire erreur, je ne suis que pauvre pérégrin bourguignon, et n’ai jamais eu affaire au père prieur. »

Le chanoine arbora un franc sourire, d’une oreille à l’autre.

« Je le sais, j’avais pour mission de vous trouver, vous êtes bien Ernaut, pérégrin de Vézelay ? »

Ernaut hocha la tête, circonspect.

« Comment m’avez-vous reconnu ? »

Le sourire redoubla, menaçant de se transformer en rire. Le jeune homme semblait surpris par l’énormité de la question. Il ouvrit les mains et les bras, mimant comme une grosse bête et allait s’expliquer quand Ernaut réalisa. Difficile de le confondre avec un autre en effet. Lorsqu’il s’enquit de la raison pour laquelle le prieur souhaitait le voir, il n’obtint qu’une réponse évasive. Il ne se pouvait tout de même pas qu’on convoque ainsi tous les pèlerins qui auraient blasphémé de temps à autre. Le prieur y aurait perdu ses journées ! Dubitatif, il suivit donc le petit homme dans le déambulatoire qui entourait le chœur, longea la chapelle Saint-Nicolas puis franchit une porte et monta un escalier.

Là, il se retrouva dans un élégant cloître, qu’ils traversèrent, passant auprès d’un édicule en pierre qui donnait de la lumière dans la chapelle Sainte-Hélène située en dessous. L’endroit était particulièrement calme par rapport au reste de la ville. Quelques silhouettes se déplaçaient discrètement, chanoines et frères lais à leur service, la plupart du temps silencieuses ou parlant à voix basse. On entendait le pépiemement des oiseaux qui s’accrochaient sur les ressauts des bâtiments.

De l’autre côté de la cour, une vaste salle accueillait quelques bancs tournés et une chaire en bois sculpté et peint, posée sur une estrade. On avait suspendu au-dessus un crucifix et une icône de la Vierge. Un faux appareillage de pierre était dessiné sur les murs dans des coloris ocre. Un magnifique lutrin supportait un épais volume, ouvert, qu’était en train de survoler avec détachement un chanoine. Il avait une trentaine d’années, le visage massif, la mâchoire lourde. Son nez court et large était surmonté de deux sourcils d’un noir de jais. Le plus surprenant dans tout cela était son regard, extrêmement doux : deux iris clairs qui semblaient effleurer les choses sans jamais chercher à les brusquer. Sa tenue, d’un noir profond, habillait un corps qui aurait pu convenir à la guerre, mais qu’on avait contraint, par des privations, à demeurer modeste. Il portait une belle croix émaillée autour du cou et à son doigt brillait une pierre sur un anneau. À l’entrée des visiteurs, il s’arrêta et, tendant la main, incita d’un geste Ernaut à s’approcher.

Celui-ci se dit au dernier moment qu’il devait sans doute baiser la bague en signe de respect. Ceci fait, le prieur alla s’assoir sur sa chaire. Il toussa plusieurs fois avant de prendre la parole tandis que son invité laissait errer son regard sur les images saintes au-dessus, n’osant pas fixer un personnage si important. Sa voix était grave, harmonieuse, modelée par des années de chant religieux.

« Tu dois avoir souci de te trouver devant moi, jeune homme. Demeure sans crainte, c’est en ami que je te reçois. »

Ernaut sourit à cette affirmation, manifestant son soulagement par un relâchement instantané de sa posture.

« On m’a prévenu que pauvresse avait encontré malemort voilà peu et que c’était pérégrine. Comme tu l’as connue, j’aurais aimé en savoir plus sur cette infortunée. Elle a été frappée aux marches de notre domaine, et je me sens donc d’autant plus coupable de ce qui lui est arrivé »

Un peu décontenancé, Ernaut lui raconta ce qu’il avait déjà déclaré à plusieurs reprises, sans trop se mettre en avant. Le prieur Amaury demeurait impassible, les mains croisées, les coudes posés sur les bras de la chaire. À un moment, un serviteur s’avança pour lui demander audience, lui susurra quelques mots à l’oreille avant d’être congédié d’un geste. Le prélat semblait répugner au moindre mouvement, en dehors du clignement lent de ses yeux.

En outre, il resta silencieux bien longtemps. Ernaut s’était tu sur de nombreuses hésitations, indécis quant à la conclusion qu’il devait faire de son récit. Il ne connaissait en fait la femme que de vue, l’ayant croisée quelques fois et il avait retenu son nom car elle était nommée ainsi qu’une amie de sa famille. Il ne savait rien de sa vie avant qu’elle ne reçoive la besace et le bourdon. Et même depuis lors, il ne pouvait prétendre l’avoir beaucoup fréquentée. Le prieur sourit discrètement, de façon paisible et se leva pour de prendre la parole.

« Je comprends mieux l’agitation en ce début de Sainte semaine. Les malheurs frappent chaque fois sans prévenir, telle la foudre qui s’est abattue sur nous voilà une dizaine d’années[^foudrea]. Mais, ainsi que notre église, nous en ressortons grandis, magnifiés par l’effort que demande chaque nouvelle épreuve. Dans son malheur, cette malheureuse a trouvé un ami pérégrin, par-delà la mort. J’en suis bien aise. »

Ernaut acquiesça avec hésitation, pas tout à fait certain de bien saisir toutes les implications engendrées par cette affirmation. Il allait répondre lorsque le prieur continua.

« Sois assuré que le destin de chaque âme qui foule ce lieu saint nous tient à cœur. En cette période de recueillement et, bientôt, de joie, nous tenons à être présents aux côtés de ceux qui ont cheminé jusqu’à nous. C’est bien sûr devoir de tout chrétien, mais nous plus encore devons nous y tenir. »

Il marqua une pause, laissant le temps à Ernaut de bien apprécier ce qu’il sous-entendait.

« J’aimerais donc savoir si tu serais prêt à nous aider en cette tâche ardue. »

Sans qu’il ne puisse se l’expliquer, Ernaut eut tout à coup l’impression d’être une musaraigne face à un gros chat. Il ne pouvait décemment répondre par la négative à une telle question. Il hocha donc la tête, avec toute la conviction qu’il pouvait manifester, glissant un « Bien sûr » quelque peu hésitant.

« Je te mercie, jeune homme. Cela échauffe le cœur de savoir que d’autres sont prêts à mettre leurs pas dans ceux du Christ. »

Il s’arrêta encore une fois, se penchant en avant et modulant sa voix avec doigté :

« De façon à pouvoir être le plus efficace possible dans notre bataille contre le démon, il me serait agréable d’être informé si grande nouvelle t’apparaissait. Si tu en as l’occasion, de temps à autre, laisse donc quelques messages à mon intention à l’un de mes frères. Peut-être enverrai-je après toi un coursier quérir nouvelles de temps à autre si le besoin s’en fait sentir. »

Il fut interrompu par le carillonnement d’une cloche, toute proche.

« Je suis désolé, il me faut mettre fin à l’entrevue, voilà vêpres qui sonnent et je me dois de m’apprêter pour l’office. Mille grâces de ton aide, jeune Ernaut. Que le Seigneur t’accompagne. »

Il lui tendit la main et fit signe au jeune chanoine d’approcher. Il était visiblement resté dans les environs tout le temps de l’entretien. Ernaut baisa l’anneau et se retira poliment, s’inclinant à plusieurs reprises. Tandis qu’il repartait vers la sortie, il se risqua à demander :

« Voilà fort impressionnant prélat. Jamais je n’aurais cru avoir l’honneur de parler avec lui.

— Le père Amaury est homme du verbe. Il aime parler et sait bien le faire.

— Certes. Pourtant je vois mal en quoi modeste pérégrin tel que moi peut lui être d’usage. »

Le chanoine esquissa une rapide grimace de mécontentement avant de répondre.

« Nous sommes tous fort inquiets de ce qui se passe dans le quartier. Les frères de Saint-Jean ne se contentent pas de faire don de marcs d’argent à Sa Sainteté Adrien. Tout leur est bon pour nous affaiblir. »

Avant qu’il n’ait le temps de demander des éclaircissements, Ernaut était à la porte. Le chanoine le congédia d’un signe de tête affable et lui ouvrit le passage. Lui aussi devait être pressé de rejoindre la congrégation pour l’office. Ernaut sortit donc après avoir rapidement salué. Pour sa part, il n’avait qu’une envie : trouver de quoi manger en rentrant chez lui au plus vite. La journée avait été bien longue et même lui aspirait à un peu de repos.
