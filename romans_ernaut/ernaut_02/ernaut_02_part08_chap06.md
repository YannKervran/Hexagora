## Chapitre 6

### Jérusalem, parvis du Saint-Sépulcre, matin du dimanche 14 avril 1157

Retrouvant le parvis qu’il avait si souvent traversé, Ernaut jeta un rapide coup d’œil aux sculptures des tympans au-dessus de l’entrée de l’église. Il ne savait pas trop où aller, n’ayant pour l’instant aucune tâche particulière à faire. Il avait eu espoir de croiser Libourc à l’office, mais cela n’avait pas été couronné de succès. Il n’avait plus guère d’excuses pour se rendre à l’hôpital auprès de Sanson et ne se sentait de toute façon guère le cœur à cela.

Retrouver la jeune fille et discuter avec elle de choses et d’autres l’aurait empli de joie, mais il n’avait pas l’entrain nécessaire pour discuter avec son vieux père. Ernaut réalisa alors qu’il avait proposé de faire un don et n’avait pas encore tenu sa parole. Il rentra donc de nouveau dans l’édifice et se dirigea à gauche vers l’autel accolé à la tombe du Christ. Un tronc y était installé, sous la surveillance d’un valet. Â ses côtés, un chanoine discutait avec des pèlerins dans une langue inconnue d’Ernaut. Il pointait les phrases qui couraient autour de l’édicule de marbre, les lisant à voix haute avant de les traduire en termes compréhensibles pour ses auditeurs. Puis il s’efforçait de leur en faire découvrir le sens caché.

Tout l’édifice, comme chacune des églises du monde, recelait maints prétextes pour les clercs à détailler les préceptes de leur Foi. Bien évidemment, le Saint Sépulcre contenait à lui seul suffisamment de riches décors et d’inscriptions d’or pour occuper les pensées d’un croyant pour de nombreuses années. Malgré tout, rien n’était accessible directement et il fallait chaque fois un intermédiaire, un lettré qui en dévoile le sens caché. Sans cela, on ne pouvait voir là que fioritures sans âmes, mosaïques orgueilleuses et peintures dispendieuses.

Ernaut comprenait désormais qu’il était difficile pour un simple fidèle de s’orienter dans son évolution spirituelle personnelle. Les cérémonies parfois ennuyeuses qu’il suivait depuis son enfance avaient pour but de célébrer le Seigneur et ses officiants avaient plus d’opportunité d’emprunter les bons chemins, de faire les choix judicieux. Les plus sincères ne se prétendaient pas supérieurs aux autres hommes. Ils avaient longuement médité sur la parole du Christ et savaient accoucher les âmes pour qu’il en sorte le meilleur. Il fallait simplement apprendre à se livrer à eux.

### Jérusalem, abords de Saint-Martin, tôt le matin du samedi 30 mars 1157

Lorsque le vent frais caressa son visage au sortir de sa pièce, Ernaut inspira l’air à pleins poumons. Le ciel était à la pluie et quelques traces au sol indiquaient que la nuit avait connu plusieurs ondées. Couché peu après avoir visité l’Arménien, le jeune homme se sentait en pleine forme. Il avait tenté de brosser au mieux sa tenue, soucieux de se présenter sous son meilleur jour pour la cérémonie à venir.

Il avait tout son temps, l’office du Feu Sacré ne commençant qu’au mitan du jour. Il était néanmoins prudent de se diriger vers le Saint-Sépulcre suffisamment tôt, étant donné l’afflux de pèlerins qui s’y rendrait. Beaucoup avaient certainement dormi aux abords pour s’assurer une place de choix. On ne savait pas encore si le roi Baudoin serait présent en personne à la célébration mais l’effervescence qui animait la communauté des fidèles était à la hauteur de l’événement.

En plus d’une poignée de cierges, Ernaut avait acheté une petite lanterne de terre, dans laquelle il pouvait fixer une bougie de cire. Elle lui servirait à récupérer une fraction de la lumière sainte de façon à la partager avec Lambert. Une nouvelle fois, celui-ci n’allait pas assister aux festivités et en était un peu déçu. Ernaut voulait donc tout de même apporter un peu de joie à son frère. Dans son sac il emportait outre cela une gourde de cuir et quelques morceaux de vieux pain au cas où la flamme se ferait désirer.

On lui avait conté qu’il fallait selon les années prier un bon moment avant que le miracle ne survienne, éventuellement jusqu’au matin suivant. Impatient d’accomplir son vœu, Ernaut n’en oubliait pas pour autant la mission qui lui avait été confiée et s’en faisait une idée certainement plus ambitieuse que ce qu’on attendait réellement de lui. Il ne savait pas si Eudes aurait le temps de faire une visite à la taverne, mais lui avait prévu d’y jeter un œil, sur le chemin du tombeau du Christ. Saïd connaissait l’endroit en question et avait expliqué au jeune Bourguignon comment s’y rendre.

Il lui fallait plonger dans le quartier au nord de la rue du Temple, dans la zone qui jouxtait à l’est l’ensemble monumental de Saint-Jean. À faible distance du grand hôpital, il était logique que la boutique en voie passer de nombreux serviteurs. Il était suffisamment près pour y boire rapidement avec quelques compagnons et pas trop pour ne pas risquer de s’y faire sermonner par un frère trop zélé. En outre, Ernaut voulait également prendre le temps de saluer Lambert et de discuter un peu avec lui avant de rejoindre le Saint-Sépulcre.

Les rues étaient emplies de monde, presque tous les commerces étant fermés. Seuls ceux qui proposaient de quoi recueillir le feu, de la nourriture ou quelques accessoires utiles aux fidèles demeuraient ouverts. Les familles avançaient, en groupe, souvent sous la houlette d’un patriarche qui commentait d’une voix forte à l’intention des plus jeunes ce à quoi ils allaient assister. Des bandes d’enfants couraient, excités par l’agitation régnant partout. Même les animaux semblaient connaître un regain d’activité, des meutes de chiens errants se faufilant régulièrement entre les jambes des passants. On entendait parler toutes les langues de la chrétienté et au-delà et, avec sa haute stature, Ernaut admirait le plus hétéroclite assemblage de couvre-chefs et de coiffures qu’il ait jamais vu : voiles, toques en pain de sucre, bonnets, capuches, turbans, chapeaux ourlés de fourrure, tonsure fraîches, nattes élaborées, chignons sévères…

Dans la rue du Temple, qui poursuivait celle de David en direction de l’est, coupant littéralement la ville en deux, la cohue était invraisemblable. On aurait cru que tous les peuples de la Terre s’étaient donné rendez-vous là. Les parfums se mêlaient à la sueur et à la poussière des derniers arrivants, les tenues bigarrées étaient arborées avec un sens consommé de la mise en scène. Le vacarme lui-même était assourdissant et Ernaut se dit qu’il n’était pas très grave que les cloches ne sonnent pas en ces jours. De toute façon, vu le bruit qui se répandait dans les rues, il n’aurait pas même entendu le tonnerre gronder au-dessus de leurs têtes. La presse était si forte qu’il fallait écarter les gens à chaque pas, comme si personne ne songeait à avancer, occupé à discuter avec ses voisins et amis, sa famille.

Pourtant, peu à peu, le flux se rapprochait indubitablement de sa destination, en direction du pôle qui les avait tous attirés là : le tombeau du Christ. Une fois parvenu dans les ruelles secondaires, il était plus facile de circuler, l’agitation devenait un vague brouhaha qui s’éteignait rapidement au fur et à mesure des détours. Le soleil perçait chichement, éclairant d’une lumière diffuse les venelles, sans pour autant réussir à en atténuer la fraicheur.

La taverne se révéla relativement large, s’ouvrant en une unique arcade sur la chaussée. Seul un côté des panneaux avait été démonté et il fallait descendre quelques marches avant de se retrouver sur le sol de terre battue. Une odeur de vieux raisin, de poussière et de moisi délicat réveilla en Ernaut une foule de souvenirs. Son père possédait des vignes à Vézelay et faisait commerce de son vin, de bonne qualité, jusqu’en des régions très éloignées. Enfant, les barriques, les tonneaux et même le pressoir, à quelques rues de là, avaient constitué autant d’éléments de jeu.

La pièce était voûtée, toute en longueur, avec des fûts sur la droite, dont plusieurs étaient en perce. Un haut meuble contenait de nombreux pichets et pots, servant pour les clients qui venaient sans leur récipient. Au fond du passage, une porte donnait sur une cour au sol grossièrement pavé. Là, un chat allongé battait de la queue tandis qu’un enfant lui grattait le ventre. Le petit appela son père, qui se présenta rapidement à Ernaut. Assez grand, un peu voûté, il devait avoir la trentaine, le cheveu sombre coupé court. Le visage maigre se fendait d’un large sourire, accentué par des yeux rieurs. Il semblait savourer à l’avance les plaisanteries qu’il ne manquerait pas de faire. Il émanait de sa personne une affabilité et une cordialité évidentes.

« Le bon jour, que vous faut-il, ami ? J’ai là plusieurs bonnes cuvées…

— Vous êtes ouvert ? Comme il n’y a personne, je n’ai pas volonté de vous déranger… Surtout en ce jour.

— Aucun souci. J’ai licence d’ouvrir ce jour, même si je ne peux le faire crier. Il faut bien que les voyageurs puissent boire. Mais il est vrai que beaucoup ont autre chose en tête en cette matinée. »

Il dévisagea Ernaut des pieds à la tête, sans s’en cacher, se grattant le front comme un galeux.

« Vous n’êtes pas au sépulcre ? Vous avez pourtant tout l’air du pérégrin venu pour cela.

— Je m’y rends. Il y a temps pour moi, on m’a dit qu’il fallait que le roi soit là pour que les huis s’ouvrent. Et ce ne sera pas avant la mi-journée. »

Le tavernier hocha la tête.

« De vrai, les chanoines n’ouvrent pas avant sixte. »

Il demeura silencieux quelques instants puis reprit, se tournant à demi vers ses fûts.

« Vous avez goûtance de découvrir mes vins ? J’ai plusieurs pièces qui ont bon succès, en provenance de Galilée, de Samarie, et même de Bezek, près de Bethléem.

— J’en serais aise, on m’a dit moult bien de votre commerce. C’est pourquoi j’ai eu envie de le voir en attendant l’office. Mais je préfère revenir en une future occasion pour déguster.

— Je comprends. Beaucoup font comme vous et c’est souvent le soir que la plupart viennent acheter. J’ai bonne clientèle du grand hôpital, les sergents qui ont achevé leur labeur aiment à boire après cela. Certains, plus tenus par leur épouse, envoient leurs enfançons quérir un pichet pour la veillée… »

Ernaut fit un large sourire, tout en embrassant le lieu du regard.

« C’est justement un mien compaing au service des frères qui m’a parlé de vous. Il tenait la nouvelle de Roussel, qui semble fort goûter l’endroit.

— Roussel ? Je ne vois mie. Il œuvre aussi à l’hôpital vous dites ?

— Il est peut-être de ceux dont la femme porte braies ! Rétorqua Ernaut, goguenard.

— Si fait, possible. Il faut dire que j’ai beaucoup de clients le soir, peut-être a-t-il eu affaire à mon valet… »

Ernaut hocha la tête en assentiment, comprenant qu’il n’apprendrait guère de chose pour l’instant. Il pouvait difficilement questionner plus avant le tavernier sans éveiller ses soupçons.

« Je repasserai prendre un pichet, histoire de taster cela.

— Oui-da, ce sera occasion pour vous peut-être de croiser votre compaing.

— Cette veillée, vous serez clôt ou pas ?

— Certes pas. Si nous avons bonne fortune, le Feu Sacré descendra pas trop tard alors beaucoup auront à cœur de fêter cela en famille. J’ai espoir de faire bon commerce à cette occasion. Mais je ne vais pas tarder à clore pour la journée, je me rends avec ma parentèle aux cérémonies, bien sûr. »

Le jeune homme acquiesça et remercia son interlocuteur avant de rebrousser chemin, sous le regard inquisiteur du chat, qui s’était approché en bondissant de barrique en barrique. Il salua son départ d’un miaulement plaintif.

En peu de temps, Ernaut se retrouva à nouveau au milieu d’une foule dense. Les abords du change syrien étaient complètement bloqués et pourtant nul ne paraissait se soucier de ne plus avancer. Il semblait quasi-impossible de venir aux abords de l’hôpital pour pouvoir y pénétrer. Ernaut soupira, il lui fallait faire tout le tour du quartier de façon à s’éloigner un peu du Saint-Sépulcre. Il tenait à tout prix à indiquer à Lambert qu’il verrait d’ici peu une chandelle allumée au feu sacré. Il espérait ainsi le réconforter quelque peu.

Il n’était plus inquiet pour lui, son état de santé s’étant récemment fortement amélioré, mais il demeurait fragile et devait s’ennuyer ferme dans son lit, sachant qu’il ratait tous les offices pour lesquels il avait si longtemps voyagé. Ernaut avait un peu l’impression de voler le rêve de Lambert, lui qui assistait à tout, sans y accorder l’importance ni la dévotion de son frère.

Voyant la foule amassée dans la rue des Palmes, il obliqua par la rue aux Herbes, vers le sud, pour rejoindre par la place et l’entrée méridionale, le grand bâtiment où Lambert était tenu. Même dans ces lieux un peu plus distants de l’église abritant le tombeau du Christ, il lui fallut forcer le passage presque à chaque enjambée tout au long de son parcours.

### Jérusalem, matin du samedi 30 mars 1157

L’entrée de la grande salle était barrée par deux valets de forte stature, l’air renfrogné. L’un d’eux tenait un balai qui aurait bien pu servir à d’autres usages si l’envie venait à un pèlerin de forcer le passage. Ils étaient là pour éviter qu’on ne prenne l’hôpital pour un accès au lieux saints, sa large porte débouchant directement sur le parvis. Inquiet de voir s’approcher un gaillard de la stature d’Ernaut, un des domestiques fronça les sourcils, comme si cela pouvait être efficace pour repousser un tel adversaire. L’homme aboya plus qu’il ne parla, peut-être pour se donner du courage.

« Halte là, pérégrin, le passage est clos, passez outre…

— Le bon jour, je viens ici à dessein. Un mien frère est tenu en vos couches, malade. J’ai espoir de le réconforter avant de me rendre à la messe du Feu Sacré. »

L’éventualité d’une réponse structurée n’était visiblement pas venue à l’idée du portier qui fit danser sa mâchoire tandis que ses lèvres cherchaient quelques mots à formuler. Il lui fallut un long moment avant de sortir un son compréhensible.

« En quelle rue qu’il est ?

— Ce doit être la cinquième ou sixième, je crois. Vous n’avez qu’à me joindre, et vous verrez que je n’ai nulle intention d’abuser de mon passage en ce lieu. »

Derrière Ernaut venait de se présenter plusieurs autres voyageurs qui avaient peut-être une intention similaire. Le valet fit un geste de la main à Ernaut, sans prendre la peine d’ouvrir la bouche pour lui indiquer d’entrer. Puis il se replongea dans une intense activité intellectuelle pour se préparer à formuler une nouvelle question aux derniers arrivants.

L’endroit semblait se tenir à l’écart de l’agitation de la ville, possédant son rythme personnel. Les domestiques allaient et venaient, portant comme à leur habitude de quoi nettoyer, des draps propres, des vêtements, ou aidant un malade à prendre place. Certains, l’air grave, emportaient sur un brancard un malheureux qui avait rendu son âme à Dieu. Les visites des médecins se faisaient toujours dans un ballet de serviteurs, allant et venant, les bras chargés de remèdes, de serviettes, de seaux, sous les indications précises et impérieuses des doctes savants.

Passant devant la table où le frère hospitalier était, comme chaque fois, fort occupé par ses documents et la supervision de l’endroit, Ernaut salua ce dernier d’un hochement de tête. Le clerc lui répondit d’un geste de la main puis se figea, comme s’il allait lui dire quelque chose, avant finalement de se replonger dans le déchiffrage d’une tablette de cire annotée. Ce qu’Ernaut ne vit pas, c’est qu’il eut le temps de faire un signe discret à un des domestiques, à qui il donna quelques brèves indications. Le valet disparut derrière une des nombreuses portes annexes.

Lambert était allongé sur le dos, des compresses de lin sur les yeux. Ernaut s’approcha et lui toucha le bras en le saluant.

« Que t’arrive-t-il ? Voilà nouvel remède ?

— Depuis quelques jours, ils se contentent de me verser quelques gouttes dans les yeux plusieurs fois par jour. Le médecin a dit qu’il s’agissait de garum nabatéen[^garum].

— Est-ce là bon signe ?

— Je le crois, oui. On m’a confirmé que je devrai pouvoir sortir d’ici peu. Juste après les célébrations de Pâques en fait. »

Lambert fit une moue, marquant bien sa déception de recouvrer la santé quelques jours trop tard. Ernaut allait s’assoir et lui répondre quand un valet s’approcha et l’interrompit dans son geste : un des frères souhaitait le voir. Le jeune homme hocha la tête, prit le temps d’expliquer à Lambert qu’il lui apporterait un peu du feu sacré lorsqu’il sortirait, demain au plus tard, et que ce serait une des prochaines choses sur lesquelles il pourrait porter le regard.

L’intention arracha un sourire à Lambert qui le congédia amicalement, conscient qu’on l’attendait. Après avoir traversé quelques pièces et emprunté plusieurs escaliers, Ernaut fut mené dans une grande salle, largement illuminée par un ensemble de fenêtres aux montants sculptés.

Les parois, couvertes de décors peints, étaient par endroits cachées de lourdes tentures, capables de dissimuler des passages. Plusieurs braseros éteints étaient placés le long des murs, et de nombreux bancs, ainsi que plusieurs chaises curiales y étaient installées.

Une estrade, sur laquelle on ne voyait aucun meuble, occupait tout le fond de l’endroit. Sur un des sièges, un gros homme était assis, habillé d’une tenue de sobre bure, de couleur indéfinissable. Il portait par dessus le manteau noir à croix blanche de l’hôpital. À ses côtés attendait frère Matthieu qui salua Ernaut d’un geste du bras quand il l’aperçut. Ce dernier s’approcha et s’inclina devant les deux clercs. Matthieu déploya sa longue carcasse, invitant de la main Ernaut à prendre place sur le banc.

« Je suis fort aise de te voir ici, jeune ami. Voilà le père Thomas, qui aurait notables éléments à te confier. »

Le clerc se leva et salua de façon cérémonieuse, dévoilant sa large tonsure. Comme nombre de prêtres à l’approche de Pâques, elle avait été refaite récemment et il n’arborait qu’une discrète couronne de cheveux bruns autour de son crâne. Son visage fin aurait pu laisser croire à une faiblesse de caractère que démentait la vigueur dans le regard. De petite taille, il bougeait avec grâce malgré son embonpoint. Sa voix, assez haut perchée, surprit Ernaut lorsqu’il prit la parole, presque dans un murmure.

« J’ai appris que vous êtes sur les traces du démon qui s’en prend aux pérégrins. Il me faut vous entretenir à ce sujet. »

Frère Matthieu salua et laissa les deux hommes seuls, s’éloignant vers les fenêtres afin de ne pas se montrer importun tout en gardant un œil sur ce qui se passait.

Le père Thomas dévisagea Ernaut quelques instants, puis lui adressa un timide sourire, comme s’il cherchait à instaurer entre eux une certaine intimité. Il se pencha vers son interlocuteur, de façon à lui parler d’un chuchotement.

« Je ne peux entrer dans les détails, car certains faits ne m’ont pas été rapportés à moi directement, mais à Dieu lors d’une confession.

— Je suis tout ouïe, mon père.

— J’ai grand peur que le mécréant que vous recherchez ne s’appelle Maciot, un pauvre hère que nous avions recueilli voilà quelques mois. »

Ernaut secoua la tête en dénégation, s’attirant un regard étonné du prêtre. Il se sentit obligé d’apporter une explication à son démenti.

« Nous avons preuve qu’il s’agit d’un nommé Roussel…

— En êtes-vous acertainé ? Je sais que Maciot avait connoissance des deux femmes, et des leurs, et qu’il a fait terrible chose…

— À quoi ressemble-t-il votre Maciot ? Peut-être sont-ils deux à frapper.

— Il était fort fatigué quand je l’ai vu, lorsque nous l’avons recueilli. Donc plutôt malingre, guère plus grand que moi. Le poil roux et les yeux clairs, torves. Il délirait à demi quand je l’ai entendu, nous craignions pour sa vie.

— Roux de poil, les yeux torts ? Ce pourrait être le même que ce Roussel alors, ou son frère.

— Maciot n’en avait pas, je peux vous en assurer. Du moins aucun qui soit parvenu jusqu’à nous. »

Thomas se mordilla les lèvres, cherchant clairement à faire le tri entre ce qu’il se pensait autorisé à divulguer et le reste.

« Ce pauvre hère avait été pris par nos ennemis un temps. Il y avait été bien mal traité. Son corps n’était que plaies et bosses, lacérations de fouet et mauvais traitements.

— Cela se pourrait donc qu’il y ait là quelques liens, en effet. Mon Roussel a connu aussi païennes geôles, ainsi que les pérégrines murdries. »

Le prêtre remua la tête, oscillant entre l’adhésion et le désaccord.

« Il y en a tant qui passent en ces terribles lieux ! Nous en accueillons si souvent…

— Je vous entends, mais pourquoi ce Maciot serait-il le murdrier selon vous ?

— On m’a dit les noms des deux pauvresses tombées sous ses coups. Ce pauvre Maciot avait cité ces noms, je m’en souviens fort bien, ainsi que Nirart et Oudinnet. Il doit craindre pour sa sauveté, je pense. Sa mécréance m’avait fort choqué à l’époque, bien que je ne puisse dire le pourquoi. »

Ernaut hocha la tête, à demi convaincu. Il demeurait un autre nom, qui n’aurait pu être cité par hasard, ou n’être que le résultat d’une captivité commune.

« A-t-il parlé de son épouse, Asceline ? »

Le prêtre fit une grimace, clairement troublé par la question.

« Je ne peux répondre en détail. Il a cité ce nom, certes. Mais je ne dirai pas plus. »

Le jeune homme souffla, un peu irrité de cette révélation qui n’en était pas une.

« Ne répondre qu’à demi ainsi lui permettra peut-être d’échapper à la justice royale, vous savez.

— Bien sûr que j’en ai conscience, je prie régulièrement à cause de cela. Il ne pourra échapper à la justice divine…

— S’il est venu à confesse, il n’a plus guère d’inquiétude à avoir de ce côté-là ! » rétorqua Ernaut d’une voix un peu agacé, se redressant.

Le prêtre se figea et ses yeux s’emportèrent un instant, dévoilant le conflit qui l’habitait et la colère sous-jacente que la réflexion d’Ernaut avait éveillée.

« Sachez que je n’ai pu l’absoudre, car il refusait de se rependre. Outre, il me semblait qu’il ne disait tout et que son forfait était bien pire.

— Comment cela a donc fini ?

— Je lui ai dit que je ne pouvais rien faire pour lui s’il n’ouvrait pas son cœur en entier. J’ai proposé de l’écouter s’il souhaitait s’épancher plus avant, que c’était nécessaire pour être lavé de ses pêchés. Il s’est alors tu, de façon définitive.

— Vous n’avez rien fait pour le faire punir ? »

La remarque attisa une nouvelle fois l’indignation de l’hospitalier et ce fut d’une voix sèche qu’il objecta.

« Il ne m’appartient pas de juger ou condamner, jeune homme. Je suis là pour soigner les âmes. J’avoue tristement avoir failli en ce cas. Je crains fort que le démon qui se logeait en sien coeur ne soit revenu, plus vaillant que jamais. J’ai grand peur que son âme ne soit en fort péril. »

Le jeune homme soupira, estimant que le prêtre cherchait à s’en tirer à bon compte, alors qu’il aurait pu empêcher tout cela.

« N’auriez-vous pas pu faire en sorte qu’on l’interroge, qu’on l’entrave, au moins pour éviter qu’il ne recommence ?

— Son forfait ne s’était pas déroulé ici, mais fort loin, et nul n’allait en demander réparation. Quant à prédire ce qui allait arriver, je n’ai certes pas cette prétention. »

Thomas marqua un temps, fixant soudain le mur, ressassant ses pensées, visiblement accablé de ce qu’il savait et de son impuissance à avoir traité pareil problème.

« N’oubliez pas qu’il existe toujours espoir que l’homme s’amende.

— Eh bien si ce maudit égorgeur n’est rapidement pris, vous devrez vivre avec le fait que par votre faute, un enfant a été meurdri. »

Le clerc se renfrogna, plus touché par les arguments qu’il ne voudrait jamais l’admettre.

« Il est toujours dangereux de se prétendre juge, garçon. Cela n’appartient qu’à Dieu de sonder les âmes, et de savoir ce dont elles sont capables. Le pire n’est jamais certain tant qu’il restera une parcelle de lumière en chacun de nous. »

Ernaut se leva, contrarié.

« Vous ne pouvez guère m’en dire et je ne pense pas que cela suffira à soulager votre âme, mon père. Si ce Maciot est bien le Roussel que je piste, alors par votre faute, des innocents ont péri. Ne comptez pas sur moi pour vous affranchir de votre dette envers leurs âmes. »

Le jeune homme repartit en direction de la porte, saluant d’un signe de tête contrarié frère Matthieu qui ne s’expliquait pas l’animosité qu’il avait vue naître entre les deux interlocuteurs. Il jugea bon d’intervenir, se plaçant sur le trajet d’Ernaut vers la sortie.

« N’y a-t-il pas quelques nouveltés en ce récit ? Frère Thomas était pourtant fort désireux de se confier à vous…

— Alors, qu’il apprenne que je n’ai pas pouvoir d’absoudre et qu’il devra voir directement avec le Créateur ! »

Éberlué par la violence de la réponse, le grand hospitalier en eut le souffle coupé. Il demeura néanmoins en travers du passage, cherchant ses mots. Le jeune homme, voyant son trouble, se radoucit quelque peu.

« Si fait, il m’a dévoilé intéressantes choses, mais aussi bien tristes. Ces pauvresses innocentes ont peut-être péri par sa faute. Il savait et n’a rien fait. »

Le visage du frère se décomposa et ses commissures de lèvres s’affaissèrent, répandant devant lui une odeur fétide qui fit reculer Ernaut. Jetant un regard affolé vers le prêtre, toujours assis sur son banc, on aurait dit qu’il suffoquait. La bouche sèche, il trouva néanmoins la force de glisser dans un souffle, balbutiant :

« Quel blasphème ! Quel sacrilège ! Comment osez-vous dire cela ? »

Comprenant qu’il était allé trop loin, Ernaut baissa la tête, cherchant à se calmer. Il n’avait pas réfléchi à la portée de ses propos et s’en voulait. Le confesseur avait tenté d’apaiser, de secourir et s’était trouvé devant un choix moral difficile. Fidèle à son engagement, il s’en était remis à Dieu. Qui lui avait répondu de bien étrange façon selon le jeune homme…

« Pardonnez mon ire, frère. C’est juste que la nouvelle m’a fort troublé.

— N’allez pas répandre semblable idée, garçon ! D’aucuns pourraient bien y voir occasion de médire des frères de Saint-Jean.

— C’est bien la dernière chose qui me viendrait à l’idée, j’en fais serment. »

Le visage bas, Ernaut adoptait une attitude honteuse, pourtant dans ses yeux continuait à briller un éclat de colère. D’un geste, Matthieu confia Ernaut à un domestique qui patientait près de la porte. Tandis que leur visiteur s’engouffrait par l’ouverture, le père Thomas sourit tristement à Matthieu qui l’interrogeait du regard, encore stupéfait par les propos qu’il avait entendus.

« Voilà jeune homme plein de détermination et de courage, mais fort bien naïf des trames du Malin. Il croit aisé d’en discerner le cheminement en un cœur. Puisse le Seigneur ne pas lui apprendre la leçon trop durement.

— Amen ! » conclut frère Matthieu, le visage troublé.

### Milieu de matinée du samedi 30 mars 1157

Lorsqu’il ressortit de l’hôpital, Ernaut était de mauvaise humeur. Il ne comprenait pas les raisons qui avaient poussé le prêtre à se taire. Il lui semblait que la meilleure des décisions quand on se trouvait face à un dangereux individu, c’était de s’assurer qu’il ne recommence pas ses forfaits. Espérer en l’amélioration de son prochain relevait d’une naïveté qu’Ernaut n’avait jamais eue, du moins dans son souvenir. Il fallait que les frères de Saint-Jean soient bien candides pour croire en de pareilles fadaises !

Tournant et retournant les arguments dans tous les sens, il n’en arrivait qu’à s’agacer encore plus. Du coup, il cheminait sans ménagement droit devant lui, en direction du parvis et des flammes qu’il y voyait, propulsant plus que bousculant les personnes qui entravaient son avancée, étouffant d’un regard noir toute tentative de rébellion. Il réussit donc à rejoindre aisément le petit groupe de sa connaissance, autour du père Ligier, de Gobert, de Sanson et sa femme et surtout de Libourc.

Tout le monde était assemblé aux abords du feu où des serviteurs du Saint-Sépulcre jetaient de modestes croix de bois. Il salua les présents avec le plus de chaleur qu’il le put, mais chacun sentit bien qu’il était contrarié. Seul Sanson osa l’apostropher, une note de gaieté un peu forcée dans la voix :

« Adoncques, jeune Ernaut, que t’arrive-t-il de si sombre en cette belle journée ? Il y a occasion de fort se réjouir, ce me semble. C’était hier qu’il fallait se lamenter.

— Faites excuses de ma mauvaise humeur, maître Sanson, elle n’a nul rapport avec vous ou ce jour. Je viens d’ouïr nouvelles qui m’échauffent le sang. »

Sanson baissa le ton et se rapprocha.

« Aurais-tu trouvé bonne piste pour le démon qui a frappé récemment ?

— Non, et ce qui me chagrine, c’est qu’il était à portée de main de certains qui l’ont laissé filer, avec force bénédiction. »

Le vieil homme comprit que continuer sur ce sujet ne ferait qu’accroître la fureur de l’adolescent et garda un silence prudent. Chacun se contentait donc de fixer devant lui, regardant les valets gérer le brasier sans vraiment les voir. Finalement, donnant un coup de coude dans les côtes du géant, Sanson indiqua d’un coup de menton le spectacle

« Bien étrange de brûler des croix en ce jour, non ? »

Ernaut opina du menton, affichant plus d’intérêt qu’il n’en avait réellement. Il ne voulait pas se montrer impoli avec le vieil homme. Non seulement il avait pour lui une certaine estime, mais il savait qu’il ne serait guère sage de se l’aliéner, dans l’espoir qu’il entretenait de resserrer ses liens avec sa famille. À leurs côtés, le père Ligier intervint, d’une voix forte, pour répondre à cette question qu’un grand nombre de ses fidèles devaient se poser.

« Il n’est plus temps pour nous de penser à la sainte Croix en ce jour, c’est le jour où nous allons faire réjouissance de ce que Christ a triomphé de celle-ci et de la mort…

— Et pis faut bien faire la place pour les croix des aut’ pérégrins ajouta Gobert, un sourire de bon sens marqué sur la figure. Sinon la chapelle du Calvaire serait tôt pleine !

— En ce cas il devraient aussi arder les flèches qu’on y voit. Je ne me souviens pas pourquoi elles sont là. Nul Romain n’a décoché sur le Christ ! » Lui répondit un de ses compagnons.

Gobert pouffa, mimant rapidement un crucifié recevant des flèches. Le père Ligier n’eut pas le temps de le rabrouer qu’il cessa et lança à la cantonade :

« Ils préfèrent les garder en souvenance des mauvaises actions des frères de Saint-Jean !

— Pourquoi dis-tu ça ? Releva Sanson, les sourcils froncés.

— Ce sont des traits décochés voilà peu, lorsque les frères ont carrément assailli le Sépulcre. »

Le vieil homme ferma à demi les yeux, intrigué par cette affirmation.

« Je ne te crois point ! En pleine Cité sainte ? Pourquoi se seraient-ils battus ?

— Demande à qui tu veux ! Riposta Gobert, sarcastique. Je n’en sais fichtre rien, il est juste acertainé que ce sont là vestiges de récente bataille et non pas relique des temps anciens. »

Un pèlerin d’un groupe voisin, dont on ne voyait que l’imposant nez au milieu du visage émacié, interpella les deux hommes :

« On m’a narré que les hommes des chanoines avaient tenté de faire taire la cloche des frères de Saint-Jean, pour éviter qu’ils la sonnent pour couvrir les sermons du Patriarche.

— N’était-ce pas le vieux Foucher lui-même qui avait commencé les hostilités en les invectivant depuis les marches, là ? Répondit Pons de Mello, désignant l’escalier qui montait à droite de la façade.

— Bah, il avait ses raisons, de sûr. Un saint homme comme lui… »

Le père Ligier fit un geste de la main pour apaiser ses fidèles, interrompant la discussion.

« Mes frères, mes frères ! Il n’y a nul bienfait à évoquer cela en ce jour. Il ne nous appartient pas de répandre si honteux commérages et certes pas maintenant. »

L’assemblée adopta rapidement une attitude de contrition qui le satisfit et il continua en lissant d’une main sa longue barbe, tourné vers Gobert.

« Si les serviteurs brûlent les croix, c’est aussi pour marquer la victoire de la lumière sur les ténèbres, sur le symbole de la mort de Notre Seigneur. Nul besoin de railler avec tes sarcasmes.

— Amen ! » Fit le manchot, mi-figue mi-raisin, en se signant furtivement.

Le soleil était désormais de la partie, assez haut dans le ciel pour apporter un peu de clarté sur la place, frappant la façade du Saint-Sépulcre. L’impressionnante majesté du bâtiment écrasait de toute sa hauteur les fidèles assemblés là, ondoyant de temps à autre selon les mouvements de quelques groupes en son sein. On attendait le roi Baudoin qui avait fait annoncer sa présence.

De nombreux pèlerins priaient, à voix basse ou à plusieurs, de façon plus voyante, mais la plupart se contentaient de patienter en silence. Ernaut était de ceux-là, le visage tourné vers le beffroi, dont les pierres récemment maçonnées luisaient comme du miel sous un ciel où, au final, le bleu rongeait implacablement le moindre lambeau de nuage. Quelques pigeons voletaient aux abords, dévisageant, intrigués par l’amas à leurs pieds, faisant le guet depuis les moulures ornant la façade.

Alors qu’il était perdu dans ses pensées, le jeune homme sentit qu’on lui tirait sur le vêtement, et il se surprit à espérer que ce soit Libourc. Mais quand il tourna la tête, ce fut pour voir le vieux visage ridé de Hersant de Bondies, une inquiétude voilant son air habituellement affable. Elle lui fit signe d’un geste de se pencher vers elle.

« Je n’ai pu m’empêcher d’entendre ce que tu as déclaré tantôt, jeune Ernaut. Le péril est donc fort grave pour ce malheureux enfançon…

— Certes oui, je le crains confirma Ernaut, le visage fermé.

— En ce cas, veille à ne point trop t’éloigner de moi. Ainsi, si je l’aperçois, je pourrais te le désigner. Avec ta stature d’ours, tu auras tôt fait de parvenir à lui. »

Ernaut acquiesça, souriant par politesse. L’idée n’était pas mauvaise, bien qu’excessivement simple. D’ailleurs, parfois, c’était celles qui fonctionnaient le mieux. Tandis qu’il échangeait avec la vieille femme, il crut apercevoir subrepticement sur sa droite le visage de Libourc scrutant dans sa direction, l’air avenant. Il n’osa néanmoins pas se tourner, craignant de se dévoiler trop ouvertement. S’accordant un instant pour retrouver le cours de ses pensées, il reprit :

« Auriez-vous souvenance d’un quelconque Maciot ou Roussel ?

— Nenni, pas récemment. J’ai bien connu un Roussel, un affreux valet qui soignait les porcs en mes jouvences, je ne crois pas que cela soit celui qui t’intéresse. »

La vieille dame offrit un sourire largement édenté à Ernaut, vite effacé devant l’air grave qu’elle obtint en retour.

« Est-ce là si sombre nom ?

— Peut-être. Sachez qu’il peut y avoir péril pour d’autres, s’ils ont eu connoissance de cet homme. Veillez donc à me faire savoir si vous en entendez parler. »

La vieille femme opina en silence, avant d’ajouter :

« Il serait bon que tu demeures aussi en mes côtés pour la grand messe de demain. Si jamais nous ne voyons Oudinnet en ce jour, cela se peut demain…

— Tu n’as qu’à te joindre à nous pour la veillée, Ernaut, ajouta Sanson, qui avait suivi l’échange. Il est fort fréquent que le Feu Sacré ne se voie guère avant la nuit. Nous pourrons demeurer pour un rapide souper après. Ainsi tu auras encore plus de chance de voir l’enfançon. »

Le jeune homme se retourna vers le vieillard, un sourire joignant alors une oreille à l’autre. Il n’ajouta rien à ce qui venait d’être dit, se contentant de hocher de la tête en assentiment. Ce faisant, il pouvait enfin admirer clairement tout son soûl le doux visage qu’il aimait tant voir. L’apaisement gagna son cœur et toute trace de colère disparut progressivement de ses traits. Il était ravi de découvrir que les deux yeux qui le fixaient ne se montraient guère rétifs.

Soudain il y eut comme une vague qui se propagea dans la foule : les portes venaient de s’ouvrir, laissant la marée humaine déferler à l’intérieur de l’édifice saint. Malgré toute son énergie et sa force, Ernaut fut emporté par l’irrépressible mouvement. Il trouva juste assez de ressource pour se maintenir à proximité de Libourc pendant toute la bousculade, profitant de la ruée pour échanger avec elle de fréquents regards tandis qu’ils étaient remués comme par une houle puissante.

C’était également pour le jeune homme l’opportunité de devoir souvent suivre la cohue qui le pressait vers elle. Acceptant ses excuses muettes à chaque fois d’un sourire charmant, Libourc ne semblait pas en être peinée outre mesure. Cela n’était certes pas le cas de sa mère, dont le visage se fermait un peu plus alors qu’elle assistait au tête-à-tête. Elle n’avait nullement la capacité de s’opposer à la foule qui les manœuvrait et ne pouvait s’interposer entre les deux adolescents, qui ne faisaient d’ailleurs rien de véritablement répréhensible.

Mais elle savait, aussi sûrement que si elle les entendait s’exprimer à haute voix, ce qu’ils se répétaient l’un à l’autre. Finalement, bien que fort mécontente, elle abandonna, préférant se préparer à l’office qui s’annonçait, décidée à l’avenir à ne pas abdiquer sans avoir farouchement combattu.

### Après-midi du samedi 30 mars 1157

Après des oraisons et des lectures auprès du maître-autel, la célébration comportait un certain nombre de processions autour de la tombe du Christ, suivant un petit groupe de croyants sélectionnés, parmi lesquels se trouvait un porteur pour la Croix. Les pieds nus, précédés de candélabres et d’encensoirs, ils imploraient le Seigneur, entourés de chanoines. À chaque tour, la tête du cortège faisait une station solennelle devant l’entrée de l’édicule, et tous s’agenouillaient dévotement, bientôt imités par la foule présente dans l’église. Régulièrement, un ecclésiastique s’approchait des portes scellées du sépulcre, espérant y découvrir le Feu sacré envoyé par le Très-Haut. Jusqu’alors leurs attentes avaient été chaque fois déçues.

Le groupe d’Ernaut avait été dilué peu à peu par les déplacements et le jeune homme se trouvait désormais en périphérie, à la limite de la galerie circulaire qui entourait le tombeau. Tandis que la procession tournait, les fidèles avaient rejoint le chœur des chanoines qui avaient lancé le Kyrie. La pureté du chant originel des clercs était devenue un grondement sourd, qui montait et descendait sans être véritablement intelligible. Toutefois, il rendait bien compte de la ferveur qui s’emparait des esprits. Certains finissaient par se contenter de psalmodier, criant de temps à autre quelque phrase issue d’un mélange de leurs suppliques et du latin ou du grec qu’ils avaient compris.

Hersant se trouvait toujours à proximité du jeune homme, mais n’était guère encline à lui indiquer l'enfant. Elle était plongée dans une sorte de transe extatique, se laissant tomber régulièrement les genoux au sol, les mains jointes ou tendues vers la coupole, implorant le Seigneur de sa voix chevrotante. Ernaut s’efforçait de la protéger lorsque le flux des fidèles les poussait, accompagnant l’avancée des processionnaires. Cela faisait déjà plusieurs fois qu’ils faisaient le tour de la rotonde, coincés contre les colonnes et les piliers. À chaque fois qu’ils arrivaient près de la nef, il fallait véritablement à Ernaut s’arc-bouter pour ne pas être éjecté de la marche, une foule de pèlerins tentant de s’infiltrer par les abords.

Il était néanmoins aidé en cela par la présence de solides sergents qui faisaient bloc autour de la personne du roi et des principaux ecclésiastiques, assemblés aux abords de l’omphalos[^omphalos]. Lorsqu’il levait la tête, il apercevait de nombreux croyants à la galerie supérieure, parmi lesquels on remarquait de riches tenues, et qui assistaient à la cérémonie avec plus de calme. Régulièrement, il jetait un coup d’œil là-haut, espérant y voir apparaître le visage familier de Régnier d’Eaucourt.

De temps à autre, il était emporté par l’enthousiasme des pèlerins assemblés, et ajoutait son chant à celui des présents, mais il demeurait la plupart du temps comme un guetteur attentif. Il observait l’assistance dans l’espoir d’y discerner un roux au regard torve. Et, parfois, il posait les yeux sans vraiment y penser sur une tête brune, ornée de longues nattes. Cela n’était généralement que très furtif, la jeune fille étant visiblement sincèrement absorbée par la cérémonie, joignant souvent le geste à la parole tandis qu’ils tournaient en rond autour du saint édicule.

Profitant d’un moment de calme pendant qu’une nouvelle tentative était faite dans l’espoir que le feu serait là, Ernaut risqua un coup d’œil plus attentif vers la galerie extérieure qui entourait la rotonde. Il aperçut alors un enfant suivi d’un homme dont la chevelure claire tirait apparemment sur le roux. Sans éclairage dans l’édifice, il demeurait difficile de l’établir précisément. Ils se dirigeaient vers la chapelle septentrionale, Sainte-Marie, d’où on pouvait sortir vers la rue du Patriarche par une longue volée d’escaliers.

Le sang d’Ernaut se mit instantanément à bouillonner: il lui semblait qu’on tentait d’emporter le petit Oudinnet à l’écart, où il serait plus aisé de lui ôter la vie. Sans plus réfléchir, il s’élança, poussant les gens autour de lui comme si ce n’étaient que des branchages qui gênaient la progression dans un sous-bois. Son mouvement entraîna de nombreux reproches et des réprimandes sévères, car il dut faire tout le tour du tombeau. Il eut tout de même la présence d’esprit de passer par l’ouest, afin de ne pas risquer de provoquer une catastrophe, en bousculant des membres de la procession.

Jouant des mains et des coudes, il cheminait rapidement, les yeux braqués sur l’homme et l’enfant. Il était désormais certain que l’adulte tenait le plus jeune par le bras et le menait de façon autoritaire. La vie d’un innocent était en grand péril, incitant Ernaut à redoubler de vigueur. Il manqua de renverser de nombreuses personnes et escalada en partie la base d’une colonne, sous les regards désapprobateurs de la foule environnante. Néanmoins, son sans-gêne lui permit de provoquer un mouvement de recul à son approche, ce qui lui facilita l’avancée.

Il n’était plus qu’à quelques pas lorsque l’homme, réalisant soudain qu’il était la cible d’un taureau furieux, se retourna. Il ne semblait pas loucher, mais avait un visage sévère, et on ne pouvait dire ce qu’il avait en tête. Il tenait d’une forte poigne un petit garçon de cinq ou six ans. Voyant Ernaut arriver dans sa direction, il se recula instinctivement afin de lui laisser le passage.

L’adolescent s’arrêta net, le dévisageant de toute sa hauteur, prêt à réagir à tout mouvement qu’il estimerait suspect.

« Alors, ami, que faites-vous avec cet enfançon ? »

L’homme ne parut pas comprendre, ce qui obligea Ernaut à se répéter, le visage commençant à s’empourprer de colère. Cela ne déclencha qu’une hébétude plus grande dans les yeux de son interlocuteur.

« Quoi ? Que voulez-vous ?

— Non, que voulez-vous, vous ? Lâchez donc son poignet, ou il va vous en cuire ! » Intima Ernaut, d’une voix qu’il s’efforçait de contenir, malgré la tension qui l’habitait.

Craignant de recevoir quelque mauvais coup d’un homme qui paraissait si déterminé, et qui devait faire le double de son poids, le suspect obtempéra, s’attirant un regard curieux de l’enfant, qui lui lança :

« Père, j’ai vraiment envie… »

Interloqué, Ernaut se pencha vers l’enfant, qui se recroquevilla de crainte, inquiet de recevoir également quelque mauvais coup. D’une voix fluette, il répéta sans conviction ce qu’il venait de demander. Le doute commençait à s’instiller dans le cerveau du jeune Bourguignon.

« Est-ce là… un vôtre marmouset ?

— Si fait, confirma l’homme.

— Pourquoi le menez-vous de pareille façon en dehors de l’église en plein office ? »

L’homme adopta une mine grise et un air troublé.

« Il n’est pas bien et a besoin de soulager ses entrailles, il me faut bien le mener ! Je n’ai guère envie de rater l’apparition du feu sacré, mais il est hors de question que je laisse mon fils seul avec le démon qui hante les rues de la cité. »

Ernaut renifla, tout en jetant un coup d’œil à l’enfant. Il n’était visiblement pas à l’aise, la paume sur le ventre, le regard anxieux. Il semblait avoir bien plus peur du géant qui l’avait interpelé que de l’homme qui l’accompagnait.

Dépité, Ernaut les laissa partir d’un geste de la main. Il échappa un soupir et tenta de faire machine arrière. Il comprit bien vite qu’il serait difficile pour lui de réussir l’opération, le vide tracé par son avance ayant été rapidement comblé par des pèlerins désireux de mieux suivre ce qui se passait. Une clameur se propageait, depuis le centre de la rotonde, car on venait de voir un embrasement dans le tombeau. Le porteur de la croix ressortit alors, un énorme cierge dans les mains, brillant d’un fort éclat dans l’édifice où l’obscurité commençait à gagner.

Tandis que la chandelle était acheminée vers le patriarche et le roi, chacun se dépêcha de fouiller dans son sac et de brandir de quoi recueillir à son tour un peu de la lumière qu’ils avaient tous longuement espéré. Ernaut lui-même se hâta de préparer deux bougies. Le temps qu’il soit prêt, la vague de lueurs vacillantes s’était rapprochée, chacun faisant profiter ses voisins de sa flamme. Alors, d’une voix puissante commença à monter un *Te Deum* vibrant de joie. La difficulté du morceau et l’empressement des croyants à se consacrer à la diffusion du Feu sacré permettaient au chant des religieux expérimentés d’emplir tout l’espace, sanctifiant la propagation de la lumière divine.

Un long moment, la ferveur de la foule absorba Ernaut, effaçant les soucis qui l’obnubilaient un instant plus tôt. Il se contentait de fixer avec joie la flammèche qui vacillait dans sa main. Lorsque le silence revint, chacun pouvait désormais brandir son cierge illuminé, remplissant l’édifice, jusqu’alors assez obscur depuis le Jeudi Saint, d’un impressionnant flamboiement. Une bénédiction fut lancée, pour ce feu descendu du Ciel, dorénavant partagé entre tous les fidèles présents.

Ernaut eut à peine le temps d’entendre des explications d’un clerc voisin qu’à nouveau un chant montait depuis le rang des chanoines. Cette fois beaucoup de pèlerins commentaient à mi-voix l’événement, plus intéressés à se montrer avec joie la flammèche vacillante qu’ils tenaient. Lorsqu’un cortège de religieux se forma et prit la direction du maître-autel, le relâchement fut encore plus perceptible. Il allait s’ensuivre un office, avec des stations auprès des fonts baptismaux. Mais de nombreux autochtones préféraient célébrer la messe dans leur paroisse avec leur propre clerc ou entre amis.

Perdu au milieu de la foule, Ernaut chercha le groupe qu’il avait quitté, se laissant entraîner par la file qui se rendait vers le parvis. Il prit néanmoins le temps d’allumer la petite lanterne qu’il avait achetée, soucieux de ne pas voir s’éteindre la lumière qu’il avait espérée tout l’après-midi. Il hésitait encore entre assister à la messe au complet et porter au plus vite ce cadeau à son frère.

Lorsqu’il se trouva près de la porte, il réalisa que le jour s’achevait et qu’il était donc bien plus tard qu’il le croyait. Du coup, il se hâta, escomptant que l’hôpital demeurerait accessible, étant donné le caractère particulier de cette journée. Chemin faisant, il aperçut Gobert et Sanson en grande discussion et fit un détour pour s’approcher d’eux. Le plus âgé sourit à Ernaut quand il le vit s’avancer.

« Fort belle cérémonie, n’est-ce pas, jeune Ernaut ? Je suis fort aise de l’avoir vue de mes yeux… »

Ne trouvant rien à répondre, Ernaut demeura silencieux, un sourire sur les lèvres, sa lanterne à la main.

« Nous allons encore ce soir nous rassembler pour veillée de prière, si tu veux te joindre à nous comme je te l’ai proposé. Le père Ligier sera là, il a entrepris de nous expliquer quelque peu ce qui se passera demain et ce que nous avons pu admirer ce jour.

— Ce sera avec grand joie, maître Sanson. Il me faut juste retrouver mon frère Lambert, qu’il puisse également prier devant la sainte Lumière que j’ai pu lui prendre.

— Charitable pensée ! Nous n’aurons pas temps de sortir de la ville pour aller près les oliviers comme l’autre soir, alors nous demeurerons près de la porte Saint-Étienne, où se trouvent quelques jardins.

— Je vous y retrouverai aussitôt quelques prières partagées avec mon frère, soyez-en remercié ! »

Le vieil homme hocha de la tête, satisfait, puis s’accrocha à Gobert, qui l’aidait visiblement à marcher. L’interminable attente avait éprouvé Sanson, mais il semblait pourtant rayonner d’un certain bonheur. Il avait pratiquement accompli son vœu, assisté aux cérémonies qu’il espérait depuis de longs mois de voyage et de privation. La moindre parcelle d’énergie était mobilisée pour aller jusqu’au bout de ces quelques jours. En le regardant s’éloigner, Ernaut sentit une bouffée d’affection le gagner, au souvenir de son propre père, également âgé, demeuré à Vézelay.

### Soirée du samedi 30 mars 1157

Dès son entrée, Ernaut remarqua que l’hôpital accueillait une activité inhabituelle : de nombreux pèlerins avaient apporté un cierge à un ami, un frère ou un fils, pour leur permettre de s’associer à l’importante célébration qui venait de se dérouler. On aurait dit la grande salle d’un riche seigneur apprêtée pour un banquet, avec toutes ces chandelles. Aucune n’était de taille imposante, mais leur foisonnement emplissait le lieu d’une impressionnante et chaude lumière.

Malgré l’affluence, l’ambiance était recueillie et le murmure qui se réverbérait dans les hautes voûtes était le résultat de quantité de chuchotements pieux plus que d’exubérance dans les échanges. Ernaut et Lambert ne dérogeaient pas à la règle. Ils avaient récité quelques prières devant la lanterne, le premier agenouillé et le second simplement assis dans son lit. Il avait été touché de l’attention de son frère et se sentait empli d’une félicité qu’il s’expliquait mal. Il était encore faible, mais cela l’avait convaincu de l’importance pour lui de se rendre le lendemain au grand office de Pâques. Ernaut avait bien tenté de l’en dissuader, pourtant rien n’y fit et il avait pris la résolution d’en parler au plus tôt aux hospitaliers qui s’occupaient de lui. Après un dernier *Pater*, le jeune homme se mit debout en se signant.

Il voyait que dehors la nuit gagnait et avait hâte de retrouver le groupe du père Ligier avant qu’il ne fasse tout à fait noir. La simple présence de Libourc l’apaisait, tout en le troublant à la fois. Lambert aperçut le coup d’œil de son frère vers l’extérieur et lui sourit.

« Il ne faudrait certes pas tarder si tu dois joindre ces pérégrins pour veillée, la noirceur avance vite.

— Ce n’est guère éloigné, à l’abord la porte Saint-Étienne.

— Je m’en voudrais que tu y arrives trop tard juste pour avoir discuté avec moi. De toute façon, il va bientôt être temps pour nous d’assister à la procession de la nuit. »

Ernaut hocha la tête et passa une main amicale sur l’épaule de Lambert.

« Demain, au matin, je passerai te quérir. Nous irons ensemble à la grand messe.

— Je suis impatient d’entendre voler les cloches pour cela, frère. Nous avons tant cheminé pour nous tenir ici. Je te l’ai déjà dit, il me tarde de voir notre vœu accompli. »

Ernaut craignait que Lambert ne fasse cela également pour le surveiller, conscient qu’il avait la tête un peu ailleurs. Que ce soit pour ses recherches du meurtrier ou son inclinaison pour une jolie fille, il existait de nombreuses raisons pouvant s’ajouter à l’habituel désintérêt d’Ernaut pour les célébrations religieuses. Ce que Lambert n’arrivait pas à saisir, c’était que son jeune frère ne pouvait demeurer sans comprendre, sans satisfaire son insatiable curiosité.

Assister aux divins mystères ne lui apportait guère de réconfort, n’éveillait que peu de choses en lui, si ce n’était une formidable envie de se faire expliquer ce qu’il voyait. Mais ce n’était pas là travail habituel de clerc et il lui aurait fallu de nombreuses années d’études cathédrales pour en découvrir la signification. Et Ernaut n’avait ni le désir ni la patience de se consacrer à un tel sujet si longtemps.

Saluant Lambert d’un sourire, il se dirigea d’un bon pas vers la sortie méridionale, ayant l’intention de trouver quelque boutique ouverte dans Malquisinat ou la Rue aux Herbes pour s’acheter de quoi manger pour la soirée. Il n’avait rien avalé en dehors de vieux quignons de pain depuis plusieurs heures et son estomac se rappelait régulièrement à son souvenir.

Lorsqu’il avait franchi la porte, la fraîcheur s’était instantanément abattue sur lui et, tout en avançant, il levait le nez dans l’air froid. Le ciel dégagé était zébré de quelques filaments cotonneux qui accrochaient la lumière rouge sang du crépuscule sur un fond bleu marine étoilé. Alors qu’il était en train de régler ses quelques emplettes à un jeune garçon qui proposait des morceaux de tourte aux herbes, il sentit une main se poser sur son bras, attirant son attention. En se tournant, il découvrit Eudes à ses côtés, un large sourire sur le visage.

« Alors, Ernaut, tu as pu assister à l’arrivée du Feu Sacré ? »

Le géant répondit d’un coup de menton affirmatif, déposant sans ménagement ses achats dans sa besace après avoir donné une piécette au vendeur. Les deux amis commencèrent spontanément à avancer vers le nord tout en parlant.

« C’était fort belle cérémonie… quoiqu’un peu longuette pour mon goût.

— J’y ai souventes fois assisté, et j’ai souvenir d’avoir patienté parfois jusqu’au mitan de la nuit… Cette année le Seigneur devait être plus satisfait de nos accomplissements. »

Ernaut grogna, à demi convaincu. Le sergent lui sourit et fit mine d’inspecter son sac du regard.

« Tu n’as pas encore acheté de quoi boire ?

— J’ai ma gourde avec moi, cela suffira, je pense.

— Ne veux-tu pas goûter quelque vin d’ici ?

— Aurais-tu idée en tête à poser pareille question, ami ? »

Eudes s’esclaffa amicalement et tapa joyeusement dans le dos de son compagnon.

« Tu commences à trop bien me connaître, dis donc ! En fait, je fais chemin vers la taverne où Roussel a ses habitudes. Tu pourrais y prétendre venir faire achat…

— Je connais déjà l’endroit, j’y ai fait passage au matin.

— Ah ? Tu y as découvert intéressantes choses ?

— Rien du tout. Il n’y avait que le tenancier, son fils et un chat. De notre chasseur rouquin pas l’ombre de la queue… »

Eudes souffla bruyamment, se grattant le visage où une barbe de quelques jours avait pris place. Il n’eut pas le temps de répondre qu’Ernaut continuait.

« Il est possible qu’il y ait plus de monde en cette veillée, du moins le vendeur l’espérait. Allons donc y faire un petit tour. Il s’y trouvera bien quelques futailles à mon goût ajouta-t-il en faisant claquer sa langue. »

De fait, l’ambiance n’était plus du tout celle du matin. De nombreux consommateurs étaient attroupés aux abords, discutant joyeusement, faisant rouler des dés malgré la période, échangeant des anecdotes avec force braillements, esclaffements et rires bruyants. Le négociant courait en tous sens, fort affairé, des pichets en main, souriant à tous, interpellant jovialement ses clients. En chemin pour aller déposer quelques récipients à l’intérieur, il salua l’arrivée du géant et de son acolyte d’un hochement de menton.

Ernaut et Eudes le suivirent, découvrant que des buveurs étaient également regroupés dans la petite cour, à la lueur de quelques lampes. La taverne elle-même était vide d’occupant, si ce n’était un jeune homme d’une vingtaine d’années en train de tirer du vin et de deux enfants qui rinçaient les brocs dans un tonneau d’eau claire avant de les mettre à sécher, à l’envers, sur les étagères de bois. Tout en haut du meuble trônait le chat, enroulé sur lui même, la queue battant selon un rythme que seul le félin entendait. L’endroit était illuminé de plusieurs lampes à huile, qui apportaient autant de lumière que de fumée ainsi qu’une odeur âcre.

S’essuyant les mains à un torchon qu’il avait noué autour de sa taille, le marchand s’avança, réalisant qu’Eudes avait une épée au côté. Son regard s’attarda quelques instants à ses pieds et détailla sa tenue, vérifiant s’il avait affaire à un chevalier ou un simple sergent.

Satisfait de son inspection, il les salua d’un signe de tête que les deux hommes lui rendirent. Puis le silence s’installa, personne n’osant commencer la discussion. Ernaut finit par prendre les choses en main et cogna du doigt sur un des tonneaux en perce.

« J’ai finalement eu désir de faire goutance de votre vin, maître. Pourriez-vous m’en tirer pichet ?

— Avec joie », répondit le tavernier, soulagé de n’avoir affaire à eux que comme clients et s’emparant avec empressement d’un pichet.

Tout en débloquant le coin de bois pour laisser s’écouler le vin, il se détendit. Il pensait que les deux hommes n’étaient finalement là que pour acheter de quoi boire. Commerçant avisé, il craignait toujours les entrevues avec les sergents de la cité. Ils représentaient la justice royale, mais aussi et surtout une administration fiscale prompte à se montrer exigeante, surtout en cette période de paiement de certaines taxes et loyers. Habitué, Eudes le comprit et tourna la tête, faisant mine d’admirer l’échoppe et ses clients.

« Vous avez là beau commerce, maître. On me l’a fort vanté.

— Grand merci, votre ami me l’a dit au matin. Un vôtre compaing, nommé…

— Roussel précisa Ernaut.

— Oui. Il me faudra le mercier, de m’envoyer ainsi acheteurs. Je n’ai malheureusement guère mémoire de lui. Il faut dire que nous avons souventes foi beaucoup de monde ici. »

Tout en parlant, il remit à Ernaut le pichet plein, indiqua le prix comprenant la consigne du pot et encaissa l’argent dans un repli de son tablier. Ce faisant, il avisa son valet qui passait, les bras chargés de brocs vides.

« Dis-moi, Garnot, as-tu connoissance d’un nommé Roussel ?

— Certes oui, maître, acquiesça le serviteur, le visage expressif.

— Il nous envoie bons clients, il faudra lui porter mon merci…

— Ce sera avec plaisir, maître. Mais je ne l’ai vu depuis quelque temps… J’en ai fait la remarque voilà peu au Boiteux. »

Le tavernier eut un sourire à l’évocation d’un nom connu.

« Il est donc compaing avec Blayves ? On retombe chaque fois sur les mêmes personnes…

— Qui est donc ce Boiteux dont vostre valet nous parle ? Interrogea Eudes.

— Un homme faisant négoce d’ouailles, qui a ses habitudes ici. On le nomme ainsi, car il a jambe folle. Vous ne le connaissez pas ?

— Je ne crois pas. Je vois tellement de monde avec mon travail. »

Le tavernier approuva en silence, attendant peut-être une réponse, mais finit par reprendre la parole.

« Il est âgé assez, le poil brun mangé de blanc par endroits. Il est souvent hors la ville, pour acheter moutons aux nomades, au-delà du lac de Galilée, sur les hauts plateaux. Je ne crois pas qu’il soit marié… »

Eudes se tourna vers le domestique, qui repassait, cette fois-ci avec des récipients pleins.

« Et ce Blayves, est-il en ville pour les fêtes ?

— Je ne sais… Pas vu depuis quelque temps. Il a peut-être préféré aller acheter des bêtes en prévision de l’arrivée de tous les pérégrins justement. Cela fait bien une ou deux semaines… »

Ernaut, qui avait écouté attentivement tout en humant son vin, et Eudes remercièrent chaleureusement avant de s’éloigner. Le tavernier semblait soulagé de les voir partir, bien qu’il n’ait aucunement changé d’attitude. Lorsqu’il rendit le pichet à Ernaut après l’avoir goûté, le sergent fronça les sourcils, contrarié.

« N’y a-t-il pas une âme encore en ville qui pourrait nous conduire à ce satané soigneur ? On dirait qu’il a fait le vide autour de lui.

— Crois-tu qu’il aurait aussi occis le marchand de moutons ?

— Comment savoir ? Si l’homme a usage d’aller et venir selon son gré, sans famille, qui aurait pu s’en soucier ? Ce qui m’échappe, c’est le pourquoi. »

Ernaut ne pouvait qu’adhérer à ce questionnement. Il ne semblait y avoir aucune logique dans les meurtres de ce dément. Peut-être était-il effectivement possédé par un diable. Il hésita un moment à dévoiler à Eudes ce qu’il avait appris auprès du confesseur de l’hôpital. Puis il convint qu’il ne pouvait garder pour lui une si importante information, quitte à ne pas s’étendre sur son origine. Indécis quant à la façon de l’amener, il annonça donc sans préambule que leur assassin avait certainement changé de nom, abandonnant celui de Maciot.

« Comment sais-tu pareille chose ?

— Je ne peux m’étendre, car j’ai fait serment de garder cela secret. Cela vient des frères de Saint-Jean.

— Ils ont bonne connoissance de ce félon ?

— Non, seulement son nom, et qu’il est dangereux, certainement lié à ces meurtreries.

— Ils auraient pu le faire assavoir plus tôt ! Grogna Eudes.

— Sincèrement, je pense que c’était compliqué pour eux. De toute façon, cela n’a aucune utilité pour le retrouver. »

Le sergent ne répondit pas immédiatement, serrant la mâchoire, les yeux lançant des éclairs.

« Comment pouvons-nous porter aide si chacun tire à soi la couverte ? Cela va fort contrarier le sieur vicomte, de sûr. Les frères se croient déjà seigneurs en la cité, rejetant toute autorité du Patriarche. Ils en viendront à s’en prendre à celle du roi si cela continue… Ont-ils ajouté quelques éléments qui pourraient nous aider ?

— Il est possible qu’il ait été tenu en fer chez les païens, pourtant il n’a pas été libéré par les frères, ils ne savent donc pas quand ni où. Mais cela serait notre lien avec les pauvresses.

— Certes.. Nous ne savons encore pourquoi ce soigneur tue plutôt qu’il n’aide pérégrins. Ils devaient se connaître avant et le mystère y trouve sa source. »

Débouchant sur la rue de saint Jean l’Évangéliste, les deux amis se quittèrent, ayant convenu de se retrouver le lendemain. Eudes était de service jusqu’au matin et aurait peut-être l’occasion de suivre de nouvelles pistes. Ernaut lui indiqua où il passerait la veillée, avec la possibilité pour lui d’y demeurer en attendant l’aurore.

Lorsqu’il parvint aux abords des jardins, il faisait pratiquement nuit noire et il se félicita d’avoir gardé avec lui la lanterne, ayant confié les chandelles à son frère. Il trouva sans mal le groupe, sur un carré de terre à l’abri de quelques palmiers parmi d’autres pèlerins. L’assemblée était installée aux abords de l’enceinte, où aucun vent froid ne se faufilait. Au-dessus d’eux, un des hommes de faction sur la muraille était accoudé, assistant lui aussi au spectacle qui devait égayer quelque peu sa rude tâche.

Le père Ligier était en plein sermon, détaillant avec conviction et de nombreux gestes démonstratifs le récit du retour du Christ après sa descente aux Enfers. Il était aidé en cela par une installation qu’Ernaut avait déjà pu apercevoir plusieurs fois depuis qu’il était arrivé en Outremer. Un drap clair avait été tendu, illuminé par l’arrière, et un bateleur s’employait à faire bouger des silhouettes de parchemin, mimant et illustrant le propos développé par le prêtre.

Ernaut s’approcha et vint s’assoir aux abords, sans faire de bruit. Il reçut quelques saluts silencieux issus de pèlerins, mais ni Sanson ni Libourc ne tournèrent la tête. Le spectacle attirait tous les regards. C’était la première fois que le jeune Bourguignon assistait à semblable récit. Les personnages semblaient vivre sous ses yeux, pourtant simples pantins articulés de cuir coloré. Lorsque le Christ se présenta à ses disciples, il sourit de l’innocence de Thomas, qui toucha du doigt les plaies du Seigneur. Et il admira l’Esprit Saint qui descendait, tel un oiseau, depuis le ciel, de plus en plus net au fur et à mesure qu’il se rapprochait des apôtres.

Enthousiasmé par pareille démonstration, le public était aux anges, applaudissait fréquemment ou s’unissait aux prières déclamées. De nombreuses bénédictions tombaient de temps à autre sur les héros du récit. Pour beaucoup, ce fut l’occasion d’enfin comprendre certains passages obscurs ou de bien identifier certains personnages des Évangiles . Leur foi populaire ne cherchait pas à décrypter tous les non-dits et la complexe symbolique des choses, mais s’émerveillait des histoires fabuleuses, des aventures des saints et des apôtres.
