## Épilogue

### Jérusalem, église du Saint-Sépulcre, fin de matinée du dimanche 14 avril 1157

Depuis les tragiques événements de Pâques, Ernaut s’était cru sur la voie de la damnation, son âme déchirée entre ses aspirations et la terrible réalité de ses actes. Au final, il lui fallait simplement admettre qu’il ne pouvait arriver à rien s’il n’était prêt à un certain renoncement. Il n’était pas tout-puissant, un être parfait, mais un modeste pécheur. Dieu l’avait certes fait à l’image de Samson et avait instillé en lui l’amour de son prochain. Il en était persuadé, convaincu de cela par son envie impérieuse de porter assistance à ceux qui n’avaient pas les capacités de se défendre.

Bien évidemment, son chemin ne pouvait être que de lumière, il n’était pas un saint. Il devait accepter la part de noirceur qui le constituait et tenter de s’en servir à une juste cause. Ce qu’il avait fait à Roussel n’avait attiré sur lui nulle condamnation, n’avait entraîné nul trouble. Bien au contraire, il avait apporté la paix, y compris au pauvre fou qui poignardait les malheureuses pèlerines. Après tout, Dieu n’avait-il pas sanctifié la guerre en son nom, pour reprendre l’endroit même où il se trouvait en ce moment ?

Par ailleurs, alors qu’Ernaut était encore hébété de son geste, Eudes l’avait soutenu, quand bien même il ne savait pas tout. Désormais, Ernaut comprenait la raison de tout cela. Ici en Outremer, dans un pays perpétuellement assailli par des ennemis implacables, les plus forts n’étaient pas raillés ou décriés, mais distingués, sortis du rang et mis en avant. On leur offrait leurs désirs sur un plateau au lieu de les punir pour ce qui n’était, après tout, qu’une façon bien naturelle d’employer les talents dont Dieu les avait pourvus.

Il souriait pour lui-même quand il retrouva le soleil à la porte du Saint-Sépulcre. Les mains sur les hanches, il leva le menton vers le ciel, comme s’il pouvait s’adresser directement à Dieu, le remerciant en un familier signe de tête. Lorsqu’il baissa de nouveau le visage, il découvrit Eudes qui s’approchait, l’épée au côté et le casque coiffé, un gros sac dans les bras. Il lui sourit avec chaleur.

Le sergent lui répondit poliment et lui tendit son baluchon, qu’Ernaut entrouvrit, empli de curiosité.

« Un vieux casque et une épée qu’on m’a prêtés, en attendant que tu achètes les tiens…

— Grand merci, ami.

— Il te faudra aussi bonne tenue gamboisée, tu devras en faire tailler une à ta stature. Rassure-toi, il n’en est nul besoin encore. Avant tout, tu dois m’accompaigner au palais royal, que tu y prêtes serment comme tous l’avons fait. Ce n’est qu’alors que tu seras de vrai un sergent de la cité. »

Eudes remarqua le sourire béat de son compagnon.

« Je vois que cela t’apporte moult gaîté de rejoindre nos rangs.

— Certes mais je ne m’enjoie pas que pour cela.

— Quoi d’autre ?

— Je me disais que j’allais me plaire ici. » ❧

*À suivre…*

<p class="qita_licence"><span class="qita_scission">❖ ❖ ❖</span></p>
<p class="qita_info_licence2">**Ce roman vous a plu ?**</p>
<p class="qita_info_licence2">N'hésitez pas à en parler, à l'envoyer à vos proches et à me le faire savoir.<br/>Cet ebook étant diffusé gratuitement, vous pouvez m'aider à écrire les prochains en me soutenant.</p>
<p class="qita_info_licence2">Faire un don sur [https://hexagora.fr/fr:donate](https://hexagora.fr/fr:donate)</p>
<p class="qita_info_licence2">Merci,<br/>Yann.<br/></p>
<p class="qita_licence"><span class="qita_scission">❖ ❖ ❖</span></p>
