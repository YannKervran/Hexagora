## Chapitre 5

### Jérusalem, église du Saint-Sépulcre, dimanche 14 avril 1157

La messe terminée, il ne subsistait plus de ces instants sacrés qu’une vague effluve d’encens dans les lieux saints. Chacun retournait à ses affaires, et seuls de rares fidèles s’attardaient auprès des autels, cherchant à prolonger le lien surnaturel qui s’était instauré de façon éphémère. Ernaut dirigea ses pas vers la sortie puis décida d’obliquer vers la chapelle d’Adam. Là, au fond de l’abside, on pouvait voir le rocher au-dessus duquel la Croix du Seigneur avait été placée. Surtout, ce serait en ces lieux que se trouvait le crâne d’Adam, le premier homme, qui avait reçu le sang du Christ par la fissure encore visible, marquant par ce sacrement sanglant le rachat des péchés de l’humanité. Depuis lors, il était possible de connaître la Rédemption par le baptême et la dévotion, une vie pieuse et l’observance des règles indiquées par les prêtres.

Ernaut ne s’y attarda pas et monta l’escalier qui menait à la chapelle au-dessus, où un riche décor de mosaïque recouvrait les murs. Un modeste groupe de pèlerins en admirait justement la beauté, l’empêchant de s’approcher de l’endroit où la Croix avait été fixée. À tour de rôle, ils s’avançaient pour mettre leur visage dans l’encoche qui l’avait accueillie. Par ailleurs, plusieurs déposaient de petites croix de bois qu’ils avaient amené avec eux depuis leurs lointaines contrées, ainsi qu’Ernaut l’avait fait en son temps.

Il patienta, hésitant à se recueillir devant le rocher. Puis il y renonça finalement et sortit directement par l’escalier qui donnait sur la façade. De là, il pouvait embrasser tout le parvis où se trouvaient comme toujours les marchands et les pèlerins affairés, certains heureux de découvrir le lieu tant espéré, d’autres pénétrés de la Foi qui les animait, les yeux pleins de dévotion. Ils venaient de toute la Chrétienté, mus par leur souhait de se rapprocher du divin.

Ernaut eut un reniflement agacé. Il ne voyait que gesticulations, populace avide et grouillante. Combien parmi ces gens portaient en réalité le démon en eux ? Il leur suffisait de se rendre en cette église, de verser quelques monnaies, de suivre des processions, de parler à un prêtre et ils se croyaient lavés du mal qui les habitait. Était-ce si simple ? Le jeune homme en doutait.

Il avait eu les mêmes inclinaisons, mais à son élan du cœur aucune réponse n’était venue. Il se sentait floué de l’amour de Dieu, sans pour autant abandonner l’espoir d’y être de nouveau confronté un jour. Il savait que le Seigneur se manifestait parfois en des lieux bien étranges, et qu’aucun endroit n’était hors de sa portée, à l’écart de sa bénédiction, quand bien même les ténèbres les plus noires le recouvriraient. La Sainte Croix avait sa place partout et apportait la lumière qui guidait sur le chemin de la Vérité.

### Jérusalem, faubourg de la porte de David, matin du vendredi 29 mars 1157

Ernaut n’arrêtait pas de s’étirer et de bâiller, fatigué de la mauvaise et courte nuit qu’ils avaient passée dans les faubourgs près de la porte de David. Abdul Yasu les avait laissés dormir dans une de ses granges et ils avaient partagé l’endroit avec des pèlerins allemands, dont l’un ronflait si bruyamment que ça en devenait drôle, du moins au début.

Après avoir rapidement acheté quelques oublies pour leur déjeuner, ils se rendirent aussi vite que possible à l’hôpital de Saint-Jean. La cité s’éveillait à peine et l’entrée de Jérusalem était encombrée de marchands de légumes, d’animaux, de fruits, qui venaient, comme chaque matin, approvisionner la population qui vivait à l’abri des murs. Quelques cochons et porcelets étaient étonnamment présents en cette fin de Carême, certainement à destination du marché à l’ouest de la ville.

Comme les porchers menaient leur troupeau par les rues, bloquant le passage et obligeant Ernaut et Eudes à de fréquents détours, celui-ci réalisa qu’ils avaient dû être amenés plutôt pour nettoyer les caniveaux, en mangeant tout ce qui pouvait y traîner. L’odeur du lisier qui émanait des bêtes lui paraissait un parfum bien agréable comparé à ce qu’il avait subi précédemment.

Soulagé après cette épreuve, Ernaut fit d’ailleurs remarquer avec amusement que les porcs couinaient moins fort que leurs compagnons ronfleurs de la nuit. Lorsqu’ils entrèrent dans l’hôpital, ils en apprécièrent la fraicheur et le calme relatif, malgré l’effervescence qui y régnait. Ce n’était pas une agitation désordonnée, plutôt le tranquille et régulier fonctionnement d’une machinerie complexe et adroitement ajustée.

Ils se dirigèrent droit vers le lit de Sanson, afin de vérifier s’il était bien formel sur le fait que la croix n’était pas celle de Nirart. Ils le trouvèrent assis sur son matelas, occupé à deviser gaiement avec ses voisins. Il devait sortir ce jour, rejoindre une nouvelle fois ses compagnons et assister aux dernières cérémonies de Pâques, parmi les plus importantes. Son humeur était donc excellente et il badinait joyeusement, s’esclaffant régulièrement, ce qui redonna du cœur à l’ouvrage aux deux enquêteurs un peu las.

Le vieil homme jeta un coup d’œil rapide tout en secouant la tête, une grimace négative sur le visage. La croix faisait un peu plus de deux pouces de haut, en bronze émaillé de couleurs vives. À n’en pas douter, c’était un objet de quelque valeur.

« Ce n’est pas à lui, de sûr. Ou alors il l’aurait scellée à nos regards. Mais vu la belle qualité de l’objet, il l’aurait certainement montrée à un moment ou l’autre, ne serait-ce que pour recevoir bénédiction. Pour moi, elle n’a jamais été sienne. »

Eudes approuva de la tête.

« Nous devrions demander au frère en charge à voir de nouvel le valet qui a nettoyé la dépouille de Nirart. Il pourra peut-être nous en dire plus. »

Les deux hommes saluèrent donc Sanson, Ernaut de façon plus appuyée que son compagnon, et ils retournèrent vers la table derrière laquelle se trouvait assis le responsable qu’Ernaut avait vu la veille. L’hospitalier, toujours aussi affable malgré ses fréquentes digressions pour régler un problème ou l’autre, leur indiqua que le serviteur ne travaillait pas ce jour et le suivant. Il avait obtenu l’autorisation d’aller passer les fêtes en famille, dans un village au nord de la cité. Remarquant la croix qu’Eudes tenait encore à la main, il s’enquit de ce qu’elle avait à voir avec le domestique.

« Nous aimerions assavoir pourquoi il l’a mise à un des pauvres qu’il a préparés. »

Le frère parut ne plus rien comprendre, les sourcils froncés et le regard dans le vague.

« Je ne saisis guère. Vous voulez voir l’homme qui a lavé la dépouille ou le serviteur de salle ?

— Pourquoi pareille question ?

— Eh bien ce pendentif est celui d’un de nos valets de salle, Roussel. Mais il n’a pas charge des soins post mortem. Il assiste les chefs de salle exclusivement pour les soins aux vifs. »

Ernaut s’avança, incrédule.

« Vous êtes acertainé ?

— Bien sûr. Il l’a arborée peu après son entrée en notre hôtel, peut-être acquise avec ses premières soldes. Je lui avais fait remarque que c’était bijou bien voyant et onéreux. Il m’avait répondu que ce n’était que babiole par rapport à la Vraie Croix et qu’il espérait qu’elle le protégerait. »

Le moine marqua un temps, comprenant l’étrangeté de la situation.

« Pourquoi pensez-vous qu’il l’aurait mise à un défunt ? »

L’air sombre, Eudes plissait des yeux, réfléchissant aux conséquences de ce que venait de dire le moine.

« Si vous êtes sûr qu’il n’a pas aidé à la toilette du corps, est-il possible qu’il l’ait donnée pour que cela soit fait par l’autre valet ?

— Le plus simple serait de le lui demander. Mais je crois que cela fait plusieurs jours qu’il n’est pas venu. Son chef de salle s’en est étonné auprès de moi, car il a toujours accompli sa besogne avec grand sérieux. »

Ernaut se pencha, de plus en plus intéressé par la conversation, laissant toutefois le sergent continuer son interrogatoire.

« Cela fait longtemps qu’il ne s’est pas présenté ?

— Je crois une semaine, depuis Pâques fleuries. »

Eudes ne pouvait empêcher sa voix de devenir fiévreuse tandis qu’il sentait que leur enquête avançait à grands pas.

« Sauriez-vous où je peux le trouver ?

— Moi non, mais certains de ses camarades savent peut-être où il loge. Il faudrait les interroger. Il était affecté à la troisième rue. »

Tournant la tête pour vérifier la zone que le frère indiquait, une travée délimitée par les puissants piliers qui soutenaient les voûtes, Eudes avait les yeux brillants d’excitation.

« Pourriez-vous nous dresser rapide portrait ?

— Il est de taille moyenne, a dû être assez fort, les privations l’ont désormais amaigri. Le cheveu bouclé, un peu plus foncé que paille, tirant fort sur le roux. Un de ses yeux regarde en oblique, ce qui lui donne un air un peu simplet, ce qu’il n’est pas. Toujours bien propre et d’un naturel obéissant.

— Quel âge a-t-il ?

— C’est un homme fait, je dirai entre vingt et trente ans.

— Je vous mercie mon frère, vous nous avez été d’un grand secours. »

L’hospitalier porta la main à sa bouche, comme pour se retenir de poser une question. Eudes le remarqua, et avant de se détourner, il l’interrogea du regard puis l’invita à exprimer ce qu’il avait en tête. Le vieil homme était hésitant, comme s’il formulait à voix haute une idée qui prenait forme dans son esprit, sans qu’il soit bien convaincu qu’elle fut judicieuse.

« Je me demandais : comment avez-vous cette croix en votre possession si elle avait été accrochée au cou du défunt ? Le corps était déjà parti pour Chaudemar… »

Les regards appuyés que se lancèrent les deux enquêteurs convainquirent le moine de ne pas aller plus avant.

« Ah… Ne m’en dites rien. Certaines choses ne méritent pas d’être sues ni contées. »

Il les salua d’un bref signe du chef et se replongea dans la lecture d’une tablette qu’il attrapa rapidement. Eudes invita silencieusement Ernaut à se retirer et ils avancèrent tranquillement au milieu de la cohue vers la travée désignée par l’hospitalier.

Ce jour commençaient les plus grandes cérémonies de l’année et beaucoup de pèlerins venaient s’enquérir de l’état de santé de leurs proches, anxieux de les voir participer aux célébrations du Samedi saint avec eux. Ernaut réalisa qu’il n’était même pas allé saluer Lambert. Il s’arrêta et interpella Eudes du regard.

« Mille pardons, il me faut visiter mon frère, voir s’il ne peut toujours pas venir à la messe avec moi. Outre, je ne peux manquer les festivités. Je suis céans pour ça avant tout. »

Le sergent prit quelques instants pour réfléchir et approuva silencieusement à l’avance ce qu’il allait dire, hochant de la tête.

« Tu as raison. Nous avons tous deux des choses à faire. La nuit a été longue et j’aimerais passer voir mon épouse et mes enfants. Puis il me faut au plus tôt avertir le sieur vicomte ou le mathessep de ce que l’époux est mort depuis plusieurs jours et que nous avons idée de qui serait le coupable.

— On a bien avancé, d’aucunes façons, non ?

— Oh oui ! Et avec chance, tu retrouveras le garçonnet que tu as manqué de peu hier. Ou même ce… Roussel. Si c’est le cas, ne tente rien, vois si tu peux le pister sans te faire voir et découvrir où il loge. On avisera après.

— Je demanderai si d’aucuns ont souvenance de ce valet, comme il me faut de toutes les façons saluer mon frère, soigné ici. »

Eudes se passa la langue sur les lèvres, peu enthousiasmé par l’idée de laisser Ernaut œuvrer seul.

« Les frères de Saint-Jean ont donné accord pour questionner. Je serai prudent, sois acertainé.

— D’accord » concéda-t-il finalement.

Eudes allait s’éloigner, se retint un instant, serrant d’une main ferme, mais puissante, le bras du jeune homme.

« Fais attention à toi, Ernaut. Si c’est notre murdrier, c’est un démon redoutable, quoi qu’en pense le frère hospitalier. Ne prends nul risque. »

Ernaut gonfla la poitrine et contracta les muscles de son dos, exagérant ainsi la force qui émanait de sa silhouette. Un sourire carnassier s’afficha presque de lui-même sur le visage pourtant sympathique habituellement.

« Je pense plutôt que ce serait à lui de prendre garde s’il affronte un homme et non plus des femmes ou un malheureux à moitié squelettique. »

Eudes détailla silencieusement le colosse qui se tenait à ses côtés. À force de le fréquenter, il commençait à en négliger la stature. Il reconnut, in petto, qu’effectivement, seul un fou oserait s’en prendre à un tel adversaire. Mais n’était-ce pas exactement ce qu’ils pourchassaient ?

### Matinée du vendredi 29 mars 1157

Avisant la foule et le va-et-vient des serviteurs, Ernaut estima plus raisonnable d’attendre que la presse soit moins dense, et les domestiques moins affairés, pour aller poser ses questions. Il ne tenait pas à ce que tous les pèlerins entendent ce qu’il avait à dire.

Et surtout, il ne voulait pas prendre le risque que le meurtrier puisse se faufiler et espionner, caché parmi les voyageurs. Il préféra donc diriger ses pas vers la rue où était hébergé son frère. Malgré tous les soins dont il était l’objet et sa volonté de se remettre au plus vite, Lambert était encore bien fatigué et dormait profondément lorsqu’Ernaut le retrouva.

Le jeune homme n’eut pas le courage de le réveiller, estimant qu’il était plus profitable pour Lambert de rester à se reposer. D’autant qu’il n’était guère convaincu que ses dernières aventures seraient aisées à conter sous un jour favorable. Il ressortit donc en direction du Saint-Sépulcre, espérant trouver là quelques personnes de sa connaissance, avec une préférence pour une jeune fille, avec qui il pourrait patienter.

Le soleil maussade n’apportait qu’une lumière diffuse sur le parvis et aucune chaleur. Cela n’entravait en rien l’animation habituelle et de nombreux groupes étaient agglutinés un peu partout. En dépassant les colonnes, Ernaut entendit parler des événements récents. Chacun y allait de son commentaire à propos des attaques sur les femmes et surtout du fait que désormais, le fou semblait s’en prendre à n’importe qui. Les remarques étaient souvent aussi catastrophées qu’ignorantes de la réalité. Le plus souvent, ce n’était là que prétexte à moraliser un peu plus, se fustiger sur ses pratiques religieuses, ou admonester une forte tête.

Toujours à l’affût de ce qui se disait, Ernaut laissait traîner ses oreilles lorsqu’il entendait parler dans une langue connue. Il en retint que pour la plupart des gens, l’assassin était un dément, qui désormais frappait sans aucune distinction. La façon dont il accomplissait ses meurtres était également prétexte à inquiétude. Si la mort état une compagne inévitable et était familière à beaucoup, la rencontrer sous des coups sauvages, bestiaux, avec une simple pierre comme si l’on n’était que des animaux, constituait une grande source de frayeur.

Par ailleurs, l’identité du mystérieux agresseur faisait l’objet de toutes les suppositions, même les plus folles. Si, au départ, il s’avérait évident pour la plupart que ce ne pouvait être l’œuvre que d’un païen, un de ces maudits musulmans toujours avides de vengeance, beaucoup évoquaient des hypothèses moins habituelles. Les récits des conteurs, d’Europe et d’ailleurs, avaient visiblement bien alimenté les cervelles et on citait pêle-mêle un démon, un esprit malin, un génie du désert, voire quelque créature fantastique, croisement invraisemblable avec un ou plusieurs animaux.

Les remarques arrachèrent à Ernaut un rictus acerbe. Il savait que ce n’était malheureusement qu’un homme à l’origine de tout cela, ce qui était bien plus effrayant au final. D’autant qu’il ne semblait guère enclin à se calmer, sans qu’on ait idée de ce qui le motivait. L’élément le plus perturbant pour Ernaut était la nature des victimes. Ce n’étaient que des personnes fragiles, faibles, comme l’était le petit Oudinnet, qu’il espérait retrouver au plus tôt. On aurait dit que l’assassin cherchait à éliminer ce qu’il y avait de plus précaire dans la cité.

Une telle lâcheté était inadmissible pour le jeune homme, qui n’avait aucun ressentiment envers ceux capables de se dresser face à un opposant de leur taille. Par contre, il ne supportait pas l’idée qu’on puisse s’en prendre à plus chétif que soi. Enfant, il avait frémi en entendant les hauts faits d’armes des chevaliers chrétiens. Si la violence lui apparaissait une façon légitime d’obtenir gain de cause, cela ne pouvait se concevoir qu’entre adversaires de force égale. Aucun de ses héros ne recourrait jamais à la lâcheté pour parvenir à ses fins. Cela était le symptôme d’un esprit malade, dérangé, veule. Il fallait qu’un homme se dresse sur son chemin et Ernaut ne doutait pas qu’il serait celui qui l’arrêterait.

Perdu dans ses noires pensées, il ne s’aperçut pas des signes que lui faisait un pèlerin assis sur les marches à droite de l’entrée. Ce ne fut que lorsque son nom fut crié qu’il fit face, le visage toujours fermé. Il avisa tout d’abord la silhouette du père Ligier, impossible à manquer. Le prêtre lui tournait à demi le dos, occupé comme toujours à s’entretenir avec ses fidèles. Parmi eux, Gobert le carrier lui faisait un sourire amical et l’attirait d’un geste enthousiaste. L’arrivée du jeune homme fendit le groupe comme un soc la glaise. Un peu surpris, le clerc l’accueillit d’une tape sur l’épaule.

« Ça alors ! Revoilà notre jeune ami. La sergenterie de la cité n’a donc plus usage de vos talents ?

— Je suis désolé de m’être éclipsé ainsi à la veillée, père curé, c’était pour…

— Il n’y a nulle offense, voyons. Porter assistance à son prochain me semble bien chrétienne activité, autant que peut l’être la prière. Et si ton prochain est vicomte le roi en la sainte Cité, le devoir n’en est que plus impérieux. »

Rasséréné par cet hommage vibrant, surtout de la part d’un clerc généralement assez strict malgré sa faconde, Ernaut se contenta de sourire. Il chercha Libourc des yeux quelques instants puis, l’ayant trouvée, tâcha de ne pas la fixer, tout en s’efforçant de veiller ses réactions. Ce fut Gobert qui brisa le silence.

« La cour va-t-elle prendre le larron ? Partout on ne parle que de cela. Chacun craint pour sa vie, et plus seulement pour son épouse ou ses filles. »

Ernaut hésita à dévoiler ce qu’il avait découvert, d’Eudes ou par lui-même. Son expérience sur le *Falconus* lui avait enseigné la prudence et la réserve. Moins on en disait, moins il était possible que l’adversaire apprenne où en étaient les traqueurs. Il se gratta le visage, cherchant une échappatoire à cette question trop directe à son goût.

« Je ne saurais dire, ami. Une chose est sûre, ce démon remue trop de boue pour demeurer longtemps impuni. La main du roi s’abattra avec force.

— Sait-elle seulement qui frapper ?

— Le sieur vicomte ne me dit pas tout, mais il semble avoir son idée. »

Gobert hocha la tête en assentiment, apparemment convaincu. Ernaut se demanda ce qui avait poussé le manchot à se renseigner. Aurait-il un intérêt autre que la simple curiosité à en savoir plus sur l’affaire ? Bien qu’amputé d’un bras, il demeurait suffisamment fort pour s’attaquer à quelqu’un, surtout des femmes fragilisées par une longue captivité. Il ne ressemblait nullement à ce fameux Roussel et n’aurait pu travailler à l’hôpital avec son membre manquant. Seulement, il pouvait tout à fait avoir partie liée avec l’assassin.

Néanmoins, la nuit dernière Gobert était avec eux, en dehors de la ville, et n’avait pas pu frapper le pauvre homme qui avait succombé dans la venelle. Ou alors avec quelques complicités de responsables d’une poterne ou l’autre. Ernaut avait appris récemment que certaines portes étaient confiées parfois à des communautés religieuses ou des notables qui acceptaient de temps à autre, contre paiement sonnant et trébuchant, de faire tourner la clef de l’accès sous leur garde. Et il était loin de connaître toutes les astuces qui permettaient de quitter l’enceinte de la Cité de façon discrète.

Il fallait absolument trouver pourquoi ce Roussel avait été pris de folie et s’était acharné après un groupe précis de pèlerines. Se rapprochant du prêtre, auprès de qui, par chance, Libourc et sa mère se tenaient, Ernaut se pencha vers le gros homme et l’entraîna de côté, vers l’entrée attenante de la petite chapelle des Trois-Marie.

« Dites-moi, père curé, tout le groupe est arrivé à temps cette nuitée pour la veille en le jardin ?

— Si fait, personne ne manquait à l’appel. Fors ceux qui ont disparu depuis longtemps, ajouta-t-il, peiné.

— Vous n’avez rien remarqué d’étrange ces derniers jours parmi vos ouailles ? Un rouquin que vous n’avez pas en charge… »

Le clerc fronça les sourcils, visiblement embarassé par la question. Il se mordit la lèvre avant de répondre :

« Pas en mes souvenirs, non. Tu n’as tout de même pas croyance que ce loup se cache parmi mes brebis ?

— Je ne sais, père, il est juste acertainé que ses victimes s’y trouvaient… »

Le père Ligier réfléchit quelques instants avant de reprendre d’une voix peinée.

« Je n’ai rien vu ni entendu de semblable, et même à confesse, rien de tout cela, je peux te l’assurer. Il n’y a que chagrin et tristesse en ce groupe depuis l’annonce de la meurtrerie de nos amies. Et nous sommes tous fort inquiets pour le pauvre Oudinnet.

— Il n’y avait pas quelque pérégrin qui aurait quitté votre groupe depuis l’arrivée de ces deux femmes ? Ou un de leurs compaings qui ne serait pas resté ? Un qui aurait les yeux torves.

— Pas à ma connaissance. Je n’en ai vu aucun s’adresser à elles fors de notre petit groupe, et elles n’ouvraient guère la bouche, si ce n’est pour prier. Nirart lui-même était plus piteux que Job. Tous nos efforts furent vains pour leur rendre quelque gaieté. »

Le prêtre se tint coi quelques instants, se passant la main sur la tonsure défraichie, visiblement déçu de ne pas pouvoir faire plus.

« Peut-être que… Tu as ouï parler d’Amalric ?

— Oui, fort peu, il est passé outre voilà déjà nombreuses semaines..

— Certes. Son trépas a fort secoué Nirart et les siens d’ailleurs. Il avait été captif avec eux. Je ne l’ai pas connu, si ce n’est de loin. Le pauvre n’a pas vécu assez pour quitter l’hôpital des frères de Saint-Jean. »

Ernaut se dit qu’il devrait tenter d’en savoir plus sur ce dernier, car il avait éventuellement parlé autour de lui tandis qu’on s’efforçait de le guérir de sa gangrène. Il était possible que se trouvât là le lien qu’il cherchait avec le soigneur disparu. Avant de mourir, un terrible secret avait pu être confié à un valet des environs. Roussel.

« Encore une question. Une de vos ouailles possède-t-elle ou possédait-elle belle croix émaillée, portée autour du col ? »

Le gros clerc chercha dans sa mémoire un long moment, levant les yeux au ciel et marmonnant pour lui-même pour finalement secouer la tête en dénégation.

« Non, pas en mes souvenirs. D’ailleurs, aucun n’a fortune suffisante pour se payer pareille frivolité. Beaucoup ont tout vendu pour venir ici, ou ont laissé leur bien en gages. Ils ne portent que la croix d’étoffe cousue en l’épaule, comme bon pérégrin.

— Je ne pensais pas forcément à fort onéreuse breloque, mais certes de quelque valeur. Personne autour de Nirart ou d’Oudinnet n’en portait de pareille ?

— Jamais vu ce genre de chose sur les pauvres âmes dont j’ai la charge. C’est là plutôt bijou d’abbé ou de dévote ce me semble. Arborer semblable orfèvrerie ne s’explique guère si on n’est pas fort croyant, et il demeure souvent grande peur de se le voire rober. »

Ernaut réalisa que le bijou avait peut-être joué sa part dans la tragédie qui se déroulait actuellement. Si sa valeur n’était pas que pécuniaire aux yeux de l’assassin, il était certainement en chasse pour la récupérer. Il s’imaginait sans doute que Nirart la lui avait dérobée, avant de mourir seul dans un coin, et qu’il avait eu le temps de la donner à son épouse ou à son amie. Pouvait-on seulement massacrer avec autant de sauvagerie pour remettre la main sur le symbole de leur foi à tous, qu’ils célébraient en ce moment même ?

Ernaut serra les mâchoires, l’air mauvais. Il ne pouvait croire que le Seigneur laisserait une attitude aussi impie demeurer impunie. Il ne se rendit pas compte de la paire d’yeux qui l’observait attentivement, à la dérobée. C’était pourtant ce à quoi son cœur aspirait depuis un bon moment.

### Fin de matinée du vendredi 29 mars 1157

Ernaut s’était finalement joint au petit groupe du père Ligier pour aller se recueillir dans la chapelle du Golgotha. L’attrait que représentait la jeune fille s’était ajouté à l’envie de prier, de libérer son esprit de toutes ses inquiétudes, ses tourments, ses interrogations. Étant donné l’affluence, ils n’avaient pas pu s’y maintenir très longtemps, de façon à ce que chaque pèlerin puisse y demeurer au moins quelques instants. En outre, malgré toute sa sincérité, Ernaut n’arrivait pas à se concentrer sur ses oraisons.

Il n’arrêtait pas de penser à l’affaire. Désireux de ne pas rester à attendre que le meurtrier frappe une nouvelle fois et s’en prenne alors à un jeune garçon, il cherchait un moyen de traquer Roussel, tournant et retournant ce qu’il savait de lui en tous sens. Estimant que l’hôpital serait plus calme désormais, il en arriva à la conclusion qu’il devait discuter dès que possible avec les hommes de salle qui côtoyaient le suspect. Il aurait également ainsi la possibilité de s’enquérir de Lambert, faisant d’une pierre deux coups.

Après avoir salué les pèlerins avec chaleur, il prit congé de façon plus cérémonieuse avec Libourc et sa mère, se proposant de porter un message à Sanson. Ce pouvait être l’occasion de resserrer les liens, encore lâches, qui les reliaient. Devant le refus pincé de la femme, il n’insista pas. Il put néanmoins échanger quelques regards avec la jeune fille, qui chavirèrent son cœur malgré toute l’innocence qu’on pouvait y lire.

Il obliqua immédiatement après être entré dans le grand logis en direction de la zone où s’activait normalement Roussel. Là, plusieurs serviteurs s’employaient à changer les draps, nettoyer les malades ou porter les repas. Il avisa un valet à l’air joufflu, dont la dentition aurait rendu fou de jalousie n’importe quel cheval. Il était occupé à retourner et remettre en forme un lit qui allait certainement rapidement recevoir un nouveau pensionnaire. Du coin de l’œil, il comprit qu’on lui voulait quelque chose et il se tourna, le regard vide.

« Y’a un problème ?

— Non non, rassure-toi. J’aurais juste besoin de te poser questions. On m’a conté qu’un mien camarade besogne habituellement ici, or je ne le vois pas.

— Qui ça ?

— Roussel. Un blond tirant sur le roux, les yeux pointant l’un vers l’autre, pas très grand.

— Ah oui. Je le connais. Il est pas là. »

Puis, satisfait de la pertinence de sa réponse, il se tourna de nouveau vers le lit et recommença sa tâche. Un peu décontenancé, Ernaut bafouilla lorsqu’il reprit.

« On m’a dit qu’il n’était pas venu à son service depuis plusieurs jours. Or, vu que je suis ici seulement pour les fêtes de Pâques, j’aurais aimé le voir quand même. »

Le valet se releva une nouvelle fois et le dévisagea, avec une vivacité dans le regard qui n’avait rien à envier à celle d’une poule. Il paraissait ne pas comprendre tout ce qu’on lui disait. Cela inquiéta un peu Ernaut, sa demande lui semblant d’une simplicité effrayante.

« Bein, vu qu’il est pas là, je ne sais comment vous allez faire.

— Tu n’as pas idée où je pourrai le trouver ? Connais-tu son logis ? »

La demande sembla bloquer complètement le cerveau de son interlocuteur qui resta, le regard dans le vague, interrompu dans sa tâche. Une voix nasillarde et haut perchée se fit entendre derrière Ernaut, d’un timbre ferme et d’une élocution rapide, qui tranchait avec l’anônnement hésitant du serviteur.

« Que voulez-vous à mon client[^client] ? »

Faisant volte-face, Ernaut se trouva devant un petit gabarit, tout en rondeur, le visage joufflu et l’air débonnaire, démenti par un regard perçant, caché dans les replis de graisse.

« Mille excuses, mon frère, je ne voulais pas déranger. Je suis en quête de Roussel.

— C’est son ami, crut bon d’ajouter le domestique, dont le cerveau semblait montrer de nouveau des velléités de fonctionnement. »

L’hospitalier chef de salle se gratta l’oreille d’un air circonspect et inspira bruyamment.

« Un compaing à vous ? »

Ernaut crut bon de se justifier, afin de ne pas s’empêtrer plus avant dans un mensonge qu’il avait proféré à l’origine pour se simplifier la tâche.

« Pas vraiment en fait. Je suis à sa recherche, je ne le connais guère. Je fais ça pour des amis. »

Le frère écoutait toujours, le visage imperturbable.

« Vous m’avez peut-être aperçu, je suis venu à moult reprises déjà. Lambert, mon propre frère est soigné ici, peu loin. »

Le chef de salle parut se contenter de cette explication et invita de la main Ernaut à s’approcher tandis qu’il s’avançait un peu à l’écart.

« Vous êtes le jeune pérégrin qui œuvre avec l’accord du frère hospitalier, ce me semble, non ? »

Ernaut hocha la tête en silence, étonné et flatté qu’on le reconnaisse aussi aisément. Il avait tendance à oublier facilement que son gabarit permettait une identification rapide. Le clerc continua :

« Si vous le trouvez, vous lui direz de ma part toute la déception qui est la mienne. Nous l’avons traité comme l’un des nôtres. Et il disparaît, tel vaurien. Au pire moment de l’année ! Je suis fort mécontent de lui, oh ça oui ! »

Tandis qu’il s’échauffait, le petit homme remontait les manches de son habit de laine noire, roulant son vêtement chaque fois plus haut.

« Il m’a fait bonne impression, depuis le début. Appliqué, efficace. Plein d’initiative. Et sa loyauté me semblait acquise ! Avec tout ce que l’Hôpital a fait pour lui, que d’ingratitude ! Il mériterait qu’on le punisse de la façon appropriée : les mains percées au fer rouge, voilà qui lui apprendrait le respect ! Sainte Vierge, venez-moi en aide, le simple fait de repenser à lui m’incite à lâcher jurons que je me suis engagé à ne jamais plus prononcer.

— Pourriez-vous m’en dire un peu plus ? Je sais juste qu’il besognait ici. Vous le connaissez fort ?

— Je le croyais. Nous l’avons recueilli et soigné durant long moment. Lorsqu’il est arrivé, il n’était que plaies et bosses. Il avait visiblement beaucoup souffert au cours de son voyage, sûrement aux mains des brigands turcs. Lorsqu’il s’est trouvé aller mieux, il a souhaité demeurer en notre hospital. D’abord en échange du couvert simplement, puis nous l’avons loué comme valet lorsqu’une place a été libre.

— Que lui était-il arrivé selon vous ?

— Je ne sais pas au vrai. Il ne l’a jamais conté. Un de nos serviteurs l’a peut-être trouvé demandant aumône en une venelle, et nous l’a mené. Heureusement, car sinon il serait de certes mort à l’heure qu’il est. Je préfère tout de même le savoir ingrat que passé outre.

— Il était seulet ?

— Oui, une pauvre chose abandonnée de tous, sauf de Dieu.

— Si je comprends bien, il n’a pas été racheté parmi captifs, à l’occasion de la trêve avec le soudan[^soudan] ?

— Pas que je sache. Le frère Raymond, qui s’occupe des tractations avec Abu Malik al-Muhallab est absent ces temps-ci. Il craint que les pillages vers Panéas ne rendent sa tâche bien difficile. Vous devriez demander à frère Rostain, c’est lui qui reçoit les malades. »

Ernaut se retourna machinalement en direction de l’hospitalier qui s’occupait de l’accueil, qu’il ne pouvait apercevoir, à l’autre extrémité de la vaste salle.

« Qui est ce À Boule Male Cale Mourre à l’Arbre ?

— Abu Malik al-Muhallab. Un négociant damascène. Il œuvre généralement à nos côtés lorsque nous essayons de racheter pérégrins indûment gardés comme esclaves chez les sarrasins. Il vient souvent en la cité, au service de parentèles ennemies pour verser rançons de captifs détenus en les geôles chrétiennes. Nous nous efforçons alors de l’assister, à notre tour.

— Vous vous souvenez quand vous avez recueilli Roussel ?

— Au moment de Carême l’an passé, à quelques jours près. Pour Pentecôte, il était tout à fait remis sur pied et œuvrait pour nous.

— Et depuis lors, il est resté à besogner ici ? Vous ne lui connaissez ni famille, ni camarades ?

— Je dois avouer que mes tâches me prennent beaucoup et je ne pourrais vous assurer de rien. Il s’est montré très dévoué, jusqu’à sa désertion récente. Il laborait sans regimber, sans jamais trouver aucune tâche trop ardue ou se dire trop las pour s’en acquitter.

— Par hasard, mon frère, sauriez-vous où il logeait ?

— Nous lui avions indiqué un homme de notre connaissance, un fournisseur qui cherchait à louer. Il s’appelle Gushak, on le surnomme le Muletier, bien qu’il n’en soit pas un, je ne sais pourquoi. Vous le trouverez en la rue des Écrivains, vers le milieu de la rue du Mont Sion. Dites que c’est frère Stasino qui vous envoie.

— Mille mercis de votre aide, mon frère. Ces renseignements me seront fort utiles.

— Si vous le trouvez, dites-lui bien que je suis très marri et que je… »

Le moine parut soudain sur le point d’exploser, le sang lui montant au visage. Il brandit un doigt impératif vers Ernaut, les lèvres pincées.

« Qu’il a intérêt à avoir excellente excuse ! »

Ernaut prit garde à ne pas sourire à cette sortie pour le moins cocasse et fit un signe de tête solennel.

« Je n’y manquerai pas. Puis-je poser ultime question avant de vous quitter ?

— Faites.

— Avez-vous souvenir d’un nommé Amalric, qui fut ramené avec des captifs voilà quelques mois. Il était rongé de gangrène et serait mort en vos lits.

— Cela ne m’évoque rien dans ma rue, mais je peux vérifier si vous attendez quelques instants.

— Je ne bouge pas de là, j’ai visitance à faire au mien frère, juste à côté. »

Le moine hocha la tête, marquant son accord, avant de partir. Le jeune homme eut à peine le temps de faire quelques enjambées qu’il fut intercepté par un serviteur, un bassin et une aiguière à la main, une serviette sur le bras.

« Excusez-moi, je vous ai entendu parler avec le chef de salle, à propos de Roussel. »

Ernaut acquiesça silencieusement, invitant son interlocuteur à continuer d’un signe de tête.

« Il l’aimait beaucoup et se fait du souci. Il sait que Roussel s’emportait facilement, mais le dira pas.

— C’est-à-dire ?

— Je l’ai déjà vu malmener d’autres valets ou s’emporter de colère, jusqu’à briser des objets. Heureusement, il a toujours réussi à se maîtriser face aux malades, du moins quand frère Stasino était là. Josselin, le simplet à qui vous avez parlé au début, pourrait vous en raconter sur ce gars ! »

Ernaut était contrarié par ces confidences spontanées qui lui inspiraient de la méfiance. Il dévisagea l’homme, d’une cinquantaine d’années, qui ne paraissait pourtant pas fielleux.

« Pourquoi vous me dîtes ça ?

— C’est tout simple, je ne l’aime pas, Roussel. Il me paraît franc comme un âne qui recule. Je suis sûr qu’il a fait un mauvais coup.

— Il a fait des choses interdites ici ? Il s’est attiré des ennuis ?

— Oh non, certes pas. Toujours à faire du zèle, à aller prier pour un oui pour un non. J’ai toujours su que c’était un faux jeton. Maintenant qu’il s’est ensauvé, j’en ai la preuve. Alors quoi que vous lui vouliez, méfiez-vous-en comme d’un cheval boiteux !

— Merci de ces nouvelles. Je saurai les garder en tête. »

Ernaut regarda le serviteur retourner à ses travaux, présentant le bassin et le broc aux malades pour qu’ils se lavent les mains avant le repas. Il paraissait plein d’attention auprès des indigents et leur parlait respectueusement, ainsi qu’on l’aurait fait à une personne d’importance. Son empressement ne semblait pas feint et l’accueil chaleureux qu’il recevait de ses patients montrait bien qu’il était apprécié. Que pouvait bien avoir fait Roussel pour s’attirer l’inimitié d’un homme pareil ?

Il en était là de ses réflexions lorsque frère Stasino le retrouva. Il lui confirma qu’il n’avait pas eu un tel patient dans sa rue, mais qu’il y en avait eu un dans celle d’à côté, pendant un long moment. Il avait été enterré depuis plusieurs semaines, ayant succombé à une gangrène qu’il avait contractée pendant sa captivité chez les païens. Il avait été racheté en même temps que des pèlerins ramenés là dans l’été dernier.

Cela correspondait bien au groupe de Nirart, Phelipote, Ylaire et Oudinnet. Ernaut le remercia avant de prendre congé. Il n’avait pas encore vu son frère de la journée et reprit la direction de son lit. Tandis qu’il était perdu dans ses pensées et marchait un peu comme un automate, il entendit passer dans la foule quelques serviteurs, frappant des bois l’un contre l’autre : l’Église appelait les fidèles pour les célébrations du jour, l’office de la montée au Calvaire. Le calendrier liturgique semblait résonner étrangement avec les sentiments du jeune Bourguignon. Abandonnant l’idée d’aller voir Lambert, l’esprit confus, il prit le chemin du Saint-Sépulcre, comme s’il avait lui-même la Croix sur les épaules.

### Fin d’après-midi du vendredi 29 mars 1157

Tandis que les derniers plaignants sortaient, Arnulf en profita pour se réinstaller à son aise sur sa chaise curiale, dont le coussin avait glissé. Il attendit que la porte de la salle soit refermée, car le sujet qu’il voulait aborder ensuite, à la fin de cette réunion de la Cour des Bourgeois, était assez sensible.

Il laissa les notables assemblés discuter un peu entre eux, commentant la décision préalable ou anticipant des nouvelles qu’ils allaient apprendre. Il échangea quelques paroles aimables avec l’un d’eux, qui sortait d’une longue maladie, à tel point qu’on avait craint pour sa vie. Puis il frappa dans ses mains pour ramener le silence, toussant pour s’éclaircir la voix.

« Je vous en prie, maîtres, tôt maintenant il nous faut aborder soucieux problème, même s’il ne dépend de votre unique pouvoir. »

Il prit une pause, dévisageant les hommes au premier rang devant lui, marquant avec emphase ce qu’il allait dire.

« Vous l’assavez tous, nous avons derechef découvert mortelle dépouille d’un malheureux, frappé de moult coups de coutel. Outre, il s’avère que le dit Nirart, époux d’une des deux femmes, a été meurtri plus avant dans la semaine. »

Un léger brouhaha commença à monter à cette annonce. Arnulf leva la main en signe d’apaisement, indiquant qu’il souhaitait poursuivre.

« Néanmoins, la situation a changé. Le meurtrier ne les a pas violentés comme les autres et il a perdu objet qui devrait nous permettre de le dévoiler. Un des plus habiles sergents du mathessep n’épargne guère sa sueur en ce sens. Je suis désolé de ne pouvoir vous en conter plus, nous apensons qu’il est plus sage de rester discret sur cet élément. La surprise pourrait enfin être nôtre. »

Pierre Salomon, qui se trouvait au premier rang de ses pairs, fit un pas en avant, attendant l’autorisation tacite du vicomte pour prendre la parole.

« Il est aise de voir que vous pensez porter conclusion à ce souci. La situation va de mal en pis. Il ne faudrait pas que les pérégrins arrivés pour ces Pâques ramènent dans leurs foyers horribles fables qui effraieraient leurs proches et les inciteraient à ne pas venir en la cité. Mais aussi, je me demandais à quel titre nous pourrions nous immiscer pareillement en une affaire qui ne nous concerne pas directement. »

Il se tourna à demi vers les autres membres de la Cour, cherchant sans doute à s’y attirer des soutiens.

« Comprenez-moi, je suis certes conscient que nous ne pouvons laisser les choses empirer, sans compter qu’il est toujours possible que des familiers se fassent connaître et demandent réparation. Nous nous devons donc d’être prêts. Malgré tout, il faut absolument que nous n’allions pas au-delà de notre mission. L’aspect légal est à considérer. »

Le menton dans la main, Arnulf hocha la tête en assentiment.

« Vous parlez vrai, maître Salomon. C’est un point qui m’inquiète aussi et c’est pourquoi je l’ai abordé avec le sénéchal. »

À cette mention, plusieurs bourgeois écarquillèrent de grands yeux, impressionnés de voir si important personnage s’impliquer dans cette histoire. La légère suspension dans le discours du vicomte montrait bien qu’il avait fait cette déclaration à dessein. Il continua de la même voix tranquille, certain désormais que beaucoup accorderaient une écoute attentive à ce qu’il était en train d’expliquer.

« Il m’a confié que le roi lui-même est fort troublé par cette histoire, qu’il prend très à cœur. Ces victimes tombent auprès du palais, et à bien effrayante rapidité. Outre, nous ne pouvons expliquer le pourquoi. Un meurtre pour larcin ou dispute, cela n’a que peu de gravité pour la communauté. Là, nous avons vrai souci, car c’est affront à notre pouvoir, ce danger qui menace sans que nous ne puissions remonter à sa source ni en débusquer les motifs. Il nous faut donc traquer les démons qui s’agitent en sous-sol afin, non pas de réparer car, vous le dites fort droitement, ce n’est pas à nous de le faire, mais pour prévenir tout débordement, afin d’empêcher que cela n’offense trop durement la communauté dont nous avons la charge. »

L’assemblée approuvait silencieusement, qui de la tête, qui d’une moue d’acquiescement. Geoffroy de Tours, jeune homme plein d’enthousiasme et au verbe haut, prit la parole et s’avança à son tour devant ses pairs, un sourire amusé passant sur son visage.

« Je suis heureux de voir que nous sommes en accord sur ce point. Cela n’est pas coutume, il m’apparait donc sage de le pointer. Toute cette affaire nous échappe, voilà évidence. C’est pourquoi je pense qu’il faudrait aller au-delà. »

L’assemblée retint son souffle, suspendue aux lèvres de l’orateur talentueux qui déclamait, une fois n’était pas coutume, assez sobrement, sans grands effets de manche.

« Nous devrions prendre leçon de toute cette histoire. Nous avons jusqu’à présent toujours laissé de côté ces affaires de meurtreries, nous contentant d’arbitrer entre plaignants et défenseurs. Voilà terrible faille que ne manque pas d’étendre le maudit qui nous nargue. Il frappe personnes sans nul moyen de défense, esseulées, loin des leurs, abattant chacun pouvant venir devant nous demander réparation. Nous sommes bien armés pour traiter des cas, disons normaux, mais lorsque le Malin brouille les pistes, nous sommes aussi démunis qu’Adam au sortir du jardin d’Éden. »

Un des jurés, habillé comme un Oriental malgré un physique nettement européen, l’interrompit, le sourcil froncé.

« Que voulez-vous dire ? Que les Assises et coutumes ne suffisent pas en cette affaire ? Qu’il nous faudrait en quelque sorte établir nouvelle loi, ou du moins, agir différemment de nos usages ?

— Justement ! Loin de moi l’idée de trahir la mission sacrée pour laquelle le roi m’a désigné. Malgré cela, je ne pense pas qu’il soit contraire à l’esprit de cette tâche que d’œuvrer au maintien d’un équilibre profitable à tous. Voilà, je le crois, le vrai sens du rôle de notre noble cour. »

Pierre Salomon, qui n’était pas né de la dernière pluie, leva un doigt, comme pour interroger un professeur, ou ainsi que le ferait un enseignant souhaitant reprendre un élève.

« Nous ne pouvons que vous suivre sur cette voie, bon ami. Tout ce que vous narrez là me semble évidence, ainsi qu’à tous nos féaux ici réunis. Mais où voulez-vous en venir ? Quelle résolution souhaiteriez-vous nous voir adopter ? Une déclaration d’intention n’est d’aucune utilité en l’occurrence. Ce qu’il nous faut, c’est une solution.

— J’y viens, maître, j’y viens. Nous devrions renforcer la sergenterie qui nous sert. Donc adopter une résolution qui demande au sénéchal de nous octroyer plus grands moyens de finance pour accomplir notre tâche. »

Le vicomte, un peu crispé, se tourna vers le mathessep qui ne bronchait pas, debout devant la porte, les bras croisés. Arnulf se demandait si son serviteur n’avait pas été dans la confidence. Il n’aimait pas être ainsi mis devant le fait accompli, et n’apprendre qu’en audience les projets et plans des hommes de l’assemblée, quand bien même il approuvait la motion.

Il allait devoir s’inquiéter de ce qui se tramait dans son dos tandis qu’il essayait de faire front à tous les ennuis. Apparemment, aucun autre juré n’était au courant de la suggestion de Geoffroy et, la surprise passée, si quelques-uns critiquaient une proposition conçue à la hâte, en réaction directe à des événements, trop influencée par les circonstances pour avoir un quelconque fondement intelligent, d’autres semblaient adhérer au point de vue présenté. L’orateur attendait, de côté, face au vicomte, espérant une première réponse de la part d’un de ses collègues. Aucun ne s’avança. Ce fut donc Arnulf qui prit la parole, se grattant la tête et s’exprimant comme à regret, lentement.

« Je ne voudrais peser sur le choix qui demeure vôtre, maîtres. Mais je me dois de vous signifier que le trésor royal n’est guère ventru. Comme vous le savez, la campagne avec les rois de France et d’Allemagne a grandement vidé les coffres et nous n’avons toujours pas retrouvé solide situation. »

Geoffroy prit quelques instants pour réfléchir puis répondit, de façon posée.

« Il me semblait que les prises récentes en la région de Panéas avaient fait du mieux au trésor royal.

— De certes, bien que je ne pourrais vous en faire le détail, n’y ayant pas participé moi-même. Cela constitua certainement un geste du Seigneur envers son humble serviteur à un moment où cela devenait plus que nécessaire. Cela n’a pas permis d’amasser grandes réserves, seulement de combler déficits passés. »

Le jeune juré parut embarrassé, n’ayant visiblement pas escompté une telle réponse. Le vicomte s’en voulut, car il rejoignait le point de vue développé. Il aurait volontiers vu les effectifs à ses ordres augmenter et œuvrait également à cela de son côté. Il jugea donc nécessaire de ne pas laisser la proposition mourir ainsi.

« Toutefois je m’en voudrais d’influencer ainsi une décision qui demeure vôtre. Je tenais simplement à vous avertir de ce que la décision qui vous sera signifiée risque fort de se voir influencée par des questions d’ordre bassement comptable. Je ne peux préjuger pour autant de celle-ci. »

L’intervention du vicomte avait coupé l’effet d’annonce de Geoffroy et les jurés commencèrent à échanger entre eux, mais aucun ne prit la parole solennellement. L’orateur se tenait immobile, attendant de voir si sa recommandation allait tomber à plat ou faire son chemin dans la tête de ses compagnons.

Pierre Salomon fut le premier à s’avancer vers lui, cassant le rang qui s’était formé pour écouter. Il semblait intrigué et soucieux de la proposition, pourtant son attitude indiquait qu’il ne trouvait pas l’idée saugrenue. Il interpela amicalement Geoffroy et lui posa quelques questions, l’entraînant de côté.

Arnulf inspira lourdement, il était d’accord avec le principe de la demande mais il savait que l’administration royale n’accepterait certainement pas. Baudoin avait de nombreuses contraintes financières et préférerait naturellement attribuer ses moyens à l’armée, conscient que les hostilités avec le seigneur d’Alep[^nuraldin] allaient recommencer. D’autant plus que depuis le décès de leur allié Mu’in al Dīn Anur, Damas était entrée dans le giron du fils de Zankī.

### Soir du vendredi 29 mars 1157

La rue de Malquisinat était un peu moins animée qu’à l’ordinaire. En cette journée de recueillement, après les cérémonies qui avaient commémoré la Passion et la Crucifixion du Christ, l’ambiance dans la ville était morose, aucune festivité n’étant bien évidemment autorisée. Partout, les croix étaient recouvertes d’un drap en signe de deuil, les statues voilées, et nulle cloche ne résonnerait plus jusqu’aux offices célébrant la victoire du Christ sur la mort. Une chape de tristesse semblait s’être abattue sur la Cité, ralentissant toutes les activités, incitant chacun à l’introspection.

Beaucoup de fidèles ne se contentaient pas de faire maigre et jeûnaient complètement dans l’attente de la Résurrection qui serait fêtée le dimanche. Plusieurs commerces étaient d’ailleurs fermés parmi les marchands de plats, dont celui de Margue l’Allemande. N’ayant prévu aucune réserve, Ernaut se contenta de pain non monté et de quelques fruits secs, ainsi que d’un pichet de vin coupé. L’après-midi passé à assister aux célébrations l’avait fatigué et ne lui avait été d’aucun réconfort. L’air abattu, il lançait aux quelques pigeons autant de miettes qu’il en avalait, avachi contre le mur, assis sur une pierre. Ce fut la voix amicale d’Eudes qui le sortit de sa torpeur.

« Je suis aise de te trouver finalement ! J’avais crainte que tu ne sois encore hors les murs. »

Le jeune homme se tourna vers le sergent et lui sourit sans joie.

« J’ai nécessité de dormir en bon lit cette nuitée. Je n’ai pas joint le groupe du père Ligier, il me fallait tout de même aller quérir nouvelles de la santé de mon frère… »

Eudes hocha la tête et s’assit à côté d’Ernaut, l’interrogeant du regard avant de prendre une gorgée au pichet. S’essuyant la bouche du revers de la main, il adopta un air satisfait :

« J’ai appris quelques noveltés. Je sais l’héberge du négociant qui aide les frères de Saint-Jean pour les captifs. Ils le logent en une de leurs maisons de la Juiverie. »

La nouvelle anima les traits d’Ernaut, qui retrouva un peu de son énergie perdue.

« L’as-tu vu ? As-tu parlé avec lui ?

— Pas encore. J’avais fort à faire. J’ai eu accord d’aller le questionner, et je me suis dit qu’il serait justice que tu sois à mes côtés. Si tu en as l’envie, tu peux te joindre à moi.

— Avec grand plaisir, d’autant que nous pourrons ensuite faire visite au logeur de Roussel, qu’on m’a indiqué. »

Acquiesçant joyeusement à la proposition, Eudes se leva et ajusta sa cotte, redressant le fourreau de son épée.

Ragaillardi par la perspective des avancées possibles, Ernaut se dépêcha de rapporter le pichet, à demi bu seulement, et suivit son ami, frottant d’une main les quelques reliefs de son repas qui s’étaient accrochés à son vêtement.

Il ne leur fallut guère de temps pour trouver la maison qu’on avait décrite à Eudes. Leurs pas rapides résonnaient entre les hauts murs, et les dernières lueurs de la journée tentaient d’insuffler encore un peu de vie dans les passages étroits, pratiquement déserts. De temps à autre, une lumière orangée jaillissait furtivement d’une porte, laissant entrer ou sortir une silhouette discrète. Aucun des deux amis n’ouvrit la bouche durant le trajet.

Un jeune garçonnet les fit pénétrer, après les avoir interrogés par un guichet et s’être enquis de ce qu’il devait faire. Il les mena jusqu’à une pièce spacieuse, dont les fenêtres avaient été fermées de volets de bois ouvragé. De nombreuses nattes et kilims recouvraient le sol. Un coffre peint et quelques tabourets octogonaux étaient disposés ici et là, portant plusieurs lampes à huile. Des coussins et traversins étaient disséminés dans la salle, servant visiblement de sièges.

Un syrien dans la force de l’âge les y attendait, habillé d’un simple *thawb* de laine à rayures, un petit bonnet sur le crâne. Pas très grand, il avait la musculature empâtée de quelqu’un qui avait renoncé à une activité physique qui l’avait puissamment charpenté. Son visage solide, massif, arborait un regard triste. Néanmoins, sous ses épais sourcils qui se rejoignaient presque, ses yeux s’agitaient en tous sens, jaugeant les deux hommes qui s’avançaient vers lui, et surtout le géant qui avait de la peine à passer sous les portes. Il leur sourit, faisant apparaître une dentition incomplète et les invita à s’assoir. D’un geste, il indiqua à son jeune domestique d’apporter de quoi se rafraîchir.

Ils eurent à peine le temps de se présenter que des verres et quelques friandises leur étaient proposés. Abu Malik fit le service, cordial, puis attendit, silencieux, qu’on lui explique les raisons exactes de leur venue. Eudes prit la parole.

« Mille grâces de nous recevoir si tard en votre logis, nous avons si fort souci que nous ne pouvions attendre outre.

— Je vous en prie, j’ai toujours plaisir à prêter assistance aux amis des frères de Yahya[^yahya]. J’ai cru comprendre que cela a trait à captifs libérés ?

— Si fait. Nous cherchons trace d’un homme, affranchi avant les Pâques dernières. Un rouquin aux yeux torts. Il était peut-être porteur d’une croix d’émaux. »

Le négociant sourit, chassant l’idée d’un revers de la main ainsi qu’il l’aurait fait d’une mouche.

« Nulle chance qu’il ait pu conserver pareil bijou avec lui. Quand bien même ses geôliers auraient toléré pareil symbole de sa foi, ils n’auraient jamais hésité à s’emparer d’une valeur.

— Il aurait pu la celer par-devers lui…

— On voit bien que vous n’avez pas idée de ce qui attend les victimes de ces prises ! Ils ne valent guère plus que des chiens. On les parque, on les scrute, on les fouille, on les dépouille. De tout… Parfois même de leur âme. »

Ernaut ne put rester muet.

« Les païens font quoi à ceux qu’ils prennent ? Et pourquoi le font-ils ? »

Abu Malik tiqua et répliqua, un peu piqué au vif :

« Nous ne sommes pas païens, ami. Nous croyons en un seul Dieu et en son prophète Muhammad — Il fit un geste d’apaisement de la main — Mais ce n’est pas là la question, vos frères chrétiens ne se montrent guère plus charitables lorsqu’ils mettent la main sur leurs ennemis.

— Vous êtes musulman ? S’esclaffa Ernaut, qui prenait conscience du fait tandis qu’il le disait à voix haute. Je croyais que la cité vous était interdite !

— Oui, je le suis. Et certes, il m’est habituellement défendu de résider ici. Je n’ai le droit de voyager que par licence royale et de demeurer ici que par l’hospitalité des frères. »

Ernaut hocha la tête, un peu désolé de s’être emporté ainsi. Abu Malik lui sourit amicalement avant de reprendre.

« Je suis fidèle sujet du roi Baudoin et je m’acquitte du *kharaj*[^kharaj], ainsi que de mes cens, comme n’importe qui. Tant que je respecte mes devoirs, j’ai permission de vivre ma foi. Il y a même une mosquée en mon casal… »

Le négociant se pencha en avant, adoptant une attitude posée, cherchant ses mots, les mains jointes.

« Malgré les risques, je suis en la Cité en cette période, car il y a grande inquiétude des troubles qui pourraient jaillir de l’assaut du roi. La trêve rompue, Mahmud ben Zankī[^nuraldin2] va sûrement sauter sur l’occasion de se venger. J’ai encore quelques amis à Dimashq[^dimashq] qui peuvent aider à prévenir cela… »

Eudes s’avança à son tour, l’air intéressé.

« Vous êtes un diplomate ? Je vous croyais œuvrer au négoce.

— Pourquoi faire l’un excluerait l’autre ? Je suis homme de paix avant tout et j’ai fui ma cité, soumise aux Turcs belliqueux et barbares. J’ai fini par être acertainé que tous ces malheurs n’étaient que le fait des hommes, de l’avidité à soumettre l’autre. De nombreux frères de Yahya partagent cet avis, et ne sont guère heureux de voir leur ordre s’armer. Alors, nous nous efforçons d’aider au mieux en œuvrant à libérer les captifs, où qu’ils soient. »

Les trois hommes demeurèrent silencieux un long moment, jusqu’à ce qu’Abu Malik reprenne, se tournant vers Eudes.

« Pour en revenir à votre question, je n’ai pas souvenir d’un tel homme.

— Et sinon avez-vous aidé au retour d’un groupe, avec femme et enfançons, qui auraient rejoint la cité voilà une poignée de mois ?

— Certes. Grâce à la trêve avec les Égyptiens, moult prisonniers ont pu être rendus à la liberté. J’étais en personne présent lors de leur arrivée. À quoi ressemblaient ceux que vous cherchez ? »

Eudes décrivit rapidement le petit groupe, en terminant par Amalric, dont il ne savait que les soucis de santé. Ce fut lui qui déclencha un hochement de tête affirmatif chez le musulman.

« Il y a avait bien ces gens, j’en ai clair souvenir. Ils n’étaient que peu à avoir assez de force pour tenir brancard pour les plus faibles. Ils étaient tous si piteux ! Surtout le garçonnet, guère plus vaillant que nourrisson. Si maigre qu’il ne pouvait marcher qu’à grand-peine. Comment oublier son petit corps malingre ?

— Et pas de rouquin aux yeux torves avec eux ?

— Non. Ils avaient été pris alors même qu’Asqalan[^asqalan] était tombé aux mains de Baudoin. De nombreux pirates écument toujours les côtes, capturant, pillant selon leur gré. Qu’on soit chrétien ou musulman leur indiffère, ils vendent à l’ennemi tous ceux qu’ils prennent. »

Eudes glissa un regard à Ernaut, au cas où ce dernier aurait eu envie de poser une question. L’adolescent hésita longtemps, ayant de nombreuses interrogations, mais aucune ne concernait directement leur affaire et il craignait de se montrer impoli avec leur hôte. Il était intrigué par l’apparente bienveillance d’un homme qui était l’ennemi de sa Foi. Il finit par se lancer, rendu confiant par l’excellent accueil qu’ils avaient eu jusque-là.

« Une ultime question, si vous le permettez, maître Malik…

— J’écoute, répondit le négociant, inquiété par ce ton cérémonieux.

— Est-ce qu’il serait possible que les geôliers paï… musulmans aient converti un prisonnier ? L’aient convaincu de venir ici, en se faisant passer pour chrétien, pour y répandre le mal ? »

Abu Malik réprima un sourire, bizarrement amusé par l’hypothèse.

« Impossible !

— N’avez-vous pas dit vous-même que chaque camp avait en ses rangs des hommes prêts à tout ?

— Certes oui, mais il est interdit chez nous de garder un fidèle comme esclave. Si jamais cet homme s’était converti, avait reconnu Allah et son prophète, suivi ce que nous dicte le Coran, il aurait été immédiatement affranchi. »

Eudes renchérit :

« D’ailleurs, il est connu que lors de la prise des cités, il est offert de se convertir pour demeurer en son hostel.

— Le seigneurs francs font de même, y compris avec les Turcs, qu’ils n’hésitent pas à employer comme soldats, pour peu qu’ils aient accepté le baptême » ajouta Abu Malik.

Ernaut baissa la tête, convaincu par ces éclaircissements. Il ne voyait pas ce qui pouvait pousser cet ancien soigneur à frapper ainsi de pauvres femmes. En tant que pèlerin nouvellement arrivé, il pensait que des démons avaient pu s’emparer de son âme. La conversion à la foi ennemie aurait pu expliquer cela. Pourtant cela n’était apparemment pas possible.

De plus, Abu Malik en était la preuve vivante : des hommes de bien existaient chez les musulmans, prêts à tendre la main à ceux qui recherchaient également la paix. Et cette dernière découverte était encore plus troublante que les autres. Si l’adversaire n’était plus aussi facile à identifier et que la bonté se cachait en des endroits inattendus, comment pouvait-on débusquer le mal sans se perdre en chemin ?

### Nuit du vendredi 29 mars 1157

La ville était emplie de mendiants, de miséreux arrivés de tout le royaume pour assister aux cérémonies. Et malgré l’accueil que leur faisaient les ordres religieux, beaucoup finissaient dans la rue, dormant pelotonnés dans un recoin, abrités des courants d’air et du bruit du mieux qu’ils le pouvaient. Certains venaient en famille, avec femme et enfants, et repartaient aussitôt les célébrations achevées.

L’afflux de pèlerins européens était également à son apogée, tous ayant convergé pour ces quelques jours vers la cité sainte, qui n’était habituellement que moyennement peuplée. Obligés de traverser presque toute la ville, Eudes et Ernaut voyaient un grand nombre de ces voyageurs épuisés tandis qu’ils descendaient tranquillement vers le sud.

Pendant le trajet, Eudes commentait à Ernaut la hausse des denrées alimentaires qu’il avait pu récemment constater, sans compter les abus et les tentatives de fraude, plus fréquentes aussi. Selon lui, tous les ans, c’était la même chose. Les habitants veillaient donc à faire leurs provisions avant la Semaine sainte, dès Carême, pour ne pas avoir à subir l’inflation des prix engendrés par la forte demande. Mais les plus misérables, qui n’avaient pas la possibilité de faire l’avance, se trouvaient souvent en difficulté. Sans compter que c’était le moment où il fallait régler les loyers, les taxes. C’était pourquoi cette série de meurtres tombait vraiment mal. L’administration royale était déjà débordée et les hommes qui la composaient fatigués.

« Comme les attaques ont été commises dans le quartier du Patriarche il a fallu, ainsi que tu l’as vu, en référer à ce dernier, ainsi qu’aux chanoines du Saint-Sépulcre, qui l’entourent. Ils sont fort pointilleux quant au respect de leurs prérogatives. »

Ernaut souleva un sourcil étonné.

« Vu que c’est un crime de sang, je croyais qu’ils n’avaient rien à voir.

— De vrai, mais ils sont toujours méfiants de voir des hommes du roi se balader chez eux, s’enquérir de choses et d’autres. Ils craignent qu’on empiète sur leurs droits. »

Le sergent réfléchit quelques instants puis reprit.

« C’est d’ailleurs idem pour l’hôpital de Saint-Jean qui, en plus, a souvent maille à partir avec les chanoines et le patriarche. Il y a peu, Foucher[^foucherangouleme] est parti auprès du pape pour essayer de le convaincre d’annuler les privilèges accordés à l’ordre du vieux Raymond[^raymondpuy]. Les chanoines ont encore en travers de la gorge la mise en place de la grosse cloche par les frères, voilà deux ou trois années. Les hospitaliers prenaient grande joie à les sonner à toutes volées chaque fois que le patriarche tenait sermon. Pas vraiment de quoi calmer la situation ! Au final, le pape a maintenu les exemptions, mais plus de retenue de leur part a été demandée. »

Fronçant les sourcils, Eudes parlait avec plus d’agitation.

« Je ne serais pas mésaise que le palais royal s’en départe de là, que nous n’ayons plus guère à nous rendre en service dans ces rues. À charge pour les clercs de garantir ordre et sécurité tout seuls ! Ils encaissent taxes et loyers, qu’ils s’en débrouillent donc un peu. »

Tout en discutant, ils ne tardèrent pas à arriver à la rue des Écrivains. C’était un étroit passage qui partait à angle droit, montant doucement vers l’ouest, au sein du quartier arménien. On y trouvait toutes sortes de boutiques, surtout alimentaires et parmi elles quelques notaires qui rédigeaient contrats civils, inventaires après décès et autre paperasserie. D’où le nom de l’endroit.

Pour l’heure, tout était fermé et seuls quelques hommes étaient assemblés à discuter et boire autour d’un escalier. Ils échangeaient tranquillement, en faisant parfois quelques gestes de la main pour accompagner leur discours, dans une langue inconnue d’Ernaut. Plusieurs chiens dormaient, allongés en travers de la rue juste à côté d’eux. Quelques lampes à huile disposées aux abords de ce lieu de rencontre improvisé apportaient un peu de lumière. Eudes s’approcha sans parler, attendant qu’on fasse attention à lui pour ne pas déranger.

Le silence finit par se faire et l’un des hommes, parmi les plus âgés apparemment, une large barbe blanche étalée sur la poitrine, sourit et leva la main en guise de salut. Sa voix rocailleuse accrochait certaines consonnes comme des graviers roulés.

« Salut à toi, sergent. Il y a un problème ? »

Eudes s’accroupit pour ne pas toiser de haut le vieillard qui se faisait accueillant.

« Non, aucun. J’ai besoin d’encontrer Gushak, qu’on dit le Muletier.

— Il a fait quelque mauvaiseté ?

— Non. Il peut m’aider plutôt. C’est le frère de Jean, Stasino, qui m’envoie.

— Ce n’est pas un peu tard ?

— Il en va de la vie d’un enfançon. »

L’ancien hocha la tête tranquillement, ses yeux ridés semblant se fermer. Puis il inclina du chef vers sa droite. Là, un petit homme replet, au visage d’aigle, les paupières qu’on aurait dit cernées de noir, fit mine de réajuster son turban.

« Je suis celui que tu cherches. Que puis-je pour ton service ? »

Il parlait avec un très léger accent qui rendait sa voix agréable, voire sympathique. L’invitant d’un mouvement de la tête, Eudes se releva et rejoignit Ernaut, qui s’était tenu un peu en retrait, caressant une des bêtes qui était venue mendier un peu d’affection.

L’Arménien se mit debout à son tour, avançant en traînant des savates jusqu’à eux, près d’une arcade fermée servant de boutique en journée. Eudes vérifia d’un coup d’œil que personne ne pouvait les entendre et répondit en chuchotant.

« Nous sommes là, car un de tes hôtes a peut-être causé des problèmes. »

L’homme parut ébahi. L’indignation qu’il s’empressa de montrer semblait un peu trop marquée à Ernaut pour être tout à fait sincère.

« Ça alors ! Je n’arrive pas à croire que j’abrite un serpent sous mon toit. Qui est-ce ? Qu’a fait ce maudit chien ?

— Rien n’est sûr pour l’instant. C’est pourquoi il me faut le voir au plus tôt. D’où cette visite très tardive.

— Ne t’inquiète pas de cela. Je suis toujours heureux de porter aide aux hommes du roi. »

Le commerçant n’était apparemment pas tombé de la dernière pluie et savait se faire comprendre à demi-mot. Il n’était pas question pour autant de commencer à marchander avec lui sa coopération.

« Ce n’est pas à moi que tu rendras service. Mais à tous les habitants. L’homme est peut-être murdrier. Dieu seul sait la prochaine victime qu’il chasse ! »

La remarque fit mouche et Gushak adopta un air pensif.

« Quel est son nom ? Souffla-t-il, comme à regret.

— On le nomme Roussel, un des sergents de l’hôpital. Ce sont les frères qui te l’ont adressé. »

L’homme explosa brutalement, haussant le ton, ce qui fit se retourner ses compagnons qui avaient repris leur conversation.

« Ah ! Ça ne m’étonne mie ! J’ai toujours su que c’était maudit larron ! »

L’arménien se tut tout aussi brusquement et semblait hésiter à poursuivre, remâchant sa colère. Il reprit, d’une voix moins forte, toujours emplie de fureur.

« Il n’est plus chez moi. Je l’ai chassé. »

Ernaut ne put retenir un soupir de désappointement, adressant un regard ennuyé et nerveux vers Eudes, qui ne broncha pas.

« Pour quelle raison ? Il ne payait pas ?

— Oh si, aucun problème de côté là ! Il devait savoir que je me plaindrais aux frères en pareil cas. Mais… je ne l’aimais pas ! »

Il accompagna sa remarque d’une grimace de dégoût, comme s’il allait cracher sur le sol. Il semblait ne pas savoir par où commencer la liste de ses récriminations à l’encontre de son ancien locataire. Il lui fallut quelque temps pour se décider, mais les deux enquêteurs attendaient patiemment, suspendus à ses lèvres.

« Il frappait les bêtes pour oui ou non, vous savez ! Et ça, mon père, que Dieu le garde, m’a toujours appris à me méfier des hommes qui font ça sans nulle raison. C’est ça, de prime, qui m’a déplu. Puis il y avait fréquentes disputes avec mes autres hôtes, pour n’importe quoi. On aurait cru qu’il aimait à faire problèmes ! J’en étais venu à ne plus supporter sa voix, à force de l’entendre hurler. Pourtant, je me faisais raison, il travaillait pour les frères noirs de l’hôpital et je fais bonnes affaires avec eux, vous savez…

— Alors vous n’avez rien dit. »

Le regard de Gushak se fit affirmatif avant de devenir honteux.

« Non, et je m’en suis mordu les doigts par la suite. »

Le marchand resta coi quelques instants, visiblement ennuyé. Puis il revint à la charge, moins exubérant, comme si l’affaire le touchait vraiment.

« Je m’absente souvent. J’achète et je vends des fruits, frais et secs. Alors je vais de-ci de-là, visiter les casaux… bref, je m’occupe de mon négoce. Et un jour que je m’en revenais de Galilée, ma femme m’a conté que Roussel l’avait apeurée. »

Les bras croisés, Ernaut se rapprocha, curieux d’entendre la suite. Gushak eut un mouvement de recul instinctif, mais continua son récit.

« Il n’avait rien fait de spécial pourtant elle avait grande crainte. Elle n’a su m’expliquer le pourquoi. À partir de là, elle refusait de sortir quand elle le savait à l’entour.

— Qu’avez-vous fait ? Vous l’avez chassé ?

— Dieu m’est témoin que je ne juge pas sans preuve. Et vous connaissez les femmes ! Elles ont tout un monde dans leur petite tête. Je ne pouvais pourtant rester sans rien faire. D’autant que je m’étais toujours méfié de lui. Alors je suis allé le voir, histoire de vérifier s’il n’avait pas maligne idée en sa sale caboche. Je lui ai indiqué qu’il fallait qu’il fasse attention, qu’on s’était plaint à moi de son attitude envers certaines femmes. En tant que Franc, on comprenait qu’il soit peu au fait de tous nos usages, mais il se devait d’être plus respectueux.

— Qu’a-t-il répondu ?

— Il s’est mis à rire et m’a assuré que ce devait être un malentendu. Que lui-même était marié et que son épouse devait le joindre d’un jour à l’autre. »

Eudes en resta bouche bée. Ce n’était pas un simple vagabond !

« Quand est-elle arrivée ? »

Gushak pouffa de rire, sans joie.

« Peuh ! C’était du vent. Après, quand je le croisais, il disait tout le temps que son Asceline était en chemin. Qu’elle devait être retenue en route… Si au début je l’ai cru, j’ai finalement découvert que ce n’étaient que menteries. Vous comprenez, un homme marié, c’est quelqu’un de prude, de responsable, à qui un père a confié sa fille. Pareille marque d’estime m’incitait à l’indulgence. Alors quand j’ai réalisé qu’il se moquait de moi, de courroux, je l’ai chassé !

— Ça fait longtemps ?

— À peu près au moment où Baudoin était à Panéas. Lorsque le roi est rentré avec son butin, Roussel n’était plus là.

— Et vous savez où il est parti ?

— En enfer j’espère ! Ou du moins, le plus loin possible d’ici. Il a créé suffisamment de torts ! »

Eudes était visiblement contrarié et finit par regarder ses pieds comme s’il espérait y découvrir une solution à son problème. Ernaut, les mains sur les hanches, considérait les options qui leur restaient. Après un long moment de silence, Gushak, qui ne s’était apparemment pas calmé, interrompit leurs réflexions.

« Vous savez ce que ce gredin a eu front de me dire ? Qu’il était bien aise de quitter mon petit logement, car son épousée préférerait grandes demeures ! Quel impertinent ! À mon avis, il était fou et son Asceline n’existait qu’en sa tête de dément. »

Voyant le désappointement des deux enquêteurs, Gushak finit par se taire, serrant des mâchoires pour contenir son énervement. Il fouillait dans sa mémoire, espérant y trouver quelqu’indice susceptible d’aider à nuire à son ancien locataire. Ce fut donc avec un visage mauvais qu’il annonça :

« Allez donc fureter à l’entour de la taverne près de la demeure d’Ibrahim, au nord de Saint-Julien. Il y avait ses usages, comme nombre de sergents de l’hôpital… »

Rasséréné par l’indication, Eudes sourit à Ernaut.

« De vrai, il y aura certes quelques compaings pour nous en dire plus sur lui, en pareil endroit.

— Je crains pour vous que ce ne soit un peu tard pour vous y rendre. Le vendeur ferme tôt, surtout en ce jour. Il ne tient pas à avoir soucis avec les frères de Saint-Jean pour impiété. Le local leur appartient. »

Entendant cela, un sourire amusé éclaira le visage d’Ernaut. Il lui semblait que, où qu’ils aillent, l’endroit appartenait à l’hôpital ou au Saint-Sépulcre. À Vézelay, il se souvenait que les bourgeois avaient fort à faire pour se défendre face aux prétentions des clercs de la Madeleine. Ici, à Jérusalem, on aurait dit que la cité tout entière avait été partagée comme une tourte entre les puissants ordres religieux, sans que personne n’y trouve rien à objecter.
