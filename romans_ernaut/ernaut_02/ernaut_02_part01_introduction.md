## Remerciements

Écrire un roman dont l'intrigue se déroule lors d'une des principales fêtes religieuses médiévales autour du lieu le plus saint du christianisme constituait sans nul doute une gageure pour l'irréductible mécréant que je suis. Je tiens donc à remercier Martine et Joël Schmitz, amis et voisins peu distants, qui m'ont grandement éclairé sur certains aspects de la liturgie catholique. Les moments passés avec eux à tenter de différencier le Propre de l'Ordinaire ou d'identifier les références des Antiennes ont été parmi les plus agréables de la recherche documentaire. En outre, la traduction du texte original en latin et les explications fournies par Joël m'ont permis de mieux comprendre le rythme des célébrations et l'importance de certains moments.  
Qu'ils soient ici remerciés.

*Pour Elora, éclatante petite lumière*

## Plan de Jérusalem

![Jérusalem en 1157](ernaut2_plan-jerusalem.jpg)

## Plan du Saint-Sépulcre

![Le sanctuaire du Saint-Sépulcre](ernaut2_plan-saint-sepulcre-vertical.jpg)


## Liste des personnages

**Pèlerins**

+ Amalric  
+ Asceline  
+ Ernaut de Vézelay  
+ Gobert  
+ Gringoire la Breite  
+ Hersant de Bondies  
+ Jeffroy l’Englois  
+ Lambert  
+ Maciot Point l’Asne  
+ Libourc  
+ Marcel l’Espéron  
+ Nirart  
+ Oudinnet  
+ Père Ligier, prêtre  
+ Phelipote  
+ Pons de Mello  
+ Sanson de Brie  
+ Ylaire

**Hôpital de Saint-Jean**

+ Raymond du Puy, grand maître de l’Hôpital  
+ Frère Garin, hospitalier  
+ Frère Matthieu, hospitalier  
+ Frère Raymond, hospitalier  
+ Frère Stasino, hospitalier  
+ Joris, client[^client]  
+ Josselin, client  
+ Roussel, client  
+ Thomas, prêtre hospitalier

**Clercs du Saint-Sépulcre**

+ Foucher d’Angoulême, patriarche  
+ Amaury de Nesle, prieur du Saint-Sépulcre  
+ Giraud, chanoine

**Administration royale**

+ Arnulf, vicomte de Jérusalem  
+ Régnier d’Eaucourt, chevalier  
+ Basequin, sergent  
+ Baset, sergent  
+ Droart, sergent  
+ Eudes Larchier, sergent  
+ Fouques, sergent du vicomte  
+ Ucs de Montelh, mathessep[^mathessep]

**Bourgeois du roi**

+ André de Tosetus  
+ Geoffroy de Tours  
+ Pierre de Périgord  
+ Pierre Salomon  
+ Rainald Sicherius

**Habitants de Jérusalem**

+ Abdul Yasu, loueur de montures  
+ Abu Malik al-Muhallab, négociant et diplomate musulman  
+ Blayves le Boiteux, négociant en moutons  
+ Gushak le Muletier, négociant en fruits  
+ Margue l’Allemande, pâtissière  
+ Mile, gardien de Chaudemar  
+ Saïd, voisin d’Ernaut
