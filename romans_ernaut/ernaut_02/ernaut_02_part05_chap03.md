## Chapitre 3

### Jérusalem, église du Saint-Sépulcre, matin du dimanche 14 avril 1157

Ernaut posa la tête sur ses genoux rassemblés. Il soupira, regardant devant lui le va-et-vient des pèlerins qui entraient dans le tombeau par petits groupes de quatre ou cinq. Il avait fait de même quelques semaines plus tôt, tandis qu’il était encore plein d’espoir. Il allait être racheté de ses pêchés, purifié, et pourrait alors commencer une nouvelle vie dans ces régions prometteuses. C’était ce qu’on lui avait laissé entendre, ce qu’il avait escompté jusqu’alors. Mais il avait finalement parcouru un autre chemin, plus tortueux, qui l’avait mené en un lieu bien ténébreux. Son embarras était d’autant plus vif qu’il n’était pas assuré de le regretter. Car il avait eu aussi l’occasion de découvrir une lueur de paix là où il ne l’attendait pas.

Sans son insatiable curiosité, s’il était resté sagement occupé à ses dévotions, il n’aurait certainement pas fait cette rencontre troublante, enivrante. Ce n’était pas cet amour-là qui était célébré dans les églises lourdes d’encens, pourtant il lui semblait ressentir une émotion similaire et même plus intense encore à l’évocation de sa première discussion avec elle. Il savait faire la différence entre le désir et l’amour, qui avait une tentation plus virginale, et il inclinait à croire que c’était ce dernier qui animait son cœur, malgré toute la noirceur dont il se sentait recouvert. Peut-être cette lumière, porteuse d’espoir, constituait-elle une partie de la réponse à son problème.

À l’approche de tierce, un grand afflux de fidèles commençait à s’amasser vers le chœur de l’église du Saint-Sépulcre, à l’est, délaissant quelque peu le sépulcre du Christ. Ernaut se releva et continua à déambuler sous les voûtes jusqu’à rejoindre les abords de l’entrée principale. Devant les tombeaux des anciens rois latins se rassemblaient les files qui menaient vers le maître-autel. De nombreux habitants de Jérusalem s’étaient mêlés aux voyageurs, reconnaissables à leur fréquent recours à des éléments de costumes locaux. Parmi eux, quantité de marchands et de bourgeois de premier plan guidaient leur famille. Certains semblaient être là plus dans le but de rencontrer des amis et s’afficher zélés chrétiens que par foi véritable.

Pour autant, Ernaut ne leur jetait pas la pierre. Il se sentait relativement proche d’eux, finalement. Il ne fréquentait les lieux de culte que par habitude et n’avait que très rarement perçu la présence de Dieu en ces occasions. Il reconnut plusieurs visages de ceux qu’il avait aperçus à la Cour des bourgeois, quelques jours auparavant. Certains se contentaient de demeurer auprès du portail, discutant affaires avec leurs contacts tandis que leurs épouses, leurs enfants, se rendaient à l’office. Ernaut se demanda quelles étaient leurs véritables motivations. Il avait pu se rendre compte qu’un bon nombre se ralliait à quelques décideurs, et que parmi ceux-ci la plupart étaient pétris d’intentions louables, du moins en paroles.

Pourtant malgré toutes leurs richesses, leur volonté de bien faire, ils n’arrivaient guère à leurs fins. Quoique toutes les puissances locales aient été réunies lors de la tragique semaine de Pâques pour affronter l’adversité, rien n’avait semblé pouvoir s’opposer aux meurtres qui touchaient les pèlerins. Les frères de Saint-Jean, dont la vocation n’était jamais démentie, avaient apporté leur soutien et les chanoines du Saint-Sépulcre témoignaient de l’implication du Patriarche et de ses suivants. Malgré cela, des innocents avaient péri injustement, et les malfaiteurs les avaient nargués, dissimulés dans l’ombre.

### Jérusalem, rue du Mont Sion, matin du mercredi 27 mars 1157

Sifflotant pour lui même, Ernaut remontait la rue le cœur léger. Il avait revêtu sa plus belle tenue, de laine teinte à la garance et des chausses neuves, de fine toile jaune. Il s’était rendu aux bains avec un plaisir non dissimulé. Il avait toujours apprécié l’eau et la découverte qu’il avait faite des établissements moyen-orientaux l’enchantait. Pour un coût modique, il pouvait profiter d’un confort dont bien des seigneurs fieffés auraient rêvé dans leurs demeures de pierre. En sortant, il s’était fait raser dans une des boutiques aux abords de la rue du Maréchal, un peu à l’est du quartier de l’hôpital.

Résultat de ces soins, son humeur était excellente malgré les quelques gouttes de pluie et le temps maussade et froid dont il se protégeait par sa chape de voyage. Il avait en outre tenu à se mettre sur la tête le turban, grâce à l’intervention providentielle de Saïd, qui transforma un échafaudage branlant en une véritable coiffe. Des rafales rabattaient de la poussière dans les rues, dont heureusement un bon nombre étaient couvertes, partiellement si ce n’était en totalité. Il avait l’intention de se rendre au palais de Baudoin, pour tenter de voir Régnier d’Eaucourt, car il comptait sur la sympathie qu’il inspirait au chevalier du roi pour demander un peu d’aide. Non pas qu’il s’attendait à ce qu’ils enquêtent une nouvelle fois ensemble, les deux femmes assassinées n’intéressant certainement pas un homme d’un tel rang. Mais il saurait être de bon conseil et lui indiquerait les choses à faire et à éviter. En outre, si Ernaut avait l’occasion de reparler de Régnier, il espérait pouvoir impressionner davantage la jolie jeune fille brune qu’il chérissait secrètement. Alors qu’il arrivait aux abords du change des Syriens, il entendit un bruit de sabots : un important contingent de soldats montés, prêts au combat, avançait au pas, en direction du sud.

Couverts de poussière humide, ils avaient l’air d’avoir chevauché longuement, les épaules basses et le front légèrement affaissé. Ils étaient néanmoins imposants, la plupart habillés de mailles et le casque sur la tête, l’épée pendant sur la hanche. Le manteau noir marqué d’une croix blanche qu’ils arboraient par-dessus leur attirail militaire les désignait comme des frères en armes de Saint-Jean de l’Hôpital. Leurs chevaux éreintés, couverts de sueur épaisse, avaient également besoin de repos et seule une mission importante avait dû les obliger à chevaucher ainsi de nuit. Les bras croisés et les yeux emplis d’admiration, Ernaut assistait à leur passage comme s’il était devant un défilé. Plusieurs hommes habillés comme des colons avec quelques pièces de costumes locales mélangées à leurs tenues occidentales s’installèrent à ses côtés et commencèrent à commenter l’événement entre eux. Le plus âgé hochait de la tête tout en parlant :

« Ils s’en retournent de Panéas. Le connétable leur a fait don d’une part du fief et ils sont allés évaluer ses besoins, afin de la renforcer.

— Ils doivent craindre représailles des sarrasins, approuva son voisin, un grand échalas à l’allure de héron.

— Difficile de dire si Noradin va maintenir la trêve maintenant que le roi l’a rompue. Tel porte le bâton qui le fera battre.

— Quelle idée d’aller le moquer ainsi, tout était si paisible. Bien trop beau pour durer !

— Certainement encore idée de quelque baron qui avait appétit de cliquaille !. »

Le vieux pensa utile d’assortir sa remarque d’un crachat par terre, signe manifeste de son mépris.

« Et c’est encore les frères soldats qui doivent recoller pots brisés. Ils ont grand mérite !

— M’ouais, ils reçoivent quand même force terres en échange, des casaux entiers et même solides forteresses. Ils devraient pas se mêler de mener batailles. Les chevaliers du Temple suffisent bien à ça. L’Hôpital devrait demeurer à soigner pauvres gens, comme à son début. Le pauvre Gérard[^gerard] doit se retourner en sa tombe !

— Et le grand hôpital qu’ils viennent de bâtir, tu crois que c’est aux fins de guerroyer ? Ils ont dépensé sans compter, pour pouvoir accueillir grande quantité de malheureux…

— Pis il leur faut bien des terres, pour produire tout ce qu’ils donnent aux malades, ajouta un troisième larron, muet jusque-là.

— C’est bien pour ça qu’ils feraient mieux d’abandonner hauberts et lances et ne pas disséminer leurs richesses. Le frère Raymond est plus fier qu’un pet ! J’ai l’impression qu’il se prend pour le patriarche, des fois, ou même le pape. »

La dernière remarque tomba alors que le passage était de nouveau libre et que le petit groupe s’éloignait tranquillement, continuant à bavarder de façon animée. Ernaut les regarda partir et entendit leur discussion se mélanger au brouhaha engendré par les commerces affairés, les acheteurs curieux, les cris des marchands ambulants et les simples badauds qui conversaient.

Le marché aux poissons du début de la rue qui conduisait vers le parvis du Saint-Sépulcre empestait l’air de senteurs salines. De massifs esturgeons occupaient l’étal que longeait Ernaut et un client au nez retroussé paraissait peu convaincu de leur fraîcheur. Une femme à l’ample voile, le visage presque entièrement recouvert passa devant lui, un panier plein de mulets de bonne taille au bras. L’endroit était toujours très animé, en ces temps de Carême, et les éventaires se vidaient très vite de leurs plus belles pièces, ne laissant aux acheteurs fainéants que des prises à l’aspect fatigué, aux relents suspects.

Quelques oiseaux, corbeaux et pies-grièches traînaient aux abords, attirés par les insectes et petits rongeurs ou dans l’espoir éventuel de quelques mauvais coups au détriment des marchands. Alors qu’il dépassait les arcades qui longeaient au sud le parvis du Saint-Sépulcre, Ernaut fut tenté un instant d’y entrer. Quelques badauds déambulaient aux alentours comme toujour, et les marchands à la sauvette, vendant des colifichets souvenirs et des médaillons bénis, attendaient le chaland ou venaient proposer leurs produits aux croyants de passage.

L’escalier extérieur était envahi par une congrégation occupée à prier sous la direction d’un petit moine à la barbe drue. Le jeune homme leva la tête vers le beffroi qui surmontait le bâtiment, sur la gauche, presque aussi élevé que la coupole dont il apercevait le couronnement face à lui. Le long de la corniche au-dessus des portails sculptés, quelques pigeons déambulaient, grattant et roucoulant d’un air absent, indifférents à tout sentiment religieux. Un valet, armé d’un balai, dont la tâche devait précisément être de maintenir l’entrée propre malgré les volatiles, agitait d’un mouvement féroce son ustensile dans leur direction, sans grand effet.

Ernaut reprit son chemin tranquillement, car le palais royal se trouvait juste là, séparé par une ruelle des chapelles occidentales du parvis. L’accès en était d’ailleurs gardé par deux soldats à l’air taciturne, qui ne daignèrent même pas lever les yeux sur Ernaut tandis qu’il s’avançait vers la porte, sur sa gauche, en haut d’un escalier. Assis derrière une table sur le côté du passage, un sergent y était occupé à gratter avec un stylet plat une tablette de cire qui lui servait d’écritoire. Le cheveu court, il avait le visage tanné d’un natif, bien que ses traits semblassent plutôt occidentaux. En outre, sous ses épais sourcils bruns brillaient des prunelles claires et vives. Il s’interrompit un instant, dévisageant d’un air interrogateur le géant qui s’était avancé vers lui. Ernaut le salua d’un signe de tête et, se voulant affable, souria.

« Voilà, je viens céans, car j’aurais aimé encontrer un mien ami… »

L’homme ne bougeait toujours pas, adoptant un air indéchiffrable.

« Un chevalier du roi, nommé Régnier d’Eaucourt. Je ne sais comment le contacter. L’ultime fois où nous nous sommes vus, c’était à Acre. Le fonctionnaire sembla reprendre vie brutalement et se redressa.

« Il n’est pas là. Ou, du moins, je ne l’ai pas vu. Vous avez message à lui délivrer ?

— Rien d’important. Un conseil à demander simplement. »

Le sergent évalua Ernaut des pieds à la tête, fermant les yeux à demi comme s’il cherchait à découvrir un objet caché sur lui.

« Vous pourriez peut-être tenter votre chance auprès d’un des bourgeois du roi. Il y aura conseil plus tard en la matinée.

— Je dois m’y rendre justement, n’est-ce pas encore ?

— Non, après tierce, revenez à ce moment. Vous pourrez demander aide et conseil à maître Salomon, ou maître Geoffroy de Tours. Ce dernier est toujours fort avisé. Vous avez souci de quel ordre ?

— C’est personnel, pas un problème légal.

— Ah, je vois. Désolé. Messire d’Eaucourt est absent pour long moment en fait. Si vous me laissez tablette ou message à son intention, je pourrai lui transmettre dès son retour. »

Ernaut secoua la tête en dénégation.

« Ne vous donnez pas cette peine. Ce n’était qu’avis amical que j’aurais sollicité. Pas de quoi déranger tout l’hôtel royal. »

L’homme sourit aimablement et entreprit de noter quelque chose sur sa tablette, feignant désormais d’ignorer son interlocuteur. L’entrevue était terminée. Ernaut lança un œil vers la porte entr’ouverte au bout du passage qui donnait sur un jardin. Il salua du bout des lèvres le fonctionnaire et redescendit l’escalier avec un faux entrain.

Quelques gouttes d’eau s’écrasèrent sur les pavés tandis qu’il sortait de la ruelle, ne provoquant toujours aucune réaction des deux sentinelles censées en garantir la sécurité. Sentant qu’une averse était en train d’arriver, il tenta de remonter sa capuche, mais en fut empêché par son haut turban. Malgré tous ses efforts, il ne pouvait rabattre le vêtement. Énervé, il arracha son couvre-chef d’un geste vif et le roula dans sa besace. Sa bonne humeur s’en était allée, disparue sans véritable raison dans les abords du palais royal. Il ne pourrait pas parler de son ami le chevalier à une jeune femme de sa connaissance.

Sans même y réfléchir, il emprunta la rue du Patriarche, remontant vers le nord. Arrivant devant l’escalier qui descendait vers la chapelle Sainte-Marie et, de là, vers le tombeau du Christ, il stoppa. Il sentait qu’un moment dans le lieu saint ramènerait le calme en lui. La ferveur qui s’en dégageait, les nombreux fidèles occupés à se recueillir, tout incitait à l’introspection en cette période. Il franchit les marches, passa sous la représentation de Marie l’Égyptienne puis obliqua à droite. Descendant les quelques degrés qui menaient au rez-de-chaussée de la rotonde ceinturant le tombeau, il dépassa quelques groupes de pèlerins qui étaient assis parmi les colonnes et se dirigea droit sur l’édicule central. Sous le haut dôme arborant les portraits des prophètes, du Christ, de la Vierge et de saint Jean-Baptiste, ouvert à son sommet, se trouvait le sépulcre qui avait accueilli Jésus jusqu’à sa résurrection. Entouré de piliers et d’arches de marbre, on accédait à ce dernier par un passage à l’est, surveillé par des gardiens qui ne laissaient pas entrer plus d’une douzaine de fidèles à la fois.

Accolé à la structure sur sa face occidentale, un autel était encadré de grilles de fer ouvragé surmontées d’un ciborium recouvert en partie d’argent et surplombé d’un crucifix et d’une colombe. Des lampes étaient suspendues sous chaque arcade. Ernaut était déjà venu plusieurs fois, seul et avec Lambert. Ils avaient eu l’occasion de se rendre dans le tombeau lui-même, s’agenouillant respectueusement pour y entrer. Ils avaient pu en baiser le rocher, comme tant d’autres avant eux. Il était étrange pour lui d’avoir pu toucher un tel endroit. C’était comme si tout ce que les prêtres lui avaient raconté depuis son enfance devenait réel. Il avait admiré de ses yeux tellement de lieux où le Christ était venu qu’il n’était pas sûr de se souvenir de tous. Mais le Saint-Sépulcre était à part, là où le plus grand des miracles était advenu, pour le Salut de l’humanité.

Ernaut n’était pas sûr d’avoir bien saisi comment cela fonctionnait, ni pourquoi c’était arrivé, mais il avait pris conscience du fait dans cette église au cœur du monde chrétien. Il était ébahi, comme frappé de stupeur, écrasé par la majesté du bâtiment, les riches décors de mosaïque, les icônes, les fresques, le marbre coloré, les statues d’or et d’argent, l’encens qui y flottait en permanence, le scintillement des cierges dans le moindre recoin. La messe à laquelle il avait assisté plusieurs fois lui était apparue comme un magnifique cortège, avec des officiants recouverts de soie bigarrée, manipulant de saints objets et chantant des hymnes solennels qu’il ne comprenait pas. Confusément, il sentait la présence palpable de Dieu en cet endroit, comme jamais il ne l’avait ressenti auparavant. Et cela l’apaisait. S’agenouillant parmi quelques fidèles en oraison devant l’autel de l’Anastasis, il se joignit à leur récitation du *Credo*.

### Fin de matinée du mercredi 27 mars 1157

Les doigts du vicomte Arnulf tapaient en rythme sur la tête de lion qui décorait l’extrémité de son accoudoir, de plus en plus vite au fur et à mesure que son énervement allait croissant. Il avait passé la nuit en selle, à patrouiller dans les rues de la ville et il n’avait guère eu le temps de se reposer. Les jérémiades qu’il entendait commençaient singulièrement à l’agacer, lentement, mais sûrement. À l’approche des fêtes de Pâques, de nombreuses échéances tombaient et l’administration royale était donc bien occupée, ainsi que les bourgeois du roi qui apportaient leur concours à la bonne marche de l’ensemble. À chaque fois c’était le même cortège de récriminations. Sans compter les soucis récents, dont les meurtres de pèlerines n’étaient pas les moindres. N’écoutant plus vraiment, il finit par taper des phalanges sur le bois, demandant le silence. Les bouches se fermèrent et les visages se tournèrent vers lui. Il inspira lentement et se redressa sur son siège.

« Permettez-moi de vous rappeler qu’il n’y a nulle novelté dans les levées de cette année. Il faut que les cens[^cens] soient versés au plus vite car, vous le savez, le trésor royal doit faire face à moult dépenses. »

Le mathessep, qui était resté silencieux jusqu’alors, s’avança de quelques pas.

« Messire, je vous entends bien. Avec l’afflux de pèlerins, tous les ans à Pâques c’est la même chose. Nous ne suffisons pas à la tâche ! Il faudrait au moins requérir une poignée de sergents en sus, le temps de collecter tout ce qui est dû.

— Si l’on doit distribuer en soldes le gain qu’on est censé faire, je ne vois guère la pertinence. De toute façon, la Grand Secrète ne m’a pas accordé pouvoir ni moyens d’embaucher, maître Ucs. Je le regrette, il va falloir faire avec. »

Un des jurés de la Cour des Bourgeois, au ventre indicateur de sa bonne fortune, tenta d’apporter une solution.

« Avec la trêve, grand nombre d’hommes d’armes se retrouvent désœuvrés. Il serait aisé de les recruter, à bas coût. »

Un de ses confrères, plus jeune, le nez aussi acéré que le regard, dodelina de la tête d’un air désapprobateur.

« Je ne pense pas qu’il soit souhaitable d’attirer des soudards en la cité, en leur donnant quelques monnaies et le temps de s’enivrer. Il y a déjà bien assez de soucis comme ça… »

Son regard de chien battu suffit à tous pour comprendre ce à quoi il faisait allusion. Le mathessep se retourna et hocha la tête en assentiment, l’air désolé.

« Ce n’est par malheur pas la première et ce ne sera pas l’ultime. Outre, apprenez que le mari de l’une d’entre elles est ici. »

Il fit un signe de la main en direction d’Ernaut, qui s’avança parmi la foule. L’homme ventripotent le détailla des pieds à la tête, circonspect.

« Es-tu son époux ? Ou son fils ? »

La double interrogation arracha quelques rires narquois, vite éteints par le regard noir du vicomte. Ernaut ne prit même pas la peine de relever.

« Je ne suis qu’un pérégrin qui les a vus, à moult occasions. Les deux pauvresses étaient amies, je le sais sans doute aucun. L’une avait fils et l’autre mari. »

La stupeur s’afficha sur quelques visages.

« Sais-tu s’ils se trouvent en la cité ?

— Il semble, maître. Tout le monde est ici pour les Pâques.

— Vont-ils demander justice ? Savent-ils qui est le murdrier ? »

Ernaut haussa les épaules, sans avoir le temps de répondre avant que le mathesse ne prenne la parole.

« Je n’en sais mie pour l’instant. Aucun des sergents ne les a trouvés, mais ils cherchent. J’espère régler cela au plus vite.

— Il faudrait le faire dans les meilleurs délais, déclara avec emphase un jeune homme à l’allure vaniteuse et aux vêtements voyants. Si jamais cela se répand, les voyageurs vont s’enfrissoner et leur nombre se réduira encore. Nous avons déjà bien des difficultés à les faire venir, depuis la honteuse défaite de Damas. »

Le gros homme du début s’empourpra, remuant la mâchoire tel un chameau, n’arrivant pas à trouver les mots devant pareille provocation.

« Comment osez-vous qualifier ainsi la campagne du roi ? Il n’avait grand choix et c’était la plus sage des décisions. »

Le brouhaha recommença et les échanges s’envenimèrent rapidement, chacun tentant de faire valoir son point de vue sans même écouter l’autre. Seuls Arnulf, le mathessep qui s’était rapproché du vicomte et le jeune bourgeois aux traits fins gardaient leur calme, discutant entre eux sans s’invectiver. Lorsqu’ils parvinrent à un accord, qui se lisait sur les trois visages, Arnulf tapa dans ses mains pour ramener une nouvelle fois le silence.

« Maîtres ! Je vous prie de rester dignes. Nous ne sommes pas dans un marché aux bestiaux. Il est vrai que ces meurtres doivent être traité comme il le convient et nous y veillerons. Nous allons voir tout de prime ce qu’en dit le mari et si le félon peut nous être livré. Je vous propose donc qu’ordres soient donnés pour qu’on le retrouve au plus vite pour savoir son sentiment. Les frères de Saint-Jean et les chanoines du Saint-Sépulcre seront informés de cela. Outre, nous seront plus vigilants lors des rondes, montrant bien par là que nous ne traitons pas la chose à la légère. »

Le bourgeois ventripotent, qui s’était échauffé au point de devoir éponger son large front d’un mouchoir, prit un air désolé.

« Messire vicomte, je ne crois pas que nous devrions nous en occuper. Les sergents doivent avant tout privilégier les opérations de collecte. En cette histoire, le mal est fait et nous ne pouvons plus rien pour ces malheureuses pérégrines, si ce n’est prier. Alors je vous proposerai plutôt de laisser la famille s’occuper de ses affaires, vu qu’apparemment il y a des gens qui peuvent faire valoir leurs droits. Nous serons toujours à temps de nous en occuper alors, en second. Après les paiements. »

Quelques visages opinaient alentour, montrant le soutien qu’il avait auprès de ses pairs. Pierre Salomon était un intellectuel dont le pragmatisme l’emportait sur tout, parfois même sur les considérations humaines. Geoffroy de Tours, le jeune bourgeois debout aux abords de l’estrade d’Arnulf, ne l’entendait pas ainsi.

« Je sais que vous parlez de raison, maître Salomon. Mais un homme comme vous sait que les négociants sont inquiets car le meurtre est toujours mauvais pour les affaires. »

Des murmures indignés répondirent à cette remontrance insultante, que le jeune homme apaisa d’un geste assuré.

« Mais ! Je sais aussi qu’injustices vous répugnent, comme à cette cour. Comment pourrions-nous faire comme si nous ignorions que pareilles ignominies sont perpétrées à quelques pas de la dernière demeure terrestre du Christ ? Comment pouvons-nous accepter en silence que cette cité sainte soit profanée par un murdrier ? Et ceci au nez et à la barbe de nos sergents, bravant et donc insultant par là même l’autorité du roi Baudoin. »

Il s’avança peu à peu face à ses pairs, vivant son plaidoyer comme si la décision qui allait être prise pouvait entraîner la mort ou l’amnistie d’un proche.

« Nous sommes les bourgeois du roi, et à ce titre, devons meilleur service que routinières activités. Nous avons devoir de rendre visible pour tous la main invisible du souverain, qui s’abat sur quiconque menace la paix de Dieu. Peut-on trouver crime plus odieux que meurtrerie, en pleine Semaine sainte ? Dans Jérusalem, notre cité ? N’avons-nous pas tous juré, main sur les Évangiles, de préserver et d’appliquer les choix du souverain ? Nous ne pouvons certes demeurer insensibles à cet appel à nos cœurs, lancé depuis leur tombe par ces femmes, mais nous avons également la charge de montrer à tous que le pouvoir du roi est présent céans, et bien présent ! »

Il marqua une pause, fixant un interlocuteur ou l’autre jusqu’à ce qu’il le sente gêné.

« Qu’ose s’avancer devant moi celui qui pense que nous aurions tort de porter si haut notre service auprès du roi Baudoin. »

Un silence gêné fit place à sa longue diatribe, lancée avec passion. Scrutant d’un regard d’aigle ses auditeurs, il posa lentement les mains sur les hanches et se tourna vers le vicomte, lui indiquant par là la décision prise par l’assemblée.

« La cour semble décidée à ne pas laisser cet affront impuni, messire Arnulf. À vous de voir comment faire au mieux. Sans pour autant négliger les tâches qui vous ont été confiées par le trésor royal, de certes.

— Fort bien. Nous allons voir si l’époux peut se montrer et nous l’inviterons à s’exprimer en cette cour. Nous n’y consacrerons pas trop de moyens, car ce n’est pas en notre pouvoir. Le jeune Ernaut nous aidera en cela, ayant quelques liens avec ces pauvresses. Donc, maître Ucs, vous ferez dire aux hommes qu’ils doivent essayer de se renseigner, en voyant avec ce jeune homme, mais sans y perdre tout leur temps. »

Tout en parlant, il s’était tourné vers le mathessep, désignant de la main Ernaut d’un geste nonchalant. Puis il fit de nouveau face quelques instants à l’ensemble de la Cour et s’éclaircit la gorge avant de reprendre, à l’intention de Ucs de Montelh :

« Il faudra être prêt pour les collectes, le sénéchal a bien insisté là-dessus. Pas question que les versements se fassent en retard ou de façon confuse. Nous sommes tous bien d’accord là-dessus ? »

Sa question, qui n’en était pas vraiment une, marqua la fin des discussions sur le sujet. Après quelques échanges approbateurs ou déçus, quelques jurés commencèrent à avancer vers la porte, profitant de ce répit dans les débats pour parler négoce ou aborder un point de droit sur une affaire qu’ils allaient juger.

Le vicomte se fit apporter de quoi se rafraîchir et indiqua au valet de proposer des boissons à tous les notables présents. La réunion risquait de durer encore quelque peu, ils avaient un problème d’héritage épineux à entendre, ainsi que plusieurs chartes à ratifier, concernant la vente ou le don de bourgeoisies[^bourgeoisie]. Ne sachant que faire, Ernaut se recula dans un coin, s’appuyant contre le mur. Il regardait de droite et de gauche, attendant un signe lui signifiant qu’il devait se retirer. Il n’était pas sûr de ce qu’il devait faire et personne ne semblait prêter attention à lui, malgré son imposante présence. Il en arriva donc à la conclusion que rien ne s’opposait à ce qu’il puisse assister à la suite des débats.

Près de la fenêtre, le mathessep admirait des serins au plumage jaunes qui bondissaient dans le jardin verdoyant sous la fine pluie qui s’abattait. Il pouvait en deviner les trilles, malgré le bruit ambiant. Il soupira un instant, se demandant comment il allait interpréter les instructions du vicomte. Il fallait toujours faire plus sans qu’on lui en donne les moyens. Et il devrait encore affronter les invectives et les regards désapprobateurs des habitants, sans compter ceux des pèlerins. Il avait saisi l’occasion lorsqu’on lui avait proposé cette promotion. Avec son épouse et ses enfants, ils vivaient d’ailleurs bien mieux, s’étaient installés dans une belle demeure et de sa situation découlait de nombreux avantages.

Malgré tout, il commençait à se dire que ce n’était pas si cher payé, vu les responsabilités qu’il avait. Ce ne serait pas en surveillant les fraudeurs ou arrêtant les meurtriers de vagabondes qu’il pourrait espérer attirer l’attention d’un riche seigneur qui ferait de lui un chevalier. Il se retourna vers la salle lorsque les discussions retombèrent. On venait de faire entrer quelques personnes, dont les échanges de regards haineux indiquaient que ce devait être encore une histoire d’argent qui allait être abordée. Il soupira et, les bras croisés, se cala contre la colonnette de la fenêtre, écoutant d’une oreille distraite les avantparliers[^avantparlier] s’échauffer la voix de quelques formules ampoulées.

### Midi du mercredi 27 mars 1157

Après avoir pris un repas rapide dans le Marché couvert, Ernaut était revenu dans le quartier de l’Hôpital. Il n’était néanmoins pas entré voir son frère, car il espérait en fait apercevoir Libourc, en provoquant une rencontre qu’il aurait prétendue fortuite. Cela lui avait donné l’occasion de croiser Gringoire, qui était d’une humeur bien plus volubile que la fois précédente.

Il venait d’apprendre que sa femme n’était pas malade, seulement enceinte, et que ça la fragilisait. Bien qu’il soit tout de même inquiet, la perspective d’avoir un enfant le rendait euphorique. C’était leur premier et il n’en finissait pas de bénir Dieu et tous les saints. Il avait offert à boire à tous les gens alentour et parlait encore plus fort qu’à l’ordinaire. Il avait déclaré avoir dépensé une vraie fortune en cierges à l’intention de la Vierge pour qu’elle protège le bébé à venir ainsi que sa mère. Il n’était plus de première jeunesse et attendait ce moment depuis très longtemps. Ce n’était pas sans fierté qu’il avait expliqué à Ernaut que ce serait le début d’une grande dynastie et qu’il s’accommoderait aussi bien d’un garçon, qu’il formerait comme il se doit, que d’une fille, qu’il destinerait à un mariage profitable.

Si le bonheur lui avait donné des ailes, il se serait pour sûr envolé dans l’instant. Malgré les battements de bras et l’agitation que déployait l’heureux marchand, Ernaut s’était efforcé de surveiller les allées et venues aux abords des bâtiments réservés aux femmes, au cas où la jeune fille de ses pensées viendrait dans les parages. Bien qu’il ait espéré la vision depuis un moment, sa poitrine se gonfla d’angoisse quand il lui sembla reconnaître la silhouette qui s’avançait hors du portail.

Elle était habillée de la même façon, mais avait ramené sa capuche sur sa tête, dissimulant son épaisse chevelure brune. Elle s’arrêta un instant, le bourdon sur l’épaule, afin de fouiller dans sa besace. Le cœur battant la chamade, Ernaut s’engagea dans sa direction, faisant mine de ne pas la voir, toutefois sans vraiment trouver un point sur lequel fixer son regard. Il s’était convaincu d’attendre le dernier moment pour porter son attention vers elle et feindre la surprise. Quand il s’y risqua, il fut très désappointé de constater qu’elle était concentrée sur ce qu’elle faisait, la tête penchée sur son sac.

Il hésita un instant, ne sachant que faire. Heureusement, elle releva subrepticement les yeux et leurs regards se croisèrent. Pour Ernaut, le temps sembla se ralentir et le battement sourd de son cœur oblitéra l’effervescence de la place. Comme un somnambule, il leva la main et sourit à demi, obliquant dans la direction de la jeune fille. Il réussit à reprendre contact avec la réalité lorsqu’il arriva auprès d’elle.

« Le bon jour, damoiselle. »

Un sourire lumineux s’afficha soudain, tandis qu’elle inclinait légèrement la tête avec respect.

« Le bon jour à vous, maître Ernaut. »

Flatté et ravi de se voir ainsi appelé par Libourc, le jeune homme inspira largement, son sourire atteignant désormais ses oreilles.

« Quelle surprise de vous encontrer ainsi ! Je venais juste de… »

Ernaut réalisa soudain qu’il n’avait pas réfléchi à l’excuse qu’il invoquerait. Sa voix se tut un instant et les mots qui lui vinrent se présentèrent un peu confusément avant de s’assembler de façon explicite.

« Un ami vient d’apprendre qu’il va avoir enfançon. Nous fêtions l’heureuse nouvelle. Sa jeune épouse est actuellement en l’Hôpital, car cela la fatigue fort. Mais tout se passe bien, par chance.

— J’en suis ravie pour elle.

— Je vous vois toute équipée pour le voyage, vous nous quittez donc, à la vigile du Jeudi saint ?

— Certes pas. Avec mère, nous devons joindre quelques amis, car nous avons prévu de nous rendre au tombeau de la sainte Vierge. Nous allons y passer nuitée en oraison, de façon à être prêtes pour les célébrations à venir.

— C’est elle que vous attendez ?

— Non, elle s’en est déjà allée. Il m’a fallu demeurer en arrière pour pressante affaire. Je devais la joindre à la porte Saint-Étienne, en l’église, avec plusieurs autres également en arrière, mais il semble qu’ils m’ont oubliée. »

Ernaut fronça les sourcils.

« Vous avez l’intention de vous rendre là-bas seule ? Ce n’est guère prudent.

— Nous ne sommes tout de même pas si loin. J’ai grande habitude de la ville, désormais.

— Je serais heureux de vous accompagner si vous le permettez. Nul doute que votre père approuverait une telle décision. »

Bien qu’elle n’eût laissé paraître aucune anxiété jusque-là, Libourc sembla soulagée à l’idée d’être escortée par le jeune géant. Elle approuva de la tête chaleureusement, les yeux plissés par son sourire.

« C’est très aimable à vous. J’espère que cela ne perturbera pas vos activités.

— Vous savez, je n’ai guère à accomplir. Nous sommes en Outremer depuis plusieurs mois et à Jérusalem depuis des semaines. J’ai fait mes dévotions un peu partout et nous attendons juste les fêtes de Pâques. Puis nous chercherons bel endroit où nous établir, avec mon frère. »

Les deux jeunes gens commencèrent à avancer nonchalamment tout en devisant, maintenant entre eux une distance respectable qui les forçait à hausser la voix lorsqu’un badaud passait entre eux.

« Vous allez vous installer ici ?

— Oui. Nous sommes venus pour rester. Pas vous ?

— Je ne sais, je vais là où mes parents décident. Difficile de dire ce que père a en tête, je dois l’avouer. Il me semble qu’il aurait bien envie de s’installer. Mais il n’a rien dit d’aussi clair jusqu’à présent. »

Tout en remontant tranquillement la rue Saint-Étienne en direction de la porte septentrionale du même nom, ils continuèrent ainsi à bavarder, parlant des villes et des lieux qu’ils avaient visités récemment ou de ceux qu’ils espéraient parcourir d’ici peu. La jeune fille était relativement réservée, néanmoins elle s’animait à l’évocation des sanctuaires où elle avait prié, aussi bien que des animaux étranges qu’elle avait découverts. Un de ses plus grands souvenirs était la vision d’un crocodile, tué par des chasseurs et ramené pour son cuir, qu’elle avait pris au début pour un dragon.

Le temps fila si vite qu’ils étaient encore en pleine conversation lorsqu’ils arrivèrent devant l’église placée à la sortie nord de la ville. Un groupe de pèlerins était installé sur les marches et alentours, discutant tranquillement. Une petite femme d’une quarantaine d’années, le visage gracieux malgré quelques rides, enfermé dans une guimpe très serrée, vint prestement jusqu’à eux. Sans même prêter attention au jeune homme, elle sourit à Libourc.

« Ah ! Te voilà enfin. Il ne manquait plus que toi et la famille de Clarembaud pour que nous prenions la route. J’étais un peu inquiète d’avoir vu les autres nous joindre sans toi. Je n’aurais jamais dû leur accorder ma confiance ! »

La jeune fille baissa la tête, adoptant un air contrit, comme si la faute était sienne et attendit patiemment que sa mère ait terminé de déverser son angoisse. Puis elle se tourna vers Ernaut qu’elle présenta de la main.

« Mère, je vous présente maître Ernaut, de Vézelay. C’est lui qui a aidé père l’autre jour. »

La petite femme ne parut pas du tout impressionnée par l’énorme masse qui la côtoyait, même si elle tiqua à la mention du titre de maître. Elle lança un regard rapide, qui ne semblait guère convaincu de ce qu’elle voyait. Bien qu’elle rendit une bonne cinquantaine de centimètres au jeune homme, elle parvenait à le toiser de haut, avec un naturel certainement dû à des années de pratique. Malgré tout, le visage autoritaire s’adoucit un peu à l’évocation de son époux.

« Je te remercie d’assister ainsi mon époux, jeune homme. Serait-ce que tu as intention de rejoindre notre groupe ? Je ne te vois pas équipé en conséquence.

— Pas du tout, j’ai juste rencontré votre fille aux abords de l’hôpital. Je ne pouvais la laisser ainsi seule en ville, c’était bien trop dangereux.

— C’est fort aimable à toi. Certains de nos compaings auraient dû prendre la peine de l’escorter, mais voilà leur façon de respecter leurs serments…. »

Elle posa la main sur l’épaule de sa fille, l’invitant d’un geste à la suivre.

« Viens avec moi, nous allons réciter courte prière avant de prendre les chemins. Je te souhaite le bon jour, jeune homme, et merci de t’être donné cette peine. »

Libourc céda de bonne grâce, non sans lancer à Ernaut un regard désolé qu’accompagnait le plus charmant des sourires. Un des pèlerins s’avança, un rictus malicieux éclairant un rude visage que les ans n’avaient pas épargné. Un de ses yeux commençait à se recouvrir de cataracte.

« ’Tention mon gars. Défense de coudoyer la gamine. Sa vieille la garde comme lait sur le feu. »

Il tendit sa calebasse à Ernaut, qui refusa d’un signe de tête. L’air rigolard, l’homme s’approcha et murmura.

« Tu devrais goûter, j’y mets pas de l’eau, moi, ça corrompt les entrailles. Ça te redonnera un peu de cœur à l’ouvrage ! »

Estimant qu’un peu de vin ne lui ferait pas de mal, Ernaut prit une gorgée, ce qu’il regretta immédiatement. La boisson tenait plus du vinaigre et sa saveur acide ne surmontait qu’à grand-peine le goût de pourriture. Il fit une grimace lorsqu’il l’avala, pour le plus grand plaisir du pèlerin qui gloussa avant de s’en envoyer une large rasade.

« Ça écorcherait la pierre, hein mon gars ? »

Lui-même semblait fort apprécier sa mixture et se pourlécha les lèvres d’un air gourmand.

« Elle a bein raison de tenir à l’œil un beau brin de fille comme ça. Surtout en ce moment. Avec tous ces maudits païens dans la région.

— Vous avez eu souci lors de votre voyage ? »

Le vieil homme souleva les sourcils, un peu surpris de la question.

« Ah non, aucun. Je pensais juste aux deux femmes qui ont été…. Elles faisaient partie des nôtres, tu sais. »

Comme l’homme semblait en veine de confidences. Ernaut prit son air le plus intéressé, invitant son interlocuteur à poursuivre par un hochement de tête encourageant. Il espérait bien découvrir où se cachaient le mari et l’enfant. Le vieux pèlerin fit un rictus qui dévoila quelques dents gâtées et usées.

« Non pas qu’elles soient de chez nous. Mais ça faisait des semaines qu’on était tous comme grande maisonnée, tu vois. Il y a aussi le gamin et pis le mari, Nirart. C’est pour eux que ça doit être dur !

— Ils sont ici avec vous ?

— Ah non, y nous sont quitté v’là une semaine, le Nirart, les deux femmes pis l’gamin.

— J’ai entendu dire qu’ils avaient été rachetés par les frères de Saint-Jean. »

L’homme hocha la tête avec tellement d’enthousiasme qu’on aurait dit qu’elle allait se décrocher.

« C’est ça. Ils étaient un bon paquet à être arrivés ensemble à l’Asnerie, peu après les fêtes de la Noël. Moi j’étais là depuis peu avec bons compaings et on a vu arriver tous ces pauvres hères, dans un tel état ! Plusieurs ont reçu fort longtemps les soins des frères. Puis ils sont repartis les uns après les autres. »

Sa voix mourut doucement et il examina Ernaut, les yeux plissés. Il semblait hésiter à s’accorder une gorgée mais reprit finalement:

« De là, Nirart et son épouse, Ylaire et son gamin Oudinnet, ils ont marché avec nous. Au début ils étaient fort craintifs et restaient toujours entre eux, ne parlant à personne. On a réussi à s’accorder un peu, malgré tout. Pourtant, ils sont toujours restés à l’écart, vu qu’ils avaient traversé tous ces malheurs ensemble. Et pis ils étaient du même coin, ça compte ça aussi.

— Vous voulez dire qu’ils étaient venus par le même voyage, tous ensemble ? Ils ne se sont pas rencontrés dans les geôles sarrasines ?

— Ah non, ils étaient amis dès avant ça. Comme ils étaient les seuls à avoir retrouvé la liberté, ils se serraient les coudes. C’est normal ! »

Un si long discours semblait avoir desséché la gorge de l’homme qui se fit un devoir de l’arroser, non sans en proposer à Ernaut, qui refusa poliment.

« Certains des leurs ne sont pas revenus ?

— Je sais pas de juste mais c’est ce que j’ai compris, oui-da. On raconte que ces galeux de païens traitent leurs captifs pire que des chiens. Alors on a jamais osé trop leur en demander, ça aurait pu leur remembrer mauvais souvenirs. »

Ils demeurèrent cois quelques instants, le vieil homme vérifiant que son petit groupe était toujours là. Quelques-uns étaient entrés dans la chapelle pour s’y recueillir avant de prendre la route. Ce fut Ernaut qui brisa le silence.

« Vous savez où je pourrais les trouver ? »

Le pèlerin adopta soudain un air méfiant, presque renfrogné.

« Pourquoi qu’tu veux les voir ?

— Il se trouve que j’ai des amis dans l’hôtel royal ici. Et le père de Libourc, maître Sanson, m’a dit que je pourrais peut-être leur porter aide. »

L’homme se fendit d’un sourire rusé que n’aurait pas renié un renard, se tapotant l’aile du nez d’un air malicieux.

« Ah ! Je vois, tu frottes le père pour cueillir la fille. »

Devant l’attitude outrée que prit Ernaut, l’homme lui flatta amicalement le bras.

« T’inquiète ! T’as bien raison ! Pas de fourreau sans couteau ! J’aurais ton âge, je ferais la même chose. Quoi qu’il en soit, je ne peux te garantir l’endroit où ils sont pour l’instant. Quelques compaings avaient prévu de cheminer au sud, vers Bethléem et la tombe de Rachel. Vu que plusieurs d’entre eux étaient parmi les plus proches de Nirart, ils peuvent se trouver là. De toute façon, ils devraient tous être revenus pour la nuit, ça m’étonnerait qu’ils ne soient pas en ville à temps pour les cérémonies de demain. »

Ernaut soupira, il lui fallait une nouvelle fois aller de l’autre côté de Jérusalem, car les voyageurs revenant du sud rentraient généralement par la porte du Mont-Sion, ouverte en cette période. Il finissait par croire qu’il n’arriverait jamais à rejoindre la famille des victimes, qu’il serait perpétuellement en chasse de leurs traces. Il salua amicalement l’homme, lançant un dernier coup d’œil vers l’église où il espérait apercevoir la jeune fille, sans succès. Dépité, il retourna en ville sous le regard imperturbable des soldats de faction.

### Début d’après-midi du mercredi 27 mars 1157

En descendant vers la porte sud de la cité, Ernaut se rendit à son domicile pour récupérer sa chape, ainsi qu’une gourde. Il ne savait pas combien de temps nécessiterait sa recherche et ne voulait pas être pris au dépourvu. Les rues étaient bien plus calmes dans cette partie de la ville et il pouvait avancer d’un bon pas. Il remarqua d’ailleurs plusieurs voyageurs à cheval.

Une fois la muraille passée, le chemin était plaisant, aisé, car il déclinait jusqu’à la vallée d’Hinnom qui ceinturait la région méridionale du Mont Sion. Beaucoup de fidèles se présentaient pour les célébrations qui allaient commencer le lendemain et pas seulement des pèlerins venus de loin. Ernaut croisa et salua au moins une dizaine de groupes avant même de parvenir de l’autre côté de la combe, abordant la remontée sur les contreforts du mont Géhon. À chaque fois, il n’obtint que des réponses négatives, quand il réussissait à se faire comprendre des voyageurs.

Tandis que les lieues défilaient, les nombreuses vignes lui rappelaient sa Bourgogne natale et les pentes environnants Vézelay. Le reste de la végétation était néanmoins bien différent, timides broussailles, fins cyprès ou modestes figuiers. Lorsqu’il arriva sur le plateau, le paysage lui apparut beaucoup plus aride. De grandes quantités de rochers affleuraient parmi les buissons, et les pins tordus se faisaient plus rares. Il apercevait au loin quelques hameaux. Le plus près, un peu sur sa droite, se ramassait au pied d’une haute tour, un autre s’étendait vers le sud.

Il n’était pas vraiment sûr de son chemin et se dirigea vers un groupe de bergers qui devisaient tranquillement tout en surveillant distraitement leurs bêtes. En s’avançant, Ernaut craint un instant qu’il ne s’agisse de musulmans. Il n’y en avait aucun dans la cité sainte elle-même, mais on pouvait en croiser dans certains villages de temps à autre. Il avait toujours fait en sorte de ne pas devoir les approcher, redoutant qu’ils ne se montrent hostiles. En l’occurence il n’avait pas le choix. Du bout des doigts, il vérifia que son lourd couteau était bien dans son sac en bandoulière puis avança, l’air décidé.

Un petit chien jaune à poil ras grogna à son arrivée, vite rappelé à l’ordre par un sifflement rapide. Obéissant, il vint se coucher auprès des trois hommes, au pied d’un muret de pierres sèches. Parmi le troupeau, quelques ovins tournèrent la tête puis se replongèrent dans une activité plus intéressante, consistant à chasser les brins d’herbe. Un des bergers s’avança vers Ernaut. Il était vêtu d’un thawb de laine fatigué, avec une couverture drapée autour du corps. Sur son crâne, le turban avait pris la couleur de la terre. Il était difficile de lui donner un âge, avec son visage très marqué, mais sa barbe était d’un noir profond, sans une tache blanche. Il tendit les mains de façon accueillante en s’inclinant.

Ernaut pencha la tête également et tenta de se faire comprendre. Il cherchait la route pour Bethléem, pour le tombeau de Rachel et répétait ses mots en montrant alternativement les deux agglomérations qu’on devinait au loin. À un moment, son interlocuteur se mit à sourire, en faisant de grands gestes démonstratifs. Il rabâchait, hilare « Bayt lahm » en tendant le bras vers le sud. Il s’adressa à ses compagnons, qui l’imitèrent, non sans avoir échangé quelques remarques et regards de connivence. Ernaut se méfia et redemanda « Bethléem ? » tout en indiquant de la main le chemin qui continuait droit sur le plateau.

Cette fois-ci, il obtint une réponse en chœur, un large sourire amusé illuminant les visages. Le petit chien, excité par toute cette agitation, se releva et décida de s’associer aux explications. Il courait en tous sens, émaillant ses dérapages d’aboiements stridents. Le jeune homme salua de la tête avec ostentation et reprit sa route.

Lorsqu’il arriva sur le chemin il se retourna et lança, de loin, de nouveau « Bethléem ? », le bras pointant vers l’horizon. Une nouvelle fois, les bergers se mirent à faire de grands gestes, amusés, tandis que leur chien s’époumonait de plus belle. Il fallut à Ernaut faire un effort de volonté pour ne pas recommencer son manège après avoir un peu avancé. Finalement, ils ne lui paraissaient pas si dangereux que ça, les païens.

À l’approche du village, il croisa une caravane d’ânes menée par une demi-douzaine d’hommes. Parmi eux, un franc de forte corpulence, habillé en partie comme un latin, semblait diriger. Il portait sa récolte de toisons de laine à Jérusalem, depuis Bethléem justement. Il confirma donc le chemin et indiqua qu’ils se trouvaient pour l’instant à Tablie, à un peu plus de la moitié de la distance jusqu’au sanctuaire de Rachel. L’endroit était facile à repérer : une tombe avec une pyramide dessus, à côté d’une église, sur le bord de la route. Il s’y rencontrait d’ailleurs toujours quelques fidèles, généralement des femmes venues demander de l’aide pour avoir un enfant.

Le hameau ne présentait guère d’intérêt, semblable à ceux qu’Ernaut et son frère avaient traversés bien souvent ces derniers mois. Une puissante bâtisse de pierres bien appareillées surveillait le casal, constitué de maisons basses enduites de terre brune, aux rares fenêtres et modestes portes.

De nombreux enclos délimitaient des espaces privés et quelques gainiers de Judée aux fleurs rosées rivalisaient avec des pistachiers le long des façades. Des bandes d’enfants jouaient parmi les arbres de la place qui s’étendait au long du grand bâtiment seigneurial. Plusieurs chiens étaient allongés devant certaines entrées, parvenant ainsi à assurer leur fonction de gardien tout en s’octroyant une sieste.

Un groupe de femmes, assises sur un banc, se tenait un peu en retrait de la route. Elles préparaient des légumes, jetant les épluchures aux poules qui leur tournaient autour. Si près de Jérusalem, le va-et-vient de voyageurs était suffisamment fréquent pour ne plus constituer une attraction notable et elles levèrent à peine la tête pour regarder passer le jeune homme. Seuls les plus facétieux des enfants se risquèrent à une rapide sarabande dans son dos, chantant dans leur langue étrange, en sautillant, une phrase qui les amusait. Lorsqu’Ernaut fit mine d’être un méchant monstre, ils s’égayèrent dans toutes les directions, riant et criant.

Il n’eut guère à aller très loin pour trouver ceux qu’il cherchait : les marcheurs revenaient effectivement vers la cité pour y être avant la nuit. Parmi eux, un imposant blond, à la barbe fleurie, arborait un grand bourdon surmonté d’une croix. Sa tonsure disparaissait sous les mèches folles. Malgré son embonpoint, il progressait sans gêne et tonitruait plus qu’il ne chantait, dans un latin aussi approximatif que celui d’Ernaut, des cantiques que pas un clerc lettré n’aurait pu reconnaître. S’approchant avec entrain, Ernaut salua avec enthousiasme le groupe, s’attirant des regards surpris. Immédiatement, le prêtre s’avança vers lui, un sourire sur les lèvres, qu’on devinait plus qu’on ne voyait sous l’imposante moustache qui se noyait dans la barbe.

« Salut à toi, ami. Cherches-tu compaings pour cheminer ? Ou as-tu perdu ton groupe ? J’ai nom Ligier et suis d’Artois, comme les âmes dont j’ai charge.

— Le bonjour, père Ligier, j’ai nom Ernaut de Vézelay et je cheminais jusqu’à vous par volenté. On m’a dit que je pourrais trouver parmi vous un nommé Nirart ainsi que garçonnet Oudinnet. »

Le prêtre fronça les sourcils et se frotta le visage, un peu embarrassé. Un vieil homme au crâne luisant, se présentant comme Pons de Mello, s’avança pour lui annoncer la mauvaise nouvelle : ils n’étaient pas avec eux.

« Sont-ils restés à Bethléem ? Demanda Ernaut avec empressement.

— Non pas. Nous étions important groupe à notre départ de Jérusalem, mais certains ont préféré se rendre à la grotte des rois mages. Nous nous sommes quittés il y a plusieurs jours déjà. »

Ernaut se tint coi quelques instants, conscient d’intriguer le groupe de marcheurs. Il lui fallait néanmoins obtenir quelques réponses s’il voulait se montrer utile.

« Vous connaissez bien Nirart et son épousée ?

— Si fait, nous voyageons ensemble depuis bien assez longtemps. Les pauvres !

— Vous avez pérégriné ensemble depuis la France ? »

L’homme secoua la tête, un peu effrayé.

« Heureusement que non pour moi et les miens. Ils ont été long temps captifs des sarrasins… »

Ernaut prit un air de circonstance à l’évocation de ce malheur, espérant s’attirer la bienveillance de Pons lorsqu’il se permit de le couper.

« Oui-da, on m’a conté par le détail cette fort triste histoire. Ils ont été rachetés par les frères de Saint-Jean, c’est cela ? »
Le prêtre, qui suivait jusqu’alors les échanges en silence, s’immisça alors dans la conversation :

« Ils sont plus ou moins avec nous depuis… la mi-carême je pense. Nous sommes arrivés par la route de la Côte à ce moment, pour le dimanche de la Joie[^laetare]. Cela nous avait semblé fort bon signe d’ailleurs ! Ils nous ont compaigné lorsque nous avons entrepris de suivre la Haute route, jusqu’à Nazareth. »

Le jeune homme pinça les lèvres. Il était désappointé d’avoir perdu du temps à cheminer jusque-là en pure perte. Attendant en silence de nouvelles questions, Ligier et Pons échangèrent un regard de connivence avant de se décider à reprendre la route. Sans plus un mot, il invitèrent d’un geste Ernaut à se joindre à eux.

Très vite, le rythme revint et les chants religieux furent de nouveau entonnés, avec plus ou moins d’exactitude et de talent, mais une foi sincère qui donnait du cœur aux jambes fatiguées. Les marcheurs avançaient d’un bon pas et ils seraient de retour en ville rapidement. Chemin faisant, Ernaut se décida à les interroger plus avant. Il demeura donc aux côtés de Pons qui lui avait répondu, ponctuant de temps à autre les silences de quelques questions anodines au milieu d’autres moins innocentes.

« Vous avez eu grand temps de les bien connaître si j’entends clair.

— Si fait, mais en quoi cela te regarde-t-il, garçon ? Je n’ai pas souvenance que Nirart m’ait parlé de toi, et je ne t’ai jamais vu t’entretenir avec eux.

— En fait, un mien compagnon est fort inquiet pour eux car il n’a nulle nouvelle de leur part depuis un long temps. Et comme il est fiévreux, tenu en la couche à l’hôpital des frères de Saint-Jean, j’ai fait promesse de l’informer. »

L’explication sembla satisfaire son interlocuteur qui demeura silencieux un long moment. Le chemin était aisé mais il se révélait un peu fatigué, certainement le résultat de semaines, sinon de mois, de privations et d’efforts quotidiens. Ernaut se surprit à l’examiner en détail.

Le dos voûté, le vieil homme lançait son bâton avec un geste régulier, levant à peine les savates à chaque pas, soucieux de s’économiser. Sa maigre besace ne devait contenir qu’une cotte de rechange et une paire de souliers ressemelés y étaient accrochés. Il devait cheminer depuis bien longtemps, espérait peut-être finir ses jours en Terre sainte. La vigueur qui animait ses mouvements, la vivacité de ses yeux sombres, l’enthousiasme de son chant semblaient venir d’ailleurs, tant son corps tout entier paraissait prêt à s’effondrer à chaque foulée. Au bout d’un moment, Pons sentit qu’on le dévisageait, et il se tourna vers Ernaut, d’un air de confidence.

« À dire le vrai, je vois bien ces gens, mais ils étaient fort plus amis avec la bande au Gobert…

— Qui est-ce ?

— Un carrier qui a connu bien douloureux accident qui lui prit le bras. Il est fort en gueule, mais assez amical. Nirart surtout l’apprécie fort, du moins à ce qu’il me semble. C’est pour ça qu’il a dû se joindre aux autres, Gobert est là depuis peu, et voulait absolument voir la grotte des Rois Mages.

— Vous devez les retrouver à la vesprée ?

— Oïl ! Nous devons nous joindre à la chapelle Saint-Mamilla, vers le Lac du Patriarche, à quelques jets de flèches de la porte de David. Nous veillerons aux abords de la cité tous ensemble pour y attendre le début des offices de demain. »

Ernaut essayait de calculer le temps qu’il leur avait fallu pour leur périple, puis estima plus simple de le demander directement :

« Avez-vous souvenir du jour de votre partance de Jérusalem ?

— Si fait, c’était deux jours avant Pâques fleuries[^rameaux]. Nous étions désolés de ne pouvoir y être en la cité, mais nous n’avions guère le choix si nous voulions voir tous les saints lieux avant de rembarquer… »

Ernaut écarquilla les yeux de surprise, cela voulait donc dire que le groupe était parti avant le premier meurtre. Peut-être s’étaient-ils tous trompés dans leur identification. La réponse plongea le jeune homme dans le mutisme.

« Si je comprends bien, tu es compaing d’un de leurs amis, ai-je bien ouï ? Relança Pons.

— Certes, voilà exacte vérité. Il se trouve – Ernaut baissa la voix – que de biens terribles mésaventures ont frappé Nirart et les siens semble-t-il et nul n’a de nouvelles de lui. Cela cause moult inquiétude. »

L’homme frotta son crâne d’une main parcheminée, toussant avant de reprendre.

« Adoncques, tu auras réponse tantôt, car nous voilà déjà à descendre en la vallée face à la cité de David[^citedavid]. »

Ernaut hocha la tête. En contrebas, la vallée de Hinnom remontait sur leur gauche, longeant le promontoire sur lequel le monastère de Sainte-Marie se tenait, au-devant de la muraille de Jérusalem en partie dissimulée derrière les frondaisons d’où pointaient quelques cyprès. Pour l’heure, ils allaient obliquer droit au nord, sans gravir les reliefs face à eux, et suivre le fond de la combe jusqu’à atteindre le quartier qui s’était développé auprès de la porte de David, au couchant de la ville.

En bas, un vent froid siffla à leurs oreilles lorsqu’ils sortirent du couvert de la végétation et ils entendaient les bannières du roi claquer au loin sur les remparts. Devant la porte de David et dans le faubourg de maisonnettes qui s’y attachait, on remarquait l’agitation habituelle de l’entrée principale de la cité. On ne voyait que peu de tentes noires aux alentours, les bédouins désireux de commercer avec les voyageurs ayant certainement pris peur de la récente attaque de Baudoin sur les tribus au pied du mont Hermon.

Pourtant, l’activité y était intense, une multitude de camelots profitait de l’aubaine pour écouler des souvenirs aux nouveaux arrivants, se proposant de les guider dans la ville auprès des lieux saints, de les mener chez le changeur le plus honnête ou le meilleur des taverniers. Dans un enclos, de nombreux animaux de bâts attendaient de repartir, ayant délivré leur chargement dans la cité, qui dévorait en cette période chaque jour ce qu’elle mettait habituellement plusieurs semaine à digérer. Sans le vent, l’endroit aurait été comme souvent recouvert de la poussière soulevée par les milliers de pieds, de savates, de bâtons, de pattes, de sabots qui foulaient chaque jour les environs.

Délaissant le tumulte de l’agglomération, le petit groupe continua quelques centaines de mètres jusqu’à atteindre une légère côte. Ils s’arrêtèrent bientôt auprès d’une vaste citerne, le Lac du Patriarche, entourée de bosquets d’aliboufiers et de pistachiers. À côté, une chapelle était enclose d’une ceinture de pierres sèches, délimitant une zone parsemée de stèles funéraires. L’endroit n’était pour l’instant occupé que de deux serviteurs affairés à remonter une partie du mur et d’un autre arrachant quelques broussailles envahissantes le long du chemin.

Le petit groupe décida de s’arrêter là, profitant de la vue vers la cité sainte. D’où ils étaient installés, ils jouissaient d’une perspective magnifique depuis le nord-ouest. Face à eux, indiquant l’angle de la ville, la puissante tour de Tancrède étalait son imposante masse. Un marchand avait expliqué à Ernaut qu’on l’avait ainsi baptisée pour célébrer un vaillant baron qui, le premier, était entré dans Jérusalem. Avant cela on l’appelait la tour de Goliath. Étant donné son aspect grandiose, Ernaut se prit à rêver d’un jour où l’on pourrait la nommer la tour d’Ernaut, en l’honneur de tous ses exploits à venir.

### Soirée du mercredi 27 mars 1157

Les ombres commençaient à s’étirer lorsqu’un groupe de voyageurs s’avança vers eux. Une douzaine de marcheurs, recouverts d’une pellicule grise, signe d’un long chemin parcouru, gravissaient tranquillement la pente pour les rejoindre. À peine eurent-ils aperçu Pons et ses compagnons qu’ils firent de grands gestes amicaux, et deux enfants se hâtèrent, faisant la course pour arriver le premier. Très vite, ce ne fut que congratulations, embrassades et réjouissances.

Seules quelques œillades à demi-inquiètes se tournaient de temps à autre vers le jeune géant qui n’était pas un familier. Ernaut, pour sa part, espérait pouvoir reconnaître Nirart simplement en dévisageant chacun des pèlerins l’un après l’autre, ce qui ne manquait pas d’alarmer encore plus. Ce fut Pons qui mit fin à l’intrigue soulevée par sa présence. Il avait discuté un petit moment avec un manchot au physique solide, bien qu’empâté, au visage étrangement fin. Faisant quelques pas vers Ernaut, il le présenta :

« Jeune maître Ernaut, voici Gobert, qui saura peut-être te montrer quelqu’assistance en ton questionnement. Mais je suis fort marri, point de Nirart avec eux… »

Déçu, Ernaut laissa s’échapper un soupir, n’eut pas le temps de prendre la parole que le carrier débita d’une voix rapide et chantante :

« Tu espères après Nirart, l’ami ? Je dois avouer que je serais bien aise d’avoir nouvelles. Nous avons grande inquiétude depuis notre départ hors la ville. Phelipote elle-même était bien troublée de le voir absent…

— Vous avez vu Phelipote voilà peu ?

— Si fait, elle était du mien groupe depuis quelque temps déjà, avec son époux et leur amie, Ylaire, ainsi que le petitou, Oudinnet. Nous avons avalé quelques lieues de concert depuis que nous faisons visitance en la Terre sainte. »

Le jeune homme se mordilla la joue sans y penser, ne sachant comment aborder la question qui lui brûlait les lèvres. Il laissa Gobert continuer.

« Nous devions départir pour faire oraisons en la grotte des rois mages et avions convenu de nous assembler à l’ancienne chapelle la Vierge, à une lieue à peine au bas de la cité, là où vivent des ermites au parmi de grottes. »

Pons fit une moue apeurée avant d’intervenir.

« Bien étrange idée de se regrouper au Chaudemar, Gobert.

— Nous étions convenus de faire mouvance avec d’autres pérégrins venus de Normandie. Un des leurs était mort en fièvres voilà peu, et ils avaient volenté de le veiller la nuit au-devant. Il nous semblait de raison de les joindre là… »

Ernaut avait entendu parler de ce lieu qui angoissait tous les marcheurs de Dieu si tôt débarqués dans la sainte Cité. Sa réputation inquiétante était l’une des premières choses que l’on apprenait en arrivant. On murmurait son nom avec appréhension, craignant d’y finir ses jours. Des rumeurs prétendaient qu’il avait été acquis avec les trente deniers payés à Judas pour sa trahison. Une église y veillait sur les morts étrangers, les voyageurs, les pèlerins, enterrés là ou, plus probablement, confiés à la vaste fosse commune maçonnée adossée à la falaise. Rien que d’y penser, il frissonna.

« …Nous avons espéré quelque temps l’arrivée de Nirart, sans succès. Alors nous avons pris la route fors lui.

— Et ses compaignes, Phelipote, Ylaire, elles vous avaient trouvés ?

— Nul besoin, elles étaient parmi nous depuis bon moment. Nous avions perdu Nirart depuis la veille, Phelipote était fort inquiète et ne voulais s’en départir sans lui. Elle a donc choisi de s’en retourner en la cité. Et Ylaire ne voulait la laisser seulette, elle l’a donc compaigniée avec le sien fils, Oudinnet.

— Qu’est-il donc arrivé à Nirart ? Demanda Ernaut d’une voix inquiète.

— Je ne sais. Il lui arrive fort souvent de faire absence. Ils sont assez secrets, ces quatre-là. Bien gentils, mais peu causants. »

Adressant un regard à Pons, le jeune homme se demandait comment annoncer la terrible nouvelle de la mort des deux femmes. Peut-être avaient-elles découvert un effroyable secret sur Nirart, qui s’était vu contraint de les supprimer. Après quelques instants de réflexion, il se décida à lancer, d’une voix faussement légère :

« Pensez-vous que Nirart ait pu avoir quelques ennuis ? Était-il homme à s’engraignier facilement, à chercher querelle ? »

Gobert s’esclaffa, un sourire aux lèvres.

« Certes pas. Si je disais le fond de ma pensée, je le verrais mieux chapon que coquelet ! »

L’homme s’interrompit brusquement, une ombre passant sur son visage.

« Pons m’a raconté que tu t’inquiétais de Nirart pour un tien compaing. Serait-ce qu’il a quelques ennuis ?

— Je ne sais, c’est là cause de mon trouble. Il me faut vous faire aveu de ce que les deux pérégrines ont été découvertes violemment meurtries, ce qui cause bien grand émoi en la cité. »

Gobert et Pons ouvrirent de grands yeux et se reculèrent un peu, frappés par la violence de la révélation. Autour d’eux, chacun se fit silencieux, conscient qu’un malaise se répandait.

« Tu ne m’as pas dit pareille chose, jeune homme.

— Je n’ai pas fait menterie, je suis vraiment en quête de Nirart pour mon ami. C’est à cause des meurtreries qu’il est fort inquiet. »

Gobert se gratta le menton, embarrassé et visiblement contrarié. Il lui fallut quelques instants pour retrouver ses esprits :

« Je ne sais quel malheur a bien pu encore les frapper, je ne peux croire que Nirart ait pu faire quelque mal à ces deux pauvresses. Il les protégeait comme mâtin ses ouailles… Et qu’en est-il du gamin ?

— Nul ne sait. En fait, il semble qu’il ait disparu aussi. Le vicomte et ses hommes pensaient que Nirart demanderait réparation des grievances faites à son épouse, pourtant il ne s’est pas montré. »

Comprenant que le groupe avait pris en affection les deux femmes et leurs compagnon et enfant, Ernaut fit un rictus désolé. Il ne lui était pas venu à l’idée que des liens proches avaient pu être tissés par les deux victimes et que l’annonce de leur mort allait toucher plus férocement certains cœurs que d’autres. Il s’en voulut de sa maladresse et cherchait un moyen d’atténuer le choc de la nouvelle. Bizarrement, il se mit à fixer ses pieds comme s’il s’y trouvait la réponse à son trouble. Pons fut plus rapide que lui à sortir de ses pensées et reprit de sa voix éraillée :

« Juste une question, garçon, qui est donc ce mystérieux ami dont nous n’avons nulle connaissance ? Tu le dis compaing de Nirart, ce qui m’interroge fort…

— Il a pour nom Sanson, vieillard malade, en l’hôpital. Ils s’étaient vus en différentes places et lieux. Chez les frères, il avait lit non loin d’un nommé Amalric, familier de Nirart, qui est trespassé voilà peu. Il semble que Nirart avait pris usage d’entretenir Sanson, et une certaine amitié s’était installée entre eux. »

Pons souffla, soulagé à la mention d’un nom familier.

« J’ai bonne amitié pour maître Sanson, brave homme, soucieux d’autrui. J’ai eu grande peine à le savoir enfiévré. Tu es donc de son pays ?

— Non. Il est tenu non loin de mon frère en l’hôpital. Nous avons appris à nous connaître. »

Pons esquissa un timide sourire, s’amusant soudain de ses pensées. Il examina Ernaut d’un air appréciateur, sans ouvrir la bouche. Il semblait s’être fait son idée sur les motivations d’Ernaut. Néanmoins, il ne dit rien et ce fut Gobert qui reprit, hochant la tête.

« Si fait, Amalric était de leur groupe, mais ne l’avons guère connu, si ce n’était de nom. Il était captif avec eux, ne s’est jamais remis semble-t-il, et le pourrissement de ses membres l’a emporté… »

Ils demeurèrent de nouveau silencieux un long moment, ce qui laissa l’opportunité au père Ligier de s’immiscer. Malgré son imposante circonférence, il semblait d’une délicatesse dont témoignait son regard affable et doux.

« J’ai grand peine à ouïr ce que vous déclamez, amis. J’ai maintes fois tenté d’apaisier les âmes de nos compères, mais si grand malheur les avait frappés qu’ils semblaient perdus en bien sombres ténèbres. Il demoraient toujours un peu en retrait, malgré toute l’amabilité qu’on cherchait à leur témoigner. Même en notre alberge, ils ne se mêlaient guère et il me fallait aller les chercher pour qu’ils daignent nous rejoindre pour oraisons.

— Ils demeuraient avec vous en la cité aussi ? Demanda Ernaut, soudain revigoré par la perspective de trouver le logement des femmes.

— C’était d’usage, oui-da. Pourtant souventes fois, ils choisirent de s’établir en l’Asnerie, car ils ne goûtaient guère d’être cellés en demeures fermées. Là-bas, ils reposaient en fenier, avec valets et palefreniers. Je n’ai guère réussi à les tenir en notre groupe que pour les nocturnes de prières, jamais en l’hôpital des frères, où ils auraient pourtant été bien mieux installés. »

Ernaut arbora soudain un large sourire, qui intrigua le prêtre.

« Que voilà intéressante nouvelle, mon père. Je vais vite voir s’il m’est possible de m’enquérir d’eux en cet endroit. Par chance, je pourrais y encontrer l’un ou l’autre de nos amis…

— Puissent Dieu et tous les saints vous exaucer mon garçon », rétorqua le gros clerc en dessinant un vague signe de croix de sa main potelée. »

Sans plus de cérémonie, Ernaut fit un large salut du bras et de la tête, non sans l’appuyer plus solennellement à l’intention de Pons et Gobert, puis repartit comme une flèche vers la porte de David. En coupant à travers le quartier du Patriarche, il aurait le temps de visiter l’Asnerie avant la fermeture de la ville. Avec un peu de chance, il dénicherait en peu de temps des nouvelles intéressantes pour le vicomte,et de quoi briller devant Libourc, avant que la nuit ne s’installe.

Tandis qu’il parcourait les rues pleines de monde, il ne prenait pas garde aux autres et bousculait joyeusement tous ceux qui se trouvaient en travers de son passage ou qui avançaient trop lentement pour lui. Tout à ses pensées euphoriques, il était insensible aux exclamations et regards courroucés qu’il ne manquait pas de s’attirer à lui. Son enthousiasme fut pourtant de courte durée. Lorsqu’il sortit par la porte Saint-Étienne, fort encombrée à la veille du Jeudi Saint, il réalisa qu’il serait difficile de questionner les domestiques et serviteurs qui œuvraient là.

La foule des pèlerins qui n’avait pas pu trouver place en ville s’était agglutinée dans le faubourg, occupant le moindre recoin, le plus précaire des abris. De nombreux marchands forains s’étaient installés, vendant des denrées, réparant les souliers, proposant des tenues de voyage défraichies. Le modeste hôpital était bondé et même l’église Saint-Étienne était remplie de marcheurs épuisés. Beaucoup avaient forcé l’allure pour parvenir à temps à Jérusalem et arboraient une mine fatiguée, des traits tirés.

Il était illusoire de penser découvrir Nirart ou Oudinnet sans aide et déranger les domestiques risquait de ne lui attirer que des remarques désobligeantes. La charge de travail était immense et personne n’avait un instant à lui consacrer, pas plus qu’à ses questions. Décidé à ne pas abdiquer si facilement, Ernaut parcourut la plupart des bâtiments, demandant après Nirart et Oudinnet à chaque fois auprès de pèlerins qui ne parlaient pas toujours sa langue, ne comprenaient pas ce qu’il voulait ou avaient simplement envie qu’on les laisse tranquilles. Bien qu’il restât jusqu’à la dernière limite, juste avant la fermeture des portes, il n’obtint pour toute réponse que des visages interrogatifs, des hochements de tête désolés voire des invectives désapprobatrices de l’intrusion.

### Début de nuit du mercredi 27 mars 1157

Un bleu ténébreux commençait à dévorer les ultimes flamboyances orangées du ciel lorsqu’Ernaut monta quatre à quatre les marches menant à l’hôpital. Il espérait voir Lambert une dernière fois avant que l’accès ne lui en soit interdit. Même s’il était fréquemment en désaccord avec lui, il savait qu’il demeurait son plus sûr confident et, à son corps défendant, s’avérait souvent de bon conseil. En outre, le fait qu’Ernaut tente à sa façon d’aider les pauvres femmes et leurs familles ne pouvait qu’attirer sa compassion.

La vaste salle scintillait des nombreuses lampes et torches, apportant une atmosphère chaleureuse. En fin de journée, l’activité était moindre, et on entendait surtout des toussements, des reniflements, quelques râles, des ronflements plus ou moins sonores. Des serviteurs s’employaient à allumer tous les luminaires, préparaient les malades pour qu’ils trouvent le sommeil. La plupart des domestiques étaient rentrés chez eux ou s’apprêtaient à le faire et seuls les gardes pour la nuit continuaient à s’affairer.

Lambert était allongé sur le dos, les mains derrière la tête et semblait perdu dans ses pensées. Néanmoins, il aperçut son frère immédiatement et l’accueillit d’un sourire lorsqu’il s’assit au bout de son lit.

« Alors, frère, te sens-tu prêt pour les jours saints qui… »

Il s’arrêta brusquement, dévisageant Ernaut, les sourcils froncés.

« Mais qu’as-tu fait à ta belle cotte ? On la dirait avoir été retornée parmi poussière et graviers ! »

Le jeune homme réalisa qu’il ne s’était pas changé avant de partir pour ses recherches et qu’il avait donc sali le vêtement qu’il destinait aux jours chômés. Il adopta un air surpris, puis rassurant, agitant la main d’un geste négligent.

« Ce n’est rien, je la secouerai avant dormir. C’est juste quelque poudrée de la marche… Je voulais assavoir si tu étais en meilleure santé, et si les frères te pensaient vaillant assez pour me joindre aux cérémonies. »

Lambert secoua la tête, la mine contrariée.

« Nenni. Il va me falloir encore espérer en ma couche et prierai donc depuis là, je le crains. »

Ernaut posa une main amicale sur l’épaule de son frère.

« Tant que ta santé le nécessite, cela est bonne chose à faire.

— Et toi ? Es-tu aprêté pour les saintes fêtes ? Tu en as terminé avec ces histoires de meurtriement ? »

Voyant que son frère baissait la tête, ainsi qu’il le faisait depuis qu’il était jeune lorsqu’on le prenait en faute, Lambert soupira, sans trop d’aménité.

« Ernaut ! Tu ne peux te consacrer à meule et à four ! Il sera bien temps de porter assistance aux bourgeois du roi une fois les Pâques achevées. Le Salut de ton âme me paraît moult plus important.

— Si fait, je le sais fort. Mais il se trouve que le mari et l’enfant ne sont plus là non plus. Oncques ne les a vus depuis vendredi de la Passion[^vendredipassion1157]. Je crains pour leur sauveté. »

Lambert modula sa voix avec chaleur, incluant seulement une pointe de paternalisme.

« Je comprends ta peur et ta volonté d’aider ces pauvresses. Mais que pourrais-tu faire ? Il y a sergents pour traquer méchants hommes. Évite périlleux sentiers, ce n’est pas ta voie, frère. Et s’ils sont morts, fais donc prières pour eux. De toute façon, même vifs, cela peut les aider. Outre, cela te sera plus utile à toi aussi. »

Les deux frères gardèrent le silence un petit moment, chacun regardant les alentours, évaluant le poids de ses dernières paroles et les arguments de son contradicteur. Puis Lambert se releva un peu plus et toucha le bras d’Ernaut pour attirer son attention.

« Moi itou, j’ai eu visite d’un bailli. Il avait ouï de notre désir de devenir colons. »

Il baissa la voix.

« J’espère que les frères de Saint-Jean ne m’en tiendront pas rigueur : il se trouve que le Saint-Sépulcre a beau casal à doter à quelque cinq ou six lieues au septentrion d’ici. Ils offrent bonnes conditions, modeste champart en plus de la dîme. Si on possède deux ou trois vingtaines de besants, il peut se trouver borgesie à acquérir. »

Ernaut écoutait attentivement, se mordillant la lèvre sans bruit.

« Il me faudrait faire le compte de ce qu’il nous reste, il est possible que nous puissions nous offrir un passage devant le vicomte et recevoir la verge pour notre bien. L’un de nous devrait bien sûr prêter serment aux chanoines, que nous ayons champart ou simplement dîme à leur verser. »

Lambert était visiblement excité par les perspectives et Ernaut peu enthousiaste, ce qui ne constituait certes pas la règle entre les deux frères. Le jeune homme finit par poser une question :

« Où se trouve cette place ?

— On la nomme la Mahomerie[^mahomerie], car il s’y trouvait quelque ancien lieu de culte des païens infidèles. On s’y rend par la maîtresse voie pour Naplouse. C’est outre Ramathes. Le bailli m’a dit qu’on pouvait guetter Jérusalem depuis le toit de l’église. N’est-ce pas merveilleux endroit ?

— Cela semble, certes. Les terres y sont-elles bonnes ?

— Certains cultivent froment ou seigle et d’autres encore y taillent le cep. Les terrasses aiment donner arbres à fruits m’a-t-il dit. »

Ernaut hocha la tête, une moue sur les lèvres, comme s’il n’était guère convaincu. Lambert s’en émut et se pencha vers son frère.

« Cela te semble mauvais endroit ? Il me semble pourtant fort attrayant. Et même si les païens y venaient, il demeure une forte tour où l’on se peut abriter. De certes, ce n’est pas arrivé depuis fort longtemps…

— Je ne sais. Tout ce que tu me contes me semble bel et bon, mais je n’ai guère appétit à cela encore, je l’avoue volontiers.

— Je ne te comprends plus, frère, ne sommes-nous pas aussi ici pour nous installer ? »

Ernaut demeura silencieux. Il n’avait pas envie de se chamailler avec Lambert, surtout qu’il n’était pas certain de ce qu’il voulait. Seulement, pour le moment, il ne désirait guère s’éloigner de Jérusalem, du moins tant qu’il n’en saurait pas plus sur Libourc. Sans pour autant occuper ses pensées en permanence, elle n’en était jamais longtemps absente. L’idée de se décider à ne plus la voir était pour l’instant insupportable au jeune homme. Il cherchait encore une façon de répondre à Lambert sans que celui-ci ne se froisse lorsqu’il aperçut un peu d’agitation au fond de la salle. Il se tourna vers son frère.

« Voilà venir la procession de complies, il va me falloir te laisser pour cette nuitée.

— Certes mais apense à cette idée, nous pourrions être établis dès avant l’été et préparer notre terre pour la saison prochaine ! »

Lambert souriait, heureux de cette perspective. Ernaut n’eut pas le cœur d’entacher ce timide bonheur par ses langueurs, qu’il ne s’expliquait guère lui-même d’ailleurs. Il s’efforça de montrer un peu d’enthousiasme avant de répondre.

« Bien sûr, frère ! Je verrai si d’aucuns connaissent ce casal et s’ils en vantent si fort les mérites. Nous aurons ainsi avis d’autre paroisse.

— Sage résolution. Il nous faut en savoir plus avant d’arrêter notre choix, tu es dans le vrai. Même si le lieu semble conforme à tous nos souhaits, comme si le Seigneur répondait à nos attentes. »

Ernaut se leva et salua son frère rapidement, voyant la procession arriver. À partir de ce moment, l’hôpital était fermé aux visiteurs, il fallait laisser les malades se reposer. Tandis qu’il prenait la direction de la sortie, le jeune homme tenta un bref coup d’œil vers Sanson. Mais celui-ci semblait caché sous ses couvertures, en plein sommeil ou bien décidé à le trouver au plus vite.

En quelques rapides foulées, Ernaut retrouva la noirceur de la nuit qui tombait. La lune était dans son dernier quartier, et elle illuminait le ciel étoilé d’une lueur blafarde. L’horizon était encore souligné d’un trait pastel, mais l’obscurité s’étendait partout dans les rues. De chaudes lumières perçaient par-delà certains murs, passaient à travers les fenêtres tandis que l’agitation de la journée disparaissait tout à fait. Les cris de quelques passereaux résonnaient parmi les sombres venelles, remplaçant les discussions animées des chalands.

### Nuit du mercredi 27 mars 1157

Quittant le quartier de l’Hôpital par la porte donnant sur le parvis, Ernaut suivit la direction du sud, en direction de la rue Saint-Martin, ou de l’Arche de Judas, qui s’étirait en descendant depuis le cœur de la ville. Les boutiques avaient été fermées de panneaux de bois et la vie s’était déplacée dans les demeures situées dans les étages supérieurs.

Des voix, de temps à autre des rires ou des chants passaient à travers les volets ouvragés. Les plus bruyants, qui recevaient des amis pour les célébrations, avaient parfois les fenêtres ouvertes et des convives étaient visibles par moment, appuyés aux chambranles ou prenant l’air penchés à l’extérieur. Quelques fêtards ou invités, en retard, parcouraient le pavé, faisant fuir les rongeurs qui s’aventuraient à la recherche de nourriture.

La lune et le ciel étoilé apparaissaient dans les sections où la rue n’avait pas de toit. Le nez au vent, Ernaut déambulait doucement, profitant de la fraîcheur de la soirée. Il était fatigué de sa journée à aller et venir sur les chemins et en ville. Il commençait à se demander si le mari n’était pas l’assassin et s’il ne s’était pas enfui en territoire sarrasin pour échapper à la justice.

La faible clarté faisait qu’on trébuchait souvent et plusieurs personnes croisèrent le jeune homme non sans un sentiment de crainte face à sa stature. En arrivant aux abords du marché couvert, il entendit des voix qui beuglaient, dans l’espoir de ressembler à un chant. Peu décidé à rentrer chez lui, il prit la direction de la petite zone éclairée, où l’on vendait du vin à taverne et faisait rouler les dés.

Lorsqu’il pénétra dans le cercle de lumière de la place, il se rendit compte que plusieurs échoppes étaient toujours ouvertes, et même quelques marchands ambulants proposaient encore de quoi se restaurer. En humant les saveurs épicées d’une sorte de tourte, il réalisa qu’il avait faim et s’offrit de quoi se contenter. Il se dirigea ensuite vers le plus proche revendeur de boissons pour agrémenter le tout d’un pichet. Il dut poser ses plats sur un muret qui servait de banc et cherchait dans sa besace de quoi payer le vin et la consigne du récipient quand il sentit une claque dans son dos, assortie d’une exclamation que seul un ivrogne pouvait faire.

« Ça alors, voilà le curieux pérégrin ! »

Il se retourna et découvrit, étonné, le sergent qu’il avait tiré d’un mauvais pas la veille. Mais il avait bien moins fière allure, habillé d’une cotte élimée, le cheveu roux en bataille et les yeux un peu fous. Il lui fit un large sourire, inquiet de ce que l’autre avait à lui dire.

« Le bon soir, maître sergent !

— Foin de pareils hommages, mon gars, on est parmi compaings. »

La voix légèrement chuintante de l’homme d’armes incitait à croire qu’il était là depuis un moment. Il était encore clair, pourtant son élocution parfois traînante et son aspect bien plus négligé que d’ordinaire trahissaient son état. Il tendit la main à Ernaut.

« Tu te plais en la cité, ami ? Ça ne fait pas guère long temps que tu es ici, n’est-ce pas ?

— Qui vous l’a dit ?

— Parce que tu crois qu’un gaillard comme toi passe inaperçu ? La cité n’est pas si grande. On finit par reconnaître chacun, surtout ceux qui n’ont pas taille… disons, bien normale. »

Ernaut commanda rapidement son vin et regarda le sergent, mi-figue mi-raisin, ne sachant pas ce qu’il devait faire, ni ce que l’on attendait de lui. Le temps qu’Ernaut soit servi, le sergent avait rejoint quelques amis et parlait tout en montrant dans la direction du jeune homme. Il lui fit signe d’approcher.

« Je vous présente celui qui m’a évité cuisants soucis hier matin. Ernaut.

— Le bon soir à tous ! »

La demi-douzaine de personnes installées dans un renfoncement du passage le salua sans entrain, quoique certains aient l’air curieux et intéressés par le nouveau venu. À l’exception d’un seul, tous avaient le faciès européen et s’habillaient à la française, cotte et chausses. Celui qui détonait avait le type méditerranéen, la peau mate, la barbe bien taillée grisonnante et de nombreuses rides le faisaient sans doute paraître plus âgé qu’il n’était. Il portait des vêtements amples, comme les sarrasins, et le turban qu’il avait sur la tête était un véritable défi aux lois de la pesanteur. C’est lui qui invita Ernaut à entrer dans leur petit cercle d’un geste de la main. Il tendit par la même occasion un bol contenant des olives agrémentées d’épices. Eudes ne lui laissa pas le temps de se sentir gêné.

« Alors, dis-moi, mon gars, en dehors de fureter, à quoi t’occupes-tu ?

— Bein, à pas grand-chose. On est là pour les fêtes de Pâques avec un mien frère. Il est actuellement soigné par les frères de l’Hôpital.

— Vous comptez repartir quand ?

— Jamais ! Si on trouve belle terre où s’installer. On n’a encore guère pris le temps de prospecter, on était là avant tout pour visiter saints lieux. Mon frère a ouï parler d’un casal nommé la Mahomerie, reste à savoir si c’est là bon lieu.

— Des colons ! Il y en a encore que ça tente de venir alors ! S’exclama un bedonnant moustachu au nez en trompette. »

Eudes lui lança un noyau en souriant.

« Nul mauvais esprit, Droart ! Ne l’écoute pas Ernaut, il ne débite que sornettes. »

Un de leurs compagnons pouffa dans son gobelet, amusé par avance de sa propre blague :

« Des fois, il en fait aussi ! »

La remarque tira des sourires à l’assemblée, Ernaut profita du silence pour entrer dans la discussion.

« Vous êtes tous des sergents de la cour ? »

Eudes fit non de la tête.

« Seulement Droart et moi. Les autres ne sont que bons à rien. Sauf Abdul Yasu, ici présent, qui loue de ses montures lorsqu’on en a besoin. »

Au regard interrogateur d’Ernaut, l’arabe précisa jovialement :

« Je possède mules et ânes. Mais pas tels ceux qui lipent ici leur vin, des braves, de ceux qui ont grandes oreilles. C’est d’ailleurs ainsi qu’on les démêle de mauvais garçons comme eux. »

La discussion continua avec entrain, l’atmosphère se déridant au fur et à mesure que les hommes se découvraient. À peine plus âgés qu’Ernaut, d’une dizaine d’années pour la plupart, ils étaient tous, sauf Eudes et Droart, célibataires, et aimaient à s’enivrer tranquillement entre amis.

Comme ils n’avaient pas d’endroit où aller, ils se réunissaient là où on les tolérait. La bonne humeur coutumière d’Ernaut eut tôt fait de les séduire tous et ils s’amusèrent bien avant dans la nuit, sans prêter attention aux négoces qui fermaient autour d’eux. Ce fut Abdul Yasu qui fut le premier à partir, suivi de peu par la plupart des hommes, dont certains partageaient le même logement.

Il ne resta plus qu’Ernaut, Eudes et Droart. Ce dernier s’était conscienceusement imbibé, toutefois pas au point d’être complètement ivre. Il marchait de façon un peu saccadée et sa diction était embrouillée, mais il demeurait relativement maître de lui et ne se comportait pas comme un soûlard braillard. Peut-être en raison d’années de service à réprimer ce genre d’agissements. Par prudence, Eudes et Ernaut le raccompagnèrent jusque chez lui, pas loin de la rue de Josaphat, dans le quartier nord-est de la ville. Ils le laissèrent aux bons soins de son épouse, qui n’appréciait visiblement guère de le voir rentrer si tard et dans un tel état. Le regard qu’elle lança à Eudes était d’ailleurs éloquent et les deux compères repartirent en riant du mauvais traitement qu’elle avait certainement envie de leur infliger.

Eudes confia à Ernaut qu’il n’avait pas le même genre de souci avec sa femme et qu’elle devait vraisemblablement dormir. Il s’arrêta donc devant un étroit bâtiment dont l’entrée était coincée entre deux boutiques, avant la rue des Tanneurs, non sans s’être fait confirmer une bonne demi-douzaine de fois qu’Ernaut s’estimait capable de rentrer chez lui sans guide. Après un dernier au revoir, il poussa sans bruit la lourde porte de bois et disparut dans le couloir sombre.

Le jeune homme se retrouvait de nouveau seul, mais il se trouvait moins nostalgique que précédemment. Il s’était fait de nouveaux amis et avait de toute façon ingurgité suffisamment de vin pour se sentir guilleret. Le trajet de retour lui parut d’ailleurs plus long qu’il ne l’avait imaginé. Ses raccourcis n’avaient peut-être pas été aussi pertinents qu’il l’avait estimé. À demi assommé avant même de toucher l’oreiller, il se laissa tomber plus qu’il ne se coucha sur son matelas.
