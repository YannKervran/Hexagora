## Prologue

### Monastère bénédictin de la Charité-sur-Loire, hiver 1223

Le novice dodelinait de la tête, le regard perdu dans le vague, admirant les flammes dansant dans l’âtre de l’infirmerie. Il attendait que le moine dont il consignait les récits se réveille enfin.

Pour l’instant, le vieil homme respirait paisiblement, ronflant parfois légèrement. Lorsqu’un convers entra, une oule fumante tenue à bout de bras à l’aide de chiffons, une bonne odeur de soupe envahit la pièce.

Les malades et les blessés s’agitèrent. Le dormeur souleva une paupière fatiguée et sourit de contentement. Réveillé par l’excitation qui gagnait l’endroit, Déodat se leva et vint assister à la distribution, rompant les miches pour les plus faibles, apportant les écuelles et les cuillers. Le potage était agrémenté de quelques tranches de lard, flottant dans un épais bouillon de chou, de navet et d’ortie. L’odeur en fit saliver le jeune frère, impatient de goûter à son tour à l’excellent repas. Lorsqu’il revint à sa place, il trempa avec gourmandise son pain, partageant un regard de connivence avec le vieux narrateur. Celui-ci n’avala qu’une partie de son brouet puis se réinstalla confortablement sous les couvertures, comme s’il avait froid malgré la chaleur ambiante. Il se frotta doucement le nez.

« Je n’ai plus si fort appétit que jadis, garçon. Dieu sait comme j’ai tenu grande joie à mangeailler, mais j’en ai fini avec ce péché-là. »

Déodat hocha la tête en assentiment et posa sa propre écuelle au sol, attrapant son écritoire.

« Souhaitez-vous narrer quelques faits sur ce point, frère ? »

Le vieillard eut un rictus ironique, amusé par l’idée.

« Certes pas. Inventorier ses vices est déjà bien déplaisant pour confesse. Je ne vois guère en quoi cela pourrait édifier d’étaler mes faiblesses. Rappelle-moi plutôt où nous en étions rendus…

— Vous avez dépeint l’arrivée en Terre sainte, les premiers temps là-bas. »

Le vieil homme acquiesça, cherchant dans sa mémoire les traces de ce qu’il souhaitait évoquer.

« Il me semble plus important de te parler ce jour d’hui des premières Pâques en la sainte Cité.

— Cela devait être merveilleux spectacle, propre à emplir de joie le cœur des voyageurs de Dieu.

— Si fait, mais la félicité des offices fut gravement perturbée en cette année-là, la cinquième ou sixième année du règne le roi Baudoin[^baudoinIIIb]. »

Déodat attrapa rapidement une plume et la trempa dans sa corne d’encre.

« Car lorsque la lumière inonde les cœurs, l’ombre n’en est jamais bien loin, tapie au revers. Ce furent des torrents de boue qui se déversèrent, des hordes de malins qui œuvrèrent en cette semaine sainte entre toutes.

— Était-ce quelque méchante attaque des païens qui causa ce trouble ?

— Non pas. Il n’est nul besoin de chercher le mal en son ennemi lorsqu’il se love en son sein propre. Le plus infâme des bourbiers, le plus terrible, n’est pas celui qui s’étend au loin, mais au plus proche de soi. Lorsque le mal est celé en un cœur, il gâte tout, tel un fruit putride en un panier. »

Le vieil homme toussa légèrement, se recroquevillant comme un parchemin trop sec.

« Mais il est tout aussi certain que c’est parmi le pourrissement que les plus belles fleurs se plaisent le mieux. Et si toute cette putréfaction répandit le mal, elle accueillit aussi les racines de la plus admirable des roses. »
