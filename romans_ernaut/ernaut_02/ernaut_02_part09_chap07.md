## Chapitre 7

### Jérusalem, tombeau du Christ, église du Saint-Sépulcre, matin du dimanche 14 avril 1157

Aux abords du tombeau sacré, Ernaut prit un instant pour admirer les arcades de marbre qui entouraient le ciborium[^ciborium] embellissant la sépulture. À l’extrémité ouest, derrière un autel secondaire, un magnifique panneau de fer ouvragé bloquait l’accès, au-dessus duquel courait une inscription en latin. Tandis qu’il en lisait les lettres, sans en deviner le sens, le jeune homme sentit un regard qui le dévisageait. Lorsqu’il se tourna, il vit le visage souriant du clerc qui avait fini de discuter avec les pèlerins et cherchait visiblement à se rendre utile. Il comprit ce qu’Ernaut était en train de faire et lui récita le texte, sans même avoir besoin d’y jeter le moindre coup d’œil. Puis il le lui traduisit :

« Ici la mort est détruite et la vie nous est rendue. Un sacrifice acceptable a été offert, l’ennemi est tombé et les péchés effacés. Les cieux se réjouissent, les régions infernales versent des larmes et la loi est rénovée. Ces choses nous enseignent, Ô Christ, combien sacré est ce lieu. »

Troublé, Ernaut fronça les sourcils.

« Cela veut-il dire que toujours l’innocent doit payer ?

— Surtout que Christ est mort pour nous sauver. »

Voyant que sa réponse ne satisfaisait pas son interlocuteur, le clerc ajouta d’une voix apaisante :

« Par ailleurs, connaissez-vous quelque bienfait qui n’ait demandé de sacrifice ? Cela ne donne que plus de valeur à l’accomplissement de la tâche. »

Ernaut baissa la tête, réalisant combien la remarque était juste. Remerciant du bout des lèvres, il tendit quelques pièces à verser dans le tronc au jeune clerc, un peu éberlué de sa réaction, puis s’éloigna sans un mot de plus. Il commençait à comprendre ce qui lui arrivait.

### Jérusalem, porte de David, matin du dimanche 31 mars 1157

Eudes était appuyé contre la devanture d’une des maisons face à la porte de David, regardant les fermiers qui franchissaient la muraille, afin de vendre leur production au marché matinal. Ils n’étaient pas très nombreux, mais certains marchands pouvaient tout de même tenter de se faufiler pour ne pas avoir à s’acquitter de toutes leurs taxes.

Malheureusement pour eux, les hommes du roi connaissaient les visages des habitués et étaient familiers des ânes et des mulets qui croulaient sous les vivres nécessaires pour l’approvisionnement quotidien de la ville. Ils interceptaient les contrevenants et les aiguillaient vers les douanes, non sans indiquer à leurs collègues de se montrer particulièrement exhaustifs dans la détermination des droits d’entrée à percevoir.

C’était leur dernière tâche avant de passer le relais à l’autre équipe, dirigée par le mathessep. Le vicomte était déjà rentré au palais et, tout aussi certainement, de retour dans un lit douillet pour une courte nuit. On disait en ville qu’on savait l’âge d’un des sergents du roi au nombre de cernes sous ses yeux, vu leurs patrouilles nocturnes régulières.

Eudes ne faisait pas exception. L’agitation des derniers jours l’avait achevé et il baillait à s’en décrocher la mâchoire, se contentant d’ahaner en guise de réponse aux rares questions de ses compagnons, tout autant épuisés que lui. Il fallut le remue-ménage causé par le renversement d’un chargement pour le sortir de sa torpeur. Un tel incident risquait de bloquer la rue et de retarder l’approvisionnement en vivre de la cité, ce qui n’était pas envisageable en ce jour de Pâques.

Ils ne furent pas trop de quatre hommes pour chasser les pigeons et tourterelles attirés par le grain qui se répandait hors du sac. Le paysan, un vieux syrien aussi fripé qu’une figue séchée, maudissait à qui mieux mieux son âne, les volatiles et tous ceux qui piétinaient son précieux produit, incluant parfois les sergents qui s’efforçaient pourtant de l’aider.

Échauffé par ce bref pic d’activité, Eudes avalait une gorgée de sa gourde lorsqu’il distingua la face rubiconde de Droart qui fendait la cohue, le ventre en avant comme un éperon de navire. Il semblait d’excellente humeur et les arômes qui s’échappèrent de sa bouche indiquaient que sa jovialité était peut-être due également à une conséquente ingurgitation de vin. Après un rapide salut à la cantonade, il se tourna vers Eudes.

« Je sais où ton gars se cache… »

Stupéfait de ce que venait de dire son ami, Eudes se rapprocha instinctivement et manqua de recracher sa gorgée.

« Le pardon ?

— Le Boîteux, là, le marchand de bestiaux, on m’a indiqué où le trouver. J’arrive de la porte des Tanneurs, où ils voient passer tous les animaux qui viennent à la foire. Le gars est un habitué, il habite d’ailleurs à côté. »

Un peu déçu qu’il ne s’agisse pas du meurtrier comme il l’avait cru de prime abord, le sergent sourit à son ami et tapa à plusieurs reprises sur l’épaule de Droart, enthousiaste.

« C’est peut-être la fin de cette horrible série de meurtres, mon vieux. Excellente nouvelle ! Tu me rappelleras de te payer godet la prochaine fois qu’on ira chez le père Josce. »

Après avoir informé brièvement ses compagnons, Eudes prit avec allégresse le chemin du sud, par la rue de David qu’il quitta pour celle de l’Arche de Judas, descendant à grandes enjambées, se faufilant dans la foule, pourtant déjà compacte, comme un poisson entre des algues. Lorsqu’il lui annonça la nouvelle, il ne fallut que peu de temps à Ernaut pour se préparer, malgré son réveil récent, tant était grande son envie d’attraper le malfaisant.

Le cœur léger, ils se rendirent rapidement à la maison indiquée aux abords du foirail. La place était calme, aucune bête n’étant présente. Les agneaux qui allaient servir pour le repas de fête du midi avaient été achetés et abattus depuis quelques jours déjà et il ne subsistait là que la puanteur animale, de nombreux parcs et barrières, ainsi que des stocks de paille nauséabonde et de foin pourri que le vent éparpillait selon son gré. L’homme avait une modeste boutique d’où s’échappait une forte odeur de suint, où il faisait donc commerce de la laine de ses bêtes. Un petit fenestron bloqué d’un volet surplombait l’arcade d’entrée, donnant certainement sur une mezzanine où il devait avoir aménagé un espace à vivre.

Interpellé par les deux enquêteurs, il passa un œil ensommeillé par là, avant de descendre ouvrir un des grands panneaux. Immédiatement un gros chien au pelage sombre, de bonne taille, vint renifler les intrus d’un air curieux. Familier, il chercha rapidement les caresses et retourna se lover à l’écart lorsqu’il n’en obtint plus suffisamment à son goût. La pièce n’était qu’un entrepôt où s’entassaient des sacs vides, des toiles et des paniers dans un coin, ainsi que de la laine, en vrac dans de vastes contenants en vannerie.

Dans un angle, une zone était entourée de barrières de clayonnage, avec un peu de paille. Certainement une resserre où garder quelques agneaux fragiles ou animaux malades le temps qu’ils se portent mieux. Une échelle menait à une plateforme du côté de la rue, où on apercevait quelques coffres. Malgré l’odeur forte qui se dégageait de l’endroit et la poussière accumulée, le lieu était plutôt bien entretenu. Le Boîteux, un peu inquiet d’être réveillé par un sergent de la ville les invita à prendre place sur un banc qu’il installa face à lui, assis sur un tabouret rustique. Puis il attendit, circonspect. Il devait avoir la trentaine, son visage en lame de couteau supportait un grand nez aquilin séparant deux yeux légèrement bridés.

Son sourire gêné, aux lèvres épaisses, dévoilait des dents irrégulières dont la blancheur tranchait fortement avec sa barbe vieille de nombreux jours, d’un noir sale. Bien qu’il ne soit pas très gros, il arborait un ventre qui tendait sa cotte. Ses mains étaient celles d’un homme habitué à travailler dur, comme son regard l’attestait. Ernaut n’aurait pas voulu être une bête de son troupeau. Eudes finit par les présenter rapidement avant d’en venir au fait.

« Nous assavons que vous êtes fort compaing avec un nommé Roussel, et il nous faut le voir au plus tôt. Il n’est plus en son logis, donc peut-être pourriez-vous nous porter aide ?

— J’en serais heureux, mais je rentre à peine en la cité, je ne l’ai donc vu depuis plusieurs semaines.

— Vous étiez…

— Parti au nord, ramener quelques moutons avec des confrères, des agneaux pour les fêtes. Nous avons coutume d’en tenir quelques têtes pour les repas de Pâques. Il y a toujours bonne demande en cette saison. Et vu le nombre de pérégrins, il convient d’aller s’approvisionner plus loin qu’habituellement. »

Contrarié, Eudes hocha la tête.

« Vous ne savez où il pourrait se trouver ?

— Certes non. Il n’est guère bavard comme compagnon. C’était peut-être dû à sa capture…

— Vous êtes certain qu’il a été pris ? Le coupa Ernaut, subitement intéressé.

— Que oui ! Je l’ai trouvé alors qu’il venait de leur échapper, à peine plus vif que mort. »

Blayves stoppa un instant, le regard perdu dans le vague, comme s’il se remémorait quelque chose de pénible.

« Le pauvre n’était qu’os et peau ! Et cette dernière était cinglée de coups de fouet. Ça, il a dû subir bien atroce capture, mais n’en a jamais parlé.

— Où l’avez-vous trouvé ?

— Pas loin de Panéas, voilà un peu plus d’une année. Je m’y rends souvent, car je commerce avec les nomades venus là pour faire paître les bêtes. C’était le moment de l’Aîd al-Adha[^aidkabir], bonne saison pour mon commerce. Des gamins l’avaient trouvé à demi mort parmi broussailles et comme les pasteurs suspectaient que c’était un fugitif, ils ne voulaient pas d’ennuis ni avec les Turcs ni avec les soldats francs. Ils ont donc préféré me le confier. »

Eudes secouait la tête, attentif, tentant d’intégrer ces nouvelles à ce qu’il savait déjà, vérifiant si tout prenait sa place de façon intelligible. Sans y penser, il laissa Ernaut continuer de poser des questions.

« Il était seul ?

— Oui. Il m’a narré par la suite qu’il s’était enfui d’un camp de travail au nord. Il travaillait à la réfection de murailles. Je crois qu’il parlait de Baalbeck, dont on dit que les Turcs la renforcent. Mais je n’en suis pas sûr. Par contre, il évoquait souvent son épouse, Asceline.

— Elle était avec lui ? Il l’avait abandonnée ?

— Non, apparemment ils avaient été séparés. Une femme n’aurait certes pas supporté les mauvais traitements qu’il avait subis et les tâches qu’on lui confiait. On aurait dit qu’ils voulaient véritablement le tuer de labeur. »

Ernaut se mordilla la joue, plissant les yeux tandis qu’il passait en revue mentalement les questions qui lui semblaient les plus pertinentes. Ce fut Blayves qui reprit spontanément.

« Il a beaucoup souffert de cela et priait souventes fois pour que le Ciel vienne en aide à la pauvresse. Cela ne lui a rien apporté au final.

— Il ne l’a jamais revue ?

— Non, pas que je sache. Je l’ai confié aux soins des frères de Saint-Jean une fois de retour en la cité. Il a d’ailleurs fini par œuvrer à leur service, certainement dans l’espoir qu’il pourrait en apprendre plus sur l’éventuelle libération d’Asceline. Jusqu’à ce que je parte voilà deux semaines, son espoir était resté vain.

— Il n’a retrouvé aucun ami qui aurait été tenu en geôle avec lui ?

— Non. Il faut dire que sa capture datait, dans l’hiver un an après la prise d’Ascalon^[Soit l’hiver 1154-1155.]. Peu auraient pu survivre à ce qu’il a enduré, le pauvre vieux. »

Le marchand hésita un instant comme s’il cherchait ses mots ou répugnait à continuer sur son idée, pourtant il finit par se décider.

« Il a gardé naturellement forte rancune envers les pirates et les marchands d’esclaves. Il me tançait souventes fois de commercer avec des musulmans. Mais il m’était aussi reconnaissant de l’avoir sauvé, alors il se calmait aussi vite qu’il s’échauffait les sangs.

— Il n’a jamais cité un certain Nirart, son épouse Phelipote ou Ylaire et son garçonnet Oudinnet ?

— Je n’en ai pas gardé souvenir. Il ne parlait que d’Asceline, mais pas si souvent, car cela lui causait grand trouble, forte douleur. »

Voyant que le marchand n’avait guère plus de choses à leur apprendre, Eudes et Ernaut finirent par prendre congé, remerciant de l’aide apportée. Blayves tenta d’en savoir un peu plus sur les raisons de cet interrogatoire, mais devant les hésitations d’Eudes, il préféra ne pas insister. Il les salua poliment puis referma le vantail derrière eux lorsqu’ils sortirent. Les deux compagnons se regardèrent, circonspects, puis reprirent lentement le chemin du cœur de la cité, vers le nord. Eudes fut le premier à briser le silence.

« Crois-tu qu’il se pourrait qu’une des pauvresses soit son épouse ? Qu’il lui ait tenu grief de quelque trahison, ou supposée?

— Je m’interrogeais pareillement. Car en tel cas, il y aurait eu menteries de leur part. Car l’une disait être épouse de Nirart et l’autre avait Ylaire pour nom et s’occupait d’Oudinnet, grand garçonnet qu’elle disait sien…

— Peut-être justement qu’il s’agit là de l’enfançon de Roussel, dont il n’a parlé mie, persuadé de le savoir mort.

— Pourquoi aurait-il occis de si horrible façon la femme qu’il aimait tant, visiblement ? Et pourquoi pourchasserait-il son fils ?

— Rien ne nous dit que c’est pour l’occire, ami, rien du tout. »

Ernaut fit une moue, peu convaincu.

« La chasse me paraît moult sauvage pour fonder pareil espoir. M’est avis qu’il s’est passé fort horrible chose en les geôles païennes. C’est là le nœud qu’il nous faut délier. »

### Début de matinée du dimanche 31 mars 1157

Avec la fatigue de la nuit, Eudes avait besoin d’aller prendre un temps de repos. Il quitta donc Ernaut peu après Saint-Martin, pour rejoindre sa famille et, surtout, son lit. Ils avaient convenu de se retrouver peu avant la grande messe qui débuterait au Saint-Sépulcre en milieu de matinée. Le jeune homme était intrigué par ce que Blayves leur avait appris.

Il avait toujours envisagé la capture par les musulmans comme la fin du voyage et il se rendait compte que la vie ne s’arrêtait pas pour autant. Il lui fallait en savoir plus sur ce qui était infligé aux captifs, comment on les traitait, ce qui pouvait éventuellement expliquer la lente descente en enfer qu’avait connue Roussel. Il était tout à fait possible qu’il ait été envoyé en pèlerinage en condamnation, pour expier une grave faute passée.

Le cas était fréquent, mais le fait qu’il ait été marié incitait Ernaut à chercher ailleurs. Il n’avait qu’une personne vers qui se tourner pour demander de telles informations : Abu Malik al-Muhallab. Malgré toutes ses préventions, Ernaut n’avait pu s’empêcher de le trouver agréable et il s’était montré fort bien disposé à leur égard la première fois. Peut-être accepterait-il de lui donner quelques éléments plus précis sur ce qu’il savait des conditions de vie des prisonniers.

Chemin faisant, Ernaut croisait de nombreux groupes de pèlerins qui se rendaient au Saint-Sépulcre. Le grand office de Pâques n’allait pas tarder, les églises résonnaient déjà des premiers appels du matin. Toutes les congrégations de la ville se retrouveraient pour l’occasion dans la cathédrale et le roi serait de nouveau présent. Néanmoins, Ernaut savait qu’il n’avait pas à se hâter ; il avait prévu de rejoindre Sanson et son groupe peu avant tierce, lorsque la messe proprement dite commencerait. Avant cela, il lui fallait passer prendre Lambert, ce qui n’était pas possible de bon matin, le temps pour lui d’avaler son déjeuner et d’être apprêté par les domestiques.

Du coup, pour l’heure, Ernaut se rendait chez le musulman, la tête baissée pour se prémunir contre le vent insistant qui parcourait les rues en sifflant, apportant des nuages gris annonciateurs de pluie. Les gens autour de lui étaient habillés chaudement, et arboraient un nez rouge de froid, mais rien ne semblait entamer leur enthousiasme.

Ernaut savait que le quartier de la Juiverie n’abritait plus aucun juif depuis bien des années. Comme tous les non-croyants, ils avaient été chassés lors de la prise de la ville et il leur était théoriquement désormais interdit d’y résider. En fait, il était surtout peuplé de Syriens et d’Arabes, dont beaucoup venaient d’Outre-Jourdain, établis là depuis près d’un demi-siècle. Beaucoup appelaient d’ailleurs l’endroit le quartier syrien. Comme un certain nombre, bien que chrétiens, n’étaient pas catholiques ou ne suivaient pas le même calendrier, leurs célébrations étaient un peu décalées et l’ambiance paraissait plus calme, moins effervescente.

Dans quelques ruelles, des hommes discutaient, balayant les abords, transportant du bois ou menant un âne avec quelques provisions. Au détour d’une placette, des gamins jouaient à s’arroser avec l’eau d’une fontaine, indifférents au froid, au grand dam de quelques femmes occupées à remplir des jarres, fort mécontentes d’être ainsi trempées. Un peu plus loin, un petit groupe d’enfants et plusieurs adolescents étaient fort affairés à courir après des volailles peu coopératives qui s’étaient faufilées hors de leur enclos. Bien qu’il soit déjà venu, il fallut quelques demi-tours à Ernaut pour se repérer, non sans avoir fait quelques erreurs dans son cheminement. Enfin parvenu à destination, il frappa à la porte, espérant que le négociateur serait dans la maison.

Une fois encore, un jeune visage entrouvrit l’huis, avant de le pousser en grand, dès Ernaut reconnu. Le géant expliqua qu’il avait besoin de s’entretenir avec son maître s’il acceptait de le recevoir. L’enfant referma, laissant Ernaut sur le pas tandis qu’il allait quérir une réponse. Ce dernier se recula, jetant un œil à droite et à gauche, levant le nez pour humer l’air et apprécier le temps à venir. Deux jeunes femmes escortées d’un vieil homme passèrent, sans un regard pour lui, suivies d’un petit chien fureteur. Elles étaient voilées, habillées de larges vêtements sombres. Leur chaperon les menait littéralement à la baguette, qu’il tenait d’un geste menaçant ainsi qu’il l’aurait fait d’une badine pour guider un animal de bât. Seul le chien parut se rendre compte de la présence du géant. La petite voix qui venait de la maison ramena Ernaut à ses affaires.

Abu Malik était cette fois habillé d’une large *’aba*[^aba], de toile rayée multicolore, qui avait dû coûter un bon prix. Il était coiffé d’un curieux chapeau de feutre, en forme de cône tronqué. Il se leva à l’arrivée d’Ernaut et le salua chaleureusement, l’invitant à s’assoir sur un des coussins. Après quelques formules de politesse, il s’inquiéta de ce que le jeune bourguignon attendait de lui. Estimant qu’il n’avait guère le temps de développer une entrée en matière, Ernaut adopta la méthode qu’il affectionnait le plus, à savoir l’approche directe.

« Vous m’avez semblé être fort informé du destin de mes frères en les fers sarrasins. J’aurais besoin d’en apprendre plus.

— Que voulez-vous savoir en particulier ?

— Comment les tient-on ? Sont-ils tenus comme bétail ou en des salles, entravés… »

Le musulman inspira lentement, profitant de l’arrivée de son jeune domestique porteur de boissons pour réfléchir un petit moment. Le sujet lui déplaisait visiblement et il ne parvenait pas à une présentation qui le satisfasse. Sa réponse fut donc extrêmement laconique.

« Leur traitement est souvent fonction de leur statut, de leur valeur, ainsi que de ceux qui les capturent. Certains Turcs n’hésitent guère à faire des exemples, même parmi riches et puissants. Le destin encontré peut varier ainsi que celui d’un grain de sable porté par le vent…

— Selon vous, qu’arriverait-il à pérégrins pris en mer par pirates égyptiens ? »

Abu Malik eut un demi-sourire, vite réprimé puis prit un peu de temps pour réfléchir, profitant du retour de son domestique pour inviter d’un geste Ernaut à piocher parmi quelques friandises proposées.

« Les biens portant seraient triés et sélectionnés pour être vendus comme travailleurs, certainement sur la côte d’Ifriqya[^ifriqiya]. Les femmes pourraient être achetées par quelque amateur, si elles possèdent des talents… »

Le musulman s’arrêta, voyant la grimace d’Ernaut.

« Il ne s’agit pas que de cela. Certes, les plus belles peuvent bien finir ainsi, mais on estime fort une servante qui sache bien la cuisine ou manie l’aiguille ou la navette avec adresse. Ce sont d’ailleurs les plus coûteuses. Il est rare que jolie fille soit prise et plus encore qu’elle demeure… intacte, après sa capture. Certes en ce cas, elle peut également être cédée à bon prix.

— Les hommes finissent donc au travail ?

— S’ils ne sont pas de quelque valeur, c’est certain. J’ai même connu un évêque qu’on avait affecté à de durs labeurs pour le briser et lui rabattre son orgueil. D’ordinaire, on préfère céler pareils captifs, en raison de leur grande valeur de rançon. »

Ernaut avala quelques gorgées de la boisson qu’on lui avait servie, à base de lait, puis grignota des amandes qu’il tenait en main depuis un moment. Pendant ce temps, Abu Malik continua.

« Comprenez que les anciens usages veulent qu’on montre sa mansuétude envers ceux que l’on prend par la guerre. Certains, comme les Turcs, ne respectent pas toujours cela, tuant parfois sans raison.

— Auraient-ils intérêt à maltraiter un homme une fois qu’ils le tiennent ?

— Selon leur gré, pourquoi pas, mais cela m’étonnerait. Une fois épargné, un captif finit généralement entre les mains d’hommes qui en connaissent le prix. Le blesser l’amoindrirait sans nul doute, ce qui n’est pas dans leur intérêt. Il faudrait qu’ils aient bonnes raisons à cela.

— Quelles pourraient-elles être ?

— Elles sont si nombreuses ! Il aurait très bien pu tenter de s’échapper ou s’essayer à quelque sabotage, comme de blesser un autre captif. »

Ernaut fronça les sourcils à cette dernière hypothèse, n’osant interrompre le marchand.

« Il faut bien comprendre qu’on les estime comme animaux en une ferme. Comme pour eux, on rabroue celui qui porte atteinte à la valeur du troupeau. Suffisamment pour qu’il ne recommence pas, quitte à l’abîmer pour le ramener dans le rang. Sinon on le met à l’écart et on l’exploite au mieux, en tentant de minimiser ses défauts.

— Selon vous, un prisonnier maltraité, affecté aux travaux de force, aurait pu se trouver là en punition de mauvaises actions ? »

Abu Malik hocha la tête, faisant une moue indécise.

« C’est dans l’ordre du possible, il y a autant d’éventualités que de situations. »

Il marqua une pause, adoptant un visage contrarié, comme s’il renâclait à aller plus avant.

« Il faut bien que vous compreniez, lorsqu’on est pris par l’ennemi, on n’est plus considéré comme un enfant d’Allah. Dans le meilleur des cas, on devient une simple marchandise comme du papier, du grain ou des dattes. On est vendu, passant de mains en mains, sans pouvoir rien y faire et les propriétaires, même s’ils ne sont pas mauvais hommes, voient en vous une source de profit, rien de plus. Alors vous pouvez être entassés comme des poulets en une resserre, travaillant aux champs toute la journée avec un maigre gruau pour toute nourriture, rendu incapable d’espérer au-delà du jour suivant. Vous pouvez aussi bien être mené pour creuser des galeries, arrachant au sous-sol ses richesses… Femmes et hommes sont mêlés comme bétail, sans aucun souci de leurs conditions. La honte et la crasse vous avilissent, font de vous moins que des bêtes. »

Il renifla, son récit amenant clairement des images en son esprit. Sa voix se brisait régulièrement, et se réduisit bientôt à un souffle.

« J’ai vu si grande misère, des hommes rendus fous par la faim, la soif… Des femmes dont les pauvres corps se trainaient tout en remuant de lourds fardeaux. Des enfants se battant pour une poignée de riz mal cuit. Je ne suis pas grand dévot, je le reconnais avec humilité, mais je ne crois pas qu’Allah ait voulu que nous agissions ainsi. S’il avait voulu faire de l’homme une bête, nous irions à quatre pattes, tels des chiens. »

Ernaut se demanda d’où venait la véhémence de ces propos. On aurait dit qu’Abu Malik se sentait personnellement touché par tous ces destins brisés. Craignant de se montrer discourtois, le jeune homme demeurait impassible, approuvant silencieusement de la tête, sans trop se dévoiler. Le musulman finit par se taire, regardant les motifs du tapis à ses pieds un long moment. Puis il reprit son récit.

« Cela fait de longues années que nous travaillons, avec les frères de Yahya, à émanciper les pauvres tombés en de mauvaises mains. Il m’est de plus en plus difficile de supporter cela, surtout maintenant que j’ai une famille. J’ai toujours crainte qu’il leur arrive pareil destin.

— Leur sort ne serait guère mieux en nos geôles vous voulez dire ? »

Abu Malik secoua la tête, comme ennuyé de confirmer cela.

« Les captifs que je ramène de vos états ne sont souventes fois guère en meilleure santé que ceux que j’y conduis. Cette terre n’est que souffrance depuis le temps des grands-pères de mes grands-pères… J’en viens à me demander si Al-Quds[^alquds] n’est pas une malédiction autant qu’une bénédiction. »

Ernaut n’osa pas demander ce qu’était ou qui était ce « alcoudze », mais hocha la tête en assentiment, comme s’il avait compris. Ce fut un petit chat venant ronronner contre leurs jambes, à tour de rôle, qui les sortit de l’étrange torpeur dans laquelle cette dernière réflexion les avait plongés. La félicité du doux animal sous leurs caresses leur mit un peu de baume au cœur pour achever leur entretien. Ernaut avait obtenu les réponses qu’il espérait et ne pouvait s’attarder. Il remercia chaleureusement son hôte et prit congé.

Lorsqu’il se trouva dehors, il était un peu perplexe, déconcerté par ses découvertes. S’engageant dans la direction de l’hôpital afin d’aller chercher Lambert, il rejoignit rapidement la rue du Temple. Là, il se sentit complètement étranger à ce qu’il voyait autour de lui.

Il suivit quelque temps une procession de fidèles qui venaient d’une paroisse de l’est, emplissant l’espace de leurs chants sacrés, inondés de joie et de ferveur. Guidés par un bourdon sur lequel trônait une grande croix de fer ouvragé, symbole de leur foi, ils poussaient de nombreux cris d’allégresse, d’imprécations saintes et de bénédictions. Le Sauveur n’allait plus guère tarder à être là, rachetant leurs péchés, purifiant leurs âmes, allégeant leurs cœurs. Pour le moment, celui d’Ernaut était bien trop ébranlé pour se sentir touché par cette grâce.

### Milieu de matinée du dimanche 31 mars 1157

De nombreux malades gardés par les frères de Saint-Jean étaient prêts à faire un grand effort pour pouvoir assister à la messe de Pâques. Les familles, les amis, les compagnons de voyage, étaient réquisitionnés pour aider à porter, soutenir, épauler. Quelques éclats de voix se faisaient entendre ici et là parmi les lits, lorsque certains patients se voyaient refuser l’autorisation de se rendre au Saint-Sépulcre. Faisant fi de toute l’abnégation de ceux qui les avaient soignés jusque-là, certains n’hésitaient pas à déverser leur frustration sur les valets ou les chefs de salle qu’ils jugeaient trop précautionneux. Lambert, pour sa part, était prêt. Il était assis sur son coffre, habillé de ses affaires personnelles qui avaient été nettoyées. Comme à tous les malades, ses biens lui avaient été retirés à son arrivée et ne lui étaient rendus que maintenant, après avoir été entreposés à l’écart.

Entretemps, on lui avait fourni de quoi se changer régulièrement, s’assurant qu’il était toujours vêtu proprement en dépit de ses maux. Il finissait de manger un morceau de pain, les yeux vers le sol. Le voyant dans sa tenue de voyage habituelle, Ernaut s’aperçut combien son frère avait maigri. Malgré l’abondance et l’excellence de la nourriture servie là, Lambert flottait désormais dans la cotte qu’il avait portée depuis leur départ de France.

Son visage était en outre grêlé de cicatrices et son regard semblait encore légèrement fiévreux. Il avait avec lui sa besace et son bourdon et s’était protégé de sa chape, avec la croix du pèlerin sur l’épaule. Il sourit à l’approche d’Ernaut et, se levant avec précaution, lui donna une accolade, une lueur fervente illuminant ses traits.

« Nous voilà prêts pour l’office, frère, après toutes ces lieues parcourues ! »

Le géant inspira lentement, approuvant du regard.

« Si fait, voilà bientôt pleine année que nous avons quitté Vézelay et ses vignes pour nous perdre sur les chemins.

— Nous perdre, mais aux fins de nous retrouver en saint lieu, frère. Et il est là, à quelques pas de nous… »

En disant cela, une joie presque enfantine semblait l’animer, tandis qu’il frappait amicalement le bras de son frère.

« J’aurais si grande joie à faire savoir cela à père !

— N’y aurait-il pas quelque voyageur de Bourgogne qui pourrait lui porter message de notre part ?

— Je n’en ai vu aucun. Je garde malgré tout espoir de trouver un pérégrin faisant la route à rebours, qui pourra décrire à notre famille combien nous sommes heureux en cette nouvelle terre. »

Alliant le geste à parole, Lambert arborait désormais un air béat, serein, comme il n’en avait jamais eu au cours de leur voyage. Ils étaient sur le point d’honorer leur sainte promesse et auraient accompli un grand devoir chrétien. Profondément croyant, Lambert voyait là une occasion magnifique de se réjouir qui confinait à l’état de grâce. Avisant l’agitation autour d’eux, il réalisa qu’il lui fallait encore faire quelques efforts avant d’atteindre son but.

S’appuyant d’une main sur son bâton, il tendit le bras vers son frère. Ernaut hocha la tête et aida Lambert à se lever. Puis ils se rendirent de concert vers la porte méridionale du bâtiment. Ernaut craignait que la sortie principale vers le parvis du Saint-Sépulcre ne soit trop encombrée et avait l’intention d’emprunter un étroit passage qui serpentait entre le domaine de l’hôpital et celui de l’église voisine de Sainte-Marie. Ce passage débouchait par une arcade dans la rue des Palmiers.

Malgré tout, même là se rencontraient de nombreux fidèles ayant eu semblable idée. Le flot avançait lentement, mais Ernaut n’eut pas le cœur de bousculer de droite et de gauche comme il en avait l’habitude. De toute façon, la presse se faisait de plus en plus dense alors qu’ils s’approchaient du sanctuaire, accentuant leur difficulté à progresser. Lorsqu’ils débouchèrent dans la grande rue, le soleil qui inondait la façade leur fit cligner des yeux.

Dégagé par le vent, un ciel d’azur, avec quelques nuages blancs qui flottaient en altitude, surplombait le monument de belle pierre taillée, récemment rebâti. À ses pieds, le parvis était rempli d’une foule attendant de pouvoir pénétrer dans l’édifice, piétinant d’impatience.

Aux abords des portes, il était visible que la place commençait à manquer à l’intérieur et qu’il allait falloir se faufiler pour s’introduire dans le Saint-Sépulcre. Il s’avérait désormais illusoire de penser pouvoir progresser autrement qu’en se laissant porter par la cohue. Nonobstant, Ernaut ne se privait pas de jouer de son physique pour ménager un peu d’espace à son frère et lui.

Assis sur les marches menant à la chapelle du Calvaire, quelques gardiens surveillaient l’avancée des fidèles, l’air sévère. Parmi eux, Ernaut eut la surprise de voir Eudes, qui lui faisait de grands gestes amicaux de la main. Il était habillé d’une belle tenue lui aussi, et ne portait pas son épée au côté comme habituellement. Il lui fit comprendre de s’approcher, d’un signe impérieux. Avec la densité de la foule, il fallut un petit moment aux deux frères pour parvenir jusqu’à lui. Après quelques saluts rapides, Eudes s’expliqua :

« Tu vas monter avec moi par la chapelle du Calvaire, nous allons être menés auprès de Hersant de Bondies. Elle est placée, avec ses compagnons, sous la grande arche. J’y ai laissé mon épouse et mes enfançons.

— Y a-t-il quelque raison à ces faveurs ? » S’interrogea Lambert.

Eudes fronça les sourcils, un peu interloqué et répondit, l’air innocent.

« Nous avons encore quelque espoir de grapper ce démon. Les frères du Saint-Sépulcre en ont suffisamment volonté pour nous assister en cela. La nommée Hersant connaît bien l’enfançon que ce fou recherche pour le tuer ou autre, elle saura nous indiquer si elle le voit. Il est de notre devoir de le mettre en sauf lieu. »

Pour toute réponse, Lambert se contenta de renifler, peut-être trop fatigué pour envisager de se lancer dans une longue diatribe. Informés, les sergents laissèrent le petit groupe monter l’escalier, sous les lazzis de quelques jaloux qui voyaient d’un mauvais œil un tel traitement de faveur, surtout en ce jour saint entre tous. Malgré leur entrée facile, les trois hommes eurent toutes les peines du monde à retrouver la place qu’Eudes leur avait indiquée.

Là, sous le grand arc qui faisait la liaison entre le chœur à l’est et la rotonde entourant le sépulcre du Christ, à l’ouest, se tenaient les quelques familiers d’Ernaut, qu’il avait appris à apprécier ces derniers jours. Il eut droit à plusieurs signes de tête amicaux et d’aimables paroles de la part de Gobert, du père Ligier et d’autres. Malheureusement pour le jeune homme, Libourc était tenue à distance par sa mère, qui faisait barrage entre le groupe et sa fille.

Il avait néanmoins pu lui adresser un bonjour muet tandis qu’il avançait. Son cœur avait fait un bond dans sa poitrine lorsqu’elle lui avait souri et salué familièrement de la main en retour, les yeux enjoués. Un instant, la richesse du bâtiment, son décor flamboyant, la rumeur née de milliers de chuchotements, l’air lourd de l’odeur de l’encens, tout cela n’avait plus aucune existence : seul comptait ce doux visage qui le fixait. Il lui fallut un petit moment pour reprendre pied et réaliser qu’Hersant de Bondies, qu’il avait finalement rejointe, lui parlait de sa voix fluette. Il se pencha vers elle, faisant mine de n’avoir pas bien entendu jusque-là. Alors qu’en fait, il n’en avait rien écouté.

« Je suis fort enjoyée que de jeunes gens comme vous s’inquiètent de ce pauvre Oudinnet. J’ai si peur pour lui, expliquait la vieille femme, se signant rapidement sans même y penser.

— Nous le trouverons, et vif, de sûr, répliqua Ernaut, avec plus de véhémence et de conviction qu’il n’en éprouvait véritablement.

— Je prie pour cela, garçon, la sainte Vierge et tous les saints.

— Vous avez espoir de le voir ici en ce jour ? »

La vieille femme sembla stupéfaite de la question, comme si on lui demandait de confirmer l’évidence.

« Que oui ! Je suis acertainée qu’il ne saurait manquer pareil office !

— S’il est empli de peur, osera-t-il seulement quitter sa cache ?

— Ce voyage représentait tant pour les siens ! Il est peut-être le dernier de son groupe encore vif. Sans en percevoir la raison, il sait qu’il est important pour lui d’être ici ce jour. Il y a eu trop de sacrifices pour cela, il ne saura rester en retrait. »

Avisant l’air impérieux de Hersant, Ernaut comprit qu’elle projetait sur l’enfant plus qu’il n’était en réalité. Il semblait peu évident qu’un petit garçon comme Oudinnet ait pu saisir ou alors peut-être intuitivement, toutes les implications du voyage jusqu’en ce lieu, pour cette cérémonie en particulier.

Jugeant inutile de heurter un sentiment qu’il devinait profond, Ernaut fit une moue qui se voulait affirmative puis leva le menton, embrassant l’assemblée des fidèles du regard. Pour l’heure, seuls les abords du maître autel, le chœur, étaient plus ou moins libres. Il doutait qu’Hersant puisse reconnaître qui que ce soit dans une pareille foule et moins encore un enfant dont la tête ne saurait émerger largement.

« Vous devriez peut-être monter quelque peu sur la base de la colonne, vous y auriez meilleure vue. Prenez donc appui sur moi pour ne pas choir. Ainsi, vous pourrez rapidement m’indiquer le gamin si vous le voyez. »

Surprise, la vieille femme n’avait visiblement pas envisagé de se prêter à de telles acrobaties, surtout à son âge. Posant une main sur la colonne, dubitative, elle interrogea le sergent du regard. Demeuré un peu en retrait, Eudes hocha la tête avant d’ajouter :

« Fort bonne idée ! Pour ma part, je vais rejoindre les miens et le reste du groupe, vers Sanson. D’aucuns pourront peut-être me désigner l’enfançon s’il passe par là-bas. Un de part et d’autre du passage, ainsi nous aurons bonne vision. Il ne devrait pouvoir nous échapper. »

Alors que le sergent amorçait son avance, écartant les gens d’un bras autoritaire, commencèrent à résonner les cloches dans le campanile récemment édifié. De plus en plus violemment, les tympans frappaient les bourdons de bronze, emportant dans l’air la joie du temps de la Résurrection. C’était la première fois depuis le Jeudi Saint qu’on les entendait de nouveau. Très rapidement, le tintamarre se propagea à travers la ville, chaque congrégation participant avec entrain à ce moment d’allégresse.

En théorie, c’était pour appeler les fidèles à l’office, mais la plupart attendaient déjà depuis l’aube dans l’église du Saint-Sépulcre le début de la messe solennelle. Après un long moment de sonneries joyeuses, le rythme ralentit pour mourir peu à peu. Avant que le silence n’ait le temps de s’installer, un chant s’élança depuis la zone orientale. Très rapidement, chacun tendit l’oreille pour s’efforcer d’entendre les voix des chanoines qui s’avançaient depuis le cloître. La grande messe de Pâques était en train de commencer.

Jamais Ernaut n’avait admiré une telle magnificence dans les tenues des célébrants, ni une pareille profusion d’hymnes et d’antiennes. Comme habituellement, il fut vite noyé par le déroulé, mais cette fois-ci, peut-être en raison de la présence de son frère qui ne perdait pas une miette du spectacle, il était proprement ébahi face à la majesté de ce qu’il voyait. Le chœur des chanoines présentait une cérémonie fascinante, dont la complexité confinait à la magie pour les non-initiés.

Les chants, les processions, l’exhibition des différents objets de culte constituaient une liturgie d’une munificence éblouissante. Le dernier des impies n’aurait pu lui même demeurer de marbre en présence d’une semblable célébration. Par instants, il sembla au jeune homme qu’il pressentait le caractère divin de ce qu’il voyait là, juste devant ses yeux, et il se surprit à s’interroger sur la présence de Dieu en ce lieu au moment où l’Eucharistie commença.

Reprenant contact avec la réalité de temps à autre, lorsque l’activité des clercs retombait un peu entre deux moments forts, il vérifiait que son frère n’était pas trop épuisé ou que Hersant était toujours correctement installée. Depuis sur sa vigie, elle pouvait assister au service avec un plaisir qui se lisait sur son visage empli de félicité. Le jeune homme effectuait également de temps en temps un rapide tour d’horizon qui se focalisait bizarrement vers sa gauche, près de Sanson et, surtout, de sa fille. Cette dernière demeurait concentrée sur ce qui se déroulait dans le chœur et ne lui adressait pas un regard. Ernaut tentait de se convaincre que c’était dû non pas à un désintérêt pour sa personne, mais à la profondeur de sa foi.

À un moment, il s’aperçut que Lambert commençait à peser plus lourdement sur son bourdon, les traits tirés, les yeux papillotants. Midi était passé depuis longtemps déjà et la célébration continuait. Ernaut fouilla dans son sac et en sortit l’outre de cuir qu’il avait portée et la proposa à son frère après lui avoir tapé sur l’épaule avec douceur. Au même instant, il sentit quelques légers coups dans son dos.

Pensant que Hersant avait également soif, il se tourna, la gourde à la main, prêt à la lui donner. La vieille femme ne le regardait pas, tendue comme un chien d’arrêt, fixant un point quelques mètres devant eux. Elle souffla à Ernaut : « Oudinnet ! Il est là ! » tout en remuant le bras dans sa direction. Lorsque le géant se retourna, il vit un petit garçon, le visage maigre, les cheveux hirsutes, qui quémandait apparemment de droite et de gauche.

Hersant s’agitait de plus en plus, au risque de tomber de son perchoir. Jusqu’au moment où les deux regards se croisèrent et qu’elle se figea, attendrie, parlant à distance au jeune enfant. Ernaut n’osa pas intervenir, de peur d’effrayer le gamin qui pourrait facilement se faufiler dans la foule, contrairement à lui. Il se contenta donc de vérifier aux alentours qu’aucun rouquin aux yeux bizarres ne rôdait. Finalement Oudinnet s’approcha, un peu apeuré, visiblement soulagé.

Il avait une dizaine d’années, le corps chétif. Ses vêtements étaient sales et abimés et son visage aurait eu besoin d’un bon nettoyage. Il jaugea Ernaut d’un regard appréciateur avant de rejoindre Hersant. Sautant plus que descendant, cette dernière manqua de tomber et elle s’empressa de serrer l’enfant contre elle, lui susurant des paroles aimables à l’oreille. Elle leva la tête vers Ernaut, les yeux pleins de larmes.

« Dieu nous a entendus, il l’a mené jusqu’à nous après tous ces jours de douleurs. Il nous revient, comme Christ ressuscité d’entre les morts ! »

Incapable de parler plus, la gorge nouée, la bouche sèche, elle cajolait l’enfant, le rassurant de son mieux.

« Vois le grand homme à mon flanc. Il est là pour te défendre, ne crains plus rien ! Tu es sauf désormais. Il te protégera. »

Ernaut s’efforçait de se montrer aimable envers Oudinnet, conscient qu’il devait lui paraître tel un monstre gigantesque. Il tenta une première approche, ébourriffant d’une main amicale la tignasse emmêlée du garçonnet. Il n’y prit pas garde, mais tandis qu’il établissait ce premier contact, le chœur des chanoines entamait d’une voix forte et solennelle le *Sanctus*.

### Après-midi du dimanche 31 mars 1157

La foule dans l’église était telle qu’il semblait illusoire de chercher à rejoindre l’entrée principale. Lambert proposa donc à son frère d’aller au nord et de sortir par la petite chapelle Sainte-Marie qui débouchait sur la rue du Patriarche par les marches. La presse y était moins dense et cela éviterait la cohue du parvis, sur lequel devait s’amasser une quantité phénoménale de pèlerins.

De quelques gestes, ils s’entendirent avec Eudes, qui prit la même direction, coincé avec les autres membres du groupe. Hersant se tenait aux côtés d’Ernaut, couvant Oudinnet du regard, gardant toujours le contact avec lui de la main voire du bras. Derrière eux suivait Lambert, d’un pas lourd, aidé de son bâton. La fatigue se lisait sur son visage, il avait certainement hâte de retrouver son lit, même s’il était heureux d’avoir assisté à la cérémonie.

Ernaut aimait passer dans cette partie de l’édifice et il profitait de leur lente progression pour admirer l’endroit. Sur le mur est de la petite chapelle, une immense fresque représentait la Vierge consolant Marie-Madeleine, dont les magnifiques cheveux rappelaient au jeune homme de bien prosaïques souvenirs. En même temps, il menait mécaniquement Oudinnet, le maintenant collé devant lui, les deux mains sur ses épaules. Il réalisa que son frère, plus faible, devrait peut-être s’avancer en premier pour monter les escaliers à son rythme.

Lorsqu’il se retourna pour le lui proposer, il aperçut du coin de l’œil un reflet métallique qui l’intrigua, au niveau des hanches d’un des fidèles qui attendaient derrière lui. Identifiant en un éclair le regard torve, il n’eut que le temps de pousser l’enfant sur le côté qu’il sentit la morsure de l’acier qui glissait en tranchant les chairs de son avant-bras. Sans même y réfléchir, il envoya son coude dans le visage de l’homme. Sa main armée se retrouvant soulevée par le geste d’Ernaut, ce dernier esquiva sans difficulté en se jetant sur les pèlerins qui allaient entamer l’ascension.

Il détala comme un félin, vif et rapide après sa tentative avortée, bousculant et culbutant tous ceux qui le gênaient dans sa montée. Sur ses traces, Ernaut, tel un étalon furieux, finit de renverser les rares qui avaient réussi à se maintenir tant bien que mal sur les premières marches. Mais il était plus imposant que sa proie, moins preste dans les degrés et accusa vite un certain retard.

Lorsqu’il déboucha dans la rue, l’assaillant courait vers le nord, cherchant à s’éloigner de cette partie de la ville densément occupée par les croyants qui sortaient de la messe. Jetant de temps à autre un regard en arrière, il ne pouvait que constater qu’à défaut d’être souple et agile comme lui pour se faufiler, Ernaut avait la détermination du sanglier forcené, projetant du sang de son bras blessé, sans aucun égard pour les personnes, les animaux ou les étals qui lui barraient le chemin.

Comprenant qu’on risquait de se méprendre sur ce qui se déroulait, le colosse essayait de crier parfois d’une voix essoufflée « À l’assassin ! Au meurtre ! » sans pour autant ralentir ni se montrer plus précautionneux. Les badauds se trouvaient poussés sur le côté après avoir senti le passage vif de Roussel, et rien ne semblait pouvoir arrêter le gigantesque chasseur, dont la fureur était décuplée par l’effort de la poursuite.

Un âne qui avançait bâté d’un amas de fourrage fit un écart tellement violent lorsque le jeune homme le bouscula qu’il échappa à son propriétaire et détala dans la foule en brayant avec frayeur. Lorsqu’ils tournèrent dans une venelle qui partait vers la tour de Tancrède, Ernaut sourit pour lui-même : l’assassin était en train de s’enferrer tout seul.

Il hurla comme un dément pour faire s’enfuir des enfants occupés à jouer au milieu du chemin, qui s’égayèrent comme des moineaux apeurés. Il ne restait guère de monde qui osât demeurer en pleine rue devant le spectacle effrayant de ce colosse, le bras ruisselant de sang, le teint rubicond, la bouche crispée et le visage tel celui d’un démon, qui ronflait comme un feu de forge.

Malgré tout, il perdait du terrain sur son agresseur dont l’avancée était plus leste, plus agile. Les deux coureurs venaient d’obliquer dans une rue qui serpentait désormais de nouveau vers le nord, en direction de l’enceinte qui n’était plus très loin. Ils filaient entre les hauts murs de jardins, où peu de gens déambulaient en ce jour de fête, et où la pente se faisait moins raide, au grand soulagement du poursuivant. Débouchant sur une petite place, ils découvrirent un attroupement d’hommes habillés de vieux vêtements, le visage emmitouflé de haillons, dissimulant des corps bossus et des moignons honteux. Des lépreux !

Ernaut tenta de freiner pour ne pas renverser le groupe tel un jeu de quilles tandis que Roussel se faufilait avec adresse jusqu’à une poterne qui perçait la muraille. Grondant comme un fauve, Ernaut profitait de la surprise et de l’appréhension des lépreux pour progresser sans avoir à trop les toucher.

Lorsqu’il passa à son tour l’ouverture, il avisa le corps d’un moine qui était en train de se relever dans la poussière, certainement bousculé par l’assassin. Il fit un écart, manquant de s’affaler dans le sol meuble du fossé, tout en essayant de lancer une phrase d’excuse dans un souffle, sans qu’aucun son intelligible ne sorte de sa gorge.

Devant lui, le fugitif avait pris une belle avance et longeait la muraille, le long du bord extérieur de la tranchée, en direction du faubourg de la porte Saint-Étienne. Il se tenait le côté d’une main et avait un peu ralenti le rythme. Rasséréné par cette vision, Ernaut s’épongea le front d’où coulait une sueur abondante qui ruisselait sur ses yeux, son visage, jusqu’à répandre une saveur salée dans sa bouche écumante.

Lorsqu’il arriva aux abords du hameau, Roussel obliqua vers un grand caravansérail dont une petite entrée avait été laissée ouverte et dans laquelle il disparut. Lorsqu’Ernaut s’y engouffra, il découvrit une vaste cour intérieure où de nombreux chameaux étaient en train d’être nourris par des valets. De larges entrepôts et des arcades où l’on apercevait des marchandises accueillaient une armée de domestiques et d’employés au repos, sous le contrôle des commanditaires et négociants surveillant leurs biens depuis les voûtes du premier étage.

Parmi ces hommes de belle allure, à la posture digne, l’adolescent remarqua une silhouette qui se faufilait avec célérité, le plus discrètement qu’il lui était possible. Cherchant un escalier, il repartit sur les traces de son assaillant, trottinant en jetant des regards un peu partout, tout en essayant de retrouver son souffle. Il ne manquait pas d’attirer l’attention sur lui, le visage écarlate, soufflant comme un taureau, le bras gauche sanguinolent répandant de façon régulière des gouttes autour de lui.

Il aperçut Roussel au moment où celui-ci disparaissait dans un passage permettant certainement d’accéder à la terrasse. Sortant son couteau de sa besace, Ernaut monta avec précaution les marches, inquiet de tomber dans une embuscade, de chasseur devenir proie. Le toit où il aboutit était relativement grand, et quelques ballots y étaient entassés, ainsi que des matériaux de construction, briques, feuilles de palme, bois, sable… qui servaient à la réfection en cours du bâtiment.

Le jeune homme tentait de se souvenir de ce qu’Enrico Maza lui avait appris[^voirt1] et se déplaçait désormais plus souplement, assurant ses appuis, cherchant tous les endroits d’où pouvait surgir le danger. L’agresseur avait parfaitement pu cacher ici une arme et s’en prendre à lui avec bien plus redoutable qu’une petite lame. Il entendit le crissement du gravillon derrière lui et se retourna, preste comme un félin, le couteau prêt à trancher, pour se retrouver devant celui qu’il avait traqué jusque-là, qui faisait enfin face.

Les cheveux châtain roux collés sur le front, Roussel avait le visage enfiévré et haletait sans bruit, la bouche ouverte, tentant de se rafraichir les lèvres d’une langue sèche. Il était plus grand qu’Ernaut n’en avait eu l’impression, bien que relativement fluet. Ses joues creuses et ses yeux caves indiquaient qu’il avait connu meilleure santé, mais la volonté qu’on y sentait briller n’augurait rien de bon.

Sans le quitter du regard, le jeune homme attrapa sa besace et s’enroula grossièrement la bandoulière autour du poignet gauche, comptant s’en faire un bouclier dans leur affrontement. Il sourit sans joie, dévoilant des canines de prédateur, le visage empli de haine, les globes injectés de sang.

Pourtant, le meurtrier n’hésita pas un seul instant. Il se jeta violemment sur lui, tentant de bloquer la main droite d’Ernaut de son bras gauche tout en frappant de son poignard la poitrine. D’un bond, le jeune homme s’effaça et laissa passer l’acier devant lui, envoyant sa sacoche sur son opposant d’un mouvement circulaire.

Celui-ci fut heurté de plein fouet dans l’épaule, projeté sur le tas de matériaux voisin, lâchant son arme dans sa chute, le souffle coupé par le choc. Ernaut en profita pour lui asséner un coup de couteau vertical, suffisant pour décapiter un bœuf, mais sa lame ne rencontra que le sol, son adversaire ayant roulé avec célérité. Ramassant une poignée de poussière, graviers et sable mêlés, il la lança dans le visage du géant. Puis il chercha son poignard des yeux, qui avait glissé vers le bord de la plateforme. Il se jeta pour le récupérer, mais fut happé dans son bond par le sac qu’Ernaut, à demi aveugle, avait une nouvelle fois fait tournoyer comme un fléau.

Il s'effondra et, emporté par son élan, manqua l'arme qu'il escomptait retrouver, ses jambes et une partie de son buste dérapant hors de la terrasse, bien près d'être complètement entraîné. Il labourait avec désespoir le sol de ses longs bras, projetant de la pierraille tout autour de lui sans parvenir à se hisser, glissant inexorablement. Un instant, le temps se ralentit pour Ernaut, à demi courbé devant son adversaire, le visage recouvert de saleté, de sueur et de sang. Encore à demi-aveuglé, il s'essuya à l'aide de sa manche, y déposant plus de sang qu'il n'enlevait de poussière. Puis il plongea ses yeux dans ceux de l'assassin et lui renvoya la même image de folie que celui-ci avait l'habitude de donner. La terreur et la fureur meurtrière avaient changé de camp. Alors, sans hésitation, un sourire barbare aux lèvres, Ernaut décocha un violent coup de pied dans la tête de Roussel, qui lui fit lâcher prise tandis que son cou cédait dans un craquement sinistre.

Le silence fut alors total et seul le vent qui lui rafraichissait le visage rappelait à Ernaut qu'il était toujours vivant. Inspirant lentement, il sentit également ses veines palpiter dans son cou, dans son bras blessé et la douleur commença à se manifester. Il s'assit par terre, s'essuyant de nouveau les yeux à l'aide de son vêtement, un peu hébété. Sans qu'il ne puisse être tout à fait certain de ce qui s'était passé, il ne put s'empêcher de repenser à ce qu'il avait cru entendre. Alors même qu'Ernaut le contemplait, le visage empli de fureur de Roussel s'était calmé un bref moment, et, plein d'espoir, il avait articulé, presque murmuré un prénom, « Asceline ». Juste avant qu'Ernaut ne le tue.

### Fin d’après-midi du dimanche 31 mars 1157

Ernaut était assis sur un petit tonneau dans la cour, non loin du cadavre désarticulé qui avait été pudiquement caché d’une toile. Une main secourable lui avait fait passer un chiffon et un seau d’eau et il s’efforçait de reprendre figure humaine, les muscles endoloris et le corps couvert d’ecchymoses.

Plusieurs sergents étaient arrivés, Eudes parmi les premiers, et s’employaient à empêcher les badauds de s’approcher, dans l’attente de la venue du mathessep qui avait également été prévenu. Les plus curieux étaient dans la galerie juste au-dessus et tendaient l’oreille dans l’espoir d’en apprendre plus sur le drame qui s’était déroulé dans leur hôtellerie. Eudes avait aidé le jeune homme à rassembler ses affaires et avait insisté pour qu’il se débarbouille avant de répondre aux questions. Puis il avait ensuite longuement étudié la dépouille de Roussel, était allé inspecter le lieu du combat sur le toit.

Désormais, il attendait, assis sur le rebord d’un des abreuvoirs de pierre, silencieux, le regard sombre. À l’arrivée de Ucs de Montelh, tout s’anima de nouveau. Le mathessep fronça les sourcils à la vue du corps, mais s’avança sans colère ni animosité en direction d’Ernaut.

« Alors, jeune Ernaut. Conte-moi ton histoire. Comment ce pauvre hère s’est-il retrouvé le corps brisé, étendu en cette cour ? »

Ernaut inspira longuement, espérant trouver comment résumer tout cela de façon claire.

« C’est de certes le murdrier qui frappait les femmes, maître Ucs. Il a tenté de frapper l’enfançon en pleine église.

— Et pourquoi a-t-il fait ça ?

— Je ne saurais dire. Il a frappé les deux femmes et le pauvre Nirart, le premier. Puis semble avoir pourchacié l’enfant. Nous avions pensé un moment que c’était peut-être son fils et que l’une des femmes était son épouse qui l’avait trahie. Vu qu’il a tenté de poignarder et l’enfançon et la mère, je ne le crois plus.

— Était-ce pérégrin ? »

Ernaut acquiesça vigoureusement.

« Nommé Maciot. Il avait été capturé en même temps que ses victimes, peu après la prise d’Ascalon. Il est parvenu à fuir tandis qu’il œuvrait à des remparts, au nord.

— À Baalbeck précisa Eudes.

— Oui, c’est cela. Il a été retrouvé et confié aux frères et a trouvé emploi comme valet chez eux, se faisant appeler dès lors Roussel. Jusqu’à ce qu’il traque ces malheureux à leur libération. »

Ucs de Montelh, les mains sur les hanches, écoutait avec attention, la tête penchée, tout en laissant son regard dériver sur l’assistance et les environs. Estimant qu’il voulait en savoir plus, Ernaut reprit.

« Le garçonnet pourra peut-être nous narrer les faits. Il est sauf, j’en suis acertainé, c’est mon bras qui a reçu le coup qui lui était destiné. J’ai pris sur moi de le traquer et il m’a mené ici, sur les toits, où nous nous sommes affrontés. C’est de là qu’il est tombé… »

Interrompant Ernaut, Eudes s’avança alors, en présentant un solide poignard.

« Voilà l’arme de ce meurtrier. Il l’avait encore à la main. »

Un peu surpris, Ernaut tourna la tête vers le sergent, s’apprêtant à le reprendre, mais voyant que l’autre l’ignorait, il se retint. Le mathessep se tut quelques instants pour réfléchir.

« C’était sa seule arme ?

— La seule que nous ayons trouvée était sur lui. Rien là-haut. »

Ernaut repensa alors à son couteau, qu’il n’avait pas pris la peine de ramasser sur le toit . Pourtant, il n’ouvrit pas la bouche.

« Et c’est la chute qui a tué l’assassin ?

— Oui, il a le corps brisé. Aucune autre blessure. » Acquiesça Eudes.

Ucs de Montelh tendit la main pour saisir l’arme et l’examina d’un air détaché, se tournant de nouveau vers Ernaut.

« Il n’a pas touché l’enfant ? Seulement vous ?

— En fait, il pointait Oudinnet. C’est parce que je l’ai mis de côté que mon bras a reçu navrure.

— Et malgré cela, vous avez décidé de le poursuivre, sans arme, aux fins de le capturer dites-vous ? »

Ernaut fit bien attention à ne pas lancer de regard vers Eudes lorsqu’il proféra son mensonge. Il se força à fixer le corps allongé sans vie à quelques pas d’eux.

« C’est ça. Je me disais qu’il était bien maigrelet et que je pourrais sans nul doute le maîtriser. En cela j’ai fait erreur. »

Le mathessep rendit le couteau à Eudes et soupira ostensiblement. On aurait dit qu’un lourd fardeau venait de disparaître de ses épaules.

« Il faudra vérifier que certains peuvent abonder en votre sens et peut-être vous faire garantir des témoins, jeune homme. Au cas où de la famille de ce chien viendrait à demander réparation. Mais pour ma part, tout est clair, vous n'avez fait que vous défendre et je ne vois pas là nécessité de vous engeôler dans l'attente d'une éventuelle plainte. C'est une histoire terminée, qui a fin comme début, de sang. »

Lorsqu'il prononça ces derniers mots, son regard demeura accroché à celui d'Ernaut, cherchant à y lire une entente tacite, un accord à ce qu'il venait d'annoncer officiellement au vu et au su de tous.  Puis il se détourna. Il allait s'en retourner lorsqu'il se ravisa. Il revint près d'Ernaut, adoptant une posture un peu solennelle. Son visage arborait un sourire énigmatique tandis qu'il parlait.

« Et si d’aventure vous envisagez possible de servir le roi Baudoin, il est prévu que nous recrutions nouveaux sergents d’ici peu. Vous pourriez intéresser le vicomte, j’en suis acertainé. D’autant que je peux le constater, vous comptez déjà compaings parmi nous. Alors, réfléchissez-y jeune homme. »

N’attendant pas de réponse, il donna quelques ordres puis repartit sur le chemin de la ville. Ernaut se tourna alors vers Eudes, qui était toujours là, à jouer machinalement avec le couteau de l’assassin.

« Il ne nous a pas crus, si ?

— Aucune idée. D’aucunes façons, je ne suis pas sûr que cela ait conséquence. Il est heureux du dénouement : le calme va revenir, les troubles sont terminés. »

Il ajouta :

« Il laisse la justice à d’autres » en pointant son index vers le ciel.

« Pourquoi avoir menti ? Le couteau était en haut, je le sais bien, et le mien devait être avec, d’ailleurs.

— Pourquoi pas ? Cela a évité moult questions ennuyeuses. C’était lui le murdrier, un bien dangereux fol, il l’a prouvé. Inutile de s’empêtrer parmi détails et risquer des ennuis. Dieu a décidé qu’il devait tomber de là-haut, alors ergoter des jours durant pour savoir si tu étais bien en train de batailler pour ta vie, je n’en vois pas l’intérêt. Sa mort nous fait économiser le pain et l’eau qu’il aurait goboyé en attendant sa pendaison. Maître Ucs l’a bien dit : fin de l’histoire. »

Ernaut acquiesça en silence. Cependant, il repensait au coup de pied qu’il avait donné. Un sacré coup de pouce à la volonté divine ! Il n’était pas suffisamment orgueilleux ou impie pour s’être cru la main de Dieu en cette occasion. Il ne regrettait pas vraiment son geste, s’interrogeait juste sur le plaisir que celui-ci lui avait procuré. L’impunité qu’on lui accordait contribuait grandement à sa stupéfaction. Il s’était abandonné à une rage meurtrière et pour tout châtiment, on lui offrait une opportunité de travail qui ne serait pas pour lui déplaire. Le royaume du Seigneur sur terre lui réservait bien des surprises…

Finissant de se débarbouiller, il indiqua le corps, dépité :

« Le pire, c’est qu’on ne sait même pas pourquoi il a fait tout ça. Les raisons pour lesquelles il les a meurtris, pourquoi il parlait sans arrêt de sa soi-disant femme à tout le monde. »

Eudes haussa les épaules.

« L’enfant nous en dira peut-être plus. Je n’ai pas encore abandonné tout espoir de comprendre ce dément. »

Tourmenté, Ernaut fronça les sourcils.

« Y a-t-il vraiment usage à cela, le pouvons-nous seulement ?

— La vérité n’est pas toujours belle à savoir, mais sa lumière peut nous éclairer à l’avenir. Sans cela, il me serait difficile de supporter pareils outrages. Il me faut entendre comment celui qui a été baptisé en Dieu peut devenir tel un démon. Pour m’en garder. Jamais je ne veux finir comme l’un d’eux, la bave aux lèvres. »

### Soirée du dimanche 31 mars 1157

Assis sur un banc, Ernaut sirotait le verre de vin qu’on lui avait servi. De retour de la muraille nord, il avait été conduit avec quelques autres jusqu’à la salle où siégeait habituellement la Cour des Bourgeois. Il y avait retrouvé son frère Lambert, Hersant et Oudinnet.

L’enfant était recroquevillé, grignotant quelque friandise sous le regard compatissant de plusieurs personnes. Se trouvait également là Fouques, l’homme du vicomte, qui avait été le premier sur les lieux, peut-être celui qui les avait tous fait mener ici. Il était habillé de fort belle manière et s’entretenait avec chacun des sergents qui pénétraient dans la pièce. Pour l’heure, il échangeait à voix basse avec Eudes, se tournant de temps à autre vers certains des membres de l’assemblée. Puis entra le chanoine Giraud, l’air toujours aussi las.

Son visage renfrogné indiquait clairement à tous son exaspération, tout en n’incitant guère à lui en demander la raison. À peine arrivé, il se fit servir de quoi boire et demeura un peu à l’écart, étudiant les présents les uns après les autres. Lorsqu’il croisa le regard d’Ernaut, il lui offrit un rictus qui tenait autant de la grimace que du sourire. Le jeune homme, circonspect, se contenta d’un salut appuyé, inclinant la tête.

Enfin, entra dans la pièce frère Matthieu, dont on aurait dit qu’il était encore plus mal en point que précédemment, accompagné du père Thomas. Le premier, drapé dans un manteau de l’ordre à croix blanche n’était habillé que d’une bure fatiguée, bien trop ample pour lui. Son compère était mieux vêtu, mais il arborait néanmoins une tenue d’une grande simplicité, de toile de laine non teinte, de couleur peu uniforme. Leur allure paraissait étonnamment modeste par rapport à la magnifique étoffe du chanoine. Ils n’eurent pas le temps de saluer tous les présents que Fouques prit la parole d’une voix autoritaire.

« Nous voilà tous présents. C’est parfait. Le vicomte voulait faire assavoir à tous que cette sombre histoire est terminée. Le vaillant jeune homme que voici – il désigna Ernaut de la main – a mis hors d’état de nuire le démon qui s’en prenait aux pérégrins. »

L’annonce déclencha un bruissement spontané, signe de relâchement de la tension qui les habitait tous.

« Ce fou a même tenté de frapper le garçonnet pendant les cérémonies en l’église du Saint-Sépulcre, mais n’a point réussi, ajouta Fouques.

— Était-ce cela qui explique la cohue ?, intervint frère Giraud. »

Ernaut hocha la tête et Fouque confirma d’une voix forte.

« Certes. Le jeune Ernaut a dû intervenir et l’a poursuivi, au péril de sa vie. Vous voyez d’ailleurs qu’il a reçu navrure en le bras à cette occasion.

— Le sang a été versé en un saint lieu, quelle tragédie ! Quel sacrilège ! Il va nous falloir le purifier… continua le chanoine.

— Peut-être pas, intervint Ernaut, car le coup me fut porté alors que j’arrivais à l’escalier. Nulle certitude que ce soit en saint lieu… »

Un peu rasséréné, le clerc pinça la bouche, à demi convaincu. Examinant Ernaut des pieds à la tête, il semblait le considérer comme le responsable de cette profanation. Fouques continua :

« Il faudra faire annonce, aux fins de rassurer le peuple assemblé en la cité, avant qu’il ne reparte. Ne laissons pas méchantes et fausses rumeurs être colportées au retour. »

Tout le monde hocha la tête en silence. Puis frère Matthieu, se grattant le menton, interpella l’assemblée, prenant à témoin l’homme du vicomte.

« Savons-nous pour quelles raisons ce pauvre diable a frappé mortellement ces femmes ?

— Et deux hommes ajouta Fouques. Non, pas de sûr, peut-être que cet enfançon saura nous narrer le fin mot de l’histoire. »

Le petit groupe se tourna alors vers Oudinnet, qui était toujours collé à Hersant. Lambert s’était également efforcé de le rassurer, lui parlant d’une voix douce. Devenir soudain l’objet de l’attention de tous déclencha chez lui un repli instinctif et son visage se ferma, tandis que ses yeux roulaient, comme fous, pointés vers le sol. Voyant cela, Eudes s’agenouilla et s’adressa à lui comme il l’aurait fait à un de ses propres enfants.

« N’aie crainte, Oudinnet, tu n’as ici qu’amis. Nous aimerions juste que tu nous narres ton histoire. »

N’obtenant qu’un silence renfrogné pour toute réponse, il tenta plusieurs approches, aidé à la fin par Lambert. Petit à petit, l’enfant reprit courage et commença à s’exprimer par monosyllabes, par des hochements de tête, avant de s’enhardir. Pour ne pas le brusquer, le sergent ne lui avait posé au début que des questions simples. Il en arriva peu à peu à leur faire récit de leur épouvantable histoire.

Oudinnet, sa mère, Nirart de Carville et son épouse Phelipote, Adequin le Foulon et sa femme Asceline, ainsi que Maciot Point-L’Asne, Amalric et quelques autres étaient du même village et s’étaient embarqués ensemble pour l’Outremer, ayant fait vœu de visiter le tombeau du Christ. Un moment, Ernaut avait tenté d’interpeller l’enfant sur le fait qu’il devait faire erreur à prétendre Asceline mariée à un certain Adequin, mais voyant qu’Oudinnet s’entêtait, il finit par le laisser continuer.

Le destin avait voulu qu’ils soient capturés par des pirates musulmans peu avant l’hiver 1155. Ils avaient longtemps attendu, parqués derrière des grilles de bois dans des sous-sols humides. Répartis en petits groupes, ils se soutenaient les uns les autres, de la voix lorsqu’ils ne partageaient pas la même cage. Leur seul réconfort était issu de leurs prières, de la chaleur qu’ils se communiquaient les uns aux autres. Parfois, leurs geôliers venaient en chercher un certain nombre, vidant progressivement les différents enclos, n’emmenant que les jeunes femmes, les hommes les plus costauds ou un ensemble indistinct.

Peu à peu ils avaient perdu le compte du temps qui s’écoulait. Plusieurs prisonniers avaient fini par décéder des mauvais traitements, de fièvres malignes ou des suites des blessures lors de leur capture. En outre, il n’y avait plus d’arrivée de nouveaux captifs, sans qu’ils sachent pourquoi. Fouques expliqua que c’était certainement dû à la trêve et à une meilleure protection des côtes. Oudinnet acquiesça, sans être trop sûr de comprendre puis continua de sa petite voix.

« Il ne demeurait que mère, maître Nirart et son épouse en notre geôle. Il y en avait une autre, où ils tenaient les malades et les blessés, confiés à Maciot et Asceline. Elle avait prodigué force soins à son époux et à Point-l’Asne quand ils avaient eu des fièvres et seul Maciot avait survécu. Depuis, les païens leur confiaient la tâche de s’occuper des… plus faibles. »

L’enfant s’arrêta, ressassant un mauvais souvenir. Il fallut qu’Hersant le berce doucement un petit moment avant qu’il ne reprenne.

« Ils faisaient de leur mieux, mais n’avaient guère espoir. Leur geôle était emplie de morts et, la nuit, on entendait Asceline pleurer et Maciot lui chuchoter réconfort. Jusqu’au jour… où il lui a fait du mal.

— Comment ça du mal ? Interrogea Fouques.

— Il… Je ne sais pas. Je… dormais. Ses cris m’ont éveillé, elle hurlait comme démente, et lui était sur elle. Mère s’est mise devant moi et a tenté de me boucher les oreilles. Mais j’ai bien ouï qu’il lui faisait mal. »

Les hommes regroupés autour de lui échangèrent des regards entendus, comprenant à demi-mot ce qui s’était passé. Lambert invita Oudinnet à poursuivre.

« Et ensuite ?

— Ça a recommencé. Plusieurs fois, des jours durant. Nous ne pouvions rien, et personne ne venait aider la pauvre Asceline. Et puis un jour, il s’est passé quelque chose, il lui a crié dessus, il lui disait qu’elle ne devait pas le frapper. Alors il l’a battue, souvent. Aussi souvent qu’il… Et puis un jour elle n’a pas crié.

— Personne n’a rien fait, rien dit ? » Demanda Eudes.

L’enfant renifla, hésita un court moment puis rétorqua d’une voix fluette.

« Les païens étaient fort mécontents. Ils ont tapé Maciot et l’ont mené hors. On l’a plus jamais revu jusqu’à ce que les frères nous libèrent. »

Le petit groupe souffla et se redressa, ayant obtenu la réponse à la question qui les taraudait tous. Ils pesaient chacun dans leur tête le poids de ces révélations. Ernaut fut le premier à briser le silence qui s’était installé.

« Le pauvre bougre était complètement fol depuis lors. Asceline n’avait jamais été sa femme, malgré ce qu’il prétendait.

— Un fieffé coquin, oui, un menteur, un violeur, un murdrier ! » Répliqua Giraud, l’air emporté, d’un timbre de plus en plus véhément, s’éteignant dans les aigus.

Ne laissant pas se développer la gêne entre eux, Frère Thomas intervint de sa voix fluette.

« Il s’est efforcé au bien un temps. Il œuvrait à l’hôpital, avait cherché à soulager son âme, mais n’y était pas parvenu, malgré mes suppliques. J’entrevois désormais l’abîme de noirceur dans lequel il était plongé. Jamais je n’aurais pensé pareils abysses.

— Comprenez, frère, il disait partout à chacun, et même à ses amis, qu’elle devait bientôt le rejoindre. Il en était persuadé. Le démon a pris possession de son âme en ces geôles ! Répliqua Ernaut.

— Il a succombé à la chair et a eu grand hontage, voilà tout. Il n’était guère plus que bête ! Il grillera en Enfer ainsi qu’il sied à pareil animal. »

La déclaration péremptoire de Giraud stoppa toute velléité de réponse de la part des présents. Frère Thomas échangea un regard désolé avec Matthieu puis Ernaut. L’hospitalier était peut-être plus enclin au pardon, ayant l’occasion de fréquenter d’anciens captifs, et toute la misère du monde.

Néanmoins le jeune homme fut troublé par l’affirmation. Il lui apparaissait que les démons de Maciot lui faisaient croire que ses actes étaient guidés par l’amour. Pourtant il ne propageait que haine et destruction autour de lui. Il repensa à son coup de pied et à l’horrible craquement. Il en avait éprouvé une grande joie sur l’instant. Ne venait-il pas d’ouvrir en grand son cœur à l’influence du diable ?
