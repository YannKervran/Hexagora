## Chapitre 4

### Jérusalem, église du Saint-Sépulcre, matin du dimanche 14 avril 1157

Depuis son enfance à l’ombre de la basilique de la Madeleine à Vézelay et encore plus dès l’instant où il avait entendu parler des voyages outremer pour combattre les infidèles ou mettre ses pas dans ceux du Christ, Ernaut avait toujours été persuadé qu’il existait quelques lieux à part sur terre. Des endroits d’où la souillure était exclue, où le mal n’avait pas de prise, où il serait à l’abri des démons qui l’assaillaient ou l’habitaient.

Contemplant les marchands à l’entrée du temple du Seigneur, il réalisait combien il avait été naïf et que le diable rôdait sans cesse aux alentours, qu’aucun sanctuaire ne restait longtemps inviolé, pour peu qu’un homme en foule le sol. S’intégrant aux groupes qui s’amassaient pour pénétrer sous la haute lanterne face au chœur, Ernaut s’efforça de ne plus penser à rien, cherchant à se fondre dans la masse des fidèles qui s’apprêtaient à assister à la messe matinale des chanoines. Après un petit moment où les serviteurs allaient et venaient sous le vacarme des cloches toutes proches, préparant le lieu à la cérémonie imminente, le silence se fit et le calme revint.

Les discussions se turent ou, du moins, baissèrent d’intensité. D’un coup éclatèrent les voix harmonieuses des clercs :« *Misericordia Domini plena est terra*[^misericordia]… » Séparés en deux files, ils avançaient depuis chacun des côtés du déambulatoire. Emplissant de leurs neumes élégants les hautes voûtes du sanctuaire, ils prirent place peu à peu sur les gradins entourant le maître autel. Tout de blanc vêtus, ils arboraient un air grave et sérieux, pénétré de la dignité de leur fonction. La messe commençait.

Malgré toute sa bonne volonté, Ernaut n’arrivait guère à appréhender ce qui se passait. Il était habitué au rythme des démonstrations, s’attendait à certains mouvements. Seulement, sans un clerc pour apporter des explications avant ou après la cérémonie à laquelle ils assistaient, la plupart des fidèles n’étaient pas en mesure de la comprendre. Concentré malgré tout sur ce qu’il regardait, il tentait à toute force d’y discerner la solution à ses problèmes, d’y voir jaillir le moyen de démêler le trouble qui l’habitait. Lambert était parti quelques jours à la Mahomerie, afin de se rendre compte de la réalité de l’offre qui était faite de s’installer là-bas, sur les terres du Saint-Sépulcre. Il s’était montré enthousiaste à l’idée de s’y rendre, bien qu’un peu déçu qu’Ernaut ait préféré demeurer à Jérusalem.

Devant le front soucieux de ce dernier, il avait vite compris que son frère avait ses propres problèmes à régler. Il était dans le vrai, mais ce n’était pas que cela. Ernaut répugnait à s’éloigner la ville où il savait pouvoir de temps à autre rencontrer Libourc à une occasion ou l’autre. Sanson était plus en forme, toutefois pas assez pour quitter l’hôpital définitivement et il faudrait encore quelques jours, sinon semaines, pour qu’il soit en mesure de partir de la cité sainte.

L’adolescent avait quelques réticences à rompre ainsi le timide lien qui le reliait à la jeune fille de ses pensées. Pourtant, il hésitait à provoquer des rencontres, craignant par son attitude ou sa présence seule d’effrayer les parents, voire Libourc elle-même. Il savait que la pureté de ses intentions n’était pas forcément un gage pour l’avenir. À tout moment, les démons pouvaient faire dévier son chemin, sans espoir de Salut. Même lorsqu’on croyait être enfin arrivé dans la vallée des larmes, il s’avérait souvent qu’il demeurait encore un long parcours jusqu’au bout de l’horreur. Quelle qu’ait été la situation difficile dans laquelle on pouvait se débattre, il n’était jamais certain qu’on ne se retrouve rapidement en plus fâcheuse posture.

### Jérusalem, abords de Saint-Martin, matin du jeudi 28 mars 1157

Ce fut la fraîcheur davantage que le bruit des cloches finissant de sonner qui parvint à réveiller Ernaut. Il avait tellement bougé qu’il n’avait plus de draps sur lui et se tenait pelotonné contre le mur. Lorsqu’il se décida à ouvrir un œil, il chercha du bras les tissus tombés au sol, grognant de mécontentement. Il s’aperçut également qu’un jour blafard filtrait sous la porte et le long des volets.

Estimant qu’il était trop tôt pour commencer sa journée, il frissonna, attrapa la couverture et se roula en chien de fusil, dans l’espoir de se réchauffer. Il avait la tête lourde, la langue pâteuse et n’avait nulle envie de se lever. Les oreilles lui avaient sifflé un temps et désormais il n’entendait que le vent qui chuintait par intermittence en passant sous l’huis, accentuant la sensation de froid.

Il demeura un long moment, retombant dans un demi-sommeil, se tournant dans un sens et l’autre, s’étirant parfois, cherchant à s’éveiller tout à fait avant de quitter son lit. Restant emmitouflé de son mieux, il s’assit d’un coup sur le bord du matelas, se frottant énergiquement le visage en espérant que cela le réveillerait tout à fait. Il réalisa qu’il avait peut-être bu plus qu’il n’aurait dû la veille et s’était vraisemblablement couché bien trop tard. Il lui fallut un bon moment pour se résoudre à affronter l’extérieur.

Il n’était vêtu que de ses braies et avait enfilé ses souliers en savate car il avait l’intention de se préparer dignement pour les célébrations de Pâques qui commençaient aujourd’hui. En premier lieu, il prévoyait de se faire raser par un barbier proche. Un ciel bas, gris et peu lumineux, ponctué de nuages ardoise, bouchait l’horizon et une brise froide et humide descendait des collines de l’ouest. La ville était calme, comme endormie, et Ernaut trouva l’atmosphère lugubre.

Il s’assit sur un tabouret, devant le seau que Saïd avait laissé à son intention et s’aspergea vigoureusement avant de faire une toilette sommaire. L’eau fraîche vint tout de même à bout de son engourdissement et, tandis qu’il enfilait ses beaux habits de laine, il se sentait ragaillardi. Il frotta énergiquement sa cotte, comme promis à son frère, parvenant à lui redonner un peu d’éclat. Il entreprit même de recréer sa coiffe enturbannée, avec un résultat, bien qu’encore imparfait, qui s’améliorait par rapport aux fois précédentes.

Après s’être fait raser, il avait l’intention de s’acheter de quoi se restaurer tout en se rendant à l’hôpital, comme il le faisait souvent. Il espérait également retrouver le groupe de pèlerins français où se trouvait Libourc et le chien de garde qui lui servait de mère. Pendant la procession, qui rejoindrait l’église Sainte-Marie au Mont Sion, au-delà du mur sud de la cité, il pourrait se rapprocher de la jeune fille sans que son gardien ne puisse faire un scandale, étant donné les circonstances.

Le cœur léger, sifflotant pour lui-même, Ernaut déboucha dans la rue marchande à côté de chez lui et fut surpris de voir que tous les commerces y étaient fermés. Interloqué un instant, il finit par se dire que c’était là chose normale pour un jour férié. Il oublia donc les soins capillaires et se dirigea d’un bon pas vers Malquisinat, où il y aurait certainement tout de même quelques vendeurs d’oublies, de fruits ou de pain, pour les voyageurs affamés.

Chemin faisant, il s’aperçut que les rues étaient quasi-désertes, y compris aux environs du change des Hospitaliers. L’activité y était généralement frénétique, avec les principales artères nord-sud et est-ouest de la ville qui s’y croisaient. Les volets des échoppes étaient rabattus et aucun badaud n’était en vue à part lui. Même pour un jour de célébrations, il y aurait dû y avoir quelques personnes se rendant auprès d’amis, rejoignant des compagnons pour aller ensemble à l’église et faire la procession de concert.

Inquiet de ce calme inhabituel, il pressa le pas vers le quartier de l’Hôpital, où il était certain de trouver du monde, le service des malades ne s’arrêtant pas pour les fêtes. Étrangement, la place devant était quasi-déserte. Quelques valets étaient affairés autour d’une charrette à bras auprès d’une poterne aux abords de Sainte-Marie Majeure. Un vagabond fouillait du bout de sa canne un tas d’immondices, déchets invendus du marché et restes de nourriture rejetés là par les négociants. La porte de l’hospice était fermée à demi, le passage barré par un domestique somnolant sur son tabouret. Ernaut le fit sursauter lorsqu’il l’aborda. L’homme était visiblement fatigué, les cernes noirs donnaient à son visage un aspect inquiétant, renforcé par un regard un peu fou et des cheveux bruns coupés aléatoirement, coiffés en bataille.

« Qu’est-ce qu’vous faites là ?

— Je viens visiter mon frère, comme chaque matin, ou de peu.

— Il doit être à la messe, à c’te heure. Vous y êtes pas, vous ?

— J’irai à l’office principal seulement, j’avais à faire avant cela.

— L’office principal ? Mais il est débuté déjà !

— Pardon ?

— Vous parlez bien de la messe à Sainte-Marie, après la procession ? »

Ernaut se mit à douter de l’intelligence de l’homme ou de sa propre intégrité intellectuelle, il n’aurait su le dire.

« Euh… Oui, bien sûr. »

Le serviteur regarda de droite et de gauche, comme s’il cherchait là une information qu’il ne détenait pas. Ou qu’il vérifiait que des complices n’étaient pas cachés, riant de la bonne blague qu’ils étaient en train de lui faire.

« Les cloches ont sonné tierce voilà bien longtemps, et la procession est passée à côté depuis un petit moment.

— Vous devez vous tromper. Je n’ai rien ouï.

— Alors vous devez être sourd. Les cloches ont battu à toute volée pour annoncer le départ de la procession du Patriarche. On a dû les percevoir jusqu’à Damas !Tout le monde y est, pour sûr. »

Devant l’air abasourdi d’Ernaut, le domestique eut un sourire qu’il s’empressa d’effacer, de peur de contrarier le tas de muscle, apparemment pas très finaud, qu’il avait en face de lui.

« Vous devriez vous hâter pour rejoindre l’église, je ne pense pas qu’ils aient beaucoup avancé. En vous empressant, vous arriverez peu après eux. Vous savez comment on y va ? »

Mais l’homme n’eut jamais sa réponse. Ernaut partit comme une flèche, trottant plus que marchant avant de se mettre carrément à courir. Il montait et descendait les rues en bondissant dans les escaliers, rasant les murs et les angles aux intersections, au risque de culbuter quiconque aurait eu la mauvaise idée de venir en sens contraire.

Il savait que pour faciliter les déplacements, la porte du Mont-Sion serait ouverte, à peu de distance de l’église, et se dirigeait donc vers le sud avec toute la célérité dont il pouvait faire preuve. Malgré le temps frais, il eut bientôt des gouttes de sueur qui coulaient sur son front et il ôta son couvre-chef qui lui tenait chaud, sans ralentir le rythme.

Les gardes de faction à la porte de Belcaire ne purent saisir l’opportunité de se moquer du retardataire, il passa dans un souffle, profitant de la pente pour sauter en de larges enjambées. Les soldats, prudents, s’écartèrent, incapables qu’ils auraient été de stopper le taureau furieux qui arrivait en droite ligne depuis plusieurs centaines de mètres par la rue des Arméniens. Il fallut d’ailleurs quelques mètres à Ernaut pour freiner, au vu de l’énorme masse de pèlerins agglutinée aux abords de l’église, dans l’ombre de la grande basilique.

Ne voulant pas trop se faire remarquer, il continua son approche rapide, mais sans plus courir, et se contenta de marcher vivement, histoire de retrouver son souffle à l’abord des bâtiments. Lorsqu’il arriva, la foule amassée était silencieuse et finissait d’entrer dans le sanctuaire. Tentant de se faire petit, il s’efforça de se faufiler afin de progresser dans le flot de croyants qui remplissait peu à peu toute la cour. Il écrasa quelques pieds, bouscula des épaules et scinda des groupes, et parvint ainsi à pénétrer dans le grand patio d’où il avait une bonne vue sur l’édifice, ainsi que sur les fidèles.

Tout en avançant, il jetait de temps à autre un regard sur la foule amassée, cherchant la chevelure brune familière. Soufflant comme un bœuf, et s’essuyant le visage rougi par sa course, il fit quelques sourires désolés aux alentours, s’efforçant de retrouver une attitude plus digne, et moins haletante, pour un pèlerin. Il avait bien failli manquer le début des cérémonies les plus importantes de Pâques.

### Sainte-Marie du Mont Sion, matin du jeudi 28 mars 1157

Les abords de l’édifice étaient remplis de tas de moellons, de poutraisons, de fosses de mortier, au milieu des loges des artisans. Les pèlerins se faufilaient partout où il n’était pas trop dangereux de s’installer, et certains n’hésitaient carrément pas à monter sur les échafaudages laissés là par les ouvriers occupés à reconstruire l’église.

Ernaut réussit malgré tout à forcer le passage jusqu’au niveau où l’ancienne basilique, couverte en charpente, laissait place désormais aux hautes voûtes de pierre qui avançaient depuis l’abside à l’est. Le fond du sanctuaire, la partie la plus sacrée, était même déjà ornée de peintures et de mosaïques, ainsi qu’il avait pris l’habitude d’en voir fréquemment en Terre sainte.

Sous les riches décors, dans le chœur, de très nombreux prélats s’étaient disposés, arborant souvent mitre et crosses en plus de leurs splendides tenues liturgiques. Au centre, plus somptueusement vêtu encore que les autres, croulant littéralement sous les orfrois de ses soieries, le vieux patriarche Foucher semblait achever d’une voix forte un sermon que les fidèles se répétaient en vague jusqu’aux plus éloignés. Le murmure des chuchotements finissait par couvrir ce qui pouvait s’entendre de la célébration.

Ernaut tournait les yeux en tous sens, dans l’espoir toujours de reconnaître les nattes de Libourc parmi les têtes hirsutes, échevelées ou sobrement voilées. Il lui fallut peu de temps pour apercevoir le petit groupe qu’il espérait, le long du côté nord de la nef principale, dans la partie ancienne non encore refaite. Profitant de ce qu’un cortège mené par un prêtre se mettait en branle, tous la tête basse, humblement vêtus et les pieds nus, il entreprit sa traversée, multipliant les excuses et les sourires sans tenir aucun compte du tumulte qu’il créait. Par moment, il jetait tout de même un coup d’œil vers le fond de l’église, suivant distraitement ce qui s’y passait.

Il s’efforçait d’arborer un visage de circonstance, dans l’éventualité où Sanson et les siens auraient l’idée d’observer derrière eux. Il n’était pas naïf au point de penser qu’il pouvait avancer parmi la foule comme un bœuf traçant son sillon en toute discrétion. Lorsqu’il parvint finalement à sa destination, un *kyrie*[^kyrie] puissant commençait à monter depuis le chœur.

Les remous de son arrivée attirèrent les regards sur lui et lui valurent une œillade désapprobatrice de la mère de Libourc. Cette dernière, pour sa part, se contenta d’un rapide sourire, avant de retrouver une attitude emplie de respect et de dévotion. Seul Sanson lui fit un signe de la main. Il était appuyé à une des anciennes colonnes du bâtiment, soutenu par une béquille, un pied bandé délicatement posé au sol. Comme toujours, il arborait un visage enjoué et serein, étrangement goguenard, et semblait s’amuser de ce qu’il voyait. Conscient de ce qu’il était là pour assister à l’office, Ernaut estima être assez près de la jeune fille pour ne plus trop se préoccuper d’autre chose que de la cérémonie.

Adoptant une posture de recueillement, il finit par se concentrer sur ce qui se passait dans le chœur. Alternant hymnes et antiennes, récitations et lectures, les prêtres offraient un spectacle édifiant à la foule amassée dans la nef et au-delà. Tout se déroulant bien sûr en latin, et le plus souvent sans être tourné vers l’assemblée, peu étaient capables de suivre, de chanter ce qu’il fallait au bon moment ou de comprendre la signification des déplacements, transports d’objets ou textes déclamés.

Ce n’est que pour les prières les plus usuelles, comme le *Credo* qui fut lancé par l’ensemble des officiants après une lecture par un des religieux, que les fidèles osaient s’associer avec assurance à la congrégation des clercs. Ernaut ne fut pas en reste, heureux de pouvoir enfin participer des lèvres et de la voix au lieu de se contenter d’assister passivement à quelque cérémonie magique.

Comme chaque fois, néanmoins, son enthousiasme et la puissance de son organe, combinés à sa prononciation plus qu’approximative du latin firent sensation, et amusèrent quelques-uns des pèlerins les moins dévots alentours. Ce n’est que lorsque l’*Ite missa est*[^itemissa] fut lancé que le public envisagea de s’agiter de nouveau. Le frémissement fut de courte durée, car les cérémonies continuaient au niveau du chœur. Une nouvelle procession s’assembla au nord-ouest et commença à s’étirer lentement vers le levant.

En tête deux possesseurs de candélabres aux cierges de cire enflammés ouvraient la voie, aidés par des valets au-devant. Puis venaient deux célébrants, également vêtus de fine étoffe blanche, avec des croix. Les suivaient deux clercs avec des fioles contenant sans nul doute huiles saintes ou chrême, puis deux porteurs d’encensoirs. Enfin, en queue de cortège, des officiants, le visage fermé, lancèrent les chants des versets alors qu’ils s’avançaient dans la nef.

La progression était rendue difficile par la densité des fidèles, peu enclins à libérer leur emplacement, gagné souvent de haute lutte. Après un long moment, une nouvelle procession rejoignit la première auprès de l’autel, et les célébrations reprirent, alternant psaumes chantés et récitations psalmodiées. La ferveur de la foule était palpable devant ces divins mystères. Le père Ligier, qui était non loin d’eux, chuchotait de temps à autre la signification de ce qui se passait, permettant à tout le groupe de comprendre de la tête autant que du cœur les cérémonies auxquelles ils assistaient.

Peu à peu, le sentiment religieux avait gagné tous les présents. Le lavement des pieds de quelques pauvres méritants constitua un moment d’intense ferveur, certains n’hésitant plus à crier leur foi par des formules aussi pieuses que naïves. Malgré la longueur des célébrations, beaucoup furent surpris lorsque retentit une cloche qui annonçait le départ progressif de tous les officiants pour un lieu où les laïcs n’avaient pas leur place.

Emplie de volutes d’encens, l’église retomba dans un silence respectueux et semblait encore résonner de l’écho des chants. Ce n’est qu’après un long moment que des voix osèrent se faire entendre, tout d’abord quelques cris de ferveur, lancés par des fidèles enthousiastes, impatients de découvrir une éventuelle suite. Puis, très progressivement, les chuchotis gonflèrent, pour rapidement se transformer en clameur.

Reprenant pied après le magnifique spectacle sacré auquel il avait assisté, Ernaut commençait à avoir son estomac qui lui rappelait qu’il n’avait rien avalé depuis le vin de la veille au soir. Le souper de la fin de journée était encore loin, mais son système digestif ne semblait pas d’accord pour attendre jusque-là. Plusieurs pèlerins autour de lui partageaient d’ailleurs quelques en-cas, essentiellement fruits, pain et fromage. Des gourdes et des outres circulaient également de mains en mains.

La foule se faisait moins dense tandis qu’un certain nombre allait prendre l’air, visitait les environs, ou cherchait à repérer la fameuse tombe du roi David dont on disait qu’elle avait été récemment murée et dissimulée après sa découverte fortuite. Les plus enthousiastes se rapprochaient de l’autel, qu’ils touchaient et baisaient religieusement. Voyant que le jeune géant était démuni, Sanson lui fit signe de la main et lui proposa de boire à son outre dès qu’il fut assez près. Ernaut accepta avec chaleur et avala une large rasade de vin coupé d’eau.

Le vieil homme était visiblement fatigué, les traits tirés par l’effort que rester debout représentait pour lui. Néanmoins rien ne semblait entamer sa bonne humeur. Après un rapide coup d’oeil pour vérifier que tout le monde était occupé ailleurs, il indiqua à Ernaut de se rapprocher et lui déclara à mi-voix :

« J’ai poignée de noveltés pour toi, mon jeune compaing.

— Auriez-vous eu encontré l’époux ?

— Certes pas, sinon je te l’aurais dit fort avant. J’ai là bonne amie qui peut nous guider vers l’enfant. »

Il désigna de la main une petite femme, très mince, dont le visage à la peau desséchée paraissait avoir été posé sur un trop fin et fragile squelette. Visiblement édentée, elle s’efforçait d’attendrir un aliment qu’elle venait de mettre en bouche, le regard rivé sur les ornements colorés à l’extrémité de la nef. Ses yeux à l’iris blanchi par les années semblaient scintiller devant le décor qui s’offrait à elle.

Sanson l’appela avec douceur, mais suffisamment de fermeté pour la détourner de sa vision. La dénommée Hersant se retourna brusquement, comme si on l’avait trop tôt arrachée à un merveilleux rêve, faisant glisser en partie son voile. Une partie de ses cheveux gris cendre, ondulés, jaillissait désormais sur le côté de son visage, lui donnant une allure de folle. Sa démarche était sans hésitation et après s’être éclairci la gorge d’une toux grasse, elle répondit à l’appel d’une voix assurée bien que fluette, interrogeant Sanson du regard sur le géant blond qui la dévisageait.

« Hersant, voici Ernaut, le jeune maître bourguignon en quête de nos amis. Il serait peut-être de quelque usage que tu lui en apprennes à propos du jeune Oudinnet. »

À la mention du garçonnet, le visage sembla retrouver un peu de jeunesse et les pommettes surplombaient désormais des champs de rides, entourant un sourire ou pas une dent n’était visible.

« Que voilà bien mignon marmot, par ma foi ! J’ai terrible peine de l’avoir plus à bercer ces jours-ci !

— Comme tu l’avais en grande amitié, peut-être saurais-tu porter aide à Ernaut en ses recherches ? Nous avons grande peur que tragique destin n’ait frappé le marmouset, vu la triste fin de sa mère. »

La vieille femme se renfrogna en un éclair, se signant avec une vivacité insoupçonnable dans son corps maladif.

« Voilà bien horrible pensée en pareil endroit, maître Sanson. N’appelez pas le malheur sur sa tête ainsi, je vous en fais prière. Je sais bien qu’il est sauf ! Je l’ai vu tantôt… »

Ernaut écarquilla des yeux ronds :

« Vous l’avez vu dites-vous ? Voilà long temps ?

— Certes pas, je l’ai vu passer tandis que nous prenions place en la grand nef pour les offices. Ma vue n’est plus si bonne, mais je suis acertainée que c’était lui : bel enfançon, un peu maigrelet, avec vieille cotte de laine grise, rapetassée, et chausses baillantes. Sa chevelure n’était guère soignée, mais son brun pelage m’est connu, ainsi que sa façon d’être. Je lui ai fait signe, mais il ne m’a pas vu, il a disparu au parmi de la foule. »

Profitant de l’occasion, Ernaut se fit décrire en détail l’enfant, sa taille, sa corpulence, ainsi que le moindre élément qui pourrait servir à son identification. Hersant ne se faisait pas prier, le souvenir du garçonnet lui étant visiblement très cher. Elle était impatiente de le retrouver, et appréciait les efforts qu’Ernaut déployait en ce sens.

« Vois-tu, j’ai eu beaux garçonnets comme celui-là… Aucun n’a vécu bien loin, il me fait souvenance de mon Ayoul, paix à sa pauvre petite âme… »

La vieille femme était visiblement émue et ses yeux se mouillèrent rapidement, la laissant sans plus de voix. Sanson attira Ernaut à lui et murmura :

« La malheureuse a perdu ce qui lui restait de famille voilà quelques années, emportés par malines fièvres. On m’a dit que ce pauvre Ayoul avait été emporté après avoir eu le bras écrasé par meule de moulin. C’était du temps du Batailleur[^louis6], elle n’a jamais pu oublier. »

Ernaut hocha la tête, ému par la détresse de la vieille femme. Lui-même n’avait jamais connu sa mère, seulement une belle-sœur qu’il trouvait acariâtre, et était troublé par la tendresse qui émanait de ce corps cacochyme, souffreteux.

Il fit un sourire de connivence à Sanson et salua Hersant, lui soufflant qu’il allait voir s’il pouvait dénicher l’enfant parmi la foule. En se penchant, il aperçut du coin de l’œil Libourc qui regardait discrètement dans sa direction, par-dessus son épaule. Lorsqu’il s’éloigna, il avait le cœur gonflé d’un bonheur qu’il ne s’expliquait guère, sous les bénédictions silencieuses de la vieille Hersant que la joie rendait agitée.

Il s’appliqua alors à parcourir les nefs, passant d’un groupe à l’autre, interrogeant sur la présence d’un enfant ressemblant à Oudinnet et n’obtint que bien maigres confirmations, rien de définitif. Jusqu’à ce qu’il tombe sur une femme au long nez, le visage triste, qui parlait en faisant régulièrement siffler ses lèvres sur ses incisives. Elle était accompagnée d’un gros homme au regard jovial, qui n’émettait aucun son, se contentant prudemment de hocher la tête avec enthousiasme à tout ce qu’elle disait.

« Pour sûr, je l’ai bien vu, le marmouset a bien failli me faire choir ! Il courait presque ! En la nave de l’église, je vous demande un peu ! Nourri comme goret, mais guère éduqué…

— Auriez-vous souvenance de là où il se rendait ? »

La mégère fit une moue qui l’enlaidissait encore plus.

« Je ne saurais dire. On aurait dit qu’il avait le feu lui arsant les fesses. Il regardait derrière lui au lieu de devant…

— Il n’était pas seul ?

— Si. Du moins je n’ai vu personne avec lui, sinon j’aurais expliqué à ses parents que c’était là fort impertinent enfançon. »

Ernaut, un peu agacé par le ton péremptoire, ne put se retenir.

« Il n’a plus de parents, horriblement meurtris par féroces païens, et sous ses yeux en plus. »

Le visage de la femme s’allongea à tel point qu’Ernaut crut que sa mâchoire allait heurter sa forte poitrine. Évitant un sourire de triomphe qui lui aurait certainement valu quelques ennuis, il battit en retraite avant de devoir subir une nouvelle salve.

Il retrouva rapidement Sanson, qui discutait avec son épouse et sa fille, ainsi qu’avec le père Ligier. Ernaut n’osa pas les interrompre et entendit qu’ils se préparaient à veiller une partie de la nuit au jardin de Gethsémani, sur les traces de l’histoire du Christ. Réalisant qu’il n’avait guère prévu de moments d’introspection en dehors des grandes messes, Ernaut se risqua à demander la permission de se joindre à eux.

« J’aurais grand plaisir à ne point demeurer en priement tout seul en la nuit. Mon frère est toujours fort malade et tenu en l’hôpital. Rejoindre frères chrétiens en cette nuitée me paraît meilleure façon d’honorer mon vœu. »

Le père Ligier, qui encadrait le petit groupe acquiesça de la tête avec chaleur.

« Si fait, voilà sages paroles. Je ne suis pas de ces curés qui exigent dîme pour faire oraisons. D’ailleurs on me nomme Ligier, pas Simon, ajouta-t-il, un sourire aux lèvres. Viens donc te joindre à notre procession, nous allons faire route d’ici peu.

— Je vous adresse force mercis, père Ligier. Je vous retrouverai en le jardin des Oliviers, il me faudra accomplir rapide tâche avant la fin du jour.

— Attention, garçon, ne va pas confondre pérégriner et balader. Tu n’es pas là pour fouler pavés et graviers, mais pour aller en les pas du Christ. Cheminer ainsi, à chevaucher deux destriers, te mènera le cul à terre. »

Le visage du père Ligier demeurait chaleureux tandis qu’il rabrouait Ernaut, comme en attestait la lueur amusée qui brillait dans ses yeux. Son message n’en avait que plus de force. Un peu contrit, Ernaut salua en bredouillant, assurant ses amis qu’il les retrouverait rapidement.

### Début d’après-midi du jeudi 28 mars 1157

La foule refluait en désordre vers la cité. Quelques-uns, peu désireux de rester coincés à l’entrée, avaient décidé de lézarder un peu sur les versants exposés au soleil qui avait percé les nuages durant la longue cérémonie. Les plus prévoyants, dont Ernaut n’était pas, avaient même de quoi se restaurer et s’installaient un peu partout, jusque vers Siloé et la fontaine, en contrebas de la pente qui s’étirait vers le sud, rejoignant la route qui partait droit vers Bethléem.

Des cultivateurs, surveillant jalousement leurs parcelles en terrasses, vérifiaient que les pèlerins ne franchissaient pas les murets ni n’abîmaient les plantations ou les arbres. Quelques enfants jouaient en faisant remonter un troupeau de moutons en direction d’un abreuvoir de bois, loin en contrebas, sous un palmier. Pour les populations non chrétiennes, la vie continuait son cours, y compris les jours de fête.

Libourc et ses compagnons s’étaient avancés vers l’est et s’étaient rassemblés près d’un bosquet de cyprès. Ernaut hésita un instant à les rejoindre, mais la remarque récente du père Ligier, debout face à ses ouailles, incitait le jeune homme à la prudence. Il ne fallait pas se rendre indésirable mais laisser ces croyants enthousiastes jouir sereinement de leur voyage en Terre sainte. Il aurait bien le temps de profiter de la présence de Libourc durant la veillée.

Il était persuadé que si le garçonnet était si empressé, c’était parce qu’il était inquiet, voire poursuivi. L’agresseur n’avait certainement pas pu lui faire de mal au milieu de la foule, mais il avait pu l’effrayer. Suffisamment pour qu’Oudinnet s’enfuie et se rende, à son corps défendant, en un endroit où nul ne pourrait être témoin de sa mort. Plus il y pensait, plus Ernaut estimait la situation vraiment critique pour l’enfant. Il enrageait de n’avoir pas pu être là plus tôt et regrettait ses abus de la veille qui avaient permis cela. Il se jura de ne plus se laisser aller pareillement.

En attendant, il était indécis quant à ce qu’il devrait faire d’ici la nuit. Il était néanmoins persuadé qu’il ne pouvait tranquillement prier en sachant qu’un innocent était en grave péril. Lambert se trompait, la prière ne pouvait tout faire. Ernaut était un homme d’action, pas un contemplatif. La Rédemption, il ne la pensait pas possible autrement qu’en agissant, en s’avançant parfois au-devant des problèmes, pour les affronter avec vigueur, seule façon pertinente selon lui de les régler.

Pour l’heure, un peu dans l’expectative, il observait sans y réfléchir les grappes de personnes qui se détachaient de la colonne principale, cherchant sans trop y croire quelqu’un poursuivant un petit garçon apeuré. Il était concevable qu’ils soient déjà passés, ou que la foule les dissimule à la vue d’Ernaut. Sans compter qu’il n’était pas complètement certain de pouvoir reconnaître l’enfant. Finalement, il avança en direction de la muraille, ayant remarqué un ressaut et une pierre où il pourrait s’asseoir pour avoir à l’œil les voyageurs qui regagnaient la cité.

Le soleil était encore assez haut et il pensait attendre là que les fidèles se soient dispersés dans la ville. Il envisageait d’aller voir son frère par la suite, histoire de lui narrer la cérémonie comme habituellement. C’était compter sans son estomac, qui lui rappela assez vite qu’il n’avait rien avalé de solide depuis une journée et qu’il serait bien temps de songer aux nourritures terrestres.

Se décidant à satisfaire cette demande pressante, il se résolut donc à opérer un détour par l’échoppe de Margue l’Allemande, qu’il espérait ouverte. À défaut, il rencontrerait bien dans le quartier quelque vendeur de quiches ou de tourtes. L’idée de devoir retourner sa chambre pour y trouver un quignon qu’il aurait à disputer aux rats et à la vermine ne l’enchantait guère. Nettoyant de la main la poussière qu’il avait amassée sur sa cotte, il s’avança tranquillement vers la porte du Mont-Sion, admirant les murailles qui protégeaient la ville sainte et suivant distraitement les rondes des rares soldats de faction derrière les merlons.

La route qui jaillissait à l’abri de la tour méridionale rejoignait Hébron, au-delà de Bethléem, et serpentait vers les territoires les plus méridionaux du royaume, Montréal et les monts du Sinaï. Ernaut savait que certains pèlerins s’y rendaient, et le nom du lieu lui était plus ou moins familier, mais cela représentait pour lui surtout une zone mystérieuse, un désert de rocaille et de sable à partir duquel commençaient les terres de légende.

Il avait désormais appris que la Babylonie n’était pas par là, mais vers le Levant, bien que les pays loin vers le sud fussent aussi aux mains des Sarrasins. Laissant son esprit divaguer, il en vint à se demander s’il n’allait pas se faire marchand pour pouvoir visiter tous ces endroits magiques d’où provenaient les épices, l’ivoire et tant de produits merveilleux.

La cité était plus animée que le matin, mais paraissait abriter une vie ralentie, les passants avançant dans le recueillement, murmurant plus que parlant. Même les soldats qu’on croisait de temps à autre semblaient se tenir un peu mieux ; il n’en avait vu aucun en train de jouer aux dés pour tuer le temps ou de s’esclaffer joyeusement en roulant des yeux vers une jolie femme.

Aux abords d’une placette où l’on pouvait tirer de l’eau, un groupe en armes était occupé à discuter un peu plus bruyamment. Un soldat s’employait à faire boire un cheval de belle apparence, à la robe alezan mise en valeur par un harnois de cuir ouvragé et une selle peinte de couleurs vives. Face au vicomte, qu’Ernaut reconnut immédiatement, Eudes était en pleine conversation avec quelques autres, visiblement à propos d’un sujet plaisant.

Son physique atypique attira instantanément l’attention sur lui et Eudes le salua de la main, quittant ses compagnons en souriant. Arnulf suivit du regard son sergent et étudia quelques secondes le grand gaillard avant de se détourner. Eudes avisa le jeune homme des pieds à la tête, le visage amusé de le voir en si belle tenue.

« Dis-moi Ernaut, quelle prestance ! »

Le sergent était, lui habillé d’une cotte de toile grossière, légèrement rembourrée, et de vêtements fatigués, quoique de bonne qualité. Il arborait comme à chaque fois en service son épée au côté et son casque en chef.

« Je reviens de la cérémonie. Si on ne s’habille beau en pareilles occasions, on ne le fait ja ! »

Eudes acquiesça silencieusement, un large sourire marquant sa bonne humeur.

« On y a d’ailleurs peut-être entr’aperçu le garçonnet. Je ne suis pas complètement assuré, mais il est fort possible que le murdrier y était en chasse après lui. J’ai grande crainte pour ce petit. »

La bonne humeur d’Eudes disparut immédiatement et il fronça le nez, comme s’il sentait quelque effluve nauséabond l’environner.

« Pour l’heure, nous le savons au moins encore vif ! Par contre, toujours nul signe de l’époux. Il ne s’est pas fait reconnaître pour le meurtriement de son épousée. Il n’a peut-être guère idée du coupable.

— Ou il a peur aussi, s’il n’est pas le murdrier. On m’a dit que l’enfant était porchacié. »

Le sergent releva la tête, intrigué.

« Pourquoi le penses-tu coupable ?

— Je ne sais, ce n’est que simple idée. Oudinnet semblait fuir aucune chose alors qu’il était parmi pérégrins pour assister à l’office. Il saurait donc qui les assaille. Et pourquoi pas Nirart ? Ce me semble raison possible. »

Les yeux dans le vague, réfléchissant aux arguments d’Ernaut, Eudes renifla plusieurs fois, une moue déformant ses traits. Finalement, son regard accrocha de nouveau celui du jeune homme, qui reprit.

« Malgré tout, nul n’a vu Nirart parmi ceux qui le connaissent. Donc je bats peut-être mauvais sentier. Je n’ose en parler, de peur de causer grande tristesse parmi ses compaings. Ou de m’en faire des ennemis.

— Tu as bien fait de m’en parler. On peut se retrouver demain au matin à l’hôpital pour peser nos idées. Je passerai après m’être reposé un peu, je suis de veille toute la nuitée. Mais ça devrait être calme. Il est rare qu’on ait des soucis durant les fêtes de Pâques. C’est souventes fois après que les gens se laissent un peu aller.

— Pour ma part, je me tiendrai toute la nuit à Gethsémani. Une veillée de prières est prévue là-bas, avec les habituels camarades de Nirart. Je tâcherai de garder les yeux aux aguets. »

Le sergent se rapprocha, levant l’index comme s’il allait interdire quelque chose à un enfant. Il se l’appliqua sur les lèvres, cherchant un instant ses mots.

« Prends garde à toi, Ernaut, le murdrier a déjà fait nombreuses victimes céans, et ce ne sont peut-être pas ses premières.

— Pas de souci, je sais me défendre. Et j’ai ça qui ne me quitte pas. »

Le jeune homme sortit son solide couteau de sa besace, que le sergent apprécia d’un coup d’œil.

« D’accord, mais ce n’est pas l’arme qui donne la victoire. Ni les muscles. Assaillir embusqué donne grande force. Alors, fais attention à toi ! Je n’aimerais guère découvrir ta dépouille en sombre ruelle du prétoire.

— Ne t’inquiète pas, j’ai déjà eu affaire à un sournois murdrier. Je ne crois guère qu’il puisse s’en vanter. »

Eudes ouvrait la bouche pour demander des précisions par rapport à cette dernière affirmation péremptoire, mais Ernaut fut le plus rapide, et indiquait du doigt derrière le sergent.

« On t’appelle au service. Je te laisse, on se verra demain. »

Le soldat se retourna et vit le vicomte Arnulf qui lui faisait signe d’approcher. Il salua brièvement Ernaut et vint s’enquérir de ce qu’on espérait de lui. La patrouille s’était égaillée un peu partout aux alentours, ce n’était donc pas du départ que le chevalier voulait lui parler. Ajustant son casque sur sa tête, il s’avança respectueusement, attendant qu’on lui adresse la parole.

« Dis-moi, Eudes, tu sembles bien connaître ce damoiseau, ou je me trompe ?

— De vrai, depuis peu. Il m’a fourni bonne aide pour calmer petite rébellion. Il prend grande ardeur à chercher l’enfançon perdu, comme on le lui a mandé. »

Le vicomte demeura silencieux, indiquant d’un regard qu’il attendait d’en entendre plus.

« Il semble qu’ils aient amis communs, avec les pérégrines meurtries. Outre, il m’a conté qu’il avait déjà entravé desseins d’un murdrier. »

Le vicomte arborait un air mystérieux et personne n’aurait pu dire s’il était courroucé, intrigué ou simplement à demi endormi.

« Selon ton estime, ce ne serait pas notre coupable, tout bonnement ?

— Cela m’étonnerait fort, messire. Je le pense homme de droiture, et peu enclin à dissimuler. D’un seul bloc, à l’image de son physique, autant que je peux en juger. »

Arnulf toussa discrètement dans sa main gantée, les yeux fixés sur Ernaut, qui venait de reprendre son chemin. Il inclina doucement la tête de droite et de gauche, comme s’il soupesait une décision, puis révéla ce qu’il avait à l’esprit.

« Alors, vois s’il ne serait pas intéressé à rejoindre la sergenterie. Nous manquons de bras, tout le monde en convient. N’en souffle nul mot à personne, il est possible que nous ayons de nouvel quelques soldes à donner. Pareil gabarit en la patrouille épouvanterait les malandrins, à n’en pas douter. Sais-tu s’il a quelque science dans l’art d’escrimer ?

— Je ne saurais vous dire. Il serait bien étonnant qu’il ne soit pas au moins bon lutteur. À partir de là, on peut lui enseigner les rudiments nécessaires. »

Arnulf demeurait silencieux, réfléchissant en clignant nerveusement des paupières.

« De toute façon, je ne vois pas quel fol serait prêt à affronter un tel ours, a fortiori armé de pied en cap, l’épée à la main. Même s’il ne sait employer sa lame que pour fendre la viande tel un boucher à l’abattoir. »

L’image fit sourire Eudes malgré le visage fermé et sérieux de son supérieur. Le vicomte avait une forte propension à conserver une humeur égale, maussade et taciturne, quel que soit le ton des phrases qu’il lançait, même lorsque tout indiquait que c’était une plaisanterie. Mais il ne s’offusquait guère de l’amusement qu’il provoquait de temps à autre, certainement à dessein de toute façon. Arnulf continua donc de sa voix monocorde :

« Très bien. Je compte sur toi pour me tenir informé de ce jeune Ernaut. Nous verrons s’il peut se mettre à notre service d’ici l’été. »

Puis, bombant le torse et élevant la voix, il donna l’ordre du départ. Un sergent lui mena son cheval par la bride et, une fois en selle, il se mit en chemin avec la patrouille en direction du site du Temple.

### Après-midi du jeudi 28 mars 1157

Ernaut se rendit immédiatement à l’hôpital. Il y retrouva Lambert qui sommeillait à demi, impatient néanmoins d’entendre la description de la cérémonie. Il souriait à l’évocation des différentes processions et bénédictions, regrettant de n’avoir pu y assister lui-même. Rasséréné par le récit plus que précis d’Ernaut, qui y ajouta quelques éléments imaginés dont il estimait qu’ils bien auraient trouvé leur place, il s’assit sur son lit, le visage réjoui, regardant dans le vague. Au bout de quelques instants, il réalisa que son jeune frère n’était pas aussi enthousiaste que lui et s’en enquit, le sourcil froncé.

« Je te vois bien fâché, frère, en ce jour béni, je sais que nous entamons bien douloureuses célébrations, mais est-ce raison pour arborer si triste mine ?

— Oh, je ne me chagrine pas pour cela, je sais que Christ ressuscitera, comme chaque année. C’est que j’ai découvert triste nouvelle… »

Comprenant qu’Ernaut était chagriné par ses recherches, même s’il les désapprouvait, Lambert posa une main amicale sur son bras.

« Y aurait-il encore quelque pauvresse meurtrie ?

— Nenni, fort heureusement, mais il m’apparait que l’époux de l’une l’est certainement, s’il n’est pas le murdrier. L’enfançon a été vu seulet, pourchacié par quelqu’un de sa connoissance. »

Ennuyé, Lambert fit une moue compréhensive. Il réfléchit, le regard vagabondant dans la salle jusqu’à l’arrêter vers l’entrée, sur une table.

« Vois donc déjà s’il est passé outre. Tu devrais t’enquérir des morts récents. Ce sont les frères de Saint-Jean qui prennent soin des miséreux trouvés en les rues. Peut-être l’a-t-on découvert et mis en terre chrétienne. Ou peut-être qu’aucun corps ne sera tel que cet homme. As-tu idée du moment où on l’aurait occis ?

— Certes oui, il serait passé voilà une semaine, ou depuis.

— En ce cas, va demander au frère hospitalier, à l’entrée de la grand’salle. Il pourra te prêter assistance, j’en suis acertainé. »

Ernaut acquiesça en silence, lentement, un sourire forcé lui barrant le visage. Il tapota doucement le bras de son frère, appréciant que ce dernier se montre si compréhensif et, même, lui apporte son soutien. Il s’avança donc d’un pas lourd vers l’entrée. Là, un des hospitaliers était occupé à répartir un groupe de mendiants en piteux état qui venaient certainement d’être ramassés dans les rues par les valets.

De taille moyenne, il avait le dos voûté et les doigts pleins d’encre des hommes qui vivaient la plume à la main. Sur le plateau disposé devant lui se trouvait un large amoncellement de tables de cire, qu’il annotait et consultait fréquemment. Un visage lisse et jovial orné d’un nez un peu couperosé donnait une impression bonhomme que renforçait sa voix douce et chantante.

Pourtant il était le chef d’orchestre du lieu, dirigeant avec talent les arrivées et les départs de plusieurs dizaines de malades chaque jour, s’assurant des capacités d’accueil et de la transmission des instructions. Une fois les patients emportés vers des lits qui avaient été préparés à leur intention, il leva les yeux vers Ernaut qui s’avançait, tout en inscrivant quelques lignes de son écriture fine dans la cire à l’aide de son stylet de bronze.

« Je peux vous aider, mon frère ? Je vous ai déjà vu, non ?
, n
Le visage affable adopta un air contrit de circonstance.

« Et il y a un souci ?

— Ah non, aucun ! Nous n’avons qu’à nous louer de la façon dont vous prenez soin de lui. En fait, je voudrais assavoir si vous aviez connaissance d’un homme meurtri voilà quelques jours. »

Le moine fronça les sourcils.

« En quoi cela vous concerne-t-il ? L’un de votre parentèle a disparu ?

— Absolument pas. J’essaie de me rendre utile auprès des hommes de la Cour des Bourgeois, et l’on m’a demandé d’ester pour les pérégrins. »

Le moine hocha la tête, peu enthousiaste.

« Et en quoi cela vous serait d’usage ?

— Afin de voir s’il n’y aurait pas là quelque indice qui trahirait son murdrier.

— Je comprends. Auriez-vous des éléments sur le pauvre homme, que je demande autour de moi si cela appelle d’aucun souvenir ? »

Ernaut lui décrivit en quelques phrases Nirart, en se basant sur ce que lui avaient dit Sanson et les autres. Bien qu’il n’ait jamais rencontré l’homme, il parvint visiblement à en faire une présentation suffisamment claire pour le moine.

« Je vais me renseigner, attendez donc aux abords. Les valets qui prennent soin des corps auront peut-être mémoire d’un tel homme. »

Après un bref sourire qui se voulait amical, le moine abandonna la table et se dirigea vers une porte, abordant quelque serviteur qui passait par là pour s’entretenir avec lui à voix basse. Ernaut se tourna, embrassant la grande salle d’un regard. Il décida d’aller patienter à l’entrée du bâtiment. Afin de tuer le temps, il finit par discuter avec le domestique chargé de surveiller les allées et venues, un fils de colons originaires de Champagne.

Quand l’hospitalier revint enfin, l’adolescent était devenu incollable sur l’art, tout relatif, d’identifier la provenance d’un pèlerin à sa simple tenue. L’homme y mettait une conviction et une passion que s’expliquait mal Ernaut, surtout lorsqu’il n’était question que de différences minimes. Il accueillit donc avec soulagement la venue du clerc. Ce dernier le mena à un valet qui sortait d’une annexe et lui ordonna d’emmener Ernaut à la chapelle ardente où on déposait les corps après les avoir nettoyés.

Emprutant différents corridors et traversant des zones où s’entassaient coffres de rangement, meubles cassés, draps et fournitures diverses en quantités incroyables, il fut guidé jusqu’à une salle où plusieurs cadavres étaient allongés sous la protection d’un imposant crucifix de bois peint. Chaque dépouille était protégée d’une couverture rouge à croix blanche, une bougie à ses côtés. Deux domestiques étaient justement en train de déposer un malheureux, certainement en provenance de la pièce où on les préparait, qu’Ernaut avait visitée plus tôt dans la semaine. Voyant leur tâche achevée, il s’approcha d’eux, d’un air martial.

« Je peux vous distraire de votre labeur quelques instants ? »

Le plus près de lui, affublé d’une profonde cicatrice de brûlure sur le visage, avait ramené de longues mèches sur sa joue gauche pour tenter de dissimuler son affliction. Malgré cela, il sembla tout à fait avenant à Ernaut et lui sourit, exhibant des dents plutôt rares. Il ne devait pourtant être guère plus âgé que le jeune pèlerin.

« Que puis-je pour vous, maître ?

— Auriez-vous souvenir d’un corps qui aurait été meurtri voilà plusieurs jours, vendredi. »

Le domestique regarda de droite et de gauche, comme si le corps allait apparaître sur la table à côté de lui. Il déambula alors dans les rangs et tandis qu’il énumérait silencieusement l’aspect des défunts autour de lui, il semblait gagner en nervosité.

« À quoi ressemblait-il ? »

Ernaut recommença sa description de Nirart, vérifiant chaque fois mentalement de qui il tenait l’information, de façon à être certain de ce qu’il avançait. Il conclut en précisant que l’homme avait peut-être été frappé de plusieurs coups de couteau. Le serviteur arbora alors une mine déconfite et s’essuya nerveusement les mains à un chiffon qu’il avait passé dans sa ceinture. Il lança une œillade à son compère, resté un peu en retrait, comme s’il cherchait un soutien. Son collègue ne paraissait guère plus rassuré que lui.

« Que se passe-t-il ? » Demanda doucement Ernaut.

Voyant que le géant semblait déterminé, le jeune homme finit par déclarer d’une voix hésitante.

« C’est que, vous voyez, nous sommes tenus de signaler lorsque les pauvres hères que nous découvrons ont connu malemort… »

Ernaut ne voyait pas où il voulait en venir et s’avança, adoptant un air menaçant sans même s’en rendre compte.

« Et ?

— Si nous avions fait découverte d’un tel corps, nous aurions dû le rapporter au frère hospitalier…

— Il n’a pas semblé avoir souvenance de pareille chose », déclara Ernaut, soudain dépité.

Le valet continuait à s’essuyer les mains, sans y penser, la mine fort désolée, hochant la tête en silence. Il semblait à Ernaut qu’il voulait parler plus avant, attendant qu’on l’y pousse. Le jeune pèlerin n’hésita pas longtemps.

« Y a-t-il autre chose ? Vous semblez celer quelque secret. Confiez-le moi, je le tiendrai en bonne garde, sans le dévoiler… »

L’autre domestique lança un regard de travers, contrarié, mais n’empêcha pas son collègue de répondre.

« De fait, nous avons eu un corps ainsi que vous le narrez. Il avait été occis par lame.

— Et vous ne l’avez pas signalé, c’est cela ?

— Il y avait tant de travail, nous avons oublié sur le moment. Et lorsque pauvresses ont été découvertes, nous avons eu peur qu’on nous en fasse reproche, alors nous avons préféré demeurer cois. »

Ernaut soupira, ennuyé. Il comprenait fort bien la réaction des servants qui risquaient leur place.

« Et que lui avez-vous fait ?

— Rien de plus qu’aux autres, il a été lavé, apprêté puis mené à Chaudemar. »

Le visage d’Ernaut se ferma instantanément. Le valet resta silencieux un long moment puis reprit, tentant de se montrer conciliant.

« Il vous serait peut-être d’usage de voir Joris.

— Et qui est-il ?

— Celui qui escure les corps. Il est rentré chez lui maintenant. Il faudrait voir avec le frère hospitalier, qu’il le fasse mander. »

Comprenant qu’ils ne tireraient rien de plus du servant, Ernaut hocha la tête et sortit. Le malheureux valet mit néanmoins un point d’honneur à l’accompagner jusque dans la salle, auprès du frère qui l’avait envoyé. Sans doute dans l’idée de se justifier s’il venait à être critiqué. Sans entrer dans les détails, Ernaut expliqua qu’il lui serait utile de rencontrer Joris, qu’il avait vraisemblablement aperçu le corps de l’homme recherché. L’hospitalier fit mander le domestique sans perdre de temps, retournant aussitôt à ses tâches. Ernaut se résolut une fois de plus à attendre. Néanmoins cette fois-ci, on lui apporta un tabouret et un verre de vin lui fut servi.

Tandis qu’il dégustait en silence l’excellent breuvage, il vit arriver le vieux Sanson, emmitouflé dans une couverture et un bonnet de feutre enfoncé jusqu’aux oreilles, qu’il laissait néanmoins bâiller largement face à la route. Le vieil homme expliqua qu’il avait préféré revenir se reposer, les festivités du matin l’ayant durement éprouvé.

Ernaut en profita pour lui confier à voix basse qu’il était possible que Nirart ait été occis avant même les deux femmes. Il n’eut pas le temps de développer lorsqu’on leur amena le jeune valet qui avait fait la toilette mortuaire. Certainement syrien, il s’exprimait avec un fort accent, mais trouvait ses mots avec aisance. Son visage fatigué et ses yeux cernés indiquaient qu’il avait été sorti de son lit, bien qu’il s’efforçât de faire bonne figure. Ernaut lui résuma la situation rapidement, laissant entendre qu’il ne lui tenait pas grief de n’avoir rien signalé et qu’il n’en parlerait à personne. Finalement, il lui demanda s’il avait remarqué quelque chose de spécial.

« Rien de surprenant, non. Il était pas en très bonne santé, fort maigre.

— Il avait connu geôles païennes, c’est pour ça.

— Ah, d’accord. À part ça, il n’avait rien de notable. Je l’ai préparé pour le couvrir du linceul et puis voilà. Je lui ai juste laissé sa croix autour du col. Je me suis dit qu’il apprécierait d’être porté en terre avec. »

Sanson renifla bruyamment, attirant l’attention sur lui.

« Quelle croix ? Il n’en a jamais eu.

— Une petite croix, un pendentif d’argent, de deux pouces environ, vous n’y avez peut-être jamais prêté attention.

— C’est surtout que je sais qu’il a tout perdu en captivité. Et il n’avait certes pas les moyens de se l’offrir depuis sa libération.

— J’en suis tout à fait sûr. Elle a chu lorsque je l’ai dévêtu. Il l’avait sur lui. »

Ernaut se figea un instant.

« Vous voulez dire qu’il ne la portait pas au cou ?

— Non, elle avait dû se détacher, peut-être… juste avant qu’il ne trépasse. »

À son regard, Ernaut comprit qu’il ne voulait pas parler des coups de couteau, mais évoquait peut-être l’agression. Ernaut se pencha un peu vers Sanson, prenant conscience que le vieil homme faisait le même cheminement dans sa tête. Il murmura entre ses dents.

« Ou il l’aura arrachée à son murdrier en tombant ! »

### Soirée du jeudi 28 mars 1157

Tirant la porte, le garçonnet peinait à soulever en même temps le seau qu’il était chargé de vider à l’extérieur, dans le caniveau. Il s’arrêta un instant, estimant l’effort qu’il lui faudrait faire pour arriver à destination avec son lourd et nauséabond fardeau.

Il prit son souffle et se lança pour un dernier coup de collier, sortant la langue pour s’assurer un maximum de concentration. Il allait renverser le tout lorsqu’il remarqua une personne allongée sur le sol, un peu en contrebas, et qui risquait donc de recevoir sur elle le contenu fétide de son récipient. Il tenta d’interpeler l’inconnu sans conviction, ni résultat, puis opta pour rebrousser chemin et signaler le fait à quelqu’un de plus autoritaire que lui.

Il revint en trottinant derrière son père, un homme d’une cinquantaine d’années, la barbe fleurie bien entretenue surplombée par un nez imposant, le regard un peu éteint des esprits jamais en repos. Soulevant les pans de son long *durrâ’a*[^durraa], il avançait en faisant bien attention où il mettait les pieds dans la ruelle. Il tenta d’interpeler l’ivrogne allongé là, sans résultat. Il se força donc à s’approcher plus encore, intimant l’ordre à son fils de rester en arrière. Il poussa du pied légèrement l’épaule de l’homme en lui demandant de se lever. L’autre ne réagit toujours pas.

Tandis qu’il réfléchissait à la meilleure façon de se débarrasser du poivrot, il s’aperçut qu’un liquide s’écoulait en contrebas dans la rigole depuis le corps. La faible luminosité du passage l’empêchait de bien voir, mais il réalisa soudain ce qu’il avait sous les yeux et fut horrifié de sa vision. Il se pencha doucement, inquiet de vérifier si un souffle était perceptible. Il approcha son oreille du visage, tourné sur le côté. Il entendit une timide respiration, sifflante et gargouillante.

« Ne bougez pas l’ami, nous allons prendre soin de vous, ne bougez pas… »

L’homme déglutit et glissa péniblement :

« … L’enfant, j’ai sauvé l’enfant… »

Le père se releva, recula doucement, portant la main à sa bouche. Une fois à bonne distance, il intima l’ordre à son petit garçon de rentrer, de façon plutôt sèche, puis appela son aîné par la porte restée ouverte. Il l’envoya chercher la patrouille du guet : une nouvelle victime répandait son sang dans la cité de Jérusalem pendant les célébrations de Pâques.

À peine arrivés les soldats constatèrent le décès du pauvre homme et posèrent sur son visage une vieille couverture poussiéreuse. Ils n’avaient aucune idée de ce qu’ils devaient faire et préféraient attendre que des responsables les rejoignent. Ils faisaient partie de ces sergents à qui on pouvait confier la garde d’une porte, mais qui n’auraient jamais eu l’à-propos de la fermer à la nuit si on ne leur en avait pas donné l’ordre. Ce fut donc avec un certain soulagement qu’ils virent arriver peu de temps après la principale patrouille, menée par le vicomte.

S’avançant le premier, Eudes souleva puis reposa la couverture sur la tête de la victime, le visage tendu comme s’il s’estimait responsable de ce qui s’était passé. Il se releva et fit face à Arnulf, qui attendait, toujours en selle.

« Il a été transpercé par lame, messire. Et de face, à moult reprises. »

Arnulf fit claquer sa mâchoire. Ses lèvres avaient totalement disparu et de sa bouche ne subsistait qu’une fente noire au milieu de son visage. Son regard, perdu sous ses épais sourcils, n’était perceptible que par le reflet des lanternes dans ses yeux sombres.

« C’est alors peut-être simple échauffourée. Une dispute entre poivrots qui a mal fini. Cela semble fort différent des agressions sur les femmes.

— Si fait. Pas de trace d’autre violence. L’agresseur est venu de face, ce qui ne ressemble guère à la façon de faire du fol murdrier. Il a plutôt usage de se comporter vilement. Là cet homme a pu se défendre, du moins tenter. Il n’était pas fort costaud, mais toujours plus que les deux pérégrines.

— Est-ce que cela pourrait être époux de la seconde ? »

Eudes souleva les épaules, indécis. L’homme qui avait découvert la victime s’avança alors, la tête inclinée en signe de respect.

« Il a parlé d’un enfant, messire vicomte, avant de passer. Il disait l’avoir sauvé. »

À cette mention, Eudes sentit son cœur s’emballer. Ernaut avait donc raison, Oudinnet était traqué, et il ne devait apparemment son salut qu’à l’intervention d’un sauveur providentiel, éventuellement Nirart. Le malheureux avait payé sa bravoure de sa vie. Il se tourna vers le vicomte, certain que celui-ci était déjà en train de réfléchir aux implications de cette déclaration. Arnulf cogita quelques instants, plissant les yeux comme s’il avait en face de lui tous les éléments qui lui étaient nécessaires pour prendre la bonne décision.

« Il faudrait faire annonce demain matin à la première heure, en le quartier de l’hôpital et auprès du Saint-Sépulcre, c’est là que ce serait plus efficace. Néanmoins je répugne à pareilles déclamations, surtout en pareil moment… Votre ami, le géant, il ne pourrait pas le reconnaître ?

— Fort possible. Il connait camarades qui le sauraient à coup sûr. Ses compaings de voyage à ce que j’ai compris.

— Il faut que je sache pour demain avant le mitan du jour. Vous pourrez faire cela pour moi ? »

Comprenant par cette dernière phrase l’importance de la mission qui lui était confiée, Eudes prit une mine embarrassée.

« Il est toute la nuit en oraisons au clôt de Gethsémani, messire. Il me faudrait sortir hors la cité. Les portes sont closes jusqu’à l’aube.

— Détail ! Il nous suffit de remonter la rue de Josaphat, cela sera rapide. Je donnerai ordre pour qu’ils vous laissent rentrer une fois votre tâche faite. Vous irez avec un autre homme. »

Il reprit ses rênes correctement, révélant par là son intention de se mettre en route immédiatement. Eudes eut juste le temps de faire signe à un compagnon de le suivre et d’indiquer aux autres d’attendre le retour du vicomte.

Lorsqu’il sortit de la ruelle, le cheval trottait à bonne cadence, le bruit de ses sabots résonnant dans le silence de la nuit qui s’installait. Ils surprirent un chien galeux qui s’écarta en grognant de quelque trophée qu’il s’était accaparé à grand-peine. De temps à autre, un visage se montrait à une fenêtre, intrigué voire inquiété par le passage d’une monture pressée à un tel moment.

Quand ils parvinrent à la porte de Josaphat, les deux sergents respiraient péniblement et ce fut en soufflant qu’Eudes alla frapper à l’huis de l’homme qui devait être de faction. Au-dessus d’eux, un arbalétrier, tranquillement accoudé, surveillait la scène. Il ne fallut que quelques instants pour que le garde reçoive ses instructions et déverrouille un petit vantail dans la grande ouverture. Dehors, le maigre croissant de lune éclairait d’une lumière hésitante la vallée qui descendait vers le sud, conférant un aspect lugubre aux anciens cimetières qui se trouvaient là.

Le vicomte ne prit pas le temps d’attendre qu’ils aient franchi le passage et repartit si tôt ses ordres donnés, au galop cette fois-ci. Nullement impressionné par le lieu qu’il connaissait bien, pour y être souvent venu en famille lorsque les conditions climatiques le permettaient, Eudes commença à suivre le chemin tranquillement, fredonnant à voix basse une comptine bien triviale. Il pensait monter entre le tombeau de la Vierge Marie et l’église de l’Agonie-du-Christ, aux abords de laquelle se trouvait certainement Ernaut, les lumières de quelques feux indiquant que des pèlerins s’y étaient installés.

Le vent rabattait leurs voix au loin, mais par moment on entendait leurs chants s’élever depuis les oliviers, mêlé au bruissement des branches et des feuilles. L’apparition de deux hommes en armes inquiéta tout d’abord les fidèles qui se recueillaient autour des foyers, mais leurs craintes furent vite dissipées, Eudes expliquant qu’il avait besoin de retrouver quelqu’un. Il fournissait une description sommaire d’Ernaut, la taille imposante du personnage suffisant généralement à le reconnaître.

Il arriva donc rapidement auprès du groupe dans lequel on avait vu l’adolescent pour la dernière fois. Ce fut le père Ligier qui vint s’enquérir de ce qui se passait, perturbé de l’irruption de soldats alors même qu’ils discutaient entre eux de l’arrestation du Christ dans ce lieu quelques centaines d’années plus tôt, jour pour jour. Il ne sembla pas surpris d’apprendre qu’ils en avaient après Ernaut.

« Il ne fait pas vraiment partie de mes ouailles, entendez. Il a souhaité se joindre à nous, je n’avais pas cœur à le lui refuser. Mais… »

Eudes comprit que le gros prêtre craignait de se voir attirer des ennuis.

« Rassurez-vous, mon père. Nous n’en avons pas après lui. Il œuvre avec nous sur une affaire importante et nous avons besoin de son aide. C’est la raison pour laquelle messire Arnulf, vicomte de Jérusalem, m’a fait déclore les portes pour venir ici en pleine nuit. »

Le clerc écarquilla de grands yeux, ainsi que les pèlerins les plus proches, étonnés d’avoir parmi eux un homme si important, bien plus qu’ils ne l’auraient cru. Ligier tourna la tête, cherchant où il avait aperçu le colosse pour la dernière fois. Il était surpris de ne pas voir son visage dépasser au milieu du groupe. Puis il comprit pourquoi : Ernaut faisait partie d’un petit comité qui s’était retiré contre un muret, dans la pénombre aux abords du cercle lancé par les flammes, pour se reposer. Il le désigna à Eudes, souriant de façon gauche.

« Voilà le jeune homme que vous cherchez, sergent. »

Eudes le remercia, s’excusa pour le dérangement et contourna le groupe pour s’approcher du dormeur dont la respiration paisible indiquait qu’il avait sombré depuis un bon moment. Au moment où il se penchait pour réveiller Ernaut, il aperçut une jeune fille, pas très éloignée, qui regardait dans leur direction par-dessus son épaule. Il fit mine de ne rien remarquer et secoua énergiquement l’adolescent. Ce dernier ouvrit les paupières comme si on venait de lui jeter un seau d’eau froide au visage et redressa le buste, affolé, les poings faits, prêt à en découdre. Il manqua d’assommer le sergent d’un coup de tête dans sa précipitation.

Un rire féminin étouffé parvint aux oreilles d’Eudes depuis les rangs des pèlerins. Il attendit quelques instants que le jeune homme se remette : les yeux grand ouverts et guère plus mobiles que ceux d’une chouette, il dévisageait les deux soldats sans croire à leur présence. Eudes lui sourit amicalement.

« Ernaut, j’ai besoin de toi. C’est le vicomte qui m’envoie. Nous avons souci et tu pourrais peut-être nous prêter assistance.

— Euh, oui. Désolé, j’étais en plein rêve.

— Il n’est pourtant pas si tard, aurais-tu difficultés à te remettre de la nuit passée ? »

N’attendant pas de réponse à sa question, le sergent s’accroupit et expliqua en quelques phrases à voix basse la découverte du corps et les inquiétudes du vicomte. Ernaut écoutait silencieusement et on n’aurait pu dire s’il était encore un peu ensommeillé ou simplement attentif. Lorsqu’Eudes lui demanda s’il était capable d’identifier le cadavre, il secoua la tête négativement.

« Nul besoin. Je sais où se trouve le corps de Nirart. Je ne peux t’expliquer en détail pour l’instant, mais il est acertainé qu’il s’agit là d’une autre victime. Souviens-toi, l’enfançon a été vu ce matin à Sainte-Marie de Sion, empli de peur. Ce ne peut être Nirart qui aurait voulu le protéger et sera tombé en le défendant… Pas plus qu’il n’a pu frapper. »

Eudes patienta quelques instants, réfléchissant à la conduite à tenir. Il ne pouvait demeurer dans le doute alors même qu’il avait en charge une importante mission confiée par le vicomte en personne.

« Où se trouve la dépouille de Nirart ? Il me faut le voir, me garantir certitude…

— En Chaudemar ! »

Eudes écarquilla de grands yeux, effrayé. Puis inspira lentement.

« Voilà évidence que nous aurions dû deviner. Le chemin est donc clos de ce côté.

— Pas nécessairement. Il porte sur lui bel indice qui nous mènera peut-être au murdrier. »

Ernaut lui expliqua alors qu’il était possible que le pauvre homme ait arraché une croix à son assassin, et qu’on l’avait enterré avec par mégarde. Un tel objet ne devait pas demeurer inaperçu, et saurait sans nul doute aider à confondre le meurtrier. Eudes soufflait comme un cheval rétif, effrayé de comprendre là où Ernaut voulait en venir. Il finit par se rendre à l’évidence.

« Mettons-nous tous deux en route sans tarder, nous devons demander quelques matériels à l’ami Abdul Yasu… »

### Nuit du jeudi 28 mars 1157

Mile somnolait à peine, ayant perdu le sommeil lorsqu’il avait obtenu son travail. Il gardait les morts, surveillait que le charnier de Chaudemar demeurât tranquille. Pourtant les défunts n’en étaient guère reconnaissants et ne le laissaient pas en paix. Toutes les nuits, ils venaient peupler ses cauchemars, remontant par légion des fosses où on les précipitait. On disait que le lieu était hanté par toutes sortes de démons et seuls les plus aventureux osaient rester dans cette zone de la vallée de l’Hinnom.

Ce n’étaient désormais plus que des jardins, mais les cultivateurs fuyaient l’endroit dès que les ombres étaient un peu trop allongées, abandonnant les morts entre eux, avec Mile pour toute compagnie, Mile comme unique victime à torturer de leurs cris geignards.

Plusieurs coups frappés à sa porte firent sursauter le gardien. Le cœur battant à tout rompre, il releva la tête puis replongea sous ses couvertures, priant pour que ce ne soit qu’un mauvais rêve, un de plus. Une voix, insistante, s’éleva tandis qu’on tambourinait de plus belle.

« Ouvrez, par tous les saints, nous voulons juste vous parler ! »

Mais Mile préférait rester dissimulé dans son lit, priant à voix basse pour que le jour revienne au plus vite. Peu après une autre voix se joignit à la première.

« Il est peut-être parti faire une ronde ?

— Nous verrions sa lanterne en ce cas, Ernaut. Il doit juste ronfler tout son soûl. »

Étonné que des esprits s’interpellent ainsi comme des compagnons, le nain sortit un œil de son abri de tissu et lança d’une voix mal assurée :

« Qui êtes-vous ? Que voulez-vous ? Laissez-moi tranquille !

— Nous voulons juste deviser. Ne craignez nul mal de nous. Ouvrez, s’il vous plaît. »

À demi convaincu, Mile sortit de sa couche, frissonnant autant de peur que de froid et se rapprocha de la porte, hésitant toujours à l’ouvrir. Il demeura en retrait, tendant le cou avec réticence comme s’il craignait que des diables l’attrapent à travers l’épaisse boiserie.

« Qu’est-ce qui me prouve que vous n’êtes pas des démons ? »

Levant les yeux au ciel, Eudes sortit de sa bourse un denier d’argent et le glissa d’une chiquenaude sous la porte. Ils n’attendirent guère que le gardien entr’ouvre l’huis, le visage à demi caché par le panneau de bois.

« Que venez-vous faire ici au mitan de la nuit ?

— Notre demande va vous paraître étrange, nous avons besoin de voir la dépouille d’un homme mené ici voilà quelques jours. »

Mile réalisa alors que les deux compères, dont l’un lui semblait aussi large et puissant qu’un ogre, vu de sa faible hauteur, devaient sans doute être des fous, perdus là dans la nuit.

« Pardon ? Vous ne pouvez, voyons ! On les met en la fosse dès qu’ils arrivent. On ne peut y accéder.

— Je sais fort bien tout cela, je connais le lieu. Nous sommes venus équipés. »

Eudes montra du doigt un âne, attaché à un buisson derrière eux, fort occupé à dévorer les feuilles tendres autour de lui. Sur son dos on voyait un long rouleau de corde.

« Nous avons justement désir de descendre. »

Convaincu désormais que toute raison avait quitté ces esprits, Mile écarquilla les yeux, aussi ébahi qu’effrayé.

« Descendre ? Au parmi de ça ? Vous avez perdu le sens commun ! Seuls les morts le font. Pour n’en plus remonter. Jamais !

— Voilà raison pour laquelle il nous faut nous y rendre. Ce que nous y cherchons ne pourra pas venir jusqu’à nous autrement. »

Se passant la main dans les cheveux nerveusement, le nain comprit alors où les deux compères voulaient en venir. Ce n’étaient que des pilleurs de tombes, des détrousseurs de cadavres. Il fit une moue de dégoût.

« Il n’y a rien à ramasser là. Les corps sont simplement couverts d’un linceul, aussi nus que le jour de leur venue au monde. Vous ne récolterez rien d’intéressant. »

Lâchant un soupir exaspéré,  Eudes s’efforça de garder son calme et répliqua d’une voix plus cassante qu’il ne l’aurait souhaité :

« Nous ne sommes pas là pour rober les morts. Nous cherchons un objet mis par erreur à un homme ayant passé voilà peu. Une croix, qui n’a de valeur qu’à nos yeux. Il est de grande importance pour nous et le vicomte que nous mettions la main dessus. Vous pourrez vérifier que nous n’emportons rien d’autre. Et nous sommes prêts à vous verser compensation pour vous avoir éveillé en pleine nuit. J’ai là onze deniers qui ne demandent qu’à retrouver leur petit frère. »

Le nain réfléchit un instant. Plusieurs jours de gages, pour ne rien faire de fatigant. Et puis ces deux-là lui semblaient de bonne foi et avaient cité le seigneur vicomte, qu’il n’était jamais sage de contrarier, ni même d’invoquer en vain. Des détrousseurs se seraient introduits furtivement, au risque de lui causer des ennuis. Tandis que là, il ne s’agissait que de contenter un maître exigeant qui avait donné des instructions précises. Stupides, certes, mais définitives.

« Très bien, je veux bien détourner les yeux. Mais comptez pas sur moi pour surveiller. Les morts rôdent par ici. »

Ernaut, qui était resté silencieux jusqu’alors, s’intéressa d’un coup à la conversation.

« Comment ça, ils *rôdent* ? »

Le nain le toisa comme s’il contait une histoire à un petit enfant effrayé.

« Nous sommes sur les terres que Judas Iscariote a payées de ses trente deniers. C’est là que les juifs abreuvaient de sang leurs dieux sanguinaires, sacrifiant enfançons et tous ceux qui leur tombaient sous la main. C’est pour ça que la terre est écarlate, elle est gorgée de la vie des innocents immolés lors de noires cérémonies[^chaudemar]. Il ajouta, baissant la voix : certains de mes compaings m’ont également assuré qu’on trouve à l’entour une des entrées de l’Enfer. »

Un peu inquiet à l’idée de se jeter dans la gueule du démon, Ernaut commença à se demander s’il avait eu raison de convaincre Eudes de la nécessité de retrouver la croix et d’examiner le corps. Pourtant, ce dernier ne semblait pas troublé. Impassible, il comptait les pièces qu’il faisait tomber dans la petite paume du gardien. Pour sa part, le nain avait recouvré tout son sang-froid.

« Je vais vous cueillir feuilles de menthe, si vous voulez. J’en ai dans le jardin.

— Pour quoi faire ? Demanda Ernaut

— Tu n’as jamais vécu près de tanneurs ou d’équarrisseurs ? Répliqua le petit homme.

— Non.

— Tu en arracheras quelques feuilles pour t’étouper les naseaux. Ça t’évitera d’être importuné par la fétide puanteur. »

L’affaire entendue, Eudes et Ernaut obtinrent, en plus des feuilles proposées, la possibilité d’allumer une chandelle de suif pour enflammer la lampe à graisse de leur lanterne. Puis Mile referma sa porte avec soulagement, impatient de retrouver le sommeil, quoiqu’un peu inquiet que les morts se vengent de cette intrusion.

Les deux compagnons attachèrent solidement la corde à une souche d’arbustes. Puis ils s’approchèrent d’une des lourdes trappes de bois qui obturaient les ouvertures. La fraîcheur qui s’échappa quand ils poussèrent le panneau les surprit. Pour l’instant, l’odeur devait encore être faible et ils ne sentaient que la menthe qui comblait leurs narines. Le sergent alluma une lanterne et la fixa à l’extrémité du filin, qu’il fit glisser doucement. Aucun d’eux n’osa se pencher pour examiner ce qu’elle éclairait.

Puis les deux hommes s’interrogèrent du regard, hésitant à s’aventurer en premier. Fanfaron, Ernaut s’avança avec vigueur, le reflet pâle de la lune révélant son sourire et ses yeux rieurs. Une fois la corde saisie, il perdit bien vite son air bravache et se concentra sur l’effort. Descendant à la seule force des bras, il n’osait pas observer vers le bas.

Le bâtiment, d’une dizaine de mètres de profondeur, était en fait accolé à une falaise, dont il développait et amplifiait le volume des grottes. Au-dessus de lui, Ernaut voyait la faible lueur de la lune et quelques étoiles, entourées d’un abîme de noirceur, comme des mâchoires d’ombre qui engloutissaient le monde qu’il quittait. Tentant de ne pas céder à la panique, il souffla un instant et continua à descendre.

Au moment où il pensait approcher du sol, il ferma les yeux, comme si ce contact allait être douloureux et sentit une surface souple sous ses pieds. Conscient d’être arrivé sur un cadavre, il pencha la tête avec angoisse, tout en reculant avec précaution. La lumière tremblante de la lanterne éclairait crûment les linceuls des corps récemment jetés. On apercevait tout autour les squelettes de défunts plus anciens, dont les mâchoires souriantes semblaient prêtes à mordre. Quelques dépouilles éventrées, allongées dans des postures improbables avaient dû être visitées par des charognards et les chairs en putréfaction gisaient répandues, exhibant leurs entrailles.

La fraicheur et l’humidité du lieu le frappèrent, loin de ce que pouvait évoquer en lui l’Enfer. Pourtant ces tas d’ossements en désordre, ces tissus agglutinés en amas immondes, animés par la flamme dansante de sa lampe, lui paraissaient hostiles malgré la fragrance mentholée qu’il percevait. Il sentit la corde bouger dans sa main et s’arracha à la contemplation du décor morbide alentour, se concentrant sur la descente d’Eudes et s’arrangeant pour qu’il n’arrive pas sur un cadavre.

Il commençait à goûter l’odeur pestilentielle de la putréfaction, qui s’insinuait par sa bouche, ne pouvant se faufiler par le nez. Il frissonna à l’avance, à l’idée de devoir ouvrir les linceuls des corps qui venaient d’être jetés par les trappes du plafond. Eudes lui tapa amicalement sur l’épaule, le visage fermé, s’efforçant de donner à sa voix une tonalité enthousiaste.

« Je te rappelle que c’est ton idée ! Hâtons-nous d’accomplir notre tâche. Je n’ai nulle envie de m’éterniser ici. »

Ernaut acquiesça, sans voix, une imposante boule lui nouant la gorge. Il se prit à remercier Dieu que son frère Lambert n’ait pas eu à subir une inhumation dans cet endroit horrible. Une tombe, ce devait être une petite fosse creusée près de l’église paroissiale, ornée d’une croix, fleurie par la famille les jours de fête. Certes pas un lieu aussi sinistre où le moindre craquement résonnait de façon lugubre entre les hautes voûtes.

Eudes avait sorti un canif pliant et commençait à découper la toile au niveau du visage du premier corps. Ernaut s’agenouilla afin de l’aider. Son regard croisa celui du sergent. Peu expressif, ce dernier lui adressa un sourire forcé.

« Je n’arrive pas à croire que je suis céans, occupé à profaner l’ultime repos de… »

Il n’eut pas le courage de terminer sa phrase, et se força à rester focalisé sur les gestes mécaniques qu’il accomplissait : couper le tissu, en écarter les pans, de façon à identifier le corps, puis chercher un pendentif en forme de croix autour de son cou. Il lui semblait que chaque seconde devenait une éternité, la vision de chaque visage entraînant l’apparition d’une nouvelle sueur froide. Mais ce n’était rien comparé à l’effroi de se rendre d’un tas de cadavres à l’autre, sous les ouvertures. La crainte de briser un os, de se coincer le pied dans un abject amoncellement le disputait à l’angoisse de trébucher et de tomber dans les tibias, crânes et vertèbres amassés là depuis des dizaines d’années, au mieux.

Il semblait à Ernaut qu’au-delà de leur petit cercle de lumière une vie hostile se manifestait, dont les longs doigts ténébreux cherchaient à les retenir, bousculant au passage quelques squelettes, soulevant de la poussière qu’on n’apercevait que du coin de l’œil. Il s’efforçait de ne pas regarder ailleurs que là où c’était nécessaire, désireux d’oublier les linceuls éventrés, les entrailles grouillantes de vermine, les nuées de mouches perturbées par cette lueur inhabituelle.

Les visages qu’il découvrait étaient souvent horriblement marqués par les maladies, suffisamment pour provoquer chez les deux hommes des nausées qu’ils avaient de plus en plus de mal à contenir. Ce n’est qu’après un intervalle de temps qui leur parut être une éternité et un trop grand nombre de dépouilles manipulées à leur goût qu’ils mirent enfin la main sur ce pour quoi ils étaient venus fouiller dans le ventre du démon.

Trop heureux de s’emparer du bijou, ils abandonnèrent finalement l’idée d’examiner le corps, leur courage étant à bout. Sautillant et enjambant, sans plus trop d’égard pour les défunts qu’ils piétinaient ni échanger un mot, ils retrouvèrent avec empressement la corde, le symbole de vie qui les reliait au monde des hommes, leur fil d’Ariane pour sortir de ce charnier putride. L’air frais qu’ils purent inspirer au sortir de la trappe leur sembla le plus suave des parfums, qu’ils avalèrent comme un doux nectar d’ambroisie, à grands traits.
