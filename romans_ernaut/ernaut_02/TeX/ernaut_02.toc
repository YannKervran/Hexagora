\babel@toc {french}{}
\contentsline {section}{Plan de J\IeC {\'e}rusalem}{i}{section*.2}
\contentsline {section}{Plan du Saint-S\IeC {\'e}pulcre}{iii}{section*.4}
\contentsline {section}{Liste des personnages}{v}{section*.6}
\contentsline {chapter}{\numberline {1}Prologue}{1}{chapter.0.1}
\contentsline {section}{\numberline {}Monast\IeC {\`e}re b\IeC {\'e}n\IeC {\'e}dictin de la Charit\IeC {\'e}-sur-Loire, hiver~1223}{1}{section.0.1.1}
\contentsline {chapter}{\numberline {2}Chapitre 1}{5}{chapter.0.2}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, rue de David, matin du dimanche 14~avril~1157}{5}{section.0.2.1}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, Temple du Seigneur, matin du dimanche 24~mars~1157}{9}{section.0.2.2}
\contentsline {section}{\numberline {}Matin du lundi 25~mars~1157}{19}{section.0.2.3}
\contentsline {section}{\numberline {}Milieu de matin\IeC {\'e}e du lundi 25~mars~1157}{28}{section.0.2.4}
\contentsline {section}{\numberline {}Fin de matin\IeC {\'e}e du lundi 25~mars~1157}{36}{section.0.2.5}
\contentsline {section}{\numberline {}Apr\IeC {\`e}s-midi du lundi 25~mars~1157}{44}{section.0.2.6}
\contentsline {section}{\numberline {}Soir\IeC {\'e}e du lundi 25~mars~1157}{53}{section.0.2.7}
\contentsline {chapter}{\numberline {3}Chapitre 2}{65}{chapter.0.3}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, parvis du Saint-S\IeC {\'e}pulcre, matin du dimanche 14~avril~1157}{65}{section.0.3.1}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, abords du change latin, d\IeC {\'e}but de matin\IeC {\'e}e du mardi 26~mars~1157}{66}{section.0.3.2}
\contentsline {section}{\numberline {}Matin\IeC {\'e}e du mardi 26~mars~1157}{76}{section.0.3.3}
\contentsline {section}{\numberline {}Fin de matin\IeC {\'e}e du mardi 26~mars~1157}{82}{section.0.3.4}
\contentsline {section}{\numberline {}Milieu d'apr\IeC {\`e}s-midi du mardi 26~mars~1157}{90}{section.0.3.5}
\contentsline {section}{\numberline {}D\IeC {\'e}but de la nuit du mardi 26~mars~1157}{96}{section.0.3.6}
\contentsline {chapter}{\numberline {4}Chapitre 3}{105}{chapter.0.4}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, \IeC {\'e}glise du Saint-S\IeC {\'e}pulcre, matin du dimanche 14~avril~1157}{105}{section.0.4.1}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, rue du Mont Sion, matin du mercredi 27~mars~1157}{107}{section.0.4.2}
\contentsline {section}{\numberline {}Fin de matin\IeC {\'e}e du mercredi 27~mars~1157}{113}{section.0.4.3}
\contentsline {section}{\numberline {}Midi du mercredi 27~mars~1157}{120}{section.0.4.4}
\contentsline {section}{\numberline {}D\IeC {\'e}but d'apr\IeC {\`e}s-midi du mercredi 27~mars~1157}{128}{section.0.4.5}
\contentsline {section}{\numberline {}Soir\IeC {\'e}e du mercredi 27~mars~1157}{136}{section.0.4.6}
\contentsline {section}{\numberline {}D\IeC {\'e}but de nuit du mercredi 27~mars~1157}{142}{section.0.4.7}
\contentsline {section}{\numberline {}Nuit du mercredi 27~mars~1157}{147}{section.0.4.8}
\contentsline {chapter}{\numberline {5}Chapitre 4}{153}{chapter.0.5}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, \IeC {\'e}glise du Saint-S\IeC {\'e}pulcre, matin du dimanche 14~avril~1157}{153}{section.0.5.1}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, abords de Saint-Martin, matin du jeudi 28~mars~1157}{155}{section.0.5.2}
\contentsline {section}{\numberline {}Sainte-Marie du Mont Sion, matin du jeudi 28~mars~1157}{159}{section.0.5.3}
\contentsline {section}{\numberline {}D\IeC {\'e}but d'apr\IeC {\`e}s-midi du jeudi 28~mars~1157}{168}{section.0.5.4}
\contentsline {section}{\numberline {}Apr\IeC {\`e}s-midi du jeudi 28~mars~1157}{174}{section.0.5.5}
\contentsline {section}{\numberline {}Soir\IeC {\'e}e du jeudi 28~mars~1157}{181}{section.0.5.6}
\contentsline {section}{\numberline {}Nuit du jeudi 28~mars~1157}{188}{section.0.5.7}
\contentsline {chapter}{\numberline {6}Chapitre 5}{197}{chapter.0.6}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, \IeC {\'e}glise du Saint-S\IeC {\'e}pulcre, dimanche 14~avril~1157}{197}{section.0.6.1}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, faubourg de la porte de David, matin du vendredi 29~mars~1157}{198}{section.0.6.2}
\contentsline {section}{\numberline {}Matin\IeC {\'e}e du vendredi 29~mars~1157}{204}{section.0.6.3}
\contentsline {section}{\numberline {}Fin de matin\IeC {\'e}e du vendredi 29~mars~1157}{210}{section.0.6.4}
\contentsline {section}{\numberline {}Fin d'apr\IeC {\`e}s-midi du vendredi 29~mars~1157}{217}{section.0.6.5}
\contentsline {section}{\numberline {}Soir du vendredi 29~mars~1157}{223}{section.0.6.6}
\contentsline {section}{\numberline {}Nuit du vendredi 29~mars~1157}{229}{section.0.6.7}
\contentsline {chapter}{\numberline {7}Chapitre 6}{237}{chapter.0.7}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, parvis du Saint-S\IeC {\'e}pulcre, matin du dimanche 14~avril~1157}{237}{section.0.7.1}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, abords de Saint-Martin, t\IeC {\^o}t le matin du samedi 30~mars~1157}{238}{section.0.7.2}
\contentsline {section}{\numberline {}Matin du samedi 30~mars~1157}{244}{section.0.7.3}
\contentsline {section}{\numberline {}Milieu de matin\IeC {\'e}e du samedi 30~mars~1157}{251}{section.0.7.4}
\contentsline {section}{\numberline {}Apr\IeC {\`e}s-midi du samedi 30~mars~1157}{257}{section.0.7.5}
\contentsline {section}{\numberline {}Soir\IeC {\'e}e du samedi 30~mars~1157}{263}{section.0.7.6}
\contentsline {chapter}{\numberline {8}Chapitre 7}{273}{chapter.0.8}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, tombeau du Christ, \IeC {\'e}glise du Saint-S\IeC {\'e}pulcre, matin du dimanche 14~avril~1157}{273}{section.0.8.1}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, porte de David, matin du dimanche 31~mars~1157}{274}{section.0.8.2}
\contentsline {section}{\numberline {}D\IeC {\'e}but de matin\IeC {\'e}e du dimanche 31~mars~1157}{280}{section.0.8.3}
\contentsline {section}{\numberline {}Milieu de matin\IeC {\'e}e du dimanche 31~mars~1157}{287}{section.0.8.4}
\contentsline {section}{\numberline {}Apr\IeC {\`e}s-midi du dimanche 31~mars~1157}{295}{section.0.8.5}
\contentsline {section}{\numberline {}Fin d'apr\IeC {\`e}s-midi du dimanche 31~mars~1157}{301}{section.0.8.6}
\contentsline {section}{\numberline {}Soir\IeC {\'e}e du dimanche 31~mars~1157}{306}{section.0.8.7}
\contentsline {chapter}{\numberline {9}\IeC {\'E}pilogue}{313}{chapter.0.9}
\contentsline {section}{\numberline {}J\IeC {\'e}rusalem, \IeC {\'e}glise du Saint-S\IeC {\'e}pulcre, fin de matin\IeC {\'e}e du dimanche 14~avril~1157}{313}{section.0.9.1}
\contentsline {chapter}{\numberline {10}Addenda}{317}{chapter.0.10}
\contentsline {section}{\numberline {}Notes de l'auteur}{317}{section.0.10.1}
\contentsfinish 
