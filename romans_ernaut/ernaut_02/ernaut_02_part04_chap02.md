## Chapitre 2

### Jérusalem, parvis du Saint-Sépulcre, matin du dimanche 14 avril 1157

Déconcerté par sa discussion, Ernaut s’éloigna de quelques pas du saint Sépulcre, le visage fermé. Il alla s’asseoir sur le sol, dans la galerie, un peu à l’écart. Il savait que le chanoine avait raison, ce qui ne faisait que renforcer son trouble. Le pèlerinage jusqu’à Jérusalem et sa visite dans différents lieux de culte avaient été requis pour son bien et le Salut de son âme.

Comme un baume longtemps attendu sur une plaie vive, ce voyage devait apporter de la paix dans sa vie. Il avait donc assisté, avec le plus de sérieux et d’application dont il était capable, à toutes les cérémonies, baisé les tombeaux de nombreux saints censés intercéder en sa faveur. Mais le mal avait fait une telle irruption dans ce moment sacré qu’il n’avait pu le négliger et le mettre de côté. Certain de son bon droit, et confiant dans ses capacités, il avait eu le désir de joindre ses efforts à ceux des forces du Bien, pour repousser les démons qui profanaient ces lieux.

Les célébrations de Pâques apportaient l’espoir aux fidèles, qui voyaient le Messie triompher de la Mort, après sa crucifixion pour les Péchés du monde. Ernaut n’avait jamais tout à fait compris comment cela fonctionnait, mais il se souvenait qu’il s’était toujours senti ragaillardi après ces quelques jours, comme purifié de l’intérieur, apaisé par les perspectives d’un avenir meilleur une fois ces mystérieuses cérémonies terminées. Et le saint Sépulcre était l’endroit où tout s’était réellement déroulé, où le corps du Christ avait attendu sa Résurrection.

Non loin de là se trouvait le Golgotha, où il avait été mis en croix. Les milliers de pèlerins qui venaient chaque année se recueillir en étaient les vivants témoins, nulle place n’était plus sainte sur terre pour les chrétiens. Ils voyageaient depuis fort loin pour avoir le privilège d’entendre la messe en ce lieu, de toucher le tombeau, de faire quelques offrandes. Le Sacré s’incarnait en cet endroit où les plus grands mystères de leur Foi s’étaient déroulés.

### Jérusalem, abords du change latin, début de matinée du mardi 26 mars 1157

Partout dans la ville, les célébrants appelaient à l’office de prime et le vacarme des cloches couvrait l’activité humaine. On n’entendait même plus le pépiement des oiseaux qui voletaient d’une corniche à l’autre, attendant avec patience que des miettes soient égarées pour se jeter dessus en un attroupement confus. Occupé à remplir son panier, Ernaut ne prêta guère attention à l’appel à la prière. Il avait été pourtant enthousiasmé lors de leur arrivée par l’incroyable quantité d’églises et de couvents édifiés là. Puis, avec le temps, il s’était amusé de ce que certains prêtres faisaient sonner leur office les premiers, de façon à attirer le plus d’ouailles possible. Il avait fini par s’y accoutumer et, comme la plupart des habitants de Jérusalem, cela ne lui servait guère plus qu’à repérer le moment de la journée.

Il avait désormais ses habitudes et la vie trouvait sa routine. Pour l’heure, il s’achetait des rissoles à jour de poisson, sorte de beignets aux fruits qu’il appréciait tout spécialement. C’était censé être un plat de jour « maigre », donc adapté au carême et pourtant il incitait au péché de gourmandise. Ernaut se fournissait généralement aux mêmes endroits et entretenait des relations cordiales, presque amicales, avec la plupart des charcutiers, pâtissiers et cuisiniers qu’il fréquentait.

Il avait une affection toute particulière pour Margue l’Allemande qui, comme son nom ne l’indiquait pas, était originaire de Normandie, arrivée là encore enfant. Veuve entre deux âges, elle avait un tempérament volontaire et une gouaille qui parvenait parfois à impressionner Ernaut. Et elle savait accommoder les plats avec tellement de talent ! Elle était présentement occupée à l’arrière de la petite boutique sombre, en haut de la rue du Mont Sion, peu loin du Change latin, surveillant le contenu des paniers que son marmiton avait rapportéé du four situé à l’étage d’un bâtiment tout près.

D’une certaine corpulence, elle était rapidement en sueur et le torchon qu’elle avait glissé dans sa ceinture servait autant à lui éponger le front qu’à essuyer ses mains. Alors qu’elle se retournait, les bras chargés de fouaces[^fouaces] dorées, elle sourit en remarquant le jeune géant appuyé l’épaule contre son pilier, obligé de se pencher pour voir l’intérieur de l’échoppe. Elle lui lança, tout en clignant de l’œil :

« Ça alors, on a planté nouvel arbre dans ma rue ?

— Ne t’inquiète pas, maîtresse, je ne prendrai pas racine ! C’est seulement la faim qui m’a mené ici. »

Elle posa son butin sur l’étal, répandant une bonne odeur de pâte chaude et de beurre et fit semblant de minauder.

« Oui, je vois, tu n’as aucune sorte d’amitié pour moi, tu n’obéis qu’à ton gros ventre.

— Non, pas uniquement ! Il y a moult échoppes bien agréables pour cela. Mais pour rien au monde je ne voudrais passer au-devant de toi sans faire salut. »

Margue dodelina de la tête en faisant claquer sa langue et écarta son voile à l’aide de son avant-bras.

« C’est ça, c’est ça. T’es mignonnet ! »

Ernaut cherchait une réponse spirituelle à faire lorsqu’une bousculade attira son attention au bout de la rue, en contrebas. Des lances montaient parmi la foule depuis la porte du Mont Sion, et quelques casques pointaient. Derrière ce petit groupe, un cavalier en gambison[^gambison] de soie avançait au pas, rendant parfois de la main un salut lancé par un commerçant. Margue se pencha par-dessus son éventaire pour voir ce qui se passait.

« Ah !? Ce n’est que le vicomte Arnulf. Il pourrait tout de même démonter quand il passe en venelle. Ce n’est pas si large !

— Surtout avec la foule en ces temps de mangeaille.

— D’un autre côté, il doit mieux voir de là-haut. On dit qu’il a œil de rapace pour dénicher fraudeurs. D’ailleurs je le préfère encore au mathessep. Celui-là, il ne pense qu’à obtenir faveurs en échange de sa bienveillance.

— Étrange, il m’a fait bonne impression. Je l’ai vu hier. Auprès de la pauvresse, il tentait de réconforter la pauvre gamine qui a trouvé la dépouille de la pérégrine. »

Le visage soudain déformé par une grimace boudeuse, Margue lâcha dans un rictus :

« C’est bien parce que tu n’as pas à subir son joug. J’ai dans la rue quelques compaings qui pourraient t’en causer deux mots, de ce bâtard. »

Réalisant ce dont Ernaut venait de parler, la femme se rapprocha et reprit un peu moins fort, s’exprimant du bout des lèvres, ainsi qu’elle l’aurait fait d’un sujet qui lui soulevait le cœur.

« Je ne sais pas si tu as ouï : il se dit que cette pauvre femme aurait été victime d’un sarrasin !

— Tu crois ?

— Je ne sais, beaucoup d’entre eux auraient grand dépit du tour que le roi leur a joué à Panéas en début d’année. Noradin[^nuraldin] lui-même aurait ordonné à des murdriers de venir venger les leurs. Elle ne serait que la première, grand massacre serait prévu pour le jour même de Pâques, dans le Saint-Sépulcre ! »

Elle appuyait son discours d’un hochement de tête grave. Ernaut écarquilla les yeux et se gratta le nez, un peu circonspect.

« Je ne vois guère comment ils pourraient faire.

— Toi, tu es jeune et bien bâti, alors forcément tu n’as jamais peur de rien. Mais crois-en une vieille folle comme moi : ces maudits païens trouvent toujours adroite façon de nous nuire. »

Achevant sa diatribe par une tape amicale enfarinée sur l’épaule d’Ernaut, la cuisinière s’éloigna alors, les sourcils froncés, inquiète apparemment de ce que faisait son apprenti à l’arrière. Se plaquant contre le pilier de la boutique, Ernaut observa le cortège des hommes d’armes et admira la prestance du vicomte tandis qu’il chevauchait paisiblement au milieu de la foule agglutinée.

Croisant un instant son regard, il baissa le chef respectueusement et obtint un hochement de tête approbateur en retour. Puis, déposant les quelques mailles[^maille] pour régler ses achats, il s’éloigna tranquillement après avoir salué amicalement l’employé qui avait garni son panier.

Il avait comme d’habitude l’intention de rejoindre la cour intérieure de l’hôpital par le passage depuis la rue de David, afin d’aller voir son frère. Le carrefour à la croisée des deux principales artères de la cité était comme toujours extrêmement fréquenté. La présence du change latin n’arrangeait rien, avec tous les voyageurs récemment arrivés pour Pâques. Par ailleurs, les trois ruelles qui partaient vers le nord, la Rue aux Herbes, Malquisinat et la Rue Couverte abritaient le plus important groupement de boutiques d’alimentation de la ville. On y trouvait de quoi se restaurer et s’abreuver à toute heure, des plats chauds et froids, selon des recettes en provenance d’Europe et d’ailleurs. Pourtant, l’agitation était plus intense qu’habituellement et les regards suspicieux des gardes surveillant le change inquiétèrent Ernaut.

Interrogeant un jeune garçon de course qui transportait un lourd panier empli de miches de pain, il apprit qu’on venait juste de repérer une nouvelle femme assassinée non loin de là, dans le marché aux volailles et au fromage à l’angle sud-est du Saint-Sépulcre. Incapable de résister à la curiosité, il partit droit vers le nord, oubliant sa résolution d’aller voir son frère. Il était en partie excité par la situation, mais également anxieux de découvrir qui était la victime. Il espérait secrètement, certainement comme la plupart autour de lui, que cela n’était pas une de ses connaissances.

La foule était de plus en plus dense en avançant, à tel point qu’il fallait se contorsionner pour s’approcher. Au débouché de la rue du Sépulcre, les curieux amassés avaient complètement bloqué le passage. Des rumeurs parcouraient l’assemblée, parlant de démons s’en prenant aux femmes. La description du corps devenait plus horrifique à chaque fois qu’elle était transmise et répétée. Certains pèlerins commençaient à s’agiter, invectivant un jeune homme casqué, le visage fin, qui se tenait à l’entrée du marché. Assez grand, il portait une épée au côté et ses yeux clairs cherchaient en tout sens une échappatoire à ce qui se transformait peu à peu en émeute. Anonymes, quelques cris fusèrent : « Que fait le roi ? », « Réagissez, par la barbedieu ! », « Combien en faudra-t-il ? », « Punissez ces maudits païens ! ».

Certains interpelaient même directement le sergent :

« Ça fait la seconde en deux jours ! Lança une voix excitée, soutenue en chœur par les autres.

— À quoi sert le guet si on n’est même pas sauf en plein Jérusalem ? Ajouta un second.

— Quand le berger se repose, le loup chie de la laine » renchérit un gros homme à face rubiconde, les poings posés sur les hanches.

Son panier toujours à la main, Ernaut força le passage, bousculant sans ménagement ceux qui le gênaient. Il eut droit à quelques œillades rageuses et plusieurs coups de coude agacés, mais, indifférent, il parvint sans encombre au premier rang. Le voyant s’approcher, le sergent eut un regard inquiet, sans reculer pour autant et cria d’une voix qui se voulait assurée :

« Demeurez cois. J’ai envoyé quérir le vicomte, nous allons nous occuper de cette pauvresse. »

Pour toute réponse, il n’entendit que des grommellements et des invectives. Un pèlerin portant besace et bourdon, large d’épaules, le regard franc, rétorqua :

« On voit bien que vous ne faites mie. Allez-vous laisser nos femmes se faire meurtrir ?

— Certes pas. Nous œuvrons de notre mieux. Il se trouve tellement de voyageurs en la cité qu’il nous faut du temps.

— Foutaises ! Elle a eu droit à du temps, la morte ? On est soucieux pour nos femmes, nos filles, nos mères ! »

Un acquiescement général gronda, comme une sourde menace qui vrombissait alentour. Resté muet jusque-là, Ernaut fit face à l’homme :

« C’est ta femme qui a été meurtrie ? »

Le pèlerin fut un instant décontenancé par la question, puis inquiété par la taille de son contradicteur. Son élocution était plus hésitante lorsqu’il répondit :

« Non… Pourquoi pareille question ?

— Parce que si elle n’est point de ta parentèle, tu n’as nul droit de demander justice, crâne de puce.

— Que me contes-tu là ? Cela n’a rien à voir…

— Bien sûr que si. Tu es là à crier comme goret, à quémander justice. A-t-on touché les tiens ? »

L’homme secoua la tête en dénégation et s’apprêtait à répondre, mais l’adolescent ne lui en laissa pas le temps.

« Alors laisse les hommes de la Cour du roi œuvrer plutôt que chercher noise…

— Et qui va protéger mon épouse, ma fille ? Il y a démon en cette cité et nous demandons au roi d’agir.

— Je suis aise d’apprendre qu’il te faut chevalier couronné pour mettre à l’abri ta parentèle. Il me semblait qu’un homme digne de ce nom saurait défendre les siens. Peut-être n’es-tu qu’un escouillé en ce cas… »

La remarque arracha quelques rires alentour. Le provocateur accusa le coup, cherchant une répartie. Ne laissant aucune chance à son adversaire, Ernaut s’avança d’un pas, se tournant face à la foule pour faire front avec le sergent.

« Entendez-moi, j’ai nom Ernaut, j’ai pérégriné depuis Vézelay jusqu’en cette sainte cité. Comme vous tous, j’ai eu grande peine à la mort de la première marcheuse de Dieu. Et je suis touché de cette nouvelle meurtrerie. Mais j’assure devant vous que la Cour s’en préoccupe, et m’a questionné fort avant sur la première femme, que je connaissais quelque peu. Si des familiers veulent demander justice, je suis prêt à jurer qu’ils seront entendus par le vicomte et les bourgeois. Que quiconque pense que je parle de déshonnête façon s’avance et me face affront maintenant. »

Quelques regards circonspects se détournèrent, les chuchotements remplacèrent les cris. Le querelleur avait reculé, mais continuait à fixer Ernaut d’un œil noir. Une agitation reprit depuis la rue du Sépulcre, les curieux étant bousculés par un serviteur enturbanné, clairement autochtone. Derrière lui tentait de se faufiler une silhouette drapée de noir. Le petit chapeau de feutre blanc surnageait à peine, mais rien ne semblait l’effrayer. Lorsque frère Garin put trouver un peu de place, il lança de sa voix habituée à commander :

« Eh bien, eh bien ! Qu’avons-nous là, mes frères ? Est-ce bien lieu et place pour chercher rixe ? »

Il sourit à Ernaut et au sergent, avant d’affronter la foule, confiant dans son autorité.

« Voyons mes frères, à quoi sert-il de s’agiter ainsi ? N’ajoutons pas la violence outre la violence. »

Hésitants à s’en prendre à un homme de Dieu, les plus vindicatifs abandonnèrent et la pression retomba graduellement. L’agitation commençait à se faire moins forte et la foule moins dense, les derniers curieux arrivés repartaient, scindés en petits groupes, commentant entre eux l’événement. Frère Garin s’avança et salua le sergent ainsi qu’Ernaut.

« Par ma foi, jeune Ernaut, tu sais tout de même te rendre utile, d’aucune fois. »

Son sourire était amical, son regard inquiet malgré tout.

« Alors, sergent, est-ce encore une pauvresse qu’on a trouvée ?

— Oui, répondit le sergent, l’air contrarié. Découverte en une petite échoppe oubliée qui sert peu ou prou de débarras. »

L’hospitalier se tordit les lèvres quelques instants avant de poursuivre, d’une voix hésitante :

« Vous croyez que c’est le même…

— Sans nul doute aucun. De même elle n’a pas été dépouillée et son visage pareillement écrasé. Tout n’est que bouillie sanglante. »

Frère Garin hocha la tête.

« Quel horrible spectacle en cette semaine qui devrait être joyeuse ! Avez-vous prévenu quelque officiel ?

— Le vicomte doit être informé désormais. J’attends juste des renforts pour emporter la dépouille.

— Fort bien. Je me tiendrai à l’hôpital pour la préparer. À plus tard, sergent. »

Puis il reprit le chemin de l’hôpital par la rue du Saint-Sépulcre, suivi de son serviteur. Ernaut se tourna vers l’homme d’armes.

« Peut-être pourrais-je la reconnaître aussi, si je peux voir.

— Comme tu le veux, la scène n’est guère attrayante. Je peux t’y mener si tu y tiens. »

Le jeune homme renifla et fit mine d’hésiter, mais la curiosité fut la plus forte, il opina du menton.

Tandis qu’ils avançaient dans le marché, à peu près vidé de ses clients, le sergent tendit la main.

« Grand merci pour ton soutien, jeune Ernaut. Sans toi je ne sais ce que j’aurais pu faire. On me nomme Eudes, Eudes Larchier. »

Ernaut accepta la poignée et serra vigoureusement.

« C’est moi qui ai reconnu la pérégrine hier, que le mathessep a interrogé.

— On m’avait dit qu’il s’agissait d’un grand gaillard, je le vois donc de mes yeux répondit le sergent, le visage rapidement éclairé d’un sourire. »

Ils arrivèrent alors dans un recoin où des paniers déchiquetés et tout un fatras de vieux objets partiellement décomposés finissaient de pourrir, mêlés à de la paille, de la terre et des déjections d’animaux.

Abandonné dans une posture grotesque, les pieds en l’air et le buste ramassé sur le sol, cachant à demi la tête, le corps avait été une nouvelle fois hâtivement recouvert. Malgré toute sa bonne volonté, Ernaut ne put s’assurer qu’il connaissait la pauvre femme. Il s’en excusa auprès d’Eudes, qui compatit volontiers, le visage révulsé à cette vision.

« Tu devrais peut-être nous compaigner à l’hôpital. Une fois lavée de ces immondices, peut-être sa face éveillera-t-elle quelques souvenirs…

— Pourquoi la confiez-vous aux frères ?

— C’est l’usage. Ils prennent soin des corps et des âmes de tous les pérégrins. Nous leur portons ceux qui meurent en la cité. Au moins connaissent-ils chrétienne demeure pour leur dépouille.

— N’y a-t-il possibilité d’exposer son corps quelque temps en une chapelle, voir si des familiers la reconnaissent ? »

Eudes, visiblement ennuyé par la question, soupira d’un air triste.

« Cela nous est difficile. Il y a déjà tant à faire pour les vivants et si peu pour s’acquitter des tâches. »

Le jeune homme marqua un temps, plissant les yeux, contrarié par ce qu’il croyait comprendre.

« Alors vous n’allez pas chercher le coupable ? »

Le sergent hocha la tête d’un air désolé.

« C’est pas notre rôle, mon garçon. C’est à la parentèle de se débrouiller, s’ils trouvent le mécréant et veulent l’accuser publiquement.

— Les chanoines du Saint-Sépulcre sont fort inquiets de ce qui se passe, ne faut-il pas répondre à leurs attentes ?

— On a déjà suffisamment à faire avec les tâches habituelles, surtout en cette période. Si en plus on devait courir après les fous meurtriers…

— Qu’est-ce qui vous dit qu’il est fou ? »

Le sergent haussa les épaules et, le visage dépité, ouvrit les mains en signe d’impuissance.

« Le corps lui-même ! Si ce n’était la trace du poignard, j’aurais cru que c’était œuvre de bête. Pareil acharnement, seul un damné en est capable. Je peux comprendre qu’on plante sa lame au parmi d’un adversaire, mais de là à le massacrer alors que son âme a déjà quitté les chairs ! »

Un autre sergent, un peu âgé, demeuré à l’abri près du corps, hésita un instant avant de formuler à voix haute l’hypothèse qui venait de faire jour dans son esprit.

« Ou alors il voulait peut-être éviter que le corps soit en bon état pour le Jugement Dernier. »

Ernaut et Eudes tournèrent la tête vers le vieil homme au regard inquiet. Il avait l’air d’être effrayé par l’idée qu’il venait d’avancer et mangeait nerveusement sa lèvre supérieure, au risque d’avaler quelques poils de sa longue moustache grisonnante. Eudes se gratta la nuque, réfléchissant quelques instants à une proposition qui n’était pas si saugrenue, dans la ville Sainte.

« En ce cas, il devait avoir bien horrible haine envers elles, pour les damner à jamais ! »

### Matinée du mardi 26 mars 1157

La pièce était grande, éclairée par une poignée d’ouvertures en hauteur, partiellement occultées par des claustras. L’odeur qui y régnait était désagréable, malgré la présence de plusieurs petits braseros sur lesquels brûlaient quelques plantes aromatiques. On sentait les effluves douçâtres de décomposition, brassage souffré issu du mélange s’échappant des chairs mortes, des gaz des viscères, des déjections des patients décédés. Ce n’était pas la saveur métallique du sang, mais une puanteur bien plus âcre, fort poisseuse, très insidieuse. Elle semblait suinter des parois, jaillir des meubles, s’exhaler du sol et s’imprégner dans tous les vêtements de ceux qui y entraient.

Une trentaine de tables étaient posées en plusieurs rangées, accueillant chacune un cadavre, parfois enfermé dans un linceul, ou simplement recouvert d’un drap de toile grossière. Une demi-douzaine de valets étaient occupés à manipuler les corps, les nettoyer. Puis ces derniers étaient portés en une petite salle faisant office de chapelle, laissés à la bienveillance d’un Christ en croix, éclairés de quelques chandelles de suif et de cire.

Eudes Larchier semblait indifférent à l’atmosphère sinistre de l’endroit, seulement gêné par l’odeur fétide qui s’y déployait. Il se pinçait nerveusement le nez de façon régulière, attendant d’avoir la réponse d’Ernaut. À leur côté, frère Garin patientait, l’air débonnaire, surveillant du coin de l’œil les domestiques affairés. Ils avaient été menés dans la salle de l’hôpital où l’on préparait les défunts avant de les inhumer. Il s’en trouvait parfois plusieurs dizaines dans la journée, selon les moments.

Ainsi que l’avait expliqué Eudes, tous les inconnus, les pauvres, les malades, les voyageurs qui n’avaient personne pour veiller sur eux, étaient récupérés, lavés, arrangés. Soucieux de leur garantir un corps pour recevoir leur âme au jour de la résurrection et du jugement dernier, les frères de Saint-Jean ne négligeaient pas leurs patients une fois leur âme partie. La défunte avait donc été longuement préparée, les soigneurs ayant eu grande peine à redonner un visage humain à ce qui n’était plus que chairs meurtries et os broyés. Plusieurs bandages maintenaient au mieux les parties qu’ils avaient reconstituées. Après un bon moment à tenter de retrouver ce que pouvaient être le nez, la bouche, ou le menton avant d’être pareillement massacrés, Ernaut remit le drap en place et confirma la connaître.

« Elle était pèlerine françoise, je ne saurai affirmer d’où. Je crois qu’elle faisait partie du même groupe que l’autre.

— As-tu souvenance d’autres de leurs compagnons, demanda Eudes, se mordant la lèvre.

— Je ne saurais les décrire, mais pourrai les reconnaître. Plusieurs hommes, avec enfants, les compagnaient.

— Comment se fait-il que nous n’ayons aucune demande de la parentèle en ce cas ? Que leur est-il tous arrivé ? »

Le silence fut la seule réponse qu’il obtint. Ernaut se demanda s’il lui fallait prévenir le prieur du Sépulcre de cette nouvelle découverte. Ils avancèrent lentement vers la sortie, guidés par Garin. Celui-ci les mena à travers un dédale de salles, de couloirs et d’escaliers jusqu’à rejoindre un petit groupe qui discutait dans une courette entourée d’une jolie colonnade sur trois côtés.

Des étourneaux sansonnets sifflaient aux abords, occupés à installer leurs nids dans les corniches supérieures. L’impressionnante quantité de fientes déposées sur le sol et les reliefs des murs attestait de l’ancienneté de leur présence. Prudent, Ernaut ne s’aventura pas au-delà de la galerie couverte. Il se trouvait là un chanoine habillé de la même tenue noire de qualité que celle qu’Ernaut avait vue la veille, certainement un des religieux du Saint-Sépulcre. Le jeune homme esquissa un discret sourire, amusé de la rapidité de réaction du pouvoir canonial.

Le frère était assez âgé, un peu ventripotent, le visage rond avec des yeux fatigués, soulignés d’épais cernes. Sa calvitie ne lui laissait que peu de chance d’afficher une tonsure bien visible. Il se présenta comme Giraud. À ses côtés, un soldat arborait une épée tenue dans un fourreau suspendu à un baudrier assez sobre. Sa cotte, de belle facture, était assez simple pour ne pas paraître luxueuse, mais suffisamment pour montrer son statut, certainement élevé. Ernaut nota néanmoins qu’il ne portait pas d’éperons dorés. Il avait les cheveux mi-longs, et une barbe taillée avec soin, en pointe. Avec ses larges bajoues, son nez en trompette semblait minuscule, surmonté de petits yeux ronds. Il parlait en faisant de grands gestes à leur entrée. On lui présenta comme maître Fouques, un des hommes du vicomte Arnulf.

Enfin, de haute stature, soulignée par son ample manteau noir frappé de la croix blanche de l’Hôpital de Saint-Jean, un frère au visage émacié observait ses interlocuteurs sans desserrer les lèvres. Ses yeux caves et ses joues creusées renforçaient l’impression maladive de tout son corps. Sa barbe, pourtant visiblement soignée, était miteuse, et quand il parlait, une odeur atroce s’échappait de sa bouche, où ne trônaient que quelques chicots brunâtres. Ses manières et son élocution étaient néanmoins très distinguées lorsqu’il salua, indiquant qu’il se nommait Matthieu. Garin termina les présentations et, d’un geste, fit comprendre à Ernaut de se tenir en retrait, silencieux.

Apparemment, le souci était de savoir comment gérer le problème autour de ces femmes assassinées. On les retrouvait dans des zones qui tombaient sous la juridiction normale du Saint-Sépulcre et de l’Hôpital. Il fallait donc veiller à ne froisser aucune susceptibilité. La question aurait pu être simple à régler, vu qu’il s’agissait d’un crime de sang et qu’il appartenait alors à la Cour des Bourgeois du roi de s’en occuper, donc au vicomte. Mais depuis de nombreuses années, les deux établissements voisins s’affrontaient à la moindre occasion et ils semblaient bien décidés à ne pas rater une si affriolante opportunité de ranimer d’anciens griefs.

Ce fut Giraud qui commença, après seulement quelques instants passés à résumer la situation.

« Bien grave situation que celle-ci. Il nous faudrait certes obtenir aval du Patriarche pour toutes ces questions. C’est là délicat problème que seul un saint homme éclairé peut résoudre.

— Vous pensez déranger monseigneur Foucher pour ces questions ? Railla Matthieu.

— Certes oui. Il nous faut un guide avisé. Je n’en vois guère d’autres capables, d’autant que les meurtreries ont eu lieu en nos abords.

— Vous pensez pouvoir tirer parti de l’absence de notre maître Raymond ! Quelle outrecuidance ! C’est là également juridiction nôtre, vous le savez bien. Je ne saurai vous laisser vous comporter comme seigneur en son fief. »

Le gros chanoine déploya un sourire fielleux avant de poursuivre.

« Je n’en doute guère, frère. Nous avons bien vu vos prouesses, après la prise d’Ascalon. Vous pensiez conquérir le saint tombeau du Seigneur les armes en mains !

— Fredaines ! Cracha Matthieu. Nous ne faisions que batailler pour nos droits !

— Niez-vous avoir lancé des traits sur la sainte église du Christ ? Menacé ouvriers et frères ? La preuve de votre forfaiture est sue de tous, exposée en la chapelle du Calvaire. »

L’hospitalier serra la mâchoire et son regard se fit haineux lorsqu’il répondit.

« Voilà bien venin du vieux Foucher issu par vos crocs ! Vous avez les premiers cherché à nuire à l’édification de notre hôpital, destiné à pauvres et pérégrins…

— Et les cloches ? Et les cloches ? Que vous sonnez avant l’heure, et parfois durant les sermons de monseigneur Foucher ! N’est-ce pas là motif de juste querelle ?

— Mes amis ! Mes amis ! Lança Fouque, impassible. Revenez à de meilleurs sentiments, je vous en conjure. Nous sommes là pour tenir conseil, pas pour tenir plaid de nos griefs les uns envers les autres. »

Les deux clercs se reculèrent instinctivement, le souffle encore attisé par la violence de l’échange. Matthieu ajouta à mi-voix :

« D’autant que Sa Sainteté Adrien nous a donné raison !

— Contre de bons marcs d’argent, certes ! Rétorqua Giraud. »

Rouge de colère devant la provocation, l’hospitalier se mit à souffler comme s’il lui fallait évacuer le trop-plein de pression dans son crâne. L’œil mauvais, il cherchait quoi répondre quand la voix de Garin se fit entendre, bien calme en comparaison.

« Que préconisez-vous, maître Fouque ? Qu’en pensent messire vicomte et les hommes du roi ? »

Le sergent lança un regard alentour, vérifiant qu’on n’allait pas l’interrompre, puis il inspira avant d’apporter sa réponse :

« Messire Arnulf est fort contrarié. Ce moment de l’année est fort occupé, pour nous tous, et celui où le nombre de pérégrins est le plus grand. Outre, il nous faut faire montre du sérieux avec lequel nous envisageons la chose, car vilain courroucé est à demi enragé…

— Une idée pour cela ? Interrogea Giraud, apparemment calmé.

— Nous espérons trouver la famille, savoir si elle demande justice, si elle connait le murdrier. Pour l’instant, nous ne pouvons guère agir au-delà.

— Je crains que les pérégrins ne soient fort marris de se savoir si mal protégés ajouta Garin. S’ils répandent ces rumeurs une fois chez eux, cela nous causera grand tort. Il est déjà fort malaisé de peupler les casaux[^casaux]. »

Fouque hocha la tête, se sachant impuissant à endiguer les problèmes. Il fit plusieurs moues, cherchant ses mots. Matthieu en profita pour s’emparer de la parole.

« Il nous faut être tenus informés de toute nouvelté sans retard. Fors cela, nous ne saurons apaiser sereinement nos hôtes.

— En ce cas, il nous faut en savoir autant que vous, car nous ne saurions guider nos fidèles si nous errons nous-mêmes parmi le brouillard », ajouta Giraud.

Matthieu haussa les yeux au ciel à cette remarque, et s’apprêtait à répondre lorsque Fouque le prit de vitesse :

« Voilà bien le moins que nous puissions faire. Mais ce qui nous manque, ce sont des bras pour s’atteler à la tâche…

— Comme pour chacun de nous, maître Fouque, je vous l’assure, répliqua Garin. Nous ne savons plus où donner de la tête. Rien que la nuit passée, il nous a fallu porter en terre sept de nos hôtes, en faire entrer trois nouveaux, en opérer six, et veiller trois malades de quarte fièvre. Ceci rien que dans ma rue. Je ne vous ferai affront de vous rappeler que nous nourrissons grand nombre de pérégrins…

— Sans compter les hommes partis s’emparer de Panéas » ajouta mezza-vocce Giraud

 Garin ne releva pas et ne laissa pas le temps à Matthieu de le faire.

« Bref, tout cela pour dire que nous ne pouvons détacher un sergent ou l’autre, à mon grand regret.

— Nous avons semblable souci avec les croyants à guider, conseiller, accueillir, autour du tombeau du Christ. Tous nos frères se relaient pour apporter réconfort et soutien spirituel. Je ne vois guère où trouver un désœuvré à vous prêter.

— Peut-être pourrions-nous demander à maître Ernaut, ici présent, de nous porter assistance auprès des pérégrins, suggéra Garin. Je le sais présent depuis plusieurs mois ici et suffisamment alerte pour être utile. Tant que nous n’aurons pas trouvé les familles, il me semble raisonnable d’avoir un fidèle qui participe à nos recherches. »

Les présents hochèrent tous la tête en assentiment et se tournèrent vers le géant, qui suivait les échanges avec attention. Il inclina le buste avec solennité et déclara accepter l’honneur qui lui était fait, qu’il s’efforcerait de se montrer à la hauteur de la mission. Encore une fois, il n’était pas tout à fait certain de ce qu’on attendait de lui, ni s’il ne se lançait pas dans une tâche trop ambitieuse pour lui. Toutefois, ce qui lui plaisait avant tout, c’était qu’on l’autoriserait vraisemblablement à aller fouiner ici et là. Et il n’en fallait guère plus pour le contenter, quelles que soient ses appréhensions. Garin lui sourit avec chaleur et lui tapota le bras affectueusement.

« Eh bien tout est dit mes amis, puisse le Seigneur nous guider en cette ténébreuse affaire.

— Amen ! » conclurent Matthieu et Giraud spontanément, surpris l’un comme l’autre, et quelque peu contrariés, de cette réponse à l’unisson.

### Fin de matinée du mardi 26 mars 1157

Semblant faire écho à l’atmosphère qui régnait dans le quartier, le temps était bas, d’un gris uniforme, et peu de lumière pénétrait dans la grande salle de l’hôpital. Des lampes avaient donc été allumées un peu partout, leurs flammes tremblotantes dans l’immensité apportant plus de réconfort que de clarté. Ernaut était assis sur le coffre au pied du lit de son frère, comme souvent, et il regardait ses pieds, les coudes posés sur les genoux, l’air absent. Il venait de détailler par le menu les incidents récents, sans omettre sa courageuse intervention auprès des sergents de ville et son intégration à l’enquête. Lambert avait écouté patiemment jusqu’au bout et réfléchissait désormais en silence. Son visage était toujours aussi fatigué, mais ses yeux avaient retrouvé leur vivacité et il ne subsistait que de rares traces de fièvre dans son corps.

« Tu sais, Ernaut, il serait sage de ne pas t’en mêler plus avant. Nous n’avons rien à voir avec ces pauvres femmes. Laissons leur famille s’occuper de tout cela. Souviens-toi que cela ne te réussit pas de t’occuper des affaires des autres. »

Ernaut fit une moue, se mordant les lèvres. Il tendit le bras pour suivre du doigt une ligne imaginaire sur son soulier.

« Certes, pourtant il me semblait charitable de porter aide à ces deux femmes. Personne ne parle pour elles. Je voulais aider les sergents, sans plus. Je ne pensais être ainsi désigné par tous ces puissants ordres. »

Lambert se releva dans le lit, sortant son buste de sous les couvertures. Il se gratta et se pinça le nez, cherchant ses mots.

« Je suis sûr qu’il n’y avait pas malice. J’espère juste que tu n’as pas simplement suivi ta curiosité habituelle ou pis encore, commis quelque péché d’orgueil. Voilà aussi mauvais conseiller que la colère… Je sais que c’est dur pour toi d’être seul ici. Je vais bientôt sortir, nous allons achever nos dévotions pour Pâques. Ton vœu sera pleinement accompli et nous pourrons enfin nous consacrer à notre installation. »

Le colosse approuvait de la tête en silence, mais gardait les yeux fixés sur ses pieds. Il se leva doucement, comme s’il se réveillait d’une longue nuit de sommeil.

« Tu parles de raison, je dois surtout penser au pourquoi de ma présence en ces lieux ! »

Il marqua une pause et reprit :

« Je vais te laisser, il me faut m’occuper de mon souper et apprêter ma tenue pour les cérémonies des jours à venir.

— C’est vraiment dommage que je ne puisse être à tes côtés en cette magnifique occasion. »

Ernaut fit un sourire forcé et claqua amicalement son frère sur l’épaule.

« Ne t’inquiète pas, il se trouvera bien d’autres occasions.

— Avant de partir, aide-moi donc à me lever, il me faut me rendre aux latrines. »

Ernaut prit son frère par le bras et l’assista le temps pour lui de s’emmitoufler dans un manteau et de chausser des savates. Puis il laissa Lambert s’éloigner, arrangeant rapidement les couvertures de la couche avant de s’en aller. Il enfilait sa besace lorsqu’il aperçut un homme aux cheveux grisonnants face à lui qui lui faisait un signe de l’index.

Pensant avoir affaire à un vieillard sénile, il ne releva pas et, après un dernier regard alentour pour vérifier qu’il n’oubliait rien, il s’apprêtait à partir. Mais l’ancien insistait et lui faisait désormais de grands gestes avec tout le bras. Comme il n’était qu’à deux lits de là, en outre sur le chemin de la porte, Ernaut décida d’aller voir ce qu’on lui voulait. La curiosité était toujours la plus forte.

« Le bon jour, l’ancien. Qu’y a-t-il pour votre service ?

— Approche-toi un instant, garçon. J’aurais besoin que tu m’aides à me rendre aux bains. »

Vexé d’être ainsi traité comme un vulgaire employé, Ernaut prit un air offensé.

« Je vais mander un des serviteurs de salle. C’est leur rôle normalement, non ? »

Le visage ridé eut un soubresaut lorsque l’homme gloussa, les yeux pétillants de malice.

« Ne te froisse pas ainsi, j’aurai des choses à te conter par la même occasion. Je t’ai entendu. Je connaissais les deux pauvres femmes qui ont été tuées, j’ai prié auprès de leurs dépouilles. Il ne s’agit pas de me baigner, juste de m’accompagner jusqu’à la porte, où ma fille m’espère pour une petite promenade avant mes ablutions. »

Les cheveux argentés épars faisaient une drôle de couronne au crâne pratiquement sphérique du vieil homme. Sa barbe était bien taillée, signe des soins qui lui avaient été apportés. Son visage était recouvert de profondes rides molles car il avait dû beaucoup maigrir récemment. La peau retombait en cascade sur ses joues et dans le cou. Tout en lui indiquait qu’il venait de surmonter une difficile épreuve et qu’il était encore extrêmement faible. Ernaut essaya d’imaginer comment était l’homme avant cela, et crut effectivement se remémorer l’avoir vu en divers lieux saints. Il ne lui semblait pas l’avoir connu de près. D’ailleurs le vieillard ne paraissait pas non plus le laisser entendre. Tandis qu’Ernaut réfléchissait à tout cela, le malade tendit un bras presque décharné, le sourire aux lèvres.

« J’ai pour nom Sanson de Brie, maître huchier[^huchier]. »

Ernaut rendit le salut, poliment, en essayant de ne pas trop se dévoiler, intrigué par les propos.

« Ernaut de Vézelay, pérégrin et colon. »

Sanson fit un clin d’œil, guilleret à l’idée de se promener un peu à l’air libre, malgré le temps à l’évidence maussade.

« Alors, on y va mon garçon ? »

Le jeune homme offrit son bras comme soutien et assista de son mieux le fragile malade, dont la volonté peinait à maintenir le corps en équilibre tandis qu’il avançait en flageolant.

« Je t’ai confié que je les connaissais, car nous avons fait commune visite en plusieurs lieux saints. Nous sommes tous originaires de France et nous sommes assemblés, pour être plus en confiance, tu vois… L’une s’appelait Ylaire, brave femme, dure à la tâche et peu causante, mais pleine de prévenance, et bonne mère avec ça !

— Elle était âgée ?

— Moins que moi, si c’est le sens de ta question. Je ne sais, elle avait peut-être dans les quarante ans, à vue de nez.

— Et l’autre ?

— Phelipote ? Elle était plus jeune, plus jolie aussi. »

Ernaut hocha la tête en assentiment. Il ne s’était donc pas trompé lorsqu’il avait identifié la première femme. Espionnant sa réaction, le vieil homme retroussa les lèvres en un sourire qui dévoilait ses dents usées.

« Elles étaient compagnées par leurs époux ce me semble. Celui de Phelipote avait pour nom Nirart. Il prenait grand soin du gamin d’Ylaire, Oudinnet. Je ne sais plus trop pour l’autre. »

Ernaut s’arrêta de marcher, interloqué.

« Nirart dîtes-vous ? Pensez-vous qu’il soit toujours à l’entour ?

— La dernière fois que je les ai vus remonte à quelque temps. Nous étions allés sur les berges du lac de Galilée, voir la table du Christ, la maison de sainte Marie-Madeleine…

— Et depuis ?

— C’est là que je suis tombé en fièvres, garçon. Nirart me faisait visite de temps à autre, quand il venait s’enquérir d’un sien ami, Amalric.

— Et celui-là, où peut-on l’encontrer ? Il sait peut-être utiles choses pour le vicomte ! »

Sanson osa un timide sourire, vite effacé, devant l’innocence de la remarque.

« Il est désormais auprès du Seigneur, avec tous les Saints, emporté par quelque pourrissement de la jambe.

— Grand désolation que personne ne sache où dénicher la famille de ces pauvresses, qu’elles aient parent pour les défendre… »

Sanson lui serra le bras avec chaleur.

« Libourc, elle, pourra t’en dire davantage.

— Libourc, qui est-ce ?

— Ma fille, que j’espère retrouver dehors justement, grâce à toi. »

Le vieil homme se raidit un peu, l’air plus sévère, mais sa voix chevrotante n’arrivait pas à contenir toute l’autorité qu’il aurait souhaité y mettre.

« Et tiens-toi décemment, je l’ai bien élevée alors je n’accepterai pas qu’un damoiseau se conduise devant elle tel vulgaire valet de ferme. »

Étant donné le grand âge qu’il donnait à Sanson, Ernaut n’avait cure de faire le beau devant une matrone qui pouvait certainement être sa mère. Voire sa grand-mère pensa-t-il après un nouveau coup d’œil au vieillard.

Arrivé à la porte, il ne put donc empêcher sa mâchoire de béer lamentablement lorsqu’il vit s’avancer vers eux une charmante jeune fille aux longs cheveux bruns ramenés en deux élégantes nattes. Son visage n’était peut-être pas le plus éblouissant qu’il eut jamais contemplé, mais elle semblait tellement animée de vie, pleine d’enthousiasme, de gaieté et de grâce que le cœur du colosse fondit instantanément. Elle était habillée très décemment, comme il seyait à une pèlerine, dans de larges robes qui ne laissaient rien percevoir de façon outrancière de sa féminité ; assurément aucun clerc n’aurait pu lui faire reproche de ses choix vestimentaires.

Lorsqu’elle les aperçut, un charmant sourire dessina des fossettes dans ses joues et elle vint à leur rencontre en saluant joyeusement son père de la main. Elle semblait ne pas avoir vu le géant à ses côtés ou du moins n’y prêtait guère attention, au grand dam du jeune homme, qui avait subitement l’impression d’être devenu un verre traversé de lumière. Pour sa part, Sanson s’agitait et arborait des mimiques qu’on réserve d’ordinaire aux bébés et enfants en bas âge. Il embrassa sa fille avec effusion, visiblement touché par l’aura de paix qui émanait de l’adolescente.

« Mon enfant, quelle joie de te revoir ! Le temps m’a paru si long !

— Et à moi donc, père ! Si les sœurs ne s’étaient pas aussi bien occupées de moi, je crois que j’aurais perdu espoir. Heureusement, j’avais nouvelles de votre santé chaque jour grâce aux valets. »

Sanson serra sa fille une nouvelle fois contre son cœur, avec tendresse, puis, réalisant qu’il se comportait de façon très malpolie, il se mit de côté et montra de la main le colosse blond à ses côtés.

« Ma fille, je te présente Ernaut, qui vient comme nous de France. Il a eu bonté de m’assister pour venir jusqu’à toi, acceptant de se voir traiter comme domestique alors qu’il est comme nous, pérégrin en ce lieu. »

La jeune fille se fendit d’une révérence rapide, assortie d’un sourire de circonstance, ses yeux bruns pétillants de malice. Elle semblait enfin intégrer Ernaut dans son univers, qui n’en demandait guère plus pour l’instant. Il se contenta de saluer en retour, la gorge un peu serrée et le cœur battant la chamade. Il avait peur que sa poitrine explose tandis qu’elle laissait son regard dériver sur lui, attendant peut-être qu’il ouvre la bouche pour dire quelque chose. Mais c’était hors des capacités d’Ernaut. Heureusement, Sanson enchaîna assez vite pour éviter qu’un silence gênant ne s’installe.

« Ce jeune homme œuvre en mémoire de ces deux pauvresses assassinées. Je crains fort qu’elles soient de nos connoissances et je suis trop faible pour être utile. Tu sauras peut-être le guider quelque peu. »

Puis, se tournant vers lui, il ajouta :

« J’ai cru entendre que tu étais en contact avec les sergents de ville, c’est ça ? »

Devinant une occasion de se mettre en avant et un sujet qui lui permettrait d’articuler plus de deux sons intelligibles, Ernaut s’éclaircit la voix avant de répondre.

« Certes. J’essaie de me rendre utile. »

La jeune fille le regarda, les yeux écarquillés, franchement admirative.

« Vous faites partie des hommes du roi ?

— Euh, non, pas vraiment. J’ai pour mission d’aider les frères de Saint-Jean et le prieur du Saint-Sépulcre, ainsi que messire vicomte. »

Réalisant qu’il était en train de se dévoiler totalement à des inconnus, en plein passage à l’entrée du bâtiment le plus fréquenté de Jérusalem, il se mordit la lèvre, cherchant comment continuer. Il estima plus prudent, et tout aussi efficace de se mettre en avant par l’évocation de ses aventures précédentes.

« Je suis ami avec un chevalier du roi et j’ai déjà eu à traiter… Comment dire… ? Des affaires délicates ! Outre, en tant que pérégrin, j’ai à cœur que si horribles attaques ne demeurent pas impunies. Alors je vais ici et là, tentant de voir si mes maigres talents peuvent servir. »

Tout en parlant, Ernaut bombait le torse et se présentait à son avantage, appuyant parfois une syllabe ou l’autre en un phrasé un peu affecté. Il savait que Régnier d’Eaucourt n’avait rien à voir avec l’histoire, mais pouvoir s’associer à un chevalier lui semblait opportun en l’occurrence. Sanson approuvait de la tête, apparemment enthousiasmé.

« Je me suis dit que tu pourrais peut-être lui indiquer l’endroit où encontrer Oudinnet ou Nirart. Je ne sais s’ils se sont manifestés auprès des sergents, mais au loin de chez eux, ils sauront apprécier toute aide encontrée, surtout de la part d’un Français. »

Libourc hocha gravement la tête, le sourire s’effaçant de son visage pour laisser apparaître un air franchement inquiet qu’Ernaut trouvait encore plus touchant.

« Hélas, père, je loge chez les sœurs depuis si longtemps que je ne les ai pas vus. Je ne savais même pas qu’ils étaient revenus à Jérusalem. S’ils sont là, ils doivent certainement loger à l’Asnerie, à la porte Saint-Étienne, ainsi qu’ils le faisaient habituellement. Je crois que le groupe avait prévu d’aller à Bethléem car beaucoup ne l’avaient pas encore vu. Certains, récemment arrivés, ne connaissaient pas le sépulcre de saint Lazare, à Béthanie, ni celui de la Vierge, vers le levant de la ville…. »

Elle se tut un instant, faisant rouler ses yeux dans toutes les directions tandis qu’elle fouillait dans sa mémoire.

« Ils sont en général avec le père Ligier, il devrait être aisé de le dénicher. »

À l’évocation du nom, Sanson sourit pour lui-même.

« Ah oui ! Mon garçon, il t’en coûtera guère de peine à les retrouver. Le prêtre qui les accompagne souffre d’un embonpoint conséquent et arbore une impressionnante barbe blonde. Outre, il a une voix à l’avenant de son physique, très impressionnante. »

Ernaut hocha gravement la tête. Il n’avait guère idée de ce dans quoi il se lançait, et avait totalement oublié les exhortations de son frère. Loin de son esprit était également sa rencontre avec le prieur Amaury ou sa désignation officielle par les autorités de la ville comme interlocuteur. Désormais, il faisait tout cela simplement dans l’espoir de retrouver au plus vite les deux globes brillants qui le regardaient avec une pointe d’admiration.

### Milieu d’après-midi du mardi 26 mars 1157

La pierre projetée par le coup de pied roula dans un nuage de poussière, bientôt traversé par le marcheur empressé. Le bâton d’Ernaut frappait le sol de façon régulière, son mouvement de balancier l’aidant à garder un rythme soutenu. Tout en avançant, il maugréait dans sa barbe, mécontent d’avoir perdu son temps. Il était allé se renseigner sur les pèlerins du groupe de Nirart, mais les informations glanées étaient vagues, à savoir qu’ils comptaient aller sur les pas du père de saint Jean-Baptiste, Zacharie.

Il lui avait alors fallu choisir entre la maison et la tombe du saint homme. Par prudence, il avait préféré commencer par l’hypothèse la plus éloignée, donc la demeure, qui se trouvait à Encharim, à l’ouest de Jérusalem[^encharim], où la Vierge était venue visiter sa cousine Élisabeth. La sépulture n’étant guère distante de Jérusalem, sur le mont des Oliviers à l’est, il s’était dit que si les fidèles s’y étaient rendus, ils seraient de retour quoiqu’il arrive le soir même. Et bien sûr, son choix n’avait pas été couronné de succès.

Il avait bien retrouvé quelques voyageurs qui connaissaient le père Ligier et Nirart, mais eux-mêmes n’étaient pas là. Il rebroussait donc chemin, quittant l’agréable vallée de bien méchante humeur. Il ne prêtait aucune attention aux lacets qui s’enroulaient autour des légers reliefs, abrités par quelques sycomores qui bruissaient dans le vent ténu. Il s’arrêta néanmoins afin de ramasser quelques fruits, car il n’aurait guère le temps de manger avant le soir. Il ne s’était muni que d’une gourde et d’un peu de pain, et la faim le tenaillait. Les rissoles de Margue étaient bien loin dans son souvenir et plus encore dans son estomac.

Il finit par s’asseoir quelques instants, profitant d’un rocher accueillant. Malgré des températures plutôt fraîches et quelques nuages flottant dans un ciel dégagé, il était en sueur et s’essuya le front de sa manche. Le paysage était magnifique et, au loin, il apercevait un groupe de jeunes qui menaient un troupeau de chèvres. Quelques cultivateurs étaient affairés à tailler des oliviers dans une parcelle en terrasse, face à lui, de l’autre côté de la vallée. Les chants de travail parvenaient parfois jusqu’à ses oreilles, selon le sens du vent.

Un lézard s’était aventuré hors de sa cachette sur le muret qui délimitait le bord opposé du chemin et semblait fixer Ernaut de son œil immobile, profitant de la chiche chaleur du soleil. Tandis qu’il grattait les minuscules figues avant de les avaler, faisant tomber les parties gâtées par les insectes, il vit approcher un paysan et son âne qui revenaient de Jérusalem. L’animal était lourdement chargé de sacs et de sacoches. D’un bâton noueux, l’homme frappait régulièrement le derrière de sa bête histoire de la maintenir à un bon rythme. Il était voûté, presque bossu, protégé d’un simple *thawb*[^thawb] de laine et d’un long vêtement fendu sur le devant, retenu par une large ceinture d’étoffe. Son épaisse tignasse frisée, blanche et noire était surmontée d’un vieux chapeau de feutre, qui avait connu meilleure forme. Avisant Ernaut il fit un salut de la main, souriant poliment sans interrompre sa marche.

Le jeune homme le regarda passer et le suivit de la tête tandis qu’il s’éloignait, jusqu’à ce qu’il aperçut un petit groupe de voyageurs qui revenait d’Encharim. C’étaient certainement les pèlerins qui rentraient également à Jérusalem. Finalement, il se dit que ce ne serait peut-être pas une mauvaise idée de les attendre. Dans sa déception, il n’avait pas pris le temps de discuter longuement avec eux quand il avait visité l’église. Comprenant que ceux qu’il recherchait n’étaient pas là, il avait voulu se dépêcher le plus possible, mais était tout de même allé prier pour lui et son frère auprès de la grotte qui avait vu naître le cousin du Sauveur. Quitte à avoir fait le trajet jusque-là, autant que ça n’ait pas été en pure perte, estimait-il.

Conscient que le soleil était déjà bien avancé dans sa course, Ernaut atermoyait, hésitant à repartir seul, rapidement car un peu inquiet d’arriver après la tombée de la nuit et donc de trouver les portes de la ville closes. Quant à visiter l’Asnerie, il pourrait s’y rendre au plus tôt le lendemain matin, dès l’aurore, afin d’être sûr de ne pas rater une nouvelle fois Nirart et le garçonnet. D’un autre côté, les quelques pèlerins qui le rejoignaient marchaient d’un bon pas et pourraient peut-être lui fournir quelques renseignements utiles tandis qu’ils chemineraient. Ils devaient savoir le temps qu’il leur faudrait pour retrouver Jérusalem : ils lui avaient confié qu’ils souhaitaient justement passer la nuit en ville, continuant leur périple sur les pas de saint Jean-Baptiste, en se recueillant à l’église qui lui était dédiée dans le quartier de l’hôpital.

Très vite, leurs chants parvinrent jusqu’à lui, des psaumes scandés avec ferveur et une relative exactitude. Un des hommes âgés, avec qui Ernaut avait discuté, l’aperçut et lui fit un signe de la main, ayant compris que leur petit groupe allait accueillir un membre de plus. Ils stoppèrent tous autour de lui et certains l’imitèrent en ce qui concernait la cueillette de fruits.

« Alors jeune homme, tu n’as pas désir de cheminer seul au final ? Nous t’avons vu partir comme balle de fronde, nous ne pensions pas te revoir de sitôt.

— Disons qu’il m’est plus agréable de marcher en bonne compagnie.

— Sans compter les brigands qu’on dit fort nombreux par ce pays, crut bon d’indiquer une femme.

— Penses-tu ! Aucun risque si près de la ville, répliqua l’homme. Les Sarrasins n’osent plus venir par ici depuis des années. Et les patrouilles des chevaliers suffisent à calmer les ardeurs des détrousseurs. »

Un troisième pèlerin, tout en ouvrant un fruit qu’il allait avaler, lança un regard amusé vers Ernaut.

« D’autant qu’il faudrait être un peu fol pour vouloir dépouiller pareil géant qui ferait peur à Goliath lui-même !

— Je ne suis pas sûr que voilà chose à dire si près de la cité de David, tu sais ! » Rétorqua l’homme âgé qui semblait être le meneur.

Il se tourna de nouveau vers Ernaut et tendit une main calleuse.

« Quelle que soit ta raison, sois le bienvenu en notre petit groupe. Plus tôt, je ne m’étais pas présenté, on me nomme Marcel l’Espéron. »

À tour de rôle, ses compagnons indiquèrent leur nom, ne laissant que peu de chance à Ernaut de se souvenir de la façon dont chacun s’appelait. Il sourit néanmoins à la cantonade et se présenta à son tour. Avant qu’il n’ait le temps de trop s’étendre, Marcel le coupa en lui pressant le bras d’une main ferme.

« Le pardon, mais il serait plus prudent de continuer à deviser en marchant, si nous ne voulons pas demeurer dans les faubourgs à la nuit. »

En moins de temps qu’il ne fallait pour le dire, chacun avait enfilé sa besace, empoigné son bâton et reprit la route. Chemin faisant, les pèlerins alternaient des chants religieux et des moments de silence, en particulier lorsqu’ils avaient une côte à monter. Ils avançaient malgré tout à bonne vitesse, leurs jambes s’étant fortifiées des récents voyages qu’ils avaient faits. Ernaut profita d’un ralentissement pour se rapprocher de Marcel, qui était en train d’avaler quelques gorgées d’eau de sa calebasse. S’essuyant la bouche du revers de la main, il en proposa d’un geste au colosse qui refusa d’un signe de tête.

« Dîtes-moi, Marcel, vous aviez bonne connoissance des deux malheureuses assassinées ? »

Le vieil homme lui lança un regard en coin, intrigué ou inquiet de voir le sujet abordé. Un index nerveux vint gratter sa joue gagnée par la barbe.

« À dire le vrai, non. La plupart d’entre nous sommes arrivés par nos propres moyens. Ce n’est qu’à force de se croiser ici et là, ou de cheminer parfois de concert, que nous avons appris à nous connaître. Pourquoi poses-tu la question ?

— Sans raison particulière. Des amis à eux sont sans nouvelle et cherchent à savoir comment vont Nirart et Oudinnet. Je pourrais peut-être les assister dans ce moment difficile. D’ailleurs, en fait, je suis venu céans dans l’espoir de les trouver avec vous.

— J’étais un peu surpris de voir pérégrin esseulé, je comprends mieux. Je suis désolé de ne pas pouvoir t’en dire plus. Ça doit faire une poignée de semaines qu’on se voit en Terre sainte. C’est d’abord Nirart et sa femme que j’ai encontrés pour ma part. Lui était un peu fatigué, alité à côté d’un mien ami. On a pris usage de donner de ses nouvelles à son épouse. Ils étaient français, comme nous, alors forcément. Comme on était parti depuis moins longtemps qu’eux, on a pu leur donner quelques nouvelles du pays.

— Ils sont là depuis quand ?

— Je ne saurais dire. Mais ils ont été bien éprouvés, surtout le garçonnet, qui ne parle guère désormais. Ils ont été captifs des sarrasins pendant de longs mois et allaient finir comme esclaves si les frères de l’hôpital ne les avaient pas rachetés. »

Interloqué, Ernaut marqua un temps, manquant de se faire percuter par les voyageurs qui le suivaient.

« Captifs ? Tout leur groupe ?

— Je ne sais exactement qui. Ylaire et Phelipote l’étaient itou, Dieu ait leur âme, ainsi que Nirart et le garçon. Certes ils étaient bien plus à avoir recouvré la liberté, les frères en ont ramené moult… Ils n’en ont jamais parlé directement, seulement quelques allusions entre eux de temps à autre, tu comprends.

— Cela voudrait dire qu’ils étaient tenus en fers depuis long moment, vu que le roi a signé une trêve depuis… »

Ernaut se creusa les méninges, il n’avait guère prêté attention à ce genre d’information. Il savait juste que la trêve avec Damas était censée toujours s’appliquer, malgré l’attaque récente du roi Baudoin sur les terres aux alentours de Panéas.

« Ils auraient alors été engeôlés plusieurs années durant. Quelle horreur ! Marcel prenait un air de plus en plus contrarié au fur et à mesure de la conversation et sa bouche arborait désormais un rictus amer.

« Certainement traités comme des esclaves. Je n’ose imaginer le sort de ces pauvres femmes. Maintenant qu’elles étaient libres, un fol les a meurtries. Elles ne méritaient vraiment pas si terrible sort ! J’espère que Nirart saura venger son épouse.

— Il faudrait qu’il se présente à la Cour s’il veut se faire entendre. Peut-être a-t-il une idée de qui a fait le coup.

— Hmm. Je ne serais pas étonné qu’un de ces maudits sarrasins soit venu jusque dans la Cité sainte pour quelque obscure vengeance. En tout cas, si c’est bien ça, Nirart saura trouver des soutiens pour jurer à ses côtés, je peux te l’assurer. »

### Début de la nuit du mardi 26 mars 1157

La muraille, habituellement couleur paille, avait pris une teinte orangée avec le soleil couchant. Quelques personnes s’activaient encore autour de la porte de David, sur le chemin de l’ouest qui partait vers Jaffa et la mer. Le faubourg qui s’y était développé bruissait de l’animation des quelques voyageurs qui allaient demeurer la nuit. Une petite troupe de cavaliers en armes attendait apparemment qu’on vienne s’occuper de leurs montures. Ils arboraient des couleurs vives sur leurs écus et la bannière de l’un d’entre eux, vraisemblablement un chevalier et ses hommes, en mission.

Lorsque les pèlerins du groupe arrivèrent dans le hameau, ils se hâtèrent de passer outre des valets qui déchargeaient des bûches pour les femmes lépreuses qui vivaient là, dans un couvent aux abords de la Ville. Ils ne ralentirent qu’auprès de la muraille, s’approchant de la monumentale porte de bois ferré. L’entrée était tenue par deux hommes casqués, l’un d’eux la lance à la main et l’autre, plus râblé, l’épée au côté. Ce dernier devisait gaiement avec un troisième, à la mise assez opulente, certainement un riche marchand local.

Derrière, on voyait les fonctionnaires des douanes en train de ranger leurs écritoires, coffrets et sièges pour la nuit, plus aucune denrée ne pouvant désormais pénétrer avant que le jour se lève le lendemain. Lorsqu’Ernaut et ses compagnons arrivèrent finalement près des gardes, ils étaient de nouveau seuls et se tenaient au milieu du chemin, devant la voûte qui traversait l’enceinte. Celui qui arborait l’épée était un peu en avant, les mains sur les hanches, attendant visiblement qu’on lui adresse la parole. Ce fut Marcel qui s’avança, saluant d’un geste de la paume.

« Le bon soir à toi, sergent. Nous sommes marcheurs de Dieu et revenons de la demeure de Zacharie, sur les traces de saint Jean-Baptiste, le cousin du Sauveur. Nous souhaiterions entrer en la sainte cité, si tu le permets. »

Le soldat fit une moue approbatrice puis sourit sans chaleur. Il détailla rapidement les tenues, s’attachant aux insignes de pèlerinages, bourdons et besaces, croix arborée sur l’épaule, vêtements et souliers fatigués.

« Vous arrivez juste à temps. On allait clore pour la nuit. Vous savez où vous rendre dans nos murs ?

— Je te mercie, nous sommes déjà venus et, pour certains, cela fait plusieurs semaines que nous demeurons ici et à l’entour. Nous avons donc usage. »

Le garde fit un signe du pouce, indiquant l’intérieur de la ville.

« Dans ce cas, vous pourrez entrer. Patientez juste un instant que les officiers de la douane aient fini de rentrer leurs affaires. »

Il s’avança un peu plus, se grattant le cou et inclinant la tête. Il avait visiblement envie de discuter un peu. Celui à la lance regardait d’un air absent, ses yeux hagards figés au loin. Il ne semblait pas un compagnon très expansif.

« Vous avez vu la grotte ? »

Marcel opina du chef.

« Nous y avons passé la nuitée.

— Fort bel endroit, cette vallée. Vous avez bu à la fontaine de la Vierge ?

— Oui, bien sûr. Les frères nous en ont parlé. Nous avons également passé long moment à l’endroit où la Vierge a visité sa sœur. Le prêtre nous a tout bien expliqué. C’étaient vraiment deux jours magnifiques.

— Vous restez jusqu’à quand ?

— Cela dépend. Moi j’aimerais bien rester jusqu’après Pentecôte, mais ça va être malaisé si je veux être de retour chez moi pour l’été. Je sais que d’aucuns vont repartir après la fête de la Vierge, peu avant que les mers ne soient de nouveau fermées[^cloturemer]. »

Le sergent d’armes acquiesçait silencieusement, soucieux d’entretenir la conversation bien que n’ayant pas grand-chose à dire. Il avait dû s’ennuyer ferme une partie de la journée et Marcel l’avait bien compris, mais il ne voyait pas ce qu’il pouvait ajouter de plus. Ernaut se permit donc d’intervenir dans la discussion.

« Vous savez s’il y a novelté sur la meurtrerie des deux femmes ? »

Le garde se tourna, plissant des yeux et regardant le jeune homme comme s’il estimait une bête qu’il envisageait d’acheter.

« Du neuf ? Pas que je sache. Il faut dire que je n’ai guère bougé de là depuis l’aube. Peut-être que demain, à la Cour, ils en parleront, si la famille se présente. »

Le soldat à la lance trouva d’un coup un semblant de vie et lança, d’une voix pâteuse :

« Moi, on m’a dit que ce seraient des Germains qui auraient fait le coup. »

Les têtes obliquèrent toutes vers lui et son compagnon souffla, les yeux au ciel.

« Je crois surtout que tu devrais pourpenser à ce que tu répètes, Basequin.

— Bein quoi ? Ça ne m’étonnerait pas. On les dit sournois.

— Réfléchis, face de Carême. Même si c’était vrai, c’est pas à nous de colporter ces fables qui risquent de remuer les sangs de tout le monde à l’approche des fêtes.

— M’ouais, en tout cas, le beau-père il dit que…

— Ton ancien, il ferait mieux d’arrêter de chercher pensées au cul de ses gobelets de vin surtout ! » Éructa son compagnon.

Le sergent à la lance prit un air buté, marmonnant pour lui-même :

« Il a peut-être grand goût pour le vin, mais lui au moins, il ne s’abouche avec des traîtres qui nous ont abandonnés[^croisade2b]. »

L’autre souffla, car la remarque avait manifestement fait mouche. Il pinça les lèvres et réfléchissait visiblement à la manière de punir cet affront. Marcel crut bon de s’immiscer, tendant la main vers le bâtiment des douanes.

« Je crois qu’ils ont fini. Peut-être pouvons-nous rejoindre nos compagnons à l’intérieur.

— C’est ça, avancez, tonna le garde à l’épée, d’une voix contrariée, tout en faisant un geste agacé de la main. Puis, se radoucissant, il ajouta : j’espère que vous passerez bonnes fêtes ici. »

Le petit groupe se dispersa peu après avoir franchi l’enceinte. Ernaut salua ses compagnons puis entreprit de se rendre à la Citerne du patriarche, en passant aux abords du Marché aux Grains, juste au nord de l’endroit où il se trouvait. Il avait envie de patrouiller un peu dans les environs, dans l’espoir peut-être d’y apercevoir le meurtrier qui sévissait.

Son imagination le poussait à se voir en sauveur de la jolie Libourc, qu’il délivrerait des griffes d’un sordide agresseur avant qu’elle ne lui tombe dans les bras, séduite par tant de bravoure. Mais la réalité fut moins exaltante. Il croisa quelques poivrots pas loin du marché aux Cochons. Bien qu’il n’y ait plus d’animaux pour la nuit et fort peu en journée en cette période de Carême, leur puanteur emplissait toujours l’air, aucune brise ne venant apporter la moindre fraîcheur à ce qui constituait une des zones les plus malodorantes du quartier du Patriarche.

Un jeune damoiseau, richement vêtu, était encadré de quelques domestiques, en route pour une agréable veillée parmi des personnes de son rang. Il passa sans un regard pour ceux qui l’entouraient, marchant délicatement dans la boue qui recouvrait le sol, résultat du piétinement par les bêtes et les hommes. Arrivé près de la citerne, le ciel s’était vraiment obscurci et il devenait difficile d’y voir dans les ruelles environnantes. Profitant d’une ouverture dans les bâtiments qui permettait l’accès au bassin, Ernaut s’assit sur un escalier aux abords de l’eau dans laquelle il aurait volontiers plongé pour se débarrasser de la sueur et de la poussière du voyage de la journée.

Il se sentait las, il s’ennuyait. S’allongeant sur le dos à même les marches, les bras croisés sous la tête, il regardait le ciel. Les derniers feux du soleil soulignaient d’un trait rouge les lambeaux de nuages qui s’effilochaient loin au-dessus de lui. Quelques oiseaux voletaient de façon désordonnée, pourchassant des insectes malchanceux.

Sans s’en rendre compte, il commençait à s’assoupir lorsque le bruit d’un cheval et d’hommes d’armes le dérangea. Il tourna la tête et observa une partie du guet, menée par le vicomte, qui descendait en direction de la rue de David. Ils venaient des abords du Saint-Sépulcre, sûrement du palais royal. Il se releva, s’épousseta d’une main nonchalante et reprit son chemin. Il n’avait guère envie d’aller les voir pour partager ses découvertes, il n’aspirait qu’à plonger dans son lit.

Malgré l’obscurité, il décida de rejoindre sa demeure par le quartier de l’hôpital, passant par les ténébreuses venelles qui serpentaient jusqu’au change des Hospitaliers. De là, il retrouverait rapidement la rue de l’Arche de Judas où il résidait, à une portée d’arbalète. Il avançait lentement, la main contre la paroi, et trébucha à de nombreuses reprises sur le sol irrégulier et encombré, la dernière fois sur un rat qui détala en couinant.

Débouchant sur la place au centre des hôpitaux, il vit que quelques acharnés s’y trouvaient toujours, les plus pieux chantant à la gloire de Dieu et les plus dissipés faisant rouler les dés ou poussant le pion sur un plateau. Il salua rapidement ceux qu’il reconnaissait, mais ne s’arrêta pas. La soirée s’annonçait calme. Peut-être n’y aurait-il rien à déplorer cette fois-ci, chacun se tenant sur ses gardes. La noirceur de la venelle qui permettait de quitter la cour parut soudainement inquiétante à Ernaut.

Afin de se rassurer, il empoigna le solide couteau offert par les marins du *Falconus*[^voirt1]. Un ivrogne urinant maladroitement au milieu du chemin fut le seul danger qu’il rencontra là. Lorsqu’Ernaut se montra dans le couloir d’accès à l’escalier menant à son logement, un garçonnet était en train de jouer avec des figurines de terre cuite. L’enfant eut un fugace moment d’inquiétude face au géant, mais, privilège de l’âge, il chassa bien vite ses idées noires et sourit au nouvel arrivant. Derrière lui, ses frères et sœurs s’amusaient à la lueur de quelques lampes dans le petit jardin d’agrément aménagé entre les hauts murs du bâtiment. Le jeune homme lui fit un rapide signe de la main et entama la montée quatre à quatre. Les propriétaires s’étaient réservé la jouissance d’une bonne moitié de la bâtisse et de la zone découverte au rez-de-chaussée.

Un escalier desservi par un couloir menant à une ruelle permettait aux différents locataires d’accéder à leur habitation sans déranger la maisonnée. Le premier étage était occupé par trois frères qui travaillaient dans la charpente. Ernaut les croisait souvent et avait même accepté une de leur invitation à boire un soir récent. Ils étaient originaires de Lorraine et participaient aux chantiers de forteresses et d’églises qui s’ouvraient un peu partout dans le royaume. Ils étaient très fiers d’expliquer qu’ils avaient pris part à la construction du beffroi du Saint-Sépulcre.

Au-dessus d’eux résidait une famille discrète, presque effacée, habillée à l’orientale : un jeune couple et deux enfants en bas âge, qui s’enfermaient dans leur logement et ne faisaient aucun bruit. Enfin, au dernier étage, sur le toit-terrasse, se trouvaient deux petites cabanes dont l’une était occupée par Ernaut et l’autre par un vieux syrien au visage ridé, Saïd, qui n’avait apparemment plus toute sa tête.

Il rendait de nombreux services, allait puiser l’eau, chercher du bois, sortait les ordures ou balayait les abords. La pièce qu’il habitait était emplie d’un bric-à-brac indescriptible qu’il tenait à faire visiter à tout nouvel arrivant comme s’il s’agissait d’une attraction importante. Il était néanmoins d’agréable compagnie et parlait suffisamment bien français pour discuter le temps d’une veillée ou prêter assistance comme guide à l’occasion. En outre, il était incroyablement doué pour jouer aux mérelles[^merelles].

Ce soir-là, lorsqu’Ernaut déboucha sur la plate-forme, elle était déserte, en dehors de quelques pigeons à peine effrayés par son arrivée. La porte fermée face à lui indiquait que Saïd était absent. Après avoir fouillé dans sa besace pour en extraire sa clé, le jeune homme déverrouilla le local où il résidait, et poussa le vantail du pied. À peine entré il se laissa glisser à terre, face à l’ouverture. Il était las et avait grande hâte que ses devoirs religieux soient accomplis. Il ne se sentait guère en phase avec la plupart des dévots qu’il côtoyait tous les jours. Il était plutôt empli de curiosité pour ce nouveau monde, et aspirait à pouvoir le parcourir tout son soûl. Il espérait également ne pas s’être fourvoyé en acceptant la mission pour le Saint-Sépulcre, et les frères de Saint-Jean.

Tandis qu’il réfléchissait à tout cela, quelques moineaux vinrent se poser sur le muret qui entourait la plateforme. Sautillant bravement jusqu’auprès de lui, ils étaient en quête de nourriture. La journée s’achevait et, dans un dernier sursaut, la lumière rougeoyante du soleil embrasait littéralement le dôme du Temple et les reliefs à l’est. Le ciel était dégagé et la nuit s’annonçait fraîche. Attrapant la lampe à huile sur le coffre à l’entrée, Ernaut se décida à aller chercher du feu avant que les ténèbres ne s’installent tout à fait.

Lorsqu’il revint sur la terrasse, Saïd était en train de réchauffer sur un petit foyer un pot de pois chiches agrémentés de légumes. Heureux d’accueillir son voisin, il se fit un devoir de partager avec lui son maigre repas. Ernaut ajouta de sa réserve quelques dattes séchées et du pain qui lui restaient afin d’améliorer leur ordinaire, le tout arrosé d’un fond de pichet de vin. Assis sur sa natte, il avalait les bouchées les unes après les autres en silence, admirant l’apparition des étoiles à peine éclipsées par la lune à son dernier quartier.

Des bruits de fête parvenaient des environs, ainsi que quelques senteurs épicées, évadées d’une arrière-cuisine. Une douce quiétude semblait régner. Cette supposée tranquillité allait-elle une nouvelle fois dissimuler un horrible meurtre ? Sans vraiment se l’expliquer, Ernaut en était persuadé, mais il n’avait aucune idée d’où et quand l’assassin frapperait. Plus inquiétant encore, il ignorait totalement qui succomberait. Tandis qu’il ruminait ces sombres pensées, il ne put s’empêcher de souhaiter que Libourc ne fût pas la prochaine. Ce soir-là, avant de s’endormir, il fit une des prières les plus sincères qu’il eut faites depuis longtemps.
