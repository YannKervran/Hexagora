# Agui l'homme

## Chanson populaire politique

Fils à diable plus fort qu'ourson,  
Il était fort bel enfançon.  
Poil soufré bien dru sur son chief,  
De sa mère tira bon lait  
Qui le fit haut et bien épais  
Mais de son père n'eut point fief.

[Refrain]
D'un pet repoussait les saxons  
Rotait flammèches sur bataillons,  
Les seigneurs n'en voulurent baron  
Mais devint roi à sa façon

Belle cousine au frais minois,  
Fit sa reine du pays anglois.  
Nenni fit l'évêque de Rome.  
La loi canon ne le tolère,  
Quasi frères furent vos pères.  
Sauve ton âme, refuse cette pomme !

Deux moustiers furent rançon de paix,  
Les Chefs Clefs eurent gage de respect.  
Alors pour faire bonne mesure pleine,  
À cadet donna crosse et mitre  
Qui maçonna nefs pour chapitre,  
Puis batailla à perdre haleine.

Envers fol et honni félon  
Parjure, qui lâcha gonfalons  
Porta ses féaux à pleins nefs  
Grand férit, mit envers piétaille  
Tourna en fuite la chienaille.  
Et lui perça d'un trait le chef. ❧

**Rq :** Agui signifie savant, fin, rusé.

### Notes

Au contraire des « Traits du fier archer », cette chanson a un sens politique qui pouvait flatter les admirateurs de Guillaume qui, de bâtard, devint Conquérant, en ajoutant la couronne d'Angleterre à son duché normand. Le nom du héros ni d'aucun protagoniste n'est cité, mais les Normands du XIIe siècle auraient eut tôt fait de combler les sous-entendus, dont le premier, le titre, n'est que trop évident. La première strophe évoque le fait qu'il était fils du duc de Normandie, qu'on a un temps assimilé à Robert le Diable, personnage légendaire.

Mais comme sa mère n'en était pas l'épouse, il a dû, très jeune, faire face à une rébellion de ses vassaux jusqu'à ce que son pouvoir puisse s'affirmer, après la victoire de Val-ès-Dunes à laquelle le roi de France Henri Ier avait largement contribué.. Les second et troisième couplets parlent du mariage de Guillaume avec Mathilde de Flandre, avec laquelle la consanguinité était trop forte selon le Pape. Guillaume passa outre, mais dut, en expiation et après quelques années, faire édifier deux abbayes dans la ville de Caen, une pour les hommes, et une pour les femmes. En outre, il avait fait nommer évêque de Bayeux son demi-frère Odon, son cadet issu du mariage de sa mère après le décès de son père. Bien que prélat, celui-ci l'assista dans toutes ses opérations de conquête et passa certainement commande de la fameuse Tapisserie qu'on peut encore admirer à Bayeux. Il reçut d'ailleurs de nombreuses terres en récompense, bien qu'il ait connu une disgrâce à la fin du règne de Guillaume, sans qu'aucune hypothèse ne puisse vraiment en donner la raison avec certitude. Enfin, la dernière strophe évoque la conquête de l'Angleterre, que Guillaume appuyait, entre autres, sur le parjure de Harold qui avait prêté serment sur des reliques lorsqu'il avait été l'hôte du duc normand quelques années plus tôt. À la bataille de Hastings, Guillaume avait mené une armée de cavaliers, à l'aide d'une immense flotte, et il s'opposa à l'infanterie de Harold. Ce dernier fut défait, et tué dans la bataille. Sur la tapisserie, il est représenté avec une flèche dans le visage, sans qu'on ne sache s'il faut entendre cela au sens propre ou au figuré (démontrant l'aveuglement du Saxon).

Comme pour la chanson précédente, il ne s'agit nullement d'un texte médiéval mais d'une évocation originale, de mon cru, transposée pour être compréhensible de nos jours tout en conservant une saveur issue des formulations anciennes et des termes désuets. Si vous souhaitez découvrir le véritable univers sonore du Moyen Âge, je vous invite à commencer le périple par l'ouvrage dirigé par Françoise Ferrand (ci-dessous) ou par une visite sur le site d'Apemutam (http://www.apemutam.org/), association pour l'étude de la musique et des techniques dans l'art médiéval.

### Références

Pierre Bauduin, *La première Normandie (X^e–XI^e siècles)*, Caen, Presses Universitaires de Caen, 2004.

François Neveux, *La Normandie des ducs aux rois*, Rennes, Ouest France, 1998.

Françoise Ferrand (dir.), *Guide de la musique du Moyen Âge*, Paris, Éditions Fayard, 1999.
