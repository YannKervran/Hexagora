#Le plus doux des mestiers

## Chanson populaire

L'enfançon âgé bien assez  
Son père choisit de le placer.  
On en fera riche musnier  
De l'avoir emplir son grenier.  
Panse rebondie comme tambour  
Sans bien fort suer à son labour.  
Je ne puis, père, élire mestier  
Où meule bestourne sans pitié  
Nul désir de finir grevé  
Mon sang vermeil en le pavé.

Alors boucher fait bon avoir  
D'un fort couteau peut se pourvoir.  
Chaque journée droit de carnage  
Les hauts morceaux en apanage.  
Je ne puis, père, élire mestier  
Où coutiaux toujours affutiés  
Espèrent trancher doigts égarés.  
Belle peur aurais de me navrer.  

Laboureur fait riches moissons.  
De s'engraisser a mille façon  
En sien grenier froment rangé  
Permet au soir fort goberger.  
Je ne puis, père, élire mestier  
Où boeufs puissants et bien cornés  
Peuvent en tout moment m'esbigner  
Finir gisant après une ruée.  

Charpentier tranchant ses tenons  
Construit chaque jour son renom.  
Bâtissant hostel et toitures  
Tire deniers de ses emboîtures.  
Je ne puis, père, élire mestier  
Où bien haut perché doit œuvrer  
Grande crainte du vent tempêtueux  
Car me laissant pauvre et boiteux.  

Au moustier pourra faire profès  
De la dîme tirer les bienfaits  
Ventru chaud vêtu toute l'année  
En prière pour mie se damner.  
Je ne puis, père, élire mestier  
Où mon pendu doit se garder  
D'aller faire visite en buissons  
Ne pouvant créer enfançon.  

D'un tel fils oiseux et rétif  
Du moindre labeur si craintif  
Las, le père ne sut que faire  
Fils à la tonsure te confère  
En village tu seras curé  
Le fils ne put rien opposer. ❧

### Notes

J'ai une nouvelle fois composé ce texte en m'inspirant des devinettes anciennes, et prenant comme sujet la moquerie incessante des rares textes populaires connus à propos des prêtres, dont l'éventail de défauts est si large qu'il en devient truculent.

### Références

Bruno Roy, *Devinettes françaises du Moyen Âge* (Cahiers d'Études Médiévales, 3), Montréal, Bellarmin, et Paris, Vrin, 1977.

Une version en ligne existe à <http://www.sites.univ-rennes2.fr/celam/cetm/devinettes/devinettes.html>\[Consulté le 15 décembre 2014\].
