# Le carillon à cul batu

## Chanson populaire à boire

Dormir après souper et faire lardon  
Engloter poulets, canards et chapons  
Gaigner panse d'abbé, mangier outre raison  
Faire donner souper par sergents larrons  
N'y gaigne guère que bien avides sochons[^sochon]  
Bien mieux partir fustaille  

Entrebattre pendu contre fendu  
Couvrir a croupetons, poilu a velu  
Couchant marmouset en creuset connu  
Donne certes moment de joie mais fort ténu  
Apporte las, enfançon ja repu  
Plûtot percer fustaille  

Quérir honneur, pouvoir de haut baron  
L'épée en main, porter plaies et horions  
Chasser païens, mettre fers à larron  
En cort assemblée requérir raison  
Semer le bien, cueillir fiel pour moisson  
Mieux servi par fustaille  

En après fort besoigne, ai désir grand  
Encontrer compères pour goûter simplement  
Doux flots vermillons frais et gouleyant  
D'un godet à l'autre s'entrechoquant  
Se faire devoir de cognebas bellement  
J'ai nom père la fustaille  

Le plus amiable des messagiers féaux  
Confère à la vie des atours royaux  
Contre poignée de mailles. ❧

### Notes

Le carillon est un autre terme désignant la quarte, une mesure de contenance mais aussi pour désigner un parchemin plié en quatre.
