# Le fils à fesse

## Chanson populaire

Bienheureux enfançon, engendré tout chantant  
Joyeux jusqu'à sa fin, à sa mort va claironnant.  
Né fringant d'une fesse, on le baptisa vesse  
On lui prédit grand ouvrage et belles prouesses  
En cour pouvait servir bellement, loyalement  
Tel l'huissier annonçant avec talent son mestre  
Se fait fière trompette, sonnant dextre et senestre  
N'a pas égal parmi archers Turcs du Levant  
Capable de prendre au talon sa visée  
Et de férir au nez en un coup ajusté  
Malgré nombreux talents, est bien peu en grâce  
D'un geste renfrogné vigoureux on le chasse  
Il fait froncer sourcils, ahonter son parent  
S'échappe alors en un timide chuintement  
Sans se donner aucune chance de fort sonner  
Car las, en nous quittant, nous laisse triste fumet. ❧

### Notes

Cette chanson très grivoise est l'amalgame de plusieurs devinettes et blagues médiévales, dont le pet constituait un des sujets favoris, comme toutes les déjections diverses que les organismes peuvent produire. De tout temps, cela a constitué un fond inusable de motifs, répétés, repris, modifiés, dans la culture populaire : blagues, devinettes et jeux. Bien évidemment nous sommes là à mille lieux de la culture savante écrite et il m'a fallu inventer pour combler de façon vraisemblable les manques issus de la documentation partielle (et partiale) des clercs érudits. Ce ne sera qu'avec Rabelais que les deux mondes en viendront à cohabiter pour la postérité.

### Références

Allen Valerie, *On Farting. Language and laughter in the Middle Ages*, New York : Palgrave MacMillan, 2007.

Bruno Roy, *Devinettes françaises du Moyen Âge* (Cahiers d'Études Médiévales, 3), Montréal : Bellarmin, et Paris : Vrin, 1977. Une version en ligne existe à : <http://www.sites.univ-rennes2.fr/celam/cetm/devinettes/devinettes.html> \[Consulté le 31 octobre 2012\].
