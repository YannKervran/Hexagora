====== {0} ======

===== Personnages =====
{4}
===== Lieux =====

===== Chronologie =====
{5}
==== Descriptif court ====
=== Mastodon ===
<WRAP prewrap 650px>
<code>
C'est le jour du nouveau Qit’a, texte court dans le monde médiéval des Croisades, diffusé sous licence libre CC-BY-SA.

## {0}

{2}

http://www.hexagora.fr/fr:qita:{3}

Pour me soutenir : https://www.hexagora.fr/fr:donate

#qita #hexagora #culturelibre #freeculture #artwithopensource #linuxartists #lecture #nouvelle
</code>
</WRAP>
=== Diaspora* ====

<markdown>
# Histoires courtes Qit’a - {6} - {0}

![](https://www.hexagora.fr/_media/fr:qita:qita.png)

Bonjour,
un nouveau [texte court Qit’a](http://www.ernautdejerusalem.com/fr:qita:start) vous attend, comme chaque mois, sur le site d’[Ernaut de Jérusalem](http://www.ernautdejerusalem.com/) gratuitement, sans DRM et sous licence {1}.
Saisissez cette occasion de découvrir autrement le Moyen-Orient des Croisades.

N’oubliez pas que vous pouvez soutenir mon travail en devenant [un de mes mécènes](https://www.hexagora.fr/fr:donate).

### {0}

{2}

Texte intégral lisible et téléchargeable gratuitement :
[http://www.ernautdejerusalem.com/fr:qita:{3}](http://www.ernautdejerusalem.com/fr:qita:{3})

En vous souhaitant bonne lecture

    #fr #qita #hexagora #ernautdejerusalem #ecriture #ecrivain #edition #editionnumerique #lecturenumerique #moyen-age #policier #nouvelle #Lire #lecture #littérature #histoire #moyen-orient #nouvelles #CreativeCommons #CCbysa #CC #bysa #histoiremédiévale #culturelibre #freeculture #artwithopensource #linuxartists

</markdown>

===== Backlinks =====
{{{{backlinks>.}}}}

{{{{tag> qit'a}}}}
