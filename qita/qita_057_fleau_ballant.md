---
date : 2016-07-15
abstract : 1154. Négociant damascène, Idris doit composer avec les aléas politiques et commerciaux quand sa cité et toute la région résonnent des affrontements entre les princes.
characters : Idris, Giorgos, Kaspar, Abu Hasan
---

# Fléau ballant

## Abords de Souq Wadi Barada, midi du youm al khemis 11 dhu al-qi’dah 548^[Jeudi 28 janvier 1154.]

La file d’ânes s’étirait dans la poussière grise et le vacarme des pierres roulant sous les sabots. Tête basse, courbée sous leur fardeau, indifférents aux murailles de roches qui les enserraient, les animaux avançaient le long de l’ancienne voie taillée dans les falaises. Malgré le soleil au zénith, leur respiration s’embuait à chaque pas. Autour d’eux, les caravaniers allaient et venaient emmitouflés dans des hardes sans couleur.

Seul homme à avoir une monture, Idris somnolait sur sa mule. Il était engoncé dans un amas de tissus fatigués qui ne laissaient qu’à peine émerger son nez et ses yeux. Il chantonnait à mi-voix une comptine qu’il destinait à ses neveux et nièces une fois rentré chez lui, à Damas. Il appréciait fort son statut de célibataire, mais prenait grand plaisir au contact des enfants. Jamais il n’était revenu de ses voyages sans un cadeau sous une forme ou une autre pour les garnements qu’il aimait gâter.

Il remarqua que le chef caravanier, reconnaissable à son immense fouet roulé à la taille s’approchait de lui. Malgré le froid, il n’était chaussé que de savates essoufflées.

« Abu Hasan ! Un problème ? s’enquit le jeune négociant.

— Des hommes installés dans les tombes, à deux ou trois portées d’arc.

— Des brigands crains-tu ? s’inquiéta soudain Idris.

— Brigands pas s’annoncer comme ça. »

Idris se redressa en selle et tordit la tête pour voir devant lui. Un petit feu, une toile hâtivement tendue entre quelques piquets. Et même…

« Ils ont des chevaux ! Ce ne peuvent être des pillards. Par contre ce sont des Turkmènes, à n’en pas douter. »

Sa main chercha instinctivement sa hanche, où il attachait généralement une épée. Plus pour se rassurer que pour en faire usage. Il n’avait jamais eu à croiser le fer avec quiconque et préférait toujours s’en remettre à ses talents de négociateur. S’il y avait parfois perdu de l’argent, il se vantait assez souvent d’y avoir conservé la vie. Le visage impassible, Abu Hasan lui demanda s’il fallait faire halte avant d’arriver à leur hauteur.

« Venir sus à nous avec ces chevaux leur serait aisé. Ils nous savent ici. *Mesure la profondeur de l’eau avant d’y plonger*. Allons donc voir si notre modeste train attirera leur intérêt. »

Idris soupira, se frotta le nez et tenta de se sortir de son amas de tissus. L’émir ’Ata, seigneur de Ba’albek était un homme respecté et craint dans la région, et il était membre de l’administration de Damas. La présence de détrousseurs était peu envisageable, sauf si le prince du Nord, l’Aleppin Nūr ad-Dīn, déployait ses forces. Il y avait toujours des auxiliaires, des soudards chargés de fourrager, qui ne s’encombraient guère de scrupules. Il remercia Allah de n’avoir avec lui aucun produit que les pilleurs appréciaient, et de ne jamais partir sans quelques bourses de vieux dirhems brunis pour les bakchichs.

Lorsqu’ils furent proches de la troupe, Idris s’avança en tête. Les soldats, car c’en était bien, ne portaient aucun signe qui eut permis de les distinguer. Nulle bannière, pas de couleurs. De simples nomades qui s’étaient loués comme auxiliaires de guerre le temps d’une saison. Donc corruptibles estima Idris.

L’un d’eux, le visage aussi ridé qu’une vieille pomme, la mâchoire ornée d’une barbe tressée et agrémentée de bagues d’argent, vint se mettre en travers du chemin. Le menton levé, il avait calé ses mains dans l’étoffe de sa ceinture. Derrière lui, les hommes s’étaient déployés en demi-cercle. Aucun n’avait d’arme au poing, mais sabres et arcs pendaient à leurs hanches. Le Turkmène attendit d’être à portée de voix et lança, dans un sourire :

« Salaam aleikum !

— Aleikum salaam, répondit Idris sans même y penser. »

L’homme fronça encore plus les sourcils et s’avança tranquillement. Son visage déformé par un rictus semblait hésiter entre le dédain et le dégoût. Mais son attitude demeurait nonchalante.

« Où allez-vous ainsi chargés ?

— Dimashq. Ce sont là denrées provenant de Ba’albak. »

Le Turkmène hocha la tête lentement.

« Quel genre ? Tissu ? Épices ?

— Rien de bien luxueux, simplement fèves, froment, lentilles… »

Idris nota que l’homme se raidit brutalement et lui adressa une moue de désapprobation silencieuse.

« Je vous assure que je ne transporte rien de plus. Juste de quoi manger.

— Je te crois, je te crois. C’est juste que l’émir ne peut accepter les spéculateurs. Il doit veiller au grain… »

Tandis que l’homme souriait avec emphase et sans joie à sa répartie, Idris cherchait à comprendre. Il avait entendu parler des manœuvres de Nūr ad-Dīn pour gêner l’approvisionnement de la ville, tout en incriminant l’accord avec les Francs comme cause de la disette. Une sueur glacée lui parcourut l’échine et sa voix se fit moins assurée lorsqu’il répondit :

« Je ne suis qu’honnête négociant. J’achète et revends à prix raisonnable.

— Voilà bel ensemble d’ânes ! Ils portent du froment ceux-là ? »

Idris acquiesça sans rien ajouter. Le Turkmène compta silencieusement et revint à lui.

« Si je vois bien, chaque bête porte un irdabb[^irdabb1], ce qui rapporte dit-on dans les six à sept dirhems de bon argent dans les suq dimashqi. Ne me dis pas que tu l’as payé en rapport!

— Je ne compte pas le revendre à pareil prix ! s’indigna Idris.

— Et pourquoi tu te gênerais ? »

L’homme avait adopté un ton de voix agacé, presque agressif, qui réduisit Idris au silence. Le Turkmène inspira longuement, secoua la tête comme s’il s’apprêtait à morigéner un enfant puis reprit la parole.

« L’émir nous a dépêchés pour empêcher que des accapareurs profitent de la misère des pauvres gens.

— Ne peut-on s’arranger ? Ma famille, mes amis, comptent sur ces victuailles. »

Idris fit mine de s’emparer de la besace de toile qu’il portait en bandoulière. Il savait y trouver les bourses de vieux dirhems.

« Crois-tu que je fais cela pour t’extorquer de la menue monnaie ?

— *Le meilleur des dons est celui qui se fait sans demande.* J’espérais juste te proposer un négoce, de ceux que je pratique souventes fois. Moi je veux passer, et toi tu possèdes l’accès à cette route, là, derrière toi. Trouvons un prix raisonnable et quittons-nous bons amis. »

Le Turkmène se passa la main sur le menton plusieurs fois, se tourna vers ses hommes. Il semblait indécis, ce qui mit un peu de baume au cœur d’Idris. Lorsqu’il s’approcha de nouveau, Idris put sentir son haleine chargée.

« Je serais heureux de te vendre ça, mais il se trouve que l’émir pense que ce chemin est sien. Et lui m’offre de belles montures, des pointes de fer d’excellente qualité pour nos flèches, des femmes et tout ce que mon poing peut prendre à l’ennemi. Qu’as-tu de plus à m’offrir ? »

Il haussa les épaules et ricana doucement. Puis agita la main :

« Allez, marchand, rebrousse chemin avant que je ne ne décide de te mettre à l’amende pour avoir tenté de voler l’émir et les pauvres.

— Rebrousser chemin ? Mais je suis attendu à Dismashq !

— Alors, passe. Mais tu devras abandonner tes ânes ! »

## Village de Taranjeh[^taranjeh], demeure du ra’is, soirée du youm al joumouia 16 muharram 549^[Vendredi 2 avril 1154.]

La maison du chef du village respirait l’opulence. Solidement bâtie en pierre autour d’une cour arborée, elle s’étendait en plusieurs ailes, comprenant des zones artisanales et de vastes entrepôts de stockage. Le majlis[^majlis] s’ouvrait largement sur un petit jardin potager par des baies ajourées, surmontées d’un auvent de feuilles de palme. Profitant de la chaleur revenue pour un jour après un hiver assez froid, le ra’is y avait installé des banquettes pour y travailler. Lorsqu’Idris s’était présenté, il l’avait accueilli aimablement et lui avait fait servir des rafraîchissements. Puis la discussion s’était lancée sur des considérations générales, comme souvent. Le négociant s’étonnait fort de la richesse apparente du lieu, ayant toujours cru que les Ifranjs écrasaient leurs sujets musulmans de taxes et de corvées diverses.

« En un sens c’est vrai, nous payons des redevances que les polythéistes ne versent pas. Mais selon le maître, nous pouvons jouir d’une relative liberté. Au final, tant que les impôts sont payés, personne ne vient nous malmener.

— Vous ne craignez pas les troupes de guerre, si près de la marche ?

— Jusque là celle-ci nous aurait plutôt servi. Aucun ici ne porte l’anneau de fer au poignet, car il nous serait rapide de fuir vers les terres amies. Et l’aisance qui nous est accordée ne l’est que pour nous inciter à nous défendre par nous-mêmes contre les pillards nomades au lieu de pactiser avec eux.

— À Dimashq, la présence des émirs ifranjs venus percevoir le tribu est toujours mal passée. Je ne sais comment vous faites.

— Peut-être sommes-nous moins fiers que les Dimashqi, rétorqua le ra’is, tout sourire. Nous n’avons que fort peu à voir nos maîtres, et nous commerçons les surplus ainsi que nous l’avons toujours fait. Ils ne viennent pas nous dire quel appel lancer du minaret et n’interviennent jamais dans les prêches. Pouvez-vous en dire autant ?

— C’est bien pour ça que nous refusons de courber la tête devant l’Alépin » reconnut Idris.

De retour des champs, plusieurs ouvriers rapportaient des paniers vides, certainement destinés à des semences. Des senteurs commençaient à se propager depuis les cuisines, mélange de condiments et d’huile d’olive chauffée. Idris attrapa un morceau de galette qu’il trempa dans une purée de pois. Il appréciait le lieu et se disait qu’il pourrait s’y faire, même s’il devait faire allégeance pour cela à des infidèles.

Il avait de nombreux défauts, il le reconnaissait, mais ne comptait pas la bigoterie parmi eux. Quoiqu’à bien y penser, il tenait avant tout à sa liberté d’aller et venir. Il devait parfois y mettre les formes, mais c’était un prix qu’il acceptait de payer. Même lorsque cela demandait de supporter plusieurs semaines de tracasserie administrative.

« J’ai pu obtenir une lettre pour commercer librement dans les cités du royaume de Badawil[^baudoinIII]. J’ai espoir d’acquérir du grain, des fèves et pois. Notre cité en manque cruellement…

— Je n’ai plus guère de quoi fournir une caravane, nous sommes en période de jointure. Mais la cité de Baniyas a beaux celliers bien garnis. S’ils n’ont pas déjà tout revendu, il se trouve là-bas des négociants qui auront de quoi vous convenir. Je saurai vous donner des noms.

— Je n’ai vu que peu d’Ifranjs dans la cité quand j’y suis passé. J’avais espoir d’y recruter des hommes pour faire escorte aux bêtes.

— Soudoyer ifranjs à votre service ? » s’étonna le ra’is.

Idris hocha la tête plusieurs fois tout en avalant un peu de leben[^leben].

« Je sais que l’Alépin ne souhaite pas la guerre avec Badawil. Pas encore du moins. Donc ses hommes hésiteront à s’en prendre à moi si je suis protégé de tels hommes.

— Voilà bien dangereux stratagème…

— *Un fou a jeté une pierre dans un puits ; mille sages n’ont pu la retirer*. Les miens ont faim, et l’émir souffle sur les braises du mécontentement. Les ventres creux suivront la main qui les nourrira, et même si c’est celle qui les a affamés.

— Inch’Allah ! » approuva le ra’is.

## Baniyas[^paneas], après-midi du youm el itnine 19 muharram 549^[Lundi 5 avril 1154.]

Idris mit du temps pour dénicher l’échoppe de Giorgos. C’était un négociant auprès duquel il pouvait faire valoir quelques amis communs qui les garantiraient l’un l’autre. En outre, c’était un marchand réputé. Un peu sur le déclin depuis quelques années, mais dont on avait assuré Idris qu’il saurait répondre à sa demande.

Le bâtiment cherché occupait l’angle d’une place où chantait une fontaine, non loin d’un quartier où des potiers opéraient. Nulle céramique ne qualité de se voyait sur les éventaires aux abords, mais seulement de grosses jattes, des oules de cuisine et des vases de transport. Quelques ouvriers passaient d’une cour à l’autre, portant bois, paniers paillés ou plateaux de pièces en attente de cuisson.

Quelques enfants s’amusaient dans un arbre poussé parmi les débris d’une maison voisine en ruines. Un mendiant marmonnait entre ses gencives édentées, s’adressant à quelque génie visible seulement de lui. Le tintement des billons de cuivre qu’Idris jeta dans sa coupelle ne lui fit même pas tourner la tête.

La minuscule échoppe était bordée sur ses deux côtés de banquettes en terre, dont l’une disparaissait sous un fatras de paniers et de sacs. En face, assis sur un mince matelas, un homme au visage rond d’une cinquantaine d’années, moustachu et mal rasé, dictait une lettre à un jeune adolescent. Lorsqu’Idris s’approcha, le marchand leva la tête vers lui et sourit d’un air engageant.

« Puis-je t’aider, étranger ? Tu me sembles à la recherche de quelque chose, ou de quelqu’un.

— Si vous êtes Giorgos, l’ami de Ahmed ben Tayyib de Ba’albak, je l’ai trouvé » rétorqua Idris avec affabilité.

À la mention de son nom, le négociant dévoila une nouvelle fois ses chicots abîmés et invita son interlocuteur à entrer. Il envoya son apprenti chercher de quoi se désaltérer puis proposa à Idris de s’asseoir face à lui. Lorsqu’ils furent installés confortablement devant quelques victuailles, il commença à poser de nombreuses questions, dont il n’attendait pas toujours les réponses, préférant présenter sa propre situation sur ce point.

Il s’animait assez fiévreusement, secouant les bras en tous sens tandis qu’il parlait d’une voix montant facilement dans les aigus. Il engloutissait les friandises sans même les mâcher ni ralentir le débit de ses paroles. Rapidement il s’adressa au Damascène comme à une vieille connaissance. Il insista à plusieurs reprises pour que le jeune marchand soit hébergé dans sa demeure le temps qu’il serait à Baniyas, ce qui serait, affirmait-il avec autorité, gage de sérieux pour les partenaires qu’il désignerait à Idris.

« Je ne sais si j’aurais besoin d’en voir nombre, je n’ai besoin que de grains et fèves. On m’a dit que vous étiez fort expert en cela.

— Celui qui t’a dit ça est trop aimable. Je ne fais plus guère d’opérations. Je m’associe aux autres, je prends des parts là, je fournis le capital ici… Mais n’aie crainte, nous trouverons bien quelqu’un avec des greniers à délester. Combien t’en faut-il ? Comment comptes-tu les emporter ?

— J’ai des ânes pour porter jusqu’à Damas. Autant de ghîrara[^ghirara] que je pourrai en trouver. Un de mes hommes s’occupe des bêtes, j’espère en assembler au moins six à sept douzaines. Si je peux en avoir plus, je les prendrai.

— Je connais l’homme qu’il te faut. Tu as tes muletiers apparemment ? Donc pas besoin de guide. Si tu peux ramener les bêtes tu auras un bon prix chez un mien ami. »

Il marqua une pause, s’éclaircit la gorge et attrapa un carnet de feuilles dans une sacoche épinglée au mur à côté de lui. Il le compulsa un petit moment à voix basse, comptant ensuite silencieusement puis reprit la discussion avec Idris.

« Le froment sera aisé à trouver, mais peut-être pas autant que tu l’aurais souhaité. Son propriétaire est avaricieux et ne sait pas lâcher quand il le faut. Chaque année il en gâte une partie à ne pas vouloir le céder à moindre prix. Mais cela ne lui sert jamais de leçon ! conclut-il en s’esclaffant.

— *L’erreur est fille de l’erreur* confirma Idris.

— Et tu es philosophe en plus de négocier ! s’enthousiasma Giorgos. Dommage que tu sois mahométan, sinon je t’aurais proposé de marier ma cadette. »

Il assortit sa remarque d’un clin d’oeil et se replongea dans ses notes, puis compulsa des tablettes. Pendant ce temps, Idris laissait errer son regard au-dehors. La journée était ensoleillée et les gens vaquaient tranquillement. De nouveau, il s’étonnait du peu de Latins rencontrés dans les murailles. Il avait dû aller jusqu’à Jérusalem pour obtenir les documents définitifs qui lui permettaient de commercer. Il avait découvert pour la première fois l’ampleur du royaume de ces conquérants venus par-delà les mers.

Homme de transaction plus que d’affrontement, il en était revenu émerveillé comme un enfant. Un peu inquiet en ce qui concernait l’avenir de sa cité, dont la splendeur n’avait rien à envier à la capitale franque selon lui. Il n’arrivait pourtant pas à envisager le monde de façon binaire. Il avait été confronté à des imbéciles dans tellement de pays, et accueillis par des visages hospitaliers dans suffisamment de ports qu’il n’accordait plus guère de foi aux puissants et à leurs territoires. Il ne s’intéressait qu’aux individus, s’amusant par avance des joies de la rencontre.

Il lança un regard à Giorgos, dont il soupçonnait la subtilité habillée de façons grossières. Sa faconde et ses plaisanteries aimables faisaient partie de ses talents de négociant, Idris en aurait juré.

« J’ai beau creuser ma pauvre cervelle et compulser mes notes, je crains qu’il n’y ait guère de pois et fèves. Tu as avec toi de quoi comparer nos mesures ?

— Je ne voyage jamais sans !

— Parfait. Car ici nous avons obligation de passer par un mesureur juré. Il possède les étalons et apprécie en boisseaux ce genre de victuailles. »

Il ajouta avec une grimace :

« Il faut bien évidemment lui verser la taxe de pesage à cette occasion, à charge de l’acquéreur. »

Idris hocha la tête en silence, il était accoutumé à cela. Il se pencha ensuite vers le vieux marchand et ajouta :

« J’aurais aussi et surtout besoin de trois ou quatre soudards aguerris. Plus pour impressionner qu’autre chose. Des Celtes. »

Giorgos fit une grimace de déplaisir et massa ses bajoues comme si une puce venait de s’y cacher.

« C’est que je n’aime guère fréquenter ces oiseaux-là. Il m’est difficile de te donner bon conseil en ce sens. Ils sont pires que des chacals. »

Il hésitait, soufflait, visiblement ennuyé. Ou comédien pas trop inspiré, se demanda Idris. Puis Giorgos lâcha, presque à regret :

« Je demanderai conseil à Kaspar, un voisin qui a grand talent en archerie. Il est parfois soudoyé pour tenir la muraille. Son père était un Ermin[^ermin], habile ferrant. Pas vraiment un ami, mais de bonne fame. »

## Baniyas, maison de Giorgos, soirée du youm el itnine 19 muharram 549^[Lundi 5 avril 1154.]

Idris s’en voulait un peu de l’opinion qu’il s’était forgée de son hôte. Il était extrêmement bien accueilli, disposant d’une chambre pour lui tout seul, avec un bon matelas en guise de lit et plusieurs coffres fermant à clef pour y poser ses affaires. Après leur discussion, il avait pu se rendre aux bains et finissait de s’apprêter pour le repas du soir. Il était prévu de rencontrer quelques contacts commerciaux, plusieurs des associés de Giorgos œuvrant dans les denrées alimentaires et la location d’animaux de bât.

Lorsqu’il parvint au bas des marches dans la petite cour où s’ouvrait la salle de réception, Idris aperçut que le maître de maison, habillé de frais dans une robe colorée, conversait avec un solide gaillard dont le dos voûté trahissait le travail d’archer. En le voyant arriver, les deux hommes se turent et se tournèrent vers lui, l’air sombre.

« Quelque soucis, maître Giorgos ?

— Je crains que vous n’ayez grande difficulté à recruter des hommes pour porter des denrées à Damas, mon ami. Kaspar, que voici, me porte bien fâcheuses nouvelles pour vous… »

La poitrine d’Idris se comprima soudainement. Il s’approcha, attentif, tandis que le géant arménien lui expliquait :

« Un coursier est passé ce jour. Le roi Norreddin attaque Damas. Votre roi a appelé Baudoin à l’aide. L’ost sera levé pour aller porter soutien aux vôtres, en vertu de nos traités. »

Idris ferma les yeux lentement. Verrait-il le jour où les vautours laisseraient Dismashq en paix ? ❧

## Notes

Au milieu de la décennie 1150, les choses deviennent de plus en plus difficiles pour le pouvoir damascène, surtout après la mort d’Anur qui avait magistralement réussi à jouer la carte diplomatique pour se maintenir entre les Latins et Nūr ad-Dīn. Ses successeurs ne furent pas de même force et il ne fallut guère de temps pour que l’émir arrive à ses fins. Faisant cela, il posait la première pierre indispensable à l’éviction des Francs de Terre sainte, en unifiant la Syrie du Nord (Alep) à Damas. La déliquescence fâtimide en Égypte allait offrir le terrain d’affrontement pour les années qui suivraient, une fois que les opposants comprirent qu’il était vain de penser pouvoir aisément faire fluctuer les frontières orientales des Latins.

Malgré la guerre endémique qui sévissait, les négociants circulaient entre les cités. Les missives privées retrouvées ne font que superficiellement référence aux batailles dont les chroniqueurs nous disent qu’elles ébranlaient les royaumes. Il semblerait que pour les marchands, la guerre ait représenté un impondérable semblable aux naufrages, maladies, brigandages et taxes. Ils s’en accommodaient et s’efforçaient de faire affaire en respectant les complexités administratives des différents pouvoirs auxquels ils étaient confrontés. Pas étonnant qu’ils aient alors été considérés comme de réels aventuriers, et que les négociants âgés aient préféré gérer leurs investissements grâce à des commissionnaires voire par correspondance.

## Références

Ashtor Eliyahu, *Histoire des prix et des salaires dans l’orient médiéval*, Paris : École Pratique des Hautes Études, 1969.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Elisséef Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.
