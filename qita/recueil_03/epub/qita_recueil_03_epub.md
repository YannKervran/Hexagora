![Framabook, le pari du livre libre](logo_framabook_epub.jpg)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d'informations sur Framasoft, consultez <http://www.framasoft.org>

Se démarquant de l'édition classique, les Framabooks sont dits « livres libres » parce qu'ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu'un utilisateur de logiciels libres. Les Framabooks s'inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l'appropriation collective de la connaissance.

Pour plus d'informations sur le projet Framabook, consultez <http://framabook.org>

Copyright 2020 : Yann Kervran, Framasoft (coll. Framabook)

Ce recueil de textes Qit'a est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

ISBN : 979-10-92674-30-9\
Dépôt légal : Mai 2020\
Couverture : Eadmer de Canterbury écrivant, vers 1140-1150. Domaine Public - [commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:Eadmer_of_Canterbury_Writing_-_Google_Art_Project_(6827628).jpg)

Remerciements
-------------

La publication de ce premier lot de quatre recueils de textes courts Qit'a a demandé beaucoup de temps et d'énergie au membres du groupe Framabook, je tiens à les en remercier, Mireille en tête, avec qui les échanges autour de corrections proposées ont toujours constitué d'agréables moments.

*À la mémoire des exclus de la Mémoire*

Poing de trop
-------------

### Jéricho, midi du vendredi 27 juin 1158

Attrapant tout ce qui lui tombait sous la main, Droin enfournait ce qu'il pouvait dans une grosse besace de cuir. Vêtements, écuelle, savon, chaussures, tout était bon. Il chercha frénétiquement la clef du petit coffre qu'il glissait sous son lit et fit jouer la serrure. À l'intérieur toute sa fortune se résumait à une poignée de pièces d'argent, un couteau de bonne taille et quelques bijoux, qui tenaient plus de la verroterie que des chefs-d'œuvre d'orfèvrerie. Il sursauta quand il entendit quelqu'un entrer dans la petite pièce. Il se retourna, hésitant à brandir la lame. Puis reconnut Fulbert, le jeune qui partageait la chambre.

« Que fais-tu, compère ? Tu devrais aller t'expliquer plutôt.

--- Et risquer amende ou pis encore. Plutôt prendre le large ! »

L'idée horrifia le jeune, qui en laissa pendre sa mâchoire avant de répondre.

« Tu es fol ! On va te percer le poing d'un fer chaud si tu fuis.

--- Et me le couper si je reste !

--- On ne sait ce que la Cour dira.

--- Tu parles, je suis là depuis quelques mois à peine, Evrard est né ici, et se trouvait sûrement déjà tonsuré au sortir du con de sa mère. Il aura beau jeu de trouver deux témoins jurés. »

Fulbert s'assit sur le lit, regardant son compagnon s'activer.

« Tu pourras lever ses plèges et prouver ton bon droit.

--- Risquer ma vie pour si misérable étron, peu me chaut. Je décampe et ils ne me verront plus jamais. »

Fulbert soupira, désolé. Il aimait bien Droin malgré son sale caractère. Ils partageaient une petite pièce aux abords des jardins qu'ils entretenaient pour les moines, sous le couvert de la verdoyante oasis. En arrivant là, Droin avait cru d'abord au Paradis, puis il avait découvert le rythme infernal du labeur exigé de lui. Evrard était le cellérier qui tenait la badine pour les faire travailler, et il avait le verbe haut, si ce n'était la main leste. Il se vengeait volontiers sur le dos des quelques manouvriers musulmans parfois réquisitionnés pour les corvées les plus pénibles. Jusqu'à ce que son nez rencontre le poing de Droin, dans un fracas de dents et d'os brisés. La douleur irradiait encore ses phalanges, jusqu'au coude. Il n'avait pas mis une telle rage dans un coup depuis des années.

« Et ta soudée ? Tu vas la perdre, en partant ainsi.

--- Les *bons* moines sauront en faire adroit usage. Ils ont si crochus qu'ils pourraient labourer des pierres sans s'y casser un ongle. Que leur monnaie les emporte aux Enfers ! »

Il boucla précipitamment son sac et se releva, l'assujettissant sur son épaule. Il empoigna le bâton avec lequel il était arrivé, quelques semaines plus tôt. C'était la première fois qu'il devait fuir ainsi, mais n'avait nul regret. Toute son enfance, il avait vu son père courber l'échine devant plus riche, plus puissant, plus fort. Mais il était fait d'un autre métal, comme Ernaut, son ami, avec lequel il s'était tant bagarré, petit. Personne ne ferait de lui un serf. Il posa une main amicale sur l'épaule de son compagnon.

« À Dieu te mande, compaing. Puisses-tu réussir là où j'ai échoué et finir laboureur avec belle famille.

--- Prends garde à toi, compère Droin. Je prierai pour ton âme.

--- Fais donc ça, oui. Elle en a bien besoin. »

Puis il poussa la porte, inondant de lumière les murs chaulés, le sol en terre battue, les deux galetas et l'étagère branlante, le toit en feuilles de palmier. Il traversa la courette où ils entreposaient les chariots à bras, le bois d'oeuvre et sauta par-dessus le muret de clôture. Sans un dernier coup d'œil derrière lui, il prit la route sous le couvert de la palmeraie, vers le sud, cherchant les reliefs qui longeaient la Rivière du Diable[^1]. Il espérait bien échapper ainsi aux recherches. Les moines du Mont de la Tentation n'allaient certainement pas faire preuve d'indulgence. S'ils étaient prodigues des bienfaits du Seigneur dans la vie à venir, ils n'étaient guère enclins à faire preuve de générosité, surtout avec un domestique rebelle.

### Abords du Fier, matin du mardi 1er juillet 1158

Droin était un peu vermoulu des nuits passées avec des cailloux pour tout matelas. Mais il se sentait plutôt bien. Il retrouvait la sensation de son départ de Vézelay, la liberté de destination en plus. Il avait juste l'estomac dans les talons. Ne connaissant pas les plantes de Terre sainte, il n'avait pas avalé grand chose depuis sa fuite de Jéricho. Il s'était également abstenu de voler quoi que ce soit dans des jardins, pour éviter de se faire remarquer.

Mais la faim le tenaillait désormais cruellement. Assis dans un bosquet de broussailles, il fouillait dans ses affaires et déroula un petit morceau de tissu rouge, un peu fatigué. Il sourit en admirant la croix que sa mère avait cousu sur son épaule avant son départ, et qu'il avait conservée dans une bourse. Il regrettait juste de n'avoir pas de quoi la recoudre histoire de se faire passer pour un pèlerin. Il vérifia également qu'il avait toujours la licence de son évêque, qui attestait de son statut, et qu'il avait fait viser par les chanoines du Saint-Sépulcre. Il y était écrit qu'il était parti en bon chrétien, avec la bénédiction des autorités, et qu'il était parvenu à son but. Il ne savait pas lire ces lignes, mais les regarder lui procurait un apaisement. Les deux sceaux qui y étaient attachés lui faisaient l'impression d'avoir une charte d'importance sur lui.

Son ventre gargouilla, désireux de connaître meilleur repas que la veille. Droin se frotta le visage.Il lui faudrait aussi trouver de l'eau. Il n'avait pas de calebasse ni d'outre et la soif le tiraillait parfois plus que la faim. Sur les reliefs face à lui, au levant, un village de pierres jaunes s'étirait aux abords d'une tour forte, dont la bannière pendait tristement. Le soleil avait brûlé les herbes, et seuls quelques arbres persistaient à ponctuer de vert les collines poudreuses, autour des zones d'où émergeait une source.

La route était assez fréquentée, plus que ne l'aurait cru Droin étant donné sa position loin au sud, aux confins du royaume avait-il pensé. Il espérait malgré tout être assez loin du bras armé des moines. Il se mit debout, épousseta sa tenue et s'étira avant de prendre ses affaires et de se remettre en route. Le village qu'il apercevait pourrait peut-être l'accueillir un temps.

Les champs avaient été moissonnés depuis des semaines et on ne semblait guère faner, vu la sécheresse. Des courtils s'attachaient aux maisons étagées, aux austères façades percées de rares et discrètes fenêtres. Des troupeaux de chèvres paissaient aux alentours, arrachant la maigre végétation qui tentait de survivre dans cet austère paysage.

Tout en grimpant vers le casal, Droin croisa quelques paysans, des Syriens à la mine sombre, qui ne lui prêtèrent pas attention. Lorsqu'il se présenta à la forteresse, il réalisa combien elle était modeste. Les moellons n'étaient guère équarris, et le mortier se pulvérisait par endroit. On était bien loin des orgueilleux châteaux des plaines littorales. Il n'avait d'ailleurs toujours pas identifié la bannière censée flotter sur le mât, qui s'obstinait à pendre mollement. Il frappa plusieurs coups sur la porte barrée, attirant un des gardes, qui passa sa tête enturbannée par-dessus le parapet.

« Que veux-tu, voyageur ? »

L'homme s'exprimait avec un fort accent, et semblait natif de la région. Droin estima que cela pouvait être à son avantage.

« Je suis humble marcheur de Dieu, en quête d'un lieu où apporter le soutien de mon bras pour le combat contre les ennemis de la Foi. »

Il fouilla dans son sac et en exhiba le document scellé, qu'il agita comme un laisser-passer. Le garde disparut et, après un bon moment, un des vantaux de la porte s'ouvrit. Derrière se tenait un grand échalas au teint bistre, le nez pelé par les coups de soleil. Il était vêtu d'un thawb négligé, mais était indiscutablement européen d'origine. Mâchonnant un cure-dent qu'il faisait passer d'un coin de sa bouche à l'autre, il semblait sortir du lit. Il bailla avant de saluer d'un signe de tête.

« Habib dit que t'es un soudard ? »

Il semblait soudain un peu circonspect quant à ce point, découvrant un errant comme il en voyait tant, même si la tenue, chausses et cotte, était indubitablement latine. Droin hésita un instant mais tenta de pousser son avantage.

« À dire le vrai, je suis marcheur de Dieu mais j'ai désir de combattre pour la vraie Foi. Je viens donc batailler à vos côtés ! » affirma-t-il tout en agitant son document.

Le garde y jeta un coup d'oeil sans qu'il soit possible de dire s'il y comprenait quelque chose.

« C'est le sire Gaston qui te mande ?

--- J'ai quitté la sainte Cité dans l'espoir de servir utilement là où mes pas me porteraient. Je pensais être plus utile ici, là où les hommes du Soudan de Babylone[^2] viennent piller, avec l'aide des nomades. »

L'homme hocha la tête, l'esprit encore embrumé de sommeil. Il lui tendit la main.

« Moi c'est Raoul. On m'appelle Sayette, vu que je suis bon archer. On a pas pouvoir de te souder, mais on va aviser sire Eustache, voir si tu peux demeurer ici le temps que le sire Gaston vienne et dise quoi faire de toi.

--- Mille grâce, l'ami. Je suis... Ernaut, et j'ai hâte d'occire tous ces mécréants !

--- Ouais, ouais. Viens-t'en, Ernaut, je vais te montrer un lieu où poser ta besace. Eustache n'a pas encore pointé le nez hors de son lit. »

Puis il tira la porte plus grand, invitant Droin à pénétrer.

La cour était modeste, bordée d'un appentis pour les chevaux, avec des auges en pierre. Des poules déambulaient, grattant un tas de fumier bien maigre. L'endroit semblait comme abandonné, écrasé sous la chaleur estivale. De tous les bâtiments, seule la tour était correctement maçonnée, les angles marqués de pierres régulièrement disposées et les assises nettes. Suivant Raoul, Droin clopina jusqu'à un bâtiment annexe.

### Château du Fier, fin d'après-midi jeudi 4 septembre 1158

La petite troupe arrivée un moment plus tôt se composait essentiellement de cavaliers, mais un seul parmi eux était chevalier. Ils formaient une bande assez pitoyable de l'avis de Droin, mais nul ne le lui avait demandé. À peine une vingtaine d'hommes, équipés comme des sergents mais avec des tenues sales, des armes usées et des chevaux fatigués. Ils avaient demandé le gîte pour la nuit, se rendant à Gaza depuis Paulmiers. Les hommes étaient visiblement harassés de leur chevauchée dans la canicule et ils s'occupaient des bêtes avec des geste las. Droin, qui n'avait guère de corvées habituellement, vint se proposer pour les aider. Il s'était présenté comme soldat, et faisait comme s'ils étaient frères d'armes, tentant d'en savoir plus sur eux. Mais la plupart répondaient dans un idiome inconnu de lui, et un seul se montra assez affable pour lui répondre sans brusquerie.

Le visage massif, les yeux ridés à force de les plisser, Galien était un Français, de Paris, clamait-il fièrement. Sa barbe blonde et ses yeux clairs contrastaient avec son visage hâlé par des années en Outremer. Il marchait en claudiquant, mais sans que cela ne semble l'handicaper le moins du monde. Tandis qu'ils menaient les chevaux à l'abreuvoir, Droin entreprit de le faire parler, ce que l'autre faisait volontiers.

« On repartira au matin, on a grand désir d'être à Gaza au plus tôt. On va botter quelques culs païens !

--- Vous joignez l'ost royal ?

--- Non pas, le sire Bertaud des Lices assemble un conroi pour venger l'assaut fait tantôt sur Gaza et Bethgibelin, plus tôt dans l'année. On va venger les frères tombés. »

Droin avait vaguement entendu parler d'opérations par des voyageurs, depuis qu'il était au château du Fier, mais il ne s'était guère intéressé à ces histoires. Il surveillait plus particulièrement les rumeurs de fugitifs recherchés. Mais rien n'était remonté à ses oreilles.

Il n'était toujours pas officiellement soldé par le seigneur du lieu, qui ne semblait pas venir bien souvent, mais s'était fait accepter par la petite communauté, échangeant gîte et couvert contre corvées et travaux que les autres n'avaient pas envie de faire. Il n'y avait pas beaucoup de latins dans ce coin perdu dans les montagnes du sud du royaume, ce qui expliquait peut-être le relatif bon accueil qu'il avait eu.

« Vous êtes nombreux à lever ainsi le ban ? »

Galien fit la moue.

« Je ne saurai dire. Des nomades du désert vont nous guider. Ils n'aiment guère le soudan eux non plus.

--- Ils sont mahométans ! Vous ne craignez pas traîtrise de leur part ?

--- Les nomades n'ont qu'une seule foi, celle des pilleurs. Butin amassé vaut toutes les prières pour eux » s'amusa Galien.

Les chevaux brossés et nourris, tout le monde se dirigea vers la grande salle, guère habituée à autant d'animation. Ordinairement ils étaient à peine une demi-douzaine à manger là. Les deux chevaliers discutaient au haut bout de la table, buvant du vin dans leur gobelet de verre. Les autres hommes se contentaient de gobelets de terre. Ils étaient tous affalés, les visages fermés, savourant le breuvage sans trop d'effusions. Quelques-uns avait tiré un plateau de mérelles[^3] tandis que d'autres s'étaient carrément étendus, la tête sur les bras, et ronflaient sans être gênés par le bruit. Droin attrapa de quoi boire et s'installa avec Galien auprès des joueurs.

« Votre sire fait-il embauche ?, s'enquit Droin.

--- On a toujours besoin de bras habiles à tenir l'épée. C'est quoi ta spécialité, Ernaut ?

--- ...

--- Tu te bats avec quoi, d'usage ? »

Devant l'hésitation de Droin, un des joueurs lui lança un regard peu amène.

« T'es soudard ou valet ?

--- Bein, soudard, mais ici on fait pas grand chose.

--- On te loue à quel prix ici, et avec quelle spécialité ?

--- Justement, j'attends encore le sire Gaston pour... »

La réponse embarrassée fit naître un sourire narquois sur les visages.

« Si tu veux gagner ta pitance l'arme en main, il en faudra plus que ça.

--- Je suis prêt à apprendre ce qu'il faut. »

L'autre joueur, assis à côté de lui, renifla et se tourna, se touchant les parties génitales.

« C'est là qu'il faut avoir ce qu'il faut, et ça s'apprend pas.

--- Ouaip, acquiesça l'autre, postillonnant de rire.

--- N'empêche, deux bras en plus, ça peut aider. Il faudrait en parler au sire Hugues, les interrompit Galien.

--- Ça fera surtout une part de butin en moins !, rétorqua le second joueur.

--- Nan, il parle de vrai, le contredit l'autre. On a grand besoin de nouveaux hommes. Et il en vaut bien d'autres. »

Galien fit un discret signe de tête à Droin.

« On causera plus tard de ta soudée, avec le sire Hugues. Il va vouloir s'assurer que t'es pas larron en fuite, mais si ils se portent garants de toi, ici, y'aura pas de problème.

### Désert d'al-Gifar, après-midi du vendredi 19 septembre 1158

Abrité derrière une dune, Droin reprenait son souffle. Il souleva son casque de fer brûlant malgré le turban et s'essuya le visage. La calebasse à son flanc était fendue, vide. Il l'envoya rouler de rage. De là où il était, il pouvait voir les restes de la troupe se disloquer en direction de l'est. « Tant mieux » grommela -t-il pour lui même. Au moins les Mahométans ne viendraient pas dans sa direction. Il avait gardé l'épée qu'il avait ramassée en fuyant, mais n'avait rien pour la ranger. Elle était poisseuse de sang, maculée de sable. S'enfonçant dos à la dune, il cherchait à reprendre son souffle. Un groupe de bédouins à dos de chameau galopait en direction des montagnes au sud, harcelé par des cavaliers qui bourdonnaient tels des guêpes.

S'il venait à l'idée d'un de ces archers montés de regarder dans sa direction, il était aussi visible qu'une mouche sur du lait. Il se laissa glisser en contrebas et suivit le fond du petit vallon, en demeurant baissé, parfois carrément accroupi. Un peu plus loin, une ligne de crête s'affaissait, bordée de quelques broussailles dénudées. S'il pouvait arriver là, un buisson suffisamment touffu pourrait suffire comme cachette jusqu'à la nuit.

Il lui sembla une éternité avant de pouvoir enfin se glisser parmi les branchages griffus, qui lui écorchèrent les chairs. Il avait soif, sa bouche était de sel et les yeux le brûlaient, mais il savait la survie à ce prix. Ensevelissant tout ce qui était métallique sous le sable, il entreprit de se faire un cocon des feuillages racornis. Puis il patienta, se faisant aussi discret qu'il le pouvait, hésitant même à respirer par moment, tellement l'angoisse étreignait son cœur.

Un blatèrement le fit sursauter alors qu'il s'était assoupi sans s'en rendre compte. Il se blessa au front contre une des branches et pesta, autant de surprise que de douleur. Un chameau paissait tranquillement les feuilles des buissons non loin de lui. Sur sa selle, un bédouin affalé était maintenu en place par une sangle de bouclier. Sur les flancs, quelques sacs, une housse à javeline et, surtout, une outre. Droin sentit immédiatement la salive lui venir à l'idée de savourer une longue gorgée d'eau, même tiède et macérée dans une peau de chèvre.

Après un rapide coup d'œil panoramique, il décoinça les branchages et se déplia doucement. Il glissa hors de sa cachette et avança doucement vers l'animal, qui le regardait d'un air narquois. Il semblait plus concerné par les plantes qu'il arrachait que par ce bipède qui s'approchait. Droin fit le tour pour s'assurer de l'état de l'homme qui pendait, à cheval sur l'arçon. Il avait le crâne ensanglanté et avait tâché le cou de l'animal. Pourtant, quand Droin lui tourna le visage, l'homme cligna doucement des yeux, tenta de bouger les lèvres, sans résultat.

« De l'eau ? Tu veux de l'eau ? »

Droin détacha l'outre et avala une longue gorgée, sentant le liquide salvateur se répandre dans son corps tandis qu'il déglutissait. Il se pencha vers le nomade :

« C'est bon, tu peux en boire. Je m'assurais juste... »

En l'aidant à se relever, Droin réalisa que la blessure était au cou et à la gorge en fait, et le bras pendait bizarrement, certainement brisé. Il aurait préféré trouver un cadavre, le chameau lui aurait servi de monture pour s'enfuir. Quoiqu'il n'était jamais monté sur une telle bête. Tandis qu'il réfléchissait, le bédouin tenta de se redresser, vacillant et épuisé. Droin l'aida à retrouver une position moins inconfortable. L'homme lui fit signe de la main, un signe confus.

« Tu veux quoi ? À manger ? Tes sacs ? »

L'homme fit non de la tête tandis que Doin lui montrait un objet après l'autre. Il finit par attraper les rênes d'un doigt faible et les fit glisser dans la main de Droin.

« Monte » souffla-t-il péniblement. Droin recula, ce qui déplut au chameau qui fit un coup de tête pour se rapprocher du buisson qu'il dévorait paisiblement. Le bédouin insista :

« Monte... Je montrer route, toi mener chameau... »

Droin souffla. Il n'était déjà pas doué à cheval, mais à chameau ! Comprenant néanmoins qu'il tenait là un moyen de s'en sortir, il s'acharna un long moment pour tenter de grimper sur l'animal, escaladant sans grâce pour enfin se trouver à peu près installé sur son dos. Le bédouin était devant lui, et s'était laissé aller contre son torse. À demi conscient, il fit plusieurs appels de langue et tapa mollement du plat de la main pour arriver à décider la bête.

« Droit au soleil... » glissa-t-il, épuisé à Droin. Celui-ci s'efforçait de diriger l'animal qui les faisait rebondir en tous sens. Sans personne pour le tenir, le nomade aurait fini dans le sable du désert. Ils avançaient bon train, même si leur cavalcade était désordonnée.

Lorsque la nuit tomba, ils étaient proches des montagnes. Près d'un ravin. Droin descendit comme il le put, finissant sur les fesses mais en un seul morceau, et puis il aida l'homme finalement évanoui de douleur. Regardant l'animal qui semblait mâcher un souvenir de fourrage, Droin échappa un sourire désabusé et vint flatter l'animal.

« Bon, ça pourrait pas être pire, je crois, t'en dis quoi, gros machin ? »

Le chameau continua sa mastication et tourna la tête, indifférent. Sur la butte au-dessus de lui une silhouette venait d'apparaître. ❧

### Notes

La justice des états latins s'appuyait souvent sur des mutilations, de différents types, que ce soient de simples marques d'infamies (marquage au fer, tranchage du nez ou des oreilles) ou des peines encore plus effrayantes : membre amputé. En plus de la sévérité censée effrayer, cela donnait la capacité de facilement reconnaître les récidivistes, généralement condamnés à mort. On comprendra aisément la mention de fugitifs dans les chartes et des peines prévues pour ceux-ci dans les textes de loi.

### Références

Foucher Victor, *Assises de Jérusalem. Première partie : Assise des Bourgeois*, Paris: 1890.

Prawer Joshua, *Crusader Institutions*, New York : Oxford University Press, 1980.

Prawer Joshua, *Histoire du Royaume latin de Jérusalem*, Paris : CNRS Éditions, 2007.

Omphalos
--------

### Jérusalem, quartier Saint-Martin, aube du lundi 5 août 1157

Avant même que le soleil ne passe le Mont des Oliviers, Saïd ouvrit un œil. Quand le temps le permettait, comme cette nuit, il dormait sur la terrasse du bâtiment, tirant sa paillasse miteuse sous l'abri en feuille de palmier. Il respirait mieux ainsi, et avait plus de place, vu le bazar qu'il accumulait dans sa petite pièce depuis des années. Il habitait là depuis qu'il avait tout perdu, ses biens, sa famille, son argent, et un peu de son esprit. Il avait un long moment erré, jusqu'à finir dans la cité et avait commencé par mendier avant de dénicher ce logement, rudimentaire mais suffisant. Il avait aussitôt commencé son abracadabrante collection, alimentée par les arrière-cours abandonnées et les tas de détritus. Il veillait néanmoins sur le tout comme un cerbère et nul n'avait le droit de contempler son trésor.

Sauf peut-être le corniaud au poil jaunâtre, ras, dont la queue battait comme une trique sur le sol maintenant que Saïd était levé. Le chien passait toutes ses nuits avec lui depuis plusieurs mois, sans que Saïd n'ait cherché à l'attirer. Il ne lui avait d'ailleurs pratiquement jamais donné de nourriture. Peut-être lui aussi avait tout-il perdu et cherchait-il quelqu'un qui saurait le comprendre. Mais une chose dont Saïd n'était pas avare, c'était les caresses, et le cabot s'en contentait, l'œil humide.

Après s'être étiré, le vieil homme alla regarder où en étaient les dattes qu'il avait mises à sécher au soleil, en prévision de l'hiver. Il en avait obtenu une bonne quantité auprès d'un fermier des environs, contre quelques jours de travail. Il en tâta quelques-unes et les avala avec un gloussement de plaisir. Ces fruits lui rappelaient des souvenirs sucrés, anciens, flous et indistincts mais synonymes de plaisir. Un prénom lui revenait parfois, joie et douleur mêlés. Khadija.

Il frotta son thawb[^4] rapé et enfila ses savates. Aujourd'hui il allait voir si des commerçants n'avaient pas besoin de nettoyer autour de leur échoppe. S'enroulant le crâne d'un chiffon gris en guise de turban, Saïd empoigna son balai. Il posa également un sac sur son épaule, contenant son repas du midi, quignon de pain et reste de tourte, ainsi qu'une calebasse vide. Il passerait à la fontaine pour la remplir et en profiterait pour se frictionner un peu le visage, les bras et les mains, histoire d'être présentable. Le cabot tournait en tous sens, comprenant que le moment de rentrer dans la rue était venu. Le soleil affleurait à peine les reliefs orientaux que Saïd se dirigeait vers l'escalier. En passant vers la porte du garçon qui habitait à côté, il frappa deux coups secs du manche de son balai, comme le jeune le lui avait demandé auparavant. Il avait le sommeil plus lourd que le vieil homme.

Il glissa sans bruit dans l'escalier, devancé par le chien plein d'entrain. Sur un des paliers, deux hommes, des latins discutaient, l'un d'eux encore en chemise et pieds nus tandis que l'autre était habillé pour sortir.

« Salaam ! lança Saïd, souriant de ses quelques dents.

--- Le bon jour, Saïd ! » répondit les deux hommes de concert.

Le chien, soucieux d'éviter un coup de pied qu'il savait possible avec ces deux-là, s'esquiva promptement dans la volée suivante. Arrivé au rez-de-chaussée, Saïd dégagea la clef qu'il portait toujours autour du cou et déverrouilla la serrure. L'animal jaillit comme une flèche, partant en courant dans la ruelle de terre, parmi la poussière et les cailloux. Des cloches sonnaient non loin lorsque le vieil homme tourna le coin pour rejoindre la grande artère.

Déjà des travailleurs cheminaient, outils et sacs à la main, parmi les animaux de bât, les cavaliers pressés et les pèlerins admiratifs. Une odeur de poussière, de sueur, de bouse et de sucre monta aux narines du vieil homme. Jérusalem se réveillait. Clopinant tranquillement, il descendait vers la ville basse, en direction du sud, vers la colline de Sion. Il s'y trouvait toujours beaucoup de voyageurs, et donc de boutiquiers, en période d'été. Surtout par un beau jour comme celui-là. Il arriva à la porte de la cité, tenue par deux sergents à la mine fatiguée. Ils regardaient passer les convois d'approvisionnement des marchés qui venaient des jardins méridionaux. L'un d'eux examinait avec attention les sacs d'une charrette menée par deux gamins. Aucun ne jeta un regard à la silhouette voûtée qui passa la porte en clopinant.

### Jérusalem, porte de Sion, matin du lundi 5 août 1157

Baset soupira et fit signe aux deux gamins qui menait le petit chariot d'entrer, puis il se rapprocha de Guillemot, l'air las.

« La peste de ces vilains qu'en font qu'à leur tête.

--- Y z'avaient quoi ?

--- Rien, juste pois et fèves. Mais j'aime pas quand on voit passer marchandies sans savoir de sûr d'où elles viennent.

--- Pour des pois...» renifla l'autre, haussant les épaules.

Baset fronça le nez, mécontent, puis cracha dans la poussière. Il officiait comme sergent depuis bientôt vingt ans, et si son mauvais caractère et son peu de courage expliquaient qu'il soit toujours affecté à des tâches annexes, on ne pouvait lui reprocher de ne pas prendre son travail au sérieux. Il avait une haute opinion de tout ce qui avait trait à sa personne, et donc de son travail. Il se voyait comme le bras armé du roi. En l'occurrence, il n'était guère qu'un doigt de pied, et sûrement l'un des plus misérables, mais il avait le sentiment de faire réellement partie du corps royal.

Il s'étira le dos, le regard perdu vers le complexe de Sainte-Marie de Sion, puis dériva vers les pentes qui remontaient depuis la vallée de l'Hinnom. La verdeur du printemps n'était plus qu'un souvenir, sauf autour des zones soigneusement irriguées ici et là, ou aux abords des bassins, comme la piscine de Siloé. Baset aimait bien tenir cet accès. Il y avait nettement moins de passage qu'à l'ouest, à la porte de David, où la plupart des marchands se présentaient. Ici, c'étaient surtout des locaux, venus porter fruits et légumes aux marchés. Les bêtes passaient plutôt par la porte des Tanneurs, plus à l'est, pour rejoindre facilement le quartier des Bouchers.

Des pèlerins franchissaient aussi l'endroit en nombre, en direction des lieux de culte au sud de la cité, mais il n'y avait guère à s'en préoccuper. Eux-mêmes avaient l'esprit ailleurs et ils ne portaient ni armes ni marchandises à taxer. Baset revint s'appuyer contre le puissant vantail de bois, les bras croisés. Depuis la défaite humiliante du roi dans les environs de Panéas, la région était calme et cela faisait un moment qu'aucun voyageur n'avait porté de mauvaises nouvelles des confins du royaume.

Cela lui convenait parfaitement. Il avait suivi de temps à autre l'ost royal dans des campagnes, mais avait décliné chaque fois que c'était possible. Il était né à Jérusalem et aimait à y demeurer. Il y avait ses habitudes, sa famille. Sa femme, Garsende, était en outre une excellente cuisinière, et il n'aimait guère s'éloigner de sa table, sachant par avance qu'il devrait croquer du pain rassis et de vieux légumes, ou de la viande dont il pourrait réparer ses semelles.

Guillemot, tout en suivant du regard un âne bâté d'un chargement colossal de fagots de bois entrer sous l'arcade, se rapprocha de lui.

« Tu as entendu parler de la charte de casal proposée par les frères de Saint-Jean ?

--- Pour où ?

--- Un patelin loin au sud du royaume, Bête gibelin... »

Baset secoua la tête.

« Non. Ça t'intéresse ?

--- J'ai le frère de ma mie qu'a bien envie d'avoir son hostel. Il œuvre ici, comme coutelier fèvre, mais pour un autre. Il a sufffisamment de sapience en son domaine pour avoir son propre étal.

--- Fèvre ? Ça m'intéresse. Il est où ?

--- Pas loin de Saint-Étienne. »

Guillemot fouilla dans une bourse sous sa cotte et en sortit un petit canif pliant, qu'il tendit à Baset.

« Bois de sycomore pour le manche. »

Baset déplia la lame et la regarda un instant avant d'en éprouver le fil du gras du pouce.

« Il y a toujours de l'ouvrage pour le bon fèvre mon vieux disait. Je demanderai à ma sœur si elle a entendu parler de ce casal.

--- Tu la vois tantôt ?

--- Je sais pas, j'ai pas eu nouvelles depuis la Saint-Bouillant[^5], quand le Pierre est passé. »

Il rendit le couteau à Guillemot, qui le rangea aussitôt. Un groupe de pèlerins sortit de la cité, le chant des Psaumes s'attachant à eux tandis qu'ils descendaient la colline. Ils croisèrent un cavalier à l'allure martiale, sur un cheval de prix. L'homme était barbu, le teint cuivré à force de soleil, et un turban éclatant ornait son crâne, tandis que l'étoffe et la coupe de ses vêtements trahissaient le byzantin. Il avançait tranquillement, l'épée au côté. Baset prit sa lance pour se mettre en travers du chemin.

« Halte-là ! Que venez-vous faire en la cité le roi Baudoin ?

--- J'ai nom Stamatès Mprato, je suis garde mandatôr porter message. »

L'homme exhiba un document scellé depuis un petit sac en bandoulière. Baset ne savait pas lire mais il connaissait très bien les sceaux des officiels, et c'était là un laisser-passer en bonne et dûe forme. Il glissa de côté et invita le cavalier à entrer, un sourire figé sur les lèvres, sans aucun naturel.

« La bonne jounée, messire. »

Le soldat opina, rangea son document et talonna doucement son cheval. Sous l'arcade, le claquement des sabots ferrés résonna sur la caillasse.

« Foutus Grecs » ronchonna Baset, avant de cracher par terre après le passage du messager.

### Jérusalem, rue du Mont Sion, fin de matinée du lundi 5 août 1157

Le dos et les fesses en compote d'avoir voyagé sans répit depuis la veille, Stamatès menait sa monture rênes lâches parmi la foule de la cité. Plus il approchait du cœur, vers le Change latin, plus la presse était forte. Il avait entendu dire que l'été voyait un grand afflux de voyageurs, mais que ce n'était pas comparable à la foule de Pâques. Il savait que les cavaliers n'étaient pas toujours appréciés mais il n'avait pas envie de mener son cheval à la bride à travers une foule pareille. Au moins pouvait-il se fier à la puissance de l'animal pour faire s'écarter les badauds.

Beaucoup de personnes profitaient de la moindre chaleur le matin pour faire leurs achats, bénéficiant également de la fraîcheur des produits et de la possibilité de choisir les plus belles pièces. Des valets affairés, porteurs de sacs, de paniers et de simples messages couraient en tous sens parmi les ménagères et les voyageurs en goguette. Des soldats, fort rares, se baladaient, les pouces dans le baudrier. Normalement les armes étaient interdites dans l'enceinte de la ville, mais nombreux étaient les hommes servant un baron ou l'autre qui étaient exemptés de cette interdiction ou qui s'en affranchissaient d'eux mêmes. La situation de guerre quasi permanente n'incitait pas à renoncer à son arme. En obliquant à l'ouest pour éviter Malquisinat, Stamatès sentit les bonnes odeurs venir lui chatouiller les narines. Depuis le matin, il n'avait avalé que des dattes et de l'eau croupie, et son ventre gargouilla à l'idée d'avaler autre chose. Il siffla un gamin qui passait au milieu d'une ribambelle et exhiba une petite pièce d'argent sous son nez, tout en montrant un étal proche.

« Petit, toi acheter fougasse pour moi et tu pourras en avoir partie. »

Le gamin exhiba un large sourire, tendit la main pour la pièce puis galopa jusqu'à l'éventaire de la marchande. Lorsqu'il revint, il tenait contre sa poitrine une impressionnante brioche dorée aux senteurs de beurre.

Stamatès en arracha une portion suffisante pour que le petit puisse faire preuve de largesse avec ses compagnons, la lui lança et mordit dans le reste, faisant avancer son cheval au pas. Alors qu'il remontait la rue de David, il vit au loin une troupe armée assez imposante, avec lances et bannières qui remontait en sa direction. Il tourna dès qu'il le put vers le nord, se rapprochant du Saint-Sépulcre, et donc du palais. Il ne trouvait pas l'endroit très pratique d'abord et aurait largement préféré être installé dans la forteresse dont les murailles étaient visibles près de la porte ouest. Il avait entendu parler du projet d'y déplacer la demeure royale, et il approuvait. Devoir sillonner ainsi les rues emplies de pèlerins n'était vraiment pas chose aisée. Sans compter qu'il se trouvait parmi eux les chrétiens les plus fanatiques, qui s'en prenaient à tous ceux qui n'étaient pas latins.

Stamatès eut à écoper plusieurs fois d'insultes lancées de loin et même de crachats, vite regrettés quand le soldat tourna ses yeux de vétéran en tout sens. Heureusement pour tous ces perturbateurs, il avait trop versé le sang pour y prendre du plaisir, et demeurait calme en toutes circonstances. C'était d'ailleurs peut-être la raison qui faisait qu'il voyageait souvent aux côtés d'un mandâtor. Il n'était pas un va-t-en guerre, malgré son allure martiale.

Lorsqu'il arriva enfin dans la cour où laisser son cheval, un chevalier au blanc manteau venait de démonter. L'homme avait le visage dévasté, il était difficile de reconnaître le nez de la bouche dans ce qui n'était plus qu'amas indistinct. De sa seule main, l'autre bras s'arrêtant au coude, il s'employait à dénouer un sac de sa selle et salua d'un signe de tête le Byzantin avant de s'engouffrer dans un couloir. Stamatès descendit de la selle avec un peu de raideur et jeta ses rênes à un palefernier, engloutissant les restes de brioche. Il en avait trop mangé, finalement, et se sentait un peu écœuré, le ventre en débâcle. Peut-être serait-il prudent de passer aux latrines avant d'aller délivrer son message aux officiels.

### Jérusalem, palais royal, après-midi du lundi 5 août 1157

Armand n'était jamais à l'aise dans les missions qui ne le menaient pas sur le terrain. Il était néanmoins évident qu'handicapé comme il l'était, il ne valait plus grand chose comme soldat. Il espérait se voir confier au moins des missions d'encadrement, à défaut de pouvoir charger lance au poing. Tout plutôt que de se voir relégué à l'arrière. Il regrettait parfois d'avoir été sauvé après sa blessure, condamné à survivre en étant moins qu'un homme, lui qui avait vécu comme un soldat depuis son enfance. Arrivé devant la porte qu'on lui avait indiquée, il pénétra sans attendre, découvrant le connétable Onfroy à sa table de travail. Celui-ci avait sa tête des mauvais jours, appuyé des deux mains devant une pile de papiers et de tablettes de cire. Plusieurs clercs et une poignée de chevaliers étaient présents. Armand salua de façon ostensible et tendit le document dont il était porteur au connétable qui le rompit et le parcourut rapidement, avant de le passer à l'homme à sa droite et de fixer le templier.

« Le roi fait tout ce qu'il peut pour aider à la libération du sire mestre Bertrand, soyez-en assuré ! Et je bouille personnellement d'assavoir que c'est à cause de Panéas qu'il est tombé.

--- Certes pas, sire connestable. Ce sont les Mahométans les seuls à blâmer. »

Onfroy s'accorda un sourire triste, reconnaissant de l'indulgence du vétéran face à lui. Il aimait les soldats et les hommes d'armes, n'appréciait que ceux qui savaient se défendre l'arme à la main. Il n'avait aucune estime pour ceux qui se tonsuraient ou préféraient la balance du marchand. Pour lui, le combat était le révélateur de l'homme. Armand portait sur son visage, sur son corps, la preuve de sa vaillance. Le connétable se tourna vers un des clercs.

« Servez donc à boire à ce chevalier de la milice ! »

Il indiqua d'une main un escabeau et s'assit dans le même temps, poussant les papiers devant lui. Il semblait épuisé, mais prenait toujours plaisir à échanger avec ceux qui fréquentaient les champs de bataille. Il avait énormément de respect pour les soldats du Temple, dont il connaissait la valeur, malgré leur indépendance qui l'irritait parfois.

« Je suis revenu aussi vite que possible, le roi ne tardera pas. Norredin a levé le siège. Mais il n'y a aucune trêve signée. Nous avons encore l'avantage d'une forte troupe. Je n'ai pas le temps de rédiger missive, et le roi ne sera pas là avant plusieurs jours. Vous entendrez donc ce que je peux en dire.

--- Les frères sont surtout inquiets du sort du sire mestre.

--- J'entends bien. Nous n'avons pu avoir nulle garantie en ce sens, c'est une des raisons qui fait qu'aucun accord n'a été trouvé. Le roi n'acceptera rien sans certitude sur ce point. »

Armand hocha la tête, soulagé d'entendre cela. Le destin des Templiers pris par les adversaires musulmans était généralement scellé en quelques heures, sauf si on préférait auparavant les humilier par un défilé honteux, plutôt que d'emporter leurs têtes comme trophées. Mais le grand maître était un otage important, il pouvait s'avérer plus intéressant d'en négocier la libération, peut-être contre une formidable rançon. Armand se morigéna intérieurement de penser qu'il valait mieux Bertrand de Blanquefort que le vieil André de Montbard, un des fondateurs de leur ordre, retiré l'année précédente comme simple moine à l'abbaye de Clairvaux et mort avant la Noël. Au moins lui aura été épargnée la tristesse d'une pareille nouvelle. La voix du connétable le ramena à la réalité.

« Vous pouvez assurer les frères de notre soutien le plus entier. Le roi n'aura de cesse de faire libérer ceux qui sont tombés pour le sauver. Il a bonne mémoire ! Dès son retour, je pense que vous aurez des nouvelles du palais à ce sujet. »

Armand se leva, reposa son verre et salua les présents, avant d'aller rechercher sa monture. Il rentra tranquillement au Temple, par la grande artère qui traversait la cité d'est en ouest. Dans sa partie orientale, on la nommait la rue du Temple, et son ordre y possédait une très grande quantité de boutiques et de demeures, y apposant parfois son symbole dans la pierre. Il soupira. Il craignait un jour de n'être plus qu'un de ces collecteurs de rente, un de ces commandeurs chargés de faire converger les richesses de leurs domaines vers le cœur de la cité de Jérusalem, pour les transformer en une formidable machine de guerre.

Tandis qu'il avançait, il fut salué avec emphase par un gros bonhomme à la mine réjouie, à qui il répondit d'un discret signe de tête, s'attirant un large sourire et une litanie de bénédictions tandis qu'il s'éloignait. Le blanc mantel attirait toujours de vives réactions de la part de ses ennemis comme de ses amis.

### Jérusalem, rue de David, fin d'après-midi du lundi 5 août 1157

Gringoire la Breite postillonnait autant qu'il vociférait, le visage éclairé de joie, comme un enfant.

« Vous avez vu ce brave chevalier ? En voilà des héros chers à mon cœur. J'ai brûlé plusieurs bons cierges pour eux auprès du tombeau du Christ, loué soit son nom »

Il accompagna sa phrase d'un rapide signe de croix avant de se pencher de nouveau sur le morceau d'étoffe que le négociant venait de déplier depuis ses étagères. Cela faisait déjà un bon moment que le gros homme, son nez aviné penché sur les produits, demandait à voir chaque lot l'un après l'autre. Sa belle mise attestait de son aisance, sans quoi le boutiquier l'aurait sûrement envoyé au diable depuis longtemps. En outre, il portait une croix cousue sur la poitrine, et il n'était jamais bon de s'aliéner ceux qui venaient de fort loin, souvent en nombre et fort solidaires les uns des autres.

« C'est que mon Ermenjart va avoir petiot ! Alors je tiens à la vêtir comme princesse, ainsi qu'il sied à belle damoiselle de mon cœur ! »

Il se râcla la gorge et ajouta, d'une voix moins tonitruante :

« D'autant qu'on ne va pas repartir avec le marmouset en ses entrailles. Et pendant ces mois d'attente, il va sans dire que je ferai visitance à ceux des boutiquiers qui ont bons produits ! »

Tout en disant cela, Gringoire tâta l'étoffe, en apprécia la souplesse et la douceur. Il n'avait rien contre les textiles nouveaux, mais il les trouvait bien inférieur à ses étoffes de laine fétiches. Ici on n'en avait que pour le coton, le poil de chameau, de chèvre, des fibres sans grand intérêt. Il y avait bien la soie, qu'il trouvait moins dispendieuse que dans ses contrées. Mais bien qu'il en reconnût la brillance, la souplesse, la chatoyance, il préférait les belles productions anglaises, souples et chaudes, délicates et solides. Mais ici cela serait extraordinairement chanceux d'en trouver, et sûrement à des prix invraisemblables. Reniflant de dépit, il indiqua au vendeur de tout remballer. Rien ne lui convenait.

Il était heureux de se trouver à Jérusalem et impatient de serrer contre lui son enfant, mais il commençait à avoir le mal du pays. Rien qu'à l'idée qu'il devrait patienter un hiver ici, la nostalgie s'emparait de son âme. Il avait trouvé une jolie demeure à occuper, où il profitait d'un confort bien supérieur à ce qu'il connaissait chez lui. Mais justement, ce n'était pas chez lui, et il le ressentait vivement. Tout son être lui criait qu'il n'était pas fait pour ce pays de chaleur et de roc, de poussière et de sang. Il n'osait guère aller sur les chemins, maintenant que leurs visites aux lieux saints étaient accomplis, pour la plupart.

Chaque nouvelle qu'il entendait parlait de ces maudits Turcs qui tuaient, volaient, ravageaient. Pire que des sauterelles ! Non, décidément, pour Gringoire la Breite, Dieu avait choisi un drôle de pays pour venir au monde et mourir. Tout à ses pensées, il ne remarqua pas d'abord qu'il arrivait aux abords de Malquisinat, mais la senteur de pommes sucrées, de miel et d'épices le fit saliver. Il admettait que sur un point en particulier, il ne connaissait aucun égal à Jérusalem. Les mets y étaient fort variés, et richement assaisonnés, pour un coût modique. Sa cotte lui semblait d'ailleurs avoir rétréci depuis qu'ils étaient installés. Il faut dire qu'il ne ratait jamais l'occasion de s'offrir une petite friandise quand il passait aux abords de Malquisinat. Et, comme par hasard, le quartier était au cœur de la cité, là où l'on se devait de passer plusieurs fois par jour.

Tandis qu'il dévorait un pâté de viande au goût prononcé de poivre, il faillit buter contre un des corniauds qui traînaient toujours dans le coin. Les perspectives de dénicher de quoi se nourrir les attiraient depuis les coins les plus éloignés de la ville. Gringoire aimait bien les chiens, il arracha une petite part de sa tourte et la jeta au cabot au poil jaunâtre, qui le happa en plein vol.

« Que voilà bien habile mâtin » s'amusa Gringoire, avant de reprendre son chemin, dévorant à pleines bouchées un en-cas qui aurait fait le bonheur d'une famille pour un jour gras.

Comprenant qu'il n'obtiendrait pas plus, le chien s'arrêta, remuant toujours de la queue. Il se gratta l'oreille et regarda autour de lui. Puis il partit comme une flèche en direction du sud. Il savait à quelle heure Saïd rentrait habituellement, et ne tenait pas à rater l'accès à son coucher, ni sa pitance du soir. ❧

### Notes

Sans que cela ne soit exactement le même exercice, ce Qit'a est en rappel d'une formule que j'avais créée voilà des années pour le magazine Histoire Médiévale, sous le pseudonyme de Paul Frossenet (anagramme de « Pour les enfants ») : *La journée de Monsieur Bonhomme*. Ici nous voyons plusieurs personnages se croiser, mais cela se déroule aussi sur une seule journée. J'avais inventé cette rubrique car je savais les lecteurs friands de vie quotidienne, de celle qu'il est difficile de s'imaginer sans éplucher de nombreux ouvrages scientifiques ou des sources difficiles d'abord.

Je pensais que cela intéresserait tout particulièrement les enfants, sans imaginer que ce serait souvent la rubrique d'entrée dans le magazine, celle par laquelle la plupart des gens commençaient. La création d'Hexagora poursuit la dynamique initiée voilà bien longtemps pour la presse. Et peut-être qu'un jour, de nouveau, des images seront dessinées autour de ces récits, comme Callixte l'a fait pour quelques textes d'Ernaut au début, et comme Jean Trolley en a tant dessinés pour Monsieur Bonhomme.

### Références

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Boas Adrian J., *Domestic Settings. Sources on Domestic Architecture and Day-to-Day Activities in the Crusader States*, Leiden et Boston : Brill, 2010.

Hostel et parentèle
-------------------

### Jérusalem, parvis du Temple, fin de matinée du samedi 9 janvier 1148

Yeux fermés, tête en arrière, Raoul laissait les flocons caresser son visage, effleurer sa langue où ils se dissolvaient en une rosée glacée. Il entendait le crissement des pas autour de lui, dans la neige légère fraîchement tombée. Ses camarades s'affairaient à construire des cibles qu'ils mitraillaient ensuite de projectiles soigneusement façonnés. Il cligna plusieurs fois des yeux, fixant toujours l'immensité crayeuse au-dessus de lui. Puis frotta de la manche son nez rougi, souriant aux anges. La ville, habituellement bruyante, disparaissait dans la blancheur des cieux et des flocons et se terrait, silencieuse, camouflant la vie dans ses entrailles réchauffées à grand renfort de braseros et de foyers crépitants. Le petit garçon secoua la tête, débarrassa la capuche de sa chape du froid duvet, puis rejoignit ses compagnons de jeu.

Les cibles étaient en place, certaines ornées d'hastes aussi impressionnants que dérisoires. Le jardin environnant le Temple du Seigneur offrait quantité d'arbustes devenus, le temps d'un matin, pourvoyeurs d'armes en bois et de supports ennemis. Emlot, son aîné de plusieurs années, posa une main amicale sur son épaule :

« Tu n'as pas envie de mettre bas tous ces félons, frérot ? »

Raoul haussa les épaules. Il se sentait mélancolique, des sentiments confus l'habitaient depuis plusieurs jours, et il ne savait comment exprimer ce qui l'inquiétait. Son grand frère, qu'il percevait comme un géant débonnaire, arrivait toujours à le réconforter, à découvrir l'histoire qu'il aimait à entendre avant de s'endormir. Pourtant, il ne trouvait comment lui décrire son malaise.

« De coutume, tu goûtes fort les jeux de neige. Aurais-tu désir de rentrer ?

--- Je sais pas. Non. »

Emlot s'accroupit face à lui, le tenant par les épaules.

« Me diras-tu seulement ce qui pèse sur ton cœur ? Est-ce à propos de Guy ? »

Raoul se mordit la lèvre, puis acquiesça doucement.

« Je me disais qu'il a jamais connu la neige...

--- Tu crois qu'y a pas occasion de faire des bonhommes au parmi du Ciel ?

--- J'ai jamais entendu le curé dire ça.

--- Auquel cas il faudrait peut-être lui demander. J'encroie volontiers que Dieu te permet si simples plaisirs en son Paradis. »

Le garçonnet fit claquer sa langue, les sourcils froncés.

--- Y va être perdu.

--- Moult enfançons sont là-haut avec lui. Il saura se faire des amis. C'était fort amiable bébé, tu le sais bien. »

L'adolescent se releva et soupira. Leur petit frère était mort de fièvres soudaines à la fin de l'automne, alors qu'il n'avait pas trois ans. En quelques jours, il s'était éteint et avait été porté en terre aux côtés de leurs ancêtres, dans les reliefs à l'orient de la ville. De telles cérémonies pour des nourrissons, de très jeunes enfants, étaient malheureusement fréquentes, répandant l'angoisse dans les familles.

« Nous pourrions faire un joli bonhomme et ensuite prier la Vierge pour qu'elle le montre à Guy. Qu'en dis-tu ? »

Un timide sourire plissa les joues rosies de froid.

« Je vais aller quérir jolis cailloux pour les yeux alors ! »

Puis sous le regard attendri de son frère, Raoul s'élança vers les escaliers, en quête de pierres brillantes, ou colorées, comme il s'en trouvait parfois au pied de l'ancienne église. Emlot toussa pour s'éclaircir la voix et chercha un tas de neige qui n'ait pas été encore piétiné ou dévasté par la horde enthousiaste d'enfants. Puisse Dieu leur faire connaître à tous de nombreux autres hivers, songea-t-il en se mettant en marche.

### Jérusalem, cloître du *Templum Domini*, matin du mercredi 6 septembre 1150

« *Alpha Beatus Creator Deus Eternitas*... »

Les petites voix aigrelettes ânonnaient avec sérieux, si ce n'est enthousiasme, tandis que la férule du chanoine désignait les différents signes peints sur une large pièce de cuir. Il hochait la tête avec emphase, le visage fermé et austère malgré la malice pétillant dans ses yeux clairs. Il savait qu'il ne faisait pas bon montrer trop de complaisance à une bande d'enfants quand on s'efforçait de les éduquer. Lorsque toutes les lignes furent énumérées, avec plus ou moins de bonheur, le clerc félicita ses élèves et pointa de nouveau la première lettre.

« Qui d'entre vous connaît un mot ou un texte commençant par cette lettre. Parmi les plus petits... »

Son regard se porta aux premiers rangs, où se tenaient les plus jeunes. Sa question provoqua de grands écarquillements d'yeux, des bouches ouvertes et des mordillements de joue sans pour autant générer quoi que ce soit de valide. Peu à peu, il se tourna vers les plus âgés, parmi lesquels se trouvait Raoul, sautillant sur son escabeau, impatient de lâcher la réponse. Le maître sourit et le désigna.

« Achard. C'est le nom du bébé de ma sœur. »

La sincérité de la réponse fit jaillir un sourire sur le visage du chanoine.

« De certes, mais j'attendais autre réponse, garçon. Quelqu'un saura-t-il parmi vous ?

--- Moi !, échappa avec vigueur un petit blond au regard renfrogné.

--- Alors, parle, nous t'écoutons.

--- *Ave Maria, grâce à pleina, dominuste cum, bene dicte à tout*

--- Juste, mon garçon. Encore que je ne sois certain que tu le possèdes parfaitement.

--- Si, père Thibault. Je le sais fort bien, comme le *Paterne austère*, car c'est moi qui surveille la cuisson des œufs ![^6] »

Le chanoine renifla, légèrement agacé. L'apprentissage du latin constituait une seconde étape dans l'enseignement, et bien des chrétiens étaient incapables de le comprendre, voire de le prononcer correctement. Ce qui ne lui rendait pas moins désagréables à l'oreille les déformations incessantes des formules écorchées.

« Nous répéterons vos prières ainsi qu'il sied à la fin du cours, garçon. Veille à bien entendre quand je prononce et à répéter avec soin.

--- Moi je serai le parrain d'Achard pour son baptême, Lorette elle a dit. Je réciterai le *Credo*, précisa Raoul.

--- Voilà bien sérieux office. Sais-tu qu'il apprendra de ta bouche cette prière essentielle entre toutes ? »

Raoul hocha la tête avec gravité, conscient de l'importance de la tâche qui lui était dévolue, sans pour autant en saisir les implications qu'y mettait le clerc.

« Saurais-tu nous montrer la lettre par laquelle commence le *Credo*, en ce cas ? Vu que tu as parlé sans en avoir licence... »

Un peu confus de se faire rabrouer, même gentiment, devant ses camarades, le garçon se leva et vint pointer du doigt, sans aucune hésitation, la troisième lettre de la première rangée. Le maître approuva d'un signe de tête silencieux et attendit qu'il soit retourné à sa place pour continuer. Il distribua aux plus petits de vieilles tablettes de cire fatiguées, ainsi que des stylets métalliques pour qu'ils s'exercent à tracer les motifs tandis qu'il s'occuperait des plus âgés.

Parmi ceux-ci, un grand nombre était capable de lire correctement, et une partie montrait des dispositions pour passer au latin. Il demeurait la question de savoir si leurs parents accepteraient d'en faire des clercs. Ils étaient en majeure partie fils d'artisans et de commerçants, de bonne naissance, mais pour qui le destin était décidé depuis longtemps. Ils hériteraient de l'activité familiale ou y apporteraient leur concours.

L'apprentissage des lettres dispensé par les religieux visait à compléter la découverte des chiffres faite en famille, de façon à commercer plus adroitement. Quasi jamais n'était-il envisagé d'en faire un érudit ou un serviteur de Dieu. Quelques exceptions demeuraient, lorsqu'un membre de la famille était lui-même tonsuré. Il avait parfois assez d'influence pour arracher à sa condition le jeune homme prometteur. Comme le chanoine Pépin, parrain d'un des aînés de Raoul, Guillaume.

Tibault l'avait eu comme élève jusqu'à son départ pour les prestigieuses écoles d'Europe. Chartres, Paris... Des noms qui sonnaient comme des chants angéliques aux oreilles du vieux tuteur, plus habitué à des langues râpant le latin le plus élémentaire. C'était sa plus grande fierté, et il n'avait plus eu, depuis près de dix ans, l'occasion de voir un de ses pupilles en si bonne voie. Il s'accommodait néanmoins fort bien de sa tâche, qu'il appréciait bien plus que les activités foncières et administratives que nombre de ses confrères devaient affronter. Au moins il avait le plaisir d'être confronté à de jeunes âmes innocentes, pétries de fraîcheur et de naïveté. Et si Dieu les avait faits maladroits à l'énoncé de la langue latine, cela l'incitait à redoubler d'efforts, en usant de la badine ou du pot de miel selon les caractères.

### Jérusalem, piscine de Siloé, soir du mardi 20 mars 1151

Un ciel gris chargé de nuages sales menaçait de déverser son fardeau humide à tout moment, mais Raoul n'en avait cure. Il était revenu du cimetière du Lion sans ouvrir la bouche et s'était esquivé avant de rentrer à la maison. Des familiers, des amis, y seraient reçus pour évoquer une dernière fois la mémoire de Rose. Sa mère. C'était une femme âgée, il le savait, mais cela ne rendait pas sa douleur moins prégnante. Il l'avait vue s'éteindre peu à peu, devenue immobile avant de se faire voler la parole, pour finalement quitter ce monde sans un mot.

Il refusait de garder d'elle un tel souvenir. Elle était généralement pleine d'entrain, affrontant les corvées sans fatigue, et les coups du sort sans se plaindre. Elle insufflait la vie dans leur maison, savait consoler d'un sourire ou réprimander d'un regard.

La religion n'était pas d'un grand réconfort pour un gamin d'une dizaine d'années, et il ruminait son malheur en jetant des pierres dans le bassin de Siloé, adossé à une des colonnes du portique.

« Tiens, t'es là, le tanneur ? »

La voix qui l'avait arraché à sa rêverie mélancolique était celle de Frogier, un fils de négociant spécialisé dans les peaux. Il traitait beaucoup avec la famille de Raoul et les deux enfants, qui avaient le même âge à quelques mois près, appartenaient à la même bande depuis des années.

« J'ai appris pour ta mère. Désolé. »

Il vint s'asseoir à côté de son camarade et entreprit de s'associer à ses lancers de cailloux.

« Tu veux pas venir avec moi ? Les frères de la Milice font entrer leur troupeau. Y'a des milliers de bêtes, pour le moins!

--- Ils sont déjà de retour des pâtures ?

--- L'Annonciation est pas si loin. Et pis y se murmure que la trêve avec Damas pourrait bien finir ce printemps. »

Pour ces jeunes gens de Jérusalem, la guerre était familière, sans qu'ils aient eu à la subir directement. Ce n'était que péripéties qui touchaient les puissants, et parfois une connaissance malchanceuse qui avait croisé le chemin de la mauvaise troupe. La plupart du temps, ils n'en avaient qu'une idée floue et confuse, tout en estimant que cela concernait avant tout ceux qui faisaient des armes leur profession. Pour l'attrait de l'aventure tout autant que le désir des richesses selon certains.

« On verra peut-être bel étendard, allons-y ! » lança soudain Raoul.

Ils galopèrent rapidement sur le flanc oriental des reliefs, en direction de la porte au débouché de la vallée du Cédron. Avant même d'y parvenir, ils entendirent le raffut et goûtèrent la poussière des milliers de sabots qui frappaient le sol rocailleux.

« Comment font-ils pour que tout tienne en leur castel ?

--- On dit qu'ils ont le Temple du roi Salomon.

--- Ouais, et même y conservent son trésor d'or et de soie, rétorqua Raoul, sarcastique.

--- Nan, sérieux. Même que mon frère y est déjà entré. C'était le palais du roi Godefroy, il est immense. »

La richesse et la réputation des frères de la Milice du Temple, qu'on commençait à appeler les Templiers, n'avaient d'égale que la curiosité dont ils faisaient l'objet. En quelques décennies, ils étaient devenus une puissance majeure du royaume, et leurs rangs comptaient parmi les plus vaillants combattants. Leur renommée faisait parfois briller d'espoir les yeux des rêveurs, comme ceux de Frogier.

« Plus tard, je goûterais fort de combattre parmi eux, lance au poing et heaume en chef.

--- Aucune chance à ça. Tu y finirais responsable des latrines, au mieux. Il faut être chevalier pour tenir le glaive en leur sein.

--- On m'a dit qu'ils acceptaient qui voulait les rejoindre.

--- Peut-être bien, mais il me serait surprise que ton vieux voie ça d'un bon œil. »

Frogier soupira longuement puis cracha dans la poussière, les yeux toujours rivés sur le vaste troupeau qui n'en finissait pas de défiler en contrebas.

« N'empêche, un jour, je porterai le blanc mantel. »

Raoul jeta un coup d'œil jaloux à son ami. Malgré sa cotte usée et recousue, ses chaussures fatiguées, il enviait cette certitude. Lui aussi rêvait de grands espaces, de voyages et de hauts faits. Mais il n'arrivait jamais à franchir le pas, ni même exprimer à voix haute ces désirs qu'il pensait coupables. Il était fils de tanneur, et n'osait souhaiter autre chose pour son avenir. Sa mère seule aurait su comprendre et encourager ses timides initiatives. Mais elle gisait désormais dans un linceul, couverte de terre froide, à quelques lieues de là. Et lui finirait donc simple travailleur dans la tannerie familiale, comme la plupart de ses frères.

### Jérusalem, quartier des tanneurs, veillée du lundi 4 avril 1155

La chaleur de la journée s'était évanouie avec les derniers filaments carmin du ciel et on s'était empressé de fermer les volets de bois. La clameur des rues laissait la place aux rumeurs domestiques, le chant criard des porteurs d'eau avait disparu au profit du murmure des berceuses enfantines. Dans la vaste pièce chaulée au sol de brique, les senteurs d'huile d'olive des lampes s'ajoutaient aux relents du repas, gros pain de froment rassis, potage épais et fromage fort.

Autour de la grande table, c'était dorénavant Durant qui tenait le haut bout. Il ne restait que la fratrie, une partie d'entre elle du moins. Emlot était assis à sa droite, à côté de Raoul, face à Lancelot et Renaudin.

Les bancs paraissaient désormais bien vides. Avec le mariage de Lorette, puis celui de Gaude, l'année précédente, ils s'étaient résolus à embaucher du personnel domestique. Deux sœurs, vieilles filles trop pauvres pour être dotées, s'occupaient de leur maison, de leur feu et de leurs vêtements. Elles partageaient leur repas, sans jamais se risquer à prononcer un mot, courbées par les ans et la vie difficile. Puis elles s'esquivaient en silence, abandonnant la famille pour la veillée.

Ce soir, malgré le décès récent du père, l'ambiance était plutôt festive. Ils avaient reçu une lettre de Guillaume, parti des années plus tôt en France se former auprès de grands maîtres. Emlot et Durant étaient seuls à s'en souvenir, mais le destin exceptionnel de l'étudiant rejaillissait sur chacun, qui s'en faisait un héros à sa mesure. Et, pour ajouter à cela, Durant avait cédé à l'insistance d'Emlot et Raoul allait commencer d'ici peu son apprentissage chez un écrivain de la Cour des Bourgeois du roi.

Durant renâclait, comme leur père avant lui, à se séparer d'un travailleur gratuit, mais il avait fini par admettre qu'un frère bien introduit dans l'administration royale constituerait un atout pour la famille. Raoul avait appris la nouvelle ce soir, lors du repas, et l'avait accueillie avec allégresse. Puis il s'était vu confier la tâche de leur lire à tous la prose de Guillaume, tandis qu'ils dégustaient un vin légèrement aromatisé en guise de digestif, avec des biscuits secs à la cannelle.

Raoul déplia le document avec soin, comme s'il craignait de l'abîmer. Ils ne recevaient que rarement des lettres, sans toujours savoir si c'était dû à la faible fréquence des envois ou à la disparition des missives sur le trajet. Il connaissait néanmoins bien l'écriture élégante de Guillaume, ses formules ampoulées et ses constructions alambiquées. Érudit, il avait été façonné par ces années à étudier dans l'ombre des cloîtres et cela transpirait à travers ses mots. D'ailleurs, parmi ses frères, nul ne doutait qu'il finirait abbé voire évêque. Son parrain, le chanoine Pépin, ne ménageait pas ses efforts pour soutenir la carrière à venir du jeune homme.

« Il parle de quitter le royaume de France, qu'on lui conseille d'aller suivre les enseignements de maîtres en droit civil à Bologne...

--- Bologne, c'est où, ça ?, grogna Renaudin. Y va encore falloir lui envoyer de quoi se payer long voyage !

--- Chaque besant envoyé à notre frère est utilisé à bon escient, le réprimanda Durant. Tu saurais cela si tu avais pris la peine d'étudier plus sérieusement.

--- Tu parles, y'avait plus guère de monnaie pour m'envoyer à Châtre ou Paris, comme lui, quand j'ai été en âge.

--- C'est Chartres, la ville où il a commencé, répliqua Durant avec dédain. Ferme donc le vidoir qui te sert de bouche. »

Puis il fit signe de la main à Raoul de continuer la lecture. Guillaume détaillait les maîtres dont il découvrait les enseignements, leur donnant des indications sur les cursus qu'il suivait, les préceptes qu'on lui inculquait. Il tentait de les rendre compréhensibles, sans se rendre compte qu'ils n'étaient que bien peu à appréhender autant de savoir conceptuel.

« Il nous remercie de l'avoir prévenu du décès de père, il prie pour son âme chaque jour et a fait dire des messes pour nos parents. »

Puis il esquissa un sourire taquin et lança une œillade à son aîné :

« Il pense aussi qu'il faudrait que tu ne perdes pas de temps avant de te marier. Qu'un hostel n'a pas d'âme sans épouse qui s'en occupe. Seulement, selon lui, la difficulté est de trouver une femme qui ait les bonnes qualités et le tempérament qui sied à sa fonction. Il nous conseille d'aller en discuter avec le chanoine Pépin.

--- Un chanoine pour conseiller en femmes, notre frère est bien un clerc » se gaussa Emlot.

La saillie fit rire la tablée, et Raoul allait enchaîner lorsque Durant confirma ce que chacun savait sans jamais en parler.

« J'ai déjà pourpensé la question de toute façon, et j'ai bon espoir de conclure accord prochainement.

--- Alors ce sera Clarice ? risqua Emlot.

--- Si son père en est d'accord, oui. »

Chacun acquiesça à sa façon, sachant que sa vie serait bouleversée. Une maîtresse de maison allait prendre les clés du lieu, veiller sur le cellier et les linges, assurerait la lignée. Eux tous, d'héritiers possibles, ils deviendraient des auxiliaires, plus ou moins acceptés, appréciés.

Raoul était heureux de ne pas avoir à rester ici, une autre carrière s'offrant à lui. Demeurer dans l'hôtel familial équivalait à renoncer au mariage, très certainement, ainsi qu'à espérer une descendance à qui léguer ses biens. Lequel de ses trois autres frères endurererait un tel destin ? ❧

### Notes

Au Moyen Âge, il était de coutume de réciter trois *Ave Maria* et trois *Pater Noster* pour estimer le temps de cuire un œuf à la coque. L'apprentissage de ces deux prières se faisait donc assez rapidement en milieu domestique, sans que la signification en soit bien claire.

Les termes d'hôtel et de parentèle désignaient les personnes de la famille qui, respectivement, habitaient sous le même toit et qui étaient en dehors. Tout cela était regroupé sous le concept de la familiarité. Ce terme pouvait englober des individus avec qui les liens de sang étaient assez distants, voire inexistants, pour peu qu'il y ait peu ou prou des relations symboliques (belle-famille, parrains/marraines, etc.) ou que la personne soit considérée comme faisant partie de la maisonnée (comme un serviteur, sa descendance, etc.).

### Références

Alexandre-Bidon Danièle, « La lettre volée. Apprendre à lire à l'enfant au Moyen Âge » dans *Annales. Histoire, Sciences Sociales*, 1989, vol.44, no 4, p.953-992.

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Boas Adrian J., *Domestic Settings. Sources on Domestic Architecture and Day-to-Day Activities in the Crusader States*, Leiden et Boston : Brill, 2010.

Universelle aragne
------------------

### Tripoli, demeure de Willelmo Marzonus, fin de journée du samedi 10 novembre 1156

Willelmo Marzonus se trémoussa sur son siège, comme s'il y découvrait soudain une punaise. Il soupira longuement et tourna son regard vers la mer, où les franges écumeuses se teintaient de fauve sous un soleil déclinant.

« J'aurais eu espoir de te voir à mes côtés pour mes voyages au printemps.

--- Comprends, je suis encore jeune et en pleine santé. Il me semble que je pourrais amasser butin avant de te rejoindre.

--- J'ai entretenu le même rêve et me voilà avec pilon de bois en guise de mollet !

--- Tu as fait le soudard bien assez auparavant, avoue-le. Je veux juste te rejoindre comme associé et pas comme valet. »

Enrico Maza avala une gorgée de l'excellent vin que son ancien compagnon d'armes lui avait servi. Il avait envisagé que l'entrevue serait difficile, mais il ne pensait pas que cela l'affecterait autant de décevoir celui qui l'avait soutenu durant toutes ses jeunes années. Il se pencha en avant, coudes sur les genoux, le verre à la main et tentant de prendre un air engageant.

« Je vais te confier mon pécule dès maintenant. Il n'y a nul intérêt à le laisser dormir et je ne vais pas le traîner sur les champs de bataille avec moi.

--- C'est de ta lame et de ton bras que j'avais grand besoin. Il est difficile de trouver bons soudards par ici.

--- En effet, c'est ce qui m'a convaincu de rejoindre l'ost de Raymond le Jeune[^7]. Il aura sûrement goût à l'aventure, à venger son père ou conquérir nouvelles terres sarrazines.

--- Son père a été assassiné par des fanatiques adversaires des Turcs et était parfois l'allié des Damascènes, sais-tu ? »

Maza en écarquilla les sourcils de surprise. Il s'attendait à trouver une situation comme en Espagne, lorsqu'ils avaient conquis et pillé nombre de villes musulmanes : un terrain offert à la rapine et au saccage. Une excellente occasion de s'enrichir. Il plissa la bouche, vexé de se voir pris en défaut.

« Il y aura bien cliquaille et richesses à récupérer. On parle de prendre des cités aux Mahométans. Lors de ma venue depuis Gibelet, je n'ai entendu que ça.

--- Le sac vaut mieux que le trousseau. Damas a échappé au roi.

--- Et Ascalon ? Voilà belle prise, tu ne peux le nier ! »

Quoique de mauvais gré, Marzonus hocha lentement la tête. Il avait largement profité de la prise de ce port, véritable antichambre de l'Égypte. Il y faisait généralement escale lorsqu'il se rendait à Alexandrie pour ses affaires. Il veillait à ne pas commercer de marchandises prohibées, plus par souci d'éviter les complications que par crainte des interdictions officielles. Il négociait surtout des cordages et toiles de mers ainsi que diverses denrées agricoles, en complément de cargaison.

Il soupira longuement, n'ayant nulle envie de se chamailler avec Enrico le jour même de son arrivée. Malgré sa déception, il comprenait le point de vue de son ami, ayant entretenu un temps semblables espoirs. Jusqu'à ce qu'il soit blessé, mettant un terme définitif à sa carrière guerrière.

Il tendit le bras, attrapa le pichet et invita d'un geste Enrico à approcher son gobelet.

« Tu as déjà quelques contacts auprès le castel comtal ?

--- J'arrive à peine, fraîchement débarqué de la nef, et suis venu directement ici. J'espérai trouver quelque appui auprès de la Compagna[^8].

--- Nul besoin. J'ai encore bons contacts dans l'ost. Des gens que tu pourras aller voir en citant mon nom. Ils te feront bon accueil, les arbalétriers sont fort appréciés car bien efficaces pour repousser les archers turcs. Tu as gardé tout ton fourniment ?

--- Harnois de guerre au complet, briqué et entretenu comme dot de pucelle depuis les campagnes d'Espaigne. »

Marzonus échappa un sourire. Enrico était bien tel que dans son souvenir, inconséquent et fanfaron. Il aurait aimé qu'il se montre aussi bon compagnon qu'autrefois. Il se leva avec lourdeur, toujours handicapé par sa jambe de bois.

« Allez, viens-t'en, compère. Nous allons te trouver grabat pour la nuit. Et avant cela, tu vas goûter à l'excellence de ma table ! »

Les deux hommes abandonnèrent la terrasse alors que la fraîcheur nocturne s'avançait depuis les côtes. Autour, l'agitation de la ville tombait et le ressac s'arrogeait la préséance, entrecoupé du bruit des navires gîtant à l'ancre. La mer s'éveillait à l'amorce de la nuit.

### Shayzar, abords de la citadelle, matin du lundi 21 octobre 1157

La matinée avait été fraîche, mais le soleil radieux avait fait rapidement monter la température. Les hommes s'étaient vite mis en bras de chemise, comme paysans en moisson. Depuis plusieurs semaines, les armées combinées de tous les princes chrétiens du Levant, plus celle du comte Thierry de Flandre, de nouveau outremer, tentaient de s'emparer de Césarée sur l'Oronte[^9]. La cité avait été aisément prise, mais l'arrogante citadelle, puissamment bâtie sur un éperon rocheux surplombant le fleuve et son pont, résistait encore.

Enrico encadrait un petit groupe d'hommes du comte de Tripoli affectés à une bricole[^10]. Il s'était vu confier la responsabilité du montage de l'engin, habitué qu'il était aux subtils mécanismes de l'arbalète. Le plus compliqué pour son équipe était d'approcher les murailles adverses qui les dominaient, sous le feu intermittent des archers musulmans et de leurs projectiles les plus divers, parfois aussi répugnants que dangereux.

Essoufflé d'avoir porté plusieurs lourdes pièces de bois, Enrico aidait à assujettir le pied des mantelets destinés à les protéger. Il établissait une sorte de redoute qui permettrait de s'associer au pilonnage faible, mais régulier de la porte d'accès principal.

« Il faudrait tenir puissamment l'étai arrière, être sûr qu'ils ne sauraient l'abattre en projetant caillasse ou pierraille. Planissez donc le sol pour notre bricole et remblayez-lui autour avec ce que vous ôterez là. Je vais aux nouvelles et demander à ce qu'on nous porte de quoi boire. »

Assez autoritaire, il était malgré tout apprécié de ses hommes. C'était un vétéran qui devait sa place à son expérience, attestée ostensiblement par la cicatrice qui lui barrait le visage. Mais il avait aussi à cœur de prendre leur parti lors des décisions difficiles et de s'assurer qu'ils ne manquaient de rien. Il avait trop souvent eu la gorge sèche et le ventre creux. C'était sans sensiblerie aucune : il était conscient que cela sapait le moral et coupait les jambes. Trop bien traité, le soldat était mou, mais trop mal, il n'était bon à rien. Il fallait le maintenir ainsi qu'une corde d'arbalète aimait-il à dire : assez tendue pour projeter puissamment le carreau, mais pas trop pour ne pas briser l'arc.

À l'écart de la forteresse, la vie de camp se déployait parmi les ruines de la cité. Ce n'était nullement leur oeuvre, les tremblements de terre ayant durement touché la région durant l'été. Les souverains francs s'étaient contentés de profiter de l'opportunité. D'autant que leur plus irréductible adversaire, Nūr ad-Dīn, gisait moribond sur son lit, murmurait-on avec espoir.

Enrico arriva dans le quartier où plusieurs forgerons s'étaient établis. Ils frappaient sans discontinuer sur le fer pour chemiser de neuf les outils, pics et tranchants de pelles de sape, pour fabriquer les clous des mantelets et chats[^11], pour les pentures, crochets et clavettes des engins de siège. Enrico aimait l'odeur du métal chauffé, le tintement des marteaux sur les enclumes, les vapeurs soufrées de certains charbons de terre, malheureusement fort rares en cette région.

Il reconnut la langue tonitruante de Paylag, l'Arménien responsable de son secteur. Pas très grand, mais poilu comme un ours, il semblait de fort gabarit en raison d'une barbe impressionnante, d'une propension naturelle à occuper l'espace et d'une voix de stentor. C'était pourtant un homme malingre, dont le physique sec ne laissait que peu présager la puissance et l'endurance qu'il manifestait lorsqu'il mettait la main à la pâte. Il était maçon de profession, formé à tailler des pierres depuis son enfance, et dirigeait ici les tirs des engins sur les portions de muraille qu'il pensait les plus fragiles.

« Ah, maître Paylag, je suis aise de vous encontrer. Nous allons commencer le montage et je voudrais que vous veniez placer l'axe du trébuchet. Je ne sais quel ouvrage nous devons viser.

--- Il me faut aussi vous faire porter les boulets de pierre. Le prince Renaud a fait venir grands renforts de carriers et nous en aurons plus qu'assez pour abattre leur porte. Outre, le maître engingneur Milos est de retour, avec les cordages et cuirs bruts espérés. J'encrois qu'il fera visitance pour estimer notre travail. »

D'un geste de la main, il l'invita à venir sous l'auvent de branchage qui lui servait de bureau, de réserve et de chambre. Il décrocha une peau de chèvre, avala quelques gorgées et la tendit à Enrico. Avant de la prendre, celui-ci lui demanda :

« Quelles nouvelles des princes, s'il y en a ?

--- Ils attendent, comme nous. Nous ne savons si leurs celliers sont bien garnis. Auquel cas ça risque d'être long. Ici, la sape n'est pas possible, les échelles peu aisées. Il nous faudra miner peu à peu de nos engins une courtine pour envisager l'assaut. Mais ils ont largement de quoi étayer au dedans et nous repousser.

--- Lorsque la ville s'est si vite rendue, j'ai eu espoir qu'il en soit de même pour la forteresse.

--- Ce n'est pas l'ost du Turc qui tient le castel, par malheur. Car nous aurions pu leur proposer honorable retraite. Ce sont des fanatiques de Masyaf, de ceux qui ont tué le père du comte. Nulle échappatoire pour eux. Ils n'en sont que plus résolus. »

Enrico avala une lampée de l'infâme mixture, vin passable mâtiné d'eau croupie dans une peau mal tannée. L'ingurgiter lui coûta une grimace, sous l'oeil indifférent de l'Arménien. Il lui rendit son outre et demanda s'il était possible de faire porter à boire à ses hommes. Et, avant de repartir, il posa la question qui lui brûlait les lèvres :

« Êtes-vous acertainé que ce sont des fanatiques au-dedans ? N'est-ce pas la cité d'un grand prince parmi les Mahométans ?

--- Nul doute. Toute la famille du fameux prince est morte, ensevelie sous les décombres de l'été dernier. Les Bâtinites, comme ils disent ici, ont été plus rapides que nous pour s'installer. Et maintenant nous devons les en déloger. »

Enrico avait déjà appris par d'autres que la famille régnante avait été décimée lors du tremblement de terre, mais il préférait en avoir confirmation. Il savait que tout ce qui avait trait au fameux Usamah Ibn Munqidh était sujet épineux. L'ancien ambassadeur de Damas auprès de la cour de Jérusalem était un homme aussi retors qu'imaginatif. Il était capable de récupérer la forteresse familiale au nez et à la barbe des Francs, l'occasion pour lui de se venger des affronts subis lors de sa fuite d'Égypte quelques années plus tôt. Mais était-il assez sournois pour recourir à une alliance aussi dangereuse ?

### Pont du Fer, midi du mercredi 22 janvier 1158

Un vent intermittent rabattait la poussière sur le convoi étiré dans la plaine. Chevauchant sur ses abords, projetant des graviers, une vingtaine de cavaliers progressait, l'air las, menés par un jeune homme à l'allure impétueuse. Ils avaient passé les murailles d'Antioche alors que le soleil n'avait pas encore dépassé le Staurin et avançaient à marche forcée en direction d'Harim par le chemin le plus court, d'une douzaine de lieues.

Les mules et chameaux, durement conduits par les caravaniers, portaient des pièces d'engins de siège supplémentaires pour assaillir la place forte. Autour des muletiers quelques soldats, l'arme à la main, complétaient la protection. Mais nul n'était inquiet. Ils étaient dans la vaste plaine de l'Amik, dont les richesses agricoles seraient à eux s'ils arrivaient à prendre Harenc une nouvelle fois. De plus, le tell d'où s'élançait la forteresse permettait de surveiller très loin les accès depuis Alep, via Azaz ou al Atharib. Une position stratégique pour se prémunir d'attaques futures contre la principauté, mais aussi un excellent point de départ pour tenter de s'emparer du cœur de la puissance turque.

Parmi les piétons, l'arbalète sur l'épaule, Enrico avançait d'un pas lourd. Il n'avait pas bien dormi la nuit précédente, ayant profité trop longuement des charmes de la grande cité du Nord. Il regrettait un peu la fatigue, mais nullement les abus. Les prostituées du camp, lavandières à l'occasion, n'étaient pas si nombreuses et il détestait devoir attendre son tour. Comparativement, Antioche accueillait suffisamment de lieux de plaisir pour contenter toute une armée.

Il leva la tête des pieds du soldat devant lui et aperçut l'agglomération proche du Pont du Fer. Récemment repris, il commandait le meilleur passage sur l'Oronte dans la région. On racontait que ses solides arches de pierre avaient été édifiées par un grand empereur romain, celui qui avait doté Antioche de ses formidables remparts. Enrico n'y voyait qu'une halte bienvenue, pour abreuver les bêtes et faire le point sur le chargement. Ils avançaient tellement vite qu'il était difficile de ressangler un sac mal fixé, de réparer un lien rompu. En outre, cela voulait dire qu'ils avaient accompli à peu de choses près la moitié du chemin.

Les quelques soldats demeurés là en faction pendant la campagne les accueillirent de plusieurs forts coups de trompe, saluant la bannière de Raymond de Tripoli. Enrico devait reconnaître qu'il avait de la sympathie pour ce jeune homme. Âgé d'à peine vingt ans, il semblait infatigable, toujours à aller et venir sur son superbe coursier alezan. Lui aussi était très confiant dans leur mission, car il avait dédaigné son lourd destrier à la robe sombre et ne portait pas de haubert. Ses longs cheveux bruns volaient lorsqu'il remontait la colonne en un rapide galop. Il n'hésitait pas à encourager les hommes, attendant de chacun qu'il se ménage aussi peu que lui.

Sauf qu'à la nuit tombée, il aurait droit à un vrai lit dans une tente aux motifs bariolés, tandis que les plus humbles se contenteraient une fois de plus d'un matelas de terre et d'un oreiller de pierre. Enrico regrettait le siège de Shayzar car il avait pu s'y dénicher un appentis, un coin bien à lui, au sec, et avec un tas de paille. Un peu poussiéreuse, certes, mais c'était mieux que souvent.

Réalisant que le comte arrivait dans son dos, il se rapprocha d'un des caravaniers et l'encouragea d'une voix forte. Il n'espérait pas tant s'attirer les bonnes grâces du valet que l'attention de son seigneur.

« Nous voilà déjà en vue du pont, compère. Encore quelques lieues et nous verrons les murailles d'Harenc. »

Il accompagna sa remarque d'une bourrade sur l'épaule et ajouta, goguenard :

« Il ne faudrait long temps avant que nous les mettions bas avec ce que nous leur amenons ! »

Tandis qu'il déclamait avec force, Raymond de Tripoli passa à sa hauteur, le sourire aux lèvres, sans leur accorder guère plus d'attention qu'aux autres. Il était habitué à plus habiles manifestations et plus chevronnés courtisans. Mais Enrico savait attendre, une trop rapide ascension était toujours suspecte, et il était plus sage de ne pas brûler les étapes. Il s'était déjà vu attribuer plusieurs fois l'encadrement de soldats et avait montré qu'il tenait assez bien en selle pour prétendre à faire partie de la cavalerie. Ainsi, il pourrait se voir confier plus d'hommes, ou accéder à des rôles plus ambitieux. Il lui fallait également donner des gages à ceux qui trouvaient que les Génois étaient trop présents, trop puissants, dans le comté. Raymond n'était encore qu'un enfant, entouré des fidèles de son père, et ne se constituerait que peu à peu une garde personnelle, promouvant ses amis et se dotant d'un réseau qu'il aurait choisi. Ce n'était qu'une question de temps.

Lorsqu'ils abordèrent le petit hameau, celui-ci semblait inhabité. Les fellahs qui s'entêtaient à vivre là voyaient aller et venir les armées depuis longtemps et se méfiaient. Ils profitaient de la situation de passage obligé pour commercer, comme l'attestaient le modeste caravansérail et le souk attenant. Mais quand les épées et les lances remplaçaient les ballots de marchandises, ils se claquemuraient chez eux, attendant que l'orage se calme, une fois de plus. Une large portion de la ville, bâtie de terre, était à l'abandon. Le Pont du Fer était autant malédiction que bénédiction.

### Tripoli, quartier du port, fin de journée du jeudi 27 février 1158

Installée sous les arcades d'un entrepôt portuaire, la petite troupe de soldats fêtait joyeusement le retour dans la capitale du comté. Ils avaient tous la besace pleine du butin et des gages amassés pendant la campagne hivernale et le cœur léger d'avoir une fois de plus survécu. Parmi les habitués du groupe occupé à festoyer, se goberger et chanter, il en manquait trois sur les douze, dont Cressin qui attendait entre la vie et la mort chez les frères, à Antioche. La fureur avec laquelle les survivants se jetaient dans l'alcool et la débauche n'égalait qu'à peine les angoisses qu'il leur avait fallu surmonter. Mais aucun ne l'aurait jamais avoué.

Mestre Clarin, qu'on appelait ainsi sans que personne ne sache pourquoi, s'affairait à leur rejouer une des péripéties de ses combats, à grand renfort d'interjections, de grimaces et gestes outrés, et ils appréciaient fort ses talents de conteur. Ils étaient de toute façon tellement imbibés de vin que le moindre prétexte à rire leur était bon. Chicot, dont le surnom était, pour sa part, évident, se rapprocha d'Enrico, son gobelet à la main :

« Tu sais si on va demeurer long temps ici ?

--- Comment le saurais-je ? Nous sommes en les murs depuis cette dernière nuitée.

--- Avec le comte Thierry présent, je me disais qu'on allait peut-être reprendre les chemins d'ici peu.

--- Et alors, tu t'es fait soldat pour quoi ? Rester au chaud à besogner ta mégère ? »

L'autre lâche un rire gras, qui se finit en rot.

« Si j'en avais une avec le tétin aussi fier que la Coleite, je saurais m'en contenter ! »

Les hommes étaient las des mois passés à marcher, installer le camp, assaillir des murailles. Ils n'avaient pas eu à subir les flèches des cavaliers turcs et s'estimaient chanceux pour cette fois. Ils saisissaient malgré tout la moindre opportunité offerte de se sentir vivant, même et surtout dans l'excès.

« Profitez donc de votre soirée et des jours qui viennent. Si le ban est derechef appelé, nous le saurons tôt assez. Je vous le ferais assavoir.»

En se levant, Enrico lâcha quelques pièces d'argent noirci sur la table, accueillies avec joie et célébrées de nombreuses bénédictions fort peu orthodoxes.Il laça son épée à sa hanche et attrapa le baluchon qui ne le quittait jamais, ainsi que son arbalète, dans sa housse de cuir, et son carquois. Il lança le tout crânement sur son épaule et s'engagea dans une ruelle proche, sur un dernier salut de la main.

Il serait bien allé se faire laver dans un établissement de bains, autant pour le plaisir de se décrasser que pour profiter des services des jeunettes espérées là. Mais il avait une mission impérieuse auparavant. Il était dans un bien triste état, avec ses souliers aux coutures usées, ses chausses pendantes et sa chevelure grasse. Mais il savait que cela n'était que de peu d'importance. L'accueil qu'on lui ferait ne serait jamais conditionné à son apparence, mais à ce qu'il avait en mémoire.

Il se soulagea dans une venelle, s'attirant un regard de désapprobation d'un gros notable accompagné de deux valets. Il rota à leur intention, leur offrant ainsi une occasion de tourner la tête, encore plus offensés. Son mépris pour ceux qui n'avaient jamais eu à défendre leur vie par le fer allait grandissant avec les années.

Il déambula un long moment dans les rues tortueuses qui montaient depuis le port vers les parties les plus opulentes de la cité comtale. Là-haut, dans le château, le roi Baudoin et le comte Thierry devaient célébrer leur réussite.

Le vieux maître de la Flandre était un homme cher au cœur d'Enrico. Il semblait être plus souvent en outremer que dans ses états, et il n'appréciait rien tant que chevaucher, lance au poing, parmi les lignes adverses. Le jeune prince d'Antioche était pareil, impétueux et colérique, mais téméraire comme un lion. Enrico aurait aimé le servir, celui-là. Mais il avait reçu d'autres ordres.

Il stoppa devant une porte discrète et sortit une modeste clef de fer, qui fit jouer sans bruit la serrure. Au-delà, c'était une petite cour, avec des écuries, un cellier, un atelier abandonné. Des lueurs vacillaient sur les bords des volets clos de la demeure. D'un pas assuré, il alla frapper à l'huis donnant dans les cuisines, un peu en retrait. Il n'eut pas longtemps à attendre et le jeune homme qui lui ouvrit faillit échapper le barda qu'il lui lança presque au visage.

« Fais dire que je suis là et fais moi donc porter de quoi adoucir ma gorge. Elle est si sèche que je crache de la poussière.»

Puis il s'abattit sur un banc disposé près de l'entrée, étendit ses jambes. Des odeurs épicées venaient titiller ses narines, tandis que plusieurs domestiques préparaient le repas. Ils n'osaient guère lever la voix, intimidés par la présence du soldat. Le jeune revint rapidement et demanda à Maza de le suivre.

Une telle rapidité l'étonnait. Habituellement, on le faisait patienter au moins symboliquement. Pour bien marquer qui était le maître et qui était le valet. Il fronça les sourcils, inquiet, et se racla la gorge. Il plissa les yeux lorsque le gamin souleva la portière d'épaisse étoffe qui donnait dans la grande salle.

« Tiens donc, mais il semblerait que mes deux moineaux aient eu cœur de me rejoindre le même jour ! » lança, d'un ton narquois le vieil homme qui se tenait au centre de la pièce, les mains dans le dos. En quelques mois, sa posture semblait s'être affaissée, peut-être empâtée, mais le regard de Mauro Spinola demeurait toujours aussi ardent.

Mauro rejoignit une silhouette installée sur un escabeau, près du feu. À côté de lui, Enrico aurait presque pu paraître riche tellement celui-ci avait l'air misérable. Ses hardes puaient à plusieurs mètres alentour. Des yeux fatigués dévisagèrent Enrico un instant avant de plonger de nouveau dans un gobelet de vin. Le vieil homme, indifférent à son aspect de mendiant, vint lui tapoter amicalement l'épaule.

« Guilhem, je te présente Enrico, qui nous sert fort loyalement auprès du jeune comte. » ❧

### Notes

Les simples servants d'armes sont rarement évoqués dans les sources, si ce n'est pour en décrier la barbarie et la vilenie dans les textes célébrant les hauts faits des puissants. À l'occasion, certains chroniqueurs en déplorent le massacre lors des batailles, sans s'en émouvoir autre mesure. Il est pourtant évident qu'ils avaient su se rendre indispensables pour les sièges, fournissant la main-d'oeuvre pour mettre en place tous les équipements. En plus de cela, lors des affrontements en terrain découvert, ils constituaient une muraille mouvante de boucliers et d'armes de tir d'où s'élançaient les charges dévastatrices de la cavalerie. L'un sans l'autre n'avait que peu de chance de triompher des forces musulmanes.

Le royaume de Jérusalem était malgré tout régulièrement en demande de ces hommes de sac et de corde, surtout pour mener ses campagnes au-delà des frontières et s'emparer des forteresses. Une partie était issue des levées féodales, fournie par les contingents territoriaux, comme les chevaliers. Mais il est fréquemment mentionné le recours à des soldats rémunérés occasionnellement, qui coûtaient d'ailleurs fort cher au trésor royal. La pénurie chronique de combattants ne fera que s'accentuer, avec le recul de l'esprit de croisade, et ce sera une des causes qui expliquera la rapide conquête du royaume de Jérusalem par Saladin au lendemain de la bataille de Hattîn en 1187.

En ce qui concerne le titre, l'expression d'*Araignée universelle* fut utilisée comme insulte envers Louis XI, homme de réseau et d'influence, qui préférait les voies détournées et tortueuses pour arriver à ses fins. Je reprends ici la formule d'un chroniqueur du XVe siècle, Georges Chastelain.

### Références

Cobb Paul M., *Usama ibn Munqidh. The book of Contemplation. Islam and the Crusades*, Penguin Books, Londres : 2008.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967

Ombres et lumières
------------------

### Jérusalem, porte de David, matin du mardi 18 mars 1158

La lumière d'un soleil frais habillait d'or les murailles de la cité, bleuissant les zones d'ombre. Dans le ciel limpide, quelques oiseaux volaient haut, occupés à se poursuivre en poussant des cris aigus. Le vacarme des cloches venait à peine de s'éteindre et les rues bruissaient de l'agitation du peuple industrieux, des animaux menés, des enfants baguenaudant. Près de la porte, une foule de chameaux de bât attendaient devant le moulin, dans une exubérance de poussière grise.

« La journée sera chaude, p'tiot, on ira pas bien loin ce jour. Mais vu le bon temps, je connais un coin où passer la nuit, peu avant le Toron des chevaliers. Vallon joliet, au parmi des oliviers, avec une source claire comme sourire de donzelle. »

Devant l'assurance de son compagnon, le jeune garçon hocha la tête en silence. Il n'attachait ses pas que depuis peu à ceux de Remigio, un saltimbanque, un jongleur comme on les appelait. Ses parents l'avaient mis en apprentissage disait-on. Mais il savait bien qu'on l'avait échangé contre quelques pièces brunies et un tas de vieux linges. Il n'était qu'une bouche à nourrir pour les siens et n'avait découvert sa valeur qu'au moment où il en voyait le montant sur la table râpée de sa maison. Il était parti sans un regard en arrière, mais le cœur gros de n'avoir personne à regretter, ni aucun signe d'affection pour le saluer.

De caractère moins ombrageux que son père, son nouveau compagnon avait également la main moins leste, mais les exigences plus fournies. Il l'avait même rebaptisé Vital, car il disait que cela plaisait aux moines chez qui il aimait à faire halte régulièrement. Sa maigre ménagerie était d'ailleurs affublée de patronymes similaires : le corniaud pelé qui faisait des cabrioles répondait au nom de Giusepe, ils avaient un perroquet, venu d'Alexandrie, Tomaso, et leur âne, une petite bête vaillante, mais dotée d'un esprit retors, avait été baptisé Martin. Remigio racontait qu'il l'avait appelé comme ça en mémoire d'un vieux compagnon, artiste comme lui, et qu'on surnommait le Bon à Rien. C'était mieux que d'être mauvais à tout, rétorquait-il invariablement.

À cette heure matinale, la porte de David était toujours encombrée par les visiteurs et marchands, désireux de pénétrer au plus vite dans la cité pour y faire affaire. Les sergents surveillaient ce qui entrait, sans se soucier de ce qui sortait, mais la cohue était telle qu'on devait se faufiler entre bêtes et gens, en prenant garde à ses pieds. Remigio tenait la longe de Martin d'une main ferme et se servait de sa badine pour se frayer un chemin, sans ménagement.

« Au sortir du faubourg, cria-t-il, y'a un hospice, où ils auront de certes du pain bis à nous donner. Ils reçoivent donations des frères de Saint-Jean pour faire héberge aux marcheurs de Dieu bloqués au dehors à la nuitée. Ils en ont bien assez pour nous aussi. Veille à prendre ta mine la plus triste, ils aiment à contenter maigres enfançons.

--- Nous avons droit au pain des pérégrins, maître ?

--- La charité s'offre à tous, c'est là maîtresse vertu, et il ne faut jamais manquer de l'exalter auprès de nos généreux hôtes. »

Le saltimbanque s'arrêta soudain, tendant la longe à son apprenti maintenant qu'ils avaient franchi la muraille. Il se pencha, adoptant un air sentencieux démenti par la lueur égrillarde dans son regard :

« Retiens bien ceci : le vénéré saint Paul disait que la plus grande des vertus, c'est la charité[^12]. »

Vital écarquilla les yeux, impressionné par le savoir de son maître. Cette naïveté tira un sourire amusé à Remigio. Le gamin lui plaisait.

« Nul besoin d'être grand clerc pour assavoir cela. Mais ça peut te valoir un quignon ou un brouet au plus froid de l'hiver de le rappeler à bon escient. »

Il assortit sa remarque d'un clin d'oeil complice et d'un rire forcé.

« Je t'en enseignerai bien d'autres. Si le fort baron aime à entendre les hauts faits des chevaliers fameux, la mégère et surtout le moine sont plus sensibles aux bondieuseries. Nous sommes là pour déclamer ce que chacun aime à entendre. »

Il assortit sa remarque d'un coup léger de badine pour remettre son équipage en route. Il se sentait d'humeur frivole, après plusieurs semaines dans la Cité sainte sans toutefois que les revenus aient été fameux. Jérusalem ne lui avait jamais réussi. Trop de religion pensait-il, et des miséreux comme s'il en pleuvait. Comment gagner sa pitance dans un tel endroit ? Mais le jeune seigneur Hugues de Césarée était un employeur généreux, et il avait décidé de passer les fêtes de Noël auprès du roi. Donc Remigio avait suivi.

Avec l'arrivée des beaux jours, les opérations militaires allaient sans doute reprendre, et il était temps de rejoindre la côte, où les périls étaient faibles et le climat plus propice. De façon générale, le jongleur aimait à rester le long de la Méditerranée et ne s'écartait guère du royaume. Il avait poussé une fois loin au nord, jusque dans le comté de Triple[^13]. C'était un lieu magnifique, riche de ses cultures opulentes et de somptueuses cités, mais il s'y plaisait moins. Il avait ses habitudes entre Judée et Galilée, connaissait les bonnes héberges, les moines tatillons et les nobles généreux.

« On fera halte à Ramla. Le marché y est bien animé, surtout en cette période de l'année, avec la réouverture des mers. De là on ira peut-être à Ascalon, par chance on y encontrera maître Bassam. C'est lui qui m'a appris le truc des poupées d'ombre. Un sacré vieux goupil, celui-là. Jamais pu savoir s'il était mahométan, ermin[^14], romain[^15] ou bon chrétien... »

Il sourit pour lui-même, retrouvant visiblement la mémoire de bons moments, et se tourna vers Vital.

« J'ai inventé de moi-même la plupart de mes pièces, mais lui en connaît tellement ! Il sait jouer pour quiconque peut le payer. Il sait même des morceaux pour les Pâques des mahométans, un pour chaque soir.

--- Les mahométans fêtent Pâques ? s'étonna le gamin.

--- De certes. Ils sont hérétiques, mais pas tant. Ils font carême et appellent ça Rame-dent, jamais su pourquoi. Ils font maigre tout au long du jour et, le soir, se dépêchent de gober ce qu'ils ont raté en journée. »

Il se gratta la tête, comme gêné par ce qu'il allait dire, puis ajouta :

« J'ai cheminé un temps avec Bassam. Il savait si bien leur langue qu'on faisait parfois halte en leurs casals. Moi j'y ai jamais rien compris en dehors de quelques mots. Si tu y arrives, ce serait bon pour nos affaires. Au nord du royaume, leurs villages sont légion, et on peut y faire bonne pitance. Juste que la plupart aiment pas le vin, encore moins la bière et que jamais tu y auras droit à une tranche de bacon. En dehors de ça, j'ai pas à dire qu'ils font mauvais hôtes. »

Il fit craquer son dos et s'étira lentement.

« Bon, avant tout, il te faut savoir tes lettres. Continue donc après moi :

> Charles le roi, notre empereur, le Grand,\
> Est resté sept ans tout pleins en Espagne :\
> Il a conquis jusqu'à la mer la terre haute.[^16]

### Bethléem, palais de l'évêque Raoul, midi du lundi 14 juillet 1158

Confortablement installés à l'ombre d'un palmier, dans une des cours privées, deux hommes profitaient de la chaleur en grignotant des pistaches qu'ils arrosaient de vin coupé d'eau. Parmi les domestiques de l'évêque, beaucoup s'étonnaient du bon accueil toujours réservé au jongleur Remigio. Et que Daimbert, le principal clerc de sa seigneurie, passât autant de temps avec lui, étonnait chaque fois, mais personne n'osait s'enquérir des relations qui unissaient ces deux-là. Les deux hommes auraient bien été en peine de s'en expliquer clairement, d'ailleurs, après toutes ces années.

« As-tu jamais eu désir de jouer pour le jeune comte de Jaffa ?

--- Le frère du roi ? Non, pas eu l'occasion. On le dit fort versé dans tous les plaisirs.

--- Ne t'y trompe pas, il est fort érudit et pourrait en apprendre, même à un vieux décloîtré comme toi.

--- Au parmi du peuple, on le dit plus enclin à goûter l'abricot qu'à lécher les pages. »

Le clerc, intéressé, se tourna vers son ami :

« Ah, vraiment ? Il est pourtant de frais marié, et fort amoureux. D'aucuns le pensent ensorcelé par Agnès.

--- Les gens s'amusent plutôt de le voir faire espousaille de la promise de son vassal si tôt ce dernier en fers sarrasins.

--- Ils ne s'inquiètent pas de l'arrivée des Courtenay ? »

Le jongleur pouffa longuement.

« Ils se moquent de cela comme laboureur de ses abeilles. Cela ne peine que les barons qui se partissaient déjà les terres du Soudan d'Égypte.

--- Tu as entendu rumeurs sur les ambitions du maréchal[^17] ?

--- Non, aucune. Mais certains pleurent, dit-on, alors qu'ils ont plein poing. Mais cela n'est qu'affaire de barons et permet à peine aux commères d'échauffer leurs langues. »

L'administrateur fit jouer le vin entre ses dents un petit moment, le regard fixé sur son verre. Remigio lui apportait des nouvelles du peuple, de ce que les amuseurs colportaient, que les négociants évoquaient, le soir, à la taverne ou dans les caravansérails. Il n'espionnait pas, à proprement parler, mais évoquait les rumeurs, les nouvelles, qui animaient cités et casaux. Les jongleurs de son genre étaient aussi importants pour célébrer la gloire d'un baron, selon qu'on les paie assez, que pour en apprendre l'état d'esprit des vilains et laboureurs, artisans et lavandières. Remigio était en outre assez malin pour savoir que cela lui ouvrait certaines portes que ses talents de conteur et marionnettiste n'auraient jamais forcées.

Daimbert avala quelques friandises, indolent, se laissant gagner par la torpeur.

« D'usage, tu n'aimes guère demeurer trop au chaud ou au froid, que viens-tu faire en les terres au mitan de l'été ?

--- J'ai eu envie de montrer le pays au petit, mais je ne descends plus avant. Je prends la route de Naplouse en quittant la cité. Peut-être pour rejoindre la Galilée.

--- Norredin aime à guerroyer par là-haut, tu devrais prendre garde.

--- J'ai déjà croisé son sénéchal, Siracons[^18], il m'a suffit de me pousser du chemin et de les regarder passer...

--- Ces démons ne t'ont pas détroussé ?

--- On ne rase pas un oeuf ! Et on dit ces Turcs grands amateurs de pantins d'ombres, j'aurais même pu gagner quelque cliquaille, qui sait ? »

Daimbert lançait un regard aussi étonné que réprobateur lorsque des cris d'oies furieuses provenant de la basse-cour les firent sursauter. Sentant que sa plaisanterie n'avait pas été appréciée, Remigio décida de changer de sujet et passa une langue gourmande sur ses lèvres fines.

« Aura-t-on droit à un jars bien épicé en la veillée ?

--- Pas toi, de certes, se gaussa Daimbert, acide. En as-tu jamais mangé, d'ailleurs ?

--- Plus qu'à mon tour, j'en ai certeté. Certains barons ont la gourle percée et abreuvent aussi bien qu'ils nourrissent. »

Daimbert renifla, hochant la tête en silence. Non loin d'eux, à l'abri d'un appentis, le gamin cirait les personnages de cuir. Il donna un coup de coude à Remigio et lança, un peu moqueur :

--- Dis donc, ton marmot, ni Turc ni baron n'a songé à le vêtir autrement que comme mendiant ? Il a une cotte que pas un chiffonnier ne voudrait comme toaille. Je vais m'enquérir auprès des celliers, voir s'il n'y a pas quelque vieille cotte de damoiseau entré en tonsure qui pourrait lui aller. »

Remigio sourit à Daimbert, en inclinant la tête en remerciement. Puis il ajouta, en faisant un clin d'oeil :

« Dieu connaît le bon pèlerin. »

### Casal de Saint-Gilles, taverne de Clarembaud, veillée du jeudi 11 septembre 1158

Les applaudissements crépitèrent dans l'obscurité de la petite salle voûtée où enfants, parents, curieux et badauds s'étaient entassés. D'un coup de menton, Remigio incita Vital à aller passer la sébile. Ce n'étaient que vilains et artisans, mais ils savaient parfois se montrer généreux, surtout quand, comme ce soir, ils avaient été enthousiasmés par le spectacle proposé.

Cela faisait plusieurs semaines que Remigio avait préparé son récit, dessiné et découpé les silhouettes de cuir pour les nouveaux personnages. À la flamme des lampes à huile, il évoquait le tragique et grandiose destin du Saut du Templier, comme il l'avait nommé. Il n'était pas certain que c'était une histoire véridique, et l'avait enjolivée à sa façon après l'avoir entendue répétée par d'autres. Mais l'essentiel était de frapper l'imaginaire, d'avoir des anecdotes passionnantes et des rebondissements maîtrisés. Il avait choisi de tester son nouveau spectacle ici, en Samarie, car il était censé se dérouler plus au nord, vers la Galilée, sur les côtes entre Saint-Jean d'Acre et Tyr. Là-bas il se devait d'être parfaitement à l'aise pour ne pas décevoir. Les réactions de ce soir le rassérénaient pleinement.

Le maître de céans, un gros barbu, Clarembaud, s'approcha, le visage tel celui d'un conspirateur. Il frottait sa main mutilée avec excitation comme s'il y voyait couler les pièces.

« Excellent spectacle, maître Remigio. Que voilà bonne idée de louer la milice du Temple. On les apprécie fort par ici. Surtout depuis leur martyr d'Ascalon !

--- Eh, ne dit-on pas que ces valeureux martyrs ont protégé le reste de l'armée depuis les Cieux ? » répondit le marionnettiste.

Le tavernier n'était jamais très à l'aise avec lui, ne sachant s'il devait prendre ses remarques pour des plaisanteries ou des appréciations dévotes. Le jongleur s'en amusait fort et forçait le trait, trop heureux d'embrouiller un esprit à bon compte. Chez lui la mystification était un art de vivre.

« Tu nous verses un godet pour arroser ça ? »

L'éventualité de voir son bénéfice de la soirée amputé de la moindre portion annihila immédiatement la joie de vivre de Clarembaud. En un instant son visage avait retrouvé le masque du commerçant harcelé par les taxes diverses, les approvisionnements difficiles et les clients tatillons. Il tenait cette capacité instinctive de son passé paysan.

« Attendons de voir la collecte. On ne doit point louer la journée dont on n'a encore vu la nuit... » ajouta-t-il, circonspect.

Lorsque Vital arriva avec la gamelle, ils trièrent les quelques jetons et oboles, n'accusant qu'un très léger pécule. Par contre, de nombreux pichets avaient été commandés et vidés.

« Pas si mal pour un casal, grogna Remigio. Avec ce que tu nous accorderas, maître Clarembaud, nous aurons de quoi reprendre la route au chant du coq demain. »

Heureux de ne pas se voir soutirer un supplément avec l'affluence inespérée, le tavernier opina, tout en allant récupérer les récipients loués pour la soirée. Pendant ce temps, le jeune Vital aidait son maître à ranger soigneusement les figures dans un coffret de pin. Ils plièrent ensuite la grande toile de coton et les lampes, avant d'enfourner le tout dans une corbeille garnissant les flancs de Martin. Lorsque tout fut en place, il ne demeurait dans la salle que quelques lumignons tremblotant, peu vaillants, insuffisants pour faire plus que dessiner de traits ocres les ombres de chaque saillie.

Remigio fit claquer ses mains l'une dans l'autre.

« Alors, maître Clarembaud, qu'en est-il de cette porée tant vantée ? J'ai tel appétit que je pourrais goboyer comme un ours tout un troupeau, pâtre inclus ! »

Vital, désormais habitué aux éclats de son maître, échappa un rire enfantin, excité comme une puce. C'était la première fois qu'il avait eu la responsabilité de remuer quelques personnages et décors.

### Château de Naplouse, cuisines, soirée du samedi 27 décembre 1158

La tablée bruissait d'éclats de voix enjouées, de cris de surprise, de rires et de gloussements gaillards, de vociférations et de hurlements hilares. Le banquet servi dans la grande salle battait son plein et, dans les communs, les domestiques n'étaient pas en reste. La reine mère, Mélisende, n'était pas présente, mais il y avait tout de même une partie de sa cour demeurée là et suffisamment pour que les plats rapportés en cuisine fassent le bonheur de beaucoup.

Installé non loin de la porte d'entrée et donc dans les courants d'air, Vital ne se plaignait pas. Il n'avait jamais mangé ainsi. De la viande, rôtie comme pour un baron, avec de la sauce épicée digne d'un roi. Ce n'était que quelques miettes arrachées à un os, mais il savourait avec une joie non dissimulée. Il regardait avec des yeux ébahis les mets se succéder, depuis le haut bout où les principaux domestiques avaient droit aux plus beaux restes, jusqu'à sa table où les errants, musiciens, jongleurs, acrobates et bateleurs, avaient trouvé place. Nul besoin de se battre, chacun obtenait sa suffisance, dans les éclats de voix et les plaisanteries à la gloire de leurs hôtes.

Remigio avait trouvé avec qui baguenauder : Anceline, une jolie femme un peu usée par les ans, mais au sourire doux, qui se disait lavandière. Le saltimbanque en avait ri, avant d'ajouter qu'elle devait frotter les linges avec son derrière plus souvent qu'avec ses mains. Elle n'avait pas grimacé à la saillie, habituée aux rebuffades désobligeantes. Au moins cette fois-ci n'y avait-il pas méchanceté dans la remarque, voire même plutôt intérêt. En effet, Remigio n'en était pas rebuté pour autant. Il avait juste vérifié les monnaies de sa bourse, au cas où.

« Tu travailles au castel, ou pour un des barons de sa cour ?

--- Je viens juste aider pour les fêtes. J'étais dans le coin, avec quelques amies, et sans ouvrage.

--- Il ne fait pas bon temps, l'hiver, pour nous autres poudreux des chemins » acquiesça Remigio.

La remarque bienveillante ragaillardit Anceline, qui passa une main amicale sur le bras du jongleur.

« Et toi, tu espères demeurer un moment par ici ?

--- Quelques jours au plus. Je fais découvrir l'art au gamin, là. »

D'un coup de menton, il désigna Vital, affairé à rogner son os avec l'enthousiasme d'un jeune dogue affamé.

« J'ai accepté de le prendre comme apprenti, et j'espère bien qu'il saura m'aider en mes vieux jours. J'ai espoir de m'attacher à puissant baron et de rimailler pour aider à célébrer sa gloire. Un jongleur qui irait déclamer pour moi serait de bon effet.

--- Tu sais donc jouer avec les mots tel le poète ?

--- J'ai étudié les arts libéraux et devais finir clerc. Mais j'aime trop la vie pour quitter le siècle. »

Anceline échappa un ricanement cristallin, revivant visiblement quelques souvenirs.

« Je connais nombre de prélats qui ne renoncent pas à certains plaisirs pour leur mitre.

--- Ma défunte mère m'a trop bien inculqué la religion pour que je finisse évêque, c'est là tout mon malheur » s'esclaffa Remigio.

Ils partirent dans un grand éclat, qu'ils accompagnèrent d'une copieuse rasade à la bonne santé du châtelain de la reine. Puis une voix forte cria :

« Les jongleurs de pantins d'ombre, c'est à vous !

--- *Fiat lux*[^19] *et umbra fuit*[^20], j'arrive ! » lâcha avec emphase et espièglerie Remigio, scellant sa promesse d'une caresse dans le dos de sa voisine tout en se levant. Puis il attrapa Vital par le col avant de s'avancer, majestueux comme un chevalier au behourd, jusqu'à l'escalier dans les ténèbres duquel il disparut dans une courbette adressée à Anceline. ❧

### Notes

Si le nom de mes deux personnages sont une référence à peine voilée à *Sans famille* d'Hector Malot, mon envie était justement de prendre le contrepied de la description sociale résolument effrayante du XIXe qu'en faisait le romancier. Sans chercher à sous-estimer les difficultés et les aléas d'une vie d'errance, ni les périls à traverser des zones de guerre à l'époque des croisades, il apparaît que des voyageurs pouvaient connaître une existence décente.

Nous sommes beaucoup mieux renseignés sur les pèlerins de toute religion que sur les artistes, mais la vie culturelle était riche et se basait en grande part sur le spectacle vivant, même si le professionnalisme n'était pas toujours de mise. Quant au théâtre d'ombres, il était issu d'une longue tradition dans ces régions. Il a pu être christianisé pour plaire au public latin, bien que nous n'ayons aucune source sur le sujet.

### Références

Broadhurst Roland, *The Travels of Ibn Jubayr*, London : Cape, 1952.

Guo Li, *The performing Arts in Medieval Islam. SHadow Play and Popular Poetry in Ibn Daniyal's Mamluk Cairo*, Leiden, Boston : Brill, 2012.

Moignet Gérard, *La chanson de Roland. Texte original et traduction*, Paris : Bibliothèque Bordas, 1969.

L'héritier
----------

### Naalein, maison-forte, fin d'après-midi du mardi 20 mai 1147

La petite cour de la maison forte résonnait des cris des chameliers tentant d'organiser le déchargement. Les bêtes blatéraient parfois de mécontentement, ce qui ajoutait encore à la pagaille. Malgré le vacarme, le seigneur du lieu, Jehan, regardait avec plaisir les pièces de bois que l'on portait dans ses celliers. Il avait prévu depuis un moment de surélever le bâtiment et de lui adjoindre une tour, d'où il pourrait surveiller les confins de la terre que le roi lui avait confiée. Il savait parfaitement qu'il était en relative sécurité, coincé dans une vallée sur la route entre Bethgibelin et Bethléem. Il ne se l'avouait guère, mais il avait surtout envie de marquer la présence d'un officiel, de montrer aux environs que le lieu était sous la coupe d'un homme. Pour le compte d'un autre, en attendant mieux.

Installé à l'une des fenêtres de la grande salle à l'étage, Isaac, son aîné, s'amusait avec quelques figurines de bois et de toile. Ils avaient reçu quelques jours plus tôt un jongleur en route vers la côte depuis Bethléem, et il recréait les exploits devenus mythiques de leurs devanciers, Godefroy de Bouillon en particulier, narrés à cette occasion. À ses pieds, ses deux sœurs jouaient également, accroupies sur un tapis aux couleurs vives. Assise un peu en retrait, leur mère, une jeune femme aux traits lourds, tirait l'aiguille, pour orner de motifs chamarrés le col du bliaud de leur père. Dans la pièce, deux servantes s'activaient à nettoyer et préparer la veillée, remplissaient les lampes, balayaient. Un clerc installé en bout de table compulsait des livres, bougeait des jetons sur un échiquier, marmonnait tout en calculant avec son abaque.

Isaac plongea le regard dans la cour en entendant un bruit de cavalcade. Plusieurs hommes venaient de pénétrer dans l'enceinte, tenant ferme leurs chevaux effrayés de trouver là tant d'animation. Il écarquilla les yeux de plaisir en reconnaissant son oncle et lâcha ses jouets pour s'élancer vers l'escalier.

« Oncle Bernard est là ! Mère ! »

Le voyageur n'eut qu'à peine le temps de mettre pied à terre que la petite silhouette se serrait contre lui, à sa plus grande joie. Il était gris de poussière, avait le visage las, mais il étrilla le gamin avec entrain.

« Alors, que devient mon futur page ? Il serait bien temps de t'initier à tout cela !

--- Laisse donc enfançons profiter un peu avant de les lancer dans le monde, frère » lui rétorqua, souriant, Jehan.

Le cadet s'avança afin d'embrasser son aîné, imposante silhouette ventrue. Le cavalier souleva son chapeau de paille et essuya la sueur coulant sur son front dégarni. Bernard Vacher n'était pas un bel homme. Ses traits empâtés, son nez tordu, sa bouche sans lèvre, et ses yeux fatigués accusaient son âge. Il savait encore manier l'épée avec dextérité et sa monture adroitement, et arborait en tout moment ses éperons dorés de chevalier. Mais les épaules commençaient à tomber, ainsi que le ventre et le fort poitrail à la suite, aujourd'hui plus qu'habituellement. Jehan, l'invitant d'un geste à le précéder, s'en inquiéta alors qu'ils rejoignaient la fraîcheur de la grande salle.

« Mauvaises nouvelles, mon frère ?

--- De certes. La Cour a décidé d'aller encontre Damas et de soutenir ce fou d'Altuntash dans sa révolte. »

Il n'en dit rien de plus, saluant d'une accolade distraite sa belle-soeur puis s'affala sur le banc. Rapidement, du vin, de la bière, et quelques fruits, accompagnés de pain, d'olives et de fromage, furent disposés devant lui. Jehan prit place sur un escabeau, tandis qu'Isaac s'installa auprès de son oncle, le servant tel un page. Bernard avala plusieurs gorgées d'un rouge capiteux avant de reprendre.

« Le jeune Baudoin est entouré de sots, et il a grand désir de marquer sa force. Il sait que la reine ne peut le suivre sur pareil terrain. Il ira donc au secours de Bosra contre Damas.

--- Il est prêt à rompre la trêve ? Il va pousser Aynar[^21] dans les bras de Noradin[^22] !

--- J'ai grand regret à le dire, mais le roi est comme jeune dogue, impatient de courir et fort mal entouré. On m'a presque accusé d'avoir reçu dinars et soieries pour trahir. »

Jehan en frappa sur le plateau de colère, mais Bernard l'arrêta de la main, une moue cynique sur le visage.

« Pas de quoi s'emporter pour autant. Le connétable et la reine savent bien qu'il n'en est rien, et l'ont fait assavoir. »

Il haussa les épaules et reprit un peu de vin, avant d'ajouter.

« Au final, il faut donc que tu convoques l'ost, pour le mener à Tibériade où le roi a fait semonce. De là, nous irons en Hauran pour porter secours à Altuntash. »

Jehan se leva avec lenteur, secouant la tête comme pour s'extraire d'un mauvais rêve. Tandis qu'il sortait, pour annoncer leur départ imminent, Bernard se tourna vers Isaac, gobant quelques olives dont il recrachait les noyaux dans son poing.

« Et toi, gamin, as-tu quelques exploits à narrer à ton vieil oncle ?

--- Père m'a fait faire une lame de bois et, avec, je malmène souvent le pal. Et j'ai sauté un muret qui faisait plus de deux pieds l'autre jour, en selle.

--- Te voilà en bon chemin pour manier la lance comme il sied à décent chevalier ! Et où en es-tu de la lecture, de l'écriture ?

--- Mère me fait réciter chaque jour, mais c'est là ouvrage de clerc. Matthieu dit qu'on finit tonsuré à trop étudier.

--- Qui est donc ce Matthieu ?

--- Le fils d'un des chevaliers ici. Il est grand et costaud, mais je le bats parfois. »

Le soldat se passa une main sur le visage, gratta son oreille et attrapa son neveu qu'il installa sur son genou. Il souriait devant la candeur de l'enfant. Il aurait aimé trouver lui aussi une fille de bonne famille et avoir un héritier, mais le destin en avait voulu autrement. De toute façon, il appréciait trop sa liberté pour s'attacher à une terre.

« Tu diras à ce Matthieu qu'il n'a que du vent entre les oreilles, et que c'est un chevalier du roi qui le lui dit.

--- Pourtant, il est plus important d'apprendre à batailler ses ennemis. La prière et l'étude, ce n'est pas pour nous.

--- Tu parles de vrai. Mais le fer de l'homme doit servir avec discernement. Nombreux sont ceux qui pensent qu'il n'y a que ténèbres et lumière. Et les clercs ne sont pas les derniers à ce jeu-là. Voilà la plus grande des erreurs, mon garçon. Ici-bas rien que des ombres, changeantes, mouvantes. Apprends à raisonner avant toute chose, ne laisse pas tes impressions l'emporter. Ton discernement sera toujours la lame la plus affilée à ton service. »

Lorsqu'il reposa Isaac au sol, Jehan entrait.

« J'ai annoncé la convocation du roi. Nous partons demain matin, à l'aurore.

--- Au moins pourrons-nous faire bonne veillée ce tantôt, avant de manger la poussière derechef » opina Bernard.

### Naalein, maison forte, fin de matinée du dimanche 28 août 1149

Au retour de la messe, alors que la famille Vacher allait prendre place à table, un messager était apparu à l'intention de Bernard. Un de ses contacts avait envoyé un pli auprès de la Cour, et il avait fallu encore qu'un sergent soit désigné pour prévenir le chevalier, plus loin au sud chez son frère. Entre-temps, le contenu en avait été lu, bien évidemment, car le connétable ne faisait guère preuve de réserve avec les hommes de soudée. Même s'il leur reconnaissait parfois du talent, ce n'étaient que des domestiques à son sens. Il avait donc joint un second feuillet, indiquant que Bernard devait se présenter au plus tôt à la Cour pour exposer la situation en détail.

Le chevalier parcourut rapidement les notes puis les tendit à Jehan, le maître des lieux. Autour d'eux, les valets commençaient de servir le plat de viande aux fèves. Seul Isaac, assis à la droite de son oncle, suivait avec attention les échanges entre les deux frères. Chacun ici était accoutumé à ce que les hommes partent en guerre, reviennent parfois blessés, répondent aux convocations. Nul ne s'inquiétait outre mesure d'un message ou des discussions à table.

« Ainsi donc le vieil Aynar est mort, sembla regretter Jehan.

--- Je ne saurais dire si je m'enjoie de la nouvelle ou pas. D'aucuns verront là bon moment pour prendre la cité...

--- Attendons de voir qui le remplacera. En fait, si ce n'est en titre. Ton billet n'en parle pas ?

--- Ce serait Mugîn ad-Dîn le nouvel homme fort. Nous pourrions avoir pire. Il n'aura nul désir de se jeter dans les bras des Turcs. »

Depuis des années, Damas jouait ses adversaires les uns contre les autres et n'avait jamais écarté l'aide franque quand il s'agissait de s'affranchir de la puissance aleppine du nord, celle de Zengui puis Nūr ad-Dīn. L'inverse était également vrai, comme le roi Baudoin l'apprit sévèrement lors de la prise manquée de Bosrah, deux ans plus tôt.

« Je ne sais quoi dire aux Barons lors du conseil. Le comte Thierry aura sûrement l'appétit aiguisé.

--- La cité a résisté aux plus puissants rois, l'ont-ils déjà oublié ?

--- Toi et moi savons bien que le fruit n'est pas si mûr qu'il tombera en nos mains aisément. Mais ce n'est pas là héroïque geste qu'il plaît à un jeune roi d'ouïr. »

Il avala quelques cuillers en silence, lichant avec un plaisir évident la sauce puissamment miellée. S'il devait confesser un péché, ce serait volontiers celui de gourmandise, et ses contacts de part et d'autre de la frontière savaient que le plus court chemin vers son cerveau passait par son estomac.

« Je demanderai audience à la reine pour m'assurer qu'elle a bonne connoissance des faits.

--- Peut-être devrais-tu en toucher aussi quelques mots au sénéchal. Il sait fléchir les barons de la Cour.

--- Je ne sais jamais sur quel pied danser avec celui-là. Il est plus glissant que serpent.

--- Il ne veut pas se trouver entre marteau et enclume et veille donc à demeurer équitable dans ses relations avec chacun. Mais il m'a fait bonne impression, je le pense prud'homme. »

Bernard hocha la tête sans conviction puis se resservit une large portion. Les sourcils froncés, il avalait désormais sans joie ses cuillerées, anticipant les jours prochains. Il lui faudrait également se rendre à Damas, il en était certain. Ne serait-ce que pour se rendre compte par lui-même des intentions du nouvel homme fort, Mugîn ad-Dîn.

Il l'avait rencontré de nombreuses fois et ne lui concédait pas le même crédit qu'à son beau-père. Il lui reconnaissait certes de l'autorité et de la volonté, mais diriger la cité de Damas demandait bien plus que cela. Il regrettait presque l'absence du vieil Usamah ibn Munqidh, réfugié en Égypte. Le diplomate était un fieffé chenapan, il n'en disconvenait pas, mais il lui accordait de grandes qualités humaines, malgré son opportunisme politique et un vrai talent pour l'intrigue. Le nouveau maître de Damas en aurait bien besoin, pour résister aux pressions des Aleppins.

Malgré tout son dévouement à la couronne de Jérusalem, Bernard rechignait à imaginer la cité dans le giron latin. La voir entre les mains d'un Thierry de Flandre ou, pire encore, d'un aventurier fraîchement débarqué et qui aurait eu l'heur de plaire au jeune roi, lui arrachait le coeur. Peut-être que ses adversaires avaient raison, il finissait par se laisser corrompre de ses trop fréquents voyages chez les musulmans.

La voix immature de son neveu le tira de ses sombres tribulations :

« Mon oncle, je pourrais venir avec vous en la Cité ? Comme l'autre fois ? »

Bernard sourit et se tourna vers son frère. Il avait beaucoup d'affection pour Isaac, qu'il traitait comme son fils, mais il prenait garde à ne pas porter ombrage à son père. L'enfant hériterait peut-être un jour de la terre de Naalein, tous deux l'espéraient, mais il devait savoir à qui il le devrait. Bernard servait d'autres seigneurs depuis si longtemps qu'il avait grande habitude de s'effacer. Lorsque Jehan hocha la tête, le gamin se mit à se trémousser sur le banc, fébrile.

« Tu y seras mon page, donc apprends bien à ouvrir tes yeux et tes oreilles et, surtout, garde ta bouche fermée et tes mains calmes. La Cour le roi, ce n'est pas manse de vilain.

--- Je ne vous ferai pas hontage, mon oncle. Je vous le promets.

--- Je le sais bien, et je compte sur toi pour qu'il en soit toujours ainsi. »

### Jérusalem, rue de Josaphat, fin d'après-midi du mercredi 22 octobre 1152

Isaac avançait d'un bon pas, négligeant la sueur qui commençait à lui parcourir la colonne vertébrale. Il ressassait la leçon du jour, l'esprit enflammé par les aventures du jeune marin qu'il venait de découvrir pendant son cours. Bien qu'héritier d'un opulent marchand, il avait dilapidé sa fortune en peu de temps et espérait se refaire par le commerce maritime. Mais le destin avait voulu qu'il prenne une énorme baleine pour une île et il fut abandonné là, peu avant que l'animal ne plonge. Son professeur, le vieux Baruch, l'avait rassuré : le héros allait connaître mille péripéties, mais finirait fort riche.

Il bifurqua sur sa gauche, empruntant la grande voie de Saint-Jean l'Évangéliste puis la rue du Temple. Il préférait demeurer au loin du Saint-Sépulcre, toujours encombré de dévots. Il envisageait juste de passer par Malquisinat, salivant à l'avance de ce qu'il pensait s'offrir comme récompense après une après-midi studieuse. Il avait eu les plus grandes peines à consacrer une partie de ses journées à étudier encore et encore, et n'avait accepté que de mauvaise grâce au début. Mais au fil des mois et des années, il appréciait de plus en plus les capacités que ce savoir mettait à sa portée. Il lui arrivait de surprendre des échanges dans la rue entre des commerçants, de reconnaître des mots lancés entre deux fellahs tandis qu'ils traversaient un casal. Et si ses amis se moquaient de lui, il leur démontrait sur le champ qu'il n'en négligeait pas les armes ni la lutte pour autant.

En pleine croissance, il s'était un peu affiné et avait fait oublier sa réputation de petit gros, qu'il avait traîné des années durant. Il n'était néanmoins pas très grand, ce qui était un avantage pour la lutte, semblait-il. Mais il n'était jamais à l'aise quand il devait monter un haut animal, avec tout le fourniment. Leur jeu, de sauter en selle depuis l'arrière équipés de pied en cap, de la maille au casque, ne lui plaisait que moyennement. Son aine gardait la trace douloureuse d'un troussequin vindicatif lors d'une tentative avortée.

Dans la rue du Temple, le cortège habituel de pèlerins s'étirait d'un lieu saint à l'autre, bigarré de soldats en armes, de marchands en goguette et de boutiquiers affairés. Parfois un train de mules, de poneys ou d'ânes bloquait le passage et il fallait jouer des coudes, en veillant à ne pas y laisser sa bourse. Certaines boutiques attiraient les curieux, comme les huchiers qui faisaient de magnifiques coffrets ouvragés, dans les senteurs de pin et de cèdre. À un endroit, plusieurs artisans martelaient le cuivre et le laiton, pour décorer plateaux, aiguières et coupes. Mais la plupart des évents se contentaient de revendre des produits fabriqués ailleurs. Parfois les sacs et les paniers s'amoncelaient tellement en devanture que le marchand devait se balancer à une corde pour entrer et sortir de son échoppe.

Il laissa passer un groupe de femmes, des ballots de linge propre sur la hanche, sans se priver d'en admirer quelques-unes au passage. Il récolta quelques œillades amusées, peut-être intéressées. Il savait qu'il n'était pas séduisant, mais sa mise trahissait le jeune homme de bonne naissance et c'était généralement suffisant pour attirer les regards des filles, du moins parmi les plus humbles.

Il stoppa chez un marchand de boulettes de viande, qu'il dévora dans une tranche de pain tout en reprenant son chemin. Il avait fait porter cet achat sur son compte, comme habituellement, et avait noté qu'il lui faudrait très prochainement payer son dû. La Toussaint marquait souvent le terme de nombreuses taxes, loyers et impôts et les commerçants avaient besoin de toutes les pièces qu'ils pouvaient trouver. Verser ce qui était attendu avant qu'on ne nous le rappelle était toujours apprécié, et Isaac se targuait de n'avoir jamais abusé du système.

Lorsqu'il pénétra dans la fraîcheur de la cour intérieure, il aperçut la silhouette massive du maître de maison et se dirigea vers lui, tout sourire.

« Mon oncle ! Vous voilà enfin de retour en l'hostel ! Quelle bonne surprise !

--- Comment te portes-tu, mon grand ? J'ai l'impression que tu prends un pouce à chaque fois que je pars quelques jours.

--- Vous êtes absent depuis avant la saint Michel, mon oncle ! »

Le chevalier grimaça et haussa les épaules, visiblement fatigué malgré sa mine réjouie.

« Le service du roi, garçon. Nul ne peut s'y soustraire ! »

Tout en avançant vers la grande salle, il étudia son neveu des pieds à la tête, satisfait de ce qu'il voyait. Il avait convaincu Jehan de le lui confier quelques mois pour qu'il se familiarise avec la capitale. Bernard n'était que chevalier de soudée, et Jehan châtelain, pour le compte du roi. Mais tous les deux espéraient qu'Isaac finirait seigneur du lieu, en plein titre. Avoir des amis au sein de la Haute Cour serait un atout, et il pourrait nouer des relations avantageuses pour faire carrière. Après tout, Bernard était un familier de la domesticité de Baudoin. il était souvent désigné pour tenir la bannière royale et son neveu pouvait en profiter. Un office, même temporaire, serait du meilleur effet. Un bon mariage plus encore, pour s'assurer de soutiens puissants.

Bernard s'affala sur une chaise curule dans la petite chambre qui lui servait de bureau, indépendante de la salle de réception. Il s'était habitué à cette façon de faire orientale, utilisant de nombreuses pièces de sa demeure à des fonctions précises. La seule zone dont il n'avait jamais eu besoin était le gynécée. Il avait bien des aventures de temps à autre, mais il n'avait aucunement la fibre romantique et peu le désir de s'attacher une famille. Il était entièrement dévoué à son frère et aux siens. Avant même qu'il n'ait eu le temps de demander, son valet lui apporta de quoi se rafraîchir, une bière légère, de l'ail confit et des pistaches.

Isaac s'installa sur un escabeau face à lui, impatient de lui apprendre ses derniers progrès. Bernard sortit d'une besace plusieurs documents et portfolios, les tria en tas sur son bureau tout en grignotant puis lâcha enfin la question tant attendue.

« Alors, quelles nouvelles en l'hostel en mon absence ?

--- Tout va pour le mieux, mon oncle. Guéraud a porté quelques vivres de Naalein et nous avons aussi pu acheter du bois pour l'hiver.

--- De la cire avec les vivres ? J'aimerais faire quelques dons aux autels d'ici.

--- Oui, les mouches à miel ont encore bien donné et vous avez plusieurs bottes de chandelles de bonne cire. »

Bernard avala une longue rasade, essuyant la mousse d'un revers de main.

« Et toi, ton apprentissage ?

--- J'ai réussi plusieurs passages à la lance à travers les annels la semaine dernière !

--- Tu vas bientôt me battre, voilà des années que je n'ai poigné le fer, ironisa le chevalier.

--- J'ai également pu chasser avec le comte de Jaffa. Nous avons traqué un lion par-delà le Jourdain. »

Bernard sourit. Il n'avait pas eu l'occasion de participer à ce genre de réjouissances depuis si longtemps ! Dès avant même la mort du vieux Foulques, son expertise des territoires musulmans était sans cesse mise à contribution, et il se faisait parfois l'impression de n'être qu'un clerc, un de ces évêques ambassadeurs toujours par monts et par vaux. Isaac commença soudain à parler arabe, avec un accent et des hésitations, mais ses progrès étaient évidents :

« J'ai aussi beaucoup pratiqué avec maître Baruch. Il dit que j'apprends bien.

--- Il est donc meilleur professeur que je ne le pensais. J'ai toujours eu méfiance des Juifs.

--- Maître Baruch est homme de savoir, et assez aimable avec moi.

--- J'aurais préféré un mahométan, tant qu'à faire.

--- Ne m'avez-vous pas enseigné de juger selon ma raison et non mes sentiments, mon oncle ?

--- Touché ! », opina Bernard en français avant de reprendre un peu de bière, goguenard.

### Jérusalem, palais royal, soirée du mardi 18 novembre 1158

Ernaut traînait dans les cuisines, comme souvent, dans l'espoir de grappiller quelques gourmandises avant le repas. Il n'était pas de service ce soir, mais avait passé la journée avec d'autres sergents, pour assister le sénéchal dans des tâches comptables. En l'occurrence, c'était plutôt de la manutention car ils avaient apporté plusieurs caisses de monnaie depuis l'atelier royal de frappe monétaire. Avec sa stature hors norme, Ernaut était fréquemment désigné pour ces travaux fatigants. Il ne s'en plaignait pas, profitant de l'occasion pour se pousser auprès des responsables. En un peu plus d'un an, il n'avait guère progressé dans la hiérarchie, chacun veillant à protéger ses intérêts propres, mais il était néanmoins assez connu. Le roi lui-même avait remarqué sa présence, disait-on.

« Si tu demeures dans les environs alors que le repas va être servi, je vais t'arracher les boyels pour en faire des saucisses » lui lança Clarembaud, un des maîtres queux.

Il ne força pas sa chance et s'esquiva, un quignon de pain à la main. En tant que sergent, il avait droit d'être nourri, en bas bout, parmi les domestiques, mais il lui fallait attendre encore qu'on ait fait corner le repas. Il décida de s'accorder un peu de repos dans une des cours voisines où des bancs appuyés contre les murs servaient parfois aux commis de cuisine pour plumer et vider les animaux. À cette heure, il y aurait de la place.

L'air de fin de journée était chargé des senteurs de l'activité diurne, mêlées des parfums des plantes aromatiques cultivées là. Et si la clameur des rues proches, incessante étant donné la proximité des lieux de pèlerinage, ne s'entendait guère, c'était parce qu'elle était remplacée par l'effervescence du palais. D'autant qu'il communiquait avec celui du Patriarche et que le tout ne formait qu'un vaste îlot au sein de la Cité, accolé au monastère et à l'église du Saint-Sépulcre.

La meilleure place était déjà occupée par un jeune homme visiblement épuisé. Sa tenue de voyage était pleine de poussière et il s'appuyait contre le mur, paupières fermées, jambes tendues et bras croisés. Ernaut allait s'éloigner pour ne pas le réveiller lorsque celui-ci ouvrit les yeux et le salua de la tête après avoir soulevé un sourcil, surpris de la masse devant lui.

« J'ai cru un instant qu'on m'envoyait quérir...

--- Garde repos. Je fais pareil que toi, compère, je cherche un endroit où souffler un peu. Tu es de quel hostel ? »

Ernaut s'affala à ses côtés tandis que l'autre lui répondait.

« Vacher.

--- Ah oui, je vois. Bernard, c'est ça ? C'est un familier de la Cour, souventes fois porte-bannière. Je le vois souvent aller et venir. On le dit grand voyageur et fort habile avec les Mahométans. Tu dois en voir du pays... »

La remarque était faite avec une envie non dissimulée, ce qui fit sourire Isaac.

« Pas tant que j'en aurais espoir. Je suis plus souvent en son hostel en la Cité que chevauchant au loin.

--- Souvent Fortune tourne sa roue. Notre temps viendra, compère... »

Ils furent interrompus par l'irruption d'un soldat, épée au côté.

« Maître Isaac, votre oncle vous mande auprès de lui. Il vous veut à ses côtés pour le repas de ce soir. »

Le jeune homme acquiesça en silence et commença à se lever tandis qu'Ernaut fronça les sourcils :

« Ton oncle ? Heu, je veux dire : *votre* oncle ? Je fais excuse, je vous ai pris pour un valet.

--- Nulle offense, je ne suis rien ici. Même pas encore chevalier... Tu as certainement plus important statut que le mien.

--- Je ne suis que sergent le roi, et pas un des plus renommés.

--- Voilà déjà bel office, que de servir son roi. Je ne peux en dire autant. Puissions-nous partager ce destin à l'avenir... »

Il tendit sa main amicalement :

« Je suis Isaac Vacher.

--- Ernaut de Vézelay. »

La réponse fit sourire Isaac, qui lui adressa un clin d'oeil.

« Vu où nous sommes, je te nommerais plus volontiers Ernaut de Jérusalem. » ❧

### Notes

Si par son besoin même de migrants latins pour maintenir ses structures (impérativement catholiques), les territoires francs levantins offraient des perspectives de promotion sociale, rien n'était pour autant simple. On cite souvent Renaud de Châtillon comme l'archétype du jeune noble porté aux plus hautes sphères par ses mérites, une politique matrimoniale audacieuse et une volonté inébranlable. Mais pour un homme tel que lui, il devait s'en trouver des centaines qui échouaient dans les sables du désert syrien, ou végétaient dans des cercles périphériques du pouvoir.

Il existait un statut de chevalier dit de « soudée », qui désignait un noble, mais payé ainsi qu'un soldat. Pour un montant annuel très élevé, mais il demeurait rien moins qu'on combattant rémunéré pour ses services, dont la pension pouvait être révoquée à tout moment. D'autres pouvaient également se voir confier les possessions d'autrui, et le roi de Jérusalem avait l'habitude de nommer des châtelains pour superviser les fortifications dépendant directement de son pouvoir. Bien que cela permît parfois de faire souche, on peut suivre le destin de tels chevaliers qui n'eurent jamais la possibilité d'aller plus haut dans la hiérarchie. Ils étaient des centaines à servir le roi de Jérusalem de façon plus ou moins directe par les levées de troupe alors que les places prestigieuses au Haut Conseil étaient rares.

### Références

Mayer Hans Eberhard, »Angevins versus Normans: The New Men of King Fulk of Jerusalem », dans *Proceedings of the American Philosophical Society*, vol.133, n°1, Mars 1989, p. 1-25.

Prawer Joshua, *Crusader Institutions*, Oxford University Press, Oxford, New York : 1980.

Shagrir Iris, *Naming Patterns in the Latin Kingdom of Jerusalem*, Prosopographica et Genealogica, Oxford : 2003.

Smail R.C. , *Crusading Warfare, 1097-1193*, 2nde édition, Cambridge University Press, Cambridge : 1995.

Gobelet rituel
--------------

### Jérusalem, porte de David, matin du lundi 3 juin 1157

La chaleur sèche venue de l'est semblait décidée à s'éterniser dans les monts de Judée, et la nuit n'avait pas apporté la moindre fraîcheur. Hommes et bêtes étaient suants, maculés de poussière, irrités par le sel de leur transpiration, agacés par les mouches. L'ombre des murailles ne suffisait pas à faire oublier l'impitoyable morsure solaire de l'Orient.

Dans un calme relatif, les caravanes pénétraient sous le regard des sergents et les contrôles des officiers de la douane. À côté, comme toujours, des pèlerins avançaient, le dos courbé, mais les yeux levés, un air béat sur leurs visages fatigués. De temps à autre, des messagers apportaient des nouvelles de l'ost royal, en campagne vers Panéas[^23] pour l'en délivrer d'un siège mis en place par les Turcs d'Alep.

Parmi les hommes occupés à trier les arrivants et à surveiller d'éventuels fraudeurs, Ernaut demeurait un des rares à faire montre de zèle. Il était encore peu fréquent qu'il ne soit pas affecté à des tâches secondaires et tenait à montrer son sérieux et son engagement à chaque fois qu'il en avait l'occasion. Le moindre paquet, la plus petite sacoche qu'il pensait pouvoir assujettir à une taxe était soumis à une inspection tatillonne. Amusés, plusieurs de ses collègues l'observaient tracasser tous ceux qui passaient à sa portée. Ils échangeaient des regards conspirateurs, impatients de le voir se confronter à un récalcitrant. Mais ce fut l'arrivée d'un trio hétéroclite qui les fit reprendre leur sérieux.

Les trois hommes arboraient des tenues grises d'avoir longtemps marché, peinaient comme s'ils allaient s'effondrer à chaque pas. Leur mise modeste et la légèreté de leurs bagages auraient pu inciter à la mansuétude de la sergenterie. Mais Ernaut n'y discernait nulle croix cousue, pas plus que de besaces à leurs flancs. Ils n'étaient pas pèlerins et pouvaient donc se voir exiger quelques taxes à leur entrée en ville. On ne voyageait pas sans raison, et la première d'entre elles était le commerce, quand bien même il s'agissait de denrées bien discrètes.

« Le bon jour, voyageurs. Avez-vous marchandie soumise à taxes ?

--- Nous sommes sergents le comte d'Édesse, porteurs d'importantes nouvelles. »

L'homme qui avait répondu paraissait anxieux, les yeux incapables de se fixer sur un point quelconque. Ses deux comparses arboraient un air contrit, peu soucieux de décoller le regard de leurs savates.

« Je ne vois nulle boîte à ta ceinture[^24]. Tu as bien quelque document ou sceau à montrer pour que je te laisse passer outre ? »

L'homme sortit un anneau où étaient gravés quelques symboles. Certainement les armes du comté d'Édesse, mais Ernaut n'y avait jamais été confronté. Si l'on évoquait encore le territoire, il n'était plus aux mains des catholiques depuis plusieurs années et la plupart des Courtenay étaient désormais exilés loin de leurs terres quand ils n'étaient pas dans des geôles musulmanes. Inquiet de faire une bévue, il lança un coup d'œil vers ses collègues, malheureusement tous occupés. Il hésitait, se mordit la lèvre, passant une manche sur son front en sueur. Le soi-disant valet avait tout de même un gros paquet au côté.

« Et que portes-tu en ce havresac ? Si tu fais commerce de marchandies, valet ou pas, tu dois t'acquitter des droits. »

L'autre arbora un air de conspirateur et se rapprocha d'Ernaut, un peu gêné.

« Il s'agit du chef de Fils-Foulque le Lépreux... Durement déniché après moult difficultés. Le sire maréchal a grand désir de la récupérer.

--- Tu trimballes une tête dans ton baluchon, compère ? Tu te moques de moi ? Mouches et vers en dégorgeraient, vu la chaleur. »

Sa réponse alluma une lueur narquoise dans l'œil du serviteur, qui le toisa alors avec amusement.

« Sire Fils-Foulque est mort voilà bon nombre d'années, tué par Dodequin[^25]. »

Le valet se rapprocha encore.

« Le farouche turc ne s'est pas contenté de lui trancher le col, il a fait de son crâne son gobelet favori. »

Ernaut fit une grimace à la mention de l'exaction. Tout en parlant, l'autre avait entrouvert son sac, y découvrant un paquet de linge. Il semblait inquiet à l'éventualité de trop en dire ou trop en montrer parmi la foule. Ernaut renâcla à l'idée de contrarier le maréchal, dont le caractère difficile était connu. Il était actuellement absent, au nord avec le roi, mais n'apprécierait certainement pas qu'un jeune sergent récemment promu ait pu gêner l'exécution de ses instructions. Il fit un signe de la main.

« Vous pouvez entrer. Et la bien venue en la sainte Cité ! »

En disant cela, il risqua encore un regard vers Baset et les autres, mais aucun ne semblait lui prêter la moindre attention. Seul Droart était inoccupé, à l'entrée du bâtiment des officiers de la douane, arrachant d'ultimes gorgées à une outre. Il s'en approcha, l'air faussement badin.

« Dis-moi, Droart, as-tu déjà ouï parler du sire Fils-Foulque et de Dodequin ?

--- Bien sûr, voilà histoire souventes fois narrée à la veillée, pour bien rappeler la férocité et la duplicité des Turcs. Pourquoi donc t'en soucier ? »

Ernaut haussa les épaules et s'empara de l'outre, sans plus rien en dire, laissant Droart dans l'expectative.

### Jérusalem, palais royal, entrepôts, midi du vendredi 7 juin 1157

La lumière qui entrait par les portes largement ouvertes du cellier dessinait des volutes dans les tourbillons de poussière dansante. Un des intendants, tablette de cire et stylet à la main, inspectait chacune des amphores qu'on lui présentait. Les sergents entreposaient délicatement celles qui pouvaient encore être utilisées sur des banquettes spécialement aménagées, celles à nettoyer dans un appentis extérieur et, enfin, celles qui étaient fissurées ou cassées dans des paniers pour être recyclées ou jetées.

Ernaut trouvait la tâche aisée, lui qui avait grandi en manipulant les encombrants tonneaux de vin chez son père. Il s'étonnait tout de même de se voir affecté à de tels travaux. Le service royal lui apparaissait soudain bien moins prestigieux que dans ses rêves. Au fin fond des méandres du palais, dans un bâtiment sombre et malodorant où s'entassaient des céramiques de stockage de toutes tailles. Droart, responsable du tri, lui avait expliqué qu'ils appartenaient avant tout à la domesticité, et se trouvaient au services du sénéchal avant celui du maréchal. Ils maniaient donc plus souvent les approvisionnements que les armes.

« Je n'aurais cru qu'on puisse tenir tant de pots en celliers, roi ou pas, souffla Ernaut en s'abattant sur une banquette.

--- Il faut bien mener les denrées depuis casals et chastiaux. Entre huile et vin, il y a bon usage de tous ces vaisseaux, lui répondit Droart, tout en lui tendant la miche apportée par un des gamins de cuisine.

--- Grand dépit qu'il n'y ait nul fleuve pour acheminer barrels.

--- Et de quel bois les ferais-tu ? Ici on a plus de terre à pots que d'arbres. »

Ernaut opina en silence et déchira avec appétit le pain qu'il trempa dans la purée froide aux oignons. Il appréciait les repas au palais, sans jamais avoir réalisé ce que cela impliquait en terme de denrées à conserver. Il découvrait les gigantesques entrepôts nécessaires à alimenter une population aussi nombreuse.

Bertaud, un des scribes, pénétra soudain dans le local, l'air désemparé. Il avait le visage rouge d'avoir couru, lui habituellement si calme.

« Il est là, le nouveau ? »

Puis, l'apercevant, il se dirigea vers Ernaut, plissant les yeux pour y voir dans la pénombre après la clarté de la cour.

« C'est toi qui a vu passer le crâne du sire Fils-Foulque ?

--- Voilà quelques jours, à la porte de David, oui-da.

--- Tu es acertainé que c'était bien la coupe de Dodequin ?

--- Le valet me l'a affirmé, mais je ne saurais dire...

--- Il faut t'en assurer ! On ne retrouve plus le paquet ! Si le maréchal apprend cela, il est capable de nous en faire percer le poing ! Surtout à toi, tu es le dernier à l'avoir vu. »

Droart, surpris par la véhémence des propos, s'approcha et secoua la tête.

« Comme tu y vas ! Il n'y a pas larcin !

--- Tu iras le dire au sire Joscelin, on verra bien s'il te laisse ta langue... »

Ernaut commençait à s'inquiéter, angoissé à l'idée d'avoir gaffé si tôt après son embauche. Il se leva, frottant sa cotte pour l'en débarrasser des miettes et de la poussière.

« Que puis-je faire pour remédier à cela ?

--- Il te faut mettre la main sur ce maudit vase !

--- Je ne sais même pas...

--- Va donc voir Brun, de la Secrète[^26]. C'est un mien ami. Il saura si on aurait pu celer le gobelet en le trésor. »

Hochant le menton avec énergie, Ernaut s'efforça de prendre un air plus assuré qu'il n'était vraiment. Puis il s'élança vers l'aile du bâtiment abritant les services fiscaux. Il ne savait pas qui était ce Brun, mais questionna tous les clercs qu'il croisait, traversant les vastes salles de compte, les escaliers aux marches usées, les petits réduits d'archives, pour finalement dénicher l'administrateur dans une alcôve d'une large pièce voûtée d'arêtes. Il travaillait sur un lutrin à la lumière du jour, recopiant des tablettes de cire posées sur un plateau à ses côtés. Les traits lourds, la panse rebondie, entre deux âges, il manifestait l'entrain d'un animal mené à l'abattoir et la vigueur d'un centenaire. Mais sa main traçait les symboles avec légèreté, sans trembler un instant lorsque la grosse voix d'Ernaut résonna dans le tranquille endroit.

« Mestre Brun, je viens de la part de Bertaud, au sujet de la coupe... »

Des sourcils froncés avec vigueur et une moue horrifiée le stoppèrent net dans sa déclaration. Le clerc susurra alors :

« Si c'est la fameuse coupe à laquelle je pense, il serait bon de ne pas hurler comme pâtre en herbage.

--- De vrai, reprit Ernaut plus doucement. Auriez-vous quelques nouvelles à son propos ?

--- J'ai bien vérifié les inventaires, elle n'a certes pas été posée au trésor. Je ne l'ai vue consignée nulle part. »

Devant l'air catastrophé d'Ernaut, il ajouta d'un ton se voulant engageant :

« Mais j'y ai pourpensé : si ces valets sont bien venus en le castel, après si long voyage, il est fort probable qu'ils ont quémandé écuelle et gorgées de vin. Tu devrais t'enquérir aux cuisines. »

### Jérusalem, palais royal, cuisines, début d'après-midi du vendredi 7 juin 1157

Pour rejoindre l'aile où étaient installés les foyers et ateliers de préparation des repas, Ernaut dut refaire tout le tour des bâtiments, craignant de devoir traverser les salles principales. Il y avait toujours le risque de tomber sur un haut baron dans la grande aula ou aux environs des chambres princières et royales. Il n'était pas naïf au point de penser qu'il pourrait passer inaperçu tandis qu'il parcourrait les lieux l'air empressé et le souffle court.

Ce fut Clarembaud, un des coqs responsables, qui l'interpella alors même qu'il venait à peine de poser un pied dans l'endroit. Quelle que soit l'heure du jour, et parfois de la nuit, l'activité était telle que le moindre intrus était immédiatement détecté et généralement vertement tancé. Cette fois ne fit pas exception, Ernaut gênait le passage de plusieurs valets trimballant des paniers emplis de poissons que des marmitons s'empressaient de vider, riant de la puanteur des entrailles qu'ils entassaient sur l'immense table.

Ernaut s'efforça de montrer sa bonne foi en se disant à la recherche des hommes du comté d'Édesse, se prétendant mandé en cela par le vicomte. Il était peu probable que personne n'ose jamais en demander vérification auprès de ce dernier. La mention d'Arnulf calma un peu les ardeurs de Clarembaud qui réfléchit un petit moment, tout en distribuant quelques tâches et remarques bien senties autour de lui comme s'il n'y prêtait pas attention.

« Je crois avoir souvenance de ces trois drôles. Maigres comme belette et gloutons comme goupils. Ils m'auraient dévoré dîner d'évêque si je les avais écoutés. Mais ils sont partis, de ce que j'en sais. Ils avaient à faire avec le sire maréchal et sont allés tout droit demander après lui.

--- Mais il est parti depuis un moment. Ont-ils pris la route pour le trouver ?

--- Et comment j'le saurais, crâne de puce ? Va donc voir auprès de son hostel. »

Ernaut s'en figea de surprise. Aller ainsi se faire connaître auprès des serviteurs de la famille Courtenay ne lui semblait guère judicieux. Voyant son indécision, Clarembaud s'agita un peu, frappant la table devant lui de l'épaisse louche tenant lieu de sceptre en cette salle.

« Sinon y'aura bien un des jeunes là-haut qui saura quoi. Reste pas là, j'ai l'impression d'avoir une grosse vache perdue au parmi de mes cuisines. Va donc demander à Raoul, le petit scribe, y devrait pas te faire peur çui-là ! »

Ernaut voyait à peine de qui Clarembaud voulait parler, mais la mention d'une petite personne l'incitait à la confiance. On rencontrait plusieurs apprentis dans les salles notariales et de compte, porteurs de messages et de tablettes, aussi discrets que des souris et à peu près aussi vaillants.

Après s'être fait refouler de la grande pièce de l'échiquier, il apprit que le gamin était aux archives. Régulièrement, les sacs de cuir contenant chartes et documents anciens et importants étaient inspectés pour vérifier leur bon état. On en profitait pour nettoyer et balayer les lieux de fond en comble, à la recherche de la moindre crotte de rongeur pouvant indiquer une future infestation. Une armée de chats était dorlotée pour prévenir toute invasion, mais cela demeurait, avec les lampes, et donc le feu, la principale angoisse des archivistes.

Raoul s'avéra n'être plus un gamin et, installé derrière une table à tréteaux dans une salle aux murs éclatants de blancheur, il supervisait le dépoussiérage des sacs avant d'en vérifier le bon état. Dans un coin de la pièce, une pile soigneusement façonnée était disposée sur une banquette carrelée de terre cuite.

« Je me souviens fort bien de ces trois hommes. »

Il baissa d'un ton et se rapprocha d'Ernaut.

« Ils ont réussi à remettre la main sur le chef du Fils-Foulque, dérobé aux héritiers de Dodequin.

--- Ils te l'ont confié ?

--- Certes pas ! C'est là coupe sertie de pierreries, d'or et d'argent. Ne le sais-tu pas ?

--- Je n'en avais jamais entendu parler. »

Raoul tira Ernaut par le bras jusque vers la porte donnant sur un petit corridor, s'exprimant à voix basse tout en surveillant ses subordonnés.

« Fils-Foulque était grand baron d'Antioche, au courage sans pareil malgré la lèpre qui le rongeait. Il connaissait fort bien les Mahométans qu'il affrontait sans relâche. Capturé par Gazzi en même temps que le prince Roger, il a été vendu ensuite à son plus farouche adversaire, Dodequin. »

Raoul marqua une pause, comme hésitant à continuer, puis reprit.

« C'était là un Turc de fort sinistre mémoire. Il aimait à se baigner dans le sang de ses ennemis vaincus, et on le disait soûlard sans limites. Il a tranché la tête de Fils-Foulque de son sabre et en a fait son gobelet préféré pour s'enivrer. »

Ernaut fit une moue dégoûtée, encouragé en cela par un hochement de tête de Raoul.

« Récupérer sa tête permettra de l'inhumer chrétiennement.

--- Crois-tu qu'ils l'ont déjà mise en terre ?

--- Certes pas. Ils avaient désir de voir le maréchal, mais tout son hostel est parti avec lui. Et ils ne vont pas s'aventurer au-dehors avec pareil trésor.

--- Ils sont encore au palais ?

--- Je leur ai dit de voir avec un des hospitaliers du chambellan, moi je ne réside pas ici. »

Ernaut était bon pour se rendre dans la cour de l'hôpital, où on accueillait les visiteurs que Gauvain de la Roche se devait de gérer, avec leur maisonnée. Gauvain régnait sur un empire de valets, domestiques et pages divers, pour l'hôtel du roi, à l'exclusion des services administratifs généraux, des finances ou de la guerre. Il avait également la haute main sur l'organisation des couchages et des hébergements.

### Jérusalem, palais royal, hôtellerie, après-midi du vendredi 7 juin 1157

Peu désireux de se confronter aux responsables les plus importants de la maison royale, Ernaut avait dirigé ses pas vers le lavoir où les lingères travaillaient le plus souvent. Il y a avait tellement de drap et de toiles à nettoyer ou repriser qu'il s'en trouvait toujours quelques-unes à battre l'étoffe ou à tirer l'aiguille. Une autre de leurs activités favorites était la propagation des rumeurs, cancans et potins. L'arrivée de ce jeune homme au physique impressionnant déclencha des sourires entendus et des regards égrillards. On n'accordait que peu de vertu à la plupart de ces femmes, qui en jouaient donc.

Il n'eut pas plus tôt posé le pied sous l'auvent que crépita une voix de crécelle au débit rapide :

« Y'a pas de miel ici, l'ours, tourne tes pas ! »

La saillie déclencha de nombreux rires, mais Ernaut, bien qu'empourpré, ne recula pas. L'agresseur ne devait mesurer guère plus de cinq pieds et se tordait le cou, aussi maigre que le reste, pour défier Ernaut du regard.

« Je cherche trois valets du sire comte d'Édesse qui font héberge au palais.

--- Et vois-tu l'un d'eux ici ? Crois-tu qu'on le cache sous nos robes ?

--- Je me suis dit que vous sauriez peut-être où je pourrais les trouver. »

La commère lança à la cantonade, sans lâcher son aiguille et son fil :

« Et v'là qu'y nous prend pour les clercs du chambellan !

--- Allons, Gelta, effraie pas ce pauvre poussin ! »

La lingère haussa ses maigres épaules sans désarmer.

« Et y f'ra quoi pour moi si je réponds à sa question ? J'y gagne quoi ?

--- Je pourrai vous aider à tordre les draps !

--- Ah, voilà qui sonne comme lai de damoiseau amoureux à mes vieilles oreilles... Avec tes bras de Samson, t'auras guère de peine. Tu t'en viendras pour les grandes lessives nous porter assistance, je compte dessus.

--- J'en fais serment !, confirma Ernaut avec espoir.

--- Y sont partis se désoiffer chez le père Josce. Le chambellan aime guère voir valets traîner savates sans trimer alors y s'sont esquivé au matin. »

Ernaut se rua vers la cour en levant la main pour remercier, sans s'apercevoir des rires qui saluèrent son départ.

### Jérusalem, taverne du père Josce, fin d'après-midi du vendredi 7 juin 1157

La taverne fit l'effet d'une douche froide sur les épaules d'Ernaut, qui avait quasiment couru depuis le palais. Essoufflé, transpirant, il parcourait du regard la vaste salle au pilier, faiblement éclairée par les étroites fenêtres. Quelques hommes se reposaient là, soldats entre deux corvées, valets se cachant d'un maître trop exigeant. Mais en journée on n'y voyait que peu de monde. Les gens venaient chercher leur vin et emportaient leurs pichets sans s'attarder. Ce n'était qu'en soirée que parfois la sergenterie du palais s'acoquinait avec celle des grandes maisons, bavardant et faisant rouler les dés, écoutant à l'occasion un jongleur de passage.

Ernaut reconnut immédiatement les trois hommes, dont l'aspect semblait moins miteux. Ils étaient installés autour d'un tabouret où deux d'entre eux déplaçaient des méreaux sur un quadrillage. Il s'approcha d'un pas décidé, attira le regard de celui qui ne jouait pas.

« Le bon jour, j'ai couru après vous depuis un moment !

--- Y a-t-il souci, sergent ?

--- Il se trouve que l'hostel le roi aimerait bien assavoir où se balade le chef du sire Fils-Foulque ! »

L'autre baissa d'un ton, l'air affolé, tandis que ses deux acolytes levaient la tête, interloqués.

« Nul besoin de crier ! Il demeure à mon flanc, comme il est depuis nombre de jours. Il y demeurera jusqu'à ce que je le remette au sire maréchal !

--- Il serait mieux gardé au castel.

--- J'en crois rien. Qui pourrait savoir qu'il est là ? En dehors d'une poignée, dont tu es... »

Il se recula d'un pas, appréciant la stature impressionnante du jeune homme.

« T'as pas idée de me le dérober j'espère ? Tu viens le prendre ?

--- Nenni, je suis féal servant le roi. »

L'autre paraissait à demi rassuré, jetant un coup d'œil autour de lui. Ses deux compagnons s'étaient levés. Puis il souffla, posa une main amicale sur l'épaule d'Ernaut.

« De vrai, tu sembles bon garçon, tout respire droiture et franchise chez toi. »

Il l'invita d'un geste à s'asseoir avec eux et lui remplit un gobelet.

« Aurais-tu désir de le voir ? Il n'y a pas grand monde, c'est l'occasion. »

Ernaut n'hésita qu'un instant. Chez lui la curiosité l'emportait généralement sur tout. Il posa son verre et avança la main vers le sac. Le valet lui tendit une boule d'étoffe sale, l'invitant à dégager la coupe avec délicatesse.

Et il découvrit dans les linges une poupée de bois et de chiffon grossièrement peinte de couleurs vives, au visage hilare.

« Qu'est-ce... »

Les trois hommes éclatèrent de rire.

« C'est toi qu'a la vache, mon gars, désormais. Et tu devras payer la tournée à tous, ce soir, comme le béjaune que tu es ! »

Les rires redoublèrent et le père Josce se joignit à eux quand on lui montra le jouet. Ernaut secouait la tête, ne sachant quoi penser. Ce fut le tavernier qui lui expliqua.

« C'est la tradition dans la sergenterie, garçon. On fait courir le béjaune nouvellement arrivé après la Vache, et quand il la trouve, il doit payer godets à tous les anciens. Puis il se charge de la passer à celui qui arrive après lui !

--- J'ai eu la Vache à la Noël, moi. Ils m'ont fait croire que c'était les langes de l'Enfant Jésus lui-même » confirma le valet conspirateur.

Ernaut soupesait le jouet dans ses énormes mains, ne sachant s'il devait sourire ou s'emporter. Puis il porta un verre de vin à ses lèvres après avoir adressé un toast silencieux à son tourmenteur de la journée, pensant « Une coupe dans un crâne de chevalier, mais quel crétin je suis ! » ❧

### Notes

Le bizutage est une pratique connue dès le XIIe siècle dans les universités européennes, où les « béjaunes » sont soumis à toutes sortes d'épreuves ou de vexations avant de devoir être mis à l'amende en abreuvant leurs aînés. Avec le temps, les débordements furent de plus en plus fréquents bien que la coutume demeurât vivace jusqu'à notre époque, pour donner ce qu'on appelle désormais des Week-End d'Intégration. On n'a aucune source attestant cet usage au sein d'autres confréries qu'étudiantes/cléricales à cette période, mais il me semblait que cela ne serait pas incongru dans une administration en partie militaire. Dans l'armée, la recherche de la clef du champ de tir par les nouvelles recrues était devenue un classique au siècle dernier.

La légende qui fait courir Ernaut est basée sur des récits autour de la disparition de Robert Fitz Fulk, seigneur de Zerdana (dans la principauté d'Antioche), capturé lors de la fameuse bataille des Champs de Sang qui vit la mort de Roger de Salerne, prince d'Antioche en 1119. Robert a vraisemblablement été livré par Il-Ghazi à Tughtekin, seigneur de Damas, avec lequel il entretenait selon Usamah ibn Munqidh des relations plus ou moins amicales. Mais celui-ci, après l'avoir décapité, aurait fait réaliser une coupe avec son crâne. Comme on prête énormément de vices au dirigeant damascène ainsi qu'une cruauté sans égale, il est difficile de se prononcer sur le sérieux de ces allégations. Mais de telles pratiques sont attestées parmi les populations nomades de la steppe eurasienne, comme la coupe fabriquée par le khan petchénègue Kurya avec le crâne de Sviatoslav Igorevitch de Kiev peu avant l'an mille.

Enfin, le terme utilisé pour désigner le jouet m'a été inspiré par une anecdote familiale du début du XXe siècle que m'a rapporté mon grand-père, lorsqu'on cherchait à interdire l'usage des langues régionales. Dans son école primaire, si un enfant parlait breton, on lui donnait la « Vache », morceau de bois avec une ficelle et qu'il s'efforçait à son tour de refiler à celui qu'il entendait parler breton après lui. L'enfant qui restait avec la Vache le vendredi se voyait puni, contraint de conjuguer « ne pas parler breton » à tous les temps, toutes les personnes, durant son week-end. Autre pratique vexatoire d'intégration, forcée, celle-là...

### Références

Asbridge Thomas, *The creation of the Principality of Antioch 1098-1130*, Rochester: The Boydell Press, 2000.

Asbridge Thomas, Edgington Susan B., *Walter the Chancellor's The Antiochene Wars*, Aldershot : Ashgate Publishing Limited, 1999, p.159-163.

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008, p.131-2.

Acédie
------

### Baniyas, après-midi du mercredi 6 novembre 1140

Un vent léger caressait les contreforts du Golan, faisant frissonner les pins et les oliviers accrochés à la pente. Hauts dans le ciel, des nuages lancés depuis les côtes lointaines couraient dans une mer d'azur. Sur les restanques environnant la cité, citronniers et orangers piquetaient de points colorés la verdure envahissante. Le chanoine avait d'ailleurs noté qu'à plusieurs endroits on récoltait les fruits mûrs à point. Privilège du lieu, il pourrait savourer les jus pressés dont il était si friand.

Profitant de l'arrêt de la caravane à l'entrée des murailles, il descendit de sa mule et fit craquer son dos. Il n'avait pas la trentaine, mais un léger embonpoint et des années d'inactivité physique l'avaient rendu fragile. Il espérait que le confort des installations serait à la hauteur, ne doutant pas de la présence de bains étant donné la richesse en eau de l'endroit. On y cultivait d'ailleurs aussi un riz réputé. Sur le vieux rempart, il aperçut des silhouettes déambulant, l'air nonchalant. La bannière de Rénier Brus flottait désormais sur les tours de l'enceinte, ainsi que celle du roi Foulques de Jérusalem, sur le donjon. Nul clocher ne s'élançait vers les cieux, seulement un minaret plus élevé que les autres, au sommet duquel on avait hâtivement dressé une croix.

Roger sourit pour lui-même. Il était de ceux qui allaient rendre la région à la vraie Foi. Ce n'était pour l'heure qu'un avant-poste sur la route de Damas, modeste, mais avoir été érigé en évêché indiquait bien le désir d'en étendre le territoire. Et vers où si ce n'était vers les cités infidèles, au Levant ? Arrivé comme novice d'un chapelain accompagnant un riche croisé, Roger était demeuré au Saint-Sépulcre et y avait prononcé ses vœux. Il avait depuis vu le royaume se renforcer et se développer au fil des ans, malgré les revers et les difficultés.

Ayant choisi de marcher à côté de la file des animaux de bât pour soulager son postérieur et son dos, il déambulait, un sourire bonhomme sur le visage. La plupart des habitants qu'il rencontrait étaient des indigènes, qui avançaient le regard bas, la mine triste. De rares Latins se croisaient, généralement en armes ou occupés à emménager dans les meilleurs bâtiments. Quelques-uns le saluèrent de loin, visiblement satisfaits de voir s'installer un nouvel ecclésiastique.

L'évêque Adam, ancien archidiacre d'Acre n'arriverait pas avant plusieurs jours. La demi-douzaine de chanoines nommés pour l'assister allait s'activer à en préparer la venue. Tout était à faire, ce qui réjouissait la face rubiconde de Roger. Il servirait comme trésorier, apportant dans ses sacs et bagages les instruments indispensables aux célébrations selon la liturgie romaine. Il n'avait pris que l'essentiel, fruit des dons d'autres établissements agrémentés des offrandes de fidèles argentés. Il comptait dès à présent démanteler le lieu de culte mahométan pour en refondre la majeure partie des décors. En outre, il avait appris que Rénier avait interdit qu'on en pille les riches tapisseries, et n'en avait distrait que de rares exemplaires pour ses besoins propres. Il y avait donc un beau butin qu'il pourrait revendre, n'ayant que peu usage de tels artefacts pour l'ornementation du sanctuaire.

Les rues qu'il parcourait lui semblaient en assez bon état, malgré quelques déprédations récentes. De nombreuses boutiques et ateliers s'entrevoyaient dans des cours plus ou moins closes, à la mode des autochtones. La file d'animaux s'arrêta finalement dans l'une d'entre elles, adjacente à l'ancienne mosquée. Les huisseries en partie forcées et l'abandon du lieu le frappèrent. Malgré l'animation des bêtes, un vent de désolation soufflait dans l'endroit. À l'étage, une galerie couverte permettait d'en faire le tour à l'abri et il y aperçut un autre clerc, le visage fermé, assistant à leur arrivée. Ce dernier disparut pour surgir au rez-de-chaussée, silhouette mince dans un habit de laine fine d'excellente qualité. Sa barbe taillée et sa chevelure strictement tonsurée encadraient une face longue, au nez pointu. Un sourire chevalin illumina soudain l'austère apparition. Il donna une accolade empressée à Roger avant de l'accueillir plus solennellement d'un baiser de paix.

« Grande joie de vous accueillir en notre clôture, frère. J'ai nom Paul d'Hébron et serai céans le cellérier.

--- Je suis Roger de Jérusalem, trésorier. J'apporte d'ailleurs de nombreux bagages à mettre au secret au plus vite.

--- Quelle joie de vous recevoir ! Nous pourrons enfin célébrer les heures dignement... »

Il donna quelques ordres d'une voix autoritaire de basse forgée par des heures de chant, avant d'inviter d'un geste Roger à le suivre.

« Nous ne sommes encore que trois arrivés, frère Jorge s'emploie à faire nettoyer ce qui sera le choeur. Nous avons aussi une petite salle fort correcte donnant sur une autre cour, avec une fontaine, qui fera une honnête salle du chapitre.

--- Avez-vous des nouvelles de notre sire Adam ?

--- Il ne saurait tarder, nous avons reçu parties de son bagage dès avant la Toussaint. Le tout l'attend désormais en son palais. Valets en suffisance le serviront et font de leur mieux pour rendre l'endroit acceptable. »

Tout en devisant, ils traversaient des salles et des couloirs, parfois en piteux état, hébergeant souvent les restes d'anciens commerces chassés lors de la prise de la ville. Finalement, Paul s'arrêta devant une échoppe contenant plusieurs recoins, où du mobilier avait été récemment installé. Une porte neuve s'agrémentait d'une impressionnante serrure de métal.

« Dans l'attente, je me suis dit qu'il serait plus judicieux de vous installer ici, vous aurez de place assez pour y resserrer les objets précieux sous votre garde. Nous aviserons plus tard avec le chapitre et notre sire Adam quant à l'avenir.

--- Quand nous aurons été dûment reçus en notre chapitre nouvellement créé et qu'un doyen nous guidera » approuva Roger, tout en posant sa besace près de l'entrée.

Il devait s'agir d'une ancienne boutique dont l'arrière-salle aveugle, voûtée d'arêtes, sentait la poussière, visiblement récemment balayée. Un lit de bonne taille flanqué d'une table basse, un coffre massif aux pentures ouvragées, deux escabeaux et un crucifix au mur meublaient la première pièce.

« Il y a pleintée de lampes en la salle commune où nous prenons nos repas. Je vous y conduirai une fois que vous aurez posé vos paquets et pris un peu de repos. Nous n'avons pas encore de cloche, un valet vous préviendra des offices et repas. Il faudra voir lesquels dépendront de vous, d'ailleurs. »

Souriant une dernière fois à son commensal, frère Paul disparut dans un froufroutement d'étoffe. Roger soupira longuement : une chambre particulière, fermée à clef, des valets en nombre. Son ascension tant espérée commençait-elle enfin ?

### Baniyas, soir du vendredi 28 décembre 1151

Protégé par une épaisse couverture aux motifs bariolés, Frère Roger frottait ses mains devant le brasero dans la salle du chapitre. Du moins ce qui en tenait lieu depuis des années. Comme ils n'étaient qu'une demi-douzaine à peine, ils s'en servaient aussi comme chauffoir, voire office de travail quand les frimas étaient trop intenses. Les années avaient tavelé le visage du chanoine, la bonne chère et les abus de boisson lui avaient offert une panse honnête et un teint rougeaud que le froid avivait. Les fils blancs gagnaient sa tonsure, et ses dents, fort mauvaises, n'étaient plus en si grand nombre que par le passé. Sa diction en était devenue mouillée, quoique sa voix résonnât aussi fort que lorsqu'il était novice.

Avec le temps, il accordait plus de soin à sa parure, la coule originelle de toile simple était plus souvent que nécessaire remplacée par des tenues aux couleurs chamarrées, de bonne coupe. Il arborait également des bagues de qualité et un épais crucifix ciselé suspendu à une lourde chaîne ornait son cou. Malgré cette apparence dispendieuse, il conservait une bonhomie naturelle, entachée de mélancolie. Il était au final aussi peu exigeant avec les autres qu'il l'était envers lui-même, ce qui en rendait le commerce aisé. Un peu de retard sur le versement de la dîme ou du cens n'était jamais dramatique selon lui et toute facilité de paiement pouvait être obtenue, pourvu qu'on en fasse la demande avec respect et humilité.

Emmitouflé comme un nourrisson, il attendait l'arrivée de leurs visiteurs. Bernard Vacher, un des barons du roi, accompagnait des émissaires de l'atabeg de Damas. Ces derniers avaient intercepté un fort parti de pilleurs turkmènes qui avaient dévasté voilà peu les territoires latins voisins. Scrupuleuses de la trêve signée, les autorités de Damas retournaient le fruit de cette razzia au souverain de Jérusalem, en signe de bonne volonté. Malgré sa proximité avec les principautés musulmanes, Roger ne s'intéressait guère à la politique extérieure et se contentait de regretter mollement que l'expansion du royaume se soit arrêtée.

Il connaissait bien Bernard Vacher, qu'il avait pu rencontrer à de nombreuses reprises. C'était un grand hâbleur, mais qui savait faire montre de dévotion et marquait toujours une déférence polie envers les serviteurs de Dieu. On le disait fort ami des mahométans, et d'aucuns le prétendaient même corrompu. Roger se contentait d'apprécier l'homme et lui faisait bon accueil quand il le croisait. Cette fois-ci était particulière : il y avait quelques objets de culte dans le butin repris et le chevalier avait donc proposé que ce soit le chapitre de Baniyas qui en dispose, à charge pour lui d'en assurer le retour en leur emplacement originel.

En l'absence du seigneur de la cité, Onfroy de Toron, Bernard Vacher avait souhaité s'entretenir avec l'évêque, qui lui avait offert de l'héberger pour la nuit. Bien que son palais ne fût que de bric et de broc, assemblage disparate de bâtiments anciens, il était bien plus confortable que l'austère citadelle. La table y était bien plus attrayante que la ration des soldats, même améliorée pour un chevalier de la maison du roi.

La porte s'ouvrit dans un tourbillon de gouttes, entraînant avec elle la fraîcheur nocturne. Trois ombres pénétrèrent, tapant des pieds et grognant après le froid. Tout en se levant pour les accueillir, Roger reconnut la lourde silhouette de Bernard, mais s'arrêta en apercevant les tenues turques chez les deux autres hommes. Le chevalier perçut la gêne et s'avança, un large sourire forçant les traits frigorifiés de son visage.

« Mon père, je suis aise de vous revoir. Je fais escorte à Yalim al-Buzzjani, émissaire de l'atabeg de Damas Mujīr ad-Dīn ʿAbd al-Dawla auprès de notre souverain Baudoin. Il voyage avec un de ses ghilmen[^27], Sawwar. »

Les deux musulmans saluèrent poliment sans ostentation, visiblement mal à l'aise de devoir fréquenter autant de représentants de la religion ennemie. Ils arboraient le sharbush[^28] et la veste croisée des professionnels de la guerre. Sans façon, Bernard les invita à s'asseoir près de lui sur le banc face à Roger.

« Nous avons là une liste des objets de culte, ou possiblement, rapportés par nos amis. Ils sont scellés dans les trois coffres dont je vous ai remis les clefs. »

Bernard tendit deux tablettes de cire au chanoine.

« Nous ferons enquête pour les restituer, soyez-en assurés. »

Il se tourna vers les deux musulmans puis vers Bernard.

« Pourriez-vous les remercier de ce retour d'objets sacrés à nos yeux ?

--- Faites-le vous-même, s'en amusa le chevalier tandis que Yalim déclarait, avec une intonation rauque :

--- J'ai usage de votre langue, je suis souvent en palabres avec les vôtres.

--- Ah ? Bien, bien, bien. C'est heureux ! En ce cas, recevez mes remerciements les plus sincères pour votre maître. »

Le turc inclina la tête poliment tandis que Roger, un peu angoissé, dévisageait Bernard. Jusqu'à ce que ce dernier écarquillât les yeux, en invite à exprimer sa pensée.

« C'est à dire que je voulais vous proposer quelque boisson, mais je n'ai nulle habitude de faire héberge à... enfin, à d'autres que nos coreligionnaires. On dit qu'ils ont le vin en horreur et j'en avais chauffé avec des épices, efficace remède contre les frimas. »

Le Turc haussa les épaules.

« Nulle gêne pour nous, nous ne serions pas contre tout ce qui peut réchauffer, après cette journée à se glacer en selle. »

Le chanoine se leva donc pour aller quérir un valet. Lorsqu'il revint, les trois hommes discutaient bruyamment en arabe, sans qu'il ne lui soit possible de comprendre. D'autant que les voix s'éteignirent quand il souleva la portière.

Ils ne mirent pas longtemps à faire le point sur les objets récupérés, Roger ayant établi sa propre liste descriptive. Cela faisait au final un bel ensemble et le geste du seigneur de Damas lui était complètement indéchiffrable. Néanmoins, il avait appris à ne pas trop s'interroger, incapable qu'il était de suivre les méandres subtils des relations entre les différentes principautés de la région. Ne disait-on pas le royaume au bord de l'explosion, la reine mère et son fils s'arrachant des lambeaux de pouvoir l'un à l'autre tandis que les ennemis de la Foi reprenaient l'initiative ?

Al-Buzzjani répondait poliment aux rares questions qui lui étaient posées, mais sans chercher à entretenir la conversation. Malgré tout, il se dérida un peu au fil du repas, fort goûteux au demeurant. Roger se félicita in petto que ce soit un jour de poisson, certain de ne pas offenser ses invités. Il fut malgré tout surpris de voir qu'ils ne regimbaient pas devant le vin, tout en faisant excellent accueil à tous les plats. Le ghūlam, impassible, avalait chaque met avec une régularité de métronome, recueillant la moindre miette déposée dans son assiette comme s'il n'était pas assuré de manger à sa faim à l'avenir.

Gourmand en diable, Roger savait se montrer disert quand on abordait la question de la nourriture et il détailla par le menu chacune des recettes. Satisfait de voir la discussion s'établir sur d'inoffensifs sujets, Bernard Vacher se contentait de hocher la tête avec enthousiasme. On fit même venir le chef de cuisine, un syrien nommé Basil qui remercia avec gêne, un peu décontenancé devant l'assemblée disparate qui louait ses talents. Il ne put s'éclipser sans dévoiler le secret de sa façon pour l'excellent poisson servi.

« C'est recette que je tiens de ma mère. Elle l'appelait le samak summâqiyya. On le prépare avec des baies de sumac, séchées et pilées. On fait tout d'abord frire les poissons, grossièrement taillés en dés dans de l'huile bien chaude, ornée de feuilles de coriandre moulues, à son goût. Puis on met de côté les pièces au chaud, allongeant l'huile pour y jeter pêle-mêle baies de coriandre et de sumac broyées, ail pilé, poivre noir fraîchement moulu. Ils accompagnent de l'oignon à poignée qui doit dorer doucement, tandis qu'on y ajoute de généreuses portions de tahîna[^29] et quelques giclées de citron pour délayer et rehausser par l'acide ces saveurs. Pour finir, on dispose le tout dans un plat, arrosant de noisettes grossièrement broyées et saisies à feu vif. »

Il quitta les convives sous les félicitations enthousiastes et des remerciements chaleureux, y compris d'al-Buzzjani.

Le voyage pour Jérusalem étant prévu le lendemain, les deux émissaires préférèrent se retirer assez tôt, menés par un valet jusqu'à leurs quartiers. Roger se retrouva autour de quelques oublies avec son invité, poussant les pions d'un jeu de jacquet. Alors qu'ils préparaient le plateau pour une nouvelle partie, Bernard sentit que le chanoine mourait de curiosité. Il avala un peu de vin lourdement épicé, rehaussé de zestes, avant de s'enquérir de la question qui taraudait le prélat.

« Je ne suis comme vous versé en toutes ces matières diplomatiques, indiqua Roger, mais ne convenez-vous pas que nous marchons sur la tête ? J'en viens parfois à me demander si les trompettes de saint Jean ne sont pas pour bientôt !

--- Seriez-vous chagriné de ce retour ?

--- Certes pas ! Mais lorsque le lion se fait agneau, je ne sais que penser. Voilà quelques mois, l'ost assaillait Damas, et celui-ci nous fait désormais assaut à son tour, mais d'amabilités.

--- Nous avons conclu un traité, fort avantageux pour nous, et l'atabeg veut marquer sa bonne volonté.

--- Ne cherche-t-il pas à nous endormir ? Nous soudoyer ? Nos devanciers prenaient les cités par force et rien ne résistait à leur Foi. Je me demande si nous avons hérité leur vaillance, à commercer ainsi avec ceux que nous devrions soumettre...

--- Je n'ai guère de réponse à vous apporter mon père. Mais je sais avec certeté qu'il est de plus dangereux adversaires au royaume, en leur cité du Nord. Et que tous ceux qui s'opposeront à eux peuvent être utiles à nos desseins. »

Le chevalier déchira un morceau d'oublie et le grignota lentement, le regard dans le vague, avant d'ajouter, tandis qu'il prenait le gobelet pour y regrouper les dés :

« Outre cela... »

Il se mordit la lèvre, sa voix mourut, puis il sourit et tendit le godet au clerc.

« À vous d'engager, mon ami »

### Baniyas, cloître canonial, midi du vendredi 13 janvier 1156

Les cuillers d'argent heurtaient les assiettes en céramique au rythme des bouchées des chanoines, résonnant dans la haute salle quasi vide. Emmitouflés dans d'épais vêtements, ils ressemblaient à des ours, lichant leur soupe dans de grands bruits de succion. Ils n'étaient que quatre, le doyen ayant depuis plusieurs mois déserté leur table, réfugié dans un chauffoir afin de préserver ses vieux os. Cela faisait des années qu'ils n'écoutaient plus de sainte lecture, le transport des précieux livres étant toujours problématique. Sans compter qu'ils n'avaient plus de novice pour cet office et qu'aucun d'eux n'avait envie de se priver d'un repas sur quatre pour faire entendre aux autres les textes sacrés.

De plus, au fil du temps, le silence réglementaire avait laissé la place à de timides échanges autour d'affaires courantes qui n'avaient pas à voir avec le chapitre. La fréquente absence de l'évêque les incitait à s'organiser au mieux en fonction de leurs besoins et impératifs sans trop se restreindre en raison de règles souvent inutiles et contraignantes, du moins selon eux.

Ce jour-là, l'ambiance était morne. Roger avait appris une mauvaise nouvelle : sa candidature n'était pas retenue pour le poste prestigieux de secrétaire de l'archevêque de Tyr. Ses collègues, Paul en tête, tentaient de le réconforter, sans grand succès jusque là.

« Pierre de Barcelone est fort avancé en âge, son successeur ne gardera peut-être pas un jeune clunisien peu au fait des choses d'ici.

--- Au Saint-Sépulcre, Pierre a été mon prieur, et sait combien j'aurais pu lui être utile. Il a pourtant estimé que je n'étais pas assez doué à la tâche. Crois-tu qu'un inconnu me préférera ?

--- M'est avis qu'il y a là collusion pour mettre en avant jeune poulain. Neveu de quelque prélat bien en cour.

--- Tu es homme de savoir et de sapience, ton mérite saura être reconnu.

--- On m'a coincé ici, comme vous, sans espoir d'en sortir. Notre sire évêque lui-même est plus souvent au-dehors qu'au-dedans. N'est-ce pas là signe de notre oubli ? »

Paul fit la moue. Il appréciait bien la place, à l'écart des principaux centres. Il y avait fait sa pelote, possédait plusieurs maisons en propre, et s'autorisait quelques libertés avec la règle de célibat, malgré des remontrances répétées. Aussi fréquentes que ses rechutes, à dire le vrai. On murmurait d'ailleurs qu'il entretenait plusieurs enfants, ne s'interdisant pas de retomber dans l'erreur avec certaines de ses anciennes amies à l'occasion d'une visite paternelle. Ses collègues ne le condamnaient que du bout des lèvres, étant en général dans une position au moins aussi délicate. Il n'était que Roger, malgré toutes ses complaintes, qui ne semblait guère profiter du relâchement des moeurs. Il aimait geindre à tout propos, dépité du faible avancement de sa carrière. Il ne manifestait néanmoins aucune animosité envers ses confrères indisciplinés, se contentant désormais de baigner dans un permanent abattement.

« Entre un trône vacillant et une Église plus prompte à promouvoir les fils à barons que ses fidèles serviteurs, je n'augure rien de bon pour les années qui viennent, mes frères. »

Il reposa violemment sa cuiller sur l'épais plateau de bois, faisant sursauter les présents. Surpris de sa propre virulence, il leur adressa un regard désolé, mais enchaîna, la bouche amère.

« Nous n'avons même pas de véritable basilique pour y célébrer l'office divin, depuis plus de dix ans que nous espérons. Verrons-nous la moindre pierre posée en ce sens ?

--- Le sire connétable Onfroy a bien indiqué qu'il parlerait en ce sens à la Cour le roi pour Noël.

--- Il n'a même pas de quoi augmenter ses murailles et solder ses troupes. Tellement aux abois qu'il compte mi-partir la cité avec les frères de Saint-Jean !

--- N'est-ce pas là bonne nouvelle ? Ils ont besants bien assez pour construire beaux édifices.

--- Parles-en au saint père Foucher, ils ont bien failli l'embrochier de leurs traits. »

Tout le monde savait qu'il n'en était rien, et que si l'opposition entre le patriarche et les frères hospitaliers de Saint-Jean avait dégénéré en conflit, jamais la personne sacrée n'avait été en danger. Mais aucun n'ignorait qu'il était inutile de chercher à pointer les énormités proférées par Roger lorsqu'il avait ses crises mélancoliques. Ils se contentèrent de plonger la tête dans leur assiette, se réfugiant dans leurs pensées. Après tout, ils ne le fréquentaient que peu. Chacun mangeait au chaud dans sa cellule soir et matin, parfois même dans une maison au dehors. Et le chapitre était chaque veillée rondement mené, le froid et l'humidité fouettant leurs ardeurs administratives.

### Baniyas, souk des cordonniers, soir du lundi 20 mai 1157

Éperdu, Roger ne savait que faire. Au matin, il était dans sa cellule à se ronger les sangs lorsqu'il avait entendu la rumeur : les musulmans venaient de percer les défenses de la ville et s'y déversaient comme autant de criquets et de serpents. Conscient que l'édifice cathédral serait un des premiers visés, il l'avait abandonné et s'était dépouillé de ses attributs les plus visibles. Il avait dissimulé quelques pièces d'orfèvrerie dans une besace à son flanc et y avait ajouté une miche de pain. Il n'avait pu obtenir aucune nouvelle des autres chanoines, certainement calfeutrés de leur mieux.

À présent, la cité était déserte, tous les habitants s'étaient enfermés, et il n'avait pu pénétrer que dans un marché dont il avait hâtivement barré le portail. Puis il s'était terré dans une alcôve abandonnée, parmi les déchets et les décombres. De temps à autre, un bruit de course, de cavalcade, des cris résonnaient dans les alentours, ajoutant l'angoisse à ses craintes.

Un moment, on avait éprouvé la solidité de la grande porte, à plusieurs reprises, puis le calme était revenu. Il n'avait vu personne se risquer hors des maisons de la zone et, même à l'approche de la nuit, aucune lueur n'osait s'annoncer aux fenêtres.

Il commençait à s'assoupir lorsque le grincement des gonds le fit sursauter. Des hommes porteurs de torches venaient de pénétrer dans la cour. Des Turcs, lourdement équipés, qui s'avançaient l'arme à la main. Il se recula dans les ténèbres, espérant passer inaperçu. En pure perte.

Des voix impérieuses hurlèrent devant sa cachette, qu'il fit mine de ne pas entendre, puis des cris agacés, rapidement augmentés d'un remue-ménage dans les décombres à l'entrée. Il tremblait de tous ses membres, incapable de faire le moindre geste. Il sentait que ses intestins allaient le lâcher. Puis une main accrocha son épaule et il fut extrait sans ménagement de son refuge.

Les coups s'abattirent tandis qu'on le traînait sauvagement au sol. Les bras devant le visage, il demeurait recroquevillé, hébété, implorant pitié. On l'obligea finalement à se mettre debout, lui arrachant une partie de ses vêtements. Puis une frappe dans le dos l'incita à avancer. La lueur ambrée des torches donnait des relents d'Enfer aux lieux qu'il traversait. Très vite, il rejoignit une petite place ornée d'une fontaine décorée de mosaïques. Mais il n'entendait plus son chant si rafraîchissant en été, pas plus que les ébats joyeux des gamins auxquels il lançait quelques billons.

Des chevaux, énervés par l'ambiance, hennissaient, s'agitaient dans une des rues adjacentes. Partout, des voix fortes rugissaient des ordres, et des suppliques leur répondaient. Des hurlements de frayeur ou de douleur s'échappaient parfois des alentours. Plusieurs groupes de prisonniers étaient assemblés. Roger eut des hauts le cœur à la vision de plusieurs têtes coupées enfichées sur des bâtons. Ses jambes défaillirent lorsqu'il aperçut les corps décapités, à demi nus, souillés, gisant dans le plus grand désordre, là où on le menait.

Tout s'enchaîna très vite. On le fit tomber sur ses genoux avec violence dans une file. Il entendait les lamentations, les suppliques, les cris de défi. Et les coups, répétés, distribués en ahanant par des hommes peinant comme bûcherons au labeur. Toujours plus près...

On le tira brutalement par les cheveux, lui tordant le cou en avant, et il s'abandonna alors, incapable d'autre chose que de murmurer le Credo, reniflant, pleurant, balbutiant. Une voix ferme résonna, des paroles furent échangées autour de lui tandis qu'on le lâchait. Une main puissante lui leva la tête. Il n'osa pas regarder.

« Avez-vous désir de vivre encore un peu ? » lui demanda-t-on dans sa langue.

Il entrouvrit une paupière inquiète, frotta son nez dont la morve coulait sur ses lèvres, hésitant à parler. Devant lui, un soldat magnifiquement équipé, vrillait son regard d'aigle sur lui. Son armure de lamelles laquées était recouverte d'un qaba[^30] aux revers brodés, de somptueux tiraz[^31] ornant les bras. À ses hanches pendaient sabre, carquois, arc courbe, et sur sa tête protégée d'un casque peint, surmonté d'un plumet, une cagoule de maille ne dévoilait que les yeux.

« Je vous connais, vous êtes un de leurs prêtres. »

L'homme, visiblement officier, lança quelques ordres et on lui amena peu après à la pointe de l'épée quelques autochtones. Bien qu'ils ne semblaient pas prisonniers, ils étaient effrayés et répondaient avec appréhension à ses questions, les mains jointes en une attitude soumise et craintive. Puis il les chassa d'un geste et revint vers Roger, qui attendait immobile, prostré.

Le soldat ôta son casque, sa coiffe, dévoilant un visage connu. Le regard s'était durci et des rides autoritaires encadraient désormais la bouche.

« Les gens d'ici vous appellent *al-tayyib*[^32], vous saviez ?

--- ...

--- Vous parlez notre langue ? »

Il arbora un sourire froid, croisa les mains dans le dos, soupira.

« Notre émir, qu'Allah lui prête longue vie, souhaite nettoyer cette enclave de tous les impurs. Rembourser le sang versé par les vôtres... »

Roger passa une langue de carton sur ses lèvres râpeuses, incapable d'avaler sa salive tant sa gorge le nouait. Le Turc se pencha vers lui, jusqu'à lui souffler sur le visage.

« D'un autre côté, votre chef s'est réfugié en la citadelle. Lui envoyer émissaire de confiance pourrait l'inciter à ne pas résister stupidement. Voudriez-vous être celui-là ? Le convaincre de l'inutilité d'un combat perdu d'avance ? »

Roger hésita un moment, incapable de comprendre. Puis il hocha la tête, lentement, comme dans un rêve, tel un animal fasciné par son prédateur.

Al-Buzzjani inspira profondément tandis qu'on éloignait le prisonnier de lui. Il ordonna qu'on le lave avant de le présenter à l'émir : il s'était souillé et empestait à plusieurs mètres à la ronde. Il fronça les sourcils, s'efforçant de ne pas penser au bon accueil qu'il avait reçu lorsqu'il avait rencontré le chanoine, des années auparavant. Au moins lui avait-il évité la décapitation immédiate. Il haussa les épaules, tentant de se libérer du poids qu'il sentait s'accumuler au fil des ans. Il était né au monde en tant que soldat, et s'accomplissait dans le service. Obéissant à son protecteur sans discuter, il tenait son rang avec une loyauté indéfectible. C'était un Ghūlam[^33]. ❧

### Notes

La ville de Baniyas fut au centre des affrontements entre le royaume de Jérusalem et les princes musulmans tout au long de la décennie 1150. La cité était idéalement positionnée entre les Latins et Damas et tirait parti des péages vers et depuis la côte. Celui qui la détenait pouvait aisément y amasser des unités en vue d'une attaque contre son adversaire. Nūr ad-Dīn ne s'y était pas trompé et saisit la moindre occasion de s'en emparer. Lorsque l'émir cherchait à conquérir Damas, l'atabeg Anur s'était arrangé pour en laisser le contrôle aux Francs, espérant ainsi une aide rapide en cas d'appel de sa part.

Une fois les territoires de Syrie unifiés, l'émir s'employa à garantir ses positions sur de tels accès et mit plusieurs fois le siège devant la cité, la rasant de son mieux quand il ne put la garder. Plusieurs décennies après, l'édification de la forteresse de Vadum Jacob par Baudoin répondait aux mêmes impératifs : avoir une base puissante aux marches du territoire, à la fois pour se protéger des chevauchées, mais également pour facilement s'avancer en terre adverse. Saladin comprit aussi bien que Nūr ad-Dīn le danger et mit toute sa force en oeuvre pour empêcher que cela ne se fasse.

Le passage décrivant le plat est une modeste allusion à la pratique gourmande de Jean-François Parrot dans *Les enquêtes de Nicolas le Floch*. Il y narre une autre époque (le XVIIIe siècle) avec un talent scripturaire et une attention aux détails historiques (et culinaires) fort inspirants.

### Références

Ellenblum Ronnie, « Who Built Qalʿat al-Ṣubayba? », dans *Dumbarton Oaks Papers*, vol. 43 (1989), p. 103-112.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Graboïs Aryeh, « La cité de Baniyas et le château de Subeibeh pendant les croisades », dans *Cahiers de civilisation médiévale*, 13e année, n°49, Janvier-Mars 1970, p.43-62.

Waines David, Sabard Marie-Hélène (trad.), *La cuisine des califes*, Coll. l'Orient Gourmand, Sindbad, Arles : Actes Sud, 1998.

Liaisons dangereuses
--------------------

### Palais royal, salle basse, après-midi du samedi 2 février 1157

Noyé de lumière, le vaste lieu voûté présentait un aspect riant malgré le froid extérieur. Se déversant par les croisées de bonne taille, un vigoureux soleil d'hiver rebondissait sur les murs étincelants de blancheur. Avec le banquet offert par Baudoin à de nombreux membres de la Haute Cour, une large assemblée de pauvres, pèlerins et nécessiteux avaient été accueillis dans une salle basse pour s'y voir proposé les reliefs du repas royal.

Le vicomte ayant l'expérience de ces groupes où tire-goussets et crocheteurs savaient se fondre dans la masse, plusieurs sergents vérifiaient qu'aucun larron ne profitait de l'occasion pour opérer larcin. Eudes et Droart étaient postés à l'entrée de la salle de l'Échiquier, adjacente, où se déroulaient habituellement les activités comptables de l'administration de Jérusalem. Tranquillement adossés au mur, les bras croisés, ils discutaient nonchalamment. L'hiver avait été calme, une trêve courant avec le farouche Nūr ad-Dīn. Les frimas et les inévitables fluctuations du prix du pain en période de jointure constituaient les principaux sujets de conversation. Avec l'arrivée prochaine du carême, les gens auraient prétexte à faire maigre.

Deux valets redescendant les tranchoirs imbibés de sauce poivrée s'approchèrent des sergents, l'air hilare. Ils étaient de leurs compagnons de taverne, aimant à faire rouler les dés ou à jacasser des rumeurs animant le palais de temps à autre. Ils semblaient fort s'amuser et se délectaient par avance de ce qu'ils allaient apprendre à leurs amis.

« Un nouveau jongleur aurait-il manqué de respect au sire prince de Galilée ? ricana par avance Droart.

--- Ben pu drôle que ça, compain. De peu qu'le sire Courtenay rende son âme, occis par une tourte mal goboyée !

--- Si fait, je crois que le jeune comte frère le roi[^34] a assailli mauvaise forteresse, cette fois.

--- Que nous contez-vous là ? » s'intéressa Eudes, hésitant à lâcher du regard un des festoyeurs amateur de gamelles autres que la sienne.

Les deux valets posèrent leurs corbeilles et se rapprochèrent, la mine réjouie et le ton conspirateur.

« Le jeune comte aime ben noyer l'épinette un peu partout, mais y l'a trempée en un lieu promis à un autre.

--- La jeune princesse d'Édesse, veuve au frais minois, a préféré fils à roi que sire d'Ibelin. On aurait surpris la charmante issir de la couche comtale.

--- Et la nouvelle est arrivée aux oreilles du sire Joscelin croquant son repas. J'ai ben cru qu'le vin allait lui jaillir hors le groin tant y s'étouffait. »

La remarque fit jaillir quelques rires enthousiastes. Si chacun reconnaissait la valeur du jeune Courtenay comme homme de guerre, personne n'en ignorait pour autant le naturel féroce et les exigences strictes. La perte du domaine familial ne lui laissait que sa réputation et sa vaillance pour faire son chemin. Ses espoirs de retrouver une principauté, même tenue en fief, motivaient chacune de ses actions et son impétuosité convenait à merveille à son poste militaire.

Il avait fait venir sa sœur dans l'espoir d'un mariage avec l'héritier Ibelin, famille en faveur depuis le roi Foulques. Mais si elle se déshonorait avec Amaury, ses ambitions seraient définitivement ruinées. Il avait brûlé tous ses vaisseaux et ne voyait pas d'autre échappatoire que de faire carrière, et faire souche, dans l'orbite du pouvoir royal. Sa superbe en était durement éprouvée, et sa méchante humeur en sortait renforcée.

« Y f'ra pas bon œuvrer aux écuries les jours à v'nir.

--- J'encrois qu'les exercices au conroi vont faire pleuvoir horions et bosses !

--- D'un autre côté, comment en vouloir au jeune sire Amaury ? Lui qui n'a besoin que d'une œillade amicale ou d'une croupe audacieuse pour succomber. »

Droart ajouta à sa remarque un geste de la main, dessinant les courbes audacieuses qu'il prêtait à la jeune femme. À son arrivée, chacun avait pu admirer la joliesse de la princesse d'Édesse, habilement mise en scène par des atours et des apprêts attirant sur sa tête les imprécations cléricales autant que les regards appréciateurs. Les serviteurs goûtaient sa splendeur, dont ils savaient qu'il n'en seraient que spectateurs, mais la noblesse bruissait de cette beauté en liberté, dont le caractère n'avait que peu à envier à celui de son frère.

Eudes ajouta finalement :

« On ne peut blâmer la princesse. Pour naviguer entre les coteries et tracer son sillon, elle n'a que son frais minois et un nom envié.

--- Comme l'araignée, de son cul elle tire sa toile ! » opina Droart, hilare.

Des éclats de rire fusèrent derechef puis les valets se décidèrent à reprendre leurs corbeilles. Au moment de partir, l'un d'eux lâcha :

« De certes, j'donnerai ben trente deniers pour assavoir c'que l'sire Hugues[^35] dira quand y se saura deux fois floué. Cornard avant espousaille et promis à offrir sienne sœur à çui qu'a orné ainsi son front. »

### Jérusalem, palais royal, chambre du comte de Jaffa, matin du jeudi 20 juin 1157

Les pas du chevalier résonnaient sur les sols dallés de pierre, de mosaïque ou de carreaux vernissés, ses éperons rythmant sa marche précipitée autant que le cliquetis de ses armes. Il tenait son casque sous le bras et n'avait que grossièrement délacé sa coiffe de mailles. Arrivant à la porte de sa destination, il fit un signe au valet qui l'avait précédé. Celui-ci entra pour l'annoncer puis ouvrit peu après largement l'huis.

Régnier n'avait jamais été reçu par le jeune comte de Jaffa, le frère du roi. Il découvrait donc la somptueuse chambre où Amaury accueillait ses familiers et amis lorsque, fréquemment, il était dans la Cité. En l'absence du souverain, il avait la charge des affaires courantes au sein du palais, mais la tâche était suffisamment tranquille pour qu'il prenne ses aises.

Les parois de la vaste pièce s'ornaient de complexes décors céramiques, abritant plusieurs niches aux profils découpés dans un style arabisant. Le sol, de marbre clair, accueillait par endroit d'épais tapis aux motifs bariolés. Un lit gigantesque, à l'occidentale, était placé au centre, nimbé de voilages et de tentures, noyé de coussins, édredons et couettes de brocard. Des coffres peints de scènes guerrières s'alignaient sur le mur opposé aux larges fenêtres de verre coloré, en partie obturées de volets ajourés. Devant la couche, un haut siège, mélange de divan à l'orientale et d'un trône européen était recouvert de somptueuses étoffes négligemment disposées. Il était flanqué d'une table où s'amassaient de branlantes piles de livres, rouleaux, tablettes, dans un désordre précaire.

Amaury était debout, face au monticule et se tenait de trois quarts par rapport à Régnier. De belle taille, sa silhouette un peu épaisse était affinée par un bliaud de soie aux motifs syriens dorés. Il dardait un regard clair, attentif sur l'arrivant, se mordant les lèvres. Il était réputé pour ne pas être homme à la parole facile et son haut front se barrait souvent de rides quand il cherchait comment s'exprimer. La chevelure blonde rappelait son frère, et le nez quasi aquilin le profil de son père. Mais il portait la barbe, courte et bien taillée, ainsi que les Templiers dont il partageait le goût pour la chasse et le dédain des jeux.

Régnier aperçut un petit christ en croix dans le capharnaüm de la table, juste là où se tenait le comte de Jaffa en fait. Il fléchit un genou et patienta, n'osant prendre la parole sans y avoir été invité, et se contentait d'attendre, retrouvant son souffle après sa folle chevauchée et sa course dans le palais.

La voix hésitante d'Amaury finit par rompre le silence, annoncée par un imperceptible bégaiement dont il avait mis des années à se défaire.

« On vous annonce porteur de mauvaises nouvelles. Quelles sont-elles ?

--- Mauvaises, mais pas dramatiques, sire. Nous avons subi male défaite, mais le roi est sauf, réfugié en l'éperon de Saphet. »

Le comte s'assit doucement tout en invitant d'un geste Régnier à se relever.

« Qu'est-il arrivé au juste ?

--- Nous repartions de Panéas[^36] quand nous avons été pris à partie par fort contingent de troupes. Damascènes, turcs, aleppins, turcomans, on aurait dit des sauterelles. Le soleil était voilé de leurs traits. »

Le regard d'Amaury erra sur la tenue de Régnier, s'attarda sur la maille rompue par endroit, la poussière. Son expression demeurait impénétrable.

« Avons-nous perdu hommes de valeur ?

--- Je le crains fort. Nous n'avons été qu'une poignée à rejoindre le refuge du castel, pas même une échelle[^37]. Seuls ceux qui percèrent la ligne ennemie pour y glisser le roi en ont réchappé.

--- Est-ce à dire que le royaume n'a plus d'ost ?

--- Valetaille et piétons demeuraient presque tous à Panéas, pour aider à remettre en état. Seule la cavalerie a été défaite. Mais il s'y trouvait tous les grands présents, peu ou prou. »

Amaury réfléchit un petit moment, les yeux rivés sur le chevalier. Puis il se redressa soudain. Il faisait penser à son frère quand il adoptait ainsi une tenue plus hiératique, masquant son léger embonpoint.

« Nous allons constituer petite escorte rapidement. Je vous veux parmi eux, que vous les meniez au roi. Voyez avec les frères de la Milice[^38] s'ils ne peuvent divertir quelques hommes. Une partie du ban d'Ascalon est ici avec moi, je peux me garantir sans eux, et ferai appeler d'Hébron en remplacement. »

Voyant que Régnier levait la main pour demander la parole, il la lui concéda d'un geste.

« Le sire roi avait l'intention de rejoindre Acre à franc étrier dès les troupes musulmanes éloignées.

--- Vous l'y joindrez donc. Passez par Naplouse, La Fève et Séphorie. Je vous donnerai mandements pour y récolter des hommes en chemin. La première chose à faire est de garantir le roi et le mener sauf ici. Je vais informer le sire comte Raymond et le prince Renaud, qu'ils soient prêts à nous appuyer si besoin était. »

Il se leva et s'approcha de Régnier, parlant de façon moins formelle.

« Allez donc prendre repos le temps pour la troupe de se préparer. Je veux son départ dès avant none. »

Puis, d'un geste amical de la main, il invita Régnier à sortir. Lorsque la porte se referma, il tira l'épaisse toile qui s'y ajoutait et fit quelques pas, jusqu'à une des fenêtres, jetant un regard vers la cour en contrebas. Des ombres allaient et venaient parmi les orangers et les massifs de fleurs multicolores.

Il hésitait sur ce qu'il devait faire. Il avait longuement prié que le Seigneur daigne lui accorder la main d'Agnès. Ce qui signifiait empêcher son union avec Hugues d'Ibelin, son vassal. Leur idylle commençait à inquiéter au plus haut niveau, sans compter les moqueries colportées par le peuple. Sa propre mère Mélisende l'avait durement tancé, lui reprochant de s'aliéner les Ibelin, une puissante famille, alliée et soutien sans faille de son défunt père.

Fallait-il qu'il obtienne satisfaction à un tel prix ? Hugues d'Ibelin était de ceux qui avaient pris part à la chevauchée pour délivrer Panéas. Était-il sauf ? Était-il libre ? Amaury n'avait pas osé poser la question qui lui brûlait les lèvres. Il hésitait également à prévenir celle qu'il chérissait, inquiet d'éveiller en elle des espoirs qu'il ne saurait combler.

Il se mordillait l'intérieur des joues, les yeux dans le vague quand on frappa à la porte. Un jeune clerc au regard myope se présenta, son écritoire sous le bras, une sacoche de notaire au côté, un de ces anonymes discrets dont il n'arrivait jamais à se souvenir du nom. Derrière lui, une cohorte de familiers s'entassaient, dans l'attente des nouvelles.

« Nous allons nous rendre en la Grande salle. J'y ferai annonce et dicterai plusieurs libelles et annonces. Nous lancerons force coursiers avant sixte, au moins une douzaine. Faites-le assavoir au plus tôt. »

Comme lors des chasses, il sentait son courage s'affermir dans l'action. Pour l'heure, le royaume avait besoin de lui, et il comptait bien ne pas manquer à son frère. Ses affaires de cœur sauraient attendre jusqu'à la veillée.

### Jérusalem, église du Saint-Sépulcre, matin du vendredi 16 août 1157

Herbelot marchait à petits pas, le visage recueilli comme il seyait à un clerc respectueux déambulant dans le Saint des Saints. Il suivait sans mot dire la haute silhouette de l'évêque Frédéric de la Roche au travers des détours et recoins du Saint-Sépulcre. Avançant rapidement, ils débouchèrent au niveau de la tribune de la rotonde, d'où la vue sur le tombeau du Christ embrasait toujours la Foi du jeune homme. Mais l'ambiance n'était guère aux oraisons et aux transports religieux. Frédéric de la Roche était plus porté sur les discussions de Cour et la politique que sur l'exégèse évangélique. On le disait d'ailleurs plus souvent coiffé du heaume que de la mitre.

Herbelot lui reconnaissait un courage physique impressionnant, mais désapprouvait ce mélange des genres. En silence, bien sûr, car il n'était pas dans ses façons de s'épancher ainsi à propos d'un supérieur, fut-il farci de tous les vices. Ce qui n'était pas le cas de Frédéric, qui lui imposait par sa vivacité d'esprit, si ce n'était par sa culture. Sans pour autant dire qu'il appréciait de devoir le servir parfois, sur les instructions de son vrai maître, l'archevêque Pierre de Tyr, il ne s'en effrayait plus. D'autant que le grand prélat lui manifestait des égards marqués. C'était en partie parce qu'il était soucieux de ne jamais abaisser le prestige d'un clerc devant les gens du commun. Il conservait néanmoins cette attitude pleine de respect, bien que souvent assez directe, dans l'intimité.

Un des serviteurs des chanoines fit tourner sa clef dans la serrure d'une petite porte et ils continuèrent leur périple dans une galerie bordant une terrasse, surplombant des jardins. Ils se trouvaient désormais dans le palais royal. Le soleil matinal commençait à en chauffer durement les pierres et, rebondissant sur les murailles, obligea les deux hommes à plisser les yeux. Ils croisèrent plusieurs domestiques affairés, qui saluèrent sans s'attarder. Ils étaient habitués à côtoyer des têtes couronnées et tonsurées de la plus grande importance et ne s'éternisaient pas en politesses.

Un large escalier déroulant ses degrés lentement les mena jusqu'au jardin, où chantait une fontaine au milieu de massifs arborés. Un vieux valet au dos plus voûté qu'une galerie de cloître binait tranquillement un parterre de roses ombragé, tandis que des gamins d'une dizaine d'années s'activaient à remplir un tonneau depuis le point d'eau à l'aide de seaux aussi lourds qu'eux.

Sur un banc à l'abri d'un petit kiosque léger en vannerie, le jeune comte de Jaffa, Amaury, était assis, un stylet à la main. L'air concentré, il notait parfois sur une tablette ce qu'un lecteur lui récitait d'un épais volume posé sur un lutrin. Les deux hommes s'interrompirent en voyant arriver le couple de clercs. L'évêque salua poliment.

« Sire comte de Jaffa, la paix du Christ soit avec vous

--- Sire évêque Frédéric, le bon jour à vous. Que vous voilà fort à point, Isaac me lisait un texte griffon que vous goûteriez fort. »

Il se tourna vers le lecteur, qui s'inclina respectueusement sans mot dire, puis continua, d'un ton badin.

« Il s'agit de la *Présentation et la composition de l'art de la guerre selon le sire Nikephore*[^39]. Empli d'instructives idées sur leur usage de la cavalerie et de la piétaille. Il faudrait le faire traduire en latin pour l'édification de nos gens.

--- Je ne serais que trop heureux de mettre mon scriptorium et quelque savant lettré de grec à votre service, sire comte. »

L'évêque marqua une pause puis se rapprocha avant d'ajouter d'une voix feutrée.

« J'aurais espoir, néanmoins de prendre entretien avec vous d'une affaire qui vous touche de fort près. Vous assavez combien je vous tiens en affection et estime. C'est donc à ce titre que je me permets de venir, en tant qu'ami.

--- Amitié dont je n'ai eu qu'à me louer, sire évêque. Vous me voyez bien aise vous compter parmi mes intimes. »

Il fit un signe de la main au traducteur pour qu'il s'éloigne et invita le prélat à s'asseoir à ses côtés. Il ne fit guère de cas d'Herbelot, sachant que les seigneurs de l'Église ne se déplaçaient jamais sans quelque séide tonsuré, dont l'usage et l'utilité lui paraissaient toujours obscurs. Il avait appris à ne pas s'en préoccuper, voire à les ignorer.

« Vous assavez que la reine votre mère m'honore d'accepter mes avis en sa Cour. Outre, même si je n'ai plus l'heur de servir directement la couronne, mes pensées, et mon cœur ne s'en éloignent guère.

--- Chacun sait dans ma famille combien votre présence est précieuse à la couronne, moi le premier.

--- C'est également à ce titre que j'ai l'audace de venir vous parler des rumeurs qui courent au parmi du royaume, et qui inquiètent fort votre mère. »

Amaury soupira et redressa le buste, lançant un regard aussi méfiant que s'il avait soudain affaire à un serpent. Mais l'évêque était opiniâtre et tenait à aller au bout de sa pensée.

« J'ai souvenance que l'union de votre famille à celle des Ibelin était déjà un projet de votre père. Ce serait là fort habile et fructueuse union. Ils se sont montrés indéfectibles dans leur service.

--- Ils servent déjà ma bannière. Qu'espérer de plus ? Il est de plus illustres lignées dont nous pourrions nous honorer.

--- Il en est de certaines familles dont l'étoile a fort brillé, mais dont l'éclat est désormais terni.

--- Le mariage n'est donc t-il là que pour fournir sergents et lances ? Péages et octrois ? s'emporta Amaury.

--- Ai-je jamais dit cela ?, s'effraya Frédéric. Je me demande seulement si vous n'avez pas cédé un peu vite à votre sang, fort jeune et bien amiable. Il faut raison porter à ces importantes matières. Ne pas trop vite céder aux naturelles bienveillances de votre cœur. »

Le jeune prince se renfrognait de plus en plus, bien qu'il agita le menton en timide assentiment. Réalisant qu'il risquait d'arriver à l'inverse de ce qu'il escomptait, l'évêque se releva, un sourire matois sur le visage.

« Ne prenez pas en mâle part ce que je vous ai confié, c'est là inquiétude d'un féal serviteur, et d'un ami. Pourpensez à tout cela et voyez de qui vous viennent les avis, je n'en demande guère plus. »

Tout en parlant, Frédéric se leva, lissant ses habits comme s'il craignait d'en avoir dérangé la superbe. Amaury hocha la tête, visiblement toujours agacé. Il lâcha un salut poli, mais froid et reprit une de ses tablettes. L'entretien était fini.

Alors qu'ils s'éloignaient, Herbelot entendit maugréer l'évêque entre ses dents.

« La peste soit de cette Courtenay. Est-elle sorcière pour le prendre ainsi dans ses rets ! »

Puis il se tourna vers Herbelot, trop énervé pour s'empêcher de le prendre à témoin.

« Le saint père Foucher ne va jamais tolérer cela. J'espérais lui éviter pareil souci. »

Puis, reprenant sa route avec vigueur, il ajouta une dernière fois mezzo-vocce :

« Que n'ont-ils péri avec leur comté, ces maudits ! » ❧

### Notes

L'union du jeune prince Amaury, alors simple comte de Jaffa (mais futur roi) avec Agnès de Courtenay fut à l'origine d'un véritable drame. En dehors du fait qu'il aurait volé la fiancée (certains dirent même l'épouse) d'un de ses vassaux, seigneur d'Ibelin et chef d'une famille en pleine ascension, une forte hostilité d'une grande partie de la Cour et des clercs aboutit à son divorce pour qu'il accède à la couronne. Il se remaria à une princesse byzantine, mais on légitima ses précédents enfants. Il s'agit de Baudoin, le roi lépreux tant célébré et de Sybille, qui fit roi Guy de Lusignan, qu'on accable de la perte du royaume à Hattîn en 1187.

Les raisons de cette animosité demeurent mystérieuses, et la mauvaise réputation d'Agnès de Courtenay tient en grande partie au portrait qu'en dresse Guillaume de Tyr (et son continuateur Ernoul, qu'on pense être de la domesticité des Ibelin). Principal historien pour notre connaissance de cette époque, il fait souvent preuve de partialité et j'offre l'hypothèse qu'il se comporte ainsi en raison de ses liens avec Frédéric de la Roche, qui le fit archidiacre lorsqu'il revint en Terre sainte. Ce prélat faisait partie des nouveaux venus en ce milieu de siècle, promu sous l'administration du roi Foulques, comme le patriarche Foucher d'Angoulême par exemple. Je lui prête donc une certaine inimitié à l'égard des anciennes familles, de souche plutôt normande, dont faisaient partie les Courtenay, comtes d'Édesse.

Il s'agit d'une simple hypothèse de ma part sur un événement difficilement compréhensible, à la portée politique complexe. Le royaume de Jérusalem était régulièrement déchiré de conflits internes qui le menèrent plus d'une fois au bord du précipice, jusqu'à sa destruction en 1187. Les dissensions entre les coteries de familles anciennement implantées et les vagues d'arrivants avides d'une ascension sociale inespérée en Europe ont, me semble-t-il, structuré le fonctionnement des états latins tout au long du XIIe siècle. Selon moi, la chute d'une famille aussi riche et puissante que les Courtenay ne pouvait demeurer sans implications dans le jeu politique.

### Références

Amouroux-Mourad Monique, *Le comté d'Édesse 1098-1150*, Paris : Librairie orientaliste Geuthner, 1988.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. III.*, Cambridge : Cambridge University Press, 2007.

Fléau ballant
-------------

### Abords de Souq Wadi Barada, midi du youm al khemis 11 dhu al-qi'dah 548[^40] {#abords-de-souq-wadi-barada-midi-du-youm-al-khemis-11-dhu-al-qidah-54840}

La file d'ânes s'étirait dans la poussière grise et le vacarme des pierres roulant sous les sabots. Tête basse, courbée sous leur fardeau, indifférents aux murailles de roches qui les enserraient, les animaux avançaient le long de l'ancienne voie taillée dans les falaises. Malgré le soleil au zénith, leur respiration s'embuait à chaque pas. Autour d'eux, les caravaniers allaient et venaient emmitouflés dans des hardes sans couleur.

Seul homme à avoir une monture, Idris somnolait sur sa mule. Il était engoncé dans un amas de tissus fatigués qui ne laissaient qu'à peine émerger son nez et ses yeux. Il chantonnait à mi-voix une comptine qu'il destinait à ses neveux et nièces une fois rentré chez lui, à Damas. Il appréciait fort son statut de célibataire, mais prenait grand plaisir au contact des enfants. Jamais il n'était revenu de ses voyages sans un cadeau sous une forme ou une autre pour les garnements qu'il aimait gâter.

Il remarqua que le chef caravanier, reconnaissable à son immense fouet roulé à la taille s'approchait de lui. Malgré le froid, il n'était chaussé que de savates essoufflées.

« Abu Hasan ! Un problème ? s'enquit le jeune négociant.

--- Des hommes installés dans les tombes, à deux ou trois portées d'arc.

--- Des brigands crains-tu ? s'inquiéta soudain Idris.

--- Brigands pas s'annoncer comme ça. »

Idris se redressa en selle et tordit la tête pour voir devant lui. Un petit feu, une toile hâtivement tendue entre quelques piquets. Et même...

« Ils ont des chevaux ! Ce ne peuvent être des pillards. Par contre ce sont des Turkmènes, à n'en pas douter. »

Sa main chercha instinctivement sa hanche, où il attachait généralement une épée. Plus pour se rassurer que pour en faire usage. Il n'avait jamais eu à croiser le fer avec quiconque et préférait toujours s'en remettre à ses talents de négociateur. S'il y avait parfois perdu de l'argent, il se vantait assez souvent d'y avoir conservé la vie. Le visage impassible, Abu Hasan lui demanda s'il fallait faire halte avant d'arriver à leur hauteur.

« Venir sus à nous avec ces chevaux leur serait aisé. Ils nous savent ici. *Mesure la profondeur de l'eau avant d'y plonger*. Allons donc voir si notre modeste train attirera leur intérêt. »

Idris soupira, se frotta le nez et tenta de se sortir de son amas de tissus. L'émir 'Ata, seigneur de Ba'albek était un homme respecté et craint dans la région, et il était membre de l'administration de Damas. La présence de détrousseurs était peu envisageable, sauf si le prince du Nord, l'Aleppin Nūr ad-Dīn, déployait ses forces. Il y avait toujours des auxiliaires, des soudards chargés de fourrager, qui ne s'encombraient guère de scrupules. Il remercia Allah de n'avoir avec lui aucun produit que les pilleurs appréciaient, et de ne jamais partir sans quelques bourses de vieux dirhems brunis pour les bakchichs.

Lorsqu'ils furent proches de la troupe, Idris s'avança en tête. Les soldats, car c'en était bien, ne portaient aucun signe qui eut permis de les distinguer. Nulle bannière, pas de couleurs. De simples nomades qui s'étaient loués comme auxiliaires de guerre le temps d'une saison. Donc corruptibles estima Idris.

L'un d'eux, le visage aussi ridé qu'une vieille pomme, la mâchoire ornée d'une barbe tressée et agrémentée de bagues d'argent, vint se mettre en travers du chemin. Le menton levé, il avait calé ses mains dans l'étoffe de sa ceinture. Derrière lui, les hommes s'étaient déployés en demi-cercle. Aucun n'avait d'arme au poing, mais sabres et arcs pendaient à leurs hanches. Le Turkmène attendit d'être à portée de voix et lança, dans un sourire :

« Salaam aleikum !

--- Aleikum salaam, répondit Idris sans même y penser. »

L'homme fronça encore plus les sourcils et s'avança tranquillement. Son visage déformé par un rictus semblait hésiter entre le dédain et le dégoût. Mais son attitude demeurait nonchalante.

« Où allez-vous ainsi chargés ?

--- Dimashq. Ce sont là denrées provenant de Ba'albak. »

Le Turkmène hocha la tête lentement.

« Quel genre ? Tissu ? Épices ?

--- Rien de bien luxueux, simplement fèves, froment, lentilles... »

Idris nota que l'homme se raidit brutalement et lui adressa une moue de désapprobation silencieuse.

« Je vous assure que je ne transporte rien de plus. Juste de quoi manger.

--- Je te crois, je te crois. C'est juste que l'émir ne peut accepter les spéculateurs. Il doit veiller au grain... »

Tandis que l'homme souriait avec emphase et sans joie à sa répartie, Idris cherchait à comprendre. Il avait entendu parler des manœuvres de Nūr ad-Dīn pour gêner l'approvisionnement de la ville, tout en incriminant l'accord avec les Francs comme cause de la disette. Une sueur glacée lui parcourut l'échine et sa voix se fit moins assurée lorsqu'il répondit :

« Je ne suis qu'honnête négociant. J'achète et revends à prix raisonnable.

--- Voilà bel ensemble d'ânes ! Ils portent du froment ceux-là ? »

Idris acquiesça sans rien ajouter. Le Turkmène compta silencieusement et revint à lui.

« Si je vois bien, chaque bête porte un irdabb[^41], ce qui rapporte dit-on dans les six à sept dirhems de bon argent dans les suq dimashqi. Ne me dis pas que tu l'as payé en rapport!

--- Je ne compte pas le revendre à pareil prix ! s'indigna Idris.

--- Et pourquoi tu te gênerais ? »

L'homme avait adopté un ton de voix agacé, presque agressif, qui réduisit Idris au silence. Le Turkmène inspira longuement, secoua la tête comme s'il s'apprêtait à morigéner un enfant puis reprit la parole.

« L'émir nous a dépêchés pour empêcher que des accapareurs profitent de la misère des pauvres gens.

--- Ne peut-on s'arranger ? Ma famille, mes amis, comptent sur ces victuailles. »

Idris fit mine de s'emparer de la besace de toile qu'il portait en bandoulière. Il savait y trouver les bourses de vieux dirhems.

« Crois-tu que je fais cela pour t'extorquer de la menue monnaie ?

--- *Le meilleur des dons est celui qui se fait sans demande.* J'espérais juste te proposer un négoce, de ceux que je pratique souventes fois. Moi je veux passer, et toi tu possèdes l'accès à cette route, là, derrière toi. Trouvons un prix raisonnable et quittons-nous bons amis. »

Le Turkmène se passa la main sur le menton plusieurs fois, se tourna vers ses hommes. Il semblait indécis, ce qui mit un peu de baume au cœur d'Idris. Lorsqu'il s'approcha de nouveau, Idris put sentir son haleine chargée.

« Je serais heureux de te vendre ça, mais il se trouve que l'émir pense que ce chemin est sien. Et lui m'offre de belles montures, des pointes de fer d'excellente qualité pour nos flèches, des femmes et tout ce que mon poing peut prendre à l'ennemi. Qu'as-tu de plus à m'offrir ? »

Il haussa les épaules et ricana doucement. Puis agita la main :

« Allez, marchand, rebrousse chemin avant que je ne ne décide de te mettre à l'amende pour avoir tenté de voler l'émir et les pauvres.

--- Rebrousser chemin ? Mais je suis attendu à Dismashq !

--- Alors, passe. Mais tu devras abandonner tes ânes ! »

### Village de Taranjeh[^42], demeure du ra'is, soirée du youm al joumouia 16 muharram 549[^43] {#village-de-taranjehtaranjeh-demeure-du-rais-soirée-du-youm-al-joumouia-16-muharram-549}

La maison du chef du village respirait l'opulence. Solidement bâtie en pierre autour d'une cour arborée, elle s'étendait en plusieurs ailes, comprenant des zones artisanales et de vastes entrepôts de stockage. Le majlis[^44] s'ouvrait largement sur un petit jardin potager par des baies ajourées, surmontées d'un auvent de feuilles de palme. Profitant de la chaleur revenue pour un jour après un hiver assez froid, le ra'is y avait installé des banquettes pour y travailler. Lorsqu'Idris s'était présenté, il l'avait accueilli aimablement et lui avait fait servir des rafraîchissements. Puis la discussion s'était lancée sur des considérations générales, comme souvent. Le négociant s'étonnait fort de la richesse apparente du lieu, ayant toujours cru que les Ifranjs écrasaient leurs sujets musulmans de taxes et de corvées diverses.

« En un sens c'est vrai, nous payons des redevances que les polythéistes ne versent pas. Mais selon le maître, nous pouvons jouir d'une relative liberté. Au final, tant que les impôts sont payés, personne ne vient nous malmener.

--- Vous ne craignez pas les troupes de guerre, si près de la marche ?

--- Jusque là celle-ci nous aurait plutôt servi. Aucun ici ne porte l'anneau de fer au poignet, car il nous serait rapide de fuir vers les terres amies. Et l'aisance qui nous est accordée ne l'est que pour nous inciter à nous défendre par nous-mêmes contre les pillards nomades au lieu de pactiser avec eux.

--- À Dimashq, la présence des émirs ifranjs venus percevoir le tribu est toujours mal passée. Je ne sais comment vous faites.

--- Peut-être sommes-nous moins fiers que les Dimashqi, rétorqua le ra'is, tout sourire. Nous n'avons que fort peu à voir nos maîtres, et nous commerçons les surplus ainsi que nous l'avons toujours fait. Ils ne viennent pas nous dire quel appel lancer du minaret et n'interviennent jamais dans les prêches. Pouvez-vous en dire autant ?

--- C'est bien pour ça que nous refusons de courber la tête devant l'Alépin » reconnut Idris.

De retour des champs, plusieurs ouvriers rapportaient des paniers vides, certainement destinés à des semences. Des senteurs commençaient à se propager depuis les cuisines, mélange de condiments et d'huile d'olive chauffée. Idris attrapa un morceau de galette qu'il trempa dans une purée de pois. Il appréciait le lieu et se disait qu'il pourrait s'y faire, même s'il devait faire allégeance pour cela à des infidèles.

Il avait de nombreux défauts, il le reconnaissait, mais ne comptait pas la bigoterie parmi eux. Quoiqu'à bien y penser, il tenait avant tout à sa liberté d'aller et venir. Il devait parfois y mettre les formes, mais c'était un prix qu'il acceptait de payer. Même lorsque cela demandait de supporter plusieurs semaines de tracasserie administrative.

« J'ai pu obtenir une lettre pour commercer librement dans les cités du royaume de Badawil[^45]. J'ai espoir d'acquérir du grain, des fèves et pois. Notre cité en manque cruellement...

--- Je n'ai plus guère de quoi fournir une caravane, nous sommes en période de jointure. Mais la cité de Baniyas a beaux celliers bien garnis. S'ils n'ont pas déjà tout revendu, il se trouve là-bas des négociants qui auront de quoi vous convenir. Je saurai vous donner des noms.

--- Je n'ai vu que peu d'Ifranjs dans la cité quand j'y suis passé. J'avais espoir d'y recruter des hommes pour faire escorte aux bêtes.

--- Soudoyer ifranjs à votre service ? » s'étonna le ra'is.

Idris hocha la tête plusieurs fois tout en avalant un peu de leben[^46].

« Je sais que l'Alépin ne souhaite pas la guerre avec Badawil. Pas encore du moins. Donc ses hommes hésiteront à s'en prendre à moi si je suis protégé de tels hommes.

--- Voilà bien dangereux stratagème...

--- *Un fou a jeté une pierre dans un puits ; mille sages n'ont pu la retirer*. Les miens ont faim, et l'émir souffle sur les braises du mécontentement. Les ventres creux suivront la main qui les nourrira, et même si c'est celle qui les a affamés.

--- Inch'Allah ! » approuva le ra'is.

### Baniyas[^47], après-midi du youm el itnine 19 muharram 549[^48] {#baniyaspaneas-après-midi-du-youm-el-itnine-19-muharram-549}

Idris mit du temps pour dénicher l'échoppe de Giorgos. C'était un négociant auprès duquel il pouvait faire valoir quelques amis communs qui les garantiraient l'un l'autre. En outre, c'était un marchand réputé. Un peu sur le déclin depuis quelques années, mais dont on avait assuré Idris qu'il saurait répondre à sa demande.

Le bâtiment cherché occupait l'angle d'une place où chantait une fontaine, non loin d'un quartier où des potiers opéraient. Nulle céramique ne qualité de se voyait sur les éventaires aux abords, mais seulement de grosses jattes, des oules de cuisine et des vases de transport. Quelques ouvriers passaient d'une cour à l'autre, portant bois, paniers paillés ou plateaux de pièces en attente de cuisson.

Quelques enfants s'amusaient dans un arbre poussé parmi les débris d'une maison voisine en ruines. Un mendiant marmonnait entre ses gencives édentées, s'adressant à quelque génie visible seulement de lui. Le tintement des billons de cuivre qu'Idris jeta dans sa coupelle ne lui fit même pas tourner la tête.

La minuscule échoppe était bordée sur ses deux côtés de banquettes en terre, dont l'une disparaissait sous un fatras de paniers et de sacs. En face, assis sur un mince matelas, un homme au visage rond d'une cinquantaine d'années, moustachu et mal rasé, dictait une lettre à un jeune adolescent. Lorsqu'Idris s'approcha, le marchand leva la tête vers lui et sourit d'un air engageant.

« Puis-je t'aider, étranger ? Tu me sembles à la recherche de quelque chose, ou de quelqu'un.

--- Si vous êtes Giorgos, l'ami de Ahmed ben Tayyib de Ba'albak, je l'ai trouvé » rétorqua Idris avec affabilité.

À la mention de son nom, le négociant dévoila une nouvelle fois ses chicots abîmés et invita son interlocuteur à entrer. Il envoya son apprenti chercher de quoi se désaltérer puis proposa à Idris de s'asseoir face à lui. Lorsqu'ils furent installés confortablement devant quelques victuailles, il commença à poser de nombreuses questions, dont il n'attendait pas toujours les réponses, préférant présenter sa propre situation sur ce point.

Il s'animait assez fiévreusement, secouant les bras en tous sens tandis qu'il parlait d'une voix montant facilement dans les aigus. Il engloutissait les friandises sans même les mâcher ni ralentir le débit de ses paroles. Rapidement il s'adressa au Damascène comme à une vieille connaissance. Il insista à plusieurs reprises pour que le jeune marchand soit hébergé dans sa demeure le temps qu'il serait à Baniyas, ce qui serait, affirmait-il avec autorité, gage de sérieux pour les partenaires qu'il désignerait à Idris.

« Je ne sais si j'aurais besoin d'en voir nombre, je n'ai besoin que de grains et fèves. On m'a dit que vous étiez fort expert en cela.

--- Celui qui t'a dit ça est trop aimable. Je ne fais plus guère d'opérations. Je m'associe aux autres, je prends des parts là, je fournis le capital ici... Mais n'aie crainte, nous trouverons bien quelqu'un avec des greniers à délester. Combien t'en faut-il ? Comment comptes-tu les emporter ?

--- J'ai des ânes pour porter jusqu'à Damas. Autant de ghîrara[^49] que je pourrai en trouver. Un de mes hommes s'occupe des bêtes, j'espère en assembler au moins six à sept douzaines. Si je peux en avoir plus, je les prendrai.

--- Je connais l'homme qu'il te faut. Tu as tes muletiers apparemment ? Donc pas besoin de guide. Si tu peux ramener les bêtes tu auras un bon prix chez un mien ami. »

Il marqua une pause, s'éclaircit la gorge et attrapa un carnet de feuilles dans une sacoche épinglée au mur à côté de lui. Il le compulsa un petit moment à voix basse, comptant ensuite silencieusement puis reprit la discussion avec Idris.

« Le froment sera aisé à trouver, mais peut-être pas autant que tu l'aurais souhaité. Son propriétaire est avaricieux et ne sait pas lâcher quand il le faut. Chaque année il en gâte une partie à ne pas vouloir le céder à moindre prix. Mais cela ne lui sert jamais de leçon ! conclut-il en s'esclaffant.

--- *L'erreur est fille de l'erreur* confirma Idris.

--- Et tu es philosophe en plus de négocier ! s'enthousiasma Giorgos. Dommage que tu sois mahométan, sinon je t'aurais proposé de marier ma cadette. »

Il assortit sa remarque d'un clin d'oeil et se replongea dans ses notes, puis compulsa des tablettes. Pendant ce temps, Idris laissait errer son regard au-dehors. La journée était ensoleillée et les gens vaquaient tranquillement. De nouveau, il s'étonnait du peu de Latins rencontrés dans les murailles. Il avait dû aller jusqu'à Jérusalem pour obtenir les documents définitifs qui lui permettaient de commercer. Il avait découvert pour la première fois l'ampleur du royaume de ces conquérants venus par-delà les mers.

Homme de transaction plus que d'affrontement, il en était revenu émerveillé comme un enfant. Un peu inquiet en ce qui concernait l'avenir de sa cité, dont la splendeur n'avait rien à envier à la capitale franque selon lui. Il n'arrivait pourtant pas à envisager le monde de façon binaire. Il avait été confronté à des imbéciles dans tellement de pays, et accueillis par des visages hospitaliers dans suffisamment de ports qu'il n'accordait plus guère de foi aux puissants et à leurs territoires. Il ne s'intéressait qu'aux individus, s'amusant par avance des joies de la rencontre.

Il lança un regard à Giorgos, dont il soupçonnait la subtilité habillée de façons grossières. Sa faconde et ses plaisanteries aimables faisaient partie de ses talents de négociant, Idris en aurait juré.

« J'ai beau creuser ma pauvre cervelle et compulser mes notes, je crains qu'il n'y ait guère de pois et fèves. Tu as avec toi de quoi comparer nos mesures ?

--- Je ne voyage jamais sans !

--- Parfait. Car ici nous avons obligation de passer par un mesureur juré. Il possède les étalons et apprécie en boisseaux ce genre de victuailles. »

Il ajouta avec une grimace :

« Il faut bien évidemment lui verser la taxe de pesage à cette occasion, à charge de l'acquéreur. »

Idris hocha la tête en silence, il était accoutumé à cela. Il se pencha ensuite vers le vieux marchand et ajouta :

« J'aurais aussi et surtout besoin de trois ou quatre soudards aguerris. Plus pour impressionner qu'autre chose. Des Celtes. »

Giorgos fit une grimace de déplaisir et massa ses bajoues comme si une puce venait de s'y cacher.

« C'est que je n'aime guère fréquenter ces oiseaux-là. Il m'est difficile de te donner bon conseil en ce sens. Ils sont pires que des chacals. »

Il hésitait, soufflait, visiblement ennuyé. Ou comédien pas trop inspiré, se demanda Idris. Puis Giorgos lâcha, presque à regret :

« Je demanderai conseil à Kaspar, un voisin qui a grand talent en archerie. Il est parfois soudoyé pour tenir la muraille. Son père était un Ermin[^50], habile ferrant. Pas vraiment un ami, mais de bonne fame. »

### Baniyas, maison de Giorgos, soirée du youm el itnine 19 muharram 549[^51] {#baniyas-maison-de-giorgos-soirée-du-youm-el-itnine-19-muharram-54951}

Idris s'en voulait un peu de l'opinion qu'il s'était forgée de son hôte. Il était extrêmement bien accueilli, disposant d'une chambre pour lui tout seul, avec un bon matelas en guise de lit et plusieurs coffres fermant à clef pour y poser ses affaires. Après leur discussion, il avait pu se rendre aux bains et finissait de s'apprêter pour le repas du soir. Il était prévu de rencontrer quelques contacts commerciaux, plusieurs des associés de Giorgos œuvrant dans les denrées alimentaires et la location d'animaux de bât.

Lorsqu'il parvint au bas des marches dans la petite cour où s'ouvrait la salle de réception, Idris aperçut que le maître de maison, habillé de frais dans une robe colorée, conversait avec un solide gaillard dont le dos voûté trahissait le travail d'archer. En le voyant arriver, les deux hommes se turent et se tournèrent vers lui, l'air sombre.

« Quelque soucis, maître Giorgos ?

--- Je crains que vous n'ayez grande difficulté à recruter des hommes pour porter des denrées à Damas, mon ami. Kaspar, que voici, me porte bien fâcheuses nouvelles pour vous... »

La poitrine d'Idris se comprima soudainement. Il s'approcha, attentif, tandis que le géant arménien lui expliquait :

« Un coursier est passé ce jour. Le roi Norreddin attaque Damas. Votre roi a appelé Baudoin à l'aide. L'ost sera levé pour aller porter soutien aux vôtres, en vertu de nos traités. »

Idris ferma les yeux lentement. Verrait-il le jour où les vautours laisseraient Dismashq en paix ? ❧

### Notes

Au milieu de la décennie 1150, les choses deviennent de plus en plus difficiles pour le pouvoir damascène, surtout après la mort d'Anur qui avait magistralement réussi à jouer la carte diplomatique pour se maintenir entre les Latins et Nūr ad-Dīn. Ses successeurs ne furent pas de même force et il ne fallut guère de temps pour que l'émir arrive à ses fins. Faisant cela, il posait la première pierre indispensable à l'éviction des Francs de Terre sainte, en unifiant la Syrie du Nord (Alep) à Damas. La déliquescence fâtimide en Égypte allait offrir le terrain d'affrontement pour les années qui suivraient, une fois que les opposants comprirent qu'il était vain de penser pouvoir aisément faire fluctuer les frontières orientales des Latins.

Malgré la guerre endémique qui sévissait, les négociants circulaient entre les cités. Les missives privées retrouvées ne font que superficiellement référence aux batailles dont les chroniqueurs nous disent qu'elles ébranlaient les royaumes. Il semblerait que pour les marchands, la guerre ait représenté un impondérable semblable aux naufrages, maladies, brigandages et taxes. Ils s'en accommodaient et s'efforçaient de faire affaire en respectant les complexités administratives des différents pouvoirs auxquels ils étaient confrontés. Pas étonnant qu'ils aient alors été considérés comme de réels aventuriers, et que les négociants âgés aient préféré gérer leurs investissements grâce à des commissionnaires voire par correspondance.

### Références

Ashtor Eliyahu, *Histoire des prix et des salaires dans l'orient médiéval*, Paris : École Pratique des Hautes Études, 1969.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Elisséef Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Le disparu
----------

### Lydda, chambre de l'évêque, matin du mardi 23 septembre 1158

L'évêque Constantin était assis à sa petite table face aux jardins. La fenêtre largement ouverte laissait entrer la chaleur naissante de la journée, accompagnée des effluves capiteux des fleurs et fruits cultivés en contrebas. En arrière-plan, les bruits du chantier de la basilique commençaient à se faire entendre. Une brise de mer agonisante apportait depuis l'occident quelques nuances salées, parfois un peu de fraîcheur. L'évêque se dit que bientôt la récolte du coton débuterait dans les campagnes de Saint-Jean d'Acre.

Bien qu'il soit relativement âgé, Constantin demeurait en assez bonne forme. Un physique robuste, empâté avec les ans, lui donnait l'allure d'un vieux chevalier. Il l'avait acquis en selle, en armure, auprès des saintes reliques qui accompagnaient généralement le roi de Jérusalem dans ses déplacements. Ses mains avaient aussi souvent servi à bénir les étendards et les combattants de la foi qu'à tenir les rênes de sa monture. Il n'était pas pour autant un guerrier, incapable de la moindre stratégie militaire, qu'il abandonnait à d'autres. Son devoir était de garantir aux troupes l'appui divin dans leurs affrontements, pas de manier une épée.

De fréquents voyages, à la tête de sa propre compagnie d'hommes en armes, avaient tanné son visage, creusé de nombreux sillons dans sa face ronde, aux larges oreilles. La bonne chère emplissait ses traits et le faisait paraître moins âgé qu'il n'était en réalité, mais le regard parfois las trahissait le poids des ans. Si les sourcils étaient à peine parsemés de blanc, son crâne ne recevait plus aucune tonsure, tellement la couronne de cheveux gris se réduisait à un souvenir. Les lèvres fines étaient rarement déformées par un sourire, dévoilant des dents usées et gâtées, mais il n'en était pas pour autant acerbe ni autoritaire. Ses domestiques n'étaient que peu confrontés à des sautes d'humeur ou des caprices et lui accordaient volontiers un caractère débonnaire.

Il avait gardé de ses années d'étude au monastère et de canonicat l'habitude de se lever au son des heures, et priait avec régularité, dans son oratoire. Son sommeil se découpait donc en plusieurs plages, dont la sieste, ce qui entraînait des en-cas assez fréquents, avec des repas plutôt légers, sauf lorsqu'il recevait des invités.

Comme souvent, il était vêtu d'un bliaud de soie claire à motifs d'oiseaux, sans ceinture, avec un impressionnant crucifix émaillé autour du cou. À sa main, l'anneau de sa fonction étincelait tandis qu'il brassait les fruits secs trempés dans du vin à l'aide d'une cuiller en argent. Il n'avait même pas touché à sa compote fraîche ni aux quelques beignets qui l'accompagnaient. Il se sentait morose.

Les pas d'un valet venu chercher son vase de nuit le tirèrent de sa rêverie. Il apostropha le domestique et lui fit signe d'approcher.

« Avez-vous eu quelques nouvelles de Josce ? Ce démon est absent depuis plusieurs jours désormais.

--- Nenni, sire. Nul ne l'a vu ni aux communs, ni en l'héberge, ni en l'hostel.

--- Il n'est pourtant pas de ses habitudes de disparaître ainsi. Crois-tu qu'il aurait quelque raison d'avoir fui ?

--- Je suis persuadé qu'il n'a que louanges à votre égard, sire. Il serait bien ingrat de ne pas vous savoir gré d'une telle amitié. »

L'évêque hocha la tête, à demi convaincu.

« Savoir ce qui passe dans la tête de telle jouvence... »

Il congédia le valet d'un geste et entreprit d'avaler des fruits. Il n'aimait pas gâcher la nourriture, d'autant qu'il avait assez goûté au pain dur de campagne ou au biscuit de mer pour apprécier les plats de ses cuisines. Une gorgée de vin légèrement coupé lui mit un peu de baume au cœur. Certainement un cru de Samarie, estima-t-il.

Le chantier résonnait désormais de cris de manutentionnaires. Il reconnaissait le grincement d'une énorme grue hissant les pierres, le tintement du marteau du forgeron. Il se dit qu'une visite impromptue de la construction lui ferait le plus grand bien. Il n'était pas toujours intéressé par les explications détaillées du maître de l'œuvre, mais appréciait fort de superviser de loin les thèmes iconographiques prévus par le chapitre des chanoines, ou de valider d'un signe de tête encourageant une décision architecturale.

C'était sa façon de faire, déléguer au maximum tout en insistant bien sur son nécessaire assentiment à toute chose. Sans pour autant s'embarrasser à vérifier si le déroulement était conforme à ce qu'il en avait demandé. Ça aurait été admettre qu'il envisage qu'on ne lui obéisse pas à la lettre. Nul n'aurait pu dire s'il s'aveuglait volontairement ou ne tenait aucun compte des manquements, mais chacun s'en félicitait, trop heureux de pouvoir en décider à sa guise, sous sa supervision purement nominale.

### Lydda, sacristie de l'église Saint-Georges, matinée du mardi 23 septembre 1158

L'épaisse porte de bois ferré menant au petit trésor était largement ouverte et le père Heimart, trésorier de la congrégation augustinienne, y penchait sa délicate silhouette pour y prendre les différents éléments qu'il présentait à l'évêque. Il espérait le convaincre de la nécessité d'étoffer leur collection d'objets liturgiques.

Manipulant un coffret, il en fit jouer la serrure et en sortit plusieurs patènes, soigneusement rangées dans des bourses de soie. Il les compta avant de les extraire avec précaution de leur étui. Il ne pouvait s'empêcher de tirer la langue tandis qu'il faisait cela.

« Voyez, père, comme nous manquons cruellement de tout. En cas de messe solennelle, nous n'aurons jamais assez de ciboires.

--- N'en avons-nous pas commandé à cet artisan d'Acre ?

--- Nous en avons seulement parlé, mais rien n'est fait. »

Il se releva brusquement, les courtes mèches frisées de sa chevelure blonde dansant autour de sa tonsure.

« D'ailleurs nous n'avons pris nulle décision encore en ce qui concerne les pyxides[^52]. Nous pourrons avoir plusieurs autels en la basilique neuve, et autant de tabernacles. Outre, il est fort probable que moult pèlerins viendront faire visitance à Saint George. »

Tout à ses réflexions, il ne prit pas garde au fait que l'évêque Constantin s'était assis sur un escabeau et regardait vers la nef. Heimart prenait sa charge de trésorier très à cœur et s'inquiétait devant l'ampleur du chantier. Il s'attendait à voir déferler dans les années futures des hordes de fidèles, et tenait à avoir tout le nécessaire pour célébrer le culte de façon appropriée.

Plus influencé par la liturgie clunisienne que par celle des cisterciens, il se plaisait à imaginer les travées emplies de chefs-d'œuvre d'orfèvrerie et d'étoffes plus somptueuses les unes que les autres. Pour cela, il n'hésitait pas à voyager dans tout le royaume pour rencontrer les artisans les plus habiles. Les convois de négociants de tissu étaient également avertis qu'il faisait bon faire halte à Lydda lorsqu'on était sur le chemin de Jaffa, car le frère trésorier était chaque fois désireux d'acquérir les belles pièces de soie qu'on possédait. Il dépensait d'ailleurs une large partie de sa prébende canoniale en ce sens.

Constantin finit par reporter son attention sur le chanoine, toujours occupé à sortir et montrer le mobilier liturgique. Il ne semblait pas décontenancé par le fait que l'évêque ne l'écoutait guère. Il en était à présenter plusieurs étoffes finement ouvragées.

« J'ai pris sur moi de faire broder ces pavillons, nous en aurons ainsi plus qu'assez. Ma seule inquiétude concerne la réalisation de corporaux »

Sa voix s'éteignit lorsqu'il vit que l'évêque Constantin faisait la moue.

« N'êtes-vous pas d'accord avec moi, père ? s'émut le jeune homme.

--- Que si, mon fils. Je suis désolé, je n'ai guère la tête à ces questions. Je vous avais promis d'en discuter, mais je n'arrive pas à y trouver intérêt. »

Devant le visage décomposé d'Heimart, il s'empressa d'ajouter :

« Ceci malgré la haute importance de ces questions. C'est vous dire comme je suis troublé.

--- Auriez-vous souci ? Voulez-vous que je fasse appeler votre chapelain confesseur ?

--- Ce n'est rien de cet ordre. Juste de l'inquiétude à propos de mon petit Josce...

--- Il a encore fugué ?

--- Il semblerait bien. Et nul ne l'a vu s'éloigner. Je crains fort pour lui, il n'est pas si habile pour se débrouiller par lui-même. Il est encore un peu jeune. »

Heimart hocha la tête doucement. Comme tout le monde au palais, il était habitué aux facéties de Josce, protégé par la main bienveillante de l'évêque. Sa disparition n'était pas pour lui déplaire, mais le désarroi que cela causait chez le vieil homme le touchait malgré tout. Il se gratta le cou, réfléchit un petit instant et arbora un franc sourire. Il avait trouvé ce qui pouvait rasséréner le prélat.

« Vous ai-je parlé de ce nouvel habit de grande fame chez nos frères de France et d'Angleterre ? »

Il s'empara d'un fin voile de lin soigneusement plié et le déploya sur son épaule en un geste frivole.

« On le nomme surplis, et il est fort apprécié pour les offices. Il serait bien avisé d'en user en lieu et place de nos aubes, lourdes et incommodantes vêtures de laine. »

### Lydda, chantier de la nouvelle basilique Saint-Georges, fin de matinée du mardi 23 septembre 1158

La chaleur déversée par le soleil commençait à se répandre, et l'évêque se félicita d'avoir pris un chapeau de paille et un éventail. Il avait décidé de voir l'avancée des travaux de la future église, sans pour autant déranger les ingénieurs et ouvriers. Il entretenait également l'espoir que Josce aurait trouvé là un espace de jeu à la hauteur de son imagination. Il ne pouvait guère définir exactement pourquoi il y était autant attaché. Enfant, il avait été confié à un établissement ecclésiastique à peine savait-il marcher. L'amusement lui était donc alors inconnu, et peut-être compensait-il sur ses vieux jours.

Il était parfaitement conscient du désordre qu'engendrait Josce et se promettait chaque fois de le morigéner comme il convenait. Il n'aurait accepté de quiconque le dixième des bêtises de Josce. Et pourtant subsistait cette affection sans jugement.

Il stoppa soudain, impressionné par l'ascension d'un chapiteau décoré, suspendu dans un écrin de toile et de cordes au bout d'un épais câble tracté par un âne à travers un astucieux enchevêtrement de poulies. Parmi les hommes nerveux qui criaient, guidaient à l'aide de filins la montée de la sculpture, se trouvait l'imagier responsable de l'œuvre. Un morceau de robuste bâche en guise de tablier, les poings sur les hanches, il admirait l'envol de son travail.

Au sein même de ce qui serait la future nef, une loge hébergeait ses collègues, dont les burins résonnaient en cadence tandis qu'ils libéraient de leur gangue de pierre les feuillages, personnages et créatures qui orneraient l'édifice. Autour d'eux, sur des planches de bois, de vieux cuirs ou du papier, des indications avaient été tracées par la main experte de Régnier, le chanoine responsable du programme iconographique.

L'évêque aperçut des pèlerins admiratifs du chantier, essaimés le long du chemin. Appuyés sur leurs bourdons, ils écarquillaient les yeux, émerveillés par la silhouette élancée des colonnes jaillissant vers le ciel. Ils ne semblaient guère prêter attention au fait qu'ils étaient au milieu du passage et gênaient les portefaix transportant mortier et moellons. Ils se poussèrent néanmoins en entendant le grincement de l'essieu d'un lourd charroi de pierre tiré par deux bœufs.

À l'entrée du chantier, la loge des ingénieurs prolongeait le bâtiment de la forge. Cette dernière était le seul élément édifié en dur. Le tintement qui en émanait marquait le rythme des journées de travail. Deux artisans et leurs aides s'employaient chaque jour à réparer les outils, à en façonner de neufs. Ils produisaient également les accessoires métalliques nécessaires à la construction. Les pièces d'art étaient commandées à des ateliers spécialisés, chargés de composer portails à motifs complexes, treillis de ferronnerie et serrures décoratives.

Tout en déambulant, l'évêque espérait apercevoir la silhouette de Josce, sachant que l'endroit recelait mille cachettes où s'amuser. Malgré toute la prévenance du prélat et de ses domestiques, le lieu offrait trop d'occasions de jeu pour en réfréner l'attirance. Lorsque, transpirant, il rejoignit les bâtiments de son palais, son estomac commençait à se manifester. Peut-être que l'appel du ventre serait assez fort pour faire rentrer le fugueur. Avant de passer la porte, il jeta un dernier coup d'œil circulaire à l'édifice qui s'annonçait somptueux. La perspective d'avoir ainsi contribué à ériger un tel monument de la Foi arracha un soupir d'apaisement à son esprit chagrin.

### Lydda, salle d'audience du palais de l'évêque, après-midi du mardi 23 septembre 1158

Constantin avait passé une partie de son après-midi à vérifier et faire sceller plusieurs chartes en attente depuis quelques jours. En tant que dirigeant de la cité de Lydda, il avait des devoirs en plus de la charge spirituelle et foncière de l'évêché. Il ne s'opposait plus aussi violemment aux prétentions des seigneurs de Ramla que ses prédécesseurs, mais il tenait à manifester la réalité de son pouvoir temporel. D'ailleurs, il devait pour celui-ci service à Baudoin un contingent de dix chevaliers et vingt sergents d'armes, dont l'étendard au Saint George était connu dans tous les territoires.

On lui avait signalé l'arrivée de son banneret, Seguin de Brebon, à la mi-journée. Celui-ci avait préféré se rafraîchir avant de venir présenter les dernières informations de l'ost royal. Cela signifiait donc qu'elles étaient plutôt bonnes. Par les fenêtres ouvertes, donnant sur la cour d'entrée, il entendait le remue-ménage engendré par le retour de la troupe. Tandis que les montures et animaux de bât étaient pris en charge, les hommes manifestaient bruyamment leur enthousiasme d'être rentrés sains et saufs en discutant de leurs aventures avec les domestiques.

Il y aurait un grand repas ce soir pour les recevoir comme il seyait. L'évêque aurait préféré demeurer tranquille dans sa chambre, mais il ne pouvait décemment pas faire un pareil accueil à ceux qui étaient allés le représenter les armes à la main. Il savait que certains d'entre eux s'inquiétaient toujours de devoir verser le sang, quand bien même ils soient bénis et au service d'une juste cause. D'ailleurs, certains respectaient scrupuleusement les pénitences infligées, espérant racheter leur violence par des pratiques spirituelles redoublées.

La grande porte grinça en s'ouvrant, laissant passage à la haute silhouette voûtée de Seguin. Manifestement sorti d'un bain, il était rasé de frais, le cheveu propre bien coupé à l'écuelle comme il l'affectionnait. Ses joues semblaient encore plus creuses qu'à l'ordinaire, reflet des semaines occupée à guerroyer, et on ne voyait de lui que ses lèvres charnues et son nez busqué. En apercevant la silhouette de l'évêque installée sur son trône, un large sourire égaya son visage austère et il s'approcha d'une démarche empressée. Il baisa la bague du prélat et attendit qu'on lui donne la parole.

« Je m'enjoie de vous voir sauf auprès de moi, mon fils. J'espère que vous n'avez que bonnes nouvelles à m'adresser.

--- Il y a tant de choses qu'il me faudra long moment pour ne rien en oublier. Tout d'abord notre sire Baudoin vous envoie son salut, et vous remercie, par son connétable, d'avoir si promptement répondu à son appel une fois de plus.

--- Nous verrons ces questions par la suite. Dis-m'en plus sur les hommes. J'espère que tous sont saufs.

--- Seulement des blessés et quelques malades à déplorer, mais tous sont revenus, sire. Depuis la bataille peu après la saint Martin[^53], nous n'avons que peu entrebattu l'armée du Soudan.

--- Le roi est-il satisfait ?

--- J'ose le croire. Nous avons affranchi les Caves de Suète[^54] et le Soudan est parti sans oser batailler. »

L'évêque approuva sans un mot puis invita d'un geste son vassal à prendre place sur un petit tabouret. Un valet leur servit du vin tandis que le silence s'installait. Ce fut le chevalier qui reprit la parole, après avoir longuement savouré la boisson.

« J'ai ouï parler des assauts faits depuis l'Égypte. Espérons que l'ost parti les combattre y mettra bon ordre.

--- Penses-tu que le ban sera levé pour cela ?

--- Nul ne m'en a parlé. Le roi est de retour en son palais et nous avons même cheminé un temps avec le sire comte Amaury[^55], qui s'en est allé dans ses terres également.

--- Parfait. Fais assavoir aux hommes qu'un banquet sera donné ce soir. Ils l'ont sûrement appris par les cuisines, mais que cela soit officiel. Ta femme et toi serez servis à ma table. »

Seguin inclina la tête en remerciement, et avala ce qu'il restait de son vin. Puis il fouilla dans une besace pour en sortir de nombreux plis. En découvrant la quantité, Constantin souffla.

« Fais donc appeler mon secrétaire, veux-tu ? Nous étions justement dans la paperasserie. »

### Lydda, chambre de l'évêque, veillée du mardi 23 septembre 1158

La voix forte de Maugier du Toron résonnait sous le haut plafond tandis qu'il évoquait les quelques soucis de police qu'il rencontrait. La ville était située sur le chemin de la côte, et les gens de passage y occasionnaient pas mal de troubles. Rien que le vicomte n'arriva à mâter, mais suffisamment pour qu'il estimât nécessaire d'en avertir le seigneur en titre du lieu.

Sous un physique gras et indolent, Maugier dissimulait un esprit vif et une volonté inébranlable. Dédaignant le cheval, il arpentait les rues et le domaine d'un pas infatigable malgré son embonpoint. On moquait d'ailleurs son habitude de toujours s'éponger le front d'un linge qui ne quittait jamais sa main, à tel point qu'on le surnommait « sire la Toaille[^56] ». Son autre caractéristique était d'être perpétuellement infesté de poux et de puces, ce qui le faisait se gratter sans cesse, incapable de demeurer immobile.

Ce soir, l'évêque espérait de lui un rapport des jours passés, et certainement quelques nouvelles de Josce. Le rusé vicomte était au courant de la disparition et avait donc chargé quelques hommes d'y être particulièrement attentifs. Ce qu'il craignait le plus, c'était qu'une troupe de jongleurs n'ait attiré le malheureux pour l'emmener au loin. Au final, tout ce qu'il savait, c'est qu'il n'avait rien appris susceptible de dérider le vieil ecclésiastique. Sans pour autant dire qu'il avait de l'amitié pour Constantin, qu'il trouvait parfois trop irrésolu, il éprouvait un certain attachement à une personne qu'il devinait finalement peu désireuse de diriger, bien que déterminée à se conformer à son devoir.

Il conclut son récit en se raclant la gorge, tout en se grattant un sourcil. Il allait avaler un peu d'hypocras et ajouta, l'air désolé :

« Il y a aussi les soucis avec ces nouveaux colons au village de Séphorie. La cour des Bourgeois doit statuer prochainement.

--- As-tu idée de qu'ils décideront ?

--- Je leur ai soufflé l'idée que le jurement sur la nouvelle charte confirmait les anciennes coutumes, ce qui leur convient fort bien. Je ne vois pas pourquoi le champart serait moindre pour quelques-uns. C'était là usage des sires de Rama[^57], pas les nôtres. »

L'évêque grogna en assentiment. Les chicaneries fiscales et légales étaient légion, certains villageois prétextant de coutumes différentes pour tenter d'échapper au versement de certains impôts. Il fallait sans cesse réaffirmer leur position pour éviter de voir disparaître certaines redevances. Il avala quelques amandes effilées, espérant que le vicomte aurait des choses à ajouter. Il se doutait bien que son inquiétude était connue, et qu'on lui aurait annoncé dès le début de l'entretien tout ce qui aurait pu soulager son angoisse, dans un sens ou l'autre. Mais il ne put s'empêcher de demander.

« N'avez vous rien découvert à propos de Josce ?

--- Nenni, et j'en suis fort déçu. Il est suffisamment bien connu des hommes, mais nul ne l'a vu.

--- Il faut dire qu'il n'a pas son pareil pour se faire discret... »

Le gros vicomte échappa un rire et agita son large ventre, goguenard :

« Il a certes grand don pour moquer avec astuce, heureusement qu'il est aussi vif pour s'échapper. »

L'évêque savait que Maugier appréciait fort Josce, qu'il gâtait de friandises jusqu'à en être déraisonnable. Cela lui garantissait en outre une totale impunité pour les bêtises qu'il accumulait dans la ville et les alentours lorsqu'il lui prenait l'envie d'y baguenauder. Le vicomte reprit une contenance et ajouta, sentencieux :

« Je suis acertainé qu'il va revenir tout penaud de la peine qu'il nous a infligée, et certes avec à ses trousses de nombreuses victimes de ses facéties »

### Lydda, chambre de l'évêque, nuit du mardi 23 septembre 1158

Simplement vêtu de sa chemise de fine laine, agenouillé devant le beau crucifix peint qui ornait le petit autel portatif qui le suivait en campagne, l'évêque Constantin susurrait les psaumes en s'efforçant de demeurer concentré. Cela lui apportait un sentiment de paix, de stabilité. Il n'avait plus guère besoin de vérifier les temps liturgiques, nourri d'une pratique ancienne, et se contentait parfois de contrôler avec les chanoines qu'ils étaient bien d'accord sur les textes à réciter.

Entendant un bruit étrange derrière lui, il se retourna soudainement, intrigué.

Sur la petite table où il avait toujours quelques fruits et friandises à portée de main pour une de ses fringales se tenait un singe vervet. Sans quitter le prélat des yeux, il grignotait avidement tout ce qui lui tombait sous la main. Sa fourrure claire maculée de poussière prenait des teintes fauves à la lueur des lampes.

Lorsque Constantin se leva, le primate fit quelques bonds souples, déliant sa longue queue, et atterrit dans les bras de l'évêque.

« Josce, petit fripon ! Où donc étais-tu caché tout ce temps ? » ❧

### Notes

Certains prélats jouaient parfois le rôle de seigneurs territoriaux, avec tous les devoirs que cela impliquait. Bien qu'ils n'aient pas à prêter hommage au roi, ce qui aurait posé souci en raison de leur statut de clerc, ils devaient soit le fournir en homme, soit, plus souvent, verser un dédommagement monétaire pour solder des troupes. De telles seigneuries ecclésiastiques n'étaient pas rares et compliquaient encore l'emboîtement des autorités légales, administratives et fiscales. Dans le cas de Lydda, des dissensions nombreuses mirent plusieurs dizaines d'années à s'éteindre face aux prétentions des dirigeants, laïcs, de Ramla tout proche.

La maison d'un évêque de ce type comportait donc à la fois les officiers et serviteurs d'un hôtel noble, en plus de l'organisation religieuse du diocèse, qui pouvait à son tour être détentrice à un titre ou l'autre de propriétés foncières ou fiscales.

En ce qui concerne le petit familier, Josce, on sait qu'il existait un commerce d'animaux pour les plus riches, dont certains aimaient à s'entourer d'une faune exotique. Les malheureuses bêtes avaient souvent une espérance de vie très courte, quand elles ne mouraient pas en chemin. Malgré tout, un certain nombre d'espèces, comme les oiseaux ou les singes, supportaient assez bien la captivité ou la domestication. On cite même des amuseurs qui usaient de tels compagnons.

### Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Mayer Hans Eberhard, « The Origins of the Lordship of Ramla and Lydda in the Latin Kingdom of Jerusalem » dans *Speculum*, vol. 60, No 3, Juillet 1985, p. 537-552.

Mariño Ferro Xosé Ramón, *Symboles animaux*, Paris : Desclée de Brouwer, 1996.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. I & II.*, Cambridge : Cambridge University Press, 1993.

Basses eaux
-----------

### Damas, fin de matinée du youm el itnine 10 chawwal 551[^58] {#damas-fin-de-matinée-du-youm-el-itnine-10-chawwal-55158}

La pluie avait transformé la rue poussiéreuse en un bourbier résonnant de bruits spongieux à chaque pas des bêtes et des hommes. La tête basse, Ibrahim sentait les gouttes parcourant son visage malgré la capuche de son durrâ'a[^59]. Chaussé de savates, il manquait de perdre ses souliers à chaque fondrière. Sans même y penser, il claqua sa badine sur l'arrière-train d'un de ses ânes, qui semblait vouloir s'arrêter et héla celui de tête pour qu'il continue à avancer. Avec la douche qu'ils recevaient depuis le matin, au moins n'aurait-il pas à brosser longuement les bêtes. Il espérait ainsi pouvoir enfin se mettre à l'abri d'ici peu.

Les rues étaient quasiment désertes, les Damascènes savaient que les intempéries n'étaient jamais persistantes. Ils attendaient donc, le nez collé aux claustras des fenêtres que la météo s'améliore. Seuls les voyageurs et les plus démunis des travailleurs étaient forcés de s'aventurer au-dehors.

Ibrahim appartenait un peu aux deux. Il menait des caravanes d'ânes entre les villages alentours et Damas, apportant les aliments frais des vergers et jardins et y emportant les produits manufacturés. Il louait les bêtes ou était embauché par de riches propriétaires, espérant amasser avec le temps de quoi s'acheter ses propres animaux. Depuis bientôt dix ans que son père l'avait poussé au travail, il n'avait guère vu progresser son pécule. Il s'acharnait malgré tout, y compris les jours où il faisait bon rester chez soi.

Il parvint enfin au bassin où il faisait boire les ânes avant de les rentrer dans leur écurie, dans un appentis contigu à l'immeuble où il habitait. Grossière maçonnerie appuyée le long d'un mur, sous une profonde arcade qui lui faisait comme un toit, l'eau y coulait claire et pure, depuis une canalisation dérivée de l'alimentation du hammam voisin. Coincé sur la margelle, se protégeant de la pluie de son mieux, il reconnut le vieil al-Dabbi.

C'était un mendiant à la silhouette tordue, la barbe miteuse et le turban en désordre, qui errait autour de son quartier, vivant des offrandes et de la bonté des riches donateurs à la mosquée. Il amusait les enfants avec ses histoires, se louait à l'occasion comme conteur lors des fêtes, mais dépendait surtout de la charité. Il salua Ibrahim en ricanant, tirant sur sa tête un repli des chiffons qui lui servaient de vêtement.

« Alors l'Ânier, on brave la pluie ?

--- *Dieu aime ceux qui persévèrent*, c'est pas vrai, l'ancien ? »

Tout en parlant, Ibrahim répartit ses animaux le long du bassin et vint se coller contre le mur, pour profiter à son tour du relatif abri. Il tordit le bord de sa capuche pour en exprimer l'eau, qui se répandit en une flaque à ses pieds.

« Tu n'as pas trouvé un endroit où te mettre au chaud ? Il ne fait pas bon rester au-dehors par un temps pareil...

--- Personne n'a envie d'entendre un vieux fou, pleurant après nos gloires passées !

--- Des raisons de se lamenter ? Je suis entre Berzé, Qâbûn et Gawbar depuis plusieurs jours et les nouvelles y arrivent chaque fois bien tard. »

Le vieil homme haussa les épaules et gloussa.

« À toi je peux bien le dire, ton père, qu'Allah le garde, est un vrai dimashqi, et je te crois comme lui. »

Il se mit à fredonner et marmonna un peu entre ses dents, tandis qu'Ibrahim surveillait d'un œil les ânes qui s'abreuvaient tranquillement. Le salmigondis devint progressivement des mots, jusqu'à former des phrases intelligibles.

« *Le monde de Damas est agréable pour celui qui l'a choisi et je ne veux pas d'autre lieu que Damas pour vivre ici-bas*. Ce doit être les larmes d'Abû Bakr as-Sanawbarî que nous recevons sur la tête, à nous voir si avilis. »

Il fouetta l'air de sa main, comme pour en éloigner un fantôme et ajouta ses postillons aux gouttes, forçant sur sa voix.

« Tu sais que le turc qui a forcé notre cité a plié le genou devant les polythéistes ?

--- Il y a eu bataille ? Je n'en ai eu nul écho !

--- Si seulement ! La honte serait moins brûlante ! »

Al-Dabbi renifla, se frotta le nez et lança, un air de dégoût barrant son visage :

« Nous devons verser tribut à nouveau, belle réussite pour l'émir ! Il n'en a que pour ses terres du Nord. Il ajouté le Bilad al-Sham à son collier de perles et n'a cure de notre honneur, de notre prospérité...

--- Payer ? Encore ? Ne devait-il pas nous affranchir de ces taxes impies ?

--- On l'a appris à la mosquée, au grand prêche dernier. Huit mille dinars sûri[^60] ! »

Ibrahim poussa un long soupir, cela n'allait pas arranger ses affaires. Sans en faire trop état, malgré sa famille, il était de ceux qui avaient vu d'un œil plutôt favorable l'arrivée de Nūr ad-Dīn. Il y espérait une occasion de sortir du marasme dans lequel la ville se complaisait depuis son enfance. Il gardait encore un souvenir effrayé de l'armée des chrétiens presque dix ans auparavant, avec leurs grands engins. Après cela, il avait craché, comme tant d'autres, dans les pas des émissaires qui venaient percevoir la redevance de la honte qui leur garantissait une relative tranquillité.

L'arrivée d'un chef de guerre compétent, dont les oulémas vantaient les qualités pieuses, avait redonné courage dans les quartiers, même si la fierté empêchait de le reconnaître publiquement. Et maintenant que son nom était crié par le muezzin, depuis la grande mosquée où le prophète avait prié lors de son voyage nocturne[^61], Nūr ad-Dīn négociait ignominieusement. La ville espérait un héros et s'était livrée à un homme.

Ibrahim leva la main pour saluer le vieux mendiant et tendit le bras pour attraper la bride d'une de ses bêtes. Il avait encore du travail avant de pouvoir se reposer. Il ajusta sa capuche et s'élança sous l'averse.

« Préserve-toi l'ancien !

--- *Le nez dans les nues, les fesses dans l'eau* ! » rétorqua al-Dabbi en agitant des doigts l'eau du bassin.

### Damas, après-midi du youm al had 29 dhu al-qi'dah 551[^62] {#damas-après-midi-du-youm-al-had-29-dhu-al-qidah-55162}

Husayn al-Hindi avançait en mâchonnant une lanière de pain, partage de la miche avec ses deux fils. Le plus grand, Hasan, portait comme lui une énorme jarre dans un filet sur son dos. Ils proposaient à boire, récoltant des piécettes de cuivre ou des jetons que le petit Ahmed, d'à peine cinq ans, recueillait dans un gobelet. Husayn était bossu, en permanence courbé sous la charge, comme son père avant lui et comme Hasan le serait bientôt. Mais il nourrissait sa famille chaque jour et accomplissait sa tâche avec ténacité.

Il lissa sa longue moustache en voyant l'attroupement près de la fontaine. Il mettait un point d'honneur à ne prendre que de l'eau claire, jamais tirée d'un bassin ou d'un puits. Son père lui avait enseigné l'importance de la réputation, et il y tenait, autant qu'à ses deux vastes récipients en terre qui leur brisaient l'échine.

Quelques femmes attendaient de remplir leurs cruches, pauvresses sans le sou, sans mari ou simples domestiques. Husayn leur adressa un salut respectueux. Il était toujours tenté de lier conversation, d'autant qu'il savait son sourire assez séduisant. Mais il se méfiait des commérages et ne faisait cela que lorsque les témoins étaient rares. Sa jeunesse sulfureuse était loin derrière lui et il n'avait pas envie de s'attirer des ennuis par son goût pour la badinerie.

Ils posèrent les céramiques contre le mur, s'étirant le dos tout en patientant. D'un gris froid, le ciel semblait prêt à déverser une averse à tout moment. Avec les faibles températures, Husayn n'aurait pas été étonné de voir de la neige le lendemain au matin. Ce qui n'arrangeait pas du tout ses affaires, ses maigres économies épargnées durant les périodes chaudes lui permettant souvent à peine de faire la jointure d'hiver, sans compter le renchérissement parfois effrayant des denrées.

Il s'accroupit, les coudes sur les genoux. Ses deux fils jouaient dans la poussière, avec des cailloux qu'ils amassaient dans les replis de leur tenue. Il en profita pour estimer le gain fait depuis le matin et grimaça devant le faible montant. Il leur aurait fallu au moins le double.

Entendant un chant familier, il tourna la tête et vit arriver deux solides Nubiens, chargés d'énormes outres : Al-Misri et son frère Razin. Husayn se leva et les salua chaleureusement. Ils se croisaient souvent à une fontaine ou l'autre, mais ne se faisaient nullement concurrence. Husayn proposait de l'eau à boire, qui demeurait fraîche dans la céramique, même en plein soleil. Les deux noirs apportaient celle pour les usages domestiques, et seuls les plus pauvres la consommaient parfois, car elle prenait vite un goût ranci, accentué par la tiédeur inévitable dans le cuir.

Razin dévoila le contraste de ses dents blanches en l'apercevant et vint le saluer. Leurs outres ne paraissaient guère ventrues. Pour eux aussi, l'hiver était un moment difficile. Malgré tout, jamais il n'avait entendu les deux frères, arrivés d'Égypte quelques années auparavant, se plaindre. Al-Misri était le plus volubile, en général, et Husayn le trouvait plus cancanier qu'une assemblée de femmes. Il avait toujours une belle anecdote à raconter, ce que semblait indiquer son air gourmand lorsque leurs regards se croisèrent.

« On dirait que tu as vu un trésor, al-Misri, s'enthousiasma Husayn.

--- *Par ici, une pomme qu'on prendrait pour une joue ; par là une grenade qu'on prendrait pour un sein. Comme il est agréable le séjour de Dârayyâ ! Là j'ai mené une vie aussi limpide que le miel*. »

Le Nubien hochait la tête en récitant les vers et s'amusait par avance de ce qu'il allait raconter. Il posa son lourd fardeau, avant de se rapprocher pour narrer son histoire.

« Tu sais que les émissaires de l'émir sont partis porter gros trésor aux Ifranjs ?

--- Toute la ville ne parle que de ça. Puissent les Ifrits leur rôtir les pieds, aux infidèles. Mais au moins nous n'avons plus à les voir en la muraille.

--- J'y vois un autre avantage... Un des émirs désignés à ce grand honneur possède plusieurs belles esclaves en plus de son épouse. Il les garde en une charmante demeure, vers Bâb Sarqi[^63]. Et là, elles se languissent de lui, confiées à deux vieilles matrones à l'odeur de chamelle.

--- Ne me dis pas que tu as pu y pénétrer...

--- C'est qu'elles aiment à prendre des bains, chez elles. Il faut bien leur porter de l'eau. Mais ce n'est pas là métier pour toi. »

Il assortit sa remarque d'une caresse amicale sur le bras d'Husayn, qui avait vérifié que ses fils n'entendaient pas. Il n'avait nulle envie que cela revienne aux oreilles de son épouse, dont le caractère aimable n'incluait pas la complaisance à la légèreté des mœurs.

« L'autre jour, nous remplissions la cuve, avec Razin, sous l'étroite surveillance d'une des vieilles. Mais comme elle était seule, une des jeunes beautés m'a chuchoté quelques mots dans le couloir.

--- De quel genre, ces mots ?

--- Du genre qui expliquent comment passer par une des fenêtres mal fermées en certaines occasions. »

Husayn ricana au sous-entendu et secoua la tête. Il n'avait jamais su dire si le Nubien était un affabulateur ou s'il avait une chance insensée. En tout cas il lui offrait l'occasion de vivre pas mal d'aventures par procuration.

« Un jour tu finiras par avoir des ennuis, à ainsi tremper ta plume à tous les pots.

--- Qui suis-je, qu'un pauvre affranchi que personne ne voit ?

--- Il n'y aura justement guère pour t'aider si un mari jaloux te fait couper les bourses, au mieux.

--- Faire de moi un eunuque pour que je vive au milieu des femmes ? Me menacer de ça, c'est comme menacer le canard de la rivière ![^64] »

Husayn secoua la tête, manifestant son désaccord en pure forme. Il tapa dans ses mains, souleva son imposante jarre et alla la placer sous le jet d'eau. Il ricanait encore tandis que le flot chantait en remplissant la panse.

### Damas, fin d'après-midi du youm al sebt 11 muharram 552[^65] {#damas-fin-daprès-midi-du-youm-al-sebt-11-muharram-55265}

Au moment de franchir le pont du Barada, Abu Malik al-Muhallab frissonna et s'emmitoufla dans les rafrafs[^66] de son turban. Un vent léger sifflait sur les murailles septentrionales de la cité, faisant voler la poussière du chemin. Quelques gamins étaient affairés à ramasser des détritus sous le pont, dont ils triaient ce qu'ils espéraient revendre. Il frémit de les voir ainsi tremper dans le courant glacé.

Face à lui la route se divisait en de nombreuses voies qui allaient serpentant parmi les jardins et les vergers. La guerre y avait prélevé son tribut, surtout sur les bâtiments les plus proches, mais l'endroit gardait une part de son charme, et ce même au plus fort de la saison hivernale. Il chercha du regard ce qui pouvait être un débit de boisson, perdu dans les décombres de ce qui avait été le haut lieu des fêtes de la bourgeoisie damascène pendant des décennies, si ce n'était des siècles.

Il lui fallut un petit moment pour trouver l'établissement qu'il espérait être le bon. Ce n'était qu'un très modeste estaminet, où un maigre feu au centre de la pièce apportait une clarté et une chaleur appréciables, au prix d'une atmosphère enfumée. Avisant sa belle apparence, le tenancier vint l'accueillir avec obséquiosité et le mena avec cérémonie à la personne qu'il avait demandé. L'homme tirait visiblement grande fierté d'avoir en ces murs un tel personnage.

Ce n'était pourtant qu'un vieil aveugle, appuyé contre une cloison rongée, qui jouait au manqala avec un gamin. Seule la qualité des étoffes de ses robes permettait de ne pas le prendre pour un mendiant. Le port de tête, également, trahissait le rang. Abu Malik s'approcha et s'inclina par pur réflexe.

« Salaam, j'ai pour nom Abu Malik al-Muhallab. J'ai bien l'honneur de parler à Altûntâsh, ancien gouverneur de Salkhad et Bosrâ[^67] ?

--- J'ai peut-être été celui-là par le passé, mais je ne suis désormais plus qu'un vieil homme auquel les cadis ont fait crever les yeux... »

Tout en répondant, il invita d'un geste son interlocuteur à s'asseoir et chassa le gamin avec lequel il jouait jusque là.

« Voyez comme le destin est ingrat. La main autrefois si forte à brandir l'épée se risque à peine à manier le jeton face à un enfant. Le brave gamin me mène chaque jour ici... *La fraîcheur étanche ma soif et puissent mes jours être abreuvés par le Barada et en goûter les pâturages*. »

Tandis qu'il récitait, un sourire triste creusait des sillons dans ses traits. Il leva la main, qui se mit à ondoyer.

« La première fois que je suis venu ici, avec mon seigneur et maître, Kumustakin, j'ai cru entrer au Paradis. Le murmure des eaux du Barada avive ces souvenirs en moi.

--- Vous n'avez pas une demeure parmi les jardins, plus accueillante que cette masure ?

--- Les juges m'ont pris ma fortune en même temps que les yeux. »

Il soupira longuement.

« J'aurais espéré que l'émir, une fois maître de la ville... »

La phrase mourut sur ses lèvres.

« Mais vous n'avez pas bravé le froid de l'hiver pour entendre les jérémiades d'un vieux fou. Puis-je vous être utile à quoi que ce soit ?

--- Je viens vous voir pour savoir si vous pourriez m'indiquer des gens à contacter pour racheter des captifs. Vous avez appris la razzia récente du roi Badawil[^68] ?

--- Difficile d'y échapper. La trêve n'aura pas tenu trois mois, voilà coûteux tribut. »

Il se gratta la joue, à la barbe soignée, mâtinée de poivre et de sel.

« Je suppose que vous pensez à des Ifranjs que j'aurais pu connaître par le passé.

--- N'importe qui capable de m'assister à la libération des captifs serait le bienvenu.

--- Mon nom risque de vous fermer des portes plus qu'autre chose. Ceux que j'ai le mieux connus... »

Il fit une moue, tourna la tête, se réinstalla sur son coussin, comme si sa posture devenait incommode.

« Il n'en est qu'un, peut-être. Un esprit rusé, et certes pas un ami. Mais parfois nos adversaires sont plus honorables que nos amis. »

Il marqua une longue pause, comme s'il se remémorait de lointains et pénibles souvenirs. Sa voix était rauque lorsqu'il se décida à parler de nouveau.

« Demande après al-Baqara[^69]. Ne te fie pas à son nom, c'est un sage. Il avait l'oreille du jeune roi. C'est un farouche adversaire, mais il y a du juste en lui. » ❧

### Notes

La cité de Damas qui avait réussi à maintenir un pouvoir indépendant depuis le début des Croisades finit par être rattachée à un empire plus vaste lorsque Nūr ad-Dīn l'intégra à ses territoires. Ce fut le résultat d'un long travail préparatoire de sa part, et lui offrait l'occasion d'unifier une large zone, suffisante pour opposer désormais un front quasi uni face aux Latins.

Pour les habitants de Damas, ce fut certainement au prix d'une profonde amertume qu'ils se virent ainsi absorbés. Ils abritaient un des lieux les plus prestigieux de l'Islam, avaient même été le siège du califat un temps. Pourtant ils étaient traités comme un fief provincial par un conquérant turc, basé dans le nord, à Alep. Saladin, qui grandit en partie à Damas, offrira de nouveau un rôle important à la cité syrienne, mais on peut sentir le ressentiment des chroniqueurs à l'idée de voir leur indépendance foulée au pied, malgré les acclamations de façade pour Nūr ad-Dīn.

L'histoire d'Altuntash demanderait de longs développements, mais son triste sort est intimement lié aux complexes jeux diplomatiques qui se sont déroulés entre Alep, Jérusalem et Damas pendant plus d'une décennie. Nous ne disposons que de peu d'informations, mais il semble s'être trouvé au centre d'un dangereux stratagème entre les trois puissances. Il est possible que cela m'offre à l'avenir l'occasion de proposer des hypothèses sur ce qui s'est passé en Syrie du sud en 1147 une fois certaines pistes mieux étudiées.

Enfin, le poème partagé entre les protagonistes, auquel la traduction ne rend pas vraiment grâce est cité par Ibn 'Asâkir et attribué à Abû Bakr as-Sanawbarî. Il appartient à une tradition de textes laudateurs sur la ville de Damas, dont le caractère sacré, la qualité architecturale et la beauté naturelle sont régulièrement mis en exergue par les artistes.

### Références

Élisséef, Nikita, *La description de Damas d'Ibn 'Asâkir*, Institut Français de Damas, Damas : 1959.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Petits dieux
------------

### Jérusalem, après-midi du mardi 27 août 1157

Allongé sur sa paillasse, Ernaut se prélassait tranquillement à l'abri d'un auvent de feuilles de palme, un pichet de vin frais et quelques rissoles à portée de main. Sa nuit de garde avait été épuisante, avec plusieurs soucis dus à des rixes entre voyageurs. Il avait donc décidé de faire une sieste avant de s'occuper de quelques tâches domestiques. D'autant qu'il espérait croiser le vieux Saïd, son voisin, pour lui demander de l'aide.

Droart avait invité tous ses collègues à profiter de sa nouvelle demeure pour le souper et la veillée. Il venait de s'installer dans une jolie maison vers Sainte-Marie-Madeleine, dans l'angle nord-est de la cité. Il y avait acheté plusieurs bâtiments, dont certains en ruine, mais cela constituait un bel ensemble et lui offrirait l'opportunité d'avoir un jardin et, pourquoi pas, des locataires, particuliers ou boutiques, une fois les édifices restaurés. Ernaut et quelques autres avaient passé une partie de la dernière quinzaine à transporter les affaires de la famille et l'ultime chargement avait été déposé quelques jours auparavant. Il était donc désormais bien temps d'arroser ça dignement.

Ernaut avait prévu d'apporter un peu de vin, histoire de sympathiser avec ses collègues. N'ayant prêté serment au roi que quatre mois plus tôt, il n'en connaissait qu'un faible nombre. Mais comme Droart était de ce premier cercle, il tenait également à marquer le coup avec un cadeau. Et c'est là qu'il avait besoin de l'aide de Saïd. Il n'avait aucune idée de quoi offrir ni d'où le trouver. Saïd était perpétuellement en quête de tâches, qu'il exécutait pour tous ceux qui voulaient bien de lui. Il connaissait donc parfaitement la cité et saurait indiquer les meilleurs artisans.

Lorsqu'il pointa le bout de son nez, portant une corbeille emplie de branchages, il était accompagné d'un petit chien au poil ras. Ernaut était toujours sidéré par cette capacité du vieil homme à récupérer des choses un peu partout. Sa cabane sur le toit était encombrée d'un fatras indescriptible des trésors qu'il avait accumulés avec les ans. Chaque jour semblait lui apporter un nouvel élément à entasser dans son modeste appentis.

« Je m'enjoie de te voir si tôt arriver, Saïd, j'aurais fort grand besoin de conseils de ta part. »

L'homme s'arrêta, affichant un large sourire qui le faisait ressembler à un fruit sec. Il était habillé d'un thawb[^70] élimé jusqu'à la trame et arborait sur le crâne un turban que peu auraient accepté en guise en serpillière. Il posa délicatement sa corbeille, puis s'approcha d'Ernaut, qui s'était accroupi en tailleur en attendant.

« Conseils pour sieste ? » rigola Saïd, gloussant comme une volaille, le regard un peu fou.

Ernaut lui tendit son pichet de vin coupé tout en secouant la tête. L'autre s'assit lourdement et s'empara de la cruche qu'il renifla, circonspect.

« J'ai désir de faire beau présent à un mien ami. Mais ne m'y entends guère en ce qui concerne les artisans ici. Saurais-tu me guider ?

--- Présent pour enfant ?

--- Nenni, il vient de s'installer en bel hostel. Quelque chose qui lui porte chance. »

Saïd plissa les yeux, s'envoyant une large rasade de vin pour améliorer sa réflexion. Il gratta sa barbe mitée et hocha la tête plusieurs fois pour lui-même. Ernaut allait lui demander ce à quoi il pensait lorsqu'il dressa un index impérieux avant de se lever. Puis il se dirigea vers son logis, en déverrouilla la porte et y disparut.

Habitué aux facéties du vieillard un peu dérangé, Ernaut ne s'en offusqua pas. Il se tourna vers le corniaud, désormais allongé entre soleil et ombre, et le flatta doucement. Un bruit de cavalcade résonna entre des murs non loin, des cris d'enfants se propagèrent et s'évanouirent. Il entendit plusieurs femmes entre lesquelles le ton monta bien vite sans qu'il soit possible de comprendre ce qui se disait, les voix se répercutant dans les rues et les courettes. Puis le calme revint, un léger vent fit ronfler et frémir sa hutte de feuillage. Il engloutit quelques beignets, dont il accorda la dîme au chien qui n'en espérait pas tant, et obtint en retour des coups de langue gluants.

C'est alors que Saïd ressortit la tête de son antre, les bras chargés d'un paquet de toile. Aussi excité qu'un enfant préparant une bêtise, il posa le tout devant Ernaut, avant de s'asseoir à son tour, et de plonger la main vers les pâtisseries.

« Qu'est-ce donc, l'ami ?

--- Cadeau pour ami toi. »

Tout en répondant, il poussa vers Ernaut le petit ballot. Celui-ci hésita un peu puis démaillota l'objet qui s'y cachait : une statuette de bronze, un personnage à l'allure étrange, corps humain et tête d'éléphant, en une pose gracile, une main levée en guise de salut, et l'autre tenant un genre d'encensoir.

« J'ai jamais rien vu de tel... Où donc as-tu trouvé ça ! Est-ce diablerie de mahométan ? »

Le vieil homme secoua la tête avec véhémence avant de stopper brutalement et de lever les épaules, indifférent.

« Bonne chance, venue par-delà les mers, loin ici, vers Levant. Pays des épices... Bon pour ton ami.

--- Tu veux dire que c'est un porte-bonheur ? »

Détaillant la statuette élégante, Ernaut ne savait que penser. Il trouvait l'objet plutôt joli, mais il ne voulait pas commettre un impair en offrant une idole païenne à un ami récent.

« Tu es acertainé que ça lui plaira ? Combien en veux-tu ? »

Saïd fronça les sourcils, ôtant de sa bouche le morceau de beignet qu'il était en train de déchiqueter, comme vexé par la question.

« Cadeau pour ami, pas vendre.

--- Je ne peux pas te prendre ça sans te payer, voyons. C'est mon ami, pas le tien.

--- Toi ami Saïd, donc moi donner cadeau toi. Et toi offrir à ami tien. Porter chance à tous, ainsi. »

Ernaut secoua la tête, ennuyé par la générosité inattendue du vieil homme. Il le savait fort désargenté, et la vente de la statuette pourrait lui permettre d'améliorer l'ordinaire. S'il avait d'autres trésors de ce type dans sa cabane, il n'était pas étonnant qu'il la verrouillât si consciencieusement chaque fois qu'il s'absentait.

Ernaut ne pouvait accepter aussi facilement de le déposséder, d'autant qu'il était lui-même assez aisé depuis qu'il avait rejoint la sergenterie du roi.

« J'accepte, mais en ce cas, il faudra m'en dire plus, et le faire autour d'un bon repas. Qu'en dis-tu ? »

Saïd hocha la tête et ricana, se tournant vers le chien pour le prendre à témoin :

« J'ai dit vrai, déjà la chance m'offre repas ! »

Ernaut s'esclaffa, désarçonné par l'aplomb du vieil homme. Celui-ci lui fit un clin d'œil et ajouta, tout en attrapant un autre beignet :

« Un jour, je conter toi toute l'histoire de cette statue. Puis il fit un large sourire : belle histoire ! »

Puis il goba la douceur sucrée dans un gloussement.

### Jérusalem, soirée du vendredi 27 juin 1158

Lorsqu'il était à son logis le soir, Droart aimait à aller voir ses enfants pour le coucher, surtout Guiote. Il avait une tendresse particulière pour son aînée, à qui il passait tous les caprices au grand dam de sa mère. Depuis qu'ils étaient dans la vaste demeure, les petits avaient leur propre chambre, avec un lit pour les garçons et un autre pour les filles. Ils y entassaient leurs jouets, leurs trésors dans des sacs et de vieilles malles récupérées.

Droart tenait à la main une statuette de bronze lorsqu'il s'assit au pied de la couche de Guiote et de ses sœurs, déjà endormies. Il la brandit sous le nez de l'enfant, en lui prêtant les borborygmes qu'il faisait.

« Te dirait-il d'avoir cette belle figure, ma douce ? »

La petite découvrait vraisemblablement l'objet pour la première fois, mais la perspective de la faire sienne lui conférait toutes les qualités. Elle s'exclama donc avec enthousiasme, les yeux emplis de joie :

« C'est ma préférée ! Je l'aime beaucoup.

--- Vois-tu, je l'aime beaucoup aussi, mais ta mère ne la porte guère en son cœur, alors il me plairait que tu la gardes par-devers toi.

--- Pour de vrai ? Tu me la donnes ?

--- Un jour, je te conterai sa belle histoire.

--- Une histoire merveilleuse, avec belles pucelles et fiers damoiseaux ? »

Droart acquiesça, un peu déchiré à l'idée de se défaire d'un cadeau auquel il tenait. Mais il ne serait guère loin et confié à quelqu'un de cher. Il ressentait tout de même un petit pincement au moment de l'abandonner, bien qu'il sache que c'était le seul moyen d'avoir la paix en son logis. Depuis qu'il l'avait reçu de la part d'Ernaut, sa femme n'avait eu de cesse de lui reprocher de posséder un tel objet impie. Elle était persuadée qu'il s'agissait d'une magie païenne, infidèle ou juive, et que cela attirerait le malheur sur leur maison de bons chrétiens.

« Par contre, c'est un secret entre nous deux, qu'il ne faut révéler à personne, d'accord ?

--- Ne pourrais-je le montrer à Sylvette et Houdart ?

--- Nenni, juste entre toi et moi.

--- Notre secret à nous ? »

Le sergent hocha la tête, attendri par la bouille innocente de son enfant. Elle lui fit un sourire en partie édenté et s'accrocha à lui avec chaleur. Il en conçut une bouffée de bonheur, émerveillé de voir qu'en effet, cette statue lui portait de nouveau chance.

### Colteja[^71], nuit du mercredi 5 juin 1387 {#coltejacotteughes-nuit-du-mercredi-5-juin-1387}

Les silhouettes à peine discernables dans la noirceur de la nuit se pelotonnaient contre la haie, à l'une des entrées du hameau. Personne n'avait de lanterne, de crainte d'être visible. On n'entendait que des chuchotements, des murmures inquiets, le gémissement d'un tout petit.

« Audeta, où donc se cache ton époux ? Grogna une voix agacée.

--- Il prenait nos dernières affaires, il s'en vient.

--- Il nous faut partir au plus vite, on dit l'Aymerigot à quelques portées d'arc. Il nous faut mettre du champ entre ses routiers et nous.

--- Je le sais bien, le Peyre » répliqua une voix autoritaire à l'arrière du groupe.

Celui qui avait répondu s'avançait, encombré de sacs et de ballots, un lourd bâton en main. On ne voyait que ses yeux dans les ténèbres et ils ne semblaient nullement effrayés comme ceux de ses compagnons. C'était un homme rude et pas craintif, il tenait à le faire savoir. Même, et surtout, si des soldats en maraude traînaient aux environs. Alban rejoignit sa femme et ses enfants d'un pas lent, puis murmura quelques directives d'une voix calme. Il hocha ensuite la tête à l'intention du meneur de leur petit groupe.

Ils prenaient le chemin de la forêt pour se mettre à l'abri des sanglantes troupes de routiers qui écumaient la région depuis des années. À quelques semaines des moissons, il s'avérait plus prudent de quitter sa maison, dans l'espoir que cela découragerait le pillage. Le grain serait bientôt là et c'était le plus important. Une bâtisse pouvait se réparer, mais la récolte, elle, devrait attendre une nouvelle année si on la perdait par un comportement inconsidéré. Vu qu'il était impossible de savoir comment les choses pouvaient se dérouler avec ces troupes de brigands sans foi ni loi, le mieux était de fuir. Seuls quelques-uns auraient préféré demeurer sur place et tenter de résister. Folie, estimaient les autres.

Alban était de ces réfractaires. Il avait trop sué sur son lopin et sa maison pour accepter aisément de s'en déposséder ainsi. Son logis était confortable, pour n'être pas si grand, et il était à lui. Sa famille était riche et respectée, sa voix écoutée au conseil. Devoir se cacher comme un lapin terrifié lui remuait les sangs. Il se sentait amputé de sa fierté d'homme. Mais il avait reconnu qu'il n'était pas de taille, que si même les châteaux n'arrivaient pas à résister, alors qu'auraient pu tenter de simples vilains comme lui ?

Ils en étaient là, jetés sur les chemins comme des va-nu-pieds, leurs maigres biens serrés sur eux. Alban avait pris soin de cacher leurs objets de valeur avant de s'enfuir, afin de retourner les chercher une fois que les choses se seraient tassées. Ou pour les récupérer quand ils se réinstalleraient. Audeta insistait pour qu'ils descendent plus bas, vers le Limousin, où les températures se faisaient plus clémentes. C'était là pays de peste et d'Anglois avait tempêté Alban, mais il avait fini par reconnaître que les terres y paraissaient moins pénibles.

Tout en marchant, il saisit son aîné Géraud par l'épaule et l'approcha de lui d'une main ferme :

« Gamin, tu vas bien m'écouter. Je vais te dire où j'ai celé nos avoirs en l'hostel, au cas où il m'arriverait malheur. Si c'était le cas, ce serait ton rôle de me remplacer. »

L'adolescent, habitué à son père autoritaire, hocha la tête avec sérieux, sans rien répondre.

« J'ai quelques monnaies dans le grain que je porte, mais une autre part est enterrée au pied du houx à senestre de la grange. »

Alban hésita un instant puis se racla la gorge.

« Y'a aussi autre chose : la statuette qui nous vient de nos ancêtres de par delà les mers. Y faut pas trop en parler, car le curé y verrait visage de démon. La grand-mère, elle a bien failli mal finir à cause de ça. Mais faut en prendre soin, c'est notre chance qu'y disaient les vieux. Je l'ai scellée en le logis. »

Il sourit pour lui-même de son mémento et murmura :

« Parmi les bêtes, le démon se terre à l'ombre de la croix.» ❧

### Notes

L'idée derrière ce récit, qui rejoint en partie celle tramant *Fil du temps*, est le destin des objets qui nous entourent et à partir desquels les archéologues tentent de reconstituer les sociétés disparues. Le titre, lointaine référence à Terry Pratchett, dévoile une autre des questions sous-jacentes, à savoir justement l'importance de l'univers symbolique qui s'attache à eux au sein d'un groupe, qui n'est certainement pas constant ni dans le temps ni dans l'espace. Sur ce sujet, si vous avez la chance de voir passer près de chez vous l'exposition *Futur antérieur : Trésors archéologiques du XXIe siècle après J.-C.*, n'hésitez pas à aller la visiter, elle offre un point de vue saisissant sur ces processus.

Il n'est pas une semaine sans révélation soi-disant exceptionnelle de découverte archéologique d'artefacts ayant voyagé sur des milliers de kilomètres, mis au jour en des lieux où ils sont clairement hétérogènes. Mais on connaît depuis l'époque préhistorique le destin de pierres retrouvées très loin de leur site d'extraction. Cela ne veut pas forcément dire qu'un être humain unique en ait assuré le transport, ni que les deux cultures aient été en contact direct (ni envisagé l'existence de l'autre). De plus, même si l'on dispose d'une information archéologique précise, nous ne pouvons généralement en tirer aucun enseignement quant à l'intrusion de l'objet culturel externe, qui demeure anecdotique. Voilà donc un terrain propice à un exercice d'interprétation romanesque, en dehors de toute considération scientifique.

Enfin, pour la petite histoire, ce Qit'a a servi de prétexte à un jeu de piste qui menait aux vestiges du village médiéval abandonné de [Cotteughes](http://www.auvergne-tourisme.info/patrimoine-culturel/trizac/site-archeologique-des-cases-cotteughes/tourisme-PCUAUV0150000406-1.html) dans le Cantal, où vous pouvez peut-être retrouver certaines traces du récit...

### Références

Boas Adrian J., *Domestic Settings. Sources on Domestic Architecture and Day-to-Day Activities in the Crusader States*, Leiden et Boston : Brill, 2010.

Combes Pascal, Piludu Brigitte, Vernet Gérard, *Résultats des sondages archéologiques dans les villages de Cotteughes et de Freydefont*, DRAC Auvergne, 1990.

Flutsch Laurent, *Futur antérieur : Trésors archéologiques du XXIe siècle après J.-C.*, InFolio, 2003.

Oultre Jourdain
---------------

### Jérusalem, palais royal, après-midi du mercredi 26 février 1158

Le vicomte Arnulf, emmitouflé sous une couverture chamarrée qui traînait jusqu'au sol, dégustait quelques fruits au vin. Il était malade depuis plusieurs jours, victime de fièvres et de toux, mais refusait de garder le lit au grand dam de ses proches. Il était donc installé à sa table de travail, dans une des salles du palais, qu'il affectionnait tout particulièrement en hiver, car elle ouvrait sur une colonnade et des jardins. Plusieurs braseros apportaient suffisamment de chaleur pour qu'une poignée de clercs de l'administration trouvent prétexte à venir là copier, classer ou vérifier des chartes sans avoir à se frotter les doigts.

Sur la pile de documents qu'il venait de compulser trônait une petite missive, hâtivement rédigée d'une main brouillonne, mais portant un sceau qu'il reconnaissait entre tous. Ce n'était pas l'imposante bulle de presque deux pouces de diamètre que chacun attribuait au souverain sans l'ombre d'un doute, mais celle qui accompagnait les messages sans importance, aux simples fins d'identification. Le roi Baudoin indiquait que la campagne dans le Nord s'était fort bien déroulée, qu'il faisait halte à Tripoli quelques jours et qu'il fallait prochainement attendre son retour triomphal, en compagnie de Thierry comte de Flandre.

Arnulf avala encore plusieurs bouchées en silence, tout en étudiant le sergent assis face à lui. Gaston vérifiait un inventaire qu'il venait de réaliser dans plusieurs arsenaux épars dans la cité. Fatigué par les combats, boiteux et parfois revêche, tout son corps accusait une vie de violence au service de la couronne. Le vicomte lui faisait confiance pour estimer la valeur des hommes. Capable de constituer une bande compétente pour n'importe quelle tâche en peu de temps, il avait un jugement sûr et se voyait régulièrement en charge de la formation des fantassins. Par égard pour son âge et sa claudication, on lui épargnait les campagnes hivernales. Connaissant la susceptibilité de Gaston, Arnulf lui avait donc demandé cet inventaire, tâche perpétuellement nécessaire, afin de le garder au chaud.

Outre cela, il avait envie de savoir ce que valaient les dernières recrues et si certaines pouvaient servir à autre chose qu'effrayer le larron ou le bourgeois peu désireux de payer la taxe. Il fallait régulièrement renouveler les unités de fantassins et on sélectionnait alors parmi la sergenterie royale ceux qui semblaient les plus aptes. Cela offrait l'avantage de s'appuyer sur des hommes qu'on pouvait former toute l'année et dont on connaissait les forces et les faiblesses.

Un des nouveaux arrivants, qui n'avait pas juré depuis plus d'un an, attirait l'intérêt du vicomte. Ernaut, dont la réputation à la porte de David inquiétait jusqu'aux marchands de Beyrouth, s'était révélé être bien plus qu'un géant au physique de colosse. Restait à savoir si cela lui serait bénéfique pour servir le royaume.

S'essuyant les doigts dans une touaille, Arnulf s'éclaircit la gorge pour interrompre Gaston dans son énumération.

« Laisse donc ces listes pour le moment et conte-moi ce qu'il en est de la formation des piétons...

--- Que voulez-vous assavoir, sire vicomte ? Il n'y a rien de particulier à en dire.

--- As-tu fini d'apprendre les ordres au dernier contingent ?

--- Oui-da. Il ne serait pas inutile de leur faire subir encore assaut par conrois[^72], histoire de les aguerrir. Mais ils savent les ordres, les cris et l'usage des bannières. »

Arnulf réprima un frisson et remonta la couverture sur ses épaules.

« As-tu eu l'heur d'étudier le jeune Goliath, fort récemment ?

--- Ernaut ? confirma Gaston dans un sourire. Certes, nous avons compaigné jusqu'à Jaffa peu après la Sainte-Catherine[^73]. Un jeune poulain susceptible de finir beau destrier. Pour peu qu'on lui tienne les rênes courtes.

--- A-t-il posé souci ?

--- Il s'emporte parfois un peu vite et n'apprécie rien tant que montrer sa force colossale à quiconque. Lui faire tenir le rang est souventes fois ardu. »

Le vicomte acquiesça, se remémorant qu'il avait pu constater lui-même l'indépendance d'esprit parfois pénible du jeune homme. Ce n'était malgré tout pas pour lui déplaire, tant qu'elle ne s'opposait pas à son autorité personnelle.

« À quel poste l'as-tu formé ?

--- Il sert comme pavesier.

--- Tu lui attribueras place en senestre de la ligne. Avec semblable donjon en la forteresse, la muraille tiendra bon.

--- Il faudra s'assurer que son arrivée en si belle place n'occasionnera pas de heurts. Ce n'est pas l'usage d'avoir telle préséance quand on n'est pas vétéran. »

La remarque de Gaston arracha au vicomte un grommellement sans enthousiasme. Le sergent appréciait ce chevalier, mais il regrettait son désintérêt envers les sentiments des humbles. Pourtant Arnulf passait beaucoup de temps en leur compagnie pour ses différentes tâches et il ne détestait rien tant que l'inefficacité, ayant pu en constater les résultats désastreux à de nombreuses reprises. Las, cela ne l'avait jamais incité à voir parmi ces serviteurs de la couronne plus que des pions qu'il affectait au mieux de ses besoins.

« Tu n'auras qu'à le prendre dans ton conroi[^74] et surveiller cela.

--- Le roi compte chevaucher sus l'ennemi si tôt rentré ? risqua Gaston.

--- Je ne sais. De toute façon, vu que Norredin[^75] est malade et que son ost a été défait, il y aura sûrement rapide bataille pour profiter de notre avantage. Bonne occasion pour éprouver au feu ces nouvelles lames. »

Gaston hocha la tête en silence. Le jeune roi n'aimait rien tant que chevaucher à la tête de son ost. Il avait déjà à son actif de nombreuses victoires et comptait souvent sur ses succès pour financer les besoins du trésor. L'été précédent, il n'avait échappé que de peu à la capture ou à la mort[^76], mais cela n'avait en rien tiédi ses ardeurs. Encouragé par son beau-frère Thierry de Flandre, il passait plus de temps en selle la lance à la main que dans son palais.

« Je vais m'occuper d'Ernaut. Il sera fin prêt pour la prochaine campagne, soyez-en assuré. »

Arnulf opina en silence et avala un nouveau fruit, se pourléchant de l'excellente friandise. Il lui faudrait voir comment s'attacher le jeune Ernaut dans sa maisonnée. C'était la valeur de ses hommes qui donnait la puissance à un hôtel. Arnulf espérait bien se servir de cette belle pierre pour y bâtir dessus.

### Terre de Suète, plaine de Buṭayḥa, début d'après-midi du mardi 8 juillet 1158

Appuyée à un palmier, l'unité d'Ernaut avait pris place parmi les broussailles peu denses qui s'étiraient en une haie sauvage jusqu'au milieu de la plaine à leur gauche. Ils s'étaient déployés en fin de matinée depuis l'arrière-garde du corps central d'armée, vers l'aile droite. Derrière eux, une échelle de cavaliers menée par le prince de Galilée attendait à l'abri de la forteresse mouvante des boucliers. Loin en retrait, les mules et animaux de portage étaient égayés en direction du lac de Tibériade, d'où ils puisaient la boisson distribuée par les fourrageurs.

La chaleur était torride et les langues remuaient de la poussière. En dehors des berges, la végétation abondante était brûlée de soleil. Ernaut souleva son casque de fer, qu'il protégeait d'un chapeau de paille, pour y passer un linge déjà détrempé de sueur. Il ne voyait rien s'approcher depuis les lignes ennemies qui semblaient toutes affairées bien plus au nord. Régulièrement des coursiers apportaient des nouvelles aux bannerets et on avançait de quelques pas, parfois d'une portée d'arc. Sans pour autant arriver au contact qu'espérait Ernaut. Il avait hâte de prouver sa valeur, certain de sortir renforcé de la confrontation avec l'adversaire.

Il souffla longuement, plissant les yeux pour tenter de comprendre ce qui se déroulait malgré les ondes de chaleur. Il ne voyait que des formes indistinctes au niveau des combats et ne percevait que de diffuses rumeurs, selon le vent. L'arbalétrier à ses côtés lui tendit l'outre emplie d'eau tiède qu'ils se repassaient depuis le matin.

« On a bonne position. C'est l'ost de Flandre à senestre qui encaisse les traits sarrasinois.

--- Ils ne s'approchent donc jamais, frapper de la lance et du glaive ?

--- Pas tant qu'ils peuvent... »

Il fut interrompu par une sonnerie de cor. Ils allaient faire mouvement sous peu. Ernaut lança la gourde à un valet qui passait par là et enfila la guiche et les énarmes de son grand bouclier. Enfin, il empoigna l'hast[^77] de sa lance, qu'il avait plantée en terre. La tension s'insinua dans les corps, crispant les mâchoires, serrant les poings et fronçant les sourcils. Gaston, à cheval, avait rivé son regard à la bannière de Galilée, prêt à réagir au moindre de ses frémissements. Lorsqu'il la vit s'incliner légèrement à plusieurs reprises, il hurla « Mouvez ! »

En un instant, les pavois se soulevèrent de terre et la ligne ondoya subitement, le temps pour elle de déferler comme une lente vague sur la plaine. Les fûts des lances semblaient une forêt aux terminaisons de métal brillant sous le soleil et, portées au rythme du pas des hommes, ils dansaient sous un vent imaginaire. Ça et là, les étoffes peintes aux couleurs des seigneurs de guerre s'élançaient vers les cieux, marquant l'emplacement des cavaliers prêts à s'avancer vers l'ennemi.

Ils n'avaient pas fait un demi-mille qu'un coursier se présenta au grand galop. Il hurla en passant devant eux « Halte ! Serrez ! » sans même ralentir tandis qu'il s'engouffrait entre deux unités. Il piqua directement vers le prince.

Décontenancé, Ernaut tourna la tête vers Gaston. Il savait que le groupe se fiait à lui pour les mouvements, qu'il était le pilier de référence. Et là il hésitait sur ce qu'il devait faire. Gaston le comprit en un instant et cria « Mouvez ! La bannière est toujours inclinée ! Mouvez ! » Malgré tout, en disant cela, il fit volter sa monture de façon à s'assurer de réagir rapidement à toute nouvelle indication et fixa la bannière. Peu après l'arrivée du cavalier, elle se redressa ostensiblement et tandis que les olifants propageaient un autre ordre, des voix s'élevèrent d'un peu partout. « Halte ! Serrez ! »

Sans même y réfléchir, Ernaut se rapprocha du groupe à sa gauche jusqu'à le rejoindre. Puis il se figea et planta sa pique en terre tandis que son voisin venait se poster au plus près de lui. La ligne se tassa comme un soufflet que l'on aurait replié, offrant une muraille ininterrompue d'écus hérissés de lances. En retrait, les arbalétriers avaient tendu les arcs, sans pour autant encocher. Ils attendaient.

L'échelle[^78] de cavalier semblait tranquille, mais les conrois étaient formés, les heaumes étaient lacés et, autour des montures, les valets et serviteurs s'activaient à équiper les retardataires. Le sol commençait à gronder du piaffement des étalons de guerre impatients de s'élancer, tenus ferme par des poignes d'acier. Ernaut inspira longuement. Il était prêt.

### Terre de Suète, abords du Yarmūk près des caves de Suète, soirée du mercredi 9 juillet 1158

Un campement sommaire s'était établi au débouché de la courte vallée fermée par la forteresse des Caves de Suète. Ernaut avait été ébahi de découvrir que ce n'était pas un édifice dû à l'homme, mais l'aménagement de la falaise terminale. Des grottes étaient agencées avec des passerelles et des encorbellements à flanc de la montagne. De là-haut devait s'offrir une vue imprenable sur les environs, vers le nord par-delà le Yarmūk et les terres contrôlées par Damas.

Le roi avait établi sa tente au plus près de la fortification, avec les principaux nobles et la cavalerie tandis que les fantassins avaient été installés de façon à protéger les abords. Bien qu'ils aient remporté une victoire la veille face à Nūr ad-Dīn, le Turc avait pu battre en retraite et conservait une capacité militaire conséquente. La leçon de l'année précédente avait été entendue par le jeune souverain de Jérusalem et des sentinelles égayées le long des accès garantissaient la sécurité. On disait même que des coursiers allaient faire venir de quoi renforcer la position des Caves de Suète avant que le roi ne reparte.

Le principal affluent du Jourdain en était réduit à serpenter entre les pierres et les racines, lointain souvenir du torrent tumultueux du printemps. L'eau vive était néanmoins fort appréciée des hommes, dont un grand nombre avait pataugé dès que cela leur avait été possible. Ernaut avait été parmi les premiers à s'ébrouer, profitant de l'occasion pour rincer ses vêtements poisseux de sueur. Il s'était après cela installé contre un rocher tiédi par le soleil de la journée, exposant ses linges pour qu'ils sèchent. Portant simplement ses braies, mais avec son imposant coutelas à la ceinture, il était ensuite allé aider à la confection du repas, un gruau hâtivement chauffé et agrémenté de dattes. Il était accompagné d'un vin léger apporté dans de larges outres et qui sentait donc un peu la chèvre.

Regroupés par affinité dans la zone enclose de cordes qui délimitait le camp, les hommes prenaient du bon temps. Quelques chants résonnaient, pas toujours bien élégants, et des lavandières parcouraient les rangs, proposant leurs talents pour s'occuper des corps et des vêtements. Ernaut regardait au loin un spectacle de jongleurs assez vulgaire lorsqu'il entendit l'arrivée d'un cheval. C'était Gaston qui revenait des Caves, où il avait certainement reçu les ordres. Il confia les rênes à un valet et s'approcha du petit groupe où était Ernaut.

« Il me faudra cinq hommes demain à l'aube.

--- Quelque bataille en prévision ? s'enthousiasma le jeune homme.

--- Certes pas. Le conseil des barons reçoit en ce moment même des coursiers venus de chez Norredin. M'est avis qu'on ne les reverra pas de sitôt. »

Apercevant la mine déconfite du jeune homme, Gaston s'approcha.

« Hé bien quoi, jeune. N'es-tu pas heureux d'avoir survécu à bataille, et sans perdre aucun morceau !

--- C'est juste que j'ai trouvé ça... »

Ernaut ne trouvait pas ses mots. Gaston s'accroupit à côté de lui et lui donna une bourrade.

« Aussi long que jour sans pain ? Je m'en doute bien. Mais c'est ça guerroyer, jeune. On attend la journée durant, sous soleil d'enfer. Et d'un coup, ça nous tombe dessus, comme un orage. Le plus souvent, ça se contente de péter au loin, comme le tonnerre.

--- Toutes ces journées à porter lance et écu, avancer, reculer... C'est quoi l'intérêt ? On aurait pu aussi bien rouler les dés allongés dans le pré. »

Gaston se gratta l'oreille, comme si ce qu'il entendait irritait sa cervelle.

« Ça, on ne peut jamais l'assavoir avant la fin de la journée. Tu t'ensouviens hier, quand on faisait avance et que le cavalier est venu nous arrêter ?

--- Certes oui. J'espérais qu'on irait à curée.

--- Ça aurait pu. Le souci, c'est que ces maudits Turcs aiment à feindre la fuite pour nous prendre dans leur nasse. Les barons ont cru à une perfidie de ce type.

--- Sauf qu'ils fuyaient vraiment et qu'on aurait pu faire belle moisson. Peut-être même grapper Norredin.

--- Et tu te voyais déjà l'assaillir pour le porter toi-même, ficelé comme conil, au sire Baudoin ! » se gaussa le vieil homme.

Il soupira longuement et se laissa tomber au sol. Il chercha de quoi se désaltérer, mais fit une grimace en apercevant l'outre de vin. Il haussa les épaules et sourit à Ernaut.

« Tu as bien œuvré ce jourd'hui, jeune.

--- J'ai rien fait, à part porter pavois et haste, avancer, tourner, reculer...

--- Toutes choses pour lesquelles on espérait de toi. Apprends donc que tu n'es pas plus Roland que moi. Adoncques, servir à ta place sans gloriole te sera fort plus utile.

--- J'espérais certes bien plus de cette levée d'ost. »

Gaston se releva péniblement, époussetant les brindilles de sa cotte.

« T'es pas si vieux. Agis ainsi que tu l'as fait ce jour, et pense à fermer un peu ta grand'goule. T'en seras pas déçu. »

Il assortit sa dernière déclaration d'un clin d'oeil et repartit en boitillant, en quête d'un vin qui siérait à son gosier. Il laissait Ernaut en plein désarroi. Le jeune homme avait été euphorique lorsqu'il avait appris qu'il allait intégrer l'ost royal pour cette campagne. Il l'avait clamé sur tous les toits et annoncé à toutes ses connaissances, même lointaines. Il avait même escompté du butin pour faire quelques emplettes afin d'impressionner Libourc et sa future belle-famille.

Revenir sans haut fait à conter lui broyait le cœur. Il lui fallait trouver un moyen de se débarrasser de ce poids. Comprenant qu'il ne servirait à rien de ruminer seul dans son coin, il se leva soudainement et s'avança parmi ses camarades.

« Quelque courageux pour oser m'affronter à la lutte ? »

En entendant la voix d'Ernaut lancer son défi, Gaston sourit pour lui-même. Le vicomte serait heureux d'apprendre que les obstacles ne minaient en rien l'énergie du jeune homme. ❧

### Notes

L'idée de ce texte a été nourrie par l'envie que j'avais depuis longtemps de présenter le monde militaire médiéval d'un point de vue assez rare, celui de l'infanterie. Loin de n'être que des bagarres sans cohérence, les affrontements demandaient au contraire une solide organisation, car les contingences matérielles et logistiques étaient assez fortes. Les batailles consistaient avant tout en des déploiements et déplacements réciproques de troupes. Le tout avec des moyens de communication assez simples : bannières, instruments de musique, quelques cris et ordres bien précis. Si dans les textes, les combats pouvaient se prolonger des heures, cela désignait la durée totale de présence sur la zone. Les engagements à proprement parler étaient souvent intenses, mais pas forcément longs, et étaient plutôt composés de flux et de reflux épisodiques.

La bataille de Buṭayḥa constitue l'un de ces nombreux affrontements mineurs entre les puissances latines et musulmanes qui n'aboutit à aucun résultat stratégique. Chacun des chroniqueurs loua les succès de son camp tout en minimisant ceux de ses adversaires. Nūr ad-Dīn rentra à Damas et repartit dans le nord où Renaud d'Antioche opérait des razzias. Baudoin put libérer la place forte des Caves de Suète du siège dont elle était l'objet et profita de sa présence pour en améliorer les défenses. Les deux souverains envisagèrent un moment de signer une trêve sans que cela ne débouche sur rien.

Par ailleurs, le titre de cette nouvelle a aussi un sens particulier pour l'auteur, car il s'agit du premier texte publié dès l'origine en licence libre. De la même façon qu'Ernaut, il me semble que c'est là un passage de frontière, afin de fouler des terres inconnues. Dans les deux cas se mêlent l'impatience d'y déployer un savoir-faire et le désir d'y voir fructifier des rêves.

### Références

Audoin Édouard, *Essai sur l'armée royale au temps de Philippe Auguste*, Librairie Ancienne H. Champion, Paris : 1913.

Élisséef Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Gibb H. A. R., *The Damascus Chronicle of the Crusades. Extracted and translated from the chronicle of Ibn al-Qalanisi*, Londres : Luzac & Co., 1932, réédition : Mineola : Dover Publication, 2002.

Nicolle David, « ̔̔̔'Ain al-Habis. The Cave de Sueth » dans *Archéologie Médiévale*, XVIII, Rouen : 1988. P;113-140.

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Verbruggen Jan Frans, *The art of warfare in Western Europe during the Middle Ages, from the eighth century to 1340*, The Boydell Press, Amsterdam, New York, Oxford :1977 (2nde édition).

Dette d'honneur
---------------

### Tyr, soirée du vendredi 18 juillet 1158

En cette chaude veillée estivale, la population demeurait dans les rues, bavardant à l'ombre des palmiers dans les cours et les places ombragées, assise sur les pierres gorgées du soleil de la journée. Une légère brise de mer apportait des senteurs iodées et salines, repoussant les miasmes de l'activité humaine vers les terres. Esquivant les badauds flâneurs, un jeune garçon avançait d'un pas rapide, la tête basse et le front soucieux. Tout en marchant, il se répétait à lui-même quelques phrases de réconfort, sans pour autant voir son humeur s'améliorer.

Un peu replet, il suait abondamment du fait de son manque d'exercice. Sa manche était trempée de sueur à force de la passer sur son visage, mais il continuait de se frotter régulièrement en un réflexe devenu inutile. Lorsqu'il arriva enfin à destination, il s'efforça de remettre un peu d'ordre dans sa tenue. Il tira sur sa cotte, ajusta sa ceinture et glissa ses lourdes boucles noires derrière ses oreilles, bloquant la touffe emmêlée sous le dôme de feutre lui servant de coiffe. Puis il frappa à plusieurs reprises.

Il fut rapidement invité à entrer dans un vestibule où la fraîcheur s'était maintenue au long de la journée. Un long banc courait sous une niche soulignée de mosaïque, abritant plusieurs céramiques au décor bariolé. La domestique l'incita à s'y asseoir le temps pour elle de prévenir le maître de maison. La porte entrouverte au fond du couloir filtrait à peine les bruits d'une fontaine et d'enfants occupés à jouer ainsi que l'agitation normale d'une riche demeure en début de soirée : servants préparant le repas ou l'arrivée de la nuit et voix de convives.

Ya'qoub Gibran poussa l'huis avec vigueur, comme à son habitude. Tête nue, vêtu d'un simple thawb[^79] de lin égyptien de qualité, il était visiblement en famille. Néanmoins rien en lui ne trahissait d'agacement ou d'inquiétude. Il rivait ses yeux de charbon sur son hôte, lissant les frisottis de sa barbe soignée. Sa voix de basse résonna dans la pièce tandis que le jeune homme s'inclinait en guise de salut, tordant son chapeau en un geste nerveux.

« À te voir ainsi échevelé, je crains mauvaise nouvelle. Comment est-elle ?

--- La toux ne fait que se renforcer et elle n'a rien quasi rien avalé en mon absence.

--- Je craignais cela. Il aurait fallu une garde-malade !

--- Elle recrache le moindre aliment. Elle souffre bien trop pour manger. »

Le médecin hocha la tête. Il avait craint que le traitement n'arrive que trop tard. La pleurésie était une maladie complexe, difficile à soigner quand elle était bien installée. Surtout dans le corps d'une vieille femme.

« Je ne vois que l'opium qui pourrait la soulager. Ou de la mandragore.

--- Si vous me donnez la prescription, j'irai aussitôt m'enquérir de ces drogues. »

Ya'qoub Gibran hocha la tête sans un mot. Il ne doutait pas que ces médicaments ne feraient que soulager la mère du garçon sans la guérir pour autant. Et cela à un coût assez élevé. Sachant la famille nécessiteuse, il avait fait des efforts dans ses honoraires, mais il n'était pas certain que l'apothicaire aurait les mêmes scrupules. Les hommes de commerce obéissaient à d'autres lois que les siennes.

### Tyr, boutique de al Faytrouni, midi du lundi 21 juillet 1158

La lourde chaleur des fours sourdait jusque dans l'échoppe qui ouvrait sur la rue par une large baie. Comme beaucoup de boutiques de verrier, l'atelier de Khalil al Faytrouni s'organisait autour d'une étroite cour ombragée. On y accédait par une venelle serpentant depuis les abords de la forteresse, non loin du quartier vénitien et du port. Les artisans tyriens, réputés par-delà les frontières du royaume, produisaient aussi bien du verre plat pour la construction que des objets de service délicatement ouvragés. Al Faytrouni était spécialisé dans les gobelets et les lampes et s'enorgueillissait de descendre d'un artiste qui avait travaillé pour des califes. Malgré tout, au fil du temps, la renommée de la maison avait baissé et la clientèle était désormais moins prestigieuse.

Assis sur une banquette parmi les pièces colorées et alambiquées, Khalil al Faytrouni versait un peu de leben[^80] dans deux gobelets aux formes simples qu'il affectionnait tout particulièrement. Il n'était pas mécontent de laisser sa canne de souffleur un petit moment, harassé par la chaleur partout présente. Le corps émacié, il ne portait qu'une chemise de toile fine et un sirwal de coton d'apparence douteuse. Il tendit la boisson au garçon installé sur un pliant face à lui puis avala à lentes gorgées, dégustant la fraîcheur autant que les arômes.

Pendant ce temps, son invité faisait grincer son siège, trépignant de devoir patienter en silence. Peu désireux de tenir sur le gril ce gamin qu'il ne connaissait guère, Khalil décida de mettre un terme à ses angoisses.

« Tu as bien fait de venir me voir. Tout le monde dans la famille ne t'aurait pas ainsi accueilli. »

Il se gratta l'oreille, accorda un maigre sourire dévoilant des dents gâtées.

« Rafqa, ta mère a fait ce qu'elle a fait, ce n'est pas à moi de la punir pour ça. On s'entendait bien, enfants. C'est en mémoire de ça que je te prêterai ce dont tu as besoin. »

Le jeune homme soupira de soulagement en entendant cela. Khalil enchaîna sans lui laisser l'opportunité de prendre la parole.

« Il faut que tu comprennes, Sharbel, que je suis pas riche pour autant et qu'il te faudra me rembourser selon les termes. Ces trois dinars, je vais les prendre dans la caisse qui me sert à acheter des matériaux pour l'atelier. Et tu mes les rendras, au rythme de un besant par semaine, durant sept semaines, après les seize deniers du premier versement. »

En entendant comment on l'appelait, l'adolescent comprit qu'on ne lui faisait pas de cadeau. Il ne s'était jamais senti proche de cette famille dont sa mère s'était coupée en vivant avec un Latin. D'ailleurs ce dernier les avait quittés depuis longtemps et n'avait laissé à son fils qu'un prénom, auquel le jeune s'attachait comme à une relique et qu'il avait fait sien : Germain.

Incapable de trouver de quoi payer les soins par ses propres moyens, il n'avait pas eu d'autre choix que de se tourner vers ces cousins qu'il n'avait croisé que de loin. Khalil était le seul qui ne leur fermait pas sa porte, mais il n'oubliait pas pour autant les égarements passés.

Germain prit un peu de boisson, afin de réfléchir et de se donner une contenance. Il n'avait plus beaucoup d'économies et avait déjà beaucoup dépensé pour les traitements. Il avait peut-être quelques possessions dont il pourrait se défaire auprès du fripier. Mais un besant par semaine, c'était bien plus qu'il ne gagnait. De toute façon, il lui fallait avant tout du temps. Cet argent paierait l'apothicaire qui ne voulait plus lui faire crédit. Il saurait bien trouver un moyen de rembourser.

« Je te mercie, cousin Khalil. C'est très généreux de ta part. Prépare donc le contrat et dis-moi quand repasser le signer »

Khalil acquiesça en vidant son verre. Il se sentait un peu honteux de demander des intérêts à ce gamin esseulé, mais il avait aussi des comptes à rendre. Son épouse était une femme intraitable et sa belle-famille, qui l'avait soutenu pour relever son maigre négoce, lui reprocherait longtemps de venir en aide des proscrits. En faisant payer son service, il pourrait toujours prétendre avoir saisi une opportunité de gagner quelques monnaies. Il regarda un instant ses mains tavelées et crevassées, aux ongles noirs et épais. Il était temps pour lui de retourner aux fours.

### Tyr, atelier de Fromont le braalier, matin du lundi 29 septembre 1158

Brandissant une tablette de cire, Fromont tempêtait, postillonnant tout en agitant les bras comme un dément. Il faisait cela pour la pure forme, incapable qu'il était de mettre la moindre menace à exécution. Il accordait autant de délais de paiement que de prêts et en était réduit à entasser les promesses non tenues dans une cassette qu'il regardait chaque jour avec dépit.

Sa tignasse brune emmêlée voltigeait à l'unisson avec une barbe où surnageaient quelques reliefs de soupe, donnant un aspect comique au petit bonhomme ventripotent. Malgré cela, Germain envisageait chaque lundi matin avec angoisse, sachant par avance qu'il n'aurait pas de quoi honorer son échéance. Il apportait tout ce qu'il avait, soit dix deniers par semaine, mais cela ne suffisait pas et il accumulait les retards.

L'artisan finit par se calmer et se laissa tomber sur un escabeau devant la table à tréteaux où son ouvrage était disposé. Il tourna la tête de droite et de gauche, comme s'il espérait faire jaillir la somme exigée de quelque recoin obscur de la pièce. Puis il souffla avec emphase.

« Je suis bien désolé pour toi, gamin, mais comprends que je ne peux accepter cela ! Je t'ai donné ma fiance, car je savais ta mère femme de bien. Jamais je n'ai eu la moindre remarque à faire à son ouvrage, toutes ces années. Et je m'entriste de ce qu'elle est passée voilà peu. »

Il marqua une pause, examinant le jeune homme planté devant lui, les yeux baissés, ne sachant que faire de ses mains.

« Tu savais fort bien que tu ne paierais pas quand j'ai accepté de te verser de quoi rembourser ce que tu devais. J'ai l'air de quoi, moi ? Si même les gamins peuvent se jouer de moi, autant bouter le feu à mon échoppe et danser sur les cendres fumantes ! »

Fromont ne savait pas s'il plaignait Germain ou s'il le trouvait insupportable. Il restait chaque fois là, à pleurnicher, l'œil larmoyant. Il disait qu'il ne pouvait payer plus, qu'il avait déjà vendu tout ce qu'il avait et que dix sous représentaient l'intégralité de ses gages au service de l'archevêque de Tyr. C'était cette dernière mention qui agaçait le plus Fromont.

« Et ne va pas croire que tu es à l'abri d'un jugement parce que tu es au service du sire archevêque Pierre. J'en sais assez sur toi pour fort le mécontenter... »

La remarque fit mouche, car Germain releva le menton et s'empourpra immédiatement tandis qu'il répondait d'une voix vive.

« Nul ne peut se plaindre de mon service en l'hostel de l'archevêque...

--- Ai-je prétendu le contraire ? Mais sait-il seulement que tu n'es pas plus baptisé que le Soudan d'Égypte[^81] ?

--- Comment ça ? bredouilla le jeune, décontenancé.

--- Je sais fort bien que nul parrain ne t'a porté sur les fonts pour entrer en l'Église. Peut-être serait-il fort marri de mieux te connaître... Sharbel ! »

Germain encaissa la menace avec effroi. Il se doutait bien que son secret n'en était guère un et que l'histoire de sa mère était connue dans le quartier, mais l'entendre dire ainsi le fit paniquer. Au palais, il était toujours passé pour un Latin, ce qu'il n'avait jamais démenti. Il resta là, tétanisé, le regard dans le vide et les entrailles prêtes à lâcher. Fromont s'en rendit compte et, un peu inquiet, rapprocha la main insensiblement des grands ciseaux posés sur la pile de drap.

« Bon, je ne veux pas non plus te créer d'ennui, gamin. Alors je te le dis tout net : si jamais la semaine prochaine, tu ne m'apportes pas mon dû, je vends les bijoux que j'ai en gages. »

La mise en garde sembla réveiller Germain, qui répliqua d'une voix éraillée par l'émotion.

« C'est tout ce qui me reste de ma mère !

--- J'en suis désolé, jeune, mais tu ne me laisses pas le choix. Tu t'en reviens avec le sou hebdomadaire, plus les six deniers de retard, sinon c'en est dit de ces bijoux. »

Germain haussa les épaules. Il était anéanti. Sa mère à peine mise en terre, il n'avait eu de cesse de payer ce qu'il devait à ses créanciers. Il n'avait plus rien en dehors de la tenue qu'il portait. Une fois les bijoux disparus, il demeurerait absolument seul, sans aucun lien avec personne. Il hocha la tête tristement et fit volte-face, retournant dans la rue d'un pas las.

Fromont expira lourdement. Ces séances le fatiguaient de plus en plus avec les années. Il avait pensé que commercer la monnaie serait moins pénible que tirer l'aiguille pour fabriquer des sous-vêtements. Il se souvenait des sommes colossales qu'il croyait pouvoir obtenir de ces opérations. Au final, il découvrait que les pièces partaient plus vite qu'elles ne revenaient et il se laissait trop facilement influencer par les misères invoquées par ses débiteurs. Il fallait que cela change. Que deviendrait sa famille s'il continuait ainsi ?

Tout en réfléchissant, il déplia la toile de coton qu'il allait devoir découper. Au moins les étoffes ne criaient-elles pas pitié chaque fois qu'on s'apprêtait à les tailler.

### Palais de l'archevêque de Tyr, logement d'Herbelot Gonteux, après-midi du mercredi 1er octobre 1158

Dépliant ses habits avec soin, Herbelot Gonteux en vérifiait le bon état avant de les trier en plusieurs tas. La mauvaise saison approchait et il privilégiait alors les étoffes de laine épaisse, plus lourdes, mais aussi bien plus confortables. Il lissa affectueusement sa vieille coule monastique, qu'il n'avait plus guère l'occasion d'enfiler. Bien qu'ayant grandi dans un couvent, il n'avait jamais fait ses vœux et demeurait simple clerc. Il était néanmoins prêtre, promis à un bel avenir, car déjà au service d'un des prélats les plus prestigieux du royaume.

Il n'était pas friand de tenues trop élaborées, portant généralement des cottes sobres de coupe bien que taillés dans des étoffes de qualité. Il préférait voir les tissus les plus riches et les vêtements les plus somptueux aux nécéssités liturgiques, ayant acquis dans le monde clunisien le goût de l'opulence décorative au service de Dieu. Ses inclinaisons personnels étaient plus simples, tant que son besoin de confort était satisfait.

Tandis qu'il se laissait aller à des rêveries, il s'aperçut que son valet, Germain, était en train de ranger les tenues d'hiver au lieu de celle d'été. Il le reprit assez sèchement, habitué qu'il était aux remontrances strictes de l'univers monastique. Voyant que le serviteur ne bronchait pas, le regard triste et désolé, il s'apaisa bien vite.

« Que t'arrive-t-il depuis quelque temps ? Toi qui ne commettais aucun impair, te voilà aussi écervelé que moineau !

--- Je m'en excuse, mestre Gonteux. Je suis pareillement distrait en raison de la mort de ma mère.

--- Ah, que me voilà contrit. J'avais oublié que tu l'avais portée en terre voilà peu. Me voilà bien, avec mon sermon. Tu en as encore souci ?

--- Je n'avais qu'elle, vous savez. Elle me manque cruellement. »

Enfant trouvé confié aux soins des moines, Herbelot fut touché par cet aveu et invita le jeune garçon à s'asseoir auprès de lui.

« Je ne saurais t'apporter parfaits conseils, mais en période de doute, je prie toujours la sainte Vierge mère de Dieu. Elle apporte réconfort aux enfants endeuillés. Tu pourrais même faire dire quelques messes en sa mémoire, cela te mettra baume au cœur, j'en suis acertainé.

--- C'est que je n'ai guère de quoi faire aumône.

--- N'as-tu pas de quoi offrir un cierge en accompagnement de la prière ? Cela soulage, je t'assure. »

Désormais plus aguerri aux atermoiements de la confession, Herbelot comprit que Germain se refermait et semblait préoccupé par bien plus grave.

« Je te vois bien plus soucieux qu'il ne sied. Si je peux t'aider en quoi que ce soit, n'aie crainte de mon ire. Je n'ai qu'à me louer de toi depuis que tu travailles à mes côtés. Dépose le fardeau qui pèse en ton cœur. »

Germain hésita un petit moment, se frotta le nez qui coulait et avoua dans une voix éteinte.

« C'est que je dois forte somme, mestre. Et ne peux honorer ma dette.

--- Que voilà vilaine affaire ! J'espère que ce n'est pas suite à funestes dévoiements.

--- Certes pas, mestre, certes pas. C'était pour payer les soins de ma pauvre mère. Le médecin, les drogues d'apothicaire... Toutes mes économies y sont passées et je dois encore une belle somme à celui qui m'a fait l'avance. »

Herbelot fit un rictus. Il avait la plus profonde antipathie pour ceux qui faisaient commerce de l'argent et d'autant plus lorsque c'était aux dépens d'un miséreux.

« Tu n'aurais jamais dû te fier à ces serpents qui vendent le temps de Dieu comme s'il leur appartenait.

--- Je ne savais que faire, mestre. Il me fallait trouver de quoi soigner ma pauvre mère.

--- Le blâme est surtout sur eux, qui font honteux commerce là où Il a foulé la terre. C'est le genre de bourse qui entraîne de certes une âme aux Enfers. Combien dois-tu ?

--- Encore un peu plus d'un besant. Un sou par semaine encore quatre semaines, plus six deniers de retard. »

Herbelot en fut soulagé, la somme n'était pas énorme et il avait largement de quoi la rembourser. Sa prébende était bien supérieure à ses dépenses et il en consacrait une partie à des dons aux nécessiteux. Habitué à se voir doté de tout ce dont il avait besoin par la collectivité, il ne trouvait nul intérêt à amasser les pièces en un trésor futile.

« Avant tout, il te faut te confesser. En recourant à des pratiques condamnables, tu as entaché ton âme immortelle. Voilà bien plus inquiétant méfait, bien que facile à nettoyer par une juste pénitence. Tu vas venir avec moi que je revête l'habit et je t'entendrai, ou tout autre confesseur que tu préféreras. »

Germain sentit une boule se former dans son estomac. Il comprenait qu'il allait franchir un point de non-retour. Jusqu'à présent il s'était contenté de donner le change quand on le croyait Latin. Mais en acceptant de se livrer à ce sacrement, il savait que son crime serait bien plus grave. Il ne s'en vit pas le courage et balbutia quelques mots avant d'arriver à ânonner péniblement son aveu.

« C'est que je ne suis pas latin, mestre Gonteux. Je suis syriaque...

--- Que me contes-tu là ? N'y a-t-il pas plus franc que Germain ?

--- C'était le nom de mon père, que j'ai repris. Ma mère m'a fait entrer en sa religion sous celui de Sharbel. »

Herbelot secoua la tête de dépit, se grattant la tonsure.

« Voilà donc que tu ajoutes l'imposture à tes méfaits...

--- J'ai toujours aimé le nom de mon père, et n'ai jamais prétendu être ce que je n'étais pas !

--- Oui da, tu t'es contenté de ne pas démentir ce qu'on croyait de toi. Ton péché n'en est pas moins net, mon garçon ! »

Germain demeura muet sous le regard lourd de reproches d'Herbelot. Pour autant, le clerc n'était pas si fâché que touché par la détresse de son interlocuteur.

« Vu que tu n'es pas à mon service direct, ton sort ne dépend pas de moi. Ce sera au sire archevêque qu'il conviendra de trancher ton sort. D'ici là, tu continueras ton office comme si de rien n'était. »

### Palais de Pierre de Tyr, bureau de l'archevêque, fin de matinée du jeudi 2 octobre 1158

Le modeste bureau était envahi des parfums maritimes, que le vent faisait entrer en rafales par la petite fenêtre. Insensible aux courants d'air qui déplaçaient les documents de la table de travail, le vieux Pierre de Barcelone, archevêque de Tyr portait les yeux au loin par-delà les toits de la ville. Il aurait pu sembler indifférent à ce qui se disait dans la pièce, mais ses familiers savaient qu'il ne faisait jamais preuve d'un tel irrespect envers ses visiteurs. Il réservait ses absences à ses proches, en faisait un signe d'intimité.

Herbelot avait expliqué rapidement la situation du valet qui le servait au sein du palais et avait plaidé sa cause. C'était selon lui un garçon travailleur et honnête, qui n'avait agi que dans l'insouciance et l'inconséquence de la jeunesse. La remarque avait amusé le vieil ecclésiastique, enclin à prêter la même inexpérience à son secrétaire, malgré toute l'affection qu'il lui vouait.

L'archevêque se tourna vers les deux hommes debout face à lui. Germain lui semblait fort piteux, les mains croisées et le regard rivé au sol. Il lui paraissait bien humble et modeste. Loin de la superbe des barons dont les fautes, pour n'être pas moins nombreuses, étaient souvent beaucoup plus graves. La voix éraillée du prélat emplit la salle sans effort, porteuse d'autorité malgré ses tremblements.

« Entends bien, mon garçon, que tu as lourdement fauté en te jurant à moi sous un faux nom, même si tu l'avais pris en mémoire d'un parent aimé. Outre cela, tu as laissé croire que tu étais fidèle sujet de la sainte Église alors que tu appartenais à une secte orientale. Amie, certes, mais distincte. »

Pierre de Barcelone marqua une pause, plissant ses yeux fatigués pour mieux voir si ces paroles faisaient leur effet sur le jeune homme.

« Dans ton errance, tu as eu l'heur d'encontrer un homme de bien, mon secrétaire, qui parle de toi en termes bien élogieux malgré tes manquements. Je porte cela à ton crédit et suis donc prêt à te tendre la main. Tu devras néanmoins laver ton âme de ces péchés si tu veux demeurer en mon hostel, et donc te confesser et suivre la pénitence qui te sera attribuée. »

Voyant que sa dernière phrase soulevait l'incompréhension de ses deux interlocuteurs, l'archevêque leva une main impérieuse.

« Comme le sacrement de la confession est réservé aux membres de l'Église, il te faudra avant cela rejoindre la communauté des fidèles et te faire baptiser. Sous ce vocable que tu aimes tant, pourquoi pas ? J'encrois que tu sauras trouver bon parrain pour te préparer à cela d'ici la Pentecôte, où cela sera. »

Il allait tendre la main pour donner son anneau à baiser puis se reprit.

« D'ici là, tu rejoindras les pénitents qui jeûnent au pain sec et à l'eau chaque vendredi, pour te faire passer le goût de ces mensonges et préparer ton âme à recevoir l'Esprit saint. »

Après avoir salué comme il seyait, Herbelot et Germain se retirèrent. Ils déambulèrent sans un mot en direction de la grande salle où le repas serait servi. Traversant les jardins, le jeune prêtre arrêta son compagnon d'une main.

« Pourpense bien la chance qui t'est donnée de repartir du bon pied. Je serai heureux d'être ton parrain au jour de ton baptême, qui sera pour toi véritable renaissance, ainsi qu'il sied. »

Puis il sortit d'une de ses manches une bourse un peu dodue qu'il glissa dans la main du valet.

« Outre, tu donneras ça aux sangsues qui te harcèlent. Nul besoin de mettre en péril ton âme plus longtemps. »

Germain en resta bouche bée un instant puis lança un regard affolé vers Herbelot.

« Je ne peux, mestre, vous êtes trop généreux !

--- Ne t'enjoie pas trop. Tu devras rembourser cette somme ! Verse un denier par semaine durant une année au moins au chapelain de la Sainte Vierge. Qu'il associe ta mère à ses messes durant ce temps. Nous serons alors quittes. Mais libre à toi de continuer par la suite. »

Puis le petit clerc ventripotent continua sa route sans plus un regard pour son compagnon hébété de tant de sollicitude. Herbelot était encore plus mal à l'aise avec les effusions sentimentales qu'avec les manquements à la Foi. Il se sentait malgré tout incroyablement satisfait de lui. À sa grande honte, tenta-t-il de se corriger. ❧

### Notes

Le prêt à intérêt était généralement interdit par toutes les religions du Moyen-Orient médiéval. Ce qui n'empêchait en rien sa pratique extrêmement banalisée. On lui trouvait de nombreuses excuses, en excluant de cette interdiction les membres d'une autre croyance, en dissimulant les intérêts dans des taux de change ou en établissant des versements différés en nature. Tous les artifices étaient bons et même des ecclésiastiques y recouraient régulièrement, quand ils n'étaient carrément pas usuriers eux-mêmes.

Malgré tout, il arrivait que des crises moralisatrices secouent le monde des affaires et qu'on annule purement et simplement certaines dettes voire qu'on confisque un capital bien mal acquis. Mais c'était malheureusement souvent avec des motifs tout aussi hypocrites que les pratiques décriées.

On trouve mention de différents styles de prêts dans les écrits retrouvés. Un grand nombre concerne des besoins de vie quotidienne et pas du tout des entreprises commerciales. Le prêt contracté par Germain/Sharbel est inspiré d'un courrier dont parle S.D. Goitein, de trois dinars, qui avait servi à rémunérer les soins d'un malade.

### Références

Ashtor Eliyahu, *Histoire des prix et des salaires dans l'orient médiéval*, Paris : École Pratique des Hautes Études, 1969.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I à 6, Berkeley et Los Angeles : University of California Press, 1999.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. IV. The Cities of Acre and Tyre*, Cambridge : Cambridge University Press, 2009.

O Fortuna
---------

### Shayzar, pont sur l'Oronte, matin du samedi 8 mars 1147

Entamant la traversée du pont, Ra'ul tourna la tête vers les montagnes à l'ouest vers le jabāl al-ʾanṣarīya[^82]. Cela faisait plusieurs années que les Nizāriyyūn[^83] s'y implantaient, parfois au détriment des maîtres de Ra'ul, les puissants Banu Munqidh de Shayzar. Il n'avait toutefois nullement l'intention d'aller dans cette direction et, sans même y penser, il porta son regard vers la pointe méridionale du jabāl Banī-ʻUlaym[^84], vers le nord. Surplombant le long plateau, des nuages d'altitude y traçaient des mosaïques d'ombre aux motifs changeants.

Inquiet à l'idée de dévoiler ses projets, il vérifia que les enfants étaient bien installés sur leur âne, replaça quelques attaches, ne sachant que faire de ses mains. De l'autre côté de l'animal, son épouse Shayma semblait indifférente. Elle marchait tranquillement, surveillant les deux garçons qui aimaient à se chamailler. Depuis l'aval, le chant des norias parvenait jusqu'à eux, sombre gémissement couvrant le grondement des flots.

Alors que le pont allait s'incurver vers l'est, ils approchèrent de la tour qui marquait la zone de péage. Un des soldats, indolent, était assis sur le parapet et s'occupait à jeter des cailloux dans l'eau. Personne d'autre n'était visible, mais Ra'ul savait qu'il y avait toujours plusieurs hommes dans l'édifice, généralement bons archers.

En les voyant, le garde se redressa et assujettit correctement sa ceinture où se balançait doucement un fourreau. Ce n'était pas un Turc, car on déléguait rarement de telles tâches subalternes à ces guerriers d'élite. Quant à l'abandonner à un Turkmène, nul émir n'avait suffisamment confiance en eux pour cela. Ra'ul savait que ce n'était qu'un fils de fellah à qui on avait attribué une arme. Al-'Abbas n'était pas un ami, mais il le croisait depuis son enfance.

L'homme se gratta le menton lorsqu'ils arrivèrent à sa hauteur. Il plissa les yeux, indisposé par le soleil. Il inclina la tête en guise de salut et les apostropha d'un ton narquois, les invitant à s'arrêter d'un signe de la main.

« Salām ! Il va falloir que vous payiez la taxe sur les marchandises. »

Avant que Ra'ul n'eut le temps de répondre, désarçonné par l'entrée en matière, le soldat enchaîna.

« On dit que les petits enfants sont le mets préféré des Ifranjs, là-bas, au nord. Et vous en avez deux beaux dans vos paniers. »

Shayma sourit et baissa la tête modestement, invitant son époux à répondre.

« Si seulement ! Ces deux démons auraient tôt fait de réduire à néant leurs villes si on les y menait !

--- Peut-être veulent-ils rejoindre l'aḥdāṯ[^85], en ce cas ? »

Voyant que les deux enfants hésitaient à répondre et commençaient à s'agiter, Ra'ul s'efforça à sourire.

« J'ai espoir qu'ils suivent mes pas dans la taille de la pierre. C'est bon métier, moins risqué que de manier lance et bouclier. »

Le soldat acquiesça en silence puis se tourna vers la forteresse à l'est qui les dominait de toute sa hauteur depuis l'éperon rocheux où elle s'étendait.

« Tu as de la chance de pouvoir fréquenter les maîtres, là-haut. Je vois passer les carriers avec les pierres, mais je n'ai que rarement l'occasion de monter au qala'at.

--- Abi Al-Asaker Sultan[^86] s'intéresse peu à mon ouvrage, mais je croise souventes fois son frère Abi Salamah[^87]. »

Hochant la tête, le garde se mit à bâiller puis leur fit signe d'avancer.

« Allez, je vous retiens pas plus longtemps. Salām ! »

Accompagnant son geste d'un sourire amical, Ra'ul salua et tira sur la longe de l'âne. Il se retint de tourner la tête vers Shayma, mais l'observa du coin de l'œil. Elle ne laissait toujours rien transparaître, avançant tranquillement au niveau des enfants. En peu de temps, ils rejoignirent le bosquet d'où partaient les routes vers le nord et vers l'ouest. Ce n'est qu'alors que Ra'ul échappa un soupir.

Face à eux s'étendait le plateau d'où naissaient les reliefs abritant l'endroit où ils se rendaient : Hisn Affamiya[^88], aux mains des Francs depuis bientôt un demi-siècle. Là d'où venait la famille de Ra'ul, capturé encore enfant.

Ils passèrent sans un mot la zone de jardins agglutinés autour des canaux d'irrigation distribuant l'eau des norias. Quelques gamins couraient dans les bosquets, adolescents impatients de se lancer le défi de sauter du haut de la roue dans les remous, le rite initiatique local officiellement réprimé par les adultes, mais secrètement admis par les hommes. Sauf par les charpentiers responsables de l'entretien des immenses godets et de leur complexe agencement de poutres et de planches. Jamais ses fils n'auraient l'occasion de s'éprouver ainsi, songea Ra'ul. Mais sa décision était prise : il devait retrouver les siens.

### Apamée, fin de matinée du jeudi 28 juillet 1149

Parcourant les rues pour rentrer chez lui, Ra'ul trouva la ville guère changée après sa conquête par les forces musulmanes. Les éventaires des commerces ouvraient de nouveau et, si les sabres et les sharbush[^89] avaient remplacé les tenues latines, la présence militaire demeurait superficielle. Il avait été convoqué, comme tous les habitants, par les nouveaux notables en charge de la cité. Le fait d'avoir négocié la reddition avait permis d'éviter le saccage et les exactions multiples que ne manquait jamais d'occasionner un siège batailleur. Le šiḥna[^90] lui avait fait l'impression d'un homme davantage inquiet du péril extérieur que des troubles à l'ordre public. On le disait assez proche du pouvoir damascène et donc relativement bien disposé à l'encontre des populations franques, pour peu qu'elles s'affranchissent de leurs obligations légales et, surtout, fiscales.

Le muḥtasib, dont la fonction était, entre autres, de contrôler les mœurs et les pratiques religieuses, l'avait plus longuement interrogé. Vieil homme au profil de ṣūfī[^91], au visage émacié et à la voix douce, il s'était montré d'une grande bienveillance, mais avait fait preuve de perspicacité dans ses questions. Apprenant que Ra'ul avait été un captif franc converti enfant à l'islam, il lui avait demandé pour quelle raison il avait abjuré. Lorsque Ra'ul avait évoqué sa capture ultérieure par les Latins, il avait acquiescé sans un mot et avait proposé de l'entendre plus tard, pour envisager un retour à la vraie foi. L'apostasie était un crime grave, rarement considéré à la légère.

Parmi tous ces visages inconnus ne surnageait que celui du vieux al-Nadir al-Jurashi, un des riches commerçants d'étoffes de la ville, dont l'influence personnelle et familiale avait permis l'élévation au rang de ra'īs. Les compétences en avaient été grandement écornées par le pouvoir zenguide, ce qui n'était pas pour lui déplaire, anxieux qu'il était de devoir désormais superviser l'activité de ceux qui avaient été ses dirigeants. Durant tout l'entretien, il s'était débattu dans les manches de sa large 'aba, jetant régulièrement des regards inquiets vers les conquérants turcs et damascènes qui formaient la nouvelle administration locale.

Lorsqu'une fois entré, Ra'ul repoussa la porte de sa modeste demeure, il sentit la présence de Shayma derrière lui. Depuis la prise de la ville, elle se rongeait les sangs à l'idée qu'on les punisse pour avoir abjuré l'islam. Il se déchaussa et ôta son turban, ne gardant que son petit bonnet tricoté. Sa voix rauque lui fit l'effet d'un coup de tonnerre dans le silence de l'endroit.

« Pour eux, on est des ḏimmīs. Il faudra payer la taxe.

--- Ils n'ont rien dit d'autre ?

--- Le muḥtasib veut me revoir, on verra bien. »

Tout en répondant, il rejoignit la pièce principale et s'affala sur une banquette, marquant par là son intention de ne pas en parler plus. Il s'installa confortablement, dos au mur et demanda à Shayma de lui apporter de quoi se désaltérer. Il s'enquit aussi du repas, façon élégante de la congédier. Après un regard désapprobateur de se voir ainsi mise à l'écart, elle disparut dans la réserve utilisée en guise de cuisine.

Ra'ul entendait les garçons qui jouaient dans la cour qu'ils partageaient avec quelques autres familles. Des arbres desséchés par le soleil tentaient d'y survivre dans un décor de poussière et de gravier. Il repensa à son entretien et il trouvait qu'il s'en était plutôt bien passé au final. La plupart des nouveaux dirigeants s'inquiétaient avant tout de la capacité de la ville à payer ses impôts. Le šiḥna s'était même félicité de voir qu'un des ḏimmīs parlait si parfaitement l'arabe. Apprenant qu'il était tailleur de pierre, il avait décidé de lui confier l'approvisionnement en moellons depuis les ruines antiques voisines. Ra'ul n'avait certes pas mentionné qu'il était un fugitif, mais le fait qu'il se soit converti au christianisme n'avait ennuyé personne. Il n'avait pas eu l'impression d'avoir été traité de façon spéciale lors de son entretien, ayant pu assister à quelques autres auparavant.

Il se demandait même si cela ne constituait pas pour lui une occasion de voir sa situation s'améliorer. Lorsqu'ils étaient arrivés avec Shayma, il ne restait personne de sa famille et il n'avait jamais été véritablement accepté. Les Latins le considéraient comme un étranger, incapable qu'il était de parler leur langue sans accent, sans compter ses habitudes de vie orientales. Les musulmans, pour leur part, estimaient avoir affaire à un impur et préféraient l'éviter. Il n'avait pas franchement été mal accueilli, eu égard à ses aptitudes professionnelles, mais plusieurs années après, il admettait qu'il n'était pas vraiment intégré. Il échangeait un peu avec ses collègues au travail, fréquentait les gens de son quartier de loin en loin, mais il ne pouvait dire être satisfait de cela. Superviser la récupération de pierres dans les vestiges représentait une avancée par rapport à son ancien rôle de simple tailleur. Et s'il devait, pour être pleinement accepté, se convertir de nouveau à l'islam, il s'en sentait le cœur. Pour lui l'essentiel était de ne plus appartenir à un maître. Il devait, en outre, reconnaître qu'il était bien plus à l'aise avec les hadiths du Prophète qu'il récitait depuis l'enfance qu'avec ces prières latines dont il ne comprenait pas un traître mot.

Lorsque son épouse entra avec un plat et du pain à la main, elle le trouva apaisé et s'en étonna. Tout en lui répondant, il laissa même apparaître un sourire, faisant plisser son visage marqué de rides par le soleil.

« Je pensais à la situation. C'étaient les troupes de Hama qui ont assiégé la ville, mais le nouvel homme fort, ici, c'est Nūr ad-Dīn Maḥmūd[^92], le fils de Zengī. Il n'a sûrement que faire des fiers Banu Munqidh qui se refusèrent à son père. Alors des fugitifs comme nous...

--- Mais le muḥtasib ?

--- Je saurais bien lui prouver que nous avons agi par nécessité. Nous pourrions aussi aller voir l'imam pour discuter de cela.

--- Mon père disait toujours "Qui se justifie sans être coupable, s'accuse." Attendons de voir ce qu'ils veulent avant de le leur donner. »

Ra'ul hocha la tête.

« Tu parles de raison. »

Il s'assit plus correctement sur le bord de la banquette et remonta ses manches, soudain ragaillardi.

« Appelle donc les garçons et apporte-moi de quoi me laver les mains, nous allons manger. »

### Apamée, ruines de la cité antique, début d'après-midi du dimanche 14 juillet 1157

Installé sous un abri rudimentaire couvert de toile, Ra'ul sentait la sueur qui dévalait le long de sa colonne. Avec les ouvriers de la carrière, ils venaient de passer un moment à tenter de regrouper les mules qui leur servaient au quotidien pour transporter les matériaux. Elles étaient habituellement attachées à une longue corde près de l'endroit où ils travaillaient, sous la supervision des plus jeunes. La présence d'un prédateur avait dû les inquiéter, car elles avaient fini par détacher la longe et toutes s'étaient enfuies, causant le plus grand désordre. Bien évidemment, cela avait eu lieu au plus chaud de la journée et il avait fallu courir après ses fichues bêtes sur tout le plateau.

Ra'ul avala un peu d'eau et tendit l'outre à son compagnon Twabah al-Kabir, aussi transpirant que lui. Puis il s'assit sur une des nombreuses pierres abandonnées là par le temps.

« Maudites bêtes, elles vont nous faire courir toute la journée.

--- Shaybah et Jibril en ont trouvé encore une ! »

Deux des ouvriers faisaient en effet de grands gestes pour tenter d'attraper une autre des bêtes qui courait en tous sens le long de la majestueuse colonnade qui s'étendait vers le sud. Le bruit de ses sabots sur l'antique pavage résonnait jusqu'à leurs oreilles. Indifférent à leur problème, Ra'ul trancha une large portion d'une pastèque et mordit dedans avec délectation. Il sentait le jus sucré lui couler sur le menton, traçant des sillons d'eau noire dans la poussière amassée là.

« Ça fait la troisième fois en moins d'une semaine... Je vais finir par demander si on ne pourrait pas avoir des chameaux !

--- Un bon muletier serait plus utile. Le vieil 'Umarah est fort amiable, mais il y voit à peine plus qu'il n'entend. Elles sentent bien qu'elles peuvent faire ce qu'elles veulent. C'est intelligent une mule. Il n'y a qu'un cadi qui puisse leur faire entendre raison[^93] ! »

Approchant sa puissante silhouette de la corbeille de fruits, Al-Kabir se coupa à son tour une belle part et se tint là, face à l'immense plateau hérissé de colonnes et de vestiges anciens, parmi les broussailles chenues et l'herbe rase roussies par l'été. Au loin, vers le sud-ouest, il apercevait les murailles de la cité perchée sur son tell, en bordure de relief. Les montagnes loin à l'occident dansaient dans la chaleur de l'après-midi sous un soleil de plomb.

« Ils sont partis où les gens qui vivaient là, à ton avis ?

--- Ils sont allés dans le nord, là où il fait moins chaud et où les mules n'existent pas ! se railla Ra'ul.

--- C'est pas le pays des Ifranjs que tu me présentes là ? ricana al-Kabir.

--- Je sais pas, je n'y suis jamais allé. Mais en fait de mules, il y en a sûrement plus qu'à mon goût ! »

Ra'ul sourit et se leva pour aller tailler une autre tranche de fruit. Il éprouva soudain un vertige dans ses jambes, crut un moment qu'il avait eu trop chaud à courir au soleil puis il vit qu'al-Kabir semblait également troublé. L'instant d'après, il sentit comme des frissons monter dans tout son corps. Puis la pastèque tomba du tabouret, roula à terre, avant que le modeste appentis de bric et de broc ne s'écroulât sur sa tête.

Il sombra alors dans un univers de chaos et d'épouvante. Tout fuyait sous ses mains, il n'arrivait même pas à se maintenir dans une position quelconque, le sol échappant sans cesse à ses appuis. Il heurtait des objets, des choses le frappaient de partout, aveuglé qu'il était sous la toile abattue. Brassé dans un maelstrom furieux, il n'arrivait même pas à se recroqueviller. Un grondement assourdissant noyait le monde dans d'effroyables borborygmes, broyant ses tympans sans qu'il puisse n'y rien reconnaître.

Puis ce fut le silence. Total. Un court instant. Puis des cris, des appels.

Ra'ul réalisa qu'il avait les yeux fermés, les ouvrit pour trouver l'obscurité. Son cœur s'emballa à l'idée d'être devenu aveugle, avant qu'il ne comprenne qu'il était tête en bas, sous la toile. Il s'efforça de se redresser, sentit qu'on l'aidait. Des bras puissants le soulevaient. Il reconnut la voix d'al-Kabir.

« Le tremblement m'a fait rouler en contrebas. J'ai fini dans les buissons. Toi tu as reçu l'abri sur la tête ! »

Le soleil aveugla Ra'ul quand il se dégagea enfin, hagard, couvert de poussière et le corps endolori.

« Tu n'as rien, inch'Allah, s'enthousiasma al-Kabir.

--- Twabah...

--- Par contre je crois que tu auras besoin d'aller aux bains ce soir !

--- Twabah...

--- Quoi, tu as mal quelque part ?

--- La cité... Elle a disparu ! »

Lorsqu'al-Kabir se tourna pour regarder au-delà du plateau, ses yeux s'écarquillèrent d'horreur. Une large portion du promontoire où la cité était implantée avait laissé la place à une gigantesque nuée poussiéreuse. ❧

### Notes

Le personnage de Ra'ul est inspiré des mémoires de Usamah ibn Munqidh. Il conte l'histoire de ce Latin captif, affranchi et converti qui finit par s'enfuir avec femme et enfants. Selon moi, elle illustre la plasticité des rapports sociaux qui existaient alors dans cette région du monde. On ne peut exclure l'adaptation littéraire du récit, car le vieil homme a un dessein à travers ses anecdotes, mais cela veut dire que de telles choses étaient envisageables, même si cela offre l'occasion de fustiger l'adversaire et ses nombreux défauts.

Les tremblements de terre étaient relativement fréquents au Moyen-Orient et certains chroniqueurs les mentionnaient dans leurs narrations, détaillant parfois le sort des populations durement affectées. Al-Qalānisī est de ceux-ci et j'ai d'ailleurs retenu la date qu'il indique dans ses textes pour celui qui aurait définitivement ruiné la cité d'Apamée.

Le titre, quant à lui, est directement inspiré du poème en latin écrit au début du XIIIe siècle dans la série dite *Carmina Burana* ou *Chants de Beuern*, l'abbaye où ils furent découverts. Sa renommée actuelle vient principalement de sa mise en musique par Carl Orff dans sa cantate en 1935-36. Le texte évoque un motif culturel médiéval très fréquent, la fortune qui tourne sa roue pour les puissants et les pauvres, frappant chacun sans ménagement.

Texte latin de *O Fortuna*

> O Fortuna\
> velut luna\
> statu variabilis,\
> semper crescis\
> aut decrescis,\
> vita detestabilis\
> nunc obdurat\
> et tunc curat\
> ludo mentis aciem,\
> egestatem,\
> potestatem\
> dissolvit ut glaciem.

> Sors immanis\
> et inanis,\
> rota tu volubilis,\
> status malus,\
> vana salus\
> semper dissolubilis,\
> obumbrata\
> et velata\
> michi quoque niteris;\
> nunc per ludum\
> dorsum nudum\
> fero tui sceleris.

> Sors salutis\
> et virtutis\
> michi nunc contraria,\
> est affectus\
> et defectus\
> semper in angaria.\
> Hac in hora\
> sine mora\
> corde pulsum tangite;\
> quod per sortem\
> sternit fortem,\
> mecum omnes plangite!

Traduction de *O Fortuna*

> Ô Fortune,\
> comme la Lune\
> de nature changeante,\
> toujours croissant\
> ou décroissant ;\
> Vie détestable\
> oppressante\
> puis aimable\
> par fantaisie ;\
> Misère\
> et puissance\
> se mêlent comme la glace fondant.

> Sort monstrueux\
> et insensé,\
> Roue qui tourne sans but,\
> distribuant le malheur,\
> et le bonheur en vain\
> insaisissable toujours ;\
> Ombrée\
> et voilée\
> pour moi sans but ;\
> Maintenant par jeu,\
> j'offre mon dos nu\
> à ta méchanceté.

> Les cours de la santé\
> et du courage\
> me sont contraires,\
> affligé\
> et défait\
> toujours asservi.\
> À cette heure\
> sans plus tarder\
> ses cordes vibrantes m'affectent ;\
> Alors le destin\
> comme moi frappe le fort\
> et chacun se lamente !

### Références

Büchler P., Joukovsky F., Micha A., *Carmina Burana*, Paris, Honoré Champion, Collection bilingue, volume 8, 2002.

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008

Al-Dbiyat Mohamed, « Les Norias de Hama sur l'Oronte. Un système traditionnel original de l'utilisation de l'eau fluviale » dans *Gestion durable et équitable de l'eau douce en Méditerranée. Mémoire et traditions, avenirs et solutions*, Actes des 5èmes Rencontres internationales Monaco et la Méditerranée, Monaco, 26-28 mars 2009, 2010, p. 191-210.

Gibb H. A. R., *The Damascus Chronicle of the Crusades. Extracted and translated from the chronicle of Ibn al-Qalanisi*, Londres : Luzac & Co., 1932, réédition : Mineola : Dover Publication, 2002.

Ṭāhir Muṣṭafā Anwār, « Les grandes zones sismiques du monde musulman à travers l'histoire. --- I. L'Orient musulman », dans *Annales islamologiques*, volume 30, 1996, p.79-104.

Mercator
--------

### Alexandrie, fondaco des Pisans, matin du lundi 1er avril 1157

Les pas feutrés du serviteur venant tirer les rideaux de la chambre réveillèrent Guido. Il grogna puis se retourna sur ses coussins de plume. La lumière qui se déversa soudain à travers les claustras révéla au marchand que le jour était bien avancé. Il ronfla de déplaisir à l'idée de s'arracher à sa couche douillette. La veille, il avait organisé un banquet pour célébrer dignement le dimanche de Pâques. Ses invités avaient pu y apprécier la richesse de sa demeure, même si la taille en était réduite. Ils avaient d'ailleurs largement débordé dans la cour centrale et la fête avait duré bien avant dans la nuit.

Guido passait la plupart de son temps de ce côté de la Méditerranée, n'étant pas rentré à Pise depuis belle lurette. La création d'un fondaco[^94] à Alexandrie quatre ans plus tôt était arrivée à point nommé pour lui. Il était las de naviguer et avait toujours apprécié le climat égyptien. Par ailleurs, sa demeure était désormais bien plus luxueuse que tout ce qu'il avait pu posséder jusqu'alors : des meubles marquetés, du stuc et des mosaïques sur tous les murs, agrémentés de tentures de soie importées de Byzance et d'Aden. Des tapis épais du Khorasan habillaient les sols ouvragés et des senteurs épicées s'échappaient de brûle-parfums damascènes en bronze et en céramique dans chacune des pièces. Il ne pouvait résider en dehors du fondaco, certes, mais il y amassait tout ce dont il pouvait rêver. Et, surtout, de là il se rendait aisément à Fustat[^95], le véritable cœur économique de la région.

Enfilant ses mules, il passa une chemise d'étoffe fine et un bliaud richement orné, de velours de soie rebrodé, puis pénétra dans la salle principale, ouverte par une large arcature sur la cour centrale, au-dessus des entrepôts. Il aimait à prendre ses repas en admirant tout ce petit monde qui s'affairait, en partie à son service. Un valet s'empressa de lui apporter des fruits frais, un peu de vin de Chypre et du fromage de brebis, le tout disposé dans de magnifiques coupes en céramique sicilienne décorée.

Des balles d'un de ses amis étaient actuellement transportées dans une des galeries. Il s'agissait apparemment de tissus et de papier, deux denrées hautement rentables. Mais Guido faisait commerce de produits encore plus intéressants : il vendait du bois d'œuvre, souvent destiné à des engins de guerre, ainsi que des armes et des matériaux qui servaient à les fabriquer. Il importait essentiellement d'Europe et y renvoyait ses navires emplis de marchandises en provenance d'Aden. En outre, il ne dédaignait pas quelques petits investissements ici et là, sur les étoffes d'Ifriqiyah[^96] en particulier, pour lesquels il ne faisait généralement que s'associer. Cela lui permettait par ailleurs de justifier ses activités sans avoir à s'étendre sur ses principales négociations, maintes fois condamnées par l'Église et les puissants monarques chrétiens.

Finissant de picorer dans les fruits, il rentra dans sa maison et prit son qalansuwa tawila[^97] qu'il recouvrit comme à son habitude d'un turban lâche qui lui repassait sous le cou, à l'égyptienne. Enfin, il enfila une 'aba[^98] à larges manches, en soie d'Alep. Ainsi équipé, il descendit faire le point avec ses clercs sur les affaires en cours. Il avait le temps de flâner avant le déjeuner. Avec la reprise des échanges commerciaux après l'hivernage, il attendait l'arrivée sous peu de produits acheminés depuis Chypre par le biais d'un de ses amis.

### Alexandrie, fondaco des Pisans, midi du mercredi 3 avril 1157

Des soucis avaient empêché Guido de profiter de son repas. Le navire qu'il attendait, en provenance du nord, était à quai depuis la veille, mais les douaniers semblaient trouver un malin plaisir à retarder la sortie des passagers. Un de ses associés, Elijah ben Moses, était venu chez lui, affolé par la tournure que prenaient les affaires. Guido décida donc de l'accompagner sur place, dans les bâtiments de la douane. Il fit préparer une monture pour s'y rendre et réquisitionna deux de ses serviteurs et un scribe.

Très vite ils se retrouvèrent noyés dans le flot de portefaix, vendeurs ambulants, voyageurs et négociants qui encombraient les abords du port de commerce. L'odeur marine se mêlait subtilement aux relents de sueur, de poisson et de marchandises qui imprégnaient les quais. Au-delà du mouillage, sur l'île, l'immense construction du phare écrasait de sa masse tous les navires ancrés là. Même le vaste enclos des douanes paraissait minuscule en comparaison.

La cohue était indicible aux abords de la porte et on s'y invectivait dans une bousculade générale. Deux soldats se tenaient devant le petit guichet, les vantaux principaux étant fermés. De nombreux voyageurs étaient retardés, retenus, questionnés, et cela provoquait toutes sortes de problèmes pour les familles et les amis qui les attendaient. Guido fut un peu soulagé de découvrir que nul n'était épargné par les ergotages administratifs du pouvoir fatimide. En tant que Latin, il était toujours un peu inquiet de voir les autorités s'intéresser de trop près à ses activités. Il savait pouvoir en appeler au vizir Ṭalā'i' ben Ruzzīk en cas de souci, mais il était conscient que les réseaux d'influence étaient mouvants.

Finalement avant même de pouvoir accéder au bâtiment, il vit s'approcher son contact, Jahm al Aswad, un maghrébin au service de son associé tunisien Muqatil ibn Sa'id al-Arabi. Le jeune homme semblait consterné et essuyait d'un fin mouchoir son visage trempé de sueur. À la vue de Guido, il agita la main amicalement, l'air épuisé. Il salua poliment de la tête Elijah et se présenta à lui avant de s'adresser à Guido.

« Je ne voyais pas la fin de ces jours, mon ami. Que les démons emportent tous ces douaniers !

--- Vous avez eu graves soucis ? demanda poliment le Pisan.

--- Pas vraiment, seulement des désagréments. Mon père m'avait prévenu que les douanes d'Alexandrie étaient tatillonnes, mais je n'aurais pas cru à ce point-là. Ça a commencé hier : des hommes sont montés inspecter toute la cargaison, avec minutie. Puis nous avons été convoqués un par un, et avons dû indiquer nos noms et notre pays d'origine ainsi que les marchandises et monnaies en notre possession, pour déterminer la zakat[^99]. Et ceci sans même s'inquiéter du temps pendant lequel nous avons eu ces biens ! Un homme avec lequel j'ai sympathisé en route, simple pèlerin de la foi, a été obligé de régler la taxe sur ses provisions de bouche, vous vous rendez compte ? »

Agitant la tête en tout sens, sans cesser de frotter son mouchoir, il ne pouvait retenir le flot de paroles indignées qui avaient macéré dans son esprit.

« Ils ont ensuite longuement questionné certains d'entre nous sur les Ifranjs, et ils m'ont mené, comme un malfaiteur, pour répondre au gouverneur, au cadi, à nombre de questions insidieuses, notant par le menu toutes mes déclarations pour voir si je ne me contredisais pas. Finalement, on nous a autorisés à débarquer il y a peu et nous avons dû passer chacun à notre tour devant des fonctionnaires mesquins pour voir nos affaires personnelles être fouillées sans pudeur. Ils ont même regardé dans ma ceinture si je n'y cachais rien ! Outre, les porteurs étaient tellement mal organisés que ce sera une chance s'ils n'ont pas mélangé mes affaires avec celles d'un autre.

--- Vos soucis sont finis, je me suis arrangé pour que vous soyez bien installés, dans l'hôtellerie des Dinandiers, près de la Savonnerie. Vous verrez qu'Alexandrie sait se montrer accueillante. Avec Elijah, nous allons vous conduire immédiatement si vous le souhaitez. »

Voyant le visage soulagé du jeune marchand, Guido donna quelques instructions à son scribe et à un de ses serviteurs pour qu'ils retournent au fondaco puis il prit le chemin de l'hôtellerie à pied. Tandis qu'ils avançaient dans les rues, Elijah et lui indiquaient à leur hôte les bâtiments de renom, montraient les grandes colonnades de l'ancien temps, désignaient les places dignes d'intérêt. Ils ne récoltèrent que des moues peu éloquentes et des hochements de tête sans enthousiasme. C'était la première fois que Jahm parcourait ces lieux, mais il ne semblait guère s'en émouvoir. La mention d'un hammam tout à côté de son hébergement illumina malgré tout le visage du nouvel arrivant. Finalement parvenus devant l'hôtellerie, Guido allait prendre congé lorsqu'il sentit que le jeune homme hésitait à dire une dernière chose.

« Il demeure autre souci que vous n'avez mentionné, mon ami ? Dites et nous ferons au mieux. Nous avons usage des diwans de la cité...

--- J'aurais préféré vous l'annoncer autrement, mais je n'ai trouvé de belle façon pour cela. Cela me soucie depuis un moment : lorsque nous avons fait escale à Ascalon, la douane des Ifranjs a fait débarquer des balles.

--- Dont certaines à moi ? Combien ?

--- À dire le vrai, seulement les vôtres. Toutes les vôtres. »

### Alexandrie, fondaco des Pisans, fin de soirée du samedi 6 avril 1157

Des lots d'étoffes en provenance du Khwarezm[^100] venaient d'être ouverts et Guido avait un doute sur les sceaux de plomb identifiant les tissus. Il se trouvait donc de bien méchante humeur, la qualité n'étant pas celle des pièces habituelles. Déjà très soucieux de la saisie de sa cargaison à Ascalon, il craignait des contrefaçons. Comme il avait reçu cette marchandise en paiement d'une livraison d'ivoire à un contact sicilien, il était contrarié parce que cela pouvait entacher la réputation de cet associé et, par conséquent, la sienne. Il connaissait un Génois de ses relations qui avait failli être ruiné car on racontait partout qu'il avait longtemps collaboré avec un faussaire qui recachetait les bourses de dinars après en avoir gratté les pièces[^101].

Guido avait donc envoyé quérir à l'étage un de ses amis et associés, Francesco Caetani, pour avoir son avis. Ce farouche gaillard était un marchand âpre au gain, d'une réputation sans faille, ayant l'appui de sa puissante famille dans leur ville natale, mais aussi un homme affable et serviable. Francesco était un habitué du Khwarezm, ayant même visité la capitale, à la frontière avec le désert oriental. Il saurait immédiatement reconnaître une copie d'une vraie production.

Le riche négociant arriva dans l'entrepôt, un éventail à la main pour se faire de l'air. Il portait une ample tenue typiquement musulmane, jubba[^102] et 'aba, avec un petit chapeau de feutre brodé. Il était tellement ventru qu'il avançait en se dandinant, faisant ondoyer les étoffes à chaque pas. Guido remarqua immédiatement sa mine également contrariée.

« Que se passe-t-il mon cher ami ?

--- J'ai reçu dans mes balles du papier qui a été abîmé lors du transport. Les bâches étaient défaillantes et de l'eau de mer a gâté une bonne partie de la marchandise. J'aurais dû m'écouter et faire faire le transport intégralement par voie de terre. La saison commerciale commence fort mal cette année. »

Guido hocha la tête, dépité. Il lui semblait que tout allait mal depuis quelques jours. Francesco essaya de combattre l'amertume qui s'installait en reprenant d'une voix plus enjouée.

« Fort heureusement, de nombreux contrats de *societas maris*[^103] que nous avons mis en place devraient être profitables. Notre envoyé du Rhin a pu se procurer auprès d'armuriers un bon nombre de lames d'épée de qualité, qui intéresseront certainement le vizir. »

La nouvelle tira un sourire crispé à Guido, qui désigna les étoffes déballées devant lui sur une table.

« Puissiez-vous parler de raison, que ces ennuis se fassent bien vite oublier... Ce qui m'inquiète pour l'instant, ce sont ces marchandises, qui me semblent suspectes. »

Accommodant, Francesco se pencha sans plus un mot et inspecta d'un œil avisé les pièces présentées, n'hésitant pas à les porter au-dehors pour les voir à la lumière naturelle. Il tourna ensuite son attention sur les sceaux qu'il détailla en marmonnant pour lui-même. Puis il reposa le tout en vrac en déclarant :

« Ce sont bien des pièces khwarezmiennes, à n'en pas douter. Mais il est vrai que ce ne sont pas les plus belles que j'aie pu voir.

--- Vous me rassurez. Je redoutais grave problème avec ces produits.

--- À votre place, je les expédierais à l'occident, où elles auront plus de prix qu'ici. »

Rasséréné par la nouvelle, Guido invita Francesco à le suivre.

« Faites-moi le plaisir de venir partager un peu de vin grec pour vous remercier de votre conseil.

--- Avec grand plaisir, s'exclama son compagnon en souriant. Vous avez reçu une nouvelle cargaison de nabidh[^104] ? Je n'arrive pas à croire qu'il vous reste encore quelques tamawiya[^105] après la veillée de l'autre jour ! »

Les deux hommes repassèrent dans la grande cour tandis que des membres de l'ahdath[^106] étaient en train de clore les portes du fondaco pour la nuit. À l'intérieur, un valet tirait les verrous du petit guichet qui permettait aux individus identifiés d'aller et venir. La sécurité des biens et des personnes était à ce prix et aucun des marchands enfermés là n'y prêtait attention.

Une fois confortablement installé sur des coussins, Guido attendit que son compagnon ait eu le temps de déguster un peu le vin avant de lancer la discussion. Francesco était un homme de ressource, qui avait de nombreux contacts dans les territoires latins.

« Dites-moi, mon ami,vous avez entendu parler de mon souci avec le comte de Jaffa ?

--- Certes oui ! Et je le déplore. Avez-vous avec eux quelque contentieux non réglé ?

--- Pas à ma connaissance. Sans compter que ces balles ne sont pas miennes, sur les documents. Elles sont toutes identifiées appartenant à différents miens contacts. Il leur aura fallu grande habileté pour les saisir. »

Francesco sirota son vin en silence, réfléchissant aux implications d'une telle opération. Il savait qu'en théorie ce commerce était passible des condamnations les plus féroces de la part des chrétiens. D'autant plus que l'Égypte se montrait de plus en plus belliqueuse sous le nouveau vizir.

« J'ai parmi mes associés un certain Jaffa al-Akka, je ne sais si vous avez déjà traité avec lui...

--- Ce nom ne m'est rien.

--- Jeune, prometteur. Il a beaucoup commercé avec les Génois, moins depuis quelques années. Grâce à un de ses amis, il m'a aidé plusieurs fois à régler de petits soucis avec la Chaîne[^107] d'Acre ou celle de Césarée. Il est en ce moment à Misr[^108], si jamais il repasse par ici, je lui demanderai avis. »

Guido acquiesça. Il jouait très gros dans cette histoire. En plus des marchandises perdues, dont le montant s'élevait à plusieurs centaines de dinars, il risquait de se voir interdire l'abord des côtes chrétiennes. Il ne tenait pas à être frappé d'une marque d'infamie, voire de finir au bout d'une corde. Il n'avait pourtant guère envie de devoir quitter ces rivages qu'il appréciait tant. Le climat de la Méditerranée occidentale lui convenait beaucoup moins, sans même parler de retourner à Pise, qu'il n'envisageait nullement.

### Alexandrie, fondaco pisan, midi du mercredi 24 avril 1157

Guido était épuisé, il avait passé une partie de la nuit à peaufiner des courriers que Jahm al Aswad allait emporter avec lui. Il lui avait fallu temporiser avec de nombreux contacts, en raison de la perte sèche que représentait l'immobilisation de ses marchandises à Ascalon. Il avait auparavant rédigé plusieurs autres lettres destinées à obtenir des informations de la part des administrations latines, faisant suite à celles parties depuis une dizaine de jours.

Ce blocage intervenait au plus mauvais moment. La navigation en Méditerranée venait à peine de reprendre après le long arrêt hivernal et il serait contraint de s'abstenir d'investir dans les trafics de cette saison. Ce qui représentait un gros manque à gagner, d'autant que Ṭalā'i' ben Ruzzīk achetait tous les produits nécessaires à son effort de guerre à un bon prix. Cela n'allait certainement pas durer, que le vizir voie ses plans couronnés de succès ou pas.

Guido avait fait l'inventaire des marchandises qu'il conservait en magasin et c'était maigre. Comme tous ses collègues négociants, il estimait que le moindre stock contrevenait à l'esprit de lucre qui l'animait. Il ne possédait même pas beaucoup de pièces, tout étant placé en participations, en cargaisons ou en balles. Il gérait des flux et des ressacs de biens et de contrats, qui s'alimentaient les uns les autres, prenant au passage sa commission. Il suffisait que l'une des sources se tarisse pour que le fragile ensemble se déséquilibre. Ce qui était en train de lui arriver.

Il finissait de classer ses documents lorsqu'il entendit des voix résonner sous les voûtes devant son logement. Il reconnaissait bien évidemment Francesco, mais pas les intonations graves qui lui répondaient. Il verrouilla la cassette où il resserrait toutes ses notes administratives importantes et s'avança vers l'entrée.

Il remarqua immédiatement la silhouette gracieuse qui accompagnait son ami pisan. Bien que d'un physique délié et impressionnant, l'homme se déplaçait avec élégance. Vêtu d'un simple thwab et d'un durrâ'a aux couleurs bigarrées, il portait sur la tête un turban de soie bleue orné d'un bijou d'argent. Tout en lui respirait l'aisance et la maîtrise. Son faciès ne semblait pas égyptien, mais il était difficile de dire s'il venait de l'Orient ou d'Europe. Son teint mat, son regard sombre et sa barbe noire l'auraient fait passer pour le cousin de n'importe qui autour de la Méditerranée. Guido bomba la poitrine, rentra le ventre et offrit son plus beau sourire, laissant Francesco faire les présentations.

« Guido, mon frère, je te présente al Jāmūs, l'ami d'un ami. »

En indiquant cela, il confirmait que ce marchand faisait partie d'un cercle de confiance, que d'autres étaient prêts à se porter garant pour lui.

Très rapidement ils s'installèrent sur des coussins, confortablement répartis sur des banquettes. Des domestiques s'empressèrent d'aller chercher quelques friandises et disposèrent du leben[^109]. Ne sachant pas si ce négociant était un fidèle scrupuleux, Guido préférait ne pas le provoquer en proposant du vin. Ils commencèrent par discuter des affaires en général, du regain d'activité militaire voulu par les émirs, des conquêtes du nouvel homme fort de Syrie, Nūr ad-Dīn. Al Jāmūs semblait très au fait de ce qui passait dans les ports latins, ce qui était de bon augure.

Francesco finit par s'esquiver, non sans avoir évoqué en partant les ennuis que causait la Chaîne d'Ascalon à certains marchands. Guido put alors enfin aborder le sujet qui l'intéressait.

« Vous avez aussi quelques soucis avec eux ?

--- J'ai déjà eu à verser de l'huile sur les bons verrous.

--- Auriez-vous idée des motifs qui les auraient incités à séquestrer ainsi mes biens ? »

Al Jāmūs réprima un sourire et avala une boulette au sésame grillé avant de répondre. Guido remarqua alors que l'homme n'avait pas tous ses doigts à la main droite, chose qu'il dissimulait adroitement dans les manches de son ample vêtement.

« Croyez-vous que le sire comte de Jaffa aime à savoir des armes pour ses ennemis passer en ses ports ?

--- Comment aurait-il pu apprendre cela ? C'est commerce fort discret...

--- Peut-être parce que je lui ai dit ? »

Guido éprouva un léger vertige. Il n'était même pas certain d'avoir bien entendu. Son invité continuait à picorer d'un air affable comme s'il venait de parler de la météo. Le Pisan avala difficilement sa salive avant de reprendre.

« Que voulez-vous dire ? Je croyais que vous étiez...

--- L'ami d'un ami. De certes. Du moins ce sera à vous d'en décider. Je suis là pour négoce. Comme vous !

--- Je ne comprends rien à ce que vous me dites.

--- C'est pourtant très simple. Vous voulez acheter libre passage dans les ports du comté de Jaffa. Moi je peux vous le vendre. Le commerce dans sa forme la plus pure. »

Guido se sentait soudain en proie à des nausées. L'homme, malgré son ton aimable et son indolence, ou peut-être plus précisément à cause d'eux, lui faisait l'effet d'un serpent prêt à mordre. Il se redressa sur son siège, se frotta le visage.

« Vous avez moyen d'acheter quelque officiel ?

--- Je peux aider certains à regarder ailleurs.

--- Combien cela me coûterait-il ?

--- Quelques papiers, rien de plus.

--- Vous voulez devenir mon associé ? »

La question fit ricaner l'homme, qui secoua la tête en dénégation.

« Je ne vois nul intérêt à vos babioles, mon ami. De simples courriers, mais réguliers m'iraient mieux...

--- Et que vous écrirais-je ?

--- Tout, mon bon. Tout.

--- Qu'est-ce à dire ?

--- Que pour ne plus froisser le comte de Jaffa, frère le roi de Jérusalem, vous ferez l'espion pour lui. » ❧

### Notes

Malgré la situation de guerre endémique au Moyen-Orient, le commerce florissait. On accueillait volontiers tous les négociants, pour peu qu'ils s'affranchissent des taxes et droits. Certaines denrées faisaient néanmoins l'objet de restrictions, permanentes ou temporaires, ce qui n'empêchait pas les plus audacieux de se risquer dans des aventures de contrebande. Si les profits étaient attrayants, les peines encourues, pour être parfois symboliques, étaient suffisamment répulsives pour être prises en compte par la plupart des marchands.

Par ailleurs, le renseignement florissait aussi, bien évidemment dans ces milieux qui avaient accès aux deux camps de façon quasi libre. Tous les opposants s'appuyaient donc sur des espions et informateurs, épisodiques ou réguliers. Même si la lenteur des moyens de communication et la difficulté de contact en rendaient l'efficacité toute relative, chacun tissait sa toile de façon à en savoir le plus possible sur ce qui se passait chez ses adversaires. La discrétion nécessaire de ces pratiques fait qu'il est désormais extrêmement compliqué d'en esquisser l'importance et l'impact réel, surtout en dehors du renseignement purement militaire lors des campagnes.

### Références

Broadhurst Roland, *The Travels of Ibn Jubayr*, London : Cape, 1952.

Elisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Nobilis in nobili
-----------------

### Jérusalem, tour de David, veillée du mercredi 27 août 1158

Noyé dans un océan écarlate, le soleil finissait de s'effacer derrière les collines de Judée. Appuyé sur le bord d'un créneau, le connétable Onfroi de Toron admirait le spectacle en silence, faisant rouler doucement son gobelet de vin sur la pierre tiédie au long de la journée. Il sentait la fraîcheur de la nuit progresser sur son échine de soldat, remplaçant la chaude moiteur qui l'avait baigné depuis le matin. Il se retourna, étira lentement son dos massif en grognant tel un ours, puis passa une main puissante dans ses cheveux afin d'en repousser l'épaisse toison en arrière.

Se lissant la moustache, il lança un œil circonspect sur les tablettes de cire parsemant le plateau devant lui. Puis il leva la tête vers la dernière silhouette encore présente avec lui sur la terrasse de la forteresse : Régnier d'Eaucourt. Un chevalier comme il les appréciait. De condition modeste, il était d'une loyauté sans faille envers ses maîtres. Et, surtout, c'était un homme de guerre, de ceux qui savaient tenir en selle sans pour autant céder trop aisément à l'euphorie d'une charge bien appuyée.

« Alors, d'Eaucourt, en avons-nous vu assez ? Ou demeurent encore quelques garnisons mal fagotées ?

--- Je ne crois que nous ayons oublié aucun point d'importance... »

Le connétable haussa les épaules, faisant danser les motifs de soie de sa cotte.

« Nous avons moindre urgence que par l'an passé. »

Classant les derniers inventaires de garnison par fief, Régnier hocha doucement la tête puis se versa de quoi se désaltérer, un vin brillant dans les reflets des flammes des lampes à huile. L'été précédent, ils avaient failli perdre le roi, en plus de nombreux combattants de grand renom, lors de la bataille de Meleha.

« Vous avez idée de la raison qui me fait monter ici pour travailler ?

--- C'est là plus belle forteresse de la cité, propre à plaire à tout mestre de guerre.

--- Voilà bon sens incarné. De fait, j'ai surtout plaisir à mirer les terres que nous avons en garde. D'ici, mon regard porte au loin et je peux voir la presse des pérégrins impatients de découvrir la ville sainte entre toutes. Cela me rappelle l'importance de ce que nous faisons. Nous sommes des gardiens, d'Eaucourt. Les clercs se disent pasteurs du troupeau et, dans ce cas, nous sommes les féroces mâtins qui veillent. Notre tâche, pour être moins sale que celle du commun, n'en est pas moins épuisante, bien au contraire. »

Il posa son verre, fit mine de regarder les tablettes de cire et rouleaux répandus sur la table puis retourna s'appuyer à un des créneaux, après en avoir caressé les impressionnants moellons.

« Durant la chevauchée en terre de Suète, avez-vous ouï parler les hommes à propos du maréchal ? »

Régnier fut un peu décontenancé par la question, écarquilla les yeux et bafouilla un peu avant de répondre.

« Pas que j'en ai souvenance... D'aucuns auraient-ils eu souci avec lui ? Vous en a-t-il fait mention ?

--- Si seulement ! Il ne vient m'entreparler que pour geindre de se voir en tel office. »

Régnier n'osa pas rebondir sur l'affirmation, mais il était un peu choqué. Être maréchal constituait une des plus belles récompenses pour les chevaliers de la maison du roi. C'était un office clef, très envié, car prestigieux. Mais bien souvent il se voyait attribué en fonction d'impératifs politiques. C'était le cas avec Joscelin d'Édesse, jeune héritier sans terre, dont le nom était la seule fortune. L'année précédente, beaucoup avaient accueilli avec soulagement la capture du bouillonnant et tonitruant Eudes de Saint-Amand, pour découvrir avec dépit l'arrivée d'un novice à ce poste.

Le connétable joua en silence quelques minutes avec son imposante moustache puis commença à faire les cent pas sur les dalles dont l'ocre avait cédé la place à la grisaille de la nuit.

« Vous avez usage de le croiser, vu votre proximité avec le comte de Jaffa. Auriez-vous idée de ce qu'il désire, de la façon dont nous pourrions le contenter ?

--- Il n'a pas plus grand désir que de reconquérir les terres perdues par les siens au nord. Il en parle souvent, surtout depuis qu'Harim est de nouveau chrétienne. Il se verrait bien lancer la reconquête depuis ce bastion.

--- Insoucieux tel son père ! Où compte-t-il trouver soudards et chevaliers pour cela ?

--- Ce n'est encore que projets en son esprit.

--- Fol espoir de béjaune, oui ! J'ai besoin d'un roc à mes côtés, pas d'un trouvère qui rêve de folies. Le maréchal, c'est un peu le cellérier de notre moustier. Sans lui, bien difficile de tenir l'ost. Sa bannière doit inspirer le respect et la ferveur chez nos hommes, outre la terreur chez nos adversaires... »

Régnier rangea quelques tablettes dans leur coffret, réunit plusieurs rouleaux dans des étuis, hésitant à proposer une idée. Elle lui était fort déplaisante, mais il se sentait en devoir de la mentionner.

« N'avons-nous pas espoir que le sire Eudes ne soit libéré pour bientôt ? On m'a dit que sa rançon était fixée. Joscelin pourra retrouver sa liberté sans que cela nuise à quiconque.

--- Crois-tu vraiment que j'ai désir de voir Saint-Amand de retour à ce poste ? Il est là où il est par la folie de ses actions, je n'ai guère désir de le reprendre. »

Sentant qu'il s'était emporté, il lâcha un sourire.

« C'est bien là mon souci : Joscelin sera bien trop heureux de laisser la place à Eudes. Il me faut donc en changer avant que l'ancien maréchal ne quitte les geôles sarrasines. Mais cela ne peut se faire sans subtilité. D'autant que j'aimerais avoir cette fois quelqu'un qui sache faire autre chose que poingner à hue et à dia. »

### Jérusalem, palais royal, chambre du roi, matin du vendredi 17 octobre 1158

La pièce était littéralement sens dessus dessous. Des coffres gisaient un peu partout, la gueule largement ouverte pour tenter d'engloutir la déferlante d'étoffes et d'accessoires. Une nuée de valets tourbillonnait habilement entre les meubles, répartissant les délicates tenues selon les indications lancées par un clerc à l'allure négligée et l'air affairé.

Au milieu de cet ouragan, la haute et solide silhouette du roi Baudoin impressionnait de tranquillité. Il était vêtu d'une cotte à motifs en écailles dessinant des reflets moirés à chaque mouvement. Devant une des fenêtres, il finissait de tailler son épaisse barbe blonde à l'aide de minuscules forces[^110] tout en jouant avec un miroir de bronze scintillant dans la lumière du matin. Comme de nombreux jeunes, il cédait assez volontiers à la mode et accordait une grande attention à son allure. Ce qui expliquait l'impressionnante armada de bagages qui se préparait en vue de son voyage vers Antioche où il devait rencontrer l'empereur byzantin Manuel Comnène.

Sans même ralentir à l'entrée dans la pièce, son frère Amaury, comte de Jaffa, l'interpella gaiement. La bonne entente entre eux était de notoriété publique et s'illustrait parfaitement dans la simplicité de leurs relations. En dehors des cérémonies officielles et solennelles, ils se comportaient en parfaits complices, l'aîné admonestant son cadet pour ses plaisanteries comme dans la plus modeste des familles. Il n'était que le sujet de leurs parents pour les faire se disputer, sans que cela ne porte jamais à conséquence.

« Alors, mon frère, te faut-il connétable pour choisir tes bagages ? »

En disant cela, Baudoin sourit à Onfroy de Toron. Il appréciait l'homme autant que l'officier et se félicitait chaque jour de l'avoir à ses côtés. Le baron plia le genou de façon rapide, mais appuyée, tandis qu'Amaury s'approchait pour étreindre son frère avec la douceur d'un ours. Baudoin lui pinça les côtes en riant :

« Mon frère, il va te falloir prendre garde à ne point trop emplir tes bliauds ! Il ne sied pas pour baron d'être gras comme moine.

--- D'aucuns finissent rois avec bedaine à faire pâlir un évêque et n'en sont pas moins fort batailleurs. »

Baudoin posa ses affaires sur un escabeau et invita les arrivants à s'installer sur des banquettes habillées de coussins, dans une vaste niche décorée de céramique vernissée. Lui-même prit place sur une chaise curule, sur une estrade face à eux. Il aimait à tenir conseil ainsi, simplement, avec ses proches.

« Les préparatifs vont-ils bon train ? J'ai grande crainte à laisser le prince d'Antioche seul avec le *basileus*.

--- D'après nos coursiers, l'ost des Grecs n'est pas encore annoncé, le rassura Onfroy.

--- Y a-t-il quelque mauvaise nouvelle à me conter, que je vous vois faire si triste figure tous les deux ? »

Amaury concéda un sourire sans joie et le rassura bien vite.

« Ce n'est pas novelté que je vais te narrer. As-tu pourpensé de mon frérastre[^111] ?

--- J'avoue ne pas y avoir vu urgence. Il me semble tenir son rôle fort justement. Pourquoi changer d'officier ? Je suis surpris de te voir ainsi pousser ta famille au loin, Amaury.

--- C'est bien de cela qu'il s'agit. Joscelin n'est pas homme de joug, tu le vois bien. Il a grand désir de reprendre les fiefs de ses pères. »

Baudoin se redressa dans son siège. Il avait nommé le jeune Courtenay, car il tenait à montrer son attachement aux vieilles familles du royaume. Son père n'avait que trop cédé à la tentation de faire venir des concitoyens angevins qu'il avait favorisés. Mais Joscelin Courtenay s'avérait inadapté à son poste et n'avait guère estimé le geste à sa juste valeur. De tout son être, il se sentait encore comte d'Édesse et n'avait nul goût à servir un autre, fût-il le puissant et prestigieux monarque de Jérusalem.

« J'entends bien cela. Mais vu que Saint-Amand devrait être des nôtres d'ici peu, il ne me sera alors guère aisé de lui refuser le poste vacant. Est-ce bien ce que nous voulons ?

--- Mon sire Baudoin, il me semblerait bien déraisonnable de lui permettre d'officier là où il a fort mal servi. À Panéas, il s'en fallut de peu que ses façons ne mènent au désastre. Il est de ces hommes à la vaillance si grande qu'il croit chacun forgé du même acier que lui. »

Baudoin sourit à la remarque de son connétable, capable de vanter les qualités d'homme qu'il cherchait à évincer. Amaury s'avança de façon à prendre la parole.

« J'ai peut-être bonne solution à ce dilemme, mon frère. Tu as souvenance de Guillaume, frère de Philippe de Cafran ?

--- Comment oublier un de ces vaillants compagnons qui a percé ma voie à Meleha ?

--- Celui-là même. C'est un cousin d'Hugues d'Ibelin, comme tu le sais, donc de bonne fame. Ses qualités en font un compagnon apprécié, aussi solide en selle que vaillant dans le behourd[^112]. Outre, il est dans ton hostel depuis fort longtemps et a prouvé maintes fois ses qualités. J'en ai parlé au sire connétable, qui a reconnu voir d'un bon œil de recevoir l'hommage d'un tel homme pour devenir maréchal. »

Baudoin comprenait que cette réunion avait été bien préparée. Il appréciait beaucoup Guillaume et savait qu'il ferait un officier de valeur. Mais nommer un simple chevalier sans fief tel que lui, sans soutien à la Haute Cour, lui ferait s'aliéner une partie des nobles partisans des nouveaux venus arrivés avec son père. Il secoua la tête lentement.

« J'entends bien. Seulement, avec un homme de ma parentèle comme maréchal, il m'est loisible de refuser de rendre son poste à un vaillant homme comme Eudes ! Mais comment ne verrait-il pas un affront de se voir remplacer par autre que grand baron ? Ce serait le désavouer de bien méchante façon. Je n'ai nulle envie de m'aliéner une partie de la Cour, déjà fort marrie de sa perte d'influence depuis que je porte la couronne. Pour l'heure, j'ai un maréchal qui me sied, et bonne excuse pour refroidir les ardeurs batailleuses de Saint-Amand. Nous démêlerons cela si, d'aventure, nous avions grande chevauchée à organiser. »

Il se leva pour signifier leur congé aux deux hommes. Il avait déjà pas mal de soucis à la perspective de devoir naviguer auprès du puissant Manuel Comnène dans les mois qui allaient venir. Il ne tenait pas à partir avec l'idée que le royaume se déchirerait en son absence. Il était conscient des carences de son administration militaire en l'état actuel des choses, mais il avait d'autres priorités.

### Antioche, porte Saint-Georges, midi du mercredi 27 mai 1159

Le souffle court et le visage empourpré, Philippe de Milly jaillit de l'escalier avec enthousiasme. Sa mince silhouette était accentuée par un haubert empoussiéré et il arborait son épée au côté. Avec la chaleur, il n'avait pas noué la coiffe et les boucles de sa chevelure brune dansaient en couronne. Son imposante barbe se déployait sur sa poitrine, camouflant la petite croix reliquaire qui ne le quittait jamais.

Sur la plateforme, abrité sous un auvent de toile, le connétable finissait son repas en dégustant des fruits au vin tout en admirant la vue depuis les murailles qui s'élançaient à l'assaut des reliefs, vers le nord.

« Sire connétable, toujours installé en la plus haute tour ! »

Onfroy sourit en entendant la voix. Philippe de Milly, son cadet de quelques années, conservait avec les ans son inlassable énergie. Plus jeunes, il s'étaient défiés mainte fois sur la lice. L'un comme l'autre, ils appréciaient ceux capables de rompre le frêne avec adresse. Le courage physique unissait ces hommes d'action.

« N'est-ce pas le propre du baron que de chercher à s'élever ? se moqua-t-il. Venez donc goûter ces friandises, elles effaceront bien vite le goût de la poussière en votre gosier.

--- Ce sera avec grand plaisir ! Je suis en selle depuis potron-minet, dans l'espoir de vous encontrer.

--- J'ai espoir que vous ne portez nulle mauvaise nouvelle ! La blessure du roi se remet-elle bien ?

--- De certes ! N'ayez nulle crainte à ce sujet. Les meilleurs physiciens grecs veillent sur lui. »

Le seigneur de Naplouse se laissa lourdement tomber sur un coffre ferré et entreprit de dénouer son baudrier, qu'il portait à l'ancienne.

« J'ai quelques petites choses à faire en la cité de saint Pierre[^113] et, surtout, je voulais vous faire part de quelques réflexions qui pourraient soulager le roi.

--- Je ne vous savais pas savant en médecine, mon ami.

--- Et encore, vous ne savez pas tout ! » s'amusa Philippe.

Il avala une longue rasade de vin avant de déchirer un des fruits qu'il engloutit sans y prêter garde.

« Vous avez appris qu'au chevet du roi alternent Saint-Amand et Courtenay ? Le premier pour retrouver l'office que dédaigne le second. »

Il continua sans se formaliser du soupir éloquent d'Onfroy.

« Nombreux sont ceux qui verraient d'un bon œil l'arrivée de Guillaume de Cafran, mais c'est là affaire délicate. »

Il s'assit sur le bord du coffre, se pencha vers son interlocuteur.

« Il suffirait pour cela d'offrir plus bel os à ronger à Saint-Amand. Un rôle prestigieux, qu'il ne saurait refuser. Mais où son impétuosité se verrait bridée.

--- Auriez-vous idée d'en faire un évêque ? gloussa le connétable.

--- Presque. Depuis que le sire roi Baudoin a démis Rohard, nul n'est plus châtelain et vicomte de Jérusalem. Les deux offices sont bien distincts. Quelle marque d'honneur et de confiance que de regrouper de nouveau cela en faveur d'un homme. »

Il sourit à son idée, puis vida son verre d'un train. Onfroy nota avec désagrément que le chevalier ne semblait jamais accorder le moindre intérêt à ce qu'il avalait, fût-ce les mets les plus raffinés. Il prit le temps de déguster une gorgée, se rassurant sur la qualité de sa boisson.

« L'idée est habile, je le concède. Seulement, nous avons hommes de valeur en ces postes. Nous ne pouvons les démettre comme cela...

--- J'en suis bien d'accord. Le vicomte Arnulf est féal et de valeur. Il pourrait devenir châtelain de Blanchegarde, mon sire Amaury a besoin d'une personne de fiance là-bas.

--- Et Tolenth ? Il n'a guère dérogé...

--- De là viennent toutes ces idées. Nous pourrions faire d'une mauvaise nouvelle une bonne. Avec le décès de Bernard Vacher, le roi n'a plus de porte-bannière officiel. Que voilà heureuse opportunité pour un Normand sans appui comme Tolenth ! »

Onfroy de Toron hocha la tête. Il avait accueilli la nouvelle du décès de Bernard Vacher avec beaucoup d'affliction. Le vieux chevalier était un des rares avec qui il s'amusait à parler arabe. Sans l'avoir exprimé très souvent, il partageait fréquemment les analyses diplomatiques et stratégiques de Vacher. Bien que jamais cela n'ait amoindri sa vaillance lors des combats, il n'avait jamais manifesté le dédain et la violence de certains envers leurs adversaires. S'il pouvait sortir quelque avantage de cette disparition inattendue, cela lui mettrait un peu de baume au cœur.

« Parfois, je me demande lequel de votre fratrie est le plus habile à dénouer pareils imbroglios, mon ami.

--- J'ai grande amitié pour le sire comte de Jaffa, chacun le sait. Et je dois avouer que je retrouve en Baudoin quelques traits de Mélisende, à qui je conserve loyauté. J'ai donc espoir qu'il finira par apprécier mon dévouement. »

Onfroy leur versa un peu de vin à tous les deux. Il aurait fallu plus d'hommes de la trempe du seigneur de Naplouse, du genre à apporter des solutions plutôt que des problèmes. Il leva son verre en un toast.

« L'idée m'agrée fort. Essayons de la soumettre à notre sire roi Baudoin. » ❧

### Notes

Même dans une monarchie comme à Jérusalem, le pouvoir se tissait d'une conjonction de courants hétérogènes, voire antagonistes, que les souverains s'efforçaient de diriger dans le sens de leurs aspirations. La terre sainte vit par exemple apparaître de profondes différences entre ceux dont les familles étaient arrivées en début de siècle et ceux qui débarquaient au fil du temps. Le roi Foulque, en particulier, eut tendance à faire venir avec lui du pays angevin et de ses environs un certain nombre de fidèles à qui il distribua les honneurs, au détriment des nobles locaux, d'origine normande en particulier avec qui il avait un contentieux depuis toujours. Par la suite, le conflit entre Foulque et la reine Mélisende puis entre celle-ci et son fils Baudoin alimentèrent les dissensions et les fractures.

Le fait que de vastes territoires s'offraient à la conquête aiguisait fortement les appétits. En outre, le roi eut sans cesse à composer avec les barons, ceux qui formaient la Haute Cour, les feudataires les plus prestigieux, généralement grands propriétaires (sans même parler des différentes puissances ecclésiastiques et des ordres religieux militaires). De nombreuses initiatives furent prises au fil du temps pour tenter d'appuyer le pouvoir royal directement sur les seigneurs locaux, de façon à s'affranchir du niveau intermédiaire de ces princes. Mais, comme la fin du premier royaume le démontrera largement, ce fut avec un succès extrêmement relatif.

Le destin de tous les officiers de la couronne est souvent très difficile à connaître et seules des traces diffuses en marquent parfois l'existence. Ce n'est pourtant qu'en s'attachant au parcours de ces personnages de second plan qu'on arrive à démêler l'extraordinaire richesse politique de la période médiévale. Des historiens précis et méthodiques parviennent malgré tout à en esquisser une image féconde à la suite d'une étude serrée de sources aussi arides que de simples chartes de donation. Pour le XIIe siècle des croisades, le travail de Hans Eberhard Mayer est, sur ce point, exemplaire.

Les lecteurs m'excuseront du jeu de mots du titre, faisant référence bien évidemment à *Vingt mille lieues sous les mers* et la devise du capitaine Nemo. Contrairement à ce dernier, les personnages du cercle que je dépeins ici cherchent avant tout à se faire un nom. Il n'en fallait pas plus pour que l'idée de la formule germe dans mon esprit.

### Références

Barber Malcolm, « The career of Philip of Nablus in the kingdom of Jerusalem », dans Peter Edbury et Jonathan Phillips, éd., *The Experience of Crusading*, vol 2 : Defining the Crusader Kingdom, Cambridge University Press, 2003, p. 60-75.

La Monte John, « The viscounts of Naplouse in the twelfth century », dans *Syria*, 1938, vol.19, no.3, p. 272-278.

Mayer Hans Eberhard, "Angevins versus Normans: The New Men of King Fulk of Jerusalem", dans *Proceedings of the American Philosophical Society*, vol.133, n°1, Mars 1989, p. 1-25.

Mayer Hans Eberhard, « The Wheel of Fortune: Seignorial Vicissitudes under Kings Fulk and Baldwin III of Jerusalem » dans *Speculum*, vol.65, no. 4, 1990, p.860-877.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem » dans *Dumbarton Oaks*, vol.26, 1972, p.93-182.

Prawer Joshua, *Crusader Institutions*, Oxford University Press, Oxford, New York : 1980.

Rey Édouard Gabriel, *Les familles d'Outre Mer de du Cange*, New-York, Burst-Franklin, 1869.

Kleos
-----

### Naplouse, demeure de la reine mère, soirée du lundi 19 avril 1154

La porte de la chapelle grinça dans le silence qui avait suivi le *Ite missa est* final. En même temps que la lumière entra une haute silhouette puis résonnèrent des éperons choquant les dalles de pierre au rythme d'un pas décidé. Au sein de la petite assemblée, toutes les têtes se tournèrent vers la femme frêle au plus près de l'autel, dans l'attente de sa réaction. Avec une grâce maîtrisée, elle fit voleter son riche bliaud, réajusta son voile et, levant le menton, dévisagea le nouvel arrivant avec autorité. Elle arbora son sourire le plus large en découvrant le visage familier de Bernard Vacher, un des chevaliers de l'hôtel du roi.

« Que voilà belle surprise, votre visite en mon exil ! »

Avant de répondre, l'imposant chevalier s'agenouilla en un vacarme de métal.

« Votre Majesté, je vous apporte le salut de votre fils notre roi Baudoin. Je suis en route pour le nord. »

Mélisende leva une main diaphane et invita le vieux serviteur de la couronne à se relever et à l'accompagner tandis qu'elle se dirigeait vers la sortie.

« Aurez-vous temps de demeurer avec moi pour le souper ? Si ce n'est pour la veillée ?

--- J'en aurais goûté l'honneur, mais il me faut joindre Sidon au plus vite, presser le rappel auprès du roi.

--- Le sire Géraud n'est pourtant généralement pas le dernier pour porter le fer à nos ennemis.

--- Certes pas. Seulement, devoir fuir avec les galées à Ascalon l'a fort marri. Lors de la semonce, il était occupé en ses affaires nautiques, loin de penser à lever son ban pour joindre l'ost royal en si grande urgence. »

Mélisende hocha la tête lentement en silence et continua d'avancer, réfugiée dans ses pensées. Elle plissa les yeux lorsqu'elle se retrouva dans le jardin, mais ne ralentit pas son allure. Indifférente à la troupe qu'elle entraînait dans son sillage, elle se dirigea d'autorité vers sa chambre, gravissant un petit escalier sans plus adresser la parole à Bernard Vacher dont le pas pesant l'accompagnait.

Arrivée dans ses appartements, elle montra d'un doigt autoritaire un escabeau à son invité puis s'assit sur un coffre au pied de son vaste lit. Entre-temps, sa suite s'était évaporée dans les différents espaces qu'ils avaient traversés. Comprenant que le soldat venait à peine de quitter la selle, la reine fit mander une aiguière et un bassin, qu'il puisse se rafraîchir, ainsi que des boissons et une collation.

Tandis qu'il se frottait les mains et se débarbouillait le visage, elle s'entretint quelques instants avec un de ses clercs. Elle donna également des instructions pour qu'on s'occupe au mieux des suivants du messager. Lorsqu'elle en eut fini, Bernard Vacher attendait tranquillement, sa silhouette avachie sur le petit siège, mais le regard vif à l'affût derrière ses paupières lourdes.

« Portez-vous missive à mon intention ?

--- Aucunement. Le roi et le sire connétable assemblent les troupes pour assaillir Nūr ad-Dīn[^114] et libérer Damas.

--- Croyez-vous qu'il y ait vraiment un risque de voir le Turc prendre la cité ?

--- Plus que jamais ! Ses émirs sont là depuis le début du mois et il a de nombreux alliés dans la place. De toute façon, il a éliminé ceux qui pouvaient s'opposer à lui.

--- Ô combien ! Muğīr n'est que l'ombre de Mu'īn ad-dīn Anur. Il n'est pas de taille. »

Bernard Vacher acquiesça gravement, avalant un peu de vin.

« Savez-vous quand les nôtres arriveront là-bas ?

--- Le sire connétable misait sur la saint Georges[^115]. Cependant nul n'y croit vraiment. Moult éperonnent en tous sens pour clamer l'urgence, mais cela demeure trop court. Outre, trop de mauvais souvenirs demeurent des dernières campagnes en ces lieux.

--- Vous savez, Bernard, que j'ai parlé contre l'attaque faite à Damas lors de la venue des rois Louis et Conrad[^116]. Mais la situation était différente, alors. Nous avons pris Ascalon l'été dernier, Damas est notre prochaine priorité.

--- Notre sire Baudoin a bien conscience de cela. Seulement, la cause est difficile. Les caisses sont vides et nul puissant baron croisé pour nous appuyer. »

La reine mère se raidit un peu, accentuant son allure empesée et hiératique. Elle avait toujours eu du respect pour le porte-bannière royal. Il était un fidèle serviteur de la couronne, peu enclin à pousser son avantage. En outre, il avait une vision assez éclairée des différentes forces en présence. Enfin, il savait rester à sa place. Elle qui avait été éduquée pour régner par un père exigeant appréciait ce genre de subordonnés. Il fallait juste de temps à autre leur rappeler qu'ils n'avaient pas toutes les clefs et les capacités pour diriger.

« J'encrois plutôt qu'il est bon que nul puissant de France, de Flandre ou de Germanie ne soit parmi nous pour en demander l'octroi. Le comte Thierry[^117], mon cousin, est féroce batailleur, mais aussi fort gourmand de terres. Je préfère que le royaume crée quelque nouvelle baronnie féale. Nul besoin de principauté avide d'indépendance. »

Le chevalier concéda un sourire. Il comprenait le point de vue de Mélisende, mais il doutait de la force royale en ce moment. La prise de Damas ne serait pas aisée, quand bien même le seigneur d'Alep en serait repoussé sans trop de dommages.

### Naplouse, jardins de la demeure de la reine, après-midi du lundi 26 avril 1154

Les voix angéliques des ecclésiastiques s'élevaient vers le ciel sans rencontrer aucun autre obstacle que de maigres nuages cotonneux. Installée autour de la fine silhouette de Mélisende sous une pergola verdoyante, une assemblée de femmes écoutait avec attention. Peu d'entre elles comprenaient les paroles arméniennes des chants, mais la douleur qu'ils exprimaient était palpable. L'évêque Nersès[^118] était un des nombreux correspondants de la reine et il avait fait porter sa dernière missive par de jeunes clercs instruits de sa création *Lamentation sur Édesse*.

Aux alentours, des jardiniers s'activaient à biner les carrés de fleurs et d'aromatiques. Dans la cour prolongeant les espaces verts, un petit groupe de valets et de lavandières battait avec vigueur des couvertures et des oreillers. Le printemps offrait les plus belles journées, avant les chaleurs sèches de l'été et après les rigueurs de l'hiver. La plupart des serviteurs appréciaient donc de se voir attribuer des tâches à l'extérieur.

Parmi la domesticité de la reine, Gui n'était pas de ceux-là. Le secrétaire n'aimait rien tant que le confort des bâtiments, peu enclin à s'exposer aux éléments, quels qu'ils soient. Son teint blafard attestait d'ailleurs bien de cette affinité. Il avait malgré tout pris sur lui de sortir, car il avait une nouvelle urgente à annoncer. Urgente et mauvaise, de celles qu'il ne prenait jamais plaisir à délivrer. Il savait suffisamment bien ses classiques pour redouter l'accueil qui lui serait fait.

Le sourire qu'il arborait donc en s'inclinant devant la reine n'était que de circonstance et suffisamment emprunté pour que Mélisende en fronçât immédiatement ses fins sourcils. Elle lui fit signe de s'approcher pour qu'il lui parle à l'oreille.

« La cité de Damas a été forcée par Norreddin, majesté. Un espie en route pour Jérusalem vient de faire halte pour changer de monture en nos écuries. »

Écarquillant les yeux de mécontentement, Mélisende fit faire silence d'une voix autoritaire. Puis elle darda son regard impérieux sur le vieil homme à la posture contrariée face à elle.

« Que me dis-tu là ? L'ost royal n'a-t-il pas mis bon ordre à cela ?

--- Il faut croire qu'il n'en a pas eu l'heurt. C'est un sergent le roi qui portait la nouvelle. »

Les yeux de la reine lançaient des éclairs et, rapidement, les rangs s'éclaircirent autour d'elle. Personne n'avait le désir de rester dans les parages quand elle voyait ses desseins contrariés. Hésitant à se dérober, Gui dansait d'un pied sur l'autre, tordant ses mains fines devant sa panse rebondie.

« Baudoin est-il de retour ? Où se trouve-t-il en ce moment ?

--- Il est demeuré avec l'ost, mais j'encrois qu'il ne demeurera pas dans le nord. Le Turc va de certes passer un moment à fortifier sa prise.

--- A-t-il porté grand mal à l'enceinte ? Aux troupes ? Baudoin n'a pas désir d'assaillir à son tour ? Fais donc venir ce messager que je l'interroge.

--- Il a juste changé de monture et s'en est allé. »

La reine serra les poings de se voir ainsi mise à l'écart. Il était un temps, encore pas si lointain, où les coursiers lui faisaient rapport, où les informations convergeaient vers elle. Elle se contint et respira lentement, attrapant un second voile pour s'en couvrir afin de déambuler jusqu'à la galerie. D'un regard elle fit comprendre à son secrétaire de l'accompagner.

« Nous partons. Je veux accueillir mon fils, le roi, quand il arrivera.

--- Croyez-vous qu'il fera halte en nos murs ?

--- J'en doute fort, c'est à nous d'aller l'encontrer.

--- Nous n'avons guère d'hommes, ma reine, ils sont tous avec l'ost royal.

--- Nous voyagerons légers. Mande le minimum de mes gens et peu d'affaires, je ne pense pas demeurer long temps à Jérusalem. »

Voyant la surprise sur le visage de son fidèle serviteur, Mélisende esquissa un sourire sans joie.

« Je n'ai plus loisir de diriger le royaume comme reine. Mais Baudoin demeure mon fils et il aura besoin des conseils de sa mère. Hâte-toi donc ! »

Abandonnant son secrétaire à l'entrée de ses appartements, la reine se dirigea vers la fenêtre à claustra qu'elle appréciait tant, ouvrant vers la cité de Naplouse et la vallée occidentale. Sans la voir, elle savait la mer proche. Si Damas était désormais aux mains d'un pouvoir fort, il fallait oublier l'espoir de s'en emparer. Elle repensa à la grande expédition lancée quelques années plus tôt et eut quelques regrets de s'y être opposée, inquiète qu'elle était du destin du royaume à l'issue d'une telle prise en présence de nombreux croisés. Mais elle ne s'attarda pas à ces mornes réflexions. Elle n'était pas du genre à se lamenter, comme Nersès, pas même de la perte de son comté natal. Femme d'action, elle échafaudait déjà de nouveaux plans de conquête.

### Naplouse, demeure de la reine, soirée du jeudi 17 juin 1154

La haute table n'accueillait que deux convives en dehors de la reine : Amaury, le comte de Jaffa, fils cadet de Mélisende et Geoffroy Foucher, un Templier à la barbe rase. Son visage anguleux et quasiment décharné contrastait avec les formes plus rebondies du jeune prince. Ils s'entendaient néanmoins très bien, amis qu'ils étaient depuis de nombreuses années. Geoffroy était un familier de Mélisende depuis son arrivée en Terre sainte, recommandé par son lointain cousin Bernard, abbé de Clairvaux[^119].

Amaury se sentait d'une humeur badine et il n'avait cessé d'argumenter auprès de sa mère pour qu'elle se décide à confier la copie de volumes techniques à son scriptorium. Elle avait en effet attiré à elle de nombreux calligraphes et miniaturistes de talent, sans compter les relieurs et graveurs, qui produisaient des manuscrits de qualité. Amaury appréciait également les livres, plus pour leur contenu que pour leur aspect, et trouvait dommage de se contenter de textes sacrés alors qu'il existait si peu de d'exemplaires d'ouvrages essentiels selon lui. Mélisende supportait avec affabilité ses assauts, sans jamais manifester le moindre agacement, bien qu'il se montrât bien plus insistant qu'elle ne l'aurait accepté de quiconque. Venant de ce fils, rien ne semblait jamais la contrarier. Elle finit malgré tout par lui répondre de façon sarcastique.

« Hé bien, que ne lances-tu pas un assaut depuis Ascalon vers Alexandrie ? Il s'y trouve peut-être quelques beaux vestiges de la bibliothèque des Romains.

--- Si fait, mère. J'en ai l'idée, un jour. Toutefois, le désert fait robuste muraille, je finirai par m'échouer dans les sables.

--- Alors demande à nos bien-aimés frères de la milice. Ils savent vaincre au parmi des dunes, n'est-ce pas, Geoffroy ?

--- Majesté,vous montrez trop de mansuétude. Ce n'était qu'escarmouche contre des fugitifs. Elle aura belle répercussion, mais c'était loin d'être l'armée califale[^120].

--- Gageons que votre cousin André saura faire fructifier tout cela. Il ne faudrait pas faire deux fois la même erreur. »

Geoffroy reposa sa cuiller et prit le temps de réfléchir à la dernière remarque de la reine. Elle parlait souvent ainsi, par allusion, sans jamais préciser le fond de sa pensée. Il avait pour elle beaucoup d'amitié, mais ne se sentait jamais totalement à l'aise devant cette femme de pouvoir. Il préférait l'environnement rude des guerriers du Temple auquel il était habitué. Il savait qu'on reprochait grandement à son ordre la façon dont l'assaut sur Ascalon s'était déroulé. Il se murmurait même qu'ils avaient tenté d'en accaparer la prise, ce qui avait mené à la capture et l'exécution de plusieurs dizaines d'entre eux par les forces musulmanes, dont le grand maître lui-même, Bernard de Tramelay. Heureusement, André de Montbard, un des fondateurs, était pressenti pour lui succéder et nul ne doutait de sa droiture et de sa vaillance. Jamais la reine n'aurait commis l'affront d'évoquer un tel souvenir devant un homme familier d'André, qu'elle appréciait entre tous. Elle avait donc une autre intention en disant cela.

Pendant ce temps, Amaury avait déjà enchaîné et parlait des richesses documentaires qu'il espérait voir advenir d'une conquête de l'Égypte. En tant que seigneur de Jaffa et d'Ascalon, il était le premier baron concerné. Il envisageait de s'appuyer sur les Bédouins de ses régions pour faciliter le passage vers le delta du Nil.

« Il sera bien utile de ne point se les aliéner, sire comte, j'en conviens, mais pour moissonner le blé d'Égypte, cela ne saurait suffire.

--- Les miliciens du Temple ont-ils quelque secret stratagème pour conquérir l'endroit ? se moqua amicalement Amaury.

--- Pas à ma connaissance. Je parlais de mon propre chef. Si je devais me lancer à l'assaut de ces terres, je me garantirais le contrôle des mers avant tout. Géraud de Sidon, malgré sa vaillance, n'a pas eu le front de s'opposer aux galées du calife. Elles vont et viennent comme des frelons sur nos côtes et nous n'avons que nos murailles et nos chaînes à leur opposer. »

Mélisende sourit au bon sens de la remarque. Elle se tourna vers son invité, intercepta son regard.

« Nous manquons de bois pour avoir belle flotte, c'en est misère. Comment pourrions-nous avoir suffisamment de naves pour les vaincre en mer ?

--- Je ne sais, à dire le vrai. Mais à Ascalon, nous avons failli être défaits par leur forçage du blocus. Imaginez ce que ce serait si nous étions de l'autre côté du désert, en leurs provinces, avec nos maigres vaisseaux à leur merci. »

La reine dévisagea alors son jeune fils, satisfaite de voir qu'il écoutait avec attention et semblait réfléchir à cela. L'Égypte était en pleine déliquescence et risquait de sombrer à son tour dans le giron d'un pouvoir prêt à s'en emparer. Cette fois, il s'agissait d'une région encore plus cruciale que Damas. Ce n'était pas juste quelques plaines maraîchères et céréalières qui étaient en jeu, ni même la frontière naturelle du désert, mais une zone extrêmement riche, nœud commercial et porte entre la Méditerranée et les mers d'Orient. Et, surtout, bien qu'elle soit gouvernée par des musulmans, une large frange de sa population était chrétienne.

Geoffroy avait indiqué que lors de l'affrontement aux abords de l'Égypte, Usamah ibn Munqidh avait réussi à passer à travers les mailles du filet. Connaissant le vieux renard, il avait sûrement trouvé refuge chez ses anciens amis à Damas. Soucieux de toujours flatter les puissants, il saurait vanter au nouveau maître de la ville la richesse des territoires à conquérir à l'Ouest.

### Naplouse, demeure de la reine, veillée du lundi 16 avril 1156

Philippe de Naplouse étira sa longue carcasse tout en bâillant. Il était fatigué des derniers jours passés en selle et, si ce n'était pour égayer un peu la soirée de la reine, il aurait préféré aller se coucher au plus vite. Il avait néanmoins renoncé à cette idée en venant saluer celle qu'il servait depuis tant d'années. Il avait recruté des montreurs de panthères sur Saint-Jean d'Acre et voulait lui en faire la surprise. Il pensait que c'était là belle attraction pour fêter la fin des Pâques.

Il avait été désarçonné par l'aspect chétif de la femme qui l'avait reçu. Elle avait toujours été mince, de petite taille, mais ces derniers mois elle semblait s'être étiolée brutalement, comme si toute la fougue, l'énergie qui l'animait depuis si longtemps s'était tarie. Philippe et son frère Henri n'avaient pas eu le cœur de refuser l'invitation au banquet du soir et à la veillée, dont les montreurs d'animaux constituaient le point d'orgue.

Philippe savait que Mélisende n'avait renoncé que de très mauvaise grâce au pouvoir. Pourtant elle respectait scrupuleusement ses engagements. Il constatait avec dépit que sa vie était intimement liée au royaume et elle ne s'animait vraiment que lorsqu'on abordait un sujet politique. Même les arts ou la religion n'évoquaient plus autant de joie en elle. Ses joues se creusaient avec les années et ses yeux semblaient chaque mois plus profondément enchâssés.

Après un excellent repas, les quelques participants s'étaient installés dans le jardin, sous les palmiers et parmi les senteurs des renoncules multicolores. Tandis que les panthères obéissaient en feulant à leur dresseur, des fruits secs et un fromage frais avaient été servis. Mélisende avait invité Philippe à côté d'elle, à la place d'honneur, et Henri se trouvait sur un escabeau un peu devant eux, en retrait.

« Cher Philippe, vous avez toujours su apporter de la gaieté en mon hostel. Je n'aurais pas cru finir les Pâques avec si charmant spectacle. »

Elle se tourna vers lui, posant sa main en un geste délicat, frêle oiseau, sur l'avant-bras noueux du guerrier.

« Votre absence m'a été cruelle, ces derniers temps. J'ai si peu d'amis...

--- Tous les preux de ce royaume donneraient leur vie sur un simple mot de vous, ma reine.

--- Vous ne m'aurez pas avec vos paroles flatteuses, mon ami, rit-elle. »

Elle baissa la voix et inclina la tête vers lui, en un geste empli de grâce.

« Autant que de votre présence, ce sont de vos conseils que j'ai besoin. N'avez-vous pas quelques liens avec les Pisans ? »

Philippe de Naplouse retint à peine une exclamation de surprise. Si elle lui était apparue fatiguée, son esprit demeurait alerte. Il faudrait qu'elle soit allongée en son sépulcre pour qu'elle cesse définitivement de se mêler de gouvernement. Philippe de Naplouse indiqua Henri du menton.

« C'est surtout le Buffle qui les connaît. Il garde à l'œil toutes les cités commerçantes qui ont quartier en nos villes. Auriez-vous quelque inquiétude à leur propos ? »

Il tapa sur l'épaule de son frère afin de l'inciter à rejoindre la conversation, lui demandant s'il pouvait répondre aux interrogations de la reine sur les Pisans.

« Ils sont plus actifs dans la principauté d'Antioche et chez les Griffons. Mais, surtout, ils viennent d'obtenir du vizir fātimide la possibilité d'ouvrir leur propre fondaco à Alexandrie. Ils sont depuis toujours moins proches du royaume que les Génois.

--- Je trouve justement que ces derniers deviennent trop gourmands. Ils oublient qui est le maître ici. J'ai tendance à croire que les hommes de Pise se montreraient plus raisonnables.

--- Le souci, ma reine, est qu'ils font négoce avec l'ennemi. Récemment, plusieurs d'entre eux ont vu leur cargaison et leurs commissionnaires saisis par les hommes du roi.

--- Voilà bonne rançon à leur proposer justement.

--- En échange de quoi, ma reine ? » s'inquiéta Philippe.

Mélisende n'aimait guère dévoiler ses projets avant de les mettre à exécution, mais désormais à l'écart du pouvoir, elle ne pouvait exercer sa volonté que par les influences qu'elle tissait. Elle consentit donc de bonne grâce à éclairer ses interlocuteurs sur son idée.

« Il va nous falloir des vaisseaux pour assaillir l'Égypte, avant que Nūr ad-Dīn n'y étende son influence. Les cités marchandes en ont de pleins ports. Pas plus que nous les Pisans n'apprécient les Normands établis à Palerme, voilà un terrain à ensemencer pour l'avenir. Mais s'ils font leurs affaires avec le calife, jamais ils ne voudront perdre un avantage commercial.

--- Au contraire, ma reine, objecta Henri. Le fait qu'ils soient déjà sur place nous offre la meilleure des opportunités. Qui peut résister au fruit complet lorsqu'il en a goûté le suc ? »

### Jérusalem, palais royal, chambre d'Amaury, soirée du lundi 11 février 1157

Quelques braseros apportaient un peu de chaleur dans la vaste pièce largement éclairée par de multiples lampes disséminées sur tous les meubles. Installés sur des sièges bas couverts de coussins, le jeune comte de Jaffa et sa mère étaient emmitouflés chacun sous une chaude courtepointe. Un clerc accompagné d'un musicien leur faisait la lecture, détaillant les aventures d'un certain Digénis Akritas. Après en avoir acheté une copie pour en faire une traduction, Amaury avait été déçu d'apprendre qu'il s'agissait d'un récit littéraire. Il s'était donc résolu à en faire une attraction de veillée.

Le lendemain, mercredi des Cendres, commencerait la longue période de recueillement du carême et, avec une météo plutôt grisâtre, il était bon d'égayer les soirées par des prouesses héroïques. Tout à sa joie de recevoir sa mère venue assister aux cérémonies données par le vieux Foucher d'Angoulême, avec qui elle était très liée, il sentait bien qu'elle n'avait guère la tête aux réjouissances. Lorsque le conteur marqua une pause, profitant de ce que son accompagnateur retendait les cordes de son psaltérion pour prendre quelques gorgées de vin, Amaury se pencha vers Mélisende.

« Je vous encontre fort soucieuse, mère. Y a-t-il quelque fardeau dont je pourrais alléger vos épaules ?

--- Nul poids en ce qui me concerne, mon garçon. J'ai juste longuement réfléchi à propos de la charte que ton frère a concédée récemment aux Pisans.

--- Sur vos conseils, j'ai cru comprendre. Ces négociants ont-ils valeur celée à mes yeux que vous vous fassiez ainsi leur avantparlier ?

--- Amaury, mon garçon, Damas nous a échappé pour de nombreuses années. Si nous ne voulons pas que les Turcs, ou pire, les Siciliens, soient les maîtres au Caire, il nous faut des navires pour y soutenir notre campagne.

--- Les Génois sont traditionnels alliés du royaume. Ils nous ont aidés fidèlement à prendre les ports...

--- Ils se montrent aussi fort rétifs à nos réquisitions. Ils doivent comprendre que nous pouvons nouer belle entente avec d'autres qu'eux pour la maîtrise des mers.

--- Qu'avez-vous donc en tête ? Leur promettre une partie de nos conquêtes ?

--- Non Amaury, je veux que tu leur cèdes une partie de tes terres, histoire d'en faire tes liges lorsque sera venu le temps de s'emparer de l'Égypte. Il en est ainsi comme des oliviers, qui ne portent fruit qu'après plusieurs années. » ❧

### Notes

Après s'être opposée de façon très violente à son fils Baudoin, Mélisende s'est assez brutalement retirée de la vie publique, ayant perdu le soutien de la majorité de la Haute Cour. Peu de ses proches ont trouvé grâce auprès du nouveau maître de Jérusalem et on n'arrive plus guère à suivre les activités politiques de la reine mère jusqu'à sa mort. Une des rares mentions de son influence se trouve dans une charte signée par Baudoin III en faveur des Pisans (Regesta Regni Hierosolymitani no 322). Le rôle de Mélisende y est signalé explicitement. Comme Amaury concède à son tour pas mal d'avantages à la cité italienne peu de mois après cela, je me suis plu à imaginer que Mélisende aidait à préparer l'avenir du royaume.

Native de Terre sainte et éduquée par Baudoin II du Bourg comme son héritière, elle a toujours fait preuve d'une audace féroce et d'un sens tactique consommé dans les jeux de pouvoir. Je lui prête donc une vision stratégique assez ample, suffisamment éclairée pour influencer durablement les orientations décidées par Amaury lorsqu'il sera roi.

En effet, au cours de la décennie suivante, l'Égypte fut au cœur des affrontements. Pas seulement entre les forces de Nūr ad-Dīn et les troupes latines, mais aussi au sein même des puissances et entre alliés. Par exemple, les ordres religieux militaires y frôlèrent la banqueroute et leur participation fut l'objet d'âpres négociations. Les opérations armées se doublaient de complexes jeux diplomatiques, d'influences interne et externe au califat décadent. Au final, Saladin y prit le pouvoir et y appuya sa conquête du territoire syrien, récupérant à son compte l'héritage de Nūr ad-Dīn. Une fois cela fait, il ne lui fallut guère de temps pour balayer le pouvoir latin.

PS: Le titre ce Qit'a est issu de mes lectures de Jean-Pierre Vernant sur la Grèce antique. Le terme désigne la gloire, la renommée des héros, célébrée par les poètes.

### Références

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972.

Heywood William, *A History of Pisa. Eleventh and Twelfth Centuries*, Cambridge University Press, Cambridge : 1921.

Reinhold Röhricht, *Regesta Regni Hierosolymitani* (MXCVII-MCCXCI), Oeniponti : Libraria Academica Wagneriana, 1893.

Un royaume en oubli
-------------------

### Caves de Suète, tente chapelle du camp royal, matin du jeudi 10 juillet 1158

Sortant du recoin à usage de sacristie entouré de paravents, Herbelot découvrit qu'il demeurait un chevalier en oraison agenouillé devant l'autel. Il reconnut sans mal l'imposant Bernard Vacher, habillé de son haubert comme à son habitude. Il était rare que quiconque s'éternise une fois la messe terminée, surtout quand le repas du matin avait été corné. S'interrogeant sur les motivations de ce recueillement, le jeune clerc fit mine d'arranger quelques accessoires, se tenant prêt à répondre à toute demande que le soldat pourrait lui faire.

Il n'avait jamais eu l'occasion de vraiment parler avec le porte-bannière royal, mais il en appréciait l'esprit vif et la parole retenue. Il était impressionné par son allure martiale qui évoquait en lui le vaillant Holomé, fidèle compagnon d'Alexandre[^121]. Seulement, ils appartenaient à deux mondes distincts et Herbelot était toujours réticent à sortir de sa réserve toute monastique, fut-ce pour lier connaissance avec des personnes qui l'intéressaient. Ayant terminé de psalmodier, le chevalier se releva lourdement, soufflant sous l'effort. Les années n'avaient pas été tendres avec lui, mille maux s'étaient installés dans son corps robuste. Malgré cela, il ne se plaignait jamais, se contentant de passer une main agacée sur son crâne et sa couronne de cheveux épars lorsque les douleurs se faisaient trop vives.

Il adressa un sourire poli à Herbelot, s'apprêtant à sortir du lieu.

« Le bon jour, sire Bernard, auriez-vous l'heurt de parler à clerc ?

--- Nul besoin mon père, je n'ai pas tiré la lame du fourreau depuis bien longtemps.

--- Même sans cela, il demeure moult forts périls pour l'âme. Avez-vous besoin d'aller à confesse ? Quelque lourdeur appesantit votre âme ?

--- Oh, de certes ! Mais je n'ai l'envie de m'épancher en ce moment. J'avais juste pensée pour un mien neveu qui commence son service près le roi. J'ai espoir que les saints veilleront sur lui.

--- La Sainte Mère de Dieu a toujours le cœur amiable pour les plus jeunes. »

La réponse fit sourire le vieux guerrier.

« Je dois dire que je le confiais plutôt à saint Georges et saint Maurice, qu'ils rendent son bras fort et son cœur vaillant, il en est grand besoin en la presse.

--- Je ne le savais pas porteur du glaive et de la lance. Était-il de la bataille ?

--- Si fait. Du moins était-il présent. Mais lui non plus n'a guère eu l'usage de ses armes. »

Peu désireux de vanter l'usage de la violence, fût-elle au service de Dieu, Herbelot invita de la main à sortir de la tente. L'auvent claquait sous le vent fort, les bannières et gonfanons dansaient furieusement. Autour d'eux, les pavillons des barons les plus notables étaient disposés en cercles concentriques. Le roi avait préféré s'installer dans la forteresse des Caves, laissant l'habitat de campagne à son armée.

« Je pensais rompre le jeûne avec les bannerets du sire Guillaume de Bures. J'y ai bons compagnons. Outre, sa table est toujours fort accueillante, avec mets variés et vins guillerets. »

La perspective d'un bon repas était le plus sûr moyen d'inciter Herbelot à accompagner quiconque, il continua donc de discuter avec Bernard Vacher jusqu'au vaste tref[^122] qui accueillait la table d'honneur du prince de Galilée. La toile bruissait sous les rafales et l'air charriait les senteurs de poussière et de foin roulé depuis les écuries. L'ombre des falaises reculait rapidement et la chaleur s'intensifiait, célébrée par les cigales et les grillons.

Leur arrivée fut saluée par une demi-douzaine d'hommes occupés à avaler un épais brouet parsemé de fruits secs. Deux coupes proposaient des bananes et des abricots, faisant saliver par avance le jeune clerc. Repoussant le banc, Bernard laissa Herbelot s'installer avant de prendre place à son tour. Il fit le service du vin puis se tourna vers ses pairs.

« J'avais espoir de croiser Naudet, cela fait bien longtemps que je ne l'ai vu !

--- Il a quitté le service du sire prince voilà un petit moment. Il parlait de joindre la milice du Temple[^123], il doivent en savoir plus à Jérusalem.

--- Je ne sais s'ils accepteront un brave au chef si argenté ! se moqua un des jeunes, tout en dévorant une brioche trempée de vin.

--- Il y a toujours des béjaunes[^124] comme toi à former ! répliqua sans aménité son aîné. Il était déjà ici à guerroyer avant même que la Milice ne soit créée.

--- J'ai souvenance de la puissance de son bras, enchaîna Bernard. Quand je revins de Flandre, voilà plusieurs décennies, je l'ai vu rompre cinq hampes de frêne à l'exercice sans poser pied à terre. Qui peut en dire autant ? »

La remarque attira plusieurs grognements d'acquiescement. Un jeune, enthousiaste, hocha énergiquement la tête et se mit à taper la table de son index.

« Une fois, il m'a conté qu'il avait guerroyé par-delà les sarrasins, chez un grand roi qui avait vaincu toutes les armées turques par chez lui.

--- Gardes-en remembrance ! Bien peu sont les braves qui y étaient et encore moins qui peuvent encore en parler.

--- De quelle bataille parlez-vous ? s'intéressa soudainement Herbelot.

--- Du temps de mes jouvences, quelques bannerets du grand-père le roi sont allés aider un royaume chrétien loin à l'est. Ils y ont défait une puissante armée turque. »

Comme il avalait un peu de vin, Herbelot faillit s'en étouffer de surprise. Il toussa plusieurs fois, but encore un peu avant de reprendre la parole.

« L'apôtre Thomas a donc bien des héritiers ? Je pensais qu'il n'y avait que mahométans vers le Levant.

--- Les terres sont bien trop vastes pour s'arrêter à eux. J'ai croisé des marchands qui parlaient de citadelles immenses, de prairies infinies où galopent des armées de preux chevaliers. Des créatures dont nous n'avons même pas idée. »

La conversation bifurqua sur toutes les étranges bêtes que les Latins avaient trouvées en arrivant en Terre sainte, puis sur celles qu'il restait à découvrir au-delà de leurs paysages coutumiers. Mais Herbelot n'écoutait plus. Il rêvassait à ce territoire merveilleux, forteresse de la foi d'où pouvaient s'élancer des milliers de chevaliers, tous frappés aux couleurs de la chrétienté. Reprenant contact avec la réalité, il toucha Bernard du bras.

« Il s'appelle comment, déjà, ce brave qui souhaite se retirer au parmi de la Milice du Christ ? »

### Jérusalem, mont du Temple, après-midi du samedi 25 octobre 1158

Les arbres dansaient au rythme des fortes rafales qui soulevaient la poussière de l'esplanade. Le bruit des travaux dans l'ancien palais royal volait jusqu'aux abords du Temple autour duquel Herbelot déambulait. Bataillant continuellement contre ses vêtements larges, le jeune clerc s'impatientait à l'idée d'enfin trouver Naudet. Il lui avait fallu plusieurs mois pour déterminer ce qu'il était devenu. Par chance, il était encore à Jérusalem, mais il n'avait pas été accepté dans la Milice du Christ comme il l'espérait. Selon un des chapelains, il avait été convaincu de se faire novice chez les cisterciens, un état plus adapté à son grand âge.

Bien que certains chevaliers aient parfois revêtu le blanc manteau sur le tard, il s'agissait en général de réception honorifique ou de personnes ayant une valeur particulière, dont l'ordre souhaitait bénéficier. Herbelot avait cru comprendre que Naudet ne rentrait nullement dans ces cas de figure. Les silences et les regards évasifs de ceux qui lui en avaient parlé étaient pleins d'éloquence pour un homme rompu aux subtils échanges monastiques. Naudet n'était guère apprécié, n'avait aucun haut fait dont se vanter et n'avait connu qu'une carrière en demi-teinte. Son plus grand exploit était d'avoir réussi à se maintenir jusqu'à un âge vénérable, ce qui en disait long sur ses capacités à éviter le danger.

N'ayant que peu l'occasion de déambuler librement en ces lieux, Herbelot goûtait le plaisir de revenir là où le Christ avait mis ses pas. Là où, chaque année, la procession des Rameaux venait s'échouer, en un ultime hommage des pèlerins. L'endroit était occupé par un collège de chanoines sous la supervision du vieillissant abbé Geoffroy et, surtout, du dynamique prieur Hugues. C'était ce dernier qui avait indiqué au secrétaire de l'archevêque que Naudet était leur hôte et qu'il se préparait au noviciat en suivant leur quotidien régulé. Tout en profitant des conditions de vie plus confortables des augustiniens, estimait Herbelot. Bien qu'étant clunisien, il connaissait assez bien les membres de l'ordre de Citeaux, dont il réprouvait la trop grande austérité. Il se demandait si quelqu'un qui avait vécu si longtemps dans le siècle, comme ce Naudet, avait la moindre chance de s'adapter à un tel environnement.

Herbelot trouvait que c'était là une forme d'orgueil de refuser de célébrer la beauté de la création. Malgré son naturel pacifique, il supportait avec difficulté les crtiques acerbes des moines blancs qui moquaient le goût du faste de Cluny. Au moins, remarqua-t-il, les chanoines du Temple profitaient d'un cloître rehaussé de nombreuses sculptures décoratives et historiées. Les scènes servaient d'ailleurs souvent à l'édification des fidèles, une école accueillant quelques enfants assis sur des bancs dans une des galeries.

Il s'adonnait à la contemplation de riches volutes florales et végétales lorsqu'il sursauta en entendant son nom dans son dos. Faisant face, il découvrit un vieil homme voûté, au visage pointu et au cheveu rare. Il souriait de ses quelques dents gâtées tout en frottant ses mains de tendon et d'os. Bien que non tonsuré, il était revêtu d'une robe blanche toute simple, dévoilant des mollets malingres et laiteux.

« Faites excuse de vous avoir ainsi surpris. C'est un tel honneur pour moi d'encontrer un clerc au service de l'archevêque Pierre.

--- Je ne suis pas là à sa requête, mais pour simple raison personnelle. Vous êtes bien Naudet, chevalier ?

--- J'étais celui-là, certes. La force a depuis long temps quitté mes bras, et ne suis désormais plus qu'un humble serviteur du Très-Haut, impatient de rejoindre des frères de la Foi pour célébrer Sa gloire jusqu'à ce qu'Il me rappelle auprès de Lui. Je n'ai pas encore choisi le nom sous lequel je renaîtrai à son service. »

L'élocution était difficile, l'homme déglutissant régulièrement, comme si sa gorge était continuellement obstruée. Il en profitait pour tendre son cou maigre, dodelinant du chef tel un pigeon ciblant sa pitance. Il commencèrent à déambuler tranquillement sous les voûtes, tout en échangeant à voix basse.

« On m'a rapporté que vous étiez au service de l'hostel le roi.

--- J'ai chevauché sous bien des bannières, mais la première que je suivis ici fut celle du roi Baudoin. »

Il se figea, levant un index sentencieux démenti par son regard matois.

« Attention, je parle là du tout premier qui porta la couronne en ces lieux ! Ah, si vous aviez pu vivre ces instants ! Le monde était si jeune, alors. Même le roi conservait sa verdeur malgré les années. »

Un rire rauque monta de sa poitrine tandis que son visage fin s'animait de rides.

« Tout paraissait possible, en ces temps. Nous parcourions la terre comme des seigneurs. Vous imaginez que le vénérable patriarche Arnulf[^125] avait perdu son siège à cause des manigances du roi bigame ? »

La phrase n'amusa nullement Herbelot, peu disposé à entendre des ragots qui pouvaient souiller la mémoire d'un haut prélat ou d'un roi. Et d'autant moins si cela impliquait une femme, a fortiori deux. Il était certains sujets qu'il préférait conserver à l'écart de son monde de recueillement serein, de prière et de dévotion.

« Justement, on m'a conté que vous avez combattu fort loin au levant, au parmi d'une armée d'un grand souverain.

--- J'ai connu tant de guerres ! Auriez-vous quelques précisions sur celle qui vous intéresse ?

--- C'était au service d'un roi lointain, qui a défait les Turcs.

--- Ah oui, je vois. Ce fût terrible bataille. Les têtes tombaient comme blé mûr en été !

--- Avez-vous souvenance du roi ? Du lieu ?

--- C'était fort loin d'ici, dans les montagnes. Ils parlaient une langue rauque que bien peu comprenaient.

--- Mais c'était là bons chrétiens ?

--- Sans nul doute. Seulement, ils étaient de la secte des Griffons[^126], avec leurs rites bien à eux. Nous avions quelques pères qui célébraient pour nous les mystères selon la vraie Foi, heureusement. »

Herbelot ne cacha pas sa déception, arborant une grimace, ce qui n'arrêta le vieil homme lancé dans ses souvenirs.

« Il me revient que leur roi s'appelait David. Avec quelques compères, nous avions craint un instant qu'il ne soit un de ces maudits juifs. Mais le sire Baudoin ne se serait jamais entremis avec pareille engeance, croyez-m'en !

--- Attendez, mais vous parlez du royaume d'Abkhazie[^127] ?

--- Bien sûr. C'est le plus levantin des royaumes chrétiens, par-delà les terres sarrasines.

--- J'avais cru un moment qu'il en existait d'autres, vu que l'apôtre Thomas y est parti propager la bonne parole.

--- Je n'ai pas la science de cela. Sur ces sujets, vous auriez intérêt à voir l'évêque de Jablé, Hugues de Nevers. »

Herbelot marqua une pause, intrigué par la mention d'un nom qui le ramenait à ses jeunes années. Tout le temps qu'il avait passé dans la clôture de la Charité-sur-Loire, il avait entendu parler de la cité de Nevers, loin au sud. Il n'imaginait guère le monde plus vaste que les quelques poignées de lieues qui s'étendaient jusque là et Bourges.

« Était-il des vôtres ? Un érudit ?

--- De certes ! Je l'ai croisé voilà des années, alors qu'il préparait son passage pour en appeler au saint Père Eugène. Il voulait attirer l'attention du saint homme sur l'alliance profitable qu'il y aurait eu à se concilier les bonnes grâces d'un grand roi vainqueur des Perses et des Mèdes, loin à l'est. Il avait conquis de haute lutte leur capitale et obtenu leur allégeance.

--- Il ne parlait pas de l'Abkhazie, vous en êtes acertainé ?

--- L'évêque était de la princée d'Antioche, il n'aurait pas fait pareille erreur. Il parlait de certes d'un grand roi par-delà les terres soumises au Soudan de Babylone. »

Le cœur d'Herbelot s'était emballé à l'idée qu'il existait un souverain conforme à ses espoirs. Il ne lui restait qu'à obtenir des détails sur les noms, les lieux, et peut-être pourrait-il appuyer ses récits sur de solides fondations. Un victorieux prince chrétien loin à l'est, pour peu qu'il fût héritier des préceptes enseignés par Thomas et fidèle serviteur de l'Église romaine, voilà qui offrait les meilleures bases pour les épîtres qu'il griffonnait lorsqu'il laissait son esprit vagabonder.

### Tyr, palais de l'archevêque, après-midi du jeudi 6 novembre 1158

Par-delà l'épaisse porte de bois, Herbelot entendait la clameur de l'activité du palais. Avec la visite de plusieurs suffragants de l'archevêque, le lieu était empli de cris, de valets et bagages. Profitant d'un moment de calme dans ses propres obligations, il s'était réfugié dans la bibliothèque du prélat. C'était la pièce dans laquelle il se sentait le plus tranquille, avec tout ce savoir à portée de main, tous ces récits merveilleux qui n'attendaient que de renaître à chaque lecture. Il avait reçu le trousseau des clefs avec une sublime délectation qui l'avait inquiété, au point qu'il en avait parlé à son confesseur.

Il compulsait un petit in-quarto à l'écriture dense, qui reprenait un certain nombre de vies d'apôtres tels qu'habituellement dépeints dans les *Virtutes Apostolorum*. Il espérait y trouver des détails sur l'activité de Thomas, surtout d'un point de vue géographique et remplissait des tablettes de cire de sa graphie soignée. Il avait également amassé pas mal de précisions qu'il avait consignées sur des feuilles de papier, dont il avait découvert l'utilisation en Terre sainte. Avec les chaleurs estivales qui faisaient fondre la cire, il en appréciait grandement la souplesse d'usage, à un tarif nettement moindre que tous les autres supports. Il continuait malgré tout à prendre des notes succinctes sur les tablettes, faisant confiance à sa mémoire pour reconstruire a posteriori les données. On n'abandonnait pas une pratique remontant à l'enfance aussi aisément.

La clenche de la porte grinça sur le mentonnet de pierre et lui fit tourner la tête. Il espérait qu'on n'allait pas l'appeler pour une tâche quelconque. Il sourit de soulagement en voyant le visage ami de Gilbert, le jeune chantre de la suite de l'évêque de Lydda.

« Le bon jour, mon père. J'ai ouï dire que vous étiez là, je viens donc à dessein pour demander usage de certains ouvrages du sire archevêque.

--- Après quels volumes espères-tu ?

--- On m'a parlé d'un antiphonaire[^128] dont le propre a été enrichi pour la Terre sainte, que le regretté père Foucher de Jérusalem[^129] avait fait rédiger. »

Herbelot sourit. Il connaissait l'appétence de Gilbert pour les hymnes et les chants. Il en goûtait également le talent et appréciait que des artistes mettent leur savoir-faire au service de la liturgie. De la main, il invita le jeune clerc à le rejoindre à la table tandis qu'il se levait pour aller chercher le lourd volume.

Quand il revint à sa place, il s'aperçut qu'il avait tellement l'habitude de prendre ses aises qu'il avait répandu ses notes sur tout le plateau. Les bras encombrés de l'imposant manuscrit, il demanda à Gilbert de pousser ses affaires.

« Préparez-vous quelque étude de science, à voir toutes ces références ?

--- J'ai depuis longtemps abandonné le goût pour ces savoirs profanes. Il n'est nul besoin de comprendre l'œuvre du Seigneur pour en louer la beauté. »

Gilbert hocha poliment sa grosse tête tout en déplaçant avec soin les différents éléments. Il n'avait pas fait les études d'Herbelot, n'était pas prêtre ordonné et se comportait toujours tel l'élève devant son maître bien qu'ils n'aient que quelques années d'écart.

« Je prépare une missive pour le nord, pour l'évêque de Jablé. J'ai espoir de la confier à un clerc de l'hostel de l'évêque de Beyrouth, Sidon voire Tripoli.

--- Le sire archevêque compte y faire visitance ?

--- Nenni, c'est là courrier personnel. »

Il se rapprocha de Gilbert, adoptant une voix sourde, offrant l'image d'un conspirateur.

« J'ai appris que l'évêque du lieu y avait bonne connoissance d'une grande bataille qui aurait vu la victoire d'un puissant souverain chrétien sur les Mèdes et les Perses, voilà plus d'une dizaine d'années. Il me faut en apprendre davantage pour un ouvrage édifiant que je compose, à destination des princes.

--- Vous parlez du roi Jean, le prêtre et roi ?

--- Comment ça ? De qui me parles-tu ?

--- J'ai entendu parler de ce grand homme, prêtre et roi, qui avait espoir de combattre les infidèles depuis le Levant. Un des chanoines, Régnier, connaît fort bien l'histoire. C'est un des plus érudits de l'hostel du sire évêque Constantin. »

Herbelot écarquillait des yeux de hibou.

« Ce Régnier est-il du voyage ? Puis-je l'encontrer ?

--- Non, il ne voyage que peu. C'est lui qui a la charge de définir les cartons pour les imagiers de la basilique. Il préfère ne guère s'éloigner pour s'assurer que ses instructions sont bien entendues. »

Herbelot acquiesça, légèrement refroidi après l'enthousiasme qui l'avait envahi. Néanmoins, il sentait le fougue qui animait ses pensées. Il ne lui serait guère difficile de trouver une occasion de se rendre à Lydda. Il y avait souvent des messages à porter, des biens à escorter ou des visiteurs à accompagner. Il s'arrangeait généralement pour échapper à ce qu'il considérait une corvée. Mais là, il avait la meilleure des motivations pour réchauffer ses ardeurs.

### Lydda, palais de l'évêque Constantin, début d'après-midi du vendredi 5 décembre 1158

Herbelot entendait chacun de ses pas faire un étrange bruit de succion tandis qu'il avançait dans les salles empierrées. Le trajet n'avait pas été long depuis Césarée, mais ils avaient été douchés par une averse subite alors qu'ils dépassaient Jaffa. Il était légèrement penaud de se présenter ainsi devant l'évêque Constantin, mais il n'avait pas d'autres souliers dans ses bagages. Heureusement, d'épaisses chausses de laine lui avaient permis de mettre ses pieds au sec en arrivant. Il avait également avalé un petit bouillon, agrémenté de soupes d'un pain rassis, mais roboratif, le tout accompagné d'un vin léger. Il se sentait donc parfaitement bien lorsqu'il pénétra dans la vaste salle d'audience.

Comme partout dans le lieu, on voyait les réfections récentes. La basilique était encore l'objet de travaux intensifs. Les jours de beau temps, en ouvrant les fenêtres, le bruit de l'activité des tailleurs, forgerons et charpentiers devait d'ailleurs monter par les magnifiques châssis ornés de verre coloré. Au sol, un dallage de pierre agrémenté de carreaux vernissés disparaissait sous les tapis du dais de l'évêque. Aux murs, des motifs géométriques complexes avaient été peints sous un long ensemble narratif racontant la vie de saint Georges.

Insolemment vautré sur le trône, un petit singe festoyait de quelques fruits secs, tout en lançant des regards inquiets vers le nouvel entrant. Assis à une modeste table près d'un brasero, l'imposant Constantin discutait à mi-voix avec un homme arborant une fantastique bedaine. Malgré la fraîcheur, il était en train de s'éponger le front à l'aide d'un impressionnant mouchoir. Circonspects, ils s'interrompirent à l'arrivée d'Herbelot.

Reconnaissant le jeune clerc, l'évêque s'efforça de sourire poliment et, d'un geste, l'invita à s'avancer. Il l'introduisit rapidement à Maugier du Toron, son vicomte.

« J'ai là quelques courriers à votre intention, ainsi que les salutations les plus amicales du sire archevêque Pierre, sire Constantin.

--- Je vous en sais gré, mon ami. Vous m'encontrez en pleins soucis. Êtes-vous là un moment ou juste de passage ?

--- J'avais espoir de demeurer pour profiter de la science d'un de vos chanoines, si vous m'en donnez permission.

--- Bien sûr, bien sûr. Ils ne sont pas moines cloîtrés, et certainement désireux de répondre aux attentes d'un clerc de l'hostel de l'archevêque. »

Voyant que l'évêque était soucieux, Herbelot décida de se retirer au plus vite, impatient de s'entretenir des questions qui l'intéressaient.

« Pourriez-vous juste m'indiquer où je pourrais encontrer le frère en charge des cartons du chantier ? C'est lui que j'ai espoir d'entretenir. »

La mâchoire du prélat béa soudainement, accompagnant le froncement de sourcils du vicomte. Maugier fut le plus prompt à réagir.

« Peut-on assavoir ce qui vous mande après lui, mon frère ?

--- Rien qui ne puisse inquiéter un sire vicomte, je vous en donne gage. Juste question érudite sur laquelle on m'a fort vanté ses talents. »

Incapable de déchiffrer l'ambiance lourde qui planait, Herbelot se tourna vers l'évêque. Celui-ci hésita un petit moment, passant une langue épaisse sur ses lèvres fines avant de se résoudre à préciser, d'une voix ennuyée.

« Nous discutions de cela même à votre entrée : on a retrouvé le corps de Régnier de Waulsort ce matin même, brûlé de la plus horrible des façons. » ❧

### Notes

Il ne faut pas concevoir les royaumes médiévaux similaires aux états modernes. Leurs frontières étaient très fluctuantes et les conséquences de leur définition très aléatoires. Une porosité demeurait, même entre des territoires adversaires, et les combats n'entraînaient pas un arrêt des flux migratoires : pèlerins, diplomates, commerçants, nomades continuaient d'aller et venir, sous un contrôle plus ou moins serré. Il arrivait donc que certains voyagent fort loin et des échanges, ponctuels, mais bien attestés, existaient entre des zones très lointaines. Cela alimentait bien sûr tous les fantasmes et les attributions les plus fantaisistes quant à l'origine de certaines denrées.

Pourtant, les dirigeants, les lettrés parvenaient à échanger de temps à autre. Les informations se propageaient dans un second temps de façon très déformée auprès des couches les plus populaires et constituaient un terreau propice à l'extrapolation. De la même façon que le rapport au temps était complètement distinct du nôtre, habitués que nous sommes à une division métrée de ce dernier au fil du jour, le rapport à l'espace conservait une dimension mythique. L'espace géographique intégrait des composantes symboliques superposées à sa réalité matérielle.

### Références

Hamilton Bernard, « The Lands of Prester John. Western Knowledge of Asia and Africa at the Time of the Crusades » dans *The Haskins Society Journal*, vol. 15, 2004, p.126-142.

Khintibidze Elguja, *The Designations of the Georgians and Their Etymology*, Tbilisi :Tbilisi State University Press, 1998.

Steinová Evina, *Biblical Material in the Latin Apocryphal Acts of the Apostles*, RMA Thesis, Utrecht University, 2011.

Boucles d'or
------------

### Casal de Mimas, manse familial de Moryse, matin du lundi 9 février 1153

Le soleil déjà vaillant s'engouffra violemment dans la petite pièce dès que la porte s'ouvrit en grinçant. En retrait, plusieurs personnes portèrent la main à leur visage tout en fronçant les sourcils. Tenant la clenche, un baluchon à l'épaule, une fine silhouette découpait d'ombre la lumière. Ses cheveux frisés brillaient d'or, dessinant sur sa tête une couronne. Malgré ce signe de bon augure, l'ambiance n'était pas à la fête. Moryse avait décidé de partir contre l'avis de tous.

« Me bénirez-vous, père et mère, avant mon départ ?

--- Pour ce que tu tiens en honneur mes avis, que vaudra ma bénédiction ? grogna son père. Prends quand même garde à toi, la terre d'ici est pas tendre. »

Il leva le bras, comme pour faire mentir sa déclaration, mais sans aller jusqu'à se dédire devant sa parentèle. Il soupira et fit un geste de dépit. Hésitant à s'opposer à son époux, la mère de Moryse s'avança en lançant des regards à la dérobée. Elle embrassa malgré tout son fils et traça du pouce le signe de croix sur son front avant de marmonner quelque prière à la Vierge. Le jeune homme sourit et offrit sa bénédiction en retour.

« Ne soyez pas tristes, dès que j'ai trouvé bonne place où m'établir, je vous le fais savoir ! »

Malgré ses fanfaronnades, il n'était guère rassuré, bien moins qu'il ne le laissait entendre. Il n'était que peu allé au-delà du casal, si ce n'était à Saint-Jean[^130]. Cette fois, ses pas ne s'y arrêteraient pas. Il avait prévu de se rendre plus au sud, d'abord à Jérusalem puis là où les offres seraient les plus intéressantes pour un colon comme lui. Il avait serré dans un sac un peu de linge de corps, un canif et une petite vache en bois qu'il conservait depuis l'enfance. À cela s'ajoutaient une calebasse en guise de gourde et quelques provisions et c'était tout. Il comptait néanmoins bâtir là-dessus sa fortune, dont il ne doutait pas qu'elle l'attendait quelque part.

Il n'était que le benjamin d'une longue fratrie et n'avait comme avenir que de servir son aîné s'il demeurait dans le manse familial. Il avait un temps espéré devenir clerc, apprenant auprès du prêtre de la paroisse quelques rudiments de lecture et d'écriture, voire de latin. Il en tirait grande fierté, mais avait rapidement revu ses ambitions à la baisse quand on lui avait parlé du célibat et des contraintes liées au clergé.

Il se tourna pour partir quand son frère, désormais en charge de famille, se manifesta d'un bougonnement, avançant sa silhouette pataude. Il sentait la sueur et le mauvais vin, les cheveux perpétuellement en bataille. Sa voix habituée à porter dans les champs résonnait sous la voûte.

« Je dis pas que t'as raison, et je pense comme le père que tu fais grosse erreur, mais t'es un homme désormais, et que je sois damné si je m'oppose à ça. »

Il fouilla sous sa chemise et détacha de son braïel une petite bourse, guère ventrue. Immédiatement ses proches s'esclaffèrent, particulièrement son père, qui voyait là une manifestation de désaveu bien cruelle.

« Te bénir serait pas honnête envers le père, d'autant que je suis d'accord avec lui. Tes bras seraient bien plus utiles sur nos terres que chez des étrangers. Mais ce serait aussi grand hontage que de laisser partir comme un mendiant un des nôtres. »

Il lui claqua le sachet dans la paume.

« Fais-en bon usage, c'est le fruit du labeur des tiens, arraché maille à maille au sol d'ici. T'avise pas de le boire ! »

Ému par cette marque de solidarité, Moryse hocha la tête, avant d'empoigner son aîné pour une bourrade maladroite. Ils n'étaient guère familiers des embrassades.

« Vous entendrez bonnes nouvelles de moi avant la Noël, soyez acertainés ! »

Puis, après un dernier sourire à ses neveux et nièces, cachés dans les jupes des femmes, il leva la main en un adieu. Il ne tourna pas la tête avant d'avoir passé la croix Saint-Jean, à l'orée du village. Là, malgré toute sa détermination, il avait les yeux embués en apercevant une ultime fois son monde d'enfant. « Satanée poussière » maugréa-t-il en se frottant le visage.

### Jérusalem, réfectoire des indigents de Saint-Jean de l'Hôpital, milieu de matinée du lundi 9 mai 1155

Des cuillers choquant et frottant le bois des écuelles, des suçotements édentés et des raclements de gorges emplissaient l'espace, en contrepoint de la voix forte d'un moine replet, ahanant quelque livre saint. Nul n'osait parler et les arrivants traînaient leurs savates en douceur pour ne pas déranger la pieuse lecture qui leur était faite, quoiqu'aucun d'entre eux n'y entende rien. Ils venaient pour le brouet, la piquette et le pain et s'accommodaient fort bien d'un incompréhensible latin vrombissant dans leurs oreilles tandis que leur panse se remplissait.

Aux extrémités des longues tables patrouillaient des frères en robe noire, l'œil sourcilleux et la bouche pincée, s'assurant que nul ne volait le voisin ou ne se permettait le moindre abus. Parfois, ils indiquaient d'un signe de tête aux valets à l'entrée qu'ils pouvaient laisser pénétrer un certain nombre de nécessiteux. Chacun recevait une gamelle emplie d'un épais gruau et une tranche de pain, ainsi qu'un gobelet de vin.

Une fois leur pitance avalée, les hommes s'esquivaient par une petite porte, déposant couvert et écuelle dans des paniers affectés à cela. Périodiquement, des serviteurs venaient emporter le tout à la vaisselle, à travers les méandres de salles et de passages du grand Hôpital.

Se dirigeant vers la sortie tout en pourléchant une dernière fois sa cuiller, Moryse salua de la tête un frère posté près des corbeilles, peut-être pour éviter les vols. Comme ils étaient dans une galerie surplombant une courette, il s'estima autorisé à parler.

« C'est belle charité que la vôtre, frère. Je n'ai pas idée de comment vous faites pour emplir tant de panses.

--- C'est là devoir de tout chrétien, nous n'en tirons nulle gloire ! »

Tout en répondant, il ne quittait guère des yeux les paniers dont on aurait pu croire qu'il craignait qu'ils ne disparaissent à tout instant. Il se risqua néanmoins un bref instant à dévisager le jeune homme devant lui, reconnaissant des traits familiers.

« Tu n'es pas pérégrin, il me semble avoir connaissance de ta face.

--- Si fait, je suis en la Cité de temps à autre depuis des mois. Je cherche de l'ouvrage, mais bien peu ont de quoi payer manouvrier. Je n'ai suivi aucun apprentissage et une fois moisson et fenaisons achevées, il n'est guère de patrons prêts à lâcher monnaie.

--- Pourquoi donc chercher à amasser de la cliquaille ? Es-tu sans parentèle, sans feu ni lieu ?

--- Le manse n'était guère étendu, là d'où je viens, au nord. Alors je suis parti chercher fortune. »

Le religieux hocha la tête, tout en supervisant l'arrivée de paniers vides en remplacement de ceux qui débordaient. De la cour leur montaient des voix paillardes, accompagnant le roulement d'un lourd charroi. Des vidangeurs, à n'en pas douter, vu l'odeur méphitique qui vint rudoyer leurs narines.

« Sais-tu que nous recherchons toujours des bras ici, à Saint-Jean. Surtout en cette période, entre Ascension et Carême. Il y a tant de marcheurs de la Foi en nos murs !

--- C'est que... N'y voyez pas offense, mais me jurer valet, ça ne me permettra jamais d'amasser de quoi avoir mien hostel.

--- Diantre, et si c'est là seul labeur qui s'offre à toi ? Y vois-tu déshonneur ? Nous ne payons pas bien cher, de certes, mais tu auras héberge et pitance et, même, deux fois l'an, linges de neuf. »

L'idée, pour séduisante qu'elle puisse être, choquait les convictions de Moryse. Il craignait avant tout de s'enfermer dans un travail subalterne, qui ne lui offrirait jamais la possibilité de fonder sa famille. Il avait trop croisé de ces infortunés, réduits à vivre dans un galetas en attendant leur mort, à laquelle personne ne prendrait garde. L'hospitalier, voyant que ses arguments ne faisaient pas mouche, persista d'un air bonasse.

« Tu peux te jurer pour un temps, libre à toi d'ensuite courir les chemins de nouvel, fort du pécule amassé.

--- C'est donc possible de ne rester qu'un temps ?

--- Qu'encrois-tu donc, gamin ? Que tous les valets et commis ont fait pareil jurement que frère profès ? »

Acceptant de lâcher ses précieuses écuelles de vue pour se garantir d'oreilles indiscrètes, il s'approcha de Moryse et lui souffla.

« Outre, tu es bon Chrétien latin, ce serait misère de te voir affecté à tâche ingrate !

--- C'est vrai que, vu comme ça...

--- Nous pouvons même te garder en sûreté ton trésor, en attendant le jour de ton départ. J'ai dans l'idée que tu as creusé quelque terrier aux alentours où celer tes noix ! se gaussa-t-il. Chez nous, les coffres sont bardés de fer, remisés en solide donjon, sous la protection de bras puissants. Ton magot y sera sauf ! »

Ce dernier point enthousiasma Moryse. Il n'avait guère fait prospérer le tas de pièces confiées par son frère, malgré sa sévère discipline, mais il redoutait toujours de se le voir dérobé. Il en avait cousu une partie dans ses vêtements et le reste réparti entre différentes cachettes, dont une faille d'un vieux mur, dans les ruines d'une maison, à l'ouest de la cité. Il craignait sans cesse que de riches commerçants décident d'y rebâtir, lui interdisant l'accès. Il afficha finalement son plus large sourire.

« Qui dois-je voir pour me faire valet vôtre ? »

### Jérusalem, salle des malades de Saint-Jean de l'Hôpital, matin du mardi 9 juillet 1157

Frère Rostain gratta sa vieille tonsure de ses doigts tachés d'encre. Il récupéra un papier des mains de Moryse, face à lui, de l'autre côté du plateau encombré de tablettes, de feuilles et de documents. Celui-ci hocha la tête, tout sourire. Il avait fière allure, avec ses cheveux fous bien taillés à l'écuelle, sa cotte à peine ravaudée et son menton rasé de frais. L'hospitalier sortit une bourse d'une petite cassette ferrée qu'il avait rapidement ouverte.

« Ton bien est là-dedans, rien n'y manque, si tu veux vérifier. En monnaies du roi, besants blancs pour la plupart.

--- Nul besoin, frère Rostain. J'ai toute confiance. »

Le jeune homme se hâta de nouer le cordon de la petite bourse à sa ceinture de braies et recouvrit le tout de sa chemise et de sa cotte.

« As-tu compère pour ne pas aller dans les rues avec pareille fortune sur toi ?

--- J'ai solide bourdon qui saura éloigner les malandrins.

--- Si tu le manies aussi lestement que le balai quand tu tenais la porte de l'hospice, je n'ai nulle crainte, s'amusa le vieux moine. »

Il soupira longuement, une nouvelle fois ému du départ d'un de ses valets. Il n'était pas toujours tendre avec eux, mais leur conservait une affection réelle, surtout avec les plus jeunes. Il avait vu Moryse gagner en confiance et devenir un homme fait, du jouvenceau qu'il était en arrivant. Il se leva pour accompagner son protégé jusqu'à la porte, profitant d'ultimes moments avec lui.

« Tu demeureras en mes prières. Et si jamais tu passes par ici, aie la bonté de passer me saluer. »

Il lui pressa le bras, en un geste d'affection pour celui qui prenait son indépendance.

« Es-tu bien acertainé que c'est le moment de partir sur les routes ? Le roi lui-même a failli tomber aux mains des Mahométans...

--- Je ne peux surseoir encore, mon frère. J'ai de quoi payer ma balle depuis plusieurs mois déjà. Vous m'avez convaincu de demeurer aux Pâques, puis à l'Ascension...

--- C'est qu'il m'est cruel de me séparer de toi, gamin. »

N'ayant jamais eu la moindre démonstration d'affection paternelle, Moryse était toujours mal à l'aise lorsque le vieil homme s'épanchait ainsi. Il préféra ne pas relever et pointa un pèlerin qui venait d'entrer. La démarche chaloupée et l'épaule puissante, il avançait d'un air buté.

« Regardez donc ce pérégrin du Beauvaisis ! Il n'en faudrait guère comme lui pour enfrissonner tous les mécréants du Soudan. »

Le moine s'amusa de ce jeu, qu'ils affectionnaient tous deux. Moryse savait déduire une foule de choses de petits détails et excellait à déterminer l'origine des visiteurs rien qu'à leur aspect. Frère Rostain leva le menton en une interrogation muette, invitant Moryse à s'expliquer.

« Il a broderie de fil sur sa besace avec la croix à l'envers, comme celle de saint Pierre[^131]. Outre cela, il porte des savates à nœuds, comme j'en ai souventes fois encontré chez eux. Enfin, de nombreux Picards aiment à orner leur bourdon de décors ainsi qu'il l'a fait.

--- Je ne sais si je retrouverai portier plus habile que toi ! Tu as le regard acéré de l'aigle. Dieu t'a fait grand don avec cela, apprends à le chérir.

--- J'y compte bien, frère. Il m'a d'ailleurs été fort utile pour choisir les éléments de ma balle. Les gens au loin de leur hostel ont toujours besoin de boucles et d'épingles. Et savoir proposer la bonne à celui qui a noué laborieusement son cordon brisé saura m'apporter fortune.

--- C'est tout ce que je te souhaite, gamin. Tu as bon cœur et frais minois, puisse la Sainte Vierge veiller sur les deux. »

Il pinça la joue de Moryse avant de le serrer contre son cœur. Celui-ci se trouva un peu dépourvu, ainsi plaqué contre la bedaine molle de l'ecclésiastique, dans les relents de camomille et de savon à raser.

« As-tu déjà choisi où aller en premier ?

--- De prime, je vais prendre ma balle et payer ce que je dois, puis je vais m'enquérir d'un éventuel appel du ban. Le roi aura peut-être désir de venger l'affront récent. Les soldats ont toujours grands besoins pour leur fourniment. »

Frère Rostain lui adressa un sourire empreint de tristesse. Après une dernière embrassade rapide, il se hissa sur la pointe des pieds pour un baiser de paix en signe de bonne fortune. Puis il disparut dans l'ombre du bâtiment, abandonnant Moryse sur le perron avec son remplaçant, un jeune homme affligé d'une dentition exubérante, accordée par un créateur facétieux en réparation d'une cervelle défaillante. Fébrile à l'idée que son prédécesseur reprenne son poste, ce dernier se plaça au milieu du chemin, jambes écartées, pouces à la ceinture.

Moryse ne lui adressa pas même un regard, à son grand dam. Il dévala les marches avec entrain. Il recouvrait sa liberté, avait amassé un pécule pour lancer son propre commerce de colporteur. Il ne doutait pas qu'il arriverait à s'établir avant que l'année ne s'achève. Ce fut en fredonnant qu'il obliqua à gauche en direction de Malquisinat, bien décidé à s'accorder un bon repas pour fêter la journée.

### Montagnes de Samarie, matin du vendredi 26 juillet 1157

Moryse s'était levé bien avant l'aube afin de marcher avant les grandes chaleurs de mi-journée. La poussière s'accrochait à ses jambes déjà collantes de sueur et il suçait depuis plusieurs lieues un caillou de façon à économiser son outre d'eau. Les sangles de sa balle lui meurtrissaient les épaules. Il s'était chargé autant qu'il l'avait pu lorsqu'il avait entendu parler de la réunion de l'armée dans le comté de Tripoli. Il était à ce moment dans le sud, inquiet de la défaite récente du roi, et pensait se contenter de faire une tournée sans craindre d'éventuelles incursions turques. Quand il apprit que l'arrivée du comte de Flandre avait décidé Baudoin III à se lancer dans des conquêtes au nord, il s'en était voulu de cette idée imbécile. Il en était réduit à avancer à marche forcée, seul, loin de la caravane habituelle. Il espérait juste rejoindre les troupes avant qu'elles n'atteignent le territoire ennemi. On lui avait garanti qu'en tant que marchand, il lui suffirait de s'affranchir des taxes pour éviter les ennuis, mais il n'avait nulle confiance dans les assurances des musulmans.

Il longeait la fin d'une interminable restanque abritant des oliviers à l'allure impeccable. Le sentier s'affaissait ensuite brusquement, certainement en direction d'un oued. Moryse espérait y découvrir ne serait-ce qu'un filet d'eau pour se rafraîchir un peu. S'appuyant un instant sur son bâton, il tenta de déplacer les lanières sur ses épaules. Tout en se déhanchant pour gérer le poids de ses paquets, il laissait son regard vagabonder sur les environs. Ce qu'il aperçut le figea soudain.

À une trentaine de pas à peine se tenait un lion. L'animal, le pelage collé de poussière grise du chemin semblait lui aussi assoiffé, haletant, la langue sur le côté. Stoppé dans sa course, il balançait doucement sa queue avec indolence, le dévisageant avec intensité. Moryse sentit son bas-ventre échapper à son contrôle. Il avait entendu parler de ces bêtes, mais elles étaient fort rares sur la côte où il avait grandi. Il en percevait désormais les effluves lourds, acides. Il serra machinalement son bâton, bien incapable de savoir ce qu'il devait faire. Lorsqu'il vit les puissants jarrets se tendre et le fauve prendre son élan vers lui, il n'eut que l'instinct de se jeter au sol, ramassé sous ses paquets.

Le poids de l'animal lui coupa la respiration, lui écrasant la face sur les graviers. Il sentit une déchirure aiguë dans son épaule avant d'être violemment projeté en travers du chemin, raclant la caillasse et les rochers avec douleur. Il tenta de brandir son bâton, mais il lui fut arraché lorsque la bête vint chercher à le saisir de ses griffes. Une intense brûlure consuma sa joue tandis que sa vision se brouillait. Il se débattait comme il le pouvait, secouant ses membres sans arriver à repousser les assauts du prédateur.

Puis tout s'arrêta brusquement, son attaquant le libéra et disparut aussi vite qu'il était apparu. Moryse se retrouva aveuglé par le soleil, le visage cuisant de douleur, désarticulé sur ses paquets répandus sur le sol. Il se toucha la joue et découvrit sa main emplie de sang avant de perdre connaissance.

Lorsqu'il se réveilla, il était nauséeux, migraineux, allongé sur de la paille dans des senteurs familières d'animaux. Les effluves puissants du crottin et des chevaux lui semblèrent une douce fragrance après l'effrayante odeur du lion. Il pesta de douleur en tentant de se tourner, caressa un bandage sur son visage. Il était dans un fenil, une cruche à ses côtés. Il essaya de se soulever, mais la souffrance fut plus forte, il retomba sur sa couche et se rendormit peu après, accablé de tourment.

Ce ne fut qu'après plusieurs réveils et repos qu'il se sentit capable d'échanger quelques mots avec un gamin qui furetait dans les environs. Plusieurs valets lui apportèrent du bouillon quand il se déclara en appétit. Régulièrement un clerc changeait les linges posés sur sa face, lui administrant par la même occasion un breuvage destiné à apaiser les douleurs. On lui disait de dormir, qu'il était sauf. Il perdit le fil du temps.

Vint enfin le jour où il put sortir et faire quelques pas au dehors. Il s'aidait de son bâton, qu'il avait retrouvé. Il y avait découvert plusieurs entailles dues aux griffes ou aux crocs du lion. On l'invita à se rendre jusqu'à la tour centrale, au milieu de la cour, où il monta dans la grande salle. Le seigneur du lieu, Robert de Retest, à qui il devait la vie, tenait à le voir.

La pièce était vaste et richement décorée, en particulier la cathèdre multicolore de l'estrade. Celui qu'on lui désigna comme le maître était, une badine à la main, occupé à entraîner au mordant un imposant chien de chasse au poil court. Les mâchoires rappelèrent de douloureux souvenirs à Moryse. Bien que fatigué, il trouva la force de s'incliner devant le chevalier. De taille moyenne, le noble était aussi puissant qu'on pouvait l'imaginer d'un homme destiné à la guerre. Son visage rond, attifé d'une barbe argentée miteuse, n'arborait qu'un crâne couleur vieux cuir en guise de cheveux. Des sourcils épais, couleur acier, surmontaient deux yeux globuleux sans cesse en mouvement tandis que la voix grondait, aboiement plus que paroles.

« Voilà le miraculé ! J'ai bien cru que nous devrions te porter en terre sans même savoir ton nom.

--- Moryse, sire. Je suis des environs de Saint-Jean. »

Tout en claquant sa cuisse pour intimer l'arrêt du jeu à son mâtin, Robert de Retest s'avança, un sourire proche de celui du lion sur les lèvres.

« Je te dois grand merci, jeune Moryse. Tu as bien failli en mourir, mais grâce à toi, j'ai pu enfin occire ce maudit félin qui assaillait mes bêtes et mes gens depuis des mois. »

Il alla se servir un verre de vin, qu'il avala en une longue et silencieuse rasade, avant de se planter de nouveau devant Moryse.

« J'ai décliné le ban du roi pour la chevauchée, car il me fallait mettre le holà à ses méfaits. Sa dépouille ornera bientôt ma chambre, ainsi qu'il sied. Pour te remercier de ton aide, bien qu'involontaire, je t'ai fait soigner et tu seras mon hôte aussi longtemps que tu en auras le besoin. »

Tout à la gestion de la douleur qui lui battait dans les tempes, Moryse arriva à peine à répondre, mais s'inclina en bredouillant un merci confus.

« Dès que tu te sentiras d'attaque, tu pourras prendre place au bas bout de ma table. Tu y goûteras bonne chère, prompte à redonner vigueur et entrain à un jeune tel que toi. »

Puis il fit claquer sa badine sur ses cuisses et se détourna. L'entretien était terminé.

### Harim, marché du camp Latin, midi du mardi 7 janvier 1158

Avisant le ciel obstrué de nuages aux ventres lourds de pluie, Moryse hésitait à sortir du petit appentis dont il avait fait sa demeure. Rencogné à l'angle d'une ruine et d'une écurie, il y avait installé ses quartiers depuis plusieurs jours. Le lieu lui avait paru propice, car il ne se trouvait pas loin des chevaux. Les palefreniers de Tripoli venaient d'ailleurs faire boire les bêtes dans un proche bassin où le jeune homme avait pris l'habitude de se ravitailler en eau. Espérant que l'averse était finie, il se leva et assujettit sa balle sur ses épaules. Bien qu'il n'ait pas à se plaindre de ses ventes, il avait encore un lourd bagage.

Prolongeant la rue autour d'un bosquet de palmiers, une vaste cour avait été attribuée aux artisans en rapport avec la sellerie. On y trouvait même plusieurs maréchaux-ferrants et un charron, plus souvent au travail sur les engins de siège que sur des attelages. Il avaient pris la place des commerçants autochtones, priés manu militari d'abandonner leurs magasins le temps de l'assaut. Ils avaient néanmoins solidement barré leurs devantures afin de rendre le pillage difficile. Chacun s'était donc aménagé une cabane de son mieux, avec une bâche pour les plus riches, sous des feuillages ou des planches de récupération pour les plus modestes. Certains avaient même négocié avec les locaux la location d'une échoppe.

Chaque jour, Moryse déambulait parmi les bourreliers, selliers ou chapuiseurs[^132], proposant à tous ses boucles. Il en avait une large provision, de cuivre, de bronze ou de fer simple, parfois d'acier, voire à décor et faisait l'article à tous ceux qui levaient les yeux vers lui. Plutôt que passer son temps à démarcher les soldats pour réparer leurs baudriers et sangles, il estimait plus efficace de vendre par lots à des professionnels. Il n'avait néanmoins pas le droit de s'installer à demeure, car il n'était pas fabricant. Le mâalem[^133] était intraitable sur ce point : il n'acceptait que des artisans dans son souk. Moryse avait malgré tout tenu à lui verser un écot, bien qu'il n'y ait nulle obligation. Il avait bien compris qu'il serait fort utile de s'en faire un allié en cédant de bonne grâce quelques deniers. Si d'aventure un client ne lui payait pas son dû, il savait qu'il trouverait une oreille attentive à ses problèmes.

Alors qu'après une vente, il remballait de la marchandise, soigneusement roulée dans des chiffons huilés, il prit conscience d'une présence dans son dos. Se retournant, il découvrit la face joviale de Guillaume de Lunel, maréchal du comte de Tripoli. D'une taille modeste accentuée par un physique musculeux et ventru, son nez cassé et sa longue mèche de cheveux étaient connus de tous. Il était réputé pour son caractère affable, ses blagues douteuses et sa familiarité avec tous.

« Alors, le boucléeur ? On fait la retape ?

--- Faites excuses, sire maréchal, je n'avais pris garde à votre présence, je vous laisse la place.

--- Doux, l'ami, c'est toi que je piste ! »

Il sortit d'une besace une paire de boucles de bel aspect, étamées, avec un mordant décoré de guillochis rehaussés de cabochons de verre coloré. Moryse en fit une moue d'appréciation. Même brisées, elles conservaient une forte valeur.

« Aurais-tu semblable parure dans tes fourniments ? Le lormier[^134] n'a rien qui puisse plaire au jeune sire comte... »

Moryse déglutit lentement. C'était la première fois qu'il avait l'occasion de fournir une personne aussi prestigieuse. En concluant cette vente, il pourrait à l'avenir faire valoir la qualité de ses produits à chacun. Il bredouilla tout en remuant désespérément son ballot de façon à le lacer. Il avait justement ce qu'il fallait, un investissement un peu fou en vue d'une telle opportunité. Deux boucles dorées, avec des motifs en émaux. Il y avait laissé une vraie fortune, mais estimait que c'était aussi une façon d'épargner. Par prudence, il ne les rangeait pas avec les autres objets, mais dans les plis d'une étoffe qu'il se roulait autour du ventre. Il répugnait à se dévoiler ainsi sur la place du marché.

« Si vous acceptez de m'attendre un court moment, à peine quelques *Pater*, je reviens avec ce qu'il vous faut, sire maréchal !

--- Voilà qu'il me fait soupirer comme bachelier après pucelle. Hâte-toi donc et ne me fais pas attendre en vain !

--- Je vous donne mon gage que cela sera du plus bel effet sur le filet du sire comte. Je reviens de suite. »

Brinquebalant son sac en tous sens, il manqua de s'empêtrer dedans et de tomber à plusieurs reprises tandis qu'il s'empressait de dénicher un recoin tranquille. Là, il dénoua fébrilement sa ceinture pour aller fouiller dans sa cachette. Lorsqu'il revint, il tenait dans ses mains ses trésors, qu'il frottait pour en faire ressortir la brillance. Le maréchal ne put s'empêcher de siffler.

« Dis donc, gamin, tu as dépouillé évêque ou abbé pour tenir pareilles beautés ?

--- Je les ai achetés à bon artisan de Jaffa, j'en fais serment. Lui-même...

--- Paix, gamin, je t'asticote. C'est exactement ce qu'il me faut ! »

Il s'empara des deux boucles, provoquant un haut-le-cœur dans la poitrine de Moryse. Ils n'avaient même pas parlé du prix. Il se sentait incapable de faire une proposition tellement cela se bousculait dans sa tête. Il déglutit, cherchant comment aborder le sujet. Mais le maréchal, tout en hochant le menton, ne lui laissa pas le temps de s'inquiéter.

« Tu passeras voir mon trésorier pour te faire payer. Je le préviendrai. »

Puis, goguenard, il frotta la tête de Moryse.

« Qui aurait cru qu'un nez crotteux comme toi cachait pareil trésor ? Je crois que tu mériterais bien du surnom de Boucles d'Or, avec ta tête en guise d'enseigne. »

Le sourire qui s'afficha sur le visage de Moryse accentua la profonde cicatrice qui déchirait sa joue de l'oreille aux lèvres. Sa chance avait tourné, il le sentait. Tout irait pour le mieux à présent. ❧

### Notes

La distribution des biens se faisait par le biais de nombreux intermédiaires et ce n'était pas chaque fois directement au fabricant qu'on achetait ce dont on avait besoin. C'était d'autant plus vrai lors des campagnes militaires. Les armées étaient suivies d'un marché où les négociants proposaient leurs produits contre le fruit des rapines ou des soldes. Pour autant, l'argent n'était pas toujours remis à l'instant même et il n'était pas rare qu'on soit réglé à échéance. Parfois les ardoises s'accumulaient jusqu'à certains montants, surtout pour les achats les moins onéreux, étant donné le fort pouvoir libératoire de certaines monnaies.

Les échanges avec frère Rostain, pour ambigüs qu'ils pourraient paraître, n'ont pas de connotation sexuelle. L'univers affectif de nos ancêtres médiévaux est différent du nôtre. Leur vision de l'amour, de l'amitié et le rapport charnel que ces deux notions impliquent n'est pas simple à appréhender pour nos esprits contemporains. L'univers affectif est un sujet relativement peu connu de l'historiographie bien qu'étudié depuis que de grands noms comme Lucien Febvre ou Marc Bloch s'y sont penchés.

### Références

Boquet Damien, *L'ordre de l'affect au Moyen Âge. Autour de l'anthropologie affective d'Aelred de Rievaulx*, Caen : Publications du CRAHM, 2005.

Carré Yannik, *Le baiser sur la bouche au Moyen Âge. Rites, symboles, mentalités. XIe-XVe siècles*, Paris : Le léopard d'or, 1992.

[^1]: Nom donné à la Mer Morte.

[^2]: Les Occidentaux avaient souvent une vision fantaisiste des souverains et territoires lointains. Il parle du calife fatimide du Caire.

[^3]: Jeu de plateau où l'on cherche à faire des alignements avec des pions sur un cadre dessiné.

[^4]: Sorte de robe à manches longues, vêtement de base des populations moyen-orientales d'alors.

[^5]: Saint Martin d'été, dit aussi saint Martin le Bouillant, fêté le 4 juillet.

[^6]: Voir les notes de fin.

[^7]: Raymond III (vers 1140- 1187), comte de Tripoli de 1152 à 1187, prince de Galilée de 1174 à 1187.

[^8]: La Commune de Gênes.

[^9]: Shayzar, sur l'Oronte, Syrie.

[^10]: Engin de siège propulsant des pierres.

[^11]: Tunnel d'attaque pour un siège.

[^12]: Première lettre de saint Paul Apôtre aux Corinthiens 12,31.13,1-13.

[^13]: Tripoli, actuellement au Liban.

[^14]: Arménien.

[^15]: Byzantin.

[^16]: Début de la Chanson de Roland, traduction par Gérard Moignet, Paris, Bordas, 1969.

[^17]: Joscelin III d'Édesse (vers 1135-avant 1200), frère d'Agnès de Courtenay, première épouse d'Amaury de Jérusalem.

[^18]: Asad al-Dīn Shīrkūh (vers 1120-23 mars 1169), général de Nūr ad-Dīn, oncle de Saladin.

[^19]: *Que la lumière soit*, première parole de Dieu au début de la Genèse.

[^20]: *Et l'ombre fut*.

[^21]: Mu'īn ad-dīn Anur (ou Unur) ( ? - 1149), principal dirigeant de l'état bouride de Damas de 1135 à sa mort en 1149.

[^22]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^23]: Aujourd'hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^24]: Les messagers portaient généralement les messages dans des boîtes spécialement conçues pour cela.

[^25]: Tughtekin, atabeg de Damas de 1104 à 1128, fondateur de la dynastie Bouride.

[^26]: Administration royale des finances, sous l'autorité du sénéchal.

[^27]: Synonyme de mamlūk, désigne en contexte militaire un esclave formé à devenir un guerrier d'élite, souvent utilisé comme officier.

[^28]: Chapeau de feutre à fronton triangulaire, un des symboles du statut de guerrier chez les Turcs.

[^29]: Crème de sésame.

[^30]: Vêtement à ouverture croisée sur le devant.

[^31]: Bande de tissu décorée fixée au niveau du biceps.

[^32]: Litt. *bon*, *généreux*, en contexte religieux : *pieux*.

[^33]: Synonyme de mamlūk, désigne en contexte militaire un esclave formé à devenir un guerrier d'élite, souvent utilisé comme officier.

[^34]: Amaury de Jérusalem, (1136-1174), comte de Jaffa et d'Ascalon puis roi de Jérusalem à partir de 1163.

[^35]: Hugues d'Ibelin (? - 1170), seigneur d'Ibelin et de Rama.

[^36]: Aujourd'hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^37]: Formation de combat équestre.

[^38]: Les Templiers.

[^39]: Plus connu sous le nom de *Praecepta militaria*, attribué au grand général puis empereur byzantin Nicéphore II Phocas (v. 912 - 11 décembre 969).

[^40]: Jeudi 28 janvier 1154.

[^41]: Mesure pour les grains qui vaut à Damas à cette époque environ soixante-dix kilos.

[^42]: Aujourd'hui village des plateaux du Golan entre Liban, territoires occupés par Israël et la Syrie.

[^43]: Vendredi 2 avril 1154.

[^44]: Pièce de réception.

[^45]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^46]: Boisson traditionnelle syrienne à base de lait fermenté.

[^47]: Aujourd'hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^48]: Lundi 5 avril 1154.

[^49]: Mesure de poids pour les grains, qui vaut 3 irdabbs, soit un peu moins de 210 kg.

[^50]: Arménien.

[^51]: Lundi 5 avril 1154.

[^52]: Boîte de rangement des hosties.

[^53]: Bataille vers Buṭayḥa, 4 jours après la Saint Martin d'été, soit le 8 juillet.

[^54]: Aujourd'hui 'Ain al-Habis, dans les gorges du Yarmouk.

[^55]: Amaury de Jérusalem, (1136-1174), comte de Jaffa et d'Ascalon puis roi de Jérusalem à partir de 1163.

[^56]: Chiffon, torchon, mouchoir, selon le cas.

[^57]: Aujourd'hui Ramla, dans la plaine de Sharon.

[^58]: Lundi 26 novembre 1156.

[^59]: Grande robe de dessus.

[^60]: De Tyr.

[^61]: Tradition rapportée par al-Hasan ben Yahyâ al-Hissani.

[^62]: Dimanche 13 janvier 1157.

[^63]: Une des portes de la cité de Damas.

[^64]: Allusion au proverbe qui dit : Menacer le brave de la mort, c'est menacer le canard de la rivière.

[^65]: Samedi 23 février 1157.

[^66]: Longue extrémité du turban.

[^67]: Villes du sud de la Syrie.

[^68]: Le roi Baudoin Ier de Jérusalem.

[^69]: Bernard Vacher

[^70]: Sorte de robe à manches longues, vêtement de base des populations moyen-orientales d'alors.

[^71]: Village abandonné des cases de Cotteughes (Cantal).

[^72]: Unité militaire, de cavalerie généralement, mais qui peut avoir un sens général.

[^73]: Cf. Qit'a « Leviathan »

[^74]: Unité militaire, de cavalerie généralement, mais qui peut avoir un sens général.

[^75]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^76]: Cf. Qit'a « Chétif destin »

[^77]: Bâton, hampe. Désigne plus largement tout ce qui comporte un long manche, arme comme outil.

[^78]: Formation de combat équestre.

[^79]: Sorte de robe à manches longues, vêtement de base des populations moyen-orientales d'alors.

[^80]: Boisson traditionnelle syrienne à base de lait fermenté.

[^81]: Les Occidentaux avaient souvent une vision fantaisiste des souverains et territoires lointains. Il parle du calife fatimide du Caire.

[^82]: Jabāl Ansariya, massif montagneux prolongeant le mont Liban vers le nord, en suivant le littoral méditerranéen.

[^83]: Communauté mystique chiite ismaélienne, dont font partie les célèbres Assassins.

[^84]: Jabāl Zāwiya, région montagneuse prolongeant le plateau d'Alep vers le sud.

[^85]: Milice urbaine.

[^86]: Iz Al-Deen Abi Al-Asaker Sultan Mrdad, oncle du fameux Usamah ibn Munqidh et émir de Shayzar (...-1154).

[^87]: Majd Al-Deen Abi Salamah Murshed (1068- 1137), père d'Usamah ibn Munqidh, qui abdiqua en faveur de son frère Iz Al-Deen Sultan pour se consacrer à la chasse et à l'étude du Coran.

[^88]: Apamée des Latins, bourg fortifié aux abords de Qal'at al-Madhīq, cité antique d'Apameia, en Syrie.

[^89]: Chapeau de feutre à fronton triangulaire, un des symboles du statut de guerrier chez les Turcs.

[^90]: Gouverneur militaire, rôle récent crée par l'administration seldjoukide.

[^91]: Mystique musulman.

[^92]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^93]: Allusion au fait que le cadi, juge musulman, se déplaçait traditionnellement à dos de mule.

[^94]: Ou encore *fondouk*, *funduq*. Caravansérail pour les marchands.

[^95]: Alors capitale du pays, désormais intégrée au Caire, dont elle était voisine.

[^96]: Afrique du Nord, du Maroc à la Lybie.

[^97]: Chapeau en forme de pain de sucre assez haut, qui peut être soutenu par une structure rigide.

[^98]: Large robe aux manches amples.

[^99]: Aumône obligatoire calculée sur les revenus et le capital des marchandises et métaux précieux conservés au moins un an.

[^100]: Région perse située au sud de la mer d'Aral, en Ouzbékistan actuel.

[^101]: Les pouvoirs publics scellaient des bourses de monnaies dont le contenu était garanti, permettant d'attester d'une valeur sans peser individuellement chaque pièce.

[^102]: Robe large, à manches très larges.

[^103]: Contrat commercial où des investisseurs complètent l'apport d'un entrepreneur souhaitant investir, en échange d'une part des profits.

[^104]: Terme par lequel les marchands en terre d'Islam désignent le vin, mais qui peut aussi désigner toute sorte d'autres boissons.

[^105]: Conteneur à vin, souvent vendu par lot.

[^106]: Milice urbaine.

[^107]: Terme désignant l'office des douanes latin. Le terme vient du fait que c'était généralement de lui que dépendait l'abaissement ou la montée de la chaîne fermant le port.

[^108]: Le Caire, *al-Qahira* (la victorieuse). C'est aussi le nom donné à l'Égypte en général.

[^109]: Boisson traditionnelle syrienne à base de lait fermenté.

[^110]: Outil de coupe des toisons, des poils en général, selon leur taille.

[^111]: Beau-frère.

[^112]: Combat, mêlée, avec une dimension ludique souvent.

[^113]: On estimait alors que saint Pierre avait été le premier évêque de la ville.

[^114]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^115]: 23 avril.

[^116]: Allusion à la Seconde croisade de 1148.

[^117]: Thierry d'Alsace, comte de Flandre.

[^118]: Nersès IV Chnorhali, Catholicos de l'Église apostolique arménienne de 1166 à 1173. Il fut également un grand écrivain et poète.

[^119]: Saint Bernard, qui fut un des grands promoteurs de la Seconde croisade.

[^120]: Voir le Qit'a *Tortueuse âme*, seconde partie.

[^121]:  Avec la matière de Bretagne et celle de France, le *Roman d'Alexandre* constituait le socle de la littérature courtoise du XIIe siècle.

[^122]: Tente, généralement de qualité et destinée à des nobles.

[^123]: Les Templiers.

[^124]: Contraction de *bec-jaune*, très jeune personne.

[^125]: Arnulf de Chocques, surnommé parfois *Malcouronne*, mauvaise tonsure. Patriarche latin de Jérusalem de 1112 à 1118 bien que temporairement démis en 1115-1116.

[^126]: Terme usuel pour désigner les Grecs, c'est à dire les Byzantins.

[^127]: Terme usuellement utilisé pour désigner le royaume de Géorgie.

[^128]: Recueil de chants religieux pour les offices.

[^129]: Foucher d'Angoulême, patriarche de 1146 à 1157.

[^130]: Saint-Jean d'Acre, aujourd'hui Akko, en Israël.

[^131]: Dédicace de la cathédrale de Beauvais.

[^132]: Fabricant d'arçon de selle.

[^133]: « Maître », en l'occurrence, responsable d'un des secteurs du marché.

[^134]: Fabricant de rênes, freins, étrivières et de sangles de sellerie, de façon générale.
