Commandes de compilation :

- odt :

pandoc qita_recueil_03_part00_garde.md qita_recueil_03_part01_introduction.md qita_recueil_03_part02_texte.md qita_recueil_03_part03_explications.md --wrap=none -o odt/qita_recueil_03_epub.odt


- epub :

pandoc qita_recueil_03_part00_garde_epub.md qita_recueil_03_part01_introduction_epub.md qita_recueil_03_part02_texte.md --wrap=none -o epub/qita_recueil_03_epub.md


- TeX :

pandoc qita_recueil_03_part00_garde_pdf.md qita_recueil_03_part01_introduction_pdf.md qita_recueil_03_part02_texte.md --wrap=none --base-header-level=1 --top-level-division=chapter -o TeX/qita_recueil_03_impress.tex
