
![Framabook, le pari du livre libre](logo_framabook_epub.jpg)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d’informations sur Framasoft, consultez [http://www.framasoft.org](http://www.framasoft.org)

Se démarquant de l’édition classique, les Framabooks sont dits « livres libres » parce qu’ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu’un utilisateur de logiciels libres. Les Framabooks s’inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l’appropriation collective de la connaissance.

Pour plus d’informations sur le projet Framabook, consultez [http://framabook.org](http://framabook.org)

Copyright 2020 : Yann Kervran, Framasoft (coll. Framabook)

Ce recueil de textes Qit’a est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

ISBN : 979-10-92674-30-9   
Dépôt légal : Mai 2020   
Couverture : Eadmer de Canterbury écrivant, vers 1140-1150. Domaine Public - [commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:Eadmer_of_Canterbury_Writing_-_Google_Art_Project_(6827628).jpg)
