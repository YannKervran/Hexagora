\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Poing de trop}{1}{chapter.0.1}%
\contentsline {section}{\numberline {}Jéricho, midi du vendredi~27~juin~1158}{1}{section.0.1.1}%
\contentsline {section}{\numberline {}Abords du Fier, matin du mardi~1\textsuperscript {er}~juillet~1158}{3}{section.0.1.2}%
\contentsline {section}{\numberline {}Château du Fier, fin d'après-midi jeudi~4~septembre~1158}{6}{section.0.1.3}%
\contentsline {section}{\numberline {}Désert d'al-Gifar, après-midi du vendredi~19~septembre~1158}{9}{section.0.1.4}%
\contentsline {section}{\numberline {}Notes}{12}{section.0.1.5}%
\contentsline {section}{\numberline {}Références}{12}{section.0.1.6}%
\contentsline {chapter}{\numberline {2}Omphalos}{13}{chapter.0.2}%
\contentsline {section}{\numberline {}Jérusalem, quartier Saint-Martin, aube du lundi~5~août~1157}{13}{section.0.2.1}%
\contentsline {section}{\numberline {}Jérusalem, porte de Sion, matin du lundi~5~août~1157}{15}{section.0.2.2}%
\contentsline {section}{\numberline {}Jérusalem, rue du Mont Sion, fin de matinée du lundi~5~août~1157}{18}{section.0.2.3}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, après-midi du lundi~5~août~1157}{20}{section.0.2.4}%
\contentsline {section}{\numberline {}Jérusalem, rue de David, fin d'après-midi du lundi~5~août~1157}{23}{section.0.2.5}%
\contentsline {section}{\numberline {}Notes}{25}{section.0.2.6}%
\contentsline {section}{\numberline {}Références}{26}{section.0.2.7}%
\contentsline {chapter}{\numberline {3}Hostel et parentèle}{27}{chapter.0.3}%
\contentsline {section}{\numberline {}Jérusalem, parvis du Temple, fin de matinée du samedi~9~janvier~1148}{27}{section.0.3.1}%
\contentsline {section}{\numberline {}Jérusalem, cloître du \emph {Templum Domini}, matin du mercredi 6 septembre 1150}{29}{section.0.3.2}%
\contentsline {section}{\numberline {}Jérusalem, piscine de Siloé, soir du mardi~20~mars~1151}{32}{section.0.3.3}%
\contentsline {section}{\numberline {}Jérusalem, quartier des tanneurs, veillée du lundi~4~avril~1155}{34}{section.0.3.4}%
\contentsline {section}{\numberline {}Notes}{37}{section.0.3.5}%
\contentsline {section}{\numberline {}Références}{38}{section.0.3.6}%
\contentsline {chapter}{\numberline {4}Universelle aragne}{39}{chapter.0.4}%
\contentsline {section}{\numberline {}Tripoli, demeure de Willelmo Marzonus, fin de journée du samedi~10~novembre~1156}{39}{section.0.4.1}%
\contentsline {section}{\numberline {}Shayzar, abords de la citadelle, matin du lundi~21~octobre~1157}{41}{section.0.4.2}%
\contentsline {section}{\numberline {}Pont du Fer, midi du mercredi~22~janvier~1158}{45}{section.0.4.3}%
\contentsline {section}{\numberline {}Tripoli, quartier du port, fin de journée du jeudi~27~février~1158}{48}{section.0.4.4}%
\contentsline {section}{\numberline {}Notes}{51}{section.0.4.5}%
\contentsline {section}{\numberline {}Références}{52}{section.0.4.6}%
\contentsline {chapter}{\numberline {5}Ombres et lumières}{53}{chapter.0.5}%
\contentsline {section}{\numberline {}Jérusalem, porte de David, matin du mardi~18~mars~1158}{53}{section.0.5.1}%
\contentsline {section}{\numberline {}Bethléem, palais de l'évêque Raoul, midi du lundi~14~juillet~1158}{57}{section.0.5.2}%
\contentsline {section}{\numberline {}Casal de Saint-Gilles, taverne de Clarembaud, veillée du jeudi~11~septembre~1158}{59}{section.0.5.3}%
\contentsline {section}{\numberline {}Château de Naplouse, cuisines, soirée du samedi~27~décembre~1158}{62}{section.0.5.4}%
\contentsline {section}{\numberline {}Notes}{64}{section.0.5.5}%
\contentsline {section}{\numberline {}Références}{64}{section.0.5.6}%
\contentsline {chapter}{\numberline {6}L'héritier}{65}{chapter.0.6}%
\contentsline {section}{\numberline {}Naalein, maison-forte, fin d'après-midi du mardi 20 mai 1147}{65}{section.0.6.1}%
\contentsline {section}{\numberline {}Naalein, maison forte, fin de matinée du dimanche 28 août 1149}{69}{section.0.6.2}%
\contentsline {section}{\numberline {}Jérusalem, rue de Josaphat, fin d'après-midi du mercredi 22 octobre 1152}{72}{section.0.6.3}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, soirée du mardi 18 novembre 1158}{76}{section.0.6.4}%
\contentsline {section}{\numberline {}Notes}{78}{section.0.6.5}%
\contentsline {section}{\numberline {}Références}{79}{section.0.6.6}%
\contentsline {chapter}{\numberline {7}Gobelet rituel}{81}{chapter.0.7}%
\contentsline {section}{\numberline {}Jérusalem, porte de David, matin du lundi~3~juin~1157}{81}{section.0.7.1}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, entrepôts, midi du vendredi~7~juin~1157}{84}{section.0.7.2}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, cuisines, début d'après-midi du vendredi~7~juin~1157}{87}{section.0.7.3}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, hôtellerie, après-midi du vendredi~7~juin~1157}{90}{section.0.7.4}%
\contentsline {section}{\numberline {}Jérusalem, taverne du père Josce, fin d'après-midi du vendredi~7~juin~1157}{91}{section.0.7.5}%
\contentsline {section}{\numberline {}Notes}{93}{section.0.7.6}%
\contentsline {section}{\numberline {}Références}{95}{section.0.7.7}%
\contentsline {chapter}{\numberline {8}Acédie}{97}{chapter.0.8}%
\contentsline {section}{\numberline {}Baniyas, après-midi du mercredi~6~novembre~1140}{97}{section.0.8.1}%
\contentsline {section}{\numberline {}Baniyas, soir du vendredi~28~décembre~1151}{101}{section.0.8.2}%
\contentsline {section}{\numberline {}Baniyas, cloître canonial, midi du vendredi~13~janvier~1156}{106}{section.0.8.3}%
\contentsline {section}{\numberline {}Baniyas, souk des cordonniers, soir du lundi~20~mai~1157}{109}{section.0.8.4}%
\contentsline {section}{\numberline {}Notes}{112}{section.0.8.5}%
\contentsline {section}{\numberline {}Références}{113}{section.0.8.6}%
\contentsline {chapter}{\numberline {9}Liaisons dangereuses}{115}{chapter.0.9}%
\contentsline {section}{\numberline {}Palais royal, salle basse, après-midi du samedi~2~février~1157}{115}{section.0.9.1}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, chambre du comte de Jaffa, matin du jeudi~20~juin~1157}{118}{section.0.9.2}%
\contentsline {section}{\numberline {}Jérusalem, église du Saint-Sépulcre, matin du vendredi~16~août~1157}{122}{section.0.9.3}%
\contentsline {section}{\numberline {}Notes}{126}{section.0.9.4}%
\contentsline {section}{\numberline {}Références}{127}{section.0.9.5}%
\contentsline {chapter}{\numberline {10}Fléau ballant}{129}{chapter.0.10}%
\contentsline {section}{\numberline {}Abords de Souq Wadi Barada, midi du youm~al~khemis 11~dhu~al-qi'dah~548}{129}{section.0.10.1}%
\contentsline {section}{\numberline {}Village de Taranjeh, demeure du ra'is, soirée du youm~al~joumouia~16~muharram~549}{133}{section.0.10.2}%
\contentsline {section}{\numberline {}Baniyas, après-midi du youm~el~itnine~19~muharram~549}{135}{section.0.10.3}%
\contentsline {section}{\numberline {}Baniyas, maison de Giorgos, soirée du youm~el~itnine~19~muharram~549}{139}{section.0.10.4}%
\contentsline {section}{\numberline {}Notes}{140}{section.0.10.5}%
\contentsline {section}{\numberline {}Références}{141}{section.0.10.6}%
\contentsline {chapter}{\numberline {11}Le disparu}{143}{chapter.0.11}%
\contentsline {section}{\numberline {}Lydda, chambre de l'évêque, matin du mardi~23~septembre~1158}{143}{section.0.11.1}%
\contentsline {section}{\numberline {}Lydda, sacristie de l'église Saint-Georges, matinée du mardi~23~septembre~1158}{146}{section.0.11.2}%
\contentsline {section}{\numberline {}Lydda, chantier de la nouvelle basilique Saint-Georges, fin de matinée du mardi~23~septembre~1158}{148}{section.0.11.3}%
\contentsline {section}{\numberline {}Lydda, salle d'audience du palais de l'évêque, après-midi du mardi~23~septembre~1158}{150}{section.0.11.4}%
\contentsline {section}{\numberline {}Lydda, chambre de l'évêque, veillée du mardi~23~septembre~1158}{152}{section.0.11.5}%
\contentsline {section}{\numberline {}Lydda, chambre de l'évêque, nuit du mardi~23~septembre~1158}{155}{section.0.11.6}%
\contentsline {section}{\numberline {}Notes}{155}{section.0.11.7}%
\contentsline {section}{\numberline {}Références}{156}{section.0.11.8}%
\contentsline {chapter}{\numberline {12}Basses eaux}{157}{chapter.0.12}%
\contentsline {section}{\numberline {}Damas, fin de matinée du youm~el~itnine 10~chawwal~551}{157}{section.0.12.1}%
\contentsline {section}{\numberline {}Damas, après-midi du youm~al~had 29~dhu~al-qi’dah~551}{160}{section.0.12.2}%
\contentsline {section}{\numberline {}Damas, fin d'après-midi du youm~al~sebt 11~muharram~552}{164}{section.0.12.3}%
\contentsline {section}{\numberline {}Notes}{166}{section.0.12.4}%
\contentsline {section}{\numberline {}Références}{167}{section.0.12.5}%
\contentsline {chapter}{\numberline {13}Petits dieux}{169}{chapter.0.13}%
\contentsline {section}{\numberline {}Jérusalem, après-midi du mardi~27~août~1157}{169}{section.0.13.1}%
\contentsline {section}{\numberline {}Jérusalem, soirée du vendredi~27~juin~1158}{173}{section.0.13.2}%
\contentsline {section}{\numberline {}Colteja, nuit du mercredi~5~juin~1387}{174}{section.0.13.3}%
\contentsline {section}{\numberline {}Notes}{176}{section.0.13.4}%
\contentsline {section}{\numberline {}Références}{177}{section.0.13.5}%
\contentsline {chapter}{\numberline {14}Oultre Jourdain}{179}{chapter.0.14}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, après-midi du mercredi~26~février~1158}{179}{section.0.14.1}%
\contentsline {section}{\numberline {}Terre de Suète, plaine de Bu\d {t}ay\d {h}a, début d'après-midi du mardi~8~juillet~1158}{183}{section.0.14.2}%
\contentsline {section}{\numberline {}Terre de Suète, abords du Yarmūk près des caves de Suète, soirée du mercredi~9~juillet~1158}{185}{section.0.14.3}%
\contentsline {section}{\numberline {}Notes}{188}{section.0.14.4}%
\contentsline {section}{\numberline {}Références}{189}{section.0.14.5}%
\contentsline {chapter}{\numberline {15}Dette d'honneur}{191}{chapter.0.15}%
\contentsline {section}{\numberline {}Tyr, soirée du vendredi~18~juillet~1158}{191}{section.0.15.1}%
\contentsline {section}{\numberline {}Tyr, boutique de al Faytrouni, midi du lundi~21~juillet~1158}{193}{section.0.15.2}%
\contentsline {section}{\numberline {}Tyr, atelier de Fromont le braalier, matin du lundi~29~septembre~1158}{195}{section.0.15.3}%
\contentsline {section}{\numberline {}Palais de l'archevêque de Tyr, logement d'Herbelot Gonteux, après-midi du mercredi~1\textsuperscript {er}~octobre~1158}{198}{section.0.15.4}%
\contentsline {section}{\numberline {}Palais de Pierre de Tyr, bureau de l'archevêque, fin de matinée du jeudi~2~octobre~1158}{201}{section.0.15.5}%
\contentsline {section}{\numberline {}Notes}{204}{section.0.15.6}%
\contentsline {section}{\numberline {}Références}{204}{section.0.15.7}%
\contentsline {chapter}{\numberline {16}O Fortuna}{205}{chapter.0.16}%
\contentsline {section}{\numberline {}Shayzar, pont sur l'Oronte, matin du samedi~8~mars~1147}{205}{section.0.16.1}%
\contentsline {section}{\numberline {}Apamée, fin de matinée du jeudi~28~juillet~1149}{208}{section.0.16.2}%
\contentsline {section}{\numberline {}Apamée, ruines de la cité antique, début d'après-midi du dimanche~14~juillet~1157}{211}{section.0.16.3}%
\contentsline {section}{\numberline {}Notes}{214}{section.0.16.4}%
\contentsline {section}{\numberline {}Références}{217}{section.0.16.5}%
\contentsline {chapter}{\numberline {17}Mercator}{219}{chapter.0.17}%
\contentsline {section}{\numberline {}Alexandrie, fondaco des Pisans, matin du lundi~1\textsuperscript {er}~avril~1157}{219}{section.0.17.1}%
\contentsline {section}{\numberline {}Alexandrie, fondaco des Pisans, midi du mercredi~3~avril~1157}{221}{section.0.17.2}%
\contentsline {section}{\numberline {}Alexandrie, fondaco des Pisans, fin de soirée du samedi~6~avril~1157}{224}{section.0.17.3}%
\contentsline {section}{\numberline {}Alexandrie, fondaco pisan, midi du mercredi~24~avril~1157}{228}{section.0.17.4}%
\contentsline {section}{\numberline {}Notes}{231}{section.0.17.5}%
\contentsline {section}{\numberline {}Références}{231}{section.0.17.6}%
\contentsline {chapter}{\numberline {18}Nobilis in nobili}{233}{chapter.0.18}%
\contentsline {section}{\numberline {}Jérusalem, tour de David, veillée du mercredi~27~août~1158}{233}{section.0.18.1}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, chambre du roi, matin du vendredi~17~octobre~1158}{236}{section.0.18.2}%
\contentsline {section}{\numberline {}Antioche, porte Saint-Georges, midi du mercredi~27~mai~1159}{240}{section.0.18.3}%
\contentsline {section}{\numberline {}Notes}{243}{section.0.18.4}%
\contentsline {section}{\numberline {}Références}{244}{section.0.18.5}%
\contentsline {chapter}{\numberline {19}Kleos}{245}{chapter.0.19}%
\contentsline {section}{\numberline {}Naplouse, demeure de la reine mère, soirée du lundi~19~avril~1154}{245}{section.0.19.1}%
\contentsline {section}{\numberline {}Naplouse, jardins de la demeure de la reine, après-midi du lundi~26~avril~1154}{248}{section.0.19.2}%
\contentsline {section}{\numberline {}Naplouse, demeure de la reine, soirée du jeudi~17~juin~1154}{251}{section.0.19.3}%
\contentsline {section}{\numberline {}Naplouse, demeure de la reine, veillée du lundi~16~avril~1156}{254}{section.0.19.4}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, chambre d'Amaury, soirée du lundi~11~février~1157}{257}{section.0.19.5}%
\contentsline {section}{\numberline {}Notes}{258}{section.0.19.6}%
\contentsline {section}{\numberline {}Références}{259}{section.0.19.7}%
\contentsline {chapter}{\numberline {20}Un royaume en oubli}{261}{chapter.0.20}%
\contentsline {section}{\numberline {}Caves de Suète, tente chapelle du camp royal, matin du jeudi~10~juillet~1158}{261}{section.0.20.1}%
\contentsline {section}{\numberline {}Jérusalem, mont du Temple, après-midi du samedi~25~octobre~1158}{265}{section.0.20.2}%
\contentsline {section}{\numberline {}Tyr, palais de l'archevêque, après-midi du jeudi~6~novembre~1158}{269}{section.0.20.3}%
\contentsline {section}{\numberline {}Lydda, palais de l'évêque Constantin, début d'après-midi du vendredi~5~décembre~1158}{272}{section.0.20.4}%
\contentsline {section}{\numberline {}Notes}{274}{section.0.20.5}%
\contentsline {section}{\numberline {}Références}{275}{section.0.20.6}%
\contentsline {chapter}{\numberline {21}Boucles d'or}{277}{chapter.0.21}%
\contentsline {section}{\numberline {}Casal de Mimas, manse familial de Moryse, matin du lundi~9~février~1153}{277}{section.0.21.1}%
\contentsline {section}{\numberline {}Jérusalem, réfectoire des indigents de Saint-Jean de l'Hôpital, milieu de matinée du lundi~9~mai~1155}{279}{section.0.21.2}%
\contentsline {section}{\numberline {}Jérusalem, salle des malades de Saint-Jean de l'Hôpital, matin du mardi~9~juillet~1157}{282}{section.0.21.3}%
\contentsline {section}{\numberline {}Montagnes de Samarie, matin du vendredi~26~juillet~1157}{285}{section.0.21.4}%
\contentsline {section}{\numberline {}Harim, marché du camp Latin, midi du mardi~7~janvier~1158}{288}{section.0.21.5}%
\contentsline {section}{\numberline {}Notes}{292}{section.0.21.6}%
\contentsline {section}{\numberline {}Références}{292}{section.0.21.7}%
\contentsfinish 
