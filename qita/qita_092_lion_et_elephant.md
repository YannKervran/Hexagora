---
date : 2019-06-15
abstract : 1159. Volontaire pour une importante campagne militaire dans la principauté d’Antioche, Ernaut va y faire la rencontre d’un chevalier hors-norme, Renaud de Châtillon.
characters : Ernaut, Renaud de Châtillon, Daimbert, Gaston
---

# Le lion et l’éléphant

## Jérusalem, rue du Temple, aube du mercredi 15 avril 1159

Dans la fraîcheur de l’aurore, son porte-manteau sur l’épaule, Ernaut avançait gaiement dans la rue commerçante encore à demi ensommeillée. La plupart des échoppes étaient fermées et les plus matinaux des vendeurs s’affairaient à encombrer le passage avec leurs sacs de fèves et de pois, leurs paniers d’épices ou de grain, leurs pots de condiments. Il leur prêtait des remarques appréciatrices sur sa fière allure, son allure martiale impressionnante et cela le mettait en joie.

Il s’était porté volontaire pour rejoindre le roi Baudoin à Antioche. On y rassemblait tout ce que les territoires latins pouvaient compter de forces militaires afin d’aller assiéger la ville de Nūr al-Dīn[^nuraldin], la prestigieuse Alep. Le basileus byzantin Manuel Comnène était descendu avec une considérable armée 
dont on disait qu’elle recouvrait les reliefs. De nombreux croisés, venus pour Pâques, avaient décidé de se joindre également à l’expédition. Depuis plus de dix ans et l’assaut manqué sur Damas, on n’avait pas vu un pareil enthousiasme, une telle effervescence.

Ernaut estimait que c’était là une occasion rêvée de faire ses preuves. Il avait déjà tout son équipement de combat, et n’avait eu qu’à se munir d’un grand sac en cuir huilé qui supporterait le voyage par bateau jusqu’au nord. La perspective de parcourir de nouveaux lieux l’excitait au plus haut point. On lui avait tant vanté la cité de saint Pierre, les montagnes de Cilicie… Et La mystérieuse Alep, qui représentait depuis des dizaines d’années le repaire des adversaires les plus acharnés du royaume latin. Il était plus que temps de s’en emparer et d’en faire une fidèle baronnie. En souriant, Ernaut s’imaginait prêtant hommage à Baudoin pour ce nouveau fief qui n’allait pas tarder à naître, en récompense de ses futurs exploits sur les murailles.

Les abords du palais étaient encore plus agités qu’à l’ordinaire. Même si la plupart des officiers et des hommes du roi étaient déjà au nord depuis des mois, c’était là que beaucoup de voyageurs se regroupaient ou venaient simplement recevoir leurs instructions. On assemblait généralement la caravane à la porte de David, à l’ouest de la cité avant de prendre la route pour Jaffa. Gaston, vétéran qui avait initié Ernaut au combat d’infanterie, s’avançait vers lui alors qu’il franchissait à peine le portail.

« Tu arrives à point, gamin. Viens-t’en avec moi à l’arsenal. »

Sans un mot de plus, il partit en direction de la citadelle de David, où la plupart des armes et armures étaient stockées. Les fantassins royaux avaient leur équipement de guerre en propre, mais ils étaient tenus de les laisser à l’abri. Ils ne conservaient que leurs protections de corps et leur épée. Le reste était distribué au moment des campagnes, ou pour les exercices. À côté du sergent boiteux, Ernaut avançait d’un pas allègre, affichant toujours sa bonne humeur du matin. Gaston la remarqua et arbora un rictus dont on ne savait s’il était gai ou cynique.

« Le voilà ce bel assaut dont tu rêvais, jeune, on dirait bien !

— J’ose l’encroire ! répliqua Ernaut, un large sourire aux lèvres.

— Ici, ils disent que *la vie est semblable au feu, elle commence par la fumée et finit par la cendre*. C’est aussi vrai de tous les combats. Garde-toi bien de trop en espérer… »

Lançant un rapide regard en coin au petit homme noueux qui trottinait à ses côtés, Ernaut se rembrunit. Gaston était souvent acide, voire carrément rabat-joie. Peut-être était-il vexé de s’être fait écarter de la troupe qui allait s’emparer d’Alep. À son âge, qu’Ernaut estimait autour de la quarantaine, il était bon d’avoir accumulé un pécule pour sa retraite, afin de profiter de la vie. Sa situation actuelle démontrait qu’à l’évidence il n’y était pas parvenu. Et il allait rater une des plus belles occasions de s’enrichir que les soldats aient connue depuis de nombreuses années.

Reconnus par les sentinelles de faction, ils pénétrèrent sans halte dans la cour où une interminable cohue d’ânes et de mulets attendaient dans les cris et l’agitation que le chargement soit fini. Des valets attachaient boucliers, lances et toiles pour les hommes du rang, sous la supervision de clercs à l’air concentré, stylets et tablettes de cire en main. D’autres sergents patientaient, aidaient à fixer les matériels. Ni Droart ni Eudes n’étaient là, ce qu’Ernaut regrettait un peu. Il n’avait aucun proche parmi les soldats avec qui il allait passer les semaines, voire les mois, à venir. Il aurait même supporté la présence de Baset, mais ce dernier arrivait à esquiver tous les enrôlements avec talent et il était exceptionnel qu’il aille bien loin de l’enceinte de la cité.

De la voix, de la main, Gaston pressait le mouvement. Il était prévu de corner le départ quand le couvent du Saint-Sépulcre marquerait tierce. D’ici là, toutes les bêtes devaient être assemblées en file à l’extérieur des murailles. Le cortège d’animaux franchissait à peine les portes de la ville que la grosse cloche se mit à sonner. Les cavaliers étaient en place loin devant, chacun sous la bannière qu’il servirait lors des affrontements. Venaient ensuite les contingents de fantassins qui patientaient en une longue colonne derrière eux. Enfin, en dernier étaient disposés les bagages, où chameaux et mules étaient menés par une légion de valets.

De nombreux habitants, et même des voyageurs, des pèlerins, s’étaient amassés pour admirer l’impressionnante procession militaire, encourageant ces valeureux combattants de la foi avec des chants, des prières, des appels de voix. Quelques familles disaient aussi au revoir à ceux des leurs qui partaient pour une périlleuse aventure. Ernaut regrettait de ne pas voir Libourc parmi tous ces badauds, tandis qu’il rejoignait son groupe. Il aurait aimé la serrer dans ses bras une dernière fois, il avait à peine eu le temps de lui faire savoir qu’il allait au loin. Il n’eut pas le loisir de laisser ces pensées sombres s’installer : la grande bannière à l’avant venait de s’incliner, ordre bien vite repris par les voix fortes des responsables d’unité. En un instant, le faubourg fut noyé de la poussière de leur avancée.

## Village de Hammam, gué de la Balanée, fin d’après-midi du lundi 25 mai 1159

De faction au gué qui traversait le Nahr Ifrin, Ernaut fut parmi les premiers à voir arriver la file des marcheurs blancs de poussière. Quelques-uns, certainement des barons d’importance, avaient reçu une monture de leur escorte byzantine. Les autres avançaient, hagards, pieds nus et en sous-vêtements sales, la barbe longue et le cheveu hirsute. On aurait dit une armée de mendiants. C’étaient les prisonniers francs que le basileus Manuel avait pu faire libérer suite à des échanges diplomatiques avec l’émir Nūr al-Dīn : plusieurs centaines d’hommes du commun et une poignée de détenus notables.

Fragiles sur leurs jambes, certains hésitèrent à entrer dans l’eau du gué malgré la corde qui avait été tendue au long pour justement les aider. Sur la rive occupée par l’armée latine, c’était la consternation. Dans un silence de mort, quelques-uns tentaient de reconnaître des visages, des silhouettes.

Au milieu des plénipotentiaires byzantins se tenait un petit groupe de Turcs, coiffés du sharbush[^sharbush], le sabre et l’arc au côté. Ils accompagnaient un civil, un arabe monté sur un magnifique étalon, qu’il ne semblait guère maîtriser. Un érudit quelconque capable de parler plusieurs langues, mais peu familier des voyages en selle. Sa posture fit ricaner les Latins, soulagés de trouver un exutoire à la tension qui s’était établie. Certains parièrent même sur sa chute probable dans le vif courant, mais ils en furent pour leurs frais, et se contentèrent de cracher dans sa poussière une fois qu’il les avait dépassés.

Parmi son groupe, Ernaut avait bien sympathisé avec Daimbert. Sergent du prince de Galilée, c’était lui qui était en charge du petit comité qui surveillait le passage à travers le cours d’eau. Il était monté sur son cheval pour faire bon accueil aux arrivants et les regardait défiler, faisant signe à tous de continuer à avancer sans ralentir.

Tandis que le flot des captifs s’étendait inlassablement devant eux, Ernaut fronça les sourcils. Il comprenait ce que signifiait la défaite, quand on n’était pas tué durant les affrontements, mais se voyait confronté pour la première fois à la réalité du destin des prisonniers. Il tentait de se montrer aimable, encourageant de la voix les hommes à ne pas ralentir, leur vantant le confort qui les attendait. À côté de lui Daimbert demeurait muet, suçotant un morceau de bois qui lui servait de cure-dent. Il ne rompit le silence que lorsque la colonne eut entièrement franchi le gué, incitant chacun à un redoublement de prudence.

« Tu crois qu’ils pourraient nous assaillir alors que leur envoyé est au parmi du camp ?

— Ces diables ont toujours quelque ruse en leur besace…

— Ils ne doivent pas se sentir si vaillants, qu’ils acceptent toutes les demandes qu’on leur fait !

— C’est surtout la vaillance des Griffons[^griffon] qui va pas tarder à manquer, m’est avis. »

Ernaut dévisagea son chef, surpris de l’entendre émettre un commentaire désobligeant sur leurs alliés. Il était plutôt fort en gueule, mais semblait n’avoir que peu de colère en lui, hurlant juste pour se faire comprendre des plus rustiques de ses hommes.

« Il se dit des choses ?

— Certes oui. Il y a eu force messages échangés et certains des Griffons du souk racontent qu’ils vont bientôt remballer. »

Ernaut n’en crut pas ses oreilles. Jamais les négociants byzantins ne quitteraient le camp sans l’armée. Cela signifiait forcément que le basileus prévoyait de prendre la route.

« On va enfin lancer l’assaut ?

— Nous, je sais pas, mais il semblerait que le roi Manuel ait décidé de s’en retourner chez lui.

— Comment ça ? Va-t-on courir sus Halepe sans lui ?

— Je serais pas surpris qu’on reparte sans même en avoir vu la muraille. »

Daimbert soupira longuement avant d’accorder un sourire amer à Ernaut.

## Village de Hammam, camp latin du gué de la Balanée, après-midi du samedi 30 mai 1159

Le petit hameau depuis longtemps déserté de Hammam accueillait des sources chaudes qui avaient permis l’installation de bains, à la jonction du camp militaire et des marchés périphériques, en bordure des aménagements de toile, de corde et de bric-à-brac qui caractérisaient tous les établissements de campagne. L’accès y était assez difficile pour les hommes du peuple, qui se contentaient souvent de se baigner dans le Nahr Ifrin. Comme beaucoup d’autres, Ernaut avait pris le parti de se rafraîchir dans une boucle de la rivière en amont des endroits où l’on risquait de souiller l’eau. Il y avait ses habitudes, avec quelques compagnons dont certains y faisaient parfois leur lessive.

Au milieu des flots, il se savonnait activement les cheveux et les oreilles, les yeux fermés, quand il perçut soudain une grande agitation autour de lui. Il plongea la tête dans le courant pour se rincer et comprendre ce qui se passait. Lorsqu’il tourna son regard vers la rive, il se figea.

Un lion, ou une lionne, à son absence de crinière, déambulait nonchalamment parmi les affaires du petit groupe, la queue ondulant tandis qu’elle reniflait les objets. Elle n’était qu’à quelques pas d’Ernaut, qu’elle considéra soudain avec curiosité. La tête penchée, la gueule à demi ouverte, elle semblait surprise par ce qu’elle voyait. Face à elle, Ernaut était nu, le corps en partie couvert de mousse, une savonnette à la main.

C’était la première fois qu’il était face à une telle bête et il percevait les remugles acides de sa transpiration ramper jusqu’à lui, il entendait son souffle alors qu’elle l’étudiait. Il n’avait aucune idée des habitudes de ces animaux et se prit à prier pour qu’elle n’eût pas faim.

Le temps parut s’étirer tandis que la lionne le fixait de ses yeux attentifs. Puis elle bâilla, dévoilant des crocs propres à briser l’échine du plus colossal des humains. Ernaut sentit ses jambes faiblir, son bas-ventre se crisper. Il n’y avait aucun bruit aux alentours et il espérait que ses compagnons étaient partis chercher de quoi faire fuir le fauve. Il serra fort son poing libre et se raidit soudain. Puis il s’élança en hurlant vers la lionne, lui balançant sa savonnette en plein museau tandis qu’il projetait de grandes gerbes d’eau en avançant. En deux bonds, l’animal s’esquiva dans les buissons, laissant Ernaut essoufflé, dégoulinant, le cœur à cent à l’heure, tout seul et nu sur la berge.

Il entendit alors de nouveau le vent dans les branchages et des voix qui s’approchaient. Une demi-douzaine de soldats accouraient avec des lances et des arcs. En voyant Ernaut ainsi immobile, indemne et ébahi, ils éclatèrent de rire.

## Village de Hammam, camp latin du gué de la Balanée, veillée du samedi 30 mai 1159

L’histoire du sergent du roi qui chassait le lion entièrement nu ne mit que fort peu de temps à parcourir le camp, apportant un peu de légèreté dans l’atmosphère oppressante qui régnait jusque là. L’annonce du départ byzantin avait plongé les troupes dans la morosité, et chacun accueillit avec soulagement l’amusante anecdote. Ernaut goûtait fort son succès et raconta la scène une bonne dizaine de fois durant son souper, agrémentant chaque nouveau récit de détails imaginatifs. Il ne fut donc que modérément surpris qu’on vienne le chercher pour aller narrer son exploit ailleurs. Il ne s’attendait néanmoins pas à être guidé jusqu’à la tente maîtresse du prince d’Antioche, Renaud de Châtillon.

Ce chevalier ombrageux dont le courage forçait l’admiration des hommes se tenait un peu en retrait après avoir dû s’humilier devant le basileus byzantin Manuel, en demande de pardon de ses exactions passées. Ernaut se souvenait que lors de sa venue en Terre sainte, ils avaient dû éviter l’île de Chypre à cause de son raid[^voirt1]. Il se racontait d’ailleurs que le féroce prince n’avait que fort peu d’égard pour ceux qui le contrariaient. Le patriarche de sa cité avait fini nu et enduit de miel en haut d’une tour pour avoir osé s’opposer à lui.

Avançant tout en frottant sa cotte de la main, Ernaut échangea quelques mots avec le valet, histoire de ne pas commettre d’impairs. Celui-ci, un jeune arménien à l’accent chantant, lui expliqua que le prince était de bonne humeur, ayant entendu des jongleurs toute la soirée. Lorsqu’il avait appris l’anecdote, il avait immédiatement désiré rencontrer le sergent qui chassait les lions sans arme, sans préciser rien de plus.

Lorsqu’Ernaut pénétra dans la tente, il fut surpris du luxe qu’il y découvrit. En dehors des mâts et des grincements des cordages, on se serait cru dans un palais : épais tapis sur le sol, nombreuses lampes à huile, tentures bariolées dont les motifs de soie accrochaient la lumière. Une dizaine de sièges confortablement rembourrés de coussin étaient disposés en demi-cercle face à l’espace servant de scène, où se produisaient un musicien et un conteur.

Au centre de l’hémicycle ainsi formé, sur un trône plus imposant que ses voisins, se tenait le prince d’Antioche. Ernaut l’avait déjà aperçu au loin, mais il le voyait véritablement pour la première fois. Il était assez grand, brun de poil et de peau. D’une trentaine d’années, il avait un visage carré d’où partait un nez légèrement busqué, fermé d’un menton en galoche. Une barbe très courte et des cheveux frisés coupés à l’écuelle indiquaient l’homme de guerre qu’il était, ainsi que les fines rides au coin des yeux qui plissaient son teint buriné. Il n’était plus si imposant une fois dépouillé de son harnois, mais ses gestes trahissaient son goût pour l’action, un corps  préparé à tous les efforts.

En voyant Ernaut entrer, il rugit littéralement, interrompant sans égard le spectacle pour permettre au jeune sergent de s’avancer.

« Ah, voilà donc notre héros ! Comment t’appelles-tu, mon garçon ? Goliath ? Samson ?

— Ernaut, sire prince.

— Ah, que ce nom sonne doux à mes oreilles ! On dirait le mien ! Peut-être m’attribuera-t-on ton exploit ? »

Il éclata de rire, satisfait de sa propre blague.

« Cela ne serait pas de trop, il y en a bien assez à dégoiser sur moi en ce moment, cela leur clouerait le bec à tous. »

D’un geste, il intima à un de ses voisins de laisser son siège et indiqua à Ernaut d’y rendre place. Le jeune homme en rougit de plaisir et de gêne tandis que le chevalier ainsi mis à l’écart lançait un regard hautain à Ernaut, ce que Renaud aperçut.

« N’ayez qu’amitié pour ce sergent le roi, compères. Voilà vaillant qui vaut bien cent Griffons, dont il nous faudrait pleines échelles ! Il chasse le lion avec un savon ! »

Puis, se tournant, hilare, vers Ernaut, il ajouta : « Avais-tu l’intention de lui frictionner le museau ? »

N’attendant même pas sa réponse, il répartit d’un grand rire et fit signe qu’on leur serve à tous du vin. Emportés par le plaisir de leur seigneur, les chevaliers firent un relatif bon accueil à Ernaut. S’ils n’étaient pas toujours à l’aise avec le peu de sens des convenances de leur prince, ils en partageaient le goût pour la bravoure.

« Figure-toi que j’ai mandé ces jongleurs, car ils sont Englois et ils connaissent un conte de leur pays qui parle d’un chevalier tel que toi. »

D’un geste, il apostropha le ménestrel, qui attendait patiemment qu’on lui demande de reprendre sa narration.

« Et comment s’appelle donc le brave qui se fait compaigner d’un lion ?

— Owein, sire prince. »

Renaud de Châtillon se tourna alors vers Ernaut et, faisant glisser un petit anneau d’or d’un de ses doigts, il le donna au géant.

« Voilà, ce sera désormais ton nom auprès de moi. Si tu as désir de venir dans les terres du Nord, si tu ne te plais plus à Jérusalem…. Quels que soient tes besoins, cet anel sera ton sauf-conduit jusqu’à moi, *Yvain*. »

Puis il demanda à Ernaut de leur raconter à tous comment on s’y prenait pour chasser le fauve quand on n’avait pas d’armes en dehors d’une savonnette. Il était espiègle et enjoué comme un enfant au premier jour de vacances et riait aux éclats à la moindre occasion. Un vrai chevalier comme Ernaut avait toujours rêvé d’en rencontrer. Peut-être ne serait-il pas de trop d’ajouter encore quelques détails propres à amuser son auditoire, se dit-il. ❧

## Notes

Le titre de ce Qit’a est issu de l’expression « L’éléphant du Christ » par lequel les musulmans désignaient alors Renaud de Châtillon, en référence à une sourate du Coran. Il s’agit ici de la première apparition dans Hexagora d’un personnage de premier plan dans mon évocation du royaume de Jérusalem. J’espère avec le temps arriver à en tracer une image plus complexe, riche et intéressante que les caricatures romantiques ou brouillonnes qu’on en trouve généralement. Influencé par les travaux de Bernard Hamilton, j’avais déjà tenté de donner une lecture nuancée de son action dans un livre édité voilà quelques années chez Histoire et Collections (« La bataille de Hattîn »), dont j’ai récupéré récemment les droits et que je prévois de republier sous licence libre.

L’anecdote autour du lion est née des très fréquentes mentions de ce fauve dans les mémoires d’Usamah ibn Munqidh, qui se vantait d’en avoir chassé sans relâche toute sa vie, sans jamais avoir été blessé par l’un d’eux. Cet animal, désormais disparu au Moyen-Orient, était alors assez répandu et causait de nombreux dommages aux cheptels et n’hésitait pas à s’attaquer parfois à l’homme. On en rencontrait aussi quelques spécimens apprivoisés, dont Usamah nous raconte qu’il en vit un mis en fuite par un mouton particulièrement belliqueux.

Le surnom que donne Renaud à Ernaut est tiré d’une possible source d’inspiration galloise de Chrétien de Troyes pour son Yvain, *Le Chevalier au lion* (écrit vers 1175-80), et qui se baserait sur la vie de saint Mungo. Le fait de placer cette référence cultuelle presque vingt ans avant la rédaction du roman champenois est une façon de mettre en lumière l’existence préalable à toute création culturelle d’un substrat qui le nourrit et en permet la floraison. Il s’agit là d’un concept qui sous-tend une grande partie des textes d’Hexagora en général et les Qit’a en particulier.

## Références

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008.

Hamilton Bernard, « The Elephant of Christ: Reynald of Chatillon » dans *Studies in Church History*, vol. 15, 1978, p. 97-108.

Schlumberger Gustave, *Renaud de Châtillon, prince d’Antioche, seigneur de la terre d’Outre-Jourdain*, Paris, Plon : 1898.
