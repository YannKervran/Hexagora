---
date: 2013-01-15
abstract: 1152-1153. Bateleur, amuseur, il n’est pas toujours facile d’amuser ses contemporains, et ce d’autant moins quand on est n’est pas taillé pour les impressionner. Il peut arriver aussi que le destin fasse des surprises cruelles, du Paradis à l’Enfer. Un avant-goût des aventures qui s’ouvrent pour Ernaut dans *Les Pâques de sang* et qui le mènent aux portes de l’Enfer.
characters: Mile, Anceline
---

# Charité bien ordonnée

## Saint-Jean d’Acre, taverne d’Ayoul, mercredi 28 mai 1152

La petite salle était bondée, la faisant paraître encore plus étroite qu’elle n’était vraiment. Deux tables y étaient installées, mais on s’asseyait partout où on le pouvait : sur les marches descendant dans le lieu, sur des tonneaux remisés, sur les trop rares bancs. La lumière se déversait à flots par les portes grandes ouvertes, amenant avec elle l’odeur de la marée, du poisson et des embruns.

Les bouches s’emplissaient de vin, se cherchaient avec frénésie, lâchaient des rots ou lançaient des trilles égrillards. On se remuait, on se chahutait, on se bousculait avec entrain dans les ricanements, les beuglements, les cris énervés. L’humanité la plus bestiale, la plus jouisseuse, s’agitait, en soubresauts joyeux, en éclats de voix, de rires, de colère, nourrie de gnôle aigre et de plaisanteries salées.

Sur une des tables, débarassée pour l’occasion, un nain dansait de façon outrée, un pichet à la main. Ses courts membres se démenaient tandis qu’il bondissait, sautait, cabriolait comme un dément, ses mèches blondes collées sur le front, répandant la boisson autour de lui. De sa voix embrumée par l’alcool, mais au timbre juste, il hurlait ses vers comme semeur au printemps, récoltant hilarité salace et encouragements paillards.

>« Bienheureux enfançon, engendré tout chantant  
>Joyeux jusqu’à sa fin, à sa mort va claironnant.  
>Né fringant d’une fesse, on le baptisa vesse  
>On lui prédit grand ouvrage et belles prouesses… »

Tout en déclamant, il mimait avec vigueur, forçant le trait, grimaçant comme un démon. Son entrain attirait à lui tous les regards, éteints ou avinés, joyeux ou étonnés. Et lui caracolait d’autant plus, souriant aux femmes, clignant de l’œil aux hommes. Il connaissait son public et savait comment le flatter. Il était du métier. Il en vint au dernier couplet, qui devait briller d’un éclat particulier.

>« Il fait froncer sourcils, ahonter son parent  
>S’échappe alors en un timide chuintement  
>Sans se donner aucune chance de fort sonner  
>Car las, en nous quittant, nous laisse triste fumet. »

Se pinçant le nez avec dégout, il fit mine de chasser ce qui se serait échappé de ses fesses en direction du visage le plus proche. Cela souleva des rires frénétiques, d’autant que le destinataire soutenait qu’il avait été frappé par une bien horrible odeur. On l’encensa, on le congratula, et quelques verres lui furent adressés. Puis chacun revint à son pichet, à quelque chair à pétrir, à son histoire à conter. Le danseur s’effondra sur le banc, tout sourire, parmi ses compagnons.

« Je crois que tu ne saurais mieux conter aucune histoire, Mile…

— Pour ça, j’ai du vécu ! » confirma le nain, jovial.

Le petit groupe autour de lui semblait moins saoul, moins fiévreux que les autres clients. Ils avaient avec eux leurs sacs, tous leurs biens serrés en de fort maigres bagages. L’un d’eux, un grand brun à la mine sévère qui n’aurait pas déparé chez des ermites rigoristes, avala un peu de vin et lança de sa voix sourde :

« On ferait mieux de quitter Saint-Jean, trop de dévots par ici. On n’amasse nulle cliquaille. Nos jongleries n’attirent guère.

— Tu parles de vrai. On ne tiendra guère comme ça, à croquer du hareng et du vieux pain gris. »

Une petite brune aux yeux clairs, qui aurait pu être belle si la vie ne s’était pas acharnée contre elle, déclara d’une voix éteinte :

« Césarée. C’est bien Césarée ! Un port, mais moins empli de pèlerins. Des marchands, des marins, un peu de soldats…

— Tu goûtes fort la sergenterie, hein ? » lui lança un de ses compagnons, narquois.

Elle ne répondit pas et plongea le visage dans son gobelet. Mile s’accouda, le menton dans ses mains potelées.

« Je ne suis jamais allé là-bas, on dit qu’il y a de périlleux marais aux abords.

— Et alors ? On ne quittera pas la cité d’aucune façon ! On passera à Haifa, on y aura bonne provende, et de là on sera presque arrivés.

— On voit bien que tu as grandes jambes, toi ! rétorqua Mile en secouant les siennes, le visage fendu d’un sourire cynique.

— Tu demanderas à ta bonne amie Anceline de te prendre en ses bras en ce cas !

— Ce n’est pas ma posture favorite avec elle » répliqua le petit homme, malicieux.

La brune sourit, de dépit autant que de plaisir. Elle était habituée à n’être évoquée que pour son corps et jamais pour son âme, qui avait pourtant grand-soif de compagnie.

« Il nous faudrait trouver sire à flatter, nous ne faisons pas bonne provende parmi les vilains, asséna le brun à l’allure sévère.

— Promesse de seigneur n’est pas fief ! affirma Mile. Mais tu as raison, il nous faut bouger si nous voulons mieux manger.

— Au pire, on peut se faire passer pèlerin.

— Tous les frères hospitaliers d’ici à Babylone connaissent nos trognes, à force. Certains n’aiment guère nous voir aux abords.

— Quelle idée aussi d’aller fouiller parmi besaces des dormeurs ! »

La réflexion conclut les échanges, et chacun but son vin par petites gorgées, indifférent au chaos environnant. Mile se passa la main sur le front, faisant claquer son gobelet sur le plateau après en avoir avalé la dernière goutte.

« Soit, partons pour Césarée. Mourir ici ou ailleurs, n’importe quelle terre fera bien l’affaire. »

Et il sourit avec chaleur à Anceline, qui ne le regardait pas.

## Jérusalem, Grand Hôpital de Saint-Jean, vendredi 30 octobre 1153

Le Paradis ? Une odeur de propre flattait ses narines, et sa joue frottait sur une toile douce et fine. Mile eut tout d’abord l’impression d’être sur un nuage. Il remua la langue dans sa bouche, un peu pâteuse. Il n’avait plus cet horrible goût acide, râpeux, mêlé de bile, de sang et de terre. Il hésitait encore à écarter les paupières.

Il n’entendait pas le chœur des anges, mais des voix calmes, des pas étouffés, des toux, quelques éternuements et même, un peu plus loin, des gémissements. Assurément pas le Paradis.

Il risqua un œil. La pièce était lumineuse. Il était couché sur le flanc, regardant de côté une vaste salle où s’alignaient des lits. Entre les rangées allaient et venaient des servants, quelques silhouettes de noir vêtues. Une odeur de potage l’environna. Il bougea les mains, sentit le drap, le poids d’une couverture sur lui. Il soupira. Face à lui, un vieillard dormait, une bulle de salive éclatant à une de ses commissures à chaque souffle.

La vision lui arracha un sourire. Il renifla, se frotta le nez. Aucun visage connu n’était aux abords. Il inspira et expira lentement plusieurs fois, profitant de la chaleur douillette de sa couche. Il allait fermer de nouveau les yeux lorsqu’une ombre vint se placer devant lui.

« Vous voilà éveillé ! Grâces à Dieu. »

Il leva le regard. Devant lui, un jeune homme au teint frais, habillé d’une longue robe de laine brute, souriait. Avenant, il n’en était pas moins repoussant de laideur, avec des yeux trop gros qui cherchaient à s’extraire de ses chairs molles, un nez sinueux qui ne savait où aller et explosait en une forme de tubercule. Le tout posé sur une mâchoire épaisse, sans menton, d’où jaillissaient des dents disposées en tout sens bien au-delà de lèvres invisibles. Mile lui sourit, gardant le silence.

« J’ai eu fort peur de vous perdre. À votre arrivée, nous n’avions guère espoir.

— Je suis où ?

— À l’hospital de Saint-Jean. Cela fait maintenant trois jours depuis votre découverte.

— Trouvé ?

— Oui. Vous étiez fort malade, dans le faubourg de la porte David. On vous a porté ici. »

Mile se redressa un peu, tirant les couvertures jusqu’à son cou.

« Mes compagnons ? »

Le moine secoua la tête.

« Nos valets vous ont mené.

— Et mes compaings ?

— Ils étaient bien mal en point, eux aussi. Vous êtes le seul en avoir réchappé. » répondit le frère d’une voix douce, emplie de compassion et de chaleur.

Mile sentit une boule se former dans sa poitrine, broyer son cœur, écraser ses entrailles.

« Tous morts ? »

Il repensa aux dernières semaines. Ils erraient depuis des jours, sans arriver à amasser de quoi suffisamment manger ou boire. Ils avaient quitté Jaffa où ils survivaient à grand-peine depuis des jours, se faisant passer pour des pèlerins, mais on les avait chassé de plusieurs endroits. Les jongleurs n’étaient pas souvent appréciés des clercs et des moines. Ils volaient des fruits dans les champs et se désaltéraient à l’eau des abreuvoirs, avec toujours la faim qui les aiguillonnait, les tenaillait à chaque instant.

Jusqu’à ce jour où ils avaient commencé à avoir les entrailles en feu, les uns après les autres. Brisés de douleur, ils s’étaient réfugiés dans une grange, vomissant, victimes de diarrhées sanglantes. Ils avaient fini par être trop faibles pour sortir à chaque flux de ventre. Mile se rappela le visage d’Anceline, décharné, haletant, les cheveux mêlés de paille, de terre, le regard vitreux. Elle lui avait souri avant qu’il ne ferme les yeux.

« Il y avait aussi une femme…

— Je ne sais. Ici nous ne recevons que les hommes.

— Vous ne soignez pas les femmes ? s’insurgea Mile, haussant le ton

— Pas ici. En un autre lieu, ce sont nos sœurs qui s’en occupent. »

Mile ouvrit la bouche, surpris de son emportement. Il hocha la tête.

« Il est possible d’avoir nouvelles de l’une d’elles ?

— Je vais m’enquérir. Il faudra me dire son nom et me la dépeindre. »

Les jours suivants, Mile reprit des forces, sous les soins constants des gens de salle, nourri de plats sinon savoureux du moins roboratifs. Il n’avait jamais avalé autant de pain blanc de sa vie. Il se sentait presque gêné de toutes ces attentions, mais il avait constaté que, s’il avait un menu particulier, tous les malades étaient traités comme lui. En contrepartie, il s’efforçait de se montrer moins iconoclaste qu’habituellement et faisait même montre d’une piété timide. Sans trop se forcer, étonnamment. Il avait tellement moqué les moines et les clercs par le passé qu’il était perturbé par tous les égards qu’on lui témoignait, sans rien lui demander en échange. De toute façon, il avait trop sué par des chemins difficiles pour avaler avec amertume le vin offert. Il dormait, mangeait et buvait donc avec empressement, faisant de son mieux pour être un patient exemplaire. Sa seule angoisse venait de son absence de nouvelles d’Anceline.

L’hospitalier ne l’avait pas trouvée. Il gardait espoir néanmoins, elle pouvait encore être inconsciente disait-on, ou avoir donné un autre nom, par méfiance ou par pudeur, ainsi que le faisaient certains. Arriva le jour où Mile fut enfin complètement rétabli. Il attendait la visite du frère responsable, avec le médecin, pour avoir l’autorisation de quitter les lieux.

Seulement, il n’en avait que peu envie. Il avait goûté les draps propres, les couvertures douillettes, les repas chauds chaque jour. L’idée de se retrouver dehors, seul, à l’approche de l’hiver, ne l’enchantait guère. Le groupe arriva devant lui, avec les valets empressés qui tourbillonnaient autour. Le médecin semblait satisfait et confirma son accord pour le départ de Mile. En entendant cela, ce dernier interpela l’hospitalier.

« Je ne sais comment vous dire, frère, mais… je n’ai nul lieu où aller. Aucune nouvelle de mon amie, toujours, et nulle famille ici. Ni feu ni lieu.

— Oh ! échappa le frère, avec une douceur et un effroi instinctifs. À l’approche de la mauvaise saison…

— Il y a peut-être tâche où je pourrais m’employer ici, il y a toujours grand besoin de valets. Je ne suis pas fort grand, mais bien vaillant. »

L’hospitalier hocha la tête, affichant un sourire capable d’effrayer n’importe quel enfant.

« Je vais voir si je peux vous trouver place dans un de nos lieux.

— Grâce vous en soit rendue, frère ! Je suis prêt à assumer n’importe quelle tâche. »

L’hospitalier fronça les sourcils et réfléchit un instant.

« Il y a bien quelque endroit où nous avons besoin d’un valet, pour le tenir en la nuit et s’assurer que nul n’y entre…

— Je suis plus féroce que mâtin et serai féal valet !

— Avez-vous déjà entendu parler de Chaudemar ? risqua timidement le moine.

— La porte des Enfers ? » échappa un des valets aux alentours.

Non, en effet, Mile n’était pas au Paradis. ❧

## Notes

Les amuseurs publics, qu’on nommait alors les jongleurs quels que soient leurs compétences : acrobaties, jeux d’adresse, contes, chansons, n’ont guère laissé de trace dans les sources textuelles. On sait pourtant qu’ils existaient, car ils sont souvent représentés dans l’iconographie, et étaient parfois employés comme figure pour des textes moralisateurs. Mais au final, de la vraie condition de leur existence au XIIe siècle, on n’en sait que peu.

À l’opposé, le grand hôpital de Saint-Jean de Jérusalem est bien connu, et de nombreux documents en traitent. Pourtant, là encore, certains aspects pourtant essentiels demeurent obscurs. Son emplacement précis fait par exemple encore l’objet de discussion malgré certaines hypothèses anciennes qu’on pensait validées. Par contre, son fonctionnement a fait l’objet d’un texte assez détaillé de la part d’un voyageur, qui semble y avoir longtemps séjourné, peut-être même comme membre de son personnel vu tous les détails qui sont fournis. En ce qui concerne Chaudemar (lieu que je n’ai pas inventé), plus d’informations se trouvent à son sujet dans la seconde enquête d’Ernaut, *Les Pâques de sang*.

Remarque : la chanson déclamée par Mile est une invention basée sur des blagues médiévales. Le thème qui l’a inspiré constituait un motif comique fréquent, d’ailleurs pas complètement oblitéré de nos jours.

Plus de précision dans le texte sur « Le fils à fesse ».

## Références

Beltjens Alain, « Le récit d’une journée au grand hôpital de Saint-Jean de Jérusalem sous le règne des derniers rois latins ayant résidé à Jérusalem ou le témoignage d’un clerc anonyme conservé dans le manuscrit Clm 4620 de Munich » dans *Bulletin de la Société de l’Histoire et du Patrimoine de l’Ordre de Malte*, N°14 (Spécial), 2004.

Boas Adrian, « Akeldama Charnel House/Carnarium/Chaudemar » dans *Jerusalem in the Time of the Crusades. Society, landscape and art in the Holy City under Frankish rule*, Londres & New York : Routledge, 2001, p.185-187.

Casagrande Caria, Vecchio Silvana, « Clercs et jongleurs dans la société médiévale (XIIe et XIIIe siècles) » dans *Annales*, volume 34, N°5, 1979, p.913-928.

John Riley-Smith, *The Knights of St. John in Jerusalem and Cyprus c.1050-1310*, McMillan St. Martin Press, Londres, 1967.
