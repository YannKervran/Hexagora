---
date: 2012-12-15
abstract: 1151-56. Le destin d’une jeune fille réchappée de la maladie sera résolument bouleversé quand elle prendra la route pour le tombeau du Christ. À peine une femme, la jeune Libourc, fragile et inquiète, découvre un nouveau monde après une enfance maladive. Soulevez un peu le voile sur la jeune femme qu’Ernaut rencontre lors de son enquête dans *Les Pâques de sang*.
characters: Cateline, Jehan le Batié, Libourc, Mahaut, Marion, Sanson de Brie
---

# Cœur vaillant

## Bords de la Marne, environs de Meaux, mardi 17 avril 1151

La carriole tirée par un âne bringuebalait en grinçant, menée par l’adroite badine courbe d’un jeune valet à la cotte crasseuse. Le regard moins vif que celui de sa bête, il mâchonnait une graminée tout en observant les embarcations sur le cours d’eau qu’ils longeaient en direction du levant.

« Prends donc garde, ne vois-tu pas que tu mènes ma fille parmi mottes et terriers ? »

La voix s’efforçant de réveiller, sans grand effet, l’adolescent était celle d’un homme assez vieux pour être son père, voire son grand-père. Sa face était ravagée par le chagrin et sa barbe mal entetenue, les cernes qui creusaient son visage le faisaient paraître encore plus âgé. Sa tenue de laine indiquait l’aisance, mais pas la richesse. Une femme plus jeune, mais tout aussi épuisée que lui marchait, une main posée sur l’enfant allongé dans le petit chariot. Caché parmi de nombreux replis de couverture, emmitouflé jusqu’au bout du nez, on en discernait à peine les traits. Les yeux mi-clos, les joues rosées par la fraîcheur ambiante et la fièvre, il laissait s’échapper un râle lorsque le cahot était trop violent. La mère tourna un visage sévère vers le vieil homme, les lèvres pincées.

« C’est folie d’avoir voulu cheminer outre les murs de la cité, Sanson. Elle est trop faible pour cela, ses douleurs sont si fortes ! »

Son époux lui lança un regard horrifié.

« Crois-tu que je m’enjoie à voir ma cadette endurer semblables souffrances ? Si je la porte auprès de saint Fiacre, c’est parce que j’y ai espoir de guérison pour elle. »

La femme se renfrogna, mais hocha la tête en assentiment. Elle lâcha, comme à regret :

« Pardonne-moi, mon ami. Il m’est fort difficile de voir Libourc ainsi endolorie, elle si aimable enfançonne. »

Sanson se redressa, les yeux emplis de colère.

« Ne parle pas d’elle telle une moribonde. Nos prières la sauveront ! On dit saint Fiacre fort efficace. J’ai de quoi faire dire bon nombre de messes, et prévu chandelles de cire pour plusieurs jours. Jean Tonnel a sauvé ainsi son second fils d’une atteinte à la gorge, la fièvre qu’il avait contractée lors de la Noël.

— Puissent tous les saints nous entendre ! »

Sanson toussa, transpirant sous son épaisse chape de laine malgré la fraicheur des températures. Il était complètement désemparé, prêt à tout pour sauver sa fille. Le front bas, la démarche hésitante, il avançait tel un animal mené au boucher, indifférent à tout ce qui n’était pas le souffle de son enfant souffreteux.

Les derniers lambeaux de brume finirent par se déchirer sous les assauts du soleil et la Marne se mit à scintiller de mille diamants. De nombreux oiseaux patrouillaient à ses abords, nageaient à sa surface. Des barques agiles s’y déplaçaient, ainsi que de pesantes barges couvertes de ballots, de tonneaux, de lourds matériaux. La vie continuait son cours, indifférente au drame des deux parents accompagnant leur enfant malade.

Lorsque la chaleur se fit plus forte, ils s’arrêtèrent près d’un manse[^manse] fatigué. L’homme occupé à passer la houe dans son jardin offrit de leur donner un peu de brouet et interpella son épouse d’une voix autoritaire, la commandant d’une façon bien peu en accord avec sa charitable proposition. Il confirma qu’ils devaient bientôt quitter les bords du fleuve, longer un bois de belle taille à main gauche, pour arriver à l’église de saint Fiacre.

Plusieurs gamins couraient dans les environs et l’un d’eux, le nez sale et les genoux crottés, aidait à épierrer les abords du potager. Le paysan, bien que bourru, se voulut encourageant, attestant des nombreuses guérisons dont il avait pu être témoin le long de cette route. Avalant avec lenteur les cuillères de potage que lui présentait sa mère, la petite fille, d’une dizaine d’années, écoutait avec attention. Elle ne pouvait néanmoins se retenir de tourner les yeux vers les enfants batifolant aux alentours, brandissant des bâtons pour mener les poules de-ci de-là. Son regard brillant peinait à demeurer concentré et se perdait souvent dans le lointain.

« Ta couche n’est pas trop dure, Libourc ? », demanda Mahaut tout en surveillant le tassement de la paillasse.

La malade secoua la tête, esquissant un sourire timide.

« Le chemin est-il long jusqu’aux reliques ? »

Sa voix rauque, issue d’une gorge nouée ne semblait pas naturelle dans ce petit corps malingre.

« Je ne pense pas… répondit la mère d’une voix faussement assurée. Veux-tu que je te frictionne un peu ? Laisse-moi voir tes cataplasmes, s’ils n’ont pas glissé.

— Je les sens encore, mère…

— Je préférerais ne pas la découvrir ici, Mahaut. Il ne fait guère chaud, et la froidure est mauvaise pour ses membres a dit le frère infirmier. » l’interrompit Sanson.

La femme se renfrogna, vexée de la remarque et s’efforça de sourire à sa fille malgré sa mauvaise humeur.

« Si tes douleurs reviennent trop fortement, préviens-m’en, veux-tu ?

— Oui mère, mais je me sens beaucoup mieux avec ces nouveaux emplâtres. »

Joignant le geste à la parole, la fillette remua un peu les membres supérieurs, mais fut vite trahie par quelques grimaces.

« Ne t’échauffe pas trop pour autant, mon enfant, cela n’aide pas à ta guérison. »

Pendant ce temps, son mari avait repris sa discussion avec le jardinier appuyé sur sa houe. Ce dernier était curieux de savoir d’où ils venaient, et ravi d’apprendre qu’ils étaient de Meaux. Il était persuadé que le fait d’être de la région incitait les saints locaux à être plus efficaces.

« Sauf vot’ respect, je crois que c’est normal qu’y s’occupent de ceux qui les prient l’an durant. Venir quémander depuis la Provence à un saint qui nous connaît pas, je vois pas bien comment ça marcherait !

— Tu ne crois pas à l’efficacité des marcheurs qui se rendent au loin ?

— Je n’ai pas le latin d’un clerc, je l’avoue, mais moi je serais guérisseur, je m’occuperais de mes voisins avant de soigner des Picards, des Normands ou des Flamands.

— Et le Christ ? »

La question ennuya l’homme, qui se frotta sa barbe naissante d’une main rugueuse.

« Ça, j’peux pas dire, le curé explique qu’il est partout, mais j’ai pas bien compris… Pour moi, y doit mieux voir ceux auprès de chez lui, comme tout le monde.

— Jérusalem… souffla le vieil homme.

— Oui, c’est ça, mais c’est pas pour nous, ça. Il marqua une pause, cherchant dans sa mémoire ce qu’il avait appris sur le lieu. C’est par-delà les mers, au centre du monde que mon vieux disait. Même que les rois et les barons en reviennent pas toujours. Y’a que le Grand Charles[^charlemagne] qui s’y rendait rapidement, pour combattre les païens. »

Sanson hochait la tête en accord, n’écoutant plus que d’une oreille. Son esprit vagabondait au loin, formulait des hypothèses, se nourrissait de nouveaux espoirs. Saluant l’homme d’un geste amical, il indiqua à sa petite troupe qu’ils reprenaient le chemin. Son valet, qui s’était allongé dans un fossé pour se reposer, indifférent à la rosée, vérifia les brancards du chariot puis d’un coup de trique sur la croupe du baudet, remit l’attelage en branle. Il n’avait toujours pas proféré une parole.

Ils croisèrent une poignée de colporteurs, le dos courbé sous leurs produits, leurs paniers. Des bergers faisaient paître les animaux sur les berges, nettoyant parfois l’accès à des plages où quelques barques gisaient. Une cavalcade de soldats les dépassa, imperméables à leur malheur, parlant d’une voix forte et braillant des chansons salaces. Leur voix mourait au loin lorsque Sanson, se rapprochant de son épouse, lui déclara avec autorité :

« De retour en notre hostel, nous demanderons licence à l’évêque de prendre la croix. »

Mahaut se raidit de stupeur et chercha ses mots un moment avant de répondre.

« Prendre la croix ? Nous n’avons pas…

— Nos fils sont tous grands et bien mariés, et je peux vendre mon métier. Tu as les courtils que ton père t’a donnés, et moi j’ai toujours les droits de pâture et de fagotage que je peux vendre sur le Bois aux Prêles.

— Mais nous ne pouvons…

— Ma décision est prise, Mahaut. Si Libourc trouve guérison grâce à saint Fiacre, je veux qu’elle puisse en remercier le Seigneur en son tombeau. »

Tirant sur son voile pour le remettre en place, Mahaut fixa son enfant allongée parmi les étoffes et des larmes lui montèrent aux yeux. Elle regarda son époux avec tendresse, étonnante douceur surgie d’un visage généralement glaçant. Reniflant pour contenir son émotion, elle laissa s’échapper d’un filet de voix brisée :

« Amen. »

## Meaux, jeudi 16 juillet 1153

Les dernières gouttes de pluie attardées tombaient du toit et s’écrasaient dans les flaques creusées à la périphérie de la cour. Les nuages, plus clairs, semblaient vouloir enfin se dissoudre. Quelques mésanges et un merle lançaient leurs trilles depuis les maigres buissons du petit jardin enclos. Face à la porte grande ouverte, Sanson de Brie était occupé à tailler une mortaise, sous l’œil attentif d’un jeune apprenti. Un autre, légèrement plus âgé, vérifiait avec le valet la planéité d’une planche après rabotage. L’atelier était empli d’odeurs de sève, de tannin, et des copeaux voletaient devant la truffe indifférente d’un chien allongé à l’entrée. Donnant sur la rue, l’éventaire laissait se propager la clameur de la place de la cathédrale guère éloignée.

Quelques badauds discutaient non loin de là, riant et plaisantant. Un goret, sur une piste depuis l’égout central, hésita à pénétrer et, inquiété par le regard vindicatif du mâtin, opta pour une prudente retraite. Le porcher, occupé à parler météorologie avec des femmes revenant de la Marne avec leurs tas de linge sentant le savon, ne semblait guère soucieux de ce que son troupeau vaquait un peu partout.

Mahaut esquiva le groupe bloquant le passage et entra dans l’atelier, un panier empli de pains au bras, suivie d’une jeune fille le dos courbé sous une hotte débordant de légumes. D’un geste impérieux, elle envoya la servante dans la cuisine, de l’autre côté de l’arrière-cour et se dirigea elle-même vers son époux. Souriant, il se releva en débarrassant sa cotte élimée des copeaux et de la sciure. Elle posa une main douce sur son épaule, établissant un contact avant de parler.

« Les Martin te passent le salut.

— La fièvre de leur petit est passée ?

— Si fait. Il court de nouveau partout comme un cabri en pré. »

Le menuisier hocha la tête, satisfait.

« De mon côté, j’ai discuté avec le vieil Aymar, de Saint-Rémy. Il ne serait pas contre l’idée de nous acheter la vieille pâture.

— Il aura l’argent dans l’année ?

— De certes. Il espère belle moisson de ses terres et devrait aussi toucher quelques monnaies de marchands venus pour la foire, à l’échéance de la Toussaint. »

Mahaut opina, récapitulant dans sa tête tout ce qu’il leur restait à faire avant de pouvoir accomplir le voyage qu’ils escomptaient. Son époux l’interrompit dans ses pensées.

« J’ai aussi vu le chanoine, le petit jeune, de la famille du comte.

— Le rouquin ? Guiraut ?

— Voilà ! Il m’a indiqué que nous devrions aller le voir au palais de l’évêque. Il veut nous expliquer en détail les privilèges du pèlerin. Il n’est nul besoin d’être prêts à partir pour cela, m’a-t-il expliqué. Mais tous ceux qui feront vœu devront être là. »

Le vieil homme lança un regard à ses aides, dans l’atelier, qui continuaient leurs tâches sans prêter attention à la discussion. Le plus jeune taillait avec application des chevilles sous un auvent à l’arrière.

« Il me semble que nous devrions partir seulement tous les trois. Trouver valet pour si long voyage sera malaisé, sans compter les dépenses. Nous pouvons patienter que d’autres prennent la croix pour ne pas cheminer seuls. »

Il s’interrompit, surpris par un mouvement de tête de sa femme. Elle regardait au-dessus d’eux, en direction du raide escalier de bois qui menait à la pièce de vie à l’étage. Il comprit lorsqu’une voix légère demanda :

« Mère, j’ai fini de recoudre les chausses, je peux descendre ?

— Soit, mais ne va pas dans la rue, demeure par ici ! »

N’en espérant plus, la jeune Libourc descendit l’escalier avec entrain, un sourire illuminant ses traits. Le visage rayonnant, elle traversa l’atelier, caressant le chien au passage et se dirigea droit vers l’appentis au fond de la cour, remise à bois dont l’ancien fenil servait de débarras.

L’échelle grinçait sous son faible poids et les planches gémirent lorsqu’elle rejoignit son royaume. En ouvrant le petit volet qui révéla les poussières voletant dans l’air, elle illumina le sombre local encombré de vieilles céramiques fendues, d’escabeaux fatigués et branlants, ainsi que d’une couverture utilisée par les valets quand ils dormaient là. En se penchant par l’étroit fenestron, elle réussit à apercevoir un coin de ciel bleu, ce qui la mit en joie. En fait elle espérait surtout entendre ce que disaient ses parents.

Ils préparaient leur départ, discutaient de toutes les formalités qu’ils avaient à faire, de la cérémonie qu’ils feraient avec l’évêque, qui leur octroierait officiellement le statut de pèlerin, de la façon dont ils allaient constituer leur pécule, comment les contrats des apprentis pouvaient être rompus. Mille petits détails rendant leur voyage plus lointain, moins immédiat pour la jeune fille.

Elle s’en réjouissait, car elle n’était pas encore prête à quitter la cité. Comme tous les enfants, elle se faisait une fête à l’idée de découvrir le monde, mais son frère aîné lui avait laissé entendre que le trajet serait long et difficile, qu’ils ne seraient pas arrivés avant des semaines, des mois. Elle avait fait la fanfaronne, mais n’était jamais allée plus loin que Saint-Denis, aux abords de la Seine et de Paris.

Désormais, elle se languissait, curieuse des aventures à venir, mais inquiète de tout quitter, en particulier le beau Clément. C’était le fils d’un des charrons en haut de la rue. Il avait deux ou trois ans de plus qu’elle et roulait des mécaniques lorsqu’elle lui souriait chaque dimanche à la messe. Ils s’étaient embrassés une fois, cachés derrière un des piliers de l’église, et elle en gardait un chaleureux souvenir. Arriverait-elle à se passer de lui pendant si longtemps ? Des éclats de voix joyeuses montèrent depuis l’entrée, dans lesquels elle reconnut ses amies Marion et Cateline. Elles l’aperçurent dans sa vigie et lui firent de grands signes en traversant la petite cour. Vérifiant que les parents de Libourc étaient occupés à autre chose, elles lui lancèrent avec entrain :

« Nous avons message pour toi, de la part du charron ! »

Rassérénée, elle oublia en un instant ses doutes et fit de la place autour d’elle pour recevoir ses amies. Pour l’heure, elle était toujours à Meaux, et il s’en faudrait d’un long moment avant qu’elle ne doive quitter sa maison.

## Route de Jaffa à Jérusalem, mardi 21 août 1156

Le petit feu crépitait, zébrant de jaune doré les visages des marcheurs avachis. Le piaffement des chevaux de leur escorte s’échappait du taillis où ils étaient attachés. Les broussailles frémissaient tandis que les animaux en arrachaient les feuilles. Une lune gibbeuse traçait à la craie les silhouettes qui déambulaient parmi le camp. Des voix à l’accent étrange animaient la nuit, ponctuant les rêves des plus fatigués des pèlerins, déjà en train de dormir.

Les visages étaient épuisés, après de longs mois sur les chemins, certains ayant fait le trajet depuis l’Europe sans prendre le bateau, si ce n’était pour traverser le détroit à Byzance. Ils régalaient leurs compagnons d’un temps de magnifiques descriptions d’églises merveilleuses, de territoires immenses. Mais souvent, leur récit se brisait sur les mille écueils sur lesquels les vies des leurs s’étaient échouées.

Il n’y avait pas un pèlerin qui n’ait connu son lot de souffrance, de maladie, de blessures ou de mort. On évoquait à mots couverts les terribles Turcs qui écumaient les montagnes au nord et dont les lames courbes moissonnaient les souffles, tandis que leurs traits sifflaient.

Mais pour l’heure, ils étaient tous fébriles, impatients d’apercevoir la cité de David, à seulement quelques jours de marche. Les regards étaient parfois enfiévrés, enchâssés dans des orbites creusées par les privations, la fatigue et la peur. Parmi eux, Sanson, Mahaut et Libourc ne dépareillaient guère. Leurs vêtements avachis, enduits de poussière, déchirés n’avaient plus leur bel éclat du jour de leur départ. Ils étaient pelotonnés les uns contre les autres, non pas tant en raison du froid, car la température était plutôt chaude en cette nuit d’été, mais par habitude.

Sanson, les yeux fermés, serrait contre lui son épouse, qui dormait paisiblement. Elle qui avait toujours tenu à se montrer selon son rang à Meaux, était étendue à même le sol, la coiffe tournée, la bouche entr’ouverte, ronflant doucement. Assise à côté d’eux, la jeune fille avait perdu son visage rond d’enfant, grandissant et s’épanouissant tandis qu’elle s’approchait de la Cité sainte. Ses longs cheveux bruns étaient gris de poussière et ses joues maculées. Elle avait tenté de se joindre aux chants de quelques dévots, mais ses lèvres crevassées demeuraient désormais closes. Épuisée, elle fixait le feu sans bouger, trop anéantie pour même fermer les paupières.

Son regard fut attiré par la blancheur d’une cape. Le fourreau battant sur ses chausses d’acier, un des frères du Temple qui les escortait s’avançait en devisant doucement avec un de ses sergents. Ce dernier était un Syrien, et ils échangeaient dans sa langue. Il portait un petit bouclier rond dans le dos, une épée droite suspendue à la ceinture, mais surtout un arc, à la façon des Orientaux, avec plusieurs courbures. Il arborait une belle barbe noire que parsemaient deux rivières argentées, de part et d’autre de sa bouche. Ici et là, des hommes pareillement habillés montaient la garde, tandis que d’autres s’amusaient autour de leur feu, apportant un peu de vie à leur morne campement.

Sentant qu’elle ne pouvait différer plus l’assouvissement d’un besoin naturel, Libourc se leva avec précaution pour ne réveiller personne et s’écarta pesamment des cercles de lumière en direction d’un taillis. Elle était consciente du danger qu’il y avait à s’éloigner, mille fois avertie par sa mère et ne tarda pas à trouver un endroit où s’accroupir pour se soulager.

Autour d’elle seul le bruissement des branches, des feuilles animait la nuit. Aucun hurlement de loup. Même les chevaux proches étaient à peine audibles. Frissonnant de ce calme, elle se hâta de s’en retourner vers la clairière. Elle venait à peine de quitter son bosquet qu’elle poussa un cri vite étouffé. Une des vigies du camp se tenait devant elle, le doigt sur la bouche. De son bras gauche, protégé d’un petit bouclier rond, il la fit passer derrière lui en silence. Il scrutait l’obscurité, cherchant à en percer les mystères, le visage crispé, le corps tendu.

Libourc n’osait plus respirer. Puis brutalement il lança avec rage un cri dans sa langue gutturale, s’avançant pour mettre Libourc dans son dos. Au même instant, la forêt prit vie, des branchages s’agitèrent, le sol trembla de la course de nombreux pieds. Des coups sourds furent échangés, l’acier tinta contre l’acier et les boucliers résonnèrent. Ils étaient attaqués !

Du camp, une trompe sonna à plusieurs reprises et tous les hommes de l’escorte se levèrent, faisant rempart de leurs corps devant les pèlerins paniqués. Au milieu d’eux, quelques longs manteaux de laine blanche virevoltaient, une croix sombre ornant leur poitrine.

Libourc s’était figée lorsque le garde l’avait mise de côté et elle tressaillait à chaque fois qu’elle sentait les assaillants frapper. Avec l’obscurité, parmi les branches, le combat était très désordonné, mais plusieurs fois elle aperçut l’éclat d’une lame, d’un fer qui cherchait à mordre dans les chairs de son protecteur. Elle n’osait plus bouger, pelotonnée contre le tronc d’un arbre, et finit par garder les yeux fermés.

L’assaut ne dura pas plus de quelques minutes, qui parurentt une éternité à Sanson et Mahaut car ils avaient découvert l’absence de Libourc. Lorsque le calme revint et que l’escorte commença à ranger les armes, ils interpellaient tout le monde d’une voix inquiète. Rapidement, ils aperçurent leur enfant à l’orée du bois, qui discutait avec un des chevaliers du Temple et un des soldats. Ils s’élancèrent, bouleversés. Mahaut allait tancer sa fille de la peur qu’elle leur avait causée, mais n’en eut pas le temps. Libourc se tourna vers elle et lui fit un sourire inquiet :

« Père, mère, je tiens à vous présenter quelqu’un. »

Elle désignait visiblement le soldat levantin devant elle. Entre eux deux, le templier, un homme au visage rude, ridé et fatigué malgré son jeune âge, souriait. Libourc continua à l’intention de ses parents.

« Ce brave mérite récompense pour sa bravoure, il m’a défendue comme un lion.

— Que faisais-tu en dehors du camp ? » rétorqua sèchement sa mère, enflammée par l’inquiétude.

Sa fille haussa les épaules et ne répondit pas. Elle dévisagea Sanson, attendant qu’il réagisse. Le vieux menuisier fixa le sergent, puis le chevalier, et enfin Libourc.

« Comment s’appelle-t-il ? » demanda-t-il d’une voix brisée.

Ce fut le chevalier qui répondit.

« Jehan le Batié.

— Tu as sauvé mon bien le plus précieux, Jehan, je ne sais comment te mercier. »

Le soldat fit un sourire mité qui révélait une dentition incomplète, ce qui lui donnait un air plus terrible encore.

« J’ai fille aussi. Belle comme tienne. »

Tout en répondant, il porta la main droite à son cœur. Du sang s’écoulait lentement du rapide bandage qu’il s’était fait, tel une moufle. Libourc regarda les gouttes sombres se former puis tomber, glissant sur la tenue, jusqu’à rejoindre une flaque à leurs pieds. C’était leur première nuit en Terre sainte. ❧

## Notes

Le voyage pour raison religieuse menait souvent dans les lieux saints les plus proches. Les sanctuaires locaux étaient très fréquentés, et le culte rendu au loin n’était pas forcément le plus fréquent, même si l’on connaissait les tombeaux les plus prestigieux.

Parmi ceux-ci, celui du Christ avait une place à part, évidemment. Et bien que la dévotion en fût populaire, aller par-delà les mers pour se recueillir là-bas n’était pas chose facile. Les autorités ecclésiastiques, du ressort desquelles dépendait l’obtention du statut officiel, protecteur, encourageaient de telles expéditions, mais il fallait encore que la motivation personnelle soit présente. Tout cela avait un coût, en terme financier évidemment, mais aussi en temps, sans compter bien sûr tous les risques physiques.

En outre, on voyageait généralement avec ses proches, avec des membres de sa communauté d’origine si on le pouvait. L’instinct grégaire regroupait les gens, perdus de façon identique en des terres où tout leur était étranger, selon leur ville, leur province, leur langue.

La condition féminine est une des grandes absentes des études scientifiques, en raison de l’inexistence relative des femmes dans les sources médiévales. Cela est d’autant plus vrai des personnes du commun, qui ne jouaient aucun rôle politique de premier plan ni n’assumaient une carrière religieuse ou savante. Lorsque l’on cherche en plus à en apprendre plus sur leurs jeunes années, les traces évanescentes deviennent carrément fantomatiques. Néanmoins, depuis quelques années, des historiens s’efforcent de rassembler les indices épars pour donner des pistes.

## Références

Graboïs Aryeh, *Le pèlerin occidental en Terre sainte au Moyen Âge*, De Boeck Univers, Paris & Bruxelles : 1998.

Schaus Margaret (éd.), *Women and Gender in Medieval Europe, an Encyclopedia*, Routledge, New York & Londres : 2006.
