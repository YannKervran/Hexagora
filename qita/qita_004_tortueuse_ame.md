---
date: 2012-02-15
abstract: 1154-1156. Plongez dans le monde du renseignement et de la géopolitique au tournant du siècle, alors que le califat fatimide est de plus en plus mal en point. Lorsque le marchand se fait diplomate et espion…
characters: Mauro Spinola, Guilhem Torte, Giacomo, Frère Raymond
---

# Tortueuse âme

## Gibelet, soirée du mercredi 26 mai 1154

« Sont-ils tous pareillement fols ? Nul prince n’a donc soupçon de jugeote que ce soit chez les infidèles ou chez nous ? »

Le vieil homme accoudé à la magnifique fenêtre sculptée de sa demeure pestait d’une voix habituée à commander, frappant du poing l’appui tandis qu’il vociférait. De rage, il avait froissé le document à l’origine de son courroux et l’avait jeté à terre. Il ne doutait pas de sa véracité, son auteur était un de ses fidèles et le coursier, Guilhem Torte, un sergent de valeur qu’il employait depuis des années. Ce dernier avait chevauché durement, la boîte de message attachée à sa ceinture, sans savoir exactement ce qu’elle recelait, mais prêt à mourir pour en délivrer le contenu à son juste destinataire, Mauro Spinola. En outre, pour avoir combattu et voyagé depuis plus de dix ans sur les pistes et dans les villes de la région, il était autant au fait de la politique locale que son maître, peut-être même plus, n’ayant nul besoin de s’absenter régulièrement pour s’en retourner dans la cité pour laquelle le vieux diplomate s’agitait tant, Gênes. Et ce qu’il avait glané comme informations tandis qu’il revenait des zones méridionales causait beaucoup d’inquiétude au génois.

Remâchant sa colère, Spinola se tourna vers le soldat assis sur un des bancs dans la salle. Sans sa tenue de guerrier latin, on aurait facilement pu le prendre pour un syrien ou un égyptien. Portant encore son haubert plein de poussière, il n’avait qu’ôté la coiffe de mailles et demeurait en partie couvert de la crasse du voyage. Ses yeux bruns brillaient dans un visage enlaidi par la fatigue, les rides, et l’inquiétude. Sa barbe naissante et ses courts et épais cheveux gras n’arrangeaient guère l’image qu’il donnait. C’était un homme de guerre, au faciès rude, à la mâchoire agressive, s’accommodant volontiers de cela sans y prêter attention. Il ne releva pas le mouvement d’humeur de son seigneur, comme si cela faisait partie des échanges normaux, et s’employait à choisir un fruit dans la coupe vernissée posée sur la table à son intention. Sa décision prise, il frotta la figue en un geste dérisoire, de sa main sale, et intervint avant de mordre à belles dents.

« L’Égypte n’est que bataille et sang ! La parentèle du calife assassiné a mandé Al-Salih Tala’i’ ben al-Ruzzayk. Il est rentré du désert méridional, menant fer et feu au Caire. La victoire paraît sienne, les comploteurs s’apprêtent à fuir vers les plateaux du Sinaï. »

Frottant son crâne qui se dégarnissait largement, Mauro commença à faire les cent pas devant les magnifiques ouvertures de sa demeure, donnant sur le port de la ville. Au-dehors, le ciel s’empourprait, ombré de nombreux nuages filandreux qui formaient autant de taches sombres au-dessus de la mer.

« Noreddin[^nuraldin], ce damné turc va de certes tenter de prendre place, ou imposer un des siens. Maintenant qu’il s’est assuré de Damas ! Après que son père ait forcé Édesse, que les démons l’emportent !

— Votre homme semblait accroire que cela faisait de l’Égypte un fruit mûr pour le roi Baudoin[^baudoinIII], au contraire. Et que de toute façon, jamais hommes du Caire ne se livreraient à un turc du calife de Bagdad. »

Le diplomate dévisagea son valet comme s’il avait proféré une grossièreté en plein milieu de la messe. Puis il reprit ses allées et venues, les bras désormais dans le dos, les yeux se plissant tandis qu’il s’efforçait de comprendre ce qui se passait, et les perspectives qui en découlaient. Son sergent, impassible, attendant la suite, en était à son sixième fruit lorsque Mauro ouvrit de nouveau la bouche, la voix vibrant de colère contenue.

« Et ce maudit syrien, ibn Munqidh, où est-il ? Si jamais il tente d’attirer Noreddin en Égypte, nous sommes perdus ! »

Le sergent fit une moue.

« Il était du complot, c’est de bonne fame. Il doit se terrer avec ses compaings parmi roches du désert. Je ne serais guère surpris qu’il espère joindre Damas, il y a forte acquointance.

— Mais puissants ennemis aussi, pour notre bonheur. Faisons assavoir sa male fortune, peut-être à l’émir ’Ayn ad-Dawla[^aynaddawla].

— Il y faudra grande habileté. Voilà fier baron, et bien féal à son sire Noreddin. Il ne faudrait pas qu’il voie là quelque manœuvre. »

La remarque fit sourire le vieil homme. L’air enfin déridé, il prit le temps de réfléchir avant de répondre, lentement, tout en se mordillant les lèvres.

« Il doit déjà en savoir autant que nous. Autant ne pas se disperser pareillement. Songeons surtout à ne pas permettre le rêve d’union entre princes syriens et calife d’Égypte. Cet espoir est mort avec l’ancien vizir Sallâr, et doit le demeurer. »

Hochant la tête, le soldat avala une bouchée, puis renifla avant d’arborer une mine assurée.

« Outre, il se murmure au palais du roi Baudoin qu’on ne va tarder à lever le ban dans le sud, pour éviter que des fuyards ne ravagent la Judée.

— Excellent ! Releva Spinola, une lueur malicieuse dans l’œil. Espérons que le roi grappe si belle occasion de tenir les ennemis du futur vizir. Cela pourrait peser bon poids en la balance à l’avenir. Au final, il y a peut-être belles gerbes à moissonner de ces stupides querelles de palais. »

Avisant le triste aspect de son messager, il se racla la gorge.

« Tu devrais prendre quelque repos, Guilhem, je vais avoir besoin que tu retournes en le royaume, mais pour m’escorter cette fois. Je ne peux demeurer dans le comté alors que tout se joue au loin. Je vais donner ordre pour que nous partions au levant demain. »

Une ombre fugace, reflet d’une déception ou d’un agacement, survola subrepticement le visage du coursier. Puis il se leva, un peu raide, heurtant son siège du fourreau de la longue épée qu’il arborait toujours sur la cuisse. Il salua d’un bref mouvement de tête et se retira d’un pas rapide, laissant retomber la lourde tapisserie derrière lui en sortant. Désormais seul, Mauro Spinola se pencha et ramassa le document, le relisant machinalement. Glissant le fin morceau de papier dans une de ses manches, il quitta à son tour la pièce, en direction des cuisines où il était sûr d’y trouver des flammes pour le détruire.

## Gaza, nuit du lundi 7 juin 1154

Dans la nuit noire, seuls les feux allumés par les hommes de faction lançaient une lumière tremblante à travers les bâtiments encore nombreux à être ruinés. La ville n’avait été réinvestie que depuis quelques années, le château tenu par les chevaliers au blanc manteau du Temple garantissait désormais une relative protection. Depuis la prise d’Ascalon, que la forteresse était auparavant censée surveiller, des colons avaient saisi l’occasion de s’installer et s’étaient attelés à la tâche de reconstruire les édifices écroulés. Le travail à accomplir était immense et des quartiers entiers n’étaient encore qu’amas de décombres, tronçons de toitures, murs béants.

À la périphérie de la ville, une de ces demeures partiellement debout scintillait des feux d’un occupant temporaire. Mauro Spinola s’y était établi avec ses serviteurs, dans le sillage des guerriers du roi de Jérusalem en quête des comploteurs fuyant l’Égypte. Ces derniers étaient venus prêter main-forte à la garnison du Temple installée là, mais le vieux génois avait tout autre projet en tête. Il pourchassait les informations, rarement les hommes.

Pour l’heure, il soupait d’un épais gruau, en silence, assis sur une poutre dans le renfoncement d’une bâtisse à demi écroulée. Il attendait d’en apprendre plus sur la victoire de la journée. Les combats avaient été rudes, mais les Latins fêtaient désormais leur réussite. Restait à savoir quelle était l’étendue de ce succès. Le treillage qui servait de porte fut soudain repoussé, laissant apparaître l’homme de faction dehors, puis Guilhem. La mine réjouie, il portait une cruche, qu’il tendit à ses compagnons en entrant.

« Grande veillée que voilà. Les sergents du Temple du Christ arrosent dignement leur butin. Je ne saurais nombrer les montures prises ce jour. Ils les ont parquées aux abords en si vaste enclos ! »

Mauro finit d’avaler sa cuiller et désigna du menton un vieux tabouret de guingois à son homme.

« J’espère qu’ils n’ont pas saisi que destriers, même turkmènes !

— Certes pas. Ils tiennent enferré Nasr ben ’Abbâs en la forte tour. »

Le vieil homme esquissa un rictus de contentement, mais ne cessa d’avaler son repas en silence, désireux d’en apprendre plus. Guilhem s’empara à son tour d’une écuelle qu’on lui tendait et engloutit quelques bouchées avant de continuer, l’air moins affable.

« Par contre, son père a connu malemort en la bataille. »

Spinola contracta les mâchoires, fronçant le nez à la nouvelle qu’il ne goûtait guère. Il darda deux yeux inquisiteurs sur le sergent, impatient de poser la question qui le taraudait.

« Et le *Syrien* ? Munqidh ?

— Nulle trace de lui. Il a réussi à trouver passage, mais on ne sait pour où !

— Le maudit ! Nul doute qu’il ne quittera les étriers qu’une fois sauf en les murailles damascènes !

— Peut-être va-t-il tenter de retrouver la protection des siens, à Shayzar… »

Le génois racla son écuelle d’un mouvement rageur, avalant rapidement une dernière bouchée.

« Je n’y crois guère. Il va mendier auprès des Turcs, plaider sa cause. Comment une garnison a-t-elle pu manquer un vieillard comme lui ! Il est encore plus vieux que moi ! »

Personne ne répondit à la question, toute rhétorique. Guilhem se contentait de dévisager son maître, avalant tranquillement sa bouillie comme si c’était son dernier repas. Il savait que des ordres n’allaient pas tarder, l’esprit minutieux de Spinola s’était mis en branle, et accoucherait d’un plan, comme toujours. Cela lui laissa le temps de finir son plat et de se servir un gobelet de vin coupé. Il en dégustait la première gorgée lorsque la voix grave reprit.

« Je vais demander audience au plus vite au châtelain des frères du Temple et lui conseiller de céder Nasr ben ’Abbâs aux Égyptiens. Tu as possibilité de te rendre prestement au Caire ?

— Je pourrais trouver nef de pêcheur pour me mener sur les côtes, vers al Arish. De là, louer rapide monture… Comptons jusqu’à la saint Gervais[^4] pour faire bon poids. »

Présentant une petite bourse qu’il avait sortie de son vêtement, Mauro la lui lança.

« En ce cas, ne tarde pas. Rends-toi au port de Taydâ et apprête-toi au départ. Attends là-bas mon message. Tu le mèneras sans tarder au nouveau vizir, avec force discrétion.

— De qui puis-je me recommander ?

— Si on te questionne fort avant, tu pourras lâcher mon nom. Mais seulement si tu devines grand risque pour toi ou ta mission. »

Congédiant son homme de la main, Spinola s’appuya le menton dans la paume, plongeant son regard vers le sol, fuyant la lumière du petit foyer. Au moment de franchir la porte, le sergent se figea, puis se retourna vers son maître, le visage interrogateur. Apercevant du coin de l’œil le geste, Mauro leva un front agacé.

« Eh bien ! Ne demeure là figé comme statue ! Tu as tes ordres ! »

Acquiesçant d’une moue qu’il s’efforça de rendre convaincue, Guilhem disparut dans la noirceur de la nuit. Il salua le garde d’un geste amical sur l’épaule et se dirigea vers son cheval, attaché auprès des vestiges d’une citerne. Lorsqu’il monta en selle, il lança un regard vers la forteresse du temple. On n’en percevait qu’à grand-peine les contours, avec la lune presque invisible, mais les fines raies de lumière qui y brillaient évoquaient autant de joyaux de plaisir au sergent. Il devait y avoir belles réjouissances parmi la garnison victorieuse. Chassant ces pensées de son esprit, il caressa de la main l’encolure de son cheval. Reniflant avec vigueur, il éperonna doucement et se mit en marche, avalé par les ténèbres.

## Gênes, matin du mercredi 2 novembre 1155

Lorsqu’il avait aperçu la puissante stature d’Imberto Mallonus déambuler parmi la foule amassée aux abords de la cathédrale Saint-Laurent, Mauro Spinola avait eu un sourire de contentement. Le navire était de retour, porteur de marchandises égyptiennes, d’argent et de nouvelles. Mais l’air grave du patronus[^patronus] avait rapidement mis un terme à cette bonne humeur. Comprenant qu’il s’agissait d’une affaire importante, il attira le marin un peu à l’écart, sur le côté des marches, indiquant au passage d’un geste à son valet de patienter en retrait. Le navigateur n’avait visiblement même pas pris le temps de se changer ni de s’apprêter pour venir le voir. Sa tenue était sale et sa mine affreuse arborait les stigmates d’une difficile traversée. Après un rapide salut, il entra directement dans le vif du sujet.

« Les Égyptiens ont viré de bord. Plusieurs de leurs nefs ont écumé les côtes du royaume, et nous n’avons qu’à grand-peine pu aborder à Alexandrie.

— Que me contes-tu là ? Tu y étais avec l’aval du vizir Ruzzayk lui-même !

— Certes, mais il s’avère qu’il y a grande colère là-bas, car on le trouve bien trop amiable avec les chrétiens.

— Alors qu’il fait assaillir nos ports ?

— L’escadre n’est peut-être pas de son fait… »

Mauro fit claquer sa mâchoire de surprise. Il n’avait pas prévu que le nouveau vizir égyptien n’arriverait pas à tenir ses officiers. Il avait misé sur lui et jouait gros, à la fois en tant que diplomate, mais aussi comme marchand. Comprenant l’embarras du vieil homme, Mallonus enchaîna.

« Par heureuse fortune, les balles… importantes étaient faciles d’accès. Je les ai donc déchargées prestement. Et j’ai préféré aller vendre le reste au royaume. Cela a reporté mon départ, mais avec le bon vent, cela n’a guère eu de conséquence. J’ai pu tirer bon prix tout de même des peaux et des étoffes. »

Hochant la tête en affichant un sourire de circonstance, Spinola eut un peu de baume au cœur en entendant cela, mais sur le fond, il était fortement affecté. Il s’en voulait de n’avoir pas su prévoir l’attaque. Il tenait son pouvoir de ses informations, mais aussi de sa capacité à pressentir l’avenir. Ses opposants au sein de la Compagna ne manqueraient pas de se servir de cela pour tenter de l’abattre. La cité n’avait retrouvé le calme que tout récemment, après les scandales liés à l’expédition espagnole. Il regrettait un peu d’y avoir pris part, cela l’avait entraîné trop loin de l’Outremer, et trop longtemps. Il fallait qu’il reprenne la main rapidement. Et pour ce faire, il avait peu d’alternatives. La seule option qu’il avait était égyptienne, les Turcs de Syrie se montrant décidément bien trop hostiles négociateurs à son encontre.

« As-tu idée de ton prochain voyage Outremer ?

— Je viens à peine de mouiller les fers, messire ! Et la clôture[^cloturemer] est proche. »

Le diplomate acquiesça, légèrement agacé. Il allait devoir trouver un autre messager pour envoyer de nouvelles instructions à son espion au loin. Une fois averti, Guilhem saurait assurément dénicher ce qui lui manquait, prendre les bonnes décisions et lui faire parvenir un rapport précis. Lui-même ne pouvait embarquer, de nombreuses affaires le retenaient dans la cité, et il était important qu’il soit là en cette période difficile. Sans compter qu’il devrait vraisemblablement faire quelques voyages vers le nord, dans des régions qu’il appréciait moins que les côtes syriennes. Sa principale crainte venait du fait que le Temple avait exigé une rançon considérable pour Nasr, soixante mille dinars.

Ruzzayk avait certainement trouvé le montant un peu exagéré, mais il avait compris la nécessité de ne pas laisser un adversaire hors d’atteinte. Il s’était d’ailleurs empressé de le livrer à la famille de ses prédécesseurs massacrés, qui se chargea de lui faire payer cruellement ses agissements. Mais il avait peut-être gardé quelque ressentiment pour la somme extorquée,versée en outre à un ordre religieux qu’il savait être des plus féroces opposants à l’Islam.

Comprenant que le patronus attendait un signe de sa part, il le remercia chaleureusement et lui proposa de passer chez lui dans la semaine pour faire les comptes et se réjouir autour d’un bon repas des affaires réalisées. Impatient de profiter d’un peu de repos après un voyage éprouvant, le marin ne se fit pas prier et il repartit avec vigueur, non sans avoir salué avec respect.

Mauro Spinola chercha des yeux son valet et le découvrit assis sur les marches, occupé à discuter fort aimablement avec une servante au joli minois. Sans qu’il ne se l’explique, il en fut agacé et appela d’une voix furieuse son domestique. Comprenant immédiatement la situation, celui-ci ne prit qu’un bref instant pour retrouver sa place à la suite de son maître. Néanmoins, avant de s’éloigner sur ses pas, il lança un regard joyeux vers la jeune fille, qui lui répondit d’un salut enthousiaste.

Heureusement pour lui, Mauro était de nouveau plongé dans ses pensées et n’accordait guère d’attention à ce qui l’entourait, avançant instinctivement en direction de sa demeure. Le vieux diplomate commençait à douter de lui, ce qu’il n’appréciait guère. D’un côté, il était possible que Ruzzayk ait finalement décidé de prendre les armes contre le royaume de Jérusalem, ce qui était stupide de sa part, sauf dans le cas où il envisageait de se rapprocher des Syriens car il n’avait pas la puissance militaire de s’attaquer seul aux troupes franques.

Mais d’un autre côté, Ruzzayk pouvait suffisamment perturber les Latins pour les pousser à fragiliser leur frontière orientale et laisser ainsi le champ libre aux Turcs d’Alep et de Damas. Cela semblait néanmoins bien incroyable. L’Égypte sortait d’une difficile guerre civile, et il était plus probable que le vizir doive faire face à des opposants qui contrecarraient sa politique de trêve avec les chrétiens, voire à des opportunistes profitant de la relative anarchie pour faire rapidement fortune.

Dans tous les cas, cela contrariait fort les plans du vieil intrigant. Et ça, il n’était certes pas prêt à l’accepter sans se battre.

## Tripoli, matinée du lundi 24 décembre 1156

Lorsqu’il leva la tête de ses papiers pour accueillir l’hospitalier, Mauro eut un instant de surprise. Il ne s’attendait pas à un tel homme. Pour lui, les frères de saint Jean n’étaient guère plus que des valets en bure, occupés à nourrir et soigner les miséreux.

Pourtant, se tenait devant lui un grand gaillard porteur du manteau noir à la croix blanche, par-dessus une vêture assez riche, long bliaud sombre et fine ceinture passementée. Malgré une impressionnante musculature, ses mouvements étaient gracieux et son visage étonnamment fin. Les joues creusées, son regard paraissait presque fiévreux, larges orbites inexpressives sous d’épais sourcils. Sur son crâne, on apercevait une discrète tonsure, luisante, refaite certainement à l’occasion des fêtes de Noël à venir. Il sourit avec chaleur et inclina légèrement le buste, se présentant comme frère Raymond.

Le vieil homme se leva et l’invita à s’asseoir devant lui. Il était confortablement installé dans une petite pièce richement aménagée de tapis et de meubles orientaux. Seul élément occidental, une belle table à tréteaux était recouverte de nombreux documents, courriers, extraits de comptes, et recevait la vive lumière qui filtrait à travers les panneaux de verre.

Un scribe, les doigts tachés d’encre accueillit d’un sourire le nouvel arrivant puis se replongea bien vite dans ses travaux d’écriture. Après avoir appelé un valet pour qu’on leur serve à boire, Mauro se présenta rapidement avant d’aborder ce qui lui tenait à cœur, mais l’hospitalier ne lui laissa guère le temps de développer.

« Le frère cellérier m’a dit qui vous étiez, messire Spinola. Outre, il serait fort malheureux pour officier de mon ordre, si présent en le comté, de ne pas vous connaître, du moins de nom. »

Le vieux diplomate accepta le compliment avec grâce, mais circonspect quant à la suite, subodorant là une entrée en matière ambiguë.

« Vos produits sont toujours de premier choix, m’a-t-il affirmé, et vous n’exigez pas fortes sommes pour ceux-ci, ce dont mon ordre ne peut que vous louer. Mais si vous avez demandé à me voir, il y fort peu de chance que ce soit pour discuter négoce.

— Certes non. Votre nom m’a été recommandé, car je recherche quelqu’un qui se trouve peut-être en geôles sarrasines. »

Frère Raymond marqua un temps puis écarquilla les yeux, attendant d’en savoir plus.

« Un des miens serviteurs a disparu depuis de nombreux mois alors qu’il était à Damas, il a pour nom Guilhem Torte. »

Le moine plissa les yeux, marquant son attention puis toussa avant de répondre d’une voix lente et posée.

« Je vois. Point crucial à prendre en compte : vous seriez prêt à verser rançon pour lui ?

— Certes ! Il vaut bien chevalier. »

L’hospitalier remua sur sa chaise et se gratta l’oreille, pensif.

« Il me faudra une description, et la date présumée et le lieu où il aurait pu être pris également, et j’en parlerai autour de moi.

— Mille grâces !

— Il me faut néanmoins vous indiquer que je n’œuvre que peu avec les Damascènes. Depuis l’arrivée des aleppins, les contacts sont moins aisés. Je m’emploie surtout à libérer des captifs de pirates égyptiens. »

Mauro hoqueta de surprise.

« Je pensais que la trêve avec eux était efficace.

— Elle l’est. Il s’agit de quelques pérégrins oubliés en leurs geôles depuis bien trop long temps. Ils ont été pris par traîtrise, peu avant la trêve. Avec un navire et des matelots maquillés. Comme ils étaient aux mains d’officiers en désaccord avec le vizir, il a été difficile de les récupérer. Et le prix demandé est exorbitant ! »

Prenant le temps de la réflexion, Mauro se tourna finalement vers son notaire et l’interpella :

« N’avons-nous pas ici quelques monnaies qui pourraient aider à la libération de captifs, Giacomo ? »

Décontenancé par la demande, le serviteur tourna la tête de droite et de gauche, comme s’il avait la capacité de faire apparaître les pièces près de son écritoire. Un soudain éclair d’intelligence lui permit de comprendre ce qu’on attendait de lui et il tendit la main vers un paquet de tablettes de cire reliées, qu’il commença à parcourir en marmonnant. N’ayant rien perdu de l’échange, frère Raymond inclina du chef avec élégance.

« Ne vous sentez pas mon obligé en ce qui concerne votre demande. Nous aidons à recouvrer les captifs, car c’est notre mission de chrétien. Nul don n’est requis.

— Je l’ai bien entendu ainsi, frère, mais il me sied aussi de prêter aide et assistance à mes frères et sœurs engeôlés. »

Pendant ce temps, le domestique s’était approché et tendait une tablette à Mauro, indiquant de son stylet une liste récapitulant des bourses scellées contenant des dinars. Le diplomate écarta la tête, cherchant à lire les minuscules inscriptions, les paupières plissées, puis annonça à l’hospitalier :

« Je peux offrir à l’instant deux bourses de pièces d’or égyptiennes, la première de trente dinars et la seconde de quinze, toutes deux scellées par le Trésor califal.

— Voilà fort charitable geste, messire Spinola. Je ne sais que dire !

— Il n’y a rien à dire, frère. Dieu me donne assez pour que je partage. »

Il indiqua d’un signe de tête qu’on lui apporte le coffret ferré dans lequel il tenait ses monnaies. Puis il dévisagea l’hospitalier, encore ébahi de l’offre. Savourant sa surprise, il afficha un large sourire, assez énigmatique.

« Noël n’est-il pas occasion de s’enjoyer ? »

Lorsque l’affaire fut réglée et le frère hospitalier reparti, encore charmé de l’incroyable don qu’on lui avait fait, Spinola demeura un long moment le regard dans le vide, savourant un verre de vin de Judée qu’il appréciait tout particulièrement. Percevant que son écrivain semblait ennuyé et faisait beaucoup de tapage à bouger ses documents, il se tourna vers lui, un peu irrité.

« Qu’est-ce donc, Giacomo ? Pourquoi t’agiter pareillement ?

— Je vous demande le pardon, messire. Je reprenais quelques comptes, car ces dinars vont peut-être nous manquer. Nous n’avons guère de monnaies, tout est en marchandises, et nous avons moult règlements prochainement.

— Tu estimes ma générosité mal à propos, c’est cela ? »

Le vieil homme parlait doucement, sans qu’il soit possible de déterminer son sentiment. Son domestique dansait d’un pied sur l’autre, désireux de se sortir du mauvais pas dans lequel il venait de s’engouffrer. Il connaissait suffisamment son maître pour le savoir prompt à la colère. D’un autre côté, ne pas le prévenir pouvait tout aussi bien attirer sur sa tête tous les malheurs de l’enfer.

« Ce n’est certes pas à moi d’en juger, mais nous départir de dinars du trésor en pareil moment, ce n’est effectivement pas facile pour vos affaires. »

Le vieux génois examinait son gobelet de verre émaillé comme s’il y voyait quelque vérité cachée, et répondit lentement, appuyant les syllabes avec emphase.

« Tu as raison, Giacomo… »

Le notaire exhala un profond soupir de soulagement, qui s’interrompit bien vite.

« …Ce n’est pas à toi d’en juger. » ❧

## Notes

L’espionnage et le renseignement jouaient bien évidemment leur rôle dans les conflits qui opposaient les puissances moyen-orientales au XIIe siècle. La bonne connaissance de la composition des armées adverses, des possibles défections qui pouvaient y être suscitées, constituaient tout un pan de la diplomatie intense qu’on pouvait observer alors. Des émissaires allaient et venaient entre les différents princes, et les marchands savaient se faire à l’occasion collecteurs d’information, ainsi que des voyageurs plus discrets, comme les pèlerins. Néanmoins, il n’est pas certain que la plupart aient eu une véritable vision d’ensemble qui leur aurait permis d’avoir une stratégie globale à l’échelle de la région.

Par ailleurs, la situation du califat fatimide après la perte d’Ascalon en 1153 se dégrada fortement. Les dirigeants se déchiraient dans des conflits internes, laissant la porte ouverte aux influences extérieures qui leur seraient fatales. Les Turcs et Kurdes de Syrie, sous le contrôle de Nūr ad-Dīn, bataillèrent ferme pour s’assurer la région de Damas en plus du nord de la Syrie, et se tournèrent ensuite naturellement vers le Caire. Mais le royaume latin de Jérusalem partageait les mêmes ambitions, ce qui entraîna de tragiques et épuisants affrontements entre les différents groupes, durant de longues années..

## Références

Bach Erik, *La cité de Gênes au XIIe siècle*, Gyldendal, Copenhague : 1955.

Elisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Gravelle Yves, *Le problème des prisonniers de guerre pendant les croisades orientales (1095-1192)*, Mémoire de maîtrise ès Arts en histoire, Université de Sherbrooke, Sherbrooke, 1999.

Prawer Joshua, *Histoire du Royaume latin de Jérusalem*, CNRS Éditions, Paris : 2007.

[^4]: 19 juin.
