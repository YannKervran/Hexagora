---
date: 2014-01-15
abstract: 1157. Pour servir le roi de Jérusalem il ne suffit pas d’accomplir des tâches de police ou de gestion. Il faut prendre conscience que l’on n’est qu’un rouage de son administration. Ainsi, Ernaut découvre que la politique de Baudoin ne se résume pas à tuer sans réserve tous les musulmans rencontrés, avec l’appui de ses frères chrétiens. Il lui faut se conformer aux manœuvres diplomatiques qui aboutissent à la rencontre avec certains de leurs adversaires d’un temps ou à venir. Ou encore découvrir la haine envers ceux qu’il pensait être leurs alliés naturels.
characters: Ernaut, Baset, Eudes Larchier, Stamatès Mpratos
---

# Frères ennemis

## Jérusalem, après-midi du vendredi 19 juillet 1157

Profitant de l’ombre du passage perçant la muraille à la porte de David, Ernaut discutait avec un messager venant du comté de Triple[^triple]. Ce dernier avait croisé les forces flamandes installées aux abords de Beyrouth qui accompagnaient le comte Thierry[^thierryalsace], une nouvelle fois porteur de la croix. De nouvelles opérations militaires se profilaient donc dans la partie nord du royaume. C’était de toute façon la saison, comme le constatait le cavalier.

Peu après la célébration de la libération de Jérusalem quelque cinquante ans plus tôt, les esprits étaient échauffés et le désir de porter l’assaut au sein du territoire musulman plus prégnant que jamais. Les affrontements autour de Panéas[^paneas] les mois précédents et la capture humiliante de nombreux hommes incitaient à la vengeance[^meleha]. En outre, le jeune Baudoin semblait déterminé à ne pas demeurer tranquille, toujours désireux de se mettre en selle pour asssaillir les Turcs, désormais maîtres du nord et de l’est.

Le bruit d’une forte compagnie de chevaux approchant attira leur attention. Ils se tournèrent vers la porte d’entrée et purent admirer une impressionnante troupe. Une vingtaine d’hommes sur de splendides montures aux harnais soignés venait de rejoindre le passage d’accès. La plupart portaient des vêtements de soie rembourrée chatoyant sous le soleil, des épées étincelantes, des turbans immaculés. Au milieu, dans une tenue encore plus flamboyante, porteur d’un couvre-chef élaboré, l’un d’eux arborait un bâton écarlate, qu’il tenait de façon ostensible.

Ernaut vit Eudes, le sergent responsable de la porte, s’approcher avec respect afin de s’enquérir de leur identité. Le riche cavalier leva la main en un salut formel et déclara d’une voix autoritaire, emprunte d’un léger accent :

« Au nom du Père, du Fils et du Saint-Esprit, Dieu seul et unique, Manuel[^manuelcomnene], fidèle de Dieu, Haut et Auguste Autocrate et Grand Empereur des Romains, m’envoie, moi son mandatôr, auprès de son bien-aimé et espéré frère spirituel Baudoin, noble d’entre tous, et distingué roi de Jérusalem. »

Il fallut à Ernaut un moment pour décrypter l’annonce, tandis qu’il dévisageait la troupe, un homme à la fois. Il n’était d’ailleurs pas le seul, la plupart des sergents de faction admiraient les chevaux et l’équipage. Des badauds s’étaient même approchés depuis les rues proches, curieux de voir plus en détail la magnifique ambassade. Baset, qui se trouvait désormais à la gauche de Ernaut, lui souffla :

« Ça faisait un petit moment que les Grecs n’avaient mandé nul messager. »

Deux sergents furent désignés pour escorter la troupe jusqu’au palais royal. Ils partirent en trottinant, suivis de la file de cavaliers. Les soldats, en passant, conservèrent une attitude de froide indifférence, certains d’attirer à eux les regards. Lorsqu’ils eurent disparu dans les rues de la cité, la tension retomba instantanément à la porte. Les hommes échangèrent des moues appréciatrices et l’un d’eux cracha par terre, là où s’était tenu le messager.

« Aussi fiers qu’un étron de pape, ces maudits Grecs.

— Y sont toujours ainsi, à toiser le monde. Mais le prince Renaud[^renaudchatillon] leur a bellement botté le cul !

— Ils ont dû apprécier ça, c’te bande de sodomites ! »

Ernaut écoutait sans intervenir. Il ne comprenait pas bien l’hostilité des sergents vis-à-vis de ceux qui étaient généralement leurs alliés contre les infidèles. Il n’avait pas encore pris conscience de la complexité des rapports diplomatiques entre les états moyen-orientaux, et estimait simplement que la place de chrétiens était aux côtés de leurs frères, face aux musulmans. Un monde ordonné, net, comme il n’en existe que dans les esprits simples ou encore jeunes.

« N’empêche, j’irais bien fouler les rues de leur cité, moi. On la dit si grande qu’il faut plusieurs jours pour aller d’une muraille à l’autre. Elle enferme moult merveilles dont le corps et l’esprit ne se lasseraient pas, un siècle durant.

— Un mien compagnon y est passé avec le roi Louis[^louis7b], mais on dit qu’ils ne laissent nul étranger y pénétrer facilement.

— Ils ont peur qu’on leur pique leurs femmes peut-être ? ironisa Baset.

— Pour ce qu’ils en font ! » s’amusa le sergent.

La répartie déclencha des rires gras, moqueurs, méfiants. La plaisanterie soudait le groupe, lui donnait son identité, exclusive des autres, de ceux qu’elle raillait. Seuls parmi ces sergents moqueurs, Ernaut tourna la tête vers le chemin qu’avait emprunté le mandatôr et son escorte, les yeux encore emplis de la magnificence des tenues et des équipements. Lui aussi se prenait à rêver quand on évoquait le nom de leur cité de légende. Byzance.

## Jérusalem, soirée du mardi 23 juillet 1157

Récemment promu sergent, Ernaut se voyait souvent confier des tâches subalternes. Il subissait tant bien que mal ces brimades, désireux de se faire accepter par la petite communauté des hommes du roi. Il parcourait donc le passage, une pelle et un seau en main, afin de ramasser les crottins et déjections des différents animaux qui franchissaient le seuil de la cité. Le tout était entassé dans un coin de la muraille, et revendu aux paysans du coin, ce qui finançait la boisson des hommes affectés à la garde des portes.

Au début on l’avait gentiment brocardé tandis qu’il s’affairait à nettoyer le sol, mais cela faisait partie des rituels d’intégration au groupe, et désormais plus personne ne prêtait attention à lui. Désormais, il attendait simplement qu’un nouveau venu prenne sa place et le seau malodorant, de façon à pouvoir le railler à son tour. Il allait vider son récipient quand il discerna un nuage de poussière sur la route de la Mer.

Des cavaliers allaient se présenter d’ici peu. Il attendit là un moment, afin d’identifier qui cela pouvait être. Il était curieux de voir ce fameux comte de Flandre dont tout le monde parlait. Un farouche chevalier, à la lance vaillante, et un vrai combattant de Dieu, venu de nombreuses fois dans le royaume. Il réalisa peu à peu que ce n’étaient certainement pas des hommes de Jérusalem et revint précipitamment dans le passage.

« On dirait que des païens s’en viennent par devers nous ! »

Plusieurs hommes s’approchèrent, intrigués, les mains sur les hanches et les yeux plissés tandis qu’ils s’efforçaient d’identifier les nouveaux arrivants. Ce fut Eudes qui parla le premier.

« Jambe-Dieu ! Des Turcs ! »

La remarque fit grimacer tous les présents. La plupart ici avait eut affaire au moins une fois aux flèches impitoyables, aux sabres assoiffés des terribles combattants musulmans. Il était rare d’en voir autrement que dans le fracas et la poussière des affrontements. Les sergents étaient comme hypnotisés. La troupe était composée d’une dizaine d’hommes, tous habillés légèrement, sans armure, mais le sabre et l’arc à la ceinture.

Ils étaient couverts de la poussière du voyage mais leurs tenues n’étaient pas celles de simples nomades. C’étaient des soldats professionnels, de ceux qu’on forme dès leur plus jeune âge et qui constituaient l’élite des armées des sultans et des émirs.

Il firent halte devant la porte, face aux sergents disposés en ligne. Le premier des cavaliers, un jeune aux longues nattes encadrant un visage creusé habillé d’une courte barbe, les yeux bruns lançant des éclairs, fit avancer son cheval au pas et leva la main, avant de déclarer d’une voix forte plusieurs mots dans sa langue. Un autre homme, qui ne portait pas le sharbush[^sharbush] mais un simple turban, s’avança alors et traduisit.

Ils étaient envoyés par le sultan Qilig Arslan[^qiligarslan], ennemi du maudit Nūr ad-Dīn[^nuraldin], roi d’Alep et de Damas. Avant qu’Eudes ne pusse répondre, une voix s’était élevée « Dieu lo veult ! » et un amas de poussière s’était écrasé sur l’épaule d’un des cavaliers. Le cheval fit un écart, tandis que le Turc grognait et cherchait l’agresseur de son regard perçant.

Ernaut avait aperçu le fautif, un valet d’un des caravansérails voisins. En trois pas il fut sur lui. Alors qu’il allait l’empoigner, il sentit la présence du cavalier dans son dos et se retourna. Le soldat le dévisageait, l’air menaçant. Il avait la main posée sur la poignée de son sabre. Ernaut lui fit un signe d’apaisement tout en s’emparant du valet par le col de son vêtement. Quelques instants plus tard, le domestique apprenait les lois élémentaires de la physique après un vol plané qui le vit atterrir dans le tas de fumier. Aucun rire moqueur ne s’éleva, chacun attendait la réaction de leurs vis-à-vis.

Le porteur du turban s’approcha d’Ernaut et lui sourit.

« Nous sommes ici en paix, merci à toi. »

Ernaut fit un bref signe de tête en salut et reprit sa place parmi les hommes de la porte. Eudes désigna trois sergents pour leur montrer le chemin du palais royal. Ernaut était de ceux-là. Il lui glissa discrètement :

« Fais attention en route. Il y a moult croisés en ces jours dans la cité. Nous devons éviter qu’un messager soit malmené. »

Ernaut lui répondit d’un sourire féroce, posant la main sur le pommeau de son épée. Puis il fit signe à l’interprète de le suivre. Fendant la foule d’un air sévère, il criait « Place ! Place ! » tout en écartant les plus lents d’un geste vif du bras. Il n’avait jamais eu à affronter des Turcs et mettait un point d’honneur à ne pas faillir à son devoir d’escorte, première tâche un tant soit peu sérieuse qu’on lui confiait.

Plusieurs badauds reluctants finirent parmi les produits qu’ils admiraient, propulsés dans les boutiques devant lesquelles ils s’attardaient. Un ours était en marche, et cet ours avait une mission.

## Jérusalem, fin d’après-midi du mercredi 24 juillet 1157

Appuyé au chambranle de la porte des cuisines donnant sur la cour, Ernaut dégustait un peu de vin tout en grignotant du pain. Il venait d’accompagner des valets à l’atelier des monnaies royales pour y déposer des barres d’argent qui allaient servir à frapper les pièces à l’effigie du roi. Il tenait une de celles-ci dans la main, toute neuve, une de celles qui avaient servi à le payer.

Autour de lui les marmitons, les coursiers, s’activaient pour préparer le repas du soir. Une odeur d’épice et de pain chaud lui titillait les narines. Il prenait souvent son repas au palais, profitant des restes des festins préparés pour la table du roi. En tant que sergent, il n’était pas parmi les premiers qui pouvaient y prétendre, mais la nourriture était suffisamment abondante et variée pour qu’il y trouve de quoi satisfaire son solide appétit et son insatiable gourmandise.

Il observait avec nonchalance un des cavaliers byzantins qui venait d’apparaître dans la cour. De taille moyenne, celui-ci était vêtu d’une cotte à revers sur le cou, d’une belle étoffe verte. Un foulard rouge ceignait sa taille, épaisse, sous son baudrier d’arme. Le cou large, les épaules puissantes auraient été à l’aise à manier la charrue ou la cognée. Il tenait une paire de bottes à la main, semblant hésiter sur la conduite à tenir. Ernaut fit quelques pas dans sa direction. Il fut accueilli d’un regard franc.

« Besoin d’aide ?

— Oui. Possible réparer semelles ? »

Ce disant, l’homme brandissait ses chaussures, qui avaient visiblement connu des jours meilleurs. Ernaut acquiesça et lui fit signe de le suivre. Il confia son gobelet à un gamin de cuisine qui passait là et se dirigea vers les rues de la cité.

« Stamatès ! déclara le grec.

— Je ne parle pas votre langage, l’ami, répondit Ernaut.

— Non, Stamatès mon nom. Stamatès Mpratos. »

Le jeune homme sourit de sa méprise.

« Et moi Ernaut. Ernaut de Vézelay ! »

Ils échangèrent un sourire et se coulèrent dans le flot de pèlerins. Le palais royal était accolé à l’église du Saint-Sépulcre, et se trouvait donc dans la partie de la cité où l’on croisait le plus de voyageurs. En cette période de l’été, il était difficile d’avancer dans les rues environnantes, entre l’hôpital de Saint-Jean et la rotonde qui abritait le tombeau du Christ. Chemin faisant, Ernaut, toujours curieux, s’efforçait de faire la conversation.

« Tu parles bien notre langue, tu es interprète ?

— Non ! Soldat. Mais je connais gens de ton pays, quand eux passés sur nos terres voilà dix ans.

— Tu as vu le roi Louis !

— Oui, le roi Louis. Bonne armée. Pas comme Conrad[^conrad], armée pas sérieuse, pillards et voleurs. »

Son visage se fendit d’un large sourire.

« Battu un peu contre eux, pour calmer. Bon soldats ! »

Ils finirent par trouver un savetier qui accepta de prendre les souliers, indiquant de repasser le lendemain.

« Ça te dirait un peu de vin, compère ? Je connais bonne taverne pas loin.

— Oui. Dans mon village, on aime le vin.

— Comme chez moi alors ! Mon père est vigneron.

— Dans le passé, démons de mon pays passaient leur temps boire le vin. Et s’amuser avec femmes ! »

Stamatès arbora un large sourire à l’évocation de ces légendes, emboîtant le pas à Ernaut.

« Les démons de Byzance ?

— Non, je ne suis pas de Polis, être du thème de Thessalonique. Petit village. Radolibos. Je pas aime grandes cités. Ici ça va encore, mais la Polis… »

Il fit un geste des mains indiquant la démesure, tout en soufflant, amusé puis ajouta un clin d’oeil.

« Je préfère voyages, campagnes du Basileus Komnenoi. »

Il empoigna sa petite masse à tête de bronze, qu’il portait passée à la ceinture.

« Je écrase têtes des ennemis partout où il faut. Polis pas pour moi.

— J’aimerais bien faire visite en ta Cité, moi.

— Belle. Très belle. Mortelle aussi. Vieille Polis. »

Il s’esclaffa et tapa amicalement sur le bras du jeune homme.

« Je te dirai secrets à savoir pour éviter ennuis dans la Polis. Allons boire ! »

Ernaut hocha la tête avec enthousiasme, avançant avec d’autant plus de vigueur. Un instant il se rappela la réputation homosexuelle qu’on prêtait aux Byzantins, mais il balaya cette pensée en un instant. Stamatès avait l’allure d’un vétéran et, un gobelet à la main, prendrait certainement plaisir à narrer ses aventures. De celles qu’Ernaut rêvait de vivre. ❧

## Notes

Les relations du royaume chrétien de Jérusalem avec les puissances locales ne se calquaient pas sur les croyances religieuses, pas plus que chez ses adversaires. Des alliances de circonstance apparaissaient avec des dirigeants musulmans, des pactes de soutien mutuel étaient régulièrement conclus.

De la même façon, l’empire byzantin jouait les uns contre les autres. Cette attitude pragmatique des pouvoirs politiques heurtait beaucoup de croisés fraîchement débarqués, qui critiquaient à l’envie ceux qui avaient trahi selon eux l’héritage des premiers croisés conquérants. Cette politique était rendue possible par la grande fragmentation de la puissance musulmane.

L’unification graduelle sous Nūr ad-Dīn puis Saladin finit par rendre inefficaces ces manœuvres diplomatiques.

## Références

Augé Isabelle, *Byzantins, Arméniens & Francs au temps de la croisade. Politique religieuse et reconquête en Orient sous la dynastie des Comnènes 1081-1185*, Paris : Geuthner, 2007.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Elisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.
