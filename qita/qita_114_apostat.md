---
date : 2021-04-15
abstract : 1149-1158. Galien, arbalétrier croisé d’origine parisienne, se retrouve à devoir répondre à une difficile question, pour quoi se bat-il vraiment ?
characters : Galien, Pâkalin, Boutoum, Hugues, Jannoni, Wikerus, Courrat, Rashid al-Nasir
---

# Apostat

## Antioche, casernement près de la porte du Chien, matin du lundi 4 juillet 1149

Galien fut réveillé par les éclats de voix d’une dispute. Inquiet de l’entendre si près, dans son dortoir, il ouvrit un œil. Son capitaine essayait visiblement de ramener de l’ordre dans une discussion animée entre deux groupes. Il se releva, s’asseyant sur sa paillasse posée à même le sol. Il s’était couché tout habillé, ayant veillé fort tard aux environs de la porte du Chien qu’il contribuait à garder. La dispute semblait se calmer et l’attroupement commencer à se disperser. Galien se tourna vers Courrat, son compère, d’un air interrogateur. Celui-ci était en train de ranger ses affaires dans son coffre.

« Rien de grave. Certains des gars pensent qu’il faut absolument tenir et d’autres avancent qu’on devrait demander l’aman[^aman]. Le capitaine leur a dit que de toute façon ça dépendait pas de nous. »

Il s’assit sur sa malle, laçant ses bottines.

« Nous devons de nouvel nous poster vers la porte du Chien ce matin, les engins semblent vouloir se tourner sur cette partie de l’enceinte.

— D’aucune façon, nous ne saurions tenir solidement les murailles avec le peu d’hommes que nous sommes.

— Je ne suis pas si sûr. Il est encore temps que le roi Jérusalem s’en vienne.

— Nul coursier du sud ne s’est présenté, que je sache…

— La reddition te semble donc une solution envisageable ? »

Galien se rapprocha de son ami et lui répondit à voix basse.

« Et alors ? Je suis sûr que de bons soldats trouvent toujours preneurs. Réfléchis un peu à ça, mon vieux. Moi, je suis pas prêt à me faire sabrer le col pour un besant la semaine. »

Il se leva pesamment puis rassembla ses maigres affaires dans son coffre, qu’il referma à clef après en avoir sorti son armement. Équipé de son casque et de son arbalète, il alla remplir son carquois de carreaux dans un des tonneaux à cet usage au bout de la pièce. Enfin, il se rendit dans une grande salle adjacente où il pouvait se restaurer. Plusieurs de ses camarades étaient là, se préparant nonchalamment à une longue veille. En quelques mois, Galien avait fait partie de diverses garnisons depuis qu’il était venu des environs de Paris avec le roi Louis. Toujours prêt à accepter la moindre tâche militaire, il avait été recruté quelques jours plus tôt pour garnir les murs de la cité, maintenant que le seigneur des lieux avait été tué dans la bataille de Fons Muratus. 

L’avancée du maître d’Alep, Nur ad Din, avait été fulgurante depuis sa victoire sur l’armée du prince Raymond. Il se disait dans les marchés que la capitulation des villes sur son chemin était promptement négociée. En cas de refus, les citadelles étaient mises à bas avec une cadence qui laissait craindre le pire. Des commérages détaillaient la détresse de la princesse Constance, qui avait confié la régence de la Cité au patriarche Aymeri.

Depuis quelques jours, des rumeurs circulaient parmi la population, et certains osaient discuter de reddition. Les esprits étaient troublés, entre les tenants d’un jusqu’au-boutisme absolu, dont les plus récents arrivés étaient, et ceux qui pensaient qu’il était possible de se rendre de façon honorable, s’appuyant sur le fait que sans armée pour les défendre, la situation était désespérée.

Le capitaine siffla le rassemblement alors qu’ils finissaient de prendre leur repas de bouillie froide, agrémenté d’une pincée de fruits secs. De sa voix rauque, il leur donna les instructions pour la journée. Les troupes musulmanes semblaient attendre, leur camp demeurant relativement tranquille. Il fallait néanmoins rester vigilants et en profiter pour se faire voir sur les murailles. Chaque tronçon en fut donc attribuée à un modeste groupe d’hommes, appuyé par des serviteurs ayant pour tâche de nettoyer et remettre en état les chemins de ronde. Galien et Courrat s’arrangèrent pour être ensemble et furent affectés à une section de courtine entre la porte Saint-Georges et le mont Silpius, où la situation était assez calme.

Chemin faisant vers le sud, alors qu’ils traversaient un secteur commerçant, Galien s’arrêta longuement chez un contact marchand, Chirag. Il l’avait connu au moment de son arrivée dans la principauté et avait travaillé pour lui à l’occasion, comme escorte pour ses caravanes. L’arménien avait de nombreuses relations d’affaires un peu partout dans la région et pratiquait le négoce, donc la diplomatie, avec un talent réputé. Galien voulait lui parler du plan qu’il avait en tête.

## Antioche, porte du Fer, soir du jeudi 7 juillet 1149

Galien rejoignit ses trois compères, appuyés contre un merlon du rempart, occupés à manger des dattes dont ils crachaient les noyaux en contrebas. Au-dehors, des feux illuminaient l’accès des gorges qui menaient à la ville, gardées par leurs adversaires. Bien souvent, des foyers étaient allumés en masse par les assiégeants afin de convaincre les captifs que l’armée qui les environnait était plus imposante qu’en réalité.

Comme il était peu probable que le gros des troupes puisse arriver par là, c’était un lieu assez calme, et il avait fallu aux compagnons batailler contre les prétentions des autres sergents qui souhaitaient aussi se retrouver dans un endroit peu exposé. Heureusement pour eux, les gardes de nuit à encadrer quelques miliciens n’étaient pas les plus recherchées. Responsable de la petite équipe sur cette section de courtine, Galien avait distribué quelques corvées à ses hommes, de la même façon que ses compères aux abords, espérant que cela leur laisserait le temps de glisser le long de l’enceinte et de s’évanouir dans le canyon entre Staurin et Silpius, avant que l’alerte ne soit donnée. Aucun d’eux n’avait envie de servir de cible aux archers.

Galien s’était muni des affaires qu’il tenait absolument à garder avec lui, ainsi que d’une arbalète, comme chacun de ses compagnons et avait confié le reste de ses possessions à Chirag. Il avait également pris une corde pour descendre la muraille. Galien et ses amis avaient choisi de tenter leur chance sous d’autres cieux.

« Bon, j’ai la lettre de Chirag qui devrait nous permettre d’être bien accueillis chez les sarrasins. Vous êtes toujours partants ? »

Jannoni, Wikerus et Courrat firent chacun à leur tour un signe de tête. Ils étaient décidés à déserter et à rejoindre le camp vainqueur. Galien fixa alors la corde à un des merlons de l’enceinte.

« À Dieu va ! dit-il en lançant un des brins par-dessus le parapet. J’y vais le premier. »

Les quatre hommes se retrouvèrent rapidement réunis au pied du mur, puis se faufilèrent comme des ombres, profitant de la très faible lune. Ils tentaient de se fondre contre les rochers qui balisaient le sentier entre les versants abrupts. Ils n’eurent pas le temps de cheminer bien loin qu’une voix se fit entendre, en arabe :

« Halte ! Qui êtes-vous, et que faites-vous là ? »

Celui qui venait de parler s’avança hors du repli de terrain qui le cachait jusque là. C’était un soldat bien armé, revêtu de mailles, accompagné d’une demi-douzaine de gardes moins bien équipés.

« Je vous ai vu descendre de la muraille. Vous êtes des espions ? Des assassins, avec toutes ces lames ?

— Nous arbalétriers, nous vouloir aider ost Alep » répondit Galien dans un arabe approximatif en tendant sa lettre.

L’officier fit un signe de tête à un de ses hommes pour qu’il lui apporte le document. Sans même le regarder, il le glissa dans sa ceinture d’étoffe et lança quelques ordres secs, afin qu’on désarme les quatre déserteurs. Puis, d’un geste du bras, il leur fit comprendre de les accompagner, solidement escortés.

Ils empruntèrent plusieurs sentiers, contournant les reliefs vers le nord, se rapprochant de la grande route menant au Pont du Fer. Ils ne tardèrent pas à apercevoir les amas de toiles et de corde, les cabanes de ce qui constituait le gros du camp de siège, en retrait des zones de combat. Ils suivirent, un peu effrayés, leurs gardiens jusqu’à une esplanade où des broussailles épineuses délimitaient un espace de détention. Aux alentours, des sentinelles patrouillaient entre des feux servant d’éclairage.

À quelque distance, une tente de belle taille avait été érigée. Le meneur y entra, les laissant attendre à l’extérieur avec les soldats. Nul ne semblait s’émouvoir de leur présence et les quatre captifs échangeaient des regards circonspects, constatant l’ampleur du campement qui abritait ceux qu’ils appelaient jusque là leurs ennemis. L’officier ressortit un long moment après, donna quelques instructions puis les abandonna aux gardes. Ils furent alors rapidement conduits à l’enclos où ils découvrirent une poignée de prisonniers sales et hirsutes, certains arborant des pansements sommaires. Galien essaya de dire dans son arabe approximatif à ses geôliers qu’ils étaient des soldats et venaient se battre, il fut poussé sans ménagement et jeté au milieu des autres.

Jannoni le regarda, interdit :

« Tu es sûr de ton ami ermin, dis-moi ?

— Autant qu’on puisse l’être. Il m’a jamais trahi.

— Moi, j’ai l’impression qu’on va finir esclaves » déclara Courrat en s’asseyant dans la poussière.

En les entendant, un des captifs s’approcha d’eux

« Demeurez coi si vous souhaitez éviter les ennuis.

— Qui es-tu ?

— J’étais de la garnison Bazmashan[^meshmeshan]. Ils m’ont proposé de m’engager si j’acceptais de me convertir. C’était ça ou finir esclave… Si j’ai bien compris, on va garnir une citadelle dans les reliefs au nord, je sais pas trop.

— Devenir un païen ? Dans les montagnes ? » s’exclama Galien.

Courrat le regarda d’un air désolé.

« Tu t’attendais à quoi ? »

Galien, contrarié, s’éloigna de ses compagnons et regarda par-dessus les reliefs, vers la muraille de la cité d’Antioche, où flottaient encore les bannières franques, ombres à peine visibles qui cachaient les étoiles par intermittence.

## Harran, baraquements des soldats, soirée du youm al joumouia 29 jumada al-thani 549^[Vendredi 10 septembre 1154.]

Comme chaque jour de prêche à la Grande Mosquée, la cité avait été en effervescence, chacun profitait de ce jour de repos pour aller visiter ses proches, faire la fête ou pratiquer quelques loisirs. Galien s’était rendu avec ses compagnons à la prière solennelle sous la supervision de son officier, puis avait passé le temps à jouer aux dés avec eux. Ils avaient également improvisé un concours de tir, rivalisant d’adresse avec quelques archers turcs.

Séparé des amis avec qui il avait déserté, cela faisait plusieurs années qu’il était cantonné dans ce baraquement, d’où on le sortait bien trop rarement à son goût pour de courtes campagnes, généralement vers l’orient. Il avait rapidement appris des rudiments d’arabe et de turc, mais demeurait complètement étranger aux jeux politiques qui expliquaient les affrontements auxquels il participait ou les garnisons qu’il renforçait ici et là. Il était affecté à Harran la plupart du temps, où il ne percevait presque rien comme solde, mais bénéficiait du gîte.

Il frappa à la porte de la salle où son émir, Rashid al-Nasir tenait ses quartiers. Celui-ci était un homme assez paternaliste, parfois autoritaire, mais sans sévérité. Il aimait plaisanter à propos de tout et il était souvent difficile de savoir ce qu’il avait en tête. Galien avait gagné son estime en ne rechignant jamais aux corvées et aux travaux pénibles.

« Al-Anṭākīyyah ! Que viens-tu quémander en cette heure tardive ?

— J’espère que je ne dérange pas, j’aurais besoin de votre avis.

— Si cela concerne les femmes, ne t’embarrasse donc pas de questions, fais assaut ! » ricana l’émir en caressant son imposante barbe.

Comprenant que Galien avait une demande plus complexe, il l’invita de la main à prendre place sur la paillasse face à lui. Il était visiblement en train d’écrire une lettre, mais il poussa son nécessaire et interrogea d’un regard son subordonné.

« Vous nous permettez qui-ci qui-là menus travaux pour les gens de la cité, et j’aurais besoin de connaître jusqu’où cette autorisation s’étend.

— Eh bien, tant que tes pas te gardent en nos murs et que tu es de retour au baraquement pour la prière hebdomadaire, ça me va. »

Galien hocha la tête, apparemment peu enthousiasmé par la réponse.

« Vous savez, j’ai usage de me louer à ’Isa al-Muzani…

— Le négociant de plats ? Oui, je connais bien sa famille.

— Voilà ! Et il se trouve qu’il prévoit un large convoi pour Alep, dans les semaines à venir. Il m’a proposé d’en être. »

Al-Nasir fit une moue qui se tourna vite en rictus réjoui.

« Alep me semble fort au-delà de nos murs… Et fort proche d’Anṭākīyyah !

— Il y aurait d’autres membres de la garnison qui se loueraient : Da’ud al-Jahdami, Pâkalin et Boutoum.

— Plusieurs canailles assemblées ne font pas un honnête homme, mon ami, s’amusa l’émir. »

Il passait sa main sur sa longue barbe tout en étudiant les réactions de Galien. À la lueur des lampes, ses yeux sombres semblaient des prédateurs camouflés sous ses épais sourcils.

« La période des campagnes est terminée, aussi paisible qu’à son habitude pour nous. Je comprends ton désir d’amasser quelques dirhems pour l’hiver. Qui serais-je pour m’opposer à cela ? »

Il assortit sa réponse d’un grand sourire, tout en ouvrant les mains en signe d’impuissance.

« Et s’il se trouve quelques absents lors de la revue de printemps, ce pourrait être dû aux fièvres de la saison froide, à quelque blessure mal soignée ou tout autre malheur du sort.

— Je serai de retour avant ramadan !

— Ne promets rien que tu ne puisses tenir, fils, *chercher à se justifier quand on n’est pas coupable, c’est s’accuser* ! »

Galien se renfrogna. Il avait bien sûr envisagé de profiter de ce déplacement qui le rapprocherait des provinces sous domination latine pour s’enfuir, mais le projet lui paraissait encore trop fou, trop flou, trop loin, pour prendre corps dans son esprit. Ce n’était rien de plus qu’une envie diffuse qui considérait soudain une porte de sortie. Al-Nasir hocha la tête.

« Pour ce travail, tu peux t’absenter, je ne te signalerai pas. Et pour le reste… N’oublie pas, fils : *ce qui est passé a fui ; ce que tu espères est absent ; mais le présent est à toi* ! »

Et sur cette parole énigmatique, il chassa Galien d’un geste de la main. Lorsque ce dernier fut dehors, l’émir s’autorisa un sourire, le temps de réaliser qu’il lui faudrait peut-être trouver quatre nouvelles recrues dans les mois à venir. Il balaya rapidement ces sombres pensées de son dicton favori : *Aujourd’hui maintenant, demain inch'allah*.

## Terre de Suète, abords du Yarmouk près des caves de Suète, matin du vendredi 11 juillet 1158

Galien et ses compagnons finissaient de rouler leurs sacs quand ils entendirent corner le premier appel. L’armée royale allait se mettre en marche, et cette fois c’était après une victoire, une de plus qui marquait un printemps plutôt florissant. L’arbalétrier avait rejoint une troupe sous le commandement d’un chevalier croisé, Hugues. Celui-ci était venu en Terre sainte dans l’espoir d’y aider au triomphe des forces chrétiennes et s’activait avec ferveur. Indifférent à la soif, la fatigue, les blessures, il dépensait sans compter pour soutenir l’effort de guerre.

C’était lui qui les avait prévenus la veille au soir de la décision du roi Baudoin de retourner à Jérusalem. Bien qu’obéissant, Hugues trouvait tel repli inopportun, prématuré, et aurait préféré que l’armée pousse son avantage pour capturer leur adversaire. Dépité de ce manque de témérité, il avait rejoint certains autres esprits échauffés autour de Bertaud des Lices. Celui-ci avait proposé de partir vers le sud où des escadrons égyptiens avaient ravagé certains territoires. Persuadé que Dieu leur apporterait une nouvelle fois la victoire, Bertaud envisageait de simplement se ravitailler dans la cité sainte avant de patrouiller la zone méridionale du royaume, jusqu’à Gaza et le désert.

La paie étant correcte, Galien avait décidé de prendre part au voyage. Il n’avait guère eu son content de butin avec les expéditions printanières et espérait encore pouvoir amasser de quoi tenir l’hiver confortablement. Il formait aussi depuis plusieurs mois le projet d’obtenir un coursier. Il sentait les années qui s’accumulaient sur ses épaules et sa blessure à la jambe se rappelait à lui de façon plus fréquente, augmentant sa claudication. Il avait donc entrepris de négocier pour se faire attribuer une monture afin de s’épargner la marche sur les sentiers caillouteux. Il espérait par ce biais être reconnu arbalétrier monté, voire capitaine pour ceux officiant à pieds. Il avait l’expérience pour cela, et ne manquaient que les quelques livres qui lui auraient permis de posséder son propre animal, garant de son statut.

Il finissait d’assujettir ses affaires sur son dos, son arme soigneusement protégée dans sa housse de cuir, quand le cor résonna pour la seconde fois. Sans se presser, les hommes se rassemblaient pour former le long cortège, chacun retrouvant la place qu’il avait tenue lors de leur arrivée. Galien déposa un de ses sacs, contenant un supplément de carreaux dans le chariot qui lui avait été assigné et rejoignit son rang. La poussière commençait déjà à irriter la gorge et certains nouaient un foulard sur leur visage pour s’en préserver. Au-dessus d’eux, un soleil éclatant resplendissait sur les reliefs gris. La journée serait interminable, mais au moins nulle crainte n’était à avoir d’éventuels assauts turcs.

Un des capitaines les longea au petit trot, balançant des outres de chèvre où allait croupir l’eau fraichement prélevée dans le Yarmouk. Des valets courbés sous de gros sacs de toile distribuaient du pain noir dans les rangs.

« On va pas faire de pause » grogna le voisin de Galien tandis qu’il tranchait avec peine la solide croute.

Galien haussa les épaules, prit sa part et fit passer la miche. Il avait l’habitude des mauvais repas avalés tout en marchant. Mais il avait hâte que cela finisse. Il n’avait plus de haine pour ses adversaires, pour en avoir partagé le quotidien durant quelques années. Ils n’étaient pas mieux traités, et ne possédaient rien qui puisse en faire les démons que le seigneur Hugues évoquait. Rashid al-Nasir ne s’était jamais montré pire officier que ceux qu’il avait subis depuis, en territoire franc. Il se contentait donc de suivre les ordres, de protéger ses compagnons, dans l’espoir, bien mince, qu’il arriverait un jour à connaître une vie moins âpre. ❧

## Notes

Le projet de parler d’un soldat qui irait servant un camp puis l’autre m’est venu il y a très longtemps, après avoir lu dans un ouvrage, dont j’ai oublié la référence, le récit d’un templier qui avait déserté la chrétienté après la chute des états latins à la fin du XIIIe siècle. Il faisait du négoce sur les rives de la mer Noire, après s’être converti à l’Islam et avoir épousé une femme.

Je me suis toujours dit que les liens sociaux plus distendus et moins denses qu’en Europe pouvaient favoriser ce genre de ruptures dans les habitudes. Nos ancêtres médiévaux possédaient dans l’ensemble la même capacité d’adaptation que nous et les circonstances pouvaient ainsi mener à de bien étranges destins. Y compris pour ceux qui risquaient leur vie les armes à la main, soi-disant pour défendre un idéal. Comme souvent, c’était au final bien plus couramment pour assurer leur propre survie et celle de leurs camarades les plus proches que pour promouvoir des causes plus larges.


## Références

Asbridge Thomas, *The creation of the Principality of Antioch 1098-1130*, Rochester : The Boydell Press, 2000.

Asbridge Thomas, Edgington Susan B., *Walter the Chancellor’s The Antiochene Wars*, Aldershot : Ashgate Publishing Limited, 1999.

Barrois Claude, *Psychanalyse du guerrier*, Paris : Presses Universitaires de France, 1993.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

