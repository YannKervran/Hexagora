---
date : 2019-11-15
abstract : 1159. Au tournant de l’hiver, alors que le prince Nūr ad-Dīn voit sa santé décliner, des ambitions naissent et des appétits s’aiguisent autour de celui qu’on espère plus que moribond.
characters : Razin, Al-misri, Idris, Sa’d ad-Dīn ’Utman, Yalim al-Buzzjani, Sawwar, Hakim
---

# Les chaussures du mort

## Damas, maison d’al-Kāla, midi du youm el itnine 20 dhu al-hijjah 553^[Lundi 12 janvier 1159.]

Avec le mauvais temps et le mois des pèlerinages à la Mecque, la ville bourdonnait tranquillement de ses activités quotidiennes sans l’agitation frénétique du reste de l’année. Razin et al-Misri, deux Soudanais exilés en Syrie depuis des années, prenaient un repas léger chez une vieille femme de leur voisinage. Désargentée, elle proposait mezzés et plats chauds à des prix modestes dans son étroit rez-de-chaussée accolé à une minuscule mosquée de la rue de l’Eau, en plein centre de la puissante cité. On y croisait une clientèle d’habitués, généralement des porteurs d’outres, qui traitaient al-Kāla comme une parente.

Les deux frères avaient invité à manger l’imam de leur congrégation, car ils avaient besoin d’un avis autorisé, à la fois sur des questions de foi, mais aussi sur certains points politiques, dont les religieux étaient souvent au fait. Ils étaient un petit nombre de vendeurs d’eau qui envisageaient de faire un présent à l’émir Nūr ad-Dīn afin de lui souhaiter meilleure santé. Ils espéraient par la même occasion appeler sur eux sa protection, voire être gratifiés de quelques privilèges. Nūr ad-Dīn était en effet réputé pour se montrer généreux avec les humbles, surtout ceux qui pratiquaient un pieux sunnisme. Les deux frères avaient pris sur eux de gérer la question du cadeau, mais, Soudanais d’origine et Égyptiens de culture, ils voulaient se doter de toutes les garanties pour ne commettre aucun impair.

Leur choix s’était porté sur le don d’un bol magique de guérison, dont on disait qu’il apportait la bonne santé à qui y buvait régulièrement eau, huile ou lait. La sélection des motifs et des symboles adaptés avait fait l’objet de nombreux et longs débats au sein de leur confrérie, et il était important de s’assurer que les formules de bénédiction soient adroites et de circonstance. L’émir avait connu de fréquents accès de maladie ces dernières années et il ne fallait pas non plus être trop obséquieux au cas où il décéderait et verrait lui succéder un ancien opposant. Flatter et glorifier sans trop s’engager était un exercice périlleux quand le pouvoir pouvait basculer d’un jour à l’autre. Les deux Soudanais l’avaient cruellement découvert quelques années plus tôt en Égypte.

Leur imam, un jeune érudit aux joues rebondies issu d’une vieille famille de Damas, était toujours partant pour partager plats et boissons, comme en attestaient ses formes replètes et son ventre généreux. En dépit de l’air débonnaire que cela lui donnait, il était très strict et peu enclin aux compromis, malgré une faconde dans le verbe qui pouvait suggérer l’illusion de la légèreté. Son engagement volontaire auprès des plus modestes alors qu’il aurait pu connaître une vie opulente de négociant lui valait une réputation flatteuse, dont il essayait de ne pas s’enorgueillir.

Il avait apporté quelques feuillets sur lesquels il avait tracé de son élégante écriture de lettré des formules dont il pensait qu’elles seraient adaptées. Le choix des sourates lui avait demandé un long travail et il venait d’en achever la lecture, commentée, à l’intention des deux frères. Razin hochait la tête avec conviction, bien que n’étant pas complètement au fait des subtilités évoquées. Il lui semblait néanmoins que c’était là l’attitude à présenter. Ce fut al-Misri, volubile comme à son habitude, qui prit la parole le premier.

« Nous te sommes redevables de cette parfaite sélection, qu’Allah te bénisse.

— Amine. Je suis heureux que la santé d’al Malik al-Ādil Mahmud^[Autre façon de désigner l’émir Nūr ad-Dīn.] vous préoccupe. Beaucoup se réjouissent, alors qu’il a amené paix et prospérité sur cette cité.

— A-t-on nouvelles récentes ? 

— Bien peu, et les prières ne seront pas inutiles pour en appeler à la bénédiction d’Allah. Les meilleurs médecins sont près de lui. Il aurait même déjà désigné son successeur si son séjour parmi nous devait s’achever.

— La rumeur disait vrai alors ? Le prince de Mossoul ?

— Oui, les principaux émirs sont venus. Il leur a fait jurer de soutenir Qutb ad-Dīn Mawdūd comme son seul et unique héritier. »

L’imam marqua une pause, tirant sur sa longue barbe, puis ajouta avec une allégresse un peu forcée.

« Mais avons-nous inquiétude à avoir lorsque tous les bons croyants s’associent pour en appeler à la Miséricorde d’Allah pour notre protecteur ? Il n’est pas encore dans la tombe qu’il faille se lamenter ! »

Al-Misri acquiesça avec enthousiasme. La présence d’un homme fort amenait paix et stabilité dans la ville depuis plusieurs années. Même si les Ifranjs savouraient ses ennuis récents et lançaient des assauts de façon régulière sur ses provinces, ils étaient chaque fois repoussés dans leurs territoires.

## Damas, porte de Bāb al-Ğābiya, fin d’après-midi du youm al khemis 30 dhu al-hijjah 553^[Jeudi 22 janvier 1159.]

Idris était descendu de sa monture afin de passer la porte, peu désireux d’attirer l’attention sur lui. Il avait quitté la caravane avec qui il avait cheminé depuis Baalbek au moment de pénétrer dans la Ghuta, l’oasis qui enserrait la ville. Il n’avait gardé avec lui que son valet, Ahmed, un jeune alexandrin qu’il formait à la pratique du commerce. Il n’avait plus foulé le sol de sa cité natale depuis bien longtemps et se réjouissait de faire à ses proches la surprise de son retour pour la nouvelle année.

Son inquiétude avait néanmoins enflé au fur et à mesure qu’il avançait dans le territoire de son enfance. En mer depuis des mois, il n’était pas préparé à la situation de siège qu’il avait découverte en revenant : des soldats en armes partout, des unités de Turkmènes amassés autour des baraquements de toiles sur le Mīdaān vert, près du Baradā, et la présence de contingents venus du nord en grand nombre. Il avait appris que des opérations militaires étaient en cours dans les zones septentrionales, les Latins se regroupant avec l’empereur byzantin afin de prendre à l’émir son joyau, la fière cité aleppine. Il s’étonnait d’autant plus de cet état de guerre si loin à l’est.

Il fut arrêté par un milicien à l’aspect malingre, qui voulait savoir s’il détenait des biens de valeur susceptibles d’intéresser la douane.

« Je n’ai rien d’autre que ce que je porte, ainsi que mon jeune valet. Je suis dimashqi et de retour parmi les miens. »

L’homme ne parut guère impressionné et le toisa sans aménité.

« D’où venez-vous ?

— Baalbek, où j’ai fait visite à des amis.

— Et avant cela ? Il n’y a guère de commerce en cette saison.

— Voilà parole bien avisée. C’est justement car nul ne prend la route en ces mois que je souhaite retrouver la douceur de ma cité natale, la belle *colline bien stable et dotée d’une source*[^sourateXXIII50]. »

Le garde se dérida à la mention de ce passage si apprécié des Damascènes et se fendit même d’un sourire disgracieux. Son visage revêche semblait réticent à l’idéer de présenter la moindre amabilité malgré ses tentatives. Il ajouta un hochement de tête, jaugeant Idris et son valet avant de s’adresser à son émir, lui demandant s’il pouvait laisser entrer le voyageur qui revenait chez lui. Occupé à échanger avec un caravanier ombrageux, l’officier acquiesça d’un geste sans trop y regarder.

Avant de pénétrer dans la cité, Idris hésita puis sourit au milicien devenu conciliant.

« À peine arrivé à Rabwé, j’ai commencé à croiser plus d’hommes de sabre et d’arc que de fellahs. Y a-t-il rumeur de guerre ? Doit-on craindre pour les siens ?

— Cela fait des semaines que l’émir garde le lit, dans la forteresse. Les chacals rôdent pour enfiler les bottes du moribond…

— *Moi contre mes frères ; mes frères et moi contre mon cousin ; mes cousins, mes frères et moi contre le monde*.

— Inch’allah ! Peut-être en saura-t-on plus à la grande prière… À nouvelle année, nouvelle moisson. »

Idris hocha le menton. Il serait sans faute le lendemain dans la mosquée cathédrale. S’il n’était pas particulièrement dévot, il n’ignorait pas que ce serait le lieu où apprendre les dernières nouvelles. Il avait eu tellement hâte de retrouver les siens, et s’étonnait d’être soudainement impatient de repartir en mer, loin des intrigues politiciennes qui rongeaient le cœur de sa ville bien aimée. Il disait souvent à ses amis qui se languissaient de lui lors de ses fréquentes et longues absences : *L’exil avec la richesse, c’est une patrie. La pauvreté chez soi, c’est un exil*. Il commençait à croire que l’exilé n’avait en outre pas à subir la cruelle désillusion de la mort lente de sa cité.

## Damas, quartier du Marché aux oiseaux, soirée du youm al talata 12 muharram 553^[Mardi 3 février 1159.]

Sa’d ad-Dīn ’Utman profitait de la soirée en lisant un de ses ouvrages d’hippiatrie. Il se targuait d’être un fin connaisseur des chevaux, qu’il élevait, comme son père et ses aïeux avant lui. Ses revenus tirés du commerce et de sa participation à différents dîwâns de l’administration damascène lui donnaient l’aisance suffisante pour se consacrer essentiellement à ce passe-temps d’importance.

Au fil des ans, il avait amassé une bibliothèque digne d’éloges, qu’il ne dévoilait qu’à ses proches ou à ceux qui lui étaient recommandés. Cela lui permettait de détenir au sein même de sa riche demeure quelques liens avec sa dispendieuse écurie installée en périphérie de la ville, sur le chemin de Ğawbar. Fraîchement marié, il espérait avoir rapidement un fils à qui communiquer l’importance de cette tradition familiale.

Il venait de se faire servir un peu de leben et des pâtisseries aux dattes, et se lavait les mains avant sa collation quand son valet entra et lui annonça l’arrivée d’un visiteur. À ce moment de la nuit, ce ne pouvait être qu’un importun ou un porteur de mauvaises nouvelles. Lorsqu’il apprit que c’était son jeune cousin Hakim, il ne put réprimer un frisson. Son parent aurait dû se trouver loin au nord, sur la route de Harrān. Il fit signe de l’introduire sans perdre de temps.

La tenue débraillée et fatiguée, sale de poussière et de boue, humide et sentant le crottin et le feu de bois augurait du pire, quoique plus angoissant encore était le regard de chien battu. Hakim bafouilla en guise de salut, inquiétant d’autant plus Sa’d ad-Dīn.

« Que s’est-il passé ? Tu n’as pu délivrer les lettres si vite !

— Nous avons été pris, mon cousin. Nous avions franchi Alep sans souci, mais une patrouille près de Manbiğ nous a reconnus comme *dimashqi*, a refusé le passage et voulait nous saisir. Allah soit loué que vous m’ayez donné un de vos rapides coursiers. Aucun de ces maudits Turkmènes n’a pu me rattraper.

— Et Ibn Magzū ? Est-il avec toi ?

— Il n’est pas si bon cavalier, je crains qu’ils ne l’aient pris.

— Au moins n’est-il pas de la famille. Espérons qu’il saura tenir sa langue !

— C’est qu’il avait les lettres cachées dans ses chaussures, mon cousin… »

Sa’d ad-Dīn éprouva un léger vertige, s’affaissant dans les moelleux coussins de son siège. Il se racla la gorge, cherchant à retrouver ses esprits.

« A-t-il la mienne aussi ? Ou juste celles des autres ?

— Le gouverneur ’Izz ad-Dīn avait insisté que son gulham garde les missives, je n’avais rien sur moi. »

Sa’d ad-Dīn secoua la tête. Son nom figurait sur le document qu’il avait écrit à l’intention de Nusrat ad-Dīn, le seigneur de Harrān et frère de l’émir. Comme ses complices, il l’incitait à venir prendre le pouvoir à Damas contre les souhaits de Nur ad-Dīn dès que celui-ci décéderait. Mais le vieux soldat semblait se remettre de ses dernières fièvres et il n’apprécierait certainement pas ce nouvel appel à trahison. Même s’il n’était pas sanguinaire comme son père, il n’était pas homme à traiter à la légère les tentatives de sédition.

« J’ai galopé comme le vent. Nous avons un jour ou deux avant que la nouvelle parvienne ici, mon cousin. Dans le cas où ils trouvent les lettres…

— De cela je ne doute pas un instant. Et si je me trompe, il est de moindre importance que je parte à la mauvaise saison pour rien. Qui sait ce que tu viens de m’apprendre ?

— Personne. J’ai voyagé tel un fantôme depuis des jours, gardant le silence avec les étrangers, évitant patrouilles et cités, dormant quelques heures à peine chaque nuit, poussant ta magnifique monture à la limite de l’épuisement à chaque étape.

— De cela je ne te remercierai jamais assez. Va prendre un peu de repos, fais-toi donner un bain. Pendant ce temps, je vais lancer des ordres, nous partons demain pour al-Misr. Je prétexterai des ennuis avec des fournisseurs. La main de l’émir ne porte pas là-bas. »

Hakim approuva, l’air las. Il allait se retirer puis, hésitant à vexer son cousin et protecteur, il fit une moue inquiète avant de livrer le fond de sa pensée.

« Ne devrions-nous pas prévenir le gouverneur ou l’intendant de la Chancellerie ?

— Ce sont des proches de Nūr ad-Dīn, il leur pardonnera. Je n’ai pas tant d’appuis, cela me vaudra d’avoir la tête coupée ! Et à toi aussi si on apprend que tu les as visités juste avant leur fuite. »

À voir l’air dépité de son cousin, Sa’d ad-Dīn eut un pincement au cœur. Il savait que sa décision pouvait paraître cruelle, mais il ne pouvait risquer d’être pris. Les hommes de Nūr ad-Dīn avaient une chance d’en réchapper en faisant jouer leurs relations. Pour sa part, si on dévoilait ses amitiés shi’ites, il était certain d’y laisser la vie. Il avait espéré attirer un prince plus tolérant, souhaitant ouvrir ainsi des ponts vers l’Égypte, mais son rêve avait vécu. Il ne lui restait plus qu’à fuir pour sauver ce qui pouvait encore l’être. Il jeta un coup d’œil à sa merveilleuse bibliothèque et poussa un long soupir de dépit à l’idée de la perdre.

## Damas, citadelle, matin du youm al khemis 19 safar 554^[Jeudi 12 mars 1159.]

Soldat d’élite formé depuis sa plus tendre enfance, Sawwar avançait avec la démarche chaloupée des familiers de la selle. Comme les marins, il trouvait le sol trop dur pour le fouler de ses pieds. Il aimait mieux le soulever en nuages de poussière, un cheval entre ses cuisses. Porté aux excès, il fêtait depuis plusieurs jours le possible départ prochain en campagne. C’était pour lui une tradition que de se délester de tout ce qu’il possédait avant de prendre le sabre et l’arc. Il voyageait léger dans l’espoir d’aller loin. Et là il venait d’apprendre une nouvelle qui lui mettait le cœur en joie : il allait découvrir al-Qahira[^alqahira], les sables du désert percés du fleuve majestueux. Il pourrait goûter aux courtisanes noires comme l’ébène dont on disait l’étreinte subtile et enfiévrée. Son sang bouillonnait littéralement, encore embrumé des vapeurs alcoolisées de la veille.

Il se figea au moment de frapper à la porte du cabinet où les officiers se retrouvaient pour préparer les opérations. Il patienta un instant puis pénétra, le visage soudain emprunt de sérieux et de solennité. La pièce, sobrement garnie de coffres, de sièges bas et de petites tables, voyait son mobilier évoluer selon les besoins et les envies des présents. Pour le moment, seul son émir Yalim al-Buzzjani était occupé à lire des courriers, sirotant une boisson épicée dont les vapeurs venaient danser jusqu’aux narines du soldat. Il s’approcha et salua de sa voix rauque.

L’officier lui accorda un sourire sans le regarder, finit sa missive puis répondit enfin au bonjour. Comme toujours, il était environné de papiers divers, livres et feuillets. Sawwar ne comprenait pas l’intérêt qu’il y avait à s’épuiser les yeux dans de telles fariboles de citadin décadent. Mais il conservait la plus haute estime pour son maître, dont il savait la science et la sagesse de grande importance. En outre, ils avaient développé une relative amitié au fil des ans, née de la camaraderie à porter le fer et le feu en selle, ou à subir des assauts ensemble.

« Tu pourrais au moins te rendre aux bains avant de te présenter à moi. Les puanteurs de tes excès te suivent comme des mouches. J’espère qu’il en est fini de ceux-ci jusqu’à l’année prochaine.

— Je suis tel le faucon dont on ne dénoue pas le lacet… Avez-vous appris quand nous partons ?

— Cela ne devrait plus tarder, l’émir a donné ses ordres hier. Tu le saurais si tu avais été présent à l’audience plénière. Demain la cité priera pour la bonne fortune du chambellan al-Muwallad al-Mustašīdī pour un départ un ou deux jours après. Temps pour nous de préparer la caravane. »

Sawwar opina avec enthousiasme. La traversée serait certainement tranquille. Les Ifranjs étant pour la plupart occupés loin au nord, aucune troupe n’aurait la puissance de s’opposer à leur avancée. Il le regrettait presque, n’aimant rien tant que le son clair du choc de l’acier ou le bruit sourd des flèches touchant au but.

« Garde aussi en tête que nous devons estimer la force des armées du vizir, voir comment ses marches sont tenues et si ses hommes sont de qualité.

— J’ai commencé à me faire mon idée en montrant à certains de ces Misri mes coins préférés ! »

Il marqua une pause, puis reprit, de façon moins goguenarde.

« A-t-on projet de s’attaquer à eux ?

— On ne m’en a rien dit, un tel secret serait forcément gardé bien serré. Mais cela m’étonnerait. L’émir a déjà fort à faire ici. Il doit surtout espérer que nos frères en religion, même s’ils ne reconnaissent pas le bon Calife, auront la force de nous venir en aide si besoin, ou, au pire, la capacité de résister aux assauts des infidèles, ou aux tentations de nous nuire.

— Du genre soutenir le mauvais prétendant à l’héritage ?

— Par exemple. Même si al-Malik semble désormais aussi vaillant qu’un jeune étalon. »

Sawwar acquiesça, mais il n’était qu’à demi convaincu du retour en santé du puissant dirigeant. Il était certain que ce dernier s’était amolli au fil des ans, à loger entre quatre murs, sous un toit de pierre. L’homme était fait pour vivre sous la toile, son arc à portée de main et sa monture attachée à son arbre. Puis il disparaissait emporté comme le sable dans la tempête. Il fallait être bien infantile pour croire que l’on pouvait tenir quoi que ce soit en cette vie. ❧

## Notes

Si nous sommes plus ou moins familiers des tergiversations du pouvoir chrétien en général et latin en particulier pendant les croisades, j’ai constaté que bon nombre de personnes imaginent encore les puissances musulmanes comme un bloc uni. Il n’est pourtant que de parcourir les sources pour voir combien les communautés et cultures étaient fragmentées, malgré une foi commune, quoique très divisée en de nombreux courants et sectes, dont le shi’isme et le sunnisme ne constituent que les ensembles les plus généraux. À cela s’ajoutaient des ambitions personnelles des princes, qui n’hésitaient pas à nouer et trahir des alliances au fil des ans, dans le but d’amasser toujours plus d’influence et de pouvoir. Ils n’étaient en somme pas plus unis ou solidaires que les barons latins. Au XIIe siècle, cela n’arrivera que brièvement, quand Saladin réussira à tenir dans sa main tous les territoires durant un temps. Mais ceci est une autre histoire…

Le titre de ce Qit’a est tiré d’une expression qui dit que *Celui qui espère après les chaussures d’un mort ira longtemps pieds nus*. Cruelle prophétie pour décourager les intrigants qui patientent auprès d’un mourant dans l’espoir de s’arroger son héritage. L’idée de ce texte, de parler de façon périphérique des tentatives de soulèvement ou de trahison autour de Nūr ad-Dīn, est née en découvrant les bols magiques du type que Razin et Al-misri veulent offrir à l’émir. De magnifiques exemplaires contemporains de ce Qit’a en avaient été présentés dans l’exposition *L’orient de Saladin. L’art des ayyoubides*, en particulier la pièce numéro 223 du catalogue.


## Références

Alaoui Brahim (commissaire), *L’orient de Saladin. L’art des ayyoubides*, catalogue de l’exposition présentée à l’institut du monde arabe, Paris : Institut du monde arabe / Gallimard, 2001.

Élisséef, Nikita, *La description de Damas d’Ibn ’Asâkir*, Institut Français de Damas, Damas : 1959.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.
