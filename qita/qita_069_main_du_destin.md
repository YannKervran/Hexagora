---
date : 2017-07-15
abstract : 1136-1149. Esclave eunuque, le jeune Yarankash va déclencher une tempête politique en Syrie, sans se douter des conséquences sur sa propre vie. Arrivera-t-il à faire pencher la balance du destin en sa faveur ?
characters : Yarankash, Usamah ibn Munqidh, Anur, Zankī, Bertaud le Sueur
---

# La main du destin

## Mossoul, citadelle des bords du Tigre, matin du youm el itnine 27 ramadan 530^[Lundi 29 juin 1136.]

Du bout des doigts, le jeune garçon poussait une araignée dans la lumière poussiéreuse filtrant sous la porte. Bouleversé par les dernières semaines, il n’osait plus guère bouger, se laissant guider d’un endroit à l’autre sans un mot, sans un geste. Il n’avait même pas réagi à son installation récente dans une minuscule salle avec une paillasse et des couvertures. Il s’était contenté de se pelotonner au chaud, profitant de ce répit sans penser au lendemain. Les repas, plus consistants, lui avaient fait le plus grand bien. Demeurait malgré tout l’angoisse de ne rien comprendre à ce qui lui arrivait.

Il avait été séparé des siens après l’attaque sur la colonne de pèlerins, tandis qu’ils longeaient la côte anatolienne. Ses parents n’avaient pas les moyens de payer le voyage en bateau et avaient rejoint un vaste groupe de toutes les nationalités peu après leur traversée du Bosphore. Cela n’avait pas effrayé les Turcmènes qui s’étaient abattus sur eux, capturant tous ceux qui n’opposaient pas de résistance, exécutant ceux qui cherchaient à se défendre. Depuis lors, Garin avait été mené de place en place, encordé à d’autres miséreux comme lui. Il avait tenté au début de discuter un peu entre prisonniers, mais les cris, les coups et les insultes l’en avaient rapidement dissuadé. Depuis, il ne faisait qu’attendre, sans intention, sans espoir, sans vie.

De temps en temps, il entendait les voix rudes des soldats à travers la porte, lançant des ordres, plaisantant ou s’invectivant. Des pas lourds ou traînants, parsemés de cliquetis et de tintements métalliques résonnaient parfois de l’autre côté de la solide huisserie. Régulièrement, un serviteur venait lui porter un broc, du pain plat et des légumes, tout en échangeant son seau d’aisance pour un propre. Mais personne ne le regardait jamais dans les yeux ni n’esquissait l’envie de parler avec lui.

Ce jour-là, des voix autoritaires se firent entendre : on s’approchait de sa porte. Les verrous jouèrent dans un claquement sec et le vaste panneau céda la place à une forte lumière noyant la galerie. Instinctivement, Garin recula, levant un bras en dérisoire protection. Dans l’encadrement se tenaient deux hommes discutant de façon animée dans une langue qu’il ne comprenait pas.

« Je peux le faire laver, si vous souhaitez avoir meilleure idée. »

Le premier était de petite taille, le faciès rond bien nourri barré d’une imposante moustache camouflant ses lèvres. Les yeux disparaissaient dans les replis graisseux de son visage avenant, dont la faconde naturelle était amplifiée par l’attitude toute servile qu’il adoptait, légèrement incliné. Sa tenue, vastes vêtements voilant sa physionomie pour n’en laisser percevoir que l’empâtement, attestait de son aisance tandis que l’impressionnant turban trahissait son goût pour les démonstrations d’opulence exubérante.

« Il sera bien temps de le récurer s’il convient. Est-ce là genre qui plaît à l’atabeg ? »

Le second personnage était assez grand, dissimulé lui aussi sous de multiples couches de vêtements aux larges drapés dont les manches touchaient quasiment le sol. Son chef était orné d’un turban moins extravagant, agrémenté d’une belle broche, tandis que son visage apprêté attestait de l’homme coquet. Sa barbe noire, mangée de gris par endroit, était tenue courte et proprement délimitée par le rasoir. Ses yeux bruns plissés donnaient une allure riante à son regard, bien qu’il s’exprimât avec autorité. Sur un de ses gestes, plusieurs valets s’engouffrèrent et empoignèrent Garin, présentant l’enfant blond ainsi qu’une bête à la foire. Il l’examina en prenant son temps.

« Il a beau visage et yeux sombres, ainsi qu’il sied. Ses cheveux couleur de blé le rendent exotique. Ses mains délicates sauront verser le vin avec adresse j’espère. »

D’un geste souple, il fit signe aux hommes de reposer le gamin à terre puis tourna la tête vers le négociant.

« Faites donc établir le contrat, il sera propriété du calife ar-Rachid billah Abu Djafar Mansur, et offert à l’atabeg Zankī, s’il l’accepte. »

L’homme à l’imposante moustache hocha la tête, satisfait.

« Ce qui plaît beaucoup à l’atabeg, c’est de conserver la fraîcheur de ces poulains en les faisant eunuques. Votre geste sera d’autant plus apprécié.

— Très bien, fais-le donc opérer par un des chirurgiens de la ville. Tu me feras porter la note.

— C’est que les meilleurs praticiens n’aiment guère à faire cela…

— Avec tous les Juifs en cette cité, il ne se trouve aucun médecin pouvant se charger de cela ?

— Ils sont toujours à chicaner sur leurs honoraires !

— Le prix ne m’intéresse pas. Que cela soit fait, promptement, mais proprement. Je n’achète pas un ghulam de vingt dinars pour le gâcher aux mains d’un incompétent. Ne regarde pas à la dépense. »

Le gros négociant acquiesça avec enthousiasme, escomptant par avance la part qui lui reviendrait dans cette opération. Ce n’était pas tous les jours que le vizir du calife offrait un si bel esclave à l’homme fort des bords du Tigre, Zankī. Il se tourna vers le garçon qui les dévisageait, l’air hagard, et demanda, dans une langue approximative et gutturale.

« Quoi ton nom ? »

Surpris autant par la question et la prononciation que par le fait qu’on s’adressa à lui, le gamin demeura muet, ce qui agaça un peu son interlocuteur qui insista, lâchant des regards gênés en direction du vizir. Ce ne fut qu’après un petit moment de stupeur qu’un son parvint à sortir de la gorge de l’enfant.

« Garin de Cachan… »

Le négociant fronça les sourcils, toujours mal à l’aise avec ces mots celtes[^ifranj]. Puis il se tourna vers un de ses domestiques, porteur de documents.

« Note son nom : Yarankash »

## Édesse, grande salle de la citadelle, soir du youm al khemis 1 jadjab 539^[Jeudi 28 décembre 1144.]

Des lampes et des braséros disposés en abondance combattaient les ténèbres et le froid. Les visiteurs venus de l’extérieur ne tardaient pas à transpirer sous leurs épais qab’a[^qaba] de laine doublée et de feutre. Un peu partout s’amoncelaient tissus et tenues d’apparat, pièces d’orfèvrerie, armes et céramiques décorées. Jouissant de ce chaos, un homme était assis sur un diwan agrémenté de coussin et de fourrures, sous un dais préservant la chaleur.

La barbe largement blanche malgré quelques poils noirs, il jouait machinalement avec une de ses longues tresses, les yeux perdus dans le vague. Son visage aux joues creusées était veiné de rides profondes sillonnant la peau parcheminée. Sous son sharbush[^sharbush], dont le fronton scintillait à la lueur des lampes, ses paupières fatiguées abritaient un regard éteint, presque absent. Jetés pêle-mêle sur ses épaules, des étoffes de prix le transformaient en statue, image de dévotion devant laquelle les émirs qui entraient se prosternaient à tour de rôle. Il se nommait Imād ad-Dīn Zankī, atabeg de Mossoul et d’Alep et il venait de conquérir Édesse.

Il paraissait indifférent aux hommes qui se succédaient devant lui, ne gratifiant que rarement les plus méritants d’un grognement ou d’un hochement de tête à peine perceptible. Les prisonniers s’entassaient, les cadeaux s’amoncelaient sans qu’il ne semble porter le moindre intérêt à ce qui se passait. Lorsque le défilé commença à se tarir, il leva la main péniblement, chassa d’un geste les derniers prétendants. Il ne restait face à lui que ses plus fidèles émirs, installés en un approximatif demi-cercle.

Yarankash était debout, en retrait du podium, occupé à recharger d’encens et de braises les fauves de bronze entourant le prince. Il conservait aussi à portée coupes, bouteilles et pichets. L’atabeg n’aimait guère attendre lorsqu’il avait soif. Il arrosait de libations ses nombreuses victoires et recherchait dans le vin l’oubli de ses échecs. Ses adversaires décriaient à l’envi son appétence pour une boisson interdite, mais il s’en moquait. Il refusait simplement qu’on évoque la question devant lui. Aucun de ses émirs n’aurait eu le courage de parler des récipients autour de lui ou du verre émaillé qu’il tenait. Le vin était un sujet tabou dans la cour de Zankī, chacun s’efforçant de faire comme s’il n’existait pas.

Il tendit son gobelet et le jeune eunuque s’empressa d’y verser le rubicond breuvage, patientant ensuite, la cruche à la main, si le prince voulait être resservi. Zankī dégusta sa boisson, sirotant les poils de ses moustaches. Sans bouger la tête, il observait ses officiers. Il en appréciait les compétences et s’en méfiait tout autant, sachant pertinemment que c’était sûrement d’eux que viendrait la trahison au moment où il s’y attendrait le moins. Sa voix chuchotante mit fin à un long moment de silence gêné.

« Les Ifranjs[^ifranj] sont-ils tous morts ?

— Ceux qui ont été découverts les armes à la main ont la tête au bout d’un piquet. Les autres attendent leur sort. »

L’atabeg approuva et prit encore une gorgée. Certains de ses émirs partageaient son goût pour le vin et ne se privaient pas de l’imiter en silence.

« Le religieux syriaque, Bar Šumna^[Métropolite Jacobite.], accordons-lui donc ce qu’il m’a demandé. Que vos hommes fassent en sorte que les biens réclamés par son culte lui soient rendus, en totalité.

— Le vieil imam arménien^[Il est fait ici allusion à l’archevêque arménien d’Édesse, Jean.] sera jaloux d’un tel privilège…

— Il n’a qu’à se montrer plus reconnaissant. Je leur ai déjà rendu leurs églises ! *Un chien reconnaissant vaut mieux qu’un homme ingrat.* »

Le silence se fit tandis que des serviteurs inquiets apportaient un plat de mouton et de riz, accompagné de pain. Les odeurs épicées se mêlaient aux relents entêtants des fumées. Les émirs se rapprochèrent et s’installèrent confortablement. À partir de là, l’ambiance fut plus décontractée. Même l’atabeg souriait lorsque le vieil Zayn ad-Dīn racontait une de ses anecdotes. Soldat émérite et général au sens tactique affiné, il conservait malgré les années un caractère espiègle qui en faisait un compagnon apprécié. Peu enclin à l’excès, il s’animait quand il narrait les facéties des guerriers qu’il avait eus sous son commandement, ou qu’il avait affrontés. Lorsqu’il acheva une de ses fameuses histoires, Zankī le remercia et fit un geste à l’intention de Yarankash. Voyant que celui-ci avait les yeux au sol, il en fut agacé et lui lança de la nourriture, un os à demi rongé.

Le jeune garçon sursauta, le cœur battant soudainement à tout rompre. Il savait que la correction viendrait tôt ou tard. L’atabeg détestait que ses esclaves ne se montrent pas empressés à lui obéir, il estimait que cela n’inspirait pas le respect à ses hommes. Chaque manquement était donc impitoyablement suivi de coups, de punitions et de vexations dont certaines entraînaient parfois la mort. Yarankash avait vu ainsi plusieurs de ses camarades finir mutilés ou exécutés pour avoir déplu à leur maître. Zankī lui lança quelques insultes et l’enjoignit à aller quérir ce qu’il lui avait fait mettre de côté un peu plus tôt. Contrit, Yarankash courut chercher la riche robe brodée de tiraz[^tiraz] prestigieux. Encore tremblant, sous le regard d’aigle de l’atabeg, il s’inclina en la déposant sur les genoux du vieil homme tandis que Zankī prenait la parole de façon plus officielle.

« Que tous soient témoins ici que je te nomme, Abū al-Ḥassan Alī ben Begtegin[^zaynaldin], isfahsalār de la cité d’Urfa[^urfa]. Tu y auras pour tâche de tenir la forteresse et de m’y représenter. Pour le reste, laisse donc les Syriaques et les Arméniens se débrouiller entre eux, tant qu’ils paient ce qu’ils doivent et respectent mon autorité. N’hésite pas à crucifier les Celtes qui auraient la mauvaise idée de traîner à l’entour. »

Il sourit en voyant le qab’a élimée du vieux soldat, dont la piété était connue, qui tranchait avec le magnifique vêtement qui venait de lui être remis.

« Tu n’auras pas à porter cette tenue quand tu mèneras les hommes. Comment te reconnaîtraient-ils si tu portais beau comme un bureaucrate baghdadi ? »

Le général sourit et se leva avant de s’incliner cérémonieusement devant son prince. En le voyant faire, Yarankash se dit qu’il aurait aimé être offert à l’émir en même temps que la robe. Il était épuisé de vivre sans cesse dans la crainte des caprices, des exigences et des sautes d’humeur de son maître.

## Qal’at Ja’bar, camp de siège, nuit du youm al sebt 5 rabi’ al-thani 541^[Samedi 14 septembre 1146.]

Les cordages de tension de la tente répondaient en grinçant au murmure des pins parasols. Les lampes suspendues à l’armature se balançaient mollement, projetant des ombres mouvantes sur les murs de toile. Se déplaçant sans bruit sur l’épaisse litière de tapis, de nattes et de fourrures, Yarankash s’employait à ranger les reliefs du repas et de la soirée. Zankī avait célébré avec ses émirs l’avancée du siège, dont il ne doutait pas qu’il serait bientôt terminé. Il s’enorgueillissait d’avoir floué le seigneur du lieu en le délestant d’une coquette somme par la ruse. Il avait proposé de partir moyennant le versement d’un fort montant. L’argent remis, il avait décidé de passer son agacement sur les messagers et avait empoché le tribut sans desserrer un tant soit peu son étreinte sur la forteresse.

Cela faisait plusieurs semaines qu’ils étaient installés sans vraiment combattre. Après avoir pillé l’opulente bourgade et saisi les biens de ceux qui vivaient là, Zankī s’était contenté d’instaurer un blocus total. Il profitait de la situation privilégiée du lieu dans la vallée de l’Euphrate pour percevoir les péages marchands qui enrichissaient jusque là les seigneurs uqaylides. Comme à son habitude lorsqu’il s’ennuyait, il avait tendance à boire copieusement chaque soir, sombrant dans une ivresse colérique et capricieuse qui éloignait de lui tous les hommes prudents.

Yarankash rassemblait les bouteilles et pichets aux abords de la couche de l’atabeg. Celui-ci était assoupi, affalé sur le magnifique matelas, noyé sous les coussins et de luxueuses couvertures. Le jeune garçon huma le vin dans une des céramiques et en apprécia les senteurs. Zankī n’aimait rien tant que les boissons les plus capiteuses et son esclave en avait pris le goût. Il en avala les restes, découvrant un breuvage charnu, rond et plein de finesse qui lui réjouit le palais. Après un rapide regard vers son maître, perdu dans les vapeurs alcoolisées, il se mit en quête du pichet pour s’en verser une coupe.

Il en était à son second verre, agrémenté de böreks[^borek] au fromage lorsqu’une voix le fit sursauter.

« Mon vin est assez bon pour toi, sale chien ? »

Zankī avait la tête relevée, les yeux entrouverts, et le fixait avec colère. Un fin filet de salive glissait des lèvres tandis que l’atabeg s’emportait, la voix rauque.

« Je vais te faire fouetter pour ça, maudit traître. »

Il se leva et, titubant, empoigna un couteau de service. Il contourna les tables basses du mezzé[^mezze] en s’approchant de Yarankash. Pétrifié, le jeune eunuque attendait, la coupe à la main. Zankī brandit la petite lame en souriant de façon sadique.

« Je vais t’arracher la langue, sale voleur ! »

Il lança la main pour empoigner Yarankash, mais celui-ci recula instinctivement. L’atabeg, emporté par son élan et déséquilibré par son ivresse se prit les jambes dans les plateaux et les coupelles du repas et s’étala de tout son long sur les coussins des invités en grognant de stupeur. Plusieurs esclaves s’approchèrent alors discrètement, inquiets et curieux à la fois. Ils ne tenaient pas à voir l’ire de Zankī s’exercer à leur encontre.

Zankī ne bougeait plus, marmonnant, la tête plongée dans les étoffes. Yarankash se pencha, indécis, espérant arriver à installer son maître dans une position plus confortable sans recevoir un mauvais coup. Lorsqu’il le retourna, il eut un mouvement de recul : le durrâ’a[^durraa] de soie fine s’ornait d’une large tache sombre au niveau du ventre. L’atabeg gémissait doucement.

« Tu m’as tué, sale petit porc. Je vais t’écorcher, te crucifier… »

Il s’était empalé sur son couteau en tombant. Désemparé, Yarankash installa son maître comme il le put sur les coussins. Voyant que quelque chose n’était pas normal, les autres serviteurs s’étaient approchés et contemplaient la scène, abasourdis. Le jeune homme, affolé, ne savait que faire. Il était persuadé que la punition serait terrible, le lendemain. Jamais le vieux prince ne le laisserait vivant.

« Il faut appeler le chirurgien sans tarder, lança un des plus âgés.

— Il va être d’une humeur massacrante, l’arrêta un autre.

— On ne va pas le laisser mourir, quand même ! »

Écoutant à peine ce qui se disait, sans un mot, Yarankash ramassa le petit couteau qui avait poignardé l’homme fort de la région. Un simple outil de service, à peine tranchant. Il contempla l’auréole grandissante, entendit les imprécations de plus en plus faibles. Il regarda ses compagnons, percevant vaguement leur discussion angoissée sans rien y comprendre vraiment. Puis il plongea la lame dans le ventre de l’atabeg, obtenant un râle plus soutenu.

Les autres autour de lui en demeurèrent sidérés. Aucun n’osa plus parler, médusés qu’ils étaient tous de ce geste sacrilège. Il posa ensuite le manche dans la main de son voisin et l’invita, d’un mouvement de la tête, à faire comme lui. En quelques instants, chacun avait porté au moins un coup au puissant seigneur de Mossoul et d’Alep qui se tut en un ultime anathème.

## Damas, geôles de la citadelle, matin du youm al sebt 29 jadjab 541^[Samedi 4 janvier 1147.]

La silhouette élancée qui avançait à la suite du soldat porteur des clefs semblait incongrue dans cette coursive poussiéreuse et sombre. La qualité des étoffes y disputait avec l’ampleur faste de la coupe et la richesse exubérante des bandes à tiraz. L’homme, d’une cinquantaine d’année, paraissait pourtant à l’aise dans cet environnement austère. Une barbe soignée ornait le bas d’un visage fin sillonné de rides nées du soleil autant que de l’âge. Les cheveux très courts ne dépassaient que peu de l’imposant turban, porté haut et agrémenté d’un onéreux bijou. Nul ne pouvait douter en voyant Usamah ibn Munqidh qu’il avait affaire à un personnage de premier plan.

Ils quittèrent la zone des cellules pour rejoindre des bâtiments moins lugubres. Abandonnant son escorte, Usamah parvint dans un petit cabinet dominant les cours d’exercice. Là, emmitouflé dans des couvertures, le seigneur de la ville, Mu‘īn ad-dīn Anur patientait. Il avait officiellement appelé à des manœuvres malgré le froid et le ciel peu clément, et les surveillait depuis ce poste de vigie. En fait il comptait surtout échanger discrètement avec l’habile diplomate et conseiller qui venait de le rejoindre. Il l’invita à s’asseoir face à lui sur un modeste tabouret. Usamah mit un peu de temps avant de s’installer confortablement, organisant les plis nombreux de sa tenue complexe.

« Je n’ai aucun doute que celui qui se vantait d’avoir occis l’atabeg soit un menteur. En se faisant passer pour Yarankash, il espère sûrement récompense.

— La peste soit de ce fou ! tonna Anur. Je ne solde pas les assassins, quand bien même ils me débarrassent d’un dangereux adversaire. »

Il demeura un moment à ruminer, les yeux errant par-delà les décors ajourés sur les corps des hommes à l’exercice. Il s’accorda un sourire de satisfaction en voyant quelques beaux échanges de coups. Puis il revint vers son conseiller, resté coi, perdu dans ses propres pensées, certainement occupé à tramer un stratagème ou à composer une poésie.

« La situation est déjà fort complexe ! Maintenant qu’il a hurlé partout son nom, je ne peux l’ignorer…

— Mon père, qu’Allah l’ait en Sa sainte garde, était un homme de foi. Il aimait à dire “Qui a le pouvoir doit plus qu’un autre pardonner”.

— Ton père, homme sage s’il en fut, avait renoncé à régner pour acquérir semblable vertu.

— *Quand un chien vous aide à passer le fleuve, ne demandez pas s’il a la gale* ajoutait souvent Jum’a[^juma] » précisa ironiquement Usamah.

La mort du puissant prince du nord bouleversait complètement les forces en présence et, comme à chaque disparition d’un personnage important, le chaos qui allait s’ensuivre offrait toutes les perspectives, bonnes ou mauvaises. Les compétences des dirigeants étaient avant tout sollicitées pour profiter à leur avantage de ces opportunités. La présence d’un homme qui se faisait passer pour l’assassin dans la cité de Damas n’était pas un sujet à prendre à la légère. Anur soupira.

« Je ne sais si les fils de l’atabeg s’accorderont, j’en ai grand doute. Mais qui que soit le gagnant, il lui faudra du temps. Il va falloir choisir avec grand soin notre champion.

— Le jeune Mahmud[^nuraldin] est un soldat de valeur, je peux en attester. Il est bien placé à Alep, envoyons-y ce Yarankash en gage.

— Tu as dit toi-même que c’était un imposteur !

— Si nous faisons savoir à tous que c’est l’assassin réfugié en nos murs que nous renvoyons à ses maîtres pour recevoir son châtiment, qui osera nous traiter de menteurs ? Que ce soit vraiment celui qui a occis l’atabeg importe peu, tant que la foule le croit.

— Et si le vrai Yarankash se manifestait ?

— Après le sort réservé à celui qui vient de se déclarer chez nous, je n’encrois nullement qu’il y aura pleinté de candidats. Il est sûrement déjà en sauveté chez les Ifranjs. J’essaierai d’en apprendre plus à ce sujet lors de mon prochain voyage à al-Quds[^alquds]. »

Une moue déforma les traits fatigués du seigneur de Damas, comme un acquiescement à contrecœur. La mort de Zankī lui offrait l’opportunité d’affirmer son pouvoir sans s’opposer ouvertement au royaume latin de Jérusalem. Pour autant, il ne souhaitait pas l’effondrement d’Alep, sachant pertinemment que Damas serait la cité suivante dans l’ordre des conquêtes latines.

« Tu dis que le jeune fils de Zankī est un vaillant soldat ? Peut-être devrais-je le soutenir par une alliance formelle ? Une de mes filles est justement bonne à marier. Les messagers acheminant le prisonnier pourraient délivrer un message d’avenir, de rapprochement entre nos cités. »

Usamah ibn Munqidh marqua son enthousiasme par un sourire appuyé. Il avait longtemps combattu aux côtés de l’atabeg Zankī et toute sa jeunesse s’était déroulée sur les rives de l’Oronte, à affronter les potentats locaux, francs et musulmans, pour le compte de sa famille à Shayzar. Ce qu’il craignait plus encore que la chute d’Alep en elle-même, c’était l’idée que le seigneur du défunt comté d’Édesse, Joscelin, puisse s’y installer après son échec récent à reprendre sa capitale. Une dizaine d’années plus tôt, une vaste campagne initiée par le basileus byzantin avait envisagé la conquête de la cité alépine, projet heureusement sabordé par les dissensions entre les francs d’Antioche et d’Édesse.

## Naplouse, cour des bourgeois, fin de matinée du jeudi 25 août 1149

Le vicomte sortit le premier, ventre en avant, suivi de peu par son appendice nasal. Les mains dans le dos, il avait comme à son habitude un œil fermé et l’autre plissé, résultat d’une mauvaise vision qui l’incitait à fixer ses interlocuteurs pour tenter de les reconnaître. On moquait parfois sa tendance à la bonhomie, sans toutefois manquer de respect à un dirigeant modéré. Il accomplissait son devoir avec sérieux et régularité. On vantait même sa relative honnêteté qui poussait à ne jamais lui offrir de pot-de-vin d’un montant mesquin. Derrière lui venait la cohorte des jurés de la Cour des Bourgeois, florilège de tenues dispendieuses ou graves, assortie de visages aussi austères que cérémonieux. Les suivaient quelques plaignants et un prévenu, encadré de deux sergents. Le jeune homme regardait ses pieds, ne laissant voir que sa chevelure blonde. Il était vêtu misérablement, chaussé de savates élimés, la cotte serrée d’un cordon de ficelle.

Il fut mené jusqu’à l’atelier du forgeron le plus proche, où attendait l’assemblée. Le vicomte Ulric monta sur un petit escabeau installé là à son attention et déclama d’une voix forte à l’intention de tous.

« Ainsi qu’il vient de l’être indiqué en l’audience plénière de la Cour, le ci-devant… Garin de Cachan, sergent reconnu félon à son maître Bertaud le Normand, aura le poing percé d’un fer porté au rouge en marque d’infamie. »

Voyant que le forgeron allait prendre le poinçon qu’il avait mis à chauffer, Ulric fit signe aux deux soldats de tenir la main du jeune homme sur le billot. Sur le devant du cercle qui assistait à la peine se trouvait le plaignant, gros homme aux chairs molles, un exploitant agricole chicaneur qui fatiguait la Cour avec ses requêtes incessantes. Il était pour une fois parfaitement dans son droit, ayant surpris un de ses domestiques à s’enfuir nuitamment. À voir la vêture du pauvre garçon, Ulric comprenait fort bien son envie de liberté. Mauvais maîtres font mauvais valets disait-on. C’était on ne plus vrai de Bertaud, que personne n’appelait le Normand, pour peu qu’il fût absent, mais « Le Sueur », cette denrée qu’il aimait à tirer de ses gens, mais dont il évitait la moindre production à son imposante carcasse.

Le jeune homme ne se révolta pas, chancelant et transpirant abondamment tandis que l’exécuteur s’avançait. Ulric tenait à ce que ça soit vite et bien fait. Quand la chair grésilla, que le burin déchira la paumes, Garin hurla et des larmes lui vinrent aux yeux. Désireux de ne pas handicaper un travailleur qui n’avait que ses bras pour se nourrir, le forgeron avait bien pris garde à frapper entre les os pour n’en briser aucun. À la requête du vicomte, un chirurgien s’empressa de bander la blessure. Les badauds commencèrent alors à s’éloigner, sans que cela ne perturbe Ulric, qui reprit la parole.

« Tu continueras à servir Bertaud le Normand jusqu’à la Toussaint ainsi que tu l’avais juré, sans percevoir plus de gages en châtiment, comme l’a décidé la Cour. Ce poing percé que tu arboreras désormais sera pour toi quotidien rappel de ton manquement, de cette main qui a trahi le maître qu’elle avait juré de servir. Puisse Dieu avoir pitié de toi. »

La Cour se dispersa alors, chacun retournant à ses activités quotidiennes en cette chaude journée. Le chirurgien laça le bandage et invita Garin à en changer les linges souvent. Puis il s’en fut à son tour, laissant le jeune homme assis dans la poussière, au milieu de la rue. Face à lui, Bertaud, les mains sur les hanches, affichait un sourire repu. ❧

## Notes

On ne sait que peu de choses du destin du jeune eunuque qui aurait poignardé Zankī. Certaines sources indiquaient que ce dernier avait tendance à s’emporter contre ses serviteurs, voire à se montrer cruel et il n’est pas impossible que ce ne soit rien de plus qu’une querelle domestique, thèse que j’ai retenue ici. Nul besoin alors d’envisager un complot ou une commande d’un adversaire de l’atabeg. Son méfait réalisé, l’eunuque fut signalé à Damas, où il n’eut que peu le temps de se vanter de son geste qu’il était remis aux dirigeants alépins avant d’être envoyé à Mossoul où il aurait été exécuté. Peut-être sans le savoir, il avait contribué à redessiner l’équilibre des forces en présence, offrant un sursis aux autorités damascènes face à l’appétit des atabegs turcs d’Alep. Ce qui en fit une cible pour la Croisade qui se déroula l’année suivante.

Ce texte est aussi l’occasion de dépeindre quelques-uns des grands personnages du côté musulman. L’historiographie est malheureusement moins riche que pour les Latins ou les Byzantins. De nombreux officiers d’importance, quoique de second rang, tels que Zayn al-Din, mériteraient des études plus documentées, étant donné leur rôle dans l’échiquier politique d’alors. Bien peu ont bénéficié d’une attention comparable à celle accordée à Usamah ibn Munqidh, dont les savoureux écrits constituent une source essentielle sur la période. Signalons enfin que Zankī lui-même n’a encore jamais été l’objet d’un travail aussi scrupuleux que celui réalisé sur le règne de son fils Nūr ad-Dīn par Nikita Élisséef.

## Références

Cobb Paul M., *Usama ibn Munqidh. The book of Contemplation. Islam and the Crusades*, Londres : Penguin Books, 2008.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I, *Economic Foundations*, Berkeley et Los Angeles : University of California Press, 1999.
