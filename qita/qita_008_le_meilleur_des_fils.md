---
date: 2012-06-15
abstract: 1140-1153. Malgré les combats incessants dans leurs territoires et aux alentours, les populations s’efforçaient de faire vivre leur famille dans le jeune royaume latin de Jérusalem. Parmi ceux-ci, on pouvait trouver des marchands, comme Sawirus de Ramathes, qui partagera la traversée à bord du Falconus avec Ernaut quelques années plus tard, dans *La nef des loups*.
characters: Sawirus de Ramathes, Ayoul, Mahran, Shabib, Yazid
---

# Le meilleur des fils

## Césarée, mercredi 3 juillet 1140, après-midi

Le soleil jouait au travers des branches du palmier, s’employait à gâcher la sieste de Sawirus. Allongé entre le tronc et un muret de pierre sèche, le marchand tentait d’oublier un peu la chaleur de la journée. Sa petite taille lui avait permis de se ménager un endroit agréable où se délasser, installé sur une couverture utilisée habituellement pour protéger le dos de sa mule Rayya du frottement des paniers. Mais pour l’heure, même elle, son infatigable servante, réfugiée à l’ombre, avait la tête basse le regard absent. Attrapant un de ses rafrafs[^rafraf], Sawirus tenta de s’en faire un masque sur les yeux. Ce fut alors au tour des mouches de venir bourdonner à ses oreilles, de se poser sur ses mains et ses narines.

Agacé par ce harcèlement, il s’assit, se frottant le visage comme s’il sortait d’un long sommeil. Une colonie de puces semblait s’être établie dans sa barbe, qu’il gratta avec vigueur. Une brise marine apportait quelques senteurs iodées, rafraichissant un peu l’atmosphère emplie de poussière. Il renifla et soupira silencieusement, les yeux à demi-clos, avisant d’un regard son potager. Son valet, Mahran, était monté sur le muret qui séparait l’enclos des terres voisines, protégeant les cultures des animaux et des maraudeurs. S’abritant du soleil d’une main, il était tourné vers le sud, scrutant par-delà les jardins.

« Vois-tu souci au loin ? »

Tout joyeux, l’homme se retourna vivement, les yeux rieurs :

« Force animaux bâtés, cavaliers en selle et pieds poudreux. Une belle caravane est aux abords de la porte depuis Nazareth. »

Sawirus se leva, époussetant son thwab[^thawb] de fin coton rayé. Jouant des pieds et des mains, il rejoignit son domestique et admira à son tour l’importante procession qui soulevait un nuage de poussière tel que les murailles et la porte semblaient sortir d’une brume dorée. Sawirus ne put s’empêcher de sourire.

« Nous avons là surtout des marchands à ce que j’en vois, fors quelques hommes du Christ à la croix écarlate, mais ils ne sont guère. Une simple escorte je dirais.

— Si fait. Ils protègent la fin de convoi. Certainement des marcheurs de la Foi de retour de la ville sainte.

— Mais les chameaux que je vois, ainsi que les mules, semblent bien chargés de vivres. Il se trouve là des négociants. Aide-moi à bâter Rayya puis empresse-toi de quérir nouvelles. Tu me retrouveras à la maison. »

Excité à l’idée des transactions qui allaient s’offrir à lui, Sawirus se hâta vers les paniers de bât, emplis de lentilles fraîchement récoltées, de bottes d’oignons et des derniers pois chiches de la saison, d’anémones et d’oeillets coupés avec soin pour décorer sa demeure. Son valet sangla rapidement et s’en fut, trottinant d’un pas lourd sur le chemin caillouteux. D’autres jardiniers curieux l’accompagnaient en direction de la grande porte orientale. Sawirus préféra éviter la cohue qui devait régner à cette entrée et choisit prudemment de faire le tour par la partie nord de la cité, guère éloignée.

Menant sa mule d’une main nonchalante, il rejoignit la muraille de pierres couleur sable, puis la longea à main gauche avant de bifurquer en direction du littoral. Tandis qu’il avançait sur le sentier au nord des fortifications, il laissait son regard errer au loin, sur la mer. Quelques esquifs, modestes embarcations de pêcheurs, ondulaient sur les flots, par-delà les dangereux rochers aux abords de la côte. Une voile de belle taille coupait l’horizon tel un canif pointé vers les cieux. Un navire de commerce, ou de guerre, qui faisait route vers le nord, peut-être vers Acre, Beyrouth ou plus loin encore.

Sawirus était heureux de ne plus avoir à voyager. Il avait désormais un garçon en âge de se charger de cette corvée, et lui se contentait de gérer ses affaires depuis sa maison. Il sourit à la pensée de sa femme et de sa fille qui l’attendaient, du repas qu’ils prendraient ensemble à la tombée de la nuit. Un peu en retrait du passage, sous les palmiers, un large groupe d’enfants jouait avec quelques chiens à l’allure famélique. Il leur sourit aussi, sans même y réfléchir, heureux de son sort et de la bonne fortune qui avait favorisé sa demeure jusqu’alors.

À l’entrée de la ville se trouvait seulement un homme de faction, qui accomplissait sa tâche avec un zèle rare, affalé dans un escalier accolé à la tour, le regard perdu dans le vague. Reconnaissant Sawirus, il le salua à peine d’un mouvement de menton, puis se replongea dans l’examen attentif de l’intérieur d’une savate usée. Pour ne pas faire de détour supplémentaire, le vieil homme décida de couper par le marché, une zone encombrée de sacs et de paniers, d’étals qui empiétaient sur la rue déjà fort étroite. Mais cela permettait de cheminer à l’ombre, car tout un fatras de canisses et de treillages avait été lancé d’une façade à l’autre, dans un désordre sans nom. Bien trop souvent, un simple oiseau en faisait s’écrouler de larges portions sur les badauds et les marchands situés en dessous, provoquant un nuage de poussière, et plus encore de cris indignés et énervés. Mais les roseaux et les fines lattes retrouvaient vite leur place jusqu’à leur affaissement suivant. Les artisans s’affairaient souvent devant leurs clients et exhibaient leur production à l’entrée de leur échoppe.

Le passage préféré de Sawirus était celui où de petits éléments de mobilier en bois tourné étaient réalisés. Une senteur délicate, mélange de toutes les essences creusées, encore vertes, embaumait l’air. Alors qu’il s’engageait dans la venelle menant à sa maison, il salua de la main quelques amis. Installés devant une petite table sur un perron auquel on accédait par une volée de marches, ils commentaient l’activité avec indolence. Il stoppa Rayya et souleva plusieurs fois le heurtoir de la grande porte arrière, par laquelle il faisait toujours entrer la mule. Ce fut une servante qui lui ouvrit, non sans avoir au préalable vérifié par le guichet l’identité du visiteur. Sans attendre, il lui donna la longe et lui demanda de décharger l’animal, puis de le confier aux soins de Mahran dès son retour.

« Justement, maître, il est déjà là, il vous attend dans la *qa’a*. »

À son air contrit, Sawirus sentit sa poitrine se serrer. Sans plus un mot, il se hâta vers le cœur de la demeure. Là, au débouché du couloir d’entrée se tenait Mahran, flanqué d’un gros homme habillé à l’occidentale mais avec un turban sur la tête. Les traits tirés, il était manifestement fatigué. La sueur avait dessiné des rides sur son visage, comme des traces de charbon dans la cendre et la poussière du voyage ternissait ses vêtements. Visiblement désolé, il souriait d’un air gauche. Il n’avait pas plus de vingt-cinq ans. Sawirus reconnut le fils d’un associé habituel. Le jeune homme salua rapidement, ne sachant que faire de ses mains. Sawirus inclina la tête à son tour.

« Maître Ayoul ! Quelle surprise, je ne vous savais pas en route pour ma cité, mais nous saurons vous loger comme il se doit, en ami.

— Je vous rends grâce de votre accueil, maître Sawirus, mais vous me voyez cheminer le cœur lourd jusqu’à votre hôtel…

— Que se passe-t-il ? Ton père aurait-il rendu son âme à Dieu ? Il est pourtant de quelques années mon cadet !

— Il ne s’agit pas de lui… Si je suis ici, c’est pour mener vos biens, à la demande de mon père justement, et pour vous informer que nous avons porté en terre votre fils, le pauvre Yazid, il y a deux huitaines de cela. Des brigands, à l’entour de Tyr et Toron, sûrement des fuyards de Panéas[^paneas2] ou de la garnison turque massacrée par le prince d’Antioche… »

Sawirus crut que son crâne allait exploser, il se retint à la porte pour ne pas défaillir. Les silhouettes se troublèrent devant ses yeux embués et sa gorge le serra tant qu’il lui paraissait être étranglé par quelque démon. Il se laissa glisser doucement au sol. De la salle voisine, un cri de femme déchira le silence. Une mère avait perdu son fils.

## Ascalon, jeudi 10 septembre 1153, matin

Le modeste groupe de cavaliers avançait doucement au travers des jardins abandonnés. Les murets en avaient été abattus, les arbres maltraités, les cultures dévastées. Parfois, quelques cabanes rapidement édifiées s’écroulaient dans un nuage de poussière. Ici et là, des croix surmontaient les fosses où des corps avaient été pieusement allongés. En tête, un peu affaissé sur l’encolure de sa monture se tenait Sawirus. Il regardait d’un œil triste le spectacle qui s’offrait à eux.

Le fossé était par endroits empli de broussailles, de terre et de gravats, sauvagement arrachés dans les potagers voisins. Des protections de bois, des vestiges de navires dépecés gisaient ici et là, arborant quelques tronçons de flèches comme les stigmates de la bataille qui avait eu lieu. Ironiquement, des souïmangas au plumage irisé lançaient leurs trilles, posés sur ces perchoirs improvisés. Les restes d’une grande tour d’assaut en partie incendiée étaient en cours de démontage, et des captifs habillés de hardes s’activaient au dégagement des éboulis de pierre au droit des murailles éventrées, sous la surveillance goguenarde de soldats repus.

Ascalon avait été prise après de longs mois de siège, désormais la bannière du roi de Jérusalem claquait dans le vent sur les remparts. Et les négociants comme Sawirus et ses compagnons s’en venaient pour voir comment rendre profitable cette désolation. La guerre engraissait le commerce, et les marchands. L’entrée se faisait par la grande porte, celle du levant, où se trouvaient désormais les hommes en charge de la ville d’Ascalon, en attendant l’installation de l’administration royale. Le garde qui les arrêta, la barbe poussiéreuse et le cheveu rare, avait le côté du visage encore tuméfié et parlait avec un chuintement sifflant. Il leur indiqua le bâtiment converti en halle, une ancienne mosquée appelée la Verte.

Une fois l’enceinte franchie, le lieu s’annonça plus paisible. La rue principale, comme la majeure partie de la cité, n’avait vu aucun affrontement et la reddition négociée avait permis à l’endroit d’échapper à la dévastation. De temps à autre, on apercevait une ombre rasant les murs, quelques chrétiens orientaux qui avaient répugné à abandonner leurs biens. Depuis un peu plus de deux semaines qu’Ascalon était tombée, la vie reprenait peu à peu ses droits, les hommes devenaient plus hardis, et les voyageurs autorisés à entrer apportaient un peu d’animation moins effrayante que les soldats en armes. Obliquant en direction de la porte de la mer, ils pouvaient désormais admirer le bâtiment qu’on leur avait indiqué, situé sur un promontoire au sud-ouest de la ville. La taille imposante de ce dernier et sa proximité avec la plage en faisaient un centre de commerce idéal.

Abandonnant les chevaux à un de leurs valets, les trois marchands pénétrèrent dans la grande cour, au travers de la cohue générale. On s’invectivait dans toutes les langues du Levant, on s’interpellait d’une arcade à l’autre, on se saluait de gestes démonstratifs. Des paquets étaient vivement hissés sur les bâts, des ballots se faisaient éventrer pour en présenter le précieux contenu. Étoffes délicates, rutilants objets de cuivre, céramiques colorées, verre, bijoux…, les richesses de la ville étaient offertes au regard, posées à même le sol sur de pauvres tapis ou brandies bruyamment par les camelots. Quelques soudards, la mine réjouie, négociaient le fruit de leurs rapines avec des négociants chicaneurs. On mordait les pièces[^1], on soupesait leur poids, on palpait les bourses de monnaies scellées[^boursescellee].

Sawirus fut bousculé par un homme à l’expression dédaigneuse qui menait une mule chargée d’un incommensurable fatras d’armures, casques, fers de lance, épées. Les trois compères se regardèrent, un peu perdus, ne sachant par où commencer. Le plus jeune, Shabib, proposa de se séparer afin de prospecter plus rapidement. Sans même attendre de réponse, il disparut dans la foule bigarrée. Sawirus n’aimait guère se trouver ainsi malmené de droite et de gauche, devant surveiller sans cesse sa ceinture, craignant qu’un larron n’y mette la main sur son argent.

Il croisa quelques visages patibulaires de guerriers en patrouille, le regard mauvais. Ils auraient certainement préféré s’activer à dépecer la ville de ses richesses plutôt qu’à garantir la sécurité de ceux qui profitaient de l’aubaine. Ils se contentaient donc de cheminer par petits groupes, heurtant de leurs armures sales les négociants affairés. Indifférente à leur avance, la foule n’était guère plus vexée de leur brutalité qu’un torrent malmené par des rochers.

Sawirus pénétra dans la grande salle, où il découvrit que se déroulait la vente des esclaves. Sur la chaire de prêche convertie en estrade, on exhibait les captifs, les mains et les pieds entravés par des cordes ou par des fers pour les plus intrépides. Pour l’heure, des soldats turcs attendaient en file, crachant et maudissant de leur langue rude leurs geôliers tout en se débattant. Un grand guerrier aux longues tresses, au port altier, venait d’être vendu pour trente-cinq dinars égyptiens, une misère pour un tel homme.

Se demandant à quel taux il pouvait acquérir des monnaies, Sawirus s’avança vers les bancs de change qui occupaient tout un pan de mur. Là, les trébuchets cliquetaient des pesées rapides, chacun comparait ses notes avec le voisin. Pour l’heure, la pièce d’or égyptienne pouvait s’obtenir pour un besant de Jérusalem, ce qui constituait une bonne affaire.

Il était perdu dans ses réflexions, se lissant la barbe, désormais blanche, tout en calculant grossièrement les profits possibles, lorsqu’il entendit claquer un fouet. Il se retourna et vit un sergent franc frapper à coups répétés sur un jeune captif musulman recroquevillé à ses pieds, s’attirant un regard indigné des marchands les moins endurcis. Sawirus s’avança calmement, interpellant l’homme d’une voix douce.

« Cesse donc, soudard, tu vas tuer ce pauvre enfançon ! »

Le bras se figea et un regard surpris croisa celui du marchand. L’homme n’était visiblement pas bien méchant.

« C’est qu’il a le teston plus dur qu’une pierre de meule ! Il refuse d’obéir.

— À tant le bestancier, tu n’arriveras guère à le faire avancer.

— Tant qu’il arrive à l’estrade plus vif que mort, ça me va. »

L’enfant desserra un peu les bras, inquiet de comprendre pourquoi sa correction s’était interrompue. Les croûtes qui zébraient ses membres, la crasse qui maculait son visage, ses cheveux et ses guenilles indiquaient qu’il subissait ce traitement depuis un petit moment déjà. Lorsque Sawirus aperçut son regard d’animal apeuré, la question franchit ses lèvres malgré lui :

« Et quel prix en demandes-tu ? »

L’homme se racla la gorge bruyamment.

« Avec tous ces captifs, si j’en tire une douzaine de besants, j’en serai aise. »

Le marchand se pencha vers l’enfant prostré, qui se recula instinctivement.

« Quel est ton nom, garçon ? »

Il n’obtint en réponse qu’un souffle muet puis un murmure à peine audible :

« Yazid. »

Sans même y réfléchir, Sawirus se redressa puis compta vingt pièces d’argent au soldat ébahi. ❧

## Notes

La prise d’Ascalon en 1153 fut une grande victoire stratégique pour les forces latines moyen-orientales. La ville tenue jusque-là par les Fâtimides d’Égypte constituait une tête de pont qui menaçait sans cesse les territoires du sud des royaumes, jusqu’aux alentours de Jérusalem. La dynastie finissante des shi’ites égyptiens constituait une proie rêvée pour les pouvoirs régionaux. Ce fut d’ailleurs Saladin qui s’en empara finalement, avant de réaliser l’unification avec les territoires syriens sous son pouvoir. Bien que la prise de la ville se fût faite suite à une reddition, les habitants musulmans en furent délogés et escortés jusqu’aux territoires fâtimides, où ils furent razziés par des pillards turcs. On sait que certains habitants restèrent, dont des Juifs et vraisemblablement des Chrétiens orientaux qui n’avaient, en principe, rien à redouter des attaquants catholiques.

Le commerce continuait bien évidemment pendant les opérations militaires, qui n’avaient pas une ampleur telle que les communications fussent impossibles. Bien au contraire, un marché suivait généralement les armées, leur fournissant des vivres et recyclant, dans les échanges commerciaux, les prises de guerre dont les esclaves. Par ailleurs, le change demeurait possible à tout moment et certains négociants spéculaient justement sur les taux. Les marchands étaient très mobiles, n’hésitant pas à quitter leur famille pour des mois, voire des années. Et il arrivait qu’ils succombent des aléas des voyages, naufrage, maladie ou brigands et pirates.

## Références

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I à 6, Berkeley et Los Angeles : University of California Press, 1999.

Moy Daniel R., *Military strategy in the Latin Kingdom of Jerusalem : The Crusader Fortification at Caesarea*, Mast. Of Arts Thesis, Norman, Okalhoma, 1998.

Prawer Joshua, *Histoire du Royaume latin de Jérusalem*, Paris : CNRS Éditions, 2007.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem. A corpus. Volume I. A-K*, Cambridge : Cambridge University Press, 2008.

[^1]: On vérifiait sommairement la qualité des pièces en les mordant et en les faisant tinter, avant de les peser.
