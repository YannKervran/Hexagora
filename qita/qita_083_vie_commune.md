---
date : 2018-09-15
abstract : 1156-1158. Joris et Marie, deux servants de l’Hôpital partagent la vie routinière des gens de labeur, ce troisième ordre voulu par Dieu et formant la majeure partie de la population. Leur existence n’est pourtant pas dépourvue de rêves et d’aspirations, aussi simples puissent-ils paraître.
characters : Joris, Marie, Gilleite, Girout, Mabile
---

# Vie commune

## Jérusalem, hôpital de Saint-Jean de Jérusalem, après-midi du mardi 18 décembre 1156

Capuche de son manteau sur la tête, bras croisés, appuyé au chambranle de la porte basse ouvrant sur la cuisine des femmes, Joris discutait avec les servantes qui allaient et venaient, portant de l’eau et des denrées pour le repas du soir. Il aimait à vaguer, usant de tous les prétextes pour prendre son temps lorsqu’une mission lui était attribuée. Bien qu’il n’ait que rarement été réprimandé pour un travail mal fait ou achevé de façon incorrecte, il s’était ainsi vu peu à peu confier des tâches de moins en moins intéressantes et agréables. Il répondait à cette déchéance avec un entrain déclinant et une inventivité redoublée pour flâner et discuter.

Entré au service de Saint-Jean quand il était tout juste sorti de l’enfance, il n’avait rien pu y apprendre d’utile pour accéder à un nouveau métier et avait fini à la préparation des morts pour leur ultime voyage. Depuis plusieurs mois, il était plus particulièrement affecté au transport des corps, au sein de l’établissement et pour leur acheminement jusqu’à Chaudemar où ils étaient grossièrement ensevelis, dans un immense charnier pour les plus humbles. Il supportait la chose avec philosophie, même s’il espérait pouvoir quitter un jour son état de valet. Enfant, il avait travaillé aux champs avec sa famille, et s’il n’y avait alors pas pris grand plaisir, il repensait à ces années comme à un temps d’insouciance et de liberté. Il estimait qu’il devait préserver ses forces pour ce moment béni où il peinerait sur sa terre et non pas au service d’un maître, fût-il parfois aussi bienveillant et peu exigeant qu’un frère de Saint-Jean.

Il avait un groupe d’amis qui renâclaient autant que lui à se fatiguer à la tâche. Ils avaient développé un talent certain pour se retrouver de façon imprévue en tous lieux de la clôture, prenant le temps d’échanger potins, rumeurs et nouvelles, voire blagues et devinettes. Étant un des rares hommes autorisés à pénétrer dans certaines zones réservées aux femmes, Joris en prenait avantage pour plaisanter avec celles qu’il rencontrait et il avait son petit cercle de commères avec qui papoter à l’abri des regards des responsables. Parmi celles-ci, Gilleite était la plus bavarde. D’une cinquantaine d’années, le physique pesant et rompu hérité d’une vie de labeur difficile, elle s’était donnée à l’ordre après la mort de son époux. N’ayant nul bien, elle travaillait pour le gîte et le couvert et n’avait prononcé que quelques vœux sans devenir sœur à part entière. Elle avait pris Joris en affection, d’autant qu’il supportait sans fatigue son babillage incessant et l’accueillait chaque fois d’un sourire.

Attendant qu’on lui confie une charrette à bras où quelques dépouilles féminines avaient été préparées dans des linceuls pour leur dernier voyage, il estimait qu’il avait toute licence pour bavarder, distrayant de leur tâche celles qui s’en sentaient d’humeur. Gilleite saisissait toujours ce genre d’occasion pour lui faire part de ses réflexions, mêlées de remarques sur ses douleurs corporelles et les aléas climatiques, avec une platitude et une inexactitude qui n’entamaient en rien l’enthousiasme qu’elle y mettait. Ces temps-ci, elle s’émouvait particulièrement du sort de la vieille reine mère Mélisende[^melisende], retirée à Naplouse, dont on disait la santé vacillante. Gilleite n’avait que des éloges à faire de son règne et déplorait que le fils n’en ait pas hérité tous les talents.

« Il devrait faire plus fréquentes visites à ses tantes. Il apprendrait d’elles meilleure religion, plutôt que de s’adonner à la lance et l’épieu ! »

Joris acquiesçait de façon machinale à ces considérations générales. Il n’avait que peu d’opinions sur la famille royale quoiqu’il appréciât tout particulièrement le comte Amaury de Jaffa, dont le talent à la chasse et auprès des femmes était vanté parmi les hommes. Il n’en soufflait évidemment rien à sa confidente du jour, qui déplorait ces attitudes qu’elle jugeait peu dignes d’un baron. Si elle trouvait tout à fait normal que les plus humbles s’adonnent à leurs vices et leur pardonnait volontiers leurs écarts, elle estimait qu’il était du devoir des puissants de se montrer vertueux et sages, de belle allure et, surtout, très pieux.

Tout en pérorant, elle remarqua d’ailleurs que le regard du jeune homme volait vers la silhouette d’une servante affectée au tamisage de la farine et le tança gentiment en lui donnant un coup de coude.

« La Marie a bien grandi ces derniers temps, hein ? »

Il confirma d’un coup de menton, sans lâcher la fille des yeux. Il la connaissait depuis des années, l’avait toujours considérée comme une gamine un peu effrontée, mais d’agréable compagnie, qu’il houspillait parfois pour la forme, dans un sourire. Aujourd’hui, pour protéger ses cheveux, elle les avait noués sous un linge en un simulacre de foulard qui la faisait paraître telle une femme mariée. Elle avait en quelques mois développé tous les signes de sa féminité, perdant pour le coup son allure de garçon dégingandé. Il semblait aussi à Joris qu’elle mettait plus de grâce dans ses gestes, plus d’affectation dans ses postures. Peut-être avait-elle senti l’attention d’un homme sur elle ?

Gilleite ricana et lui donna une légère bourrade.

« Tu sais, elle s’est pas donnée à l’ordre. Elle sert ici jusqu’à ce qu’elle trouve un mari. »

Elle eut un mouvement d’épaules, ses traits se durcissant soudain.

« Enfin, si elle a occasion que quelqu’un s’occupe d’en dénicher un qui s’accorderait de la dot de misère d’une orpheline !

— Elle n’a plus personne ?

— Je crois pas, peut-être quelques frères, mais elle est sans le sou, pour sûr. Tu pourrais en faire honnête femme, je vois bien qu’elle est à ton goût ! Les sœurs seraient ravies de la voir marier bon chrétien, elle qui n’est que baptisée. »

Joris étira un rictus en réponse. Il savait qu’il ne pourrait obtenir un manse en tant que célibataire et une orpheline d’origine musulmane serait plus aisée à épouser, moins exigeante sur ce qu’il devrait fournir. Toutefois, en la regardant, il se dit qu’il pourrait aussi s’éprendre de cette jeune fille au caractère bien trempé. D’autant qu’il avait toujours trouvé les Syriennes plus attirantes que les Européennes. Peut-être y retrouvait-il l’image de sa mère, qu’il avait tant aimée, enfant ? 

## Jérusalem, hôpital de Saint-Jean de Jérusalem, fin de matinée du vendredi 4 janvier 1157

Marie finissait à peine de border le lit dont elle venait de changer les draps qu’on lui amenait une femme dont la toux avait épuisé la voix. Elle lui sourit sans chaleur, habituée à recevoir tant de malades qui ne survivaient que quelques jours qu’elle évitait de trop sympathiser. La pauvre était brûlante de fièvre, avait été recueillie par un valet d’une boutique qui l’avait trouvée sans connaissance dans la rue[^rue]. Elle avait pu murmurer son nom, Luce, et qu’elle était pèlerine. Épuisée par ses quintes, les paupières gonflées par le manque de sommeil, elle était aussi très amaigrie par les privations du long voyage. Elle se laissait faire comme une enfant, à peine capable de lever les bras ou de tenir les yeux ouverts. Marie l’aida à enfiler une chemise de laine propre et rassembla ses rares affaires dans un sac qu’elle confia à la sœur responsable de la travée. Ses habits seraient nettoyés et éventuellement réparés et conservés jusqu’à son départ ou son décès. À voir la croix encore cousue à son épaule, elle n’avait pas accompli son vœu et était vraisemblablement arrivée depuis peu.

Au moment où Marie achevait de l’allonger, ramenant sur elle l’épaisse couverture, Luce murmura quelques mots. Elle avait un de ses compagnons, Marcel l’Espéron, logé à l’Asnerie, qui serait certainement soulagé de l’apprendre aux bons soins des sœurs. Sans cela, il risquait de s’inquiéter de sa disparition. Marie la rassura, affirmant qu’un des valets porterait la nouvelle. Avant de la quitter, elle lui toucha le front, brûlant de fièvre. Malgré son évident épuisement, elle toussait tellement qu’elle n’avait guère de chances de s’endormir de sitôt. Marie l’indiqua à la sœur responsable de sa rue, qui s’occuperait de demander un traitement au médecin lors de sa visite. Il fallait avant tout que la malade soit en mesure de se confesser au plus vite, surtout si son état était grave. Le péril de son âme était bien plus urgent que celui de son corps, par essence mortel.

Comme elle avait achevé ses tâches matinales, on lui confia plusieurs messages à aller porter du côté des hommes. Les patientes ne pouvaient recevoir leurs époux, fils ou parents et d’incessants échanges se faisaient par des commissionnaires afin de transmettre les nouvelles, les demandes ou les questions. Marie saisissait toujours cette occasion de franchir la clôture pour tenter de s’esquiver au-dehors du quartier. Les accès du couvent féminin étaient généralement gardés par des cerbères peu aimables et guère enclins à la bienveillance, mais celles de la partie masculine étaient sous la surveillance de valets bien moins regardants, surtout si on leur adressait un charmant sourire en guise de salut.

Elle vint donc transmettre les messages au frère à l’entrée de la vaste salle, qui les nota sur une tablette, puis prit congé en prenant garde à ne pas se diriger devant lui vers la grande porte ouvrant sur le Saint-Sépulcre. Elle avait l’habitude d’emprunter une autre sortie, moins visible, qui permettait, par une placette et une ruelle adjacente, de se retrouver dans la cité. Alors qu’elle descendait l’escalier menant dehors, elle croisa Joris qui revenait apparemment de la chapelle ardente, portant sur un brancard avec un compère un corps enveloppé d’un linceul. Elle lui accorda un sourire, qu’elle accompagna d’un geste amical de la main. Elle appréciait assez le valet, qui ne l’avait jamais traitée en fillette. Elle avait subi jusqu’à l’écœurement les bons soins et la prévenance des sœurs et lui était reconnaissante de n’avoir pas vu en elle qu’une pauvre petite chose à protéger.

Tandis qu’elle le regardait s’éloigner avec son lourd chargement, elle lui trouva une certaine allure. Il s’était visiblement fait raser récemment et ses cheveux n’étaient pas trop mal peignés. Ses vêtements propres indiquaient qu’il était également soucieux de son apparence et attentif à ses affaires, ce qui n’était pas une vertu si répandue chez les jeunes hommes. Ayant pris ses renseignements, elle savait qu’il n’était pas marié ni promis. S’il n’était pas trop porté sur le vin ou les dés, il pourrait faire un bon époux, estima-t-elle. Elle était consciente de la précarité de sa situation et avait apprécié qu’il lui adresse un regard intéressé qu’elle ne jugeait pas déshonnête. Depuis qu’elle arborait des formes féminines, elle avait bien vu qu’on ne la jaugeait plus de la même façon, et elle ne goûtait pas toujours les frottements ou les contacts plus ou moins fortuits que de nombreux hommes se permettaient. Au moins Joris se contentait-il de ses yeux. Et il avait un sourire vraiment charmant…

## Jérusalem, Malquisinat, soirée du mercredi 27 mars 1157

En dépit de Carême et durant toute la Semaine sainte, les boutiques du quartier au centre de la ville ne chômaient pas, avec l’afflux de voyageurs et de pèlerins venus assister aux festivités. Les commerces de bouche bénéficiaient d’exemptions, les établissements religieux ne pouvant à eux seuls nourrir la myriade de croyants. Malgré la presse, Joris réussit à obtenir les rissoles à jour de poisson[^rissolepoisson] qu’il affectionnait tant, mais pour la boisson, il avait renoncé au vin de sa taverne préférée pour se rabattre sur une petite bière qu’il estimait correcte. Il rejoignit Marie, qui l’attendait assise sur le rebord d’une citerne publique. Ils avaient profité de l’agitation de la Semaine sainte pour s’esquiver chacun de son côté afin de prendre un repas ensemble. Il y avait également souvent des spectacles à thème religieux ou plus simplement de jongleurs, d’acrobates ou de montreurs d’animaux et ils escomptaient bien une très agréable veillée. La nuit était belle et, malgré les rumeurs qui parlaient depuis quelques jours d’un démon tueur de pèlerines, ils avaient prévu de s’amuser.

Si Joris habitait en dehors de la clôture et faisait ce qu’il souhaitait de son temps libre les soirs et les jours fériés, Marie avait dû s’organiser avec ses compagnes de chambrée pour cacher son absence et franchir le mur. Avec la cohue des Pâques, il y avait tant à faire que les sœurs n’exerçaient plus une discipline aussi ferme qu’habituellement. Ce n’était pas la première fois que la jeune fille quittait ainsi le couvent, mais jusque là cela n’avait jamais été pour rejoindre un homme. Ils avaient combiné cette escapade comme deux écoliers facétieux, mais ne prenaient que graduellement conscience de ce que cela impliquait. Leurs sourires s’emplissaient d’une gravité croissante tandis qu’ils goûtaient ensemble le plaisir des festivités.

Joris avait croisé quelques amis, mais il avait repoussé toutes les invitations, ayant promis, annonçait-il, de faire les honneurs de la veillée à Marie. Il obtenait souvent en réponse des regards entendus et des sarcasmes à peine subtils. Mais il gardait une attitude polie envers la jeune fille, n’ayant que peu d’idée sur la manière on faisait la cour de façon correcte à une future épouse. Il n’avait en tête que les modèles des grands chevaliers dont on lui avait narré les aventures depuis son enfance. Et c’était là rarement une part importante de leurs exploits. Il attendait donc simplement le bon moment pour évoquer avec elle les perspectives d’union, Marie n’ayant nul père ou frère à qui poser légitimement la question. Il avait longuement récapitulé tous les avantages dont ils bénéficieraient en se mariant, le principal étant selon lui la possibilité de s’établir en leur propre demeure.

Pour l’instant, Marie ne pensait à rien de tout cela. Elle s’amusait beaucoup, prenant prétexte du moindre spectacle, de la plus petite manifestation pour exprimer sa joie et applaudir à tout rompre. Elle n’avait que peu d’occasions d’ainsi se divertir. Elle avait l’habitude de passer ses jours de repos à coudre son trousseau ou à des jeux de table sous la surveillance d’une sœur, dans une des cours intérieures du couvent. Elle n’en sortait que rarement et lorsque c’était avec une permission, c’était chaque fois en groupe pour aller se recueillir en un édifice saint ou fleurir la tombe d’une importante personne, souvent bienfaitrice de leur ordre. Pourtant en elle demeurait un feu ardent qui ne demandait qu’à déborder de ces lieux étroits où la vie l’avait confinée depuis la mort de ses parents. Par son baptême, elle avait échappé à une existence de misère en un casal gris, mais elle regrettait amèrement ses jeunes années, dont elle ne gardait que des bribes de souvenirs, des images de courses dans les collines et de jeux dans les rues ensablées, parmi les poules et les chèvres.

Ils revenaient d’un spectacle de montreur de singes, en direction du nord de la cité où d’autres festivités les attendaient, poussés par le flot humain chantant psaumes, cantiques et rengaines paillardes selon l’humeur et, alors qu’ils arrivaient sur une placette ornée d’un tamaris desséché et de quelques débris d’un escalier ne menant plus nulle part, Joris estima que le moment était propice. Ils venaient de se mettre en retrait du passage, assis sur un banc en devanture d’une boutique, fermée pour la nuit, et regardaient défiler une longue troupe de pèlerins alémaniques dont les voix fortes faisaient du *Credo* un grondement de tempête. Il prit la main de Marie, qui sentit instinctivement que l’instant s’emplissait de solennité. Elle réussit malgré tout à lui adresser un timide sourire d’encouragement.

« Voilà, Marie. Je voudrais que tu penses à une idée que je crois pas bête, pour nous deux. On n’a pas tant, ni l’un ni l’autre, mais j’encrois qu’on saurait se dénicher un hostel et le faire prospérer. Je te sais vaillante et adroite, et moi je sais travailler la terre. J’ai bon espoir de pas demeurer valet inféodé. Alors on pourrait se jurer notre foi et… »

Au fur et à mesure qu’il marmonnait le discours tant de fois rabâché et répété, il perdait ses mots, hésitait sur les idées. Il n’osait regarder le visage de la jeune fille, inquiet d’y lire un rejet, voire pire encore, du dédain ou de l’amusement à son encontre. Il savait que les femmes pouvaient être féroces et il n’aimait pas se dévoiler ainsi. Mais s’il voulait avancer dans la vie, il lui fallait prendre des risques. Après tout, il n’y avait là aucun témoin qui pourrait se gausser d’un éventuel refus. Sa voix finit par mourir avant d’achever sa dernière phrase. Il leva les yeux vers Marie, pour la découvrir le regard modestement baissé, une ombre de sourire sur les lèvres, sans une once de moquerie. Elle lui serra la main, de façon à peine perceptible et hocha la tête doucement. Puis elle tourna son visage vers lui.

« Si je dois me promettre à toi, j’aimerais que cela soit en tant que femme… »

Elle n’osa rien dire de plus, attendant une réaction de Joris. Il sourit à son tour, un peu timidement, et approcha ses lèvres.

## Clepsta, demeure de Girout la Cirière, fin d’après-midi du mercredi 12 février 1158

La porte largement ouverte laissait pénétrer la lumière, ainsi que le froid, dans la pièce principale du manse, encombrée d’une vaste table. Un soleil timide apportait une rare chaleur là où ses rayons s’avançaient et Joris en goûtait les effets sur son dos tout en dégustant un fond de vin miellé. Face à lui, sa sœur Girout s’employait à l’achèvement d’une structure en paille de seigle. Si son époux ne confiait jamais ses abeilles, ses « mouches à miel » à quiconque, Girout estimait qu’elle était la seule à faire des ruches de qualité. Elle s’occupait de l’extraction de la cire, qu’elle vendait en larges plaques  circulaires à des artisans en ville ou à des communautés religieuses.

L’endroit fleurait bon l’opulence paysanne. Plusieurs coffres à pentures longeaient les murs, de nombreuses céramiques dont certaines, de service, décorées, ornaient les étagères et plusieurs chaudrons de fer et un de cuivre pendaient au-dessus du petit foyer d’angle où un feu moribond était maintenu. En plus d’un sol en carreaux de terre cuite, l’hôtel avait une citerne et un beau jardin où les enfants s’amusaient avec les volailles de la basse-cour. À cela s’ajoutaient des champs en suffisance, sans compter un droit de pâture dans les zones en jachère et quelques bois. Les emplacements des ruches y étaient âprement discutés chaque année avec les voisins, qui disposaient de privilèges similaires.

Même la tenue de Girout indiquait leur relative opulence. Sa robe était de laine épaisse, d’un orange profond, et son voile était de soie, certes un peu passée, mais avec quelques décors brodés en bordure. Joris n’était plus venu depuis des mois et pouvait constater l’enrichissement de sa sœur. Il y voyait un présage de ce qu’il pourrait obtenir une fois établi. Reposant son gobelet, il se décida à annoncer la nouvelle qui l’avait mené jusque là.

« Je suis venu te voir pour t’annoncer que je vais quitter le service des frères. »

Girout le dévisagea de ses grands yeux, trop écartés dans son visage plat, la faisant ressembler à une chouette. Elle pinça les lèvres, attendant de savoir ce qu’annonçait cette entrée en matière.

« Ils ont obtenu gain de cause pour des terres à Mirabel, dans le comté de Jaffa. Ils offrent de bonnes conditions pour qui veut s’y établir.

— Il y a de l’ouvrage pour simple manouvrier ?

— Si j’y vais, ce sera en chef de famille. J’ai une bonne amie, les épousailles se feront avant de partir. »

De surprise, Girout posa son ouvrage. Elle avait toujours pensé que Joris, benjamin de la fratrie, resterait valet et célibataire. Sans avoir, il n’avait aucune chance de trouver une femme qui accepte de s’installer avec lui. Elle craignait qu’il ne se soit résolu à une mésalliance. Son frère vit bien son embarras, même s’il avait espéré qu’elle manifesterait un peu plus de joie à cette annonce.

« C’est une fille de Saint-Jean, orpheline. Pour elle aussi, le mariage, ça signifie quitter la servitude.

— Tu sais que le père t’a rien laissé, à sa mort ? Tout est allé au Pierre quand il s’est installé. »

Joris hocha la tête. Leur aîné avait obtenu la quasi-totalité des biens de leurs parents, ayant peu à peu récupéré tous les outils pour sa propre maison. Il était établi dans un casal voisin, et s’il avait conservé auprès de lui un frère et deux sœurs, il n’avait guère plus de relation avec Joris ou Girout. 

— Le père avait gardé tout de même quelques affaires, et c’est de ça que je voulais t’entretenir. J’aimerais porter sa belle cotte, celle en soie bleue, pour mes épousailles. »

Le rictus de désappointement de Girout fut si éloquent que Joris n’eut pas à attendre sa réponse. Sa voix monta d’un cran tandis qu’il se penchait en avant.

« Je sais qu’il l’avait gardée, il la tenait de l’ancêtre et lui accordait grande valeur. Une relique de la prise de la Cité. Elle était trop petite pour le Pierre.

— Elle n’est plus ici. Quand il a fallu vider le manse, on a fait au mieux. »

Joris fronça les sourcils.

« Elle est passée où ? »

Girout haussa les épaules, soupira.

« Elle était bien abîmée, surtout depuis que le chien l’avait mordue au sortir de la messe. Je craignais qu’elle ne perde tout son lustre. C’était du souci, de garder si belle toile. Et puis elle était trop menue pour le Clarin. Tu n’avais pas donné de nouvelles depuis long moment… On ne t’a même pas vu à la mort du père !

— Hé quoi ? Je l’ai su après l’enterrement ! Si tu crois que c’est aisé d’obtenir quelques jours pour venir ici ! Je ne suis pas maître de mes journées ! »

Il grogna encore, avala une maigre rasade, finissant son verre, qu’il reposa un peu violemment, s’attirant un regard de reproche de son aînée. D’instinct, il adoucit sa voix.

« T’en as fait quoi ?

— Elle intéressait un fripier, avec quelques autres affaires dont je souhaitais me défaire.

— Il me restera donc vraiment rien des vieux ? »

Désireuse de ne pas laisser entendre qu’elle pourrait offrir une compensation, ce que son époux n’accepterait que difficilement, Girout fit mine de plonger le nez dans ses brins de paille. Joris s’agita un temps sur ton escabeau, maugréant sans rien dire d’intelligible, puis lâcha finalement, d’un ton plus conciliant.

« J’espère au moins que Clarin et toi vous serez là pour ma bénédiction. »

## Jérusalem, grand hôpital de Saint-Jean, soirée du lundi 3 mars 1158

Sœur Mabile était une grande femme au physique élancé, dont la longue tenue de laine sombre rendait le teint cireux et le visage triste. Un nez discret aux fines narines surplombait une bouche nerveuse, aux lèvres minces, en perpétuelle recherche d’un sourire de circonstance. Ses yeux, fatigués et un peu myopes, glissaient dans une langueur permanente d’un point à l’autre, ne se fixant jamais nulle part et ne se risquant à dévisager quiconque que par le travers ou à la dérobée. Elle avait pris son rôle de marraine très à cœur depuis le baptême de Marie, lui répétant au départ avec patience qu’elle ne se prénommait plus Jamila et l’édifiant avec grandiloquence sur le modèle parfait que constituait la mère de Dieu pour toute femme.

Pour le moment, elle n’était pour une fois guère loquace, lançant des regards inquiets vers sa filleule. Marie était venue la voir dans sa cellule après le souper pour lui annoncer son intention d’épouser Joris, et donc de quitter le couvent. Elle voulait également savoir comment se déroulait la remise de sa dot. Si la moniale n’était pas en soi opposée à toute vie hors de la clôture, elle ressentait ce souhait comme une trahison de la part de la petite fille à qui elle avait consacré tant d’années. Elle avait toujours espéré, et même cru, que Marie finirait comme elle par prendre le voile et prononcer des vœux monastiques.

« Es-tu bien certaine de vouloir tomber ainsi dans le siècle ? Tu ne le connais guère, c’est un endroit de périls et de tentations ! »

Marie se retint de répondre que, donnée à l’ordre alors qu’elle n’était qu’une enfant, sœur Mabile n’en savait pas plus. Et si elle y voyait un endroit plein de dangers, Marie y espérait un espace du possible, où ses rêves pourraient prendre place. Joris lui faisait bon effet et, s’ils avaient déjà partagé une couche à plusieurs reprises, elle ne l’avait pas trouvé irrespectueux ni violent. Parfois un peu naïf, mais elle accordait à cela un certain charme. Et, surtout, il l’avait traitée dès l’abord comme une adulte, la partenaire avec laquelle il pourrait enfin œuvrer à l’accomplissement de ses aspirations. C’était le premier à la considérer comme une vraie personne et cela l’avait séduite.

« C’est pour cela que je souhaite y cheminer avec un époux à mes côtés. C’est un homme de bien, au service de Saint-Jean lui aussi. »

Un éclair de reproche, vite maîtrisé, brilla dans les prunelles de la religieuse. Un valet ne pouvait avoir grandes qualités, étant donné que la place que chacun s’était vu fixée par Dieu était à proportion de ses vertus.

« J’ose encroire que vous ne souillez pas ces lieux saints par quelque débauche ! L’on peut s’accorder à certaines pratiques, une fois mariés, afin de peupler la terre du Seigneur de bons chrétiens, mais je ne saurais entendre parler de toi ainsi que d’une souillon. »

Marie pinça les lèvres. Elle savait que sa marraine avait les plus grandes répugnances à parler de tels sujets, et elle préféra faire porter la discussion sur l’aspect religieux, qui lui semblait terrain plus assuré.

« Nous avons eu l’accord du chapelain pour nous bénir dès après les Pâques. Comme Joris peut avoir son congé avant la Saint-Jean d’été, nous pourrions nous installer avant le plus fort des chaleurs. »

Sœur Mabile se passa une main angoissée sur le visage. Ce n’était pas la première fois qu’une de ses filleules ne répondait pas à ses aspirations. Au moins Marie avait-elle le bon sens de faire les choses de façon plus ou moins convenable.

« Je sais que ce n’est pas de mon rôle de marraine, mais j’aimerais assez voir ton futur espousé avant cela. Que je puisse t’en dire ma pensée.

— Ce serait fort amiable à vous, mère. Je lui ai déjà parlé souventes fois de vous et de toutes vos bontés, et il serait de certes heureux de vous rassurer quant à ses projets. »

Sœur Mabile acquiesça, rassérénée par ce peu discret hommage à sa sollicitude. Elle se fit également forte de se renseigner auprès du chapelain sur ce valet. Elle estimait que cela demeurait de son devoir de tout mettre en œuvre pour garantir le Salut de Marie. Si la jeune fille renonçait au parcours idéal de la vie monacale, il fallait s’assurer qu’elle aurait de quoi protéger son âme en tant que bonne épouse et mère. Un homme sobre, travailleur et pieux pouvait être toléré, à défaut de se consacrer au Christ.

« Ce mercredi, le prêtre nous bénira de la croix de cendre. Profite de ce Carême pour bien pourpenser cela et prendre la bonne décision. Tu as encore temps de faire bon choix d’ici la fin des Pâques. »

## Mirabel, manse des Terres d’Épines, fin d’après-midi du mardi 24 juin 1158

La porte s’ouvrit en grinçant après que Joris en eût déverrouillé la serrure. Il brandit la clef avec un sourire et la noua à sa ceinture, tout en invitant Marie à entrer. Il n’y avait qu’une pièce, en terre battue, aux murs fraîchement chaulés. Deux petites fenêtres à simple volet apportaient de la lumière et un peu d’air, des céramiques à large gueule étaient enfouies au fond, coiffées de couvercles de bois. Le seul meuble était un cadre au sol, empli de paille : leur lit. Marie s’avança, les traits illuminés par le plaisir. Elle voyait l’endroit pour la première fois, pressa l’épaule de son compagnon.

« Nous voilà chez nous, ça y est ?

— Oui. Ce ne sera vraiment officiel que lorsque j’aurai juré auprès de l’intendant, mais ce n’est que broutille. »

Il considérait leur royaume intime avec autant d’allégresse qu’elle, même s’il y était déjà venu à plusieurs reprises. Durant les semaines précédentes, il avait nettoyé la citerne, étayé quelques murets autour du jardin, entreposé un peu de bois, monté un abri pour d’éventuelles poules. Il avait voulu que tout soit prêt pour l’arrivée de sa jeune épouse.

Ils avaient loué un âne pour les aider à acheminer les affaires depuis Jérusalem, en plus des lourds paquets dont ils étaient chargés. Ils avaient de la toile pour le couchage, ainsi que quelques chiffons. Ils apportaient quelques ustensiles ménagers, de fer ou de bois, louches et cuillers essentiellement, et des écuelles. Joris avait tenu à s’équiper de quelques outils qu’il estimait d’importance : marteau, clous et lames de scie. Sa sœur et son beau-frère leur avaient fait don d’une belle étoffe, dont ils se feraient des vêtements pour les jours de repos et de fête ainsi que d’un bon sac de grains de froment. Joris n’avait pas de terre où les semer, mais il espérait pouvoir rapidement aider au défrichement des espaces sauvages voisins, où les redevances seraient faibles pour les colons.

Ils avaient encore quelques monnaies, dont ils pourraient se servir pour leurs premiers repas, des pots ou des fournitures lourdes qu’ils n’avaient pu apporter. Joris avait déjà prospecté aux alentours pour se proposer à divers travaux. Il y aurait du labeur prochainement pour certains des bâtiments des frères de Saint-Jean et il avait l’intention de se doter d’une pelle voire d’une pioche pour cela. Il semblait prêt à réquisitionner toute l’énergie qu’il avait préservée dans son emploi de valet.

Ils déchargèrent leurs paquets, libérèrent l’âne qu’ils laissèrent en pâture dans le petit jardin, sa longe fixée à un maigre pistachier en bordure, puis s’assirent à terre, avalant le poisson séché, les lentilles et le pain qui les avait nourris durant tout le voyage. Joris avait tenu à puiser de l’eau dans leur citerne, qu’ils buvaient comme si c’était de l’ambroisie, les yeux brillants et la joie au cœur.

« On prendra les chapeaux neufs que j’ai tressés pour aller aux feux de ce soir, proposa Joris. Nos tenues ne sont pas tant belles qu’on risque de nous croire bien pauvres, mais au moins nous aurons belle allure.

— Avec les longues soirées, j’aurai tôt fait de nous confectionner de quoi paraître à la messe sans honte. Ne t’ayant pas sous la main pour les essayages, je craignais de couper la belle étoffe. Je n’ai pas usage de tailler si belle toile ! »

Joris sourit. Il ne l’avait pas encore sorti, mais il avait aussi prévu d’offrir à Marie un voile de fine étamine, digne d’une riche bourgeoise voire d’une femme de petit baron. Il l’avait trouvé chez un fripier à Jérusalem et l’avait longuement marchandé pour pouvoir l’acquérir. À cause de la dépense, il regrettait un peu de n’avoir pu s’acheter une pioche de fer comme il l’espérait, mais il avait envie que son épouse soit élégante et bien coiffée. Il en allait de leur statut à tous les deux après tout.

Au moment de partir de la maison, Joris s’apprêtait à verrouiller la porte quand Marie retint son geste.

« N’est-ce pas à l’épouse de tenir les clefs de l’hostel ? »

Il la lui remit de bonne grâce, avec une courbette outrée, en lui assurant qu’elle verrait bientôt son trousseau s’étoffer de celles de leurs réserves, coffres et celliers. Marie lui fit promesse de le lui rappeler, puis ajouta, d’un air plus solennel :

« Il va nous falloir bonne cave, car il se peut que nous ayons bientôt une bouche en plus à nourrir.

— Que veux-tu dire ?

— Si je suis arrivée un peu en retard ce matin, ce n’est pas dû à un souci avec mes paquets. Cela fait un moment que les aliments ne me tiennent pas au matin. Ça et puis d’autres choses…  » ❧

## Notes

Il m’a fallu un long moment pour me décider sur le titre et l’approche à donner à ce Qit’a. Mes difficultés étaient issues du fait qu’il s’agit là d’un sujet au cœur du large projet qu’est Hexagora. Ce n’est certes qu’une histoire ordinaire de personnes simples, dont la vie est marquée par leur rencontre et leur union, leur descendance à venir et leur subsistance par un travail manuel possiblement peu créatif. En gros, ce que j’imagine être l’aventure humaine vécue par pratiquement tous nos ancêtres depuis des milliers d’années. Nulle tragédie d’ampleur, aucun haut fait d’armes ni complexe projet politique. Des gens comme il s’en trouvait une écrasante majorité et dont nous ne percevons que très peu.

Quand on parle du Moyen Âge, on évoque les trois ordres : clérical, militaire et travailleur/paysan. À ce dernier, on colle parfois abusivement l’étiquette de Tiers-État, pourtant anachronique, dont l’indétermination contient toutefois en elle-même une indication quant à l’estime qui y est portée. Mais ce classement, pour commode qu’il soit afin de comprendre la société médiévale, quoiqu’hérité de l’imaginaire même des penseurs d’alors, fausse notre représentation mentale par l’impression d’égalité quantitative qu’il induit.

Les paysans, manouvriers, petits artisans formaient l’essentiel de la population, dans un ratio bien loin d’un tiers. Leur vie de labeur, simple et possiblement monotone de notre point de vue, constituait la règle. J’avais donc envie de manifester ce *commun* par un texte qui ne respecte pas trop le schéma quinaire avec sa nécessaire progression, ni le besoin de spectaculaire auquel, en tant qu’émetteur de culture, je dois parfois avoir recours pour revivifier des structures usées jusqu’à la trame. Je devais en outre bâtir sur des assises sablonneuses, cette majorité n’ayant que très exceptionnellement fait l’objet des attentions des littérateurs d’alors. Il m’a fallu extrapoler à partir de nombreuses sources matérielles, archéologiques, mais aussi ethnologiques, pour donner à voir quelque chose.

Pour l’anecdote, l’idée de cette histoire est venue de la découverte d’une charte (RRR 611) qui indiquait le règlement d’un conflit qui opposait le seigneur de Ramla aux frères de Saint-Jean de Jérusalem à propos d’une zone située entre les moulins de Mirabel et une terre « aux épines ». J’ai estimé que ce serait un bon endroit où de jeunes mariés peu argentés pouvaient bâtir leurs espoirs de lendemains heureux.


## Références

Duby Georges, *Les trois ordres ou l’imaginaire du féodalisme*, Paris, Gallimard, 1978.

Riley-Smith Jonathan (ed.), Ellenblum Ronnie, Kedar Benjamin, Shagrir Iris, Gutgarts Anna, Edbury Peter, Phillips Jonathan et Bom Myra, *Revised Regesta Regni Hierosolymitani Database* http://crusades-regesta.com/ (consulté le 24/01/2018)

