---
date: 2014-03-15
abstract: 1144-1150. Tandis que les puissants et leurs serviteurs armés vont et viennent, s’emparant des territoires et des cités comme s’il s’agissait de fruits mûrs, les populations subissent. Alors que le comté arménien d’Édesse n’en finit pas de se désagréger sous les assauts turcs, un jeune homme, fils de croisé latin et d’une arménienne, tente de survivre malgré la tourmente.
characters: Barsam, Clément, Guy, Iohannes, Kâlila le Chien, Maryam, Meskitâ, Ucs de Monthels
---

# Seul au monde

## Sarûdj, comté d’Édesse, matin du mardi 26 décembre 1144

« Sanguin[^zengi] a forcé Édesse ! Sanguin a forcé Édesse ! »

Le cavalier menait son cheval comme si la mort le traquait, galopant dans les rues, au travers des marchés, le visage tel celui d’un dément. Il hurlait tantôt en syriaque, tantôt en langue franque. Sa tenue ne laissait guère de doute : il devait appartenir à la garnison latine de la capitale du comté.

La peur, l’incompréhension, la panique jaillissaient sur son passage, comme des flammes dans un amas de broussailles. Parmi la foule, un jeune homme, Iohannes, pinça les lèvres, qu’il avait déjà fines et ferma les yeux, catastrophé. Il stoppa un instant, afin de voir si sa famille réagissait à l’annonce. Il était encombré de nombreux sacs et menait une vieille ânesse sur laquelle était juchée une femme âgée, sa mère, Meskitâ, emmitouflée dans ses voiles. À ses côtés se tenait son épouse, Maryam, le regard bas, avec dans ses bras leur fils, Barsam, né quelques mois plus tôt. Enfin, derrière eux, suivi de leur éternel compagnon, le chien Kâlila, marchait son père Clément, un Franc. Celui-ci se sentait suffisamment vaillant pour arborer son épée, bien que son dos puissant se soit affaissé depuis quelques années déjà ; il porta les doigts à sa bouche et siffla avec vigueur, interpellant le messager :

« Viens-tu de la cité ? Es-tu seul ? »

L’homme stoppa sa monture d’un geste brusque et la fit volter.

« J’en arrive ! Nous ne sommes que peu à nous être enfuis. Nous avons été trahis, les portes ont été décloses alors que nous tenions encore. La brèche récente ne leur avait guère laissé bon passage !

— L’amân[^aman] a-t-il été demandé et accordé ?

— Pas que je sache, l’archevêque était résolu à tenir jusqu’au retour du comte Joscelin[^joscelinII]. »

Clément le remercia d’un signe de tête et salua, avant de se retourner vers son fils, l’air contrarié.

« Je ne peux quitter la cité sans savoir ce qu’est mon frère Colin devenu…

— Père, il nous faut partir céans, les Turcs de Sanguin peuvent jaillir à tout instant. Peut-être sont-ils déjà en chemin…

— M’est avis plutôt qu’ils fêtent leur victoire et pillent les maisons de nos amis. »

Il serra le poing sur le pommeau de son arme.

« Cul Dieu ! Que faire ?

— Jurer et invoquer le nom du Seigneur en vain ne sera d’aucun usage, mon ami » lui lança sa femme, les yeux indulgents, mais le visage défait.

Il acquiesça en silence. Autour d’eux, la rue s’était vidée, les habitants de la ville se préparaient à un assaut, en s’enfermant et en priant, ou en rassemblant quelques affaires de valeur pour aller se cacher au loin. Se grattant la tête comme s’il pouvait en activer le fonctionnement, Clément finit par déclarer :

« Partez pour Bilé[^bile]. Il n’y a pas vingt lieues à marcher. Vous devriez y arriver demain ou dans deux jours au plus tard. Vous m’y attendrez. Peut-être Colin voudra-t-il se joindre à nous… Au moins j’en saurai plus.

— Nous ne pouvons partir sans vous, père, nous risquons de ne jamais vous revoir, répliqua Iohannes.

— Fais comme je l’ai dit, fils, répliqua le vieil homme, d’un air contrarié. Il ne t’appartient pas de discutailler en semblable moment. Je te confie ta mère, et veille sur ta famille. »

Après une rapide étreinte à sa femme, Clément partit d’un bon pas, sans un regard en arrière. Iohannes soupira, esquissa un timide sourire à Maryam et reprit le chemin de la porte occidentale, vers le plateau en direction de Bilé et de son pont sur l’Euphrate. Kâlila hésita un instant, la truffe indécise, puis disparut à la suite de son vieux maître en deux bonds agiles.

## Édesse, midi du samedi 2 novembre 1146

Maryam sourit doucement à son époux tout en balançant Barsam contre son sein. Elle était trop effrayée pour agir. Dehors, tout était calme dans le quartier, rien ne laissait penser que les Turcs étaient de nouveau dans la ville. De toute façon, ils ne pouvaient encore piller ce que les hommes de Joscelin avaient pris quelques jours auparavant. Devant elle, Iohannes faisait les cent pas, indécis.

« Peut-être que leur nouveau chef, ce Noradin[^nuraldin], ne sera pas pire que Sanguin. Ils n’en veulent qu’aux envahisseurs francs après tout…

— Crois-tu vraiment qu’il goûtera que certains aient ouvert les portes au comte ?

— Il a besoin de nous, il ne peut faire esclave tous les marchands, artisans ou paysans… Le pays serait ruiné et de peu d’intérêt. »

Maryam leva les yeux au ciel, tétanisée de peur, incapable de répondre. Iohannes hésita un instant avant de continuer :

« Nous n’aurions pas dû revenir, ni surtout, nous séparer ! »

Il ferma les yeux, grimaçant comme si la douleur qu’il éprouvait en esprit affectait son corps entier.

« Il me faut aller chercher mère. Je ne peux la laisser seule en sa demeure, père me l’a confiée. »

Il s’approcha de la porte, marqua un temps avant de retourner embrasser son épouse. Des larmes coulaient des yeux sombres de Maryam, perles délicates qui mouraient sur ses joues. Il lui caressa le visage, confiant.

« Tire fort la barre derrière-moi, je ne serai guère long. »

Tandis qu’il franchissait le seuil, il n’entendit pas Maryam lâcher dans un souffle :

« Ne m’abandonne pas, je t’en prie. »

Il n’avait pas fait deux pas que la porte claquait derrière lui.

Avançant avec précaution, Iohannes se penchait à chaque croisement pour vérifier qu’aucun soldat turc n’était dans les parages, puis il courait jusqu’à la plus proche ruelle, cherchant à éviter les grands axes. Une fois seulement, il entendit des cris, des braillements, des invectives, le chaos d’un affrontement. Il s’activa d’autant plus, rasant les murs comme un chat en maraude.

Arrivé devant la courette donnant accès à la maison de ses parents, il frappa du poing, en se faisant connaître. Une éternité s’écoula avant que sa mère ne vienne lui ouvrir. Elle était armée d’un long couteau. Le soulagement se lisait sur son visage tandis qu’elle attrapait quelques affaires pour suivre son fils. Tout au long du trajet, elle ne lui lâcha la main à aucun moment. Elle retint un cri d’horreur lorsqu’il leur fallut enjamber plusieurs corps, entassés de façon grotesque dans une mare écarlate. Des soldats francs en armure, sûrement des sergents du comte, dont les têtes coupées avaient été amassées en un pitoyable trophée attirant les mouches et les chiens errants. De la fumée s’élevait des environs de la citadelle. Le fer, le feu et le sang.

Il ne semblait au jeune homme qu’il n’avait connu que ça depuis qu’il était né. Lorsqu’ils parvinrent dans son quartier, son cœur s’arrêta : des Turkmènes s’éloignaient en braillant, de nombreux objets sans valeur éparpillés au long de la rue dans leur sillage. Il courut jusqu’à sa maison. La porte en était fracassée. Des pleurs s’en échappaient. Au milieu de la pièce principale, assis parmi les vestiges de tous ses biens se tenait un enfant, son enfant, Barsam, hurlant de peur, un morceau d’étoffe déchiré à la main, seul.

## Route de Joha, après-midi, mardi 22 août 1150

Le nuage de poussière était si dense qu’il fallait se protéger la bouche pour ne pas en avaler à chaque bouffée d’air. Les arbres, les hommes, les bêtes, tout était recouvert d’un film cendreux, pénétrant dans les narines, les oreilles, les yeux. Depuis plusieurs lieues Iohannes portait son fils à demi endormi, trop épuisé désormais pour demander à boire comme il le faisait jusqu’alors. Hagard, le petit fixait sa grand-mère Meskitâ avancer d’un pas lourd, appuyée sur son bâton. La bouche protégée de son voile, elle marmonnait pour elle-même, des prières pour en appeler à l’aide divine ou des imprécations contre tous ces soldats qui les jetaient une nouvelle fois sur les chemins.

De temps à autre, ils étaient bousculés par les cavaliers qui venaient repousser quelques Turcs agressifs. Mais ils ne s’en occupaient guère, trop épuisés, trop fatigués, brisés par des années de combat, de pillage, de fuite, où l’ennemi avait eu tous les visages. Ils se contentaient de suivre la colonne de réfugiés en direction d’Antioche.

Le comté était définitivement perdu, Joscelin avait été pris, emmené au loin dans une geôle sarrasine. Le jeune roi de Jérusalem, Baudoin, était venu négocier avec le basileus grec[^manuelcomnene] et lui avait cédé les dernières forteresses encore tenues par les Francs. Alors qu’il allait repartir, il avait eu pitié des populations livrées à elle-même. Il avait donc organisé un convoi et disposé ses hommes, ceux de la principauté d’Antioche, du comté de Tripoli, de façon à protéger les miséreux chassés de leurs foyers. Et depuis, ils marchaient sous le féroce soleil estival, harcelés par les troupes de Nūr ad-Dīn.

Une clameur agita la colonne tandis que des cavaliers ennemis s’approchaient au grand galop. Habituellement, ils lâchaient leurs traits de trop loin pour que les lourds chevaliers francs puissent les frapper sans se mettre en péril. Les arbalétriers, les archers, tentaient de répliquer, mais le temps qu’ils se déploient, les Turkmènes étaient généralement déjà repartis. Il suffisait de s’abriter derrière un muret, un arbre, un animal, et d’espérer. Les lèvres craquelées en profitaient pour avaler quelques gorgées d’eau tiède et boueuse, avant de reprendre la route.

Cette fois, les sarrasins ne décochèrent rien et ils arrivèrent au contact, brandissant leurs sabres, leurs masses, leurs lances, taillant, brisant et perçant. Leurs torses étaient recouverts d’armures métalliques et des plumes ornaient leurs casques d’acier peints. C’étaient des guerriers lourds, des Turcs d’élite, pas de simples nomades.

Iohannes attrapa sa mère et la protégea de son bras, l’emmenant à l’écart du chemin, contre un buisson où ils pourraient se lover tous les trois. Il tentait de se garder de droite et de gauche, comme un animal effrayé, perdu au milieu des destriers, des coups de lame, des corps qui tombaient. Il ne vit rien d’annonciateur à la douleur qui lui fit exploser le crâne. En un instant, les ténèbres remplacèrent la poussière.

Lorsqu’il retrouva ses esprits, Iohannes était installé sur un cheval. Balloté, il était maladroitement soutenu par quelques compagnons charitables. Il ouvrit les yeux et fut aveuglé par la lumière. Sur ses lèvres, un goût métallique déplaisant, du sang. Il porta la main à sa tête, sentit un bandage, comprit. Puis il chercha sa famille, mais n’aperçut que des visages inconnus, d’hommes de guerre fatigués, transpirant. Des Francs. À ses côtés, un des hommes qui le soutenait lui adressa un sourire, simple fente dans sa barbe enduite de poussière collée :

« Alors, l’ami, toujours vivant ? »

Iohannes voulut répondre, mais sa bouche, sa gorge, ses lèvres étaient trop sèches pour ça. L’homme lui tendit une gourde presque vide, d’un air aimable. Il en avala un peu, juste de quoi pouvoir susurrer :

« Et ma mère, mon fils ? »

L’homme le regarda, les sourcils écarquillés, une moue sur le visage.

« Tu étais seul, l’homme. On t’a trouvé quand les hommes de Triple[^triple] ont bouté fort ces maudits païens. Tu gémissais alors on t’a ramassé. Je t’ai même prêté ma selle le temps que tu te reprennes. »

Il se tourna vers un de ses compagnons, moins bien équipé.

« Tu as vu s’il y en avait d’autres encore vifs ?

— Certes non, maître Ucs, en tout cas, ni femme ni enfançon. Et ma main au feu qu’il n’y en avait aucun d’occis non plus. Ils sont peut-être plus avant en la colonne. »

Iohannes ferma les yeux, trop accablé pour réagir. Ce fut le sergent qui le rattrapa à temps :

« Prêtez main-forte, vous autres, il a encore tourné de l’œil ! » ❧

## Notes

Le comté d’Édesse fut le premier territoire d’importance perdu par les forces latines arrivées depuis la Première Croisade. Si la cité principale tomba en 1144, cela faisait déjà plusieurs années que le comte Joscelin II tentait de faire face à des adversaires de plus en plus déterminés. La perte des villes se fit comme une lente agonie, lors de laquelle les populations furent durement malmenées, selon qu’elles oscillaient vers un pouvoir ou l’autre. Majoritairement chrétiennes, elles avaient été déçues par leurs nouveaux dirigeants, mais ne pouvaient être assurées du sort qui leur serait réservé sous domination musulmane. Pourtant, des mariages mixtes avaient permis la création de nombreux liens entre indigènes et Européens croisés, la lignée comtale elle-même étant en partie d’ascendance arménienne.

## Références

Amouroux-Mourad Monique, *Le comté d’Édesse 1098-1150*, Paris : Librairie orientaliste Geuthner, 1988.

Guillaume de Tyr, *Historia rerum in partibus transmarinis gestarum*, livre 17, chap.17, Recueil des Historiens des Croisades, Occidentaux tome I, p.786
