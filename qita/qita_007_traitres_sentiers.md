---
date: 2012-05-15
abstract: 1148. L’avancée des troupes de la Seconde Croisade fut extrêmement éprouvante, pour les contingents, français comme allemands. Découverte de la réalité de cette aventure armée au loin pour un jeune homme encore bien naïf. Les plateaux anatoliens, la traversée des contreforts du Mont Cadmus, marquèrent à jamais un serviteur cheminant auprès de son maître, quelques années avant *La nef des loups*.
characters: Ganelon, Alerot, Aimon l’Englois, Régnier d’Eaucourt, Bone-amie la mule
---

# Traîtres sentiers

## Abords de Laodicée, plateaux anatoliens, nuit du samedi 3 janvier 1148

Appuyé contre un tronc nu, l’homme regardait la vapeur s’échappant de sa bouche monter vers le firmament. Les étoiles scintillaient, indifférentes. La lune n’était qu’à quelques jours de son plein et éclairait d’une lumière froide le camp et les murailles proches de Laodicée. Vers le sud, un peu à l’est, une ombre gigantesque dévorait le ciel : le mont Cadmus. Un souffle d’air glacé fit frissonner Ganelon qui baissa la tête vers le maigre feu qu’il partageait avec ses trois compagnons : Alerot le second valet, son ami Aimon l’Englois, un archer qui les assistait et les protégeait et surtout Régnier d’Eaucourt, son seigneur et maître, chevalier de l’ost du roi Louis.

Pour l’heure, il était le seul éveillé, responsable du maintien des flammes. Au moins ils arrivaient à se réchauffer, même si le bois vert et mouillé rabattait sur leurs visages une fumée suffocante chaque fois qu’ils s’en approchaient. Tous les foyers alentour produisaient une semblable colonne, éclairée d’une lumière orangée à sa base. Le camp était calme, les discussions se faisaient à voix basse, et même les bêtes, les rares montures et animaux de bât qui avaient survécu jusque-là, ne bougeaient guère. Les quelques prédateurs qu’ils avaient découverts ici, chacals dorés, hyènes rayées, ne s’aventuraient pas aussi près d’une cité comme Laodicée.

Les patrouilles craignaient bien plus les féroces Turcs qui les assaillaient à tous moments, décochant leurs flèches avant de galoper à l’abri. Ganelon tira sur ses épaules la couverture dont il s’était enveloppé, grelottant malgré ses pieds près du feu. Il aurait bien avalé un peu d’eau, histoire d’oublier sa faim et de se remplir l’estomac, mais il n’avait guère envie de bouger dans le froid. Loin de la côte, le ravitaillement faisait défaut. En fait, il semblait au valet qu’il n’avait rien mangé de correct depuis qu’ils avaient quitté sa Picardie natale. Lui qu’on appelait volontiers le Gros n’était plus que l’ombre de lui-même. Il s’était résigné à percer de nombreux trous à sa ceinture au fur et à mesure de leur avancée.

Depuis qu’ils étaient dans les territoires byzantins, la situation était devenue effroyablement difficile. Les paysans qu’ils croisaient, ceux qui n’avaient pas eu le temps de fuir, ne lui paraissaient guère différer des guerriers qui s’opposaient à leur marche. Quant aux soldats qui tenaient les villes, leur comportement était souvent agressif, hostile. Alerot disait que c’était la faute aux troupes allemandes, qui les avaient devancés sur la route. Ganelon pensait que c’était plutôt parce que ces maudits Grecs avaient partie liée avec les sarrasins. Il avait eu un moment d’espoir lorsque la rumeur avait couru que le roi allait rejoindre l’empereur de Sicile et qu’à eux deux ils prendraient la trop fière cité de Constantinople. L’évêque de Langres soutenait l’idée qu’il suffisait d’attendre à Adrianople les navires siciliens.

Ganelon avait vite déchanté quand les voiles aperçues furent celles des Byzantins chargés de transporter rapidement les unités françaises sur la côte anatolienne. Ces maudits orientaux ! Leur fourberie était désormais légendaire, et les hommes de la troupe se racontaient toutes les occasions où ils y étaient confrontés. Ils étaient supposés être des frères chrétiens, mais se comportaient comme des païens infidèles. Ils cachaient les vivres, refusaient l’entrée de leurs cités, se réfugiaient dans les montagnes à leur approche. On disait que si l’armée allemande avait été perdue, c’était parce que leurs guides l’avaient menée dans des chemins impossibles, puis qu’ils s’étaient enfuis afin de prévenir l’ennemi.

Le craquement d’une brindille dans le silence givré le fit se retourner. Leur mule, Bone-Amie, semblait intriguée par ce qu’elle voyait à ses pieds. Elle se mit à frapper du sabot et à gratter tout en tirant sur sa longe, perturbant le grand destrier du chevalier. Ganelon se leva péniblement et avança vers elle, sortant la main dans la froidure pour lui flatter l’encolure.

« Là, ma belle, demeure calme, il ne s’agit pas d’éveiller les nôtres compères. Nous avons fort parcours à suivre demain. »

Bone-Amie vint se frotter affectueusement, avec toute la délicatesse d’une bête de bât de plusieurs centaines de kilos, manquant de faire tomber le valet. Ganelon eut un sourire fugace. Il aimait les animaux, car il n’était jamais déçu par eux. Son maître avait acheté la mule pour un prix indécent, lorsqu’ils avaient débarqué du navire, mais Ganelon et Alerot s’étaient félicités de leur bonne étoile. Une telle assistance était inestimable, vu le long périple qu’ils devaient accomplir. Ils avaient vite baptisé à leur goût la courageuse bête, n’ayant pu comprendre le baragouinage du vendeur. Et tous les deux passaient une grande partie de leur temps à chercher de quoi nourrir le groupe.

Car si les hommes manquaient de vivre, les animaux en étaient réduits à dévorer des arbrisseaux gelés, des lichens et des mousses, le moindre brin d’herbe qui pointait sur le bord du chemin. L’hiver, la traversée préalable des forces allemandes et la traîtrise des Grecs avaient fait disparaître tout le fourrage. Le roi avait réuni le conseil en soirée, et ils devaient partir le lendemain en quête des habitants de la ville, réfugiés dans les montagnes, de façon à pouvoir obtenir d’eux les vivres qui leur permettraient de rejoindre Adalia.

Ganelon soupira. Il commençait à trouver le temps long. Plus de six mois avaient passé depuis qu’il avait assisté, au sein d’une immense foule, à la prédication du grand abbé Bernard de Clairvaux devant le magnifique sanctuaire de Saint-Denis, aux portes de Paris. D’où il était, il n’avait suivi le discours que grâce aux autres spectateurs, qui se répétaient de loin en loin les paroles du saint homme. Le moine était déjà légendaire, même pour un simple valet picard comme Ganelon, mais il ne lui était apparu que comme une vague silhouette blanche, perdue parmi les riches habits des dignitaires de l’Église. À leurs côtés, devant les trois énormes portails colorés, se tenaient d’imposantes figures aux vêtements bigarrés, grands barons, comtes et le roi Louis en personne.

Ganelon avait été gagné par la ferveur religieuse et s’était senti investi d’une mission divine. Le départ avait empli son coeur d’allégresse, même s’il ne faisait que suivre son maître. Il lui semblait avoir sa part à jouer dans les événements à venir. Des centaines et des centaines de lieues plus tard, il en était là, après avoir escaladé des montagnes, franchi des mers et survécu à des batailles. Dans le froid, sous la lune au milieu d’une foule endormie. Avec une mule pour seule compagnie.

## Mont Cadmus, jeudi 8 janvier 1148

Le froid n’était pas suffisant pour geler le sol et l’étroit sentier n’était que boue et pierres amoncelées. Les marcheurs hésitaient, claudiquant d’un pas à l’autre. Sabots et chaussures dérapaient pareillement. Les plus chanceux trébuchaient à peine, les malheureux glissaient jusqu’à un précipice, qui avalait gens et bêtes sans distinction. Les rares zones plus faciles recelaient des bandes de pillards turcs, embusqués derrière les rochers, qui tiraient leurs traits assassins sur tout ce qui s’aventurait à leur portée. Perdu dans les nuages accrochés au relief accidenté de ce haut passage, le convoi avançait tellement lentement que chacun finissait par croire qu’il attendait plus qu’il ne marchait. Ganelon et Alerot s’aidaient de bâtons et progressaient avec quelques autres, protégés par une douzaine d’archers et d’hommes équipés de boucliers. Enviés au départ, ces derniers haletaient plus fort que leurs compagnons de devoir soulever régulièrement leurs larges pavois. Mais les nombreux tronçons de fûts et fers plantés dans le bois témoignaient de l’importance de leur bagage.

Une clameur monta de l’arrière. Rapidement, les écus se placèrent en ligne avec les tireurs derrière. Parmi eux, Aimon soufflait sur ses doigts gelés, rendus douloureux par les gerçures. Les flèches étaient encochées, les regards inquiets attendaient de voir apparaître un éventuel ennemi émerger des brumes cotonneuses. Le vacarme se fit plus fort et un frémissement fut perceptible dans la colonne en contrebas : des cris de bataille, des hennissements, le son des corps s’écroulant, de la foule prise de panique, des rochers traîtres qui roulaient. Puis jaillirent plusieurs cavaliers sur leurs petits chevaux nerveux, leurs tresses volant dans le vent. Ils portaient le chapeau à bord de fourrure que Ganelon n’avait que trop vu depuis quelque temps. Taillant de leurs sabres ou décochant des flèches rapides de leurs arcs courbes, il forçaient le passage plus qu’ils ne cherchaient à tuer. Il sembla à Ganelon qu’ils fuyaient ou du moins tentaient de s’éloigner. Il ne put retenir un cri :

« Dieu le Veut ! Hardi compaings, frappez-les sans crainte ! Percez-les de vos fers ! »

Quelques archers décochèrent, mais de nombreux croisés se trouvaient autour des Turcs. Ceux-ci approchaient tant bien que mal, talonnant leurs montures sans ménagement. Ils n’étaient plus qu’à une trentaine de pas lorsque Ganelon comprit pourquoi ils gravissaient le chemin : des cavaliers francs les poursuivaient de près, frappant de la lance les retardataires, piétinant les corps de ceux qui avaient glissé de leur selle. Régnier d’Eaucourt était avec l’avant-garde, sous le commandement de l’oncle du roi, mais il aurait aimé sans nul doute voir un tel spectacle. Cela redonna du courage à la petite troupe. Ils se sentaient prêts à affronter l’ennemi.

Le premier turc qui s’approcha reçut un trait dans le bras, ce qui ne l’empêcha pas de continuer sa route et de rejoindre une sente abrupte qui montait droit vers le sommet. Plusieurs cavaliers le suivirent de près, dont certains avaient encore des flèches en main. Profitant de ce qu’on faisait moins attention à eux, ils tirèrent quelques volées rapides avant de s’évanouir dans la végétation à flanc de montagne, projetant des gerbes de pierraille tandis qu’ils avançaient. Instinctivement, Ganelon avait plongé derrière Bone-Amie, et se sentit honteux lorsqu’il se releva et aperçut un empennage dépasser des bâts.

Il enfonça la main pour voir si la blessure était importante, mais découvrit que le fer était planté dans un sac. Il se mit à rire et se retourna vers Alerot, hilare. Son compagnon ne partagea jamais son moment d’euphorie, il était penché au-dessus d’Aimon, frappé d’une flèche à la poitrine, crachant du sang. Il avait perdu connaissance, peut-être en s’écroulant au sol. Ganelon voulut leur porter secours, mais les cavaliers en grand haubert de mailles, porteurs de la croix sur l’épaule, s’approchèrent, hurlant :

« Avant ! Avant ! Ne faites pas halte ! Il faut passer outre le col au plus vite ! »

Ganelon secoua la tête et montra son compagnon blessé :

« Il nous faut panser notre compaing, il vient de recevoir navrure parmi le corps, il crache du sang… »

Un des chevaliers avança sa monture et hurla d’un air contrarié :

« Mordiable ! Vas-tu obéir, foutre vérolé ! D’autres attendent en aval, et tu occupes la sente !

— Et comment je fais ? Je ne peux l’abandonner ici…

— Porte-le sur ta mule ou tes épaules, je m’en gabe, avance ou je m’arrange pour que tu lui tiennes compagnie, en pareil état ! »

Seul le regard inflexible de l’homme était visible, recouvert qu’il était d’acier des pieds à la tête, sa ventaille rabattue sur le bas du visage et un casque à nasal sur la tête. Il avait en main une lame rouge de sang à peine séché, brandie d’un air menaçant. Sa monture elle-même était nerveuse, remuant en tout sens, les yeux affolés. Résigné, le petit groupe se mit en branle, Ganelon et un autre soldat se penchèrent pour prendre Aimon avec eux, mais celui-ci éructa des bulles écarlates, manquant de s’étouffer. Alerot, qui tenait la longe de Bone-Amie intervint :

« Il ne faut certes pas le mouvoir ainsi, il y a grand risque de l’occire pour de bon…

— Il faudrait au moins arracher fer et fût, non ?

— Comment donc ? Je ne m’y entends guère en chirurgie !

— Et moi donc ? Nous ne pouvons tout de même pas l’abandonner ici… »

Tandis qu’ils discutaient, la colonne avait repris son avance et ils gênaient le passage, s’attirant des regards désapprobateurs des marcheurs épuisés et inquiets. Alerot commençait à progresser, entrainé par Bone-Amie qui suivait la file.

« Il me faut continuer, je ne peux barrer le passage, essayez de le porter à deux. »

Ganelon lança un regard désolé au soldat resté pour l’aider et ils tentèrent de soulever une nouvelle fois Aimon. Le flot de sang fut tel qu’ils le lâchèrent presque brutalement sur le sol. Ganelon se frottait le visage, comme s’il essayait de fuir un mauvais rêve. Son compagnon le dévisagea, d’un air contrit.

« Il n’y a rien qu’on puisse faire, compère. Il n’y a guère d’espoir pour lui. Sans un habile chirurgien, il passera d’ici peu. Rejoignons les autres avant qu’ils ne soient trop loin. »

Ganelon lança à l’homme un regard empli de haine et de colère.

« Tu veux qu’on l’abandonne, comme un chien crevé dans une fosse ?

— Peut-être une bonne âme saura le soigner parmi nos suivants. Pas nous. Et si nous restons là, nous risquons nos vies aussi. »

Ganelon hocha la tête, consterné par l’horrible évidence.

« Aide-moi au moins à l’installer au mieux sur le côté. Enroulons-le dans une couverture. »

Lorsqu’ils reprirent leur avancée, d’un pas aussi rapide qu’ils le pouvaient, pour rejoindre Alerot, Ganelon sentait les larmes lui embrouiller les yeux et s’écouler sur ses joues. Il ruminait en son for intérieur, déchiré de n’avoir pu rien faire. Tandis qu’il posait un pied puis l’autre, comme un somnambule, il passa auprès du corps mutilé d’un guerrier turc tombé. Quelques croisés se dépêchaient de le dépouiller de ses affaires de leurs mains fiévreuses. Rageur, Ganelon asséna en passant un coup de pied au cadavre à demi nu :

« Honnis soient ces Orientaux et leur pays maudit » ❧

## Notes

Le passage des reliefs anatoliens constitua une des épreuves les plus terribles pour les contingents franco-allemands de la Seconde croisade en 1147-1148. Arrivées les premières, les forces de Conrad furent d’ailleurs décimées et il dut faire demi-tour, physiquement très affecté. Le roi Louis de France lui-même n’en réchappa que par ses capacités de guerrier et grâce au fait que ses assaillants ne l’avaient certainement pas reconnu.

Le passage aux alentours du Mont Cadmus constitua l’un des moments parmi les plus difficiles pour les hommes, le train de bagages s’étant retrouvé assez mal protégé en raison de soucis de commandement à l’avant. Malgré cela, la vaillance de l’arrière-garde et du roi permirent de retourner la situation et l’armée française réussit à passer le col et à se rassembler sur le versant opposé.

## Références

Eudes de Deuil, *La croisade de Louis VII, roi de France*, publiée par Henri Waqet, Paris:Geuthner, 1949. Une version électronique de la version de Guizot (1824) existe à l’adresse : <http://remacle.org/bloodwolf/historiens/odondedeuil/table.htm> (consultée le 8 juillet 2011).

Nicolle David, *The Second Crusade 1148: Disaster Outside Damascus*, Londres : Osprey Publishing, 2009.

Nicolle David, Kervran Yann (trad., adapt.), *La Seconde croisade et le siège de Damas en 1148 - Un pari manqué*, à paraître.
