---
date : 2020-12-15
abstract : 1148-1161. Aigrie par la dureté de sa vie, la vieille Ghadir offre à tous l’aspect familier de la mégère acariâtre. Mais peut-on la résumer à l’apparence qu’elle performe jour après jour ?
characters :  Ghadir, Tha’lab al-Fayyad, Abdul Yasu, Maryam de Jérusalem, Tahir, Jabir, Bankala
---

# La raison des femmes

## Jérusalem, quartier de la porte d’Hérode, après-midi du mercredi 7 avril 1148

Peu désireuse de se mélanger aux servants qui avaient accompagné son maître lors de son déplacement, Ghadir demeurait à l’entrée de son refuge, la cuisine, faisant mine de balayer le sol d’un vieil amas de brindilles. Ce faisant, elle ne manquait rien de ce qui déroulait dans la cour sans avoir à s’y mêler, accumulant de quoi nourrir sa verve à propos de l’incompétence des employés loués à la tâche, de l’inconséquence des jeunes et, de façon générale, de l’inaptitude fondamentale de tous les hommes. Assis à ses côtés dans une posture tout aussi attentive, Bankala se pourléchait du bon repas qu’elle venait de lui offrir. Elle avait dressé le chien à lui obéir au doigt et à l’œil, portant sur son dos des paniers lors de sa tournée des souks ou veillant d’un grognement à ce que nul ne l’aborde quand elle n’en avait pas envie.

Les valets occupés à décharger les balles dans l’entrepôt connaissaient l’irascible gouvernante et certains mollets gardaient un souvenir cuisant de leur rencontre avec Bankala. Ils prennaient donc soin de demeurer discrets tout en accomplissant leur tâche au plus vite. Ceux qui avaient pris part au voyage étaient de toute façon impatients de rentrer chez eux, maintenant que la nouvelle de leur retour était certainement parvenue jusqu’à leur demeure.

Le maître de maison, Tha’lab al-Fayyad était un négociant au long cours. Bien que vieillissant, il tenait à faire au moins un déplacement chaque année, mais il s’aventurait de moins en moins loin, choisissant ses escales et ses destinations parmi son cercle d’intimes où il savait pouvoir loger confortablement. Il était donc resté depuis le début d’année sur la côte, goûtant la clémence des cieux riverains de la Méditerranée.

Comme il était veuf depuis de nombreuses années, Ghadir avait géré la demeure en son absence, n’ayant que la charge du jeune Abdul Yasu. Celui-ci profitait du départ de son père pour vivre de façon encore plus licencieuse qu’à l’accoutumée, malgré les récriminations de la vieille femme. Il savait néanmoins qu’elle ne rapportait rien de ses agissements et qu’il fallait lui arracher la moindre confidence sur ce qui se passait dans la maison ; pour être acariâtre, elle n’en était pas pour autant commère. Elle réservait la primeur de ses épanchements au molosse qui ne la quittait jamais, préférant visiblement la compagnie d’un canidé muet et obéissant à celle de ses contemporains.

Déterminée à s’installer à l’abri, mais face à la porte, soucieuse de ne pas interrompre sa surveillance lorsqu’elle entamerait la préparation du repas, elle se retirait dans sa cuisine quand elle entendit un appel du vieux Tha’lab résonner dans les couloirs. Soupirant, elle abandonna sa vigie et s’en fut d’un pas trainant, ajustant ses vêtements afin de paraître décente à son employeur. Pour le côtoyer depuis plusieurs dizaines d’années, elle ne s’accordait certainement pas la licence de se présenter sans avoir conformé son apparence à ses propres critères de respectabilité : un voile impeccablement fixé et une mine contrite. Lorsqu’elle pénétra dans le majlis où il se tenait, elle le découvrit les sourcils froncés, mains sur les hanches.

« Qu’est-ce donc ? Que s’est-il passé ? Le khamaseen[^khamaseen] a-t-il soufflé en mon absence ? »

Ghadir baissa les yeux révérencieusement une fraction de seconde puis dévisagea son maître. Elle avait déplacé tous les meubles, suffah et tapis dans la pièce, tâche qu’elle repoussait depuis des années bien qu’elle l’estimât essentielle. Elle y avait consacré le temps nécessaire pour faire les choses de façon idéale.

« Rien ici n’était organisé comme il seyait, abu… Sa voix se fit moins forte, presque hésitante. J’ai entendu bien méchantes remarques des domestiques voisins, qui répétaient ce qui se disait. Ce majlis ne te faisait pas honneur, j’en ai pris la mesure, car c’est là mon devoir de te servir correctement. »

Tha’lad al-Fayyad se rengorgea, faisant trembler son goitre tandis qu’il regardait les lieux d’un œil neuf. Il se frotta le visage un instant, se gratta le nez.

« Tu as agi avec sagesse, mais tu n’as pas à décider ainsi sans mon consentement. C’est ma demeure et nul ne doit savoir que je n’ai pas voulu l’arrangement de la pièce où je reçois.

— Tu as déjà tant à faire, abu. J’ai désiré t’épargner cette tâche qui n’est pas de ton rang.

— Ni du tien ! C’était là celui de ma pauvre épouse, que Dieu la bénisse. »

Ghadir adopta une posture contrite, faisant le dos rond avec le naturel né d’une pratique ancienne.

« Souhaites-tu que je remette tout comme avant ?

— Non, non. Je n’aime pas qu’on bouge les choses autour de moi. Nous allons voir ce que ça donne et nous arrangerons ce qui me déplaît. Mais plus de telles initiatives, compris ? »

Puis il la chassa d’un geste de la main, la renvoyant à ses occupations domestiques tandis qu’il s’installait sur les coussins, certainement pour une courte sieste comme il les affectionnait en fin de journée, avant le souper.

À peine de retour dans son univers, Ghadir alla vérifier où en étaient les valets et maugréa de constater qu’ils avaient fini de tout décharger. Elle ne pourrait donc pas diriger sa vindicte vers eux. Elle se contenta d’une caresse au chien et alla chercher dans la réserve de quoi préparer le repas du soir. Elle avait prévu originellement de faire des boulettes aux légumes, mais Tha’lab appréciait l’agneau par-dessus tout. Elle pourrait peut-être en emprunter un morceau à une des voisines. Et elle avait le temps de faire quelques pâtisseries à la pistache et au miel si elle se dépêchait. Cela réconforterait Tha’lab du voyage.

Tout en s’activant pour concevoir un repas attrayant à son maître, elle bougonnait. Malgré ses récriminations, le vieux ne doutait pas qu’elle ait pris la bonne décision, comme toujours. Les hommes étaient incapables de prendre soin d’un foyer ou d’en faire un lieu accueillant, tout le monde savait ça. Ils s’attachaient à tout critiquer car ils avaient besoin de manifester leur autorité. Mais nul n’ignorait que la maison, dans cette famille, c’était le domaine réservé de Ghadir. 

## Encharim, casal d’al-Najjar, matin du jeudi 26 décembre 1157

Confortablement confinée dans la grande salle, la famille du vieil al-Ajjar profitait des fêtes pour se rassembler. Cousins lointains, frères et sœurs distants, tout le monde avait fait le voyage pour retrouver une nouvelle fois le vieil homme, conteur émérite, dont la diction approximative et chuintante n’entamait en rien son pouvoir d’évocation. Pendant une semaine, le moindre recoin bruissait des jeux des plus jeunes et des conversations des adultes. Près du feu, les femmes s’activaient chaque jour à préparer des plats d’exception, puisant dans les réserves accumulées depuis des mois pour s’octroyer le plaisir de plantureux repas.

Ghadir aidait une de ses nièces à ajuster grossièrement les vêtements des enfants, se contentant de rapides ourlets pour raccourcir une robe ou une manche. Tout en tirant l’aiguille, elle faisait part de son insatisfaction vis-à-vis de ces jeunes qui ne savaient pas prendre soin de leurs affaires et qui auraient mérité d’aller nus par les rues, comme les animaux qu’ils étaient. Seuls les plus éloignés de la famille, ou les plus fiers des parents, étaient choqués de ses propos. Ce fut son frère, Jabir, qui l’apostropha un peu sèchement depuis le coin des hommes alors qu’elle morigénait un de ses petits-fils.

« Laisse donc Tahir. Tu as beau jeu de tancer ainsi garçonnet si peu en âge !

— Gare, elle pourrait tout aussi bien m’en prendre à toi, ricana un de ses voisins.

— Je réprimande comme il convient pour éduquer ces petits, n’ayant pas la chance d’avoir les miens à choyer. Je fais ma part pour aider ceux-ci.

— En les grondant à propos de toute chose ? Tu as l’affection bien acide, ma sœur.

— Je suis tel que la vie m’a faite. J’ai eu mon content de malheur. »

Elle poussa un soupir à faire vibrer les murs, évoquant par cette manifestation sonore son veuvage précoce et la perte de son enfant puis le désaveu de sa belle-famille qui avait suivis. Aucun de ses proches n’ignorait son infortune, pas plus que quiconque dans son voisinage. Mais Jabir subissait les diatribes de Ghadir depuis trop longtemps pour s’en laisser attendrir. Il estimait qu’elle s’était toujours comportée ainsi, revêche et acrimonieuse. Et il ne voulait pas rater une occasion de montrer à tous ici qu’il était son aîné en plus d’être un homme.

« N’est-ce pas toi qui a tout fait pour aller minauder en la cité ? À ne rien réclamer que t’installer à Jérusalem ? Père a beaucoup renâclé à te laisser épouser un melkite[^melkite]. Ces gens-là n’ont pas d’honneur. Maintenant tu moissonnes ce que tu as semé, ivraie incluse. »

Ghadir baissa le front. Elle ne voulait pas faire d’esclandre, préférant régler ses comptes à sa façon. Jabir était fanfaron imbu de lui-même, elle n’obtiendrait que sa colère, voire ses coups, si elle le provoquait ici. Mais il n’y aurait pas toujours du public et il aurait besoin d’elle un jour. Il serait alors bien temps de lui faire souvenir de son comportement. Lui qui avait hérité de tout, sans rien faire, et qui n’avait eu qu’à reprendre le florissant atelier de charpente familial.

Une de ses jeunes nièces, qui tirait le fil à ses côtés lui adressa un sourire encourageant et y ajouta à mi-voix :

« Tu sais, ma tante, il y aura toujours une place pour toi en notre demeure, quoi qu’en dise Jabir. Il ne laisserait jamais sa propre sœur dans la rue si tu venais frapper à sa porte. »

Ghadir lui retourna un regard épouvanté.

« Revenir parmi poules et mulets ? Mieux vaut encore ces paons de Celtes[^celte] de la ville ! »

## Jérusalem, maison de bains, matin du mardi 21 avril 1159

Le mardi était réservé aux femmes dans le hammam que fréquentait Ghadir. Elle ne s’y rendait pas toutes les semaines, mais aimait s’accorder ce moment de plaisir chaque fois qu’elle s’autorisait un peu de relâchement dans son emploi du temps. Elle en profitait chaque fois pour s’offrir quelques pâtisseries sur le retour, l’égoïsme renforçant la volupté des confiseries. Elle avait en effet un peu de trajet à faire, préférant fréquenter l’établissement qu’elle avait connu lors de son mariage à celui, moins éloigné, qui était rattaché à son quartier.

Elle avait sa place depuis des années, installée sur un muret derrière une petite colonne, un peu en retrait. Elle n’aimait pas se trouver à la vue d’autrui et choisissait la discrétion des abords et des recoins. De là elle pouvait s’employer à regarder les autres, à les juger en silence, activité qui lui convenait fort bien. Elle profitait de temps à autre de la présence d’une oreille pas trop loin d’elle pour s’épancher à propos de ce qui nourrissait sa rancœur de l’instant. Mais les habituées étaient méfiantes et seules les néophytes s’approchaient généralement.

Elle avait cette fois-là découvert une compagne de malheur de son âge, qui compatissait d’autant mieux qu’elle n’écoutait guère, exploitant juste les moments où elle pouvait insérer ses propres griefs dans la logorrhée de Ghadir. Elles se rejoignaient néanmoins sur leur commune détestation des jeunes femmes, qui ajoutaient à l’indécence un irrespect total envers leurs personnes.

« Cela fait des années que je sers bien mon maître, sans jamais rien demander pour moi. Ni me plaindre de mon sort, insistait Ghadir. Il avait même dit qu’il ferait de moi sa concubine, si ce n’est son épouse. Mais les années filent et je ne vois rien changer. »

Elle se rapprocha de sa confidente, vérifiant qu’aucune oreille juvénile ennemie ne trainait aux alentours.

« Et voilà qu’il y a la fille de son petit dernier qui se prend pour la maîtresse de la maison. Elle ordonne, organise, demande les clefs et inspecte les réserves ! Comme si cette demeure était sienne, et pas celle de Tha’lab. Aucun respect pour ses aînés !

— Et il ne sévit pas ? Il tolère de se laisser mener ainsi ? s’horrifia l’autre. Moi, mon Ahmed n’aurait jamais accepté ça ! D’ailleurs…

— Il a trop bon cœur, voilà son problème. Le fils en profite et elle tient celui-ci par ses minauderies. Mais cela ne prend pas avec moi ! Oh ça non… J’aurai tôt fait de la mettre au pas ! »

Elle remâcha sa colère un instant, qui fut suffisant pour sa voisine embraye sur ses propres soucis.

« C’est toujours pareil. Il s’en faut d’un rien pour que les hommes fassent n’importe quoi ! Mon pauvre Ahmed, lui, était bien sage comparé à tous ces fous. Jamais il n’aurait traité sa mère comme mes fils le font ! J’aurais su combien ils se montreraient peu reconnaissants, j’en aurais bien moins fait pour eux.

— Et encore, toi tu as la chance de pouvoir réformer tes garçons. Moi je n’ai plus rien, Dieu m’a tout pris, époux et enfant. Je n’ai que mon pauvre Bankala !

— Ils nous usent jusqu’à la moelle, puis nous oublient. Des ingrats, voilà ce qu’ils sont ! Si au moins j’avais eu une fille ! Je l’aurais gardée pour mes vieux jours. Elle m’aiderait bien, en ces temps difficiles.

— Les jeunes, elles n’ont rien dans la tête. Pas comme nous à leur âge. Moi j’ai toujours été bonne enfant : jamais ma mère n’a eu à se plaindre. Elle n’avait que prières à la bouche quand elle parlait de moi. »

Poursuivant leurs monologues en duo, les deux vieilles continuèrent un moment leur conciliabule discret, recroquevillées dans la noirceur de leur alcôve, noyées dans les chaudes brumes du hammam. Puis elles se saluèrent courtoisement et se séparèrent, un peu soulagées quoiqu’insatisfaite l’une et l’autre de voir que nulle ne prêtait attention à leurs paroles emplies de sagesse.

## Jérusalem, quartier de la porte d’Hérode, midi du jeudi 6 octobre 1160

Abdul Yasu rentrait d’une course matinale lorsqu’il entendit du bruit dans sa chambre, où dormait généralement sa fille. Il reconnut une berceuse fredonnée, qui lui rappelait des souvenirs. Le sourire sur les lèvres, il s’apprêta à surprendre sa femme, certainement occupée à calmer ou nourrir leur petit enfant. Il marqua donc un temps d’arrêt quand il découvrit le dos tordu de Ghadir qui balançait délicatement Maryam en chantonnant un air qui lui était familier. Étonné de voir la vieille gouvernante exprimer autant de douceur, il s’appuya, attendri, contre le chambranle de la porte pour admirer la scène. Ghadir mignotait le nourrisson avec une telle prévenance qu’il se demandait si un djinn n’avait pas lancé un sort sur elle. Ill aurait observé un lion lécher un agneau il n’aurait pas été moins surpris.

Tournant sur elle-même, Ghadir finit par se présenter de côté et découvrit sa présence. Il s’en fallut de peu qu’elle ne sursautât, arrêtant immédiatement sa chanson tandis que ses commissures de lèvres retombaient avec lourdeur. Elle le foudroya du regard, mais sans pour autant lâcher l’enfant.

« Ta femme néglige ses devoirs, j’ai entendu la petite qui pleurait. Je pense qu’elle a faim.

— Est-ce pour cela que tu lui fredonnais une berceuse ? Pour la rassasier ?

— Je faisais ce qu’il y avait à faire, vu qu’en dehors de moi, personne ne se soucie des autres dans cette maison !

— Allons donc ! J’ai bien reconnu. N’était-ce pas ce que tu me chantais, enfant ?

— Je ne sais pas, je n’ai jamais été douée pour les berceuses. C’est la première chose qui m’est venue en tête ! »

Abdul Yasu ricana. Il était un des rares à se montrer imperméable à sa mauvaise humeur, ce qui énervait d’autant plus Ghadir. Il ne pouvait pourtant retenir un sourire narquois qui en accroissait chaque fois la violence. Elle lui posa le bébé dans les bras sans ménagement.

« Tiens, plutôt que de moquer une vieille femme aux membres fatigués, va donc le donner à ton épouse.

— Où est-elle ?

— Aucune idée. Sûrement à fouiner quelque part… »

Un peu encombré par l’enfant, Abdul Yasu ne se poussa pas de la porte, fixant Ghadir avec désinvolture.

« Tout de même, cela faisait bien des années que je ne t’avais pas entendue chanter. Cette enfant te procure du bonheur aussi, avoue-le donc.

— Surtout du travail en plus, maugréa-t-elle.

— N’y a-t-il rien qui te redonne le sourire ?

— C’est comme ça depuis que je suis née. Déjà lors, tout le monde était en joie autour de moi tandis que j’étais dans les pleurs ! » ❧

## Notes

Ghadir représente un des figures les plus complexes que j’avais à brosser, et cela explique pourquoi on ne l’avait aperçue qu’en ombre jusqu’à présent. Pour l’esquisser, j’ai fait un mélange de *Vieux Fusil*, le personnage campé par Biyouna dans *La source des femmes* assaisonné de *Tatie Danièle* interprétée par Tsilla Chelton dans le film éponyme. Cela me semblait correspondre à tous les topos que j’avais pu retrouver dans la littérature autour des femmes un peu âgées  dans le moyen-orient d’alors.

Si on leur concède volontiers une aisance à l’oral, elles n’ont que peu d’espace pour la déployer et doivent se restreindre dans leur liberté de parole qui doit subir l’autorité masculine, du moins de façon formelle. L’autre aspect intéressant du personnage dépeint par les sources est le fait que les femmes âgées sont représentées comme une malédiction pour leur entourage. Surtout si elles n’ont pas accompli leur devoir de procréer des fils… Il y avait là je trouvais un cocktail passionnant pour tracer un caractère riche et complexe qui, pour n’être pas forcément sympathique d’abord, pouvait donner plus à voir que ces rapides traits, tant décriés sous la plume des auteurs classiques et des correspondances privées.

Le titre vient d’un dicton arabe, dont le propos sexiste m’avait suffisamment frappé pour que naisse en moi l’envie de le faire mentir : *la raison des femmes, c'est la folie ; leur religion, c'est l'amour*. C’est également un renvoi à l’ouvrage de même nom de la philosophe Geneviève Fraisse.

## Références

Benkheira Mohammed Hocine, « Hammam, nudité et ordre moral dans l’islam médiéval (I) », *Revue de l’histoire des religions*, 2007, vol. 224, num. 3, p. 319‑371.

Benkheira Mohammed Hocine, « Hammam, nudité et ordre moral dans l’islam médiéval (II) », *Revue de l’histoire des religions*, 2008, vol. 225, num. 1, p. 75‑128.

Cortese Delia, Calderini Simonetta, *Women and the Fatimids in the World of Islam*, Edinburgh, Edinburgh University Press, 2006, vol. 1.

Fraisse Geneviève, *La raison des femmes*, Paris : Plon, 1992.

Goitein Shelomo Dov, *A Mediterranean society; the Jewish communities of the Arab world as portrayed in the documents of the Cairo Geniza*, Berkeley, University of California Press, 1967-1993, vol. Volume 1 à 6.

Marin Manuela, Deguilhem Randi (dir.), *Writing the feminine. Women in arab sources*, Londres - New York, I.B. Tauris, 2002, vol. 1.

Myrne Pernilla, Narrative, *Gender and Authority in ‘Abbāsid Literature on Women*, Västerås, Edita Vastra Aros AB, 2010, vol. 22.

Tillier Mathieu, « Women before the qāḍī under the Abbasids », *Islamic Law and Society*, 01/01/2009, vol. 16, num. 3‑4, p. 280‑301.

Spectorsky Susan A., *Women in Classical Islamic Law. A survey of the Sources*, Leiden - Boston, Brill, 2010, vol. 5.
