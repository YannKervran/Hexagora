\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Jouvences}{1}{chapter.0.1}%
\contentsline {section}{\numberline {}Jérusalem, matin du mardi~16~avril~1157}{1}{section.0.1.1}%
\contentsline {section}{\numberline {}Mahomeriola, fin de matinée du dimanche~8~septembre~1157}{4}{section.0.1.2}%
\contentsline {section}{\numberline {}Jérusalem, midi du jeudi~6~février~1158}{6}{section.0.1.3}%
\contentsline {section}{\numberline {}Notes}{9}{section.0.1.4}%
\contentsline {section}{\numberline {}Références}{10}{section.0.1.5}%
\contentsline {chapter}{\numberline {2}Frères ennemis}{11}{chapter.0.2}%
\contentsline {section}{\numberline {}Jérusalem, après-midi du vendredi~19~juillet~1157}{11}{section.0.2.1}%
\contentsline {section}{\numberline {}Jérusalem, soirée du mardi~23~juillet~1157}{14}{section.0.2.2}%
\contentsline {section}{\numberline {}Jérusalem, fin d'après-midi du mercredi 24 juillet 1157}{16}{section.0.2.3}%
\contentsline {section}{\numberline {}Notes}{19}{section.0.2.4}%
\contentsline {section}{\numberline {}Références}{20}{section.0.2.5}%
\contentsline {chapter}{\numberline {3}Eau de vie}{21}{chapter.0.3}%
\contentsline {section}{\numberline {}Jérusalem, porte de David, fin de matinée du mardi~15~avril~1158}{21}{section.0.3.1}%
\contentsline {section}{\numberline {}Jérusalem, porte de David, midi du mardi~15~avril~1158}{25}{section.0.3.2}%
\contentsline {section}{\numberline {}Bords du Jourdain, dimanche~27~avril~1158}{28}{section.0.3.3}%
\contentsline {section}{\numberline {}Notes}{32}{section.0.3.4}%
\contentsline {section}{\numberline {}Références}{33}{section.0.3.5}%
\contentsline {chapter}{\numberline {4}Seul au monde}{35}{chapter.0.4}%
\contentsline {section}{\numberline {}Sarûdj, comté d'Édesse, matin du mardi~26~décembre~1144}{35}{section.0.4.1}%
\contentsline {section}{\numberline {}Édesse, midi du samedi~2~novembre~1146}{37}{section.0.4.2}%
\contentsline {section}{\numberline {}Route de Joha, après-midi, mardi~22~août~1150}{39}{section.0.4.3}%
\contentsline {section}{\numberline {}Notes}{42}{section.0.4.4}%
\contentsline {section}{\numberline {}Références}{42}{section.0.4.5}%
\contentsline {chapter}{\numberline {5}Ost réal}{43}{chapter.0.5}%
\contentsline {section}{\numberline {}Baisan, soir du vendredi~30~mai~1158}{43}{section.0.5.1}%
\contentsline {section}{\numberline {}Tibériade, midi du samedi~31~mai~1158}{47}{section.0.5.2}%
\contentsline {section}{\numberline {}Saint-Jean d'Acre, soirée du samedi~31~mai~1158}{51}{section.0.5.3}%
\contentsline {section}{\numberline {}Notes}{54}{section.0.5.4}%
\contentsline {section}{\numberline {}Références}{55}{section.0.5.5}%
\contentsline {chapter}{\numberline {6}Au nom du père}{57}{chapter.0.6}%
\contentsline {section}{\numberline {}Jérusalem, soir du jeudi~21~novembre~1157}{57}{section.0.6.1}%
\contentsline {section}{\numberline {}Tyr, midi du mardi~7~janvier~1158}{60}{section.0.6.2}%
\contentsline {section}{\numberline {}Bethléem, matin du mardi~18~février~1158}{64}{section.0.6.3}%
\contentsline {section}{\numberline {}Notes}{66}{section.0.6.4}%
\contentsline {section}{\numberline {}Références}{67}{section.0.6.5}%
\contentsline {chapter}{\numberline {7}Scripta manent}{69}{chapter.0.7}%
\contentsline {section}{\numberline {}Saint Jean d'Acre, fin de matinée du jeudi~14~octobre~1154}{69}{section.0.7.1}%
\contentsline {section}{\numberline {}Plages au sud de Saint Jean d'Acre, après-midi du jeudi~14~octobre~1154}{72}{section.0.7.2}%
\contentsline {section}{\numberline {}Damas, fin d'après-midi de youm~al~arbia 3~rabi’~al-thani~584}{76}{section.0.7.3}%
\contentsline {section}{\numberline {}Notes}{79}{section.0.7.4}%
\contentsline {section}{\numberline {}Références}{80}{section.0.7.5}%
\contentsline {chapter}{\numberline {8}Mal chancre}{81}{chapter.0.8}%
\contentsline {section}{\numberline {}Césarée, midi du mercredi~3~août~1149}{81}{section.0.8.1}%
\contentsline {section}{\numberline {}Césarée, après-midi du dimanche~7~août~1149}{84}{section.0.8.2}%
\contentsline {section}{\numberline {}Césarée, matin du lundi~8~août~1149}{86}{section.0.8.3}%
\contentsline {section}{\numberline {}Césarée, midi du lundi~8~août~1149}{89}{section.0.8.4}%
\contentsline {section}{\numberline {}Notes}{91}{section.0.8.5}%
\contentsline {section}{\numberline {}Références}{92}{section.0.8.6}%
\contentsline {chapter}{\numberline {9}Les trois loups}{93}{chapter.0.9}%
\contentsline {section}{\numberline {}Apchon, fin d'après-midi du mercredi~25~juillet~1156}{93}{section.0.9.1}%
\contentsline {section}{\numberline {}Plateau du Limon, jeudi~26~juillet~1156}{97}{section.0.9.2}%
\contentsline {section}{\numberline {}Plateau du Limon, après-midi du jeudi~26~juillet~1156}{99}{section.0.9.3}%
\contentsline {section}{\numberline {}La Bedice, soir du jeudi~26~juillet~1156}{102}{section.0.9.4}%
\contentsline {section}{\numberline {}Plateau du Limon, fin de matinée du vendredi~27~juillet~1156}{104}{section.0.9.5}%
\contentsline {section}{\numberline {}Plateau du Limon, après-midi du vendredi~27~juillet~1156}{107}{section.0.9.6}%
\contentsline {section}{\numberline {}Plateau du Limon, soirée du vendredi~27~juillet~1156}{110}{section.0.9.7}%
\contentsline {section}{\numberline {}Notes}{112}{section.0.9.8}%
\contentsline {section}{\numberline {}Références}{113}{section.0.9.9}%
\contentsline {chapter}{\numberline {10}Par delà les ombres}{115}{chapter.0.10}%
\contentsline {section}{\numberline {}Monts du Liban, mardi~10~décembre~1157}{115}{section.0.10.1}%
\contentsline {section}{\numberline {}Monts du Liban, mercredi~18~décembre~1157}{116}{section.0.10.2}%
\contentsline {section}{\numberline {}Monts du Liban, vendredi~20~décembre~1157}{118}{section.0.10.3}%
\contentsline {section}{\numberline {}Monts du Liban, vendredi~3~janvier~1158}{120}{section.0.10.4}%
\contentsline {section}{\numberline {}Sidon, mardi~16~septembre~1158}{122}{section.0.10.5}%
\contentsline {section}{\numberline {}Notes}{124}{section.0.10.6}%
\contentsline {section}{\numberline {}Références}{125}{section.0.10.7}%
\contentsline {chapter}{\numberline {11}Marchande d'oublies}{127}{chapter.0.11}%
\contentsline {section}{\numberline {}Baisan, matin du jeudi~24~mars~1149}{127}{section.0.11.1}%
\contentsline {section}{\numberline {}Baisan, fin de matinée du jeudi~24~mars~1149}{130}{section.0.11.2}%
\contentsline {section}{\numberline {}Baisan, matin du vendredi 25 mars 1149}{133}{section.0.11.3}%
\contentsline {section}{\numberline {}Jérusalem, midi du mardi~9~août~1149}{135}{section.0.11.4}%
\contentsline {section}{\numberline {}Notes}{136}{section.0.11.5}%
\contentsline {section}{\numberline {}Références}{137}{section.0.11.6}%
\contentsline {chapter}{\numberline {12}Liberté}{139}{chapter.0.12}%
\contentsline {section}{\numberline {}Baalbek, nuit du youm~al~sebt 26~dhu~al-qi’dah~550}{139}{section.0.12.1}%
\contentsline {section}{\numberline {}Notes}{145}{section.0.12.2}%
\contentsline {section}{\numberline {}Références}{146}{section.0.12.3}%
\contentsline {chapter}{\numberline {13}Fils de la louve}{147}{chapter.0.13}%
\contentsline {section}{\numberline {}Rive de la Save, fin de matinée du mercredi~21~novembre~1151}{147}{section.0.13.1}%
\contentsline {section}{\numberline {}Pélagonie, soirée du lundi~10~janvier~1155}{149}{section.0.13.2}%
\contentsline {section}{\numberline {}Venise, après-midi du mardi~31~mai~1155}{152}{section.0.13.3}%
\contentsline {section}{\numberline {}Abords de Sivas, fin d'après-midi du lundi~12~décembre~1155}{155}{section.0.13.4}%
\contentsline {section}{\numberline {}Jérusalem, soirée du jeudi 1\textsuperscript {er}~août~1157}{158}{section.0.13.5}%
\contentsline {section}{\numberline {}Notes}{161}{section.0.13.6}%
\contentsline {section}{\numberline {}Références}{161}{section.0.13.7}%
\contentsline {chapter}{\numberline {14}Cens caché}{163}{chapter.0.14}%
\contentsline {section}{\numberline {}Jérusalem, soirée du vendredi~1\textsuperscript {er}~mars~1157}{163}{section.0.14.1}%
\contentsline {section}{\numberline {}Jérusalem, après-midi du jeudi~21~mars~1157}{166}{section.0.14.2}%
\contentsline {section}{\numberline {}Jérusalem, soirée du samedi~20~avril~1157}{169}{section.0.14.3}%
\contentsline {section}{\numberline {}Notes}{173}{section.0.14.4}%
\contentsline {section}{\numberline {}Références}{174}{section.0.14.5}%
\contentsline {chapter}{\numberline {15}Sainte-Marie des Crocodiles}{175}{chapter.0.15}%
\contentsline {section}{\numberline {}Saint Jean d'Acre, après-midi du lundi~11~mars~1157}{175}{section.0.15.1}%
\contentsline {section}{\numberline {}Saint-Jean de Tyre, après-midi du mercredi~13~mars~1157}{178}{section.0.15.2}%
\contentsline {section}{\numberline {}Abords de Castel-Fenouil, midi du jeudi~14~mars~1157}{181}{section.0.15.3}%
\contentsline {section}{\numberline {}Notes}{185}{section.0.15.4}%
\contentsline {section}{\numberline {}Références}{186}{section.0.15.5}%
\contentsline {chapter}{\numberline {16}L'ombre des puissants}{187}{chapter.0.16}%
\contentsline {section}{\numberline {}Damas, fin d'après-midi du youm~al~had 7~jumada~al-thani~548}{187}{section.0.16.1}%
\contentsline {section}{\numberline {}Damas, midi du youm~al~sebt 23~ramadan~548}{190}{section.0.16.2}%
\contentsline {section}{\numberline {}Damas, matinée du youm~el~itnine 10~safar~549}{194}{section.0.16.3}%
\contentsline {section}{\numberline {}Damas, soirée du youm~al~talata 8~rabi’~al-thani~549}{196}{section.0.16.4}%
\contentsline {section}{\numberline {}Notes}{199}{section.0.16.5}%
\contentsline {section}{\numberline {}Références}{200}{section.0.16.6}%
\contentsline {chapter}{\numberline {17}Retraite}{201}{chapter.0.17}%
\contentsline {section}{\numberline {}Château Emmaüs, fin de matinée du jeudi~9~mai~1157}{201}{section.0.17.1}%
\contentsline {section}{\numberline {}Château Emmaüs, après-midi du samedi~11~mai~1157}{205}{section.0.17.2}%
\contentsline {section}{\numberline {}Aqua Bella, matin du samedi~25~mai~1157}{208}{section.0.17.3}%
\contentsline {section}{\numberline {}Notes}{211}{section.0.17.4}%
\contentsline {section}{\numberline {}Références}{212}{section.0.17.5}%
\contentsline {chapter}{\numberline {18}Legenda sanctorum}{213}{chapter.0.18}%
\contentsline {section}{\numberline {}Jérusalem, veillée du vendredi~27~décembre~1157}{213}{section.0.18.1}%
\contentsline {section}{\numberline {}Jérusalem, après-midi du samedi~28~décembre~1157}{216}{section.0.18.2}%
\contentsline {section}{\numberline {}Tyr, après-midi du jeudi~9~janvier~1158}{219}{section.0.18.3}%
\contentsline {section}{\numberline {}Tyr, veillée du mardi~25~février~1158}{221}{section.0.18.4}%
\contentsline {section}{\numberline {}Notes}{223}{section.0.18.5}%
\contentsline {section}{\numberline {}Références}{224}{section.0.18.6}%
\contentsline {chapter}{\numberline {19}Marcheurs de lumière}{225}{chapter.0.19}%
\contentsline {section}{\numberline {}Cathédrale de Beauvais, après-midi du dimanche~3~juin~1156}{225}{section.0.19.1}%
\contentsline {section}{\numberline {}Abords de Salzburg, midi du vendredi~6~juillet~1156}{229}{section.0.19.2}%
\contentsline {section}{\numberline {}Col de Chipka, fin de journée du vendredi~14~septembre~1156}{231}{section.0.19.3}%
\contentsline {section}{\numberline {}Faubourgs de Byzance, matin du samedi~29~septembre~1156}{234}{section.0.19.4}%
\contentsline {section}{\numberline {}Faubourg de l'Asnerie, Jérusalem, veillée du jeudi~20~décembre~1156}{236}{section.0.19.5}%
\contentsline {section}{\numberline {}Notes}{238}{section.0.19.6}%
\contentsline {section}{\numberline {}Références}{243}{section.0.19.7}%
\contentsline {chapter}{\numberline {20}Fil du temps}{245}{chapter.0.20}%
\contentsline {section}{\numberline {}Alexandrie, midi du youm~al~talata 12~jha'ban~489}{245}{section.0.20.1}%
\contentsline {section}{\numberline {}Jérusalem, après midi du youm~al~khemis 22~jha'ban~492}{248}{section.0.20.2}%
\contentsline {section}{\numberline {}Clepsta, après-midi du samedi~8~septembre~1156}{250}{section.0.20.3}%
\contentsline {section}{\numberline {}Abords du Jourdain, près de Jéricho, soir du youm~al~sebt 27~jha'ban~658}{253}{section.0.20.4}%
\contentsline {section}{\numberline {}Caverne 38, falaise du Qarantal, près de Jéricho, mercredi~7~juillet~1993}{258}{section.0.20.5}%
\contentsline {section}{\numberline {}Notes}{259}{section.0.20.6}%
\contentsline {section}{\numberline {}Références}{260}{section.0.20.7}%
\contentsline {chapter}{\numberline {21}Nœud gordien}{261}{chapter.0.21}%
\contentsline {section}{\numberline {}Port de Gibelet, midi du lundi~19~juin~1150}{261}{section.0.21.1}%
\contentsline {section}{\numberline {}Tripoli, matin du mardi~7~octobre~1152}{263}{section.0.21.2}%
\contentsline {section}{\numberline {}Tripoli, veillée du samedi~7~mars~1153}{266}{section.0.21.3}%
\contentsline {section}{\numberline {}Côtes du royaume de Jérusalem, midi du jeudi~10~mars~1155}{268}{section.0.21.4}%
\contentsline {section}{\numberline {}Tripoli, après-midi du lundi~6~février~1156}{270}{section.0.21.5}%
\contentsline {section}{\numberline {}Notes}{272}{section.0.21.6}%
\contentsline {section}{\numberline {}Références}{273}{section.0.21.7}%
\contentsline {chapter}{\numberline {22}Fille de Magdala}{275}{chapter.0.22}%
\contentsline {section}{\numberline {}Césarée, matin du samedi~2~juin~1156}{275}{section.0.22.1}%
\contentsline {section}{\numberline {}Environs de Caco, matin du mardi~8~janvier~1157}{278}{section.0.22.2}%
\contentsline {section}{\numberline {}Calanson, soir du lundi~22~avril~1157}{280}{section.0.22.3}%
\contentsline {section}{\numberline {}Plaine de la Bocquée, matinée du dimanche~15~septembre~1157}{284}{section.0.22.4}%
\contentsline {section}{\numberline {}Notes}{286}{section.0.22.5}%
\contentsline {section}{\numberline {}Références}{287}{section.0.22.6}%
\contentsfinish 
