![Framabook, le pari du livre libre](logo_framabook_epub.jpg)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d'informations sur Framasoft, consultez <http://www.framasoft.org>

Se démarquant de l'édition classique, les Framabooks sont dits « livres libres » parce qu'ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu'un utilisateur de logiciels libres. Les Framabooks s'inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l'appropriation collective de la connaissance.

Pour plus d'informations sur le projet Framabook, consultez <http://framabook.org>

Copyright 2020 : Yann Kervran, Framasoft (coll. Framabook)

Ce recueil de textes Qit'a est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

ISBN : 979-10-92674-32-3\
Dépôt légal : Mai 2020\
Couverture : Les rois mages en route pour Bethléem, vers 1170-1175. Copenhague, Royal Library, Ms. Thott 142.3o. Domaine Public - [commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:12thcPsalter3Kings.jpg)

Remerciements
-------------

La publication de ce premier lot de quatre recueils de textes courts Qit'a a demandé beaucoup de temps et d'énergie au membres du groupe Framabook, je tiens à les en remercier, Mireille en tête, avec qui les échanges autour de corrections proposées ont toujours constitué d'agréables moments.

*À la mémoire des exclus de la Mémoire*

Jouvences
---------

### Jérusalem, matin du mardi 16 avril 1157

Après l'afflux du matin, lorsqu'entraient tous les marchands de légumes, de fruits et de bestiaux à destination des foires et étals, le travail était plutôt calme à la porte de David. Les négociants arrivaient au compte-goutte, avec leurs caravanes de chameaux, de mules ou d'ânes, dans un nuage crayeux et les cris des convoyeurs. Ils étaient aiguillés vers le bâtiment des douanes, tandis que leur convoi emplissait l'air de poussière et d'odeurs animales.

Le péage ne concernait pas les simples voyageurs, les pèlerins ou les locaux. Donc le plus souvent, les sergents de faction se contentaient d'attendre que la foule et le temps passent, à l'ombre des murailles, espérant le passage du soleil au zénith, moment où ils seraient relevés. Certains jouaient au palet, d'autres encore fermaient les yeux, les fesses dans le gravier, cachés dans un recoin. Ernaut n'était pas de ceux-là.

Récemment nommé, il accomplissait son devoir avec zèle, sous les sourires et parfois les quolibets de ses compagnons. Sa haute stature marquait d'ombre le passage sous la voûte perçant la muraille. Il était l'écueil qui brisait le flot. Ou du moins s'imaginait comme tel, les pouces dans le baudrier, jambes écartées, le front haut et le regard inquisiteur. Eût-il été habillé de gris qu'on l'aurait pris pour une antique statue de Samson, oubliée là des siècles durant. Les yeux au loin, perdus dans les monts de Judée, parcourant les terrasses d'oliviers et d'aliboufiers, de cyprès et de figuiers, revenaient instantanément se poser sur quiconque se présentait à l'entrée du passage. Il n'esquissait pas le moindre geste pour les exempts de taxe.

Baset, un de ses collègues, petit homme au visage pointu, le doigt prompt à moquer ou à se réfugier dans l'une des cavités de son visage ricanait avec un de ses compères, un valet ventripotent qui traînait dans les environs, entre deux commissions pour un maître débonnaire.

« On a levé bon féal avec celui-là !

--- Crédieu, y ferait pas bon l'inquiéter. Si la baffe te rate, la tempête à la suite t'enrhumera à coup sûr ! s'amusa l'autre.

--- De certes je préfère l'avoir à mes côtés. Il a tant espoir d'œuvrer droitement qu'il aime à assurer pires corvées.

--- Ça lui passera...

--- Comme à tous » opina Baset gravement.

Il se tourna vers le valet, fouillant sous sa cotte pour détacher sa bourse du braiel.

« T'irais pas nous quérir cruchon de vin bien coupé ? J'ai grand soif et la vinasse d'ici a goût de pissat. »

Le joufflu attrapa les pièces et partit de sa démarche traînante, levant la main pour saluer Ernaut tandis qu'il pénétrait dans la cité. Le jeune homme le suivit d'un regard, tournant légèrement la tête, un demi-sourire sur les lèvres. C'est alors qu'il discerna des silhouettes connues qui s'approchaient depuis les rues de la ville. Son expression se fit soudain plus enjouée et toute sa silhouette se détendit. Sanson de Brie, sa femme Mahaut et, surtout, Libourc sa fille, s'avançaient droit vers lui.

Arrivés à quelques pas, le vieil homme salua amicalement, tandis que Libourc se contentait d'un sourire à son intention. La poitrine du jeune homme n'était que braises et il ne savait désormais plus quoi faire de ses mains, qu'il ne pouvait garder crânement sur ses armes. Embarrassé, il décida de les croiser dans son dos, histoire de les cacher.

« Le bon jour à toi, maître Ernaut, lança gaiement Sanson.

--- Le bon jour à vous, maître Sanson. »

En disant cela, il ne s'attarda guère sur les deux parents mais échangea quelques regards avec la jeune fille, découvrant avec ravissement l'apparition des fossettes aux creux des joues. Il se rendit compte après un instant qu'il dévisageait Libourc alors même qu'il leur bloquait le passage à tous trois. Le visage de Mahaut n'aurait pu être plus acide lorsqu'elle déclara :

« Auriez-vous ordre de tenir les pèlerins en deçà des murs, sergent ? »

Ernaut écarquilla les yeux.

« Certes pas ! Je.. C'est juste que...

--- Nous allons visiter un casal non loin, maître Ernaut, peut-être en as-tu entendu parler ? » glissa Sanson, de sa voix douce mais décidée.

Ernaut lui adressa un regard reconnaissant et réussit à enchaîner :

« Peut-être, un mien frère espère un casal à une poignée de lieues au septentrion, à la Mahomerie.

--- Non, moi c'est à... »

Sanson fronça les sourcils, leva les yeux.

« Mahomeriola, mon ami, lui indiqua Mahaut.

--- Voilà, sur la route de la côte. »

Ernaut ne put réprimer un soupir de mécontentement. Si Sanson s'était installé dans le même casal que son frère Lambert, il aurait eu toutes les excuses pour leur rendre visite, pour être en compagnie de Libourc.

« Nous cherchons loueur de montures pour nous y rendre, justement, y faire visite à l'intendant et se rendre compte des manses. En connais-tu un bon ?

--- De certes ! Il y a Abdul Yasu, dont l'écurie est située en deçà du bosquet de palmiers où vous voyez la charette emplie de bois. Recommandez-vous de moi, il me connaît. »

Disant cela, Ernaut bomba le torse, heureux de se mettre en avant. Il n'osa risquer un œil vers Libourc, mais sentait bien qu'elle le fixait, et devina un sourire sur son visage.

« Grand merci, mon garçon. Je suis aise d'avoir compère en la sergenterie du roi Baudoin. J'ai espoir que tu viendras rompre le pain dans ma demeure quand je serai installé.

--- Rien ne me ferait plus plaisir, maître Sanson.

--- Alors c'est dit, grand merci à toi, maître Ernaut, je te souhaite le bon jour. »

Ernaut ne sut quoi répondre et bredouilla un salut incompréhensible, tandis qu'il dévisageait Libourc qui s'éloignait. Après quelques pas, elle fit voler sa natte d'un geste gracieux et en profita pour le fixer à son tour, le gratifiant d'un sourire porteur d'espoir.

### Mahomeriola, fin de matinée du dimanche 8 septembre 1157

À la sortie de la messe, les gens s'assemblaient devant le parvis, au frais des pins qui bordaient la placette, entre le gargouillis de la fontaine et le chant des cigales. Les enfants jouaient autour des buissons brunis par le soleil, couraient entre les hommes occupés à échanger les dernières nouvelles du royaume, des cultures ou, plus important encore, des vicissitudes du voisin. Ernaut avait pris l'habitude de venir partager le dîner dominical avec Sanson et les siens, profitant de l'occasion pour passer du temps avec Libourc. Comme à chaque fois, Mahaut était partie en avant pour finir de préparer le repas tandis que son mari, intarissable bavard, s'attardait à discuter.

Les deux jeunes gens profitaient de sa présence pour flâner également à l'ombre des arbres, comme d'autres de leur âge, déambulant aux alentours entre les bosquets. Ne sachant que faire de ses mains, Ernaut dépeçait une pomme de pin tout en parlant. Il se sentait de plus en plus en confiance avec Libourc et ne craignait plus les moments de silence, qu'ils comblaient de regards qui se suffisaient. Parfois, quand ils se trouvaient à l'écart d'un chemin, loin des regards toujours curieux, ils se laissaient aller à plus d'intimité, s'accordant baisers et chastes caresses. Ils jouissaient de leur jeunesse, de leur passion naissante, de ces promesses innocentes.

Ce jour-là, Libourc sentit bien qu'Ernaut n'était pas aussi dissert qu'habituellement. Il hésitait sur les mots et ne finissait pas ses phrases, comme si un souci le contrariait. Elle se rapprocha de lui, entrelaçant leurs bras et saisissant sa main.

« Tu me sembles d'humeur bien sombre, ce jour d'hui, Ernaut !

--- Sombre ? Certes non. C'est juste que... Je n'ai pas grande habileté avec les mots.

--- Alors déverse ton sac et nous ferons juste tri. »

Ernaut inspira lentement et déposa un baiser dans la chevelure de la jeune fille.

« Je pensais à Guillaume et Anseline...

--- Que de bon temps nous avons pris à leur mariage, s'emporta Libourc. Je n'avais pas dansé ainsi depuis... Depuis jamais en fait ! Tu caroles si adroitement, mon bel ami. »

Elle se serra contre lui.

« Bon temps fait le pied léger... Je me demandais si... »

Il marqua un temps, se passant la langue sur les lèvres, hésitant. La jeune femme lui pressa le bras.

« Et quoi donc ? »

Ernaut lança tout à trac :

« Verrais-tu d'un bon œil que je parle mariage à ton père ? »

Libourc sentit son cœur s'emballer et ses joues se colorèrent tandis que la joie illuminait son visage. Elle s'arrêta brusquement. Il la dévisagea, surpris de sa propre hardiesse, soudain bien désemparé devant le silence.

« Eh bien alors quoi ?

--- Bien sûr que oui, mon beau ! Je n'ai pas plus cher espoir que de m'unir à toi. »

Elle se tourna face à lui et enserra son cou de lutteur de ses bras minces, attirant son visage à elle. Sa voix se fit discrète, souffle léger couvrant les bruits alentours.

« Devenir ton épouse devant Dieu et les hommes, voilà mon rêve le plus doux ».

Puis elle scella sa déclaration d'un baiser d'abord très chaste puis empli de passion. Ils ne se séparèrent qu'à regret, lorsque des gamins qui les avaient espionnés se mirent à leur tourner autour, chantant et riant tout en se moquant des amoureux.

« Les petits démons » s'amusa Libourc tandis qu'elle faisait mine de les chasser de la main.

Ernaut la ramena contre son cœur et grogna contre les petits diables, d'un grondement d'ours empli de joie tandis qu'il goûtait ses lèvres une nouvelle fois. Il allait se marier.

### Jérusalem, midi du jeudi 6 février 1158

Un temps mitigé avait succédé à des journées froides et maussades. C'était la première fois depuis plusieurs jours que le soleil émergeait dans le ciel d'hiver. La chaleur de la mi-journée faisait oublier les frimas de la nuit, et à l'abri du vent, contre les murailles sud de la cité, il faisait bon s'alanguir sous les rayons bienfaisants.

Ernaut avait fini son service de la nuit, qu'il avait passé à patrouiller dans les rues et le long des remparts. Il était las, et avalait donc avec bonheur la tourte à la viande et aux herbes que Mahaut avait confectionnée. Il avait encore de nombreuses réticences à son sujet, ce qu'elle lui rendait bien, mais il reconnaissait qu'elle cuisinait divinement. Il espérait secrètement que Libourc avait hérité ce don de sa mère, dans la perspective de se délecter à chaque repas jusqu'à la fin de ses jours.

Il les avait rejoints au marché aux bestiaux, où Sanson était venu, espérant trouver de quoi se constituer un cheptel. Il avait besoin d'animaux de bât et envisageait d'investir dans quelques têtes d'ovins. Mais il n'avait pas trouvé son bonheur et repartait déçu. Cela n'avait néanmoins pas entamé son moral et il parlait de son établissement dans le casal de Mahomeriola avec les accents chantants d'un jeune homme. Il travaillait à avoir une presse à olives chez lui en plus d'un moulin à concasser.

« Une fois tout cela installé, il ne me restera plus rien de ce que j'ai amené de Champagne. Pas une maille ! Mais nous aurons beau manse.

--- La terre vaut plus que monnaies d'or répétait mon père, et j'encrois qu'il disait vrai, abonda Mahaut.

--- Vous ne saurez œuvrer seul en si vaste domaine, maître Sanson ? Cela me paraît bien grand, s'inquiéta Ernaut.

--- Il y a abondance de valets. J'aurais bien acquis un esclave, mais...

--- Il est hors de question qu'un de ces Mahométans vive sous mon toit, le coupa Mahaut, pincée. Et puis, j'ai doutance que Dieu approuve ce genre de choses. »

Ernaut fronça les sourcils, étonné de voir Mahaut proférer une telle remarque. Elle s'en aperçut et continua.

« Pour sûr que voilà païens malfaisants ! Mais il me semble qu'il faut en faire de bons chrétiens plutôt que des serviteurs qui flattent notre orgueil.

--- Toi, tu écoutes trop les sermons des bons pères, ma douce » s'amusa Sanson.

La remarque éteignit la discussion et chacun s'employa pendant un moment à avaler son repas. Ernaut et Libourc échangeaient des regards complices lorsque leurs mains se frôlaient près du pain ou d'un pichet. Tandis qu'elle piochait parmi des dattes, Mahaut se tourna vers le jeune homme :

« Dis-moi, Ernaut, plutôt que d'avoir serviteurs inconnus, ne voudrais-tu pas labourer aux côtés de mon époux ? Tant qu'à se faire valet, autant le faire sur une terre de sa famille. »

Libourc soupira et Ernaut fit la moue, vexé de la remarque.

« Je ne suis nullement valet, j'ai prêté serment au roi de Jérusalem et le sers comme loyal sergent. Il n'y a là nul déshonneur, rien à comparer avec le valet qui s'emploie à récurer les auges à cochons ou à fendre le bois.

--- Vois-tu déshonneur à fendre le bois de ton parent ?

--- Je ne...

--- La paix, vous deux, s'emporta Sanson. Mahaut, tu ne peux demander à Ernaut de venir en la maison de son espousée, cela ne se passe pas ainsi. »

Ernaut hocha la tête en accord, reconnaissant que Sanson ramène sa femme à la raison. Il avala un peu de vin, le temps de se calmer. Libourc lui serra la main en soutien, souriant avec douceur, inclinant de la tête à de la retenue de sa part. Il enrageait des remarques assassines de Mahaut, mais n'avait guère de moyens de s'en prémunir. Il choisit donc de se justifier, une fois encore.

« Je n'ai pas prétention de profiter des biens qui ne sont pas miens. J'ai conscience que Libourc aura belle dot et je saurai lui garantir un douaire à la mesure. Quand nous partagerons un toit, ce sera le nôtre, et j'ai espoir qu'il ne soit pas moins solide que le vôtre.

--- J'ai nul doute à cela, indiqua Libourc, dans un sourire.

--- Moi non plus, Ernaut, opina Sanson. Car même si j'ai bonne amitié pour toi, sache que je n'aurais jamais donné ma fille à un homme qui ne la vaille pas. Outre tu sais manier l'épée et cela peut être de quelque utilité en ce pays. L'affaire est dite et je n'y reviendrai pas. »

Il conclut sa déclaration d'un toat silencieux et avala son gobelet de vin tandis que des étoiles brillaient dans les yeux des jeunes gens, indifférents au visage maussade de Mahaut, aussi gris que son voile. ❧

### Notes

Au tournant du XIIe siècle, le mariage latin demeure une affaire essentiellement laïque. Si l'Église s'emploie depuis des années à en faire un moment important de la vie du fidèle (qui aboutira à en faire un sacrement quelques dizaines d'années plus tard), il constitue avant tout une union civile. Ce qui importe aux familles, c'est de réguler les transferts de biens que le départ des jeunes gens et leur installation peuvent provoquer. Généralement, c'est la femme qui quitte le foyer familial pour rejoindre son époux et de complexes échanges sont mis en place pour dédommager les différentes parties qui se voient lésées, amputées d'un membre productif.

Ce n'est d'ailleurs pas l'apanage de la société médiévale européenne et on retrouve de nombreux textes traitant des mêmes soucis dans la documentation légale juive ou musulmane.

L'ingérence de l'Église dans cette cérémonie sera aussi l'occasion de remettre en valeur le consentement des futurs époux, insistant sur l'engagement moral que représente l'union de deux âmes. Cela a certainement participé à renforcer l'importance du sentiment amoureux dans la négociation de telles cérémonies, au détriment des impératifs politiques et économiques familiaux, vraisemblablement prévalents jusque-là.

### Références

Ross David, « Allegory and Romance on a Mediaeval French Marriage Casket », dans *Journal of the Warburg and Courtauld Institutes *11, 1948, p.112-142.

Schnell Rudiger, Shields Andrew, « The Discourse on Marriage in the Middle Ages » dans *Medieval Academy of America* 73, 1998, p. 771-786.

Searle Eleanor, « Seigneurial Control of Women's Marriage: A Rejoinder » dans *Past and* *Present Society *99, 1983, p. 148-160.

Sheehan Michael M., *Marriage, Family, and Law in Medieval Europe: Collected Studies*, Toronto: University of Toronto Press, 1996.

Frères ennemis
--------------

### Jérusalem, après-midi du vendredi 19 juillet 1157

Profitant de l'ombre du passage perçant la muraille à la porte de David, Ernaut discutait avec un messager venant du comté de Triple[^1]. Ce dernier avait croisé les forces flamandes installées aux abords de Beyrouth qui accompagnaient le comte Thierry[^2], une nouvelle fois porteur de la croix. De nouvelles opérations militaires se profilaient donc dans la partie nord du royaume. C'était de toute façon la saison, comme le constatait le cavalier.

Peu après la célébration de la libération de Jérusalem quelque cinquante ans plus tôt, les esprits étaient échauffés et le désir de porter l'assaut au sein du territoire musulman plus prégnant que jamais. Les affrontements autour de Panéas[^3] les mois précédents et la capture humiliante de nombreux hommes incitaient à la vengeance[^4]. En outre, le jeune Baudoin semblait déterminé à ne pas demeurer tranquille, toujours désireux de se mettre en selle pour asssaillir les Turcs, désormais maîtres du nord et de l'est.

Le bruit d'une forte compagnie de chevaux approchant attira leur attention. Ils se tournèrent vers la porte d'entrée et purent admirer une impressionnante troupe. Une vingtaine d'hommes sur de splendides montures aux harnais soignés venait de rejoindre le passage d'accès. La plupart portaient des vêtements de soie rembourrée chatoyant sous le soleil, des épées étincelantes, des turbans immaculés. Au milieu, dans une tenue encore plus flamboyante, porteur d'un couvre-chef élaboré, l'un d'eux arborait un bâton écarlate, qu'il tenait de façon ostensible.

Ernaut vit Eudes, le sergent responsable de la porte, s'approcher avec respect afin de s'enquérir de leur identité. Le riche cavalier leva la main en un salut formel et déclara d'une voix autoritaire, emprunte d'un léger accent :

« Au nom du Père, du Fils et du Saint-Esprit, Dieu seul et unique, Manuel[^5], fidèle de Dieu, Haut et Auguste Autocrate et Grand Empereur des Romains, m'envoie, moi son mandatôr, auprès de son bien-aimé et espéré frère spirituel Baudoin, noble d'entre tous, et distingué roi de Jérusalem. »

Il fallut à Ernaut un moment pour décrypter l'annonce, tandis qu'il dévisageait la troupe, un homme à la fois. Il n'était d'ailleurs pas le seul, la plupart des sergents de faction admiraient les chevaux et l'équipage. Des badauds s'étaient même approchés depuis les rues proches, curieux de voir plus en détail la magnifique ambassade. Baset, qui se trouvait désormais à la gauche de Ernaut, lui souffla :

« Ça faisait un petit moment que les Grecs n'avaient mandé nul messager. »

Deux sergents furent désignés pour escorter la troupe jusqu'au palais royal. Ils partirent en trottinant, suivis de la file de cavaliers. Les soldats, en passant, conservèrent une attitude de froide indifférence, certains d'attirer à eux les regards. Lorsqu'ils eurent disparu dans les rues de la cité, la tension retomba instantanément à la porte. Les hommes échangèrent des moues appréciatrices et l'un d'eux cracha par terre, là où s'était tenu le messager.

« Aussi fiers qu'un étron de pape, ces maudits Grecs.

--- Y sont toujours ainsi, à toiser le monde. Mais le prince Renaud[^6] leur a bellement botté le cul !

--- Ils ont dû apprécier ça, c'te bande de sodomites ! »

Ernaut écoutait sans intervenir. Il ne comprenait pas bien l'hostilité des sergents vis-à-vis de ceux qui étaient généralement leurs alliés contre les infidèles. Il n'avait pas encore pris conscience de la complexité des rapports diplomatiques entre les états moyen-orientaux, et estimait simplement que la place de chrétiens était aux côtés de leurs frères, face aux musulmans. Un monde ordonné, net, comme il n'en existe que dans les esprits simples ou encore jeunes.

« N'empêche, j'irais bien fouler les rues de leur cité, moi. On la dit si grande qu'il faut plusieurs jours pour aller d'une muraille à l'autre. Elle enferme moult merveilles dont le corps et l'esprit ne se lasseraient pas, un siècle durant.

--- Un mien compagnon y est passé avec le roi Louis[^7], mais on dit qu'ils ne laissent nul étranger y pénétrer facilement.

--- Ils ont peur qu'on leur pique leurs femmes peut-être ? ironisa Baset.

--- Pour ce qu'ils en font ! » s'amusa le sergent.

La répartie déclencha des rires gras, moqueurs, méfiants. La plaisanterie soudait le groupe, lui donnait son identité, exclusive des autres, de ceux qu'elle raillait. Seuls parmi ces sergents moqueurs, Ernaut tourna la tête vers le chemin qu'avait emprunté le mandatôr et son escorte, les yeux encore emplis de la magnificence des tenues et des équipements. Lui aussi se prenait à rêver quand on évoquait le nom de leur cité de légende. Byzance.

### Jérusalem, soirée du mardi 23 juillet 1157

Récemment promu sergent, Ernaut se voyait souvent confier des tâches subalternes. Il subissait tant bien que mal ces brimades, désireux de se faire accepter par la petite communauté des hommes du roi. Il parcourait donc le passage, une pelle et un seau en main, afin de ramasser les crottins et déjections des différents animaux qui franchissaient le seuil de la cité. Le tout était entassé dans un coin de la muraille, et revendu aux paysans du coin, ce qui finançait la boisson des hommes affectés à la garde des portes.

Au début on l'avait gentiment brocardé tandis qu'il s'affairait à nettoyer le sol, mais cela faisait partie des rituels d'intégration au groupe, et désormais plus personne ne prêtait attention à lui. Désormais, il attendait simplement qu'un nouveau venu prenne sa place et le seau malodorant, de façon à pouvoir le railler à son tour. Il allait vider son récipient quand il discerna un nuage de poussière sur la route de la Mer.

Des cavaliers allaient se présenter d'ici peu. Il attendit là un moment, afin d'identifier qui cela pouvait être. Il était curieux de voir ce fameux comte de Flandre dont tout le monde parlait. Un farouche chevalier, à la lance vaillante, et un vrai combattant de Dieu, venu de nombreuses fois dans le royaume. Il réalisa peu à peu que ce n'étaient certainement pas des hommes de Jérusalem et revint précipitamment dans le passage.

« On dirait que des païens s'en viennent par devers nous ! »

Plusieurs hommes s'approchèrent, intrigués, les mains sur les hanches et les yeux plissés tandis qu'ils s'efforçaient d'identifier les nouveaux arrivants. Ce fut Eudes qui parla le premier.

« Jambe-Dieu ! Des Turcs ! »

La remarque fit grimacer tous les présents. La plupart ici avait eut affaire au moins une fois aux flèches impitoyables, aux sabres assoiffés des terribles combattants musulmans. Il était rare d'en voir autrement que dans le fracas et la poussière des affrontements. Les sergents étaient comme hypnotisés. La troupe était composée d'une dizaine d'hommes, tous habillés légèrement, sans armure, mais le sabre et l'arc à la ceinture.

Ils étaient couverts de la poussière du voyage mais leurs tenues n'étaient pas celles de simples nomades. C'étaient des soldats professionnels, de ceux qu'on forme dès leur plus jeune âge et qui constituaient l'élite des armées des sultans et des émirs.

Il firent halte devant la porte, face aux sergents disposés en ligne. Le premier des cavaliers, un jeune aux longues nattes encadrant un visage creusé habillé d'une courte barbe, les yeux bruns lançant des éclairs, fit avancer son cheval au pas et leva la main, avant de déclarer d'une voix forte plusieurs mots dans sa langue. Un autre homme, qui ne portait pas le sharbush[^8] mais un simple turban, s'avança alors et traduisit.

Ils étaient envoyés par le sultan Qilig Arslan[^9], ennemi du maudit Nūr ad-Dīn[^10], roi d'Alep et de Damas. Avant qu'Eudes ne pusse répondre, une voix s'était élevée « Dieu lo veult ! » et un amas de poussière s'était écrasé sur l'épaule d'un des cavaliers. Le cheval fit un écart, tandis que le Turc grognait et cherchait l'agresseur de son regard perçant.

Ernaut avait aperçu le fautif, un valet d'un des caravansérails voisins. En trois pas il fut sur lui. Alors qu'il allait l'empoigner, il sentit la présence du cavalier dans son dos et se retourna. Le soldat le dévisageait, l'air menaçant. Il avait la main posée sur la poignée de son sabre. Ernaut lui fit un signe d'apaisement tout en s'emparant du valet par le col de son vêtement. Quelques instants plus tard, le domestique apprenait les lois élémentaires de la physique après un vol plané qui le vit atterrir dans le tas de fumier. Aucun rire moqueur ne s'éleva, chacun attendait la réaction de leurs vis-à-vis.

Le porteur du turban s'approcha d'Ernaut et lui sourit.

« Nous sommes ici en paix, merci à toi. »

Ernaut fit un bref signe de tête en salut et reprit sa place parmi les hommes de la porte. Eudes désigna trois sergents pour leur montrer le chemin du palais royal. Ernaut était de ceux-là. Il lui glissa discrètement :

« Fais attention en route. Il y a moult croisés en ces jours dans la cité. Nous devons éviter qu'un messager soit malmené. »

Ernaut lui répondit d'un sourire féroce, posant la main sur le pommeau de son épée. Puis il fit signe à l'interprète de le suivre. Fendant la foule d'un air sévère, il criait « Place ! Place ! » tout en écartant les plus lents d'un geste vif du bras. Il n'avait jamais eu à affronter des Turcs et mettait un point d'honneur à ne pas faillir à son devoir d'escorte, première tâche un tant soit peu sérieuse qu'on lui confiait.

Plusieurs badauds reluctants finirent parmi les produits qu'ils admiraient, propulsés dans les boutiques devant lesquelles ils s'attardaient. Un ours était en marche, et cet ours avait une mission.

### Jérusalem, fin d'après-midi du mercredi 24 juillet 1157

Appuyé au chambranle de la porte des cuisines donnant sur la cour, Ernaut dégustait un peu de vin tout en grignotant du pain. Il venait d'accompagner des valets à l'atelier des monnaies royales pour y déposer des barres d'argent qui allaient servir à frapper les pièces à l'effigie du roi. Il tenait une de celles-ci dans la main, toute neuve, une de celles qui avaient servi à le payer.

Autour de lui les marmitons, les coursiers, s'activaient pour préparer le repas du soir. Une odeur d'épice et de pain chaud lui titillait les narines. Il prenait souvent son repas au palais, profitant des restes des festins préparés pour la table du roi. En tant que sergent, il n'était pas parmi les premiers qui pouvaient y prétendre, mais la nourriture était suffisamment abondante et variée pour qu'il y trouve de quoi satisfaire son solide appétit et son insatiable gourmandise.

Il observait avec nonchalance un des cavaliers byzantins qui venait d'apparaître dans la cour. De taille moyenne, celui-ci était vêtu d'une cotte à revers sur le cou, d'une belle étoffe verte. Un foulard rouge ceignait sa taille, épaisse, sous son baudrier d'arme. Le cou large, les épaules puissantes auraient été à l'aise à manier la charrue ou la cognée. Il tenait une paire de bottes à la main, semblant hésiter sur la conduite à tenir. Ernaut fit quelques pas dans sa direction. Il fut accueilli d'un regard franc.

« Besoin d'aide ?

--- Oui. Possible réparer semelles ? »

Ce disant, l'homme brandissait ses chaussures, qui avaient visiblement connu des jours meilleurs. Ernaut acquiesça et lui fit signe de le suivre. Il confia son gobelet à un gamin de cuisine qui passait là et se dirigea vers les rues de la cité.

« Stamatès ! déclara le grec.

--- Je ne parle pas votre langage, l'ami, répondit Ernaut.

--- Non, Stamatès mon nom. Stamatès Mpratos. »

Le jeune homme sourit de sa méprise.

« Et moi Ernaut. Ernaut de Vézelay ! »

Ils échangèrent un sourire et se coulèrent dans le flot de pèlerins. Le palais royal était accolé à l'église du Saint-Sépulcre, et se trouvait donc dans la partie de la cité où l'on croisait le plus de voyageurs. En cette période de l'été, il était difficile d'avancer dans les rues environnantes, entre l'hôpital de Saint-Jean et la rotonde qui abritait le tombeau du Christ. Chemin faisant, Ernaut, toujours curieux, s'efforçait de faire la conversation.

« Tu parles bien notre langue, tu es interprète ?

--- Non ! Soldat. Mais je connais gens de ton pays, quand eux passés sur nos terres voilà dix ans.

--- Tu as vu le roi Louis !

--- Oui, le roi Louis. Bonne armée. Pas comme Conrad[^11], armée pas sérieuse, pillards et voleurs. »

Son visage se fendit d'un large sourire.

« Battu un peu contre eux, pour calmer. Bon soldats ! »

Ils finirent par trouver un savetier qui accepta de prendre les souliers, indiquant de repasser le lendemain.

« Ça te dirait un peu de vin, compère ? Je connais bonne taverne pas loin.

--- Oui. Dans mon village, on aime le vin.

--- Comme chez moi alors ! Mon père est vigneron.

--- Dans le passé, démons de mon pays passaient leur temps boire le vin. Et s'amuser avec femmes ! »

Stamatès arbora un large sourire à l'évocation de ces légendes, emboîtant le pas à Ernaut.

« Les démons de Byzance ?

--- Non, je ne suis pas de Polis, être du thème de Thessalonique. Petit village. Radolibos. Je pas aime grandes cités. Ici ça va encore, mais la Polis... »

Il fit un geste des mains indiquant la démesure, tout en soufflant, amusé puis ajouta un clin d'oeil.

« Je préfère voyages, campagnes du Basileus Komnenoi. »

Il empoigna sa petite masse à tête de bronze, qu'il portait passée à la ceinture.

« Je écrase têtes des ennemis partout où il faut. Polis pas pour moi.

--- J'aimerais bien faire visite en ta Cité, moi.

--- Belle. Très belle. Mortelle aussi. Vieille Polis. »

Il s'esclaffa et tapa amicalement sur le bras du jeune homme.

« Je te dirai secrets à savoir pour éviter ennuis dans la Polis. Allons boire ! »

Ernaut hocha la tête avec enthousiasme, avançant avec d'autant plus de vigueur. Un instant il se rappela la réputation homosexuelle qu'on prêtait aux Byzantins, mais il balaya cette pensée en un instant. Stamatès avait l'allure d'un vétéran et, un gobelet à la main, prendrait certainement plaisir à narrer ses aventures. De celles qu'Ernaut rêvait de vivre. ❧

### Notes

Les relations du royaume chrétien de Jérusalem avec les puissances locales ne se calquaient pas sur les croyances religieuses, pas plus que chez ses adversaires. Des alliances de circonstance apparaissaient avec des dirigeants musulmans, des pactes de soutien mutuel étaient régulièrement conclus.

De la même façon, l'empire byzantin jouait les uns contre les autres. Cette attitude pragmatique des pouvoirs politiques heurtait beaucoup de croisés fraîchement débarqués, qui critiquaient à l'envie ceux qui avaient trahi selon eux l'héritage des premiers croisés conquérants. Cette politique était rendue possible par la grande fragmentation de la puissance musulmane.

L'unification graduelle sous Nūr ad-Dīn puis Saladin finit par rendre inefficaces ces manœuvres diplomatiques.

### Références

Augé Isabelle, *Byzantins, Arméniens & Francs au temps de la croisade. Politique religieuse et reconquête en Orient sous la dynastie des Comnènes 1081-1185*, Paris : Geuthner, 2007.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Elisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Eau de vie
----------

### Jérusalem, porte de David, fin de matinée du mardi 15 avril 1158

Les éclats de voix faisaient se tourner les têtes. Parmi les voyageurs, on se félicitait de n'avoir pas affaire au géant qui gardait la porte. Les sergents, eux, s'amusaient de ce qu'Ernaut avait encore pris à partie un négociant qui tentait de jouer au plus fin. Peu connaisseur des hommes dont les familles importaient en ville et certainement pas enclin à se laisser adoucir par des menaces ou des promesses, le jeune homme ne laissait passer aucune occasion de faire du zèle.

Il s'entêtait d'autant plus dans ses investigations que l'on s'efforçait d'y résister. Sa dernière victime était un marchand d'allure orientale qui vitupérait, dans des grands froufrous de son 'aba[^12] de lin coloré. Il ponctuait chaque phrase de grands effets de manche, pathétique papillon autour d'un bousier acharné.

« N'est-ce pas là toile de soie ? lança Ernaut, un grognement narquois s'échappant de sa gorge.

--- De la soie ? Non, certes non ! Peut-être juste quelques fils en décor ! Mais assurément pas habits de soie !

--- Pourquoi avoir plié ces draps au parmi de grossières étoffes de coton ?

--- Je n'ai pas fait les paquets, j'ai valet pour ça.

--- Et les valets en font à leur tête en votre négoce ? »

Le marchant se tut, vexé. Il pointait des lèvres comme une mégère ruminant une bonne nouvelle dans le voisinage. À l'écart, deux de ses serviteurs gardaient la tête basse, oscillant entre amusement et crainte des représailles. Ernaut pencha la tête vers une autre des balles, sur le second animal du train.

« Et là-dedans ? Vous allez encore annoncer bouses et étrons, rien de valeur sur lequel payer octroi ?

--- Un peu de respect, sergent, vous savez à qui vous parlez ?

--- Ouais, à une face de Carême, qui espère tromper la Secrète royale[^13], en pleine semaine sainte. J'ai l'air d'un agnel de six semaines ? »

Tout en parlant, il fit quelques pas vers son interlocuteur, gonflant la poitrine comme un taureau dans l'arène. Les veines saillant dans son cou battaient le sang qui empourprait sa face exaspérée. Il semblait à deux doigts d'abattre son poing massif dans le visage flasque face à lui. L'homme le comprit et fit mine de reculer. Il avoua, à contrecœur comme si c'était un secret d'état :

« J'ai là bonnes longueurs de papier d'Alexandrie, du khôl et de l'encens perse ainsi que quelques mesures de poivre fin.

--- À la bonne heure ! Allez voir l'office du péage pour payer ce que vous devez, et vous pourrez entrer. Bienvenue en la sainte Cité, voyageur. »

Sa dernière phrase sonna étrangement aux oreilles du négociant, et il crut y percevoir une menace. Si tous les hommes au service du roi de Jérusalem étaient comme ce Samson à face de lion, il n'était pas certain de souhaiter demeurer bien longtemps. Il aurait tendance à toujours regarder par-dessus son épaule, craignant de le voir surveiller la moindre de ses actions, le plus petit de ses négoces. Il obtempéra en silence, trop heureux de s'en sortir sans avoir reçu un coup, et un peu déstabilisé par l'apparente honnêteté de son interlocuteur. La plupart du temps, les fonctionnaires tâtillons se calmaient bien vite avec quelques monnaies, ou un échantillon des marchandises. Mais là, bizarrement, ce sergent se contentait de bien faire son travail. Quelle étrange façon de voir les choses, songea le marchand, inquiet de voir le monde changer de façon si inquiétante.

Le jeune homme sourit d'un air satisfait lorsqu'il vit le fraudeur se diriger vers le bâtiment des douanes. Il prit une grande inspiration et se tourna vers l'entrée, prêt à fondre sur une nouvelle victime. Et se trouva face à un pèlerin, l'air narquois, qui le toisait sans peur, les pieds plantés au sol, une main sur la hanche. Un air de défi flottait sur son regard et aucune crainte ne semblait l'effleurer face à la masse de muscles frémissants. Non content de demeurer là, il lança, guilleret :

« Bah alors, le gros, toujours à chicaner plus modeste que soi ? »

Ernaut écarquilla les yeux, tiraillé entre l'envie de mettre une baffe directement et celle de voir jusqu'où l'inconnu irait. Puis un sourire se traça un chemin dans le visage écarlate.

« Le Groin ! Ça alors ![^14] »

L'autre lui fit une bourrade amicale.

« Ouais, enfin, j'préfère qu'on me connaisse pas sous ce nom ici, gros. J'te demande pas si ça va, on dirait bien que oui ! »

Ernaut demeurait sans voix. Il n'avait pas vu son camarade depuis des mois, des années. La joie de découvrir un visage familier, évocateur de souvenirs d'enfance, camouflait le fait qu'ils avaient passé leur jeunesse à se cogner consciencieusement l'un sur l'autre. Il enlaça le voyageur d'une embrassade d'ours en riant.

« Tu t'es perdu que tu te retrouves ici ? »

La silhouette maigrelette disparut dans l'étreinte amicale. L'infortuné tentait de parler tandis que son torse subissait une accolade apte à étouffer un sanglier.

« Je viens pour les Pâques, comme tous ici ! »

Ernaut reposa son ami d'enfance et le toisa de bas en haut.

« Tu es bien léger pour si long voyage, bien digne pèlerin tu fais là à cheminer en t'en remettant à la fortune du Christ.

--- Je ne viens pas de si loin, tu sais, je demeure à Jéricho.

--- Au Jourdain ? Et ça fait long temps ?

--- Pas tant, je te conterai mon périple si tu en as désir. »

Ernaut acquiesça, enthousiaste.

« Je finis d'œuvrer en cette porterie à la mitan du jour, qui ne saurait tarder. Nous pourrons aller dîner à Malquisinat, en se racontant les nouvelles. Tu es parti depuis quand de Vézelay ?

--- J'ai pris route peu après les Mais de l'an passé, avant la saint Jean d'été. »

La silhouette s'était voûtée et le regard endurci, mais Ernaut était heureux de contempler son ami d'enfance, Droin, fils du porchier, avec lequel les bagarres étaient fréquentes. Un des rares à ne pas avoir peur de la vaste carrure d'Ernaut. Il désigna une des rues adjacentes.

« Il y a taverne juste à l'entrée de la rue de David, là. Tu n'as qu'à m'y attendre. Je ne serai pas long. »

Droin opina, un sourire sur les lèvres.

« C'est drôle de te retrouver ici, au centre du monde, portant l'épée à la hanche. J'ai souvenance qu'on se bestaillait hier sur les bords de la Cure.

--- J'ai encore pouvoir de te mettre une pignée, à ta convenance » rétorqua Ernaut, amusé.

Droin haussa les épaules, un rictus moqueur sur les lèvres. Il ravala sa répartie, trop heureux de trouver un vieux compagnon pour risquer de se l'aliéner si tôt. Puis il s'éloigna d'un pas léger, son bourdon frappant le sol avec gaieté, après un dernier signe de la main

### Jérusalem, porte de David, midi du mardi 15 avril 1158

Assemblage de recoins et d'escaliers organisés autour d'une cour, la taverne était bondée. En cette période de semaine sainte, de nombreux voyageurs se présentaient aux porte de la cité, certains plus avides de boissons terrestres que de nourriture céleste. Installés à une table où s'époumonaient des allemands au teint cramoisi, Ernaut et Droin dévoraient des beignets à jour de poisson[^15], arrosés d'un vin léger au goût de framboise. Ils n'avaient guère eu la possibilité d'échanger plus de quelques mots, leurs compagnons vociférant à qui mieux-mieux tandis qu'ils encourageaient un jongleur occupé à des acrobaties. Ils en profitèrent donc pour engloutir rapidement leur repas, échangeant des sourires tout en avalant à belles dents les plats qu'on leur apportait. Malgré sa faible carrure Droin ne le cédait que peu en appétit à Ernaut.

Puis la chaleur et la lumière envahirent peu à peu la cour tandis que le soleil tournait et les clients se firent plus rares. Les deux compagnons purent prendre leurs aises sur les bancs et commandèrent un nouveau pichet et des oublies[^16] pour finir leur repas. Le saltimbanque eut droit à quelques piécettes de cuivre qu'il se hâta de convertir en vin, s'installant à côté d'eux. Il se mit alors à siroter tranquillement, le visage au soleil, les yeux fermés. Les ronflements ne se firent guère attendre.

« Alors Droin, quelles nouvelles du pays ? Bonnes, j'ai espoir !

--- Les tiens prospèrent toujours. Le vieil Ermond conserve bon pied, malgré une fièvre qui l'a fort tenu alité. Il en a perdu large portion de sa panse. »

Ernaut sourit à l'évocation de son vieux père.

« Il s'esbigne toujours avec la Laurette ?

--- Certes ! Leurs voix portent dans la rue souventes fois. Il faut dire qu'elle se sent maîtresse du logis, avec ses deux garçons !

--- Deux ! Ça alors ! Mon frère doit être sacrément fier.

--- Pas qu'un peu. Et puis l'un d'eux semble tirer de toi, aussi gros et grand que braillard. Alors forcément, la Laurette, elle s'y entend pour faire la belle au marché. »

Les nouvelles s'enchaînèrent, à propos de gens auxquels Ernaut n'avait plus pensé dès la butte de Vézelay perdue dans le lointain. Pourtant, il prenait plaisir à entendre les potins, les derniers ragots, le destin de ceux qu'il avait croisés enfant. Sa vie outremer. Il se faisait l'impression d'un vétéran, lui qui n'avait pas encore connu vingt printemps. Le silence s'installa tandis que les nouvelles tarissaient et que la chère pesait sur les estomacs et les esprits.

« Mais dis-moi, Droin, tu as mis sacré long temps pour venir jusqu'ici, tu as pris le chemin de terre ?

--- Tout le monde n'a pas la fortune d'un Ermond pour se payer belle nef de voyage ! Je n'ai pu trouver navire qu'entre Byzance et Antioche.

--- Byzance, tu es allé à Byzance ?

--- Ouais, rétorqua Droin, dans un sourire fier. Enfin, j'ai vu les faubourgs, parce qu'ils m'ont pas laissé entrer. Mais c'est grand, comme, pfff, je sais pas, grand comme tu peux pas imaginer ! Du bateau, en partant, j'ai pu voir la côte, c'est que de la muraille, qu'a l'air toute petite alors que quand t'es aux pieds, t'en vois pas la fin. »

Ernaut acquiesça. Lui-même n'avait jamais vu la capitale du monde grec, là où résidait le basileus Manuel dont l'ombre s'étendait sur tous les royaumes moyen-orientaux. Il donna une tape amicale à Droin, désireux d'en savoir davantage.

« Tu viens ici comme pèlerin de la Foi, tu vas t'en retourner quand ?

--- Non, je reste. J'ai bien trop sué pour arriver. J'ai cru jamais voir le clocher du sépulcre. Et puis ton voyage m'a donné des idées, moi aussi j'ai envie d'avoir ma terre. Pas comme le vieux qui garde les cochons des autres.

--- Tu t'es installé ?

--- Non, pas encore. Pour le moment, je me loue à des frères moines, à Jéricho. L'ouvrage est pas trop dur et la table est bonne. Je veux pas me tromper et choisir le bon endroit, à l'abri de ces maudits païens. De toute façon, je pense chercher à entrer dans la maison d'un noble, comme palefrenier. C'est que je m'y entends en chevaux, je m'occupais un temps de ceux des frères moines, au village. Avec un peu de chance, on me soudoiera et je pourrais gratter quelques rapines pour emplir mon escarcelle, histoire de me payer bel hostel. »

Ernaut se mit à rire, enthousiaste.

« Je vois que t'as pas changé, toujours autant désireux de changement ! »

Il gratifia d'une tape amicale son ami, qui répondit d'un sourire.

« Et toi, ton vieux disait que t'allais faire le colon ici, et je te retrouve armé comme Roland, gardien de la sainte cité.

--- Lambert s'est installé, lui. Moi je ne sais pas encore. On m'a proposé de servir le roi, ça ne se refuse pas. Il marqua un temps et une onde de douceur passa sur ses traits. Et puis si je veux me marier et m'établir, je veux le faire bien, alors j'essaie de me faire un pécule. »

Droin s'agita, se pencha en avant.

« Si ça se trouve, on va finir flanc à flanc, l'épée au poing, à trucider du païen ! Ça m'ferait quelque chose, de sûr.

--- Le Gros et le Groin, railla Ernaut.

--- Ouais, ça serait notre cri de guerre : Gros et Groin, poingnez ! »

Il partit d'un grand rire sonore, avala un peu de vin. Le silence emplit de nouveau l'espace peu à peu tandis qu'ils sirotaient la chaleur et le vin.

« Tu es déjà venu à Jéricho ?

--- Certes, Lambert a voulu qu'on fasse visite à tous les lieux que le père curé lui avait énumérés.

--- Parfait, alors tu sauras y revenir. Viens-t'en donc me voir un de ces quatre, on se baignera au fleuve comme on faisait dans la Cure.

--- Et je te ferai boire la tasse, comme à l'époque ?

--- T'en as eu ta part je crois me faire souvenance... »

Ernaut sourit. Il était heureux d'avoir retrouvé une tête connue, quand bien même c'était celle sur laquelle il avait le plus tapé toutes ces années. Il choqua son gobelet contre celui de Droin.

« Je viendrai après les Pâques, on a fort ouvrage pour l'heure à la sergenterie du roi. Mais je viendrai, j'en fais promesse. »

### Bords du Jourdain, dimanche 27 avril 1158

Le vent portait les chants des pèlerins massés sur le chemin entre Saint-Jean et le lieu du baptême du Christ, à quelque centaines de mètres, jusque dans le taillis où Ernaut et ses proches s'étaient installés. Ils avaient bien sûr rendu visite aux lieux saints, mais profitaient désormais de la tiédeur du jour, alanguis parmi les bosquets d'acacias en fleur, baignant dans leur odeur sirupeuse.

Répondant à l'invitation de Droin, ils étaient venus en nombre. Eudes, sa femme et ses enfants, Droart et sa famille au complet étaient là. Mais aussi Sanson, Libourc et Mahaut venus de Mahomeriola et même Lambert, qui avait fait le déplacement depuis la Mahomerie. Ils avaient trouvé un bras du fleuve enserrant une petite clairière parsemée d'ombre, protégée du flot des croyants par un épais amas de roseaux.

Face à eux, sur l'autre rive, le sol rocailleux s'élevait rapidement vers des plateaux arides. Une compagnie de cavaliers soulevait un panache de fumée, des templiers reconnaissables à leur cape éclatante de blancheur. En ces périodes d'afflux de pèlerins d'outremer, ils parcouraient inlassablement les territoires pour en garantir la sécurité.

Ernaut était affalé contre un tronc de palmier, la tête sur une couverture repliée en guise d'oreiller. Il se laissait bercer par le bourdonnement d'abeilles affairées, colonies placées là par des apiculteurs désireux de profiter de l'abondance de fleurs en ce printemps lumineux. Droin s'approcha de lui, semant les gouttes sur son passage. Il avala une gorgée de vin et grapilla quelques raisins secs.

« Tu viens pas à l'eau ?

--- J'ai crainte d'empirer les fièvres légères qui m'assaillent depuis quelques jours. Je sens une fièvre humide me gagner et je préfère éviter de lui donner de quoi se renforcer.

--- T'étais pas si frileux, gamin ! »

Ernaut haussa les épaules, indolent.

« Les choses changent. »

Au fond de lui-même, il ne sentait nul mal, mais un fond de superstition l'empêchait de se baigner près d'un endroit aussi sacré. Il avait tout de même certains lourds péchés sur la conscience, et n'avait pas encore eu le courage d'aller s'en ouvrir à un confesseur. Bien qu'il n'ait pas été le plus assidu des fidèles, il n'avait guère envie de subir le courroux divin en le défiant ainsi.

Droin acquiesça et retourna vers le groupe qui barbotait le long de la rive. Les personnes âgées se contentaient de tremper les pieds, les mollets éventuellements tandis que les plus jeunes n'hésitaient pas à se baigner plus largement. Les enfants nus sautaient, éclaboussaient et soulevaient des gerbes d'eau, riant et criant. Les adultes se contentaient de garder leur chemise par pudeur, dans un lieu considéré comme sacré par tous, quand bien même ce n'était pas exactement le lieu du baptême.

Lorsque Droin les rejoignit, Ernaut entendit les voix qui s'interrogeaient sur son refus de se joindre aux baigneurs. Son goût pour l'eau était connu de tous, et le fait qu'il se refuse à en profiter étonnait ses proches. Droart lui cria :

« Viens-donc, Ernaut, il n'y a pas tant de fond que tu risques de t'y noyer !

--- Penses-tu, il nage mieux que poisson, répliqua Lambert.

--- Il craint d'empirer une fièvre qui le gagne » précisa Droin, qui avançait avec précaution dans le lit du cours d'eau.

Eudes, Sanson et Perrotte discutaient avec passion à mi-voix, assis sur un ressaut de terre, battant l'eau tandis qu'ils échangeaient avec sérieux. Ernaut accueillit d'un sourire Libourc qui sortait des flots tout en tordant le bas de sa chemise, dévoilant des jambes d'un blanc laiteux. Le jeune homme s'efforçait de ne pas laisser son regard errer plus qu'il ne fallait sur la jeune femme, qu'il découvrait en sous-vêtements pour la première fois.

La fine étoffe de lin souple lui dévoilait bien plus que ce qu'il avait pu imaginer jusqu'alors et il pouvait deviner les hanches, les cuisses et la poitrine de Libourc tandis qu'elle s'employait à rattacher correctement sa lourde natte. Légèrement échevelée, elle lui adressait un sourire à chaque fois que leurs regards se croisaient. Ce semblant de toilette achevé, elle vint auprès du jeune homme, s'agenouilla à ses côtés, tout en maintenant une prudente distance. Tandis qu'elle tranchait un peu de pain et de fromage, Ernaut voyait le tissu épouser sa peau, l'encolure dévoiler un peu de son épaule, l'ourlet abandonner une cheville fine. Elle finit par s'assoir en tailleur face à lui, dévorant un en-cas avec enthousiasme.

« Quelle magnifique journée, puisse-t-elle ne jamais finir ! »

Pour toute réponse, elle n'obtint d'Ernaut qu'un sourire. Elle plissa les yeux, le visage illuminé de joie.

« Quelque malaise t'a gagné, que tu ne souhaites pas te baigner? Tu m'as pourtant confié aimer fort cela.

--- Je... Oui, j'ai crainte que mon mal n'empire. »

Elle prit un air désolé.

« Profitons du soleil ensemble alors...

--- Les premières ardeurs sont les plus agréables. D'ici peu, nous devrons le fuir.

--- Quel dommage que nous n'ayons pas si belle eau au casal, pouvoir se baigner ainsi est telle bénédiction.

--- En ville, j'ai pris usage des bains et je dois avouer que je goûte fort cette invention. »

Certaines pensées de ce qui se passait dans ces établissements vinrent soudain à l'esprit d'Ernaut, qui se mordit la langue tandis que ses joues s'empourpraient. Libourc inclina la tête, les yeux emplis de joie, légèrement taquins. Avait-elle perçu l'hésitation dans la voix du jeune homme ?

« Tu voudrais donc nous voir nous installer en ville, pour continuer à profiter de ce confort ? »

Il n'était pas certain du sens à donner à cette question et remua un peu, gêné. La jeune femme semblait s'amuser de la situation, arborant un air d'une totale innocence qui déstabilisait d'autant plus Ernaut.

« Un cours d'eau proche pourra faire l'affaire, ou un cuvier en mon hostel.

--- Messire a goûts de byzantin, à ce que je vois. »

La lueur espiègle qui s'était allumé dans le regard de Libourc arracha un sourire à Ernaut. Il n'était jamais certain de ses intentions, et la femme idéalisée qu'il l'imaginait être était peut-être bien éloignée de la réalité. Elle finit d'avaler quelques miettes et soupira d'aise. Puis tendit la main pour caresser la joue d'Ernaut tandis qu'elle murmurait :

« Je vais encore profiter de ces flots un petit moment, mon beau. Puis je reviens à toi. »

Elle scella sa promesse d'un éphémère baiser sur les lèvres et s'éloigna d'un pas rapide. Tandis qu'elle marchait, elle tenait l'étoffe de sa chemise dans le poing, dénudant ses jambes, faisant voler l'étoffe à chaque pas tandis que le col avait glissé sur une épaule blafarde.

Ernaut goûta sur ses lèvres le souvenir de sa bienaimée, sourit pour lui-même et ferma les yeux sur la vision de la jeune femme. Le moment lui semblait tout indiqué pour une sieste. ❧

### Notes

La cité de Jérusalem, bien que considérée au centre du monde, profitait de son importance religieuse et politique mais, située en dehors des principaux axes, dans une région montagneuse, n'était pas un centre commercial important au Moyen Orient. L'activité marchande y était régulée par l'administration royale, qui établissait les péages et frais de douane qui s'appliquaient aux denrées franchissant ses portes.

La majeure partie des voyageurs qui franchissaient ses muraille venaient en raison de la dévotion envers les lieux saints, surtout aux dates importantes du calendrier chrétien. Bien évidemment Pâques tenait la première place, mais d'autres moments plus laïques, attiraient les foules, comme l'anniversaire de la prise de la ville quelques décennies plus tôt, à la mi-juillet. Les nombreux croyants qui venaient pour visiter les lieux saints le faisaient pour des raisons extrêmement variées, belliqueuses ou pas, par simple foi ou à la recherche d'une nouvelle vie. Il est difficile de se représenter les intentions qui devaient parfois se mêler au sein d'un individu. Toujours est-il que beaucoup de personnes venues d'Europe décidèrent de rester, profitant des opportunités, des offres de peuplement des propriétaires terriens. Ils créérent une société latine originale, en plein Moyen Orient, qui survécut pendant presque deux siècles.

### Références

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Seul au monde
-------------

### Sarûdj, comté d'Édesse, matin du mardi 26 décembre 1144

« Sanguin[^17] a forcé Édesse ! Sanguin a forcé Édesse ! »

Le cavalier menait son cheval comme si la mort le traquait, galopant dans les rues, au travers des marchés, le visage tel celui d'un dément. Il hurlait tantôt en syriaque, tantôt en langue franque. Sa tenue ne laissait guère de doute : il devait appartenir à la garnison latine de la capitale du comté.

La peur, l'incompréhension, la panique jaillissaient sur son passage, comme des flammes dans un amas de broussailles. Parmi la foule, un jeune homme, Iohannes, pinça les lèvres, qu'il avait déjà fines et ferma les yeux, catastrophé. Il stoppa un instant, afin de voir si sa famille réagissait à l'annonce. Il était encombré de nombreux sacs et menait une vieille ânesse sur laquelle était juchée une femme âgée, sa mère, Meskitâ, emmitouflée dans ses voiles. À ses côtés se tenait son épouse, Maryam, le regard bas, avec dans ses bras leur fils, Barsam, né quelques mois plus tôt. Enfin, derrière eux, suivi de leur éternel compagnon, le chien Kâlila, marchait son père Clément, un Franc. Celui-ci se sentait suffisamment vaillant pour arborer son épée, bien que son dos puissant se soit affaissé depuis quelques années déjà ; il porta les doigts à sa bouche et siffla avec vigueur, interpellant le messager :

« Viens-tu de la cité ? Es-tu seul ? »

L'homme stoppa sa monture d'un geste brusque et la fit volter.

« J'en arrive ! Nous ne sommes que peu à nous être enfuis. Nous avons été trahis, les portes ont été décloses alors que nous tenions encore. La brèche récente ne leur avait guère laissé bon passage !

--- L'amân[^18] a-t-il été demandé et accordé ?

--- Pas que je sache, l'archevêque était résolu à tenir jusqu'au retour du comte Joscelin[^19]. »

Clément le remercia d'un signe de tête et salua, avant de se retourner vers son fils, l'air contrarié.

« Je ne peux quitter la cité sans savoir ce qu'est mon frère Colin devenu...

--- Père, il nous faut partir céans, les Turcs de Sanguin peuvent jaillir à tout instant. Peut-être sont-ils déjà en chemin...

--- M'est avis plutôt qu'ils fêtent leur victoire et pillent les maisons de nos amis. »

Il serra le poing sur le pommeau de son arme.

« Cul Dieu ! Que faire ?

--- Jurer et invoquer le nom du Seigneur en vain ne sera d'aucun usage, mon ami » lui lança sa femme, les yeux indulgents, mais le visage défait.

Il acquiesça en silence. Autour d'eux, la rue s'était vidée, les habitants de la ville se préparaient à un assaut, en s'enfermant et en priant, ou en rassemblant quelques affaires de valeur pour aller se cacher au loin. Se grattant la tête comme s'il pouvait en activer le fonctionnement, Clément finit par déclarer :

« Partez pour Bilé[^20]. Il n'y a pas vingt lieues à marcher. Vous devriez y arriver demain ou dans deux jours au plus tard. Vous m'y attendrez. Peut-être Colin voudra-t-il se joindre à nous... Au moins j'en saurai plus.

--- Nous ne pouvons partir sans vous, père, nous risquons de ne jamais vous revoir, répliqua Iohannes.

--- Fais comme je l'ai dit, fils, répliqua le vieil homme, d'un air contrarié. Il ne t'appartient pas de discutailler en semblable moment. Je te confie ta mère, et veille sur ta famille. »

Après une rapide étreinte à sa femme, Clément partit d'un bon pas, sans un regard en arrière. Iohannes soupira, esquissa un timide sourire à Maryam et reprit le chemin de la porte occidentale, vers le plateau en direction de Bilé et de son pont sur l'Euphrate. Kâlila hésita un instant, la truffe indécise, puis disparut à la suite de son vieux maître en deux bonds agiles.

### Édesse, midi du samedi 2 novembre 1146

Maryam sourit doucement à son époux tout en balançant Barsam contre son sein. Elle était trop effrayée pour agir. Dehors, tout était calme dans le quartier, rien ne laissait penser que les Turcs étaient de nouveau dans la ville. De toute façon, ils ne pouvaient encore piller ce que les hommes de Joscelin avaient pris quelques jours auparavant. Devant elle, Iohannes faisait les cent pas, indécis.

« Peut-être que leur nouveau chef, ce Noradin[^21], ne sera pas pire que Sanguin. Ils n'en veulent qu'aux envahisseurs francs après tout...

--- Crois-tu vraiment qu'il goûtera que certains aient ouvert les portes au comte ?

--- Il a besoin de nous, il ne peut faire esclave tous les marchands, artisans ou paysans... Le pays serait ruiné et de peu d'intérêt. »

Maryam leva les yeux au ciel, tétanisée de peur, incapable de répondre. Iohannes hésita un instant avant de continuer :

« Nous n'aurions pas dû revenir, ni surtout, nous séparer ! »

Il ferma les yeux, grimaçant comme si la douleur qu'il éprouvait en esprit affectait son corps entier.

« Il me faut aller chercher mère. Je ne peux la laisser seule en sa demeure, père me l'a confiée. »

Il s'approcha de la porte, marqua un temps avant de retourner embrasser son épouse. Des larmes coulaient des yeux sombres de Maryam, perles délicates qui mouraient sur ses joues. Il lui caressa le visage, confiant.

« Tire fort la barre derrière-moi, je ne serai guère long. »

Tandis qu'il franchissait le seuil, il n'entendit pas Maryam lâcher dans un souffle :

« Ne m'abandonne pas, je t'en prie. »

Il n'avait pas fait deux pas que la porte claquait derrière lui.

Avançant avec précaution, Iohannes se penchait à chaque croisement pour vérifier qu'aucun soldat turc n'était dans les parages, puis il courait jusqu'à la plus proche ruelle, cherchant à éviter les grands axes. Une fois seulement, il entendit des cris, des braillements, des invectives, le chaos d'un affrontement. Il s'activa d'autant plus, rasant les murs comme un chat en maraude.

Arrivé devant la courette donnant accès à la maison de ses parents, il frappa du poing, en se faisant connaître. Une éternité s'écoula avant que sa mère ne vienne lui ouvrir. Elle était armée d'un long couteau. Le soulagement se lisait sur son visage tandis qu'elle attrapait quelques affaires pour suivre son fils. Tout au long du trajet, elle ne lui lâcha la main à aucun moment. Elle retint un cri d'horreur lorsqu'il leur fallut enjamber plusieurs corps, entassés de façon grotesque dans une mare écarlate. Des soldats francs en armure, sûrement des sergents du comte, dont les têtes coupées avaient été amassées en un pitoyable trophée attirant les mouches et les chiens errants. De la fumée s'élevait des environs de la citadelle. Le fer, le feu et le sang.

Il ne semblait au jeune homme qu'il n'avait connu que ça depuis qu'il était né. Lorsqu'ils parvinrent dans son quartier, son cœur s'arrêta : des Turkmènes s'éloignaient en braillant, de nombreux objets sans valeur éparpillés au long de la rue dans leur sillage. Il courut jusqu'à sa maison. La porte en était fracassée. Des pleurs s'en échappaient. Au milieu de la pièce principale, assis parmi les vestiges de tous ses biens se tenait un enfant, son enfant, Barsam, hurlant de peur, un morceau d'étoffe déchiré à la main, seul.

### Route de Joha, après-midi, mardi 22 août 1150

Le nuage de poussière était si dense qu'il fallait se protéger la bouche pour ne pas en avaler à chaque bouffée d'air. Les arbres, les hommes, les bêtes, tout était recouvert d'un film cendreux, pénétrant dans les narines, les oreilles, les yeux. Depuis plusieurs lieues Iohannes portait son fils à demi endormi, trop épuisé désormais pour demander à boire comme il le faisait jusqu'alors. Hagard, le petit fixait sa grand-mère Meskitâ avancer d'un pas lourd, appuyée sur son bâton. La bouche protégée de son voile, elle marmonnait pour elle-même, des prières pour en appeler à l'aide divine ou des imprécations contre tous ces soldats qui les jetaient une nouvelle fois sur les chemins.

De temps à autre, ils étaient bousculés par les cavaliers qui venaient repousser quelques Turcs agressifs. Mais ils ne s'en occupaient guère, trop épuisés, trop fatigués, brisés par des années de combat, de pillage, de fuite, où l'ennemi avait eu tous les visages. Ils se contentaient de suivre la colonne de réfugiés en direction d'Antioche.

Le comté était définitivement perdu, Joscelin avait été pris, emmené au loin dans une geôle sarrasine. Le jeune roi de Jérusalem, Baudoin, était venu négocier avec le basileus grec[^22] et lui avait cédé les dernières forteresses encore tenues par les Francs. Alors qu'il allait repartir, il avait eu pitié des populations livrées à elle-même. Il avait donc organisé un convoi et disposé ses hommes, ceux de la principauté d'Antioche, du comté de Tripoli, de façon à protéger les miséreux chassés de leurs foyers. Et depuis, ils marchaient sous le féroce soleil estival, harcelés par les troupes de Nūr ad-Dīn.

Une clameur agita la colonne tandis que des cavaliers ennemis s'approchaient au grand galop. Habituellement, ils lâchaient leurs traits de trop loin pour que les lourds chevaliers francs puissent les frapper sans se mettre en péril. Les arbalétriers, les archers, tentaient de répliquer, mais le temps qu'ils se déploient, les Turkmènes étaient généralement déjà repartis. Il suffisait de s'abriter derrière un muret, un arbre, un animal, et d'espérer. Les lèvres craquelées en profitaient pour avaler quelques gorgées d'eau tiède et boueuse, avant de reprendre la route.

Cette fois, les sarrasins ne décochèrent rien et ils arrivèrent au contact, brandissant leurs sabres, leurs masses, leurs lances, taillant, brisant et perçant. Leurs torses étaient recouverts d'armures métalliques et des plumes ornaient leurs casques d'acier peints. C'étaient des guerriers lourds, des Turcs d'élite, pas de simples nomades.

Iohannes attrapa sa mère et la protégea de son bras, l'emmenant à l'écart du chemin, contre un buisson où ils pourraient se lover tous les trois. Il tentait de se garder de droite et de gauche, comme un animal effrayé, perdu au milieu des destriers, des coups de lame, des corps qui tombaient. Il ne vit rien d'annonciateur à la douleur qui lui fit exploser le crâne. En un instant, les ténèbres remplacèrent la poussière.

Lorsqu'il retrouva ses esprits, Iohannes était installé sur un cheval. Balloté, il était maladroitement soutenu par quelques compagnons charitables. Il ouvrit les yeux et fut aveuglé par la lumière. Sur ses lèvres, un goût métallique déplaisant, du sang. Il porta la main à sa tête, sentit un bandage, comprit. Puis il chercha sa famille, mais n'aperçut que des visages inconnus, d'hommes de guerre fatigués, transpirant. Des Francs. À ses côtés, un des hommes qui le soutenait lui adressa un sourire, simple fente dans sa barbe enduite de poussière collée :

« Alors, l'ami, toujours vivant ? »

Iohannes voulut répondre, mais sa bouche, sa gorge, ses lèvres étaient trop sèches pour ça. L'homme lui tendit une gourde presque vide, d'un air aimable. Il en avala un peu, juste de quoi pouvoir susurrer :

« Et ma mère, mon fils ? »

L'homme le regarda, les sourcils écarquillés, une moue sur le visage.

« Tu étais seul, l'homme. On t'a trouvé quand les hommes de Triple[^23] ont bouté fort ces maudits païens. Tu gémissais alors on t'a ramassé. Je t'ai même prêté ma selle le temps que tu te reprennes. »

Il se tourna vers un de ses compagnons, moins bien équipé.

« Tu as vu s'il y en avait d'autres encore vifs ?

--- Certes non, maître Ucs, en tout cas, ni femme ni enfançon. Et ma main au feu qu'il n'y en avait aucun d'occis non plus. Ils sont peut-être plus avant en la colonne. »

Iohannes ferma les yeux, trop accablé pour réagir. Ce fut le sergent qui le rattrapa à temps :

« Prêtez main-forte, vous autres, il a encore tourné de l'œil ! » ❧

### Notes

Le comté d'Édesse fut le premier territoire d'importance perdu par les forces latines arrivées depuis la Première Croisade. Si la cité principale tomba en 1144, cela faisait déjà plusieurs années que le comte Joscelin II tentait de faire face à des adversaires de plus en plus déterminés. La perte des villes se fit comme une lente agonie, lors de laquelle les populations furent durement malmenées, selon qu'elles oscillaient vers un pouvoir ou l'autre. Majoritairement chrétiennes, elles avaient été déçues par leurs nouveaux dirigeants, mais ne pouvaient être assurées du sort qui leur serait réservé sous domination musulmane. Pourtant, des mariages mixtes avaient permis la création de nombreux liens entre indigènes et Européens croisés, la lignée comtale elle-même étant en partie d'ascendance arménienne.

### Références

Amouroux-Mourad Monique, *Le comté d'Édesse 1098-1150*, Paris : Librairie orientaliste Geuthner, 1988.

Guillaume de Tyr, *Historia rerum in partibus transmarinis gestarum*, livre 17, chap.17, Recueil des Historiens des Croisades, Occidentaux tome I, p.786

Ost réal
--------

### Baisan, soir du vendredi 30 mai 1158

Au loin se dessinait la modeste fortification, entourée de palmiers et de sycomores, noyée parmi les maisons du casal et les ruines antiques. Un tel amas de pierre taillées était toujours une aubaine, et on ne se privait pas d'y prélever tout ce qui pouvait être utile. En deçà se dessinait la butte plongeant dans la vallée du Nahal Harod. Eudes ralentit un peu l'allure et chercha un meilleur confort en selle.

Le cri d'un rapace lui fit tourner la tête en direction du fleuve, dont il apercevait la ligne verte sinuant au milieu des plantes desséchées à l'approche de l'été. Un lent convoi soulevait la poussière du chemin, s'éloignant en direction de l'ouest, sans qu'il ne soit possible de savoir si c'étaient des voyageurs, des commerçants ou des soldats. La plaine qui s'étendait entre Samarie et Galilée, bordée par le Jourdain, était parsemée de villages et de champs, de cultures dont il ne restait désormais plus que les traces poussiéreuses. Les zones iriguées par la main de l'homme arboraient un vert insolent ponctuant l'étendue roussie.

Malgré une position peu éminente à la Haute Cour, Adam de Baisan était un homme riche et puissant, qui devait le service de vingt-cinq chevaliers. Jouxtant la zone frontière, il maintenait l'ordre dans la région et protégeait un accès possible vers le cœur du royaume.

Dépassant sans ralentir le pas les maisons qui se ramassaient autour de la fortification, dans l'ombre des bosquets, Eudes avaient hâte de délivrer son message. Il avait chevauché depuis le matin, quittant Jérusalem aux aurores pour parvenir ici avant la fin de journée. Voyant des valets occupés à ranger et nettoyer une vaste zone aménagée pour la cavalerie, il ne put retenir un sourire. Le seigneur de Baisan maintenait ses troupes en excellente forme et les faisait s'entraîner régulièrement. Recevoir une rente de ses mains n'avait pas que des avantages. On passait plus de temps à user les cuirs de selle qu'à profiter de ses besants et des bliauds de soie rebrodée. On le disait également grand amateur de chasse, n'hésitant pas à aller chercher des proies et du gibier à son goût par-delà le Jourdain.

À peine Eudes eut-il le temps de s'approcher de la porte de la forteresse qu'un court son de trompe résonna et plusieurs têtes s'affichèrent entre les merlons. Une voix, avec un fort accent levantin, lui demanda de se faire connaître.

« Je suis Eudes, sergent le roi Baudoin, en service pour mander message au sire Adam. »

Un sifflement rapide fit ouvrir la porte et le cavalier entra. Des valets vinrent lui tenir la bride et l'un d'eux apporta même un pichet de vin coupé. La cour, empierrée grossièrement, résonnait de l'activité d'un domaine bien tenu. Plusieurs hommes allaient et venaient depuis les écuries, menant eau et foin aux montures qui avaient participé à l'exercice. Un bâtiment encombré d'échafaudages et d'appentis accueillait plusieurs maçons et la loge des tailleurs de pierre, qui devaient adapter les blocs arrachés aux ruines romaines voisines. Un nouveau logis prenait forme le long du mur est, dans le bruit régulier des poinçons frappant la pierre. Enfin, sous les arcades proches d'un haut bâtiment pourvu d'une cheminée, les cuisines certainement, des femmes s'employaient à plumer des volailles. À cela s'ajoutait des cohortes de serviteurs qui allaient et venaient d'un logis à l'autre, les bras encombrés de bois, de vêtements, d'outils, ou dans l'intention d'en quérir. Plusieurs enfants s'amusaient autour d'un arbre où ils avaient aménagé une cabane comme siège de leurs exploits.

Guidé par un jeune serviteur d'une douzaine d'années, Eudes monta à l'étage de la tour. Là, dans une vaste pièce aux murs chaulés et ornés d'un décor sommaire de faux appareil, le seigneur Adam de Baisan discutait avec plusieurs hommes, le teint aussi hâlé que le sien, en chemise et chausse, ainsi que le font des compagnons d'aventure. Ils étaient assis autour d'une table, avec une collation légère et plusieurs pichets de vin, qu'ils buvaient dans des verres ornés de pastilles colorées.

À l'écart, auprès d'une fenêtre, plusieurs femmes habillées de tenues élégantes discutaient tout en admirant des étoffes. Le valet abandonna Eudes près des soldats, qui se turent, curieux. Le seigneur du lieu avait une voix douce, agréable, mais la bouche était façonnée pour mordre, pour diriger les hommes dans la bataille. Il fronça les sourcils, habitué à plisser les yeux pour se prémunir du soleil et examina en détail la tenue d'Eudes tandis qu'il parlait. Ce faisant, il oscillait de son large buste.

« Quelles nouvelles portes-tu de notre biau sire Baudoin roi de Jérusalem[^24] ?

--- Sire Adam, le roi vous mande à son ost, pour chevaucher sus le turc Noradin[^25], qui assaille présentement une forteresse royale.

--- Mes lances sont prêtes, j'ai ouï parler de la campaigne des infidèles. Où le roi nous veut-il ?

--- Il cheminera au plus tôt, pour la cité de Tibériade avec le sire Thierry, comte de Flandre. Le connétable fera le conroi de là. »

Adam hochait la tête doucement, inventoriant en silence ce qu'il lui faudrait préparer. Il ne sembla reprendre conscience de la présence d'Eudes qu'un petit moment plus tard. Celui-ci n'avait pas osé bouger.

« As-tu autres missions à accomplir ?

--- J'ai messages pour le sire prince de Galilée et puis je dois joindre Saint-Jean[^26] où il se peut encontrer quelque seigneur porteur de croix désireux de batailler avec le sire roi.

--- Tibériade est trop loin pour que tu y sois ce soir, passe-donc la nuit en mon hostel, je te ferai lever tôt à la matinée, ce sera mieux.

--- Grand merci, sire. »

Le chevalier fit un sourire poli et se détourna de Eudes afin de discuter de la campagne à venir avec ses hommes. Le sergent sortit de la salle et retrouva la chaleur de la fin de journée.

Le ciel n'était plus d'un bleu uniforme, des lueurs orangées commençaient à le ronger du côté de la mer. Eudes héla un valet et demanda un coin où s'installer pour la nuit. Puis il rejoignit un petit groupe de curieux qui tapait des mains et chantait. Une troupe de bateleurs présentait un rapide aperçu du numéro destiné à animer le souper du seigneur du lieu. Ils sautaient et jonglaient, accompagnés d'une chalemie[^27] et d'une cornemuse. Une veillée de plus avant que la guerre n'arrive.

### Tibériade, midi du samedi 31 mai 1158

Le soleil faisait scintiller les eaux du lac sous un ciel orné de rares nuages d'altitude. Eudes chevauchait sur un chemin ondulant le long de la berge étroite, parmi les palmiers et les broussailles épineuses. Des canards disputaient à une bande de foulques le contrôle de la zone qu'il venait de quitter, où des touffes de roseaux s'accrochaient à la berge rocailleuse. Il avait chevauché un temps avec un cavalier au service de l'Hôpital, qui rapportait des nouvelles à l'intendant local. La préparation de la guerre touchait aussi les ordres religieux, qui avaient à nourrir et équiper de nombreux hommes. Mais un faux-pas de sa monture en avait arraché un des fers, la rendant boiteuse. Eudes avait donc dû laisser son compagnon de voyage en arrière, ne pouvant se permettre le moindre retard.

Il était soulagé d'entrer enfin dans les faubourgs, au sud de la cité, parmi les antiques rues laissées à l'abandon ou transformées en jardin. Il ne venait pas souvent dans la principauté, et appréciait d'autant plus le lieu. Sur les berges d'un vaste lac, où le Christ et ses disciples avaient vécu, la ville offrait un abord moins austère que Jérusalem, perdue au milieu des reliefs rocheux et poussiéreux. Les jardins étaient luxuriants et on disait que les bains, réputés depuis l'époque romaine, alimentés par des sources chaudes, garantissaient santé et vigueur.

Il chevauchait tranquillement parmi les ruines de l'ancienne cité le long d'un aqueduc, parmi les dalles oubliées, les murs éventrés et les colonnes tronquées. Certaines rues étaient encore habitées, souvent autour des édifices religieux ou de certaines zones thermales, mais la plupart avaient été transformées en verger, pâture ou simplement abandonnées aux herbes folles et à la végétation. Peu avant d'arriver à la muraille, il entendit résonner le marteau sur l'enclume, dans un bâtiment de belle taille qui devait abriter un établissement d'importance. Une forge, en dehors de la ville elle-même, pour éviter le bruit et minimiser les risques d'incendie.

Le sceau royal lui permit de passer rapidement dans les rues, où il demeura en selle. Tibériade n'était pas une cité très animée. On y croisait plus de pêcheurs que de négociants. Dépassant un entrepôt où des enfants et quelques femmes s'affairaient à enfourner justement des poissons dans des tonneaux de saumure, il fit un détour pour éviter un passage encombré. Une caravane de chameaux déchargeait une impressionnante cargaison de sel dans un bâtiment du port, sous le contrôle d'une poignée d'hommes en riche tenue colorée, abrités sous un auvent de toile sur une terrasse à l'étage supérieur du bâtiment.

Il parvint à l'entrée du château, au cœur de la ville et descendit enfin de selle, les jambes raides, le postérieur endolori. Il se fit connaître comme envoyé du roi, porteur d'un sceau royal pour sa mission. On le mena sans tarder parmi des couloirs frais, blanchis à la chaux, traversant des salles emplies d'une cohorte de domestiques. Par les arcades d'un passage où on l'abandonna un petit moment, il admira la cour centrale. De nombreux hommes discutaient, allaient d'un bâtiment à l'autre dans un ballet bien réglé. C'était une véritable ruche.

« Le sire prince va vous entendre »

L'appel le fit sursauter et il rejoignit le valet jusqu'à une porte qui ouvrait directement dans une vaste salle dont les larges fenêtres sculptées offraient une vue saisissante sur le lac. Il demeura interdit, surpris par la magnificence du lieu. Les murs, peints de tons chamarrés, présentaient une fresque aux motifs géométriques en partie basse tandis qu'au-dessus, des figures s'animaient. Le sol, en terre cuite, était émaillé de carreaux vernissés.

Un groupe d'enfants et de femmes était installé près des fenêtres, occupé à écouter un jongleur déclamer une histoire ou les nouvelles de lointains royaumes. Sur une estrade, à main gauche, un imposant siège sculpté accueillait des coussins et des fourrures de fauves. Devant, un groupe d'hommes discutait autour d'une table, un verre à la main. Tous étaient habillés de tenues de prix, mais sans ostentation. Le domestique l'abandonna près d'eux. Voyant que tous les regards se portaient sur lui et reconnaissant la figure altière du prince Guillaume au sein des chevaliers, il s'inclina respectueusement et se présenta.

Le seigneur de Tibériade et de Galilée avait un physique un peu empâté, mais de longues saisons la lance à la main avaient buriné son faciès mou, pour y tracer celui d'un visage fatigué mais passionné. Ses yeux s'agitaient en profondeur, sous les replis des paupières épaisses et les sourcils perpétuellement froncés.

« Quelles nouvelles le roi Jérusalem, sergent ?

--- Je suis mandé pour vous informer de la venue prochaine du roi en vos terres pour y assembler l'ost réal. Le sire Baudoin court sus la troupe de Norredin qui assaille la forteresse des Caves de Suet[^28].

--- Le coureur est passé par mes terres. Mes féals sont déjà en route pour la cité. J'ai usage d'hoster le roi pour aller férir en terre sarrazine, il ne sera pas dit que j'ai failli à mes devoirs. »

Eudes inclina la tête. Sa mission n'était parfois que formelle, les seigneurs les plus importants, comme le prince de Galilée, ayant leurs propres informateurs, il était fréquent qu'ils prennent leurs dispositions avant même de recevoir les demandes officielles. Il hésitait à préciser la venue du comte Thierry, mais c'était certainement superflu. Il allait se retirer quand le prince l'interpela.

« Tu fais retour en la Cité ce jourd'hui même ? J'aurais message pour le sire roi.

--- Non, sire, je dois encore aller à Saint-Jean, dans l'espoir d'y trouver quelque baron porteur de la croix désireux de se joindre à l'ost.

--- En ce cas, tu peux aller. »

Il le congédia d'un signe de main, portant son attention à un des chevalier à sa droite. Eudes reprit donc le chemin vers la porte qu'il avait empruntée mais un jeune gamin au visage dévoré par une marque lie-de-vin l'intercepta et le mena par un escalier qui donnait directement dans la cour.

« Vot' roncin a été brossé et vous attend, sergent le roi.

--- J'aurais bien aimé croquer collation avant de remettre mon cul sur la bête. Tu ne saurais pas me dénicher un peu de pain et de quoi mettre dessus ? »

Le gamin acquiesça et partit en trottinant. Pendant ce temps, Eudes vérifia ses sangles, inspecta les pieds de sa monture. Il resserrait un lacet maintenant ses fontes quand il vit plusieurs hommes s'approcher de lui. Le plus grand avait une bedaine bizarrement posée sur les allumettes lui servant de jambes. Un long nez pointait hors de sa face, encadré d'yeux globuleux.

Arborant un sourire courtois, il était escorté de deux solides gaillards, dont les muscles saillaient sous les vêtements tandis qu'ils portaient des sacs au contenu cliquetant. Il s'arrêtèrent près d'Eudes et lâchèrent leur butin dans un fracas métallique.

« Salut l'ami, on dit que tu viens de Jérusalem, de l'hostel le roi. Et qu'on te nomme Eudes.

--- On dit vrai. Que me veux-tu ?

--- J'ai nom Daimbert. On a là quelques rapines de nos derniers combats avec les païens. Il paraît que tu saurais quoi en faire. Ici, pas moyen de trouver à qui vendre un bon prix. Si tu nous trouvais des acheteurs, on serait prêt à partager avec toi les cliquailles. »

Tout en parlant, il dévoila le contenu de ses sacs : une demi-douzaine d'épées musulmanes, en assez bon état mais sans fourreau, des têtes de masse en bronze, en forme d'étoile ou de concombre, un casque d'origine turque. Et surtout, un large lot de pointes de flèches. Eudes se pencha pour les examiner.

« Ce sont des traits turcs, pas évident à emmancher pour nous autres...

--- Ouais, je sais bien, mais ce serait dommage de les vendre à fonte.

--- Le truc, c'est que je n'ai rien sur moi, là.

--- Aucun souci, mes deux compères seront témoins de ce que tu prendras. Je serai à la Cité pour la Toussaint, si d'ici là tu ne m'as pas payé. »

Eudes examina une fois encore les armes, hésita. Pendant ce temps, le gamin était revenu avec un morceau de pain, des dattes et plusieurs lanières de poisson séché.

«Je te prends les épées, ça je sais que je pourrai leur trouver acheteur.

--- Bien, je vais te les nouer dans un de ces sacs. »

Rapidement, les armes furent emballées dans un morceau d'étoffe grossière. Eudes jura devant les deux hommes qu'il acceptait le marché et conserverait un tiers du prix de la vente pour ses efforts. Daimbert et ses acolytes l'accompagnèrent à la porte de la cité et le saluèrent lorsqu'il lança sa monture.

En ce début d'après-midi, le soleil régnait sans partage sur les collines de Galilée, dont les teintes vertes du printemps roussissaient chaque jour un peu plus. Eudes sortit son vieux chapeau de paille et s'en coiffa, le fixant avec un morceau de tissus tandis que sa monture entamait la montée des reliefs conduisant vers l'ouest.

« Foutu moment pour prendre la route » pesta-t-il entre ses dents.

Il n'obtint pour toute réponse que le chant des cigales et des grillons.

### Saint-Jean d'Acre, soirée du samedi 31 mai 1158

Le ciel commençait à rougeoyer, projetant des raies enflammées sur les crêtes écumeuses des vagues. Une brise de mer apportait des senteurs salines et iodées, repoussant la puanteur des quartiers du port. La sueur des portefaix s'ajoutait aux effluves des mille produits déchargés là chaque jour, en plus des relents issus des ruelles, des latrines et des tas d'ordures que les porcs fouaillaient avec des grognements de plaisir.

Eudes avalait quelques beignets à la saveur épicée, aux rares morceaux de viande. Assis sur un muret face au port, il admirait la danse des mâtures au repos. Une armada de barques et de barges environnait les grosses nefs ventrues, les galées racées. Des lanternes accrochées aux châteaux étaient allumées peu à peu. Déjà, les voix se déplaçaient depuis les quais vers les rues environnantes, où les dos fatigués et les mains calleuses cherchaient le réconfort dans le jus de treille.

Il sortait à peine de la Cour de la chaîne, à la pointe sud de la cité, où il avait trouvé le châtelain royal. Les fortifications n'étaient pas si confortables que les vastes salles du bâtiment des douanes, même s'il était agité d'une activité bourdonnante du matin au soir. Il avait pu porter la convocation royale et n'avait plus qu'à se rendre à l'Hôpital, près de la muraille, au nord. La plupart des voyageurs fraîchement débarqués se retrouvaient là le temps de s'organiser. Un excellent endroit où rencontrer des chevaliers en quête d'une cause à laquelle vouer son épée le temps de son séjour en Terre Sainte.

Jetant quelques miettes aux mouettes, il se leva et remonta la grande rue qui menait droit vers l'église Saint-Jean Baptiste. Les minces échoppes s'alignaient, proposant à la fois les délices de l'orient aux nouveaux venus et les souvenirs de l'occident pour les exilés. Les devantures se fermaient tandis qu'il avançait, les paniers étaient rentrés à l'abri, les éventaires relevés et les sacs rangés. Des lueurs orangées se répandaient depuis les ouvertures, claustras et fenêtres des étages tandis que des voix se faisaient entendre sur les terrasses. Des chiens à l'allure misérable furetaient dans les ordures abandonnées par les marchands de nourriture. Une carriole emportait une vaste carcasse de viande enveloppée d'un linge humide vers la demeure d'un riche bourgeois ou le fondaco[^29] d'une des opulentes cités italiennes. Acre se préparait pour la nuit.

Dépassant la vaste église d'où provenaient des voix célestes chantant les psaumes pour Vêpres, Eudes pénétra par un passage voûté dans la cour de l'Hôpital. Un portier affalé sur une chaise bancale ronflait consciencieusement dans un recoin, bénissant les arrivants de ses borborygmes. De nombreuses personnes se tenaient là, le long du bâtiment à l'ouest dont la vaste salle de l'étage servait de dortoir. Mais beaucoup dormaient dans l'église ou aux abords. La plupart étaient tellement perturbés après le long voyage en mer qu'ils se contentaient de dormir sous les arcades, profitant de l'air frais après des semaines, voire des mois, enfermés dans une cale malodorante. Quelques silhouettes habillées de bure noire s'occupaient de distribuer des couvertures, de répartir les couchages.

On entamait la saison où les arrivées se faisaient plus nombreuses chaque jour et les frères de l'Hôpital étaient habitués à gérer ce ballet incessant d'allers et venues. L'un d'eux, en tenue grossière et salie, mais avec une tonsure récente et un sourire épinglé sur sa face ronde s'avança vers lui et le bénit.

« Êtes vous marcheur de Dieu, mon frère ?

--- Non pas, frère hospitalier, je suis sergent le roi Baudoin. »

Le moine sourit d'autant plus largement et l'invita à le suivre en direction du bâtiment à l'est.

« En quoi notre modeste maison peut porter aide au sire roi ?

--- Je suis là pour voir si d'aucun chevalier aimerait se joindre à l'ost réal pour chevaucher sus Noreddin.

--- Nul homme d'épée n'est ce jour en nos murs, je suis désolé. On dit que le *Luna*, parti en même temps que le *Peregrina* de Gênes, arrivé voilà plus de dix jours, ne devrait pas tarder. Il s'y trouvera peut-être ce que vous espérez. »

Eudes acquiesça en silence, un peu déçu, et fatigué de ses deux journées à chevaucher. Le clerc le dévisagea et lui posa une main amicale sur l'épaule.

« Auriez-vous désir de prendre votre souper avec nous ? Nous allons entamer la distribution sitôt vêpres achevées, au retour du frère hospitalier.

--- Je vous mercie mais je préfère retourner avant la nuit en mes quartiers, à la forteresse le roi. »

Le moine hocha la tête en silence et le bénit, avant de le laisser pour aller prendre en charge un autre pèlerin. Eudes lança un dernier coup d'oeil circulaire à la vaste cour, bordée de bâtiments industrieux et surplombée par le fin clocher qui s'élançait vers les cieux, à l'est.

« Bon, d'abord, trouver bonne taverne où je pourrai trouver acheteur pour ces lames... »

Enjoué à l'idée de lamper quelques gorgées d'un bon vin importé, en la cité des mille plaisirs, principale porte du royaume, Eudes prit la direction du quartier du port en chantonnant pour lui-même

> « Dormir après souper et faire lardon\
> Engloter poulets, canards et chapons... » ❧

### Notes

La composition des armées royales se basait sur les troupes des vassaux en plus des hommes directement rattachés à la maison du souverain. Il pouvait s'y ajouter des croisés désireux de mettre leur force au service de la chrétienté ainsi que de simples mercenaires, payés à la tâche. À cela s'ajoutaient les hommes entretenus par les ordres religieux indépendants, comme le Temple et l'Hôpital. La réunion de tout ceci demandait donc un certain temps, et l'armée n'était rassemblée que le temps de mener des opérations, de façon à éviter le difficile approvisionnement d'un groupe aussi nombreux, avec une grande quantité de montures et d'animaux de bâts. Contrairement à ce qui était la règle en Occident, le roi de Jérusalem pouvait en théorie maintenir son armée perpétuellement en état de guerre, mais le coût de telles dispositions incitait à se contenter de convocations ad hoc.

### Références

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. IV. The Cities of Acre and Tyre*, Cambridge : Cambridge University Press, 2009.

Smail R. C., *Crusading Warfare, 1097-1193, Second edition with a new Bibliographical introduction by Christopher Marshall*, Cambridge : Cambridge University Press, 1995.

Au nom du père
--------------

### Jérusalem, soir du jeudi 21 novembre 1157

Le glissement des souliers n'était pas le seul bruit qu'on entendait à la sortie du chapitre de Saint-Jean de l'Hôpital. Le cloître bruissait des voix feutrées, des commentaires susurés à son voisin, des questions lancées à ses camarades. Les lampes traçaient des ombres aiguës parmi les feuillages des chapiteaux, habillaient d'or rouge les plantes endormies pour l'hiver, au centre de la cour. La longue silhouette de frère Matthieu était entourée d'un cercle de curieux, impatients d'en apprendre plus. Il était de ceux qui fréquentaient souvent le palais royal, ou voyageaient pour les besoins de l'ordre. Quoique peu porté aux commérages, il aimait à montrer son importance par le savoir qu'il avait du monde par-delà la clôture de leur couvent.

« Il est bien trop tôt pour savoir qui sera le prochain sire patriarche, mes frères.

--- N'y-a-t-il aucun favori ? Le roi a sûrement quelque protégé à mettre en avant...

--- Le sire Baudoin guerroie au nord ces temps-ci. Peut-être ne sait-il pas encore que le vieux Foucher est passé. Je serai bien étonné que la situation soit tôt réglée.

--- Alors, prions pour que le Seigneur nous envoie un prélat plus compréhensif ! »

La dernière remarque avait été proférée avec une colère contenue qui contrastait avec la clémence du propos. Celui qui l'avait lancée était un moine respecté de la communauté, frère Garin, un des responsables des malades en charge d'une des *rues*, une des allées du grand Hôpital. Il avait connu tous les affrontements avec le vieux patriarche et le chapitre du Saint-Sépulcre. Quelques visages hochèrent en assentiment, l'invitant à poursuivre.

« Maintenant qu'il est mort, nul besoin de s'acharner sur sa mémoire et en bons chrétiens, nous le mettrons dans nos prières, comme savent le faire les âmes charitables. Mais j'espère que nous aurons moins d'ennuis avec le prochain.

--- Il avait reçu le siège de Tyr des mains du vieux Guillaume[^30], non ?

--- Si fait, et devenu comme lui patriarche après ça... »

Les visages se fermèrent, le temps qu'ils envisagent la suite logique de la proposition. Une voix timide brisa finalement le silence.

« Le vieux Pierre de Barcelone ?

--- Il est bien trop âgé pour devenir patriarche.

--- Les ans n'ont rien à voir là-dedans. Nous avons bonne entente avec lui pour l'heure. C'est homme de raison, dévot et craintif de Dieu. »

Sur cette dernière remarque, le groupe se défit peu à peu, les clercs profitant du répit avant l'office des vigiles pour vaquer à leurs occupations personnelles. Frère Matthieu s'avançait vers le dortoir lorsqu'une main le retint. Stasino, un autre des responsables d'une des rues semblait ennuyé, les plis de graisse de son visage poupin noyant son regard d'habitude acéré dans une mer d'ombre. Matthieu n'aimait guère les façons de faire de cet Italien, qu'il estimait retors, ou du moins trop emphatique dans ses démonstrations de piété comme dans ses colères. Lui avait appris à demeurer en retrait, calme, ainsi qu'il seyait à un clerc. Un esprit et un corps agités ne pouvaient contenir qu'une âme tourmentée selon lui.

Il fit néanmoins bonne figure et, d'un sourire, invita son compagnon à parler. Ils s'installèrent dans un recoin du cloître, entre les colonnes et les chapiteaux sculptés. Le chant nocturne d'un pipit tardif résonnait parmi les buissons et les arbres soigneusement taillés de la cour.

« Un souci, frère Stasino ? »

Le petit homme semblait osciller d'un pied sur l'autre, les plis de son ample froc jouant dans la chiche lumière des lampes et de la lune. Il passa finalement une main sur son visage replet et prit la parole, visiblement à contrecœur.

« C'est à propos de mon client, celui qui n'a pas pu avoir chrétienne sépulture.

--- Vous parlez de celui qui troubla les Pâques ?

--- Celui-là même, si fait. La désignation du nouveau sire patriarche donnera lieu à moult réjouissances, et pardon sera accordé à de nombreux pécheurs. »

La sincérité et la charité de la demande adoucirent les traits habituellement fermés du grand clerc.

« Il a commis fort méchants péchés, et ne saurait être aisément accueilli dans la communauté.

--- J'en ai bien conscience, frère, et j'avoue que je l'associe malgré tout à mes prières, espérant qu'il trouve par-delà la mort la paix qu'il n'a su encontrer ici-bas. »

Matthieu s'assit sur un des bandeaux sculptés et réfléchit un petit moment avant de reprendre.

« Le sire patriarche Foucher était homme intraitable, accroché au plus modeste de ses privilèges et détestait la moindre offense. Il y a espoir que le nouveau sera plus enclin au pardon, cela ne serait que facile. D'autant qu'il n'aura peut-être pas connu les blasphèmes directement.

--- Vous pensez au sire archevêque Pierre de Tyr alors ? »

Le grand moine haussa les épaules.

« Je l'ai dit, je n'en sais rien encore, il est trop tôt. C'est là usage fréquent, et tradition fort ancienne. »

Stasino acquiesça, rasséréné à l'idée que le débonnaire Pierre de Tyr remplace le prélat autoritaire qu'était le vieux Foucher. Il fut coupé dans ses pensées par Matthieu.

« N'oubliez pas que le roi aura son mot à dire. Le sire évêque Raoul, son chancelier, aura peut-être désir de prendre revanche sur Pierre. Il n'a pu garder le siège de Tyr, il apprécierait de certes être celui qui couronne les rois.

--- Est-il homme de pardon ?

--- Comme chacun de nous, frère Stasino. Plus enclin à pardonner les offenses faites à d'autres qu'à lui-même... »

### Tyr, midi du mardi 7 janvier 1158

Accueillant deux braseros, la petite pièce était étouffante de chaleur. Pourtant, le vieux Pierre de Barcelone, archevêque de Tyr était assis sous une couverture fourrée, et un bonnet de feutre écarlate retombait sur son front parcheminé. Le visage las, creusé de rides, respirait la fatigue, sinon la tristesse. Une main hésitante, aux ongles soignés, aux veines saillantes, émergeait de l'amas de vêtements brodés et passementés, serrant un verre vide rehaussé de motifs en relief.

À ses côtés, installé lui aussi sur une chaise curule, mais d'une façon beaucoup plus maitrisée et stricte, un clerc au nez pointu, au regard vif, attendait, prêt à noter sur une tablette de cire ce que l'archevêque lui dicterait. Il sirotait tranquillement son vin, cherchant à se faire oublier dans la discussion, comme le moine discret qu'il aimait être.

Assis face à eux, un autre prélat se tenait, de grande taille, plus jeune, et surtout plein d'énergie. Il était vêtu d'une tenue de prix, d'un bliaud qui n'aurait pas dépareillé à la Haute Cour. Néanmoins, sa tonsure soignée ne laissait pas le moindre doute sur son statut, pas plus que la lourde croix émaillée qu'il arborait sur la poitrine, ainsi que son anneau. Frédéric de la Roche, évêque d'Acre était un homme à l'activité débordante, qui accompagnait volontiers le roi Baudoin lors de ses expéditions guerrières. On le disait aussi habile à parler de Végèce[^31] qu'à citer les Pères de l'Église. Il digérait les paroles du vieil archevêque tout en faisant glouglouter le vin entre ses joues, l'air pensif. Il semblait percevoir la ville et, au-delà, la mer par les deux petites fenêtres donnant au nord. Puis son regard revint sur Pierre, sur le clerc à ses côtés, sur les documents qui recouvraient la table sombre, sur les plumes et les encriers précieux, pour mourir sur les motifs arabesques des fresques murales. Ce n'est qu'en arrivant sur l'imposant Christ de bois peint qui paraissait les dévisager tous qu'il se décida à ouvrir de nouveau la bouche.

« Mon sire, si vous ne souhaitez pas devenir patriarche, cela va sans nul doute inciter à plus de candidatures. Le royaume se remet à peine des troubles de la succession.

--- Je sais bien que d'aucuns estiment qu'il est d'usage pour l'archevêque de Tyr de finir patriarche, mais Dieu m'est témoin que je suis bien fatigué assez pour m'en garder. Nul doute que le roi saura choisir celui qui convient le mieux. »

Le vieil homme sourit, sans joie, et planta son regard dans celui de son suffragant.

« Ne soyez pas inquiet, je n'ai guère d'appétit pour un autre siège, et guère plus pour la chère. Mon siège sera vacant assez tôt pour que vous puissiez y prétendre.

--- Père, mes prières ne demandent qu'à vous soutenir, loin de moi l'idée...

--- Ne vous en défendez pas, mon fils, votre espoir est bien légitime. Vous êtes homme de sapience et au fait des affaires du siècle. Dieu ne veut que mettre ainsi votre patience à l'épreuve, car c'est là qualité essentielle pour le berger. Je vous porte en assez bonne estime pour assavoir que vous préféreriez me succéder en me sachant à Jérusalem qu'allongé en mon tombeau. »

L'évêque sourit de mauvaise grâce à cette assertion perfide et reposa son verre, se levant dans le même mouvement.

« En ce cas, l'élection sera encore décidée par les gens de la Haute Cour.

--- La reine conserve forte influence sur ces choses. Sa piété et ses largesses donnent beaucoup de poids à ses recommandations, sans parler même de ses sœurs, la mère Yvetta ou la comtesse de Flandre.

--- Avez-vous idée de leur favori ?

--- Vous êtes plus au fait des choses du siècle que moi, mon ami. »

L'évêque Frédéric opina sans un mot puis s'avança et s'inclina pour baiser la bague de son supérieur. L'archevêque Pierre n'était pas un homme intéressé par la politique, du moins pas par celle qui dépassait le territoire dont il avait la charge.

Le calme demeura un long moment après le départ de l'ecclésiastique. Herbelot, le clerc, faisait glisser parfois des documents les uns sur les autres, dans l'espoir que cela attirerait l'attention du prélat. Celui-ci s'obstinait à fixer sa main, qui avait repris le verre vide. La voix fatiguée, éraillée lorsqu'elle n'était pas employée à un sermon dans la basilique, brisa le silence feutré.

« Dis-moi, Herbelot, que penses-tu de notre évêque ? »

Le jeune clerc se tortilla sur son siège, mal à l'aise à l'idée de se montrer irrespectueux. On lui avait inculqué l'importance de l'obéissance pendant toute son enfance chez les moines, et il ne savait pas comment se comporter face aux vieux renards de la politique. En outre, il était persuadé également que, bien souvent, les questions qu'on posait aux subalternes comme lui n'étaient que rhétoriques et qu'il suffisait d'attendre. Son intuition et sa prudence furent une nouvelle fois payantes.

« Tu n'en dis rien ? Voilà bien sage opinion. J'ai forts regrets de voir un clerc si empli de science se consacrer au siècle avant tout. Que nous importe de savoir qui sera le patriarche ? Nous le saurons bien assez tôt. Je me demande d'ailleurs s'il n'avait espoir de m'inciter à avouer vers qui mon cœur penche. »

La dernière phrase s'acheva dans un souffle et le silence revint. Herbelot en profita pour tenter de glisser quelques documents, chartes et diplômes que l'archevêque devait viser avant leur mise au propre. Mais Pierre de Tyr n'avait pas envie de s'adonner à des tâches administratives et survola les feuillets d'un œil indifférent avant de se tourner vers son clerc, de nouveau.

« Tu es comme moi, Herbelot, un homme de cloître, de silence et de prière, n'est-ce pas ?

--- Je le confesse bien volontiers, père.

--- En fait j'ai espoir que le prochain patriarche sera ainsi. Notre bienaimé père Foucher, que Dieu l'ait en sa sainte garde, était un grand homme, apte à faire de belles et louables choses. Mais j'ai parfois regretté qu'il ait dû se plonger si fort avant en le siècle. Frédéric est ainsi, comme l'était aussi Raoul de Bethléem, qui tenta de me ravir ce siège à mes débuts.

--- *Maria optimam partem elegit quae non auferetur ab ea*[^32] »

La citation fit naître un sourire enjoué, presque infantile, au sein des traits affaissés du vieil homme qui reposa enfin son verre. Puis il vint tracer d'un doigt léger un signe de croix sur le jeune clerc, le regard empli de bienveillance.

« Amen »

### Bethléem, matin du mardi 18 février 1158

« Le sire roi ne fera donc rien contre cela ? »

L'évêque Raoul de Bethléem était furieux, il secouait les manches tout en hurlant, allant et venant dans la petite pièce qui lui servait de cabinet de travail.

Face à lui, le sergent Baset, de l'administration royale, n'en menait pas large. Il avait été envoyé pour informer le chancelier de Baudoin que ce serait finalement le prieur du Saint-Sépulcre, Amaury de Nesle, qui avait la préférence pour l'élection au patriarcat de Jérusalem. Par la même occasion, il avait escorté Daimbert, le clerc principal qui supervisait toute la correspondance officielle du prélat et du royaume. Lorsqu'on le congédia d'une main agacée, il détala comme un lapin dans l'escalier de la tour, laissant retomber la lourde portière de tissu derrière lui. Il manqua de peu de glisser en dévalant les marches, soucieux de mettre le plus de distance entre lui et la méchante humeur de Raoul. Il pouvait encore entendre vociférer l'intraitable ecclésiastique depuis l'étage inférieur, dans un grondement en partie étouffé.

« Je pensais que le roi me soutiendrait, moi qui le sers fidèlement depuis des années !

--- Je crains, mon sire, que le roi n'ait justement quelque crainte de vous perdre comme chancelier si vous deveniez patriarche. Nous avons déjà vécu cela par le passé. »

Raoul fit une grimace d'agacement et fronça les sourcils en direction de son clerc.

« Je sais bien, mais la situation n'est pas la même ! Le pouvoir du roi n'est plus si fragile. »

Il rumina un instant et s'abattit sur son siège, affalé comme un ivrogne. Dépité, il se passa une main sur le front.

« Ce ne peut être que Mélisende, cette vieille truie ! »

Le clerc sursauta à l'insulte, choqué et écarquilla les yeux. Raoul s'en aperçut et concéda un rictus d'excuse.

« Je ne sais ce qu'elle a à me reprocher ! Je l'ai fidèlement servie, et elle m'a déjà trahi par le passé. Quels gages lui faut-il donc ?

--- On dit que le prieur Amaury avait le soutien de mère Yvetta surtout, et celui de la comtesse de Flandre.

--- Elles ne sont rien...

--- Peut-être pas, mais on les voit souvent à la Cour, et nul ne peut nier leur engagement auprès de l'Église.

--- Le jeune Amaury de Nesle... »

L'évêque semblait abattu, comme si le monde venait de s'écrouler. Il chercha un gobelet de verre peint et le tendit à Daimbert pour qu'il le lui remplisse. Puis il avala en silence plusieurs gorgées, analysant la situation. On pouvait presque entendre les engrenages s'entrechoquer dans sa cervelle affairée, calculatrice, méthodique.

« On le dit fort peu au fait du siècle, homme de cloître et d'autel... N'y aurait-il pas là quelque manigance de l'évêque d'Acre ?

--- Il aurait été plus adroit pour lui de pousser son archevêque, de façon à pouvoir prendre sa place.

--- Pierre de Barcelone n'a nulle ambition, il est là justement pour cette raison, j'ai payé bien assez pour le comprendre. Par contre, le voilà fort âgé, et Frédéric d'Acre espère peut-être le voir gésir en tombel d'ici peu. »

Il avala d'une longue lampée bruyante le fond du verre, le reposa avec douceur sur un coffre marqueté puis se leva, avant d'aller à la fenêtre. En ces mois de frimas, le châssis était fermé, mais il le débloqua et huma l'air froid qui s'engouffrait dans la pièce, en provenance des monts de Judée qui se déployaient devant lui.

« Les puissants de ce monde sont bien ingrats, Daimbert. La reine a payé ma fidélité de son désaveu et le roi récompense mes talents par l'égoïsme. *Quæritis me non quia vidistis signa, sed quia manducastis ex panibus et saturati estis*[^33]. Le seul qui vaille la peine dans cette famille, c'est le jeune Amaury.

--- D'ailleurs, avec la mort du vieux Foucher disparait le principal opposant à son mariage avec la jeune Courtenay. »

Raoul tourna la tête vers son clerc et lui accorda un sourire rusé.

« Ah, Daimbert, Daimbert, que deviendrais-je sans toi ? »

Il vint poser une main amicale sur l'épaule du petit homme puis reprit place dans son siège.

« Donne des ordres, je repars pour la Cité. Il me faut entretenir le comte de Jaffa de mon soutien en cette histoire de consanguinité. » ❧

### Notes

Le royaume de Jérusalem n'était pas un territoire comme les autres et les affaires ecclésiastiques étaient intimement liées à la politique séculière. Tous les postes des grands prélats avaient un pouvoir réel au point de vue administratif et militaire. Le roi de Jérusalem et la noblesse en général s'employaient donc à influencer toutes les élections, sans que cela n'engendre pour autant d'opposition de principe comme la querelle des Investitures dans les pays européens.

L'équilibre était maintenu par un savant jeu diplomatique, de manipulations de la part des ordres religieux, des clercs et des nobles.

On s'associait par communauté de point de vue, parfois de façon temporaire, sans vraiment tenir compte de son appartenance à un milieu ou l'autre. Chacun avait à cœur de ménager ses intérêts propres, ceux de son groupe d'origine, tout en pesant au mieux sur la politique du royaume (et des états latins d'Orient en général). Cela ne signifiait pas pour autant que les ecclésiastiques étaient des cyniques sans états d'âme, le comportement d'un certain nombre d'entre eux démontra au contraire qu'ils étaient au final des serviteurs assez scrupuleux de l'Église.

### Références

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972, p.93-182.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Scripta manent
--------------

### Saint Jean d'Acre, fin de matinée du jeudi 14 octobre 1154

La matinée tirait à sa fin, les canots de pêcheurs étaient déjà pour la plupart rentrés et les quais empestaient les prises fraîchement libérées des filets. Une nuée de mouettes et de goélands tournait aux alentours, plongeant dans les vagues emplies des déchets rejetés par des mains expertes. Dans la zone réservée aux poissonniers, sur la rive ouest, les chalands se pressaient, inquiets d'obtenir les meilleures pièces. Certains poissons étaient encaqués directement, au milieu des barques échouées. Les derniers navires en partance pour l'Europe faisaient le plein avant la clôture hivernale des mers, et les affaires allaient bon train.

Affalés contre la coque d'un imposant canot à voile, quelques hommes se partageaient des brochettes, du pain et une outre de vin tout en jaugeant les passants. L'un d'eux, le torse vigoureux, taillé comme un lutteur, conservait le regard au loin, les yeux plissés. Nullement en repos, il était à l'affût, aux aguets comme un chien de chasse. Un de ses voisins lui donna un coup de coude dans les côtes :

« Le *Leo*, c'est pas un bateau de Césaire ? Les païens l'ont pris, Gaston ? »

Le costaud grogna et avala sa bouchée avant de répondre d'une voix enrouée :

« Aucune idée. Sûrement. »

La réponse laconique ne satisfit pas son compère qui insista, après avoir avalé une goulée de vin.

« Quand même, on est pas beaucoup pour assaillir pareille nave ! Je l'ai déjà vue à quai.

--- Le sire connétable a dit qu'on n'aurait pas de souci.

--- Couler si beau navire, quand même... »

Gaston se rembrunit et haussa le ton :

« Bougre de cul de lapin ! T'es là pour la double solde, nan ? Alors tu fermes ta grand' goule ! »

Il attrapa une des larges cognées assemblées en un tas à leurs côtés et la balança d'un geste énervé aux pieds de son interlocuteur.

« Tiens, affile plutôt ça, on en aura besoin. »

Puis il tourna la tête vers chacun d'eux, l'air mauvais, mâchonnant ses lèvres minces, avant de lancer :

« Si jamais y vient à l'idée d'un de vous de parler une fois sur le *Leo*, il a intérêt à savoir nager, j'vous le promets ! »

Un des hommes, grisonnant, la barbe drue et le nez couperosé cracha dans le sable avant de déclarer, l'air inquiet.

« Moi ce qui m'plaît pas, c'est qu'le vent tourne et qu'y va pas faire bon à prendre la mer si on tarde trop.

--- Tu veux qu'on mette à l'eau maintenant, Gobin ?

--- On serait pas plus mal. De toute façon, le *Leo* va pas tarder. Il a dépassé Césaire hier. Et pis, vu les courants du golfe, au plus tôt on le coince, au mieux on sera. Y'a quelques hauts-fonds qui iront bien mais s'agit pas de les louper...

--- On doit pas le couler ? intervint une voix hachée.

--- Certes pas ! éructa Gaston. On doit l'échouer, mais si y va par le fond, son contenu sera perdu. Et nous on aura droit aux verges au lieu de la cliquaille. »

Il se tourna vers Gobin, plissant les yeux après avoir regardé le ciel, en direction de la cité :

« On attend jusqu'à ce que les frères de Saint-Jean sonnent sixte, à la mitan du jour. »

Chacun médita sur ces paroles en silence pendant un temps.

Une légère brise soulevait le sable sec, le soleil était encore chaud en cette saison. L'outre se vida peu à peu sans bruit. Quelques-uns sortirent leurs dés tandis que deux autres chantaient une chanson à boire, accompagnés de la guimbarde d'un troisième. Gaston s'installa les mains derrière la tête, les yeux mi-clos, jouant avec un morceau de bois entre ses chicots. Ce furent les cris d'un gamin qui le réveillèrent en sursaut. Le navire tant espéré était annoncé, ils avaient juste le temps de le rejoindre.

Sans attendre, le canot fut mis à l'eau et le mât dressé. La petite voile latine permettait de naviguer avec souplesse et rapidité. En peu de temps ils dépassaient la pointe où se bâtissait le quartier du Temple, en perpétuel chantier. Gobin manœuvrait avec talent le frêle esquif, louvoyant sans donner l'air d'y réfléchir. À bord, les visages s'étaient fermés, les mains se crispaient. Gaston examina les trois arbalètes qu'ils emportaient. L'humidité ne leur convenait pas trop, leurs arcs collés, en tendon et corne, avaient tendance à se fragiliser. Mais cela avait l'avantage de calmer tout de suite les ardeurs combatives de beaucoup. Un carreau dans le ventre signifiait la plupart du temps une morte lente et douloureuse. Puis, comme ses camarades, il vérifia son gambison matelassé, assujettit sa ceinture.

Jusque là, pas un mot ne fut échangé à voix haute tandis qu'ils avançaient vers leur proie. La voile ondoyant, les cordages grinçant sous la main experte du marin, la vague fendue en embruns par la proue couvraient les chuchotements. Gobin décrivait un large cercle pour aborder par bâbord. La nef ventrue était bien visible, et on pouvait même discerner les gens à bord. La silhouette d'un lion jaune se dessinait sur le château avant, griffes et crocs brandis en avertissement.

D'un sifflement, Gaston invita les hommes à se rapprocher de lui. Tous tenaient leurs armes désormais, essentiellement des haches.

« Bon, je le répète : on leur dit rien. On monte à bord, on les rassemble et pis on échoue la nave. Compris ? »

Il n'obtint en réponse que des acquiescements graves, un silence empreint de crainte. Mener un combat était toujours hasardeux, et le faire en mer encore plus dangereux. La plupart ne savaient pas nager, et le poids de leur équipement pouvait les entraîner au fond bien vite. Il distribua les grappins à ceux qu'il pensait le moins timorés.

« On fait ça vite, et bien. On s'accroche, on monte et on fait ce qu'y a à faire.

--- Et si des soldats nous attendent ?

--- Je vous l'ai dit : y z'ont dû avoir le mot à bord. Faut juste avoir doutance des passagers. Il y a peut-être quelques Turcs qui compaignent les femmes et les enfants, mais on est trop nombreux pour qu'ils puissent faire quelque chose. »

Un cri venu du *Leo* attira leur attention, les visages curieux se tournèrent à l'unisson. Derrière eux, Gobin lâcha d'une voix grave :

« Tenez-vous prêts, on va toucher leur coque ! »

### Plages au sud de Saint Jean d'Acre, après-midi du jeudi 14 octobre 1154

Abrité du vent par le ressaut herbacé d'une dune, le chevalier écoutait tranquillement tout en déambulant. Attentif, il gardait les yeux au sol la plupart du temps, ne les relevant que furtivement, par intermittence. Son interlocuteur parlait avec un fort accent égyptien, mais de façon assez fluide. Son débit était néanmoins saccadé, en grande partie en raison de l'émotion certainement, ainsi que de l'effort qu'il avait fait pour venir à la nage depuis l'épave échouée parmi les vagues.

Ses vêtements étaient encore humides de la traversée et nul n'avait fait le moindre geste pour lui proposer une couverture ou de quoi se changer. Ils avançaient pourtant au sein d'un groupe d'hommes affairés. Une dizaine de chevaliers et nombre de serviteurs semblaient bien occupés entre la plage, l'épave et la dune. C'était en ce dernier endroit, sous un dais rapidement tendu que se tenait la silhouette royale de Baudoin III. C'était justement de lui qu'il était question dans les récriminations du naufragé.

« Tu as vu l'*aman* scellé de ton sire, nous ne sommes pas ennemis.

--- J'en suis d'accord et lui aussi. Tu n'es pas traité en tant que tel d'ailleurs, ni aucun de ton hostel.

--- Mes maîtres sont inquiets, car ils n'ont vu venir à eux que des pillards, et nulle aide ne leur a été offerte. D'aucuns attendent encore dans la carcasse, inquiets de leur sort. »

Le chevalier hocha la tête avec circonspection.

« Voilà remarque de fidèle serviteur, j'en conviens. Il va me falloir t'expliquer certaines choses... »

Le chevalier inspira longuement, retenant à grand-peine un sourire narquois. Il étendit le bras autour d'eux, de façon grandiloquente.

« Sais-tu où nous sommes ? »

L'Égyptien écarquilla les yeux, décontenancé par la question. Il balbutia quelques mots incompréhensibles avant de répondre, hésitant :

« Sur une plage non loin d'Akko[^34] ?

--- De certes, en bord de mer. Mais en des terres du roi de Jérusalem, tu en conviens ?

--- Oui, concéda l'autre du bout des lèvres.

--- Fort bien, donc tu dois connaître le droit d'épave alors ? »

Le silence qui accueillit sa question le fit sourire, avec un manque de naturel évident.

« Non ? Voilà bien grande surprise ! Laisse-moi donc t'en conter la teneur en ce cas. »

Il se gratta le menton un petit moment, levant les yeux au ciel. Entretemps, ils étaient parvenus au sommet de la dune et voyaient l'épave échouée du *Leo*, autour de laquelle plusieurs canots étaient emplis de sacs, de ballots et de paniers.

« Il s'agit d'une coutume fort ancienne en ces pays comme en d'autres, qui font que le sire d'un lieu peut se servir à volonté au parmi des épaves échouées en ses terres depuis la mer. Le sire Baudoin ne fait donc là que suivre bien ancienne loi de vos pays. »

Le jeune homme fronça les sourcils, interloqué. Il entrevoyait où la discussion le menait mais semblait encore refuser d'y croire. La situation était par trop artificielle pour qu'il l'accepte.

« Mais... la nef n'a pas échoué. Nous avons été attaqués et le *Leo* a été volontairement sabordé par...

--- Par qui ? Vois-tu ces hommes autour de nous ? Le sire Baudoin saurait les punir comme pillards qu'ils sont. Il vous protégera, toi et l'hostel du sire Usama comme il l'a promis à Norredin, je m'en porte garant ! Tu es en sécurité désormais.

--- J'ai pourtant l'impression que... »

L'Égyptien hésitait, impressionné par le déploiement des forces en présence. On lui donnait toutes les garanties, apparemment, mais ce dont il était témoin, c'était un simple pillage, de ceux qui se déroulent après les batailles, lorsque le perdant est dépouillé par le vainqueur. Les sergents, les valets, ne mettaient pas les biens de l'émir Usama en sécurité, ils les rapportaient à leur maître Baudoin.

« Allez-vous nous aider à former une caravane ?

--- Et comment ! Nous vous fournirons même vesture et de quoi manger pour cheminer.

--- Grâce vous en soit rendue, mais nous saurons faire avec ce que nous avons emporté. »

Le chevalier s'arrêta brusquement et se pencha, parlant soudain d'une voix sourde.

« Le sire Baudoin y tient. Vous avez tout perdu dans votre naufrage, mais nous vous fournirons de quoi rejoindre votre maître. »

Le domestique ferma les yeux et hocha la tête en silence. Désormais il craignait pour les femmes, emmenées à l'écart par quelques sergents. Il imaginait le pire pour la famille de l'émir, et n'osait s'aventurer à présumer du destin des servantes. Le chevalier lui tapa avec allégresse sur l'épaule.

« Porte message de la sauveté de tous. Nul n'attentera à vos vies, le sire Baudoin de Jérusalem en est garant. Et nous vous fournirons escorte pour cheminer, une fois que nous aurons récupéré la cargaison de cette épave. »

Puis il planta le jeune serviteur là, au milieu de la plage.

Un canot venait de s'échouer, mené par trois hommes. Ils se faisaient aider pour tirer plus au sec l'embarcation lourdement chargée. Des ballots de toile cirée, certainement une partie de la bibliothèque du maître. Il possédait plusieurs milliers de tomes, et chaque ouvrage, que ce soit de poésie, de science, de législation ou de religion, faisait sa fierté.

Il détenait plusieurs exemplaires du Coran, dont certains avaient été copiés par des membres prestigieux de sa famille, les seigneurs de Shayzar, sur l'Oronte. Un de ses plus précieux venait d'Ifriqiya[^35], où des calligraphes experts avaient tracé les sourates sacrées dans le style coufique qu'il affectionnait tant. Yusuf ne put s'empêcher de frémir quand il vit un des lourds paquets tomber dans une gerbe écumeuse parmi les vagues. Comme son maître allait se maudire d'avoir cru en la parole des Ifranjs !

### Damas, fin d'après-midi de youm al arbia 3 rabi' al-thani 584[^36] {#damas-fin-daprès-midi-de-youm-al-arbia-3-rabi-al-thani-58436}

Abu al-'Abbas soupira d'aise lorsqu'il sortit du bain de la rue du Palmier. Il se sentait détendu, propre et plein d'entrain. Pour une fois, il n'était pas gêné par la cohue habituelle qui régnait aux abords de Bab as-Sagir, hochant la tête d'un air satisfait tout en admirant les allées et venues depuis l'entrée du hammam. Il portait une de ses plus belles jubba, en lin fin d'Égypte, réhaussée de bandes colorées, et un turban soigneusement monté ornait son chef. Ce soir il devait recevoir quelques amis chez lui, et avait même commandé des musiciens pour l'occasion.

Il n'était pas particulièrement amateur de festivités mais la perspective d'un moment à discuter de tout et de rien en bonne compagnie le rassérénait. Il avait perdu son unique fils plusieurs années auparavant et, depuis, sa demeure était toujours un peu triste malgré les cris de ses petites filles. Son épouse Nusayba était encore jeune, et il priait Allah régulièrement dans l'espoir qu'il bénisse leur union d'un nouveau garçon.

Il finit par s'aventurer dans la foule, en direction de l'est. Habituellement il ne venait pas aux bains si loin de chez lui, vers darb ad-Daylam, mais il avait décidé de passer voir un négociant en livres. Cela faisait des années qu'il enrichissait peu à peu sa collection d'ouvrages. Il privilégiait les œuvres littéraires, poésie et contes essentiellement. Et, bien sûr, des exemplaires du Coran. Il avait eu l'occasion d'en acquérir un fragment lors d'un voyage lointain, quand il n'était qu'un jeune commissionnaire, à Kairouan. Depuis, il tentait de compléter ses volumes pour avoir en sa demeure l'intégralité des sourates du livre sacré. Mais c'était là un luxe rare, étant donné le prix exorbitant de ces ouvrages prestigieux.

La boutique ouvrait directement sur la rue par un couloir peu large encombré d'étagères où s'amassaient les recueils les plus modestes, qu'on pouvait s'offrir pour moins d'un dinar. De la poésie parfois, mais surtout des manuels scientifiques, utilisés par les savants et les hommes cultivés : astronomie, mathématique, médecine. Al-'Abbas en avait quelques-uns chez lui, comme les *Axiomes* d'Euclide ou l'*Introduction à l'astrologie* de Abu Ma'shar, achetés dans un lot pour une poignée de pièces. Le marchand, un jeune qui avait hérité l'affaire de son oncle, connaissait bien ses clients et allait de temps à autre chercher des ouvrages fort loin. Il s'avança en s'inclinant, les mains jointes en signe de respect, saluant avec emphase son visiteur.

« Vous arrivez fort bien, j'allais prendre une collation. Faites-moi le plaisir de m'accompagner. »

N'escomptant aucune réponse, il lança quelques ordres d'une voix autoritaire en direction de la cour à l'arrière de la petite pièce sombre. Puis il invita d'un geste son visiteur à prendre place sur un des tapis disposés sous un claustra par où s'insinuait la chiche lumière. Ils ne parlèrent que de banalités en attendant qu'on leur serve les plats : abricots cuits aux pistaches grillées, prunes séchées, le tout accompagné de lait miellé à l'eau de rose. Le boutiquier faisait des efforts visibles pour mettre l'éventuel acheteur en confiance.

Al-'Abbas était lui-même négociant, spécialisé dans les vivres, à destination de l'armée essentiellement, et il savait reconnaître un bon professionnel. Il appréciait d'être traité avec les égards dus à un client d'importance. Une fois rassasiés, les deux se turent un instant jusqu'à ce que le marchand, dans un sourire, glissât :

« Peut-être voudriez-vous voir le volume que j'ai déniché ? J'ai espoir qu'il convienne bien à vos goûts. »

Al-'Abbas acquiesça, s'essuyant les mains avec soin tandis que son interlocuteur sortait un volume de bonne taille, plus large que haut, enveloppé dans un sac d'étoffe. Il le dévoila avec un art consommé, le posant entre eux sur un lutrin avant d'en révéler le contenu. C'était indéniablement un ouvrage de premier plan, réalisé sur parchemin. La page qu'admirait al-'Abbas comportait trois lignes magnifiquement tracées dans une encre noire, ponctuées de ronds bornant la fin des versets. Les voyelles brèves y étaient indiquées en rouge, et des diagonales élégantes marquaient les points diacritiques. Al-'Abbas ne résista pas à l'envie de lire avec respect le texte devant ses yeux.

« Et quiconque croit en Allah, Allah guide son cœur[^37]... »

Il demeura muet un petit moment, en admiration devant la page, fasciné. Le vendeur avala plusieurs gorgées de boisson en silence, souriant, confiant. Il attendait la question, qui mit un certain temps à venir.

« Quel serait le prix que vous demandez ?

--- Il s'agit là d'une œuvre d'exception, même si elle ne contient que quelques sourates. Il me semble que six dinars serait prix honnête. »

Al'-Abbas souffla. C'était une belle somme, plus que ce qu'il dépensait habituellement pour sa famille en un mois. Mais c'était indiscutablement un très bel ouvrage, peut-être destiné à l'origine à une mosquée.

« Cela me semble cher. J'ai déjà beaucoup de dépenses à faire en ce moment, avec les trousseaux de mes filles : plusieurs bijoux à réaliser, boucles d'oreilles et bagues.

--- Oui, je comprends, mais il s'agit là d'un travail sur parchemin, pas sur simple papier. Admirez le tracé, c'est une main élégante et talentueuse qui a fait ce travail. On ne voit pas beaucoup de ce genre d'ouvrage par ici, c'est un style de l'Ifriqiya.

--- Oui, j'avais reconnu. Il est vrai que c'est un bel ouvrage, mais aussi une belle somme !

--- Vous aviez quel montant à consacrer à un livre sacré ? »

Le marchand retint un sourire intérieur. La discussion était lancée, en espérant qu'elle aboutisse à un tarif juste. Il conservait quelques arguments de poids en réserve, pour maintenir le montant élevé. Mais quoi qu'il arrive, il ferait une bonne affaire. Il avait récupéré le livre dans un lot vendu une bouchée de pain par des Turkmènes de retour du pillage des terres Ifranjs. Ces barbares incultes ne savaient pas différencier un manuscrit rare d'un recueil de fables populaires. Quels arriérés que ces nomades qui n'accordaient aucun prix aux ouvrages de qualité, préférant l'évanescence de leurs récits contés. ❧

### Notes

Le titre est la deuxième partie de l'expression *Verba volant, scripta manent*, attribuée à Caius Titus lors d'un discours au sénat romain et qu'on traduit en français par « Les paroles s'envolent, les écrits restent. »

Je n'ai pas résisté à l'envie de lier un souvenir conté par Usama ibn Munqidh dans ses mémoires (le pillage de ses biens par le roi Baudoin III malgré le sauf-conduit octroyé à sa famille) avec une page de Coran présentée lors de l'exposition « Les trésors fatimides du Caire ». Il est toujours émouvant de supposer les différents commanditaires, possesseurs successifs de ces ouvrages multiséculaires, témoins palpables de notre passé commun.

Cet émir, Usamah ibn Munqidh, personnage de première importance dans le Moyen-Orient des croisades était un érudit, un poète reconnu, bien qu'il soit désormais plus connu pour ses anecdotes historiques. J'ai donc imaginé lui attribuer un livre parmi les milliers qu'il indique avoir détenus. Cette perte fut d'ailleurs celle qui lui fut la plus pénible et il en parle encore avec affliction plusieurs dizaines d'années après.

### Références

Ashtor Eliyahu, *Histoire des prix et des salaires dans l'orient médiéval*, Paris : École Pratique des Hautes Études, 1969.

Cobb Paul M., *Usama ibn Munqidh. The book of Contemplation. Islam and the Crusades*, Londres : Penguin Books, 2008.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem. A corpus. Vol. IV The Cities of Acre and Tyre with Addenda and Corrigenda to Volumes I-III*, Cambridge : Cambridge University Press, 2008.

Collectif, *Trésors fatimides du Caire*, Catalogue de l'exposition présentée à l'Institut du monde arabe du 28 avril au 30 août 1998, Paris : Institut du monde arabe, Gand : Snoeck-Ducaju & Zoon : 1998.

Mal chancre
-----------

### Césarée, midi du mercredi 3 août 1149

Le ressac couvrait la rumeur déversée depuis les rues agitées. La plage, encombrée de barques et de filets, s'étendait jusqu'aux pieds de la muraille couleur de miel. S'avançant dans la mer comme une épée dans les chairs, le château des seigneurs comtes de Césarée s'affranchissait de l'enceinte de la cité et défiait les vagues qui venaient lécher ses fondations. Par-delà les dunes de sable couvertes d'arroche, la palmeraie se déployait à perte de vue, abritant jardins et vergers, cabanons et potagers. Installés dans un creux du relief, deux jeunes gens flemmardaient, alanguis, écrasés par la chaleur d'un soleil radieux, dont l'ardeur était à peine entamée par la brise marine.

« Le jeune sire Eustache a fait serment de me prendre en sa mesnie comme valet d'armes, Anceline, tu imagines ? »

La jeune fille, petite silhouette élancée aux longs cheveux bruns emmêlés, plissa ses yeux clairs, de plaisir.

« N'a-t-il pas déjà bel hostel ? Es-tu bien certain qu'il ne se dédira pas, Fouques ?

--- Certes non, le vieux sire comte Gauthier l'a bien confirmé à mon père. Il pense équiper son fils droitement, qu'il lui fasse honneur en combat. Pour cela, il lui faut fidèles soudards. »

Le garçon étira les bras, soupirant d'aise tout en imaginant l'avenir radieux qui s'offrait à lui.

« Nous aurons notre propre demeure, avec une citerne et un jardin !

--- Ne devras-tu pas suivre ton maître partout ? Dormir en son hostel ?

--- Pas chaque jour, et j'aurais bien assez de besants pour avoir mon feu. Nos enfants y naîtront, mes fils, qui me succéderont. Peut-être que l'un d'eux finira clerc ou chevalier...

--- Un clerc ? Que vas-tu inventer là ? »

Le jeune homme éclata de rire.

« Bein quoi, Anceline ? Voilà vie fort tranquille ! »

Il fredonna, le sourire aux lèvres:

> « D'un tel fils oiseux et rétif\
> Du moindre labeur si craintif\
> Las, le père ne sut que faire\
> Fils à la tonsure te confère\
> En village tu seras curé\
> Le fils ne put rien opposer. »

La jeune fille termina avec lui, de sa voix vibrante et chaleureuse, le visage rayonnant. Au fonds de son âme, elle demeurait pourtant inquiète, sachant que la famille de son bien-aimé ne l'appréciait guère. Elle était enfant de la rue, perdue après la mort de ses parents, pèlerins sans fortune échoués sur les rivages du royaume. Mendiant, chantant sur les places, elle vendait ses bras comme lavandière. Certains disaient qu'elle louait plus, à l'occasion, quand le froid se faisait trop pressant ou que son ventre sonnait creux. Elle n'avait pas vingt ans, mais son regard était déjà fatigué, éteint.

Jusqu'à ce que ses yeux croisent le beau Fouques, jeune homme plein d'ambition et de courage qui était tombé éperdument amoureux d'elle. Ils avaient passé le printemps et l'été à batifoler dans les dunes, s'enlaçant à la moindre occasion, se volant des baisers à l'ombre de la cathédrale au jour des grandes cérémonies.

« Ton père ne dira rien de nous voir ?

--- Il ferait beau le voir me critiquer sous mon toit. Si je gagne ma soudée, je n'aurai aucun compte à lui rendre. »

La jeune femme acquiesça, inquiète.

« Et une fois que tu lui auras donné des petits-fils, il comprendra, ajouta Fouques, le sourire aux lèvres.

--- Comment nous marier, alors que je n'ai nulle dot, rien à t'apporter...

--- Nous ne serons pas les premiers à qui cela arrive. En travaillant dur, on peut fort bien réussir en ces lieux. »

Il releva le buste, dévisageant la jeune fille, la couvrant d'un regard tendre.

« Ne t'enfrissonne pas ainsi, tout ira bien. Ce n'est affaire que de quelques semaines, de mois tout au plus. À la Noël, nous serons chez nous. »

Il caressait doucement le bel ovale du visage, traçant des chemins de tendresse du bout des doigts. Plusieurs appels de cor résonnèrent au loin, lui faisant tourner la tête vers la ville.

« Des voyageurs arrivent au castel du sire comte. Peut-être aurons-nous plaisir de voir quelques jongleurs de talent ! »

Il se leva avec vigueur, tendant la main pour aider sa compagne.

« Ne tardons pas. Si les visiteurs sont d'importance, il y aura banquet ce soir et nous pourrons profiter des reliefs à la veillée, et goûter les acrobaties. »

Main dans la main, les deux jeunes gens s'élancèrent vers la porte de Jaffa, traversant les bosquets d'aloès bordant le chemin d'accès. Le soldat de faction ne tourna même pas la tête, occupé qu'il était à fouiller dans les sacs et les besaces de deux Bédouins accompagnés d'un chameau croulant sous les paquets. Arrivés dans les rues ombragées, ils se lâchèrent la main, mais continuèrent à progresser avec enthousiasme, indifférents aux boutiques et aux étals.

Peu de badauds s'attardaient en cette période de la journée et la plupart des échoppes d'artisans ne laissaient échapper aucun son, à l'heure où la chaleur incitait à la sieste. Ils rirent en égayant quelques volailles outragées, avant de déboucher sur la placette longeant le port, devant les tours qui donnaient accès au château. Une impressionnante caravane attendait que les valets aient ouvert les lourds vantaux pour pouvoir avancer. Seuls demeuraient les domestiques et les serviteurs, les personnages d'importance ayant déjà franchi les portails. Fouques apostropha un petit rouquin armé d'un épais bâton ferré, le saluant avec enthousiasme avec de le questionner sur son maître.

« C'est le sire comte de Jaffa, fils le roi Foulques.

--- Il s'en vient lever l'ost ?

--- Pas que je sache, non. »

Fouques allait le presser davantage de questions, mais son interlocuteur s'éloigna pour réprimander une bête rétive, tout en admonestant un jeune valet chargé de s'en occuper.

« S'il n'y a pas la guerre, nous aurons sûrement des réjouissances, viens-t'en donc, ma douce, faufilons-nous outre la porte en la cour du sire vicomte. Profitons de cette soirée de fête. »

Puis, après avoir déposé un baiser sur la joue d'Anceline, il l'entraîna parmi les domestiques.

### Césarée, après-midi du dimanche 7 août 1149

« Nul doute sur le mal qui frappe le jeune sire Eustache, Anceline. »

La voix triste de Fouques n'était qu'un souffle, à peine discernable dans le bruissement des branches de palmiers au-dessus de leurs têtes. Il tenait Anceline dans ses bras, par l'épaule, assis sur un ressaut du terrain parmi les bosquets environnant la porte orientale. La jeune fille gardait les yeux mi-clos, fixés sur le sol, abasourdie. Elle ne répondit que tardivement.

« Il ne tiendra pas le comté de son père, alors ?

--- Non, cela a été clairement expliqué en l'assemblée de la cort. Il est prévu qu'il rejoigne les frères de Lazare.

--- Il se retire du monde. »

L'angoisse lui fit monter les larmes aux yeux et elle continua, d'une voix brisée :

« Cela veut dire que tu vas devoir œuvrer avec ton père et que nous ne pourrons célébrer nos espousailles ?

--- Certes non. C'est juste que... »

Le jeune homme était ennuyé, il se mordit la lèvre à plusieurs reprises, cherchant à rassembler ses pensées.

« Le sire comte de Jaffa est frère le roi comme tu le sais, et il y aurait peut-être besoin de sergents en la sainte cité. Sergent le roi, moi, tu imagines ? »

Anceline écarquilla les yeux de frayeur, se tournant vers lui.

« Tu vas partir, tu me quittes ? C'est cela que tu voulais me conter ?

--- Non, je veux que... J'ai désir que tu me compaignes en la sainte cité. Nous pourrons y être heureux aussi bien qu'ici.

--- Partir avec toi ? Mais je n'ai rien pour le voyage et je ne connais personne là-bas, comment je ferai ?

--- Tu m'auras, moi. Et mon père m'a fait promesses de quelques besants pour m'installer. »

Le jeune femme hocha la tête, mais son visage demeurait fermé. Finalement, elle se frotta les yeux, les joues, de ses paumes avant de longuement soupirer.

« Je ne sais pas, Fouques, ça me parait fort périlleux.

--- Crois en moi, Anceline. Père est convaincu que je trouverai bonne place là-bas, sinon il ne dessererrait pas les cordons de sa bourse aussi aisément, tu le sais bien. »

Il esquissa un sourire amusé, auquel la jeune femme répondit, sans conviction.

« Le départ du frère le roi est prévu pour dans trois jours, tu n'auras qu'à venir à la caravane de départ, au matin, et nous cheminerons ensemble. Prends toutes tes affaires, car nous ne viendrons pas ici de si tôt, je crains. »

Se rapprochant de Fouques, elle lui prit le bras et plongea son regard dans le sien.

« Je n'ai jamais quitté Césaire depuis mes enfances, Fouques. Je n'ai rien ici, pas de feu ni de famille, mais j'y ai des amis, mes habitudes. Je n'ai ja osé cheminer ailleurs. C'est bien trop dangereux pour femme seule.

--- Tu n'as donc pas désir de me suivre ? Nous aurons la vie dont nous rêvions, mais en une autre cité. Et sergent le roi, c'est excellent travail. D'être mon épousée fera de toi une femme honorable, plus personne n'osera se moquer. J'aurais l'épée à la hanche et chacun nous respectera. »

Anceline sourit timidement, attendrie par la douceur de son compagnon. Elle vint cueillir ses lèvres d'un délicat baiser.

« Je serai ta femme, Fouques, je n'ai nul désir d'autre chose.

--- Tu m'attendras à la porte de la cité ce mercredi ?

--- Je serai là, avec ma besace et mes maigres avoirs. Toute à toi, mon bel ami. »

Vibrant de tout son être, elle scella sa promesse d'une étreinte passionnée.

### Césarée, matin du lundi 8 août 1149

Fouques sursauta quand il sentit une main lui secouer l'épaule. Il voyait l'ombre de son père, penché sur sa paillasse, venu le réveiller. Ses frères et sœurs, amassés comme lui dans un coin de la réserve, dormaient encore à point fermé. Le jour pénétrait pourtant par la porte ouverte de la grande pièce à vivre.

« Il te faut te lever, mon garçon, tu as force ouvrage à faire ! »

Fouques s'étira et repoussa sa couverture. Il attrapa rapidement sa cotte et ses chausses, puis glissa ses pieds dans ses savates. La poussière volait dans les rais de lumière du matin jeune, par les ouvertures donnant sur la cour. Il cligna des yeux en y débouchant, se protégeant de la main. Arrivé devant la citerne, il y lança le seau qu'il remonta en bâillant, avant de s'asperger le visage et les bras. La fraîcheur de l'eau le réveilla tout à fait. Ce fut en finissant d'enfiler sa cotte qu'il rejoignit son père et sa mère à table. Du pain et du lait, ainsi que des fruits l'attendaient. Il sourit, ragaillardi à la perspective du repas.

« Vous avez nécessité de mon aide ce jour d'hui, père ? Quelque belle vente en perspective ? »

Il se figea en voyant ses parents assis côte à côte, l'air sérieux et renfrogné.

« Tu n'as pas à t'en inquiéter, fils. Ta mère a préparé tes habits et quelques douceurs comme en concèdent les femmes aimantes. »

Il marqua une pause, indiquant du doigt un sac de toile grise posé sur un des bancs. Puis il sortit de ses replis de vêtement une petite bourse.

« Je ne suis qu'un modeste blatier[^38], et je n'ai donc que peu à t'offrir. Alors voilà tout ce que nous pourrons jamais te donner pour t'installer. »

Il posa la modeste escarcelle, peu ventrue, sur la table.

« Je sais que tu es vaillant à la tâche, et j'ai donc bon espoir que tu sauras t'en débrouiller. »

Fouques n'était pas certain de comprendre et son regard allait d'un de ses parents à l'autre, hésitant entre espoir et angoisse.

« Vous avez donc eu réponse du sire comte pour moi, père ?

--- Si fait, je ne te l'ai pas dit hier, mais tu pars avec le frère le roi ce jour d'hui. Tu prêteras serment au roi en arrivant à la cité et le servira comme sergent. Te savoir fils d'un ancien mesureur de blé[^39] a fait bonne impression. »

Le jeune homme sentit son cœur s'arrêter, toute vie abandonna son visage.

« Ce jour ? Mais je croyais que c'était...

--- Il semble que le sire comte de Jaffa fasse selon ses envies, et on m'a dit de te mener à la première heure au castel. Il fait route tantôt. »

Le vieil homme se leva pesamment et se dirigea vers la porte, s'emparant du bâton qu'il ne quittait jamais. Assujettissant un bonnet de toile sur son crâne dégarni, il lança, d'un air sec :

« Salue ta mère promptement et rejoins-moi dans la cour, il ne s'agit pas d'être en retard. »

Fouques se frotta le visage puis se lança auprès de sa mère, s'agenouillant.

« Mère, saviez-vous que je devais partir ce jour ? »

Le visage las échappa un soupir consterné.

« Tu sais combien ton père n'aime pas la désobéissance, Fouques.

--- Mère, je vous en conjure ! Vous savez que je ne peux partir ainsi, dès potron-minet ! Je dois prévenir... »

Sa voix mourut tandis que sa mère l'attirait à lui. Elle l'enlaça avec douceur un court instant et lui chuchota :

« Tu es désormais un homme, fils, ne laisse plus quiconque voir tes larmes. Que Dieu te garde, mon enfant, je prierai pour toi. »

L'éloignant d'elle lentement mais avec fermeté, elle traça un signe de croix sur le front de son fils, essuya ses larmes d'une main décidée et concéda un baiser.

« Brûle une chandelle de bonne cire pour tes vieux parents auprès du tombeau du Christ quand tu en auras le loisir. »

La silhouette du père se traçait dans l'embrasure de la porte, profil courbé mais déterminé, impassible.

« Femme, laisse notre aîné partir comme un homme, foin des jérémiades ! »

### Césarée, midi du lundi 8 août 1149

Anceline, les bras encombrés d'une énorme corbeille de linge, avançait en trébuchant dans la ruelle depuis le lavoir jusqu'aux bâtiments des frères de Saint-Jean. Ils comptaient parmi les clients de lavandières les plus fréquents, ayant besoin de changer souvent les draps de leurs hôtes, sans négliger toute la vêture qui passait entre leurs mains. Elle déposa avec plaisir le lourd paquet devant la porterie. Les femmes n'étaient pas les bienvenues dans l'enclos et des valets venaient donc généralement prendre les affaires sous un auvent protégeant des intempéries. Le frère cellérier ou l'hospitalier se chargeait habituellement de noter, comptabiliser, en s'installant parfois sur une petite table lorsque l'ouvrage était conséquent.

Anceline escomptait voir le cellérier, car elle voulait demander son compte. Elle était réglée chaque semaine, selon son travail. Mais elle préférait récupérer au plus tôt l'argent qu'on lui devait ici et là, de façon à pouvoir partir avec toute sa fortune. Le vieux moine espéré sortit, dandinant sa silhouette rebondie.

Anceline aimait bien ce frère à l'esprit fin, amateur de bons mots. Même s'il laissait parfois courir ses mains en des endroits qui lui étaient défendus. On lui prêtait une liste de maîtresses longue comme le bras mais jamais il n'avait fait d'avances à Anceline, bien qu'il ait été manifeste qu'il la trouvait à son goût. Le sourire ravi qu'il arborait là encore en était une nouvelle preuve évidente.

« Alors, belle Anceline, tu as désir de toucher ton compte ? Quelques soucis impérieux ? Nous pouvons t'aider si tu en as le désir.

--- Mille grâces, frère Aymon, c'est juste que je vais quitter Césaire. »

Le visage avenant se fit soudain contrarié.

« Que voilà cruelle nouvelle ! Tu ne te plais plus en notre cité ? »

Le visage rayonnant, Anceline lança, pleine d'entrain :

« Je vais me marier, mon frère, et mon futur époux part tantôt pour Jérusalem, alors je vais le suivre.

--- Jambe-Dieu ! Tant de cœurs vont se briser ici à cette nouvelle ! »

Il sourit, s'avançant pour demander d'une voie pateline :

« Qui est l'heureux élu ?

--- Fouques, un fils de blatier qui part pour rejoindre la sergenterie du roi avec le sire comte de Jaffa. Nous faisons route ce mercredi.

--- Mercredi ? Tu dois faire erreur, ma belle. Le sire comte a corné son départ dès la première heure. Il doit déjà apercevoir la tour de Caco en ce moment. »

Anceline sentit ses jambes flageoler, son regard se brouilla. Frère Aymon s'en émut et tendit une main compatissante, inquiet.

« Que t'arrive-t-il ? »

Trop bouleversée pour répondre, Anceline abandonna le frère et détala à toutes jambes. Elle passait comme une furie dans les rues, les robes relevées pour courir plus à son aise, indifférente aux regards curieux, courroucés ou désapprobateurs. Elle filait pour sa vie, n'avait qu'une idée en tête. Jamais elle n'avait osé s'approcher trop de la boutique du père de Fouques, car elle le savait hostile à son égard. Mais cette fois, elle ne reculerait pas.

Elle s'arrêta brusquement devant la double porte par laquelle s'exhalaient les senteurs poussiéreuses du grain. Le vieil homme était là, assis sur un escabel, gardant l'entrée comme un cerbère tapi dans l'ombre. Il la toisa lentement puis agita la main comme pour l'effrayer :

« Va t-en, maudite ! Mon fils n'est plus là ! Tu ne l'auras jamais ! »

Elle hésita un instant, fit mine d'avancer, pour le défier. Elle demeurait indécise. Elle aperçut le bâton qui ne le quittait jamais, et fantasma sur ce qu'elle pouvait en faire. Elle se délectait de voir le détestable crâne ridé se fendre comme un fruit pourri, d'imaginer la silhouette fracassée sur le sol. Mais elle se contenta de renifler et tourna talon. Quand elle disparut à l'angle de la rue, le vieux sortit la tête de l'ombre, comme une gargouille curieuse, puis cracha avec morgue. ❧

### Notes

La lèpre posait un double problème aux sociétés médiévales. Tout d'abord, bien évidemment, il y avait l'aspect sanitaire. Certaines formes étaient contagieuses et il fallait résoudre la question du soin à prodiguer aux malades. Mais il s'y ajoutait un autre souci : on estimait que le corps était le reflet de l'âme, et que donc le dépérissement de l'un était signe d'une affection de l'autre. C'est une des raisons qui poussait à l'ostracisme des lépreux, quand bien même ils appartenaient à la noblesse. L'ordre de saint Lazare fut créé pour leur permettre de conserver une utilité sociale tout en préservant les gens sains. Le cas de Baudoin IV, à la fin du siècle, questionnera grandement cette vision des malades souffrant d'affections graves - car tous les « lépreux » n'étaient pas véritablement atteints de cette maladie, mais pouvaient présenter des symptômes similaires.

Le mariage, au milieu du XIIe siècle, demeure avant tout un contrat civil en vue de former une unité économique viable. Bien que les sentiments n'en soient pas forcément exclus, il est essentiel pour les futurs époux de pouvoir s'installer dans de bonnes conditions. C'est pourquoi le rôle des familles est généralement important, car elles contribuent (via la dot, le trousseau, le douaire, etc.) à la constitution des premiers apports. Ne pas pouvoir se rattacher à un réseau familial ou l'autre est donc un réel souci, d'autant plus pour les femmes, qui sont souvent considérées comme des mineures et qui, donc, peinent à exister en dehors de tout mâle référent (père, mari, frère, cousin, oncle).

### Références

La Monte John L. , « The lords of Caesarea in the Period of the Crusades », dans *Speculum*, vol.22, n°2, avril 1947, p.145-16

Mitchell Piers D., « An evaluation of the leprosy of King Baldwin IV of Jerusalem in the context of the medieval world », dans Hamilton Bernard, *The leper king and his heirs. Baldwin IV and the crusader kingdom of Jerusalem*, Cambridge : Cambridge University Press, 2000, p. 245-258

Sheehan Michael M., *Marriage, Family, and Law in Medieval Europe: Collected Studies*, Toronto: University of Toronto Press, 1996.

Les trois loups
---------------

### Apchon, fin d'après-midi du mercredi 25 juillet 1156

La pierre des murets du chemin était mangée de mousse, de joubarbe, de capillaire et de lichen, et de hautes herbes frémissaient aux abords, dans l'attente d'être fauchées. Agrippé à un massif rocheux surplombant le plateau, un imposant château s'élançait à l'assaut du ciel. Forteresse de moellons sombres et de mortier solide, ses courtines déchiraient les nuages.

Dans son ombre, à ses pieds, un hameau se blottissait entre deux ressauts du terrain. Le village était animé, visiblement florissant, et de nombreux artisans tenaient leur étal sur la place jouxtant la robuste chapelle, autour d'un couderc. Après avoir laissé leurs montures près d'un abreuvoir, Ernaut et Lambert se dirigèrent vers une bâtisse de bonne taille, aux portes grand ouvertes.

Le maréchal-ferrant était présentement occupé à parer le sabot d'une vache avant d'y poser un fer. Un jeune valet nettoyait les abords, un balai de genêt à la main. Lambert salua, dans un dialecte mélangeant son bourguignon natal et le peu d'occitan des montagnes qu'il connaissait. Il n'était pas aussi à l'aise qu'Ernaut pour apprendre les langues. L'artisan répondit poliment, mais brièvement, attentif à son ouvrage. Il était gris de poil, avec une belle moustache et une barbe courte. De grands yeux bruns, francs et volontaires, un sourire amical aux dents irrégulières ornaient un visage lourd. Sa silhouette, empâtée, trahissait l'homme aisé, malgré des mains et des épaules de travailleur. Il remarqua immédiatement les croix cousues sur les vêtements.

« Nous avons usage d'accueillir des hôtes, je peux vous offrir de dormir en mon fenil si vous le souhaitez. Nous serons heureux de faire héberge à des marcheurs de Dieu pour la nuitée. J'ai nom Aubert.

--- Grâces vous en soient rendues, je suis Lambert et voilà mon frère Ernaut. Nous serions heureux d'apporter de quoi compaigner le pain en ce cas. Il nous reste fromage et biscuits.

--- Foin de cela, gardez vos provendes pour mauvais chemins. J'ai celliers garnis bien assez pour nourrir le pèlerin. »

Il détailla Ernaut de la tête aux pieds et ajouta, en souriant :

« Même s'il a taille d'un ours et semble capable d'avaler génisse et son veau pour le souper ! »

Il invita Ernaut et Lambert à le suivre dans la demeure jouxtant la grange. Une robuste bâtisse, solidement assemblée, avec un étage coiffé de chaume. Des volailles gambadaient aux alentours et un chien de belle taille dormait sur le seuil. Aubert poussa la porte et disparut dans l'obscurité. Des voix d'enfants l'accueillirent tandis que, d'un geste, il incitait ses hôtes à pénétrer. La vaste pièce était occupée par une table imposante, des coffres et un foyer dans un angle.

Assise sur un banc, une femme était affairée à découper des légumes et une tranche de lard. Des jouets épars sur le sol étaient manipulés par une demi-douzaine de gamins dont le plus vieux n'avait pas dix ans.

Aubert présenta sa femme Bénédite et, d'un geste, proposa de s'installer pour « ôter la poussière du chemin ». Puis il se lança dans d'interminables questions sur les voyageurs, d'où ils venaient, ce qu'ils faisaient. Il était aussi curieux que disert sur lui-même et, lorsque le valet rentra après avoir rangé l'atelier, il semblait à Ernaut qu'il connaissait Aubert depuis des années. Bénédite était également accueillante, quoique bien plus discrète, n'ayant guère le choix de toute façon vu la faconde de son époux. En outre, le jeune pèlerin avait sympathisé avec Gui, un des gamins, et il le ravissait avec ses réflexions joviales et ses anecdotes étranges. Le jour tirait à sa fin, les ombres grandissaient à l'extérieur lorsqu'Aubert remarqua que sa femme avait enfermé les poules à l'abri. Il alla chercher le pain et y découpa quelques généreuses tranches d'un canif bien affûté.

« C'est de mon grain, vous m'en conterez du bien. Fraîchement moulu de la moisson.

--- Vous avez eu bonne année ?

--- De certes, les silos sont emplis et le courtil donne bien. Je vois là bonne saison qui commence, si les orages ne viennent tout gâcher. »

Sa femme distribua une écuelle et une cuiller à chacun, et une généreuse portion de soupe rehaussée de lard y fut versée sur le pain émietté. Les plus jeunes eurent droit à un surplus de lait crémeux. Aubert apporta un pichet de vin avec fierté.

« Il vient d'un coteau de Galvaing, un mien voisin. Moi j'y entends rien à la vigne. »

Le breuvage parut bien léger aux deux frères habitués aux capiteuses liqueurs de Vézelay. Mais ils firent néanmoins bon accueil à une boisson si généreusement partagée. Ils n'étaient pas toujours hébergés avec autant d'égards, se contentant souvent d'une soupe diluée servie dans une grange malodorante ou un appentis venteux. Lorsqu'ils en eurent fini avec le repas, ils s'installèrent tous à la porte de la demeure. Les journées étaient encore assez longues pour profiter de la veillée sans devoir se serrer près du feu. Les enfants étaient couchés et les discussions voisines résonnaient autour du couderc. Aubert triait des pois tout en devisant, tandis que Bénédite reprisait une chemise, à la lueur d'une petite lampe à graisse. Ils en vinrent à parler du départ.

« Si vous partez pour Saint-Flour, le mieux est par le Limon, mais c'est là terre à saignes[^40] et barthes[^41]. Soyez fort prudents et suivez bien le passage indiqué par les quirous[^42].

--- C'est terre gaste[^43] ?

--- Un peu, par endroit, mais il s'y trouvent chabannes[^44] et hameaux donc ne craignez pas trop. Vous trouverez la terre de Diane[^45] en l'autre ribier[^46]. La voie est bien connue des charreyres, vous ne cheminerez pas seuls. »

Il se tut un moment puis, voyant que personne ne parlait, préféra ne pas laisser le silence s'installer.

« Je vous ai dit que, chemin faisant, vous pourriez aller faire visitance à la Font-Sainte ? Peu au-delà de Saint-Hyppolite ? On y prie belle statue de la sainte mère de Dieu que les comptours[^47] ont rapporté d'outremer justement... »

Ernaut s'étira lentement. La nuit serait courte après une telle veillée mais pour une fois qu'il n'avait pas à se contenter des vêpres suivies d'un repas silencieux au sein de religieux moribonds, il n'allait pas s'en plaindre. Il eut un sourire quand il s'aperçut que Lambert oscillait régulièrement sur son tabouret, les yeux papillotant de fatigue.

### Plateau du Limon, jeudi 26 juillet 1156

Le temps superbe s'était imposé au fil de la journée, et le passage du fond de vallée encore brumeux était désormais loin derrière. Ernaut et Lambert cheminaient tranquillement parmi les doux reliefs d'un vaste plateau qui rejoignait les paysages accidentés au sud, où pointaient de nombreux pics. Les cris des milans et le chant des oiseaux dans les broussailles avaient accompagné leur avancée. Ils croisaient régulièrement des paysans brandissant leurs grandes faux pour la fenaison. Plusieurs fois, des rémouleurs ambulants, colporteurs, charretiers, les avaient aiguillés sur les bifurcations à prendre, les sentes à éviter.

Ils arrivaient dans une zone où l'empreinte de l'homme se faisait moins sentir. Aucun muret n'était visible pour délimiter les parcelles, seules de rares croix ou des pierres crossées indiquaient les frontières. De nombreuses tourbières parsemaient les creux du relief. Les bois devenaient forêts, où nul ne pénétrait sans bonne raison. Il demeurait malgré tout de vastes étendues herbeuses, vers le nord, où des troupeaux paissaient sous la garde de vachers. Peu de moutons et de chèvres, mais pas mal de bovins, signe indiscutable de la richesse de ces lieux. L'Auvergne était bénie de Dieu, et sa paysannerie y prospérait malgré les rudes mois d'hiver qu'on lui prêtait.

« Nous ne saurons arriver à Saint-Flour avant la nuit, la route est trop fort escarpée, se lamenta Lambert.

--- En ce cas, préfères-tu pousser au plus loin ou nous contenter de Diane? ou de Murat ?

--- Les jours sont longs assez pour... »

Un sursaut de sa monture stoppa net Lambert dans sa phrase. Le pauvre cheval roulait des yeux alarmés en tirant sur ses rênes. Le cavalier avait bien de la peine à garder le contrôle, sans parler de rester en selle. Ernaut connut rapidement les mêmes difficultés, et les hennissements effrayés ajoutèrent à la panique générale.

Jusqu'au surgissement d'une forme noire, oblongue, vive et insaisissable. Un loup. Un énorme loup au poil hirsute, tout en crocs et griffes, la gorge crachant sa colère et sa haine des hommes. Il s'affola un moment parmi les jambes des chevaux puis bondit dans un taillis, aussi vite évanoui qu'il était apparu.

Sa disparition ne calma pas le cheval de Lambert, qui se mit à tourner en rond, entraîné par la main maladroite crispée à une des rênes. D'une ruade il se débarrassa de ce poids gênant et partit, en saut de puces alternant avec un galop désordonné, droit sur le chemin. Ernaut avait lâché ses guides et s'accrochait des deux mains à la selle, les jambes crochetées autour du ventre de sa pauvre monture telles les mâchoires d'une tenaille. Il rentra la tête dans les épaules quand il se retrouva parmi les branches d'un bosquet de noisetier. Mais il tint bon, et son cheval finit par se calmer.

Cherchant où était Lambert, il descendit rapidement, flattant l'encolure de la bête affolée tout en la menant par la bride avec autorité. Son frère était à terre, le visage contracté, et se massait la cheville droite. Ernaut se pencha vers lui.

« La peste de cette malebeste ! Ça va ?

--- Je ne sais, j'ai grand dol qui me gagne la jambe. Rien de cassé néanmoins. »

Ernaut hocha la tête, soulagé. Il s'agenouilla vers son frère et l'aida à ôter sa chausse. La cheville présentait une marque rouge violacé qui s'assombrissait à vue d'œil.

« Demeure ici le temps que je trouve ton cheval. Il n'a pas dû aller bien loin. »

Il aida Lambert à s'assoir sur un tronc couché là par un ancien orage et s'éloigna, non sans s'être muni de son bâton de marche. Si jamais le loup repointait son museau, il pourrait bien lui en coûter.

Gravissant un talus, il plissa les yeux, s'efforçant d'apercevoir le cheval. Il ne voyait que la lande, les hautes graminées ondoyant sous le vent, les bosquets qui, peu à peu, se rejoignaient en une forêt cheminant sur les versants escarpés. À deux ou trois portées d'arc, il repéra un troupeau surveillé par plusieurs chiens et vachers, et se dit qu'ils avaient peut-être remarqué leur monture. Il prit donc dans cette direction, droit au nord, fouettant l'herbe de son bâton tout en avançant.

« Désolé l'ami, nul cheval par ici, il a dû s'égarer vers Diane ou au parmi des bosquets au sud.

--- C'te maudit leu mène la danse depuis de nombreuses saisons. Un gros, dru de poil, noir comme charbon. De la semence de démon ! Il doit avoir sa tanière dans la forêt, mais nul ne l'a trouvée. »

Un des pâtres proposa de venir chercher Lambert et le cheval restant, pour qu'ils soient en sécurité avec eux. Et si Ernaut tardait trop, ils le mèneraient à leur hameau. Ils revinrent donc à plusieurs vers le chemin et expliquèrent la situation. Lambert n'était guère enthousiaste à l'idée de laisser son frère partir seul.

« Tu n'es pas louvier, Ernaut, et les terrains sont traîtres ici, Aubert nous l'a bien dit.

--- Nous n'avons guère le choix, frère. Nous ne pouvons cheminer avec un seul cheval. Et la remonte nous coûterait bien trop cher.

--- Tu parles de raison. Mais prends garde à toi.

--- Ne t'enfrissonne pas, je ne serai pas long. Cette maudite carne a dû galoper un temps, jusqu'à ce qu'elle comprenne qu'elle était à l'abri. »

### Plateau du Limon, après-midi du jeudi 26 juillet 1156

Ernaut sentait sa gorge sèche et regrettait à présent son optimisme. Il n'avait pas pris de gourde et les rus qu'il croisait ne l'inspiraient guère. Le terrain ondulait régulièrement, abritant en ses creux des tourbières qui gargouillaient sous ses pas. Il prenait garde à ne pas se faire piéger et avançait prudemment, appelant la monture de Lambert de temps à autre. La lande de bruyère se transformait parfois en mer de graminées dansant sous le vent léger, cachant les reliefs, dérobant au guetteur le tracé des sentes.

Lorsqu'il parvenait à un bosquet de pins, depuis l'ombre bienvenue, il portait son regard au loin, cherchant à deviner la silhouette de l'animal. À plusieurs occasions, il s'était donné de faux espoirs, trompé par quelques chevreuils dont la robe évoquait la couleur alezan du cheval. Seuls les chants des grillons et le craquètement des geais répondaient à ses appels, ou le cri d'un milan en chasse qui tournoyait loin au-dessus de sa tête.

Il longeait la vieille forêt, là où les troupeaux s'aventuraient peu. Parfois il rencontrait les restes de cabanes grossièrement assemblées, anciens refuges pour des charbonniers, des bûcherons ou des tourneurs. Sur les pentes d'un de ces hameaux improvisés, il découvrit des groseilles et des myrtilles, une poignée de framboises. Il dévora avec plaisir les fruits, assis face au paysage qu'il surplombait. Devant lui, loin au nord un mont s'élançait vers les cieux brumeux, noyé de bleu dans la chaleur de l'après-midi. Partout autour, ce n'étaient que trouées et vallons, ressauts et collines sur les plateaux, vallée profonde qui l'en séparait. L'homme n'était pas à sa place ici, et la nature le faisait sentir aux voyageurs égarés, aux pâtres aventureux. Parfois le vent portait à ses oreilles le tintement des sonnailles des troupeaux gardés au loin. Il lui arrivait de discerner les taches beiges et brunes, rousses et noires des bêtes à cornes, parmi les nuances de jade, de céladon et d'émeraude.

Cela faisait un bon moment désormais qu'il battait la campagne, sans résultat, et il commençait à perdre patience. La chaleur et le chant des insectes lui donnaient la suée. En dehors des bêtes aperçues au loin, il n'avait remarqué aucune trace et se demandait si le cheval n'avait pas fui en direction des habitations. Il serait bien temps de le découvrir plus tard. Pour l'instant, il voulait être certain que le loup ne l'avait pas attaqué ou qu'une fondrière ne l'avait pas englué.

Profitant de l'ombre d'un bosquet de pin, il ôta son chapeau de paille et passa sa manche sur son front moite. L'odeur de résine et l'humidité lui faisaient du bien. Il soupira, demeura un long moment ainsi, les yeux mi-clos à regarder sans voir devant lui. Il se sentait mal à l'aise dans ce lieu. Ce n'était pas que l'absence d'activité humaine, l'hostilité d'un monde sauvage peu désireux de se faire domestiquer. Il discernait une présence à l'orée de sa vision. Jamais assez présente pour se dévoiler mais suffisamment pour inquiéter.

On lui avait conté tellement d'histoires de loup qu'il ne doutait pas que ce fut la male bête qui les avait mis en si mauvaise posture. Certains récits en faisaient un animal stupide, infiniment moins rusé que le renard. Mais tout le monde savait que c'était le roi des espaces sauvages. Certainement moins fort que l'ours puissant, au cri rauque, et moins discret que le goupil au museau pointu. Pourtant il demeurait le prince de ces lieux, et ne se privait pas de le rappeler à l'homme qui se croyait invulnérable.

Ernaut commençait à se demander si ce vieux loup n'était pas un changeur de forme, un de ces sorciers qui adoptaient l'apparence de la bête pour tourmenter à plaisir. Il sourit à l'idée, peu désireux de s'épouvanter tout seul. Il regarda le soleil, autour duquel moutonnaient langoureusement quelques nuages cotonneux. Il était temps de se diriger vers les hameaux s'il ne voulait pas se faire prendre par les ténèbres. Aventureux, peut-être, mais certes pas au point d'errer nuitamment en des lieux appréciés des démons.

### La Bedice, soir du jeudi 26 juillet 1156

La chaleur de la journée se sentait malgré l'heure avancée. Installés devant leurs modestes cabanes, les manouvriers qui avaient invité Ernaut et Lambert discutaient dans leur dialecte occitan tout en se partageant une soupe épaisse de gruau et de légumes verts, dans laquelle ils puisaient à l'aide de lambeaux de pain gris. Ils cuisinaient dans un appentis grossièrement couvert de branchages, appuyé contre les cases mitoyennes où chacun resserrait ses affaires. La plupart étaient célibataires et d'autres avaient laissé leur femme le temps de la saison, dans une vallée en contrebas. Ils venaient aider aux champs là où le travail était plus important que les bras qualifiés pour le faire.

La cabane qu'ils partageaient avec Bénech, leur hôte, n'avait que la porte pour ouverture. Épaulée à droite et à gauche par une case semblable, en pierre sèche, elle était couverte de chaume pelé envahi de mousse grise. Sur le sol, des paillasses réalisées avec des feuilles de hêtre, les « plumes de bois » constituaient les seuls meubles. Une perche accueillait le rechange, et deux-trois sacs les modestes biens de l'habitant. Une lampe à graisse fournissait une maigre lumière, empruntée au feu extérieur.

Appuyés au mur chauffé par le soleil, les deux frères goûtaient un peu de calme. La cheville de Lambert n'était pas cassée, et du repos suffirait à remettre les choses en place. Un des villageois avait réalisé un emplâtre et, le pied désormais enrobé de feuilles de chou, Lambert ne sentait plus la douleur. Il profitait de la quiétude de la soirée.

Dans le hameau, tout était paisible, la longue journée de travail était terminée et parfois des voix s'échappaient des courtils ou du couderc. Des silhouettes dansaient autour des lampes tandis qu'on s'aventurait aux latrines ou qu'on rentrait après une veillée chez un voisin. Il n'y avait guère d'habitants ici, simple lieu-dit où des colons venaient dans l'espoir d'arracher un peu de bonne terre à la nature hostile, malgré le froid hivernal et les marécages omniprésents. Ernaut se demanda un instant si les conditions seraient aussi difficiles une fois l'outremer atteint. Il avait l'impression que ces gens étaient encore plus pauvres que la famille de Droin le Groin[^48].

Un des manouvriers les apostropha soudain :

« On se disait qu'il faudrait voir le Vieux Loubeyre, lui saura bien trouver vot' monture.

--- Il a bonne connoissance des lieux ?

--- De certes, et surtout des leus ! Il chasse pour les seigneurs de Diane, pour les comptours... »

Les visages hochèrent gravement la tête, comme s'ils hésitaient à en dire plus. Puis un des hommes, aux joues mangées de barbe grisonnante, ajouta :

« Il arrive qu'il vende une patte à clouer sur l'étable, pour conjurer le démon. Mais d'aucuns disent qu'il serait un peu sorcier.

--- Dame, renchérit un autre, à vivre comme les loups, il en est devenu un.

--- Ou a toujours été l'un d'eux ! C'est des bêtes féroces, sans pitié entre elles. »

L'ouvrier le plus proche d'Ernaut lui donna un coup de coude.

« C'est un vieux machin, aussi âgé que les pierres que tu vois par les terres gastes. Il a usé de sorts pour enlever une donzelle de Murat. »

Ernaut en écarquilla les yeux.

« Il en a fait quoi ?

--- Dame, à ton avis ? répliqua l'autre, la voix emplie de sous-entendus. C'était une vraie beauté en plus, une de ces filles dont les rois rêvent de voir ne serait-ce que la cheville.

--- Des menteries tout ça, Raoux. J'ai encontré un Muratais qui m'a dit que la famille était d'accord. Courir sus le leu, ça nourrit bien, et la donzelle avait maigre dot, voilà ce qu'on m'a dit.

--- M'ouais, pourtant frais minois fait dot assez ! »

Les hommes ricanèrent, émoustillés à la mention de jeunes beautés qu'ils ne voyaient que peu, isolés dans la montagne pendant de longs mois. Certains parmi eux couraient les filles quand ils rentraient chez eux, dans l'espoir de fonder une famille le jour où ils auraient assez épargné pour cela. Le voisin d'Ernaut se tourna vers lui et lui administra une bourrade amicale tout en se levant.

« Demain, je te montrerai comment trouver la cabane du Vieux Loubeyre. Si lui ne sait pas trouver ton canasson, personne le pourra ! »

### Plateau du Limon, fin de matinée du vendredi 27 juillet 1156

Le vent agitait les branches des bouleaux et des pins, faisait frémir les épais buissons de houx. Pourtant, le soleil ne semblait guère décidé à vaincre la couche de nuages laiteux. Quelques geais craquetaient en volant d'un arbre à l'autre, inquiets de voir un intrus dans leur domaine.

Ernaut avançait sur l'étroit sentier qui ondulait parmi les roches lisses, les hautes fleurs jaunes et les bosquets de petits genêts. L'endroit paraissait oublié de l'homme malgré le chemin bien tracé. Des pierres avaient été disposées dans les vallons humides, permettant de traverser sans risque les zones marécageuses. Il n'osait pas imaginer ce qui arriverait à un voyageur imprudent dans une telle région. Il était heureux que la route principale ait été jalonnée de quirous[^49] dressés de loin en loin.

Devant lui, l'orée de la forêt marquait de ténèbres la limite des espaces où l'homme tentait de s'imposer. Il pénétra sous le couvert frais sans ralentir, soucieux de ne pas montrer ses réticences. Le vrombissement des mouches emplit bientôt ses oreilles et il devait faire attention à fermer la bouche pour ne pas gober par inadvertance un de ces satanés insectes. Quelques chants d'oiseaux, le bruit de pics à l'œuvre s'aventuraient parfois entre les troncs. Son bourdon frappant le sol souple, le crissement de son pied sur les aiguilles et la mousse lui semblaient résonner jusqu'au plus loin des reliefs, alertant les présences maléfiques de son arrivée. Les senteurs résinées, généralement agréables, lui irritaient le nez et il trouvait un air lugubre à toutes ces branches dévorées de lichen. On aurait dit la cendre recouvrant des os sans âge.

Le fracas d'une course précipitée le fit sursauter. Il s'efforça au sourire en pensant que ce devait être une biche ou un cervidé quelconque, encore plus effrayé que lui. Posant son bâton contre l'épaule, il déboucha sa gourde et s'humecta les lèvres. Cette fois, il avait pris de quoi se restaurer. Il croqua un morceau de pain dur et replaça le tout dans sa besace. Lorsqu'il entendit une voix rauque résonner derrière lui.

« Bien dangereuses pour pérégrin que ces voies ! »

Il fit volte-face et chercha qui avait prononcé ces mots. Il ne vit que des formes indistinctes, branches et feuillages. Puis ce qu'il avait pris pour un amas en contrebas s'anima.

Un petit homme, recouvert d'une épaisse chape velue s'avançait. Il avait les cheveux mi-longs éparpillés autour d'un crâne en partie chauve et une barbe hirsute. Bien que son visage ait l'air usé et fatigué, sa démarche était pleine de vigueur et ses mains respiraient la puissance. Il arborait une robuste ceinture où était enfiché un couteau à lame grossière, et un bâton ferré de trois pieds de haut. Il ne fit presque aucun bruit lorsqu'il s'approcha. Il exhalait une odeur forte, animale, et pas la simple sueur du travailleur. Aucun relent d'oignon ou d'ail, si fréquent. Il semblait être parfaitement à sa place, empuanti des miasmes de la forêt, vêtu d'un gris semblable aux troncs. Malgré cela, il souriait, de tous ses chicots marron, les yeux plissés, rendus invisibles sous les sourcils touffus.

« Es-tu Loubeyre ? »

Le visage de l'autre devint carrément amusé.

« Ai-je donc telle fame que les forains me connaissent déjà ?

--- Je viens de la Bedice, on m'a dit que je te trouverai en ces lieux.

--- Tu me cherches donc ? »

Il inclina la tête, frémissant des narines comme une bête curieuse.

« Et pourquoi donc, l'ami ?

« J'ai nom Ernaut, et mon frère et moi, qui cheminons pour Jérusalem, avons fait mâle rencontre avec un gros leu, qui a fort effrayé un de nos chevaux. Je lui fais donc chasse.

--- Au leu ou à ta bête ?

--- À mon cheval. »

Le sourire de Loubeyre devint carnassier et il se pourlécha lentement.

« Tu es homme de raison. C'est un grand vieux loup, rusé et perfide, qui ne se prendra pas aisément.

--- Je n'ai que faire de cette male beste, retrouver le cheval me suffira.

--- On va le pister. Tu allais par la sente des Quiroux ?

--- Oui-da, nous avions franchi modeste gué peu avant. »

Le louvetier hocha la tête doucement et fit signe à Ernaut de le suivre. Il avançait d'un pas rapide mais léger, arrivant à se déplacer sans heurter la moindre branche, sans faire craquer la plus modeste brindille. Derrière lui, Ernaut se sentait comme un ours mal dégrossi. On aurait dit qu'un troupeau de bovins furieux pourchassait Loubeyre.

Une fois les traces de la bête fuyarde retrouvées, il ne fallut guère de temps au vieux chasseur pour la localiser. Elle paissait tranquillement dans un vallon, à l'abri d'un bosquet de hêtres, la selle sur le flanc et le filet perdu. Elle ne portait aucune marque de morsure et ne boitait visiblement pas. Un peu inquiète, elle hésita à l'approche des deux hommes mais en peu de temps, un licol improvisé lui était passé autour du cou et elle suivait docilement.

Le Vieux Loubeyre souriait de satisfaction.

« Je te l'avais dit, c'est pas un chevalier[^50], ce leu. Il fait parfois du dégât, mais il sait que les chevaux, ça porte souvent des gens d'armes. Alors il les évite.

--- Mille grâces de votre aide. Je ne sais comment vous mercier.

--- Moi j'aurais bien une idée, et ça te coûtera qu'un peu de temps. »

Ernaut s'arrêta pour flatter le cheval et attendre que son compagnon en dise plus. Le chasseur tourna la tête de-ci de-là, comme s'il auscultait les environs, à la recherche d'indices qu'il serait le seul à percevoir.

« On va mener ta bête à mon hostel. Elle y sera en sauveté. Pis on ira faire un boulot où j'ai besoin d'un gars costaud

--- Quel genre ?

--- Ça va te plaire. On va se venger de ce démon. »

### Plateau du Limon, après-midi du vendredi 27 juillet 1156

Les deux hommes progressaient rapidement, courbés pour éviter les branches. Le louvetier s'abaissait parfois tellement qu'on aurait dit une bête se déplaçant à quatre pattes. Il avait échangé son bâton contre une lance de bon frêne, épaisse et courte, au fer large d'une paume, affilé comme un rasoir. Il s'arrêtait régulièrement pour renifler, écouter, indiquant d'un geste impérieux le silence à son compagnon. Puis il finit par se poser dans une sente et fit signe à Ernaut de s'approcher pour lui murmurer :

« Nous ne sommes plus guère éloignés de la tanière. On a pris au large, car le vent nous est pas favorable. Si elle nous sent trop tôt, la nichée aura tôt fait d'être vidée. Attends-moi là. »

Puis il rampa jusqu'à la crête, vague indistincte ondulant sur les bruyères mauves. Il resta un moment à étudier par-delà le relief puis redescendit.

« Tu me suis exactement, ne fais rien que je fais pas. »

Puis il s'avança avec rapidité en directement d'un bosquet de pins appuyé sur plusieurs gros rochers. Parmi leurs racines entremêlées de pierre, on devinait un puits de noirceur. Loubeyre s'assit à proximité et renifla un instant à l'intérieur, tendant l'oreille.

« Vérole, la mère a filé !

--- Quoi ?

--- Les petits sont là, mais la mère est dans le coin, à attendre qu'on se dévoile pour attaquer, j'en ai certeté. »

Il soupira.

« Bon, pas grave, pousse la grosse pierre qui bloque le passage, on va dénicher les oisillons tant qu'on est là.

--- Et s'ils sont plusieurs à venir ? »

Ernaut n'était que peu motivé à l'idée de s'attaquer à des travaux de terrassement alors qu'une horde de loups risquait d'arriver à tout moment.

« Aucune chance. J'ai toujours cru que le vieux était seul, et pis j'ai découvert qu'il avait une femelle. J'ai mis du temps à trouver cette tanière, alors vu qu'elle est pleine, on va les zigouiller. Avant que la meute se forme. »

D'un mouvement de tête, il invita Ernaut à se mettre au travail. Une grosse pierre enchevêtrée dans les racines empêchait quiconque de pénétrer dans le tunnel d'où s'exhalait une forte odeur. Tandis qu'Ernaut s'efforçait de débloquer le comblement rocheux, il lui sembla entendre comme des glapissements. Plusieurs fois, il s'arrêta, le souffle court, et s'attira un regard courroucé du louvetier. Et, finalement, il parvint à faire rouler le bouchon, dans un grognement libérateur. Loubeyre se retourna, l'air satisfait. Il glissa son couteau hors de son étui et tendit sa lance à Ernaut.

« Tu vas demeurer ici. Peu de chance qu'ils viennent. Ils nous savent déjà là pourtant. Mais ils ont trop peur.

--- Vous êtes sûr ?

--- Tu n'as pas la taille pour aller dans ce fin boyel chercher les petits, alors on a pas le choix. »

Ernaut opina tandis que le louvetier plongeait les mains en avant dans le trou, sa lame devant lui. Il lui fit un clin d'oeil avant de passer la tête. Puis, enfin, les pieds disparurent complètement et seuls des glissements et râles d'effort parvinrent aux oreilles d'Ernaut. Celui-ci choisit de s'appuyer contre le talus, inquiet de se faire assaillir de dos par une bête courroucée de le voir s'attaquer à ses petits. Il savait ces animaux forts discrets et capables de s'approcher à quelque pas de vous sans un bruit. Jusqu'à ce que leur mâchoire se referme sur votre gorge.

À un moment des glapissements et des halètements s'échappèrent de la tanière mais il n'osa appeler Loubeyre, de peur d'attirer l'attention sur lui. Plusieurs fois, il lui sembla reconnaître la forme d'un loup dans les broussailles environnantes. Pourtant rien n'approchait et il n'entendait rien de particulier. Jusqu'à la voix du louvetier, par la bouche de l'antre.

« Tiens, jeune, attrape ces louveteaux ! »

Et Ernaut vit apparaitre, pêle-mêle, les cadavres ensanglantés des petits que le chasseur était allé traquer jusque dans leur tanière. Il en éprouva un pincement au cœur, loin de reconnaître de futurs terribles mangeurs d'hommes, démons à forme animale.

Loubeyre ressortit de là couvert de poussière et de sang, avec une vilaine griffure sur la joue et la main pleine d'ecchymoses. Il ne semblait pas y prêter la moindre attention et capta le regard d'Ernaut mais n'ajouta rien. Il s'épousseta tranquillement, nettoya le sang et détacha une ficelle de sa besace, avec laquelle il noua la demi-douzaine de petits cadavres. Nulle joie ne s'exprimait sur son visage malgré la tâche menée à bien. Il balança son fardeau sur son épaule, reprit sa lance et partit, sans un mot pour Ernaut.

### Plateau du Limon, soirée du vendredi 27 juillet 1156

Le petit feu de brindilles crépitait, les flammèches coloraient d'ocre les visages et les mains. Ernaut était tranquillement installé sur un tabouret au coin de l'âtre, face au vieux Loubeyre. L'un et l'autre regardaient la danse incandescente tandis que les braises chauffaient le ventre des oules pansues où la soupe mijotait.

Derrière le chasseur, dans l'ombre, un galetas de bonne taille accueillait trois garçons que leur mère, une jeune femme, s'efforçait de coucher. Les deux hommes prenaient de temps à autre une gorgée de vin dans le gobelet qu'ils tenaient à la main. Dans un coin de la pièce, accrochées à une poutre, les dépouilles des louveteaux oscillaient. Le louvier les regarda un moment puis porta ses yeux sur Ernaut.

« Difficile de les voir comme des démons, hein ?

--- Je n'avais jamais vu de petits...

--- Et des gros, en as-tu jamais vu ?

--- De loin, on les entendait parfois. »

Le vieux secoua la tête.

« Tu sais pourquoi on les tue, jeune ?

--- À cause des attaques ? Ils causent tant de dommages...

--- Sornettes ! Quelques bons chiens et ils n'osent approcher. »

Il s'éclaircit la gorge un long moment avant de continuer :

« On jalouse leur liberté. Elle nous broie le cœur... »

Ernaut fronça les sourcils, se pencha en avant, interloqué. Le Vieux continua.

« J'ai pas toujours chassé. J'ai porté le fer et le feu pour le compte de puissants barons, qui en remontraient à pas mal de loups question sauvagerie.

--- Pourquoi vous les chassez alors ?

--- On me fout la paix, ici, et on me paie bien.

--- Pourtant, vous ne les détestez pas, c'est ça ?

--- J'ai peine à croire que Dieu aurait pas mis quelque qualité en une de ses créatures. Même chez l'homme, il en a mis, alors... »

Ernaut soupira, laissa son regard errer dans la modeste chaumière. La jeune femme du louvetier vint s'assoir entre eux, en silence. Elle déposa les écuelles sur un banc et les servit. Ernaut admira le profil doux, le visage fin. Sa fragilité apparente tranchait avec l'aspect hirsute de son époux. Mais ses mains étaient bien celles d'une travailleuse de la terre, aux ongles abimés et aux doigts calleux.

Pendant ce temps, le louvier avait découpé quelques longues lanières de pain épais. Le vieux en tendit une à Ernaut en ajoutant :

« J'ai entendu voilà quelques années un jongleur qui contait l'histoire d'un gars, un Breton, le Bisclavret. C'était un homme qui devenait loup. Tu en as déjà entendu parler ?

--- De certes ! Les garous ! Les plus terribles des démons !

--- Pas celui-là. Lui avait le cœur pur et c'est son épouse qui avait comploté pour lui nuire. À la fin il se fait reconnaître ses droits et le roi lui pardonne.

--- Que voulez-vous dire ? Que certaines de ces males bestes ne sont pas méchantes ?

--- Dame, que crois-tu ? J'aimerais voir mes trois fils, ces louveteaux, aussi braves et vaillants que les bêtes que je pourchiace saison après saison. »

Ernaut hocha la tête et déchira son pain dans la soupe, puis il attrapa sa cuiller et commença son repas. Pendant un temps, on n'entendit plus que les longs lapements et les bruits de mastication. Puis, alors qu'il sauçait le fond de son écuelle, Ernaut ne put se retenir :

« Vous savez, il se dit dans le hameau que vous seriez...

--- Un meneur de loup ? Dame, je sais bien ! »

Il s'esclaffa et s'essuya la bouche dans sa manche.

« Il n'y a là que ramassis de crétins et familles d'imbéciles. Plus proches du bœuf que de tout autre bête, alors forcément, le loup...

--- Vous en êtes un ? »

Le Vieux Loubeyre arbora un large sourire et avala un gros morceau de pain, l'air amusé, puis fit un clin d'œil.

« Si on te demande... » ❧

### Notes

Les longs trajets à l'époque médiévale, s'ils se faisaient sur des routes et des voies relativement fréquentées, comportaient leur lot de surprises et de problèmes. On n'était jamais sûr de la direction qu'on prenait, aucune signalétique précise ne jalonnant l'avancée. Il fallait donc se fier aux indications des autochtones et de ceux avec qui on partageait le chemin un temps durant. Le soir, même pour les pèlerins, l'accueil pouvait être problématique, sans parler de la météorologie. Les textes abondent d'anecdotes sur les aléas des voyages et on disait que nul ne pouvait se prétendre voyageur s'il n'avait pas connu l'inconfort d'une nuit dans un fossé humide.

Le rapport à la nature sauvage avait évolué depuis le Haut Moyen Âge, en ce douzième siècle, et partout l'homme apposait sa marque, domptait les forces qu'il avait subies depuis des siècles. On asséchait les marais, on déboisait, on défrichait et ensemençait chaque fois que c'était possible. Même les zones de moyenne montagne furent investies à l'année, profitant d'un redoux commencé peu après l'an Mil et qui durera jusqu'à la moitié du XIVe siècle. À partir de là de nombreux hameaux en Auvergne furent abandonnés en Auvergne sur les plateaux entourant le Puy Mary.

Si d'aventure vos pas vous mènent dans cette région, ne manquez pas d'aller visiter la Maison de la Pinatelle à Chalinargues, qui explique la domestication de ces zones par l'homme au fil du temps.

Si vous avez envie d'admirer les paysages qu'Ernaut et Lambert traversent lors de ce récit, vous pouvez en avoir un aperçu en parcourant le [sentier de découverte du Frau de Vial](http://www.tracegps.com/fr/parcours/circuit6058.htm), sur la commune de Ségur-les-Villas. Quant au chemin entre Apchon et Murat, sur le plateau du Limon, il existe toujours, c'est [le sentier des Quirous](http://www.puymary.fr/sites/www.puymary.fr/files/fiche_individuelle_rando_le_sentier_des_quirous_v2013.pdf).

### Références

Ad. de Chalvet de Rochemonteix, *La maison de Graule. Étude sur la vie et les œuvres des convers de Citeaux en Auvergne au Moyen-Âge*, Paris : Alphonse Picard, 1888.

Collectif, *Voyages et voyageurs au Moyen Âge*, Paris:Publications de la Sorbonne, 1996.\[En ligne\], consulté le 23 septembre 2011, Adresse URL: <http://www.persee.fr/issue/shmes_1261-9078_1996_act_26_1>

Heath Sydney, *Pilgrim Life in the Middle Ages,* London, Leipsic: T. Fisher Unwin, 1911.

Mallouet Jacques, \* Auvergne de nos racines\*, Barembach : Éditions J.-P. Gyss, 1985.

Verdon Jean, *Voyager au Moyen Âge*, Paris : Perrin, 2003.

Webb Diana, *Pilgrims and Pilgrimage in the Medieval West*, London - New York: I.B. Tauris, 1999.

Harf-Lancner Laurence, « Bisclavret » dans *Lais de Marie de France*, Paris : Librairie générale française - Le livre de poche, Collection Lettres gothiques, 1990, p. 117-135.

Par delà les ombres
-------------------

### Monts du Liban, mardi 10 décembre 1157

Le vent descendu des cimes hurlait sur les reliefs, crachant des embruns de neige sale dans un grondement rageur. Saturant l'atmosphère de grésil immaculé, le fin blizzard griffait les yeux, mordait la peau exposée, ployait les arbres, brisait les branches. Sur un sentier malingre, parmi les caillasses roulantes s'avançait pourtant une silhouette. Une main décharnée s'employait à retenir une couverture mitée. Les pieds à demi nus, amas de poussière, de plaies et de gerçures, butaient à chaque pas. Rien ne pouvait les faire renoncer.

Le marcheur s'arrêtait régulièrement, ajustait sa dérisoire protection face aux assauts furieux des éléments et reprenait sa route. Squelette maintenu debout par sa simple volonté, il ne sentait plus la faim ni le froid, la soif ni la douleur. Il n'avait qu'un but : atteindre les montagnes où se tenait celui qui le libérerait, l'homme qui avait les réponses. Il en avait entendu parler alors qu'il s'éloignait de la côte, un jour où il tentait de mendier, camouflant les flétrissures qui suppuraient encore sur son visage.

Des fers portés au rouge avaient été apposés sur ses joues et son front, arrachant dans la douleur ses derniers lambeaux de raison. Des passants s'entretenaient d'un homme, dans la montagne, qui avait trouvé la solution à tous les maux de ce monde. Ils l'évoquaient avec révérence, comme s'ils craignaient de le voir apparaître au coin de la rue, descendu de sa cache rocailleuse. Le cœur soudain empli d'allégresse, il n'avait pas attendu qu'on le chasse une nouvelle fois, sous les lazzis des enfants et les pierres des parents. Une fois, même, on avait suggéré de le pendre sans plus tarder. Ils ne voyaient de lui que les signes de son infamie, sans plus reconnaître dans le corps décharné un frère, un compagnon affamé, malade, blessé. Un humain.

Il avait alors pris la route, chapardant dans les champs et les jardins à la nuit tombée, avançant sans relâche en direction des monts où la délivrance l'attendait. Un jeune berger, au cœur plus tendre, l'avait hébergé un temps. Il l'avait remercié en lui volant sa couverture, qui le protégeait désormais des assauts de la tempête. Il lui fallait continuer, atteindre le prochain col, dépasser le relief suivant, jusqu'à le trouver. Le vieux de la Montagne.

### Monts du Liban, mercredi 18 décembre 1157

Sayed referma la porte du pied, les bras encombrés de bûches. Il les lâcha en un tas désordonné dans un fracas d'écroulement qui amusa les deux bambins assis près des flammes. Inspirant bruyamment tandis qu'il se relevait en faisant craquer son dos, il se dépêcha d'aller mettre la barre à l'huis et assujettit un boudin de paille à sa base.

« Le vieux Gibran dit que ça va durer encore trois jours, le vent a tourné par-delà le Barouk[^51]. C'est jamais bon qu'y dit. »

Sa femme, occupée à touiller un pot où fumait un épais gruau hocha la tête sans un mot. Il fouilla dans un repli de son vêtement et en sortit un morceau de markouk[^52] enveloppant un peu de viande fumée.

« Tiens, Josef a donné ça aussi, pour l'inconnu. Il veut pas qu'on soit seuls à devoir le nourrir.

--- Tu as accepté ? Ils croient que je sais pas prévoir ? Qu'on est des mendiants ?

--- Non, on nous a confié sa garde mais les anciens ont décidé que c'était le village qui l'accueillait, et c'est à tous de le nourrir. »

Elle pinça les lèvres et acquiesça de mauvaise grâce. Puis prit la viande. Au fond, cela la soulageait. Ils n'étaient pas si riches qu'ils pouvaient refuser cette aide au plus fort de l'hiver. Ce n'était pas que l'inconnu mangeait tant que ça, il avalait à peine quelques cuillers de kichk[^53] chaque jour. Mais la vie était rude pour une famille modeste comme la sienne. Elle s'arrangerait pour que la viande serve à ses enfants avant tout.

Sayed s'assit près du feu et se frotta les mains. Il épousseta les traces de neige fondue qui subsistaient sur ses épaules. Fascinés par leur père, les deux petits le regardaient, les yeux écarquillés. Dehors, le vent sifflait sur les toits, faisait vibrer les planches des fenêtres étanchées de torons d'herbe sèche. Son épouse, Massina, avait pris place à ses côtés après avoir posé un pichet de sahlab[^54] entre eux. Il s'en servit une tasse qu'il se mit à boire avec lenteur, le regard tourné vers l'obscurité du fond de la salle, vers la paillasse aménagée pour leur visiteur. Les enfants avaient repris leurs jeux, remuant et poussant des jouets de bois grossiers tout en arpentant leurs contrées imaginaires. Massina reprisait une toile, chantonnant une de leurs chansons favorites.

Blottis autour du feu, ils étaient tranquilles, à l'abri. Si la tempête les séparait du monde extérieur, elle les en protégeait aussi. Nul brigand, nul seigneur pillard n'osait s'aventurer dehors par un temps pareil. Seuls les fous le faisaient, comme cet homme dont Sayed avait la garde. Un de ces Celtes, venus de par-delà les mers, le visage dévasté de brûlures infamantes.

Sayed n'aimait guère avoir affaire à eux. En fait, il n'aimait avoir affaire à personne. Surveiller son maigre troupeau de chèvres dans la montagne ou sarcler ses cultures, voilà ce qui le contentait. Il se sentait libre, dans ces moments-là, et se surprenait à fredonner la même chanson que Massina, qu'ils avaient tant hurlé, enfants, dans les monts environnants. Il regarda ses mains tavelées, crevassées. Il n'avait pas trente ans et déjà son dos se voûtait. Sa belle Massina ne souriait plus que de quelques dents. Et il y voyait de moins en moins bien d'un œil, depuis quelque mois, quand les fièvres avaient bien failli l'emporter en même temps que leur fille cadette.

La vie n'était pas tendre pour les montagnards comme eux, et il n'avait pas besoin qu'un Celte à l'esprit dérangé s'ajoute à cela. Dès la tempête terminée, il proposerait qu'on le laisse à son destin. Un de ceux qui comprenaient leur langue disait que ce fou parlait d'un vieux scheik. C'était peut-être un sage, un ermite qui aspirait à la sainteté dans l'abnégation, l'oubli de soi. Sayed secoua la tête, dépité. Il méprisait ces hommes égoïstes, qui cherchaient leur bonheur seuls, sans se soucier des autres. Lui avait une famille à sa charge, il se comportait comme il fallait. Et ce n'était pas un vagabond de passage qui mettrait tous ses efforts en danger. Certainement pas.

### Monts du Liban, vendredi 20 décembre 1157

Le grincement de la porte forcée par une soudaine bourrasque réveilla Sayed en sursaut. La blancheur de la neige qui commençait à s'accumuler à l'entrée l'éblouit lorsqu'il ouvrit les paupières. En un instant, il fut hors du lit, se ruant vers la table où il empoigna un solide couteau, à manche épais. Le froid mordait ses jambes nues. Il avança néanmoins jusqu'à l'huis et risqua un coup d'oeil au-dehors.

L'aube naissait à peine et le village, noyé de neige, dormait encore paisiblement. Aucune maison n'avait allumé le feu, personne n'était en vue. C'est là qu'il aperçut les traces. Légères, en partie effacées, s'éloignant de sa demeure. L'angoisse étreignit son cœur et il se retourna, affolé. Le renfoncement qu'il partageait avec Massina était calme, il distinguait la silhouette de sa femme allongée, endormie. À leurs pieds, les matelas des enfants, également paisibles.

Il resserra sa prise sur le couteau et s'approcha de l'endroit où le Celte se reposait. Il n'y avait plus rien, l'homme était parti, avec la couverture. Sayed grogna, soulagé. Si l'inconnu avait envie de mourir, ça ne le concernait pas. Il était malgré tout contrarié de voir qu'il s'était fait voler, et en demanderait réparation au conseil du village. Apaisé, il lâcha la lame sur la table, tout en allant refermer la porte. Le froid commençait à le gagner et il frissonna quand ses pieds effleurèrent la neige. Il bloqua la barre et ajusta le boudin de paille, soupirant une fois cela fait. Il n'avait jamais aimé le vagabond.

« Sayed ? Que se passe-t-il ? s'inquiéta Massina dans son dos.

--- Rien. Le Celte s'en est allé.

--- Tu l'as laissé faire ? Il va mourir !

--- Il est parti depuis un moment, ses traces dans la neige sont à peine visibles.

--- Il faut prévenir les autres, aller le chercher. »

Il revint à sa couche sans un mot, et en retrouva la chaleur avec un frissonnement de plaisir. Il se frotta le visage.

« Il nous a volé la couverture et peut-être plus. Crois-tu qu'il vaille la peine qu'on se risque là dehors en cette saison ? »

Massina ne répondit rien, le visage plongé dans les ténèbres. Elle lâcha un soupir.

« C'était serpent malfaisant, soyons heureux qu'il ne nous ait pas fait grand mal. Que ses pas le mènent au loin, ça me va. »

Tirant la couverture sur ses épaules, il se rapprocha de sa femme dont la chaleur le réconfortait.

« Pour l'heure, le jour est à peine levé, il n'est pas temps. Dormons. »

### Monts du Liban, vendredi 3 janvier 1158

La petite troupe cheminait en file indienne sur l'étroite sente serpentant à flanc de falaise. Le temps était froid, mais, le blizzard retombé, ils tentaient enfin de retourner chez eux. Du moins le lieu qu'ils nommaient ainsi pour cet hiver, un abri sous roche qu'ils avaient fermé par des murs de pierres sèches, du torchis et des branchages.

Ils ne restaient jamais plus d'une saison ou deux au même endroit, craignant qu'un potentat local ne vienne les en déloger l'épée à la main. Ils n'étaient pas si nombreux et guère vaillants lorsqu'il fallait s'affronter à une troupe entraînée et bien équipée. Pakâlin, leur chef, était un déserteur de l'armée damascène, jeté sur les routes quand ses maîtres avaient perdu la ville. C'était le seul qui sache vraiment se battre, à cheval comme à pied. Et il tirait à l'arc avec talent.

Malgré tout, là, il ressemblait à un loqueteux, emmitouflé dans des hardes pour se protéger du froid, menant leur dernier cheval. Il ne restait que son casque d'acier, à la peinture fort écaillée, pour attester de son statut. Ils n'étaient qu'une dizaine, avec un maigre butin. Partis fourrager quelques jours plus tôt, ils n'avaient guère pu extorquer aux paysans misérables du coin.

L'hiver était rude pour tous, et on ne pouvait prendre ce que les gens n'avaient pas. Ils avaient déjà mangé plusieurs de leurs montures au fil des semaines. Le groupe stoppa soudain. Pakâlin avait levé le poing et semblait intrigué. Un de ses lieutenants, Boutoum, un solide anatolien aux mains puissantes, s'avança, tirant sur son foulard pour parler.

« Un souci, chef ? »

Le Turc ne répondit pas, les yeux fixés devant lui. Une maigre silhouette se tenait là, un squelette décharné à peine protégé de lambeaux sales, le visage brûlé par le froid, couvert de boue. Pakâlin dégaina doucement son sabre, tendant les rênes de son cheval à Boutoum.

« Qui es-tu ? Que fais-tu là ? »

L'homme ne bougea pas, se contentant de tourner la tête vers le vide de la vallée en contrebas.

« Pousse-toi de mon chemin ! »

Pakâlin s'avança, tout en se dépêtrant de ses vêtements chauds afin d'être plus à l'aise pour frapper.

« Tu m'as pas compris, l'homme ? »

Il se retourna vers sa troupe :

« Nous aurions bien besoin d'un nouvel esclave, qu'en dites-vous ? Ça m'éviterait de devoir le tuer pour qu'on passe ! »

Quelques hochements de têtes et rires nerveux approuvèrent. Le Turc fit encore un pas et se figea quand il entendit la voix forte :

« Es-tu le Vieux ?

--- Quoi ?

--- Es-tu le Vieux de la Montagne ? Celui qui apaise les cœurs ? »

Pakâlin s'esclaffa.

« Ouais, de juste, c'est moi. Viens me servir, vieux débris, et tu connaîtras le bonheur ! »

Le visage décharné s'éclaira un instant puis se déforma sous l'emprise de la colère, de la haine. Sa voix se brisa lorsqu'il hurla :

« Tu mens ! Tu n'es pas... »

Il se tut lorsqu'il vit le brigand s'avancer, le sabre au clair, le visage déterminé, il hésita un instant et cria :

« Tu n'as pas le droit, tu ne sais pas qui je suis ! Je suis le gên... »

Son cri se perdit dans un grondement gigantesque, accompagné d'un tremblement qui alla en s'amplifiant. Tous les hommes se jetèrent au sol ou y tombèrent sans le vouloir. Le cheval rua subitement, affolé, arrachant les rênes des mains de Boutoum. Il roulait des yeux paniqués tandis qu'il sentait la terre se dérober sous ses sabots. Il sautait, tentant de se détacher de ce roc fuyant, traître, se cabrant et frappant dans le vide. Jusqu'à trouver le dos de Pakâlin.

Le Turc n'eut pas le temps de réagir qu'il volait déjà dans le profond précipice, bientôt rejoint par la pauvre bête assassine de son maître. Un hurlement de terreur fut la dernière chose que les hommes de Pakâlin entendirent de lui. Terrés la face contre le sol, épouvantés, aucun n'osait bouger. Il fallut plusieurs minutes après la fin des secousses pour que Boutoum se relève. Il fit quelques pas hésitants, ramassa le sabre demeuré dans la poussière. La silhouette était toujours là, désormais agenouillée. Le brigand s'interrogeait, sa lame tremblait tandis qu'il la brandissait vers l'inconnu.

« Par l'épée de saint Michel, mais t'es qui toi ? »

### Sidon, mardi 16 septembre 1158

L'évêque Amaury faisait les cent pas, visiblement ennuyé. L'archevêque était à ses côtés, assis sur un des petits sièges pliants disposés là pour qu'ils profitent de la douceur du midi. Dans le cloître ombragé, le vent léger faisait frissonner les arbustes et les buissons taillés, mêlant les fragrances des fleurs aux effluves marines. Face à eux, un clerc boiteux au visage triste attendait qu'on le questionne plus avant, semblant indifférent à sa tâche, les yeux dans le vague.

L'évêque reprit :

« Il n'a donc rien dit ? Tu ne sais même pas son nom ?

--- J'en suis désolé, mon sire, il n'a même pas paru me voir. D'aucuns le nomment al Djinn, le démon, ou le génie. Mais lui ne dit rien. Il était là, assis à même le sol, à marmonner, le poil hirsute collé de boue et de crasse.

--- Était-ce là quelque prière ?

--- Je n'en ai reconnu aucune, sire archevêque. Je ne crois pas qu'il s'agissait de latin non plus. »

L'évêque s'agita :

« Serait-ce là des appels en mahométan ?

--- Difficile à dire, mais il ne m'a pas semblé, même si je ne suis pas spécialiste. Je ne connais que quelques mots, pour parler avec les gens au-dehors... »

L'archevêque Pierre toussa pour intervenir de sa voix faible :

« Les gens des villages le vénèrent-ils ? N'est-ce pas celui qu'on appelle le Vieux de la Montagne ?

--- Il ne me semble pas. On ne parle pas de lui depuis très longtemps, et il ne semble pas s'intéresser aux gens. Quelques paysans superstitieux viennent lui porter nourriture et offrandes, mais je n'ai jamais entendu qu'il ait fait quoi que ce soit. En dehors des miracles.

--- Des miracles ? Quel type de miracles ?

--- Il protège les lieux des brigands on dit. Il parle à la terre, elle lui obéit. »

L'évêque reprit place dans son siège et soupira.

« S'il ne prêche pas, il n'y a guère à s'en inquiéter. Ce n'est qu'un ermite, sans grand intérêt.

--- Il semble bien, en effet. Mais je m'inquiète toujours de voir ces anachorètes incontrôlables aller de par le pays. Ils peuvent jeter le trouble dans les cœurs.

--- De toute façon, nous n'avons guère de paroisses en ces lieux. Cela touche plus les Grecs.

--- Si fait, vous avez raison, mon fils. Aucune crainte à avoir pour la fréquentation de nos églises ou le versement de la dîme. Mais je m'inquiète toujours de voir des idées étranges se propager dans les cœurs. »

L'évêque opina, réfléchit un instant.

« Voulez-vous que je fasse en sorte de le déloger ?

--- Non, cela ne ferait que déplacer le souci. Gardez-le juste à l'œil. Peut-être n'est-il qu'un saint homme s'efforçant au bien. »

L'évêque esquissa un sourire, amusé. Il congédia le clerc d'un geste de la main et tourna les yeux vers le délicat jardin de la cour, orné de mille couleurs, où voletaient des papillons et quelques passereaux. Le ciel accueillait quelques nuages paresseux qui jouaient avec la lumière. Un saint, dans son diocèse ? Et pourquoi pas après tout ? La manne des pèlerins se déversait jusqu'à présent surtout en Galilée, Samarie et Judée. Quelques miracles plus au nord pourraient les y attirer, ainsi que leurs dons et offrandes. Qui était-il pour s'opposer à la volonté divine ? s'amusa-t-il en son for intérieur. Il sourit à l'archevêque et murmura :

« *O altitudo divitiarum sapientiæ, et scientiæ Dei*[^55] » ❧

### Notes

La justice médiévale latine utilisait régulièrement les signes d'infamies (marques au fer, section du nez, des oreilles...), de façon à pouvoir aisément identifier les fauteurs de trouble et condamner très sévèrement les récidivistes (souvent à la peine capitale). À cela s'ajoutait la mise au ban de la communauté, par l'exil forcé. Le coupable était ainsi placé à l'écart de la société dont il avait transgressé les règles, et perdait l'accès à toutes les solidarités. Cela pouvait revenir au final à une condamnation à mort.

Le clergé au XIIe siècle n'a pas encore investi tous les pans de la société médiévale et les mentalités demeurent généralement assez souples malgré les mouvements de réforme ecclésiastiques qui appellent à plus de rigueur. À la fois dans l'observance des règles pour les clercs eux-mêmes mais aussi pour les fidèles. Beaucoup de prélats se contentent de voir leurs ouailles assister aux offices et verser la dîme. Ils n'interfèrent que peu dans leurs croyances, cherchant à canaliser les pratiques plutôt qu'à contrôler les esprits. Beaucoup de saints personnages accèdent ainsi à une reconnaissance officielle de l'Église alors même qu'ils ne se présentent pas comme très orthodoxes au départ.

### Références

Régnier-Bohler Danielle, *Croisades et pèlerinages. Récits, chroniques et voyages en Terre Sainte XIIe-XVIe siècle*, Paris : Robert Laffont, 2002.

Le Goff Jacques, Rémond René (dir.), *Histoire de la France religieuse. Des origines au XIVe siècle*, Paris : Éditions du Seuil, 1988.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Marchande d'oublies
-------------------

### Baisan, matin du jeudi 24 mars 1149

Les mouvements de son compagnon sur la paillasse réveillèrent Margue. Recroquevillée contre le mur, elle enfouit la tête dans un moelleux coussin de laine pour ne pas voir le jour s'insinuant par les volets de bois. Ancelin bâilla bruyamment puis la secoua sans ménagement. La poussière scintillait dans les rais de lumière vive.

« J'ai déjà ouï l'père Martin sortir ses poules. »

Elle ne réussit qu'à marmonner sans émettre un son intelligible, mais se redressa néanmoins. Au bout de leur couche dormait à poings fermés leur enfant, Robert, petit garçonnet d'une paire d'années. Le seul qui ait survécu de tous ceux qu'elle avait eus jusqu'alors.

Elle soupira, étendit les bras, souriante. Le soleil étincelant mettait fin à plusieurs jours de pluie ininterrompue. Même si c'était excellent pour les cultures, elle était fatiguée de déambuler sous des averses. Et aujourd'hui, elle devait se rendre au château du seigneur Hugues. Il avait annoncé son retour, avec quelques attardés des grandes armées allemandes et françaises. Ils prévoyaient de faire route vers le nord, après avoir envisagé de participer à une vaste offensive sur Ascalon, au sud du royaume.

L'intendant avait mandé de nombreux domestiques pour organiser un impressionnant banquet. Cuisinière renommée, Margue se louait à chaque fois pour aider à préparer les mets. Pâtés, tourtes et oublies, venaisons, chapons et rôts n'avaient aucun secret pour elle. Ayant grandi dans la demeure du sire de Picquigny[^56], que ses parents avaient rejoint lors de sa venue outremer, elle était à son aise pour régaler nobliaux et damoiselles.

Elle s'étira comme un chat alangui, à plusieurs reprises, puis fouailla parmi son épaisse chevelure brune histoire d'achever de se réveiller. Son époux en était déjà à lacer ses souliers qu'elle était toujours en chemise, sous les couvertures. Il toussa puis cracha avant de se tourner vers elle.

« T'amèneras l'eau avant de partir. Pour le dîner, je m'arrangerai.

--- Il se trouve du fromage frais encore, ainsi que du brouet que tu n'auras qu'à chauffer.

--- Je sais pas si j'aurais désir de monter le feu à la vêprée. J'irai p't-être chez l'Thibaut. »

Margue retint une grimace de déconvenue. Elle n'aimait guère ce célibataire, récemment arrivé au casal, qui semblait passer son temps à boire et jurer. On lui prêtait des mœurs dissolues et une fâcheuse tendance à s'emporter. Depuis qu'il le fréquentait, Ancelin était d'ailleurs devenu plus bourru, plus distant. Il ne levait guère la main sur elle ou le petit, sauf à de rares exceptions, mais il bougonnait de plus en plus fréquemment et parlait peu, si ce n'était pour se plaindre ou critiquer ses voisins.

Malgré tout, Margue sourit. Une journée au château, à préparer un repas de fête lui changerait les idées. Les quelques deniers qu'elle rapporterait lui permettaient de n'avoir à subir aucun reproche de son mari, bien qu'elle sache pertinemment qu'il détestait la voir quitter leur foyer. Il aimait que le souper soit prêt, au soir, le feu allumé et l'eau tirée. Rien ne l'emportait plus que de découvrir qu'elle avait pris du retard, à discuter avec les autres femmes, au lavoir ou au four.

Elle s'assit au bord de la banquette tandis qu'il finissait de lacer sa ceinture. Entre temps, il avait poussé la barre de la fenêtre et le jour, ainsi que la fraîcheur, pénétraient largement dans la pièce. Margue frissonna et se dépêcha d'attraper ses chausses, accrochées avec sa vieille cotte à la perche à la tête du lit. Elle avait cette robe de bonne laine depuis des années. Elle l'avait agrandie avec le temps de bandes de tissu de moindre qualité, tandis que son corps s'empâtait à chaque grossesse. Elle avait toujours eu tendance à l'embonpoint mais la maternité avait encore forcé le trait, et elle arborait désormais une silhouette opulente, hanches lourdes, cuisses larges et ventre tombant, ainsi qu'une poitrine qui ne manquait pas d'attirer l'œil des hommes.

Elle finit de s'habiller en démêlant son épaisse toison qu'elle noua avec habitude avant de la dissimuler sous une coiffe de lin grisonnant. Son époux était déjà sorti et le bruit qu'il faisait dans l'appentis voisin lui indiquait qu'il prenait les outils pour se rendre à leur courtil, au sein de la palmeraie irriguée. Le boucan qu'il faisait pour s'emparer d'une houe et d'une bêche ne manquait jamais de la surprendre. Ces bruits réveillaient presque chaque fois le petit Robert, qu'elle n'allaitait plus elle-même. Elle ne lui donnait plus le sein depuis quelques mois, son lait s'étant tari à la fin de l'automne. Une voisine l'avait aidée au sevrage, tandis qu'elle habituait son fils aux bouillies et gruaux. Le voir ainsi allongé, offert et paisible pendant son sommeil, lui arrachait toujours un large sourire. Il était de bonne constitution et avait déjà survécu à une crise de fortes fièvres. Doté d'un solide appétit, il grandissait et grossissait de façon correcte.

Comme chaque mère, Margue vivait avec la peur qu'une maladie ou un accident n'emporte son enfant avant qu'il ne soit adulte. Mais cette fois, elle sentait bien qu'il était robuste et vaillant. Le choix de son prénom n'y était pas pour rien, elle en était convaincue. En Normandie, on racontait aux petits les aventures du duc Robert, qui s'y entendait si bien pour berner les foules qu'on le surnommait le Diable.

C'était tout le mal qu'elle souhaitait à son enfant, de tracer ainsi son chemin dans ce monde ardu et farouche. Si Ancelin ne s'y opposait pas, elle envisageait de proposer au sire Hugues de prendre leur fils comme domestique, dans l'espoir d'en faire un sergent. Peut-être un de ceux qui portaient haubergeon et empoignaient l'épée. Au moins pourrait-il se défendre contre les nombreux périls qui assaillaient le commun. Mais, pour l'heure, avant d'en faire un grand guerrier, il fallait lui préparer son brouet du matin.

### Baisan, fin de matinée du jeudi 24 mars 1149

La cuisine était de proportion modeste, et on s'y frottait, s'y coinçait, s'y gênait lors des jours affairés. Assise sur un escabeau, près de la porte vers les réserves, la cellérière veillait, si pingre qu'on aurait cru lui tirer une pinte de sang à chaque mesure de farine qu'elle concédait.

Malgré cela, l'endroit était agréable, et la chaleur du vaste foyer était bienvenue en ce mois encore frais. Les odeurs de légumes, de jus grésillant, de pâtes levant, de fruits macérant, d'épices parsemées, de vin assaisonné se mêlaient en une éblouissante et appétissante symphonie. La seule exception à ce tourbillon de saveurs alléchantes demeurait la zone où l'on préparait les poissons. Corvée dont on se déchargeait sur les plus grognons, les moins habiles, les plus détestés. Ils devaient éviscérer, trancher, écailler, dresser les filets sans discontinuer, jusqu'à être imbibés à leur tour des senteurs de pêche.

Margue était occupée à tamiser de la fleur de farine pour les pâtisseries les plus délicates lorsque l'activité de la cuisine se ralentit soudain. Une des servantes à l'entrée, son panier de raves à la main, venait de demander le silence. Un cor résonnait depuis l'enceinte. Un appel, puis un second. La femme écarquilla des yeux affolés.

« Ennemis aux abords! »

En un souffle, la cuisine se vida. Même l'intendante, dans son empressement, oublia de pousser la porte du cellier avant de sortir dans la cour. Depuis les autres bâtiments, les serviteurs s'étaient aussi avancés, apeurés. Ils se blotissaient tous auprès de la tour centrale, bien que leur maître ne soit pas encore arrivé. Une poignée de soldats couraient le long des courtines. Le portail avait été clos rapidement et, depuis la vaste esplanade, Margue ne pouvait voir que les communs, le donjon et l'immense ciel gris pommelé, menaçant à tout instant de lâcher quelques gouttes.

Un des gamins affectés aux écuries grimpa sans demander l'autorisation jusqu'au rempart pour jeter un coup d'œil, vers le nord. Il cria de sa voix aigrelette :

« Une troupe d'infidèles sur la grand colline !

--- Nombreux ?

--- Guère, répliqua un des soldats. Mais trop peu pour n'être plus que des éclaireurs. »

L'absence de sire Hugues et la faible garnison présente semèrent l'inquiétude parmi la foule. Jusqu'à ce qu'un chevalier sortît de la tour. Les épaules larges, avec un cou de taureau et un air renfrogné, Geoffroy était un homme d'expérience, circonspect et posé. Sa simple apparition suffit à calmer les esprits. Il dévala les marches et traversa la cour sans un mot, la main sur son épée. Il lâcha un sourire forcé au fèvre avant de grimper sur le mur d'enceinte. Puis il donna des instructions aux quelques sergents. Un des soldats courut aux écuries, bientôt rejoint par des valets. Enfin il se tourna vers la foule amassée en contrebas.

« Quelques cavaliers qui se massent sur la colline au-dessus de l'Harrod[^57]. Je vais faire prévenir le sire Hugues, qui ne doit plus être fort éloigné. Ce peut n'être que des nomades qui espèrent faire leurs choux gras après le départ des armées de Louis et de Conrad[^58].

--- Et les nôtres ? Ceux du casal ?

--- Je vais faire ouvrir la porte pour ceux qui pourront nous rejoindre. Nulle inquiétude à avoir en pareilles murailles. »

Il sourit, puis se gratta le menton un petit moment.

« Je ne crois pas qu'il puisse s'agir de puissant ost. Belvoir nous aurait dépêché messager en tel cas.

--- Sauf si les infidèles l'ont pris, hasarda une voix anxieuse. »

Geffroy s'emporta, et lâcha quelques postillons.

« Ne dites pas pareilles sornettes ! Il s'agit là d'un puissant castel, malaisé à forcer. Et qui a bien temps de lancer quelques coursiers prévenir à l'entour. »

Margue jetait de temps à autre un coup d'œil vers le portail, voir si son époux s'y présentait. Elle avait bien amené Robert avec elle, pour le confier à une amie servante au château, comme d'autres gosses. Avec la peur qui s'était répandue, chaque mère aspirait à tenir ses enfants dans ses bras. Elle savait qu'Ancelin n'était pas le genre d'homme à prendre des risques. Il lâcherait sa houe à la première silhouette turque pour s'enfuir à toutes jambes.

Ce n'était pas tant qu'elle l'aimait tendrement, mais elle s'y était habituée au fil des ans. Il n'était pas le compagnon dont elle avait pu rêver secrètement, enfant, mais il y en avait de pires. Il travaillait avec application, si ce n'était avec acharnement, et n'avait pas la main trop leste. Sans cette tendance grandissante à lever le coude, elle se serait estimée assez chanceuse. Suffisamment en tout cas pour espérer de lui qu'il arrive à la rejoindre au plus vite.

### Baisan, matin du vendredi 25 mars 1149

Chacun avait dormi comme il l'avait pu. Margue avait trouvé refuge avec son fils dans les cuisines. Elle y avait traîné une paillasse rudimentaire, et bénéficiait ainsi de la chaleur du foyer. Si le repas n'avait pas été aussi festif que prévu, au moins avait-il donné l'occasion d'un feu conséquent.

Sire Hugues était arrivé en milieu d'après-midi, avec une troupe de presque cinquante soldats, ce qui avait grandement remonté le moral des réfugiés. Entretemps, de nombreux habitants du casal avaient rejoint la forteresse, portant leurs maigres biens dans des sacs, menant leurs quelques bêtes afin de les mettre à l'abri. Tout ce qui ne serait pas serré entre ces murs risquait le pillage ou le feu. À la fin de la journée, la majeure partie des femmes était dans le château, ainsi que les hommes dont les terres étaient les plus proches. Au grand dam de Margue, Ancelin n'était pas de ceux-là. Leurs parcelles étaient parmi les plus septentrionales, vers l'occident.

Cela les mettait aussi les plus loin des soldats musulmans qu'ils avaient aperçus, mais n'offrait guère de chance de le voir arriver sain et sauf. Lorsqu'elle s'assit sur sa couche, Robert dormant toujours, Margue obtint un sourire sans joie de sa voisine, une vieille domestique édentée qui s'occupait des linges de sire Hugues. Elles se levèrent de concert et se rendirent dehors. Là, plusieurs avaient construit des cabanes rudimentaires de leurs maigres affaires, pour ceux du moins qui n'avaient pu trouver refuge dans un galetas quelconque. Margue constata avec soulagement que les remparts étaient régulièrement garnis de soldats, parfois affalés la tête sur les coudes appuyés au mur, à regarder au loin. Elle apostropha un gamin qui cavalait, une outre à la main.

« Quelles nouvelles de la nuit ?

--- Y'a forte troupe qui passe dans la vallée au nord, mais y se sont pas arrêtés pour nous bouter fer et feu. Y z'ont peur de sire Hugues !

--- Des pillards comme disait sire Geoffroy, alors ?

--- Oui-da, semblerait bien »

Puis il décampa sans demander son reste. Alors qu'elle allait rentrer se chercher de quoi manger, une troupe pénétrait par la porte principale, à peine une demi-douzaine d'hommes bien équipés. Des Allemands qui étaient arrivés hier et un chevalier de l'ost personnel du seigneur de Baisan.

Elle s'approcha d'eux tandis qu'ils discutaient en descendant de cheval. L'un des croisés, un grand blond au nez camus, lui sourit, comprenant qu'elle venait aux nouvelles. Il lança ses rênes à un gamin et, ôtant son casque, s'avança vers elle. Il parlait avec un fort accent germanique malgré son vocabulaire plus que complet.

« Rien dans les parages...

--- Les mahométans sont partis ?

--- Ils ont continué par la vallée droit au couchant. Ils sont campés à quelques lieues amont.

--- Dieu soit loué ! Nous voilà saufs !

--- Je ne sais. Ils ont fourragé assez loin et vos hostels sont fort pillés. Ils n'ont pas laissé le moindre grain... »

Margue haussa les épaules.

« Nous ne sommes pas si fort éloignés de la moisson. Et il n'est rien que nous ne puissions reconstruire. Et des habitants du casal ? Sont-ils saufs ?

--- Comme vous le voyez, ici même ! »

Margue sentit son cœur défaillir.

« Nul autre ?

--- Nous n'avons encontré nulle âme.

--- Et à l'église ? Certains y ont de certes trouvé asile ! »

Le soldat secoua la tête.

« Ils ont même emporté la sainte Croix qui s'y trouvait, après en avoir brisé les portes.

--- Et vous n'avez ?... »

L'Allemand baissa le nez, désolé tandis que les yeux de Margue s'embrumaient.

### Jérusalem, midi du mardi 9 août 1149

Eberhard accompagna Margue jusqu'à la porte de Sainte-Marie des Allemands. La pierre couleur de miel rayonnait de chaleur et les parfums sucrés des jardins alentour embaumaient l'air lourd. Des enfants passèrent en criant, attirant l'attention de Robert qui aurait bien quitté les bras de sa mère pour les rejoindre. Un peu au nord, quelques commerces encadraient une taverne, devant laquelle traînaient toujours des ivrognes.

« Alors tu es acertainée, tu demeures céans ?

--- Oui-da. J'ai eu commerce avec les frères de Saint-Jean qui ont boutiques à proposer. Je suis habile assez au fournil pour proposer pâtés et oublies. »

Il s'approcha d'elle, lui prenant la main.

« Je ne suis pas riche, mais ma famille possède du bien, chez moi, à Rothenburg. Nous pourrions y prendre commerce, comme tu en as désir ici.

--- Je te mercie de ta proposition, mais je n'ai nul désir de franchir les mers comme je l'ai fait enfant. »

Elle caressa la joue rugueuse de l'homme, lui souriant avec chaleur.

« Il te faut repartir avec ton sire, et moi ma vie est ici...

--- Je pourrais voir à prêter serment à un quelconque baron et demeurer.

--- Devenir félon à celui que tu as servi jusqu'alors ? Je ne te le demande pas et ne l'accepterait point. Demeure féal, ainsi que tu l'as toujours été, mon bel ami. »

Elle déposa un baiser évanescent sur la pommette de son compagnon et s'écarta, à regret.

« Mes prières auprès du tombeau du Sauveur te porteront chance. »

Puis elle s'éloigna d'un pas qu'elle espérait assuré en direction du nord, vers la rue du Temple. Elle devait retrouver le frère hospitalier au change latin. Elle n'avait pour tout bien que ce qu'elle portait sur elle, dans ses baluchons.

C'était son mari qui avait prêté serment au sire de Baisan, et elle n'avait nul désir d'y demeurer après sa disparition. Elle avait vendu ce qu'elle pouvait, donné ce qui restait et avait pris le chemin avec les Allemands qui avaient souhaité se rendre à Jérusalem, avant leur départ.

Eberhard avait été un compagnon attentionné tout au long des dernières semaines, mais elle ne voulait pas finir femme à soldat. Et il y avait Robert, qu'elle espérait bien voir grandir en un lieu protégé et non pas déambuler sur les routes au milieu des soudards. Peut-être que les frères de Saint-Jean accepteraient de l'éduquer, d'en faire un lettré, qui sait ?

Elle retrouva le petit homme tonsuré dans l'échoppe qu'elle se proposait de louer. L'endroit sentait la poussière et l'humidité, mais il était assez spacieux et à proximité du four, ce qui lui faciliterait la tâche. Le clerc dévorait à belles dents une oublie bien garnie, dont le jus coulait sur ses doigts. Il l'aperçut et sourit, dévoilant une partie de son déjeuner :

« Ah ! Notre amie l'Allemande ! » ❧

### Notes

Le peuplement des zones rurales des territoires latins du Levant se faisait beaucoup par des chartes qui garantissaient certains droits en échange de redevances. Néanmoins, de façon à attirer les populations, celles-ci étaient souvent bien moins lourdes qu'en Europe. Lors de l'installation, les chefs de famille prêtaient généralement serment auprès du propriétaire des terres. Certaines coutumes d'alors sont parvenues jusqu'à nous, parfois partiellement.

Il faut concevoir les foyers comme des unités de vie et de production, et leur constitution ne nécessitait pas forcément l'amour des conjoints. Des considérations économiques entraient en ligne de compte : il était essentiel d'avoir suffisamment de bien pour garantir la survie du groupe. On a donc longtemps dépeint ces couples comme basés sur la raison plus que sur le cœur. Pourtant, on découvre dans les sources suffisamment d'exemples qui prennent à contrepied cette image d'Épinal.

Non pas que tous les mariages (qui étaient alors des unions essentiellement civiles, l'Église n'ayant pas encore complètement intégré cette cérémonie dans ses sacrements) aient tenu compte de l'avis des deux époux, mais il ne faut pas les caricaturer à l'inverse. Par ailleurs, les sentiments envers les enfants étaient réels et profonds, comme ils peuvent l'être aujourd'hui, malgré la très forte mortalité infantile et l'espoir de l'aide de leur force de travail pour les vieux jours.

Les oublies sont une pâtisserie du genre des crêpes, et leur nom est à rattacher à la même racine qu'oblat et oblation, *don*.

### Références

Ellenblum Ronnie, *Frankish Rural Settlement in the Latin Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1998.

Schaus Margaret (éd.), *Women and Gender in Medieval Europe, an Encyclopedia*, Routledge, New York & Londres : 2006.

Schnell Rudiger, Shields Andrew, « The Discourse on Marriage in the Middle Ages » dans *Medieval Academy of America* 73, 1998, p. 771-786.

Sheehan Michael M., *Marriage, Family, and Law in Medieval Europe: Collected Studies*, Toronto: University of Toronto Press, 1996.

Rey Emmanuel Guillaume, *Les colonies franques de Syrie aux XIIe et XIIIe siècles*, Paris : A. Picard, 1883.

Liberté
-------

### Baalbek, nuit du youm al sebt 26 dhu al-qi'dah 550[^59] {#baalbek-nuit-du-youm-al-sebt-26-dhu-al-qidah-55059}

Ombre parmi les ténèbres, accroupi près d'un imposant bouquet de genévrier, un homme en haillons attendait, immobile. La brise glissait sur lui, sur ses hardes, pourtant il n'avait pas froid. Son esprit était entièrement tourné vers son but.

Ses yeux hagards épiaient fièvreusement le garde censé surveiller les esclaves. Le prisonnier avait réussi, à force de patience, à creuser un trou dans le mur de la masure où on les entassait, mais il avait été le seul à oser s'y faufiler. Les autres captifs étaient trop fatigués, trop désespérés pour s'y risquer. Pas lui ! Il avait une furieuse soif de liberté.

Ni le fouet ni les privations n'avaient altéré sa volonté. Il se sentait plus fort que jamais, en rage et plein d'ardeur. On lui avait retiré la femme qu'il aimait, trainée loin de lui. Puis on l'avait fouetté, durement, avant de l'amener dans cette ville maudite pour y travailler comme un forçat. Tandis qu'il s'épuisait à porter les gravats, acheminer les pierres, il se répétait sans cesse le nom de sa bien-aimée, se raccrochant à son souvenir pour ne pas sombrer. Il était décidé à s'enfuir pour la retrouver, la libérer de ses geôliers infidèles. La Terre sainte s'était révélé un enfer emplis de démons et d'ordures dont ils devaient s'échapper.

Malgré la fureur qui l'habitait, il restait tapi, semblable à une pierre dont il avait la couleur poussiéreuse. Un hamster s'y trompa, passant devant lui ainsi qu'il l'aurait fait devant un amas rocheux, indifférent. Inquiété par l'effluve que le vent venait de rabattre, l'animal fit un détour. Sur son trajet se trouvait un autre humain, celui-là même que le fugitif surveillait avec attention.

Mustafâ al-Dimashqi tapait des pieds pour se réchauffer, agacé de rester seul à surveiller le chantier dans cette glaciale nuit noire où la rare lumière des étoiles ne lui était d'aucun réconfort. Il entretenait les flammes qui lui réchaufaient les articulations et éclairaient une petite zone autour de lui, mais sans cesser de râler contre cette tâche injuste.

Cela faisait plusieurs années qu'il avait rejoint l'aḥdāṯ[^60] de sa cité et on ne lui confiait jamais que des tâches secondaires. Garder les montures, approvisionner le camp, encadrer les prisonniers, voilà tout ce qu'on acceptait de lui ! Il avait espéré s'enrichir en devenant soldat, en détroussant les riches voyageurs Ifranjs[^61], mais au final il n'était que le commis des véritables soldats, ces maudits Turcs qui s'accaparaient les riches butins.

Il craignait d'avoir des problèmes avec sa femme, car il ne ramenait guère d'argent au foyer. Elle ne lui en faisait pas reproche, elle n'aurait jamais osé. Néanmoins il voyait bien dans ses yeux qu'elle le considérait comme un raté. Elle n'arrêtait pas de lui parler de ses frères, qui réussissaient dans les affaires, de ses cousins qui habitaient de magnifiques demeures.

Il s'en était ouvert à son amir[^62] et avait demandé à se voir confier de plus grandes responsabilités. Voilà pourquoi il se retrouvait là, toutes les nuits, à garder un œil sur le baraquement des esclaves affectés à la réfection des murs d'enceinte de la ville. Il y avait plusieurs dizaines de captifs et leur valeur était importante, lui avait-on assuré. Mais ce n'était pas ainsi qu'il ferait fortune ! Abu Bakr, son supérieur, s'était moqué de lui et lui avait refilé la corvée dont personne ne voulait, sous prétexte de mission de confiance. Alors Mustafâ se gelait les pieds et les mains en râlant contre son sort.

Le prisonnier espérait pouvoir se faufiler hors de la cour sans être vu. Il fallait juste que cette maudite sentinelle se décide à lui tourner le dos et il pourrait tenter de se glisser silencieusement jusqu'à l'entrée sud. Il avait repéré l'autre côté, mais il y était trop visible depuis les remparts de la ville. Malgré la nuit sans lune, il était facile pour une sentinelle de l'apercevoir dans la vaste étendue vide de toute végétation. Depuis le temps qu'il patientait, il commençait à sentir des crampes, des fourmillements dans son corps fourbu. Il ne pouvait se permettre de perdre trop de temps. Plus tôt il s'échapperait, plus il aurait de chance d'échapper à des poursuivants lancés sur ses traces dès la reprise du travail au matin.

Il réfléchissait à un moyen de faire diversion, peut-être en lançant un caillou. Son regard glissa dans la pénombre et s'arrêta sur une pierre, bien trop grosse pour être lancée, mais tout à fait indiquée pour s'en faire une arme. Il fronça les yeux d'un air approbateur, un sourire malsain déformant ses traits. À défaut de contourner la vigie, il pouvait s'en débarasser. Se pourléchant les babines comme un prédateur embusqué, il s'empara à tâtons de son arme improvisée, l'assujettit avec application dans sa paume, la soupesant et, de petits gestes vifs, fit mine de frapper.

Mustafâ commençait à s'endormir debout et se donna quelques gifles sur les joues pour se réveiller. Il était impatient de voir la relève arriver. Il attrapa l'outre accrochée à une branche et se désaltéra puis entreprit de fouiller dans sa sacoche où il avait un peu de pain et des fruits secs. Il poussa du pied une bûche qui lui servirait de siège, au plus près du feu puis s'assit, engloutissant sans plaisir son en-cas, mâchant bruyamment. Une rafale de vent souleva de la poussière et la propagea sur son campement sommaire, le faisant frissonner. Il étendit ses pieds bottés à proximité des braises, soupirant d'aise à la sensation de chaleur.

Au loin, les hurlements des chacals, entrecoupés d'aboiements, indiquaient qu'une meute partait en chasse. Mustafâ était heureux de n'avoir pas un troupeau à surveiller. Les humains étaient plus faciles à garder, généralement assez dociles. Et plus aptes à éviter les prédateurs.

Il allait cracher un nouveau noyau de datte dans les flammes lorsqu'il perçut du coin de l'œil un mouvement. Il se retourna trop tard, l'homme était déjà sur lui. Il leva tout de même les bras pour ne pas recevoir le coup sur la tête, roulant sur le sol dans le choc, entraînant son agresseur dans sa chute. L'empoignade était aussi violente que désordonnée. Aucun des deux adversaires n'était habitué à la bagarre. Ils se tiraient, se repoussaient, s'arrachaient les cheveux ou se frappaient des coudes avec la hargne de deux bêtes furieuses.

Tout en se débattant, le captif tentait désespérément de retrouver sa pierre, qui lui permettrait de frapper un coup décisif. Mustafâ espérait pouvoir dégainer son petit couteau, passé dans le foulard qui lui tenait lieu de ceinture. Mais le Franc eut plus de chance. Il posa les doigts sur ce qu'il recherchait tandis qu'il était allongé sur le dos, écrasé par son adversaire. Frappant avec l'énergie du désespoir, il sentit le choc sourd contre le crâne et son ennemi s'écroula sur lui comme un tas de guenilles.

Se dégageant rapidement, le fugitif s'agenouilla sur le corps désormais inerte, du sang s'écoulant de la plaie derrière l'oreille. Interdit, il examina la pierre sanguinolente puis son adversaire, sentant le souffle qui s'exhalait encore lentement des lèvres. À plusieurs reprises, il fit mine de soulever son arme pour l'abattre sur le visage de l'homme, mais ne put s'y résoudre.

La tension était retombée. D'un geste dégoûté, il jeta son arme dans les broussailles où il se dissimula bien vite, non sans avoir reniflé de colère une dernière fois. La voie était libre, il ne lui restait qu'à courir. Instinctivement, il attrapa au passage l'outre en peau de chèvre et la besace du gardien et détala comme une bête traquée, se propulsant parfois à l'aide de ses mains lorsqu'il était déséquilibré par sa course sur la route caillouteuse. Par moments, il jetait derrière lui un regard affolé tel un renard à la chasse, comme si des chiens allaient le pourchasser. Mais tout demeurait calme.

Essouflé par son départ précipité, il s'arrêta près de la grande pierre que ses geôliers appelaient Hajar al-Houbla. Elle était tellement énorme qu'une fois taillée, les anciens bâtisseurs n'avaient pu la bouger. Elle serait suffisamment haute pour offrir un point de vue sur les alentours. Il grimpa dessus en un éclair. D'autres feux illuminaient le campement qu'il avait fui, certains plus hauts que d'autres : des cavaliers étaient lancés à sa poursuite ! De colère, il se mordit la lèvre. Il aurait dû achever le gardien. Sa faiblesse allait lui coûter cher.

Lorsque Mustafâ ouvrit les yeux, il avait l'impression que sa tête allait exploser. Contemplant sa main après l'avoir portée à son crâne, il s'aperçut qu'elle était couverte de sang, de poussière et de graviers agglomérés. Il s'assit lentement puis regarda autour de lui, encore sonné par le coup. Il s'efforçait de remettre ses idées en place, conscient que de sa rapidité dépendait son avenir : un des esclaves sous sa garde venait de s'échapper. Cela voulait dire de gros ennuis en perspective ! Il chercha son sac pour en sortir la trompe de cuivre qui lui servirait à sonner l'alerte, mais il ne la trouva pas. En plus le fugitif l'avait détroussé !

Il se releva à grand peine, se déplaçant lentement vers un appenti près de l'entrée. Là, il se mit à taper sur le bois de la porte de toute ses forces en hurlant « Évasion ! Alerte ! Un esclave s'est sauvé ! Alerte ! » en essayant de ne pas succomber à l'onde de douleur qui se propageait depuis sa tête, jusqu'à lui ébranler les viscères.

Les sentinelles de la cité ne furent pas longues à dépêcher trois cavaliers, des membres de l'aḥdāṯ eux aussi, que Mustafâ connaissait bien. Ils étaient légèrement équipés, seulement d'épées et de masses, et sans armure. Deux d'entre eux brandissaient des torches.

« Il m'a attaqué, y'a pas longtemps, je suis demeuré à terre, assommé. Il est de sûr parti vers le bosquet de sapins, au couchant, pour s'y cacher, ou sur la route d'Anjar. »

Un des cavaliers lança un regard noir vers la sentinelle défaillante.

« Il y a long chemin d'ici le Wadi al-Tayim, il n'est pas prêt de rejoindre les terres ifranjs ! »

Les chevaux s'élancèrent sans guère attendre, projetant les gravillons en tous sens tandis qu'ils galopaient sur le chemin poussiéreux. Le calme revint soudain, après les hennissements des bêtes, les cris des hommes. Mustafâ porta de nouveau la main sur sa blessure et contempla sa paume recouverte de sang, aussi noire que la nuit dans l'obscurité sans lune. Il fit une grimace, qui de douleur devint de colère. Il pensait avoir atteint le fond lorsqu'il s'était trouvé seul à garder les captifs. Mais il enrageait de découvrir que sa situation pouvait encore empirer, à cause de ces maudits Ifranjs ! Impuissant, il leva vers le ciel une main implorante. Qu'avait-il fait de si terrible pour être ainsi perpétuellement châtié ?

L'évadé vit les cavaliers prendre la route du sud vers lui. Il redescendit précipitamment de la pierre et, avisant qu'elle était penchée et offrait un petit espace dégagé à sa base, il se glissa dessous. Il espérait laisser passer les chevaux et reprendrait son chemin par la suite, en les sachant devant lui. S'écorchant et se meurtrissant les membres, il réussit à disparaître totalement sous l'énorme roche, comme un animal craintif terré dans l'ombre de son antre.

Léchant ses mains griffées, il essayait de calmer son souffle, au cas où les poursuivants viendraient inspecter les environs. Il s'efforçait aussi de ne pas succomber à la fureur qui montait en lui. Il s'était montré faible en laissant la vie sauve au gardien. Ce dernier avait pu rapidement donner l'alerte. Crachant et pestant en silence, il se jura de ne jamais refaire ce genre d'erreur. Quoiqu'il lui en coûte, sa main ne faiblirait plus ! ❧

### Notes

Les fréquents combats en Terre sainte ne donnaient pas lieu à des massacres systématiques, et les populations étaient plus souvent razziées qu'exécutées. Parmi les soldats, beaucoup voyaient dans les combats l'occasion de s'enrichir.

La prise de captifs constituait une source estimée de revenus, qu'ils soient ensuite relâchés contre rançon, généralement pour les plus riches et puissants ou avec le plus de relations, ou simplement utilisés comme esclaves, éventuellement pour alimenter un marché aux ramifications internationales.

Les pèlerins, voyageurs isolés loin de leurs familles, souvent mal équipés et peu fortunés rentraient logiquement dans cette dernière catégorie et les récits évoquent parfois la disparition de groupes entiers, sans plus de précisions. Parfois certains chroniqueurs évoquaient le destin de tous ces sans-grades qui, à l'occasion d'un traité ou de la libération de captifs prestigieux, recouvraient alors la liberté.

### Références

Friedman Yvonne, *Encounters between enemies. Captivity and ransom in the Latin Kingdom of Jerusalem*, Leiden : Brill, 2002.

Gravelle Yves, *Le problème des prisonniers de guerre pendant les Croisades Orientales 1095-1192*, Maîtrise ès Arts en Histoire, Université de Sherbrooke, 1999.

Fils de la louve
----------------

### Rive de la Save, fin de matinée du mercredi 21 novembre 1151

Les toiles de l'armée byzantine avaient rongé le paysage le long de la Save[^63], engloutissant un petit village et la dérisoire forteresse de rondins censée le protéger. La fumée des feux de camp traçait des colonnes grises jusqu'à la voûte laiteuse du ciel. L'humidité du matin avait reflué en même temps que la brume, mais le froid maintenait son emprise, marquant par endroit le sol de gelée blanche. Tout ce qui pouvait brûler avait été découpé, arraché et livré aux flammes sur un vaste périmètre. Les fourrageurs devaient désormais s'aventurer à plusieurs lieues pour trouver de quoi réchauffer les articulations craquantes des soldats.

À perte de vue, la plaine s'étendait, les reliefs lointains mangés dans les lambeaux de brouillard qui persistaient dans les creux des champs et des pâtures. Des hameaux parsemaient le paysage, reliés par de maigres sentiers bordés de haies et de murets, séparés de bosquets aux branches nues. Une large bande de terre boueuse coupait au travers, sombre ruban qui s'attachait aux basques de l'armée byzantine.

Un peu au nord du cantonnement, vers l'entrée orientale, des soldats avaient réquisitionné des canots et des pirogues pour tenter de pêcher de quoi améliorer l'ordinaire. Près de là, assis sur un moignon de mur dans ce qui devait être à l'origine un enclos à bétail, un cavalier s'employait à réchauffer de la graisse qu'il appliquait avec soin sur sa selle. Le cheveu hirsute coincé sous un bonnet cylindrique de feutre, emmitouflé dans un épais kaftan en peau de mouton, il soufflait dans ses mains tout en regardant les apprentis pêcheurs s'escrimer sans guère de matériel ni plus de succès. Un de ses compagnons, à la démarche pesante, se rapprocha de lui, ses cuirs sous le bras. Il le gratifia d'une tape amicale sur l'épaule et s'installa à son tour.

« On devrait traverser d'ici à deux jours au plus tard. L'empereur a grand désir de fustiger ces maudits Huns[^64].

--- Je goûterai fort d'être en selle, à profiter de la chaleur de ma monture. »

L'autre acquiesça, tout en démontant les sangles et étrivières de sa selle.

« Au moins ici ils auront compris ce qu'il en coûte de se rebeller.

--- Le crois-tu ? Le pansebaste sebaste[^65] Kantakouzenos y a laissé la main, à ce qu'on dit.

--- Mais l'empereur a occis lui-même leur grand chef, Bakchinos.

--- Ce n'était que le meneur des Huns, et Manuel a décidé de les poursuivre de sa colère. »

Le premier cavalier posa son chiffon et se frotta les mains sous les aisselles afin de les réchauffer tout en se tournant vers son compagnon.

« Tu sais, Stamatès, j'ai pas mal guerroyé toutes ces années, et j'commence à me demander si ça sert à quelque chose, tout ça. »

Son compagnon étira les lèvres en un sourire gercé.

« Par l'archange Michel, que t'arrive-t-il ? T'as la lance qui s'amollit avec les ans ?

--- J'suis sérieux, l'âge me gagne et je fatigue. J'ai porté le fer partout où le basileus[^66] le souhaitait, et j'le ferai jusqu'à mon dernier souffle. Pourtant je me demande si ça aura une fin.

--- Tu penses au gamin, c'est ça ? »

L'autre ne répondit pas, les lèvres soudainement pincées, reniflant tout en fixant les remous et les tourbillons de la Save. Il avait perdu son aîné quelques mois plus tôt, alors qu'il s'entraînait à cheval pour intégrer à son tour l'armée impériale. Stamatès souleva sa pesante carcasse et s'étira lentement, s'ébrouant et grognant comme un ours. Il s'approcha de son ami et lui confia doucement :

« La Polis[^67] fait tellement d'envieux. Nos richesses, notre pouvoir, notre puissance sèment la faim dans les cœurs. Qui ne rêve pas d'être Romain ?

--- Les morts. Eux ne rêvent plus. »

### Pélagonie, soirée du lundi 10 janvier 1155

La neige tombée récemment s'était mélangée à la boue et à la poussière ; la plaine ne comportait désormais plus que des traînées grises, aux reflets bruns. Seuls quelques toits conservaient la mémoire des flocons blancs. Devant la modeste cité de Pélagonie[^68], une ville de toiles de tente avait pris place, entourée de barrières, d'épieux et de fossés. Des feux scintillaient dans des fosses, apportant chaleur et lumière en cette fin de journée.

Tout au long des méandres des voies qui permettaient d'approcher de l'enceinte, des gardes contrôlaient, inspectaient ceux qui se présentaient. En sa qualité de cavalier vétéran, Stamatès avait hérité de la responsabilité de la porte de la ville, avec une poignée de fantassins à encadrer. Bien qu'il soit parfaitement conscient qu'il ne s'agissait là que d'une tâche annexe, jugée servile par beaucoup, il mettait un point d'honneur à paraître sous son meilleur jour. Il était amené à croiser beaucoup de hauts dignitaires, des fonctionnaires impériaux et des officiers qui allaient et venaient au service du basileus, installé dans la forteresse.

Il avait brossé sa barbe fournie et ses cheveux ondulés et arborait, par-dessus son klibanion[^69], un court manteau de laine rouge qu'il avait acheté à prix d'or avant de partir de Byzance. Une large écharpe verte, de bel aspect, pendait sous son baudrier d'arme. En outre, il veillait à ce que ses hautes bottes de cavalier demeurent propres, malgré la boue et la neige.

Sa fonction avait par ailleurs un énorme avantage : il dormait dans une des tours de la ville, dans une petite salle qu'il partageait avec ses hommes et où se trouvait une partie de l'arsenal, conservé dans des coffres scellés aux murs. Le matelas était de paille, mais au moins n'était-elle pas humide, voire pourrie, comme pour bon nombre de ses compatriotes condamnés à dormir sous toile.

La journée tirait à sa fin et il allait être temps de clore les portes pour la nuit. Stamatès déambulait sous la grande arche, soucieux de vérifier qu'aucun groupe ne se présentait pour entrer, depuis le camp, ou pour sortir, depuis les rues adjacentes. Il n'en vit surgir que son vieux compagnon de combat Nicéphore, au petit trot. D'un mouvement sec du poignet, celui-ci arrêta sa monture et se pencha, son haleine alourdie de vin traçant de la buée dans la fraîcheur de l'air vespéral.

« Le basileus a fait arrêter son cousin » lança-t-il tout à trac.

Stamatès fronça les sourcils. Il avait vu arriver en début d'après-midi Andronic Comnène en grand appareil, tel le fier duc de Branicevo et Belgrade qu'il était. L'homme traînait une réputation sulfureuse derrière lui mais sa vaillance, sa force physique, avaient toujours attiré l'admiration des soldats.

« Que s'est-il donc passé ?

--- Il semblerait que Manuel ait épuisé sa patience avec son cher cousin. Déjà que son aventure avec la fille d'Alexis l'avait mise à mal.

--- On ne démet pas duc comme Andronic pour histoires de coucheries.

--- Il a ridiculisé le neveu du basileus et le pansebaste sebaste Jean avec son histoire d'évasion. »

Le souvenir de l'anecdote amena un sourire sur les lèvres de Stamatès, et un hochement de tête de désapprobation de la part de Nicéphore.

« Ris pas, on n'humilie pas ainsi des dignitaires de leur rang.

--- Que ne sont-ils pas aussi braves et habiles que lui ?

--- Plaire aux soldats ne lui sera d'aucun secours. Et s'il est enferré, c'est parce qu'on l'accuse de vouloir s'allier aux Huns. Il avait déjà été envoyé ici, loin de la Polis, pour ne pas succomber aux tentations des intrigues de palais. Il descend du grand Alexis[^70], comme le basileus, et pourrait avoir des prétentions dangereuses.

--- Que va-t-il devenir ?

--- Il repart pour la Mère des cités, mais chargé de fers et non plus d'honneurs, pour y méditer dans une geôle impériale. »

Nicéphore salua son ami et relança son cheval au trot en direction du camp. La cathédrale se mit à sonner les vêpres. Stamatès huma l'air froid, tapa des pieds puis donna l'ordre de barrer les portes pour la nuit. Soufflant dans ses mains gelées, il tourna son regard vers la forteresse dont les ouvertures s'ornaient de lumières vacillantes. Machinalement, il lissa son foulard vert, heureux de n'avoir pas à naviguer parmi les puissants et les dignitaires. Au moins pouvait-il compter sur quelques amis.

### Venise, après-midi du mardi 31 mai 1155

Assis sur le pont de la galée, sous la voile déployée en tente, Stamatès regardait l'ondée s'abattre sur les eaux de la lagune, sous une voûte aussi grise que les vagues. Les bâtiments eux-mêmes semblaient avoir perdu toute couleur, mangés par la brume. Un des marins lui avait expliqué qu'il n'y avait que trois ciels possibles à Venise : le brouillard, la pluie ou le brouillard *et* la pluie.

En tout cas, la traversée de l'Adriatique s'était déroulée sans trop de mal malgré une mer agitée et un temps maussade tout du long. L'humeur des hommes s'en ressentait et certains grognaient qu'il aurait mieux valu se fier aux conditions météorologiques pour leur départ qu'aux auspices astrologiques. De son point de vue, Stamatès trouvait la situation assez enviable. Accompagner le sebaste Michel Paléologue à Venise pour une mission diplomatique revenait pour lui à dormir et manger dans un endroit tranquille, sans risquer de finir dans la boue.

Il avait espéré pouvoir visiter la cité lacustre, mais n'en avait guère eu le temps. À peine étaient-ils arrivés que Stamatès était désigné pour repartir bientôt, comme escorte à un *mandâtor*[^71], Arcadius Zonaras, à bord d'un des navires les plus rapides. Le basileus attendait la réponse avec impatience, disait-on. Ses émissaires étaient là pour tenter de contrer les visées expansionnistes des rois barbares de Sicile, qui infligeaient de cuisantes défaites depuis des années aux troupes impériales. La vanité sans borne et l'opportunisme des Siciliens entravaient la politique outremer, en obligeant l'armée et la flotte à se montrer sans cesse sur le qui-vive sur ses frontières occidentales.

Un sifflement lui fit tourner la tête. Sur le quai, Katarodon lui faisait de grands signes, l'invitant à descendre à terre. N'ayant aucun supérieur à qui rendre compte en l'absence du diplomate, Stamatès franchit la passerelle en quelques enjambées.

« Quelles nouvelles du palais ?

--- On doit aller retrouver le mandâtor plus tard, ils tiennent banquet. Puis on monte à bord pour mettre au large demain. »

Stamatès avisa le large sourire de son compagnon.

« Les nouvelles sont bonnes on dirait.

--- J'en sais trop rien. Je crois oui, tout le monde a l'air de joyeuse humeur. Mais c'est pas pour ça que je m'enjoie. »

Stamatès pencha la tête, intrigué. Il ne connaissait guère Katarodon, jeune recrue appartenant à une famille aisée de Byzance. Il avait à peine vingt ans mais se trouvait déjà là où le vétéran avait mis plus d'une dizaine d'années à arriver, après de nombreuses campagnes. En outre, ses tenues d'apparat généralement bleues et blanches trahissaient son appartenance à la haute société ou, du moins, son désir de s'y intégrer.

« J'ai eu bonne fortune d'encontrer un lointain cousin à moi, Loukas, un marchand. Il est ici pour négoce, assez bien installé. Il propose de nous inviter à souper. »

Stamatès saliva rien qu'à l'idée de manger autre chose que des biscuits de mer, du poisson salé et du gruau insipide. Aucune chance néanmoins de pouvoir déguster du kavourmas[^72] comme sa famille le préparait à Radolibos[^73]. La perspective était pourtant engageante et son amitié naissante avec Katarodon lui permettait d'espérer pouvoir se faire des relations qui aideraient à améliorer sa condition. Il aimait son statut de soldat, mais pour devenir officier, il savait que la vaillance et la compétence ne suffiraient pas. Des appuis politiques et des amis bien placés étaient indispensables. Il ajusta la capuche de son manteau et étendit un bras pour flatter l'épaule de Katarodon.

« Au moins aurons-nous bonne chère avant de mettre à la rame. Hâtons-nous d'aller dévorer avant de retrouver le mandâtor.

--- Le soleil est encore haut, ami. Nous avons même temps d'aller flâner près des rues des boutiquiers. J'aimerais fort trouver de ces bijoux francs qu'on dit si beaux, décorés d'émail brillant. »

Stamatès échappa un rire gras.

« Quelque jeunette à séduire en la Polis ?

--- Si seulement ! Mes parents ont décidé de m'unir à gamine dont la famille pourrait grandement nous aider. Elle n'a pas dix ans et il me faudra attendre pour profiter de cette union. Mais cela n'empêche nullement de leur témoigner de mon respect par quelques cadeaux. Je sais qu'ils apprécient fort ces bijoux, si chers à Byzance.

--- Voilà judicieuse idée, jeune. Allons donc dénicher cette précieuse babiole puis nous lamperons quelques goulées de bon nectar en une taverne, pour choisir lequel porter à ton cousin. »

Il s'esclaffa, tout en tirant son manteau sur ses épaules tandis que la pluie redoublait. Ils avançaient dans les flaques claquant sous leurs semelles, indifférents aux visages fatigués des domestiques affairés, bavardant bruyamment, plaisantant l'un de l'autre. Heureux comme des enfants, ils goûtaient le plaisir d'être l'élite du monde d'alors, s'amusant de leur voyage dans les confins barbares des territoires. Il étaient des Romains de la Polis.

### Abords de Sivas, fin d'après-midi du lundi 12 décembre 1155

Le verglas faisait craquer l'herbe sous les pieds des chevaux et la glace dévorait les eaux du Kizilirmak, saupoudrant les bras morts d'un givre pulvérulent. La vallée s'élargissait peu à peu, en direction de l'est, et bientôt ils allaient déboucher sur la vaste esplanade où la route depuis Charsianon juqu'à Mélitène croisait, dans la cité de Sivas, celle qui traversait le plateau depuis le nord. Le froid habillait de fumée les paroles et chacun économisait son souffle.

Le responsable de l'intendance avait demandé à ce qu'on leur apporte au plus vite un mouton, afin de préparer le souper pour les hommes et l'ambassadeur. Seulement, malgré le temps, un pâtre avait sorti les ouailles du village où ils avaient fait halte, histoire d'épargner au maximum les réserves de foin.

Stamatès avait été désigné pour accompagner un de leurs escorteurs, Zeheb, auprès du paysan pour lui acheter une bête. Stamatès appréciait le Turc car il employait sans trop de difficulté le grec, suffisamment en tout cas pour parler de tout et de rien, comme on le faisait pour s'occuper lors des longs trajets. Pas très grand, il avait un œil fou à demi aveugle qui le faisait ressembler à un démon. Son nez fin, cassé à de multiples reprises, son sourire en partie édenté et son épaisse toison brune aux minces nattes ornées de brins de tissu colorés travestissaient en fait un esprit allègre, un sens de l'absurde et de l'humour, quoique parfois très rustique, qui ne manquait jamais de surprendre les Byzantins.

Il rappelait à Stamatès un compagnon de combat qu'il avait fréquenté tandis qu'il guerroyait dans les septentrions de l'empire, un auxiliaire venu peut-être des mêmes confins que Zeheb. Ce dernier arborait une impressionnante masse de métal, d'un seul bloc d'acier, à tête de félin, très semblable à celle qu'il voyait présentement à la ceinture de son guide. Ils avançaient au petit trot en direction d'un relief derrière lequel le berger devait emmener les bêtes, à l'opposé du fleuve, sur les contreforts des pentes qui les enserraient depuis qu'ils avaient pris la route.

Un soleil blanc sans chaleur se faisait manger peu à peu par les crêtes, laissant les doigts d'ombre progresser dans les creux et les bosquets. Stamatès frissonna et rentra le menton dans son vêtement doublé de mouton. Zeheb se racla la gorge et désigna le troupeau qui cheminait vers eux, à une poignée de portées d'arc.

« Tu prendras garde aux chiens. Gros et pas faciles, déclara-t-il sans ambages. Les gros trucs jaunes. Pas méchants mais méfiants. »

Stamatès comprit alors que ce qu'il avait pris pour des moutons de loin était en fait de gros molosses de couleur similaire, à la gueule noire. Leurs cous puissants s'ornaient de colliers cloutés et leur démarche pesante indiquait des bêtes lourdes, résistantes, vigoureuses.

« Ça peut te tuer un loup d'un claquement de mâchoire et te broyer la jambe pareil. Plus méchant, ça ferait des bons guerriers. »

Il agrémenta sa remarque d'un large sourire connaisseur puis renifla, s'essuyant le nez de sa manche. Les ténèbres bleutées commençaient à l'emporter sur les lueurs orangées des pentes méridionales. Le bruit des cloches sourdes, les bêlements leur parvinrent peu à peu.

L'affaire fut rondement menée, sous le contrôle scrupuleux des mâtins qui allaient et venaient autour des ovins, obéissant aux sifflements de leur maître. Trois grosses bêtes dont la placidité rendait la puissance encore plus menaçante. Zeheb proposa d'égorger leur achat non loin de là, pour se faciliter le retour, et il accomplit le geste machinalement, sans même demander de l'aide. Immobilisant le mouton entre ses jambes, presque assis dessus, il lui tira la tête en arrière et dégaina un long couteau. Avant qu'elle n'ait eu le temps de comprendre, la bête était morte, son sang s'écoulant à gros bouillons dans les broussailles. Zeheb sourit, tout en maintenant le cou.

« Tu entends pester l'autre ? Il dit on va attirer les loups avec sang. Il a pourtant fait payer assez cher pour qu'on en fasse à notre volonté. »

Ils lièrent la dépouille à l'arrière de sa monture, guère effrayée par l'odeur du sang puis repartirent prestement, pressés de retrouver la chaleur des bâtiments où les attendaient un bon feu.

« À Sivas, je te mènerai chez une vieille qui prépare un des plats d'ici. Avec foie et oignons[^74]. »

Il fit claquer ses lèvres en un bruit de succion.

« Dommage ton chef nous laisse pas prendre du vin du malik. Ça irait bien avec, pour y tremper leur lavash[^75]...

--- Je crains que ton malik n'apprécie pas qu'on boive son vin, non plus.

--- Ça, c'est sûr ! Et si ton chef se montre sans cadeau, vous devoir retourner chez vous sans tête. »

La réponse se voulait sans malice, et Stamatès ne s'en offusqua pas. Il savait que leur venue auprès du prince de Sivas, Yagi Basan, était dangereuse. Mais le basileus avait besoin de l'aide des dirigeants danishmendides[^76] pour occuper le sultan Qilig Arslan, qui menaçait sans cesse ses possessions anatoliennes. Convaincre l'adversaire de son ennemi qu'on était son allié demandait en général beaucoup de présents. Il rit de bon cœur, conscient que l'amitié entre soldats n'empêchait nullement de tirer le sabre ou l'épée pour décapiter celui avec qui on avait trinqué la veille. Il espérait ne pas avoir à défendre sa peau contre Zeheb mais savait qu'il pourrait lui trancher la tête si son devoir l'exigeait. Sans haine, mais sans regret.

### Jérusalem, soirée du jeudi 1er août 1157

Stamatès revenait tranquillement du mont du Temple, qu'il avait enfin eu l'occasion d'aller visiter. C'était la première fois que ses pas le menaient à Jérusalem et il ne voulait pas manquer de voir de ses yeux les lieux saints. Il était loin d'être dévot, mais la curiosité s'ajoutant à sa maigre foi, il avait insisté auprès du mandâtor pour obtenir congé de temps à autre. De toute façon, ils étaient en sécurité dans la cité. Le roi Baudoin avait toujours manifesté de grands égards pour le basileus et on disait même qu'il s'était récemment brouillé avec le prince Renaud d'Antioche, qui posait tant de problèmes aux Byzantins.

Dans le ciel, de la pourpre et de l'ocre se déversaient dans l'azur jusque là invaincu de la journée et l'activité des rues changeait. Les tavernes débordaient plus largement, les commerces et les artisans fermaient les uns après les autres. La zone qu'il venait de franchir ne présentait pratiquement que des arcades closes et des volets tirés. Comme l'odeur métallique l'indiquait, cette partie de la rue du Temple était investie par les bouchers, ainsi qu'en attestaient les traces de sang, les abats que se disputaient des chiens errants.

Déambulant le nez en l'air, il s'amusait de ce que les Celtes pensaient que cette petite ville était le centre du monde. Byzance, la Polis, comme il l'appelait, la Mère des Cités, était d'une taille bien plus considérable. Elle s'étendait sur les deux rives du Bosphore et abritait des palais et des monuments autrement plus impressionnants, s'ils n'étaient pas aussi saints.

« Hé, le Grec ! »

L'appel le tira de ses rêveries et il aperçut le jeune colosse avec qui il avait sympathisé, depuis quelques jours. Heureux de rencontrer un visage ami, il s'approcha, un sourire aux lèvres.

« Es-tu là à effectuer mission pour ton sire ?

--- Non, je acheter manger.

--- Que te dirait d'aller plutôt verser bonne goulées de vin en gosier ? Je cherchais justement compère pour aller à la taverne. »

Le Byzantin acquiesça et mit ses pas dans ceux d'Ernaut, en direction des rues où étaient installées la plupart des échoppes de nourriture toute prête. Ils arrivèrent bientôt devant l'arcade qui desservait le lieu de boisson espéré.

Descendant quelques marches, ils se trouvèrent dans une longue salle où étaient stockés les fûts, le long d'un mur. Plusieurs lampes à huile éclairaient la zone d'une lumière dansante. En entrant, Ernaut attrapa le chat qui était venu se frotter à ses jambes, amenant l'animal à ronronner bruyamment.

« Ho là, maître Garnier ! Y a-t-il mesures de vin pour gosiers empoussierés ? »

Le tenancier, grand gaillard au dos voûté, les rejoignit depuis la cour où il discutait avec quelques acheteurs.

« Le bon soir, ami Ernaut ! Tu viens quérir un pichet de Galilée ?

--- Pas ce soir, fais-nous donc savourer ton Bezek. Mon ami qui est là vient de Byzance, il a droit au meilleur. »

Le tavernier glissa un sourire et attrapa un pichet qu'il remplit auprès d'un tonneau, avant de le tendre aux deux hommes, empochant les piécettes données par Ernaut.

« Si tu veux t'installer dans la cour, y'a de la place. »

Sans lâcher le chat, le jeune homme s'empara du pichet et alla s'asseoir sur un des bancs, à côté de voyageurs qui parlaient un dialecte italien, peut-être des Pisans. Stamatès s'installa face à lui puis ils se mirent en devoir de taster le nectar qui leur avait été servi.

« Tu sais quand vous repartez, compère ? demanda Ernaut au bout d'un moment.

--- Non. Je ne sais pas pourquoi être venus. Juste accompagner mandâtor.

--- Il y a force voyageurs qui viennent ces temps, j'aurais jamais cru que même des mahométans allaient et venaient ici, porteurs de sauf-conduits. »

Stamatès haussa les épaules.

« Affaires de puissants, pas pour nous...

--- Moi je serais roi de Jérusalem, j'aurais vite fait d'assaillir villes ennemies et d'instaurer la paix du Christ comme il sied à bon monarque.

--- Pas si simple, s'amusa Stamatès. Toujours compliqué faire choses.

--- Il suffit d'une bonne armée et de savoir la mener ! »

Stamatès délaça la petite masse à tête de bronze qu'il arborait toujours à sa ceinture et la posa sur la table.

« C'est comme ça, être basileus, ou roi, ou baron.

--- Que veux-tu dire, l'ami ?

--- Arme puissante et semble aisée, habile à fracasser têtes et bras... »

Il avala une lampée de vin, l'air mystérieux puis continua, se penchant en avant, soudain espiègle.

« Mais très difficile bien user, car coup doit être subtil. Jamais simple de toucher et toujours grand risque à bien manier. »

Ernaut fronça les sourcils, penchant la tête.

« Je ne vois pas le rapport.

--- Être pareil. Bien manier force n'est jamais juste taper fort. Habile est celui qui sait l'employer juste. »

Il tourna la poignée de la masse vers Ernaut, l'invitant à la prendre en main.

« Cadeau pour toi, compère Ernaut. Pour souvenir leçons d'un Romain. Force n'est rien sans habileté. »

Le jeune homme écarquilla les yeux, surpris du présent, mais touché du geste. Il fit voler la petite tête de bronze dans l'air. On lui avait dit que les Grecs étaient bizarres, hautains et précieux. Il lui semblait pourtant qu'il faudrait de peu qu'il y ait une entente réelle entre leurs deux peuples. Pour peu qu'il y ait plus de gens comme Stamatès. ❧

### Notes

Byzance au milieu du XIIe siècle est une puissance incontournable au Moyen-Orient, mais il ne faut pas oublier qu'elle entretient de nombreux liens avec l'Europe. Une partie de ce qui est désormais l'Italie, la Sicile, était d'anciennes provinces byzantines.

De nombreux territoires des Balkans étaient pleinement intégrés à l'empire, ou sous son contrôle en titre. Malgré sa formidable puissance, le basileus était souvent contraint de mener des campagnes sur plusieurs fronts à la fois, maritimes et terrestres. La diplomatie requérait donc une grande finesse et une subtilité qui donnaient une image assez trouble de l'attitude byzantine dans l'échiquier des forces en présence.

On ne compte plus les allusions à la duplicité grecque dans les sources latines. Bien que les croisades aient été lancées à l'origine pour venir en aide à ce frère chrétien oriental, l'arrivée massive de contingents latins qui se taillèrent leurs propres royaumes peut, a contrario, être perçue comme une trahison à leur endroit, ou du moins une déloyauté. Le basileus alors à la tête de l'Empire byzantin, Manuel Comnène, était néanmoins partisan de l'entente avec les territoires latins d'outremer, et multiplia les gestes de bonne volonté, malgré quelques manœuvres diplomatiques parfois contrariantes. Son ambition, très compréhensible, était de demeurer la puissance référente de la région.

### Références

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Lilie Ralph-Johannes, (trad. Par Morris J. C., Ridings Jean E.), *Byzantium and the Crusader States, 1096-1204*, Oxford : Clarendom Press, 1993.

Magoulias Harry J. (trad), *O City of Byzantium: Annals of Niketas Choniates*, Détroit : Wayne State University, 1984.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Cens caché
----------

### Jérusalem, soirée du vendredi 1er mars 1157

Les cloches conventuelles sonnant vêpres finirent par se taire, laissant place à un silence fatigué au sein de la ville sainte. Le père Josce traînait les pieds, allant et venant avec ses cruches à la main, qu'il déposait sur de rudimentaires tables à tréteaux. Une poignée de consommateurs affalés sur les bancs regardaient vaguement le spectacle offert par deux jongleurs sans grâce qui gesticulaient plus qu'autre chose.

La salle se développait autour d'un massif pilier central, rongé de salpêtre, griffé par les ans et les visiteurs. Des lampes à graisse apportaient lumière chiche et fumée épaisse au lieu, maculant les plafonds de traînées noires. Droart était assis là, les jambes tendues, faisant glouglouter le vin dans sa bouche. Malgré l'aspect peu engageant, la taverne offrait des crus de qualité correcte à des prix accessibles. En outre, son abord repoussant faisait qu'elle n'était pas trop fréquentée par les voyageurs, ce qui était plutôt un avantage en cette période de Pâques.

La ville était assaillie de hordes de pèlerins, de pieds poudreux, de curieux, de dévots, qui se déversaient dans les rues, dans les hôpitaux, le moindre local pouvant les héberger, les accueillir entre deux prières. Chaque année voyait ce flux printanier s'abattre sur Jérusalem. Les sergents tels que Droart étaient alors mis lourdement à contribution, entre la perception des cens, le contrôle des portes et la sécurité dans les quartiers.

Le calme, même sordide, de l'endroit lui offrait l'occasion de s'extraire de l'effervescence des rues, de son travail. Il aimait également sa modeste maison, mais avec les enfants et sa femme, il n'avait pas la même impression de tranquillité. Ce fut donc en grimaçant qu'il ouvrit les yeux quand il sentit quelqu'un s'asseoir face à lui.

Petit, le visage massif et les joues tombantes, l'homme paraissait âgé, avec son dos voûté et la calvitie qui ne laissait apparaître que de maigres fils de crin gris. Un pichet à la main, il souriait, offrant la vision de pauvres chicots usés par de la farine de piètre qualité.

« Mestre Droart de la sergenterie le roi ?

--- Mouais. »

L'interlocuteur accentua son rictus et avança le pichet pour emplir de verre le Droart, ce que ce dernier ne refusa pas malgré un air peu engageant. Cela ne rebuta nullement l'arrivant, qui s'installa sur un tabouret.

« Faites excuse de vous aborder ainsi, mais j'ai grand besoin d'entreparler avec vous. »

D'un mouvement du menton, le sergent l'invita à continuer, tandis qu'il avalait une large goulée d'un vin fruité. Le vieux s'était fendu d'un cru de qualité. Un bon point pour lui.

« Je sais que vous êtes de ceux qui prélèvent le cens en ma rue. »

Entrée en matière irritante qui contracta les sourcils de Droart sans lui faire desserrer les dents pour autant.

« J'ai grand souci en ce moment, rapport au fils qu'a disparu... »

Le vieil homme bougeait sans cesse sur son tabouret, faisant grincer le bois et vibrer la table branlante. Une mouche agacée n'arrêtait pas de quitter son corps agité pour s'y reposer presque aussitôt.

« On a petit commerce de portage, voyez-vous... Mais je suis plus tant vaillant qu'en mes vertes années. Alors sans le fils... Et pis, je dois nourrir sa marmaille. Sa femme a le tétin sec, on doit trouver du lait pour le p'tiot...

--- Vous me dites ça pour quoi ?

--- Bein, dans quelques jours, je dois payer mon besant, comme à la saint Michel, la saint Jean et Noël. Mais je l'ai pas... »

Droart soupira. Encore un quémandeur qui venait pleurer sa misère pour échapper aux taxes.

« T'as de la cliquaille pour le vin quand même on dirait.

--- C'est pour vous, le père Josce m'a dit que vous le goûtiez fort, celui-là. »

Droart reposa son gobelet. L'homme le dévisageait, l'air implorant, l'œil humide. Sa tenue fatiguée trahissait sa pauvreté et son teint blafard semblait indiquer qu'il ne mangeait pas à sa faim chaque jour.

« Combien qu't'en a des mioches ?

--- J'ai les cinq du fils, le dernier marche même pas, encore en langes. Et pis la mère de sa femme, une vieille qui peut avaler que du gruau. Elle a perdu toutes ses dents en venant ici.

--- Je comprends ton souci, l'homme, mais j'y peux rien. Le cens, c'est pour le roi. »

L'autre hocha le menton faiblement puis, penchant la tête, ajouta d'une voix indécise :

« J'me suis dit qu'vous pourriez marquer mon cens comme payé sur vos rôles, au moins pour cette fois, ça m'aiderait bien. P't-être que le fils va revenir dans l'été. Il était parti au nord, en Galilée avec un marchand, peu après l'Épiphanie. »

Il tendit la main et déposa une petite bourse de toile sur le plateau, l'air de rien. Droart y risqua un œil et demanda, d'un air contrarié :

« C'est quoi ça ?

--- Y'a un vingt de carats là-dedans. Tout ce que j'ai pu gratter.

--- Ça fait même pas la moitié. »

Le vieux acquiesça, l'air honteux.

« J'aurai pas plus d'ici Pâques, je le sais bien. Mais des fois que vous pourriez trouver que ça peut suffire. Et pis si vous trouvez façon de me rayer de la liste pour cette fois sans avoir à payer plus, vous pourrez garder la bourse pour vous. Ce serait pas souci pour moi, bien au contraire.

--- Ouais, tu t'en tirerais à bon compte. Payer même pas la moitié de ta taxe, le vieux. »

L'autre baissa la tête, blessé par la remarque. Il soupira, fit jouer ses lèvres sur ses dents et se leva, l'air accablé. Il tendit la main pour récupérer la bourse, mais Droart y plaqua ses doigts avant lui.

«Attends, j'ai pas dit non, je vais voir ce que je peux faire. »

### Jérusalem, après-midi du jeudi 21 mars 1157

« Un petit chaudron de cuivre, montra Droart.

--- Un petit chaudron de cuivre » répéta en marmonnant le clerc.

Assis sur un escabeau à l'entrée de la pièce, il notait scrupuleusement sur sa tablette de cire ce que Droart lui énumérait. Le bourgeois qui habitait cette propriété du roi était décédé quelques semaines plus tôt, sans aucun héritier connu. Ses possessions allaient donc être reversées à la couronne et devaient être inventoriées avant de décider de leur sort, vendues ou conservées. L'absence du maître de maison avait permis aux araignées et aux souris d'en prendre à leur aise. Dans les pots de la cuisine, de petites crottes s'ajoutaient souvent aux restes alimentaires.

Quelques beaux plats vernissés, en céramique vert et brun étaient soigneusement rangés sur une étagère. Le sergent les admira, un peu jaloux de ne pas avoir la possibilité de s'offrir si magnifique vaisselle. Le défunt était un négociant, apparemment, qui serait mort sur le chemin entre Acre et Tibériade. Un de ses compagnons de route avait rapporté la nouvelle quelques jours plus tôt, en même temps que ses paquets. Tout en fouillant dans les affaires pour ne rien oublier d'important, Droart continuait de lister tout ce qui tombait sous son regard. Un des coffres abritait des jattes en bois de belle taille.

L'une d'elles, au couvercle tiré, semblait de bonne contenance, qu'il estima à deux mines de grains. Elle était étonnamment pleine, alors que le marchand devait savoir qu'il s'absenterait un moment. Abandonner ainsi du froment chez soi était le plus sûr moyen d'attirer tous les nuisibles du quartier. Esquissant un sourire facétieux, il dégaina son couteau et planta la lame qui ne s'enfonça guère avant de heurter quelque chose.

« Le petit malin, murmura-t-il.

--- Quoi donc ? Je n'ai pas compris.

--- Rien, attends un instant. »

Il plongea la main dans les grains et sentit un petit sac, de ceux qui servaient à ranger des monnaies. Il fit une moue, estimant du bout des doigts la contenance et le nombre de pièces. Le clerc se leva et s'approcha, curieux.

« T'as trouvé quoi, compère ? »

Droart retira sa main et la frotta sur son vêtement. Il ne connaissait pas trop mal Bertaud, un petit gars né dans un casal des environs, formé à l'école des chanoines du Saint-Sépulcre. Il avait assisté à son mariage quelques saisons plus tôt, et, avec plusieurs collègues, lui avait offert à cette occasion un beau morceau de toile de laine pour se faire tailler une cotte. Qu'il portait ce jour même, d'ailleurs.

« Quel âge il a ton marmot, rappelle-moi ?

--- Il aura tantôt une année, on le baptise pour les Pâques, la semaine prochaine. »

Droart hocha la tête doucement, hésitant encore un instant avant de se jeter à l'eau.

« Quelques cliquailles pour lui payer doux langes de lin, ainsi qu'une étoffe fine à ton espousée, ce serait pas de trop alors.

--- Pourquoi tu me dis ça ?

--- Parce qu'y a moyen de verser quelque lardon en notre brouet... »

Ce disant, il sortit la bourse de la jatte, la secouant pour en faire deviner le contenu. Bertaux écarquilla les yeux, stupéfié par cette apparition. Il allait parler, mais seule une bulle de salive se forma à la commissure de ses lèves. Le sergent soupira.

«Oui-da, je sais bien, mais si on prend quelques monnaies, qui s'en plaindra ? Le gars est mort, et le roi, bein il nage dans l'or, c'est un roi.

--- Voler ainsi, cela va à rebours du mien serment.

--- Ouais, je sais bien, mais on en prendrait qu'un peu. Pas le tout. »

Il délaça le cordon et examina le contenu, triant du doigt les pièces.

« Y'a plusieurs dinars, des argentins d'Antioche et sarrasins... Je dirais qu'il y en a pour au moins deux ou trois marcs. Bon poids. »

Le scribe se passa la langue sur les lèvres, réticent à parler. Il se gratta l'oreille nerveusement.

« Je te propose qu'on esbigne quelques dinars, mi-partis entre toi et moi. On laisse le plus gros au sire. »

Il commençait à extraire des pièces, qu'il posait délicatement dans sa paume, jusqu'à en avoir deux piles de douze.

« Voilà, on a là de quoi poivrer nos tranchoirs, l'ami. »

Il plongea son regard dans ceux de son compagnon, tendant la main vers lui. L'autre hésitait, fixant alternativement les pièces et Droart.

« T'attends quoi ? C'est pas les dix deniers du Judas que j'te propose. Nul mal pour personne, et de quoi mieux établir ton hostel. »

Tendant la main avec précaution, comme s'il l'approchait d'un piège à loups, Bertaud finit par s'emparer des monnaies. Cela fait, Droart dissimula bien vite les siennes et échappa un rire bref.

« Un pilon à broyer poivres et épices » aboya-t-il joyeusement, désignant un meuble posé là.

### Jérusalem, soirée du samedi 20 avril 1157

Droart finissait de se rincer, puisant dans un seau d'eau fraîche à l'aide d'une profonde cuiller. Échauffé par la brume ardente, il frissonnait sous la violence de la différence de température. Terminant par le visage, il évacua le trop-plein de ses yeux en les frottant vigoureusement, puis attrapa le linge qu'il avait posé sur une tringle en arrivant. Il sortit alors de la salle, ses soques de bois clapotant dans les flaques.

La pièce suivante était moins chaude, maintenue tiède, et accueillait les baigneurs qui souhaitaient se délasser avant de s'en aller. Des paillasses garnissaient des banquettes, et de nombreuses lampes à huile en verre irradiaient une lumière dansante depuis le plafond. Étant donnée l'heure tardive, il ne restait plus grand monde dans l'endroit, seulement un groupe qui discutait à voix basse en sirotant un pichet et en dégustant des sucreries.

Le sergent s'avança vers eux et toussa pour attirer leur attention. Il avait convenu de ce rendez-vous avec Kaspar, un de ses compagnons de débauche arménien. C'était un fils de commerçant qui se compromettait dans de nombreux trafics. Mais il était surtout un joyeux compère, toujours le mot pour rire et les mains dispensatrices de besants. Il accueillit Droart d'un signe enthousiaste, le présentant à un homme entre deux âges. Un peu avachi, appuyé sur un coude, il se nommait Grifon de Tripoli.

Visiblement métissé, il avait le teint olivâtre, mais les iris clairs et le cheveu, quoique rare, demeurait d'un ton châtain. S'il n'avait cette moue dédaigneuse en permanence et ses yeux tristes, il aurait pu être séduisant. Plusieurs dents qui lui manquaient sur le devant rendaient sa diction sifflante et l'obligeaient à avaler souvent sa salive. Droart s'assit sur le bord de la banquette et attrapa une boulette au sésame, prêt à entendre ce qu'ils avaient à lui dire. Kaspar lui résuma rapidement la situation.

« Grifon est bon compère à ma famille. Nous achetons et vendons l'un à l'autre et avons déjà fait contrats de partenariat ensemble. Sa spécialité, c'est le sucre, et c'est là le souci. »

L'autre acquiesça puis enchaîna.

« Avec la guerre contre l'Égypte, il est malaisé d'y acheminer mon sucre, alors que c'était là-bas que je le vendais au meilleur prix. »

Il soupira, avala une friandise comme pour se consoler.

« Il se plante moult champs de canne à sucre, et les engins à broyer deviennent légion. Nous sommes désormais si nombreux à produire que notre réputation, au comté, commence à en souffrir.

--- La famille de Grifon exporte surtout du sucre sale[^77] et excellent, les meilleures qualités.

--- Pas de dégoutté ou de moyen chez nous, non. On le garde pour les ventes locales » confirma le marchand.

Droart tendit un gobelet pour goûter de leur piquette, les interrompant.

« En quoi ça me regarde, tout ça ? Marchandies vont et viennent, les prix montent et descendent, c'est le lot de ceux qui s'adonnent au négoce.

--- Justement, avec tout ce sucre qui se déverse comme de la Mane, difficile de le vendre au même prix qu'avant.

--- Et les chemins ici sont fort mauvais, donc on passe de préférence par chameaux. La taxe se fait par tête.

--- Je sais tout ça, c'est... »

Il réfléchit un instant.

« Quatre besants la tête à l'entrée, compléta l'autre. Et c'est là le souci. Avant, une charge me rapportait, bon an mal an, dans les quinze livres.

--- Dites plutôt dans les vingt à vingt-cinq, maître, je connais les prix.

--- Vous parlez de la vente à l'échoppe, moi je vends aux apothicaires et épiciers, pas au quidam. »

Droart hocha la tête de mauvais gré.

« Bon, même à quinze, voilà faible taxe.

--- Je ne m'en plains pas. Mais voilà que désormais je peine à vendre au-delà de dix livres, avec toute cette concurrence, et la taxe ne change pas, elle ne tient pas compte de la baisse.

--- Et porter par terre jusqu'ici, en chariot ? »

L'autre leva les yeux au ciel.

« Trop compliqué, pour Jérusalem. Autant me couper un bras et l'offrir aux pauvres de cette cité, j'en aurais plus grand bénéfice.

--- Alors vous proposez quoi ? »

Kaspar se rapprocha et fit un sourire de connivence à Droart.

« C'est une idée mienne. J'ai par-delà les murs quelques enclos bien fermés et petite grange fort commode, en contrebas des vignobles de Saint-Étienne.

--- Ouais, je vois.

--- J'ai proposé à mestre Grifon qu'on réduise la taille de la caravane une fois aux abords. Difficile de fixer lourds paquets si on chemine longtemps, mais si ce n'est que pour passer la porte, on peut fixer bien trois à quatre kintars par bête.

--- Et diminuer d'autant la taxe à payer, je vois. Mais personne sera dupe et on enquêtera pour savoir où sont les autres bêtes... »

Le négociant toussa et confirma d'un mouvement de tête.

« Il faut que les formes soient respectées, et si un sergent de la sainte cité aide la caravane à passer, j'aurai grande joie à l'en récompenser, avec ses compères. »

Droart avala une grande gorgée de vin, le faisant passer d'une joue à l'autre, en réfléchissant.

« Ça peut se faire, ouais. Je ne suis pas tant souvent à la porte de David, mais j'y ai quelques bons amis. Suffit juste que Baset soit pas dans le coin.

--- Qui ça ? s'interrogea Grifon.

--- Un mien compère de la sergenterie, bouffi de tellement de défauts qu'il ahonterait le diable lui-même. Mais y refuserait tout net de tremper là-dedans. Plus chatouilleux de son honneur qu'une pucelle de sa rondelle. Il me faudra distribuer quelques cliquailles ici et là. Vous prévoyez ça pour quand ?

--- Une caravane devrait arriver vers la saint Vital[^78].

--- Si fait. Il y a aussi un nouveau, une sorte de Goliath à museau de goupil. Il me faut estimer d'où vient le vent avec lui. Je peux escompter combien à distribuer parmi sergenterie ? »

Grifon hésita, jeta un coup d'œil à Kaspar puis lâcha, du bout des lèvres.

« Si toutes mes bêtes passent en deça sans encombre, il y aura deux livres à vous départir.

--- Dix besants ? Combien y'aura de têtes ?

--- Seize avoua l'autre, à regret.

--- Alors seize besants m'agréeraient plus avant. »

Grifon fit mine de calculer puis opina du chef. Kaspar en gloussa de plaisir et leva son gobelet pour un toast.

« Puissent nos ventres prospérer et nos vits joyeusement se dresser ! » ❧

### Notes

Les fonctionnaires au service d'un puissant, fût-il le roi de Jérusalem étaient appelés sergents, et tous ne faisaient pas profession de porter les armes. Une tâche essentielle des dirigeants d'alors était d'administrer un vaste patrimoine foncier, des droits et taxes, loyers et redevances. Ceux qui prêtaient serment se retrouvaient plus souvent à devoir superviser des opérations comptables qu'à dégainer l'épée. Le terme sergent est à rapprocher de celui de serviteur, et n'avait pas l'acception militaire stricte qu'on lui connaît désormais.

Malgré tout, certains hommes accompagnaient également l'armée lors de ses déplacements, mais on leur confiait habituellement toutes les corvées annexes de garde et de surveillance, avec un encadrement de professionnels expérimentés, voire de chevaliers.

La réputation de corruption des personnels intermédiaires faisait les gorges chaudes de la population, qui attribuait rarement au prince les dysfonctionnements d'un territoire, en rendant responsables la domesticité et les administrateurs. Les dirigeants, comme saint Louis un siècle plus tard, tentaient périodiquement de contrôler les exactions et abus de ceux censés les servir. Les peines infligées étaient d'ailleurs souvent assez lourdes : marques d'infamies, membre tranché et la mort en cas de récidive.

Les qualités du sucre présentées viennent de l'ouvrage d'Elyahu Ashtor, *Histoire des prix et des salaires dans l'Orient médiéval*, Paris, 1969, p.134. C'est une présentation d'une note venant des lettres et comptes issus de la genizah du Caire. En valeur décroissante, on pouvait trouver du sucre excellent, sale, moyen et dégoutté. Le prix entre le moins cher et le plus cher était de plus du simple au double. Si vous souhaitez en savoir plus sur le sujet, il existe un livre très complet sur la question : Mohamed Ouerfelli, *Le sucre. Production, commercialisation et usages dans la Méditerranée médiévale*, Leiden-Boston, Brill : 2008.

### Références

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Boas Adrian J., *Domestic Settings. Sources on Domestic Architecture and Day-to-Day Activities in the Crusader States*, Leiden et Boston : Brill, 2010.

Favier Jean, *Finance et fiscalité au bas Moyen âge*, Paris : Société d'édition d'enseignement supérieur, 1971.

Foucher Victor, *Assises de Jérusalem. Première partie : Assise des Bourgeois*, Paris: 1890.

Prawer Joshua, *Crusader Institutions*, New York : Oxford University Press, 1980.

Reinhold Röhricht, *Regesta Regni Hierosolymitani* (MXCVII-MCCXCI), Oeniponti : Libraria Academica Wagneriana, 1893.

Sainte-Marie des Crocodiles
---------------------------

### Saint Jean d'Acre, après-midi du lundi 11 mars 1157

Confortablement installés sous une pergola embaumée de la délicate fragrance des cyclamens, les deux hommes sirotaient un vin frais tout en appréciant le calme de cette journée. Le plus âgé, Sawirus, était emmitouflé dans un épais 'abba[^79] de laine rayée, une calotte de feutre ornant son crâne dépouillé. À côté de lui, également assis sur une banquette au matelas moelleux, son interlocuteur semblait noyé dans la masse de tissu qu'il arborait, d'amples drapés engloutissant son corps maigre, ne dévoilant que des mains osseuses et un visage décharné.

Il ne devait pourtant guère être âgé, aucun sel ne venant encore éclaircir les poils de sa barbe ou de ses cheveux bruns, soigneusement coupés et coiffés en arrière. Sous des arcades creusées par le manque de sommeil s'affairaient des yeux inquiets, comme s'ils redoutaient à chaque instant de découvrir un prédateur aux alentours. Il s'exprimait néanmoins d'une voix assurée et ses gestes n'avaient rien d'hésitant.

Plus d'un négociant s'était fait berner par l'aspect craintif de Seguin Jacob. Fils d'un juif converti, il avait transformé les maigres avoirs familiaux en un réseau de commerce tentaculaire, s'étendant de Byzance à Grenade. Il voyageait peu, mais accueillait volontiers ses contacts et ses partenaires dans sa vaste et luxueuse demeure, à peu de distance au nord de la Fonde[^80] d'Acre.

« Je n'ai espoir de vous garder encore long temps, alors ? J'attends la *Lionne des mers* et *Le beau soleil*, qui doivent décharger moult intéressantes marchandies.

--- Je vous mercie de me faire si bel héberge, mais il est temps pour moi de m'en retourner. Mahran est là depuis presque une semaine déjà, et j'ai fait négoce de tout ce que j'avais. Il ne me reste qu'à passer à la Fonde régler les taxes en suspens.

--- N'avez-vous pas déjà versé votre écot ?

--- Si fait, j'ai payé sur les balles que le jeune Grec m'a achetées, toile et papier, mais il me restait encore à régler sur les verres et les épices qui ont mis à la voile ce jeudi. »

Seguin acquiesça d'un mouvement de tête, avalant un peu de vin tout en se tournant vers le petit jardin enclos orné d'une fontaine qui prolongeait son logis.

« Vous auriez hostel ici, ce serait fort plus aisé pour vous de commercer, mon ami.

--- Mon épouse répugne à quitter Césaire. Moi-même j'y suis fort attaché. J'avais acheté cette demeure pour mes vieux jours, et ils sont là. Donc je me conforme à ce que j'avais prévu. »

Seguin n'insista pas. Il savait que le vieux marchand avait perdu son fils unique[^81], des années plus tôt, alors qu'il commerçait. Il avait sûrement espéré profiter d'un repos bien mérité, tandis que son enfant aurait pris sur lui de faire fructifier les avoirs familiaux. Malgré le temps, la douleur était toujours vive, et Sawirus n'aimait guère à en parler.

« Dès que Mahran aura fait changer le fer de ma monture, je prendrai la route.

--- Avec les naves arrivant chaque jour plus nombreuses, vous n'aurez guère de mal à trouver caravane à laquelle vous joindre.

--- Nul besoin, s'amusa Sawirus. La côte est fort tranquille et je la connais bien assez. En deux journées de chevauchée, je serai rentré. Juste mon valet et moi, nous serons rapides. »

Seguin se leva, s'excusant un instant auprès de son invité. Il traversa la salle principale, vaste pièce aux murs chaulés rehaussés de tapis de prix, dont le sol en était aussi moelleux qu'une prairie à force d'y entasser les nattes et les textiles. Derrière une portière, un petit cabinet lui servait de bureau, et il y avait un coffre fort, bandé de fer, dont les aciers étaient noyés dans la maçonnerie. Au-dessus, de nombreuses niches accueillaient les documents sans valeur, les notes, les courriers dont Seguin était continuellement abreuvé. Il chercha parmi ceux-ci plusieurs papiers et les glissa dans une pochette de cuir qui se fermait d'un lacet.

Lorsqu'il revint vers Sawirus, celui-ci avait les yeux clos et il allait faire demi-tour quand ce dernier l'interpella.

« Restez, mon ami. Le sommeil me déserte, avec les ans, quand bien même mes paupières se closent. »

Il se tourna et souleva un sourcil lorsqu'il vit le paquet. Seguin lui fit un sourire et le lui tendit.

« Quelques messages à faire parvenir à des miens amis, à Jaffa. Peut-être aurez-vous possibilité de transmettre à des voyageurs de confiance.

--- Bien évidemment. Surtout en cette saison, les frères de saint Jean et ceux aux blancs manteaux vont recommencer leurs incessants voyages entre les ports et la sainte cité. »

Seguin abonda et ajouta :

« Si seulement cela pouvait suffisamment les occuper pour que la trêve avec Damas ne cesse pas...

--- Auriez-vous entendu rumeurs contraires ? Norredin a versé tribut de plusieurs milliers de dinars lors des accords, à l'automne.

--- Avec l'attaque du roi Baudoin sur les bédouins du Golan et l'arrivée promise du comte de Flandre[^82], je crains fort que les combats ne reprennent avec violence. Le Turc n'est pas homme à se laisser humilier, d'autant qu'il a désormais dans son escarcelle Damas, Mossoul et Alep. »

Sawirus soupira, conscient qu'il n'avait aucun pouvoir sur les incessants combats entre les dirigeants locaux. Quand ce n'étaient pas les Latins qui prenaient l'initiative, c'était un prince turc, ou un chef nomade, ou encore le vizir égyptien. Les armées allaient et venaient, saignant les terres de leur acier insatiable. À peine avaient-ils conquis une place qu'ils convoitaient la voisine ou entreprenaient de se venger d'une autre perdue peu auparavant au détriment d'un principicule[^83] aussi violent et avide qu'eux-mêmes.

L'essentiel était de ne pas se trouver dans les parages lorsque les lames étaient dégainées ou que les flèches volaient. Et pour ça, les plaines côtières de Samarie étaient relativement sûres. Il reposa son verre et sourit à son ami.

« D'ici deux ou trois jours, je ferai route pour ma demeure. J'ai voyagé bien assez pour attendre de voir depuis mon hostel comment cette saison va s'annoncer. »

### Saint-Jean de Tyre, après-midi du mercredi 13 mars 1157

Un ciel de plomb noyait de ses filaments cotonneux les pentes des contreforts du mont Carmel, écrasant une mer dont les vagues écumeuses se brisaient sans vigueur sur la côte sablonneuse en larges bandes mousseuses. Le sentier de gravier et de pierre se déroulait en ligne droite, n'esquissant que de timides lacets pour éviter des massifs de broussailles épineuses, des amas de rochers. Au pied des reliefs à leur gauche, un village ramassé auprès de bâtiments religieux et d'une petite tour laissait échapper les échos d'une activité humaine. Quelques barques tirées sur la côte devaient servir aux pêcheurs du lieu.

Sur le chemin qui fendait les parcelles ensemencées, un large groupe avançait : des marcheurs dont la voix portait les hymnes jusqu'aux oreilles de Sawirus. Autour d'eux gravitaient quelques cavaliers dont certains arboraient le blanc manteau des chevaliers du Christ. Ils allaient se rejoindre à une intersection un peu au sud, auprès de laquelle pourrissaient les vestiges de fourches patibulaires. La route était suffisamment fréquentée et patrouillée pour être sûre, et nul seigneur n'avait eu récemment à pendre des brigands pour servir d'avertissement. Même les pirates égyptiens ne venaient guère ici, aucune richesse ne valant la peine d'échouer son navire sur ces plages.

Par ce temps voilé, difficile de dire si des vaisseaux croisaient au large, galées ou simples embarcations de commerce ou de pêche. Chastel-Détroit, où il avait l'intention de passer la nuit, était généralement ravitaillé par voie de mer, comme beaucoup d'agglomérations littorales. Bien que ne disposant pas d'un port, il était plus rapide et facile de transborder sur de petites barges que d'acheminer par caravane ce dont l'hôpital avait besoin. C'était une héberge mise en place par les hommes au blanc manteau, parmi la lande côtière désolée, sur un saillant rocheux.

Lorsqu'ils dépassèrent l'affleurement qui marquait le croisement, un cavalier les attendait. Un Syrien, vêtu d'une armure d'étoffe crasseuse, sur un bidet fatigué. Un turban désordonné ornait son crâne et un sourire forcé barrait son visage. À sa hanche pendaient une longue épée et un petit bouclier. Il leva la main en guise de salut à leur approche et les interpella gaiement d'une voix rauque. Il était là certainement pour s'assurer que Sawirus et Mahran ne constituaient aucun danger pour les pèlerins. Les deux hommes stoppèrent leur monture et le valet en profita pour attraper la gourde qu'il avait trop solidement fixée derrière lui pour la prendre aisément pendant la marche.

Pendant ce temps, Sawirus échangea quelques formules polies avec le soldat. Bientôt la cohorte de marcheurs arriva, encadrée par ses bergers en armes. En queue venait une file de mules lourdement chargées de balles, de tonneaux et de filets. Un des chevaliers du Temple s'avança au trot, d'un air décontracté. Sawirus nota qu'il avait le bras gauche qui s'arrêtait au niveau du coude. Et, à son approche, il dévoila un visage dont les chairs avaient été violemment arrachées. Le nez ne se devinait plus que par les orifices déchirés d'où sortait une respiration sifflante et la bouche elle-même s'ouvrait en une crevasse absurde sur des moignons de chicots. Le regard par contre était aussi ferme et vif que celui d'un maître et la voix profonde portait par-dessus le fracas des pieds, des sabots et des caillasses roulées.

« Faites-vous route au sud, amis ? » lança-t-il alors même qu'il se trouvait encore à bonne distance.

Sawirus hocha la tête avec emphase et attendit que le chevalier soit plus prêt pour répondre complètement, indiquant qu'il allait à Césarée mais comptait faire halte à Chastel-Détroit. À ce nom, le templier afficha ce qui devait lui servir de sourire.

« Fort bien, nous y faisons route de même. Auriez-vous désir de vous joindre à nous ? Nous cheminons fort lentement, mais il est toujours bon de le faire de concert.

--- Je ne suis guère empressé. Il y a bon temps pour arriver à Césaire, sans forcer le pas. Et aller en bonne compagnie raccourcit les voyages. »

Sawirus s'efforçait de ne pas examiner les blessures de l'homme, cherchant à attacher son regard ailleurs tandis qu'il lui parlait.

« À la bonne heure ! Ces pérégrins parlent fort peu ma langue, et j'ai toujours plaisir à deviser, en selle. Sur ces côtes, guère plus d'occupation à espérer.

--- Je suis bien aise de ne pas avoir autre souci que celui de tenir en selle et d'espérer arriver sans endurer pluie ou mauvais temps. »

La remarque arracha un grognement qui devait être de joie.

« Je dois avouer que je m'ennuie un peu, maintenant qu'on m'a mandé en ces chemins. Je chemine depuis Acre jusqu'à Jaffa cette fois, plus souventes fois au travers de la plaine d'Esdrélon. »

Il soupira, levant son moignon.

« Mais bon, que puis-je être de plus qu'une nourrice en ces lieux abrités ?

--- Rien que votre présence doit affermir cœurs et esprits craintifs.

--- De certes, si on devait compter la vaillance en plaies et bosses, j'en aurais bons coffres emplis. »

Malgré sa laideur et son aspect brusque, le templier se révéla un agréable compagnon. Le chemin se déroula rapidement tandis qu'ils devisaient de banalités, de généralités sans importance. Le chevalier était fils cadet d'un nobliau antiochéen, d'ascendance nordique disait-il, farouche hériter des féroces païens qui terrifiaient jusqu'au grand Charles lui-même. Mais la guerre était terminée pour lui, ses blessures ne lui permettant plus de pointer la lance comme il seyait à un chevalier. Il finissait de blanchir sous le harnois, attendant de n'être plus bon qu'à cocher les inventaires sur des tablettes de cire quand son cul ne supporterait plus la rigueur de la vie en selle.

Il n'avait pourtant pas plus de trente printemps, estimait Sawirus.

### Abords de Castel-Fenouil, midi du jeudi 14 mars 1157

La pause annoncée, certains marcheurs s'étaient rapidement égaillés parmi les buissons marécageux, mais quelques-uns étaient demeurés près du groupe, aux alentours d'un bosquet entouré de palmiers aux troncs moussus. Une courte berge vaseuse disparaissait dans des flots croupis mangés de roseaux et d'algues aux relents de pourriture.

Dans un des méandres peu éloignés, un couple de grèbes pataugeait, plongeant à la recherche de nourriture. Le ciel bleu cendré accueillait de longues lanières grises pelucheuses qui occultaient le soleil en alternance. Une bise légère, venue du large, mordait les joues, les nez et faisait craquer les massettes et phragmites sans parvenir à rider les eaux stagnantes.

Sawirus profitait de cette pause pour ramasser quelques fleurs qu'il porterait à son épouse. Malgré les années et leur fréquent éloignement, il conservait beaucoup d'affection pour elle et ne manquait que rarement l'occasion de le lui manifester. Il avait déjà une belle moisson d'asphodèles et d'anémones quand un cri résonna, tandis qu'un couple de fauvettes piaillant passa près de lui.

Une des pèlerines arrivait d'un buisson, la robe encore en partie relevée, le voile en bataille et les bras agités comme des moulins à vent. En un instant, l'effervescence gagna le groupe et affola les montures. Le chevalier du Temple dégaina son épée avant d'aller jeter un œil dans la roselière d'où la femme avait jailli. Il revint rapidement, circonspect, et intima l'ordre à ses hommes de prendre leurs lances et de le suivre.

« Que se passe-t-il donc, frère ? s'inquiéta Sawirus.

--- Un dragon était caché parmi les herbes hautes. Il est embusqué, mieux vaut l'occire avant qu'il ne nous attaque. »

Sawirus ne put retenir un sourire amusé. Il traversait les marécages si souvent qu'il ne s'inquiétait plus guère des crocodiles, surtout en cette saison.

« Vous faites escorte pérégrine pour la première fois en ces lieux, frère Armand, m'avez-vous confié.

--- Si fait, confirma le soldat, tout en délaçant une botte de javelines d'un roncin.

--- Alors, ne vous embarrassez pas de cette bestie. Elle ne nous causera nul dommage.

--- Je sais bien qu'il ne s'agit nullement d'un démon ailé crachant le feu. Mais j'ai ouï dire que ces dragons étaient féroces et pouvaient tuer d'un coup de queue ou accrocher un homme pour le noyer. »

Sawirus s'approcha, posant une main sur le sac de toile qui contenait les armes.

« Il ne fait guère chaud en cette saison, c'est pourquoi la pauvresse est tombée sur ce crocodile en dehors de l'eau. Il aime à paresser sous le soleil, fort maigre en ces jours. Durant l'hiver et ses frimas, il n'a guère d'appétit. Il est dangereux au fort de l'été, quand il se cèle par les eaux et attend qu'un imprudent s'approche de la berge. »

Le chevalier se gratta ce qui lui tenait lieu de menton et éructa avant de s'esclaffer et d'abandonner le délaçage.

« J'aime autant, je n'ai guère envie de chasser en étant seul à savoir tenir lance et épée ! J'aurais néanmoins été curieux de savoir si sa peau vaut acier comme on le dit.

--- Quelques-uns le chassent, depuis ma cité. Mais il ne s'en voit plus tant, et ils savent éviter les hommes. Ou alors n'en laissent aucune trace quand ils en croisent un. »

La remarque amusa le templier, qui se fendit d'une grimace éloquente, quoiqu'effrayante sur son visage fracassé. Sawirus se pencha pour arracher un asphodèle et ajouta :

« J'ai des miens amis qui ont œuvré avec le moulin que nous n'allons guère tarder à croiser. Je ne crois pas qu'aucun n'ait eu des ennuis avec ces serpents. Il suffit d'y prêter attention. »

Le chevalier claqua amicalement le cheval d'un geste automatique et retourna vers ses hommes, afin d'annuler ses ordres. La rencontre inopinée avait malgré tout affolé les marcheurs de Dieu, et ils attendaient tous de reprendre la route au plus vite, parlant dans leur dialecte tout en s'agitant. Un sifflement sec plus tard, les cavaliers étaient de nouveau en selle, et le prêtre dépenaillé qui encadrait les pèlerins levait son bourdon pour indiquer le départ.

Quelques enfants piaillèrent, tendant les bras dans l'espoir d'être portés. Des gorges fatiguées toussèrent en fluxions glaireuses, et des râles accompagnèrent les premières foulées, vite remplacés par la frappe régulière des bâtons, et bientôt couverts par le chant des Psaumes.

Une fois remonté, Sawirus tira sur son nez un des rafrafs de son turban, puis bâilla. Il était assommé, las des voyages, et trouvait la selle chaque fois plus inconfortable. Il repensa machinalement aux deux Yazid : le premier, son fils qui n'avait eu l'occasion de lui succéder, regretté chaque jour sur cette terre. Il aurait pu assurer à Sawirus de quoi vivre sur ses vieux jours, lui permettre de profiter de son épouse, de sa demeure, sans plus avoir à errer par chemins et ports. Le second Yazid, qu'il avait recueilli, il allait le revoir bientôt. Il l'avait confié à l'école cathédrale de Césarée pour lui inculquer quelques notions d'écriture et de comput[^84]. Le garçon s'était étrangement bien intégré et se rendait avec enthousiasme à ses leçons.

Il se passionnait avec tellement d'entrain au cursus, au *quadrivium*[^85], que Sawirus avait cru un temps qu'il y avait anguille sous roche. Il connaissait suffisamment le caractère facétieux du jeune homme pour craindre qu'il passe ses journées à rimailler des chansons paillardes plus qu'à étudier. Mais les chanoines lui avaient confirmé qu'il était studieux et appliqué. Il avait redouté un instant qu'il ne soit tenté par une vie errante de jongleur, mais peut-être avait-il seulement besoin d'abreuver son âme. Ou alors, il avait compris tout l'intérêt qu'il y avait à devenir clerc. Sans y prendre garde, Sawirus se mit à fredonner :

> « Las, le père ne sut que faire\
> Fils à la tonsure te confère[^86]... » ❧

### Notes

Malgré la situation de guerre endémique, de nombreuses personnes arpentaient les routes et chemins du Moyen-Orient. Marchands et pèlerins, essentiellement, allaient et venaient, parfois au moment où des combats se déroulaient à quelques dizaines de kilomètres de là. Il existe des relations de voyages qui ne font aucune allusion aux affrontements alors même que l'auteur s'était trouvé dans des zones de conflit. Il semble que, comme la météo et les attaques de bêtes sauvages, la présence de troupes ennemies soit intégrée parmi les maux possibles que le voyageur pouvait rencontrer. Il était donc normal de chercher à s'en prémunir et d'y porter attention, mais sans que cela n'empêche de se déplacer. Des commerces de location de montures et d'animaux de bât existaient d'ailleurs, et les textes de loi comportaient des rubriques pour gérer la blessure ou la disparition de l'animal loué.

En ce qui concerne la faune rencontrée lors de ces déplacements, le contact avec des créatures peu habituelles a permis en certains cas à l'imaginaire de prendre le pas. Mais même sans cela, on est souvent étonné de la méconnaissance à propos de bêtes pourtant répandues. De rares familiers, généralement ceux qui avaient à fréquenter au quotidien ou à éliminer les nuisibles, étaient plus au fait de la réalité. Les faibles connaissances zoologiques expliquent qu'un grand nombre d'approximations et d'erreurs grossières aient continué à être maintenues tout au long de la période, parfois en s'appuyant sur des aberrations remontant à l'Antiquité.

### Références

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. I & II.*, Cambridge : Cambridge University Press, 1993.

Mariño Ferro Xosé Ramón, Girard Christine, Grenet Gérard (trads.), *Symboles animaux, Un dictionnaire des représentations et croyances en Occident*, Paris : Desclée de Brouwer, 1996.

L'ombre des puissants
---------------------

### Damas, fin d'après-midi du youm al had 7 jumada al-thani 548[^87] {#damas-fin-daprès-midi-du-youm-al-had-7-jumada-al-thani-54887}

Abu Sa'id Husayn al-Sabi ibn Ahmad était un homme surprenant. On le disait adepte du luxe et enclin à céder à tous les appétits charnels. En cette fin de journée, il était revêtu d'un magnifique qaftan aux couleurs chaudes, dont les reflets moirés éclairaient son visage de teintes chatoyantes. Sur son crâne, un turban de soie claire, avec des motifs brodés de fils chamarrés laissait tomber de longs rafrafs[^88] dans son dos. Pas trop longs néanmoins pour ne pas heurter les censeurs les plus regardants. À ses doigts de nombreuses bagues scintillaient tandis qu'il levait son gobelet de verre peint pour avaler une gorgée d'eau citronnée. Mais ce qui était le plus fascinant chez lui, c'étaient ses yeux. Ils dévoraient son visage aigu de Persan, faisaient oublier les épais sourcils noirs, qui se rejoignaient au-dessus de son nez fin. Ils oblitéraient sa bouche sensuelle, sa barbe brune soigneusement taillée et d'où saillaient deux pommettes olivâtres.

On disait qu'avoir l'iris bleu était signe du démon, mais dans le cas d'Abu Sa'id, ils étaient tellement clairs qu'ils en devenaient hypnotiques. Cerclés d'une bande sombre, ils avaient la couleur de la glace, qu'on ne voyait jamais dans le désert syrien. Personne n'osait le fixer, de peur de perdre son âme dans ces fragments d'Enfer. On l'appelait d'ailleurs al Zarqa'[^89], jamais en face, mais sans qu'on sache si cela lui plaisait ou le fâchait. En tout cas, peu nombreux étaient ceux qui le confrontaient à ce surnom. Abu Malik était de ceux-là. Il avait rencontré le fonctionnaire à l'occasion de ses levées de fond pour libérer des prisonniers, un jour de prière dans la grande mosquée. C'était lorsque les Francs vinrent de toute l'Europe pour les attaquer, se contentant au final de ruiner les jardins de la Ghuta[^90].

La générosité de l'homme le surprit, car il avait entendu les pires choses sur lui. Cela l'incita à en apprendre plus. Et, au fil des ans, ils étaient devenus amis. Al-Zarqa' appartenait à l'administration municipale, rattaché à l'un ou l'autre diwan, au fil des changements de dirigeants. C'était un fonctionnaire compétent, tout le monde le reconnaissait. Il maniait les chiffres avec aisance et possédait une mémoire phénoménale. En outre, son écriture était souple et déliée et il savait s'exprimer dans plusieurs idiomes presque couramment. Son père, et son grand-oncle avant lui, étaient de grands voyageurs et des commerçants avisés qui avaient fait fortune dans le négoce avec les Indes et Bagdad. Certains disaient qu'une esclave capturée Outremer avait porté le malheur dans sa famille, avec ses yeux bleus. Il jouissait pourtant d'une belle rente et d'une bonne situation sociale. À cela s'ajoutaient une culture solide et un esprit acéré, adepte d'un humour souvent ironique.

Abu Malik revenait d'un après-midi dans la mosquée de son quartier, où l'imam et lui avaient établi un inventaire des fonds disponibles pour aider à la libération des captifs d'Ascalon dont on venait d'apprendre la chute aux mains des Francs. Bien qu'il ne soit guère regardant sur ce point, Abu Malik était gêné par le fait que c'étaient des Égyptiens qui avaient été capturés, car cela signifiait pour beaucoup que c'étaient des shiites. La générosité était alors moins prononcée. Pourtant de nombreux croyants ascalonites[^91] partageaient leur tradition sunnite, ce n'étaient que les dirigeants qui étaient en majorité écrasante d'une autre obédience.

Devoir ainsi batailler sans cesse pour semblables faux semblants était parfois épuisant, d'autant plus lorsque ses propres affaires tournaient au ralenti. Le réconfort d'un familier était donc le bienvenu, et il savait al Zarqa' assez proche de ses convictions pour s'épancher et se renseigner sur les événements sans craindre pour sa réputation ou son avenir. Malgré ses yeux clairs et son visage persan, al Zarqa' était avant tout *dimashqi*, un véritable Damascène. Et lui aussi s'inquiétait de ce qui se passait. Il brossait la situation en grimaçant, évoquant les échanges avec les autres fonctionnaires qu'il fréquentait chaque jour.

« Beaucoup craignent de voir notre cité forcée par les infidèles. Et la nouvelle de la perte d'Asqalan étreint les cœurs.

--- J'aurais grande surprise à les voir sous nos murs. Mugir ad-Din sait comment traiter avec eux.

--- Je l'ose croire, tu es homme de sapience sur ce sujet. Pourtant je ne suis pas certain que l'exil de Mu'ayyad soit une bonne chose, ni la nomination de son frère au vizirat. »

Abu Malik opina, s'empara d'un abricot à la peau dorée qu'il croqua avec gourmandise.

« L'espoir peut venir de Ba'albak, l'émir 'Ata...

--- L'eunuque est fort apprécié du peuple, mais je ne sais s'il saura garder notre cité indépendante et prospère. Nūr ad-Dīn a beau multiplier les cadeaux et les marques de respect dans ses lettres, il nous affame, quoi qu'en pensent les gens.

--- Tu ne l'aimes guère, ce Turc, on dirait bien » s'amusa Abu Malik.

Lui non plus n'avait guère d'amitié pour les guerriers nomades qui détenaient le pouvoir depuis des dizaines d'années en Syrie, au gré des changements de dynastie. C'étaient des hommes de sang et d'acier, au contraire des Syriens, qui aimaient le négoce, le raffinement et la paix.

« Tu sais l'adage : *dis du bien de tes amis, et de tes ennemis, ne dis rien !* répondit dans un demi-sourire le fonctionnaire. Ce que je vois, c'est qu'il se murmure partout que le prix du grain augmente parce que le prince a vendu les récoltes aux Ifranjs. C'est là un mensonge honteux. L'appétit insatiable de l'émir a bien failli nous faire basculer dans le giron des infidèles, si ce n'était le sang froid de certains. Verser un tribut, d'accord, mais il ne s'agit pas de s'inféoder. »

Abu Malik était d'accord. Cela faisait des décennies, peut-être des siècles, aussi loin que la mémoire de ses proches pouvait remonter, que les Damascènes naviguaient à vue entre les potentats locaux, les dynasties guerrières, les ambitions princières. Il était bien révolu le temps où battait là le cœur de l'empire musulman, l'âme de la religion. Mais la cité demeurait farouchement attachée à son indépendance, à sa prospérité et à son histoire. Et ses notables refusaient de plier le genou si même les formes n'étaient pas respectées. Ils avaient leur fierté, et la plus indiscutable des réalités ne suffisait pas à les y faire renoncer.

### Damas, midi du youm al sebt 23 ramadan 548[^92] {#damas-midi-du-youm-al-sebt-23-ramadan-54892}

Abu Malik se dépouilla de son épaisse jubba de laine colorée en pénétrant dans le couloir de sa demeure. Une banquette spécialement disposée à droite lui permettait de se défaire de ses bottes pour enfiler des babouches rangées parmi les nombreuses paires de savates destinées aux visiteurs. Il était engoncé dans plusieurs couches de vêtements chauds, l'hiver étant particulièrement rigoureux cette année. La veille, un crachin frigorifiant l'avait surpris alors qu'il faisait des emplettes pour la soirée. En cette période traditionnelle de fêtes, il avait été étonné de la morosité de l'ambiance, de la figure austère que lui présentaient les commerçants. Même le souk des marchands de bric-à-brac, habituellement fourmillant d'activité, de chalands curieux, de vendeurs criards, d'objets amusants, semblait désert.

Il avança jusque dans la minuscule pièce à côté de l'entrée où Ali, son principal domestique, se tenait généralement. Il était en effet en train de trier et plier une pile de linge de maison, avec l'aide de la petite Shaima. Il leur signala qu'une livraison de ses commandes ne devrait pas tarder puis, comme il allait pour partir, le vieux valet lui indiqua qu'un visiteur, son cousin Idris ibn Hamzah, l'attendait dans le majlis[^93]. Il acquiesça et se rendit d'un pas allègre dans la pièce.

Idris était en fait de la famille de son épouse. Le jeune homme commerçait avec le nord essentiellement, achetant des troupeaux pour les acheminer jusqu'à Damas où il les revendait comme animaux de boucherie. C'était un garçon plein d'entrain, charmeur et irrévérencieux. Il ne fut donc nullement surpris de le découvrir en train de grignoter quelques dattes alors que le crépuscule était encore bien loin. Idris accueillit nonchalamment Abu Malik d'un sourire, sans se lever pour son aîné. Il est vrai que les deux familiers se fréquentaient beaucoup, mais Abu Malik était toujours contrarié de voir qu'Idris en prenait parfois vraiment trop à son aise avec la politesse.

Un jour, il s'était même présenté en son absence, lui avait indiqué Ali, pour saluer sa cousine en l'absence de tout homme dans la maison. Il s'était presque imposé auprès des femmes, des amies de Fazila, qui n'avaient guère apprécié d'être ainsi exposées à un inconnu. De sévères remontrances l'avaient dissuadé de recommencer, mais il s'affranchissait régulièrement des conventions et faisait souvent la honte de ses parents. La demeure d'Abu Malik était finalement la seule où il savait pouvoir être reçu chaque fois que l'envie lui en prenait, et il aimait discuter avec son cousin de choses et d'autres. Tous deux étaient adeptes de musique, et il arrivait qu'ils louent des interprètes pour la veillée. Idris était encore jeune, sans épouse, mais ne semblait guère pressé d'accumuler du bien pour établir sa famille. L'argent coulait à flot de ses mains et il passait tout en babioles pour ses maîtresses, en tenues luxueuses, en plats extravagants ou en soirées licencieuses.

Abu Malik s'installa face à lui, sur un matelas, calé contre des coussins. La pièce était réchauffée par deux braseros de céramique représentant des éléphants, qui répandaient également de douces senteurs. Les tapis suspendus, les étoffes qui fermaient les portes, faisaient de l'endroit un cadre douillet. Idris avait posé devant lui, sur un lutrin, un petit ouvrage de poésie qu'il feuilletait à l'arrivée de son cousin. Il se redressa, s'appuyant contre le mur derrière lui.

« Alors, comment te portes-tu, mon ami ? Voilà bien des semaines que je ne t'ai vu ! accueillit-il Abu Malik.

--- Je m'enjoie de te recevoir une nouvelle fois. Ta cousine et moi avions grande peur pour toi. »

Idris balaya la remarque d'un geste nonchalant de la main.

« Les Turcs ne m'ont rien fait, il est juste impossible de commercer à son aise. Ils prennent tout, à des prix dérisoires. Et gare à celui qui refuse leurs avances. Il se trouve bien vite raccourci d'une tête.

--- Le principal est que tu sois rentré pour achever les fêtes avec nous.

--- De quelles fêtes parles-tu ? s'indigna faussement l'invité. On dirait que la ville n'est plus emplie que de vieillards fébriles et de pauvres sans le sou. Comment fêter joyeusement ramadan, quand chacun ne fait que se lamenter ?

--- Tu ne te prives pas de briser le jeûne, toi... Laisse chacun faire selon son cœur. »

Idris regarda la datte qu'il avait en main et sourit.

« Je rentre à peine de mon voyage et tu es le premier que je visite. On peut donc dire que je suis encore sur les routes, non ? »

Provocateur il goba la datte avant d'en cracher le noyau, rigolard.

« Oublie tes soucis et les soucis t'oublieront, comme dit le sage...

--- Tu as longtemps été absent, c'est pour ça. La ville n'est guère joyeuse ces temps-ci.

--- Tu m'étonnes, avec la face grisâtre de Haydara comme vizir pour nous servir le leben[^94], même le plus joyeux des sufis se pendrait en quelques jours.

--- Fais attention à ce que tu dis, mon ami. Ta langue, et même plus, pourraient se voir tranchés de tenir tels propos. »

Idris haussa les épaules, indifférent.

« Je ne fais que répéter ce que j'entends depuis que j'ai laissé ma monture à Bab al-Gabiya. Un vase ne répand jamais que ce qu'il contient.

--- Justement, s'il venait à succéder au prince Mugid, il saura se souvenir de ceux qui ont parlé en mal de lui. »

Le jeune homme écarquilla les yeux, figeant sa main qu'il avait lancée de nouveau vers le plat de dattes.

« C'en est à ce point là ? »

Abu Malik acquiesça en silence. Décidément, on ne se souviendrait pas de ramadan 548 comme d'un mois de fête.

### Damas, matinée du youm el itnine 10 safar 549[^95] {#damas-matinée-du-youm-el-itnine-10-safar-54995}

Abu Malik était dans la courette, à réconforter Fazila lorsque plusieurs coups résonnèrent dans le couloir d'entrée. Ali risqua un œil par le judas grillagé puis tira rapidement la barre, laissant le petit Hassan se couler discrètement dans la demeure. Le gamin approcha en courant, l'air affolé.

« Ils sont entrés, abu[^96], ils sont entrés ! »

Le marchand en ouvrit les yeux de terreur, se passant la main sur le visage. Son épouse lui caressa le bras, désemparée. Les autres membres de la maisonnée s'étaient approchés tandis que le gamin reprenait son souffle.

« Que sais-tu ? Qu'as-tu vu ? Parle !

--- Le levant de la cité a été forcé, abu. Ils ont ouvert les portes et personne ne s'est opposé à eux !

--- L'adhath, l'askar du prince ? Ils n'ont rien fait ?

--- Nulle part on n'en voit la trace, abu. La rue appartient aux Turcs. »

Abu Malik inspira profondément, cherchant la réaction appropriée. Il habitait au sud de la grande mosquée, certainement pas l'endroit où les soldats viendraient en premier. D'autant qu'ils n'étaient pas éloignés de la forteresse, où le prince Mugid était installé depuis le début du siège, une semaine plus tôt.

Invitant son épouse et ses filles d'un geste du bras, il pénétra dans le majlis et s'assit sur un des matelas, réfléchissant en silence. Devant lui, et aux abords de la pièce, tout le monde attendait. Sa femme cajolait les enfants doucement, les réconfortant de son mieux malgré sa propre peur. Abu Malik finit par tousser afin de s'éclaircir la voix et s'exprimer d'un ton clair.

« Tout d'abord, Hassan, tu vas te poster discrètement sur la terrasse qui permet de surveiller la rue des Aveugles. Si tu vois quoi que ce soit d'inquiétant, tu viens nous prévenir. »

Le gamin détala en un instant, le son de ses pieds nus claquant sur les marches de terre cuite des escaliers.

« Ensuite, Ali, tu vas apporter quelques affaires des entrepôts dans l'entrée, de façon à pouvoir complètement en bloquer l'ouverture si l'envie leur prenait de tenter de la forcer. Je ne pense pas qu'ils prêteront attention à la porte des dépôts, elle est trop épaisse et solide. »

Il se félicita intérieurement d'avoir toujours su garder une attitude modeste et de ne pas avoir décoré son entrée de peinture extravagante, de ferrures sophistiquées ou d'un heurtoir luxueux. Rien ne désignait sa porte comme celle d'une opulente maison. Les pilleurs allaient certainement s'en prendre en premier aux endroits où ils espéraient dénicher des richesses en grand nombre, sans pour autant devoir y mettre trop d'effort.

Il se tourna vers sa femme et lui proposa de demeurer dans leur chambre, avec les filles, et de les occuper à quelques jeux. Il n'y avait de toute façon pas grand-chose à faire. Tout ce qu'ils pouvaient faire, c'était attendre et prier. Ils avaient de quoi manger, Abu Malik ayant eu la prévoyance de faire des provisions dès l'instant où l'émir 'Ata' avait été décapité, victime des manigances de ses ennemis. Il avait craint depuis lors que la situation dégénère encore.

Un moment, il avait envisagé de se proposer comme émissaire auprès des Francs, chez qui il avait de nombreux amis, pour tenter de les convaincre de l'urgence de la crise : le maître d'Alep était en train de mûrir le fruit, et ne tarderait pas à le cueillir. Mais son épouse l'en avait dissuadé. Elle craignait de le voir partir en ces temps troublés, autant par peur de demeurer seule que de le savoir sur les routes.

Idris lui-même, l'infatigable voyageur n'avait plus donné signe de vie depuis son départ en shawwal[^97], au plus fort de l'hiver. Il avait expliqué que c'était pour lui un devoir que de chercher à acheminer des denrées à Damas alors même qu'on tentait de l'étouffer. Il ne voyait pas là le fait du prince Mugid qui aurait soi-disant vendu les récoltes aux Ifranjs, mais plutôt le résultat de la frousse des négociants qui n'osaient pas faire leur métier correctement, de peur de déplaire au Turc d'Alep. En outre, avait-il ajouté dans un grand sourire, c'était une bonne occasion de faire de copieux bénéfices. Lorsqu'il était venu faire ses adieux à son cousin, le jeune homme s'était amusé de son air sombre. Il avait disparu dans la rue après un signe de la main, criant sans se retourner « Ne t'afflige pas avant le malheur ! »

### Damas, soirée du youm al talata 8 rabi' al-thani 549[^98] {#damas-soirée-du-youm-al-talata-8-rabi-al-thani-54998}

La lumière des lampes à huile faisait danser les ombres des pièces du nard[^99]. De part et d'autre du plateau de jeu en bois marqueté, Abu Malik et al-Zarqa' réfléchissaient en avalant de temps à autre une gorgée d'amreddine[^100] fraîchement préparé. Les rumeurs d'une fête dans une maison voisine leur parvenaient, étouffées par la cour centrale, sur laquelle la porte avait été ouverte pour profiter de la douceur du soir. Depuis l'étage, les lumières traçaient en motifs de feu les délicates arabesques des moucharabiehs de la pièce où s'occupaient les épouses et les enfants.

Al-Zarqa' avait apporté un petit livret qu'il avait déniché quelque jours plus tôt dans une boutique près de darb ad-Daylam. C'était un recueil de vers de Tarafa, un poète d'avant le Prophète, très apprécié des connaisseurs en général et d'Abu Malik en particulier. Tandis qu'ils jouaient, le commerçant jetait de temps à autre un coup d'œil sur l'ouvrage, dont le style sobre et dépouillé convenait bien selon lui au propos tragique de l'œuvre. Tout en lâchant la pièce qu'il venait de pousser, al-Zarqa' évoqua les événements récents et demanda si Abu Malik avait entendu parler du retour de l'émir ibn Munqidh.

« Comment échapper à cette nouvelle ? s'amusa son ami. Il est arrivé encore plus pouilleux qu'un mendiant. Et plus furieux que jamais ! Il aurait été volé par les Ifranjs sur le chemin de l'Égypte.

--- Il a surtout fui Le Caire par peur d'y laisser sa tête. Les nouvelles là-bas sont plutôt mauvaises.

--- Ce n'est plus une ville, mais un nid de serpents. Le fils tue le père, le frère assassine le frère. »

Al-Zarqa' acquiesça en silence. La déliquescence du califat fatimide n'était un secret pour personne. Une révolution de palais succédait à l'autre régulièrement et seuls les plus violents et les plus ambitieux semblaient survivre. Humilié de son échec à s'emparer du pouvoir, Usamah ibn Munqidh avait dû rejoindre Damas en toute hâte, espérant que son nouveau maître, Nūr ad-Dīn, l'y accueillerait bien.

« Vous étiez bons amis, l'émir et toi, ce me semble ? Étonnant qu'il ne t'ait pas encore fait visite, glissa al-Zarqa' dans un sourire

--- Amis ? Je n'irais pas jusque là ! Nous avons certes fait visite aux rois Ifranjs maintes fois ensemble et une certaine estime nous lie. C'est juste que je le trouve... »

Abu Malik fronça les sourcils, les dés dans la main, tandis qu'il cherchait ses mots.

« ... Insaisissable. Tu ne sais jamais quel visage il t'offrira, du poète, du guerrier, du diplomate, du prince.

--- Il en est toujours ainsi des hommes d'un si haut rang. L'émir a offert la paix à notre cité qui en avait bien besoin. Peut-être accueillera-t-il le vieil homme et saura adroitement user de ses talents.

--- Je crains qu'une nouvelle période de guerre n'ensanglante nos territoires. Avec une telle puissance, l'émir aura certainement désir de rejeter les Ifranjs à la mer. »

Al-Zarqa' se racla la gorge, déplaça un pion et tendit les dés à son compagnon.

« Ce n'est pas ce qui se dit. C'est un vieil homme fatigué. Avec de vastes domaines à diriger. Il a tant à faire pour les maintenir sous sa coupe qu'il n'aura pas forcément désir de sortir l'épée.

--- Si Dieu le veut. Puisses-tu parler de raison.

--- J'ai entendu dire qu'il est surtout inquiet de ce qui se passe en Égypte. Comme notre cité, qui a été proche plusieurs fois de basculer dans le giron des infidèles, Le Caire est fragile, et le roi Badawil[^101] a désormais Asqalan d'où s'élancer vers le delta. »

Se rencognant dans ses coussins, Abu Malik regarda son ami d'un air étonné.

« Tu veux dire qu'il pense à s'allier aux shiites du Nil ?

--- Oh, certainement pas ! s'en amusa al-Zarqa'. Mais comme il a su cueillir le fruit du bilad al-sham, il moissonnerait bien le blé d'Égypte. »

N'auront-ils jamais assez de terres sous leur emprise et de richesses dans leurs coffres, pensa pour lui Abu Malik. Aucun prince n'était jamais satisfait de ce qu'il avait, et pleurait alors qu'il se gavait d'honneurs et de puissance. Tous se revendiquaient désormais du jihad pour justifier leurs entreprises guerrières, et parfois même leurs conquêtes sur leurs coreligionnaires.

Il se remémora son cousin Idris, l'iconoclaste, qui aimait à citer des dictons et des proverbes dont il n'appliquait que rarement la sagesse à lui-même. Il aurait dit en pareil cas :

« La guerre sainte la plus méritoire est celle qu'on fait à ses passions. »

Puis il aurait avalé en une gorgée une boisson alcoolisée, accompagnant son geste d'un clin d'œil. ❧

### Notes

Après le décès du prince Anur, qui avait résisté à la formidable épreuve de l'attaque franque lors de la seconde croisade sans pour autant tomber dans le giron zengîde, la ville de Damas sombra dans le chaos. Les décisions politiques devenaient de plus en plus critiques et, en l'absence d'un véritable meneur, la tendance oscillait entre le ralliement aux princes chrétiens et celui au puissant émir du nord, Nūr ad-Dīn.

Ce dernier avait pour lui l'avantage de la religion commune et, surtout, il sut placer adroitement ses pions pour s'attirer le soutien d'une large frange de la population. Lorsqu'il se présenta finalement sous les murs, il ne fallut qu'une semaine de combats sans vigueur pour qu'il s'empare de la cité grâce à une complicité interne et sans qu'une réelle opposition ne l'empêche alors de prendre le contrôle des lieux.

Il se résolut à négocier avec le prince Mugid pour obtenir la forteresse, mais cela se déroula sans heurts. Une fois le destin de Damas scellé, solidement implanté dans le giron de Nūr ad-Dīn, il demeurait une inconnue majeure, qui pouvait faire basculer l'équilibre du Moyen-Orient. L'Égypte était dans un état de déliquescence tel que ses voisins commençaient à y voir un territoire à conquérir. Très rapidement, les dirigeants moyen-orientaux se mirent en marche, espérant prendre de vitesse leurs adversaires. Cet affrontement sera au centre des manœuvres politiques, diplomatiques et militaires de la décennie suivante.

### Références

Élisséef, Nikita, *La description de Damas d'Ibn 'Asâkir*, Institut Français de Damas, Damas : 1959.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

El-Shorbagy Abdel-moniem, « Traditionnal Islamic-Arab House : vocabulary and syntax », dans *Internationnal Journal of Civil & Environmental Engineering*, vol. 10, n°4, 2010.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Schmidt Jean-Jacques (trad.), *Les Mou'allaqât. Poésie pré-islamique*, Paris : Seghers, 1978.

Retraite
--------

### Château Emmaüs, fin de matinée du jeudi 9 mai 1157

Frère Raoul descendit de sa monture, un petit âne au tempérament placide, avec aussi peu de grâce qu'il y avait grimpé. Sa large bedaine et son manque d'activité physique rendaient la tâche bien ardue, faisant de l'exercice un spectacle ridicule. Ce faisant, il nota avec agacement l'hilarité mal dissimulée des deux valets qui l'avaient accompagné avec le train de mules et les deux chameaux. Dans le nuage de poussière soulevé par ses efforts, il remit un peu d'ordre dans sa tenue et ajusta sa ceinture par-dessus la coule.

Il n'aimait guère sortir de l'enceinte de l'hôpital de Jérusalem, et encore moins de la ville. Néanmoins, ses futures attributions l'obligeraient à voyager fréquemment, ce qu'il déplorait à grands soupirs à chaque fois qu'il y pensait. Peut-être avait-on estimé parmi ses supérieurs que l'ampleur de son estomac ne seyait guère à un serviteur de Dieu humble et parcimonieux. Quelques mois passés à se tanner les fesses sur le dos d'un bourricot, le long des chemins empoussiérés de Judée allaient vite mettre bon ordre à cela.

Il souleva son chapeau de paille et s'épongea le front, coiffa sa chevelure tonsurée d'une main moite. Ses bajoues luisaient de sueur grasse, coulant dans son cou irrité par la rude toile de ses vêtements. La chaleur l'achèverait bien si sa foutue monture ne le précipitait pas d'abord dans un ravin. Faisant craquer son dos contusionné, il dodelina péniblement vers l'entrée principale du caravansérail accolé à l'église.

Le bâtiment s'organisait autour d'une cour ceinturée de galeries couvertes. Il y apprécia la fraîcheur, non sans frissonner du soudain changement de température, mais grimaça en y voyant un imposant groupe de pèlerins discuter bruyamment. Dans une langue inconnue de lui, en plus. Grommelant, il les dépassa, jetant un œil habitué quoique distant aux silhouettes harassées, aux épaules voûtées.

Une petite porte ouvrait directement sur l'endroit où il pensait dénicher le cellérier responsable du lieu. La pièce exiguë n'abritait qu'un tabouret devant une modeste table à tréteaux, des étagères couvertes de tablettes et de rouleaux poussiéreux, ainsi qu'une lampe éteinte. Mais pas d'hospitalier en vue. Il continua à avancer vers les cuisines, où il savait trouver quelqu'un à toute heure.

Une odeur de brouet s'en échappait, rappelant à l'infortuné voyageur que son repas du matin était bien loin. Plissant les yeux à cause de l'obscurité relative, il manqua de peu de renverser un moine qui sortait du lieu, se contentant de le heurter de son ventre moelleux.

« Mon frère ! échappa l'autre, surpris.

--- La paix de Dieu, frère. Je suis Raoul, de Bethléem, et je m'en viens depuis le chef hospice, en la sainte cité.

--- J'ai nom Chrétien, céllérier de ce lieu. »

Tout en parlant, il l'invita à le suivre d'un geste de la main.

« Nous n'avons certes pas de salle capitulaire, mais notre réfectoire sera tout aussi bien. »

L'endroit était petit, chaulé de frais, avec un imposant crucifix de bois peint accroché au mur, qui semblait surveiller les tables sombres et les tomettes de terre cuite. Cela sentait l'humidité, mais la lumière issue des fenêtres jouait dans la poussière, apportant un peu de gaieté sous la morne figure du crucifié. Frère Chrétien tira un banc et s'assit tandis que Raoul s'affalait tel une baleine, expirant comme soufflet de forge. Il s'essuya une nouvelle fois le front pendant que son compagnon envoyait quérir du vin et du fromage. Puis il se tourna vers son visiteur.

«Alors, quelles sont les nouvelles en la Cité ?

--- Pèlerins et marcheurs s'en viennent, et nous ramassons toujours autant de miséreux. À croire qu'il s'en fabrique tout exprès pour nous chaque jour. »

Chrétien acquiesça, tout en accueillant le pichet, les verres et le plat. Il servit son frère en silence puis ajouta :

« Quelles nouvelles du nord ? De nos frères tombés ? »

Raoul secoua la tête ostensiblement, le visage renfrogné, déchirant un morceau de pain.

« Ce fut là tragédie comme seuls les Anciens en chantaient jusqu'à aujourd'hui. Tant de pertes ! Nous n'avions pas besoin de cela... »

Puis il se consacra à l'engloutissement exhaustif et minutieux de tout ce qui pouvait être comestible sur la table, sans plus rien ajouter. Il accordait néanmoins de temps à autre un regard méfiant au fils de Dieu accroché au mur, comme s'il craignait que ce dernier lui pique sa part. L'autre frère attendait, son verre à la main. Ce ne fut qu'après avoir avalé l'ultime bouchée, suivie d'un rot discret, que Raoul fit de nouveau entendre sa voix.

« J'ai pour tâche de vous demander de préparer les bois d'œuvre d'Aqua Bella. Nous allons les envoyer, comme beaucoup de choses, à Beit Gibelin. Vous n'en avez plus usage ici, de toute façon ?

--- Non, ils sont remisés ici depuis plusieurs semaines déjà.

--- C'est parfait, préparez-les pour un transport jusqu'à la Cité, nous ferons convoi d'ici peu.

--- Quels projets pour Beit Gibelin ?

--- Avec la prise d'Ascalon, nous devrions attirer colons en masse. Nous allons leur bâtir une église paroissiale pour inciter à cela. »

Frère Chrétien hocha la tête, enthousiaste, sans rien ajouter.

« Nous avons par mal heurt perdu moult provendes avec cette histoire de Panéas, alors je bats le rappel d'un peu partout.

--- Il demeure de quoi bien œuvrer, je n'ai guère de craintes en cela.

--- Puisse Dieu vous entendre, nous avons tant à faire. Et tellement à accueillir chaque jour !

--- Il est vrai que cette année voit grande foule de pérégrins. Je ne crois pas avoir jamais autant accueilli de marcheurs de Dieu. »

Raoul siffla la dernière goutte de son verre et regarda avec dépit dans la cruche vide, avant d'ajouter.

« Je vous mène d'ailleurs les quelques draps que vous avez mandés pour Aqua Bella.

--- Parfait, peu à peu, le cellier s'emplit. J'ai grande hâte de voir l'endroit accueillir nos premiers frères. »

Tentant d'avaler les traces d'humidité persistant dans son gobelet, Raoul hocha la tête. Il espérait d'ailleurs bien être un des premiers à bénéficier de l'hospitalité de l'endroit. N'avait-il pas l'âge de se reposer un peu ? Il n'était pas loin de ses quarante cinq ans après tout. Et s'il devait cheminer par les routes du royaume jour après jour, il ne faudrait guère plus de quelques semaines avant qu'il ne soit brisé de partout.

### Château Emmaüs, après-midi du samedi 11 mai 1157

Frère Chrétien ne remarqua pas l'hospitalier au sein de la foule des pèlerins qui sortaient de l'église. Les murmures de leurs conversations avaient éclipsé le chant des cigales et des grillons, et l'attroupement avançait bruyamment parmi les pins et les palmiers. Il les salua, de façon automatique, son panier à la main tout en se dirigeant vers l'accès au lieu de culte. Il avait cueilli quelques fleurs dans le jardin pour orner les autels de frais. Le sourire de son confrère le stoppa net, un peu confus.

« Je ne vous avais pas vu au parmi de la foule, frère.

--- Nulle offense ! J'ai profité de mon passage ici pour aller faire oraisons. »

Frère Chrétien sourit. Il était toujours touché par les manifestations de foi simple, surtout parmi ceux de son ordre, où on rencontrait aussi pas mal d'ambitieux et de carriéristes.

« Vous compaignez ces pérégrins ? s'enquit-il.

--- Non pas. Il se trouve juste que j'ai cheminé de concert un peu avec eux. Je m'en viens de Calanson, suite à vos demandes. »

Le cellérier écarquilla les yeux.

« Oh, mais en ce cas, vous êtes...

--- Frère Guillaume, oui. La paix sur vous, frère. »

Un large sourire déformant désormais ses traits, Chrétien attrapa la manche de son compagnon et partit en direction de la porte de l'hôpital, obstruée par les pèlerins. Il échangeait régulièrement des denrées avec le cellier de Calanson, propriété de l'ordre située dans la plaine, sur la route de la côte.

Ils se faufilèrent parmi les marcheurs et traversèrent la cour. Chrétien confia ses fleurs à un jeune valet pour qu'il s'occupe de changer les bouquets puis invita son confrère à le rejoindre dans la petite salle où il conservait ses documents. L'air était chaud, empli de la poussière soulevée, et aucun vent ne venait rafraichir les nuques échauffées. La douceur de l'ombre leur fit donc grand bien.

En s'asseyant, Guillaume nota le pupitre sur lequel plusieurs tablettes étaient posées, la corne à encre accueillant une poignée de plumes, les rouleaux de papier entassés. Tout cela lui rappelait sa propre petite cellule. Il sourit, dévoilant une dentition irrégulière qui cadrait assez bien avec son faciès rustique. Il n'était certainement pas fils de nobliau confié à l'institution pour y faire carrière.

« Je fais chemin pour le maître hospice, je me suis dit que ce serait bonne occasion pour vous encontrer. Et voir de mes yeux le castel qu'on réserve aux anciens.

--- Aqua Bella est quasiment fini, nous œuvrons à le meubler. Pour l'heure, je fais office de cellérier pour eux aussi, mais je ne doute pas qu'un frère sera spécialement désigné pour cela. »

Guillaume acquiesça en silence, le coude appuyé sur la table, le regard traînant ici et là.

« Vous faudra-t-il quelque chose de mes greniers ? Les récoltes sont assez bonnes et nous avons larges provendes.

--- Si vous avez tiges de fraiche moisson, je ne dis pas non. Il va falloir faire paillasses assez pour garnir les lits. D'ailleurs ceux-ci sont en train d'être bâtis en la sainte Cité.

--- Je pense que cela peut se faire. »

Frère Chrétien s'excusa, le temps d'aller régler un problème d'hébergement pour les arrivants. Il revint une cruche et deux gobelets à la main. Pendant ce temps, frère Guillaume s'était appuyé contre le mur et commençait à dodeliner de la tête.

« Auriez-vous désir de vous allonger avant vêpres, mon frère ?

--- Je ne dis pas non, j'ai fort veillé ces derniers jours, et les voyages en pleine chaleur sont toujours éreintants.

--- Des soucis en votre casal ? »

Guillaume soupira, attendant d'être servi en vin et d'en avoir avalé une gorgée pour répondre.

« Nous avons eu quelques troubles avec des soudards descendus de Panéas.

--- Des Turcs ? s'inquiéta soudain Chrétien.

--- Certes non. Des baptisés, comme vous et moi. Mais l'un d'eux, pris de boisson, s'est attaqué à une pucelle et l'a forcée. La pauvresse n'ayant personne pour la défendre à la cour du vicomte, il nous a fallu l'y représenter.

--- Le coquin a été condamné?

--- Oui et non. J'ai demandé, comme de juste, qu'il fournisse de quoi la faire nonette, qu'elle puisse laver cet affront sous le voile. Mais elle avait disparu entretemps...

--- La pauvrette.

--- L'homme a payé l'amende au vicomte et j'ai obtenu qu'on l'escouille, tout de même ! Mais il a fait appel à son baron, espérant grâce pour ses bourses. »

Chrétien renifla, décontenancé. Comment la justice pouvait-elle s'exercer si les condamnés pouvaient échapper aux sanctions par l'aide de leurs patrons ? Il serait néanmoins malaisé d'obtenir rémission de l'amende, les puissants ayant les doigts encore plus crochus qu'usuriers lombards. Mais payer quelques sous ne saurait effacer la faute, et l'enfant serait à jamais marquée du sceau de l'infamie, sans espoir de rédemption si elle fuyait le voile.

Il fut ramené à la réalité par le bruit du gobelet heurtant la table.

« Je ne suis que fils de vilain, et n'y entends guère à ces choses, mais cela me désole de voir que même notre ordre ne peut porter bon secours à une gamine.

--- Vous savez ce qu'elle est devenue ?

--- Aucune idée. Peut-être qu'un de ces soudards l'aura trucidée pour la celer dans un coin... Dans l'espoir que cela permettrait à son compère d'échapper au jugement. »

Chrétien se mordit la lèvre, ennuyé. Il était également trop souvent confronté au malheur, à la mort et la tristesse pour ne pas en être affecté.

« Les affaires de sang ne sont pas pour nous, mon frère. Laissons cela aux barons.

--- C'est quand même dommage d'accueillir le voyageur comme nous le faisons et d'en oublier la protection des miséreux sans feu ni lieu. »

Il se renfrogna, se frottant le nez de sa grosse paluche avec une vigueur telle qu'il semblait vouloir l'arracher de son visage. Son compagnon lui posa une main amicale sur le poignet.

« Nous pouvons tout de même prier pour elle, frère.

--- Ouais, prier, ça on sait faire. »

### Aqua Bella, matin du samedi 25 mai 1157

Les chaussures ornées d'éperons de frère Pons résonnaient sous les voûtes tandis qu'il appréciait d'un œil connaisseur le travail des maçons. La pièce était largement éclairée par les fenêtres au sud, et le soleil pénétrait à flot en cette belle journée. Il s'avança vers l'abside à l'est et en caressa les murs finement lissés. Puis, se grattant le cou, il se tourna vers le père Daniel qui était resté à l'entrée, admirant la salle dans toute son ampleur. Leurs voix résonnaient dans l'endroit vide.

« Voilà belle salle où prendre repos, mon père. Je n'avais pas fait visite depuis long temps, et je m'enjoie de voir si bel ouvrage.

--- Nos frères apprécieront de certes le confort du lieu. Malades ou blessés trouveront leur place ici.

--- Vous aurez quelque docte médecin pour veiller sur eux ?

--- Nous avons un frère fort versé en chirurgie qui sera des nôtres, et des médecins du chef hospice viendront faire visite chacun à leur tour. Ainsi nous bénéficierons de leur science à tous. Le temps d'en trouver un qui nous rejoigne. »

Le chevalier hochait la tête en assentiment, déambulant tout en devisant. Il avait vu sortir l'édifice de terre au fil des semaines, des mois, depuis le château voisin de Belmont dont il avait la charge. Il s'était tout d'abord emporté contre ce projet, qu'il jugeait préjudiciable à son domaine, mais finalement, il comprenait l'intérêt d'un tel lieu, où les frères malades, âgés ou blessés, pourraient être soignés tranquillement. Un bel endroit où finir ses jours, à l'occasion, auprès des eaux gazouillantes qui surgissaient des sources environnantes, parmi les chênes et les pistachiers.

« Nul doute qu'il y aura usage de l'endroit avec Norredin qui s'est remis en chasse...

--- De bien obscurs moments nous attendent, je le crois aussi.

--- Obscurs, je ne sais. Il est acertainé que nous ne pouvons pas laisser ce païen fouler nos droits ainsi. Le roi va sûrement lever le ban pour aller bouter hors ce Turc. »

Daniel opina en silence. Il n'était pas de ceux qui avaient vu avec enthousiasme le développement de la fonction militaire au sein de leur ordre. Il était prêtre, un de ceux qui se sentaient le plus poussés vers une vie contemplative, comme un simple moine. Il avait d'ailleurs un long moment été tenté de rejoindre les frères de Bernard de Clairvaux.

Puis on lui avait parlé du projet d'Aqua Bella, un lieu de retraite et de repos, où un prieur serait nécessaire. Il avait accepté avec joie, trop heureux de concilier ses appétits de retrait du siècle avec la possibilité de demeurer hospitalier. Mais c'était avant de faire connaissance avec frère Pons, le châtelain de Belmont, forteresse voisine, également propriété de l'ordre. Lui était un fils de banneret, cadet sans avenirs qui avait vu dans le développement du bras armé des frères de saint Jean la perspective de faire carrière, lance au poing, garantissant son âme et son renom de concert.

« Il faudra me confier un ou deux de vos sergents quand ils seront là, que je les prenne en main une poignée de jours... »

Voyant le visage étonné du prieur, frère Pons afficha un sourire sarcastique.

« Ne vous enfrissonnez pas, je ne saurais en faire des chevaliers en si peu de temps. C'est juste pour leur apprendre les signaux à connaître, vu que nous sommes en vue les uns des autres. Cela pourra servir.

--- Ne sommes-nous pas en sécurité ici, au cœur du royaume ?

--- La flèche vole par-dessus n'importe quelle muraille, mon père. C'est la prudence, plus que la force qui fait de nous de bons soldats de Dieu. »

Le clerc se renfrogna. Il s'estimait un soldat de Dieu lui aussi, mais combattait différemment. Et certainement pas en versant le sang. Ce n'était pas là un rôle décent pour qui espérait apporter son âme pure au jour au Jugement Dernier, il en était persuadé.

« Vous comptez accueillir quand les premiers frères ?

--- Dès que la consécration aura eu lieu..., soupira le prieur.

--- La peste du patriarche ! Il fait encore problème ?

--- Il use de la moindre miette de son pouvoir pour nous nuire. J'aurais vu bon signe d'avoir la messe solennelle au jour de la Pentecôte[^102], mais cela n'a pas été possible. Il n'y a donc désormais plus d'urgence, mais j'ai espoir avant la saint Jean d'été[^103]. »

Le soldat acquiesça en silence, laissant son regard admirer la pièce une dernière fois, avant qu'elle ne s'emplisse de toux, d'éructations diverses et d'odeurs de corps fatigués et abîmés.

« Puisse le Seigneur vous entendre. L'été se passera en selle, à pourchacier le païen, et nul doute que nous y gagnerons horions et plaies. »

En entendant cela, le prieur se signa, dans l'espoir que le Seigneur les épargnerait, à défaut de leur éviter les combats. ❧

### Notes

L'ordre de Saint-Jean de Jérusalem, appelé communément l'ordre de l'hôpital représentait une énorme institution, qui comprenait un vaste domaine s'étendant sur de nombreux territoires. La gestion s'en faisait de façon à assurer la mission d'accueil initialement prévue, à laquelle s'était ajoutée au fil des ans une tâche de protection des pèlerins. Pour aboutir à la création d'une branche armée, similaire à la milice du Christ, les Templiers.

Il faut se garder néanmoins de penser l'ensemble comme un monolithe, et des tensions existèrent tout au long de son existence entre les différentes tendances. On y rencontrait aussi bien des prêtres que des laïcs, des hommes de guerre comme de simples serviteurs manuels. On a heureusement conservé pas mal d'archives, grâce à la survie de l'ordre jusqu'à nos jours (désormais appelé Ordre de Malte), mais il demeure difficile de dépeindre exactement la vie au jour le jour dans les établissements qui composaient cette mosaïque.

Le long de la route de Jérusalem depuis la côte, Château Emmaüs (Abu Ghosh), Aqua Bella (Khirbat Iqbala) et Belmont (Suba) synthétisent bien en peu d'espace toutes ces tendances : accueil des pèlerins, forteresse pour protéger la zone, lieu possible de retraite (hypothèse proposée par Denys Pringle, que j'ai retenue). Pour ceux qui se rendent en Israël, les trois endroits sont encore visitables, les deux premiers en assez bon état, et au sein d'un parc naturel pour le second, où les sources sont mises en valeur. Le tout à quelques kilomètres du centre de Jérusalem.

### Références

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. I & II.*, Cambridge : Cambridge University Press, 1993.

Riley-Smith Jonathan, *The knights of St John in Jerusalem and Cyprus c. 1050-1310*, Londres : McMillan, 1967.

Legenda sanctorum
-----------------

### Jérusalem, veillée du vendredi 27 décembre 1157

Malgré les épaisses chausses de laine, Herbelot sentait le froid lui étreindre les pieds. Il tentait par moment de remuer les orteils de façon à faire circuler le sang, mais rien ne semblait réchauffer ses extrémités. Ses mains, enfouies dans les manches, avaient trouvé refuge sous ses aisselles et il n'émergeait de sa capuche que le bout de son nez pointu, aussi rouge que celui des clercs qui l'environnaient. La ville sainte était toujours plus froide que la côte, et il en faisait la triste expérience.

Depuis qu'ils étaient arrivés, quelques jours plus tôt, pour les célébrations de Noël, il ne faisait que grelotter et renifler. La vaste rotonde du Saint-Sépulcre lui semblait une vraie glacière, et la foule pressée dans ses murs n'avait pas suffi à en réchauffer les pierres. De plus, il n'y avait guère d'entrain dans les cérémonies, qui voyaient pour la première fois l'absence définitive du maître des lieux, le patriarche Foucher, dont la voix tonnait habituellement si vigoureusement contre les ennemis de l'Église.

Remplaçant ses sermons adroitement menés, ce n'étaient que chuchotis et murmures feutrés, alors que l'élection de son successeur occupait de nombreux esprits. Les frères de l'Hôpital, débarrassés de leur plus farouche adversaire, jubilaient sans trop le montrer et les clercs ambitieux en appelaient à l'arrière-ban de leurs soutiens pour s'installer dans la cathèdre. Le cloître des chanoines, que rejoignit Herbelot après vêpres, résonnait des confidences échangées et des sourires de connivence.

La corne de la lune accrochait des dentelles de brume dans un ciel étoilé. Encore un signe de froid, se lamenta en silence Herbelot. Il tapa des pieds et toussa, profitant de la liberté accordée au-dehors. Il avait grandi dans un monastère aux habitudes strictes et se conformait sans même y penser à la discipline la plus sévère. Pour lui, la règle de vie des chanoines du Saint-Sépulcre était bien souple et permissive, et lui semblait comme des vacances.

Il avait accompagné l'archevêque Pierre de Tyr, et en tant que fonctionnaire du prestigieux prélat, était hébergé dans le palais du patriarche. Mais il aimait assister aux offices des heures, qui avaient si longtemps rythmé son existence, enfant. Cela lui apportait une régularité qu'il appréciait fort pour le calme que cela insufflait dans son esprit.

Il quitta les frères dans le cloître pour rejoindre sa chambre par le dédale de couloirs et de courettes qui s'emboîtaient dans le labyrinthe des bâtiments canoniaux. Il partageait là une petite pièce à deux alcôves, avec un des suivants de l'évêque de Lydda, Gilbert. Joufflu et ventru comme seuls savent l'être ceux qui passent plus de temps à bâfrer qu'à s'activer, il était malgré tout un agréable compagnon, à la voix douce, au timbre juste, faite pour les hymnes les plus difficiles. Il était animé d'une passion sans borne pour tout ce qui avait trait à la musique, et espérait bien un jour devenir chantre de sa congrégation.

En attendant, il se contentait d'aider comme clerc de second rang, prenant des notes, classant les documents et portant les messages. Il ne s'en offusquait pas et fredonnait gaiement sans cesse du matin au soir, ponctuant la moindre tâche de strophes légères. Chaque soir, Herbelot profitait de quelques interprétations, accompagnées du psaltérion ou de clochettes, qui lui ravissaient le cœur. Il avait sympathisé avec le jeune homme, heureux de trouver une oreille compatissante à son sort.

Lui aussi était un oblat[^104], originaire d'Allemagne. Son vrai nom était Geizbart, et il était orphelin, recueilli par une généreuse patronnesse qui l'avait doté pour le monastère. Il parlait régulièrement d'elle, dame Ottilie, comme d'une sainte. Qu'il n'avait jamais eu l'occasion de rencontrer, mais pour qui il priait souvent, comme nombre d'enfants qu'elle avait ainsi sauvés. Il espérait aider au salut de son âme, convaincu que les dévotions à son intention porteraient leurs fruits.

Herbelot trouva le jeune clerc assis, occupé à accorder les cordes de son psaltérion à la lueur des lampes.

« La bonne nuit, frère, lui lança-t-il en entrant, tout sourire.

--- La paix de Dieu, mon frère, répondit Gilbert, sans quitter son ouvrage des yeux.

--- Vous avez souci de votre instrument ?

--- Non pas. C'est juste l'humidité et le froid qui le font sonner de bien male façon. J'ai grande hâte de retrouver mon douillet logis à Lydda.

--- Je n'aurais pas cru dire un jour regretter de célébrer la naissance du Christ notre Sauveur auprès de son tombel. Mais voilà bien tristes cérémonies. »

Gilbert opina sans un mot, tout à son ouvrage, laissant le silence s'installer un moment avant de reprendre.

« J'ai ouï dire que de nouveaux courriers vont être faits, pour inciter barons et souverains à venir sous le signe de la Croix.

--- Sans le père Bernard pour les sermonner, je ne sais si beaucoup auront le courage de venir.

--- Gardez espoir, mon frère. Le puissant comte Thierry ne bataille-t-il pas en ce moment même dans les fiefs de Norredin ? Il doit bien y en avoir d'autres comme lui. Il faut seulement les rappeler à leurs devoirs. »

Herbelot soupira, moins enthousiaste que son compagnon. Le comte de Flandre était le frère par alliance du roi Baudoin, et un croisé convaincu, un homme de foi autant que de guerre. Les dirigeants d'Europe ne se souciaient plus guère des territoires latins de Terre sainte. Il lui semblait que la flamme qui avait animé leurs prédécesseurs avait fini par s'éteindre après avoir longtemps vacillé, inlassablement soutenue par le père abbé de Clairvaux, Bernard.

Il s'installa sous les couvertures et souhaita une bonne nuit à son compagnon de chambrée. Avant de s'allonger, il récita plusieurs *Ave Maria*, persuadé que la mère de Dieu était la plus à même de venir en aide à ceux dans le besoin.

### Jérusalem, après-midi du samedi 28 décembre 1157

« *Fuit homo missus a Deo, cui nomen erat Joannes.* »

Herbelot avalait sans trop y penser son repas, en silence, comme de coutume. On n'entendait que des raclements de gorge discrets, le frottement des pas des valets qui servaient les plats, les reniflements des malades. Malgré la relative souplesse des règles, il appréciait qu'ici aussi on mange en commun, sans parler, tandis qu'un frère lisait un texte sacré, ou règlementaire. Tout ce qui était propice à l'introspection et l'étude plaisait à son esprit méthodique et routinier.

« *Erat lux vera, quæ illuminat omnem hominem venientem in hunc mundum.* »

Les lentilles étaient agrémentées d'épices au goût douceâtre et leur couleur dénonçait l'ajout de safran, mais au moins n'y avait-il pas de viande ou de garniture trop riche. Le vin était également de qualité supérieure, et la croûte du pain frais craquait sous la dent. Malgré son éducation et toutes ses préventions, le jeune clerc appréciait fort la bonne chère, dernier plaisir auquel les moines s'adonnaient sans trop de retenue. Leurs vastes domaines et leur richesse permettaient de doter grassement les celliers, et la dérive était par trop fréquente. Un peu de tempérance n'était néanmoins pas malvenu selon Herbelot, pour qui les joies de la table constituaient un péché coupable qu'il était bien souvent obligé d'avouer à confesse. Ce n'était certes pas son seul défaut, mais celui auquel il succombait le plus aisément. En dehors peut-être de celui de la vanité, qu'il reconnaissait bien moins volontiers.

« *Hic erat quem dixi : Qui post me venturus est, ante me factus est : quia prior me erat.* »

Un plat de darnes de poisson, à l'ail, au persil et au citron fut déposé à son intention devant lui. Le fumet brulant lui réchauffait le visage autant qu'il satisfaisait ses narines. Il avala le contenu de l'écuelle avec un grand plaisir, autant pour le goût que pour la bienfaisante sensation de chaleur qui glissait en lui.

« *Et ego vidi : et testimonium perhibui quia hic est Filius Dei.* »

Une fois la compotée de fruits avalée, tandis que les valets débarrassaient le couvert, la file des chanoines se mit en marche vers le cloître. Le lecteur avait achevé le premier chapitre de l'Évangile selon saint Jean et s'employait à ranger l'imposant grimoire dans un placard d'où il ne ressortirait qu'au prochain repas. L'église était assez riche pour avoir plusieurs copies de tels précieux livres, sans devoir transporter les ouvrages d'un lieu à l'autre au gré des besoins.

Après manger, l'ambiance était en général assez détendue, et de nombreux clercs profitaient de ce moment de récréation pour discuter dans le cloître. Quand Herbelot arriva dans l'aile est, il entendit justement un petit groupe qui palabrait avec animation.

« Je ne crois pas qu'il puisse y avoir meilleur salut de l'âme que celui de prononcer ses vœux, mon frère, assénait un grand moine au nez tordu.

--- Mais tout le monde ne peut se tonsurer pour autant...

--- D'autant qu'il faut bien combattre les infidèles, ajouta un troisième.

--- Ils n'ont qu'à se faire clercs au soir de leur vie, nul besoin de se faire oblat.

--- Dieu approuve les combats contre les ennemis de sa Foi. Les saints ont permis la prise des cités où nous sommes ce jourd'hui.

--- C'est le relâchement des chrétiens désormais qui est puni par Le Très Haut. »

Aucun des chanoines présents ne remit en cause cette dernière assertion. Le clerc poussa son avantage.

« Nous voyons arriver certains saints inconnus de nous jusqu'alors mais leurs prouesses ravissent le cœur du juste. Ils sont comme les Macchabées les guerriers saints du Seigneur, mais affirment haut et clair que de la prière viendra le Salut, pas du combat l'épée en main.

--- Il faudrait un tel homme à la tête des royaumes, un moine qui saurait faire juste usage de la force. Seul l'oint du Seigneur sait tenir son cœur pur des appâts du siècle.

--- Le roi Godefroy l'avait bien compris, et tenait patriarche pour son sire.

--- Puisse Dieu nous envoyer de nouvel un tel roi ! » confirma le moine.

Bien que récemment arrivé en Terre sainte, Herbelot partageait l'analyse de ses compagnons tonsurés. Depuis la perte du comté d'Édesse, le royaume latin semblait malmené, en passe de disparaître régulièrement sous les assauts des infidèles. Il en rejetait la faute sur les dirigeants, plus prompts à se disputer le moindre butin, le plus petit casal, que de porter la parole du Seigneur comme l'avaient fait leurs illustres prédécesseurs quelque cinquante ans plus tôt.

La puissance militaire ne suffirait pas si la Foi ne guidait pas leurs pas, la grande expédition de 1148 l'avait amplement démontré. Le roi Baudoin était encore jeune, et il y avait espoir qu'il se montre à la hauteur des attentes. Il était né ici, en Terre Sainte, et serait peut-être le souverain qui saurait rallumer la flamme qui éclairerait leur avenir. Herbelot pria pour que ses précepteurs lui aient instillé l'amour de Dieu plus que des choses de ce monde

### Tyr, après-midi du jeudi 9 janvier 1158

La pluie sonnait aux carreaux de verre du petit cabinet où l'archevêque aimait à se réfugier pour traiter les affaires administratives. Une épaisse couverture de laine anglaise sur les genoux, un brasero à ses côtés, il répondait à son courrier, approuvait des textes et chartes d'un signe de tête entre deux siestes. Quand le temps le permettait, il ouvrait les châssis et admirait l'horizon maritime, les vagues et les voiles y dansant. Il ne s'émouvait guère des cris et des bruits de la ville qui parvenaient ainsi jusqu'à sa demeure et s'amusait parfois des appels lancés dans les rues.

Aujourd'hui, il se contentait de caresser la fourrure épaisse du petit chien au poil gris qui lui chauffait les genoux. Le roquet, à l'ambition de molosse quand il était jeune, était au fil du temps devenu aveugle et avec sa vue sa méchanceté était partie. Il faisait désormais fête à qui le flattait et dormait partout où ses pas le conduisaient, toujours dans l'orbite du prélat. Herbelot, qui ne l'avait pas connu dans ses vertes années féroces, appréciait fort l'animal et se baladait parfois avec Hieronimus dans les bras lorsqu'il allait déambuler dans les jardins.

« Je me demande si je ne vais pas préparer un sermon spécial pour le dimanche de la Purification de la Vierge » déclara soudain le vieil archevêque.

Délaissant les documents qu'il relisait, Herbelot attrapa une tablette vierge et un stylet et s'apprêta à noter. Mais rien ne vint durant un long moment, jusqu'à ce que l'archevêque Pierre souriât avant de s'adresser au jeune clerc.

« Le Sire Christ lui-même a été présenté au Temple, n'est-ce pas là signe que toute puissance ne vient que de l'Église ? Qu'en dis-tu ?

--- Je voyais le moment plus propice à célébrer la mère de Dieu, comme chacun. Mais je n'ai pas votre sapience, père »

L'archevêque s'amusa de la répartie, toute de docilité de son subordonné. Il aimait bien Herbelot, dont le penchant pour la vie monastique l'agréait fort, bien qu'il le trouvât parfois trop timoré et bien trop obséquieux. Son enfance chez les bénédictins en avait fait un mouton, enclin à ne jamais chercher à lever la tête du troupeau et peu désireux de s'attirer l'ire du pasteur.

« Avec l'élection du nouveau patriarche, il me revient en tête toutes les bassesses que les hommes acceptent pour cueillir les fruits de leur ambition. Je crois qu'il est temps de rappeler que le premier devoir, surtout des puissants, est d'honorer Dieu en toute circonstance. Qu'il n'est nul pouvoir qui puisse refléter, ne serait-ce qu'imparfaitement, Sa Majesté. »

Tout en hochant la tête, Herbelot notait scrupuleusement, sachant que ces quelques remarques jetées apparemment sans ordre étaient souvent le fruit d'une longue réflexion et fourniraient les bases d'un texte plus solide.

« Je vois chaque jour des barons au cœur avide des attraits de la poussière du siècle, et sans un saint homme à leur côté, ils se perdent. Regarde le roi Louis de France, sans Suger pour lui insuffler sagesse et tempérance, il gaspille pire que le fils prodigue. »

Il se passa une main chenue sur son visage osseux, essayant d'en faire glisser la fatigue qui s'y lisait dans les plis crevassés.

« Des gens de cœur et de sapience, comme Frédéric[^105], pour lequel j'ai grande estime, se font engluer par la tourbe du siècle. Il faut certes avoir cœur de saint pour espérer régner ici-bas sans se voir souillé par ses propres désirs. »

Il marqua un temps, ajoutant à mi-voix pour lui-même, ou pour son confident qui comprit qu'il ne s'agissait pas d'une remarque à noter :

« C'est pour cela que j'ai toujours fui les honneurs, je sais ma chair trop faible. Puisse ceux qui les empoignent faits d'un or plus pur ! »

Il sortit un carré d'étoffe de sa manche et se moucha longuement, bruyamment, avant de reprendre son monologue.

« Nous n'appartenons à nulle terre, pas plus qu'aucune province n'est nôtre. Il nous faut apprendre à servir avant tout, car nous sommes tous Ses sujets. »

Il se tourna une fois de plus vers son clerc, un demi-sourire sur son visage décharné, une lueur amusée dans son regard brouillé et larmoyant. Herbelot se chercha une contenance dans les notes qu'il suivait des yeux sur la cire, puis échappa doucement :

« Les clercs, les prêtres apprennent à servir, ainsi que je l'ai fait en moustier, à la Charité des saints Pères.

--- Tu parles de juste, comme souventes fois, mon jeune ami. Un souverain devrait avant tout être le prêtre de son royaume. »

### Tyr, veillée du mardi 25 février 1158

Herbelot parlait d'une voix douce tandis qu'il lisait, le doigt sous la ligne, le texte du manuscrit. Un de ses privilèges, en tant que clerc de l'archevêque de Tyr, était de pouvoir accéder à tout moment à la bibliothèque du prélat, dont il avait les clefs. Il pouvait détacher des meubles les recueils de prix et ouvrir les armoires où étaient resserrés les plus précieux. Il ne s'y trouvait rien de licencieux, mais pas mal d'ouvrages décorés finement, importés à grands frais des scriptoriums les plus prestigieux. Le volume que parcourait Herbelot était d'ailleurs orné de magnifiques lettrines de couleur ornées de feuillages aux complexes volutes, couchées sur un vélin doux au toucher.

Il acheva sa lecture, empreint de sérieux.

« *Gratia Domini nostri Jesu Christi, et caritas Dei, et communicatio Sancti Spiritus sit cum omnibus vobis. Amen*[^106].»

Il releva la tête, comme remâchant la leçon apprise de ce texte. Il aimait beaucoup saint Paul. Pas tant pour le contenu que par la richesse de ses épîtres. Comme l'abbé de Clairvaux, il avait entretenu une correspondance nombreuse et variée, désireux de répandre la profondeur de ses connaissances des mystères divins auprès de tous les fidèles en détresse. Il apportait la lumière à ceux qui erraient dans les ténèbres.

Il avala un peu du vin chaud qu'il s'était fait porter et s'assit confortablement sur la cathèdre emplie de coussin que l'archevêque affectionnait les rares fois où il venait. La faible lueur des deux lampes à huile tapissait de carmin et de safran les profils des ouvrages soigneusement empilés sur les étagères. La fenêtre, barrée d'un épais volet de bois, ne laissait pas deviner la noirceur de la nuit.

Herbelot se sentait bien, ici. Il aimait se recueillir sur les textes sacrés et philosophiques, ou feuilleter de temps à autre quelques volumes profanes, comme les extraits de la Chanson d'Alexandre qu'il appréciait fort. Il s'y narrait bien évidemment les parties qui racontaient la jeunesse du grand conquérant et le siège de Tyr, la ville même où se trouvait Herbelot. Plus il y repensait, plus il était persuadé qu'il était nécessaire de faire comme le grand saint Paul, qu'il fallait écrire des missives. Non pas pour quémander comme le faisaient la plupart des gens ici, mais pour conseiller, infléchir, orienter. Les rois, le basileus, avançaient tels des enfants dans les ténèbres, sans guide éveillé pour les mener, sans modèle à imiter. Ils suivaient leurs instincts, forcément vils et bas, mais ne savaient pas comment faire appel à toutes leurs ressources pour devenir des monarques défenseurs de la vérité divine. Il leur fallait quelqu'un qui leur écrive, qui leur indique comment faire, qui les inspire et les galvanise. Un grand souverain, qui serait à la fois un sage, un prêtre et un homme de foi. Il repensa à saint Jean. ❧

### Notes

Les citations en italique dans la seconde partie du texte sont extraites du premier chapitre de l'Évangile selon saint Jean.

> « *Fuit homo* *missus a Deo,* *cui nomen erat Joannes.* », verset 6\
> Il y eut un homme envoyé de Dieu, son nom était Jean.

> « *Erat lux vera, quæ illuminat omnem hominem venientem in hunc mundum.*», verset 9\
> Cette lumière était la véritable lumière, qui, en venant dans le monde, éclaire tout homme.

> « *Hic erat quem dixi : Qui post me venturus est, ante me factus est : quia prior me erat.* », fin du verset 15\
> C'est celui dont j'ai dit: Celui qui vient après moi m'a précédé, car il était avant moi.

> « *Et ego vidi : et testimonium perhibui quia hic est Filius Dei.* », verset 34\
> Et j'ai vu, et j'ai rendu témoignage qu'il est le Fils de Dieu.

Il est difficile de se représenter l'univers mental des oblats qui, comme Herbelot, scrutaient le monde à travers le prisme unique de leur éducation religieuse. Évoluant dans un contexte social relativement préservé, où ils ne connaissaient que peu les aléas de la vie du commun, il leur semblait naturel de plaquer sur le moindre fait la grille de lecture catholique et savante dont ils étaient imprégnés.

Cela ne signifiait pas pour autant qu'ils n'étaient pas capables de penser par eux-mêmes, la richesse intellectuelle du XIIe siècle est là pour le prouver . Par ailleurs, cela créait une universalité culturelle qui transcendait les origines géographiques, permettant de se sentir comme chez soi dans la plus petite communauté religieuse, pourvu qu'elle respectât au minimum les règles générales.

Le titre de ce texte vient du début du titre originel de la *Légende dorée*, ouvrage du XIIIe siècle rédigé par Jacques de Voragine, qui présente la vie de nombreux saints : « Ce qui doit être lu des saints... »

### Références

Voragine, Jacques de, Roze Abbé J.-B. M., *La légende dorée*, Paris : Édouard Rouveyre éditeur, 1902. Une version en ligne existe : <http://www.abbaye-saint-benoit.ch/voragine/index.htm> \[consulté le 9 avril 2015\].

Le Goff Jacques, Rémond René, *Histoire de la France religieuse. Tome 1. Des dieux de la Gaule à la Papauté d'Avignon (des origines au XIVe siècle)*, Paris, éditions du Seuil, 1988.

Marcheurs de lumière
--------------------

### Cathédrale de Beauvais, après-midi du dimanche 3 juin 1156

La pierre froide frigorifiait la joue et l'oreille de Marcel, allongé sur les dalles, les bras en croix. À côté de lui, les vitraux traçaient des arabesques colorées sur le sol, tandis que les voix des chanoines et des fidèles montaient dans la nef. Il sentit des pas se rapprocher, devinant les souliers brodés de l'évêque Henri[^107] à la périphérie de son champ de vision.

« *Domine deus pater omnipotens cuius sempiterne divinitatis potentiam et inmense maiestatis dominationem*... »

L'encens se répandait sur lui tandis que l'oraison s'élançait vers les cieux. Ce matin, la messe de Pentecôte avait été longue, étendu qu'il était à l'entrée du chœur, mais le chanoine lui avait expliqué que c'était un moment très favorable à la remise du bourdon, de la besace et de la croix et qu'il serait bon d'y assister en humble pénitent. Puis d'attendre ainsi, en oraison, que l'office de célébration pour leur départ se déroule également, jusqu'au moment où on leur distribuerait leurs attributs. Que son voyage en serait d'autant plus sanctifié.

Pour l'instant, la dureté du dallage lui meurtrissait les genoux et il ne se sentait guère en mesure de laisser son esprit accompagner les prières. Il réprimait régulièrement l'envie d'éternuer et reniflait pour faire disparaître la goutte qui s'amassait lentement au bout de son nez.

« ...*peracto presentis uite cursu, suorum mercedem laborum recepturus feliciter ualeat*. »

Les mains de jeunes clercs l'aidèrent à se relever, à se mettre à genoux, la tête baissée. Il frissonna et se gratta rapidement le visage, tandis que l'évêque s'éloignait de lui dans un froufroutement de soie rouge. Lorsqu'il revint, un garçon présentait sur un coussin devant lui les croix qu'avec ses compagnons de route ils arboreraient sur leur poitrine tout au long du pèlerinage. Apposant ses doigts gantés et bagués, l'évêque susurra quelques mots, et un autre officiant s'approcha, tenant un ouvrage ouvert. La voix forte résonna de nouveau sous la haute voûte.

« ...*Benedic domine hanc crucem quam famulus tuus pro devotione sancti domini nostri Jesu Christi...* »

Profitant de ce que l'évêque passait entre les rangs des croyants qui s'apprêtaient à partir pour Jérusalem, Marcel se dandina un peu d'un genou sur l'autre. Ils étaient presque une vingtaine à recevoir croix, besace et bourdon ce jour, et la cérémonie n'en finissait pas. Il entendait le vague brouhaha issu de la nef cathédrale, où la foule de leurs proches amassés discutait sûrement de tout et de rien alors que les prêtres officiaient. Il se souvint des innombrables affaires qu'il avait traitées à l'abri de ces murs, tandis que les divins mystères se déroulaient.

Il y avait même échangé son premier baiser, encore enfant. Avec la jeune Laïs, désormais mariée à un négociant en vin parisien. Peut-être était-elle morte, d'ailleurs, depuis toutes ces années. La vision de son visage rond parsemé de son, du charmant petit nez retroussé, de ses boucles retenues par un ruban vermillon, tout lui revint soudain en mémoire. Il revoyait la poussière dansant dans la lumière tandis qu'il goûtait aux lèvres de son amie. La voix autoritaire de l'évêque l'arracha brutalement à ses rêveries lorsqu'on lui tendit une croix d'étoffe rouge.

« *Suscipe frater mi Marcellus victoriosissimum sancte crucis vexillum per quod secure possis omnium inimicorum tuorum maliciam superare...* »

Tandis que la distribution continuait, Marcel caressa de l'index la douce étoffe de fine laine teinte. Deux bandes croisées, qu'il allait coudre sur son vêtement, censées le garder de tous les malheurs, pour peu que sa Foi soit pure et sincère. À son retour, il la conserverait dans un coffret.

Il l'en sortirait à la veillée, en même temps que la palme rapportée de Jéricho, devant les enfants ébahis et admiratifs, comme son oncle quand Marcel était enfant. Pierre Deux Croix l'appelait-on, car il s'était rendu deux fois en Terre sainte. La première un peu par curiosité, et aussi par défi. Et la seconde, pour y porter une mèche de cheveux de son épouse défunte et de ses deux filles mortes en bas âge.

Les bâtons et les besaces furent rapidement disposés devant les pèlerins, aux pieds de l'évêque. Le prélat s'empara d'un aspersoir et relut de nouveau le lourd volume que lui présentait le jeune clerc. Il secoua la main, faisant s'envoler quelques gouttes du liquide béni à chaque oscillation du poignet.

« *...ut digneris hanc sportam et fustem istum benedicere, quatinus eos qui illos in signum peregrinationis et suorum corporum sustentationem sunt receptori...* »

Marcel se sentait rajeuni par l'office, malgré l'inconfort. Il s'était lavé, fait couper les cheveux, raser la barbe et portait des vêtements neufs. Ses souliers aux coutures récentes lui meurtrissaient d'ailleurs un peu les orteils, à demeurer ainsi agenouillé. Oubliant la gêne, il s'enchantait des odeurs de savon montant à ses narines, mélangées aux volutes d'encens et à la saveur douceâtre des cierges colossaux qui l'entouraient.

Lorsque l'évêque et ses officiants se présentèrent devant lui, il tendit les bras comme on lui avait dit de le faire, prêt à recevoir les attributs qui l'accompagneraient sur les chemins jusqu'en Outremer. Un jeune clerc lui remit la besace que la femme de son frère lui avait confectionnée. Elle l'avait taillée dans une étoffe de prix, achetée dans une balle pendant la foire Saint-Martin à Pontoise, quelques mois plus tôt. Elle avait brodé le rabat et la bandoulière de croix de laine colorée.

« *In nomine domini nostri Jesu Christi, accipe hanc sportam peregrinationis habitum...* »

Le sac posé sur l'épaule, ses yeux coulèrent vers le solide bâton de marche en frêne qu'il avait fait tourner par Guéraud, un des fils de ses anciens voisins. La pointe ferrée ne risquait pas de glisser sur le sol et la tête s'ornait d'une petite croix de bronze qui scintillerait au soleil, illuminant ses journées lieue après lieue. Il se sentait en outre suffisamment aguerri pour en user au besoin contre les crânes de ceux qui tenteraient de s'opposer à son périple ou, plus prosaïquement, de le détrousser.

« *Accipe hunc baculum sustentationis itineris ac laboris uie peregrinationis habitum*... »

Lorsqu'il se releva, un peu bancal, Marcel arborait un sourire rayonnant. Les prêtres s'étaient évanouis par une porte dérobée, et seuls des acolytes demeuraient, occupés à ranger le matériel liturgique. Une odeur d'encens persistait, porteuse d'élan sacré. On se félicitait, on s'embrassait sans trop de ménagement.

Marcel rejoignit un neveu et sa femme et les enlaça de concert, avant d'attraper leurs enfants en une brassée enthousiaste et rieuse. Les gamins, sans trop savoir pourquoi, étaient gagnés par l'effervescence et couraient en tout sens en criant. Ils singeaient la cérémonie et les formules sacrées entendues plus tôt, et que personne ou presque n'avait compris en dehors des célébrants.

Les marcheurs convinrent entre eux de se retrouver à l'aurore le lendemain matin devant la cathédrale, afin de prier une dernière fois saint Pierre, à qui l'église était dédiée, de les mener sains et saufs jusqu'au tombeau du Christ. Puis ils franchirent les portes en une foule compacte, agitée, enthousiaste.

### Abords de Salzburg, midi du vendredi 6 juillet 1156

Installé sur un talus surplombant un ruisseau gargouillant, le petit groupe de marcheurs faisait une pause sous un bosquet de saules et de noisetiers, en contrebas du sentier. Un peu plus loin au sud s'étendait une vaste cité, aux puissants remparts, abritée sous un imposant château bâti sur une éminence. Au-delà, les reliefs des Alpes ornaient de crêtes l'horizon.

Les champs aux alentours étaient emplis de froment dont la couleur dorée annonçait la moisson prochaine. Depuis le chemin s'entendait le grincement de l'essieu d'un char lourdement chargé de pierres, tiré par un bœuf. Le paysan, baguette sur l'épaule sifflait paisiblement, le visage protégé par un chapeau de paille. Loin devant lui, un convoi de poneys disparaissait dans les frondaisons d'un bosquet ombragé. La plaine respirait l'opulence et la tranquillité, à si peu de distance d'une puissante cité.

Marcel massait ses pieds après les avoir trempés dans le petit cours d'eau. Il appréciait chaque fois de pouvoir ainsi se délasser, dès que l'occasion s'en présentait. Il attribuait à cette habitude le fait qu'il n'avait eu à déplorer aucun souci depuis leur départ, un mois plus tôt. Ils avaient marché d'un bon pas, parcourant les territoires alémaniques sans encombre, trouvant chaque soir une héberge confortable et de quoi se nourrir. Seulement, depuis quelques jours, des dissensions avaient surgi au sein du petit groupe.

Aubertin, un des plus jeunes, qui les avait rejoints à Strasbourg, maintenait qu'ils auraient dû couper par les montagnes et suivre la côte. Il savait que Venise, la célèbre ville marchande se rencontrait par là-bas et prétendait qu'il pourrait être avisé d'envisager de traverser par mer.

Profitant de l'arrêt, il expliquait à Emeline et Frogier, deux laboureurs des environs de Bailleul sur le Thérain, à Froidmont, que cette cité représentait le dernier moment où cela serait encore pertinent d'obliquer au sud. Son attitude agaçait Marcel, qui avait en quelque sorte pris la tête du groupe. N'ayant aucun clerc parmi eux vers qui se tourner, son statut de doyen l'avait naturellement désigné. Il interpela Aubertin d'une voix cassante.

« La paix, garçon, on continue au levant. »

L'autre leva la tête, fronçant le nez comme s'il allait mordre.

« Tu n'es point baron de cet ost, le Marcel. Il n'y a qu'hommes libres ici, nul serf !

--- Moi j'y vois au moins un bec-jaune, désireux d'apporter discorde en notre groupe. Si tu es acertainé de ton idée, prends ton bourdon et pars au sud.

--- Cheminer seul n'est pas bonne chose pour le pérégrin, tu le sais comme moi.

--- N'as-tu pas licence du sire évêque en ta besace ? le railla Marcel.

--- Il n'est point question de ça. Tu le sais fort bien. »

Marcel enfila et noua son soulier et se releva, s'approchant du jeune homme.

« Nous avons moult palabré de cela. Nous suivons au plus droit par voie de terre. Nous envisageons le retour par mer, plus rapide, pour ne pas trop se perdre en chemin. La discussion est close. »

Aubertin baissa le regard, mais pinça les lèvres. Il n'avait pas les moyens de se payer la traversée, Marcel le savait. En faisant partie d'un groupe, il pouvait espérer que son passage serait pris en charge avec les autres. Mais son attitude rebelle lui avait attiré pas mal d'inimitiés au sein des marcheurs.

Ils étaient partis une vingtaine de Beauvais, désormais presque trente-cinq à avancer, bâton en main. Un prêtre les avait suivis un temps, jusqu'à ce que des coliques l'obligent à rester dans un monastère, quand ils avaient commencé à longer la chaîne des Alpes. Aubertin s'était rapproché du petit groupe des Beauvaisiens et Marcel suspectait que c'était parce qu'il avait été condamné à faire ce pèlerinage pour un crime quelconque dans sa cité d'origine. Il n'aimait pas du tout son attitude. Mais n'avait guère de moyens de s'en débarrasser.

### Col de Chipka, fin de journée du vendredi 14 septembre 1156

Le majestueux panorama qui s'offrait aux yeux des marcheurs harassés les récompensait de la fatigue accumulée à gravir les pentes. Jusqu'à se noyer dans les nuages au loin, près de nouveaux contreforts montagneux, les plaines au sud s'étendaient. Des villages semblaient si petits qu'on aurait pu les écraser d'un doigt. De part et d'autre, les monts du Grand Balkan, couverts de forêts, se découpaient en alternance de vallées et de crêtes verdoyantes.

Assis auprès du rocher orné d'une croix, Marcel se reposait un peu en attendant les retardataires. Peu à peu, le groupe s'était étiré tandis que l'effort creusait les visages et épuisait les souffles. Ils avaient décidé de passer ce col aujourd'hui malgré la fatigue accumulée. Il le regrettait un peu désormais. Ils avaient encore pas mal de chemin à parcourir et le soleil baissait déjà.

Les derniers voyageurs croisés, une caravane de marchands de toile byzantins qui se rendaient à Craiova, étaient loin derrière eux. Aucun local ne déambulait plus par les routes si tard, ce qui leur poserait problème s'ils se trouvaient bloqués en montagne en pleine nuit. Dormir parmi fossés et haies n'était jamais agréable, devoir le faire sur un relief était bien plus dangereux. En dehors du risque, toujours présent, de finir dans une crevasse, la météo était bien trop changeante pour que cela soit prudent de le faire. D'un autre côté, continuer à marcher alors même qu'on ne voyait plus ses pieds ne l'enchantait guère. D'autant que la lune, en fin de son dernier quartier, n'apporterait pas de lumière, si elle parvenait à percer les nuages qui s'amoncelaient.

Lorsque les retardataires se montrèrent, essoufflés et écarlates, il ne leur laissa guère le temps de se reposer, leur expliquant qu'il leur fallait hâter le pas s'ils espéraient arriver à une halte pour la nuit. Maugréant et pestant, les marcheurs entamèrent donc la descente le visage maussade.

Les bourdons frappaient le sol en silence, tandis que les pieds fatigués faisaient rouler les cailloux, soulevaient la poussière du chemin. Les lacets se succédaient, chaque virage les rapprochait de la plaine d'où les fumées des villages montaient vers le ciel désormais bouché. Les arbres se mirent à frissonner sous le vent, incitant les plus frileux à se couvrir la tête de leur capuche. Au détour d'un des ultimes tronçons, le groupe s'immobilisa soudain.

Devant eux, au milieu du chemin, une douzaine d'hommes attendait. Tous avec des lances, des haches, et des arcs pour quatre d'entre eux. Assis sur une souche, appuyé sur le manche de sa cognée, un grand gaillard souriait d'un air sadique. Voyant que les pèlerins s'étaient arrêtés à quelque trente mètres d'eux, il se leva nonchalamment et s'approcha. Autour de lui, tous étaient nerveux. Les tireurs avaient encoché et deux d'entre eux semblaient prêts à bander. La voix du chef résonna dans le silence.

Il parlait avec une intonation rauque, dans une langue que personne ne connaissait. Mais nul n'en était besoin, pour deviner qu'il avait l'intention de les détrousser, voire pire. Marcel hésitait sur la décision à prendre. Il ne pouvait parlementer avec les brigands, ne comprenant rien à leur dialecte. Il savait que sur leur groupe d'une vingtaine d'hommes, seule une dizaine était apte à se défendre correctement et tous n'en auraient pas le cœur.

Tandis qu'il réfléchissait, le truand continuait à pérorer et semblait s'agacer du manque de réaction face à lui. Les détrousseurs étaient désormais tous tendus, crispés sur leurs armes. Marcel échangea un coup d'œil avec Gillot, un charpentier qui avait perdu une main dans un accident, mais dont la force était demeurée intacte. Ce dernier hocha la tête lentement, empoignant son bâton plus serré. Marcel hésitait à cause des arcs. Avant qu'ils n'arrivent au contact, plusieurs flèches auraient volé vers eux. D'un autre côté, rien ne lui garantissait qu'ils seraient laissés sains et saufs s'ils obtempéraient.

Il n'avait jamais été de ceux qui pliaient sans se battre, l'évêque Henri pouvait en attester. Il ne pouvait admettre que de simples larrons des montagnes allaient réussir là où le propre frère du roi n'y parvenait pas. Il leva la main en un signe amical et fit quelques pas en avant, pas trop pour ne pas inquiéter les brigands, mais suffisamment pour attirer leur attention sur lui. En passant parmi ses compagnons, il interpela à voix basse les hommes qu'il jugeait les plus aptes à se battre.

Arrivé devant le groupe, il fit mine de fouiller dans sa besace. Il n'y conservait pas grand-chose, les monnaies qu'il avait l'intention de dépenser pour la traversée de retour étant cousues dans ses vêtements. Mais il faisait semblant d'y chercher quelque richesse à donner. Le chef commença à rire, mais Marcel n'aurait pas juré qu'il était dupe de son jeu. Un des archers s'était relâché, mais les autres larrons paraissaient plus nerveux que jamais.

Marcel exhuma le parchemin que l'évêché lui avait remis, attestant de son statut de pèlerin. Il le leva bien haut, comme si c'était là un important document, souriant aux brigands. Il faisait mine de marcher comme un souffreteux, avançant avec hésitation. Il savait que sa barbe poivre et sel le vieillissait, et il espérait que la nuit tombante accentuerait cette impression.

Quand il ne fut plus qu'à une dizaine de pas des détrousseurs, il se rua subitement en avant, son bourdon brandi droit sur le meneur. Il hurla tel un dément, en appelant à la rescousse ses compagnons. Avant même que quiconque réagisse, il abattait sa croix de bronze en direction du sourire détestable.

### Faubourgs de Byzance, matin du samedi 29 septembre 1156

Le petit hospice accolé à la modeste église bénéficiait d'un jardin bien entretenu, d'agrément autant que potager. Marcel y déambulait tranquillement, goûtant les premières lueurs de l'aube, avant que le vacarme de la cité toute proche n'emplisse les oreilles. Ils étaient bien hébergés, et la nourriture était, sinon de qualité, au moins roborative. De plus, un savetier voisin s'employait à réparer leurs semelles et à leur garantir une paire de secours, le tout pour un montant honnête.

Ils étaient désormais une quarantaine, fragments agglomérés de marcheurs épars. La plupart étaient originaires de France, mais ils avaient rencontré ici des Pisans, dont le groupe avait été décimé par des fièvres et des coliques lors de leur traversée. La longue route avait clairsemé le noyau de Beauvaisiens. Frogier avait été grièvement blessé par une flèche à Cipka, et Emeline était resté auprès de lui, dans un village qu'ils avaient croisé peu après. Honfroi, le vieux changeur, était décédé, épuisé, avant même qu'ils n'arrivent au pied des Alpes. Aubertin, le trublion, avait disparu une nuit, alors qu'ils étaient dans la vaste plaine des Huns. Il ne le regrettait guère, celui-là.

Pour le moment, ce qui l'inquiétait, c'était l'état de Lienart. Fils d'un des principaux drapiers de la cité, membre actif de la commune il le connaissait depuis des années. Le jeune s'était épris de religion et avait souhaité prendre la coule plusieurs fois, sans parvenir à convaincre son père. Finalement, il s'était rabattu sur un pèlerinage au cœur de la chrétienté, peut-être dans l'espoir d'y trouver sa voie. Il était alité depuis deux jours, après s'être plaint de flux de ventre depuis un bon moment.

Les Pisans lui avaient dit qu'ils étaient bloqués ici depuis plusieurs semaines. Ils avaient espéré voyager par bateau jusqu'à Antioche, au moins, mais aucun navire ne les avait acceptés. Ils proposaient donc de se regrouper pour suivre la côte anatolienne. Les effroyables récits qu'ils avaient entendus sur les reliefs des plateaux les avaient incités à la prudence. Une troupe d'Allemands bien armés et équipés, avec plusieurs petits barons, y avait laissé une demi-douzaine d'hommes . Ils avaient cru y rester à vouloir braver la fureur des Turcs belliqueux qui contrôlaient la zone.

Luce, une des Strasbourgeoises, sortit alors qu'il allait rentrer. Sa silhouette empâtée s'était amincie au fil des lieues et son visage fin trahissait la fatigue. Elle s'était cassé le nez et avait perdu deux dents en chutant dans un ravin, mais il demeurait en elle une certaine grâce qui rappelait à Marcel sa propre fille. Il n'en avait plus aucune nouvelle depuis qu'elle était partie avec ce Normand mal embouché.

Il sentait qu'il reportait sur la jeune femme un peu de l'affection qu'il ne savait plus guère exprimer depuis qu'il était veuf, sans enfant dont s'occuper. La mort de son benjamin, Martin, d'une mauvaise fièvre, avant même qu'il ne trouve une épouse, avait été le déclencheur. Plus rien ne le retenait à Beauvais, si ce n'était ses devoirs, et il aspirait à autre chose. Au grand dam de ses amis et relations, il avait annoncé qu'il prendrait la besace et le bourdon à la Noël dernier.

On avait ri de sa résolution, se moquant de ses habitudes de bonne chère, d'amateur de putains, de belles étoffes et de lit de plume. Mais il avait tenu bon, peut-être renforcé dans sa décision par les railleries. Il avait vendu tout ce qu'il possédait, en avait confié une grande partie aux frères de saint Jean de l'Hôpital, dans l'espoir soit de revenir, soit de demeurer en Outremer. Il avait presque cinquante ans, et ne regrettait rien de sa longue vie. Il était juste déterminé à ne pas gâcher le peu qui lui restait, ne s'intéressant plus guère à ce bas monde.

Un moment, il avait hésité à rejoindre un couvent, mais l'austérité au quotidien le rebutait un peu. Le pèlerinage était aussi l'occasion de découvrir un ailleurs, et mourir en faisant cela lui convenait. Et le sourire de Luce, même en partie édenté, éveillait en lui des démons qu'il pensait depuis longtemps endormis. Échappant un rictus amical, il s'esquiva pour la laisser passer. Coquette, elle plissa les yeux en remerciement, se cachant la bouche d'une main pudique. Elle n'acceptait pas de bonne grâce sa nouvelle apparence.

Le regard caressant la silhouette qui s'éloignait, il soupira puis alla chercher ses affaires. Il fallait qu'il voit Remiggio, le Pisan le plus mature, pour discuter du voyage à venir.

### Faubourg de l'Asnerie, Jérusalem, veillée du jeudi 20 décembre 1156

Luce frissonnait malgré l'épaisse couverture à la puissante odeur de chèvre. Ils étaient tous assis ou allongés, affalés, dans la paille d'un bâtiment annexe de l'hôpital de Saint-Jean. Arrivés en fin d'après-midi, ils avaient préféré reporter leur entrée dans la cité sainte au lendemain, avec le jour nouveau. Ils avaient soupé d'un gruau poivré qui avait des effluves d'ambroisie sous leurs palais enthousiastes. Ils étaient enfin au terme de leur voyage, le but de ces mois de souffrance et de dévotion !

Marcel n'était plus le gros homme pansu de naguère, désormais sec comme un chêne en hiver, et les joues moussues de barbe grise. Ses cheveux, qu'il n'avait guère coupés depuis son départ, étaient noués en une dérisoire queue de cheval. Mais il souriait, satisfait. De Beauvais, il ne restait que neuf personnes.

Certains arriveraient peut-être plus tard, retardés par la maladie ou les blessures. Il pensait en particulier à Mainet, un potier dont les rhumatismes le tourmentaient tellement qu'il n'avait guère pu suivre leur rythme de marche. Avec un peu de chance, il serait là pour les Pâques. Plein de son bonheur, Marcel en souhaitait à tous les absents.

Dans quelques jours, il assisterait à la grande messe de Noël dans l'église qui abritait le tombeau du Christ. Puis il aurait de nombreuses semaines pour aller découvrir les endroits que les curés avaient évoqués devant lui, depuis son enfance. Il était surtout curieux de voir le Jourdain, là où on pouvait être baptisé de nouveau au même endroit que le Christ.

Luce lui avait avoué, un des soirs derniers, qu'elle aurait aimé prendre le voile dans une des abbayes de Jérusalem, mais elle avait envie de visiter les lieux saints avec lui auparavant. Elle avait apprécié assez justement la lueur qui habitait le regard de Marcel, mais s'y était refusée, n'accordant que son amitié. Elle ne souhaitait plus que le Christ pour époux, lui avait-elle confié.

Tout ce qu'il avait obtenu d'elle, c'était un baiser bien chaste sur le front et une caresse de la joue. Elle avait en elle des blessures qui ne pouvaient se guérir dans le siècle apparemment. Quelques années plus tôt, Marcel aurait peut-être balayé tout cela d'un geste moqueur et tenté d'embrasser les lèvres de force. Mais il était trop épuisé, trop calme, pour s'essayer à cela désormais.

Il ne savait guère ce qu'il allait faire par la suite, car il ne s'était jamais projeté au-delà de son arrivée en Terre sainte. Il avait d'ailleurs finalement dépensé toutes les monnaies dissimulées sur lui, depuis bien longtemps. La dernière avait fini chez un apothicaire de la côte contre quelques remèdes pour soigner un des Pisans. Il avait gardé la lettre des frères de Saint-Jean, énumérant les biens par lui confiés à leur garde. Peut-être pourrait-il l'échanger contre quelques besants ici. Cela lui éviterait le retour interminable et fastidieux. Il avait même assez d'argent pour entrer fort honorablement dans un couvent. ❧

### Notes

Le départ en pélerinage, y compris pour une destination proche, n'était pas une mince affaire. La plupart se contentaient de cultes locaux, de saints pas trop éloignés, au maximum d'une ou deux semaines de marche. Mais l'attrait pour Jérusalem ne se démentit pas, même au sein des couches populaires.

Être pèlerin donnait droit à l'assistance des établissements hospitaliers au long du chemin et, plus généralement, d'un accueil au moins minimal de la part des coreligionnaires. Des lettres rédigées par les autorités ecclésiastiques étaient remises aux marcheurs, de façon à leur permettre de prouver leur état, pour bénéficier de l'exemption de taxes, d'hébergements gratuits, etc. Malgré le statut qui en théorie garantissait la charité, il était plus sage de porter avec soi de quoi acheter de la nourriture ou les bonnes grâces de potentats locaux.

Il demeure malheureusement difficile de connaître exactement les conditions de voyage des pèlerins les plus modestes justement parce que ce sont ceux qui sont passés le plus inaperçus dans les sources. Ce n'est généralement qu'à travers des mentions indirectes qu'on peut les suivre.

Au XIIe siècle, le trajet vers la Terre sainte se faisait souvent par voie de terre, les marcheurs se regroupant pour affronter les périls. En outre, ils croisaient de nombreux autres voyageurs, suffisamment pour s'assurer la plupart du temps un cheminement en relative sécurité.

Le cérémonial encore balbutiant au XIIe siècle est attesté bien qu'apparemment conçu selon les besoins *ad hoc* de la congrégation où il se déroulait. Celui présenté dans la première partie s'appuie sur un texte extrait du pontifical de Bari (Ms. Graz Univer. 239, fol. 143v-146v), publié par Kenneth Pennington (que je remercie ici de m'avoir aimablement fourni un tiré à part), traduit et interprété par Joël Schmitz, qui m'avait déjà bien éclairé lors de la recherche documentaire pour le tome 2 des aventures d'Ernaut. N'ayant que partiellement utilisé la matière par lui traduite, nul doute que je la réemploierai à l'avenir.

Vous trouverez ci-dessous l'intégralité de son travail, ainsi que le renvoi vers la transcription de Kenneth Pennington.

Le titre « Marcheurs de lumière » est inspiré de l'évangile selon saint Jean, chap.XI, versets 9-10, lorsqu'il évoque la résurrection de Lazare.

**Ms Graz. Univer. 239 fol. 143V --- 146v**

Traduction de J. Schmitz, d'après Pennington Kenneth, *Traditio, Studies in ancient and medieval history, trought, and religion*, volume XXX, 1974, Fordham University Press, « The rite for taking le cross in the twelfth century », p.431-32.

*Office concernant ceux qui se rendent à Jérusalem*

Qui demeure à l'abri du Très Haut... (Ps 90). Heureux ceux dont la conduite est intègre... (et qui marchent dans la loi de Dieu... Ps 118). Enseigne-moi, Seigneur, ta voie... (guide-moi au droit chemin... Ps 26,11). Gloire au Père, etc... Seigneur aie pitié. Christ aie pitié. Notre Père... et ne nous laisse pas succomber à la tentation... J'ai dit, Seigneur, aie pitié. Guéris mon âme (car j'ai péché contre toi... Ps 40,5). Envoie-leur, Seigneur, ton secours de ton sanctuaire depuis Sion (Ps 19,3). L'ennemi jamais ne l'emportera sur lui ni le fils d'iniquité (Ps 88,23). Le Seigneur soit avec vous... (etc.) (Oraison) : Seigneur Dieu et Père tout puissant, dont le pouvoir divin est éternel et la souveraine majesté infinie, et vers qui nous nous rendons maintentant dans la foi, nous te demandons de regarder avec bienveillance la dévotion de ton serviteur qui espère en l'immensité de ta miséricore et qui souhaite humblement la grâce ineffable de ta tendresse, et qui, à cause des manquements à l'amour de ton nom, et par refus des tentations du monde, a voulu se rendre avec empressement, là où ton Fils, Notre Seigneur Jésus-christ, est né par ta volonté d'une vierge, selon la chair, est mort et ressuscité avant de monter au ciel.

D'après l'oracle du prophète qui a dit de lui (Jésus-Christ)...\
« Nous prierons en un lieu où il a séjourné », et parce que la fragilité humaine ne peut donner aucun fruit digne de pénitence sans ton aide, mais que grâce au don et à la largesse de tous tes biens, il va marchant vers toi au travers du chemin de tes commandements de toujours, Toi secours de ton peuple, protège le, pour son mérite qu'il doit conserver, et garde le pour ce mérite qui doit être protégé, dans l'accomplissement de la vie présente. Qu'il soit fort, lui qui est sur le point de recevoir avec bonheur la récompense de son labeur. Par ce même (Christ NS, etc.)

*Bénédiction de la croix*\
Dieu éternellement tout puissant, toi qui, par la soumission et le supplice de la sainte croix de ton fils Notre Seigneur Jésus Christ, as permis de racheter l'homme déchu par la faute diabolique, dans ta clémence porte ton regard sur cet étendard de la sainte croix, et dans ta tendresse daigne la bénir, afin que, à la lumière de la grâce de l'Esprit Saint, soit donné le mérite d'obtenir la rémission de tous ses péchés par le digne fruit de la pénitence, et la vie éternelle, à quiconque aura pris la croix en l'honneur de ton saint nom. Par (etc.)

*Autre*.\
Bénis, Seigneur, cette croix que ton serviteur est sur le point de prendre sur lui par dévotion de ton saint nom. Accorde lui, nous te le demandons, de montrer extérieurement de l'humilité, tant dans le regret que l'aveu de son repentir, de sorte que s'en suive pour lui une véritable purification de son âme, par l'effet de ta très grande largesse, ainsi que l'expiation de ses péchés. Par (etc.)

*Lorsque la croix est remise*\
Reçois cette croix signe de ton pélerinage au nom de notre Seigneur Jésus Christ, afin que, préservé par elle de toutes mauvaises rencontres (de tous malheurs), tu parviennes à ta destination, en temps opportun, et mérites de l'atteindre avec l'approbation du Christ. Qui avec le Père (etc.)

*Autre*.\
Reçois cette croix, signe de ton pélerinage, au nom de la rédemption de notre Seigneur Jésus Christ, et qu'à la manière de l'ange de Tobie lui-même, elle te précède sur ton chemin comme guide et compagnon, et de même que son ange le dirigeait, qu'elle te conduise à destination, te soit un compagnon joyeux, et qu'elle te préserve des incommodités du chemin. Que l'Esprit Saint daigne t'être un compagnon de voyage et écarte les méchants loin de toi.

Par lui-meme. En unité avec lui (Jésus-Christ)...

*Autre*.\
Reçois, mon frère N... l'étendard très victorieux de la sainte croix par lequel, comme d'une hache, tu puisses l'emporter sur la malice de tes ennemis, et te dresser victorieux en soldat, avec tous ceux qui suivent fidèlement Jésus-Christ, jusqu'au temps de l'ultime rétribution, lors de ton retour de la guerre, après avoir terrassé les ennemis et orné de la palme de la victoire, tu puisses recevoir, de notre seigneur Jésus-Christ qui est le chef suprême, des couronnes impérissables de gloire, et régner sans fin avec lui, dans son palais éternel. Qui avec le Père (etc.)

*Bénédiction de la besace et du bâton*\
Seigneur Jésus Christ, rédempteur et créateur du monde, toi qui as recommandé à tes bienheureux apôtres qui partaient prêcher de prendre avec eux seulement un bâton, nous te demandons avec insistance et dévotion de daigner bénir cette besace et ce bâton, afin que ceux qui sont sur le point de les recevoir en signe de pérégrination et de sustentation de leurs corps, reçoivent la plénitude de ta grâce céleste, et éprouvent la force de ta bénédiction, et que, de même que le bâton d'Aaron se désunit lui-même des Juifs rebelles en fleurissant dans le temple du Seigneur (Nb 17,10-11), tu les absolves de tous péchés, eux qui doivent être ornés de la marque des serviteurs du bienheureux Pierre pour que, au jour du jugement, ils soient séparés et placés à ta droite, en vue d'être couronnés. Par (etc.)

*Quand il reçoit la besace*\
Au nom de notre Seigneur Jésus Christ, reçois cette besace de voyage, afin que tu puisses accéder, en bonne santé et corrigé de tes fautes, au pays des bienheureux apôtres Pierre et Paul et des autres saints où tu souhaites parvenir et que, ton voyage accompli, tu puisses faire ton retour vers nous, sain et sauf. Par (etc.)

*À la remise du bâton*\
Reçois ce bâton pour soutenir ta marche et t'aider dans les moments pénibles de ton voyage, afin d'être fort pour soumettre toute bande d'ennemis et de parvenir ainsi sans danger au pays des bienheureux apôtres Pierre et Paul et des autres saints où tu souhaites te rendre, puis revenir vers nous, sain et sauf.

### Références

Ciggaar Krijnie N., *Western travalers to Constantinople. The West and Byzantium, 962-1204: Cultural and Political Relations*, Leiden - New York - Cologne : Brill, 1996.

﻿Collectif, *Voyages et voyageurs au Moyen Âge*, Paris : Publications de la Sorbonne, 1996.\[En ligne\], consulté le 23 septembre 2011, Adresse URL: [http://www.persee.fr/web/revues/home/prescript/issue/shmes\\\_1261-9078\\\_1996\\\_act\\\_26\\\_1](http://www.persee.fr/web/revues/home/prescript/issue/shmes_1261-9078_1996_act_26_1){.uri}

Heath Sydney, *Pilgrim Life in the Middle Ages*, London, Leipsic: T. Fisher Unwin, 1911.

Pennington Kenneth, « The rite for taking le cross in the twelfth century », dans *Traditio, Studies in ancient and medieval history, trought, and religion*, volume XXX, 1974, Fordham University Press, p.429-35.

Verdon Jean, *Voyager au Moyen Âge*, Paris : Perrin, 2003.

Webb Diana, *Pilgrims and Pilgrimage in the Medieval West*, Londres - New York: I.B. Tauris, 1999.

Fil du temps
------------

### Alexandrie, midi du youm al talata 12 jha'ban 489[^108] {#alexandrie-midi-du-youm-al-talata-12-jhaban-489108}

Yusuf al-Yama al-Bandaniji suait abondamment, les gouttes salées lui irritant le cou. Il avait beau s'aérer avec un large éventail, il lui semblait respirer à l'entrée d'un four. Retranché dans la pièce la plus fraîche de la maison de son ami Abu Muqatil, il désespérait de pouvoir rentrer bientôt à al-Quds[^109]. Sa demeure y disposait d'une courette ombragée en été, où gazouillait une fontaine recouverte de mosaïque. Le simple fait d'y penser faisait frissonner sa nuque épaisse. Il avala quelques longues gorgées de sirop de canne fortement dilué.

Devant lui s'étalaient de nombreuses étoffes qu'un négociant avait déballées à son intention. À l'approche de ramadan, avec toutes ses festivités, il tenait à ce que son épouse soit dignement habillée. Il voulait donc lui offrir une robe de soie, voire une pièce de tissu neuve pour qu'elle se fabrique le vêtement. Le marchand était un petit homme à la peau bronzée, légèrement cuivrée, un de ces Égyptiens hautains qui méprisaient ceux qui n'avaient pas vu le jour sur les rives du Nil. Sa lippe dédaigneuse claquait régulièrement sous son nez aquilin et ses yeux de faucon paraissaient de jais dans la pénombre des claustra. Yusuf se pencha vers une pièce qui l'intriguait et la montra du doigt.

« Qu'est ceci ?

--- Une magnifique toile issue des ateliers de Fustat, parmi les plus rigoureux. Connais-tu celui qu'on nomme al-Najm ?

--- Pas que je sache...

--- C'est un homme empli de sapience et de talent. Il dirige un des ateliers les plus réputés. Cette étoffe est née là-bas, entre les doigts experts de ceux qu'il dirige. »

Yusuf fit signe de la main qu'il voulait éprouver le tissu par lui-même. Le drapier déploya le pan en un geste emphatique, le gonflant telle une voile de navire pour en démontrer toute la finesse et la légèreté. Les motifs bleus chatoyèrent un instant dans les rais de lumière.

« Vois cette superbe étoffe intacte ! Elle est de belle taille, et l'on pourrait y couper plus d'un vêtement. »

En disant cela, il retint un sourire en réalisant que l'acheteur était d'une circonférence telle qu'on aurait été bien en peine d'y faire une simple 'aba[^110] pour lui. Heureusement, Al-Bandaniji semblait concentré sur l'examen minutieux des fibres, admirait les motifs qui se déployaient en assemblages géométriques.

« Quel serait le prix pour cette pièce ?

--- Il me faudrait la peser. Trois dinars le ratl[^111] généralement. »

Yusuf manqua s'étrangler.

« Tu te moques de moi ! Tu me prends pour un fellah stupide ou quoi ! Ce n'est que de l'*harrir*[^112], je ne vais tout de même pas la payer comme si elle venait de Perse !

--- Ah, je me suis trompé en effet, tu parles de juste. Ce morceau coûte seulement 25 dinars les 10 ratls.

--- Je ne mettrai jamais plus de deux dinars le ratl pour une pareille étoffe. »

Le marchand se renfrogna, faisant mine de replier la toile.

« Tu négliges la qualité des décors. Ils sont de bonne tenue, ne partiront pas au lavage ou avec le temps. Nos teinturiers savent faire des couleurs qui résistent à notre beau soleil égyptien.

--- Je n'ai guère envie de payer un tel prix, moi qui n'ai pas à supporter cette fournaise. Je t'en propose 21 dinars les 10 ratls.

--- À 22 je peux encore m'en sortir. Si je vais en dessous, mes associés penseront que je les ai volés. Un si beau travail ne peut être vendu à moins.

--- 22 alors, c'est dit » accepta Yusuf, hochant la tête avant d'avaler une nouvelle longue rasade.

« Aurais-tu beaux turbans à me proposer ? Pas de ces vulgaires chiffons à deux-trois dinars, un vrai beau couvre-chef, que je sois fier d'arborer à la mosquée pour youm al joumouia[^113]. »

En disant cela, il eut soudain une pensée pour Sabiha. Si jamais il ramenait à son épouse de quoi l'habiller de neuf et qu'il n'avait rien prévu pour elle, celle qu'il aimait appeler sa petite gazelle risquait bien de se montrer lionne. Elle n'était généralement que douceur et tendresse, mais jamais Allah n'avait fait de femme plus jalouse. Elle mesurait l'amour que lui portait Yusuf à l'aune des ratls de cadeaux qu'il lui faisait plus qu'à ses démonstrations d'affection empressées. Pas question de se contenter d'un simple *mindîl*[^114] pour la satisfaire, s'il achetait cette belle étoffe de soie bleue pour son épouse, il faudrait qu'il fasse au moins aussi bien pour elle. Il soupira, la nuque moite. L'Égypte n'était vraiment qu'une vaste fournaise.

### Jérusalem, après midi du youm al khemis 22 jha'ban 492[^115] {#jérusalem-après-midi-du-youm-al-khemis-22-jhaban-492115}

Yasmina aperçut du mouvement dans la chambre des maîtres tandis qu'elle se rendait sur la terrasse, histoire de voir ce qui se passait dans les alentours. Intriguée, elle souleva la portière d'épais tissu. Elle découvrit Shahid qui s'acharnait auprès d'un des gros coffres avec un énorme couteau de cuisine.

« Mais qu'est-ce que tu fais ? »

Le domestique sursauta, en échappa son outil, manquant de se trancher un doigt.

« Yasmina ! Tais-toi, tu vas attirer les maîtres ! »

Elle s'approcha, les sourcils froncés, la voix grave comme si elle s'adressait à un enfant.

« Tu as perdu la tête ? Qu'essaies-tu de faire ?

--- Il faut se sauver. Viens avec moi, Yasmina, les Ifranjs vont tuer tout le monde ici. Il faut se sauver ! »

Elle secoua la tête en dénégation.

« Nous sommes à l'abri ici. Si tu sors, tu vas y laisser ta tête.

--- Rester, c'est mourir. Prenons ce qu'on peut et fuyons. »

Il soupira, hésitant à se lancer. Il avait toujours trouvé Yasmina à son goût, même si elle était un peu plus âgée que lui. Elle était parfois un peu trop infantilisante, mais elle le voyait comme le domestique qu'il était. Une fois dehors, dans leur propre demeure, elle apprendrait à le respecter comme un homme. Du moins l'espérait-il. Il lui prit la main, et la fixa, ce qui déclencha un malaise immédiat.

« Viens avec moi, Yasmina. Je te protégerai et, inch'Allah, nous nous marierons une fois tout ça fini. »

Elle s'esclaffa sans aménité.

« Tu comptes me protéger avec ton couteau ridicule ? Ils ont des épées d'acier et des armures de fer dehors. Nous ne ferons pas deux pas que tes entrailles seront versées dans la rue et moi emmenée par ces chacals puants. »

Elle retira sa main sans brusquerie, mais avec fermeté.

« Nous n'allons pas fuir tous les deux, Shahid. Je t'aime bien, comme un petit frère, alors je t'en conjure, reste ici avec moi et les maîtres. »

Le jeune homme haussa le menton de défi, blessé dans son orgueil.

« Tu crois que je ne saurai pas me défendre, *te* défendre ?

--- Je crois que ce n'est pas le moment de sortir surtout ! »

Ignorant sa réponse, il reprit sa lame et s'acharna brièvement sur la délicate serrure du coffre, révélant un ensemble de vêtements de prix, d'étoffes brodées, teintes et décorées.

Il s'empara d'une robe de soie chatoyante, aux motifs bleus géométriques.

« Regarde, rien qu'avec ça, on a de quoi vivre un long moment.

--- Tu es fou... »

Démontrant d'un geste qu'elle n'avait pas forcément tort, il lui brandit le couteau sous la gorge. Les yeux écarquillés d'horreur, elle tenta de s'écarter mais il la retint par le poignet.

« Si tu ne viens pas, je...

--- Je ne dirai rien, j'en fais serment, Shahid. Je ne trahirai pas. Mais ne me fais pas de mal, je t'en conjure... »

Hésitant un instant, il la relâcha lentement, jouant avec ses les lèvres comme un enfant pris en faute.

« Viens avec moi, Yasmina. Nous pourrions mieux vivre qu'ici. Damas n'est pas si loin.

--- Comment aller jusque là bas ? Comme de vulgaires vagabonds ? »

Il s'empara d'une poignée d'étoffe qu'il entreprit de se nouer autour de la taille, sous ses propres vêtements de travail. Une voix résonna dans l'escalier, depuis le rez-de-chaussée, attisant la fébrilité des deux domestiques.

On appelait en bas.

« Range ça, intima Yasmina sans succès, tentant de reprendre les tissus qu'il sortait désormais sans plus de retenue.

--- Va voir ce qu'ils veulent ! cracha-t-il d'un air offensé. Si tu ne viens pas avec moi, va te terrer avec eux. Et mourir quand les Ifranjs forceront la porte. »

La jeune femme se mordit la lèvre, affolée. Elle hésita à caresser la joue de son compagnon de domesticité, mais elle craignait qu'il comprenne de travers cette démonstration d'affection. Il courait à la mort et ne voulait rien entendre. Comme elle se levait pour aller répondre aux maîtres, Shahid nouait le beau vêtement en soie à motifs bleux sur sa poitrine. Et soudain un bruit formidable retentit dans le couloir d'entrée. On venait de forcer la porte dans un fracas épouvantable.

### Clepsta, après-midi du samedi 8 septembre 1156

Clément fit couler la dernière goutte de son verre dans son gosier desséché. Il n'avait pas marché longtemps mais la chaleur des collines de Judée était assommante pour le colporteur qu'il était. Il allait d'un casal à l'autre, porteur de babioles : rubans, boutons, colifichets et petits accessoires, épingles, pendentifs en étain. Il approvisionnait ainsi les habitants sans qu'ils n'aient à se déplacer jusqu'à Jérusalem, distante de plusieurs lieues. Il profitait de l'allègement progressif de sa balle pour acheter de vieux vêtements qu'il recyclait avec l'aide de sa mère.

La vie n'était pas toujours aisée mais il aimait cette liberté qu'elle lui offrait, et parfois il savourait de bons moments, comme en cet instant. Il était arrivé la veille tardivement et n'avait pas eu le temps de présenter ses articles, ce qu'il avait fait au matin. Une de ses acheteuses lui avait proposé de venir examiner des habits dans l'hôtel de ses parents, où il profitait pour l'instant du confort d'un matelas, dans une niche ombragée. La femme était allée chercher les pièces qui pourraient l'intéresser, qu'elle avait mises immédiatement à l'abri au décès de son père, avait-elle expliqué. Elle demeurait avec son époux et ses enfants quelques rues plus loin.

Sa silhouette se découpa bientôt dans la porte inondée de lumière, suivie d'un gamin portant une corbeille bien remplie. La matrone n'était guère jolie, le visage rougeaud et plat, avec des yeux écartés et trop grands, mais elle était accueillante et sympathique, assez bavarde. Sa tenue soignée attestait en outre d'une bonne maîtresse de maison. Clément estima qu'il ne serait guère judicieux de faire des offres trop basses, elle ne serait certainement pas dupe et pourrait s'en offenser. Il s'approcha de la table où elle disposait les vêtements les uns après les autres.

« Je ne vois là que de bien ordinaires toiles. Mais la façon en est belle, et la trame encore solide » approuva-t-il.

Tout en examinant les morceaux de tissu, il les triait un à un, dans une sélection devant lui. Il voyait les pupilles de la femme s'allumer d'une lueur avide tandis que le tas augmentait. Puis un peu de malice s'y mêla lorsqu'elle sortit le dernier vêtement. Clément en écarquilla les yeux. Elle tenait à la main une cotte de soie claire aux motifs bleux géométriques. Assurément une pièce de choix, quoiqu'un peu courte et sans grande ampleur.

« Cette pièce aussi est à vendre ?

--- Oui-da. Mon père y tenait fort, car il en avait hérité du sien. C'était un vêtement gagné par mon aïeul lors de la prise de la sainte Cité. Nous sommes croisés de la première heure dans ma famille. »

Sa voix modulée avec fierté accusait le prestige qu'elle accordait à une telle affirmation et Clément ne voulut pas la vexer, se contentant d'opiner mollement du chef tout en tendant la main vers la cotte. Immédiatement, il vit que le bas était composé d'une myriade de petits morceaux adroitement assemblés.

« Elle est fort abîmée, c'est dommage.

--- J'ai fait de mon mieux, on ne voit guère les coutures.

--- Certes, mais des pans de grande ampleur donnent plus de valeur. On peut plus facilement y retailler à son gré. Qu'est-il arrivé à la toile ?

--- Je ne sais guère. Mon père ne m'en a jamais rien dit. Je me suis contenté de repriser. »

Elle n'allait certainement pas avouer à un pied poudreux, un quasi-inconnu, que son père s'était fait mordre par un mâtin un jour qu'il sortait de l'église dans sa belle cotte. Il en avait été si contrarié qu'il avait refusé de se rendre à la messe pendant de longs mois, se houspillant avec le curé à la moindre occasion à propos de cette attaque qu'il estimait démoniaque.

« Je crains ne de pouvoir revendre la cotte en elle-même, et y retailler fera perdre beaucoup de valeur. Vous en espériez bon prix, je pense.

--- Je ne le céderai certes pas comme vulgaire coutil, je n'ai aucune urgence. C'est l'héritage de mon père après tout. »

Clément fit mine d'examiner l'étoffe avec dépit, mais il savait qu'il pouvait le transformer. Un tissu de si belle qualité pourrait être découpé pour en faire des pochettes à vendre aux pèlerins, pour qu'ils y resserrent les précieuses reliques qu'ils ramenaient de Terre sainte. Parfois ce n'était qu'un caillou de la ville où Jésus avait marché, mais le fait de le confier à la soie en rehaussait la sainteté, quand on le montrait à ses amis et familiers restés en France, en Normandie ou en Auvergne. Il y avait même de quoi faire quelques petits couvre-chefs, simples bonnets circulaires qui protégeaient le crâne du lourd turban posé au-dessus.

Reprenant les pièces qu'il avait sélectionnées auparavant, il additionna rapidement. Quelques chausses, une cotte de solide chanvre et une autre de laine, des chemises plus ou moins râpées, en coton...

« Je vous proposerai pour le tout dix besants de mes marchandises, ou huit en monnaie sonnante et trébuchante. J'ajoute encore cinq besants d'affaires pour la cotte de soie, ou trois et deux rabouins en monnaies. »

La paysanne compta sur ses doigts et remua le paquet de linge qu'il avait devant lui, de façon à en vérifier le contenu.

« Cela ne fait tout de même pas lourd pour cotte de soie.

--- Elle est fort usée, et trop petite pour la plupart des hommes. Sans compter que le bas en est bien abîmé.

--- Iriez-vous jusqu'à dix-sept besants de marchandies pour le tout ? En sus, je vous promets le gîte et le couvert chaque fois que vous passerez jusqu'à Noël prochain. »

La garantie d'une héberge, et de repas certainement de qualité, vu la matrone, convainquirent Clément. Ne pas avoir à sortir de monnaies d'argent lui plaisait, car il évitait d'en trop avoir sur lui, par peur des détrousseurs. Il était rare qu'on mentionne des voleurs aussi près de la ville, maintenant que les musulmans n'y venaient plus guère, mais il savait par expérience que des gens pouvaient être attaqués et disparaître sans laisser aucune trace pour peu qu'ils aient trop de bien sur eux.

« Marché conclu, répondit-il avec enthousiasme.

Puis il ajouta, un large sourire aux lèvres : cela comprend aussi le souper de ce jour ? »

### Abords du Jourdain, près de Jéricho, soir du youm al sebt 27 jha'ban 658[^116] {#abords-du-jourdain-près-de-jéricho-soir-du-youm-al-sebt-27-jhaban-658116}

Salih fut réveillé par sa mère qui le secouait doucement. Il jeta un œil par-dessus la couverture et vit que le jour était levé ; plusieurs de ses sœurs étaient déjà affairées. Parmi elles, Fazila lui fit un sourire, tout en préparant les galettes de pain qu'elle mettait à cuire dans le petit four de terre. Il entendait derrière le *qata*[^117] la voix de son père mêlée à celle de ses oncles.

Il se leva, encore un peu ensommeillé et regarda à travers un des trous de la toile de séparation. Il vit que son frère aîné était là lui aussi et que les hommes discutaient des événements récents. Des djinns terribles venaient du nord, de par delà les montagnes, et ils s'affrontaient aux seigneurs des villes voisines, sans que Salih comprît bien en quoi cela les concernait, eux[^118].

Se frottant les yeux, il revint chercher de quoi se restaurer, se faisant réprimander par les femmes plus âgées, car il encombrait le passage. Il prit un gobelet de lait frais, un morceau de pain et sortit devant la tente, pour s'asseoir sur une pierre à l'écart.

Tout en grignotant son repas, il finit de se réveiller en regardant ses sœurs et sa mère traire les chèvres à l'ombre des arbres de l'oasis. En partant, son père vint lui frictionner affectueusement la tête, accompagnant son geste d'un sourire. C'était un des hommes les plus influents de la tribu, bien que n'étant pas le scheik. Il était néanmoins assez âgé, tout le monde respectait son expérience. Et lui avait une tendresse particulière pour son dernier fils, Salih, auquel il pardonnait bien des bêtises qu'il n'aurait jamais tolérées d'un autre de ses enfants.

Après avoir ramené son gobelet, Salih partit en quête de ses amis, avec lesquels il aimait passer le temps. Sa mère, le voyant s'éloigner, l'apostropha :

« Salih, ne t'éloigne pas trop. Tu dois aussi aller à la corvée de bois ce matin, n'oublie pas.

--- Non non je reviens vite, je vais voir 'Ali et Zaid » répondit-il en se mettant à courir.

Il ne lui fallut pas longtemps pour retrouver ses deux camarades de jeu. Ils étaient occupés à ramener un chevreau, l'air sérieux et affairés.

« Qu'est-ce qui se passe ?

--- C'est Abu Hamzah qui nous a demandé de ramener ce chevreau. Il s'était coincé dans un paquet de broussailles. »

Tout en parlant, ils aiguillaient l'animal d'un air attentif, le guidant vers le reste du troupeau, sous la surveillance de plusieurs hommes dont Abu Hamzah, le chef des pâtres. Les enfants partirent ensuite vers la rive du Jourdain pour s'amuser à faire des ricochets. Ils y retrouvèrent plusieurs femmes de corvée d'eau, dont ils se cachèrent parmi les bosquets pour échapper à des tâches domestiques. Tandis qu'ils choisissaient avec soin les pierres qui glisseraient le mieux sur les flots, 'Ali leur proposa :

« Et si on allait fouiller les ruines après manger ?

--- Tu sais qu'on n'a pas le droit, c'est dangereux là-bas répliqua Zaid.

--- C'est une réponse de fillette ça. Je suis sûr qu'on pourrait y trouver des choses intéressantes. Mon frère 'Umar, y m'a dit qu'il y avait ramassé des pointes de flèches quand il était plus jeune. On pourrait se faire des arcs comme les djinns, et s'entraîner à tirer, pour aller chasser.

--- Oui ! Cherchons de quoi se faire des arcs et des flèches. Je sais où trouver la ficelle dont on aura besoin. »

Les trois enfants se mirent alors à la recherche de ce qui pourrait leur servir dans leurs desseins guerriers, oubliant qu'ils devaient aller à une autre recherche, de bois, avec les femmes.

Plus tard dans la journée, Salih était assis à l'abri de l'auvent, appuyé contre le *qach*[^119], sanglotant piteusement lorsque son père arriva pour manger. Il s'enquit de ce qui se passait, interrogeant du regard son épouse. Elle lui répondit d'un air pincé, sachant par avance que son autorité serait désavouée.

« Il a oublié encore une fois qu'il avait du travail à faire ce matin. Il a disparu avec ses petits copains et n'est reparu que pour manger. Alors, il est puni, il n'a rien pour son repas. S'il veut manger, il n'a qu'à s'acquitter de ses corvées.

--- Tu faisais quoi au lieu de faire ce que ta mère t'avait dit ? »

Salih répondit entre deux hoquets :

« On cherchait de quoi se faire des arcs, pour aller chasser. On a trouvé des sacs de trésor...

--- Quelle chance ! Viens avec moi, fils, raconte-moi ce que tu voulais faire. »

Son père l'entraîna dans ses quartiers, où il fut rapidement servi, ainsi que ses deux cadets pas encore mariés. Contrevenant à ce que sa femme avait décidé, il partagea son repas avec Salih. La bonne humeur du gamin revint aussi vite qu'elle était partie. Il convint de ramener son petit trésor dans l'espoir qu'il ait de la valeur.

Tandis que les adultes s'allongeaient pour prendre un peu de repos après manger, Salih s'esquiva avant de croiser de nouveau sa mère. Il retrouva ses compagnons de jeu dans la tente voisine, où se trouvait également sa sœur Fazila, qui y apprenait de nouvelles techniques de tissage auprès d'une femme expérimentée. Zaid et 'Ali discutaient de ce qu'ils allaient faire des petites bourses de toile qu'ils avaient dénichées, emplies de poussière des bords du Jourdain.

Ils en avaient vidé le contenu et prévoyaient de s'en faire chacun un sac à merveilles. Les trois gamins eurent tôt fait de retourner vers les ruines. Le terrain montait régulièrement jusqu'au plateau qui accueillait le village abandonné, parmi les palmiers. Une végétation rase avait commencé à envahir ce qui devait être à l'origine un des fleurons de la présence franque en Terre sainte.

Les enfants croisèrent deux cavaliers probablement venus de la grande cité voisine. Ils avaient stoppé là, car l'endroit offrait un excellent point de vue sur les environs. Indifférents aux gamins, ils s'approchèrent ensuite nonchalamment du camp, où ils restèrent un moment à discuter, avant de mettre pied à terre et de se rendre dans la tente du scheik. Un certain nombre de têtes de bétail fut rapidement séparé du troupeau par Abu Hamzah et ses hommes. Les trois compagnons réunirent ensuite leurs trouvailles et repartirent tranquillement vers le camp.

Le soir, Salih était tout excité, courant de-ci de-là avec ses amis. Ils trottaient d'une tente à l'autre, se cachant parmi les broussailles qui les protégeaient du vent. Une excellente vente avait été conclue par un de ses oncles aujourd'hui et un repas de fête était prévu. Salih avait aidé ses sœurs et sa mère à préparer le *mensaf*[^120], et c'est lui qui avait eu l'honneur de poser la tête de mouton sur le plat prêt à être servi. Son arrivée fut accompagnée d'exclamations de plaisir.

Il rejoignit ensuite les femmes avec lesquelles il dînait en compagnie de quelques cousins. Il était encore trop jeune pour partager le repas des hommes. On discuta essentiellement tissage, car l'une d'entre elles était en train de finir une nouvelle tente. Fazila y apprenait le travail en longs panneaux des fils en poils de chèvre. Cette toile était pour un jeune qui allait bientôt se marier. Il avait assez de têtes dans son troupeau et avait conclu un accord avec une famille d'une tribu voisine. La cérémonie était prévue pour la fin de l'hiver. On évoqua aussi la situation de Fazila. Elle ne tarderait pas à se voir dotée d'un époux. Leur père s'en occupait avec sérieux, ayant déjà éconduit plusieurs prétendants qu'il ne trouvait pas à son goût.

Durant tout le temps du repas, Salih couvait des yeux un panier où les petites bourses de tissu coloré avaient été mises à l'écart. Stockées avec d'autres guenilles dans une grotte proche, à l'abri, elles seraient ensuite revendues à des négociants pour les fabricants de papier. Salih regrettait juste de ne pas avoir pu conserver une des jolies petites bourses pour lui. Lorsque sa mère le déposa sur sa couche pour la nuit, il l'attrapa par le cou en un geste affectueux un peu outrancier. Puis il lui murmura :

« Mère, je pourrais garder un des petits sacs, celui avec les dessins bleus ? Je promets de faire mes corvées aussi longtemps que tu voudras. »

Mais sa mère avait élevé trop d'enfants pour se laisser prendre à ce jeu. Elle tira la couverture sur lui et répondit.

« Fais d'abord tes corvées, et Allah veillera à te récompenser. »

### Caverne 38, falaise du Qarantal, près de Jéricho, mercredi 7 juillet 1993

La truelle de l'étudiant grattait soigneusement le sol, dévoilant les uns après les autres les vestiges qu'il dégageait ensuite à l'aide de pinceaux et d'outils fins et précis. Depuis quelques jours, c'était l'effervescence dans le petit groupe qui participait à la campagne de fouilles. La découverte d'un trésor était toujours grandement excitante, et il ne fallait nullement qu'il soit d'or pour provoquer l'enthousiasme.

En l'occurrence, il s'agissait de simples fibres, de fragments textiles, mais en telle quantité que cela permettrait de mieux connaître les usages médiévaux. L'endroit était daté des alentours du XIIIe siècle et la formidable documentation écrite qu'on avait pour la période saurait éclairer avec pertinence le moindre brin exhumé. À peine reconnu et extrait de sa gangue, le plus petit morceau d'étoffe était soigneusement numéroté, empaqueté, aux fins de nettoyage et de conservation avant l'analyse. Si le climat sec du lieu pouvait préserver miraculeusement des fibres sur des centaines d'années, cela pouvait être ruiné par une mauvaise manipulation, un traitement inadapté.

Le jeune étudiant de l'université d'Haifa qui fouillait tout à côté jeta un coup d'œil curieux par-dessus l'épaule de la veinarde qui s'employait à dégager un lambeau de tissu. La zone F, où ils se trouvaient avait jusqu'à présent dévoilé le plus de trésors, mais lui n'avait eu droit qu'à des morceaux de ce qui devait être de la corde ou, du moins, de la vannerie de ce type. Ils s'interrompirent un instant tandis que le responsable de leur secteur, qui était allé aux nouvelles, revenait l'air ravi.

« Il y a plusieurs morceaux de qualité dans ce qu'on a trouvé l'autre jour. De la soie, en particulier un gros morceau à motifs bleus, géométrique, le 704. »

Voyant le visage émerveillé des jeunes étudiants archéologues qui avaient participé à la découverte de ce trésor, le responsable ajouta « Il va y avoir de la saisie à faire, avec tous ces fragments. »

La perspective de longues heures à saisir des données informatiques fastidieuses : taille et densité des fils, sens de torsion, nature des poils, technique de tissage, teintures..., rien ne pouvait entamer la joie des ouvriers de fouille enthousiastes devant la richesse de leur trésor. Pas même le fait que seule une poignée de spécialistes obscurs se pencherait sur l'étude qui en sortirait. ❧

### Notes

Ce texte est inspiré par mes études d'archéologie, au cours desquelles j'ai eu la chance de pouvoir manipuler de nombreux objets de vie quotidienne, essentiellement de la céramique, pour laquelle j'ai toujours eu une tendresse particulière. J'ai rapidement éprouvé un sentiment d'intimité avec les fabricants et les utilisateurs passés, simplement en raison du fait que je tenais par moment des artefacts dont ils usaient chaque jour.

Le dernier passage est aussi un clin d'œil au travail pas facile et souvent méconnu des archéologues, dont la réalité est à des années-lumière des aventures dépeintes dans les films à grand spectacle. Par contre, j'ai toujours rencontré chez eux une véritable jubilation à découvrir des artefacts, non pas pour les objets en eux-mêmes, car cela concerne plus les pilleurs et/ou les collectionneurs, mais à cause du formidable potentiel que chaque vestige recèle quant à notre histoire. Je ne prétends aucunement avoir présenté là une image fidèle de la campagne de fouilles dirigée par Orit Shamir et Alisa Baginski, mais il me semblait que sans cela, le récit aurait été incomplet

### Références

Baginski Alisa, Shamir Orit, « Textiles' Treasure from Jericho Cave 38 in the Qarantal Cliff Compared to other Early Medieval Sites in Israel » (2012), *Textile Society of America Symposium Proceedings*, Paper 742. En ligne : <http://digitalcommons.unl.edu/tsaconf/742> (consulté le 12 janvier 2015).

Baginski Alisa, Shamir Orit, « Textiles' Hoard from Jericho Cave 38 », dans *The Qarantal Cliff, Hoards and Genizot as Chapters in History*, Haifa, 2013, p.77-88. En ligne : <https://www.academia.edu/3659997/Shamir_O._and_Baginski_A._2013._Textiles_Hoard_from_Jericho_Cave_38_in_the_Qarantal_Cliff._Hoards_and_Genizot_as_Chapters_in_History._Haifa._Pp.77_-88_> (consulté le 12 janvier 2015).

Nœud gordien
------------

### Port de Gibelet, midi du lundi 19 juin 1150

La clameur du port valait celle de Gênes, même si la taille en était bien modeste comparativement. Des chameaux dans un enclos près des murs de la cité, des palmiers qui apportaient un peu d'ombre indiquaient clairement que l'on était au Levant. Les hommes enturbannés, les visages olivâtres étaient encore plus nombreux, et les navires arboraient des couleurs et des formes exotiques. Devant le bâtiment de la Chaîne, les douanes où Willelmo était allé inscrire les biens qu'il importait, sa femme Benedetta et ses deux enfants attendaient, un peu inquiets.

Lorsque le vétéran sortit, cette vision l'amusa et il souriait largement quand il les rejoignit.

« Alors, femme, montre un peu entrain à fouler ce sol sacré ! »

Benedetta lui lança un regard noir. Il n'avait toujours pas réussi à briser le caractère de cette génoise au cœur fier, malgré les années, et les raclées autrefois régulières. Il avait fini par abandonner l'idée de la dresser à son idée, et s'étonnait de ne pas en être autrement affecté. Lui qui se vantait d'avoir passé par le fil de l'épée plus d'hommes, et de femmes, qu'il n'avait pu le compter, échouait à se faire respecter dans sa propre maisonnée. Il haussa les épaules, se penchant ensuite vers la petite Berta, sa dernière et lui murmura :

« Tu sais qu'ici ils ont des singes ? Des marmousets comme compagnons. Il te dirait d'en posséder un ? »

Empêtrée dans les robes de sa mère, la petite acquiesça avec un sourire incomplet.

« Allons-nous chercher hostel pour loger ?, l'interrompit Benedetta.

--- Si fait, femme. Nous allons voir... un certain Domenico Bota, qui possède logement à louer pour une famille comme la nôtre. Nous allons nous installer ici un temps.

--- Ne voulais-tu pas aller au plus vite auprès du comte ?

--- Nous ne pouvons partir ainsi, comme valets sans maître. Je vais t'installer comme il sied, puis je louerai monture pour préparer notre venue en la grande cité. »

Benedetta sembla réconfortée à l'idée et marmonna un vague assentiment. Elle n'avait jamais eu à se plaindre de son époux sur le plan matériel. Fille d'un humble pêcheur, elle avait fait bon mariage selon ses critères. Il ramenait pas mal d'argent de ses expéditions de soldat, la laissait gérer la maison comme elle l'entendait et, surtout, il était souvent absent. De plus, avec le temps, sa main devenait légère, se faisant volontiers plus caressante que violente. Elle qui avait été habituée au ceinturon de son père, elle en était à la fois soulagée et un peu déstabilisée. Pas au point de s'en inquiéter malgré tout. Elle avait deux enfants dont s'occuper, sans compter les trois qu'elle avait perdus et à Gênes, ils avaient un logement dont elle pouvait s'enorgueillir, avec du verre aux châssis des fenêtres et de la vaisselle décorée sur la table.

« Tu vas voir, femme, les monnaies d'or vont pleuvoir comme l'ondée d'automne sur nos têtes. Il y a toujours emploi pour soldat avisé. Avec mes lettres, je suis acertainé de trouver bon poste auprès du sire comte. Je pourrai diriger un petit groupe de sergents, monté comme un chevalier. Pour ça, on soudoie jusqu'à six sols la journée. »

Il s'esclaffa, arrêtant sa femme d'un geste emphatique, et bloquant les autres badauds.

« De quoi t'habiller en laine d'Angleterre et soie de Babylone ! »

La remarque finit par arracher un sourire à Benedetta.

« Avoir bel hostel me sied mieux, tu le sais bien.

--- On aura et l'un et l'autre ! Et valets pour nous servir, comme barons en leur fief. Je ne te parle là que de ce que le sire comte me versera. Il y a toujours du butin à grappiller. Regarde ce que j'ai pu amasser en Espaigne, alors imagine : ici, les ânes chient de la soie et les porcs des épices. Nous serons bien vite aussi riches que les Doria. »

Tout en parlant, ils franchirent l'entrée dans la ville, passant la muraille d'ocre pour pénétrer dans le dédale des venelles ombragées. Près du port, l'odeur de la marée, du varech et des poissons était encore forte, mais elle disparut peu à peu au profit des senteurs plus riches, plus sèches des souks et de la poussière des rues à boutiques.

### Tripoli, matin du mardi 7 octobre 1152

Willelmo tenta de s'asseoir un peu mieux sur sa couche, mais la douleur à la jambe le fit grimacer et même exhaler un râle. Benedetta, assise à côté de lui, en train de réparer un des vêtements des enfants, se tourna, inquiète.

« Rien de grave, je m'installe mieux, voilà tout.

--- Désires-tu assistance ?

--- Non pas. J'arrive encore. Curieux comme je peux souffrir d'un membre qu'on m'a coupé. »

Willelmo lança un regard triste sur le drap aplati qui s'étendait là où aurait dû se trouver son mollet droit. Il avait été amputé quelques semaines plus tôt, après qu'une flèche lui eût brisé le tibia. Le chirurgien avait craint que toute la jambe ne pourrisse et avait donc coupé sous le genou. Depuis lors, le soldat se reposait chez lui. Il avait reçu au début la visite de ses nouveaux compagnons d'armes.

Désormais, ils avaient repris le cours de leurs activités et l'avaient un peu oublié. Un temps, il avait enragé de son sort et abusé du vin arrosé de pavot qui l'aidait à supporter la douleur. Il ne gardait qu'un souvenir confus de cette période. Il n'avait refait surface que depuis peu, et commençait à accepter. Benedetta et les enfants lui apportaient beaucoup de réconfort. Mais leur présence lui rappelait qu'il ne pouvait demeurer ainsi très longtemps.

Sa femme était enceinte de nouveau, et il ne pouvait rester inactif. Ils avaient de quoi vivre plusieurs mois grâce à la gestion parcimonieuse de Benedetta, mais il était de son devoir de trouver une solution à la situation. Tout en réfléchissant à cela, il jouait d'un brin de fil, volé à sa femme, comme il l'avait toujours fait depuis qu'il était enfant. Il récupérait alors des mèches dans l'atelier de son père, cordier réputé, et refaisait les complexes boucles qu'il voyait les marins utiliser. Il interrompit d'un geste le travail de son épouse.

« Il faudrait dire à Bachir de faire venir sa femme, Benedetta. Tu es fort grosse, et je ne veux pas risquer de perdre l'enfant.

--- Es-tu fol ? Dilapider notre bien à prendre valets alors que tu n'as plus de travail !

--- J'ai fort pourpensé cela. Nous avons de quoi payer servante au moins jusqu'à tes relevailles, que tu ne fatigues pas trop. Pour ma part, je pourrai prendre une part de nos avoirs et faire négoce. Tripoli est un port, et tous les navires ont besoin de bons et beaux cordages. Je m'y entends en cela, mon père me l'a fait rentrer dans la caboche à coups de torgnoles bien senties.

--- Je ne sais...

--- Moi si. Bachir m'aidera dans mon travail. Il parle les langues d'ici et sait suffisamment compter. De simple valet, il peut devenir une sorte de clerc pour moi.

--- Sait-il seulement écrire pour cela ?

--- Et moi donc ?, s'amusa Willelmo. Je sais juste signer de mon nom !

--- Alors il te faudra apprendre, que chacun sache qui est le maître en cet hostel. »

Willelmo réfléchit un petit moment, soudain plus grave.

« Tu parles de raison. Si j'ai espoir de faire négoce, il me faut pouvoir rédiger contrats et lettres. J'irai trouver à l'évêché un clerc qui puisse m'enseigner cela. Je ne suis pas plus idiot que ces tonsurés, après tout ! »

Benedetta se pencha soudain, puis, suspendant son geste, hésita à embrasser son époux sur la joue. Celui-ci lui attrapa la nuque et lui avala les lèvres en un baiser langoureux. Lorsqu'ils se séparèrent, il susura :

« Je n'ai plus qu'une jambe, mais je suis toujours un homme debout, Benedetta. Il ne sera pas dit que je ne sais nourrir les miens. »

Elle se releva, les joues enfiévrées, et tira sur sa cotte au ventre rebondi pour retrouver une contenance.

« Je n'ai jamais pensé le contraire, mon ami.

--- Tu n'auras plus à craindre que je prenne un mauvais coup, je me contenterai d'affronter d'obèses marchands et des artisans âpres au gain. Et sur les routes, qui oserait s'en prendre à un ancien soldat ? »

Il attrapa la cruche et le gobelet posé à côté de lui et se servit une large rasade d'un vin clair, au goût léger. Puis il entreprit de faire un nœud au motif alambiqué, soudain espiègle comme un enfant.

« Il était dit que je ne finirai pas chevalier. Mais rien ne m'empêchera d'en avoir la richesse ! »

### Tripoli, veillée du samedi 7 mars 1153

À la lueur des lampes à huile, Willelmo additionnait et retranchait, vérifiant sans cesse ses totaux, de crainte de faire une erreur qui lui coûterait cher. Son assurance martiale et sa connaissance du travail de la corde lui avaient permis de se tailler rapidement une belle place dans la confrérie marchande, mais il était toujours inquiet de faire un impair dans la gestion de ses biens. Bachir s'était d'ailleurs révélé un assistant compétent et zélé. Il ne savait ni lire ni écrire, mais comptait fort bien de tête et parlait plusieurs idiomes locaux. Jusqu'à présent il s'était montré d'une fidélité sans faille. Et son aide à la mort de Benedetta avait grandement soulagé Willelmo.

L'embauche de son épouse Massina lui avait permis de ne pas avoir à se soucier des enfants. Il les aimait, forcément, mais ne se sentait guère à l'aise. C'était une affaire de femmes que d'éduquer les tout petits.

Oberto, à bientôt dix ans, pouvait commencer à suivre un peu son travail, pour être formé, mais il était encore trop jeune et fragile pour être vraiment utile. Et le benjamin, Ansaldo, né en prenant la vie de sa mère, avait eu besoin des soins d'une nourrice, dénichée par la vaillante Massina. Elle épiçait un peu trop les plats et ne savait pas coudre les vêtements qu'il affectionnait, mais en dehors de cela, il en était satisfait. Irrité par ces chiffres qui ne voulaient pas trouver leur place, il envoya promener les papiers et la tablette d'un geste agacé, manquant de renverser la lampe. Il tourna sur son escabeau et laissa son regard dériver dans la pièce. Il avait aménagé son ancienne chambre pour en faire son cabinet de travail. Un châssis de verre fermait la fenêtre, et on pouvait l'obturer d'un volet de bois, ou plus fréquemment d'un simple rideau aux motifs géométriques bleus et blancs.

Sur le sol, une natte tressée cachait les carreaux de terre cuite abîmés tandis que les pans de plusieurs étoffes légères recouvraient de couleurs pastels les parois chaulées. Une table, deux bancs et un escabeau s'appuyaient contre le mur opposé au lit, et un coffre lourd, de chêne ferré prenait place sur le côté. C'était là qu'il mettait à l'abri ses documents précieux, l'argent qu'il amassait. Une autre huche, de pin, et aux ferrures moins épaisses, servait à ranger ses habits sous la fenêtre. Il se leva, claudiquant sur sa jambe de bois. En fin de journée, quand il avait trop marché comme cet après-midi, son moignon était douloureux d'avoir frotté dans son emplacement. Il tira le rideau et ouvrit le châssis, s'accoudant au chambranle pour humer le vent du large. L'odeur d'embruns lui rappela des souvenirs. La lune gibbeuse dessinait de craie blanche les toits environnants, les mâts des navires amarrés, la crête des vagues. Il ne faisait pas froid. Des cris éclatèrent non loin, dans une langue inconnue de Willelmo. Deux soulards qui s'invectivaient, peut-être le couteau à la main. Il sourit, soudain replongé quelques années en arrière, quand il était de ces soudards, aventuriers sans crainte du lendemain. Le bruit de la porte de sa chambre le fit se retourner. Bachir le salua poliment, mais sans obséquiosité. C'était un homme libre, qui se montrait serviable sans servilité, ce que Willelmo appréciait grandement. Il détestait la faiblesse et la veulerie, autant chez les autres que chez lui. Cela ne lui inspirait que mépris. C'était peut-être pour cela qu'il se prenait à tant regretter Benedetta et son caractère ombrageux et fier. Il n'était pas un de ces damoiseaux qui chantaient leur amour, pleuraient leur bienaimée. Pourtant il sentait comme un vide dans ses entrailles quand il rejoignait son lit, seul.

« Qu'y a-t-il, Bachir ?

--- Les enfants en appellent à toi, mestre Willelmo. Ils espèrent un baiser avant de dormir.

--- Que ne sont-ils déjà au lit ?

--- Tu leur avais accordé soirée de contes, mestre. C'est pour ça que Massina a fait les awaïmets[^121]. »

Willelmo grommela ce qui fut pris pour un acquiescement par Bachir. Il se retira discrètement.

### Côtes du royaume de Jérusalem, midi du jeudi 10 mars 1155

La voile du *Fortissimus* était tendue sous une belle brise du nord-ouest, les côtes défilaient à bonne vitesse, les plages succédant aux affleurements rocheux. Ils avaient dépassé Jaffa peu auparavant et longeaient désormais le sud du royaume. C'était là que Willelmo craignait le plus une attaque de brigands égyptiens. Il avait suivi les conseils de plusieurs contacts, qui lui avaient expliqué que la flotte égyptienne était très demandeuse de fournitures. Il était mal à l'aise avec l'idée de commercer trop ouvertement avec des ennemis, et tirait nerveusement sur un morceau de ficelle dans l'espoir d'en faire une pomme de touline. Le goût des embruns le ramenait quelques années en arrière, lorsqu'il parcourait les mers comme un chasseur et non comme une proie.

Il s'essuya le visage à l'aide d'un rafraf, une des longues traines de son turban. Il avait adopté ce couvre-chef quelque mois plus tôt, de façon à la fois de protéger sa calvitie grandissante du soleil, mais aussi pour y celer des choses. Bachir lui avait montré comment dissimuler documents et petites monnaies dedans. En outre, il lui avait expliqué que cela amortissait efficacement les coups, sans pour autant trahir un usage militaire. Par la suite, il avait découvert que les rafrafs, pouvaient servir de nombreuses façons, dont la moins correcte était de s'essuyer les mains, le visage, voire le nez pour les plus malpolis.

Bachir le rejoignit sur le pont, portant leur repas, du pain plat garni de purée de pois chiches aux herbes. Caboter permettait de s'arrêter autant qu'on le voulait pour refaire le plein de produits frais, et on mangeait donc aussi bien qu'à terre. Une outre de vin, certainement dilué, accompagnait l'en-cas. Le valet aida son maître à s'asseoir, opération toujours malaisée avec la jambe de bois, d'autant plus sur un navire sans cesse en mouvement.

« Le *patronus* m'a dit qu'il espère faire brève escale à Ascalon. Il n'y a guère de marchandies à y décharger. Nous devrions apponter à Alexandrie d'ici une semaine.

--- Je me demande encore si je ne devrais pas vendre mes filins et cordes à Ascalon et, de là, repartir au nord.

--- Nous avons quelques contacts en la cité égyptienne. Al-Baghdadi nous a donné le nom de trois de ses associés qui achèteront certainement à bon prix. Avec ses lettres, nous n'aurons nul souci à trouver preneur. Il a très bonne réputation.

--- De cela je ne doute guère. Mais commercer avec les infidèles, j'y répugne tout de même.

--- Ils sont peut-être musulmans, mais ils doivent pêcher, naviguer, commercer comme nous. Ils achètent beaucoup d'accastillage, de voiles et de bois, de poix et de corde. À très bon prix, car leur nation en est pauvre. En tant que marchand, il est juste que nous leur vendions, non ?

--- Je trouve cela un peu trop simple. Ce sont des adversaires que j'ai combattus le fer en main durant des années. Et là, je vogue vers eux pour les assister.

--- Tu sais, maître, moi je suis né ici, au Levant, et suis bon chrétien. Seulement, je ne peux me permettre de haïr le musulman, car c'est mon voisin. D'ailleurs le vieux comte[^122] lui-même a fait alliance un temps avec Norredin, quand il y trouva son intérêt. »

Willelmo opina, sans conviction. Jusqu'alors, les musulmans étaient des ennemis lointains qui habitaient des cités à piller dont il revenait les mains pleines de butin. Il n'avait pas envisagé qu'en Terre sainte, il vivrait auprès d'eux et, comme le disait Bachir, s'en ferait des voisins. Pas des amis, certainement pas. Mais un familier, avec un visage, un métier, une famille. Pouvait-on sereinement faire comme si de rien n'était ?

### Tripoli, après-midi du lundi 6 février 1156

Willelmo somnolait, tandis que ses doigts s'agitaient, occupés à faire et défaire un nœud de ride jamais achevé. Il employait pour cela un échantillon de corde qu'il avait prélevé chez un de ses fournisseurs.

Il avait entrepris de rédiger seul une lettre pour un ami resté à Gênes, dont il se disait qu'il ferait un bon garde du corps lors de ses voyages. Enrico était plus jeune, mais avait connu les campagnes en Espagne, et ils avaient partagé bien assez de coups durs. Certainement que Maza se moquerait de la panse rebondie de son ancien compagnon et de sa façon de gérer ses affaires. Mais il était un homme fidèle à sa parole et un soldat compétent. Willelmo avait appris qu'il se faisait payer comme arbalétrier sur les navires, un poste bien rémunéré mais toujours soumis aux aléas.

Willelmo lui parlait donc de la douceur de vivre au Levant, de la fortune qu'il amassait dans le négoce, sans plus avoir à tirer la lame. Il gardait sous silence la mort de Benedetta, car il ne savait pas si Maza approuverait son choix de se remarier avec Jezabel. Sûr qu'il trouverait la jeune femme à son goût, avec ses longs cheveux bruns et son visage fin. Mais épouser une Levantine, même chrétienne, avait été une décision difficile à prendre. Willelmo ne regrettait nullement. La fille de Massina et Bachir était très dévouée à son foyer, adorait ses enfants et espérait bien lui en donner d'autres.

Elle mettait autant d'ardeur à tenir la maison qu'à le satisfaire sous les draps. Il l'avait embauchée au début pour assister sa mère dans les tâches domestiques, vu qu'il était de plus en plus souvent absent pour son négoce. Puis, à force de la fréquenter, il s'était surpris à suivre du regard les courbes qui se dessinaient sous les amples robes, à s'interroger sur ce que recelaient ces épais vêtements. Elle n'avait jamais minaudé et avait toujours fait en sorte de ne pas à avoir à repousser les avances d'un maître concupiscent. Puis il s'était aperçu qu'il hésitait à profiter d'elle, car il avait de l'estime pour le père, qu'il traitait plus comme un associé que comme un employé. Il lui avait même concédé, avec le temps, une part sur certains bénéfices et délégué le travail en rapport. Et puis ses enfants appréciaient fort Jezabel, qui les gâtait un peu trop à son goût lorsqu'il était absent.

Vu qu'il était nouveau dans la cité de Tripoli et pas encore suffisamment riche, il n'espérait guère pouvoir s'attirer les faveurs d'une maison prestigieuse pour conclure un mariage avantageux. D'autant qu'il était veuf avec déjà deux garçons, héritiers possibles. La famille de Bachir était chrétienne, d'un culte qui reconnaissait l'autorité du pape, donc rien ne s'opposait à leur union. De plus, Jezabel insista pour se faire baptiser selon le rite catholique avant le mariage. Elle n'attachait en fait que fort peu d'intérêt à la chose religieuse et se conformait aux usages latins avec aussi peu d'enthousiasme qu'elle avait manifesté envers la foi maronite.

Pour installer les siens confortablement et montrer son enrichissement à tous, il avait emménagé dans une belle demeure avec un jardin, et une terrasse d'où il avait une jolie vue sur la mer. De plus, il bénéficiait d'assez de pièces où accueillir ses associés et entreposer des marchandises. En ce moment, il hébergeait Abdel ben Mokhtar al-Tabari, un de ses contacts damascènes avec lequel il entretenait beaucoup de relations. Il investissait pas mal dans les productions de cuivre et de céramique que celui-ci achetait dans sa ville avant de les acheminer un peu partout, jusqu'à l'Ifriqiyah. Il avait même proposé à Willelmo de l'accompagner un jour dans un de ses voyages, qui pourrait le mener, qui sait, en Espagne. Willelmo avait toujours été discret sur son passé, indiquant qu'il avait été marin et soldat. Mais il renâclait un peu à l'idée d'aller en invité là où il avait débarqué dans le feu et le sang.

Il pencha le nez vers sa lettre, ponctuée d'hésitations et de taches d'encre. Il avait bien progressé mais frère Benedetto, le chanoine qui lui apprenait à écrire, à lui et à ses enfants, froncerait sûrement les sourcils devant un travail aussi bâclé. Il soupira d'aise et se frotta le crâne, désormais orné d'une simple couronne de cheveux coupés courts, puis avala une longue gorgée de vin de Chypre, un luxe auquel il avait pris goût. Puis il reprit la plume, en trempa l'extrémité dans le petit encrier. Il fallait à tout prix finir cette maudite lettre avant que le *Rubens* ne mette à la voile dans les jours qui venaient, s'il voulait espérer voir Maza avant la fin de l'année. ❧

### Notes

Il est malaisé d'entrer dans le quotidien de ceux qui, depuis l'Europe, faisaient le choix de s'installer au Moyen-Orient. Comme le colonialisme plus tardif l'a bien démontré et ainsi que Ronnie Ellenblum l'a écrit, les sociétés pouvaient se juxtaposer, vivre à côté l'une de l'autre sans se mélanger ou s'influencer.

Malgré tout, la promiscuité incitait immanquablement certains à tisser des liens. Sans aller jusqu'à l'interconfessionnalité, on connaît des unions entre Latins et Levantins. Les conversions étaient en outre possibles. D'autre part, l'installation dans ces nouvelles terres se faisait aussi en abandonnant les rapports anciens que l'on avait dans son territoire de naissance. Si, dans les chartes, on nomme parfois certains depuis leur origine européenne (le Bourguignon, le Provençal...), on voit également apparaître des toponymes plus proches, originaires de Terre sainte. Des réseaux se créaient entre les nouveaux arrivants et ceux qui étaient là depuis plus longtemps, enfants issus des premières vagues d'immigration.

En outre, on trouve dans les textes, comme ceux d'Usamah ibn Munqidh, la mention de Latins très orientalisés. L'émir cite l'exemple d'un Franc antiochéen qui refusait le porc à sa table, pour ne pas indisposer ses invités musulmans. On ne peut négliger l'hypothèse que ce fin lettré ait souhaité démontrer ainsi la supériorité de sa propre culture, qui finissait à s'imposer à tous mais il est possible que cela fasse écho à une réalité. On trouve bien dans l'inventaire après décès d'un riche marchand vénitien (Gratianus Gradenigo) des tenues désignées comme de type oriental. Le commissaire priseur n'avait aucun intérêt à falsifier la provenance du vêtement dans sa liste. Enfin, certains Latins savaient s'exprimer dans les idiomes locaux, pouvant se passer d'interprètes lors des négociations (comme Renaud de Sidon, bien connu pour cela).

### Références

Louise Buenger Robbert, « Twelfth-Century Italian Prices: Food and Clothing in Pisa and Venice », *Social Science History*, vol.7, n°4 (Autumn), 1983, p. 381-403.

Paul M. Cobb, *Usama ibn Munqidh. The book of Contemplation. Islam and the Crusades*, Londres : Penguin Books, 2008.

Ronnie Elleblum, *Frankish Rural Settlement in the Latin Kingdom of Jerusalem*, Cambridge University press, Cambridge, 1998.

Fille de Magdala
----------------

### Césarée, matin du samedi 2 juin 1156

La fraîcheur de la terre battue remontait le long des jambes de Layla, depuis ses pieds nus. Vêtue d'une robe mille fois rapetassée et réparée, trop courte par-dessus sa fine chemise de coton, elle sautillait sur place pour se réchauffer.

Elle aurait préféré se trouver dehors, dans la cour dont elle voyait la lumière pénétrer depuis l'autre pièce, où maître Garin stockait les marchandises à vendre. Elle filait sans s'y appliquer, rompant régulièrement le brin ou faisant des bourres auxquelles elle ne prêtait aucune attention. Une fois encore, sa fusaïole tomba au sol alors qu'elle vagabondait, l'esprit absent.

Lizbet, une grande brune tout en bras et jambes, guère plus épaisse qu'une limande à force d'avaler le brouet insipide qui leur servait de repas deux fois par jour, gloussa en réaction. Beleite, dont la bonne nature était durement éprouvée par le caractère irascible de Layla et sa mauvaise volonté évidente, soupira de façon ostentatoire. Elle était mieux habillée que les deux autres, et clairement bien nourrie, ses appâts féminins plus marqués lui permettant visiblement d'améliorer l'ordinaire.

« Tu vas nous attirer soucis à mal œuvrer ainsi, Layla...

--- Peu me chaut ! Je ne compte pas rester ici. »

Beleite secoua la tête.

« Et où iras-tu ? Les sœurs se sont occupées de toi, tu pourrais au moins leur rendre cette grâce. Maître Garin n'est pas mauvais baron. Il a déjà doté une de ces œuvrières.

--- Il a logé une de ses putains avec un de ses valets, tu veux dire . »

Lizbet gloussa à la réponse, ce qui fit également rire Beleite, un peu de mauvaise grâce.

« Si fait, tu parles de vrai. On dit que Durant a si belles cornes qu'on y peut crocher son mantel...

--- Et qu'Hersent y met son linge à sécher .

--- Au plus fort de l'été, les chiens aiment à dormir à leur ombre, fort étendue . »

Les rires repartirent de plus belle, ce qui finit par attirer Garin. Sa voix forte tonna sous les voûtes de la pièce contiguë.

« Ho là, pucelles. J'espère que le fil s'enroule aussi vite que vos gorges déroulent tous ces rires . »

Son imposante silhouette, enrobée d'une couche épaisse de tissus de prix, quelle que soit la saison, vint cacher la lumière. Il n'était pas très grand, mais généreux de carrure et de ventre, avec une barbe qu'il taillait une fois l'an, pour la sainte Barbe justement.

La cinquantaine passée, il était marié à une bigote qu'il s'employait à tromper avec une régularité qui lui avait valu le sobriquet de Garin la Sonnaille, rapport à son pendu souvent occupé à battre. Il avait néanmoins poursuivi de ses assauts son épouse suffisamment d'années pour en obtenir une marmaille aussi nombreuse que remuante, qu'il entretenait grâce à son prospère commerce de drap.

Il s'intéressait à tout ce qui pouvait finir en étoffe : achat et revente de toisons, cardage, filage, tissage, teinture. Il investissait partout, et était associé à la moitié des fabricants de la côte. En outre, cela lui donnait l'occasion de souvent voyager et d'embaucher des femmes, habiles à faire le fil disait-il, et à dresser son membre, murmurait-on.

Il n'était pas mauvais bougre, mais pouvait avoir la langue acide s'il n'obtenait pas ce qu'il espérait. Il n'était malgré tout pas rancunier et oubliait ses griefs aussitôt son désir comblé. Lorsqu'il pénétra dans la pièce, Lizbet et Beleite se levèrent, et Layla finit de ramasser sa fusaïole.

« Encore à répandre ton ouvrage, Layla ? »

Il soupira.

« Las, que vais-je faire de toi si tu n'es guère habile de tes doigts. »

Le regard qui caressa le corps maigre ne laissait aucun doute quant au fond de sa pensée.

« Je crois assavoir malgré tout que tu as la langue habile, cela pourrait plaider en ta faveur, ça, mignonne. »

Il gronda de contentement à sa blague et vint se coller contre Layla, palpant son bassin osseux d'une main gourmande.

« Bien dommage que tu aies le tétin si maigre. J'aime les jeunettes en chair... Mais on trouvera bien à t'employer, va. Je ne suis pas mauvais sire... »

Se tournant vers Beleite, il se mit à lui malaxer un sein comme pain en pétrin, puis émit un grognement qui se voulait de contentement avant de ressortir. Passant le chambranle, il ajouta, la voix redevenue sérieuse.

« Mets plus de cœur à l'ouvrage, petite, et tu auras du beurre dans ton brouet. On n'a rien sans rien dans la vie. »

Puis il s'éloigna d'un pas allègre, chantonnant pour lui-même :

« *J'ai outil fier et adroit tantôt courbe tantôt droit...* »[^123]

### Environs de Caco, matin du mardi 8 janvier 1157

La pluie avait redoublé à l'amorce de l'aube, achevant la nuit en véritable averse, après une bruine insidieuse qui avait apporté les senteurs salines de la mer toute proche. Le dérisoire abri de feuilles de palmes de Layla n'avait guère été efficace et son petit recoin de ruines était désormais de boue et d'eau.

Elle-même était trempée, grelottante sous ses vêtements de coton . Ses cheveux dégoulinaient, maintenant le froid et l'humidité dans son cou. Quelle que soit la position qu'elle adoptait, elle ne parvenait pas à se réchauffer. Elle s'était installée quelques semaines plus tôt à côté des vestiges de ce qui était vraisemblablement le tombeau de quelque personnage important. Mais le notable avait été oublié, et après avoir été amputé de ses plus belles pierres, le bâtiment finissait de s'écrouler saison après saison.

Les murs ne montaient guère plus haut que la taille, et tout ce qui avait pu avoir de la valeur avait été pillé. Un bosquet de palmiers s'y accrochait, près du lit d'un petit ruisseau asséché en été. Au plus fort de l'hiver, il gargouillait joyeusement. Au départ, la jeune fille était heureuse de sa découverte. Elle avait tenté de s'aménager un petit coin douillet, bien à elle, mais le vent et sa méconnaissance des lois les plus élémentaires de l'architecture n'avaient abouti qu'à un misérable appentis. Elle s'y pelotonnait dans les feuilles, espérant le sommeil, avec une butte de terre pour oreiller. Elle n'avait malheureusement aucun moyen de faire du feu, et aurait hésité à en faire de toute façon.

Elle vagabondait en bordure des champs cultivés et des jardins, cueillant fruits et légumes à la lueur de la lune et des étoiles. Souvent elle avait salivé en entendant des poules caqueter, à l'idée de gober un œuf, voire de croquer dans la viande juteuse. Mais elle se contentait de graines broyées, de tout ce qui pouvait croiser sa route tandis qu'elle cheminait par la campagne. Elle goûtait à la liberté depuis la première fois.

Née dans un casal misérable, où ses parents s'épuisaient pour un baron malfaisant, sire Robert, elle avait tôt appris à baisser la tête et le regard, à courber l'échine, quand il passait. Cruel et éternellement insatisfait, il aimait à entendre la badine craquer sur le dos des récalcitrants. Ses soldats, aussi malveillants que lui, brisaient les chevilles des captifs pour leur ôter l'envie de galoper au loin. Dès qu'elle fut en âge de marcher, on la mit au labeur. D'abord, à des tâches domestiques, puis lorsqu'elle fut adolescente, elle rejoignit les travailleurs des champs. Là elle épierrait, sarclait, arrosait, récoltait, pour engraisser et enrichir celui qui la récompensait d'insultes et de repas maigres, pris dans une masure branlante.

Ils étaient serfs, attachés à la terre et à son maître, guère plus estimés que des meubles. Sa mère lui avait enseigné que cela était dû à leur foi, qu'ils étaient des disciples du Prophète et du Dieu unique. Elle n'avait jamais rien compris à ça, ne voyait pas en quoi cela la concernait. On lui disait qu'elle était une femme et qu'elle devait obéissance à son père, qu'elle était impure et qu'elle était punie pour ses fautes. Un jour viendrait où Allah exterminerait tous les malfaisants comme le sire Robert, et alors elle serait récompensée d'avoir suivi la vraie foi. En attendant, elle maugréait et accomplissait les rites qu'on lui indiquait, mais sans jamais vraiment y comprendre quoi que ce soit. Elle avait vu sa mère mourir de maladie après s'être tuée à la tâche, affaiblie par des grossesses à répétition.

Son père, fier et autoritaire malgré son assujettissement, n'avait jamais manifesté le moindre chagrin et avait épousé sa belle-sœur, qui vivait avec eux depuis toujours. Rien n'avait vraiment changé, et par la suite Layla s'était demandé de qui elle était la fille. Dès qu'elle en eut l'occasion, elle se joignit à un groupe de fugitifs et s'efforçait depuis lors de survivre. Récupérée par des nonnes qui lui avaient offert un repas chaud, elle avait été placée chez Garin pour y apprendre un métier, chichement dotée par la charité des moniales. Mais contrairement à Beleite, elle n'était pas faite pour le joug. Elle avait envie de sentir la brise, le soleil sur ses joues, de faire selon son désir comme les héroïnes des contes que sa soeur Rayya lui narrait quand elle était petite. Rayya, morte lors de son premier accouchement, en même temps que son enfant. Ils gisaient désormais dans une terre froide qui appartenait à un autre. Elle se secoua, tentant de se réchauffer par les frissons.

Le vent qui avait accompagné le jour blafard avait chassé les nuages, mais lorsqu'elle se leva, elle en ressentit la morsure avec d'autant plus de douleur. Elle ramassa les chiffons qu'elle avait noués en un sac, où elle gardait quelques réserves de nourriture puis se décida à aller chercher de quoi manger. L'activité lui ferait du bien. Le bruit d'une chevauchée la fit hésiter un instant, et elle tendit le cou pour voir par-dessus les tronçons de murs. Une vaste compagnie avançait sur le chemin en provenance de Caco. Des chevaux, montés par de fiers soldats en armes, dont certains portaient une cape blanche, encadraient des mules et des poneys, ainsi que quelques chameaux. Cette riche caravane prenait la direction du sud. Elle contempla avec envie les balles de marchandises soigneusement empaquetées, les filets de provisions pendant sur les flancs. Faire la route dans les traces de cette troupe lui garantirait la tranquillité. Nul malfaisant n'oserait traîner aux alentours d'une pareille escorte armée. Sans même jeter un dernier regard à son abri précaire, elle se mit en marche, soudain ragaillardie.

### Calanson, soir du lundi 22 avril 1157

Tout d'abord, Layla rampa, puis, ramassant d'une main hésitante ses habits déchirés, clopina avec difficulté jusqu'au muret d'un jardin, où elle s'appuya. Le souffle court, des larmes plein les yeux, elle ne se sentait que plaies et bosses, crasse et sang.

L'homme qui avait abusé d'elle s'était endormi dans le caniveau, comme l'animal qu'il était, et ronflait bruyamment, sur le flanc, son canif encore entre ses doigts gourds. Tremblante, la jeune fille ne savait pas quoi faire. Une douleur déchirait ses entrailles à chaque mouvement, une nausée la gagnait. Elle n'entendit personne approcher.

« Sainte Vierge ! » s'exclama une voix rauque.

Layla tourna la tête et aperçut plus qu'elle ne vit le petit groupe. Un moine habillé de noir, quelques femmes, des ombres dans la lumière de leur lanterne.

Le clerc s'agenouilla vers le ronfleur et soupira, puis rejoignit à pas lents de la jeune fille, déjà entourée.

« Que t'arrive-t-il, mon enfant ? Est-ce tien compagnon qui gît là ? »

La question hypocrite déchaîna la colère d'une brune dont les yeux clairs brillèrent de rage contenue.

« Que me chantes-tu, moine ? Tu vois bien qu'il a forcé la gamine . »

La réponse, plus que le ton, choqua le moine, qui n'avait guère envie d'être confronté à ce genre de choses. Le dégoût que cela lui inspirait lui dessina un visage horrifié. On ne savait dire ce qui l'effrayait le plus, de la mention d'un tel acte ou du fait que cela ait été pris de force.

« Il faut en appeler à la sergenterie, on ne peut laisser cela...

--- Oui-da, il faut soigner cette petite. Je la reconnais, elle traînait aux alentours du camp, à rendre menus services. »

Le soûlard remua dans son sommeil et se tourna, dans un grognement sonore qui les fit tous sursauter. La petite brune qui avait pris les choses en main le désigna d'un mouvement de tête.

« Il faudrait s'assurer de lui, qu'on puisse le juger.

--- Mais qui donc..., commença le moine, soudain stoppé par le regard impérieux de la femme.

--- Allons, moine, ne me dis pas que tu connais si mal le siècle ! »

Se penchant vers Layla, elle l'aida à se relever, arrangeant du mieux qu'elle le pouvait ses maigres effets. Elle déposa sur ses épaules une couverture et lui susurra quelques mots de réconfort, sans obtenir de réaction. L'adolescente agissait comme une somnambule. Leur arrivée fit grand bruit dans le petit hôpital et le frère en charge de l'accueil refusa catégoriquement qu'on mette Layla avec les pèlerins et voyageurs.

À sa mine dégoutée, c'était plus par répulsion envers la jeune fille que par souci de la protéger. Elle eut néanmoins la chance de se voir affecter une pièce, débarras de meubles et d'outils où un lit sommaire fut installé, simple paillasse sur quelques planches.

Puis les clercs laissèrent les femmes prendre en charge Layla, tandis qu'eux s'occupaient de garder le violeur à l'œil, le temps de statuer sur son sort. Son crime était évident, mais la jeune fille n'avait personne pour demander réparation, et le coupable était un soldat dont le baron n'apprécierait peut-être pas que des moines le tiennent captif.

Layla avait un peu retrouvé ses esprits et s'était réfugiée dans un mutisme buté. Elle avait accepté les habits et le repas chaud avec empressement, et se pelotonnait sous les couvertures. À ses côtés, la brune qui l'avait prise en charge, et qui l'avait nettoyée avant de l'aider à se vêtir était assise, cherchant à la faire parler.

Layla se contentait de fermer les yeux, ou de fixer la mèche de la petite lampe à graisse sur l'escabeau à côté de son lit. Devant l'insistance de la femme, elle consentit à la regarder, s'efforçant à mettre du sens sur ce qui sortait de ses lèvres. Elle fronçait les sourcils, tentait de comprendre. Puis elle finit par articuler péniblement :

« Que va-t-il m'arriver ?

--- Je ne sais de sûr, mais il nous faut assavoir ce qui s'est passé. »

Layla se mordit les lèvres et remonta la couverture sous son menton, soudain renfrognée. La femme insista .

« Je m'appelle Anceline, je suis comme toi, pauvresse sans feu ni lieu. Si le gros dit que tu étais avec lui contre monnaie, il n'aura que petite amende pour t'avoir frappée.

--- Je ne suis pas... » lança violemment Layla, indignée par le sous-entendu.

« Ai-je dit cela ? Et quand bien même, je le suis bien, moi. »

Stoppée dans sa fureur par cette surprenante confidence, Layla fronça les sourcils et examina de plus près la femme face à elle. Elle n'était peut-être pas si âgée que ça, mais il lui manquait plusieurs dents, et sa chevelure s'ornait d'argent. Des rides et une couperose avilissaient des traits fins, un sourire las. Les mains étaient crevassées, les ongles courts sales et les veines saillaient sur des doigts osseux.

« Je fais la lavandière pour les soldats, mais parfois certains se sentent bien seuls, alors je les câline. Mais je garde toujours canivert[^124] caché en mes robes, au cas où l'un d'eux me croirait à sa pogne.

--- Qu'est-ce qui va m'arriver ? »

Anceline se rapprocha et parla plus doucement, comme à une enfant.

« S'il est prouvé que tu n'es pas telle que moi, on va l'escouiller et le mettre à l'amende. Et toi tu finiras nonnette.

--- Nonnette ?

--- Au couvent, à prier pour le salut de ton âme, après ce péché, et pour le sien, car c'est là bonne charité . »

La remarque fit s'esclaffer Layla, de fureur autant que de dérision.

« Mais je n'ai rien fait, c'est lui qui...

--- Je me doute bien, mais entre ceux que notre fendu rend fou de désir et ceux qu'il dégoûte, il est bien malaisé de faire sa route ici-bas. Moi j'ai choisi de m'en servir pour m'affranchir de tous ces bâtards . »

Elle caressa avec douceur la main de la jeune fille.

« Ils ferment la porte avec une barre, de peur que tu ne t'échappes. Je viendrai te voir encore demain, et en repartant, je pourrai bien oublier de la tirer de nouveau. D'ici là, pourpense à cela, et remplis-toi la panse autant que tu le pourras. La suite, tu ne la devras qu'à Dieu, et à ta décision. »

Anceline se leva et sourit une dernière fois, avec gravité et tristesse. La petite flamme creusait les sillons de son visage à en faire une vieille femme, elle qui n'avait pas trente ans. Layla ne répondit pas lorsqu'elle sortit, non sans une ultime bénédiction.

### Plaine de la Bocquée, matinée du dimanche 15 septembre 1157

Le petit feu de camp fumait tant que Layla renonça à s'en occuper. Elle avait la flemme d'aller quérir du bois sec et saurait bien trouver un volontaire pour cela, en échange de quelques promesses.

Assise sur un caillou, elle reprisait maladroitement un vêtement, sans aucun soin. Elle détestait encore plus coudre que laver. Seulement, avec ses compagnes, elles étaient officiellement là pour servir de lingères aux soldats, et il fallait bien parfois donner le change. Le vent rabattit soudain vers ses oreilles les voix des fidèles à la messe célébrée en plein air dans une clairière voisine. Elle haussa les épaules, amusée. Elle ne comprenait guère plus cette croyance que celle de ses ancêtres, même si elle faisait semblant de la suivre, histoire qu'on ne la suspecte pas d'être une ennemie.

Elle avait vite intégré qu'il s'agissait avant tout de sauver les apparences dans un monde hypocrite. Elle n'était pas plus chrétienne que lavandière, mais savait en donner suffisamment l'illusion pour qu'on la laisse en paix. Coleite arriva en soufflant, un gros sac à l'épaule. C'était une solide fille aux longs cheveux blonds, au visage rond et au nez retroussé. Son imposante poitrine lui valait une clientèle nombreuse, et elle était la plus prospère de toutes, s'attirant une certaine jalousie des autres en général et de Layla en particulier, elle qui n'était que peau et os. Malgré tout, Coleite dirigeait plus ou moins leur groupe, et possédait le petit âne et la carriole avec laquelle elles transportaient leurs affaires à la suite des soldats. Elle lâcha le sac dans un soupir de contentement.

« J'ai pu acheter farine à bon prix, on aura de quoi se faire des galettes pendant un moment. J'ai avancé votre part. »

Layla hocha la tête et se leva, voyant un cavalier s'approcher de leur petit campement. Les lingères se regroupaient généralement pour se garantir les unes les autres, en cas d'intentions malveillantes de la part des soudards, mais elles se menaient entre elles une concurrence féroce. La survie était à ce prix. L'homme n'était pas un chevalier, sa posture et sa monture le trahissaient. Mais il était vêtu d'un haubergeon et portait une épée. Il avait donc certainement la bourse bien garnie, arbalétrier, ingénieur ou capitaine. Voyant que Layla se dirigeait vers lui, il s'approcha d'elle. Son visage rond avait connu des jours meilleurs et la barbe qui l'envahissait, de gris mêlé, le vieillissait fortement. Il était encore vigoureux et semblait habitué au commandement. Étrangement, il se recoiffa de la main comme un damoiseau avant de s'adresser à l'adolescente.

« Le bon jour à toi, fille. Je viens voir si d'aucunes lavandières auraient envie de venir à mon camp cette nuitée.

--- Cela se peut, y a-t-il beaucoup de linge à frotter ?

--- Tant et plus, de l'ouvrage pour demi-douzaine je dirai. Il y aura du vin pour les gosiers fatigués, aussi.

--- Je suis avec la Coleite, tu la connais ?

--- Non, mais je demande que ça » répondit l'homme avec un sourire grivois.

Il se gratta l'oreille, embrassant la maigre installation des femmes d'un coup d'œil.

« Venez en fin de journée, dans le camp du sire comte de Flandre Thierry.

--- On demande après qui ?

--- Gosbert, je suis un de ses capitaines. »

Layla s'amusa à faire une révérence moqueuse, ce qui arracha un sourire au cavalier. Puis, la saluant de la main, il s'éloigna vers le vaste camp des armées latines. Lorsque Layla revint auprès du foyer, elle constata que Coleite avait trouvé du bois sec et faisait repartir le feu.

« Y voulait quoi ?

--- On a du boulot pour la soirée.

--- Linge à gratter ?

--- Couenne à frotter. »

Coleite acquiesça et souffla sur les braises tandis que Layla s'asseyait pour reprendre son ouvrage. Suspendant son geste, elle repensa aux derniers mois. Finalement, elle tenait sa liberté, qu'elle espérait depuis tant d'années. Elle se l'offrait avec son sexe, ce sexe qui faisait d'elle une moins que rien, mais qui menait les hommes, et certainement le monde. Elle sourit soudain. Un jour, il faudrait qu'elle tente de séduire un chevalier, voir s'ils étaient aussi bêtes que les autres à l'espoir de battre pendu contre fendu comme le disait la chanson. ❧

### Notes

La condition féminine médiévale au sein des classes populaires est très difficile à connaître, car il est rare que les sources s'y intéressent, et cela se fait bien souvent via un prisme masculin. La pression familiale est néanmoins prégnante, peut-être encore plus que pour les hommes. Pour celles qui n'avaient nulle parentèle sur laquelle s'appuyer, il n'y avait guère d'opportunités, surtout si elles n'avaient jamais été mariées. Le statut de veuve pouvait en effet permettre une vie indépendante honorable, tandis qu'une jeune fille qui demeurait ainsi était montrée du doigt, sauf à appartenir à une congrégation religieuse.

Les différentes cultures en Terre sainte médiévale s'accordaient toutes néanmoins sur un fait essentiel : la femme était une incapable, une mineure, qui devait avoir des référents (masculins bien évidemment) pour avoir une existence décente. À l'opposé, les femmes qui accompagnaient les armées se voyaient couvertes d'opprobres, souillant les combattants de la foi selon certains clercs misogynes. Pourtant, on ne pouvait les interdire complètement, car elles rendaient de réels services aux hommes en campagne. En nettoyant le linge, et parfois les soldats, elles permettaient d'apporter une hygiène, minimale, au sein des troupes.

Bien évidemment, la prostitution qui y était liée était le sujet de tous les commérages. Cela constituait en fait un des rares moyens pour ces femmes de vivre en marge de la société où elles ne pouvaient exister par elles-mêmes. D'ailleurs, l'hypocrisie était telle que de temps à autre, les bordels étaient financés par des congrégations religieuses. La boucle était bouclée, les hommes reprenaient le pouvoir dans ce maigre bastion où pouvait s'exprimer une relative indépendance féminine.

### Références

Schaus Margaret (éd.), *Women and Gender in Medieval Europe, an Encyclopedia*, Routledge, New York & Londres : 2006.

Walter Wiebke, *Femmes en Islam*, Paris : Sindbad, 1981.

[^1]: Tripoli, actuellement au Liban.

[^2]: Thierry d'Alsace, comte de Flandre.

[^3]: Aujourd'hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^4]: Défaite des latins face aux forces de Nūr ad-Dīn, le 19 juin 1157, qui vit la mort ou la capture de nombreux dignitaires. Voir Qit'a *Chétif destin*.

[^5]: Manuel Comnène, empereur de Byzance à partir de 1143 (1118-1180).

[^6]: Renaud de Châtillon (v. 1120-1187), prince d'Antioche de 1153 jusqu'à sa capture en 1160.

[^7]: Louis VII (1120-1180), roi de France depuis 1137, qui a participé à la Seconde Croisade entre 1147 et 1149.

[^8]: Chapeau de feutre à fronton triangulaire, un des symboles du statut de guerrier chez les Turcs.

[^9]: Qilig Arslan (? -- 1192), sultan seldjoukide de Rum à partir de 1156.

[^10]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^11]: Conrad III de Hohenstauffen (1093-1152), roi germanique.

[^12]: Large robe aux manches amples.

[^13]: Administration royale en charge des finances, dirigée par le sénéchal.

[^14]: Voir Qit'a *Les croisés du Poron*.

[^15]: Désigne une pâtisserie qui peut être mangée les jours maigres, comme durant le Carême.

[^16]: Crêpes.

[^17]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^18]: Un sauf-conduit en cas de reddition.

[^19]: Joscelin II, comte d'Édesse (vers 1110-1159).

[^20]: Aujourd'hui Al-Bira, en Turquie.

[^21]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^22]: Manuel Comnène, empereur de Byzance à partir de 1143 (1118-1180).

[^23]: Tripoli, actuellement au Liban.

[^24]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^25]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^26]: Saint-Jean d'Acre, aujourd'hui Akko, en Israël.

[^27]: Instrument à vent, de la famille du hautbois.

[^28]: Aujourd'hui 'Ain al-Habis, dans les gorges du Yarmouk.

[^29]: Ou encore *fondouk*, *funduq*. Caravansérail pour les marchands.

[^30]: Guillaume de Malines, ancien archevêque de Tyr puis patriarche de 1130 à 1145.

[^31]: Publius Flavius Vegetius Renatus est un écrivain romain fameux au Moyen Âge pour ses textes sur la tactique militaire.

[^32]: « Marie a choisi la meilleure part, elle ne lui sera pas enlevée », évangile selon saint Luc, chapitre X, verset 42, qui fait allusion à Marthe et Marie, où Jésus Christ fait l'apologie de la vie contemplative.

[^33]: « En vérité je vous le dis, vous me cherchez, non parce que vous avez vu des miracles, mais parce que vous avez mangé des pains et que vous avez été rassasié », extrait de Jean, chap. VI, verset 26.

[^34]: Saint-Jean d'Acre, aujourd'hui Akko, en Israël.

[^35]: Afrique du Nord, du Maroc à la Lybie.

[^36]: Mercredi 1er juin 1188.

[^37]: Sourate 64, « La grande perte » verset 11.

[^38]: Marchand de blé en petites quantités, au détail.

[^39]: Profession libérale jurée ayant pour rôle, contre rétribution, de mesurer les grandes quantité de grains vendues par les marchands.

[^40]: Marécage.

[^41]: Terres incultes.

[^42]: Pierre dressée.

[^43]: Désigne les zones impropres à l'exploitation agricole.

[^44]: Buron, grange et étable parmi les pâtures.

[^45]: Nom médiéval de la ville de Dienne (Cantal).

[^46]: Vallée.

[^47]: Comtour ou comptour, désigne les barons les plus éminents d'Auvergne.

[^48]: Voir Qit'a *Les croisés du Poron*.

[^49]: Voir les notes

[^50]: Se disait des loups qui attaquaient les chevaux.

[^51]: Mont culminant à 1943 mètres d'altitude.

[^52]: Pain plat très fin des montagnes libanaises.

[^53]: Céréales concassées à la base de la mouné, les conserves traditionnelles préparées pour l'hiver.

[^54]: Boisson chaude lactée, épaissie de farine ou de gruau selon les régions, parfois arômatisée.

[^55]: « Ô profondeur de la richesse, de la sagesse et de la science de Dieu ! ». Extrait de saint Paul, Épître aux Romains 11, 33.

[^56]: Gormond de Picquigny ( ? -- v.1128).

[^57]: Cours d'eau dont la vallée passe juste au nord de Baisan.

[^58]: Conrad, roi d'Allemagne et Louis VII, roi de France, venus en Terre sainte pour ce qu'on nomme la Seconde croisade.

[^59]: Samedi 21 janvier 1156.

[^60]: Milice urbaine.

[^61]: Terme désignant les Européens.

[^62]: Officier.

[^63]: Rivière affluent du Danube, qui conflue à Belgrade.

[^64]: Hongrois

[^65]: Titre nobiliaire de la cour byzantine, initié au XIe siècle.

[^66]: Titre donné à l'empereur byzantin.

[^67]: Terme désignant *la* ville, à savoir Byzance.

[^68]: Aujourd'hui Bitola, Macédoine.

[^69]: Armure lamellaire.

[^70]: Alexis Ier Comnène (v.1058-1118), empereur byzantin (1er avril 1081-15 août 1118).

[^71]: Ambassadeur, messager byzantin.

[^72]: Plat à base de porc grillé à l'huile d'olive.

[^73]: Village du thème de Thessalonique.

[^74]: Le tjvjik, qui comprend aussi les reins, le tout poêlé.

[^75]: Pain plat traditionnel arménien.

[^76]: Dynastie turque islamisée, régnant alors sur une partie de l'Anatolie.

[^77]: Voir les notes pour la qualité du sucre.

[^78]: Le 28 avril.

[^79]: Large robe aux manches amples.

[^80]: Bâtiment où l'on règle les taxes douanières.

[^81]: Voir Qit'a *Le meilleur des fils*.

[^82]: Thierry d'Alsace, comte de Flandre.

[^83]: Prince de petite envergure.

[^84]: Calcul du calendrier, généralement religieux.

[^85]: Ensemble des quatres sciences dites mathématiques : arithmétique, musique, géométrie et astronomie. Cela fait suite au trivium, consacré aux trois arts : grammaire, dialectique et rhétorique.

[^86]: Voir la chanson « Le plux doux des mestiers ».

[^87]: Dimanche 30 août 1153.

[^88]: Longue extrémité du turban.

[^89]: « Aux yeux clairs ».

[^90]: Pour la Seconde croisade, en 1148.

[^91]: d'Ascalon.

[^92]: Samedi 12 décembre 1153.

[^93]: Pièce de réception.

[^94]: Boisson traditionnelle syrienne à base de lait fermenté.

[^95]: Lundi 26 avril 1154.

[^96]: *Père*, façon polie d'appeler un aîné père d'un garçon.

[^97]: Début décembre 1153.

[^98]: Mardi 22 juin 1154.

[^99]: Jeu similaire au backgammon, avec des règles et des positionnements un peu différents.

[^100]: Jus de pâte d'abricot.

[^101]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^102]: Le 19 mai cette année là.

[^103]: Le 24 juin.

[^104]: Enfant donné à un monastère pour en faire un religieux.

[^105]: Frédéric de la Roche, alors évêque d'Acre et futur archevêque de Tyr.

[^106]: Dernier verset du chap. 13 de la Seconde épitre de saint Paul aux Corinthiens : « Que la grâce de notre Seigneur Jésus-Christ, l'amour de Dieu et la communication du Saint-Esprit soient avec vous tous ! »

[^107]: Henri de France (1121-1175), fils du roi Louis VI le Gros, évêque de Beauvais (1149-1162) puis archevêque de Reims (1162-1175).

[^108]: Mardi 5 août 1096.

[^109]: Nom arabe de Jérusalem.

[^110]: Large robe aux manches amples.

[^111]: Unité de mesure du poids, variant d'un lieu à l'autre.

[^112]: Soie de qualité commune.

[^113]: Le vendredi, principal jour de prière, où l'on se rend volontiers à la grande mosquée de sa ville.

[^114]: Type d'écharpe pouvant servir de couvre-chef ou de ceinture.

[^115]: Jeudi 14 juillet 1099.

[^116]: Samedi 7 août 1260.

[^117]: Mur de toile qui sépare le *muharram*, partie réservée aux femmes du *raba'a*, destiné aux hommes.

[^118]: Il s'agit des invasions mongoles au Moyen-Orient, auxquelles les Mamlouks s'opposent.

[^119]: Emplacement le long du qata, côté femmes, où sont stockées la plupart des affaires qui ne sont pas utilisées quotidiennement.

[^120]: Plat traditionnel à base de mouton, de lait de chèvre et de riz.

[^121]: Petits beignets ronds à la fleur d'oranger.

[^122]: Raymond II de Tripoli avait fait alliance un temps avec Nūr ad-Dīn lorsqu'il a dû faire face aux prétentions d'Alphonse-Jourdain sur le comté, en 1149.

[^123]: Voir la chanson *Les traits du fier archer*.

[^124]: Canif, petit couteau.
