## Remerciements

La publication de ce premier lot de quatre recueils de textes courts Qit’a a demandé beaucoup de temps et d’énergie au membres du groupe Framabook, je tiens à les en remercier, Mireille en tête, avec qui les échanges autour de corrections proposées ont toujours constitué d’agréables moments.

*À la mémoire des exclus de la Mémoire*
