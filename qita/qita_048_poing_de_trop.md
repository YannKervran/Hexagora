---
date: 2015-10-15
abstract: 1158. D’un caractère toujours aussi emporté, Droin se voit contraint d’abandonner ses projets d’établissement en Terre sainte. Sa fuite jusque dans les confins du royaume va le mener au-delà de tout ce qu’il aurait pu imaginer.
characters: Droin le Groin, Fulbert, Evrard, Habib, Sayette, Eustache, Galien, Hugues, Nijm
---

# Poing de trop

## Jéricho, midi du vendredi 27 juin 1158

Attrapant tout ce qui lui tombait sous la main, Droin enfournait ce qu’il pouvait dans une grosse besace de cuir. Vêtements, écuelle, savon, chaussures, tout était bon. Il chercha frénétiquement la clef du petit coffre qu’il glissait sous son lit et fit jouer la serrure. À l’intérieur toute sa fortune se résumait à une poignée de pièces d’argent, un couteau de bonne taille et quelques bijoux, qui tenaient plus de la verroterie que des chefs-d’œuvre d’orfèvrerie. Il sursauta quand il entendit quelqu’un entrer dans la petite pièce. Il se retourna, hésitant à brandir la lame. Puis reconnut Fulbert, le jeune qui partageait la chambre.

« Que fais-tu, compère ? Tu devrais aller t’expliquer plutôt.

— Et risquer amende ou pis encore. Plutôt prendre le large ! »

L’idée horrifia le jeune, qui en laissa pendre sa mâchoire avant de répondre.

« Tu es fol ! On va te percer le poing d’un fer chaud si tu fuis.

— Et me le couper si je reste !

— On ne sait ce que la Cour dira.

— Tu parles, je suis là depuis quelques mois à peine, Evrard est né ici, et se trouvait sûrement déjà tonsuré au sortir du con de sa mère. Il aura beau jeu de trouver deux témoins jurés. »

Fulbert s’assit sur le lit, regardant son compagnon s’activer.

« Tu pourras lever ses plèges et prouver ton bon droit.

— Risquer ma vie pour si misérable étron, peu me chaut. Je décampe et ils ne me verront plus jamais. »

Fulbert soupira, désolé. Il aimait bien Droin malgré son sale caractère. Ils partageaient une petite pièce aux abords des jardins qu’ils entretenaient pour les moines, sous le couvert de la verdoyante oasis. En arrivant là, Droin avait cru d’abord au Paradis, puis il avait découvert le rythme infernal du labeur exigé de lui. Evrard était le cellérier qui tenait la badine pour les faire travailler, et il avait le verbe haut, si ce n’était la main leste. Il se vengeait volontiers sur le dos des quelques manouvriers musulmans parfois réquisitionnés pour les corvées les plus pénibles. Jusqu’à ce que son nez rencontre le poing de Droin, dans un fracas de dents et d’os brisés. La douleur irradiait encore ses phalanges, jusqu’au coude. Il n’avait pas mis une telle rage dans un coup depuis des années.

« Et ta soudée ? Tu vas la perdre, en partant ainsi.

— Les *bons* moines sauront en faire adroit usage. Ils ont si crochus qu’ils pourraient labourer des pierres sans s’y casser un ongle. Que leur monnaie les emporte aux Enfers ! »

Il boucla précipitamment son sac et se releva, l’assujettissant sur son épaule. Il empoigna le bâton avec lequel il était arrivé, quelques semaines plus tôt. C’était la première fois qu’il devait fuir ainsi, mais n’avait nul regret. Toute son enfance, il avait vu son père courber l’échine devant plus riche, plus puissant, plus fort. Mais il était fait d’un autre métal, comme Ernaut, son ami, avec lequel il s’était tant bagarré, petit. Personne ne ferait de lui un serf. Il posa une main amicale sur l’épaule de son compagnon.

« À Dieu te mande, compaing. Puisses-tu réussir là où j’ai échoué et finir laboureur avec belle famille.

— Prends garde à toi, compère Droin. Je prierai pour ton âme.

— Fais donc ça, oui. Elle en a bien besoin. »

Puis il poussa la porte, inondant de lumière les murs chaulés, le sol en terre battue, les deux galetas et l’étagère branlante, le toit en feuilles de palmier. Il traversa la courette où ils entreposaient les chariots à bras, le bois d’oeuvre et sauta par-dessus le muret de clôture. Sans un dernier coup d’œil derrière lui, il prit la route sous le couvert de la palmeraie, vers le sud, cherchant les reliefs qui longeaient la Rivière du Diable[^rivierediable]. Il espérait bien échapper ainsi aux recherches. Les moines du Mont de la Tentation n’allaient certainement pas faire preuve d’indulgence. S’ils étaient prodigues des bienfaits du Seigneur dans la vie à venir, ils n’étaient guère enclins à faire preuve de générosité, surtout avec un domestique rebelle.

## Abords du Fier, matin du mardi 1er juillet 1158

Droin était un peu vermoulu des nuits passées avec des cailloux pour tout matelas. Mais il se sentait plutôt bien. Il retrouvait la sensation de son départ de Vézelay, la liberté de destination en plus. Il avait juste l’estomac dans les talons. Ne connaissant pas les plantes de Terre sainte, il n’avait pas avalé grand chose depuis sa fuite de Jéricho. Il s’était également abstenu de voler quoi que ce soit dans des jardins, pour éviter de se faire remarquer.

Mais la faim le tenaillait désormais cruellement. Assis dans un bosquet de broussailles, il fouillait dans ses affaires et déroula un petit morceau de tissu rouge, un peu fatigué. Il sourit en admirant la croix que sa mère avait cousu sur son épaule avant son départ, et qu’il avait conservée dans une bourse. Il regrettait juste de n’avoir pas de quoi la recoudre histoire de se faire passer pour un pèlerin. Il vérifia également qu’il avait toujours la licence de son évêque, qui attestait de son statut, et qu’il avait fait viser par les chanoines du Saint-Sépulcre. Il y était écrit qu’il était parti en bon chrétien, avec la bénédiction des autorités, et qu’il était parvenu à son but. Il ne savait pas lire ces lignes, mais les regarder lui procurait un apaisement. Les deux sceaux qui y étaient attachés lui faisaient l’impression d’avoir une charte d’importance sur lui.

Son ventre gargouilla, désireux de connaître meilleur repas que la veille. Droin se frotta le visage.Il lui faudrait aussi trouver de l’eau. Il n’avait pas de calebasse ni d’outre et la soif le tiraillait parfois plus que la faim. Sur les reliefs face à lui, au levant, un village de pierres jaunes s’étirait aux abords d’une tour forte, dont la bannière pendait tristement. Le soleil avait brûlé les herbes, et seuls quelques arbres persistaient à ponctuer de vert les collines poudreuses, autour des zones d’où émergeait une source.

La route était assez fréquentée, plus que ne l’aurait cru Droin étant donné sa position loin au sud, aux confins du royaume avait-il pensé. Il espérait malgré tout être assez loin du bras armé des moines. Il se mit debout, épousseta sa tenue et s’étira avant de prendre ses affaires et de se remettre en route. Le village qu’il apercevait pourrait peut-être l’accueillir un temps.

Les champs avaient été moissonnés depuis des semaines et on ne semblait guère faner, vu la sécheresse. Des courtils s’attachaient aux maisons étagées, aux austères façades percées de rares et discrètes fenêtres. Des troupeaux de chèvres paissaient aux alentours, arrachant la maigre végétation qui tentait de survivre dans cet austère paysage.

Tout en grimpant vers le casal, Droin croisa quelques paysans, des Syriens à la mine sombre, qui ne lui prêtèrent pas attention. Lorsqu’il se présenta à la forteresse, il réalisa combien elle était modeste. Les moellons n’étaient guère équarris, et le mortier se pulvérisait par endroit. On était bien loin des orgueilleux châteaux des plaines littorales. Il n’avait d’ailleurs toujours pas identifié la bannière censée flotter sur le mât, qui s’obstinait à pendre mollement. Il frappa plusieurs coups sur la porte barrée, attirant un des gardes, qui passa sa tête enturbannée par-dessus le parapet.

« Que veux-tu, voyageur ? »

L’homme s’exprimait avec un fort accent, et semblait natif de la région. Droin estima que cela pouvait être à son avantage.

« Je suis humble marcheur de Dieu, en quête d’un lieu où apporter le soutien de mon bras pour le combat contre les ennemis de la Foi. »

Il fouilla dans son sac et en exhiba le document scellé, qu’il agita comme un laisser-passer. Le garde disparut et, après un bon moment, un des vantaux de la porte s’ouvrit. Derrière se tenait un grand échalas au teint bistre, le nez pelé par les coups de soleil. Il était vêtu d’un thawb négligé, mais était indiscutablement européen d’origine. Mâchonnant un cure-dent qu’il faisait passer d’un coin de sa bouche à l’autre, il semblait sortir du lit. Il bailla avant de saluer d’un signe de tête.

« Habib dit que t’es un soudard ? »

Il semblait soudain un peu circonspect quant à ce point, découvrant un errant comme il en voyait tant, même si la tenue, chausses et cotte, était indubitablement latine. Droin hésita un instant mais tenta de pousser son avantage.

« À dire le vrai, je suis marcheur de Dieu mais j’ai désir de combattre pour la vraie Foi. Je viens donc batailler à vos côtés ! » affirma-t-il tout en agitant son document.

Le garde y jeta un coup d’oeil sans qu’il soit possible de dire s’il y comprenait quelque chose.

« C’est le sire Gaston qui te mande ?

— J’ai quitté la sainte Cité dans l’espoir de servir utilement là où mes pas me porteraient. Je pensais être plus utile ici, là où les hommes du Soudan de Babylone[^soudanbabylone] viennent piller, avec l’aide des nomades. »

L’homme hocha la tête, l’esprit encore embrumé de sommeil. Il lui tendit la main.

« Moi c’est Raoul. On m’appelle Sayette, vu que je suis bon archer. On a pas pouvoir de te souder, mais on va aviser sire Eustache, voir si tu peux demeurer ici le temps que le sire Gaston vienne et dise quoi faire de toi.

— Mille grâce, l’ami. Je suis… Ernaut, et j’ai hâte d’occire tous ces mécréants !

— Ouais, ouais. Viens-t’en, Ernaut, je vais te montrer un lieu où poser ta besace. Eustache n’a pas encore pointé le nez hors de son lit. »

Puis il tira la porte plus grand, invitant Droin à pénétrer.

La cour était modeste, bordée d’un appentis pour les chevaux, avec des auges en pierre. Des poules déambulaient, grattant un tas de fumier bien maigre. L’endroit semblait comme abandonné, écrasé sous la chaleur estivale. De tous les bâtiments, seule la tour était correctement maçonnée, les angles marqués de pierres régulièrement disposées et les assises nettes. Suivant Raoul, Droin clopina jusqu’à un bâtiment annexe.

## Château du Fier, fin d’après-midi jeudi 4 septembre 1158

La petite troupe arrivée un moment plus tôt se composait essentiellement de cavaliers, mais un seul parmi eux était chevalier. Ils formaient une bande assez pitoyable de l’avis de Droin, mais nul ne le lui avait demandé. À peine une vingtaine d’hommes, équipés comme des sergents mais avec des tenues sales, des armes usées et des chevaux fatigués. Ils avaient demandé le gîte pour la nuit, se rendant à Gaza depuis Paulmiers. Les hommes étaient visiblement harassés de leur chevauchée dans la canicule et ils s’occupaient des bêtes avec des geste las. Droin, qui n’avait guère de corvées habituellement, vint se proposer pour les aider. Il s’était présenté comme soldat, et faisait comme s’ils étaient frères d’armes, tentant d’en savoir plus sur eux. Mais la plupart répondaient dans un idiome inconnu de lui, et un seul se montra assez affable pour lui répondre sans brusquerie.

Le visage massif, les yeux ridés à force de les plisser, Galien était un Français, de Paris, clamait-il fièrement. Sa barbe blonde et ses yeux clairs contrastaient avec son visage hâlé par des années en Outremer. Il marchait en claudiquant, mais sans que cela ne semble l’handicaper le moins du monde. Tandis qu’ils menaient les chevaux à l’abreuvoir, Droin entreprit de le faire parler, ce que l’autre faisait volontiers.

« On repartira au matin, on a grand désir d’être à Gaza au plus tôt. On va botter quelques culs païens !

— Vous joignez l’ost royal ?

— Non pas, le sire Bertaud des Lices assemble un conroi pour venger l’assaut fait tantôt sur Gaza et Bethgibelin, plus tôt dans l’année. On va venger les frères tombés. »

Droin avait vaguement entendu parler d’opérations par des voyageurs, depuis qu’il était au château du Fier, mais il ne s’était guère intéressé à ces histoires. Il surveillait plus particulièrement les rumeurs de fugitifs recherchés. Mais rien n’était remonté à ses oreilles.

Il n’était toujours pas officiellement soldé par le seigneur du lieu, qui ne semblait pas venir bien souvent, mais s’était fait accepter par la petite communauté, échangeant gîte et couvert contre corvées et travaux que les autres n’avaient pas envie de faire. Il n’y avait pas beaucoup de latins dans ce coin perdu dans les montagnes du sud du royaume, ce qui expliquait peut-être le relatif bon accueil qu’il avait eu.

« Vous êtes nombreux à lever ainsi le ban ? »

Galien fit la moue.

« Je ne saurai dire. Des nomades du désert vont nous guider. Ils n’aiment guère le soudan eux non plus.

— Ils sont mahométans ! Vous ne craignez pas traîtrise de leur part ?

— Les nomades n’ont qu’une seule foi, celle des pilleurs. Butin amassé vaut toutes les prières pour eux » s’amusa Galien.

Les chevaux brossés et nourris, tout le monde se dirigea vers la grande salle, guère habituée à autant d’animation. Ordinairement ils étaient à peine une demi-douzaine à manger là. Les deux chevaliers discutaient au haut bout de la table, buvant du vin dans leur gobelet de verre. Les autres hommes se contentaient de gobelets de terre. Ils étaient tous affalés, les visages fermés, savourant le breuvage sans trop d’effusions. Quelques-uns avait tiré un plateau de mérelles[^merelles] tandis que d’autres s’étaient carrément étendus, la tête sur les bras, et ronflaient sans être gênés par le bruit. Droin attrapa de quoi boire et s’installa avec Galien auprès des joueurs.

« Votre sire fait-il embauche ?, s’enquit Droin.

— On a toujours besoin de bras habiles à tenir l’épée. C’est quoi ta spécialité, Ernaut ?

— …

— Tu te bats avec quoi, d’usage ? »

Devant l’hésitation de Droin, un des joueurs lui lança un regard peu amène.

« T’es soudard ou valet ?

— Bein, soudard, mais ici on fait pas grand chose.

— On te loue à quel prix ici, et avec quelle spécialité ?

— Justement, j’attends encore le sire Gaston pour… »

La réponse embarrassée fit naître un sourire narquois sur les visages.

« Si tu veux gagner ta pitance l’arme en main, il en faudra plus que ça.

— Je suis prêt à apprendre ce qu’il faut. »

L’autre joueur, assis à côté de lui, renifla et se tourna, se touchant les parties génitales.

« C’est là qu’il faut avoir ce qu’il faut, et ça s’apprend pas.

— Ouaip, acquiesça l’autre, postillonnant de rire.

— N’empêche, deux bras en plus, ça peut aider. Il faudrait en parler au sire Hugues, les interrompit Galien.

— Ça fera surtout une part de butin en moins !, rétorqua le second joueur.

— Nan, il parle de vrai, le contredit l’autre. On a grand besoin de nouveaux hommes. Et il en vaut bien d’autres. »

Galien fit un discret signe de tête à Droin.

« On causera plus tard de ta soudée, avec le sire Hugues. Il va vouloir s’assurer que t’es pas larron en fuite, mais si ils se portent garants de toi, ici, y’aura pas de problème.

## Désert d’al-Gifar, après-midi du vendredi 19 septembre 1158

Abrité derrière une dune, Droin reprenait son souffle. Il souleva son casque de fer brûlant malgré le turban et s’essuya le visage. La calebasse à son flanc était fendue, vide. Il l’envoya rouler de rage. De là où il était, il pouvait voir les restes de la troupe se disloquer en direction de l’est. « Tant mieux » grommela -t-il pour lui même. Au moins les Mahométans ne viendraient pas dans sa direction. Il avait gardé l’épée qu’il avait ramassée en fuyant, mais n’avait rien pour la ranger. Elle était poisseuse de sang, maculée de sable. S’enfonçant dos à la dune, il cherchait à reprendre son souffle. Un groupe de bédouins à dos de chameau galopait en direction des montagnes au sud, harcelé par des cavaliers qui bourdonnaient tels des guêpes.

S’il venait à l’idée d’un de ces archers montés de regarder dans sa direction, il était aussi visible qu’une mouche sur du lait. Il se laissa glisser en contrebas et suivit le fond du petit vallon, en demeurant baissé, parfois carrément accroupi. Un peu plus loin, une ligne de crête s’affaissait, bordée de quelques broussailles dénudées. S’il pouvait arriver là, un buisson suffisamment touffu pourrait suffire comme cachette jusqu’à la nuit.

Il lui sembla une éternité avant de pouvoir enfin se glisser parmi les branchages griffus, qui lui écorchèrent les chairs. Il avait soif, sa bouche était de sel et les yeux le brûlaient, mais il savait la survie à ce prix. Ensevelissant tout ce qui était métallique sous le sable, il entreprit de se faire un cocon des feuillages racornis. Puis il patienta, se faisant aussi discret qu’il le pouvait, hésitant même à respirer par moment, tellement l’angoisse étreignait son cœur.

Un blatèrement le fit sursauter alors qu’il s’était assoupi sans s’en rendre compte. Il se blessa au front contre une des branches et pesta, autant de surprise que de douleur. Un chameau paissait tranquillement les feuilles des buissons non loin de lui. Sur sa selle, un bédouin affalé était maintenu en place par une sangle de bouclier. Sur les flancs, quelques sacs, une housse à javeline et, surtout, une outre. Droin sentit immédiatement la salive lui venir à l’idée de savourer une longue gorgée d’eau, même tiède et macérée dans une peau de chèvre.

Après un rapide coup d’œil panoramique, il décoinça les branchages et se déplia doucement. Il glissa hors de sa cachette et avança doucement vers l’animal, qui le regardait d’un air narquois. Il semblait plus concerné par les plantes qu’il arrachait que par ce bipède qui s’approchait. Droin fit le tour pour s’assurer de l’état de l’homme qui pendait, à cheval sur l’arçon. Il avait le crâne ensanglanté et avait tâché le cou de l’animal. Pourtant, quand Droin lui tourna le visage, l’homme cligna doucement des yeux, tenta de bouger les lèvres, sans résultat.

« De l’eau ? Tu veux de l’eau ? »

Droin détacha l’outre et avala une longue gorgée, sentant le liquide salvateur se répandre dans son corps tandis qu’il déglutissait. Il se pencha vers le nomade :

« C’est bon, tu peux en boire. Je m’assurais juste… »

En l’aidant à se relever, Droin réalisa que la blessure était au cou et à la gorge en fait, et le bras pendait bizarrement, certainement brisé. Il aurait préféré trouver un cadavre, le chameau lui aurait servi de monture pour s’enfuir. Quoiqu’il n’était jamais monté sur une telle bête. Tandis qu’il réfléchissait, le bédouin tenta de se redresser, vacillant et épuisé. Droin l’aida à retrouver une position moins inconfortable. L’homme lui fit signe de la main, un signe confus.

« Tu veux quoi ? À manger ? Tes sacs ? »

L’homme fit non de la tête tandis que Doin lui montrait un objet après l’autre. Il finit par attraper les rênes d’un doigt faible et les fit glisser dans la main de Droin.

« Monte » souffla-t-il péniblement. Droin recula, ce qui déplut au chameau qui fit un coup de tête pour se rapprocher du buisson qu’il dévorait paisiblement. Le bédouin insista :

« Monte… Je montrer route, toi mener chameau… »

Droin souffla. Il n’était déjà pas doué à cheval, mais à chameau ! Comprenant néanmoins qu’il tenait là un moyen de s’en sortir, il s’acharna un long moment pour tenter de grimper sur l’animal, escaladant sans grâce pour enfin se trouver à peu près installé sur son dos. Le bédouin était devant lui, et s’était laissé aller contre son torse. À demi conscient, il fit plusieurs appels de langue et tapa mollement du plat de la main pour arriver à décider la bête.

« Droit au soleil… » glissa-t-il, épuisé à Droin. Celui-ci s’efforçait de diriger l’animal qui les faisait rebondir en tous sens. Sans personne pour le tenir, le nomade aurait fini dans le sable du désert. Ils avançaient bon train, même si leur cavalcade était désordonnée.

Lorsque la nuit tomba, ils étaient proches des montagnes. Près d’un ravin. Droin descendit comme il le put, finissant sur les fesses mais en un seul morceau, et puis il aida l’homme finalement évanoui de douleur. Regardant l’animal qui semblait mâcher un souvenir de fourrage, Droin échappa un sourire désabusé et vint flatter l’animal.

« Bon, ça pourrait pas être pire, je crois, t’en dis quoi, gros machin ? »

Le chameau continua sa mastication et tourna la tête, indifférent. Sur la butte au-dessus de lui une silhouette venait d’apparaître. ❧

## Notes

La justice des états latins s’appuyait souvent sur des mutilations, de différents types, que ce soient de simples marques d’infamies (marquage au fer, tranchage du nez ou des oreilles) ou des peines encore plus effrayantes : membre amputé. En plus de la sévérité censée effrayer, cela donnait la capacité de facilement reconnaître les récidivistes, généralement condamnés à mort. On comprendra aisément la mention de fugitifs dans les chartes et des peines prévues pour ceux-ci dans les textes de loi.

## Références

Foucher Victor, *Assises de Jérusalem. Première partie : Assise des Bourgeois*, Paris: 1890.

Prawer Joshua, *Crusader Institutions*, New York : Oxford University Press, 1980.

Prawer Joshua, *Histoire du Royaume latin de Jérusalem*, Paris : CNRS Éditions, 2007.
