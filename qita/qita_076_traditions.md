---
date : 2018-02-15
abstract : 1145-1156. Abdul Yasu, riche héritier d’une dynastie marchande de Jérusalem, se voit contraint de suivre la voie de ses aînés alors qu’il se serait parfaitement satisfait d’une vie moins industrieuse. Pourtant, même s’il accepte finalement de se plier au modèle qui avait toujours prévalu dans sa famille, il trouvera le moyen de franchir certaines limites.
characters : Abdul Yasu, Tha’lab al-Fayyad, Ghadir, Droart
---

# Traditions

## Jérusalem, quartier de la porte d’Hérode, matin du mardi 6 mars 1145

Un grand fracas d’origine inconnue réveilla Abdul Yasu en sursaut. Il habitait non loin d’un souk où l’on façonnait le cuivre. Ce n’étaient pas tant les petits marteaux traçant les décors qui faisaient du bruit que le transport des marchandises allant et venant depuis la porte de David où l’on taxait les produits. Il résidait chez son père, Tha’lab al-Fayyad, dans une chambre du rez-de-chaussée de la très confortable demeure. Il y était fort bien traité, profitant de la richesse de la maison sans trop s’embarrasser de considérations matérielles. Il avait opté pour cette modeste pièce, car, si elle avait l’inconvénient d’être proche de la rue, elle offrait l’avantage d’être aisément accessible depuis la cour arrière, où s’ouvrait un guichet donnant sur l’extérieur. Il pouvait ainsi aller et venir en toute discrétion, sans que l’on puisse surveiller son emploi du temps.

Il se leva et fit une toilette soigneuse, se curant les ongles et les oreilles avec attention. Puis il enfila tuban[^tuban], sirwal[^sirwal], fine chemise et durrâ’a[^durraa] de splendide laine zébrée de couleurs vives, posa sa qayah[^qayah] sur son crâne et assujettit méticuleusement son turban de soie paille, à longs rafrafs[^rafraf] brodés de bleu sombre. Il était toujours très élégant et tenait à paraître impeccable à chaque occasion. De plus, il prévoyait en fin de journée de se rendre aux bains, de façon à être parfait pour une soirée festive avec quelques amis.

En sortant de sa chambre, sa sacoche au côté, il croisa Ghadir, la vieille gouvernante, qu’il salua poliment. Celle-ci lui accorda un sourire puis soupira en détaillant du regard la tenue d’apparat du jeune homme. Elle traitait Abdul comme s’il était encore un enfant, mais, paradoxalement, semblait exaspérée par son comportement juvénile. Indifférent à ces usuelles manifestations d’humeur, il lui donna des instructions pour qu’on lui porte des affaires aux bains voisins en fin d’après-midi. Avant qu’il n’ait le temps de s’esquiver en direction de Malquisinat, où il savait dénicher ses friandises préférées, znout assit[^znoutassit] arrosés de mouhalabie[^mouhalabie], elle lui indiqua que son père souhaitait le voir.

Tha’lab al-Fayyad était un vieil homme bossu dont les membres grêles s’échappaient d’un corps empâté par les ans et la trop bonne chère. Sa voix elle-même était gênée par les replis tremblotants qui pendaient dans son cou, faisant onduler sa maigre barbe quand il prenait la parole. Ses yeux larmoyants et fatigués n’y voyaient plus si bien qu’il devait souvent demander à ce qu’on lui lise ses ouvrages favoris ou les lettres de ses amis. Abdul pensait que c’était la raison qui le menait en ces lieux.

« Entre, fils. Prends place, j’ai des choses à te dire. »

Abdul se méfia de l’entrée en matière. Son père était assez laxiste et peu exigeant, et bon nombre de voisins moquaient sa désinvolture envers ses domestiques et sa famille, mais il semblait peu s’en soucier. Il avait néanmoins parfois des lubies, des velléités autoritaires qui, sans durer, étaient pénibles à affronter.

« J’ai reçu tantôt belle lettre de ton frère Ma’ali. Il t’envoie ses meilleures pensées et espère que tu étudies ainsi qu’il sied… »

Le début était peu encourageant. Ma’ali était son aîné, parti voilà des années vers l’Égypte et au-delà. Il était désormais fort loin, naviguant dans la mer vers les Indes. Bien implanté dans sa ville, il avait contracté un riche mariage et échangeait avec les plus hautes autorités musulmanes, les plus opulents négociants de la place. Il faisait la fierté de son père et semblait, à en lire ses courriers, un des plus heureux et influents hommes de sa cité. De quoi agacer n’importe quel cadet. De malaise, Abdul en remua sur son coussin.

« Il a accompagné son pli de plusieurs balles de denrées, à vendre et pour notre maison. En particulier une belle pièce de soie qu’il espère te voir porter pour ton mariage. »

C’était donc cela. Depuis quelques mois, Tha’lab al-Fayyad n’avait de cesse de rappeler à son fils qu’il était temps pour lui de s’établir. Ce qui signifiait abandonner cette vie de loisirs et d’amusement pour s’adonner à un travail rémunérateur en vue de fonder sa propre famille. Il avait désormais plus de vingt ans et c’était déjà bien tard pour se mettre au labeur. Il aurait dû partir comme commis des années plus tôt, mais demeurait dans le confort, retenu par l’affection d’un père âgé.

« Je me disais que nous pourrions prélever de ses largesses de quoi t’installer. As-tu réfléchi à cela, depuis notre dernière conversation ?

— Je continue d’étudier vos notes, je ne sais encore quelles marchandies seraient les plus intéressantes.

— Le commerce ne se fait pas sur le papier, Abdul ! s’emporta son père. *Jeunesse sans discipline, maison sans toit* ! As-tu été discuter dans quelque souk ? Réfléchi aux besoins des uns et des autres ? Travaillé ton expertise en savoir de négoce utile ? »

Abdul plongea du nez, conscient de déroger constamment à ses devoirs. Il avait reçu la meilleure éducation qui soit, profitant de la richesse familiale sans subir les contraintes s’exerçant sur les premiers-nés. Sa mère étant morte peu après sa venue au monde, ses proches s’en étaient fort émus et il avait été élevé préservé de toutes les difficultés, choyé par la gouvernante autant que par sa fratrie et son père. Il en tirait une certaine mollesse de caractère qui l’incitait à vivre dans l’inconséquence. Au grand dam de ceux qui l’entouraient, conscients qu’il ne serait peut-être pas capable de faire quoi que ce soit de sa vie.

Son père lui conservait pourtant une affection que les semonces n’altéraient pas plus que les manquements. Rien ne semblait résoudre Tha’lab al-Fayyad à sévir pour de bon. Il secoua la tête de dépit, faisant trembler ses multiples mentons, puis se gratta longuement le nez.

« Tu as bien quelque passion dans la vie en dehors de la musique et des jeux ? Tu m’as fait acheter ce superbe alezan voilà deux saisons, est-ce pour en tirer commerce ? Ou s’agit-il là encore d’un caprice passager ? »

Vexé de se voir ainsi jeté à la face un de ses égarements, Abdul grogna et rétorqua vivement.

« Point du tout, je me disais qu’il ferait bon effet en des écuries de louage et pourrait saillir juments et ânesses.

— Astucieuse idée, mon fils, j’en conviens. Que ne me l’as-tu dit plus tôt ? As-tu trouvé un lieu où organiser ton affaire ? Des pouliches en vue ? »

Abdul hésita à prolonger le mensonge. Il n’avait jamais eu d’autre intention que de parader sur son superbe étalon. Il avait toujours apprécié les chevaux et goûtait fort les jeux équestres, mais de là à s’installer les pieds dans le crottin… Tandis qu’il cherchait un moyen de détromper son père, celui-ci s’emballait, réfléchissant déjà aux partenaires qui pourraient aider Abdul dans son entreprise.

## Jérusalem, porte de David, fin d’après-midi du vendredi 25 mars 1149

Jérusalem était toujours très agitée à l’approche de Pâques, mais cette année-là, l’effervescence était à son comble. Après l’échec de l’assaut sur Damas, le roi des Français Louis avait décidé de demeurer là pour l’importante fête religieuse. Son armée encore imposante occupait donc l’endroit en plus des habituels pèlerins. Cet afflux de visiteurs de renom attirait également de nombreux artistes, commerçants et écornifleurs de toutes sortes, impatients de bénéficier de la largesse d’une cour aussi foisonnante. La présence de la fameuse Aliénor d’Aquitaine et son goût pour les célébrations courtoises enflammaient les esprits.

Abdul Yasu tirait profit de cette activité. Ses écuries, installées aux abords de la porte de David, accueillaient des montures de voyageurs. De plus, il louait à bon prix ses plus beaux animaux pour permettre aux importants personnages de s’afficher en ville ainsi qu’il seyait à leur rang. Il avait fini par s’habituer à son commerce, abandonnant les corvées les plus salissantes à un employé et conservant les contacts avec les clients, en particulier les plus riches.

Non soumis aux droits et taxes, les pèlerins pouvaient entrer dans une file distincte de celles des marchands. S’y adjoignaient les habitants qui allaient et venaient depuis leurs jardins ou les fournisseurs bénéficiant d’une exemption. Naturellement Abdul prit sa place dans ce groupe et menait son petit âne d’un air badin. Cherchant à ne pas croiser le regard d’un des hommes de faction, il leva le nez, comme pour en évaluer la météo prochaine. Quelques filaments blancs s’étiraient paresseusement dans une mer d’azur. Plusieurs oiseaux tournaient, en quête de moucherons, s’élançant depuis les aspérités des puissantes murailles.

Il sursauta quand une main ferme vint lui frapper l’épaule. Tout à son rôle, il sourit benoîtement au garde qui le toisait d’un air soupçonneux. L’homme ne paraissait pas agressif, le visage rond et rougeaud, un petit nez en trompette surplombant une belle moustache.

« Dis-moi, compère, ton âne me semble porter bien lourds bagues[^bagues] ! »

En disant cela, le sergent tâtait de la main les sacs amoncelés et rapidement ficelés sur le dos du petit animal. Abdul chercha à se composer un visage emprunt de modestie et de sincérité.

« Ce ne sont que mors, freins et selles de mes bêtes. Je les mène en réparation. »

Il tenta un sourire puis enchaîna.

« Vous me connaissez, je vais et viens moult fois chaque jour, pour le commerce de mes bêtes, en les écuries non loin. »

Tout en parlant, il indiqua grossièrement la direction de ses bâtiments. Le soldat se tenait impassible, un léger sourire à peine tracé sur les lèvres, les pouces coincés dans son ceinturon d’armes.

« Oui-da, j’ai bonne connoissance de ta face, mais aucune de ces marchandies. Il te faut aller dans l’autre groupe et payer la taxe ainsi qu’il sied.

— Mais ce n’est pas là commerce ! Juste des affaires miennes qui ont grand besoin de reprises. »

Le garde fit une moue et entreprit de jeter un coup d’œil au contenu d’un des sacs. Pas le plus gros, posé au-dessus, mais un des plus petits, adroitement ficelé sous les autres. Autour d’eux, les voyageurs avançaient, indifférents à ce contrôle qui ne les concernait pas.

Une belle courroie teintée de rouge apparut, ornée de bossettes argentées. Le sergent siffla discrètement.

« Si c’est là mauvaises sangles, tu dois louer destriers de baron fieffé ! »

Abdul grimaça. Ce n’était pas son premier essai, de mêler ainsi de beaux harnais neufs à ses vieilles courroies rompues. Il les faisait venir d’Égypte et les revendait un bon prix aux riches seigneurs croisés désireux de ramener de prestigieux souvenirs de leur voyage au centre du monde. Les quelques précédentes fois, il avait pu passer sans encombre, mais ce nouveau sergent semblait plus suspicieux que les autres. Pourtant, Abdul nota néanmoins qu’il n’avait pas appelé auprès d’eux les officiers de la douane.

« Mon valet aura peut-être par mégarde rangé ce harnois neuf avec les anciens…

— C’est tout un, erreur ou pas. La taxe est due pour chaque marchandie, entrée à dessein ou pas…

— C’est que j’ai grand ouvrage à faire ce jour ! Ne peut-on au moins faire en sorte que les choses se fassent promptement ?

— Que me suggères-tu, l’ami ? »

Abdul hésita un instant. Le sergent pouvait être de ceux qui prêchaient le faux pour glaner le vrai, espérant faire moisson de coquin à bon compte en laissant la proie s’enferrer toute seule. Il avait pourtant le visage franc et ouvert autant qu’un Latin pouvait l’arborer. Son teint couperosé témoignant de son évident penchant pour la boisson, cela donna une idée à Abdul Yasu.

« Disons que je pourrais vous faire passer la taxe un peu plus tard, histoire de ne pas perdre de temps. J’ai usage de passer dans quelques tavernes au sud du Change Latin. Nous pourrions y convenir d’une rencontre.

— Je m’en voudrais fort de retarder ton commerce, l’ami. Mais il me faut évaluer le montant de la taxe en détaillant ce que tu as dans ces sacs ! Dénouons donc là tout ce fourniment !

— Oh, il n’y a guère plus que ce que tu as vu. Pour une valeur d’une livre, peut-être deux, et certes pas plus.

— L’octroi étant de trois besants le cent pour ce frein, cela ferait… hum, dans les quinze deniers. Si on y ajoute selle et courroies pour compléter harnois, on arrive à deux sous. »

Abdul répondit d’un rictus. Pour avoir la face avinée, l’homme n’en était pas moins assez fin et fort en chiffres. Avec une épée au côté, en plus. Il ne serait d’aucune utilité de se récriminer, au risque d’attirer l’attention et de se faire prendre. D’autant qu’il demeurait un beau bénéfice vu qu’il se trouvait dans ses bagages deux équipements complets dont un rapide brossage suffirait à en éliminer la poussière et en rehausser le brillant.

« D’accord, sergent. Grand merci. Nous nous retrouvons en la veillée là-bas ?

— C’est dit. »

Le sergent lui fit signe de la tête d’avancer promptement puis, alors qu’Abdul frappait la croupe de sa bête, il conclut :

« J’ai nom Droart. »

## Jérusalem, Malquisinat, midi du mardi 10 juillet 1156

La touffeur de l’air et les impitoyables rayons du soleil incitaient chacun à chercher la moindre parcelle d’ombre au plus fort du jour. Le temps d’une collation, Droart et Abdul Yasu avaient pris l’habitude de partager un petit recoin où un muret servait de banc, à l’abri d’auvents de fortune en feuilles de palme, non loin des échoppes proposant pâtés et tartes, crêpes, galettes et brochettes. À force de se croiser, sans même avoir une transaction en cours, ils aimaient à échanger tranquillement en dégustant leur repas parmi la foule des voyageurs et manouvriers familiers des lieux.

Les odeurs des caramels de cuisson y disputaient aux senteurs épicées des mousses et tartinades où l’on plongeait des légumes frais taillés en bâtonnets ou des portions généreuses de pain. Le vin, pas toujours de grande qualité, y était néanmoins assez fort pour inciter sinon à la sieste du moins à l’assoupissement. Et il y avait même de la place assez pour y tracer de quoi jouer aux mérelles, aux jeux de table de façon générale, voire aux dés pour ceux qui ne craignaient pas de tenter leur chance si près des lieux saints.

Abdul appréciait fort le jeune sergent, dont le bagou était à la hauteur de ce qu’il avait estimé lors de leur première rencontre. Ils avaient encore fait quelques affaires, mais se voyaient également pour le simple plaisir d’échanger des nouvelles sur le royaume, la cité et leurs voisins. Droart était assez friand de commérage, aussi impatient de les répéter que d’en apprendre de nouveau. Appartenant chacun à une communauté différente, ils pouvaient initier l’autre à tout un monde de risibles comportements. Abdul Yasu savait parler la langue des Latins et ils conversaient habituellement ainsi, mais Droart en profitait pour se familiariser avec l’arabe, qu’il employait sans grand talent et un accent qui faisait s’esclaffer Abdul.

Alors qu’ils finissaient leur repas avec des galettes au miel et à la pistache, Droart proposa à Abdul de se joindre à lui le soir pour la veillée qu’il passait généralement avec quelques amis.

« Nous passons le temps ainsi que nous le faisons tous les deux certains mitans du jour. Je suis acertainé que tu apprécieras mes compères. En particulier Eudes, le sergent au poil cuivré que tu as vu tantôt à la porte.

— C’est bien amiable à toi, mais il n’est pas d’usage de passer veillées ensemble…

— Et pourquoi donc ? Si le père Foucher estime que les autels de tes prêtres ont leur place en son moustier, pourquoi donc ne pourrais-tu pas te joindre à nous ? »

Bien qu’il appréciât l’invitation, Abdul était mal à l’aise à cette idée. Il n’était pas dans les usages de se mélanger avec d’autres communautés. Il était certes chrétien, quoique peu fervent et d’un culte distinct de celui des catholiques romains, mais il se sentait différent de ces derniers. Langue, culture, origine, tout en lui l’incitait à maintenir cette séparation. Il n’avait d’ailleurs jamais eu beaucoup d’échanges avec d’autres que Droart.

Celui-ci se rinça la bouche d’une longue rasade au pichet, essuyant les miettes accrochées à son menton d’un grand mouvement de bras.

« Tu m’as dit que ton frère a solide commerce avec les Mahométans dans sa cité. Et pourquoi tu n’aurais pas bonne intelligence avec nous ? Toi aussi tu trouves qu’on sent prou l’ail et l’oignon ? plaisanta-t-il.

— J’aurais grand plaisir à découvrir tes sochons, mais peut-être qu’eux seront moins heureux de m’encontrer. Il est de bon ton de fréquenter les siens.

— Foin de cela. Mes pères ne sont pas nés ici, mais moi si. Cela nous fait déjà un point commun. S’il se trouve quelque drôle à chicaner sur ce point, je lui ferai rentrer dans la gorge ses arguties. »

Il tendit le broc à Abdul, assorti d’un clin d’œil.

« Là d’où vient ma famille, ils ont bonne connoissance des vilains sur des lieues à la ronde, sachant combien de grains ils ont commercé avec chacun, la taille des œufs qu’ils versent en dîme ou les femmes qu’ils leur ont mariées. Un homme sans lieu n’est rien de plus que poussière portée par le vent. Et moi je suis d’ici, comme toi, alors il serait déraisonnable de faire comme si je te connaissais pas. Tout bon Chrétien est mon frère, disent les pères curés. Donc tant que tu te fais pas hérétique, j’aurai bon plaisir à te compter parmi les miens ! »

## Jérusalem, quartier de la porte d’Hérode, fin d’après-midi du lundi 16 décembre 1157

Abdul résidait toujours chez son père, continuant à profiter des avantages à n’avoir pas à gérer sa propre demeure. Il bénéficiait également des plats de Ghadir, dont la main s’alourdissait sur les épices avec l’âge, mais sans qu’il le déplore. Il épargnait un peu de l’argent que son affaire de location lui rapportait, tout en s’accordant le droit d’en dépenser une large part en soirées, musique et boissons.

Il rentrait d’une journée un peu morose, le mauvais temps n’incitant guère aux voyages. De plus, il avait eu des frais médicaux, une de ses juments ayant développé un glome. C’était une bête fragile qu’il n’avait pas payée très cher, mais dont l’état de foulure chronique, malgré des soins et une attention constante, la rendait sensible à ce genre d’affection. Il en avait été quitte pour des applications d’emplâtre goudronné et un peu de repos à prévoir pour l’animal.

Passant par le centre de la Cité, il en avait profité pour acheter quelques kenafas dont l’odeur de miel fleuri l’avait tenté tout au long de sa fin de parcours. Il savait son père friand de ces desserts et Ghadir peu douée pour les réaliser.  Tha’lab al-Fayyad l’accueillit aimablement quand il se présenta à lui, l’invitant à aller rapidement se laver les mains pour prendre le repas ensemble. Il semblait particulièrement enjoué.

Tandis qu’ils se désaltéraient d’un leben[^leben] frais du jour, Abdul eut l’explication de cet enthousiasme. Une lettre venue d’Égypte était arrivée sans que son père n’ait eu le temps d’en prendre connaissance. Il espérait qu’elle soit de Ma’ali et déchanta un peu lorsqu’il apprit que l’expéditeur n’était que Durayd al-Khayyat al-Tusi, un de ses nombreux contacts d’affaire à Fustat. Il eut malgré tout un sourire malicieux quand il invita son fils à la lui lire.

Les informations qu’elles contenaient étaient surtout d’ordre commercial, après les salutations et échanges de bons vœux d’usage en introduction. Il indiquait les transactions qu’il avait opérées pour le compte de Tha’lab al-Fayyad, pour une belle somme au moment de la clôture hivernale des mers. Puis le sujet en changea brusquement, ce qui fit balbutier Abdul Yasu avant qu’il ne s’interrompe tout à fait.

« Pourquoi donc t’envoie-t-il des nouvelles de sa fille, père ? Tu as l’intention de reprendre épouse ? »

Abdul se voyait mal accueillir dans leur foyer une fille qu’il aurait à traiter comme sa mère. D’autant que les jeunes femmes qui épousaient des hommes très âgés avaient tendance à asseoir leur autorité de façon un peu trop empressée à son goût. Il eut un fugace sourire à l’idée de la réception d’une pareille nouvelle par Ghadir, qui faisait dans la maison ainsi qu’elle l’entendait depuis des années. D’un signe de la main de son père, il reprit la lecture. Rapidement le rictus se crispa pour s’accompagner d’un ébahissement grandissant. De surprise, sa voix devint moins forte et, de stupeur, il s’arrêta une seconde fois.

« Avez-vous projet de me faire épouser sa fille, cette Shaima, père ?

— C’est bien ce qui est convenu, en effet. Al-Khayyat est de bonne fame et un ami ancien. Sa fille est en âge de se marier et il a de quoi la doter en suffisance pour que l’offre soit honnête.

— Mais père, jamais nous n’avons évoqué cela !

— Voilà chose faite. Je lui ferai porter réponse dès le printemps revenu et il serait bon que tu joignes à ma lettre un petit mot pour montrer ton enthousiasme à l’idée d’unir nos deux familles. Je compte sur toi pour mettre en avant la bonne santé de ton commerce. »

Abdul Yasu en avait le souffle coupé. Il avait à peine passé la trentaine et pensait bien profiter de son célibat encore quelques années. Il n’avait aucun désir de s’établir et de devoir consacrer son pécule à l’entretien d’une famille ou d’une maison. Prodigue de nature, ce n’était pas tant la dépense qui l’effrayait, mais plutôt l’idée de devenir responsable d’autres que lui. Il déglutit lentement puis demanda :

« Est-elle jolie au moins ? Je ne voudrais pas d’une épouse qui ne soit pas attrayante au regard.

— J’ai dans mon vieux khaff[^khaff] plusieurs lettres d’al-Khayyat. Il s’y trouve joli portrait de cette juvénile beauté, que son père composa de façon droitement rimée. Je t’en donnerai connoissance. »

Voyant que son fils était contrarié, il s’éclaircit la gorge avant de reprendre d’une voix qui se voulait conciliante.

« Tu ne semblais guère enclin à aller te proposer dans les familles de nos amis, il fallait bien que je m’occupe de cela. Avec les années qui passent, tu passes trop de temps avec ces Ifranjs ! J’avais grande crainte que tu n’y fasses malavisée rencontre. Ta réputation n’est déjà pas tant fameuse qu’une mésunion l’aurait plus encore entachée. » ❧

## Notes

Au XIIe siècle, parmi la bourgeoisie urbaine arabe, quelle que soit sa religion, il est habituel de commercer au sein d’un vaste réseau de correspondants qui dépasse les frontières. Un homme se doit de prouver sa capacité à gagner sa vie suffisamment pour s’établir avant de prétendre à avoir une famille. Ne pas être en mesure de subvenir à ses besoins est un motif valable de divorce pour une épouse, même si dans la réalité bien peu sont prononcés à leur initiative.

Pour les jeunes femmes, on leur demande surtout d’être nubiles et vierges. Ces deux exigences sociales expliquent la grande différence d’âge souvent constatée. Par ailleurs, le consentement des futurs mariés est tout à fait accessoire, l’union étant le résultat d’une politique familiale d’extension de ses amitiés et influences. Comme les noms l’attestent, l’individu n’existe que par son intégration à une filiation et, de façon plus générale, à une communauté. Cela explique peut-être aussi les fortes réticences envers les pratiques exogames, qui sont encore plus exigeantes que le simple respect de sa propre religion.

## Références

Bianquis Thierry, *La famille arabe médiévale*, Bruxelles : Éditions Complexe, 2005.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I à 6, Berkeley et Los Angeles : University of California Press, 1999.

Goitein Shlomo Dov, Friedman Mordechai Akiva, *India traders of the Middle Ages. Documents from the Cairo Geniza (‘India Book’)*, Leiden - Boston : Brill, 2008.
