---
date : 2017-09-15
abstract : 1157. Découvrant un complot qui le dépasse, Abu Malik s’efforce d’en comprendre les rouages, tout autant pour se préserver, lui et sa famille, que dans l’espoir d’éviter de briser une paix chèrement acquise.
characters : Bernard Vacher, Amīn ad Dīn Zayn al-Hağğ, Abu Malik al-Muhallab, al Zarqa’, Fazila, Yalim al-Buzzjani, Frère Raymond
---

# Pour quelques dinars de plus

## Jérusalem, grand hôpital de Saint-Jean, soirée du samedi 19 janvier 1157

Il était rare pour Abu Malik de se rendre dans le principal établissement des frères de Saint-Jean. Avec leur militarisation croissante, il s’y trouvait de plus en plus mal à l’aise. Pourtant, malgré toutes ses craintes, il n’avait jamais eu à subir la moindre remarque, et encore moins de brimades, de la part des soldats en armes qu’il avait pu y croiser. Comme beaucoup de chevaliers de la Milice du Temple, il s’y recrutait de nombreux hommes nés en terres levantines, habitués à fréquenter des personnes d’autres religions que la leur. Tant qu’on ne remettait pas en cause leur prééminence, qu’on leur versait les impôts, ils n’étaient pas belliqueux. En quelques décennies, ils étaient devenus de vrais Orientaux.

Chaudement emmitouflé dans son épaisse jubba de laine colorée, il sentait le froid des pierres à travers ses souliers malgré ses chaussettes. L’hiver était une fois de plus très rigoureux à Jérusalem et les bâtiments, frais en été, se transformaient en de véritables glacières. Il voyait la buée qui s’échappait des lèvres des personnes qu’il croisait tandis qu’il était mené auprès de frère Raymond, qui avait demandé après lui. Il sentit avec délice les odeurs du repas dans la zone des cuisines, où il ne s’attarda cependant pas. Ils allaient au-delà des lieux ouverts aux voyageurs et pèlerins, dans la partie réservée aux moines.

Il finit par pénétrer dans une vaste salle parsemée de lutrins, où des copistes devaient s’user les yeux en journée à écrire chartes et documents, peut-être des ouvrages littéraires ou religieux pour les plus chanceux. L’endroit sentait la colle de peau, la gomme arabique et l’huile des lampes. Une seule lueur était visible, au fond de la pièce, éclairant une table avec de nombreux pots et gobelets emplis de plumes et de pinceaux. Un hospitalier de haute taille, au physique impressionnant, y griffonnait sur des tablettes, le crâne abrité sous une profonde capuche. On avait tiré à ses côtés un chariot à braises qui apportait un peu de chaleur. Abu Malik sourit en le reconnaissant.

« Frère Raymond ! Grande joie de vous encontrer de nouvel ! »

Le clerc se leva et fit bon accueil à celui qu’il considérait comme un ami, malgré leurs différences. Il paraissait épuisé, son fin visage creusé de rides de fatigue. Il demeurait néanmoins aussi alerte qu’à son habitude et, d’un geste, invita son hôte à s’asseoir sur un escabeau.

« Je vous prie de faire excuse pour cette fort tardive invitation. Mais j’ai cru apprendre que vous repartiez d’ici peu à Damas ?

— En effet, je n’aime guère traîner en ces froides montagnes l’hiver venu. Ce n’est que par grande nécessité que je suis ici. Je ne pensais pas vous y voir, on m’avait dit que vous étiez parti au nord.

— J’arrive à peine de Tripoli, en effet. »

Le moine replia quelques documents, referma certaines de ses tablettes et rapprocha instinctivement le buste d’Abu Malik, bien qu’ils fussent seuls dans le lieu.

« J’aurais une question délicate pour vous. Je vous sais homme de paix, ainsi que moi, et elle se trouve en fort périlleuse situation. »

Le Damascène fronça les sourcils, inquiet de ce préambule. Les ombres autour d’eux lui semblèrent soudain menaçantes, curieuses et hostiles. Frère Raymond se passa plusieurs fois la langue sur les lèvres, cherchant visiblement à exprimer une idée difficile, ou hésitant à aborder un sujet délicat.

« Ce que je vais vous dire est frappé du plus grand secret. On m’a autorisé à vous en parler, en raison de la confiance entre nous. Mais je vous exhorte à la plus grande des prudences. Gardez cela sous le manteau autant qu’il est possible. »

L’hospitalier se gratta le menton, renversant ses ultimes préventions.

« Le tribut versé l’a été en fausse monnaie. Le roi est furieux de la chose et entend bien réparer l’outrage. Il vocifère comme diable en bénitier. Connétable et sénéchal m’ont incité à me renseigner sur l’ambiance qui règne dans le Bilad al-Sham[^bilad_al_Sham]. »

Abu Malik écarquillait des yeux ronds, bien en accord avec sa bouche béante. Cela faisait de nombreuses années que Damas s’assurait d’une bienveillante neutralité des Latins en leur versant une rente garantissant la trêve. Le nouveau dirigeant de la cité, Nūr ad-Dīn[^nuraldin], fort occupé dans ses territoires au nord, avait donné l’impression de vouloir tempérer avec le royaume de Jérusalem et avait perpétué cette habitude. Certainement dans l’attente de se trouver en position de force.

« Certains barons de la Haute Cour sont au courant et tiennent à savoir ce qui se trame dans l’ombre. Cela n’est pas de bonne intelligence pour Nūr ad-Dīn que de chercher querelle en ces temps, ce me semble.

— Certes pas. Il n’est d’ailleurs pas en les murs, plus préoccupé de ses domaines alépins. Il sait l’adhan[^adhan] proclamé en son nom et ses hommes à la tête des diwans[^diwan], cela lui convient. »

Frère Raymond soupira. Il se doutait de cette réponse qui l’inquiétait plus qu’elle ne le rassurait. Cela signifiait que des intrigants cherchaient à provoquer la guerre ou qu’ils étaient prêts à prendre ce risque pour faire main basse sur le montant du tribut.

« Sauriez-vous vous enquérir de cela ? Si nous trouvons les malfaisants, cela peut éviter de sanglants conflits qui ne sauraient tarder.

— Il se trouve des amis miens désireux de vivre en paix. Certains sont de l’administration, peut-être auront-ils eu vent de ce complot ? Rien n’est jamais aussi secret qu’on l’espère. »

Dépité, Frère Raymond hocha la tête. Il était bien conscient que malgré la volonté royale ne pas ébruiter l’affaire, les rumeurs iraient bon train. Les plus belliqueux des croisés y trouveraient de quoi faire prospérer les graines de la discorde. Abu Malik, comprenant la question urgente, fit mine de se lever. L’hospitalier l’arrêta d’un geste de la main et lui glissa un papier plié.

« Outre cela, pour nos affaires habituelles, j’ai là quelques noms de personnes qui se trouvent peut-être en geôles damascènes. Il en est une en particulier, pour qui rançon peut être versée. »

Il rouvrit le petit document pour y retrouver le nom qu’il avait sur le bout de la langue.

« Guilhem Torte. Si jamais les combats devaient reprendre, il serait bon de s’enquérir de ces malheureux au plus tôt, sans quoi ils moisiront en cachot encore des mois… »

Abu Malik prit le document et acquiesça. Il avait lui-même pu faire libérer quelques coreligionnaires à la faveur de la trêve. Il était heureux de pouvoir les ramener chez eux avant le début de la nouvelle année^[Correspondant au 14 février du calendrier chrétien.], mais les réjouissances seraient de courte durée. Il sentait que la situation devenait de plus en plus délicate pour les hommes comme lui, ambassadeurs et diplomates malgré eux. Il s’annonçait un temps d’acier et de sang, il le percevait au fond de lui. Il accorda un sourire amer à l’hospitalier.

« Je verrai ce que je peux faire. Je vous adresserai dévoué messager dès que j’en saurais plus pour une affaire ou l’autre. »

Frère Raymond inclina le chef en guise de salut. Lui aussi pressentait de sombres futurs. Son visage se réfugia dans l’ombre de sa capuche tandis qu’il s’affairait à ranger ses derniers documents.

## Damas, demeure de Al-Zarqa’, soirée du youm al sebt 19 dhu al-hijjah 551^[Samedi 2 février 1157.]

Les gouttes de pluie dansaient dans le bassin au centre de la cour sur laquelle donnait largement le majlis[^majlis]. Malgré l’averse, il ne faisait pas trop froid, mais les hommes installés sur les matelas étaient chaudement habillés, la pièce n’ayant aucun système de chauffage. Abu Malik avait finalement décidé de s’ouvrir de son problème à son ami Al-Zarqa’[^alzarqa]. Celui-ci était un fonctionnaire réputé, doublé d’un commerçant compétent, dont Abu Malik estimait grandement la probité. Derrière le sourire rusé et les yeux inquisiteurs se cachait un esprit acéré, avide de justice. Il savait néanmoins naviguer à vue dans les cercles du pouvoir et avait réussi à se maintenir en poste malgré la récente administration.

Il avait lui-même posé quelques questions et avait décidé d’inviter un soldat en qui il avait suffisamment confiance pour lui présenter Abu Malik. Yalim al-Buzzjani était un émir de l’ancien atabeg de Damas Mujīr ad-Dīn Abd al-Dawla qui avait demandé à rejoindre la troupe de Nūr ad-Dīn quand son maître avait choisi de se retirer de la région. Il était d’origine turque, mais avait longtemps servi les bourides, au point de se sentir aussi damascène qu’eux. Mais, surtout, avait avancé al-Zarqa, il était d’une grande loyauté une fois sa parole engagée et ne verrait pas d’un bon œil que des traîtres tentent de tromper l’émir.

Abu Malik avait expliqué l’histoire en quelques phrases rapides et laissait l’officier digérer les informations. Au fur et à mesure du récit, son visage s’était contracté et ses yeux s’étaient réduits à des fentes. Tout en réfléchissant, il jouait machinalement avec une de ses longues tresses brunes. Comme le sharbush[^sharbush] et le qaba[^qaba], c’étaient, avec le sabre et l’arc, des attributs militaires constitutifs de son identité. Lorsqu’il prit la parole, sa voix était rauque, retenue, et il parlait lentement, visiblement choqué par le méfait.

« Vous me dévoilez grave nouvelle. Bien lourde à celer. »

Il dévisagea al-Zarqa’ avec intensité, bravant le regard clair de son interlocuteur sans ciller.

« L’émir m’a fait l’honneur de me faire sien, et je suis désormais tenu de le servir loyalement. Nous sommes plusieurs à avoir juré ainsi. Mais certains n’ont que faire des paroles accordées.

— L’or fait tourner bien des têtes ! opina Abu Malik.

— Il les fait souvent tomber, aussi » enchérit le soldat, le visage sévère.

Il se réfugia dans ses pensées encore un moment avant de briser de nouveau le silence.

« Le gouverneur Nağm ad-Dīn Ayyūb est fidèle, de ce que j’en sais. Il ne semble guère désireux de porter le fer et le feu chez l’ennemi. Il parle plus de ses matchs de polo que d’autre chose ces temps-ci. Sans compter que le tribut est dérisoire en regard du montant de l’iqta[^iqta] qu’il perdrait si pareille forfaiture venait aux oreilles de l’émir. Il nous faut donc chercher ailleurs.

— Les coffres étaient à la garde du wali de la citadelle Izz al-Dīn. Je ne le connais guère…

— C’est un ghulam de l’émir. J’ai eu peu affaire à lui. On le dit solide dans la mêlée, très bon archer et tacticien avisé. Il est aussi très autoritaire.

— Il ne serait peut-être pas très heureux d’apprendre que les monnaies confiées à sa garde ont été falsifiées. Ne pouvons-nous l’interroger discrètement ?

— C’est à voir. Il ne me semble pas homme à pouvoir se faire pareillement flouer.

— Cela voudrait dire qu’il serait au courant. Soit d’un plan de l’émir, dont nous ne savons rien, soit de la conspiration qui porte atteinte à son honneur. »

Embarrassé, Yalim hocha le menton. Il évoluait depuis l’enfance dans ces milieux de pouvoir, où le danger n’était pas moindre dans les soirées aux discours feutrés qu’au plus fort des batailles. Il avait vu autant de têtes tomber après des manigances qu’au cours des mêlées. Il détestait cet univers de faux-semblants et en arrivait parfois à porter plus grande estime à des adversaires acceptant un affrontement loyal les armes en main qu’à ses coreligionnaires amateurs d’intrigues sournoises.

« Pour moi, indiqua al-Zarqa’, le personnage central est le mutawali ad-diwan^[intendant de la chancellerie]. C’est lui qui était chargé de rassembler la somme, il avait toute latitude pour en falsifier le versement.

— Qui est-ce désormais ? s’enquit Abu Malik.

— Amīn ad-Dīn Zayn al-Hağğ. Un obscur fonctionnaire venu dans l’orbite de l’émir. »

À sa voix, Yalim ne portait guère d’estime au personnage.

« S’il y a eu complot, je ne serai pas étonné qu’il en soit. Il avait toutes les clefs en main pour cela. Et cela ne m’étonnerait guère de lui.

— Quelques taches obscurciraient donc son honneur ?

— Je n’en sais que peu sur lui, mais il a les yeux d’un homme prêt à tous les commerces. »

Al Zarqa’ lança un regard à Abu Malik. Ce dernier n’en menait pas large. Il faisait confiance à son ami et, par extension, à Yalim. Mais il commençait à regretter de s’aventurer en si périlleux terrain. Il lui semblait qu’il s’apprêtait à frapper de ses mains nues un essaim de guêpes. En ces périodes tumultueuses, garder sa tête sur ses épaules dépendait parfois étroitement de sa propension à ne pas mettre son nez en des endroits inconnus. Mais c’était trop tard pour les remords. Il repensa à son cousin Idris. Il ne lui aurait jamais parlé d’une telle affaire, conscient que cet esprit volage n’aurait pas su tenir sa langue. Mais il était certain que ses yeux auraient pétillé et qu’il aurait lancé, goguenard : « Donne un cheval à celui qui dit la vérité ; il en aura besoin pour s’enfuir. »

## Jérusalem, hôtel de Bernard Vacher, fin d’après-midi du mardi 26 février 1157

Abu Malik était confortablement installé sur un siège rembourré de coussins moelleux. Le bâtiment n’était pas en très bon état, mais ça avait été une riche demeure par le passé. On sentait qu’elle était délaissée. L’homme qui l’avait accueilli était un soldat renommé, vétéran de nombreuses batailles. Il semblait par contre avoir négligé sa vie personnelle et possédait une maison sans âme, au silence à peine troublé par une poignée de domestiques. Si rien ne manquait, chaque chose paraissait avoir été retenue pour son unique caractère utilitaire et fonctionnel. Même dans cette pièce de réception, les ornements brillaient par leur absence.

Bernard Vacher lui faisait l’effet d’un homme las. Des cernes sous les yeux accentuaient son attitude avachie. Il s’attendait à un fier-à-bras à l’allure martiale, il découvrait une personne épuisée qui parlait d’une voix éteinte. Abu Malik l’avait appris : c’était lui qui avait accepté le tribut contrefait. Il devait certainement être sous le coup d’une disgrâce, malgré ses années de service fidèle. Abu Malik n’avait abordé la question qui l’inquiétait qu’après avoir évoqué le problème des prisonniers récents, qu’il espérait pouvoir faire libérer avant que les choses ne s’enveniment. L’absence de frère Raymond, en déplacement, l’avait contraint à dévoiler ce qu’il avait découvert à ce chevalier dont il ne savait que peu.

C’était de nouveau un risque qu’il prenait, se fiant à la parole d’Altûntâsh^[Voir le Qit’a *Basses eaux*]. Chaque nouvelle décision lui pesait encore plus que la précédente. Néanmoins, il était persuadé que ce qu’il faisait était aussi utile que de faire libérer des captifs. Sa conscience, ou Allah, ne l’aurait pas laissé en paix si des hommes perdaient la vie par suite de sa passivité.

Bernard Vacher, la mine renfrognée, avala un peu de vin.

« Je ne vous mercierai jamais assez de m’apprendre tout cela. J’imagine combien cela vous coûte et les risques que cela vous fait prendre.

— Un mien cousin aurait rétorqué qu’*il n’est pas d’un brave homme de tarder à rendre service*.

— Ce me semble que le dicton précise *ni de se presser à la vengeance* » sourit sans joie Bernard Vacher.

Ls deux hommes échangèrent un regard de connivence, soulagés de trouver un compagnon d’aventure en cette périlleuse voie.

« Le plus important est que l’émir n’ait nul désir de provoquer batailles. Cela au moins est de quelque réconfort.

— Sauf que votre roi vient d’allumer l’étincelle qui embrasera peut-être l’été. C’est là possible espérance des comploteurs. »

Bernard Vacher maugréa. Il ne pouvait préciser son idée : Nūr ad-Dīn n’ayant pas prémédité tout cela, il n’avait pas de plan offensif, de stratégie mûrement réfléchie pour les semaines à venir. Il serait aussi pris au dépourvu de la récente attaque de Baudoin sur le Golan que le roi de Jérusalem l’avait été par le versement du tribut en fausse monnaie. Au moins aurait-il une bonne nouvelle à annoncer au connétable.

« Je vais voir ce que je peux faire pour tenter d’éviter que la situation s’envenime. Nulle forteresse n’a été prise. Il ne s’agit que de rembourser ce qui a été razzié.

— Il demeure les blessés, les captifs et les morts. Leur mémoire pèsera lourd dans toute discussion. »

Bernard Vacher baissa la tête, les sourcils froncés. Il avait suffisamment arpenté les rues de Damas pour savoir qu’on ne pardonnerait pas aisément au roi de Jérusalem d’avoir lancé ses troupes sur des pacifiques nomades alors même qu’une trêve était en cours. Si les puissants n’hésitaient pas à trahir quand ils y voyaient un avantage, les modestes gens en concevaient longue rancune. Surtout, comme le disait Abu Malik, lorsque des humbles avaient été victimes, une fois de plus.

« Pourquoi votre roi n’a pas annoncé publiquement l’affront ?

— Il est déjà assez marri qu’on l’ait cru assez stupide pour ne pas reconnaître de fausses monnaies. Il n’a guère envie de clamer à tous qu’on le prend si peu en considération. Tous nos adversaires auraient beau jeu de le moquer et de ne lui accorder plus aucun crédit.

— L’ombre de la reine l’inquiète donc encore ? »

Le chevalier lança un regard surpris au négociant damascène. Il ne s’attendait pas à ce que la politique interne du royaume soit si bien connue chez leurs adversaires. L’idée lui était venue, en effet, que Baudoin craignait surtout que sa mère, Mélisende, n’apprenne sa déconvenue. Il n’avait réussi à l’éliminer du pouvoir que récemment et se sentait encore en délicate posture par rapport à elle, qui avait dirigé les territoires d’une main de fer pendant des années, aux côtés de son époux Foulque puis en tant que régente. Pour toute réponse, Bernard Vacher se contenta de hausser les épaules.

« Il va me falloir tenter de convaincre le connétable et le sénéchal, ainsi que le roi, que l’émir n’a rien manigancé. Histoire d’apaiser les esprits. Ensuite, espérons que les comploteurs seront démasqués. Avez-vous idée en ce sens ?

— Je n’ai guère vers qui me tourner. Chaque visage amiable peut cacher une âme de conspirateur. Je connaissais les anciens membres de l’administration, mais bien peu sont demeurés en place. Des amis à moi pistent la vérité…

— Ne prenez pas de risque. Si les voleurs n’ont pas craint de provoquer batailles, ils n’auront certainement pas d’égard pour vous.

— Je louvoie au milieu des serpents, j’en ai bien conscience. »

Bernard Vacher hocha la tête lentement.

« En pourpensant toute l’affaire, je me suis dit que Muhammad ben afar, le chambellan qui m’a remis les coffres, était certainement dans la confidence. Il avait accès aux monnaies et aurait pu éventer toute l’opération. Peut-être pourrez-vous en apprendre plus sur lui. Il m’a fait l’effet d’être veule plus qu’intrépide.

— Pareils hommes se dévoilent parfois dangereux, frappant sans discernement. Mais je vous remercie, je me renseignerai. »

Le négociant s’apprêtant à partir, Bernard Vacher lui laissa quelques coordonnées de contacts utiles, en cas de besoin. Sa main ne saurait le protéger par-delà l’Arbre de la mesure[^arbremesure], mais il lui fit serment de lui faire escorte pour peu qu’il soit sur les terres latines. Son frère tenait un château royal, à Naalein, et serait averti d’accueillir au mieux le Damascène. Le chevalier se sentait humilié par l’histoire et se raidissait dans sa posture d’homme loyal et fidèle à sa parole. La proposition arracha un sourire triste à Abu Malik lorsqu’il prit congé.

## Damas, demeure de Abu Malik, soirée du youm al sebt 25 muharram 552^[Samedi 9 mars 1157.]

La maison était en effervescence, malgré les efforts d’Abu Malik pour ne pas inquiéter ses domestiques et sa famille. Il ne s’était confié, quoique superficiellement, qu’à Fazila, son épouse, afin de ne pas alimenter les inévitables rumeurs qui ne manqueraient pas de naître à peine auraient-ils franchi Bab al-abiya^[La principale porte occidentale de Damas.]. Officiellement, il prenait la route pour affaires durant plusieurs mois et avait décidé d’emmener sa femme avec lui, car ils devaient aller saluer des cousins. La maisonnée s’était étonnée de ce départ bousculé.

En réalité, cela faisait suite à une discussion qu’Abu Malik avait eue la veille, lors de la prière du vendredi à la Grande Mosquée. Al Zarqa’ lui avait confié que des remous commençaient à se faire sentir dans les sphères du pouvoir. Des échos de leurs questions posées ici et là retentissaient un peu plus fort qu’ils ne l’auraient espéré. Le fonctionnaire avait conseillé à Abu Malik de s’absenter un moment, laissant le temps aux choses de se tasser. S’il disparaissait, on ne pouvait le soupçonner d’enquêter sur des sujets interdits. Quant à al-Zarqa’, il ne s’inquiétait guère de la curiosité dont il serait l’objet, ses demandes pouvant tout à fait être légitimées par ses attributions officielles et sa réputation tatillonne.

Éclairé d’une maigre lampe, Abu Malik triait certains de ses documents de commerce, désireux de ne pas négliger les échanges en cours lorsque Fazila vint l’interrompre. Elle repoussa à demi la porte derrière elle, surveillant du coin de l’œil que personne ne s’approchait dans la salle adjacente.

« J’ai fini de préparer nos habits. Ne peux-tu m’en dire plus sur notre destination ?

— Je ne sais pas encore. La famille d’al-Baqara^[Bernard Vacher] tient le casal de Naalein, nous allons tout d’abord nous rendre là-bas. Puis nous aviserons.

— J’espère que nous n’aurons pas à demeurer chez les polythéistes. Nous avons nos vies ici : nos amis, nos familles…

— Ce n’est que pour un temps ! »

Les yeux de Fazila se firent inquisiteurs, la bouche pincée.

« Je ne comprends pas pourquoi nous devons pareillement fuir notre cité ! Si tu te sens menacé, tu n’as qu’à te plaindre au qadi !

— C’est une affaire compliquée, Fazila. En parler publiquement ne causerait que grands torts.

— Plus grands qu’ainsi tout quitter comme des voleurs ? Alors même que nous n’avons rien fait ?

— Cela nous dépasse, Fazila. Les conséquences vont au-delà de ce que je peux imaginer. »

La jeune femme leva le menton, plissant la bouche en signe de mécontentement. Elle avait cru épouser un simple commerçant, de bonne réputation par ses actions charitables, et elle découvrait que c’était un intrigant qui refusait de lui expliquer par le menu ce qu’il avait fait pour les exposer ainsi, elle et ses filles. Comprenant qu’il ne lui dirait rien de plus si elle cherchait l’affrontement, elle décida d’adopter une attitude plus conciliante. Elle s’accroupit devant lui, affichant un visage plus amène.

« Tu es un bon mari, Rashid, j’en rends grâce à Allah chaque jour. J’ai seulement grand désir d’être à tes côtés en chaque épreuve de la vie. Nous avons surmonté la mort de Malik sans nous déchirer, en confiance. Ne m’écarte pas ainsi. »

Abu Malik reposa ses feuillets et lui prit la main, touché de cette déclaration.

« T’en dévoiler plus risquerait de te mettre en danger. Prends ce voyage pour ce que nous prétendons qu’il est : l’occasion de cheminer ensemble et d’aller voir des proches. Nous irons de certes à Misr[^misr] voir tes cousins, depuis le temps que tu m’en fais la demande. Avant cela, nous ferons juste halte au royaume de Badawil[^baudoinIII]. »

Elle lui accorda un sourire engageant avant de se relever. Lorsqu’elle se tourna pour repartir, elle ne retint plus son désappointement et son visage s’allongea de dépit. Elle ne répondit que par un hochement à la dernière instruction de son époux.

« J’ai fait mander Ibrahim pour demain matin. Il chargera les bêtes et nous pourrons prendre la route rapidement. »

À l’idée de ce voyage, Abu Malik se sentait fébrile comme il ne l’avait plus été depuis des années. Il parcourait les routes entre Damas et Jérusalem si souvent qu’il était familier de chaque lacet, chaque arbre, tout autant que des hébergements en chemin. Mais c’était la première fois qu’il partirait la peur au ventre, sans certitude quant à son retour. ❧

## Notes

La suite des événements initiés dans *Pour une poignée de dinars* demeure tout aussi conjecturale que son début. Je profite de l’occasion pour présenter un peu plus en détail le fonctionnement humain des deux principaux pouvoirs qui s’affrontaient pour le contrôle des territoires levantins. Loin d’être des structures monolithiques, guidées par des dirigeants tout-puissants au destin d’exception, c’étaient des amalgames de volontés disparates, qui ne tiraient pas toujours dans le même sens.

Bien que cela ne soit qu’un exercice spéculatif, j’avais envie de vous proposer de suivre la façon dont un tel complot aurait pu exister et comment il aurait affecté la vie quotidienne de simples individus. Même s’il s’agit de fiction (et tout en assumant clairement ce postulat), je pense salutaire d’imaginer les conséquences pour des acteurs mineurs, mais représentatifs, je l’espère, des deux camps. Nous sommes ici loin d’une vision de l’histoire déterministe, propre à bâtir des romans idéologiques. J’adopte un point de vue plus fractal, tentant d’analyser les rapports de force depuis les plus vastes groupes jusqu’au cœur de chaque individu entraîné dans ce grand tourbillon. Bien évidemment, pour avoir un quelconque intérêt dans l’alimentation de la perception et de la réflexion historiques, cela ne peut se concevoir qu’en puisant dans une documentation la plus large et sourcée que possible.

## Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tomes I, II et III, Damas : Institut Français de Damas, 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I, *Economic Foundations*, Berkeley et Los Angeles : University of California Press, 1999.
