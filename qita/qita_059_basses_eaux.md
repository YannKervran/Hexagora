---
date : 2016-09-15
abstract : 1156-1157. Peu après la prise de Damas par Nūr ad-Dīn, tandis que s’installe l’hiver, les habitants de la cité, appréciateurs de ses charmes chantés par les poètes, échangent autour de leur nouvelle situation.
characters : Ibrahim, al-Dabbi, Husayn al-Hindi, Razin, Al-Misri, Abu Malik al-Muhallab, Altuntash
---

# Basses eaux

## Damas, fin de matinée du youm el itnine 10 chawwal 551^[Lundi 26 novembre 1156.]

La pluie avait transformé la rue poussiéreuse en un bourbier résonnant de bruits spongieux à chaque pas des bêtes et des hommes. La tête basse, Ibrahim sentait les gouttes parcourant son visage malgré la capuche de son durrâ’a[^durraa]. Chaussé de savates, il manquait de perdre ses souliers à chaque fondrière. Sans même y penser, il claqua sa badine sur l’arrière-train d’un de ses ânes, qui semblait vouloir s’arrêter et héla celui de tête pour qu’il continue à avancer. Avec la douche qu’ils recevaient depuis le matin, au moins n’aurait-il pas à brosser longuement les bêtes. Il espérait ainsi pouvoir enfin se mettre à l’abri d’ici peu.

Les rues étaient quasiment désertes, les Damascènes savaient que les intempéries n’étaient jamais persistantes. Ils attendaient donc, le nez collé aux claustras des fenêtres que la météo s’améliore. Seuls les voyageurs et les plus démunis des travailleurs étaient forcés de s’aventurer au-dehors.

Ibrahim appartenait un peu aux deux. Il menait des caravanes d’ânes entre les villages alentours et Damas, apportant les aliments frais des vergers et jardins et y emportant les produits manufacturés. Il louait les bêtes ou était embauché par de riches propriétaires, espérant amasser avec le temps de quoi s’acheter ses propres animaux. Depuis bientôt dix ans que son père l’avait poussé au travail, il n’avait guère vu progresser son pécule. Il s’acharnait malgré tout, y compris les jours où il faisait bon rester chez soi.

Il parvint enfin au bassin où il faisait boire les ânes avant de les rentrer dans leur écurie, dans un appentis contigu à l’immeuble où il habitait. Grossière maçonnerie appuyée le long d’un mur, sous une profonde arcade qui lui faisait comme un toit, l’eau y coulait claire et pure, depuis une canalisation dérivée de l’alimentation du hammam voisin. Coincé sur la margelle, se protégeant de la pluie de son mieux, il reconnut le vieil al-Dabbi.

C’était un mendiant à la silhouette tordue, la barbe miteuse et le turban en désordre, qui errait autour de son quartier, vivant des offrandes et de la bonté des riches donateurs à la mosquée. Il amusait les enfants avec ses histoires, se louait à l’occasion comme conteur lors des fêtes, mais dépendait surtout de la charité. Il salua Ibrahim en ricanant, tirant sur sa tête un repli des chiffons qui lui servaient de vêtement.

« Alors l’Ânier, on brave la pluie ?

— _Dieu aime ceux qui persévèrent_, c’est pas vrai, l’ancien ? »

Tout en parlant, Ibrahim répartit ses animaux le long du bassin et vint se coller contre le mur, pour profiter à son tour du relatif abri. Il tordit le bord de sa capuche pour en exprimer l’eau, qui se répandit en une flaque à ses pieds.

« Tu n’as pas trouvé un endroit où te mettre au chaud ? Il ne fait pas bon rester au-dehors par un temps pareil…

— Personne n’a envie d’entendre un vieux fou, pleurant après nos gloires passées !

— Des raisons de se lamenter ? Je suis entre Berzé, Qâbûn et Gawbar depuis plusieurs jours et les nouvelles y arrivent chaque fois bien tard. »

Le vieil homme haussa les épaules et gloussa.

« À toi je peux bien le dire, ton père, qu’Allah le garde, est un vrai dimashqi, et je te crois comme lui. »

Il se mit à fredonner et marmonna un peu entre ses dents, tandis qu’Ibrahim surveillait d’un œil les ânes qui s’abreuvaient tranquillement. Le salmigondis devint progressivement des mots, jusqu’à former des phrases intelligibles.

« _Le monde de Damas est agréable pour celui qui l’a choisi et je ne veux pas d’autre lieu que Damas pour vivre ici-bas_. Ce doit être les larmes d’Abû Bakr as-Sanawbarî que nous recevons sur la tête, à nous voir si avilis. »

Il fouetta l’air de sa main, comme pour en éloigner un fantôme et ajouta ses postillons aux gouttes, forçant sur sa voix.

« Tu sais que le turc qui a forcé notre cité a plié le genou devant les polythéistes ?

— Il y a eu bataille ? Je n’en ai eu nul écho !

— Si seulement ! La honte serait moins brûlante ! »

Al-Dabbi renifla, se frotta le nez et lança, un air de dégoût barrant son visage :

« Nous devons verser tribut à nouveau, belle réussite pour l’émir ! Il n’en a que pour ses terres du Nord. Il ajouté le Bilad al-Sham à son collier de perles et n’a cure de notre honneur, de notre prospérité…

— Payer ? Encore ? Ne devait-il pas nous affranchir de ces taxes impies ?

— On l’a appris à la mosquée, au grand prêche dernier. Huit mille dinars sûri[^suri] ! »

Ibrahim poussa un long soupir, cela n’allait pas arranger ses affaires. Sans en faire trop état, malgré sa famille, il était de ceux qui avaient vu d’un œil plutôt favorable l’arrivée de Nūr ad-Dīn. Il y espérait une occasion de sortir du marasme dans lequel la ville se complaisait depuis son enfance. Il gardait encore un souvenir effrayé de l’armée des chrétiens presque dix ans auparavant, avec leurs grands engins. Après cela, il avait craché, comme tant d’autres, dans les pas des émissaires qui venaient percevoir la redevance de la honte qui leur garantissait une relative tranquillité.

L’arrivée d’un chef de guerre compétent, dont les oulémas vantaient les qualités pieuses, avait redonné courage dans les quartiers, même si la fierté empêchait de le reconnaître publiquement. Et maintenant que son nom était crié par le muezzin, depuis la grande mosquée où le prophète avait prié lors de son voyage nocturne^[Tradition rapportée par al-Hasan ben Yahyâ al-Hissani.], Nūr ad-Dīn négociait ignominieusement. La ville espérait un héros et s’était livrée à un homme.

Ibrahim leva la main pour saluer le vieux mendiant et tendit le bras pour attraper la bride d’une de ses bêtes. Il avait encore du travail avant de pouvoir se reposer. Il ajusta sa capuche et s’élança sous l’averse.

« Préserve-toi l’ancien !

— _Le nez dans les nues, les fesses dans l’eau_ ! » rétorqua al-Dabbi en agitant des doigts l’eau du bassin.

## Damas, après-midi du youm al had 29 dhu al-qi’dah 551^[Dimanche 13 janvier 1157.]

Husayn al-Hindi avançait en mâchonnant une lanière de pain, partage de la miche avec ses deux fils. Le plus grand, Hasan, portait comme lui une énorme jarre dans un filet sur son dos. Ils proposaient à boire, récoltant des piécettes de cuivre ou des jetons que le petit Ahmed, d’à peine cinq ans, recueillait dans un gobelet. Husayn était bossu, en permanence courbé sous la charge, comme son père avant lui et comme Hasan le serait bientôt. Mais il nourrissait sa famille chaque jour et accomplissait sa tâche avec ténacité.

Il lissa sa longue moustache en voyant l’attroupement près de la fontaine. Il mettait un point d’honneur à ne prendre que de l’eau claire, jamais tirée d’un bassin ou d’un puits. Son père lui avait enseigné l’importance de la réputation, et il y tenait, autant qu’à ses deux vastes récipients en terre qui leur brisaient l’échine.

Quelques femmes attendaient de remplir leurs cruches, pauvresses sans le sou, sans mari ou simples domestiques. Husayn leur adressa un salut respectueux. Il était toujours tenté de lier conversation, d’autant qu’il savait son sourire assez séduisant. Mais il se méfiait des commérages et ne faisait cela que lorsque les témoins étaient rares. Sa jeunesse sulfureuse était loin derrière lui et il n’avait pas envie de s’attirer des ennuis par son goût pour la badinerie.

Ils posèrent les céramiques contre le mur, s’étirant le dos tout en patientant. D’un gris froid, le ciel semblait prêt à déverser une averse à tout moment. Avec les faibles températures, Husayn n’aurait pas été étonné de voir de la neige le lendemain au matin. Ce qui n’arrangeait pas du tout ses affaires, ses maigres économies épargnées durant les périodes chaudes lui permettant souvent à peine de faire la jointure d’hiver, sans compter le renchérissement parfois effrayant des denrées.

Il s’accroupit, les coudes sur les genoux. Ses deux fils jouaient dans la poussière, avec des cailloux qu’ils amassaient dans les replis de leur tenue. Il en profita pour estimer le gain fait depuis le matin et grimaça devant le faible montant. Il leur aurait fallu au moins le double.

Entendant un chant familier, il tourna la tête et vit arriver deux solides Nubiens, chargés d’énormes outres : Al-Misri et son frère Razin. Husayn se leva et les salua chaleureusement. Ils se croisaient souvent à une fontaine ou l’autre, mais ne se faisaient nullement concurrence. Husayn proposait de l’eau à boire, qui demeurait fraîche dans la céramique, même en plein soleil. Les deux noirs apportaient celle pour les usages domestiques, et seuls les plus pauvres la consommaient parfois, car elle prenait vite un goût ranci, accentué par la tiédeur inévitable dans le cuir.

Razin dévoila le contraste de ses dents blanches en l’apercevant et vint le saluer. Leurs outres ne paraissaient guère ventrues. Pour eux aussi, l’hiver était un moment difficile. Malgré tout, jamais il n’avait entendu les deux frères, arrivés d’Égypte quelques années auparavant, se plaindre. Al-Misri était le plus volubile, en général, et Husayn le trouvait plus cancanier qu’une assemblée de femmes. Il avait toujours une belle anecdote à raconter, ce que semblait indiquer son air gourmand lorsque leurs regards se croisèrent.

« On dirait que tu as vu un trésor, al-Misri, s’enthousiasma Husayn.

— _Par ici, une pomme qu’on prendrait pour une joue ; par là une grenade qu’on prendrait pour un sein. Comme il est agréable le séjour de Dârayyâ ! Là j’ai mené une vie aussi limpide que le miel_. »

Le Nubien hochait la tête en récitant les vers et s’amusait par avance de ce qu’il allait raconter. Il posa son lourd fardeau, avant de se rapprocher pour narrer son histoire.

« Tu sais que les émissaires de l’émir sont partis porter gros trésor aux Ifranjs ?

— Toute la ville ne parle que de ça. Puissent les Ifrits leur rôtir les pieds, aux infidèles. Mais au moins nous n’avons plus à les voir en la muraille.

— J’y vois un autre avantage… Un des émirs désignés à ce grand honneur possède plusieurs belles esclaves en plus de son épouse. Il les garde en une charmante demeure, vers Bâb Sarqi^[Une des portes de la cité de Damas.]. Et là, elles se languissent de lui, confiées à deux vieilles matrones à l’odeur de chamelle.

— Ne me dis pas que tu as pu y pénétrer…

— C’est qu’elles aiment à prendre des bains, chez elles. Il faut bien leur porter de l’eau. Mais ce n’est pas là métier pour toi. »

Il assortit sa remarque d’une caresse amicale sur le bras d’Husayn, qui avait vérifié que ses fils n’entendaient pas. Il n’avait nulle envie que cela revienne aux oreilles de son épouse, dont le caractère aimable n’incluait pas la complaisance à la légèreté des mœurs.

« L’autre jour, nous remplissions la cuve, avec Razin, sous l’étroite surveillance d’une des vieilles. Mais comme elle était seule, une des jeunes beautés m’a chuchoté quelques mots dans le couloir.

— De quel genre, ces mots ?

— Du genre qui expliquent comment passer par une des fenêtres mal fermées en certaines occasions. »

Husayn ricana au sous-entendu et secoua la tête. Il n’avait jamais su dire si le Nubien était un affabulateur ou s’il avait une chance insensée. En tout cas il lui offrait l’occasion de vivre pas mal d’aventures par procuration.

« Un jour tu finiras par avoir des ennuis, à ainsi tremper ta plume à tous les pots.

— Qui suis-je, qu’un pauvre affranchi que personne ne voit ?

— Il n’y aura justement guère pour t’aider si un mari jaloux te fait couper les bourses, au mieux.

— Faire de moi un eunuque pour que je vive au milieu des femmes ? Me menacer de ça, c’est comme menacer le canard de la rivière !^[Allusion au proverbe qui dit : Menacer le brave de la mort, c’est menacer le canard de la rivière.] »

Husayn secoua la tête, manifestant son désaccord en pure forme. Il tapa dans ses mains, souleva son imposante jarre et alla la placer sous le jet d’eau. Il ricanait encore tandis que le flot chantait en remplissant la panse.

## Damas, fin d’après-midi du youm al sebt 11 muharram 552^[Samedi 23 février 1157.]

 Au moment de franchir le pont du Barada, Abu Malik al-Muhallab frissonna et s’emmitoufla dans les rafrafs[^rafraf] de son turban. Un vent léger sifflait sur les murailles septentrionales de la cité, faisant voler la poussière du chemin. Quelques gamins étaient affairés à ramasser des détritus sous le pont, dont ils triaient ce qu’ils espéraient revendre. Il frémit de les voir ainsi tremper dans le courant glacé.

 Face à lui la route se divisait en de nombreuses voies qui allaient serpentant parmi les jardins et les vergers. La guerre y avait prélevé son tribut, surtout sur les bâtiments les plus proches, mais l’endroit gardait une part de son charme, et ce même au plus fort de la saison hivernale. Il chercha du regard ce qui pouvait être un débit de boisson, perdu dans les décombres de ce qui avait été le haut lieu des fêtes de la bourgeoisie damascène pendant des décennies, si ce n’était des siècles.

 Il lui fallut un petit moment pour trouver l’établissement qu’il espérait être le bon. Ce n’était qu’un très modeste estaminet, où un maigre feu au centre de la pièce apportait une clarté et une chaleur appréciables, au prix d’une atmosphère enfumée. Avisant sa belle apparence, le tenancier vint l’accueillir avec obséquiosité et le mena avec cérémonie à la personne qu’il avait demandé. L’homme tirait visiblement grande fierté d’avoir en ces murs un tel personnage.

 Ce n’était pourtant qu’un vieil aveugle, appuyé contre une cloison rongée, qui jouait au manqala avec un gamin. Seule la qualité des étoffes de ses robes permettait de ne pas le prendre pour un mendiant. Le port de tête, également, trahissait le rang. Abu Malik s’approcha et s’inclina par pur réflexe.

 « Salaam, j’ai pour nom Abu Malik al-Muhallab. J’ai bien l’honneur de parler à Altûntâsh, ancien gouverneur de Salkhad et Bosrâ^[Villes du sud de la Syrie.] ?

— J’ai peut-être été celui-là par le passé, mais je ne suis désormais plus qu’un vieil homme auquel les cadis ont fait crever les yeux… »

Tout en répondant, il invita d’un geste son interlocuteur à s’asseoir et chassa le gamin avec lequel il jouait jusque là.

« Voyez comme le destin est ingrat. La main autrefois si forte à brandir l’épée se risque à peine à manier le jeton face à un enfant. Le brave gamin me mène chaque jour ici… _La fraîcheur étanche ma soif et puissent mes jours être abreuvés par le Barada et en goûter les pâturages_. »

Tandis qu’il récitait, un sourire triste creusait des sillons dans ses traits. Il leva la main, qui se mit à ondoyer.

« La première fois que je suis venu ici, avec mon seigneur et maître, Kumustakin, j’ai cru entrer au Paradis. Le murmure des eaux du Barada avive ces souvenirs en moi.

— Vous n’avez pas une demeure parmi les jardins, plus accueillante que cette masure ?

— Les juges m’ont pris ma fortune en même temps que les yeux. »

Il soupira longuement.

« J’aurais espéré que l’émir, une fois maître de la ville… »

La phrase mourut sur ses lèvres.

« Mais vous n’avez pas bravé le froid de l’hiver pour entendre les jérémiades d’un vieux fou. Puis-je vous être utile à quoi que ce soit ?

— Je viens vous voir pour savoir si vous pourriez m’indiquer des gens à contacter pour racheter des captifs. Vous avez appris la razzia récente du roi Badawil[^baudoinI] ?

— Difficile d’y échapper. La trêve n’aura pas tenu trois mois, voilà coûteux tribut. »

Il se gratta la joue, à la barbe soignée, mâtinée de poivre et de sel.

« Je suppose que vous pensez à des Ifranjs que j’aurais pu connaître par le passé.

— N’importe qui capable de m’assister à la libération des captifs serait le bienvenu.

— Mon nom risque de vous fermer des portes plus qu’autre chose. Ceux que j’ai le mieux connus… »

Il fit une moue, tourna la tête, se réinstalla sur son coussin, comme si sa posture devenait incommode.

« Il n’en est qu’un, peut-être. Un esprit rusé, et certes pas un ami. Mais parfois nos adversaires sont plus honorables que nos amis. »

Il marqua une longue pause, comme s’il se remémorait de lointains et pénibles souvenirs. Sa voix était rauque lorsqu’il se décida à parler de nouveau.

« Demande après al-Baqara^[Bernard Vacher]. Ne te fie pas à son nom, c’est un sage. Il avait l’oreille du jeune roi. C’est un farouche adversaire, mais il y a du juste en lui. » ❧

## Notes

La cité de Damas qui avait réussi à maintenir un pouvoir indépendant depuis le début des Croisades finit par être rattachée à un empire plus vaste lorsque Nūr ad-Dīn l’intégra à ses territoires. Ce fut le résultat d’un long travail préparatoire de sa part, et lui offrait l’occasion d’unifier une large zone, suffisante pour opposer désormais un front quasi uni face aux Latins.

Pour les habitants de Damas, ce fut certainement au prix d’une profonde amertume qu’ils se virent ainsi absorbés. Ils abritaient un des lieux les plus prestigieux de l’Islam, avaient même été le siège du califat un temps. Pourtant ils étaient traités comme un fief provincial par un conquérant turc, basé dans le nord, à Alep. Saladin, qui grandit en partie à Damas, offrira de nouveau un rôle important à la cité syrienne, mais on peut sentir le ressentiment des chroniqueurs à l’idée de voir leur indépendance foulée au pied, malgré les acclamations de façade pour Nūr ad-Dīn.

L’histoire d’Altuntash demanderait de longs développements, mais son triste sort est intimement lié aux complexes jeux diplomatiques qui se sont déroulés entre Alep, Jérusalem et Damas pendant plus d’une décennie. Nous ne disposons que de peu d’informations, mais il semble s’être trouvé au centre d’un dangereux stratagème entre les trois puissances. Il est possible que cela m’offre à l’avenir l’occasion de proposer des hypothèses sur ce qui s’est passé en Syrie du sud en 1147 une fois certaines pistes mieux étudiées.

Enfin, le poème partagé entre les protagonistes, auquel la traduction ne rend pas vraiment grâce est cité par Ibn ’Asâkir et attribué à Abû Bakr as-Sanawbarî. Il appartient à une tradition de textes laudateurs sur la ville de Damas, dont le caractère sacré, la qualité architecturale et la beauté naturelle sont régulièrement mis en exergue par les artistes.

## Références

Élisséef, Nikita, *La description de Damas d’Ibn ’Asâkir*, Institut Français de Damas, Damas : 1959.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.
