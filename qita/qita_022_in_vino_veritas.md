---
date: 2013-09-15
abstract: 1156. Contrairement à ce qui a longtemps été envisagé, l’implantation latine dans le royaume de Jérusalem ne s’était pas circonscrit aux villes. Des gens originaires d’Europe s’étaient installés comme colons, souvent dans des zones aménagées exprès, en tout cas toujours bien à l’écart des communautés non chrétiennes. Les musulmans qui étaient demeurés sur le territoire chrétien avaient un statut de serfs, d’esclaves, et étaient considérés au même titre que du mobilier. Bien que leur sort soit parfois considéré comme relativement préservé, de l’aveu même de chroniqueurs musulmans de passage, certains eurent à subir la dure férule de seigneurs violents et des zones entières se dépeuplèrent.
characters: Bertaud le Sueur, Layla, Robert de Retest
---

# In vino veritas

## Terre sainte, abords de Naplouse, après-midi du mercredi 22 février 1156

Bertaud le Sueur était satisfait. Assis sur le muret de pierre sèche ceinturant sa parcelle, il savourait des salaisons. Les chairs flasques de son visage tressaillaient à chaque bouchée, tandis que se tendait son long cou ridé, mal rasé. Goûtant une pause en cette fin de journée, il admirait son bien : une poignée d’acres d’oliviers disséminés sur une restanque au-dessus de Naplouse.

Cela n’avait pas été sans peine, de nombreux cultivars avaient dû être taillés, parfois sévèrement, pour les fortifier, mais l’entretien en était désormais plus facile. Non pas qu’il ait jamais transpiré à un tel labeur, Dieu l’en préserve. Il s’en tenait à donner des ordres, à surveiller le travail des autres. Personne n’aurait osé l’appeler de son surnom, « Le Sueur » en sa présence, car le sobriquet lui avait été choisi par ceux-là mêmes qui trouvaient qu’il n’en dépensait guère, de sueur. Assuré de sa supériorité naturelle, il laissait cela à des manouvriers, des journaliers, sur lesquels il pouvait en outre déverser sa mauvaise humeur et ses remarques assassines.

Mastiquant avec énergie, il était d’ailleurs en train de préparer une éruption libératrice. Guère de raison à cela, pourtant : depuis le matin, ses ouvriers coupaient les branches pour la fructification sans qu’il n’y trouve motif à réprimande. Rien n’était retiré qu’il n’aurait ôté lui-même, sans pour autant entraîner d’oubli qu’il aurait pu pointer avec brio. C’était peut-être cela même qui commençait à l’agacer : ils ne faisaient rien de travers, n’offraient aucune occasion à son ire de s’enflammer.

Un craquement sonore résonna, assorti d’un juron expressif. Le faux-pas tant espéré avait fini par arriver. Bertaud se leva avec vigueur et s’avança à grands pas vers l’origine de l’incident. Il n’était pas encore sur place que les invectives s’élançaient de sa bouche, accompagnant des miettes de son repas assaisonnées d’une généreuse portion de postillons.

« Foutre ! N’as-tu jamais appris à respecter bien d’autrui, gâte-blé ? »

L’ouvrier, le manche brisé de la serpe toujours en main, n’eut guère le temps de répondre, les exclamations fusaient, impatientes, heureuses de surgir.

« Bien deshonnête est l’œuvrier qui s’emploie à la ruine de son maître ! Ne peux-tu prêter attention à mon oustillement, maudit croque-lardon ? »

L’employé se crut invité à répondre à cette question et tenta timidement d’objecter que l’emmanchement était bien fatigué, la soie de fixation rongée par la rouille. Cela décupla la fureur de Bertaud, le visage désormais cramoisi et les yeux exorbités.

« Cherches-tu à me faire insulte, face d’étron ? Voilà bien façons de fils à putain ! »

Mordant sa lèvre à défaut d’autre chose, il se tut un instant sans pour autant retrouver son calme. Sa voix était blanche de colère lorsqu’il ajouta :

« Puisque, non content de briser mon bien, tu m’en fais reproche, je vais te retenir deux des cinq deniers que je t’avais promis. Et si jamais je daignais un jour te reprendre, sache que tes gages seraient écornés d’un demi-denier par rapport aux autres, pour ta punition et ton édification ! »

Devant tant de fureur et de mauvaise foi, le malheureux se contenta de baisser la tête, subissant patiemment plusieurs salves triomphales du propriétaire vindicatif. De mémoire d’ouvrier, jamais personne n’avait réussi à avoir le dernier mot sur Bertaud. Et certes pas quand il avait l’opportunité de s’épargner de la dépense.

## Naplouse, soirée du mercredi 22 février 1156

Déambulant dans les ruelles étroites de la ville, un large broc vide à la main, Bertaud avait tout oublié de l’incident. Il était familier de ces coups d’éclat, dont la vague refluait aussi vite qu’elle s’abattait. Par ailleurs, les travaux de taille étaient presque achevés, et il sortait de chez le forgeron qui pouvait lui réparer la serpe pour un coût modique, inférieur à la retenue infligée.

Sifflotant, il marchait gaiement en direction de la taverne où il achetait chaque soir son vin pour le lendemain, un pichet d’un carton^[Voir les explications en fin de texte.]. Lorsqu’il entra dans la salle voûtée, il fut accueilli par le vieux chien de la maison, qui était bien le seul à lui témoigner spontanément de l’amitié. Il caressa la bête d’une main distraite tout en cherchant la silhouette du tenancier à la lueur discrète des lampes à huile. La voix le surprit tandis qu’il écarquillait les yeux sans résultat.

« Le bon soir, maître Bertaud. Je suis à vous dans un instant, je cale une futaille reçue ce jour des monts de Judée. Du rouge capiteux, qui n’a rien à envier aux productions de Chypre. Il pourrait vous plaire, j’en ai certeté.

— Je ne sais, j’ai usage de mon clairet chaque jour. À quel prix faites-vous le pichet ?

— Le carton en est à quatre deniers.

— Mordiable ! Que voilà bien dispendieux breuvage.

— Aucune truandaille en cela, il est vraiment digne de table de roi. Je ne saurai en rebattre un denier. Si vous en avez appétit, je peux vous en faire taster quelques goulées, pur, sans même le laver. »

Joignant le geste à la parole, le tavernier tira le coin du tonneau qu’il venait de mettre en perce et remplit un petit godet qu’il tendit à son client. Avalant avec méfiance les premières gouttes, ce dernier ne tarda pas à vider son récipient avec un plaisir évident.

« Je dois l’avouer fort goûteux, un vrai miel. Mais bien coûteux pour ma modeste bourse, je vais donc me contenter de mon vin habituel.

— Un carton de jaunet de Belvoir ? Je vous emplis ça. »

Prenant le pichet des mains de Bertaud, l’homme le remplit d’un vin clair, à la belle robe dorée à la lueur des flammes et l’échangea contre quelques monnaies argentées.

## Naplouse, après-midi du samedi 25 février 1156

Le temps frais n’incitait guère à l’indolence et Bertaud s’était résolu à couper quelques broussailles pour allumer un feu. Heureux de se réchauffer, il frappait avec entrain dans les branchages lorsqu’il entendit un bruit de course. Sourcils froncés, il se tourna, prêt à réprimander le manouvrier qui venait le déranger.

Il eut la surprise de voir passer une demi-douzaine de silhouettes en guenilles, cavalant de façon désordonnée. Parmi elles trottinait une fillette d’une dizaine d’années, l’air affolé, le visage sale. Il eut à peine le temps de croiser son regard fiévreux, implorant. Pourtant, sans raison claire, un début de sourire s’était dessiné sur ses traits avides. Reprenant ses esprits, il s’éclaircit longuement et bruyamment la gorge et s’absorba de nouveau dans sa tâche.

De retour à sa parcelle, les bras chargés, il y découvrit une troupe de soldats en armes, à cheval, menés par un homme en grand haubert. Un chevalier de haut rang, cela se voyait à son allure, et pas un chef facile, cela se lisait sur son visage rude.

« As-tu vu esclaves sarrasinois en fuite, l’homme ? »

Un peu interloqué, Bertaud hocha la tête en silence puis, lâchant sa brassée, fit un signe en direction du bosquet d’où il venait.

« Oui-da, j’en ai encontré une poignée voilà peu, en cette direction. Ils ne peuvent être bien loin. Je peux vous conduire si vous en avez désir. »

Acceptant la proposition d’un signe de tête, le chevalier mit sa monture au pas derrière Bertaud. Ce dernier trottinait, espérant s’attirer les bonnes grâces d’un important seigneur, tout en aidant à la capture de quelques musulmans. Il les avait toujours eus en horreur, se refusait à en employer sur ses terres. S’il en avait eu le pouvoir, ils auraient été tous contraints de se convertir, ou chassés au loin du royaume de Jérusalem.

Très rapidement le groupe de fugitifs fut en vue, entouré de cavaliers, rassemblé comme un troupeau pour l’abattoir. Le chevalier fouilla dans une de ses fontes et lança quelques pièces à Bertaud, qui les accueillit avec un grognement de satisfaction.

« La merci à vous, mon sire, je n’espérais pas récompense, mais suis seulement bon chrétien.

— Hé bien, tu sauras alors quoi faire de cette cliquaille pour soigner ton âme. Je pourchassais ces vauriens depuis le matin, échappés d’un de mes casaux. »

Il approcha sa monture d’un des fuyards, l’obligeant à reculer d’un coup de poitrail de son étalon. Puis d’un claquement de doigt, il mit en branle ses hommes.

« Celui-là me semble bien fier. Apprenez-lui donc à tenir son rang. Un pied brisé devrait l’inciter à demeurer en mon fief. »

L’homme se débattit de son mieux, mais ne pouvait rien faire face au nombre. Devant ses compagnons d’infortune, tenus en respect à la pointe de l’épée, on lui brisa une cheville, d’un coup sec, avec une méthode trahissant une effroyable expertise.

Désireux d’assister au spectacle, Bertaud fut néanmoins indisposé par les cris de douleur et il tourna la tête, tenté un instant de se boucher les oreilles. Il en profita pour laisser son regard errer sur les autres esclaves en fuite.

« La fillette ! Il se trouvait enfançonne parmi eux, j’en jurerai ! Et je ne la vois mie en leur assemblée…

— Peu me chaut, répondit le chevalier, l’air amusé. Elle nourrira les chacals bien tôt. Elle n’a pas grande valeur. Pas même le prix d’un goret, nul besoin d’échauffer les sangs de mes chevaux pour elle. »

## Naplouse, soirée du samedi 25 février 1156

La démarche sautillante, la tête haute, Bertaud interpellait gaiement les connaissances qu’il croisait, arrachant quelques grimaces étonnées aux commerçants salués avec tant d’enthousiasme. Il avait en poche de quoi s’offrir quelques douceurs pour son repas, à commencer par un carton du meilleur cru du tavernier. Celui-ci l’accueillit sur la pas de la cave :

« Alors, maître, je vous encontre bien léger cette soirée ! Agréable journée ?

— Tel que tu me vois, j’ai bien intention qu’elle continue ainsi. J’ai en ma bourse plusieurs deniers qui ne demandent qu’à joindre les tiens, en échange d’un carton de ton nectar de Judée.

— Vous faites abandon de votre carton de jaune pour le rouge ? Je dois m’attendre à pluie de rainettes, s’esclaffa le vendeur, tout en s’effaçant pour laisser entrer son client.

— N’est-on pas aux vigiles de dimanche ? Bonne fin vient à bonne semaine. Il n’est nulle malice à se faire du bien. »

Sur le chemin de sa demeure, Bertaud se surprit à siffloter, le pot bien calé contre son sein. Il en humait ainsi les excellentes vapeurs. Débouchant dans une ruelle, il manqua renverser une fillette. Agacé, il s’apprêtait à la réprimander lorsqu’il la reconnut.

C’était la gamine qu’il avait aperçue dans le taillis, courant avec ses proches.

Elle l’avait suivi. Il se rappela qu’il lui avait timidement témoigné de la sympathie, bien qu’à son corps défendant. Elle se tenait là, au milieu du caniveau, impérieuse et implorante, les cheveux emmêlés, les habits dépenaillés et déchirés. Bertaud se pencha vers elle, tentant d’afficher son air le plus affable. Après tout, bien que sarrasine, elle n’était pas si vilaine. Il se passa une langue fine sur les lèvres.

Certes maigrichonne, mais une fois lavée, elle pourrait devenir acceptable. Sa bouche se fendit en un onctueux sourire. Il n’avait pas connu de femmes sans avoir à payer depuis des années, et toujours les mêmes, de moins en moins appétissantes au fil des ans. Alors il finissait par trouver du charme à la petite donzelle face à lui. Il tendit une main vers son visage, se pencha un peu.

En un éclair la lame jaillit de sous les hardes. Et se ficha dans la gorge du vieil homme. La bouche figée en un rictus stupéfait, il s’écroula comme un tas de linge sale, brisant au passage son pichet. Dans le caniveau, le sang et le vin se mêlèrent, coulant en un mince filet sous les pieds de la fillette.

Elle le toisa un instant puis cracha sur le cadavre, les yeux emplis de larmes, avant de prendre ses jambes à son cou. ❧

## Notes

L’idée de ce texte court est venu d’un concours de nouvelles dont le sujet était « Carton rouge ». Bien que le sens médiéval d’un quarton (ou carton) en soit éloigné de celui évoqué à première vue par l’expression imposée, cela m’a donné envie de relier les deux (le carton ou quarton est une mesure de capacité de vin, d’environ 2 litres). Il s’agit donc d’un exercice de style qui prend pour prétexte à la fois la répression sévère que certains seigneurs francs exerçaient à l’encontre de leurs serfs musulmans et les tentatives avérées de fuite de certaines communautés musulmanes des environs des Naplouse à cette période.

## Références

Godefroy, Frédéric. *Dictionnaire de l’ancienne langue française et de tous ses dialectes - Du IXe au XVe siècle Tome 6 - Parsommer-Remembrance*. Paris: Librairie Émile Bouillon, 1889 ou <http://www.cnrtl.fr/definition/quarton>)

Kedar Benjamin Z., « Some new sources on Palestinian Muslims before and during the Crusades » dans *Die Kreuzfahrerstaaten als Multikulturelle Gesellschaft: Einwanderer und Minderheiten im 12. und 13. Jahrhundert*, éd. Hans Mayer, R. Oldenbourg, 1997, repr. dans *Franks, Muslims, and Oriental Christians in the Latin Levant*, Aldershot: Ashgate, Variorum Collected Studies Series, 2006, pp 129-40.

Talmon-Heller Danielle, « The Shaykh and the Community: Popular Hanbalite Islam in 12th-13th Century Jabal Nablus and Jabal Qasyûn », dans *Studia Islamica*, N°79, 1994, p. 103-120.
