---
date : 2017-11-15
abstract : 1158. Toujours en fuite après ses démêlés avec les Hospitaliers de Saint-Jean de Jérusalem, Droin découvre un nouveau monde tandis qu’il s’enfonce dans le désert.
characters : Droin, Nijm, Abu Bā’z
---

# Désert

## Sinaï, camp de bédouins, matin du lundi 22 septembre 1158

Les fesses dans la poussière, ligoté à un poteau, Droin attendait. Plusieurs fois il avait vu la nuit succéder au jour. Il avait pesté, s’était enflammé, agité, avait cherché à se libérer. Puis il s’était résolu. Il patientait, planté au milieu de toiles gris brun, au sein d’une oasis formée par quelques maigres arbustes et une poignée de broussailles racornies par le soleil. Chaque jour on lui apportait une ration d’eau croupie dans une outre en peau de chèvre, avec une gamelle de riz assaisonné de quelques légumes. Mais jamais on ne lui parlait. Chacun des nomades autour de lui semblait le considérer comme quantité négligeable. Seuls les enfants tournaient parfois vers lui des regards curieux ou moqueurs. Pour autant, aucun ne s’approchait. Au vu de tous, il était surveillé par chacun.

Il connaissait désormais les rituels du lieu. Le matin, il voyait les femmes émerger les premières des tentes, se regrouper autour de la source, échangeant à voix basse tout en puisant l’eau. Puis quelques-unes activaient un petit feu où confectionner les repas. Pendant ce temps, tandis que quelques gardiens nocturnes venaient s’étendre sur les nattes, la plupart des hommes allaient s’occuper des bêtes : chameaux, chèvres, moutons et quelques chevaux.

Ensuite apparaissaient les enfants qui couraient ici et là, affairés à rendre de menus services ou à s’amuser entre eux. Ils se regroupaient de temps à autre sous une des grandes toiles au plus près d’un arbre vénérable aux branches noueuses et écoutaient attentivement un vieillard à la barbe chenue. Même le désert recèle des clercs pour gêner les jeux, ironisa Droin.

À la mi-journée, hommes et femmes, chacun de leur côté, partageaient le repas entre amis avant une sieste. Puis les travaux reprenaient tranquillement au long de l’après-midi, selon le courage et les compétences de chacun. Parfois un cri résonnait et les mains empoignaient arc, lance, poignard ou épée. Sans que rien ne pénètre dans le périmètre. Puis le soir venait, où le souper, toujours léger et rapide, se prolongeait en veillée entre voisins, familiers ou amis. Enfin, de nouveau les étoiles scintillaient sur le camp endormi, en compagnie de quelques hommes qui déambulaient, fantômes parmi les ombres.

On lui avait donné une vieille robe déchirée, vêtement et couverture pour les nuits fraîches. Il devait se satisfaire du gravier et du sable en guise de matelas et d’oreiller. Il s’en moquait, sa vie avait sans cesse été ainsi, d’inconfort. Toute son enfance, il l’avait partagée avec les cochons que son père gardait. Ses frères et lui occupaient la cabane où les animaux étaient enfermés. Malgré les années, il en détestait toujours autant l’odeur. Il en haïssait également les cris et les grognements de contentement quand on les nourrissait.

Un petit groupe se matérialisa soudain autour de lui. Sans une parole, on le libéra de ses entraves et il fut incité vivement, mais sans violence, à les suivre. Il comprit très vite qu’on le menait à la plus grande des tentes, où se tenaient plusieurs bédouins au port altier. Au centre de l’assemblée, un homme au visage strié de rides, les yeux comme des fentes, tirait sur les poils de sa barbe. Il ne semblait pas mieux vêtu que les autres, ni plus riche, mais il était évident que tous ici le considéraient comme un chef. Le baron du coin pensa Droin.

Il s’assit, environné de son escorte, face au vieillard. Un temps passa, les regards échangés établissant les rapports de force entre les présents. Plus on se trouvait auprès du centre de gravité du pouvoir, plus les postures étaient hiératiques, les bouches pincées et l’attitude empreinte d’importance. Le vieux se contentait de tirer sur les poils de son menton, les plis au coin de ses paupières s’accentuant à peine par instant. Un cri de rapace emplit le ciel, des bêlements roulèrent jusqu’à eux, des rires d’enfants jaillirent non loin, puis une voix éraillée, presque un souffle, se fit entendre.

« Quel est ton nom, étranger ? »

Le ton était ferme, sonore et autoritaire, mais sans agressivité. Droin se revit quelques années en amont, alors qu’ils étaient appelés à tour de rôle par le cellérier du monastère pour verser la dîme. Son propre timbre ne lui parut pas aussi assuré qu’il l’avait espéré. Les journées de silence avaient érodé sa diction.

« Droin de Vézelay. »

Il avait hésité à mentir, puis n’en vit pas l’intérêt. Nul ici ne pouvait savoir ce qu’il avait fait ni le dénoncer aux hospitaliers. Il était en dehors du monde connu.

Le scheik hocha le menton, déglutit lentement, tourna la tête autour de lui avant de reprendre.

« Peux-tu me conter ce qui t’a mené là où nous t’avons trouvé ? »

Droin ne comprenait pas où l’échange pouvait le conduire, mais ne vit aucun souci à conter par le menu ses aventures. Il ne cacha pas qu’il avait quitté les religieux en désaccord profond avec eux, mais en négligeant de reconnaître clairement qu’il était légalement en fuite. Il ne voulait pas tenter les appétits de ces nomades dont on disait le plus grand mal dans les cités. Tandis qu’il parlait, revivant par le récit l’étrange parcours qui l’avait mené en ce lieu désolé, il prit conscience combien il s’était affranchi de ses origines. Cette pensée le réconforta et il termina son histoire avec bien plus de vigueur qu’il n’avait commencé. Après un court silence, les hommes face à lui échangèrent tranquillement à voix basse. Il se demanda combien ici connaissaient sa langue et avaient compris son exposé.

« Tu dis que tu as pris le chameau pour vous sauver, toi et l’homme que tu as trouvé ?

— Oc. Enfin, c’est plutôt lui qui m’a soufflé de le prendre, parce que moi, les chameaux… »

Il ponctua sa fin de phrase d’une grimace éloquente, qui lui attira quelques sourires dans l’assistance.

« Et que comptais-tu faire de l’homme ?

— Je sais pas. J’allais pas le laisser mourir comme une bête, quand même. J’ai guère eu le temps d’y penser. je voulais surtout trouver un abri. Et ensuite, rejoindre une cité ou l’autre… Il va comment, d’ailleurs ? C’est un des vôtres ?

— Sais-tu seulement où tu es ?

— En plein dans votre fief, on dirait bien ! »

La répartie sembla amuser le scheik. Droin aimait faire le fier-à-bras, gardant le menton haut et le regard plein de morgue.

« L’homme que tu as aidé s’appelle Nijm. Il se remet doucement, mais demeure bien faible. Il n’a ouvert les yeux que ce matin. »

Le scheik abandonna sa barbe pour joindre les deux mains de façon un peu solennelle.

« Il ne se souvient guère de ce qui s’est passé, mais rien dans son récit ou le tien ne semble indiquer mauvaises intentions de ta part à son égard. Tu peux donc te considérer comme notre hôte. »

Il désigna un moustachu au nez busqué, aux yeux ronds agités.

« Voici Abu Bā’z. Il s’occupera de toi le temps que tu passeras parmi nous. »

Abu Bā’z offrit un sourire aux dents jaunes.

Avant que Droin n’ait eu le temps de composer une réponse, tous les présents étaient levés et commençaient à s’éloigner. Seul le scheik demeurait assis, prenant ses aises pour une sieste, désormais complètement indifférent à ce qui se passait autour de lui. Abu Bā’z posa une main ferme, mais amicale sur l’épaule de Droin, puis, d’un geste, l’invita à le suivre. Un peu décontenancé, le jeune homme se vit arpenter les lieux qui lui étaient jusqu’alors interdits. Ils se rendirent à une petite tente près d’un muret en partie écroulé, ultimes reliefs d’un très ancien habitat. Abu Bā’z semblait déterminé à rattraper toutes les journées de silence contraint de Droin : il était volubile, parlait de tout et de rien, s’amusait de la plus formelle des réponses.

Il était éleveur, comme tous ici, et caravanier à l’occasion. C’était ce qu’il préférait faire, ayant alors la possibilité d’échanger des histoires avec ceux qu’il escortait. Il mélangeait parfois les langues et Droin devait lui rappeler de temps en temps qu’il était un Franc.

Sous la tente se trouvait Nijm, en train de dormir. Sans plus d’égard, Abu Bā’z s’installa sur une des nattes et sortit du matériel de bourrellerie d’un sac.

« Un de mes chameaux a brisé sa sangle » précisa-il avant de se mettre au travail. De l’autre côté d’une toile divisant l’espace en deux s’affairaient des silhouettes féminines, allant et venant au fil de leurs activités. Une fillette déposa devant Droin un paquet. Avisant le regard interrogateur du jeune homme, Abu expliqua :

« Tu as là un vêtement chaud pour la nuit, un turban et menues affaires qui te seront utiles.

— Je n’ai pas de quoi payer, s’inquiéta Droin.

— Vous êtes drôles, vous autres habitants à ciel couvert. Vous n’avez aucun respect de ce qui compte vraiment, mais voulez parfois payer ce qui vous est dû. *Je donne une datte au pauvre, pour en goûter la vraie saveur*. »

Puis il ricana doucement, tout en s’affairant à réparer sa courroie. Droin fouilla dans le sac et y trouva quelques objets de toilette et un petit couteau. Il s’empressa d’en nouer le fourreau à son braïel[^braiel]. Voyant son geste, Abu lui sourit et alla lui chercher un autre paquet.

« On a aussi nettoyé ton épée. Une belle lame, même si elle a été difficile à nettoyer. Elle méritera plus belle gaine que ces chiffons ! »

## Sinaï, camp de bédouins, après-midi du mercredi 8 octobre 1158

Les effluves des chèvres parquées dans un enclos de buissons et de pierres sèches venaient se mêler à l’odeur irritante de la poussière soulevée par le vent. Droin n’y prêtait plus guère attention, profitant du début de l’après-midi pour se reposer à l’ombre de la toile. Il avait accompagné Nijm pour une courte balade autour du camp. Le bédouin se remettait à une vitesse étonnante, capable désormais de marcher sur les terrains accidentés alors même que ses blessures étaient à peine refermées. Chaque jour une des femmes venait nettoyer les plaies et changer ses pansements, enduits d’un emplâtre agressant le nez.

« Je pense que nous pourrons rejoindre ma tribu d’ici une lune, confia Nijm, indolemment allongé sur le paquet de ses affaires.

— C’est loin d’ici ?

— Il nous font aller au Nord, vers le cœur des territoires de Badawil[^baudoinIII]. Les miens sont le plus souvent autour de Bahr-Lt[^bahrlut] en cette saison, à récolter le sel avant de bientôt partir sur les zones de pâturages. »

Droin fit mine de comprendre et se renferma. Il n’était pas enthousiasmé à l’idée de retrouver trop vite les territoires qu’il avait fuis. Il n’avait aucune idée de la véhémence des hommes de Saint-Jean^[Voir Qit’a *Poing de trop*.], mais s’il devait se fier à la pugnacité des religieux de Vézelay, il lui faudrait se faire oublier un moment. Il demeura silencieux, d’une brindille traçant des motifs dans le sable.

« Tu connais la princée d’Outre-Jourdain ?

— Nous y allons parfois, pour compaigner des caravanes. De nombreux frères de mon clan habitent ces lieux. Nous savons bien négocier avec eux le passage.

— Je me disais que j’irais bien là-bas. Le baron du coin édifie vaste citadelle et fait bon accueil aux colons.

— J’ai vu les murs du karak al-Shawbak[^kerak] partir à l’assaut des cieux. Les tiens ne sont tant heureux qu’ils empilent deux pierres l’une sur l’autre. »

Sa remarque, nuancée d’un sourire, se voulait sans malice. Droin commençait à apprécier ce nomade aux yeux tristes et aux silences éloquents. Peut-être n’était-ce que le fait de sa convalescence, mais il savourait cette distance maintenue entre eux, ce respect de son espace personnel. Il n’y avait que bien rarement goûté.

Nijm s’empara d’un pichet, accompagnant le mouvement d’épaule d’une grimace. Jamais il ne se plaignait, mais laissait échapper des rictus qui en disaient long sur les souffrances qu’il endurait. Après avoir avalé une rapide gorgée, il s’affaissa de nouveau. Dehors, des gamins riaient en poussant du pied une boule de bois. Droin lança un furtif regard vers un groupe de femmes occupées à cuire des pains plats, non loin d’eux.

Les bédouins avaient proposé aux deux hommes une petite toile à eux, à côté de celle d’Abu Bā’z, où chaque jour un enfant ou Abu lui-même parfois, venait leur apporter de quoi manger. Jamais on ne leur avait demandé quoi que ce soit. Droin s’étonnait de cette libéralité alors que la tribu lui semblait à peine plus riche que sa propre famille. La plupart allaient dans un vêtement élimé, sans chaussure, les plus jeunes à demi nus. Il leur aurait à peine accordé un regard s’il les avait croisés aux abords d’une des villes.

Il détailla son sac, sa couverture, les nattes sous lui et la tente au-dessus de sa tête. Il ne manquait même pas une petite lampe à graisse pour y voir au plus noir de la nuit. Et tout cela leur avait été fourni sans un mot, sans aucune demande en retour.

« Ici, ce sont gens de ton peuple ? demanda-t-il à Nijm.

— Ce ne sont pas des al Fadl ni même des Banu Tayy^[Tribu et clan bédouins.], mais nous avons bonne entente avec eux. Nous échangeons souvent le serment du pain et du sel entre nous. »

Une fois encore, il ne comprenait pas ce dont son compagnon parlait, mais acquérait une image plus large de l’Outremer. Le terme lui paraissait bien étrange, perdu au milieu des roches et du sable. Tout autour de lui ce n’étaient que dunes, graviers et poussière. À perte de vue un monde qui semblait minéral, mais qui regorgeait de vie pour celui qui prenait le temps de l’écouter, de l’observer. Et aux abords des sources, c’était une explosion végétale, teintant d’émeraude un univers en dégradé ocre.

« Je peux te guider jusqu’à la forteresse celte, proposa Nijm. D’ici, en partant au levant, nous trouverons Wadi Musa[^wadimusa]. De là, une piste régulière mène au nord, vers les miens. »

Leur échange s’interrompit quand une petite fille vint déposer un panier contenant du pain tout chaud de la fournée. Il était accompagné d’un pot de lait caillé et de fromage frais agrémenté de quelques rares olives. Ils se distribuèrent le repas puis mangèrent en silence. Tout le camp s’était arrêté un moment, laissant les bruits du désert s’imposer le temps que les estomacs se remplissent. Pour Droin, la portion paraissait congrue alors même qu’il avait remarqué qu’il était plutôt bien traité. Seulement, son appétit semblait plus important que celui de ces austères bédouins. Il commençait par ailleurs à s’habituer aux saveurs prononcées du fromage, des boissons. Quand ce n’était pas issu de la traite des chamelles, c’était tiré du pis des chèvres. Droin sentait en permanence les odeurs douceâtres des produits laitiers qu’on consommait tout au long de la journée imprégner jusqu’à ses propres vêtements.

Il avala sa dernière bouchée, s’essuya la main de sa manche puis se désaltéra longuement. Il n’avait pas non plus la capacité des nomades du désert de se rafraîchir d’une courte rasade. Il s’installa alors pour une sieste, mais avant de fermer les yeux, il se tourna vers Nijm, qui s’apprêtait à faire de même.

« J’irai avec toi vers le levant jusqu’à la cité du prince d’Outre-Jourdain, ainsi que tu me l’as proposé. Je verrai si je peux y trouver quelque manse qui manquerait de bras pour fructifier. »

## Cité antique de Pétra, Wadi Sabra, soir du jeudi 23 octobre 1158

Droin somnolait sur sa monture, fatigué et vermoulu de la journée demeuré en selle. Ils avaient fait une belle pause à la mi-journée, plus par habitude de son compagnon que par nécessité, mais cela n’avait pas suffi à lui faire retrouver des sensations normales dans son arrière-train. Il n’était plus aussi mal à l’aise avec les chameaux que par le passé, mais il avait toujours préféré voyager sur ses pieds. Pour lui, les animaux manifestaient un caractère au mieux un peu trop facétieux pour apprécier de se trouver sur leur dos.

Ils longeaient un passage caillouteux, une pente encombrée qui se hissait entre deux versants aux crêtes orangées. Ils remontaient le côté opposé de la vallée qu’ils venaient de franchir, séparant deux énormes massifs rocheux. Nijm tendit le bras vers le contrefort à leur gauche.

« En haut du wadi, nous prendrons étroit sentier taillé dans la roche. Nous serons alors arrivés. Les tribus sont souvent installées là. Mes cousins nous feront bon accueil. »

Ils parvinrent en effet devant une étroite faille qui paraissait déchirer la pierre en un mince fil de ténèbres. Les dernières lueurs du jour n’y apportaient aucune lumière. Alors qu’il pénétrait à la suite de Nijm dans le goulet, Droin contracta les épaules, s’attirant un regard désapprobateur de sa monture, gênée par le mouvement sur ses rênes tandis qu’ils disparaissaient entre les hautes murailles. Il lui semblait qu’il pourrait toucher les deux côtés du passage et hésitait à le faire, par peur de découvrir quelque chose qu’il n’apprécierait pas.

Les pas lourds de leurs chameaux faisaient résonner les graviers roulant à chaque enjambée. Droin avait l’impression qu’une caravane de bédouins s’était engouffrée à leur poursuite. La plus petite de ses respirations lui faisait l’effet d’un vacarme assourdissant et sa monture grondait comme une armée de dragons en furie. Ils débouchèrent heureusement assez vite sur une large esplanade qui s’étendait jusqu’aux pieds d’immenses falaises de roche stratifiée, déchiquetée par les vents et le sable du désert. Plusieurs passages tranchaient les murailles et des grottes perçaient d’ombres les parois. En de nombreux endroits, des feux scintillaient. Nijm se rapprocha de lui, un sourire faisant briller ses dents dans l’obscurité qui s’installait.

« Ce soir, avec un peu de chance, nous partagerons le mensaf[^mensaf] avec les miens ! »

Il talonna son chameau et s’élança joyeusement dans la poussière, suivi par un Droin, moins enthousiaste, quoique satisfait de décoller ses fesses de là où elles se ratatinaient depuis des heures.

## Cité de Kérak, zone des jardins occidentaux, début d’après-midi du vendredi 31 octobre 1158

Arrêtés à l’abord du croisement qui montait vers la porte du Burj az-Zahar^[Quartier de Kérak.], Nijm et Droin partageaient quelques dattes dont ils crachaient les noyaux dans la poussière. Sur la restanque au-dessus d’eux, des familles s’employaient à ramasser les olives. Les plus jeunes et les plus intrépides grimpaient aux branches, y dénichaient les plus beaux fruits tout en laissant les plus murs tomber par eux-mêmes au sol. Là, les plus âgés les saisissaient par poignées, les lançant en l’air pour en souffler les feuilles mélangées avant de les jeter dans des paniers. L’ambiance était à la fête, les cueilleurs s’invectivant d’une parcelle à l’autre, quand ils n’entonnaient pas en chœur une chanson rythmée.

Au-dessus de ces terrains soigneusement entretenus se dressaient les murailles ambrées de la cité de Kérak. La ville où les deux hommes avaient convenu de se séparer. Droin repartait mieux doté que lorsqu’il avait fui dans le désert et sa tenue n’avait plus rien de latine. Il avait même abandonné les chaussures et n’enfilait ses savates que sur les plus agressifs des sols. Il avait regroupé toutes ses affaires dans une hotte et trois sacs, mais avait refusé le chameau que Hijm voulait lui offrir. Il n’avait aucune sympathie pour les animaux et n’était pas à l’aise à l’idée de revendre pareil cadeau. Il était trop peu familier des dons pour les accepter sans réticence.

Il tourna son regard vers le sud, où la forteresse continuait de s’édifier. On voyait des structures de bois suspendues au-dessus du vide, où des hommes s’activaient tels des insectes. Il se demandait quel effet cela pouvait bien faire d’embrasser le territoire alentour depuis les terrasses sommitales. Le paysage devait être à couper le souffle. Il pourrait s’y faire embaucher comme portefaix, histoire de satisfaire sa curiosité, pensa-t-il.

Nijm puisa un peu d’eau dans un bassin et se frotta rapidement le visage. Il tourna le regard vers l’ouest, en direction de l’oued qui mordait le haut plateau avant de s’écouler dans la vallée de la Mer Morte. Il semblait impatient de repartir, de retrouver les siens. Droin l’enviait d’être à la fois si libre et si bien intégré dans une communauté. Il s’était fait pèlerin et colon par dépit, afin de fuir une famille qu’il n’aimait guère.

« Tu es certain de ne pas vouloir garder cette bête ? Elle est à toi, je te l’ai donnée.

— Je crois qu’elle n’est guère en accord avec cela, ricana Droin. Non, je te mercie, compère, mais je ne suis pas à l’aise avec ces bêtes. Elles ont aussi mauvais caractère que les porcs, et je n’ai guère d’amitié pour ces derniers.

— À tant les décrier, tu resteras en ma mémoire comme al-Nazf[^alnazif], mon ami » se moqua Nijm.

Puis il fouilla dans un de ses sacs de nourriture, y déchira deux petits morceaux de pain qu’il frotta du sel sorti d’une boîte et en tendit un à Droin. Celui-ci, un peu surpris, accepta.

« Si tu l’acceptes, j’aimerais que nous prêtions serment. Tu m’as sauvé alors que je n’étais rien pour toi, soyons donc comme des frères dès cet instant. »

Un peu pris au dépourvu, Droin accepta sans trop y réfléchir et répéta à la suite du bédouin la formule que ce dernier lui traduisit dans sa langue.

« Par le pain et le sel, je ne te trahirai pas. »

Une fois cela fait, Njim lui sourit avec chaleur et lui donna une accolade bourrue. Puis il alla rattacher soigneusement ses affaires sur sa monture.

Un vieil homme assis sur un âne bardé de paniers d’olives ralentit pour les saluer d’un signe de son couvre-chef de guingois. Droin en profita pour l’apostropher. Comme le fellah parlait sa langue, il en espérait quelques renseignements sur la cité.

« Sais-tu si on trouve embauche en la citadelle ?

— Oh, il y a toujours de l’ouvrage pour la main habile, mais les hommes noirs ne paient guère, de ce qu’on dit. Ce sont eux qui agrandissent le rempart.

— Des hommes noirs ? »

Nijm intervint et échangea quelques paroles en arabe avec le vieux paysan, avant de traduire pour Droin.

« Ce sont les prêtres de vos cités, qui s’habillent de noir et nourrissent les pèlerins. Ils s’arment de plus en plus au fil des ans. On les rencontre souvent avec les hommes de la Milice du temple. »

Droin en fit grincer ses dents de mécontentement. Il fallait qu’ils viennent jusqu’ici, en plein désert. Nijm surprit son désarroi et, d’un regard, l’interrogea.

« Tu accepterais de me garder avec toi encore un moment ?

— Nous avons prêté le serment du pain et du sel. Partout où je serai, ma tente sera tienne. »

Droin haussa les épaules, admirant une nouvelle fois les hautes murailles. Quand on n’avait rien à perdre… Lorsqu’il se retourna, Nijm arborait un visage hilare, lui adressant un clin d’œil tandis qu’il lui tendait les rênes de son chameau. ❧

## Notes

Les populations bédouines font partie des grandes inconnues des sources médiévales. On ne les cite que de façon périphérique et les chroniqueurs y apportent rarement beaucoup d’attention. Ils représentaient pourtant une fraction importante des territoires où les pouvoirs latins et syriens s’affrontaient. Et contrairement à l’opinion professée par Dom Raphaël dans la traduction de F.-J. Mayeux dans ses textes du début du XIXe siècle, leur culture a beaucoup évolué avec le temps, sans parler des tribus et des clans qui ont migré sur de vastes zones. J’ai choisi néanmoins de me servir d’anciennes sources de voyageurs de ce type pour aller à leur rencontre, car elles regorgent d’anecdotes à propos de populations avant qu’elles ne soient imprégnées par le style de vie occidental.

Le titre de ce *Qit’a* est en rapport avec le fabuleux *Désert* de Jean-Marie Gustave Le Clézio, qui fut pour moi un vrai choc lors de sa découverte voilà des années. C’est lui qui m’a fait comprendre ce qu’était une expérience esthétique de lecture. Par ailleurs, ce grand écrivain a une sensible appétence pour l’ailleurs et y fait allusion aussi bien dans ses interviews que dans ses textes. Son talent pétri de curiosité littéraire et culturelle m’a montré une voie que j’essaie d’arpenter par mes propres moyens.

## Références

Bonnenfant Paul, « L’évolution de la vie bédouine en Arabie centrale. Notes sociologiques », dans *Revue de l’Occident musulman et de la Méditerranée*, n°23, 1977, p. 111-178.

Cowan gregory, *Nomadology in architecture. Ephemerality, Movement and Collaboration*, Dissertation for master in Architecture, University of Adelaide, 2002.

Le Clézio Jean-Marie Gustave, *Désert*, Paris: Gallimard, 1980.

Mayeux F. J., *Les bédouins ou Arabes du désert*, 3 tomes, Paris : Ferra jeune, 1816.
