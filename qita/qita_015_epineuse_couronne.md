---
date: 2013-02-15
abstract: 1152. Les souverains de Jérusalem ne se sont jamais succédés de façon simple et, parfois, les solutions trouvées pour garantir le maintien de la lignée se sont avérées bien dangereuses. Lorsque deux personnes estiment avoir droit à diriger seules, l’affrontement est inévitable, et quand cela se déroule dans un royaume fragile qui voit grandir à ses frontières de puissants adversaires, que reste-t-il comme solution aux populations ? Retrouvez quelques personnages du premier et second tome des aventures d’Ernaut, emportés par la tourmente des caprices des puissants.
characters: Droart, Eudes Larchier, Pierre Salomon, Régnier d’Eaucourt, Raoul, Onfroy de Toron
---

# Épineuse couronne

## Abords du Saint-Sépulcre, Jérusalem, matin du lundi 31 mars 1152

Les acclamations grondaient, montaient et retombaient ; les hourras volaient parmi les cieux, la joie emplissait d’aise les cœurs. Les applaudissements résonnaient, faisant concurrence aux exclamations, aux chants de liesse. Comme toujours à Pâques, le parvis du Saint-Sépulcre n’était que bousculade et cohue, rires et hymnes. Pourtant, cette fois-ci, les pèlerins n’étaient pas seuls à jubiler.

Objet de cette ferveur, un jeune homme se tenait sur les marches flanquant le portail d’entrée, encadré de quelques nobles en grande tenue, bliauts de soie et mantels brodés. D’à peine vingt ans, il était habillé plus magnifiquement encore que son entourage, recouvert d’orfrois et de passementerie. Mais ce qui le distinguait entre tous, c’était la couronne qui ceignait son front. La couronne des rois de Jérusalem qu’il arborait face à la foule venue l’acclamer. Il souriait, saluant les présents, son peuple, comme ses ancêtres avant lui.

Parmi les anonymes, un rouquin s’enthousiasmait, criait, emporté par la liesse communicative. Les dernières années avaient été rudes, avec la perte de territoires au nord, l’échec de la grande campagne menée avec les souverains allemands et français face à Damas. En outre, depuis la mort de Foulque[^foulque], la reine mère[^melisende] et le jeune Baudoin[^baudoinIII] ne parvenaient pas à s’entendre pour diriger le royaume. Habituellement le roi se faisait de nouveau couronner à l’occasion des fêtes de Pâques, profitant de la présence des voyageurs lointains pour proclamer son pouvoir. Or, depuis plusieurs années le peuple de Jérusalem n’avait pu assister à pareille cérémonie. Son apparition sur les marches, ceint du symbole de son autorité, avait embrasé la population inquiète et fait retentir des hymnes de joie, des Actions de grâce.

Eudes tendit l’oreille à ce qui se disait alentour : le vieux Patriarche n’avait pu accepter de poser lui-même la couronne sur le front du jeune Baudoin, car Mélisende était absente, et cela ne pouvait se faire sans elle. Pourtant le roi était là, face à la foule, acclamé comme le souverain qu’il était. Recevant un léger coup de coude, Eudes se tourna vers son compagnon, un brun enrobé répondant au nom de Droart. Guère plus âgé, celui-ci venait d’entrer comme sergent dans l’administration royale. Il passait d’ailleurs la plupart de sa solde gagnée à surveiller les portes de la ville en liquides divers et variés qu’il ingérait avec ses amis. Empli de retenue, son visage était inquiet malgré l’enthousiasme ambiant.

« Ça va pas plaire à la reine… dit Droart.

— Pourquoi donc ? Le roi a coutume de se faire ainsi reconnaître.

— Elle a été couronnée de nouvel en même temps que lui, et saura rappeler qu’elle dirige le royaume également.

— Il est temps pour elle de laisser place, non ?

— Elle est fille et femme de roi, sa place est aux côtés de Baudoin ! »

Eudes comprit que son ami ne goûtait guère de voir le jeune roi manifester tant d’indépendance. Comme beaucoup d’habitants de Jérusalem, Droart appréciait beaucoup Mélisende. Si elle n’était pas aussi habile que son fils à la guerre ou dans les affaires diplomatiques, son talent pour gérer le domaine et satisfaire les populations était reconnu.

Fille de Baudoin du Bourcq, elle était née pratiquement en même temps que le royaume et l’incarnait pleinement depuis un demi-siècle. En outre, depuis une vingtaine d’années qu’elle dirigeait, on s’était habitué à sa présence, sa silhouette hiératique lors des cérémonies. Ainsi qu’à son caractère ombrageux, bien qu’il trouvât rarement à s’exercer à l’encontre des plus humbles.

Baudoin monta les quelques marches qui menaient à la chapelle du Calvaire, y pénétrant après un dernier salut à la foule. Voyant la presse sous les portes du sanctuaire, Droart et Eudes renoncèrent à y entrer et préférèrent s’éloigner. Il leur fallut jouer des coudes et se faufiler péniblement avant de s’extraire de la rue, près du change syrien. De là, ils rejoignirent rapidement Malquisinat, où ils espéraient trouver de quoi se restaurer.

C’était un jour férié, mais les commerces de bouche étaient ouverts, par permission exceptionnelle. L’afflux de pèlerins et de voyageurs était tel qu’il était difficile de nourrir tout le monde correctement au sein des établissements religieux. Ceux qui en avaient les moyens, ou le désir, pouvaient donc acheter des mets tout prêts aux charcutiers, pâtissiers, cuisiniers.

Pour l’heure, Droart n’aurait pas craché sur une friandise, après les longues semaines de Carême, même s’il avait surtout envie d’un peu de vin pour se désaltérer. Il trouva finalement un pichet de ce qu’il cherchait, à un prix qui le fit râler car augmenté certainement en raison de la forte demande. Il lâcha les pièces en grognant et proposa à son compère d’en prendre quelques lampées. Il burent debout, appuyés contre l’arcade en pierre d’un vendeur d’oublies dont l’odeur sucrée leur caressait les narines, tout en regardant aller et venir la foule qui s’agitait en ce jour de fête. Droart fit une grimace, repensant à ce qu’ils avaient dit un peu plus tôt, sur le parvis.

« J’espérais ces histoires achevées et là, venir se faire acclamer en la cité où la reine est si fort appréciée… »

Il ne finit pas sa phrase, préférant avaler un peu de vin. Eudes en profita pour avancer ce qu’il pensait.

« Avec ce qui se passe en la terre des Turcs, il est bon que le roi se fasse voir. Moult campagnes se préparent, de certes.

— Je ne dis le contraire. Seulement, pourquoi faire pareil affront à la reine ? Ils ont parti le royaume, ce me semble. Il n’est guère prude de revenir là-dessus. »

Eudes ne pouvait qu’approuver. Le jeune Baudoin manifestait plus d’indépendance depuis quelques mois, plus d’assurance en raison de ses succès à gérer les territoires dans le nord. Si tout le monde s’accordait sur le fait qu’il était nécessaire que le royaume soit dirigé par quelqu’un de compétent, la crainte que leurs ennemis proches profitent du moindre flottement était la plus forte. Personne n’aimait l’incertitude née des affrontements de la volonté des puissants.

## Maison de Pierre Salomon, Jérusalem, soir du mardi 8 avril 1152

La grande salle était éclairée de nombreuses bougies et lampes, signe de la fortune du propriétaire. Les murs peints de motifs géométriques faisaient écho aux entrelacs savants des tapis. Les châssis de verre avaient été tirés des fenêtres, laissant la brise fraîche pénétrer dans la pièce, apportant les exhalaisons du jardin d’agrément contigu.

Assis sur des coussins et des banquettes, une quinzaine de bourgeois à l’évidente opulence discutaient âprement des événements, sans prêter attention aux friandises et aux boissons amenées là par le maître de maison. Celui-ci, ventripotent, d’âge mûr, se grattait le front, l’air abattu. Il échangeait à voix basse avec le dernier arrivant, encore chaussé de ses éperons dorés, une veste de voyage sur les épaules. Son visage las était collé de la poussière des chemins, mais son regard demeurait déterminé. Dégarni, le nez aquilin, il inspectait la salle avec application, identifiant les présents les uns après les autres. Lorsqu’on lui proposa un verre, il le prit avec un sourire poli. Puis il attendit que Pierre Salomon l’annonce. Ce dernier tapa dans ses mains plusieurs fois pour attirer l’attention et faire taire les échanges. Comme habituellement, il transpirait abondamment, et semblait au bord de l’apoplexie.

« Compères, le sire Régnier d’Eaucourt arrive avec moult nouvelles. Je crois que vous devriez faire silence et l’écouter… »

Puis il secoua la tête, se désolant à l’avance de ce qu’il allait entendre, avant de se laisser tomber sur un matelas. Le chevalier salua d’un rapide signe de menton, très martial, et prit la parole d’une voix forte, si ce n’est assurée.

« J’arrive de Mirabel, où le roi est entré. »

Plusieurs cris de surprise, peut-être d’indignation, se firent entendre, mais il ne s’interrompit pas pour autant.

« Le connétable Manassès a été prié d’abandonner sa charge, car le roi n’a mie besoin de ses services. »

Nouvelles exclamations.

« Des espies du roi l’ont informé que la reine Mélisende avait cheminé hors la cité de Naplouse et faisait route pour la sainte cité.

— Êtes-vous ici pour nous demander de lui garder portes scellées ? s’indigna un vieil homme à la voix aiguë.

— J’étais homme du sire connétable, et me voilà mandé par le sire Onfroy de Toron qui m’a pris à son service. Il n’est pas dans ses coutumes de demander félonie, répondit sèchement le chevalier.

— Peuh ! Il a bien accepté les miettes de la reine quand il voyait le vent venir » protesta un jeune homme à la barbe drue.

Régnier lui adressa un regard à effrayer la Mort elle-même, mais s’abstint de tout commentaire. Il enchaîna.

« Le roi est déterminé à avoir les moyens de ses tâches. Il ne pourra les accomplir si la reine ne se dépossède pas quelque peu.

— Il en veut encore plus, quoi ? s’étrangla un des présents.

— C’est le roi, après tout, maître Turcame, protesta avec froideur un vieil homme à la toison d’argent.

— Justement, il devrait avoir meilleur jugement sur sa mère la reine, qui est femme de savoir et de vertu. »

Les commentaires fusèrent alors de toute part, approuvant ou critiquant les dernières paroles. La plupart, néanmoins, reconnaissaient la bienveillance de la reine et le manque d’égard que Baudoin manifestait. Ils avaient été choyés par l’administration efficace de Mélisende et redoutaient, en bons gestionnaires, d’abandonner cela pour se retrouver sous la coupe d’un jeune homme dont on ne savait pas grand-chose finalement, en dehors de son goût pour les femmes. Pierre Salomon tapa de nouveau à plusieurs reprises dans ses mains.

« Mes amis, écoutez donc le sire d’Eaucourt. Il n’est pas simple messager, il assistait à nos conseils, et vient là en ami.

— Merci à vous, maître Salomon. Je dois remonter en selle au plus vite, aux fins de voir la reine. J’aime à croire qu’une entente est encore possible.

— Grâces vous soient rendues, sire, d’avoir pris le temps de nous informer de cela. Peut-être auriez-vous un conseil à nous donner, ou une autre information à nous délivrer.

— Je viens à vous, car je crois que les Bourgeois du Roi sont amis de la paix, comme je le suis. Je n’ai nul besoin de porter conseil à vos oreilles. Vous saurez faire les bons choix le moment venu. N’oubliez pas que nos ennemis ne sont pas si loin. Damas n’est qu’à une chevauchée de notre cité. »

Il inclina sèchement le buste pour prendre congé, non sans avoir salué Pierre Salomon d’un geste amical sur l’épaule et d’un sourire. Puis il disparut derrière la tenture qu’il avait soulevée pour sortir. Un silence pesant s’infiltra alors dans la pièce, paralysant les coeurs, immobilisant les langues. Seul Pierre Salomon osa faire entendre sa voix.

« Alors, compères, que ferons-nous si le roi et la reine veulent entrer en la cité ? »

## Abords de la tour de David, Jérusalem, fin de matinée du lundi 14 avril 1152

Les mains s’accrochèrent aux cordages et les hommes tirèrent de toutes leurs forces pour abaisser le balancier faisant pivot, propulsant la pierre de la fronde avec vigueur, dans un ronflement inquiétant. Le projectile siffla et percuta le mur avec un bruit sec, y arrachant un éclat de moellon dans sa dislocation. L’ingénieur responsable de la bricole échappa un grognement de rage et sermonna les soldats. Ils avaient dévié vers la droite, car ils n’appliquaient pas tous le même effort. La porte de la forteresse les narguait, intacte.

Tandis qu’il préparait un nouveau boulet, des voix se firent entendre : l’accès s’ouvrait et une main se montra. Plusieurs archers et arbalétriers se mirent en position derrière les mantelets grossièrement disposés dans la rue. On appela les capitaines, les officiers. Un chevalier en grand haubert de mailles, mais la coiffe détachée, pendant sur ses épaules, s’approcha. Ses cheveux coupés courts étaient hirsutes et son visage renfrogné était sali du frottement de l’armure. Il franchit les protections et se posta à quelques pas en avant, les mains sur les hanches, l’air bravache, défiant un quelconque tireur de le viser depuis la forteresse royale. Il attendait sans rien dire.

La porte s’ouvrit plus complètement et un homme s’avança à son tour. Lui aussi était en tenue de guerre, le casque lacé et le bouclier au bras. Il n’avait néanmoins pas son épée dégainée. Voyant qu’il n’était pas en danger, il descendit les marches avec soulagement et franchit, quasiment en sautant, le fossé comblé de bric et de broc. Arrivé devant le chevalier, il défit son heaume et laissa tomber sa ventaille[^ventaille], révélant un visage également fatigué et sale, mais au cheveu plus rare. Il souriait, d’un air un peu désolé, et posa sa main sur l’épaule face à lui.

« Je suis aise de te voir, Raoul, j’ai message pour le sire de Toron. »

Lorsqu’ils franchirent les protections de bois, la porte de la forteresse était déjà refermée. Les soldats retournèrent à leur ouvrage, empoignant les cordes de l’engin de siège. Une fois un peu en retrait, Raoul stoppa son compagnon.

« Je ne peux te laisser aller ainsi, Régnier. Tu as disparu auprès de la reine depuis presque une semaine.

— Le sire de Toron m’avait missionné, Raoul.

— Je sais bien, mais tu servais Manassès de Hierges avant cela. D’aucuns parlent félonie.

— Qui ose dire cela ?

— Personne ne le fera face à toi, mais les langues sont acides. »

Il soupira et indiqua le baudrier.

« Il va falloir que tu me confies ton épée si tu veux voir le connétable. »

À son froncement de sourcil, Raoul comprit que Régnier d’Eaucourt attendait d’en savoir plus. Ils avaient chevauché bien trop souvent botte à botte, brisé assez de lances côte à côte pour qu’il le traite comme un étranger ou un félon.

« Ce n’est pas encore officiel, mais le sire de Toron sera le nouveau connétable. »

Il eut un sourire contrit.

« Je sais bien ce que tu penses, que chacun se place maintenant que le jeune roi a le vent avec lui.

— Je n’aurais pas cru possible que Baudoin assaille sa propre mère en la cité de Jérusalem, surtout. Sais-tu que des hommes ont été blessés ?

— Bien sûr, ne vois-tu pas que j’ai porté haubert tous ces derniers jours ? Les archers de la reine n’ont guère été plus amables ! »

Régnier inspira, frappant du poing sur le mur.

« Tout cela est ridicule ! S’entrebattre alors que Norredin[^nuraldin] rôde comme un loup affamé !

— Il faut faire cesser cette folie au plus tôt, je m’en accorde. Un jeune roi, à la main leste et de grand courage, voilà ce qu’il nous faut.

— As-tu oublié le serment prêté à la reine Mélisende ? A-t-elle démérité ? »

Raoul baissa les yeux, ennuyé. Il passa sa langue sur des lèvres craquelées avant de répondre.

« Les choses passent, Régnier. Il est temps pour le royaume d’avoir un roi à sa tête. »

Régnier d’Eaucourt grogna, le visage crispé. Il secoua la tête en désaccord, puis finalement ses épaules s’affaissèrent. Ses mains se portèrent à son baudrier, qu’il détacha sans un mot. Il le tendit à Raoul.

« Allons voir le sire connétable, mettre sauf ce qui peut l’être de la terre du Christ. » ❧

## Notes

La situation politique du royaume de Jérusalem au XIIe siècle n’a jamais été très stable. La difficulté pour la dynastie régnante à se maintenir en place, la relative puissance des grands barons, l’imbrication dans les jeux des puissances régionales, ont entretenu le pouvoir royal dans un équilibre précaire, jusqu’à son effondrement face à Saladin dans le dernier quart du siècle.

Les solutions trouvées pour tenter de consolider la continuité de la lignée furent parfois à l’origine des problèmes. Ainsi, Foulque tenait lui-même sa couronne de son mariage avec Mélisende, et Baudoin avant lui avait fortement ancré le pouvoir dans les mains de la reine, de façon à éviter qu’il ne soit accaparé par un époux étranger. Cela a malheureusement débouché sur une crise lorsque le jeune Baudoin fut en âge de régner et désireux de le faire.

La dissension alla jusqu’à un accord éphémère de partition du royaume et se termina par un siège en règle de la Tour de David où Mélisende s’était réfugiée, par les forces de Baudoin. Abandonnée par la plupart de ses soutiens, la reine mère accepta finalement de se retirer à Naplouse et ne prit plus part au gouvernement jusqu’à sa mort une dizaine d’années plus tard.

S’appuyant sur une analyse très fine des chartes délivrées par les chancelleries des deux dirigeants, Hans Eberhard Mayer propose une lecture plus complexe des événements, qu’il place en 1152, qui ont vu les deux souverains s’opposer. Je n’ai donc pas retenu le récit issu des écrits de Réné Grousset (*Histoire des croisades et du royaume franc de Jérusalem - II. 1131-1187 L’équilibre*, Paris : Perrin, 1935, 2006) auquel je préfère généralement le livre de Joshua Prawer, plus récent, pour resituer l’histoire du royaume dans une perspective de longue durée.

## Références

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972, p.93-182.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.
