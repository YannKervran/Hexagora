---
date : 2019-07-15
abstract : 1151-1159. Garin, alias Yarankash, fuit les provinces du nord de la Syrie et apprend à vivre en homme libre dans les territoires latins. Mais à quel monde appartient-il désormais ?
characters : Yarankash, Hugues, Shams al Khilafa, Fouques
---

# En marche

## Chêne d’Abraham, faubourgs de Hébron, midi du jeudi 3 mai 1151

Installé contre un tronc du bosquet, à l’abri de sa ramure, Garin grignotait un peu du quignon de pain plat qui lui restait. Il admirait le vieux chêne tortueux qui déroulait ses branches face à lui, ses ramilles feuillues ondulant mollement dans une impalpable brise. Il était seul à profiter de l’endroit, la chaleur du soleil ayant chassé les pèlerins qui s’étaient égayés dans la campagne après la fin des Pâques à Jérusalem. Cela faisait deux jours qu’il contemplait le vénérable tronc noueux. Il avait vu passer des fidèles, qui se faisaient conter les miracles qu’avait opérés la sainte plante : c’était là qu’Abraham avait vécu et installé sa tente.

Il avait une petite provision de bouche, essentiellement de poisson séché et de figues et une source peu éloignée l’abreuvait à satiété. Il n’en demandait pas plus. Pour dormir, il se roulait dans le manteau qui lui servait contre le froid et la pluie, profitant de la douceur du climat. Depuis des mois, il prenait la route dès qu’il sentait qu’on tentait de lui causer des problèmes ou qu’on cherchait à le retenir. Il avait passé un temps comme manouvrier au service des frères de Saint-Jean, dans un casal non loin d’Acre, mais il n’arrivait pas à rester en place.

Les signes d’infamie qui avaient percé ses paumes à Naplouse[^voirqitamaindestin], le désignant comme un serviteur parjure, ne lui laissaient guère d’occasion de s’installer, de toute façon. À peine ces stigmates étaient aperçus, les visages se fermaient, l’accueil se faisait plus réservé et les offres de travail moins enthousiastes, moins longues.

Cela lui convenait. Il errait d’une saison à l’autre entre Césarée et Jaffa sur la côte et s’aventurait de temps en temps dans les reliefs intérieurs jusqu’aux berges méridionales de la Mer Morte. Il ramassait les olives, transportait le sel, curait les fossés, portait pierres, mortier, bagages et matériaux. Sans jamais rester en place. Un petit chien l’avait suivi plusieurs semaines, mais il avait disparu une nuit sans lune, alors qu’ils dormaient dans les collines de Samarie. Garin se demandait s’il avait trouvé un meilleur maître ou fini sous la dent d’un chacal voire d’un lion.

Parfois il tressait des chapeaux avec des pailles négociées en salaire. Puis il les revendait aux ouvriers des champs, contre nourriture et boisson, en échange de vieux vêtements usés. Il avait aussi guidé des caravanes de pèlerins entre Acre ou Jaffa et Jérusalem. Il y avait appris d’autres langues, de nouvelles coutumes, avait sympathisé avec ces voyageurs venus d’outremer. Mais il demeurait toujours en marge, refusant de se plier à l’usage d’un lieu, de s’engager à suivre les codes d’un groupe. Il veillait surtout à ne jamais s’aventurer trop au nord, où on aurait pu le reconnaître. Il s’était laissé pousser la barbe, se faisait tailler les cheveux de temps en temps à l’écuelle et portait une tenue aussi latine que possible, oubliant l’esclave qu’il avait été, dans les territoires musulmans. Il avait même un petit pendentif en forme de croix, et avait appris les prières indispensables pour avoir bon accueil dans les couvents. Mais dans son cœur, il n’éprouvait aucun attrait pour cette religion, pas plus que pour celle qu’il avait pratiquée durant des années quand il servait Zengī[^zengi].

Il hésitait à passer le Jourdain, à aller admirer les forteresses les plus éloignées au sud, rêvant de la cité méridionale dont on disait qu’on pouvait y trouver des navires commerçant avec les Indes. Il gardait un bon souvenir des bateaux, quand il était encore avec sa famille. La traversée du Bosphore avait été particulièrement rapide, mais il avait aimé le goût des embruns sur sa langue, la caresse du vent dans ses cheveux. Le grincement des voiles et des cordages lui avait donné l’impression qu’ils allaient s’envoler.

Il entendit sonner sixte dans le couvent voisin, appelant les frères à la prière. La chaleur allait croître, avec le soleil au mitan du ciel. Il épousseta ses mains à destination des fourmis avant de se faire un oreiller de sa besace et s’étendit dans l’herbe déjà sèche, mains sous la tête. Puis il s’endormit en fredonnant une comptine que, jadis, sa mère lui chantait.

## Ascalon, camp de siège, veillée du lundi 17 août 1153

Assis en bordure des jardins, Garin tressait des cordons avec de vieux brins glanés près des navires démantelés pour faire les engins de siège. Il faisait cela en écoutant distraitement les aventures militaires d’un vétéran. Il avait l’impression d’avoir déjà entendu la moindre anecdote d’intérêt, les récits les plus inventifs. Chaque histoire était la même que la précédente. Les langues changeaient, les conteurs avaient leurs tics propres, mais leurs souvenirs ne variaient que peu.

Il avait rejoint la grande armée qui attaquait Ascalon, et gravitait autour du souk al-askar, le marché qui s’accrochait aux soldats en campagne. Il donnait la main en échange de nourriture, faisait ses petites affaires, menus trafics et services sans lendemain. Il veillait à ne pas trop s’aventurer au milieu du village de toile des guerriers, où les chemins étaient tracés de corde, mais aimait venir admirer les assauts, à l’abri depuis les arrières. Cela le ramenait quelques années en arrière sauf que désormais il vivait tout cela en homme libre.

Si la ville tombait, il avait l’intention de s’infiltrer à la suite des conquérants. Mais ce n’était pas dans l’espoir de piller, juste de pouvoir entrer et déambuler à son aise. Les maisons abandonnées à la hâte lui semblaient comme autant de magnifiques promesses, de merveilleux territoires à visiter. Il pourrait peut-être s’attribuer un logement, non loin du port, d’où il pourrait voir chaque matin les vagues se briser sur la plage.

Mais pour le moment, l’ambiance était plutôt morose dans le camp chrétien. La principale tour d’assaut avait été incendiée deux jours plus tôt. Et si le vent avait finalement rabattu les flammes sur la muraille, entrainant l’effondrement d’un pan de courtine la veille, la joie n’avait que peu duré. Un peloton des hommes de la Milice du temple s’était engouffré dans la brèche, s’octroyant le prestige d’être les premiers à entrer à la suite de leur grand maître. Leurs corps pendus décoraient désormais les remparts, les fātimides ayant veillé à illuminer le chemin de ronde de lampes en ces nuits obscures de dernier quartier.

Un sergent, occupé aux dés jusque là vint s’assoir en silence près de Garin, un pichet de bière à la main. Tout en sirotant sa boisson, il regardait la cité qui les narguait, l’air buté. Il se tourna au bout d’un moment vers son voisin, dont les doigts dansaient sans même qu’il y prenne garde.

« Bien des années que ces démons s’abritent en leurs murailles. Ne pourra-t-on les mettre à bas ?

— J’ai guère la science de ces choses, mestre. L’ost le roi me semble puissant assez à cela.

— Si fait ! Aucun autre ne pourrait assembler tant de bannière et d’engins. Et pourtant nous voilà envasés dans les sables depuis Carême ! »

Garin haussa les épaules, conciliant. Le soldat lui tendit son pichet.

« J’ai nom Fouques, sergent Baudoin. Je n’ai pas souvenance de t’avoir vu en son hostel…

— Moi c’est Garin, je ne sers personne, et ne suis là que pour aider au marché ».

Puis il s’offrit une lampée avant de rendre le récipient et de reprendre son ouvrage en silence. Des voix leur parvenaient depuis les murailles, portées par le vent du large. Une des phrases fit grogner Garin, ce qui éveilla l’intérêt de Fouques. D’un regard, il s’enquit de ce qui se passait.

« Ils nous insultent et nous maudissent…

— Tu comprends leur langue ? Moi qui suis né au royaume je n’en connais que des bribes.

— Je travaille souvent pour des Judéens, des Samaritains : fellahs, marchants ou artisans. Alors j’ai usage de leur parler, oui. »

Fouques acquiesça, s’offrant une nouvelle gorgée tout en tendant l’oreille. Il entendit quelques phonèmes, qu’il fut incapable de reconnaître.

« Et là, ils disaient quoi ?

— Des choses pas terribles pour nos épouses et nos mères, nos filles… »

Il finit sur un rictus moqueur, Fouques hocha le menton.

« Certes, nous en avons tout autant à leur intention. Sauf que nos femmes sont loin d’ici, et les leurs sont à portée de nos mains… 

— Leurs bras n’en sont que plus vaillants ! Ce ne sont pas des Turks qui se battent pour un butin, mais des citadins qui ont peur.

— Il est vrai que chaque fois que la ville fut soumise, ils se sont rebellés, même contre leurs barons quand ceux-ci s’étaient livrés à nous. »

Garin savait ce que cela signifiait dans l’esprit du jeune sergent : soit les habitants se rendaient et fuyaient, soit ils seraient impitoyablement massacrés. Dans un cas comme dans l’autre, il pourrait explorer à loisir les lieux. Il sourit doucement à Fouques, acquiesçant à son alternative silencieuse.

## Région de Gaza, zone d’el Djifar, veillée du samedi 6 septembre 1158

Au milieu de l’oasis, au pied d’un palmier, Garin s’activait à détacher des dattes de leur branchette, les avalant pour tout dîner. Il gardait en permanence sur lui un petit sac de toile fine où il conservait des vivres de voyage. Autour de lui, tout était calme, les abris avaient été dressés pour la nuit et des feux parsemaient les environs.

Il sursauta quand il entendit qu’on l’appelait dans le camp voisin. Il avait été recruté comme caravanier et interprète par un modeste baron qui prenait part à une attaque dans les territoires frontaliers avec l’Égypte : un coup de main avec quelques centaines d’hommes. Garin espérait ainsi pouvoir traverser les zones désertiques et rejoindre la vallée du Nil dont on disait qu’elle était une merveille à voir.

Hugues, le chevalier à leur tête, était un robuste gaillard à la voix tonitruante, nasillarde à cause de son nez cassé. Débarqué depuis quelques mois en Terre sainte, il piaffait d’impatience de mener la charge contre les infidèles. Il avait levé une petite troupe pour l’accompagner dans son périple et ne connaissait rien du pays où il évoluait. Curieux et papillonnant comme un enfant, il était tout le temps en train de poser des questions.

Lorsque Garin se présenta à lui, il était assis sous l’auvent qui lui servait de tente et partageait un repas avec un des shaykh bédouins. Persuadé qu’il s’agissait là d’un des barons locaux, il s’efforçait de faire bonne figure face à leurs coutumes. Il avait très vite compris que c’étaient, comme lui, des hommes de chevaux et appréciait leurs poésies qui en parlaient. Il demandait donc souvent à ce qu’on les lui traduise. Mais pour le moment, il voulait en apprendre plus sur les nouvelles des éclaireurs. Le vieux Bédouin expliqua à Garin, en termes laconiques, ce qu’il en était.

« Ils ont vu une belle caravane qui avance en notre direction. Nous devrions pouvoir les surprendre d’ici deux ou trois jours, pensent-ils.

— Ils ne nous attendent pas ? Et leurs espies ?

— Le shaykh dit qu’il n’y a aucune autre tribu dans les parages. Ils doivent se contenter de leurs yeux. Et ce ne sont pas ceux de l’aigle. »

Le chevalier tonna d’un gloussement propre à effrayer un loup.

« Sont-ce des cavaliers ? Des archers ? De la piétaille ?

— Il dit : une poignée de soldats des bords du Nil, mous comme de la glaise et craintifs comme des chatons. Ils fuiront comme le sable sous le vent dès qu’ils nous verront, abandonnant leurs bagages et leurs vivres. »

Hugues opina. Il avait entendu parler de ces païens qui faisaient demi-tour dès qu’ils apercevaient une charge de bonnes lances de frêne. Il espérait néanmoins que quelques courageux oseraient lui tenir tête, histoire qu’il puisse briser une hampe ou deux. Il n’était pas contre le butin, mais il portait la croix du Christ sur la poitrine afin de combattre en son nom. Manquer à verser le sang à cette occasion lui semblait presque une insulte à la face de Dieu.

Garin passa une bonne partie de la veillée à servir de truchement au chevalier, dont l’enthousiasme était partagé par le Bédouin qui s’amusait de l’aspect infantile de son hôte. Les étoiles scintillaient depuis longtemps dans le ciel quand il fut autorisé à aller se coucher.

Il déambula dans le camp jusqu’à trouver un espace où s’installer pour la nuit. Quelques soldats ronflaient déjà, tandis que les plus anxieux échangeaient à voix basse autour des braises des petits feux qui avait chauffé leur brouet. Il s’emmitoufla dans son vieux manteau élimé non loin d’eux, mais sans être trop près. Le matin du combat était toujours bruyant et affairé pour les guerriers. Il aimait les regarder s’activer, à distance, tout en finissant de se réveiller.

La lune était presque pleine et il la vit qui arborait un sourire tandis qu’elle veillait sur le soleil des hommes. Au contraire d’eux, elle savait certainement quel serait leur destin le lendemain. Il lui fit un clin d’œil et se tourna pour s’endormir.

## Désert d’al-Gifar, fin d’après-midi du vendredi 19 septembre 1158

En très peu de temps, l’attaque avait tourné au désastre et les Latins s’étaient rapidement dispersés, harcelés par des contingents d’archers montés qui bourdonnaient autour d’eux sans relâche. Lorsque les premiers fuyards étaient revenus vers les bagages, Garin avait décidé de tenter sa chance. Il avait ôté et jeté sa croix dans la poussière et s’était mis en marche tranquillement en direction des dunes où avait lieu l’affrontement.

Il avait avancé d’un bon pas, sans se dissimuler, la main sur la tête pour tenir son chapeau de paille, veillant à ne pas trop s’approcher des endroits où il voyait des unités évoluer. Il craignait surtout d’être pris pour un combattant en train de fuir et d’être abattu d’une flèche.

Il venait de rejoindre une piste moins meuble que la zone où il était jusque là quand il aperçut un petit contingent de cavaliers qui trottaient dans sa direction. Il lui était impossible de se cacher d’eux et il estima que ce serait le moment de tenter sa chance. Au moment où le soldat de tête, un émir armuré de soie, faisait stopper son superbe étalon alezan d’un geste sûr, il leva la main et les salua d’une voix allègre, appelant la bénédiction d’Allah sur eux.

L’officier lui lança un regard étonné, découvrant ses cheveux blonds et sa vêture latine. Sans lui laisser le temps de la réflexion, Garin s’inclina respectueusement et demanda dans son meilleur arabe s’ils pouvaient le sauver et le ramener en terre d’Islam.

« Qu’es-tu donc pour parler ainsi ? N’es-tu pas de ces infidèles que nous finissons de débander ?

— Ils m’avaient forcé à les servir, c’est vrai, mais je ne suis pas l’un de ces polythéistes. Et je n’aspire qu’à fuir loin d’eux.

— D’où es-tu ? Quel est ton nom ?

— Ahmed al-Dimashqi. J’ai longtemps vécu par-delà le Bilad al-Sham[^bilad_al_Sham]…

— Tu as le poil bien clair pour un Turk, et guère l’allure d’un Perse ! Es-tu en train de mentir ?

— J’ai été pris enfant et élevé dans la religion par mon maître, qui me fit libre au jour où je me convertis. J’ai ensuite travaillé à ses côtés, dans les périples qu’il faisait pour ses affaires. J’ai été capturé voilà deux hivers, par ce chien de Badawil[^baudoinIII] alors que je m’occupais de ses montures, vers Baniyas. »

L’Égyptien se détendit, mais ne semblait pas totalement convaincu.

« Tu as de la chance que ce soit jour de prière et que je n’aime pas condamner le voyageur sans raison. Tu vas nous accompagner et répondre à quelques questions. Tu auras sûrement grand désir de nous narrer tout ce que tu sais sur les troupes que nous avons vaincues.

— C’est pour moi un honneur que de mettre mes pas dans les vôtres. Puis-je apprendre quel nom je dois louer auprès d’Allah pour cette bonne fortune ? »

Un des cavaliers répondit d’une voix rogue, faisant avancer son cheval d’un air autoritaire.

« C’est l’émir Muhammad ben Mukhtār Shams al Khilāfa qui t’offre l’aman, veille à t’en souvenir ! »

Garin hocha respectueusement la tête, prêt à suivre avec empressement ceux qui lui ouvraient les portes d’un nouveau monde. ❧

## Notes

Le titre de ce Qit’a m’a été inspiré par mes lectures adolescentes, quand je dévorai la série de bandes dessinées de science-fiction *Laureline et Valérian* de Pierre Christin et Jean-Claude Mézières. Un des tomes, *Sur les frontières*, parlait de ces lieux de transition, de connexion dans lesquels erraient ceux qui n’appartenaient plus à aucun monde. Je me suis dit que cela conviendrait parfaitement à Yarankash, tout en jouant sur la polysémie du terme « marche », entre son acception médiévale et celle d’aujourd’hui, de mise en mouvement.

Il y a bien sûr fort peu de documentation sur les marginaux, qui sont, par nature, exclus des récits habituels, si ce n’est pour en illustrer parfois les chroniques judiciaires. Mais j’avais envie de suivre le destin possible d’un de ces déracinés qu’on évoque à mi-mots dans certaines sources.


## Références

Christin Pierre, Mézières Jean-Claude, *Sur les frontières*, Tome 13 de la série *Valérian*, Paris : Dargaud, 1988.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Jackson David Edwards, Lyons Malcolm Cameron, *Saladin. The Politics of Holy War*, Cambridge : Cambridge University Press, Édition Canto, 1997.
