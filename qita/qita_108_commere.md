---
date : 2020-10-15
abstract : 1158-1159. Accueillant dans sa famille une jeune femme indigène, Girout supporte difficilement de renouer avec le passé. Mais, entre autres par solidarité féminine, elle se convainc finalement d’accepter cette nouvelle arrivante.
characters : Girout la Cirière, Joris, Marie, Clarin
---

# Commère

## Clepsta, hostel de Girout et Clarin, soirée du dimanche 6 avril 1158

Malgré un relatif bon accueil le matin, Joris sentait la tension qui s’était installée au cours de la journée. Sa sœur n’était jamais d’un enthousiasme débordant, mais il avait compris à mille petits signes indéchiffrables pour d’autres que quelque chose la chiffonnait. Son époux Clarin lui-même n’avait fait aucun effort pour se montrer aimable, quoiqu’on n’aurait pu dire si c’était dû à son caractère taciturne, à un mécontentement engendré par quelque raison personnelle ou une simple conformation à l’humeur de sa compagne. Quant à Marie, jeune orpheline fiancée de Joris, elle se faisait une telle fête d’être accueillie dans ce qu’elle voyait comme sa future famille qu’elle ne semblait pas être sensible à l’atmosphère tendue.

Lorsque Girout proposa à Joris de l’accompagner dehors pour rentrer les poules et prendre un peu de quoi alimenter le feu, il comprit qu’elle souhaitait en fait s’entretenir avec lui en privé. Le moment des explications était venu. Sa sœur et son beau-frère habitaient un beau manse, mais dont le potager était distant afin de profiter d’une irrigation collective. C’était également là que les volailles étaient regroupées le soir, dans un cabanon branlant contre lequel s’appuyait un tas de bois et la niche d’un féroce mâtin qui surveillait les parcelles cultivées. Indifférent au gros animal qui vint le renifler, Joris posa la corbeille et entreprit d’y charger des bûches. Il laissait à Girout l’initiative et demeura silencieux tandis qu’elle faisait un tour du jardin afin de vérifier qu’aucune attardée n’y traînait. 

Elle revint finalement vers lui, jeta un coup d’œil dans le poulailler avant de mettre en place la planche de fermeture. Puis elle s’essuya les mains sur son tablier tout en le regardant faire.

« C’est pas bien malin d’avoir rien dit, mon frère. Tu espérais quoi en me faisant pareille surprise ? »

Décontenancé d’une telle entrée en matière, Joris se figea et posa lentement la bûchette qu’il tenait avant de se relever, l’air ébahi.

« De quoi tu causes ?

— Ta Marie, là, tu penses que ça se voit pas qu’elle est païenne ? »

Devant l’absurdité de la remarque, il pouffa ostensiblement.

« Élevée par les sœurs de Saint-Jean ?

— Tu entends très bien de quoi je parle, te fais pas plus bête que t’es. Elle est pas si bonne chrétienne que son nom le prétend…

— Elle a perdu père et mère et grandi entre les murs d’un couvent, en la Cité. À ce compte, nous sommes tous deux plus païens encore !

— Que voilà Paradis aisément acquis ! Mais *la caque sent toujours le hareng*, nul doute que ses ancêtres ont craché sur la croix ! »

Joris souffla, devinant la colère qui montait en lui.

« Et alors ? On doit lui faire grief d’un temps passé qu’elle a pas connu ?

— C’est pas de ça qu’il est question, tu le sais bien !

— Eh bien non, je ne sais ! Raconte donc, à moi qui suis si fol ! »

Girout se mordit la lèvre, semblant soudain hésitante. Sa colère se muait en confusion, ses doigts se mêlant à l’étoffe de son tablier sans qu’elle y prête attention. Elle s’assit finalement sur un billot destiné à fendre le bois, se frotta plusieurs fois le visage. Sa voix se fit plus calme quand elle se décida à répondre.

« Je n’ai rien contre elle, frère, elle me semble gentille garce. Mais as-tu désir que tes enfants soient traités comme nous ? Que ta Marie soit méjugée ainsi que mère l’a été ?

Joris dévisagea sa sœur longuement sans répliquer, puis s’assit à son tour sur un caillou appuyé au muret.

« J’ai pas pensé à ça. Pour moi elle est bonne chrétienne. Tu aurais rencontré sa marraine, la Mabile, t’aurais aucune crainte là-dessus. Elle arriverait à faire reproche de mécréance au patriarche lui-même si elle en avait l’occasion.

— Va l’annoncer à tous les gamins, les voisins et les langues acides qui médiront d’elle et de vos enfançons. Mioche, j’en ai entendu plus qu’à mon tour, des « *fille de païenne* », quand c’était pas pire. Même le Clarin, il y allait de sa ritournelle à l’époque. Et si le père m’a fait belle dot, c’était aussi pour ça. Personne ne voulait épouser la fille de *la sarrasine*.

— Mère n’était nulle païenne, elle communiait à l’Église comme tout le monde.

— Elle a reçu le baptême pour se marier. Quand t’es venu, ça s’était un peu tassé, mais pour le Pierre et moi, ça a été dur… »

Girout soupira longuement, n’en finissant pas de se nettoyer les mains de son chiffon.

« Si on apprend que t’as marié une fille comme elle, ça va encore nous retomber dessus. »

Elle marqua une pause, se relevant en se tenant les reins.

« Et t’avise pas d’aller faire visite au Pierre. Il verrait ça d’un mauvais œil…

— Tu veux dire qu’il serait moins charitable que ma sœur qui me fait remontrance d’avoir encontré bonne fille à épouser chez les nonnes de Saint-Jean ?

— Sois pas insolent ! Je l’ai servie à ma table ! Et je discuterai avec le Clarin, savoir comment ça va se passer. Mais le Pierre, lui, il t’ouvrira même pas sa porte. »

## Mirabel, manse des Terres d’Épines, matin du mercredi 3 septembre 1158

Savourant l’abri du petit appentis qu’il avait dressé contre un des murs de la maison, Joris mangeait un léger gruau, rehaussé de quelques raisins secs tout en profitant de l’ombre. Le jour n’était pas très avancé, mais la chaleur et le soleil étaient déjà bien présents. Il avait passé la matinée à finir de ramasser les épis de sorgho chez un voisin et il leur fallait désormais emprunter une mule pour porter le tout sur les toits où ils seraient mis à sécher. Joris avait négocié d’être rémunéré en grain, car il comptait bien emblaver sa parcelle dès le printemps suivant.

Il avait bien avancé le défrichement et s’y employait tous les jours où il ne trouvait pas d’ouvrage aux alentours. Il était loin d’avoir de quoi ensemencer tout ce qu’il avait nettoyé, mais, grâce à l’aide de l’intendant, il avait commencé à discuter avec quelques villageois dans le but d’y planter leurs graines et d’en partager la récolte. Comme il devrait aussi leur louer l’attelage et l’araire pour préparer le sol, il n’aurait qu’une faible part de la moisson, mais cela ne le dérangeait pas. Il voyait chaque jour son domaine s’agrandir, chaque soir sa maison l’accueillir.

Tandis qu’il finissait son écuelle, il suivait des yeux quelques nuages approchant de l’ouest, de la côte. On lui avait dit que c’était la saison des orages et il redoutait que de fortes averses ne viennent compromettre les récoltes. Un temps égal, des pluies suffisantes au bon moment, du soleil en quantité et pas de froid précoce, voilà ce qu’il demandait dans ses prières nocturnes. En plus de la bonne santé de son épouse, dont le ventre s’arrondissait chaque jour. Il avait trop vu de femmes mourir en couches pour ne pas s’en inquiéter. Même si c’était en périphérie de son esprit, la crainte ne le quittait jamais vraiment.

Marie revenait justement du lavoir, sa corbeille de linge sur la hanche. Ils avaient bénéficié d’un don de vieux draps de l’Hôpital, rapiécés et effilochés, mais dont elle pensait pouvoir tirer chemises et langes pour l’enfant à naître. Essoufflée, elle s’assit à ses côtés, étendant ses avant-bras humides au soleil pour les faire sécher.

« Le fils à Hellysent et Bernar a malines fièvres, encore une fois.

— Tout le temps malade, ce gamin…

— Ils ont demandé au père curé de le baptiser, au cas où. »

Joris hocha la tête. Les bébés mourraient, il n’y pouvait rien.

« Elles parlaient aussi de la foire à venir, pour la Saint-Michel. C’est usage ici que colporteurs s’assemblent en nombre. On y trouve toiles, jarres et outils de fer…

— Je dirais pas non à une houe d’acier, pour sûr, mais on n’a rien pour payer. Et on est trop tard venus pour obtenir crédit auprès d’eux.

— Odouart se porterait pas garant ? »

Joris haussa les épaules. Odouart avait le manse voisin du leur et leur avait fait un relatif bon accueil. Lui aussi était  relativement désargenté et travaillait comme un forcené à améliorer son maigre domaine. Frère cadet d’un paysan bien installé dans un casal d’Acre, il avait décidé de tenter sa chance par lui-même. Son épouse était employée en tant que nourrice dans des maisons nobles des environs d’Acre, ce qui lui apportait quelques maigres revenus pour s’équiper. Bien implanté dans le village, on l’écoutait lors des assemblées et il paraissait en bonne voie pour finir au conseil des jurés et des anciens.

« Je lui en causerai tantôt. On doit épierrer la parcelle du contrebas dans les jours qui viennent. »

Marie approuva en silence, hésita à se lever, puis, avant cela, prit son courage à deux mains.

« On pourrait profiter de la foire pour faire porter message à ta famille.

— Y’a pas tant à leur dire.

— Quand même ! Clarin et Girout nous ont reçus. Ce serait bien qu’ils sachent pour le gamin qui vient. »

Joris continuait de racler sa gamelle, pourléchant sa cuiller de bois entre deux phrases.

« On aura bien temps à l’hiver.

— Pourquoi attendre ? Moi, je serais heureuse d’avoir bonnes nouvelles des nôtres. Ça t’enjoierait pas, pareil message de ta sœur ou de ton frère ?

— J’ai pas toujours su pour mes neveux. C’est pas tant important quand on vit loin.

— On peut les inclure dans nos prières, ou leur prévoir menu présent quand on les voit. C’est comme ça qu’on fait, non ? »

Joris fit une moue peu éloquente, se leva avant de faire mine de rentrer dans la maison.

« Je vais aller couper le bois de nos terres, si jamais on me cherche. »

Comprenant qu’il cherchait à éviter la discussion, Marie acquiesça du menton.

« Moi je vais me rendre au jardin. Après le linge, j’ai du zaatar[^zaatar] à ramasser et je voudrais nettoyer le petit coin au levant. Si on pouvait y mettre des fèves, ce serait bien.

— Il faudrait aussi de l’eau, la jarre est presque vide, j’ai vu tantôt. »

Marie opina, mais elle n’était pas décidée à accepter si aisément sa défaite.

« Tu me diras ce soir, pour le message ? Certains des marchands viennent de Jérusalem, ce serait l’occasion ou jamais. »

## Clepsta, terres de Girout et Clarin, midi du jeudi 2 octobre 1158

Girout retrouva Clarin en contrebas d’un bosquet d’acacia, appuyé contre un rocher pour en savourer la fraîcheur. Elle vint s’assoir sans un mot et lui tendit une calebasse de vin coupé d’eau. Puis elle s’employa à disposer entre eux de quoi se restaurer. Pas plus bavard qu’elle, Clarin regardait la parcelle de cotonniers qu’avec quelques autres paysans, il dépouillait de ses capsules depuis deux jours. Au-delà, dans les reliefs environnants, des labours commençaient à zébrer de rouge et de brun le gris ocre des lopins brûlés par le soleil estival.

« Je crois pas que ça soit intéressant, le coton. On en fera pas sur nos biens.

— Tu as eu le nez creux de ne pas suivre le Tassin. »

Clarin ne réagit pas au commentaire de son épouse, habitué qu’il était à voir confirmer la moindre de ses remarques, vite érigée en aphorisme par les siens.

« C’est pas bonne terre pour ça. Et ça gâche de l’eau… On fera comme je pensais, on va plutôt acheter nos propres bêtes, qu’on louera pour le portage, les labours, le foulage.

— Le bélier se fait vieux aussi, il met moins d’ardeur à couvrir.

— Si y crève dans l’année, on demandera au Gilet, le Normand. Il a de beaux mâles. Il nous vaudra bien ça, vu comme je l’ai aidé pour sa citerne. »

Tout en échangeant, ils puisaient dans un pot un solide fromage de chèvre, mélangé d’herbes aromatiques, de compotée d’ail et d’oignons, qu’ils étalaient sur un épais pain noir. Face à eux, en aval, un couple de rapaces planait sur les courants chauds, lançant de temps à autre un cri perçant. Tout en mâchant, Girout en vint au fait de ce qu’elle avait en tête depuis quelque temps.

« J’ai eu des nouvelles du Joris, aussi. Par un pied poudreux revenant du sud. »

Sans prêter attention au grommellement de son époux, elle continua.

« Ils auront un petit avant le printemps, à ce qu’il dit.

— Tant mieux pour eux…

— Et ils m’ont demandé d’être marraine du gamin, qu’ils appelleront de mon nom si c’est une fille.

— Pour sûr, vont pas lui donner le nom de ta mère… »

Girout dévisagea son époux, les lèvres pincées. 

« C’est pas rien, quand même, lui donner mon nom !

— Ils doivent escompter que ça leur donnera droit à quelques monnaies…

— Ils se sont mariés sans même qu’on y aille, je peux bien tenir leur enfançon pour en faire bon chrétien. »

La lueur courroucée qui brilla dans le regard de son compagnon convainquit Girout qu’elle s’aventurait en terrain glissant. Clarin avait catégoriquement refusé de se montrer à la cérémonie. Il ne voulait rien avoir à faire avec « la petite païenne » comme il l’appelait, et certainement pas à Jérusalem, où il connaissait du monde. Il s’était déjà emporté, en lui interdisant de reparler de cette affaire. Elle baissa les yeux, faisant mine de battre sa coulpe. Elle avala quelques bouchées, sans prêter attention à ce qu’elle mangeait. Puis elle revint à la charge, d’une voix adoucie.

« Quand même, Clarin, c’est mon frère ! Et il s’agit de veiller à ce que le gamin soit bon fidèle. Tu sais bien comment il est, le Joris, tête folle. Il est de mon devoir de prendre soin de lui.

— Tu es désormais de ma famille, surtout. Ils t’apitoient avec leurs histoires d’enfançons. T’as le cœur trop tendre, voilà ce qu’il y a. Dès que ça cause de marmots, tu te fais tout miel… »

Le ton était plus dépité qu’irrité, cette fois. Girout conserva un mutisme stratégique, attendant que le processus fasse son chemin dans l’esprit sévère de son époux. Il n’était pas si mauvais homme qu’il se plaisait à en faire démonstration. Elle ne fut donc pas étonnée quand il lâcha abruptement :

« Il est espéré pour quand, ce marmot ?

— Le début de Carême. Ça sera pas aisé pour elle, surtout que ça sera son premier. Je serais de bonne assistance, c’est là soucis de femmes.

— D’aucune façon, ça sera déjà bien assez si tu y vas. On garde nos monnaies pour le bétail, on a rien à distraire pour eux. »

Girout hocha le menton. Il était hors de question qu’elle se présente chez son frère les mains vides pour une naissance. Elle avait bien en tête quelques vieux vêtements et linges mis de côté qui seraient utiles. Et d’ici là, elle trouverait moyen de se procurer quelques jouets pour le bébé. De toute façon, ce n’était pas là affaire d’homme. Clarin n’avait rien à en savoir.

## Mirabel, manse des Terres d’Épines, soir du mardi 27 janvier 1159

Profitant de la présence de sa sœur, Joris avait pu s’absenter pour participer à une veillée chez un voisin. On allait y discuter des labours de printemps, de l’organisation des travaux collectifs et de la rotation des cultures. Les décisions faisaient habituellement l’objet d’un consensus, si ce n’est d’un accord général, de façon à ce que les récoltes soient les plus avantageuses pour chacun. C’était pour lui la première veillée où il s’absentait sans inquiétude, sachant que son épouse était entre de bonnes mains.

Marie et Girout avaient donc soupé toutes deux, assises sur le bord de la couche. Gênées de se retrouver pour la première fois seules, elles se contentaient de remarques pratiques et de questions anodines. Girout éprouvait un léger pincement au cœur de découvrir l’indigence dans laquelle Joris vivait. Il n’y avait pas de table, juste deux tabourets et un coffre en guise de meubles, et la plupart de leurs biens étaient entassés dans des paniers plus moins bancals, certainement l’œuvre de Joris lui-même. Elle avait aussi pu voir leurs réserves, fort basses.

Elle prenait conscience que son petit frère était devenu chef de famille, et qu’il s’employait ardemment à avoir un foyer prospère pour les siens. Ce n’était plus le gamin au nez morveux qui enchaînait bêtise sur bêtise. Elle regretta un instant de n’avoir pas apporté plus, même si elle n’avait aucun doute que Clarin en aurait fait une scène.

Marie avait le visage émacié, les traits tirés de fatigue et de douleur, mais elle refusait de se poser. Il faudrait pourtant la convaincre de s’allonger un peu, avec l’arrivée du bébé proche. Il était mauvais que la mère soit épuisée, Girout le savait. Elle irait voir quelles voisines pourraient aider, et qui était la meilleure accoucheuse par ici. Elle avait découvert avec surprise qu’on ne regardait pas trop de travers sa belle-sœur dans le casal. Elle y avait constaté que la communauté était assez diversifiée dans les environs, et que les villages indigènes y étaient plus nombreux.

« Je n’aurais jamais cru qu’un bébé puisse autant bouger, lâcha tout-à-trac Marie, tandis qu’elle se tenait le ventre.

— Moi, mes filles ont toujours été très agitées. Les garçons étaient plus fainéants, occupés à dormir. »

Marie sourit.

« Je n’ai guère de souvenirs de femmes grosses, quand j’étais enfant. Au couvent, on ne les voyait que pour la délivrance…

— C’est fort heureux pour la réputation des nonnes, ironisa Girout dans un rictus, tout en se levant pour débarrasser leurs écuelles. Je vais aller les frotter.

— Tu es mon invitée, ce n’est pas à toi de le faire ! s’exclama, indignée, Marie.

— Tu es grosse jusqu’aux yeux. Il est temps pour toi de souffler un peu. C’est mieux pour le bébé.

— Tu vas tout de même pas faire mes corvées en ma demeure ?

— Eh quoi, penses-tu que Joris les fera ? »

L’idée d’imaginer un homme occupé à la cuisine ou à l’entretien du linge les fit pouffer toutes les deux.

« Je suis sérieuse, Marie. Je suis là pour ça. La famille… »

Elle ne finit pas sa phrase, comprenant que le mot en disait bien assez en l’occurrence. Puis elle entreprit de frotter les récipients de bois avec de la cendre, avant de les rincer rapidement dans le seau.

« Chaque fois que mes enfants sont nés, mes sœurs sont venues, et plusieurs voisines ont aidé, aussi. C’est le moment où se soutenir entre femmes. Pas d’hommes pour nous diriger, il faut en profiter. Ils ont tellement peur de ce qui se passe alors qu’on a enfin la paix, pour quelques trop courtes semaines.

— Joris est bon époux, tu sais.

— J’espère bien ! Sinon je pourrais encore lui frotter les oreilles, tout chef de famille qu’il est désormais. »

Redevenant sérieuse, Girout revint s’assoir vers Marie. Elle ne pouvait s’empêcher d’éprouver de l’affection pour cette femme courageuse. Mais tellement naïve ! Elle avait l’impression d’entendre ses petites sœurs.

« Tu sais, Marie, tu as de certes encontré bon garçon avec mon frère. Mais si se marier ça donne une famille, ça te pose surtout des chaînes aux pieds. » ❧

## Notes

Les activités quotidiennes des femmes des classes populaires demeurent extrêmement difficiles à décrire précisément. Les sources sont très éparses (je me sers abondamment des travaux autour de Montaillou, postérieur de plus de 100 ans), vu que cela concerne un univers fort éloigné de ce qui nous est parvenu, issu généralement de milieux aristocratiques, cléricaux, urbains et, surtout, masculins. La difficulté est de ne pas proposer une lecture a posteriori progressiste et donc d’imaginer obligatoirement le pire, ou de tomber dans un angélisme célébrant les temps passés. Le monde était rude pour tous, hommes et femmes, et il n’est pas évident qu’elles aient été aussi puissamment dominées que dans certaines périodes ultérieures.

Il demeure malgré tout un élément sur lequel nous avons des informations relativement abondantes, c’est la complexité de l’accouchement et de ce qui tourne autour. Il s’agissait assurément de la première cause de mortalité féminine, ce qui explique peut-être le succès des vocations religieuses. C’était un moment où la société relâchait son emprise sur le corps féminin, ainsi que sur les injonctions diverses, préférant isoler la future mère, au moins jusqu’à ce qu’on nommait les *relevailles*. Il s’ouvrait alors un espace de non-mixité certainement propice au passage d’une culture féminine si ce n’est féministe, comme on peut en voir dans les assemblées au sein des hammams dans les communautés méditerranéennes. C’est en tout cas l’hypothèse que j’ai décidé d’adopter.

Le titre vient d’un sens ancien du mot, qui désigne les personnes liées par le baptême d’un enfant.


## Références

Hodgson Natasha R., *Women, Crusading and the Holy Land in Historical Narrative*, 2017, coll. « Warfare in History », 304 p.

Le Roy Ladurie Emmanuel, *Montaillou, village occitan de 1294 à 1324*, Paris, Gallimard, 2008, coll.« Folio/Histoire », n˚ 9.

Lewis Katherine J. et al. (éd.), *Young Medieval Women*, New York, St. Martin’s Press, 1999.

Shahar Shulamith, *The Fourth Estate: A History of Woman in the Middle Ages*, trad. par Chaya Galai, New York, Routledge, 1983.

