---
date: 2015-09-15
abstract: 1156-1157. Lorsqu’on est sans famille, et plus encore pour une jeune fille, difficile de trouver sa place dans la société. Layla, jeune échappée d’origine musulmane découvre au fil des jours que sa fuite est loin d’avoir résolu tous ses problèmes.
characters: Gosbert, Layla, Beleite, Coleite, Garin la Sonnaille, Frère Guillaume de Calanson, Lizbet
---

# Fille de Magdala

## Césarée, matin du samedi 2 juin 1156

La fraîcheur de la terre battue remontait le long des jambes de Layla, depuis ses pieds nus. Vêtue d’une robe mille fois rapetassée et réparée, trop courte par-dessus sa fine chemise de coton, elle sautillait sur place pour se réchauffer.

Elle aurait préféré se trouver dehors, dans la cour dont elle voyait la lumière pénétrer depuis l’autre pièce, où maître Garin stockait les marchandises à vendre. Elle filait sans s’y appliquer, rompant régulièrement le brin ou faisant des bourres auxquelles elle ne prêtait aucune attention. Une fois encore, sa fusaïole tomba au sol alors qu’elle vagabondait, l’esprit absent.

Lizbet, une grande brune tout en bras et jambes, guère plus épaisse qu’une limande à force d’avaler le brouet insipide qui leur servait de repas deux fois par jour, gloussa en réaction. Beleite, dont la bonne nature était durement éprouvée par le caractère irascible de Layla et sa mauvaise volonté évidente, soupira de façon ostentatoire. Elle était mieux habillée que les deux autres, et clairement bien nourrie, ses appâts féminins plus marqués lui permettant visiblement d’améliorer l’ordinaire.

« Tu vas nous attirer soucis à mal œuvrer ainsi, Layla…

— Peu me chaut ! Je ne compte pas rester ici. »

Beleite secoua la tête.

« Et où iras-tu ? Les sœurs se sont occupées de toi, tu pourrais au moins leur rendre cette grâce. Maître Garin n’est pas mauvais baron. Il a déjà doté une de ces œuvrières.

— Il a logé une de ses putains avec un de ses valets, tu veux dire . »

Lizbet gloussa à la réponse, ce qui fit également rire Beleite, un peu de mauvaise grâce.

« Si fait, tu parles de vrai. On dit que Durant a si belles cornes qu’on y peut crocher son mantel…

— Et qu’Hersent y met son linge à sécher .

— Au plus fort de l’été, les chiens aiment à dormir à leur ombre, fort étendue . »

Les rires repartirent de plus belle, ce qui finit par attirer Garin. Sa voix forte tonna sous les voûtes de la pièce contiguë.

« Ho là, pucelles. J’espère que le fil s’enroule aussi vite que vos gorges déroulent tous ces rires . »

Son imposante silhouette, enrobée d’une couche épaisse de tissus de prix, quelle que soit la saison, vint cacher la lumière. Il n’était pas très grand, mais généreux de carrure et de ventre, avec une barbe qu’il taillait une fois l’an, pour la sainte Barbe justement.

La cinquantaine passée, il était marié à une bigote qu’il s’employait à tromper avec une régularité qui lui avait valu le sobriquet de Garin la Sonnaille, rapport à son pendu souvent occupé à battre. Il avait néanmoins poursuivi de ses assauts son épouse suffisamment d’années pour en obtenir une marmaille aussi nombreuse que remuante, qu’il entretenait grâce à son prospère commerce de drap.

Il s’intéressait à tout ce qui pouvait finir en étoffe : achat et revente de toisons, cardage, filage, tissage, teinture. Il investissait partout, et était associé à la moitié des fabricants de la côte. En outre, cela lui donnait l’occasion de souvent voyager et d’embaucher des femmes, habiles à faire le fil disait-il, et à dresser son membre, murmurait-on.

Il n’était pas mauvais bougre, mais pouvait avoir la langue acide s’il n’obtenait pas ce qu’il espérait. Il n’était malgré tout pas rancunier et oubliait ses griefs aussitôt son désir comblé. Lorsqu’il pénétra dans la pièce, Lizbet et Beleite se levèrent, et Layla finit de ramasser sa fusaïole.

« Encore à répandre ton ouvrage, Layla ? »

Il soupira.

« Las, que vais-je faire de toi si tu n’es guère habile de tes doigts. »

Le regard qui caressa le corps maigre ne laissait aucun doute quant au fond de sa pensée.

« Je crois assavoir malgré tout que tu as la langue habile, cela pourrait plaider en ta faveur, ça, mignonne. »

Il gronda de contentement à sa blague et vint se coller contre Layla, palpant son bassin osseux d’une main gourmande.

« Bien dommage que tu aies le tétin si maigre. J’aime les jeunettes en chair… Mais on trouvera bien à t’employer, va. Je ne suis pas mauvais sire… »

Se tournant vers Beleite, il se mit à lui malaxer un sein comme pain en pétrin, puis émit un grognement qui se voulait de contentement avant de ressortir. Passant le chambranle, il ajouta, la voix redevenue sérieuse.

« Mets plus de cœur à l’ouvrage, petite, et tu auras du beurre dans ton brouet. On n’a rien sans rien dans la vie. »

Puis il s’éloigna d’un pas allègre, chantonnant pour lui-même :

« *J’ai outil fier et adroit tantôt courbe tantôt droit…* »[^voirfierarcher]

## Environs de Caco, matin du mardi 8 janvier 1157

La pluie avait redoublé à l’amorce de l’aube, achevant la nuit en véritable averse, après une bruine insidieuse qui avait apporté les senteurs salines de la mer toute proche. Le dérisoire abri de feuilles de palmes de Layla n’avait guère été efficace et son petit recoin de ruines était désormais de boue et d’eau.

Elle-même était trempée, grelottante sous ses vêtements de coton . Ses cheveux dégoulinaient, maintenant le froid et l’humidité dans son cou. Quelle que soit la position qu’elle adoptait, elle ne parvenait pas à se réchauffer. Elle s’était installée quelques semaines plus tôt à côté des vestiges de ce qui était vraisemblablement le tombeau de quelque personnage important. Mais le notable avait été oublié, et après avoir été amputé de ses plus belles pierres, le bâtiment finissait de s’écrouler saison après saison.

Les murs ne montaient guère plus haut que la taille, et tout ce qui avait pu avoir de la valeur avait été pillé. Un bosquet de palmiers s’y accrochait, près du lit d’un petit ruisseau asséché en été. Au plus fort de l’hiver, il gargouillait joyeusement. Au départ, la jeune fille était heureuse de sa découverte. Elle avait tenté de s’aménager un petit coin douillet, bien à elle, mais le vent et sa méconnaissance des lois les plus élémentaires de l’architecture n’avaient abouti qu’à un misérable appentis. Elle s’y pelotonnait dans les feuilles, espérant le sommeil, avec une butte de terre pour oreiller. Elle n’avait malheureusement aucun moyen de faire du feu, et aurait hésité à en faire de toute façon.

Elle vagabondait en bordure des champs cultivés et des jardins, cueillant fruits et légumes à la lueur de la lune et des étoiles. Souvent elle avait salivé en entendant des poules caqueter, à l’idée de gober un œuf, voire de croquer dans la viande juteuse. Mais elle se contentait de graines broyées, de tout ce qui pouvait croiser sa route tandis qu’elle cheminait par la campagne. Elle goûtait à la liberté depuis la première fois.

Née dans un casal misérable, où ses parents s’épuisaient pour un baron malfaisant, sire Robert, elle avait tôt appris à baisser la tête et le regard, à courber l’échine, quand il passait. Cruel et éternellement insatisfait, il aimait à entendre la badine craquer sur le dos des récalcitrants. Ses soldats, aussi malveillants que lui, brisaient les chevilles des captifs pour leur ôter l’envie de galoper au loin. Dès qu’elle fut en âge de marcher, on la mit au labeur. D’abord, à des tâches domestiques, puis lorsqu’elle fut adolescente, elle rejoignit les travailleurs des champs. Là elle épierrait, sarclait, arrosait, récoltait, pour engraisser et enrichir celui qui la récompensait d’insultes et de repas maigres, pris dans une masure branlante.

Ils étaient serfs, attachés à la terre et à son maître, guère plus estimés que des meubles. Sa mère lui avait enseigné que cela était dû à leur foi, qu’ils étaient des disciples du Prophète et du Dieu unique. Elle n’avait jamais rien compris à ça, ne voyait pas en quoi cela la concernait. On lui disait qu’elle était une femme et qu’elle devait obéissance à son père, qu’elle était impure et qu’elle était punie pour ses fautes. Un jour viendrait où Allah exterminerait tous les malfaisants comme le sire Robert, et alors elle serait récompensée d’avoir suivi la vraie foi. En attendant, elle maugréait et accomplissait les rites qu’on lui indiquait, mais sans jamais vraiment y comprendre quoi que ce soit. Elle avait vu sa mère mourir de maladie après s’être tuée à la tâche, affaiblie par des grossesses à répétition.

Son père, fier et autoritaire malgré son assujettissement, n’avait jamais manifesté le moindre chagrin et avait épousé sa belle-sœur, qui vivait avec eux depuis toujours. Rien n’avait vraiment changé, et par la suite Layla s’était demandé de qui elle était la fille. Dès qu’elle en eut l’occasion, elle se joignit à un groupe de fugitifs et s’efforçait depuis lors de survivre. Récupérée par des nonnes qui lui avaient offert un repas chaud, elle avait été placée chez Garin pour y apprendre un métier, chichement dotée par la charité des moniales. Mais contrairement à Beleite, elle n’était pas faite pour le joug. Elle avait envie de sentir la brise, le soleil sur ses joues, de faire selon son désir comme les héroïnes des contes que sa soeur Rayya lui narrait quand elle était petite. Rayya, morte lors de son premier accouchement, en même temps que son enfant. Ils gisaient désormais dans une terre froide qui appartenait à un autre. Elle se secoua, tentant de se réchauffer par les frissons.

Le vent qui avait accompagné le jour blafard avait chassé les nuages, mais lorsqu’elle se leva, elle en ressentit la morsure avec d’autant plus de douleur. Elle ramassa les chiffons qu’elle avait noués en un sac, où elle gardait quelques réserves de nourriture puis se décida à aller chercher de quoi manger. L’activité lui ferait du bien. Le bruit d’une chevauchée la fit hésiter un instant, et elle tendit le cou pour voir par-dessus les tronçons de murs. Une vaste compagnie avançait sur le chemin en provenance de Caco. Des chevaux, montés par de fiers soldats en armes, dont certains portaient une cape blanche, encadraient des mules et des poneys, ainsi que quelques chameaux. Cette riche caravane prenait la direction du sud. Elle contempla avec envie les balles de marchandises soigneusement empaquetées, les filets de provisions pendant sur les flancs. Faire la route dans les traces de cette troupe lui garantirait la tranquillité. Nul malfaisant n’oserait traîner aux alentours d’une pareille escorte armée. Sans même jeter un dernier regard à son abri précaire, elle se mit en marche, soudain ragaillardie.

## Calanson, soir du lundi 22 avril 1157

Tout d’abord, Layla rampa, puis, ramassant d’une main hésitante ses habits déchirés, clopina avec difficulté jusqu’au muret d’un jardin, où elle s’appuya. Le souffle court, des larmes plein les yeux, elle ne se sentait que plaies et bosses, crasse et sang.

L’homme qui avait abusé d’elle s’était endormi dans le caniveau, comme l’animal qu’il était, et ronflait bruyamment, sur le flanc, son canif encore entre ses doigts gourds. Tremblante, la jeune fille ne savait pas quoi faire. Une douleur déchirait ses entrailles à chaque mouvement, une nausée la gagnait. Elle n’entendit personne approcher.

« Sainte Vierge ! » s’exclama une voix rauque.

Layla tourna la tête et aperçut plus qu’elle ne vit le petit groupe. Un moine habillé de noir, quelques femmes, des ombres dans la lumière de leur lanterne.

Le clerc s’agenouilla vers le ronfleur et soupira, puis rejoignit à pas lents de la jeune fille, déjà entourée.

« Que t’arrive-t-il, mon enfant ? Est-ce tien compagnon qui gît là ? »

La question hypocrite déchaîna la colère d’une brune dont les yeux clairs brillèrent de rage contenue.

« Que me chantes-tu, moine ? Tu vois bien qu’il a forcé la gamine . »

La réponse, plus que le ton, choqua le moine, qui n’avait guère envie d’être confronté à ce genre de choses. Le dégoût que cela lui inspirait lui dessina un visage horrifié. On ne savait dire ce qui l’effrayait le plus, de la mention d’un tel acte ou du fait que cela ait été pris de force.

« Il faut en appeler à la sergenterie, on ne peut laisser cela…

— Oui-da, il faut soigner cette petite. Je la reconnais, elle traînait aux alentours du camp, à rendre menus services. »

Le soûlard remua dans son sommeil et se tourna, dans un grognement sonore qui les fit tous sursauter. La petite brune qui avait pris les choses en main le désigna d’un mouvement de tête.

« Il faudrait s’assurer de lui, qu’on puisse le juger.

— Mais qui donc…, commença le moine, soudain stoppé par le regard impérieux de la femme.

— Allons, moine, ne me dis pas que tu connais si mal le siècle ! »

Se penchant vers Layla, elle l’aida à se relever, arrangeant du mieux qu’elle le pouvait ses maigres effets. Elle déposa sur ses épaules une couverture et lui susurra quelques mots de réconfort, sans obtenir de réaction. L’adolescente agissait comme une somnambule. Leur arrivée fit grand bruit dans le petit hôpital et le frère en charge de l’accueil refusa catégoriquement qu’on mette Layla avec les pèlerins et voyageurs.

À sa mine dégoutée, c’était plus par répulsion envers la jeune fille que par souci de la protéger. Elle eut néanmoins la chance de se voir affecter une pièce, débarras de meubles et d’outils où un lit sommaire fut installé, simple paillasse sur quelques planches.

Puis les clercs laissèrent les femmes prendre en charge Layla, tandis qu’eux s’occupaient de garder le violeur à l’œil, le temps de statuer sur son sort. Son crime était évident, mais la jeune fille n’avait personne pour demander réparation, et le coupable était un soldat dont le baron n’apprécierait peut-être pas que des moines le tiennent captif.

Layla avait un peu retrouvé ses esprits et s’était réfugiée dans un mutisme buté. Elle avait accepté les habits et le repas chaud avec empressement, et se pelotonnait sous les couvertures. À ses côtés, la brune qui l’avait prise en charge, et qui l’avait nettoyée avant de l’aider à se vêtir était assise, cherchant à la faire parler.

Layla se contentait de fermer les yeux, ou de fixer la mèche de la petite lampe à graisse sur l’escabeau à côté de son lit. Devant l’insistance de la femme, elle consentit à la regarder, s’efforçant à mettre du sens sur ce qui sortait de ses lèvres. Elle fronçait les sourcils, tentait de comprendre. Puis elle finit par articuler péniblement :

« Que va-t-il m’arriver ?

— Je ne sais de sûr, mais il nous faut assavoir ce qui s’est passé. »

Layla se mordit les lèvres et remonta la couverture sous son menton, soudain renfrognée. La femme insista .

« Je m’appelle Anceline, je suis comme toi, pauvresse sans feu ni lieu. Si le gros dit que tu étais avec lui contre monnaie, il n’aura que petite amende pour t’avoir frappée.

— Je ne suis pas… » lança violemment Layla, indignée par le sous-entendu.

« Ai-je dit cela ? Et quand bien même, je le suis bien, moi. »

Stoppée dans sa fureur par cette surprenante confidence, Layla fronça les sourcils et examina de plus près la femme face à elle. Elle n’était peut-être pas si âgée que ça, mais il lui manquait plusieurs dents, et sa chevelure s’ornait d’argent. Des rides et une couperose avilissaient des traits fins, un sourire las. Les mains étaient crevassées, les ongles courts sales et les veines saillaient sur des doigts osseux.

« Je fais la lavandière pour les soldats, mais parfois certains se sentent bien seuls, alors je les câline. Mais je garde toujours canivert[^canivert] caché en mes robes, au cas où l’un d’eux me croirait à sa pogne.

— Qu’est-ce qui va m’arriver ? »

Anceline se rapprocha et parla plus doucement, comme à une enfant.

« S’il est prouvé que tu n’es pas telle que moi, on va l’escouiller et le mettre à l’amende. Et toi tu finiras nonnette.

— Nonnette ?

— Au couvent, à prier pour le salut de ton âme, après ce péché, et pour le sien, car c’est là bonne charité . »

La remarque fit s’esclaffer Layla, de fureur autant que de dérision.

« Mais je n’ai rien fait, c’est lui qui…

— Je me doute bien, mais entre ceux que notre fendu rend fou de désir et ceux qu’il dégoûte, il est bien malaisé de faire sa route ici-bas. Moi j’ai choisi de m’en servir pour m’affranchir de tous ces bâtards . »

Elle caressa avec douceur la main de la jeune fille.

« Ils ferment la porte avec une barre, de peur que tu ne t’échappes. Je viendrai te voir encore demain, et en repartant, je pourrai bien oublier de la tirer de nouveau. D’ici là, pourpense à cela, et remplis-toi la panse autant que tu le pourras. La suite, tu ne la devras qu’à Dieu, et à ta décision. »

Anceline se leva et sourit une dernière fois, avec gravité et tristesse. La petite flamme creusait les sillons de son visage à en faire une vieille femme, elle qui n’avait pas trente ans. Layla ne répondit pas lorsqu’elle sortit, non sans une ultime bénédiction.

## Plaine de la Bocquée, matinée du dimanche 15 septembre 1157

Le petit feu de camp fumait tant que Layla renonça à s’en occuper. Elle avait la flemme d’aller quérir du bois sec et saurait bien trouver un volontaire pour cela, en échange de quelques promesses.

Assise sur un caillou, elle reprisait maladroitement un vêtement, sans aucun soin. Elle détestait encore plus coudre que laver. Seulement, avec ses compagnes, elles étaient officiellement là pour servir de lingères aux soldats, et il fallait bien parfois donner le change. Le vent rabattit soudain vers ses oreilles les voix des fidèles à la messe célébrée en plein air dans une clairière voisine. Elle haussa les épaules, amusée. Elle ne comprenait guère plus cette croyance que celle de ses ancêtres, même si elle faisait semblant de la suivre, histoire qu’on ne la suspecte pas d’être une ennemie.

Elle avait vite intégré qu’il s’agissait avant tout de sauver les apparences dans un monde hypocrite. Elle n’était pas plus chrétienne que lavandière, mais savait en donner suffisamment l’illusion pour qu’on la laisse en paix. Coleite arriva en soufflant, un gros sac à l’épaule. C’était une solide fille aux longs cheveux blonds, au visage rond et au nez retroussé. Son imposante poitrine lui valait une clientèle nombreuse, et elle était la plus prospère de toutes, s’attirant une certaine jalousie des autres en général et de Layla en particulier, elle qui n’était que peau et os. Malgré tout, Coleite dirigeait plus ou moins leur groupe, et possédait le petit âne et la carriole avec laquelle elles transportaient leurs affaires à la suite des soldats. Elle lâcha le sac dans un soupir de contentement.

« J’ai pu acheter farine à bon prix, on aura de quoi se faire des galettes pendant un moment. J’ai avancé votre part. »

Layla hocha la tête et se leva, voyant un cavalier s’approcher de leur petit campement. Les lingères se regroupaient généralement pour se garantir les unes les autres, en cas d’intentions malveillantes de la part des soudards, mais elles se menaient entre elles une concurrence féroce. La survie était à ce prix. L’homme n’était pas un chevalier, sa posture et sa monture le trahissaient. Mais il était vêtu d’un haubergeon et portait une épée. Il avait donc certainement la bourse bien garnie, arbalétrier, ingénieur ou capitaine. Voyant que Layla se dirigeait vers lui, il s’approcha d’elle. Son visage rond avait connu des jours meilleurs et la barbe qui l’envahissait, de gris mêlé, le vieillissait fortement. Il était encore vigoureux et semblait habitué au commandement. Étrangement, il se recoiffa de la main comme un damoiseau avant de s’adresser à l’adolescente.

« Le bon jour à toi, fille. Je viens voir si d’aucunes lavandières auraient envie de venir à mon camp cette nuitée.

— Cela se peut, y a-t-il beaucoup de linge à frotter ?

— Tant et plus, de l’ouvrage pour demi-douzaine je dirai. Il y aura du vin pour les gosiers fatigués, aussi.

— Je suis avec la Coleite, tu la connais ?

— Non, mais je demande que ça » répondit l’homme avec un sourire grivois.

Il se gratta l’oreille, embrassant la maigre installation des femmes d’un coup d’œil.

« Venez en fin de journée, dans le camp du sire comte de Flandre Thierry.

— On demande après qui ?

— Gosbert, je suis un de ses capitaines. »

Layla s’amusa à faire une révérence moqueuse, ce qui arracha un sourire au cavalier. Puis, la saluant de la main, il s’éloigna vers le vaste camp des armées latines. Lorsque Layla revint auprès du foyer, elle constata que Coleite avait trouvé du bois sec et faisait repartir le feu.

« Y voulait quoi ?

— On a du boulot pour la soirée.

— Linge à gratter ?

— Couenne à frotter. »

Coleite acquiesça et souffla sur les braises tandis que Layla s’asseyait pour reprendre son ouvrage. Suspendant son geste, elle repensa aux derniers mois. Finalement, elle tenait sa liberté, qu’elle espérait depuis tant d’années. Elle se l’offrait avec son sexe, ce sexe qui faisait d’elle une moins que rien, mais qui menait les hommes, et certainement le monde. Elle sourit soudain. Un jour, il faudrait qu’elle tente de séduire un chevalier, voir s’ils étaient aussi bêtes que les autres à l’espoir de battre pendu contre fendu comme le disait la chanson. ❧

## Notes

La condition féminine médiévale au sein des classes populaires est très difficile à connaître, car il est rare que les sources s’y intéressent, et cela se fait bien souvent via un prisme masculin. La pression familiale est néanmoins prégnante, peut-être encore plus que pour les hommes. Pour celles qui n’avaient nulle parentèle sur laquelle s’appuyer, il n’y avait guère d’opportunités, surtout si elles n’avaient jamais été mariées. Le statut de veuve pouvait en effet permettre une vie indépendante honorable, tandis qu’une jeune fille qui demeurait ainsi était montrée du doigt, sauf à appartenir à une congrégation religieuse.

Les différentes cultures en Terre sainte médiévale s’accordaient toutes néanmoins sur un fait essentiel : la femme était une incapable, une mineure, qui devait avoir des référents (masculins bien évidemment) pour avoir une existence décente. À l’opposé, les femmes qui accompagnaient les armées se voyaient couvertes d’opprobres, souillant les combattants de la foi selon certains clercs misogynes. Pourtant, on ne pouvait les interdire complètement, car elles rendaient de réels services aux hommes en campagne. En nettoyant le linge, et parfois les soldats, elles permettaient d’apporter une hygiène, minimale, au sein des troupes.

Bien évidemment, la prostitution qui y était liée était le sujet de tous les commérages. Cela constituait en fait un des rares moyens pour ces femmes de vivre en marge de la société où elles ne pouvaient exister par elles-mêmes. D’ailleurs, l’hypocrisie était telle que de temps à autre, les bordels étaient financés par des congrégations religieuses. La boucle était bouclée, les hommes reprenaient le pouvoir dans ce maigre bastion où pouvait s’exprimer une relative indépendance féminine.

## Références

Schaus Margaret (éd.), *Women and Gender in Medieval Europe, an Encyclopedia*, Routledge, New York & Londres : 2006.

Walter Wiebke, *Femmes en Islam*, Paris : Sindbad, 1981.
