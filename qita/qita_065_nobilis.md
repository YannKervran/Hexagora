---
date : 2017-03-15
abstract : 1158-1159. En charge des affaires militaires du royaume, le connétable Onfroy de Toron doit aussi apprendre à ménager les différents cercles d’influence qui gravitent autour du roi Baudoin III.
characters : Onfroy de Toron, Régnier d’Eaucourt, Baudoin III de Jérusalem, Amaury de Jérusalem, Philippe de Milly
---

# Nobilis in nobili

## Jérusalem, tour de David, veillée du mercredi 27 août 1158

Noyé dans un océan écarlate, le soleil finissait de s’effacer derrière les collines de Judée. Appuyé sur le bord d’un créneau, le connétable Onfroi de Toron admirait le spectacle en silence, faisant rouler doucement son gobelet de vin sur la pierre tiédie au long de la journée. Il sentait la fraîcheur de la nuit progresser sur son échine de soldat, remplaçant la chaude moiteur qui l’avait baigné depuis le matin. Il se retourna, étira lentement son dos massif en grognant tel un ours, puis passa une main puissante dans ses cheveux afin d’en repousser l’épaisse toison en arrière.

Se lissant la moustache, il lança un œil circonspect sur les tablettes de cire parsemant le plateau devant lui. Puis il leva la tête vers la dernière silhouette encore présente avec lui sur la terrasse de la forteresse : Régnier d’Eaucourt. Un chevalier comme il les appréciait. De condition modeste, il était d’une loyauté sans faille envers ses maîtres. Et, surtout, c’était un homme de guerre, de ceux qui savaient tenir en selle sans pour autant céder trop aisément à l’euphorie d’une charge bien appuyée.

« Alors, d’Eaucourt, en avons-nous vu assez ? Ou demeurent encore quelques garnisons mal fagotées ?

— Je ne crois que nous ayons oublié aucun point d’importance… »

Le connétable haussa les épaules, faisant danser les motifs de soie de sa cotte.

« Nous avons moindre urgence que par l’an passé. »

Classant les derniers inventaires de garnison par fief, Régnier hocha doucement la tête puis se versa de quoi se désaltérer, un vin brillant dans les reflets des flammes des lampes à huile. L’été précédent, ils avaient failli perdre le roi, en plus de nombreux combattants de grand renom, lors de la bataille de Meleha.

« Vous avez idée de la raison qui me fait monter ici pour travailler ?

— C’est là plus belle forteresse de la cité, propre à plaire à tout mestre de guerre.

— Voilà bon sens incarné. De fait, j’ai surtout plaisir à mirer les terres que nous avons en garde. D’ici, mon regard porte au loin et je peux voir la presse des pérégrins impatients de découvrir la ville sainte entre toutes. Cela me rappelle l’importance de ce que nous faisons. Nous sommes des gardiens, d’Eaucourt. Les clercs se disent pasteurs du troupeau et, dans ce cas, nous sommes les féroces mâtins qui veillent. Notre tâche, pour être moins sale que celle du commun, n’en est pas moins épuisante, bien au contraire. »

Il posa son verre, fit mine de regarder les tablettes de cire et rouleaux répandus sur la table puis retourna s’appuyer à un des créneaux, après en avoir caressé les impressionnants moellons.

« Durant la chevauchée en terre de Suète, avez-vous ouï parler les hommes à propos du maréchal ? »

Régnier fut un peu décontenancé par la question, écarquilla les yeux et bafouilla un peu avant de répondre.

« Pas que j’en ai souvenance… D’aucuns auraient-ils eu souci avec lui ? Vous en a-t-il fait mention ?

— Si seulement ! Il ne vient m’entreparler que pour geindre de se voir en tel office. »

Régnier n’osa pas rebondir sur l’affirmation, mais il était un peu choqué. Être maréchal constituait une des plus belles récompenses pour les chevaliers de la maison du roi. C’était un office clef, très envié, car prestigieux. Mais bien souvent il se voyait attribué en fonction d’impératifs politiques. C’était le cas avec Joscelin d’Édesse, jeune héritier sans terre, dont le nom était la seule fortune. L’année précédente, beaucoup avaient accueilli avec soulagement la capture du bouillonnant et tonitruant Eudes de Saint-Amand, pour découvrir avec dépit l’arrivée d’un novice à ce poste.

Le connétable joua en silence quelques minutes avec son imposante moustache puis commença à faire les cent pas sur les dalles dont l’ocre avait cédé la place à la grisaille de la nuit.

« Vous avez usage de le croiser, vu votre proximité avec le comte de Jaffa. Auriez-vous idée de ce qu’il désire, de la façon dont nous pourrions le contenter ?

— Il n’a pas plus grand désir que de reconquérir les terres perdues par les siens au nord. Il en parle souvent, surtout depuis qu’Harim est de nouveau chrétienne. Il se verrait bien lancer la reconquête depuis ce bastion.

— Insoucieux tel son père ! Où compte-t-il trouver soudards et chevaliers pour cela ?

— Ce n’est encore que projets en son esprit.

— Fol espoir de béjaune, oui ! J’ai besoin d’un roc à mes côtés, pas d’un trouvère qui rêve de folies. Le maréchal, c’est un peu le cellérier de notre moustier. Sans lui, bien difficile de tenir l’ost. Sa bannière doit inspirer le respect et la ferveur chez nos hommes, outre la terreur chez nos adversaires… »

Régnier rangea quelques tablettes dans leur coffret, réunit plusieurs rouleaux dans des étuis, hésitant à proposer une idée. Elle lui était fort déplaisante, mais il se sentait en devoir de la mentionner.

« N’avons-nous pas espoir que le sire Eudes ne soit libéré pour bientôt ? On m’a dit que sa rançon était fixée. Joscelin pourra retrouver sa liberté sans que cela nuise à quiconque.

— Crois-tu vraiment que j’ai désir de voir Saint-Amand de retour à ce poste ? Il est là où il est par la folie de ses actions, je n’ai guère désir de le reprendre. »

Sentant qu’il s’était emporté, il lâcha un sourire.

« C’est bien là mon souci : Joscelin sera bien trop heureux de laisser la place à Eudes. Il me faut donc en changer avant que l’ancien maréchal ne quitte les geôles sarrasines. Mais cela ne peut se faire sans subtilité. D’autant que j’aimerais avoir cette fois quelqu’un qui sache faire autre chose que poingner à hue et à dia. »

## Jérusalem, palais royal, chambre du roi, matin du vendredi 17 octobre 1158

La pièce était littéralement sens dessus dessous. Des coffres gisaient un peu partout, la gueule largement ouverte pour tenter d’engloutir la déferlante d’étoffes et d’accessoires. Une nuée de valets tourbillonnait habilement entre les meubles, répartissant les délicates tenues selon les indications lancées par un clerc à l’allure négligée et l’air affairé.

Au milieu de cet ouragan, la haute et solide silhouette du roi Baudoin impressionnait de tranquillité. Il était vêtu d’une cotte à motifs en écailles dessinant des reflets moirés à chaque mouvement. Devant une des fenêtres, il finissait de tailler son épaisse barbe blonde à l’aide de minuscules forces[^forces] tout en jouant avec un miroir de bronze scintillant dans la lumière du matin. Comme de nombreux jeunes, il cédait assez volontiers à la mode et accordait une grande attention à son allure. Ce qui expliquait l’impressionnante armada de bagages qui se préparait en vue de son voyage vers Antioche où il devait rencontrer l’empereur byzantin Manuel Comnène.

Sans même ralentir à l’entrée dans la pièce, son frère Amaury, comte de Jaffa, l’interpella gaiement. La bonne entente entre eux était de notoriété publique et s’illustrait parfaitement dans la simplicité de leurs relations. En dehors des cérémonies officielles et solennelles, ils se comportaient en parfaits complices, l’aîné admonestant son cadet pour ses plaisanteries comme dans la plus modeste des familles. Il n’était que le sujet de leurs parents pour les faire se disputer, sans que cela ne porte jamais à conséquence.

« Alors, mon frère, te faut-il connétable pour choisir tes bagages ? »

En disant cela, Baudoin sourit à Onfroy de Toron. Il appréciait l’homme autant que l’officier et se félicitait chaque jour de l’avoir à ses côtés. Le baron plia le genou de façon rapide, mais appuyée, tandis qu’Amaury s’approchait pour étreindre son frère avec la douceur d’un ours. Baudoin lui pinça les côtes en riant :

« Mon frère, il va te falloir prendre garde à ne point trop emplir tes bliauds ! Il ne sied pas pour baron d’être gras comme moine.

— D’aucuns finissent rois avec bedaine à faire pâlir un évêque et n’en sont pas moins fort batailleurs. »

Baudoin posa ses affaires sur un escabeau et invita les arrivants à s’installer sur des banquettes habillées de coussins, dans une vaste niche décorée de céramique vernissée. Lui-même prit place sur une chaise curule, sur une estrade face à eux. Il aimait à tenir conseil ainsi, simplement, avec ses proches.

« Les préparatifs vont-ils bon train ? J’ai grande crainte à laisser le prince d’Antioche seul avec le *basileus*.

— D’après nos coursiers, l’ost des Grecs n’est pas encore annoncé, le rassura Onfroy.

— Y a-t-il quelque mauvaise nouvelle à me conter, que je vous vois faire si triste figure tous les deux ? »

Amaury concéda un sourire sans joie et le rassura bien vite.

« Ce n’est pas novelté que je vais te narrer. As-tu pourpensé de mon frérastre[^frerastre] ?

— J’avoue ne pas y avoir vu urgence. Il me semble tenir son rôle fort justement. Pourquoi changer d’officier ? Je suis surpris de te voir ainsi pousser ta famille au loin, Amaury.

— C’est bien de cela qu’il s’agit. Joscelin n’est pas homme de joug, tu le vois bien. Il a grand désir de reprendre les fiefs de ses pères. »

Baudoin se redressa dans son siège. Il avait nommé le jeune Courtenay, car il tenait à montrer son attachement aux vieilles familles du royaume. Son père n’avait que trop cédé à la tentation de faire venir des concitoyens angevins qu’il avait favorisés. Mais Joscelin Courtenay s’avérait inadapté à son poste et n’avait guère estimé le geste à sa juste valeur. De tout son être, il se sentait encore comte d’Édesse et n’avait nul goût à servir un autre, fût-il le puissant et prestigieux monarque de Jérusalem.

« J’entends bien cela. Mais vu que Saint-Amand devrait être des nôtres d’ici peu, il ne me sera alors guère aisé de lui refuser le poste vacant. Est-ce bien ce que nous voulons ?

— Mon sire Baudoin, il me semblerait bien déraisonnable de lui permettre d’officier là où il a fort mal servi. À Panéas, il s’en fallut de peu que ses façons ne mènent au désastre. Il est de ces hommes à la vaillance si grande qu’il croit chacun forgé du même acier que lui. »

Baudoin sourit à la remarque de son connétable, capable de vanter les qualités d’homme qu’il cherchait à évincer. Amaury s’avança de façon à prendre la parole.

« J’ai peut-être bonne solution à ce dilemme, mon frère. Tu as souvenance de Guillaume, frère de Philippe de Cafran ?

— Comment oublier un de ces vaillants compagnons qui a percé ma voie à Meleha ?

— Celui-là même. C’est un cousin d’Hugues d’Ibelin, comme tu le sais, donc de bonne fame. Ses qualités en font un compagnon apprécié, aussi solide en selle que vaillant dans le behourd[^behourd]. Outre, il est dans ton hostel depuis fort longtemps et a prouvé maintes fois ses qualités. J’en ai parlé au sire connétable, qui a reconnu voir d’un bon œil de recevoir l’hommage d’un tel homme pour devenir maréchal. »

Baudoin comprenait que cette réunion avait été bien préparée. Il appréciait beaucoup Guillaume et savait qu’il ferait un officier de valeur. Mais nommer un simple chevalier sans fief tel que lui, sans soutien à la Haute Cour, lui ferait s’aliéner une partie des nobles partisans des nouveaux venus arrivés avec son père. Il secoua la tête lentement.

« J’entends bien. Seulement, avec un homme de ma parentèle comme maréchal, il m’est loisible de refuser de rendre son poste à un vaillant homme comme Eudes ! Mais comment ne verrait-il pas un affront de se voir remplacer par autre que grand baron ? Ce serait le désavouer de bien méchante façon. Je n’ai nulle envie de m’aliéner une partie de la Cour, déjà fort marrie de sa perte d’influence depuis que je porte la couronne. Pour l’heure, j’ai un maréchal qui me sied, et bonne excuse pour refroidir les ardeurs batailleuses de Saint-Amand. Nous démêlerons cela si, d’aventure, nous avions grande chevauchée à organiser. »

Il se leva pour signifier leur congé aux deux hommes. Il avait déjà pas mal de soucis à la perspective de devoir naviguer auprès du puissant Manuel Comnène dans les mois qui allaient venir. Il ne tenait pas à partir avec l’idée que le royaume se déchirerait en son absence. Il était conscient des carences de son administration militaire en l’état actuel des choses, mais il avait d’autres priorités.

## Antioche, porte Saint-Georges, midi du mercredi 27 mai 1159

Le souffle court et le visage empourpré, Philippe de Milly jaillit de l’escalier avec enthousiasme. Sa mince silhouette était accentuée par un haubert empoussiéré et il arborait son épée au côté. Avec la chaleur, il n’avait pas noué la coiffe et les boucles de sa chevelure brune dansaient en couronne. Son imposante barbe se déployait sur sa poitrine, camouflant la petite croix reliquaire qui ne le quittait jamais.

Sur la plateforme, abrité sous un auvent de toile, le connétable finissait son repas en dégustant des fruits au vin tout en admirant la vue depuis les murailles qui s’élançaient à l’assaut des reliefs, vers le nord.

« Sire connétable, toujours installé en la plus haute tour ! »

Onfroy sourit en entendant la voix. Philippe de Milly, son cadet de quelques années, conservait avec les ans son inlassable énergie. Plus jeunes, il s’étaient défiés mainte fois sur la lice. L’un comme l’autre, ils appréciaient ceux capables de rompre le frêne avec adresse. Le courage physique unissait ces hommes d’action.

« N’est-ce pas le propre du baron que de chercher à s’élever ? se moqua-t-il. Venez donc goûter ces friandises, elles effaceront bien vite le goût de la poussière en votre gosier.

— Ce sera avec grand plaisir ! Je suis en selle depuis potron-minet, dans l’espoir de vous encontrer.

— J’ai espoir que vous ne portez nulle mauvaise nouvelle ! La blessure du roi se remet-elle bien ?

— De certes ! N’ayez nulle crainte à ce sujet. Les meilleurs physiciens grecs veillent sur lui. »

Le seigneur de Naplouse se laissa lourdement tomber sur un coffre ferré et entreprit de dénouer son baudrier, qu’il portait à l’ancienne.

« J’ai quelques petites choses à faire en la cité de saint Pierre^[On estimait alors que saint Pierre avait été le premier évêque de la ville.] et, surtout, je voulais vous faire part de quelques réflexions qui pourraient soulager le roi.

— Je ne vous savais pas savant en médecine, mon ami.

— Et encore, vous ne savez pas tout ! » s’amusa Philippe.

Il avala une longue rasade de vin avant de déchirer un des fruits qu’il engloutit sans y prêter garde.

« Vous avez appris qu’au chevet du roi alternent Saint-Amand et Courtenay ? Le premier pour retrouver l’office que dédaigne le second. »

Il continua sans se formaliser du soupir éloquent d’Onfroy.

« Nombreux sont ceux qui verraient d’un bon œil l’arrivée de Guillaume de Cafran, mais c’est là affaire délicate. »

Il s’assit sur le bord du coffre, se pencha vers son interlocuteur.

« Il suffirait pour cela d’offrir plus bel os à ronger à Saint-Amand. Un rôle prestigieux, qu’il ne saurait refuser. Mais où son impétuosité se verrait bridée.

— Auriez-vous idée d’en faire un évêque ? gloussa le connétable.

— Presque. Depuis que le sire roi Baudoin a démis Rohard, nul n’est plus châtelain et vicomte de Jérusalem. Les deux offices sont bien distincts. Quelle marque d’honneur et de confiance que de regrouper de nouveau cela en faveur d’un homme. »

Il sourit à son idée, puis vida son verre d’un train. Onfroy nota avec désagrément que le chevalier ne semblait jamais accorder le moindre intérêt à ce qu’il avalait, fût-ce les mets les plus raffinés. Il prit le temps de déguster une gorgée, se rassurant sur la qualité de sa boisson.

« L’idée est habile, je le concède. Seulement, nous avons hommes de valeur en ces postes. Nous ne pouvons les démettre comme cela…

— J’en suis bien d’accord. Le vicomte Arnulf est féal et de valeur. Il pourrait devenir châtelain de Blanchegarde, mon sire Amaury a besoin d’une personne de fiance là-bas.

— Et Tolenth ? Il n’a guère dérogé…

— De là viennent toutes ces idées. Nous pourrions faire d’une mauvaise nouvelle une bonne. Avec le décès de Bernard Vacher, le roi n’a plus de porte-bannière officiel. Que voilà heureuse opportunité pour un Normand sans appui comme Tolenth ! »

Onfroy de Toron hocha la tête. Il avait accueilli la nouvelle du décès de Bernard Vacher avec beaucoup d’affliction. Le vieux chevalier était un des rares avec qui il s’amusait à parler arabe. Sans l’avoir exprimé très souvent, il partageait fréquemment les analyses diplomatiques et stratégiques de Vacher. Bien que jamais cela n’ait amoindri sa vaillance lors des combats, il n’avait jamais manifesté le dédain et la violence de certains envers leurs adversaires. S’il pouvait sortir quelque avantage de cette disparition inattendue, cela lui mettrait un peu de baume au cœur.

« Parfois, je me demande lequel de votre fratrie est le plus habile à dénouer pareils imbroglios, mon ami.

— J’ai grande amitié pour le sire comte de Jaffa, chacun le sait. Et je dois avouer que je retrouve en Baudoin quelques traits de Mélisende, à qui je conserve loyauté. J’ai donc espoir qu’il finira par apprécier mon dévouement. »

Onfroy leur versa un peu de vin à tous les deux. Il aurait fallu plus d’hommes de la trempe du seigneur de Naplouse, du genre à apporter des solutions plutôt que des problèmes. Il leva son verre en un toast.

« L’idée m’agrée fort. Essayons de la soumettre à notre sire roi Baudoin. » ❧

## Notes

Même dans une monarchie comme à Jérusalem, le pouvoir se tissait d’une conjonction de courants hétérogènes, voire antagonistes, que les souverains s’efforçaient de diriger dans le sens de leurs aspirations. La terre sainte vit par exemple apparaître de profondes différences entre ceux dont les familles étaient arrivées en début de siècle et ceux qui débarquaient au fil du temps. Le roi Foulque, en particulier, eut tendance à faire venir avec lui du pays angevin et de ses environs un certain nombre de fidèles à qui il distribua les honneurs, au détriment des nobles locaux, d’origine normande en particulier avec qui il avait un contentieux depuis toujours. Par la suite, le conflit entre Foulque et la reine Mélisende puis entre celle-ci et son fils Baudoin alimentèrent les dissensions et les fractures.

Le fait que de vastes territoires s’offraient à la conquête aiguisait fortement les appétits. En outre, le roi eut sans cesse à composer avec les barons, ceux qui formaient la Haute Cour, les feudataires les plus prestigieux, généralement grands propriétaires (sans même parler des différentes puissances ecclésiastiques et des ordres religieux militaires). De nombreuses initiatives furent prises au fil du temps pour tenter d’appuyer le pouvoir royal directement sur les seigneurs locaux, de façon à s’affranchir du niveau intermédiaire de ces princes. Mais, comme la fin du premier royaume le démontrera largement, ce fut avec un succès extrêmement relatif.

Le destin de tous les officiers de la couronne est souvent très difficile à connaître et seules des traces diffuses en marquent parfois l’existence. Ce n’est pourtant qu’en s’attachant au parcours de ces personnages de second plan qu’on arrive à démêler l’extraordinaire richesse politique de la période médiévale. Des historiens précis et méthodiques parviennent malgré tout à en esquisser une image féconde à la suite d’une étude serrée de sources aussi arides que de simples chartes de donation. Pour le XIIe siècle des croisades, le travail de Hans Eberhard Mayer est, sur ce point, exemplaire.

Les lecteurs m’excuseront du jeu de mots du titre, faisant référence bien évidemment à *Vingt mille lieues sous les mers* et la devise du capitaine Nemo. Contrairement à ce dernier, les personnages du cercle que je dépeins ici cherchent avant tout à se faire un nom. Il n’en fallait pas plus pour que l’idée de la formule germe dans mon esprit.

## Références

Barber Malcolm, « The career of Philip of Nablus in the kingdom of Jerusalem », dans Peter Edbury et Jonathan Phillips, éd., *The Experience of Crusading*, vol 2 : Defining the Crusader Kingdom, Cambridge University Press, 2003, p. 60-75.

La Monte John, « The viscounts of Naplouse in the twelfth century », dans *Syria*, 1938, vol.19, no.3, p. 272-278.

Mayer Hans Eberhard, "Angevins versus Normans: The New Men of King Fulk of Jerusalem", dans *Proceedings of the American Philosophical Society*, vol.133, n°1, Mars 1989, p. 1-25.

Mayer Hans Eberhard, « The Wheel of Fortune: Seignorial Vicissitudes under Kings Fulk and Baldwin III of Jerusalem » dans *Speculum*, vol.65, no. 4, 1990, p.860-877.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem » dans *Dumbarton Oaks*, vol.26, 1972, p.93-182.

Prawer Joshua, *Crusader Institutions*, Oxford University Press, Oxford, New York : 1980.

Rey Édouard Gabriel, *Les familles d’Outre Mer de du Cange*, New-York, Burst-Franklin, 1869.
