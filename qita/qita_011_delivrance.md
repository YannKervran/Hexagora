---
date: 2012-10-15
abstract: 1155. Découvrez la fin du voyage de certains marcheurs de Dieu, de ceux qui peuplent la cité sainte dans Les Pâques de sang. Le voyage outremer, pour rejoindre les lieux saints du christianisme, était empli de dangers. Et même lorsque des trêves étaient conclues entre les puissances qui s’y affrontaient, aucune garantie n’existait pour les pèlerins d’arriver à bon port.Les actes de piraterie n’étaient l’apanage d’aucun et se confondaient avec les affrontements pour des raisons politiques et/ou religieuses.
characters: Adequin le Foulon, Ahmed, Amalric, Asceline, Giovanni, Ja’mal Abd al’Aziz, Maciot Point l’Asne, Nirart de Carville, Oudinnet, Phelipote
---

# Délivrance

## Large des côtes de Terre sainte, matin du dimanche 16 octobre 1155

Le jeune homme regardait la voile du *Peregrina* battre avec rage, bousculée par un souffle fougueux. Pourtant les matelots chargés d’en retendre les attaches ne semblaient guère s’en émouvoir. Ils étaient tournés vers l’ouest, les yeux écarquillés sur une minuscule tache signalée par la vigie. Autour de Giovanni, une foule s’était amassée à tribord, dans l’espoir de pouvoir contredire l’annonce. Une petite galère rapide, qui n’arborait clairement aucun signe chrétien s’approchait à grande vitesse. Un vaisseau hostile les avait pris en chasse.

Le jeune marin se mordait la lèvre, plissant les paupières sans parvenir à effacer l’inquiétante vision. Ce fut la voix de Nirart, un des pèlerins français, qui le fit émerger de sa torpeur.

« Avons-nous chance de trouver havre sauf avant qu’il n’arrive sur nous ? »

Frappé de mutisme, le matelot fronçait le nez de désagrément. Ses paupières fatiguées se baissèrent lorsqu’il finit par répondre.

« Je crains bien que non. Avec ce vent arrière en rafale, nous ne sommes guère aidés. La galée arrive par le travers, à force rames. Elle est donc moins gênée par les vagues. »

Après un long moment, le voyageur secoua la tête, la voix tremblante :

« Et ne pouvons-nous pas voguer au plus près des côtes ? Nul soutien ne pourrait en venir ? Il s’agit là tout de même du puissant royaume chrétien de Jérusalem… »

La moue éloquente en guise de réponse lui compressa la poitrine, lui arracha le souffle. Sa femme l’accompagnait, ainsi que tous ses amis du village. Partis de France, ils avaient cousu sur leurs vêtements la croix des pèlerins avec la joie dans leur cœur, impatients de s’agenouiller auprès du tombeau du Christ.

À quelques encablures de la Terre promise, à une poignée de jours de voyage de leur destination, après tous ces sacrifices sur la route, ils voyaient leurs espoirs anéantis, emportés dans un tourbillon de violence et de sang. Ils avaient toujours su que le risque existait, mais avaient préféré le négliger. Leurs âmes simples ne pouvaient envisager si atroce réalité.

Oubliant Giovanni, Nirart fixa son épouse Phelipote, pelotonnée contre lui, cherchant un bien maigre réconfort dans ses bras. Tournant la tête pour baiser tendrement son front, il s’aperçut alors que l’agitation gagnait l’équipage. Sur le château arrière, le patronus lançait ses ordres, visiblement décidé à ne pas abdiquer sans se battre. Il semblait croire son vaisseau assez rapide pour semer le poursuivant. En un instant, les haubans étaient emplis de mains affairées, les voiles assurées par des gestes précis.

Bien que l’angoisse soit présente, quasi palpable, il ne subsistait aucun doute dans les regards, dans les voix des marins aguerris. Leur seul espoir résidait dans la fuite. Ce n’était pas les quelques armes désuètes ni les trois soldats du bord qui pourraient les protéger de la furie de leurs assaillants. Le salut se trouvait dans leur adresse à contrôler le Peregrina avec talent. Un labeur qu’ils connaissaient.

Inutiles sur le pont, on repoussa les pèlerins dans la cale nauséabonde d’où ils avaient jailli aux cris de la vigie. Mais avant cela, sans ménagement, on sépara les hommes des femmes et des enfants. Lorsque Nirart demanda pour quelle raison, on le mena sans plus d’explication auprès d’un tonneau d’où l’on extrait à son intention une épée, tout en lui passant une targe[^targe] fatiguée, amenée depuis le château avant.

« Je croyais qu’il y avait espoir de les semer… N’est-ce pas ce que vous croyez ? » glissa-t-il.

Le matelot au rude visage tanné qui venait de lui donner le bouclier inspira lentement, entre angoisse et exaspération.

« Espoir, de certes. Mais guère plus… »

Puis il tapa sur l’épaule du pèlerin en un geste qui se voulait réconfortant, tout en faisant signe au suivant de s’avancer. Tandis qu’on le menait sur un côté avec les autres hommes équipés, hagard parmi une foule de stupéfaits, Nirart lança un regard vers la trappe qui descendait vers les entrailles du navire. Voyant qu’on en clouait l’accès, il en eut le cœur brisé : il n’avait même pas eu le temps d’embrasser son épouse comme il l’aurait souhaité.

## À bord de la galée égyptienne, fin de matinée

Ja’mal Abd al’Aziz triait avec soin les flèches qu’il allait décocher dès que leur navire serait assez proche du bateau franc. Il était particulièrement adroit pour lâcher des traits visant à trancher les cordages, déchirer les voilures. Il n’était donc pas de ceux qui s’épuisaient présentement à souquer pour intercepter le vaisseau ennemi.

À bord ne se trouvaient que des soldats, qui se faisaient rameurs à l’occasion, le temps de s’emparer de leur proie. Et parmi eux, Ja’mal n’était pas un novice, ayant appartenu à la flotte d’Ascalon, avant que la ville ne tombe aux mains des chrétiens. Là, il avait pris part à de fréquentes batailles. Bon archer, il était toujours resté en retrait des affrontements, autant qu’il le pouvait.

Une méchante blessure suite à sa rencontre avec l’arme d’un lourd cavalier de Jérusalem lui avait valu d’osciller entre la vie et la mort durant de longues semaines. Aujourd’hui encore, des années après, il claudiquait et ressentait la douleur dans ses hanches de temps à autre. Ses compagnons le brocardaient parfois, mais lui conservaient néanmoins du respect.

C’était d’ailleurs bien la seule chose qu’il avait pu conserver. Malgré le butin amassé, les gages corrects qu’il percevait en se mettant parfois au service de chefs de guerre turcs, il n’avait jamais réussi à épargner le moindre dinar. L’argent lui glissait entre les doigts comme du sable. Il savait pourtant que le temps n’était guère clément pour les soldats. Lorsque certains émirs avaient décidé d’armer des vaisseaux pour attaquer les côtes franques, il avait tout de suite voulu se joindre à l’expédition. En dépit de l’opposition du vizir, ibn Ruzzîk, qui préférait traiter avec l’ennemi, une escadre avait pris la mer.

Ja’mal était à bord d’une des galées, fin vaisseau conçu pour la course et les coups de main rapides. D’autres, plus ventrus, avaient été emplis d’hommes parlant la langue des Francs, habillés comme eux, pour aller chercher des informations et s’emparer par la ruse de ce qu’ils pourraient saisir.

Ja’mal était un soldat, un soldat usé. Il fallait absolument que cette dernière campagne lui rapporte. Avec assez de biens, il pourrait s’offrir un mariage satisfaisant. Peut-être pas avec la plus belle ou la plus talentueuse des épouses, mais au moins avec une qui le soignerait sur ses vieux jours. Et, qui sait ? qui pourrait lui donner quelques enfants.

Il était assis à côté d’Ahmed, pour qui c’était la première saison. Un blanc-bec jouant les fiers à bras, mais qui semblait présentement ne pas en mener large. Ja’mal en avait vu beaucoup comme ce garçon, pleins d’entrain tant que l’épreuve n’était pas encore là. Bien peu la réussissaient correctement, sans y laisser tout ou partie de leur âme, de leur corps ou de leur vie. Pour l’heure, le jeune homme ramait avec application, tentant de jeter un coup d’œil à la dérobée, afin d’estimer la distance demeurant à parcourir. Interceptant son regard, Ahmed le dévisagea un instant. Le vieux soldat lui sourit avec tout ce qui lui restait de dents et leva les sourcils, l’air faussement amusé.

« Tu as grande chance pour ton premier assaut, c’est nef de voyageurs. Ils sont moins prêts à en découdre que les marchands qui tiennent à leurs ballots. Nous allons moissonner comme gais laboureurs… »

Amusé par l’idée, le jeune homme dévoila à son tour des dents, régulières celles-là, mais une sourde appréhension demeurait dans ses prunelles. Craignait-il la blessure, la mort ou la rencontre avec lui-même ? Il ne le savait guère.

## Cale du Peregrina, vers midi

Asceline et Phelipote, l’épouse de Nirart, se serraient l’une contre l’autre, appuyées contre une des cloisons de la cale. Les ténèbres avaient envahi la zone, combattues par une poignée de lampes qui n’apportaient guère de lumière, et aucune chaleur. Le tumulte qui grondait au-dessus d’elles empêchait d’entendre les pleurs, les gémissements de celles et ceux qui, comme elles, étaient condamnés à attendre.

Les chocs résonnant contre le plafond de chêne faisaient sursauter les petits groupes, frissonner les plus vaillants et arrachait un sanglot de désespoir aux plus inquiets. Le cliquetis des armes était encore plus sinistre, tout comme les cris, en langue familière ou inconnue, et les râles porteurs de sourde angoisse. Asceline se tourna vers son amie et le garçonnet qui se trouvaient à côté d’elle, les incitant à répéter la prière à la Vierge, généralement prompte à secourir les nécessiteux. Mais le grondement déchirant qui précéda un fracas retentissant au-dessus de leurs têtes, résonnant dans tous les bois du navire, les fit ânonner toujours moins fort, à demi implorants, ajoutant leurs larmes à l’offrande gémissante de leurs voix.

Une des femmes se leva soudain, hurlant comme une démente à l’intention du plafond :

« Que Dieu et tous les Saints fassent mettre à glaive tous ces maudits ! »

Puis, prenant à témoin la masse humaine agglutinée dans un recoin empli d’ombre, elle vociféra :

« Christ en soit témoin, pas un de ces souillards ne posera sa main sur moi, plutôt me navrer au parmi du cœur ! »

Ajoutant le geste à la phrase, elle brandissait un petit couteau, certainement destiné à la cuisine.

« Mais il leur faudra m’approcher d’abord et je jure sur les Saintes Évangiles que je jouterai bataille avant cela ! J’en escouillerai quelques poignées… »

Puis, comme hallucinée, elle déambula dans la cale, brinquebalée par les cahots du navire tandis qu’elle frappait d’imaginaires assaillants.

Au-dessus d’elle, toujours résonnait le vacarme des soldats, des matelots et des voyageurs bataillant sur le pont. Après avoir laissé échapper un gémissement, Phelipote trouva entre deux hoquets de pleurs la force de murmurer à Asceline :

« Allons-nous donc finir ainsi ?

— Allons, bonne amie, garde ta foi en Christ. Il nous a menés ici, puisse-t-il nous sauvegarder encore… »

Sa compagne garda le silence un long moment, reniflant de temps à autre, les yeux noyés de larmes.

« Je crains pour mon époux. Le pauvre n’a jamais été bien vaillant. Le plus loyal des compagnons, de certes, mais guère habitué à…

— Il n’est pas seul. Mon frère Amalric, mon époux ou Point-l’Asne sont à ses flancs. Aucun ne laissera l’autre en arrière. »

Phelipote hocha la tête lentement, incapable de parler, la gorge nouée. Au-dessus d’elles, le vacarme prospérait, bruits de corps s’écrasant, de pas affolés, précipités, de ferraille cliquetante, de bois se brisant. Par-dessus le tout se répandaient des cris… de terreur, de rage, de haine, de désespoir, de douleur, aucun ne manquait à l’appel. La crainte de reconnaître l’une ou l’autre de ces voix était la plus épouvantable pour les infortunés enfermés dans les tréfonds du navire.

## Pont du Peregrina, midi

Abrité par un amas de tonneaux désormais brisés et de cordages rompus, Giovanni se traînait sur le côté. De la main gauche, il se tenait le flanc, sans y avoir même jeté un regard, dans un ultime espoir de nier la réalité. Chaque mouvement lui extorquait un cri ou un gémissement de douleur.

Non loin, près du château arrière, un groupe de matelots et de pèlerins résistait, empêchant les musulmans d’avancer et de déloger les archers embusqués à l’étage. Une fois appuyé, il risqua un œil vers l’avant du navire. Plusieurs cadavres et moribonds jonchaient le sol, dont plusieurs assaillants, ce qui arracha un sourire sans joie au marin. Des combats isolés continuaient, avec fureur, empêtrés dans la grand-voile qui s’était abattue avec fracas lorsque les haubans en furent coupés. Le mât avait brisé un des plats-bords et gisait en partie coincé dans ce qui restait de la chaloupe, oscillant au gré des vagues dans lesquelles son extrémité reposait, emmêlé dans la toile.

Giovanni roula sur lui-même et inspira avant de se résoudre à examiner son ventre, levant la main pour la découvrir maculée de sang, de son sang. À la vue de ses entrailles béant au-dehors, il eut un haut-le-cœur et sa bouche s’emplit d’une saveur saline et métallique. Il manqua défaillir et, des yeux, chercha du réconfort vers ses compagnons, à quelques mètres de là. Ils tentaient de se maintenir à l’abri des tireurs ennemis, derrière leurs targes. Son regard croisa celui de Nirart, encore debout, son pavois devant lui, le visage décomposé. L’effroi que Giovanni surprit quand le pèlerin l’aperçut lui fit lâcher un soupir accablé qui se mua en grognement de rage.

À l’avant du navire, plusieurs groupes s’affrontaient autour de la terrasse. Ahmed était parmi eux, son épée à la main, tenant ferme son bouclier. Indécis, il frappait de temps à autre par-dessus la ligne, sans grand espoir d’atteindre les hommes qui s’étaient retranchés contre un des escaliers d’accès à l’étage. Les forces étant à peu près égales de part et d’autre, une sorte de statu quo s’était instauré depuis un moment, aucun d’eux n’osant prendre l’initiative d’un assaut. Les coups d’estoc menés à la lance étaient plus symboliques qu’efficaces, et personne n’avait encore tenté de forcer la situation.

Conscient qu’il n’était guère utile à cet endroit, Ahmed se tourna, cherchant du regard un affrontement où il pourrait employer sa lame. Il n’avait guère envie qu’on le traite ensuite de poltron. Nul ne saurait jamais la boule d’angoisse qui lui taraudait la poitrine, lui coupait le souffle, lui asséchait la bouche. Faisant chemin vers l’arrière parmi le pont empli de décombres, il enjambait les plis de la voile avec précaution, veillant à ne pas glisser dans les flaques de sang, les déjections immondes, les chairs arrachées. Il s’efforçait de ne pas prêter attention aux pénibles affrontements alentour, avançait courbé pour ne pas s’offrir à d’éventuels tirs ennemis. Agenouillé, il guettait le moment où s’élancer vers le mât principal, où il serait de nouveau à l’abri, quand il entendit un bruit sur sa droite, accompagné d’un mouvement furtif.

Brandissant son épée, il se tourna, l’air aussi déterminé et résolu que possible. Mais il ne découvrit qu’un moribond rampant, dans une mare de sang mêlé d’eau de mer, une jambe à demi tranchée et des tronçons de flèches dépassant de ses flancs. Dans un sursaut bien inutile, il tentait d’avancer. Vers quelle destination se demanda le jeune musulman. Le regard était aveugle et la bouche emplie de caillots ne pouvait plus parler. Ahmed réprima avec peine un frisson de terreur. Lorsqu’il se tourna, l’homme gisait affalé, la vie s’échappant de son corps désormais à gros bouillons.

Ahmed finit par rejoindre ses compagnons, qui progressaient peu à peu vers l’arrière, protégés de leurs épaisses targes. Ja’mal était parmi eux, indiquant d’une voix ferme mais discrète ce qu’il convenait de faire. Il fallait à tout prix demeurer unis, avancer très doucement, n’offrir aucune cible au dernier archer ennemi. Une fois qu’il serait tombé, ils pourraient s’élancer sur le groupe de Francs. Ja’mal dévisagea Ahmed et lui fit signe de se baisser et de les rejoindre. Là il donna ses ordres, le visage fermé, les traits inquiets.

« Dès que nous n’aurons plus à craindre les traits ennemis, je crierai ’Allah est grand’. Alors, vous avancerez comme les démons que vous êtes et bousculerez les idolâtres de vos boucliers. Ceux armés d’une lance doivent aider mais ceux qui n’ont ni l’un ni l’autre doivent demeurer en arrière. Est-ce bien compris ? »

N’obtenant qu’un murmure d’approbation, il s’empourpra de colère et gifla Ahmed, qui était désormais le plus près.

« Je n’ai rien entendu ! »

Choqués par le geste, les hommes rugirent avec plus de détermination, ce qui sembla satisfaire le vieux soldat, le visage barré d’une grimace hideuse. Lorsqu’après un long moment, le cri « Allah est grand » retentit, la pression accumulée jusque-là explosa en une fureur haineuse. La ligne de boucliers adverses fut enfoncée avec vigueur, les combattants tombèrent, enchevêtrés, mêlés. On bousculait plus qu’on ne se battait, les armes souvent entravées, l’acier fouaillant les corps sans discernement.

Ahmed s’était élancé à son tour, levant son épée dans un geste de défi, hurlant comme un animal tout l’effroi et la colère qui l’avaient envahi. Mais il ne trouva aucune chair à trancher, aucun ennemi à affronter. Nul endroit où sa lame pourrait s’abattre : assortiment de coups ahanant, d’empoignades féroces, de pugilats tourmentés. Affolé, il cherchait en tout sens l’éventuel adversaire sournoisement tapi à portée de bras. Sentant sur sa nuque le regard attentif du vieux Ja’mal, il savait qu’en ce jour, on le jugerait à la hauteur de ses actes.

Bondissant sur un amas de débris, il débusqua un Franc oublié par ses compagnons. L’homme haletait, hagard, larmoyant, les mains emplies de ses propres entrailles, incapable de réaliser ce qui lui arrivait. De sa bouche s’écoulait plus de sang que de salive, maculant son torse et se répandant sur son ventre béant. Ses yeux hallucinés, exorbités exprimaient toute la terreur qui ne pouvait trouver son chemin au travers de la gorge gargouillante. Lentement, il leva la tête vers le soldat ennemi, la lèvre pendante tel un dément.

Dans un souffle la lame s’abattit rageusement, couvrant d’un ultime voile le regard du jeune homme. ❧

## Notes

Malgré les fréquentes trêves et les relations diplomatiques, la situation de guère quasi-permanente en Terre sainte et dans les régions environnantes offrait de nombreuses occasions à des guerriers déterminés d’acquérir biens et pouvoirs. Aussi, lorsque le pouvoir égyptien conclut un accord avec le royaume de Jérusalem quelques années après qu’il eût perdu la ville d’Ascalon, de nombreux émirs ne l’entendirent pas ainsi et lancèrent des raids navals sur les côtes levantines.

Les pèlerins n’étaient pas les seules cibles, et bien souvent les opérations militaires n’étaient envisagées qu’en fonction du gain qu’elles pouvaient générer. Malgré tout, la valeur des esclaves était suffisante pour que la capture d’un navire de croyants adverses permette de s’enrichir tout en accomplissant des hauts faits pour sa propre religion.

## Références

Elisséef Nikita, *Nūr ad-Dīn Un grand prince musulman de Syrie au temps des Croisades*, Damas : Institut Français de Damas, 1967.

Gravelle Yves, *Le Problème Des Prisonniers De Guerre Pendant Les Croisades Orientales (1095-1192)*, Mémoire de Maîtrise, Université de Sherbrooke, 1999.

Prawer Joshua, *Histoire du Royaume latin de Jérusalem*, 2nde édition, Paris : CNRS éditions, 2001,2007.
