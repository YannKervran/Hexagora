---
date: 2016-10-15
abstract: 1157-1387. Passant de main en main, au travers des siècles, un objet acquiert différents statuts selon les désirs et projections qu’il suscite chez ses propriétaires. Jusqu’à être perdu ?
characters: Ernaut, Droart, Saïd, Guiote, le Peyre, Audeta, Alban, Géraut
---

# Petits dieux

## Jérusalem, après-midi du mardi 27 août 1157

Allongé sur sa paillasse, Ernaut se prélassait tranquillement à l’abri d’un auvent de feuilles de palme, un pichet de vin frais et quelques rissoles à portée de main. Sa nuit de garde avait été épuisante, avec plusieurs soucis dus à des rixes entre voyageurs. Il avait donc décidé de faire une sieste avant de s’occuper de quelques tâches domestiques. D’autant qu’il espérait croiser le vieux Saïd, son voisin, pour lui demander de l’aide.

Droart avait invité tous ses collègues à profiter de sa nouvelle demeure pour le souper et la veillée. Il venait de s’installer dans une jolie maison vers Sainte-Marie-Madeleine, dans l’angle nord-est de la cité. Il y avait acheté plusieurs bâtiments, dont certains en ruine, mais cela constituait un bel ensemble et lui offrirait l’opportunité d’avoir un jardin et, pourquoi pas, des locataires, particuliers ou boutiques, une fois les édifices restaurés. Ernaut et quelques autres avaient passé une partie de la dernière quinzaine à transporter les affaires de la famille et l’ultime chargement avait été déposé quelques jours auparavant. Il était donc désormais bien temps d’arroser ça dignement.

Ernaut avait prévu d’apporter un peu de vin, histoire de sympathiser avec ses collègues. N’ayant prêté serment au roi que quatre mois plus tôt, il n’en connaissait qu’un faible nombre. Mais comme Droart était de ce premier cercle, il tenait également à marquer le coup avec un cadeau. Et c’est là qu’il avait besoin de l’aide de Saïd. Il n’avait aucune idée de quoi offrir ni d’où le trouver. Saïd était perpétuellement en quête de tâches, qu’il exécutait pour tous ceux qui voulaient bien de lui. Il connaissait donc parfaitement la cité et saurait indiquer les meilleurs artisans.

Lorsqu’il pointa le bout de son nez, portant une corbeille emplie de branchages, il était accompagné d’un petit chien au poil ras. Ernaut était toujours sidéré par cette capacité du vieil homme à récupérer des choses un peu partout. Sa cabane sur le toit était encombrée d’un fatras indescriptible des trésors qu’il avait accumulés avec les ans. Chaque jour semblait lui apporter un nouvel élément à entasser dans son modeste appentis.

« Je m’enjoie de te voir si tôt arriver, Saïd, j’aurais fort grand besoin de conseils de ta part. »

L’homme s’arrêta, affichant un large sourire qui le faisait ressembler à un fruit sec. Il était habillé d’un thawb[^thawb] élimé jusqu’à la trame et arborait sur le crâne un turban que peu auraient accepté en guise en serpillière. Il posa délicatement sa corbeille, puis s’approcha d’Ernaut, qui s’était accroupi en tailleur en attendant.

« Conseils pour sieste ? » rigola Saïd, gloussant comme une volaille, le regard un peu fou.

Ernaut lui tendit son pichet de vin coupé tout en secouant la tête. L’autre s’assit lourdement et s’empara de la cruche qu’il renifla, circonspect.

« J’ai désir de faire beau présent à un mien ami. Mais ne m’y entends guère en ce qui concerne les artisans ici. Saurais-tu me guider ?

— Présent pour enfant ?

— Nenni, il vient de s’installer en bel hostel. Quelque chose qui lui porte chance. »

Saïd plissa les yeux, s’envoyant une large rasade de vin pour améliorer sa réflexion. Il gratta sa barbe mitée et hocha la tête plusieurs fois pour lui-même. Ernaut allait lui demander ce à quoi il pensait lorsqu’il dressa un index impérieux avant de se lever. Puis il se dirigea vers son logis, en déverrouilla la porte et y disparut.

Habitué aux facéties du vieillard un peu dérangé, Ernaut ne s’en offusqua pas. Il se tourna vers le corniaud, désormais allongé entre soleil et ombre, et le flatta doucement. Un bruit de cavalcade résonna entre des murs non loin, des cris d’enfants se propagèrent et s’évanouirent. Il entendit plusieurs femmes entre lesquelles le ton monta bien vite sans qu’il soit possible de comprendre ce qui se disait, les voix se répercutant dans les rues et les courettes. Puis le calme revint, un léger vent fit ronfler et frémir sa hutte de feuillage. Il engloutit quelques beignets, dont il accorda la dîme au chien qui n’en espérait pas tant, et obtint en retour des coups de langue gluants.

C’est alors que Saïd ressortit la tête de son antre, les bras chargés d’un paquet de toile. Aussi excité qu’un enfant préparant une bêtise, il posa le tout devant Ernaut, avant de s’asseoir à son tour, et de plonger la main vers les pâtisseries.

« Qu’est-ce donc, l’ami ?

— Cadeau pour ami toi. »

Tout en répondant, il poussa vers Ernaut le petit ballot. Celui-ci hésita un peu puis démaillota l’objet qui s’y cachait : une statuette de bronze, un personnage à l’allure étrange, corps humain et tête d’éléphant, en une pose gracile, une main levée en guise de salut, et l’autre tenant un genre d’encensoir.

« J’ai jamais rien vu de tel… Où donc as-tu trouvé ça ! Est-ce diablerie de mahométan ? »

Le vieil homme secoua la tête avec véhémence avant de stopper brutalement et de lever les épaules, indifférent.

« Bonne chance, venue par-delà les mers, loin ici, vers Levant. Pays des épices… Bon pour ton ami.

— Tu veux dire que c’est un porte-bonheur ? »

Détaillant la statuette élégante, Ernaut ne savait que penser. Il trouvait l’objet plutôt joli, mais il ne voulait pas commettre un impair en offrant une idole païenne à un ami récent.

« Tu es acertainé que ça lui plaira ? Combien en veux-tu ? »

Saïd fronça les sourcils, ôtant de sa bouche le morceau de beignet qu’il était en train de déchiqueter, comme vexé par la question.

« Cadeau pour ami, pas vendre.

— Je ne peux pas te prendre ça sans te payer, voyons. C’est mon ami, pas le tien.

— Toi ami Saïd, donc moi donner cadeau toi. Et toi offrir à ami tien. Porter chance à tous, ainsi. »

Ernaut secoua la tête, ennuyé par la générosité inattendue du vieil homme. Il le savait fort désargenté, et la vente de la statuette pourrait lui permettre d’améliorer l’ordinaire. S’il avait d’autres trésors de ce type dans sa cabane, il n’était pas étonnant qu’il la verrouillât si consciencieusement chaque fois qu’il s’absentait.

Ernaut ne pouvait accepter aussi facilement de le déposséder, d’autant qu’il était lui-même assez aisé depuis qu’il avait rejoint la sergenterie du roi.

« J’accepte, mais en ce cas, il faudra m’en dire plus, et le faire autour d’un bon repas. Qu’en dis-tu ? »

Saïd hocha la tête et ricana, se tournant vers le chien pour le prendre à témoin :

« J’ai dit vrai, déjà la chance m’offre repas ! »

Ernaut s’esclaffa, désarçonné par l’aplomb du vieil homme. Celui-ci lui fit un clin d’œil et ajouta, tout en attrapant un autre beignet :

« Un jour, je conter toi toute l’histoire de cette statue. Puis il fit un large sourire : belle histoire ! »

Puis il goba la douceur sucrée dans un gloussement.

## Jérusalem, soirée du vendredi 27 juin 1158

Lorsqu’il était à son logis le soir, Droart aimait à aller voir ses enfants pour le coucher, surtout Guiote. Il avait une tendresse particulière pour son aînée, à qui il passait tous les caprices au grand dam de sa mère. Depuis qu’ils étaient dans la vaste demeure, les petits avaient leur propre chambre, avec un lit pour les garçons et un autre pour les filles. Ils y entassaient leurs jouets, leurs trésors dans des sacs et de vieilles malles récupérées.

Droart tenait à la main une statuette de bronze lorsqu’il s’assit au pied de la couche de Guiote et de ses sœurs, déjà endormies. Il la brandit sous le nez de l’enfant, en lui prêtant les borborygmes qu’il faisait.

« Te dirait-il d’avoir cette belle figure, ma douce ? »

La petite découvrait vraisemblablement l’objet pour la première fois, mais la perspective de la faire sienne lui conférait toutes les qualités. Elle s’exclama donc avec enthousiasme, les yeux emplis de joie :

« C’est ma préférée ! Je l’aime beaucoup.

— Vois-tu, je l’aime beaucoup aussi, mais ta mère ne la porte guère en son cœur, alors il me plairait que tu la gardes par-devers toi.

— Pour de vrai ? Tu me la donnes ?

— Un jour, je te conterai sa belle histoire.

— Une histoire merveilleuse, avec belles pucelles et fiers damoiseaux ? »

Droart acquiesça, un peu déchiré à l’idée de se défaire d’un cadeau auquel il tenait. Mais il ne serait guère loin et confié à quelqu’un de cher. Il ressentait tout de même un petit pincement au moment de l’abandonner, bien qu’il sache que c’était le seul moyen d’avoir la paix en son logis. Depuis qu’il l’avait reçu de la part d’Ernaut, sa femme n’avait eu de cesse de lui reprocher de posséder un tel objet impie. Elle était persuadée qu’il s’agissait d’une magie païenne, infidèle ou juive, et que cela attirerait le malheur sur leur maison de bons chrétiens.

« Par contre, c’est un secret entre nous deux, qu’il ne faut révéler à personne, d’accord ?

— Ne pourrais-je le montrer à Sylvette et Houdart ?

— Nenni, juste entre toi et moi.

— Notre secret à nous ? »

Le sergent hocha la tête, attendri par la bouille innocente de son enfant. Elle lui fit un sourire en partie édenté et s’accrocha à lui avec chaleur. Il en conçut une bouffée de bonheur, émerveillé de voir qu’en effet, cette statue lui portait de nouveau chance.

## Colteja[^cotteughes], nuit du mercredi 5 juin 1387

Les silhouettes à peine discernables dans la noirceur de la nuit se pelotonnaient contre la haie, à l’une des entrées du hameau. Personne n’avait de lanterne, de crainte d’être visible. On n’entendait que des chuchotements, des murmures inquiets, le gémissement d’un tout petit.

« Audeta, où donc se cache ton époux ? Grogna une voix agacée.

— Il prenait nos dernières affaires, il s’en vient.

— Il nous faut partir au plus vite, on dit l’Aymerigot à quelques portées d’arc. Il nous faut mettre du champ entre ses routiers et nous.

— Je le sais bien, le Peyre » répliqua une voix autoritaire à l’arrière du groupe.

Celui qui avait répondu s’avançait, encombré de sacs et de ballots, un lourd bâton en main. On ne voyait que ses yeux dans les ténèbres et ils ne semblaient nullement effrayés comme ceux de ses compagnons. C’était un homme rude et pas craintif, il tenait à le faire savoir. Même, et surtout, si des soldats en maraude traînaient aux environs. Alban rejoignit sa femme et ses enfants d’un pas lent, puis murmura quelques directives d’une voix calme. Il hocha ensuite la tête à l’intention du meneur de leur petit groupe.

Ils prenaient le chemin de la forêt pour se mettre à l’abri des sanglantes troupes de routiers qui écumaient la région depuis des années. À quelques semaines des moissons, il s’avérait plus prudent de quitter sa maison, dans l’espoir que cela découragerait le pillage. Le grain serait bientôt là et c’était le plus important. Une bâtisse pouvait se réparer, mais la récolte, elle, devrait attendre une nouvelle année si on la perdait par un comportement inconsidéré. Vu qu’il était impossible de savoir comment les choses pouvaient se dérouler avec ces troupes de brigands sans foi ni loi, le mieux était de fuir. Seuls quelques-uns auraient préféré demeurer sur place et tenter de résister. Folie, estimaient les autres.

Alban était de ces réfractaires. Il avait trop sué sur son lopin et sa maison pour accepter aisément de s’en déposséder ainsi. Son logis était confortable, pour n’être pas si grand, et il était à lui. Sa famille était riche et respectée, sa voix écoutée au conseil. Devoir se cacher comme un lapin terrifié lui remuait les sangs. Il se sentait amputé de sa fierté d’homme. Mais il avait reconnu qu’il n’était pas de taille, que si même les châteaux n’arrivaient pas à résister, alors qu’auraient pu tenter de simples vilains comme lui ?

Ils en étaient là, jetés sur les chemins comme des va-nu-pieds, leurs maigres biens serrés sur eux. Alban avait pris soin de cacher leurs objets de valeur avant de s’enfuir, afin de retourner les chercher une fois que les choses se seraient tassées. Ou pour les récupérer quand ils se réinstalleraient. Audeta insistait pour qu’ils descendent plus bas, vers le Limousin, où les températures se faisaient plus clémentes. C’était là pays de peste et d’Anglois avait tempêté Alban, mais il avait fini par reconnaître que les terres y paraissaient moins pénibles.

Tout en marchant, il saisit son aîné Géraud par l’épaule et l’approcha de lui d’une main ferme :

« Gamin, tu vas bien m’écouter. Je vais te dire où j’ai celé nos avoirs en l’hostel, au cas où il m’arriverait malheur. Si c’était le cas, ce serait ton rôle de me remplacer. »

L’adolescent, habitué à son père autoritaire, hocha la tête avec sérieux, sans rien répondre.

« J’ai quelques monnaies dans le grain que je porte, mais une autre part est enterrée au pied du houx à senestre de la grange. »

Alban hésita un instant puis se racla la gorge.

« Y’a aussi autre chose : la statuette qui nous vient de nos ancêtres de par delà les mers. Y faut pas trop en parler, car le curé y verrait visage de démon. La grand-mère, elle a bien failli mal finir à cause de ça. Mais faut en prendre soin, c’est notre chance qu’y disaient les vieux. Je l’ai scellée en le logis. »

Il sourit pour lui-même de son mémento et murmura :

« Parmi les bêtes, le démon se terre à l’ombre de la croix.» ❧

## Notes

L’idée derrière ce récit, qui rejoint en partie celle tramant *Fil du temps*, est le destin des objets qui nous entourent et à partir desquels les archéologues tentent de reconstituer les sociétés disparues. Le titre, lointaine référence à Terry Pratchett, dévoile une autre des questions sous-jacentes, à savoir justement l’importance de l’univers symbolique qui s’attache à eux au sein d’un groupe, qui n’est certainement pas constant ni dans le temps ni dans l’espace. Sur ce sujet, si vous avez la chance de voir passer près de chez vous l’exposition *Futur antérieur : Trésors archéologiques du XXIe siècle après J.-C.*, n’hésitez pas à aller la visiter, elle offre un point de vue saisissant sur ces processus.

Il n’est pas une semaine sans révélation soi-disant exceptionnelle de découverte archéologique d’artefacts ayant voyagé sur des milliers de kilomètres, mis au jour en des lieux où ils sont clairement hétérogènes. Mais on connaît depuis l’époque préhistorique le destin de pierres retrouvées très loin de leur site d’extraction. Cela ne veut pas forcément dire qu’un être humain unique en ait assuré le transport, ni que les deux cultures aient été en contact direct (ni envisagé l’existence de l’autre). De plus, même si l’on dispose d’une information archéologique précise, nous ne pouvons généralement en tirer aucun enseignement quant à l’intrusion de l’objet culturel externe, qui demeure anecdotique. Voilà donc un terrain propice à un exercice d’interprétation romanesque, en dehors de toute considération scientifique.

Enfin, pour la petite histoire, ce Qit’a a servi de prétexte à un jeu de piste qui menait aux vestiges du village médiéval abandonné de [Cotteughes](http://www.auvergne-tourisme.info/patrimoine-culturel/trizac/site-archeologique-des-cases-cotteughes/tourisme-PCUAUV0150000406-1.html) dans le Cantal, où vous pouvez peut-être retrouver certaines traces du récit…

## Références

Boas Adrian J., *Domestic Settings. Sources on Domestic Architecture and Day-to-Day Activities in the Crusader States*, Leiden et Boston : Brill, 2010.

Combes Pascal, Piludu Brigitte, Vernet Gérard, *Résultats des sondages archéologiques dans les villages de Cotteughes et de Freydefont*, DRAC Auvergne, 1990.

Flutsch Laurent, *Futur antérieur : Trésors archéologiques du XXIe siècle après J.-C.*, InFolio, 2003.
