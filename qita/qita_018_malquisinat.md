---
date: 2013-05-15
abstract: 1158. L’administration politique et financière n’a pas attendu d’atteindre la complexité de nos sociétés contemporaines pour causer souci à tous. Qu’on soit homme de pouvoir, exécuteur ou assujetti, chacun a toujours eu l’impression de se voir attribué le plus lourd fardeau. Quand bien même on vivait au centre du monde, à Jérusalem, aux abords du tombeau le plus vénéré de la Chrétienté, le Paradis semblait bien loin. Les élans religieux se voyaient régulièrement battus en brèche par la pesanteur du quotidien. Un petit texte inspiré d’un document législatif, ou comment les sources les plus austères en apparence peuvent stimuler l’imagination...
characters: Aloys, André de Tosetus, Arnulf, Baset, Fouques, Margue l’Allemande, Odo de Turcame, Rohard de Tosetus
---

# Malquisinat

## Quartier du palais royal, Jérusalem, fin de matinée du lundi 3 mars 1158

Dans la rue donnant accès à une des portes du palais royal, une foule bigarrée s’avançait mollement. Le brouhaha des discussions se répercutait entre les hauts murs dominés par le clocher du Saint-Sépulcre, sous le regard curieux des pigeons et de quelques chats en maraude sur les toits. Le soleil n’avait pas encore vaincu la fraîcheur de la pierre, et dans l’ombre bienvenue, chacun y allait de son commentaire à propos de la séance de la Cour des Bourgeois. Une poignée de citadins, joyeux de s’être vus accordé ce qu’ils demandaient, parlaient fort et riaient avec ostentation de leur bonne fortune. Certains, épaules rentrées, visage morne, ruminaient leur détresse, leur colère.

D’autres, enfin, glosaient sur les décisions rendues, les informations révélées, avec compétence, amusement ou surprise indignée. Parmi ces derniers, un petit groupe d’hommes à l’allure opulente bruissait de mécontentement scandalisé. Un rouquin, le cheveu en bataille et les traits fins, le nez pareil à un bec, avait la voix acerbe :

« Ce n’est pas tant le montant que le principe ! »

Un de ses compagnons, le visage mangé de barbe drue, bien taillée, les dévisagea de ses yeux globuleux et ajouta, en hochant la tête d’un air sentencieux :

« Je vous l’avais bien dit, ce béjaune a gueule d’ogre et ne sait que gloutoyer !

— Tout de même, maître Turcame, c’est le roi ! objecta un gros homme accablé, à moitié asphyxié de parler en marchant.

— Et quoi ? Le roi n’est pas Dieu que je sache. Sa mère n’aurait jamais osé faire pareille chose. »

Le petit groupe acquiesça lentement, tout en demeurant craintif de le faire aussi près du palais de Baudoin III[^baudoinIIIb]. La vieille Mélisende[^melisende] s’était retirée à Naplouse à l’écart de la chose publique et ne comptait plus guère de soutiens au sein de la Haute Cour. Parmi les habitants de Jérusalem, nombreux étaient ceux qui la regrettaient, elle qui avait su si bien se concilier leurs bonnes grâces, ne ménageant pas sa peine pour ses chers bourgeois.

Un petit méditerranéen au cheveu noir et rare, la barbe raide, haussa les épaules.

« Je ne peux néanmoins donner tort au roi, certaines rues sont fort sales et les puanteurs en la saison d’été sont terribles.

— De cela je veux bien convenir, maître Tosetus, mais c’était affaire à entreparler en Cour avant d’en prendre décision, répliqua Odo de Turcame. Bien sûr qu’il faut curer venelles et courettes, et d’autant mieux pour les Pâques, avec l’affluence de marcheurs de Dieu. De cela tout le monde s’en accordera. Mais je n’aime guère les façons de faire de Baudoin, qui ne respecte pas nos coutumes.

— Nous devrions en discuter avec nos compères jurés et peut-être aborder l’affaire avec le sire vicomte à une prochaine assemblée plénière ? » proposa le gros homme.

L’idée ne souleva pas l’enthousiasme, mais aucun n’y objecta. Ils obliquèrent à gauche et continuèrent d’avancer, passant sans même y jeter un oeil devant l’entrée du Saint-Sépulcre, le parvis le plus vénéré de la chrétienté. Ils serpentaient au travers de la rue emplie de pèlerins, de croyants au regard exalté, mais sans guère y prêter attention. Ils parcouraient ces lieux depuis tant d’années qu’ils en étaient arrivés à en oublier le caractère merveilleux, pour l’ensemble du monde chrétien.

Alors qu’ils se faufilaient devant un marchand de cierges qui accrochait des faisceaux de chandelles sur ses panneaux, l’homme aux cheveux roux grogna :

« D’où lui est venu cette soudaine idée, d’ailleurs ? La cité ne me paraît guère plus sale qu’habituellement…

— Peut-être a-t-il eu envie d’aller faire balade vers la porte des Tanneurs, s’amusa Turcame. Les effluves en sont certes ignobles !

— Il est rentré voilà quelques jours à peine de la chevauchée au nord avec le sire Thierry[^thierryalsace], il s’est habitué à la vie sous la tente et ne supporte plus ce qui lui paraissait bien normal jusqu’alors. »

À l’évocation du nom du seigneur croisé, Odo de Turcame fit la grimace et une lueur de compréhension brilla dans ses yeux.

« Je suis acertainé que c’est là mâle influence du sire comte de Flandre !

— Et pourquoi donc ? s’interrogea André de Tosetus.

— Tous ces barons d’outremer aiment à tenir le pouvoir bien serré et ne lâchent que de mauvaise grâce la décime part de ce que nos souverains ont toujours considéré normal de nous accorder. »

Il s’arrêta, considérant d’un geste large la rue, les boutiques, le quartier et au-delà, la cité tout entière.

« Tous nos compères l’auront fort mauvaise de voir neuve taxe leur être extorquée, c’est là mauvaise coutume qui s’installe !

— La Secrète[^secrete] a toujours grand faim de monnaies et n’aime rien tant que nouveaux moyens de les faire venir à elle.

— Voilà bien ridicule moyen en tout cas. Ce n’est pas en condamnant à payer ceux qui escurent mal devant leur hostel qu’on fera venir les gens. Sans borgesies bien tenues, pas de loyer, sans commerce pas de péages… C’est compte de benêt de penser qu’il y a là bonne source. Celui qui a soufflé pareille idée au roi n’a que du vent entre les oreilles ! »

Sa fureur avait fait tourner les têtes dans sa direction et certains s’inquiétaient de ce qui pouvait ainsi passionner les jurés qui les représentaient auprès du roi. Sous peu, la ville allait bruire de nouvelles, aussi farfelues qu’imaginatives, sur ce qui avait bien pu se dire lors de la séance. Le gros homme passa une manche sur son front transpirant, invitant de la main ses compagnons à avancer de nouveau.

« Venez donc boire quelque vin de Judée à mon logis. J’en ai reçu tonnels voilà peu, et il saura apaiser la flamme de votre courroux. Tant que la nouvelle n’est pas criée, nous avons encore bon temps pour voir si nous ne pouvons convaincre le roi de revenir sur cette décision.

— Il nous faudrait l’oreille du sénéchal peut-être. Si on lui démontre qu’extorquer par mal usage sept sous et demi à aucuns bourgeois en fera perdre bien plus au trésor, il comprendra vite qu’il n’y a rien à gagner à appliquer pareille décision. » expliqua le petit homme, les yeux rieurs.

Le rouquin acquiesça avec vigueur, soudain calmé.

« Voilà fort bonne idée, maître Tosetus. Le roi n’a pas tant pillé à Panéas ou récemment à Damas pour risquer de perdre fortune juste aux fins d’impressionner aucun baron étranger de passage. Il nous faut lui en faire montrance ! »

Tandis que leurs élégantes tenues se perdaient dans le flot ininterrompu de curieux, d’exaltés, de marchands, de valets, de pèlerins, qui emplissait la rue, les riches bourgeois souriaient de satisfaction. Tels des généraux à l’approche d’une cité enclose, ils venaient de voir des failles en la muraille ennemie et savaient désormais comment lancer l’assaut. Il ne serait pas dit qu’ils laisseraient un jeune roi leur retirer les privilèges durement acquis par leurs aînés.

## Malquisinat, Jérusalem, matin du mardi 4 mars 1158

Les mains sur ses larges hanches, Margue réfléchissait, les yeux traînant sur l’étagère où elle entreposait la farine. Elle se tourna vers son valet, un jeune homme à l’air perpétuellement étonné, les oreilles en chou-fleur. Grignotant un morceau de fouace sèche avec l’application d’une vache, il attendait les instructions.

« Porte à moudre encore deux boisseaux de froment, du plus fin. Ce jour nous devrions avoir bel ouvrage, c’est le dernier jour charnel, il faut en profiter. Oublies et brioches devraient bien se vendre. »

Une fois son serviteur parti, elle entreprit de balayer le sol de sa petite boutique. Elle tenait une échoppe près du carrefour principal de Jérusalem, proposant à la vente des plats à emporter, des pâtisseries, des beignets, tartes et confiseries. À l’approche de Carême, les gens profitaient des derniers jours pour faire bombance et elle réalisait alors ses meilleures affaires de plusieurs semaines. Il existait bien des recettes savoureuses acceptables pour les jours de jeûne, mais le commerce s’en ressentait malgré tout, en dépit de l’afflux régulier et grandissant de pèlerins jusqu’à Pâques.

Pour l’heure, en milieu de matinée, elle s’estimait satisfaite, ayant vendu une large partie des produits restant de la veille. En outre, une fournée avait été portée au four du Saint-Sépulcre non loin. Elle profitait de l’accalmie pour remettre de l’ordre dans son échoppe où son imposante silhouette se faufilait avec habitude. Une fois la vaisselle terminée, elle vida son seau d’eau dans le caniveau, d’un geste ample qui aspergea les environs. Quelques badauds surpris froncèrent les sourcils, inquiets d’avoir été éclaboussés.

Au moment où elle reprenait place derrière son éventaire, elle fut apostrophée par un boutiquier voisin, vendeur de pâtés et tourtes de viande.

« As-tu entendu parler de la nouvelle taxe de sept sous et demi, l’Allemande ? »

Elle s’essuya le front de l’avant-bras, penchant son buste de matrone pour voir son compère.

« De quoi me parles-tu ?

— Le roi veut nous faire payer nouvelle fois, les gens de la Secrète ont trouvé bonne idée pour nous prendre encore.

— Et pourquoi donc cela ? Et tous les combien ? »

Le voisin s’avança dans la rue. De petite taille, il avait les pommettes et le nez violet des amateurs de vin, les yeux bridés à force de les plisser. Il passait une main nerveuse dans des cheveux grisonnants coupés courts.

« Ce serait amende si la rue est trop sale au-devant de notre échoppe !

— Nous veut-il devenir cureteur de ruisseau ? Ce n’est pas à nous d’escurer pareil lieu. »

Le boutiquier n’attendait que cette occasion pour laisser sa colère éclater et sa voix monta dans les aigus tandis qu’elle prenait de la vigueur. Bientôt son visage devint intégralement cramoisi.

« De certes ! Nous avons déjà fort à faire en nos ouvroirs, si en plus nous devons surveiller rues et passages ! Nous payons cens, il me semble que c’est déjà assez. Le Saint-Sépulcre ou les frères de Saint-Jean n’ont qu’à faire passer les porcs plus souvent. Ils ont pleins dortoirs de valets gras comme chapons, qu’ils les envoient gratter venelles ! »

Margue opina du menton. Elle suivait néanmoins du regard les passants, espérant toujours que certains s’arrêtent. Désireuse de ne pas les effrayer par des discussions agressives, elle se rapprocha du petit homme appuyé contre la colonne qui séparait leurs deux étals.

« Les Bourgeois du roi ont laissé faire ? Ne sont-ils pas là pour nous protéger ?

— Pfff, ils ne sont bons qu’à s’aider eux-mêmes. Pas un n’a jamais trimé de sa sueur. Ils achètent aux Mahométans pour revendre aux Génois, peu leur chaut nos soucis de boutiquiers. Ils pétrissent livres et marcs^[Unités monétaires de compte.] comme nous farine et oeufs.

— Détrompez-vous, maître, ils sont fort courroucés de cela et comptent bien en reparler en conseil », objecta une voix féminine, grave et et douce.

Les deux commerçants se tournèrent de concert vers la femme qui, un panier au bras, examinait les marchandises offertes à la vente par Margue. Elle était vêtue d’une robe de laine rouge de bonne qualité, ancienne, mais bien entretenue, un voile lâche posé sur ses cheveux d’encre. Le visage aussi digne que celui d’une princesse byzantine, des plis marquaient ses yeux et sa bouche fine, prompte à donner des ordres. Aloys était connue pour son établissement de bains, où les jeunes filles n’étaient guère farouches. Elle leur sourit poliment, se rapprochant pour ne pas avoir à élever la voix.

« Certains des bourgeois du roi pensent que pareille ordonnance n’a nulle valeur, étant donné qu’elle n’a pas été prise avec leur aval.

— Quelle mouche a bien pu piquer le sire roi pour s’inquiéter ainsi des caniveaux ? N’a-t-il pas plus graves soucis avec les Turcs qui se baladent aux alentours de Damas ? »

La petite femme rouge et la grande au physique empâté haussèrent les épaules en même temps. Margue renifla et répondit la première :

« Il suffit qu’il ait senti mauvaise odeur monter de la rue au près du palais, et il s’en sera ému… Difficile de savoir ce qui motive puissants barons. Ils ne sont pas comme nous.

— En attendant, je ne vois pas comment nous pourrons nourrir nos familles si on nous prend par taxes et cens la moindre monnaie par nous gagnée. Un jour, c’en sera trop et… »

Il abattit sa main dans son poing, fendant comme un couperet, le visage empli de colère. Puis, hélé par un chaland, il repartit à sa boutique sans s’arrêter de grommeler. Les deux femmes échangèrent un regard circonspect et reprirent le cours de leurs activités après un salut discret.

## Rue de David, près du change, début de soirée du jeudi 3 avril 1158

Suivant une effluve intéressante, le chien avançait sans prendre garde aux jambes, aux pieds qui le bousculaient, le poussaient, le heurtaient. Il était habitué à se faufiler parmi les hommes et les femmes de la cité. Il connaissait les odeurs des porteurs de triques, s’arrangeait pour les éviter avant même de les voir. Le poil gris, légèrement frisé, il avait le ventre creux des bêtes vagabondes et un œil perdu lors d’une bagarre pour de juteux déchets.

Cela faisait maintenant des jours et des jours qu’il errait dans le quartier où il savait la nourriture abondante. Il fallait souvent la disputer aux gros animaux grogneurs, mais dans l’ensemble il était plutôt satisfait de lui. Parfois il obtenait des restes frais, jetés par quelque main compatissante, et même une caresse fugace de temps à autre. Il aimait bien les gamins, toujours prêts à jouer avec lui, à courir, sauter en riant. Pour les grands, les adultes, il avait développé une technique bien à lui, mélange de regards implorants et d’attitude digne, les pattes avant croisées, la tête penchée, la langue léchant les babines régulièrement. Pour l’heure, il avait repéré deux proies dont il espérait bien tirer profit. Il s’installa donc entre elles deux, à l’abri d’un renfoncement du mur et commença son entreprise de séduction silencieuse.

« Le fort souci de toutes ces chevauchées, c’est le nombre d’espies parmi nos gens. Noreddin[^nuraldin] est informé de tout avant même que les hommes soient en selle.

— Le roi a fort compris la leçon de Meleha[^meleha], Fouques, et pourpense à tout cela. »

Les deux hommes étaient habillés de belles tenues. Le premier, d’une cotte de laine souple, d’un vert tendre, rehaussé de bandes brodées par endroit. Ses chausses, ses souliers, le baudrier et la poignée de son épée étaient à l’avenant, d’excellente qualité sans ostentation. Le vicomte Arnulf ne succombait guère au luxe, mais savait tenir son rang. Il échangeait avec un jeune homme, la barbe courte taillée en pointe, les cheveux mi-longs, un nez en trompette jaillissant d’entre ses joues flasques. Il n’avait pas aux chevilles les éperons d’or des chevaliers, mais portait beau lui aussi : cotte de laine orange, chausses souples, ceinture et fourreau clairs agrémentés de décors métalliques. Ils discutaient, assis sur un banc de pierre, tout en avalant un peu de vin, non loin de la troupe de sergents qui se restauraient parmi les boutiques de Malquisinat. Arnulf s’étira, grognant.

« Le prince Guillaume[^guillaumebures] pense qu’il serait sage de fortifier plus à l’ouest de Saphet, indiqua le sergent, pointant du doigt sur une carte imaginaire tracée entre eux deux.

— Ce serait fort bon lieu, approuva le vicomte, avec Panéas[^paneas] qui garde le pied du mont Hermon. Mais l’ouvrage me parait moult ardu : il y a sept ou huit lieues de Saphet au Jourdain, et le passage est prisé par la cavalerie turque. Avec Belfort au nord qui les contient, ils se faufilent par là jusqu’en Galilée où rien ne les retient plus. »

Il avala une gorgée de vin, dubitatif.

« Je crains fort que nous devions encore et toujours batailler en ces lieux, tant que la cité de Damas ne sera pas nôtre… »

Voyant son verre vide, il le tendit au soldat, qui se leva pour aller le rendre au tavernier. Pendant ce temps, Arnulf fit quelques pas, flattant d’une main légère le chien efflanqué. Comprenant finalement qu’il n’y avait aucune nourriture à gagner, ce dernier s’éloigna rapidement, la queue entre les jambes, déçu. Un groupe d’acheteurs libéra alors la devanture d’un marchand d’oublies, de pâtisseries et la vision des tartes et beignets fit saliver le vicomte. Il s’approcha, parcourut des yeux l’éventaire, choisissant quelques friandises à avaler avant de se remettre en selle pour la ronde du guet. Derrière le plateau, le vendeur souriait d’un air professionnel, un peu intimidé de servir si important personnage. Une fois l’affaire conclue, il empocha les piécettes et se pencha, comme pour parler, mais sans s’y risquer. Le vicomte l’invita d’un geste à s’exprimer, tout en mordant dans la première pâtisserie aux fruits.

« Je voulais vous demander, sire vicomte, si la rumeur est vraie qu’il va nous falloir payer pour escurer les rues. »

Arnulf fronça les sourcils, contrarié que la nouvelle se propage ainsi, aussi vite et aussi mal. Il secoua la tête en dénégation.

« Non. Le roi a décidé de mettre à l’amende ceux qui ne nettoieraient pas au-devant de leur hostel.

— Ah… échappa le marchand, le visage morne.

— C’est pour le bien de tous. En certaines venelles, les immondices sont telles qu’on ne peut y marcher sans risquer de tomber au parmi de la merdaille ! Il n’est tout de même pas si compliqué de faire venir charette ou mulets pour issir tout cela hors les murs.

— De vrai. Mais nous sommes modestes œuvriers et savoir que l’amende s’adresse à nous appuie le fardeau en nos épaules. Peut-être serait-ce aux propriétaires des lieux de s’acquitter de la taxe. N’avez-vous point envisagé cela au conseil ? »

Arnulf soupira.

« Rien n’est encore décidé de toute façon, mais ne serait-ce que pour le commerce, il est bien plus sage de tenir sa rue propre et nette. Bien rares ceux qui aiment à manger les pieds dans le fumier, non ? »

Ne laissant pas le temps à son interlocuteur de répondre, il s’éloigna et rejoignit son cheval et les sergents, qui finissaient d’avaler leur repas en discutant joyeusement. Son air renfrogné inquiéta l’un d’eux, un petit brun au visage contracté. Il vint tenir l’étrier au chevalier pour qu’il se remette en selle.

« Quelque souci, messire vicomte ?

— Certes oui, Baset, il s’en trouve toujours pour demander plus, mais aucun n’est jamais prêt à porter la main en sa bourse. Tout pour rien, voilà ce qu’ils veulent tous ! »

Ne comprenant pas où voulait en venir le vicomte, le sergent fit un signe de tête à ses compagnons, accompagné d’un sifflet rapide. Il était temps de commencer la ronde de nuit.

## Église Saint-Sauveur, Jardins de Gethsémani, après-midi du lundi 7 avril 1158

Le glissement des pas légers sur les dalles avait cédé la place aux raclements sur le sol de gravier et de poussière. Les visages tristes, les épaules basses, franchissaient la porte en silence, dans les volutes d’encens répandues par de jeunes clercs. Le vieux Tosetus, dont les fils André et Rohard avaient repris fièrement le commerce, avait été en son temps un des notables les plus puissants de la cité de Jérusalem. Homme d’action, prompt à la colère, mais dévoué aux siens, il s’était lancé en politique très tôt, et avait fini par obtenir une grande influence à la Cour des Bourgeois. Il avait été un fidèle du roi Foulque[^foulque] puis de la reine Mélisende dont il n’avait jamais accepté la mise à l’écart, qui avait entrainé la sienne propre. Depuis plusieurs années, il se tenait loin des célébrations officielles, des intrigues, et savourait la vie qui s’écoulait depuis son petit jardin aux abords de la porte Saint-Étienne. C’était là qu’il s’était effondré brutalement, trois jours plus tôt, en partie paralysé jusqu’à sa mort quelques heures plus tard.

Généreux donateur, il avait obtenu le privilège d’être enterré auprès de l’église qu’il avait richement dotée de son vivant, sur les contreforts du Mont des Oliviers. Des messes seraient régulièrement célébrées pour le repos de son âme. Après une ultime bénédiction et l’inhumation du corps, quelques proches, venus soutenir une dernière fois la famille endeuillée, s’étaient retrouvés autour de l’officiant. Le visage grave de circonstance, il semblait que ses traits avaient été figés dans cette mimique mi-austère, mi-attristée, qui convient en toutes circonstances à un ministre du culte empli du sérieux de sa fonction. Pour le moment, il hochait la tête gravement tandis que les présents énuméraient les hauts faits du récent défunt, forcément innombrables. André, le plus jeune des fils, était le plus prolixe sur le sujet.

« Je suis aise de savoir qu’il a pu rendre son âme à Dieu en toute paix, il a bien mérité du Paradis.

— Sa générosité était de bonne fame, mon fils, et cela comptera aussi au jour du Jugement, répliqua le clerc d’une voix monocorde.

— S’il y a certeté en cette terre, c’est bien qu’il a sa place au sein des Anges. »

Tout le monde hocha la tête avec solennité à l’affirmation péremptoire, jusqu’à ce que le prêtre ose timidement ajouter :

« Il n’est jamais inutile de dire prières et messes pour l’âme des vivants et des morts, quelles qu’aient été leurs bienfaits, mon fils.

— Malgré toutes ses largesses, il n’y a donc pas garantie de Paradis pour lui ?

— Nul n’est sûr de rien en ce bas Monde. Il nous faut espérer en la bienveillance divine, et se tenir prêt à la seule vérité connue de nous tous, l’amour de Dieu.

— Nulle certitude outre l’Amour de Dieu? », s’inquiéta André, le visage tendu.

« L’amour de Dieu… et les taxes ! » plaisanta son confrère Odo en lui tapant sur l’épaule, un sourire évanescent sur les lèvres. ❧

## Notes

*Malquisinat* désigne le quartier de Jérusalem où l’on trouvait beaucoup de commerces de bouche. L’étymologie, difficile, a parfois évoqué les relents de cuisine qui devaient s’y répandre, curieusement affublés d’une connotation négative. J’ai choisi de m’amuser de cette interprétation et de parler d’une autre cuisine, bien souvent indigeste, concernant la gestion de la chose publique.

Le gouvernement des territoires en Terre sainte ne se faisait pas de façon aussi centralisée que le schéma théorique pourrait le laisser croire. De nombreuses tensions existaient entre les différents groupes sociaux, jaloux de chacun conserver ses privilèges, défendus par leurs membres les plus éminents. Même les bourgeois, dont on a tendance, au choix, à sous-estimer le rôle en Europe ou à peut-être en surestimer l’importance outremer, bénéficiaient d’un statut reconnu. La reine Mélisende s’était d’ailleurs attiré un fort soutien de leur part, demeurés fidèles dans le conflit qui l’opposa à son fils Baudoin, tandis que la noblesse s’avéra plus volage (ou plus malcommode à châtier).

La création de l’amende de nettoyage des rues a dû engendrer un certain nombre de problèmes, car le manuscrit qui en consigna l’existence, des dizaines d’années plus tard, rappelait que cela s’était fait sans le consentement de la Cour des Bourgeois. Le vicomte était donc incité à la clémence dans son application. Même si le besoin de salubrité apparaissait à tous, personne n’était prêt à devoir en supporter la charge. Je vous propose de lire la retranscription du comte Beugnot au XIXe siècle, qui a inspiré « Malquisinat ». Il est curieux de voir comme un texte législatif peut acquérir de la saveur avec les ans et venir chatouiller l’imagination :

>**Chapitre CCCIII** *Ici dit la raison de l’escouver de la ville et des rues, coument hom doit faire par raison.*  
Bien sachés que la raison ne prent mie à droit nus de VII. sos et demy d’escouver les rues, por ce que li rois Bauduins y mist ces establissemens sans le conseill de ses homes et de ses borgeis de la cité. Et por ce coumande la raison et l’assise que puis l’on a fait crier le banc par la vile c’on esnete les rues, et aucun home ou aucune feme faut à celuy banc et qui riens ne fasse net devant son hostel, la raison juge que le vesconte det aver de ce mesfait mout grant pitié, si que prendre ne deit que au mains il pora, et deit pardouner ces VII. sos et demy par pitié.

## Références

Le comte Beugnot, *Assises de Jérusalem. Tome II. Assises de la cour des Bourgeois*, Paris: 1843, p.225.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972, p.93-182.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Prawer Joshua, *Crusader Institutions*, New York : Oxford University Press, 1980.
