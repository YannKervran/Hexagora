---
date: 2011-12-15
abstract: 1148. Tandis que les croisés s’attaquent à la ville de Damas, la flotte génoise est en Espagne, au large d’Almería. Un jeune soldat y découvre la réalité des expéditions maritimes au loin.
characters: Willelmo Marzonus, Enrico Maza
---

# Faux espoirs

## Cap de Gata, sud d’Almería, vendredi 25 juillet 1147

Les quinze galées génoises du consul Balduino Doria se balançaient en cadence, à une large encablure du rivage andalou. Le soleil, impitoyable, transformait les côtes déchiquetées en flammes sombres, ondoyant sous le regard des hommes aux yeux collés par le sel. Ils attendaient.

Depuis des semaines, ils patientaient, inutiles rameurs allongés sous de dérisoires abris de toile. Parfois quelques ordres étaient criés d’un bord à l’autre, un canot était mouillé, allant aux nouvelles ou à l’approvisionnement. Rien ne venait bousculer la monotonie des jours qui s’écoulaient, pas même un vaisseau ennemi aventureux. Le capitaneus[^capitaneus] Oberto Farrugia était affalé, à l’ombre du château arrière, mollement assoupi, chassant de temps à autre les mouches bourdonnant à ses oreilles d’un geste distrait. Il s’était rendu avec espoir à la réunion du matin sur le navire amiral, auprès des consuls. Les troupes de l’empereur Alphonse[^alphonsecastille] n’étaient toujours pas annoncées, en retard de trois mois désormais. Seuls quelques matelots trouvaient la force de discuter, coincés entre les bancs de nage, assis parmi leurs affaires personnelles.

Willelmo Marzonus, vétéran de l’assaut sur les Baléares décrivait par le menu ses exploits passés et imaginaires aux recrues les plus novices. Les yeux rieurs et la malice qu’on y voyait animaient le visage hâlé, tanné par la vie en mer. Malgré sa jeunesse, le sel commençait à gagner son épaisse chevelure noire et sa barbe mal taillée. Tandis qu’il narrait ses aventures, quelques postillons écumeux trouvaient leur chemin à travers la dentition fragmentaire et venaient rejoindre les autres taches à l’origine incertaine maculant sa tenue.

« L’an passé, on a pas eu besoin des princes hispaniques pour rafler la mise. Et on était juste vingt-deux nefs de guerre ! On a connu Oberto Torre plus aventureux !

— Ils doivent penser que belle cité comme Almería ne se force pas avec quelques équipages. Il nous faut bonne cavalerie castillane, rétorqua un jeune basané au torse puissant, Enrico Maza.

— Fadaises ! Nous tenons en cale tout le nécessaire : chat[^chat], pierrières[^pierriere] et arbalètes en tour... Que faut-il en sus ? On se cuit la panse à espérer ici, à serper le fer[^serperfer] ici pour le mouiller là... Sans aucun butin en vue. Nous avons foison d’hommes, de naves, assaillons-donc ! L’empereur sera bien aise de venir cavaler par les rues en notre pouvoir, quand il daignera nous joindre. »

Un des hommes les plus âgés, qui avait place auprès des officiers de poupe s’immisça dans la conversation :

« Au matin, ils devisaient de missionner coursier auprès la cour hispanique, peut-être allons-nous enfin connaître bonnes nouvelles d’ici peu.

— Prions que ce soit de vrai, compagnons, notre chère cité de Gênes espère pour tous les trésors que nous amasserons... »

## Almería, mardi 19 août 1147

« Hardi compaings, demeurez en bons rangs ! Laissons leur temps de nous approcher au mieux... »

Le jeune Enrico s’était équipé légèrement, d’une armure d’épaisse toile rembourrée et arborait à son flanc l’épée neuve qu’il s’était offerte au départ de Gênes. Il avait noué son nasal avec fébrilité et regrettait désormais de ne l’avoir pas suffisamment serré. Couvert de son large bouclier, il lançait de temps à autre des regards en coin à ses compagnons alentour, s’efforçait de contenir la nervosité qui le gagnait. Ils avaient pris pied sur la plage, comme prévu, mais devaient attendre l’approche des forces ennemies avant de retourner au plus vite dans les vaisseaux, à l’abri.

En premier lieu, ils avaient discerné deux éclaireurs sur la crête. Puis la poussière annonciatrice d’une considérable troupe les avait rejoints, accompagnée de la clameur et du brouhaha des guerriers musulmans. Les cris de ralliement incompréhensibles, le tumulte des tambours et des trompettes avaient tout d’abord ébranlé l’assurance des compagnies génoises. Puis lorsque l’horizon s’était rempli d’une marée de boucliers, de lances, d’épées et d’arcs, certains jurons avaient fusé. Les officiers gardaient les yeux rivés sur le navire d’Ansaldo Doria, qui devait lancer le signal du retrait puis de l’attaque générale, en concertation avec les Barcelonais embusqués non loin de là. Le plan avait paru limpide à Willemo et Enrico : en attirant les forces musulmanes loin de la cité fortifiée, ils allaient les prendre dans une vraie souricière. Ils n’avaient pas réalisé que pour un tel projet, il fallait un appât, au destin parfois héroïque, toujours tragique.

Des chocs sourds vinrent frapper les larges pavois : les archers commençaient leur œuvre. Les arbalétriers génois restés en retrait et les servants des balistes sur les navires s’activaient de leur mieux, mais ne pouvaient s’opposer au nombre. Enrico reçut un coup violent dans les côtes :

« Maza ! Tiens la ligne et baisse la tête, bougre de pisseux ! » lui beugla le capitaine responsable de son unité.

Une flèche glissa sur son épaule puis vint tinter contre le casque d’un compagnon. Quelques râles se firent entendre, bientôt suivis d’appels à l’aide. La presse se fit plus forte, les hommes se tenaient serrés les uns contre les autres, offrant comme seule cible à l’adversaire un bloc solide.

Les projectiles volaient de toutes parts et se frayaient un chemin jusqu’aux chairs, jusqu’aux os, jusqu’aux tripes. Un cri se répercuta soudain : « Aux naves ! Aux naves ! ». Enrico fut tenté de tout lâcher pour courir, mais il ne le pouvait pas, à demi porté par ses compagnons. Ils reculaient lentement. De sec le sable fut vite mouillé sous ses pieds, et il se retrouva en quelques pas dans les vagues. Il tâtonnait du talon, se déplaçant sans oser ouvrir les yeux, collé contre ses frères d’armes, cherchant à se fondre dans la masse. Les flèches continuaient à siffler, il sentait leurs pointes se ficher dans le bois.

Peu à peu, le bruit de l’eau se fit plus fort, les cris des marins qui aidaient à rembarquer plus présents.

« Aux filins ! Hissez et à vos rames ! »

Le tumulte ambiant submergea le jeune homme. Partout, des soldats s’agrippaient aux cordages le long de la coque, aidés par leurs compagnons déjà à bord. On se passait les armes, les écus, on jetait ses affaires avant de monter. Oubliant le péril, Enrico se retourna et présenta son pavois, vite chargé, puis chercha des yeux quelques prises pour grimper se mettre à l’abri. Parmi les cris et les appels, il reconnut Willelmo, penché au-dessus de lui, le bras tendu, la paume offerte. Il sauta, saisit la forte poigne, battant des pieds pour se propulser.

Il était bousculé par des épaules, des coudes, des mains qui furetaient dans l’espoir de se hisser. Certains retombaient, dans une gerbe d’eau accompagnée d’exclamations inquiètes. Bientôt il ne resta plus à Enrico qu’à enjamber la pavesade[^pavesade] pour être en sécurité. Au moment où il se laissait choir sur son compagnon, qui ne l’avait toujours pas lâché, il sentit une intense douleur percer son flanc. Un filet de sang s’écoula de sa bouche lorsqu’il s’écroula sur le banc, inconscient. Willelmo le retourna, inquiet, et avisa finalement un matelot à ses côtés, l’air goguenard.

« Ce nigaud a réussi à s’emmancher son fourreau dans la panse ! La douleur l’a assommé ! »

## Almería, matin du vendredi 17 octobre 1147

Chacun s’était aménagé un abri aux abords du rivage, auprès des galées échouées. Assis sous sa cabane de branchages, Enrico ajustait les énarmes de son bouclier. Après un rapide conseil, les consuls avaient ordonné de se préparer à un grand assaut pour le matin. Ses compagnons les plus proches, avec qui il partageait le feu et ce coin de plage, commentaient la décision de leurs dirigeants. On craignait une désertion des forces espagnoles.

« Le capitaneus disait que le comte d’Urgel était des envoyés auprès les païens de la cité. Ces maudits adorateurs d’Apollon ont promis cent mille maravedis[^maravedi] aux forces de l’empereur pour qu’il se retire... »

Un arbalétrier au visage maigre affecta une moue désapprobatrice tandis qu’il finissait de ranger ses cordes de rechange.

« M’étonnerait fort qu’Alfonse se déshonore de telle façon ! Nous ne sommes qu’à quelques pas de la victoire. N’avons-nous pas déjà deux tours prises et plus de cinquante pieds de muraille abattus ? Bien fol le goupil qui hésite à croquer la poule une fois le chien occis... »

Les hommes approuvèrent silencieusement, inquiets, nerveux à l’idée de l’attaque, mais aussi de la possible désaffection de leurs alliés. Ils attendaient depuis plusieurs mois, avaient enduré un long voyage, supporté les privations pendant le siège qu’ils tenaient depuis août. Plus encore que la mort, ils redoutaient d’avoir accompli tout cela en vain, de repartir plus pauvres et pitoyables qu’ils n’étaient arrivés.

Un cavalier passa aux abords, hurlant à chacun de se rendre auprès du camp principal, à la Porta Lena, de façon à former les compagnies d’assaut. Tout en s’avançant, Willemo vint frapper fraternellement sur l’épaule d’Enrico.

« Ce jour d’hui nous serons plus riches à la nuitée qu’au lever, ami. Une poignée de sarrasins maudits outre quelques coudées de pierre nous séparent de notre récompense. À nous de la prendre ! Et vaillamment ! »

Enrico lui sourit, une récente lueur féroce animant ses yeux.

« Cul-Dieu ! Puisses-tu parler de vrai. J’ai grand faim de monnaies d’or, d’étoffes soyeuses et de langoureuses chairs...

— Tu en auras tout ton soûl, crois-m’en ! Plus que tes mains pourront en palper ! »

Willelmo éclata d’un rire gras.

« Vigiles de la sainte Luce[^sainteluce], voilà heureux signe que le Créateur nous envoie. Puisse ma lame ouvrir quelques lumières dans les crânes ennemis. »

Enrico opina, un large sourire carnassier illuminant son visage. Il n’était plus le jeune garçon plein d’idéal embarqué à Gênes. Il avait vu des amis succomber le crâne écrasé sous des pierres projetées depuis les murs de la ville, d’autres les entrailles ouvertes par des lames impitoyables.

La douleur, les cris, la sueur et les larmes emplissaient ses jours tandis que la nuit, les lamentations, les gémissements des blessés et des malades, la faim le maintenaient éveillé. Plus d’une fois, il avait adressé une prière silencieuse à la Vierge, allongé dans sa cabane branlante, le visage fouetté par le vent de sable qui balayait la côte.
Il la suppliait de le protéger un jour de plus, jusqu’au crépuscule suivant, sans oser prétendre à plus de temps ici-bas. Puis au matin, il retrouvait toute sa morgue tandis qu’il revêtait son armure de toile, laçait son casque, apprêtait ses lames. Enrico était soldat, la mort marchait à ses côtés.

## Almería, midi du vendredi 17 octobre 1147

Depuis que les trompettes avaient sonné l’assaut, les Génois avaient senti le temps se ralentir. Marchant inexorablement sur la ville, ils avaient repoussé les tentatives désespérées des habitants de contenir l’envahisseur. Alors que le mitan du jour était passé, les forces chrétiennes s’étaient répandues dans tous les quartiers, et le massacre avait commencé. Les demeures, les biens, les personnes, tout n’était que proie ou butin. Chacun s’empressait d’amasser dans ses sacs ce qui se pouvait prendre, se réservait les captifs, sélectionnait les femmes à son goût pour de coupables plaisirs à venir. Quelques réduits résistaient, sans espoir, pour l’honneur, sans guère de conviction.

Enrico et Willelmo déambulaient au hasard des rues avec quelques acolytes, forçant les demeures qui leur paraissaient receler des richesses. Dans leurs besaces tintaient les objets de métal précieux, bijoux et pièces, triste butin happé dans les maisons saccagées. Ils riaient comme des chasseurs ivres, les poings rougis du sang de leurs victimes. La victoire ne révélait certes pas le meilleur en eux. Un claquement fit sursauter Enrico, qui fronça les sourcils en direction du bruit. Une porte lui semblait avoir été fermée à leur approche. Confiant, il s’avança et, d’un coup de pied, frappa violemment dans l’huis. Un râle surpris accompagna la brusque ouverture. Au sol, le jeune soldat aperçut une jambe dans l’obscurité de la maison, une cheville fine ornée d’une élégante chaussure. Il s’élança en lançant à ses frères d’armes :

« Foutre ! Voilà diablesse qu’il conviendrait de dresser ! »

Il s’avança dans l’ouverture, les yeux plissés, tentant de vaincre l’obscurité. Il devinait une forme mouvante à ses pieds, vêtue de couleur claire, de petite taille. Elle brandissait ses mains, implorant dans une langue inconnue de lui. Enrico se retourna, cherchant l’approbation de ses amis. L’un d’eux hurla, la bave aux lèvres, l’air triomphant :

« Voilà bien ce qu’il te fallait ! La mignonne supplie qu’on l’honore ! Comment refuser si appétissante proposition ! »

Enrico fit face, regardant la jeune femme qui tentait de se relever devant lui, paniquée. Il la gifla sauvagement du revers de la main, claquant de la mâchoire comme un prédateur affamé. Elle retomba sur le sol, non sans avoir violemment heurté et renversé quelques meubles bas. Derrière lui, ses compagnons scandaient son nom, l’encourageaient de paroles lubriques, de commentaires paillards. Il ne pouvait plus reculer. Il appuya les genoux sur le ventre de sa victime et chercha à lui agripper les cheveux. Il voulait plonger dans son regard avant de commettre son forfait, se repaître de sa terreur. Toutefois, lorsque leurs yeux se croisèrent, il ne trouva aucune peur, seulement de la haine. Et il ne vit la lame dans sa main que trop tard.

Lorsque ses chairs furent frappées, la douleur lui déchira le crâne ; il se jeta en arrière, les tempes bourdonnant de souffrance. Grognant un blasphème, Willelmo dégaina son épée et trancha le visage de la femme d’un seul geste. À ses pieds, allongé dans son propre sang, Enrico hurlait de douleur, les mains sur la tête. Finalement la Vierge ne lui avait pas accordé cette dernière journée. ❧

## Notes

Alors que les participants à la Seconde croisade se préparaient à leurs combats en Terre sainte, et échouèrent devant Damas, une coalition entre Espagnols et Génois (et Pisans) décida d’aller assiéger Almería, loin au sud, en Andalousie. L’attaque devait en être faite en mai 1147, mais le retard des forces d’Alphonse VII ne permit pas de commencer les opérations avant l’été.

Après plusieurs mois d’intenses affrontements, la ville fut conquise par un assaut initié par les consuls, qui craignaient (à tort ou à raison) que les Espagnols abandonnent le siège. Par la suite, les navires participèrent à la prise de Tortose, au sud de Barcelone, fin décembre 1148. Bien que victorieuse, cette longue et difficile expédition fut à l’origine d’une crise économique et politique majeure au sein de la cité italienne.

Le récit de ces opérations a été fait par Caffaro di Rustico (1080 — vers 1164), ancien consul de Gênes, chef de guerre, diplomate, ambassadeur. Écrivain prolifique, il a rédigé entre autres les Annales génoises, pour la période de 1099 à 1163. Ayant participé tout jeune à la Première croisade, il a également raconté les exploits de ses compatriotes en Terre sainte.

## Références

Bach, Erik, *La cité de Gênes au XIIe siècle*, Gyldendal, Copenhague : 1955.

Epstein, Steven, *Genoa and the Genoese 958-1528*, The University of North Carolina Press, Chapel Hill & Londres : 1996.

Loud, G.A., trad., (page consultée le 7 juin 2011), *The Capture of Almeria and Tortosa, by Caffaro*, Leeds Medieval History Texts in Translation Website, University of Leeds, 2004, [En ligne]. Adresse URL : [http://www.leeds.ac.uk/arts/downloads/file/689/medieval\_text\_centre](http://www.leeds.ac.uk/arts/downloads/file/689/medieval\_text\_centre)

[^sainteluce]: Les Génois firent cet assaut la veille de la sainte Luce.
