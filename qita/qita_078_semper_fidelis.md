---
date : 2018-04-15
abstract : 1132-1152. Poussé par l’ambition de son père, Baset devient sergent du roi. Son premier combat lui fera sentir à quel point ses aspirations étaient loin de cette carrière. Il en gardera par la suite un souvenir poignant.
characters : Baset, Rufin, Gauguein, Thomas regrattier, Jehan de Berry, Rolant Templier
---

# Semper fidelis

## Jérusalem, vallée du Hinnom, fin d’après-midi du mercredi 5 février 1130

En contrebas d’arides terrasses où prospéraient de tortueux et anciens oliviers, une zone avait été abandonnée au gravier et aux pierres, aménagée en une plateforme permettant les exercices militaires. Les soldats y venaient de temps à autre accomplir de martiales manœuvres, quoique la place fût un peu resserrée pour les larges déploiements de cavalerie. En cette fin de journée, trois montures y trottaient, soulevant la poussière en un nuage gris. Brinquebalant sur les bêtes, de jeunes garçons s’efforçaient de tenir en selle, grimaçant à cause des douleurs et des crampes. Aucun son ne sortait néanmoins de leur bouche, subjugués qu’ils étaient par le regard impérieux de leur père Thomas, petit homme au physique tassé, pérorant depuis le centre de la carrière, les sourcils froncés, le front plissé et la moue sévère, occupé à cingler l’air de sa badine pour maintenir le rythme.

« Talons en bas ! Yeux au loin ! »

Ses ordres claquaient comme sa baguette et il accompagnait parfois sa réprimande d’un caillou adroitement lancé pour bien marquer le destinataire de l’instruction. Sourcillant plus encore, il huma le temps, inquiet qu’une averse ne vienne obscurcir le ciel, amenant d’autant plus vite la nuit. Il payait les montures à la journée et n’aimait guère voir ses deniers partir sans en tirer la quintessence.

« Rufin ! On te dirait pareil à sac mal arrimé ! Ne peux-tu faire corps plus étroit avec ta bête ? »

Il s’accorda une longue lampée d’une calebasse où macérait un vin largement coupé d’eau, dont il tenait que c’était un bon fortifiant pour demeurer l’esprit alerte et le corps échauffé. Il se gargarisa de sa boisson, appréciant sans le montrer ce qu’il voyait de ses fils. Il leur avait payé des cours d’équitation, dispensés par un sergent d’armes de la maison du roi et, à en observer le résultat, il estimait que c’était là fortune bien dépensée. Selon lui, le fait de tenir en selle différenciait le commun du notable. Il avait donc consenti à ce sacrifice avec la même abnégation qui le faisait économiser pour doter correctement ses filles. Il avait les mains brûlées par son commerce de regrattier de sel^[Négociant en sel.] et voulait à toutes forces voir ses enfants arriver plus haut qu’il ne s’était élevé.

Il avait donc concocté un programme pédagogique dont il prévoyait qu’il ferait de Rufin, Gauguein et Baset de solides prétendants à devenir hommes d’importance, sinon de renom, dans le royaume. Il estimait avoir fait sa part en ce chemin, une seule génération le séparant de la pauvreté la plus indigente, ses parents ayant récolté le sel pour d’autres, y abandonnant leur santé. À peine avait-il compris qu’un destin identique l’attendait qu’il avait fui son Languedoc natal, rejoignant la cohorte qui avait pris la route de la Terre sainte après l’appel lancé par le Pape. Il narrait parfois à la veillée les moments forts qu’il avait connus lors de ce long périple, mais sans jamais en évoquer le dénouement, sinon en une ellipse qui le dévoilait agenouillé sur le tombeau du Christ. Il avait été de ces malheureux débandés dans les plateaux anatoliens, pourchassés et exterminés par des Turcs impitoyables. Le seul souvenir qu’il ne pouvait en cacher était une claudication et celui qu’il tentait de dissimuler, une franche terreur à la vision d’archers montés.

Estimant que la pratique avait assez duré, il siffla la fin de la leçon et invita ses fils à s’approcher. Il leur donna à chacun un quignon et leur accorda une belle lampée, qu’ils avalèrent sans se faire prier.

« Il va être temps assez de s’en retourner à l’hostel. Frictionnez donc un peu vos bêtes. »

Tandis que les garçons s’exécutaient en silence, il en profita pour vérifier qu’ils y mettaient tout le soin qu’il attendait d’eux. Lorsqu’il vit Rufin inspecter négligemment les pieds de sa monture, il lui décocha une gifle sur l’arrière du crâne.

« Veux-tu risquer de la faire boiteuse ? Penses-tu que le loueur aura plaisir à retrouver une bête estropiée ? »

Puis il lui intima de recommencer les quatre sabots, cette fois sous son contrôle. Le cheval, sentant la nervosité, se montra rétif et agité, ce qui compliqua d’autant plus la tâche à son fils. Ce dernier s’exécuta pourtant sans un mot, craignant de recevoir, en plus d’une nouvelle réprimande, un autre soufflet. Rufin étant le plus âgé des trois, Thomas avait la main fort leste avec lui, mais s’efforçait de ne frapper que pour corriger et pas dans l’emportement du mécontentement ou de la colère. Il estimait sa violence tout éducative.

« Baset, pour s’en retourner, je prendrai ta bête. Tu iras en croupe ! »

Le benjamin acquiesça sans mot dire et commença à régler les étrivières plus longues pour permettre à son père de grimper en selle. Surveillant ses moindres gestes, Thomas cherchait un motif de le corriger, conscient qu’il ne lui accordait pas tant de considération qu’aux autres, mais le gamin n’offrait que peu l’occasion de prêter le flanc à la critique. Il obtempérait docilement, avait la réussite modeste et l’échec plus discret encore, soucieux de ne jamais attirer l’attention sur lui. Son père tentait de ne pas le négliger malgré cette incroyable faculté à se faire oublier. Il en concevait quelque angoisse à l’idée que cela dénotait chez lui un manque d’ambition et veillait donc à en nourrir les appétits aussi fortement qu’il le pouvait.

« Tu veilleras à secouer ta cotte, elle est tellement grise de poussière qu’on te croirait mendiant crotteux ! Je n’ai guère désir de te voir frotter à mes habits, sale comme tu es. »

Baset n’osa qu’un timide « Oui, père » en réponse et s’activa immédiatement à brosser ses vêtements avec vigueur. Il en était désespérant de docilité, à craindre qu’il ne fût jamais bon qu’à obéir. Thomas lança un œil vers Rufin et s’avança vers lui en secouant sa badine.

« Rufin ! Serre donc ta sous-ventrière, bougre d’âne ! Tu vas finir le cul à terre si on repart ainsi ! »

## Casal Techua, fin de matinée du vendredi 15 septembre 1139

Le village n’était pas un hameau de colons, mais une ancienne agglomération dans laquelle les seigneurs latins avaient érigé une maison forte. Quelques bâtisses neuves s’y étaient accolées, pour les plus aventureux des pèlerins qui avaient eu le désir de faire souche ici en profitant des avantages fiscaux accordés.

Pour le moment, la seule habitante en était une poule, occupée à gratter le sol aux abords d’un abreuvoir de pierre. Elle observait d’un œil circonspect la troupe armée qui s’était égaillée en tout sens. Ses membres avaient constaté l’abandon du village, le forçage de quelques portes et inspectaient les lieux sans rencontrer aucune résistance.

Arrivé parmi les derniers, avec les sergents du roi, Baset avait trouvé une cruche partiellement brisée dont il se servait comme d’un gobelet. La soif lui brûlait la gorge, avec toute la poussière avalée à chevaucher à l’arrière du convoi venu de Jérusalem au matin. La veille, il avait été réquisitionné par le porte-bannière du roi, Bernard Vacher, pour se joindre à un groupe de la milice du Temple. Une bande de turcs ou de turcmènes avait été signalée dans les abords de ce hameau, profitant de l’absence de l’armée royale, en campagne loin à l’est dans le Hauran, pour mettre au pillage le secteur.

Baset avait espéré y couper, comptant sur son inexpérience au combat, mais à son grand désarroi on l’avait désigné comme bon cavalier. Il avait prêté serment quelques semaines plus tôt, pleinement satisfait que ses tâches fussent essentiellement administratives, en plus du guet. Même là, leur nombre faisait qu’ils avaient rarement à tirer l’épée ou pointer la lance, ce qui lui convenait parfaitement. S’il estimait important de servir loyalement, il n’avait guère envie que cela se fasse au détriment de sa survie.

Il avait donc été soulagé de découvrir que les brigands avaient décampé, ne voulant pas se frotter à une troupe armée alors qu’ils avaient fait provision de butin. Toutes les portes n’avaient pas été défoncées, mais les pillards avaient tenté de s’introduire un peu partout, profitant de la moindre fenêtre pour aller fouiller puis répandant les reliefs de leur tri ici et là au hasard des rues.

Drouet, qui dirigeait le petit groupe de sergents du roi, attendait les instructions de Bernard Vacher, en grande concertation avec le maître de la milice. Ce dernier semblait décidé à donner la chasse aux détrousseurs, ce qui ne soulevait guère d’enthousiasme chez Drouet, normand au caractère grognon.

« Je serais bien mal engroin qu’on aille risquer nos fesses pour la picorée de ces brigands. Nul n’est mort ici et ils n’ont guère eu le temps de faire bombance…

— Il n’y avait donc personne dans la tour grosse pour s’opposer à eux ? demanda Baset.

— S’il y avait là soldaterie, elle a dû mener les habitants en quelque abri. Les vallées à l’entour comportent grottes et caves à foison. De quoi se rembarrer solidement le temps que la tempête passe.

— Ils ne craignent guère pour leur grain !

— Ces brigands ne sont pas barons tels Sanguin[^zengi] ou Anour[^anur] ! Ils ne vont pas s’encombrer avec si lourd butin. Les nomades grappent étoffes et tout ce qui a de la valeur sans trop peser. Les vilains s’encachent avec leur avoir, abandonnant ce qui n’est que de peu de prix à leurs yeux. »

Un des chevaliers de la milice, l’allure sévère et le visage fermé, s’approcha d’eux en menant son destrier au pas. Il désigna Drouet d’un coup de menton.

« On m’a dit que tu savais bien la route pour se rendre à Saint-Abraham[^abraham] ?

— Oc, sire frère de la Milice. Un mien cousin a ses mouches à miel dans la région.

— Tu ouvriras la voie en ce cas ! »

Puis il se tourna vers Baset.

« Tu nous compaignes aussi, si jamais il faut adresser messages depuis là-bas. »

Drouet et Baset hochèrent la tête et se mirent en branle sans oser questionner le chevalier sur la situation. Sa mine austère et son regard lointain n’incitaient que peu au dialogue. En outre, Baset se méfiait de ces soldats impétueux, dont il reconnaissait le courage, mais redoutait la témérité. Du moins quand cela pouvait déboucher sur un danger pour lui.

## Casal Techua, chemin méridional menant vers Hébron, midi du vendredi 15 septembre 1139

Le trio de cavaliers menait bon train, faisant voler les graviers et la poussière. Ils longeaient les reliefs en une piste ondulant vers le sud jusqu’à une voie vers l’ouest et Hébron. De nombreux pruniers y côtoyaient les inévitables oliviers. Sur les versants les mieux exposés se déployaient des vignes dont les vendanges ne sauraient tarder. Aucune âme n’était visible aux environs, chacun ayant pris soin de se dissimuler dès la bande de pillards repérée. Les pâtres avaient rassemblé leurs bêtes dans des ruines et des cavernes connues d’eux seuls et les villages ne pouvant opposer une résistance efficace s’étaient vidés, en direction de caches et de retranchements.

Les paysans de la région avaient fini par s’habituer aux allées et venues des groupes de voleurs : cela faisait plus d’un siècle qu’ils en étaient les victimes. N’ayant nulle terre lointaine où s’enfuir, ils subissaient les assauts et les affronts en priant à la mosquée, l’église ou la synagogue, pour que leur Dieu les débarrasse au plus vite de ces détrousseurs.

Baset gardait la tête baissée, s’efforçant de suivre le rythme haletant imposé par le templier. Il en voyait le blanc manteau qui dansait devant sa propre monture, plissant les paupières lorsque le soleil accrochait un éclat métallique sur le casque peint du chevalier. Baset avalait la poussière des deux cavaliers malgré le foulard dont il s’était entouré le bas du visage et sentait les larmes s’agglomérer en un ciment crasseux le long de ses tempes. Il y avait longtemps que sa bouche n’était que de carton et son nez plâtré et il espérait à chaque lacet découvrir les fortifications d’Hébron. Chaque courbe se dévoilait comme une nouvelle épreuve qu’il s’efforçait de passer l’une après l’autre.

Soudain, alors qu’ils abordaient une courte descente en surplomb de quelques ruines de terrasses en friche, il aperçut plus qu’il ne vit le cheval de Drouet faire un brusque écart, à se jeter contre des broussailles avant de s’effondrer subitement. Le templier n’eut pas le temps de réagir que sa propre monture sautait avec lui dans le vide à droite, laissant le champ libre pour que le roussin de Baset décide de son propre chef de bondir par-dessus l’obstacle.

Baset en perdit les étriers, retomba durement sur la selle, en échappa une des rênes. Se cramponnant de la jambe au troussequin, il empoigna une touffe de crin. Sans plus rien contrôler, il s’efforçait de ne pas être projeté à terre tandis que la bête se mettait à ruer, gênée par ce poids mort en déséquilibre sur son dos.

À demi aveuglé et peu enclin à observer son environnement, il n’eut pas le temps de voir le Turcmène vers lequel sa monture le menait qu’il vola en sa direction, fesses en avant, propulsé par un coup de cul circulaire. Le choc violent lui fit grincer les dents, vida ses poumons et résonna de ses reins à ses épaules.

 Il reprit ses esprits pour se découvrir une jambe en l’air coincée dans la fourche d’un buisson épineux, le nez dans la terre et le bras tuméfié, allongé en partie sur le Turc. Lorsqu’il se tourna, il constata qu’il en avait écrasé le visage sur une pierre en le heurtant. Hagard, il tenta de se remettre dans une position plus confortable et vit qu’il avait brisé aussi l’arc et des flèches. Frottant ses reins, il y palpa des meurtrissures à travers les déchirures de sa cotte. Il se sentait nauséeux et hoqueta plusieurs fois, à la limite du vomissement.

 Lorsque ses haut-le-cœur se calmèrent, il prit conscience d’une voix qui appelait. Il fut surpris d’en comprendre les mots. Il se mit debout, encore vacillant et flageolant sur ses jambes. La poussière retombait doucement, recouvrant mollement le sol. Il aperçut la monture de Drouet, deux flèches profondément enfoncées dans la gorge, un antérieur clairement fracturé et un flanc écorché par sa selle brisée. Elle claudiquait sur le chemin, dans l’espoir de rejoindre le cheval de Baset, dont un nuage poudreux indiquait la direction de fuite.

 Drouet gisait, tel un amas de chiffon, le cou rompu et les membres en désordre, curieusement entassé contre un rocher. Il n’avait plus rien à y faire et Baset se contenta de lui dédier un rapide signe de croix. La voix persistait, c’était donc le templier qui appelait. Il avait chuté avec sa monture dans un petit ravin. La pauvre bête avait répandu le contenu de son crâne sur une pierre et, par sa mort, avait immobilisé son cavalier sous elle. Le chevalier échappa un soupir de soulagement en voyant Baset.

 « Jambe Dieu ! Tu as occis ce maudit Turc ! Grâce à Dieu, tu as eu le bras plus vaillant que ses flèches traîtresses. »

 Baset hocha la tête sans mot dire. Il ne voulait pas détromper le chevalier en lui expliquant que Dieu avait choisi une partie bien moins noble de son anatomie pour mener à bien son dessein. Il descendit péniblement dans la petite combe dans l’espoir de porter secours au blessé. Lorsqu’il tenta de le tirer, l’autre hurla soudainement de le lâcher. Puis il haleta, le visage cramoisi de douleur, les traits enfiévrés.

 « Par la malepeste, j’ai la jambe brisée, je crois bien ! Je ne sens que plaies et horions là-dessous ! »

 Il respira longuement plusieurs fois, dévisageant Baset.

 « Ton cheval est où ?

— Enfui. Il m’a jeté sur le Turc et n’a pas demandé son reste.

— Il n’y a personne aux alentours, au moins ? Il est rare que ces pillards soient esseulés. »

Baset sentit soudain sa vessie prête à se vider et son visage perdit toute couleur. Il risqua un œil inquiet autour d’eux. Le templier comprit bien vite qu’il n’était pas de l’étoffe dont on fait les grands soldats.

« Regarde à l’entour. Il y a peu de chances qu’il se soit trouvé ici tout seul, à pied. Ces démons ne vivent sans monture. »

Encore peu assuré sur ses appuis et sentant des douleurs sourdre de chaque partie de son corps, Baset crapahuta de son mieux aux abords. Rien ne bougeait autour d’eux, ce qui le rasséréna un peu. Il explora le coin avec d’autant plus de détermination, pour y dénicher finalement un bel étalon alezan attaché dans un bosquet. Vu son équipement, c’était là la monture du Turc qui les avait attaqués. Un sac posé au sol contenait des affaires de palefrenier, sans qu’il en connaisse l’usage exact. Étudiant la bête, il comprit bien vite que l’homme s’était arrêté pour soigner une boiterie. Néanmoins, les oreilles aux aguets, elle se laissa mener sans problème.

Il traversa de nouveau le chemin, mais le cheval refusa de s’approcher du cadavre, renâclant et menaçant de lui échapper. Il l’attacha à un arbre et retourna voir le templier.

« Il semble avoir eu souci avec sa monture. Elle refuse d’approcher et je n’ai pas trouvé longue corde pour vous dégager avec son aide. »

Le teint blanc, le templier acquiesça. Il sentait ses forces l’abandonner. Peut-être était-il plus blessé qu’il ne l’avait cru de prime abord. Tant qu’il serait bloqué sous le cheval, il ne pourrait le savoir exactement.

« En ce cas, tu vas prendre cette bête afin de retourner au casal demander de l’aide à mes frères.

— Je ne peux vous abandonner. Si jamais d’autres pillards s’en venaient ?

— De cela je ne doute guère, d’autant qu’ils vont voir arriver à eux ton cheval avec un harnachement qu’ils sauront reconnaître comme ennemi. Peu me chaut, j’ai juré en les mains du maître et mourrai glaive au poing si c’est la volonté de Dieu. Mais de cela rien n’est joué, contente-toi de galoper au loin. Ta célérité est mon espoir. »

Baset hésita à lui dire qu’il ne saurait aller bien vite avec un cheval boiteux, mais il se contenta de lui laisser à portée de main une outre qu’il avait dénouée de la selle du Turc.

« Je reviens au plus vite, sire chevalier. À Dieu vous mande !

— Vole, maraud, pour mon salut. Et pour ton priement à Dieu, remembre-lui mon nom : Rolant… »

## Jérusalem, Saint-Sépulcre, matin du lundi 15 septembre 1152

Des nuages d’encens se répandaient depuis la rotonde du tombeau du Christ jusqu’au petit autel annexe où Baset avait habitude de venir se recueillir. Comme chaque année, il avait porté un cierge de belle taille et lâché une pièce dans le tronc des messes, puis s’était installé pour y réciter les seules prières qu’il connaissait : *Ave*, *Pater* et *Credo*. Il le faisait toujours dans cet ordre, car il estimait qu’il était plus habile de commencer par en appeler à la Vierge, plus encline à se montrer bienveillante, puis au Père qui saurait ne pas être jaloux de son attention première. Enfin, il glissait ses demandes avant de confirmer, par sa litanie finale, qu’il était bon chrétien, afin de rassurer Dieu de son éligibilité à voir ses vœux exaucés.

Finissant ses oraisons, il se signa rapidement et prenait le chemin du palais pour y débuter son service lorsqu’il fut apostrophé par un des chanoines. Habillé de belle laine noire, il avait le visage replet et le regard franc, un sourire aimable figé sur la face. Son allure bonhomme incita Baset à froncer les sourcils, toujours méfiant des sollicitations qui se préparaient derrière si avenantes façades. Il avait trop souvent eu affaire à des menteurs et des escrocs, que ce soit au péage des portes ou lors des rondes du guet, pour conserver une opinion favorable envers ses prochains.

« Le bon jour à vous, mon fils. On ne vous voit pas si souventes fois en nos murs, mais il me semble que vous y brûlez chaque an beau cierge en cette date. Seriez-vous nommé Nicomède ou lié peu ou prou à lui ? »

Décontenancé par la question dont il ne voyait pas l’intérêt, Baset suspecta immédiatement le préambule à une quête, à laquelle il comptait bien se dérober. Il se fit le visage le plus rétif qu’il avait en réserve et répondit d’un ton morne.

« Jamais ouï parler de lui, père chanoine. Désolé…

— Faites excuse, j’avais cru, à vous voir ici fêter ainsi son martyr. Il est de certes fort déconnu des fidèles et, même les plus instruits de nous n’en savent guère en dehors de ce que nous en a dit le Saint-Père Adrien. »

Le prêtre inclina la tête, espérant que Baset se dévoilerait un peu plus. Avant que le silence ne tourne au malaise, ce dernier se résolut à répondre a minima au clerc, estimant qu’il semblait inoffensif et, surtout, simplement curieux. Il pourrait ainsi s’en débarrasser à bon compte.

« Il se trouve que je viens prier pour Rolant.

— Un vôtre parent ?

— Aucunement.

— Est-ce quelqu’un qui a rendu son âme à Dieu en ce jour ?

— Je ne sais.

— Pourquoi donc le choix de ce jour ?

— Il serait bien temps qu’on fête les Rolant, et hui me paraît aussi bon qu’un autre. »

Il haussa les épaules, adressa un dernier message muet à la flamme du beau cierge et s’en fut sans un mot de plus. Nullement rebuté, le chanoine dédia un sourire désabusé au sergent qui lui tournait le dos, puis, machinalement, ajusta le placement de la nappe d’autel brodée.

« Saint Rolant ! Que voilà étrange lubie ! Le pauvret aura trop entendu les aventures du grand Charles^[Charlemagne. Allusion à *La chanson de Roland*, qui célèbre, entre autres, les hauts faits de ce chevalier légendaire de l’empereur.] ! » ❧

## Notes

Le baptême du feu de Baset, pour fort peu héroïque qu’il soit, se déroule lors d’un des nombreux affrontements dont il nous est peu parlé dans les sources. Guillaume de Tyr indique en quelques lignes que les Templiers y perdirent beaucoup d’hommes face à des pillards. Il ne faut pas imaginer une belle bataille rangée, mais une série d’escarmouches et d’embuscades qui prenaient place sur un assez vaste territoire. Comme souvent, les effectifs de chaque côté étaient très faibles, bien plus proches de la centaine que du millier.

La scène finale joue avec le fait que le 15 septembre soit désormais bien la saint Roland, en mémoire d’un Italien du XIVe siècle et pas du tout de ce malheureux chevalier que j’évoque. Si j’ai choisi cette date, c’est pour illustrer le fait que les choses ne sont pas toujours aussi assurées qu’on le croit dans les héritages culturels. Il y a souvent jonction, fusion, contagion entre des traditions diverses pour établir des idées considérés comme avérées et univoques par la suite. La recherche historique se bâtit à rebours en allant à la recherche des sources, parfois bien moins définitives que les certitudes communément enseignées.

Enfin, le titre de ce Qit’a, *Semper fidelis*, « Toujours fidèle » évoque à la fois la question de la fidélité des sources et aux sources, de l’image que l’on se construit du passé, mais également de la fidélité de Baset à son caractère, aux ambitions de son père, à son maître le roi et à son propre souvenir traumatisant. Mais c’est aussi une devise très guerrière qui peut faire référence au courage du templier, cette antienne ayant été adoptée par de nombreux combattants dans l’histoire, dont le corps états-unien des Marines est certainement le plus connu.

## Références

Guillaume de Tyr, *Historia rerum in partibus transmarinis gestarum*, Recueil des Historiens des Croisades, Occidentaux tome I

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem. A corpus. Volume I. A-K*, Cambridge : Cambridge University Press, 2008.

Edgeller Johnathan James (B.A.), _Taking the Templar Habit: Rule, Initiation Ritual, and the Accusations against the Order_, MA Thesis, Texas Tech University, 2010.
