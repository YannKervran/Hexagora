---
date : 2018-11-15
abstract : 1139-1159. Gauguein et Rufin, deux frères amateurs de technique se découvrent avec le temps une passion et des capacités pour concevoir et manipuler les engins de siège. Jusqu’où les mènera leur attrait pour ces machines de guerre ?
characters : Baset, Rufin, Gauguein, Thomas le Regrattier, Paylag, Stamatès Mpratos, Parisete, Garsende
---

# Ingénieuses bricoles

## Jérusalem, boutique de Thomas le regrattier, début d’après-midi du vendredi 6 janvier 1139

Jeune homme d’à peine vingt ans, Baset aimait venir dans la boutique de son père lorsque celle-ci était fermée. Il trouvait toujours une excuse, de balayage ou de rangement quelconque, pour se retrouver dans ce petit monde qu’il connaissait si bien. Les jours d’ouverture, il y apportait les mesures, les paniers, rebouchait les jarres et tonneaux contenant le précieux sel gemme, accueillait les clients d’un salut poli. Il triait aussi régulièrement les aulx, oignons et échalotes qu’ils vendaient en grande quantité, pour en ôter les pourris et les desséchés.

Il n’aimait néanmoins guère faire le valet et porter des commandes, ce qui l’obligeait à quitter son nid. Il s’acquittait de toutes les tâches qu’on lui confiait avec enthousiasme, pourvu qu’elles le cantonnent à ces quelques pièces. Il avait appris à différencier les nombreuses variétés de sel à leur odeur, leur couleur, leur brillance. Et il était désormais aussi habile que son père à reconnaître du premier coup d’œil ce qui était de première qualité, la fleur, des fonds de marais. Il aimait par-dessus tout faire des tresses d’oignons, qu’il accrochait fièrement à l’éventaire dès l’ouverture.

Une fois de retour de la messe matinale, il avait rapidement troqué ses beaux vêtements pour sa vieille tenue de travail et avait entrepris de trier de petits oignons sauciers stockés dans des paniers en paille de seigle. Malgré le froid, il avait laissé la porte arrière de la boutique ouverte afin de bénéficier de la chiche lumière hivernale. Un des chats de la maison en avait profité pour venir s’installer vers lui, ondulant de la queue tout en faisant sa toilette. Il goûtait le calme de la présence du jeune homme, bien moins agité que sa sœur cadette Ascelote, toujours à le pourchasser pour lui infliger caresses et câlins qu’il subissait d’un air affligé.

Baset entendit donc ses deux frères entrer dans la cour, bavardant avec animation. Son aîné, Rufin, revenait de sa rencontre avec les jurés du métier de fabricant d’arbalètes. Il avait achevé sa formation depuis un moment et prévoyait de s’établir maître à son tour. Leur père était enthousiaste à l’idée de voir un de ses enfants devenir un artisan dans une profession prestigieuse. Il avait d’ailleurs promis de payer le prix d’entrée du métier, permettant à son fils de s’installer rapidement sans trop s’angoisser pour les premiers temps. Baset dédaigna un instant son travail pour les accueillir, impatient de savoir si les choses allaient se faire.

« Le mestre du métier est fort impatient de voir s’ouvrir nouvel atelier ! Il y a tant à faire qu’ils n’arrivent à fournir et le sénéchal le roi s’en plaint fort ! s’enthousiasmait Rufin.

— Il a précisé qu’il avait déjà une commande à laquelle Ruffin pourrait prendre part, s’il avait ses approvisionnements et oustils à temps, précisa Gauguein, plein de ferveur.

— Le père est allé se reposer un temps, mais a demandé à ce qu’on l’éveille si les nouvelles étaient bonnes, indiqua Baset, tout sourire.

— Ce dimanche, il y aura quelque pratique au terrain près la porte Jehoshaphat. Tous les jurés y seront, et nous y conviendrons de la date de mon serment au métier ! »

Bondissant de joie, Baset courut réveiller leurs parents, allongés dans la grande salle derrière les rideaux de leur lit. Installée à jouer à la poupée près de la table, Ascelote avait entendu le bruit et les avait déjà prévenus. Ils se levèrent rapidement et accueillirent les trois garçons avec enthousiasme. Pour une fois, Thomas se laissait aller à sourire et demanda à sa femme de leur apporter un pichet de vin. Il avait plutôt bien marié deux de ses filles, verrait un de ses fils tenir sa propre échoppe d’ici peu, avec un autre en bonne voie pour devenir un charpentier compétent. Et il ne l’avait pas encore dit, mais un de ses amis s’apprêtait à proposer Baset comme sergent du roi. Il ne lui resterait alors que sa petite dernière dont il faudrait s’occuper. Ascelote, dont les formes se dessinaient plus féminines chaque mois, pourrait trouver un honnête prétendant d’ici deux ou trois ans, malgré la faible dot qu’il avait pu lui constituer.

Parisete, leur mère, sortit quelques fruits secs et la fouace qu’elle avait achetée pour le repas de fête de la veillée, pour l’Épiphanie. Le moment en serait doublement heureux et elle augurait du bon de cette belle coïncidence. Il n’y avait pas eu autant de joie dans la maison depuis le mariage d’Heloys, trois années plus tôt : elle avait épousé un riche propriétaire dont on disait qu’il pourrait devenir intendant seigneurial du casal où ils vivaient désormais.

## Jérusalem, maison de Thomas le regrattier, soirée du mardi 17 avril 1145

La journée avait été longue pour Thomas. Après la frénésie de la Semaine sainte, il avait dû encore faire face à beaucoup de demandes. Beaucoup de pèlerins s’étaient munis de quelques provisions avant de repartir et certains visiteurs des casaux environnants avaient profité de leur venue dans la cité pour Pâques afin de faire des emplettes. Il n’avait donc guère chômé, désormais assisté seulement d’un petit valet qui n’était pas aussi vaillant à la tâche que Baset, malgré les réprimandes et les corrections qu’il lui infligeait.

C’était au moment de fermer qu’il avait vu arriver ses deux fils Rufin et Gauguein, dont il était sans nouvelle depuis plusieurs mois. Ils étaient partis travailler à la citadelle de Kérak, loin dans les déserts du sud, par-delà la rivière du Diable[^rivierediable] afin d’en améliorer les défenses. Rufin était entre autres un des responsables de l’approvisionnement des arsenaux et Gauguein l’avait rejoint pour l’assister dans ses réalisations les plus techniques ou les ouvrages les plus lourds. Leur visage avait gagné quelques rides et Rufin arborait une belle barbe brune qui renforçait son faciès d’ours.

Parisete leur fit un accueil chaleureux, même si elle maugréa de n’avoir pas le temps de leur préparer un repas à la hauteur. Elle envoya le valet prévenir Baset, à l’hôtel du roi, qu’il était convié à souper avec eux tous. Elle n’aimait guère sa bru Garsende, l’épouse de Baset, et estima inutile de l’inviter. D’autant qu’avec un bébé en bas âge, elle devait elle aussi assez peu apprécier de voir sa routine perturbée. Selon Parisete l’absence de son fils serait de moindre conséquence, pour peu que celui-ci sache faire montre de l’autorité qu’on attendait d’un chef de famille quand il rentrerait.

Thomas fit le service lui-même, claudicant autour de ses enfants comme une abeille affairée. Puis il avala quelques gorgées, savourant son bonheur.

« Alors, les garçons, quelles nouvelles du désert ?

— La citadelle aura bientôt belle allure, mais il s’en faut de beaucoup qu’elle soit achevée. On a sans cesse manque de bois d’œuvre.

— On est de passage pour aller récupérer une cargaison venue de Tripoli. Elle nous attend à Jaffa, précisa Gauguein

— C’est à vous que l’on confie pareille responsabilité ?

— Le mestre des bois est homme de grand savoir, mais il aime guère mulets et chameaux. Le Rufin avait besoin de fournitures, aussi.

— Oui, on reste pas. Demain matin, dès les portes ouvertes, on reprend la route. »

Le père hocha la tête, satisfait. Il s’étonnait de voir son cadet prendre l’ascendant sur Rufin, qui était pourtant maître juré désormais. Mais tant que les deux frères s’entendaient bien, cela n’était pas de grande importance. 

« À vous tanner pareillement les fesses, quand est-ce que vous trouverez femme pour me pondre nichée de bons enfançons ? Nulle garcelette de bonne famille n’espère après vous en Outrejourdain ?

— Rien de la sorte ! le détrompa Gauguein. D’autant qu’un de mes compères apprentis m’a fait belle proposition pour un autre chantier, ailleurs.

— Vous ne vous plaisez pas là-bas ?

— On aimerait mieux être plus proche d’ici… Ce serait à Ibelin, la nouvelle forteresse du vieux Barisan, pour protéger des Égyptiens. »

Parisete retint un sourire. Elle avait vu avec regret ses deux fils aînés partir au loin dans des terres inconnues. Même si leur nouvelle situation était un peu plus risquée, de ce qu’elle en savait, au moins aurait-elle plus de chances de les croiser. Elle rêvait du jour où elle pourrait accompagner toute sa famille à une cérémonie d’importance, dans leurs belles tenues, afin de montrer à l"envie combien ils avaient réussi. Elle prit place à table avec eux, posant un peu de pain et des olives, pensant à la robe qu’elle devrait peut-être se faire tailler pour une telle occasion.

## Ascalon, camp de siège chrétien, fin d’après-midi du jeudi 9 juillet 1153

Gauguein retrouva son frère dans le petit abri de palmes et de toile qu’ils s’étaient arrangé contre un talus sablonneux. Ils étaient dans les cordes du campement des hommes d’Ibelin, où ils participaient à l’entretien des matériels de combat, essentiellement armes de tirs et mantelets de protection. Le travail était harassant, car il fallait faire avec une permanente pénurie de bois. À peine un convoi arrivait-il que chaque atelier s’efforçait de se faire attribuer les plus belles pièces. Retailler depuis les membrures de navires et les charpentes de maisons démontées ne suffisait pas toujours à satisfaire la demande.

Rufin touillait une petite compotée d’oignons aux lentilles avec un air gourmand. Avec les années, sa silhouette s’empâtait de plus en plus. Travaillant la plupart du temps assis à façonner des arcs ou à ajuster des assemblages, il prenait chaque année un peu plus de ventre, contrairement à Gauguein qui déambulait sans cesse sur les chantiers. Même lorsqu’il avait achevé son ouvrage, il trouvait toujours à aller interroger les plus savants, questionner les plus inventifs. Avec le camp de siège, il était à la fête, admirant avec émerveillement les engins qui lançaient toutes sortes de projectiles, pierres et traits, ou les gigantesques tours d’assaut roulantes.

Il suspendit une outre de vin léger à leur cabane et se laissa tomber au sol, dénouant ses souliers pour se masser les pieds.

« je suis fourbu, j’ai couru jusqu’au camp de la porte sud ce jour d’hui.

— Quelque nouvelle attaque en préparation ?

— Je ne sais. C’était juste pour faire connoissance d’un ermin[^ermin], fuyard du comté perdu. Fort savant en engins, et capable d’en remontrer en mathématiques à un clerc féru de comput[^comput].

— Tu lui voulais quoi ?

— J’ai ouï dire qu’il acceptait des valets. Je me disais que ce serait bonne occasion d’en apprendre plus. »

Ruffin soupira. Ils avaient une situation tranquille dans l’hôtel d’Ibelin, partageant un manse confortable, et avec la perspective d’avoir chacun le leur dès qu’ils auraient trouvé un parti convenable pour s’établir. Il désespérait parfois de constater combien son frère ne tenait pas en place. Il ne le dissuadait pas pour autant de tous ses projets, reconnaissant qu’il s’agissait souvent de décisions judicieuses. Il fallait juste que quelqu’un s’occupe de l’intendance de façon appropriée. Vu qu’il était l’aîné, il s’en attribuait la responsabilité, car il estimait qu’il était de son devoir de veiller sur son cadet.

« J’ai eu l’occasion de discuter un peu avec lui. Mestre Paylag est quelque peu rude d’abord, mais de savoir que j’ai un frère maître juré facteur d’arbalète l’a beaucoup intéressé. Lui est maçon, mais son père fabriquait des arcs, à la manière des Turcs. Tu devrais venir avec moi le rencontrer. Il te plairait.

— Qu’as-tu donc en tête ?

— Rien de spécial. Je me disais juste que notre sire Hugues[^huguesibelin] ne verrait pas d’un mauvais œil que nous sachions établir pierrières et bricoles[^bricole], mangonneaux et béliers. C’est ce savoir que nous pourrions apprendre de mestre Paylag.

— Qui sert-il ?

— Il a suivi les Courtenay et appartient à l’hostel le roi désormais. »

Rufin grimaça. Avec la mort du vieux Barisan, la famille d’Ibelin avait été déchirée à cause du conflit entre Baudoin III et sa mère Mélisende. Au service de cette dernière, le connétable du royaume, Manassès de Hierges, avait épousé la veuve d’Ibelin et pris les armes contre le roi. Il avait été exilé l’année précédente, lors de la victoire du jeune prince et les Ibelin commençaient à rentrer un peu en grâce, avec Hugues, qui régnait désormais sur le fief. Mais les rancœurs n’étaient pas toutes éteintes. Il faudrait faire bien attention à ne froisser aucune délicate susceptibilité. Si les hommes de savoir étaient toujours prisés et appréciés, on ne les traitait souvent guère mieux que des destriers au sang vif : rênes courtes et mors serré.

## Jérusalem, citadelle de la tour de David, début d’après-midi du jeudi 29 août 1157

Sous la chaleur écrasante d’un soleil triomphant, hommes et bêtes recherchaient la moindre trace d’ombre. Profitant d’une courte sieste après un dîner frugal, les serviteurs étaient installés à l’abri du mur sud de la cour centrale blanche de poussière. Baset, encore vêtu de sa cotte rembourrée, avait le visage écarlate, trempé de sueur, malgré sa marche lente. Il tenait à la main son casque de cuir de crocodile, moins éprouvant et plus léger que les habituelles protections en métal, et se passait régulièrement un mouchoir sur le front.

Après avoir pris renseignement auprès de valets, il pénétra dans un des vastes dépôts aux puissants piliers, accueillant avec félicité la fraîcheur qui y demeurait tapie, même au plus fort de l’été. C’était là une des zones de l’arsenal, où certains engins de siège étaient entreposés entre deux campagnes. Il entendit et reconnut les voix qu’il cherchait avant de voir les visages de leurs propriétaires.

« La bonne journée, miens frères !

— Jambe Dieu, c’est le gamin ! » répondit d’un air enjoué son aîné Gauguein.

En l’apercevant, Rufin avança d’un pas lourd pour venir l’étreindre à la manière d’un ours, dont il avait désormais tous les attributs physiques. Il devenait aussi de moins en moins loquace, se contentant de hocher ou d’incliner la tête pour souligner les propos de Gauguein d’un grognement inarticulé. Baset leur fit une longue accolade, souriant largement de revoir enfin ses frères.

« Le Guillemot m’a dit que vous étiez là. Quel bon vent vous mène ici ?

— Le comte de Jaffa nous a mandé auprès son frère le roi pour préparer future campagne.

— Il y a de la prise de cité ou de citadelle dans l’air ?

— Je ne sais au juste, mais on va être nombreux, avec la troupe du comte de Flandre. Peut-être que le connétable a ouï parler que Norradin est trop faible à nouveau pour protéger ses villes… »

Rufin haussa les épaules, frappant du plat de la main sur un lot de poutres qu’il était jusque-là en train d’inspecter.

« Tu vas en être ? Tu as ouï parler de quelque chose ?

— Avec les branlements de terre, moult cités sont fragiles. Il y a belle picorée à faire au Nord dit-on…

— Si seulement le roi des Grifons[^griffon] pouvait joindre l’ost… On dit que ses engingneurs sont parmi les plus habiles. Ils ont des machines de guerre d’une incroyable puissance, dont le savoir remonte aux Romains. Tu imagines ? »

Baset hocha le menton sans conviction. Il n’avait jamais pu s’intéresser à la politique en dehors de la cité de Jérusalem et n’avait qu’une idée très vague de la géographie et des territoires au-delà de la Judée. Il n’aimait guère s’aventurer au loin et avait développé un réel talent pour se faire affecter à des tâches qui ne l’envoyaient que rarement par delà l’enceinte, et presque jamais sur les champs de bataille. Il avait d’ailleurs un peu perdu le contact avec ses parents depuis que ceux-ci avaient décidé de s’installer dans un petit manse des faubourgs d’Acre. Son père avait eu envie de retrouver les lieux de sa jeunesse et de profiter de la mer pour ses vieux jours.

« Vous viendrez prendre un pot de bière à l’hostel à la veillée, voire souper avec les miens ? J’ai la place de vous loger si vous voulez. Les gamins seront contents de vous voir.

— Ce sera avec plaisir, on comptait en fait te demander héberge dans les jours à venir. Ici ce n’est pas tant confortable, sans même parler des talents de cuisinière de ton espousée. Outre, tu as des nouvelles de la famille ?

— Rien de l’Heloys depuis long temps. Par contre, Pierre passe parfois ici, et me porte nouvelles de la Colete. Tout va bien pour eux. Je vois Ascelote qui-cy qui-là, elle vient souventes fois à l’hostel. »

Il baissa la voix, retrouva son habituel air morne.

« Elle vient conter ses malheurs à Garsende. Son époux…

— Je l’avais dit au père que c’était male union ! tonna Gauguein. Il faudrait y mettre bon ordre !

— Le père veut pas en entendre parler. Il estime que la femme n’a pas à contredire son époux. Et puis ce sont riches négociants, plusieurs fois jurés du roi.

— Tu parles, elle a eu le cadet, avec presque rien. »

Baset lui lança un regard courroucé. Lui aussi était le plus jeune des frères et il savait qu’il n’avait pas le statut de ses aînés, prestigieux ingénieurs au service du roi. Gauguein lui accorda un sourire désolé et le gratifia d’une bourrade.

« En tout cas, on passera volontiers boire un pichet avant le souper. Peut-être organiser beau repas de fête ce dimanche ? On pourra apporter quelque belle volaille à se partager. »

## Acre, manse de Thomas le regrattier, début d’après-midi du mercredi 17 décembre 1158

Lorsque Baset vit le visage de sa mère, il comprit instantanément que les nouvelles étaient vraiment mauvaises. Il s’attendait au pire et entra dans la petite pièce sombre sans un mot. À son grand soulagement, son père était installé en bout de table, de la paille de seigle éparpillée à ses pieds, des corbeilles inachevées et ses outils pour en faire des paniers répandus autour de lui. Malgré son grand âge et ses doigts tordus, il n’avait pas perdu ses habitudes. Son sourire ressemblait à une grimace et sa voix coassante résonna sans joie.

« Prends un siège, Baset. La mère, verse-lui donc à boire, tu vois bien qu’il a chevauché au froid ! »

Puis il acheva ce qu’il avait entamé, ne lançant plus un regard vers son fils tandis que ce dernier dégustait son verre de vin en combattant son angoisse. Face à lui, Parisete reprisait des torchons, la tête baissée. Elle aussi semblait avoir subitement vieilli. On ne voyait guère ses cheveux d’argent sous son voile, mais son visage était sillonné de rides, ses traits devenant austères avec le temps. Elle aurait pu être une des sévères sœurs de Saint-Jean, se dit Baset. Il risqua quelques regards autour de lui : un bon lit, deux coffres, une batterie de cuisine de qualité, deux niches au mur avec des plats décorés, un tonneau en perce. Ses parents n’étaient toujours pas aisés, mais ils semblaient bien vivre. Il hésitait chaque fois à proposer de l’aide à son père, trop fier pour accepter que quiconque lui porte secours, même sa propre famille.

Le vieux Thomas repoussa finalement son ouvrage, se débarrassa du linge qui lui protégeait les genoux et s’approcha de la table, indiquant d’un mouvement de tête à son épouse qu’il voulait aussi être servi de vin.

« Bon, je sais pas trop comment le dire, alors ce sera court. J’ai reçu mauvaises nouvelles : tes deux aînés ont connu male mort lors des campagnes le roi. Un de leurs compagnons me l’a fait savoir…

— Quoi ? Mais où ça ? Comment ?

— Aucune idée, c’était pendant un assaut. J’ai pas su le détail. Toujours est-il qu’on les reverra pas. »

Lançant des regards de côté à Baset, il finit son verre lentement pour lui laisser le temps de digérer la nouvelle.

« Enfin bon, c’est dit. On y reviendra pas, c’est trop de douleur pour la mère. Tu le feras savoir aux filles. »

Puis il se leva, silhouette courbée par les ans, au pas traînant. Il alla remuer le feu, y déposa une bûchette.

« La mère, tu mettras du lard dans les potherbes[^potherbe] de ce soir. Qu’on soigne notre gamin, avec toute cette route.

— Tu peux demeurer quelques jours, fils ? demanda Parisete, la voix étranglée.

— Non, je dois repartir au plus tôt. Le vicomte m’a donné court congé. Je dois porter quelques messages ici au matin puis remonter en selle aussi vite que possible. Avec le roi au loin… »

Il n’acheva pas sa phrase, voyant les larmes monter aux yeux de sa mère. Il hésita un instant à lui prendre la main, mais n’avait jamais osé une telle familiarité avec ses parents. Il pinça les lèvres, frappa des phalanges sur la table et se leva, subitement fort empressé.

« Il me faut d’ailleurs panser ma pauvre bête, elle m’attend devant l’hostel.

— Je vais te montrer où l’installer. Un voisin a de la place dans son courtil, un appentis où il loge son âne. Les bêtes aiment pas ça, demeurer seules. »

## Antioche, quartier de la porte Saint-Georges, soirée du mercredi 15 avril 1159

Stamatès plissa les yeux quand il pénétra dans le petit local au sol de terre battue. Des effluves d’oignons se mêlaient à l’odeur de sueur et de poussière des voyageurs. Il reconnut de nombreuses langues parmi le brouhaha des discussions et alla se renseigner un moment auprès du gros chauve qui s’affairait à activer les feux. Puis il vint se poser devant une table où deux hommes, l’un poilu et hirsute, l’autre mince et vêtu de la meilleure laine, avalaient un épais brouet où nageaient des tartines de pain. À son approche, ils levèrent la tête d’un air interrogateur.

« J’ai nom Stamatès Mpratos. Vous frères Celtes ? »

Gauguein hocha la tête, invita le soldat à s’asseoir vers eux et fit un signe pour avoir un gobelet de plus.

« C’est bien nous. C’est toi qui va souventes fois au royaume de Jérusalem ?

— Allé une fois seulement, mais bons amis là-bas. J’ai usage de vos coutumes maintenant.

— Tu pourrais porter des nouvelles à notre famille ? On a fait écrire une lettre pour eux.

— Sûr. Je demander à ami sergent le roi.

— Ce serait parfait ! Notre frère, Baset, est aussi de l’hostel le roi. Il n’a pas eu de nouvelles depuis fort longtemps. On a demandé à un ami qui retournait à Acre de dire à notre père qu’on allait devenir soldats grifons l’été dernier, mais depuis… »

Stamatès approuva, lui-même était un vétéran des campagnes et n’avait jamais fondé de famille. Il se consacrait à ses neveux et nièces, heureux de n’avoir pas trop d’attaches.

« Vous partir quand ?

— Aucune idée. Mestre Bonifacius a dit que nous ne serions vraiment membres de l’armée du… basileus[^basileus] qu’une fois que nous serons de votre Église. Nous avons déjà vu par trois fois le prêtre et je ne sais quand nous serons baptisés selon vos rites. D’ici là, nous devons demeurer ici.

— Maintenant que basileus venu réprimander prince Renaud, plus tranquille ici. Profiter belle cité, ancienne. »

Gauguein acquiesça. Ils avaient passé les derniers mois à s’émerveiller de l’incroyable richesse des territoires septentrionaux et la rencontre avec les troupes de l’empereur byzantin les avait ébahis. Ils avaient découvert de nouveaux engins propulsant des boulets de pierre sans l’usage de cordes et de valet tirant dessus, dont le contrepoids était une huche emplie de terre et de gravats. Sa précision et sa puissance avaient arraché des gloussements de joie à Gauguein. De son côté Rufin avait particulièrement admiré sans un mot les armes à torsion, dont les délicats réglages l’intriguaient au plus haut point.

Les deux frères avaient rapidement sympathisé avec les techniciens malgré la barrière de la langue, mais jamais ils n’avaient pu en apprendre autant qu’ils le souhaitaient. C’était là une partie des nombreux secrets de guerre que les byzantins ne dévoilaient à personne. Du coup, quand Bonifacius, un des responsables, était venu leur dire qu’il était toujours possible de se proposer comme recrue, ils avaient sauté sur l’occasion. Il leur fallait abjurer leur foi catholique et devenir orthodoxes, puis se jurer à l’empereur byzantin, mais ils acceptèrent sans trop de regrets, emportés par l’enthousiasme.

Malgré la bonne entente entre les souverains, Gauguein s’était épuisé en plusieurs mois de tractations avec le maître ingénieur du roi pour obtenir un congé de leur service. Ils avaient réussi à plaider pour la valeur diplomatique de cette affectation et l’intérêt qu’il y aurait à apprendre de leurs alliés byzantins. Néanmoins, au fond de lui, il ne se sentait nullement concerné par les questions de loyauté ou d’appartenance religieuse. Il était en quête de savoir et il suivait les pistes qui lui semblaient les plus prometteuses. Rufin avait manifesté plus de scrupules, mais il avait fini par se laisser tenter par les formidables perspectives que cela lui ouvrait.

Rufin fit glisser la lettre au soldat byzantin et lui proposa de boire une autre tournée, à leur bonne fortune à tous. Quand Gauguein leva son gobelet, il arborait un sourire radieux.

« Si le père pouvait nous voir, il serait drôlement fier ! » ❧

## Notes

On ne sait pas grand-chose de la réalité du quotidien des hommes responsables des engins de siège pour le XIIe siècle. Il semblerait que leur statut ait pu être très fluctuant et leur embauche variée. Si on se fie à des sources légèrement plus tardives, il n’y avait pas de corps de métier à proprement parler et les réputations s’attachaient volontiers aux individus. Les ingénieurs arméniens étaient toutefois plébiscités, en particulier pour les techniques de sape. L’art des engins de trait demeurait assez largement répandu, avec des spécificités régionales. Malgré tout, les Byzantins semblent avoir dominé la question, avec leur maîtrise de la pyrotechnie et des systèmes les plus imposants. Il est possible que ce soit dans leurs provinces que le lourd trébuchet à contrepoids ait été conçu, même s’il a été rapidement été copié et adapté par leurs adversaires et alliés.

Depuis quelques années, des chercheurs en histoire des techniques étudient de près l’existence de ces hommes de l’ombre, assaillants ou architectes des défenses. On peut citer le nom de Nicolas Prouteau, dont les travaux sur la question sont toujours du plus haut intérêt. L’auteur espère qu’à la date de sortie de ce *Qit’a* la publication de sa thèse dans une édition accessible ne saurait tarder.

## Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Faucherre Nicolas, Mesqui Jean, Prouteau Nicolas, *La fortification au temps des croisades*, Rennes : Presses Universitaires de Rennes, 2004.

Rogers Randall, *Latin siege warfare in the twelfth century*, Oxford : Oxford University Press, 1997.
