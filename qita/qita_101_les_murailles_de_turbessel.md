---
date : 2020-03-15
abstract : 1148-1161. Héritier d’un comté qui n’en finit pas de disparaître, Joscelin de Courtenay grandit dans l’ombre d’un père batailleur et opiniâtre. Quels enseignements tirera-t-il de cet exemple lorsqu’il sera temps pour lui reconquérir ses terres ?
characters : Joscelin de Courtenay, Agnès de Courtenay, Baudoin III, Joscelin II de Courtenay, Béatrice de Courtenay 
---

# Les murailles de Turbessel

## Forteresse de Turbessel, fin d’après-midi du jeudi 22 avril 1148

Le jeune homme avait les joues en feu d’avoir couru tout l’après-midi, occupé à s’affronter à divers jeux de force dans les environs avec ses camarades. Il grimpa la volée d’escalier qui menait au passage longeant les créneaux sans effort. Il ne ralentit qu’en découvrant son père Joscelin, le comte d’Édesse, appuyé sur la muraille grise, le regard perdu au loin. À côté de lui se tenait, immobile, le messager arrivé quelques instants plus tôt sur un roncin éreinté. La mine austère et l’air pincé du comte n’auguraient rien de bon, d’autant qu’il avait près de lui un de ses cruchons de vin qui ne le quittait guère depuis quelques mois.

Joscelin le jeune prit le temps de retrouver son souffle, ajusta sa cotte, espérant lui redonner un aspect décent en tirant dessus et en la frottant rapidement des plus grosses taches de poussière. Puis il s’avança, d’un pas prudent.

« Enfin tu te présentes à moi ! Je t’ai fait mander voilà bien long temps ! Penses-tu qu’il sied à fils de comte de courir les chemins comme manant ? »

Son père le toisait d’un regard courroucé, ses yeux injectés de sang et son dodelinement de tête trahissant l’abus de boisson. Le jeune homme se contenta de baisser le menton, se préparant à affronter la tempête sans un mot.

« Ta mère me tance que tu t’adonnes à jeux de vilains ! À quoi as-tu donc passé ta journée ? Que faisais-tu ?

— Nous avons joué au cheval fondu avec Kostan, Godebert et Sempad. »

Le comte le foudroya d’un œil sévère.

« Me prends-tu pour nigaud à ne citer que fils de loyaux chevaliers de mon hostel ? Il s’en trouve bien plus de compères pour pareil amusement… »

Il grogna, s’accorda une gorgée de son pichet, lança un regard sur le messager, toujours immobile à ses côtés, comme s’il le découvrait. Puis il le chassa d’un geste agacé de la main, avant d’avaler encore un peu de vin. Sa voix se fit moins dure quand il reprit la parole.

« J’ai espoir que tu as gagné, au moins. Que nos gens sachent qui sont les maîtres. »

Le jeune homme acquiesça, gardant le silence. Son père l’invita à ses côtés et lui montra le paysage qui s’ouvrait devant eux à l’est, par delà les collines qui bordaient la vallée du Sadjur.

« J’ai parfois l’impression que le vent me porte les senteurs des jardins de l’étang^[De la ville d’Édesse.]… Ce matin il m’a bien semblé entendre sonner la grande cloche de Saint-Jean… »

Il se tourna vers son héritier, le visage déformé par la colère et le dégoût.

« Au final, les barons de France, de Flandre, de Franconie ne vont rien faire pour nous ! Ce mâtin de Raymond[^raymondpoitiers] a réussi à faire fuir ce moine de Louis[^louis7]. Il aurait besogné sa propre nièce, le fils à putain ! Ils préfèrent les jardins de Damas à nos montagnes, dirait-on, et ils escomptent se tailler beau royaume plutôt que nous venir en aide. Seule Mélisende, qui a du sang des nôtres, comprenait cela, mais elle n’a rien pu faire…^[Allusions aux événements de la seconde Croisade, qui a décidé d’aller assiéger Damas au lieu de porter assistance aux territoires du Nord, comme c’était espéré lors de l’appel.] »

Il secoua la tête, s’abandonnant au dépit, les yeux larmoyants. Il se chercha un peu de réconfort dans la boisson, s’essuya la bouche avec agacement, comme si ses lèvres le démangeaient.

« Je ne vais pas me contenter de Marasse, Cresson, Bile et Hazart, que croient-ils ? Je vais repasser l’Euphrate et reprendre ce qui est mien. Et tu chevaucheras à mes côtés, fils. Qu’ils voient que nous n’allons rien céder. Bientôt nous presserons les grappes de Sarudj entre nos doigts pour en goûter le suc ! »

Il posa un bras protecteur sur les épaules de Joscelin, le serrant contre lui avec vigueur.

« Bientôt nous arpenterons les doubles murailles de Roha[^roha], notre ville… »

Le jeune homme souriait de voir son père reprendre ainsi confiance, défiant leurs ennemis invisibles, cachés dans les replis montagneux vers l’orient et le nord. Il avait déjà chevauché à ses côtés et n’aspirait à rien de plus que de se montrer à la hauteur de cet homme irascible et vindicatif, batailleur et sanguin, au courage indomptable.

## Forteresse de Turbessel, veillée du lundi 26 septembre 1149

Le jeune Joscelin aimait venir en soirée échanger avec les sentinelles, partager les anecdotes des vieux guerriers, faire rouler le dé à l’occasion, fraternisant avec les simples soldats. La lune presque inexistante en son dernier quartier n’occultait que de rares étoiles dans son entourage proche et un air sec et froid permettait d’admirer leur scintillement. Quelques nuages effilochés depuis l’ouest grignotaient de ténèbres le ciel et annonçaient de la pluie. Au sommet d’une des tours, un brasero maintenu bas offrait l’occasion de se réchauffer les mains. On y plongeait les flèches destinées à embraser les machines des attaquants s’ils osaient se mettre à portée, ce qui n’était guère arrivé jusqu’à présent.

Cela faisait plusieurs jours que les seldjoukides de Konya, menés par le fils du prince Mas’ud, Kilig Arslan, établissaient leur tentes et leurs engins d’assaut autour des murailles de Turbessel. Ils avaient déjà pris Marash et semblaient bien décidés à ne rien laisser du comté aux souverains latins. Des feux de camp parsemaient la plaine et les rives du cours d’eau. Il était d’usage d’en entourer les abords d’une citadelle assiégée afin de paraître plus nombreux qu’on ne l’était vraiment. Même sans cela, le moral des défenseurs était au plus bas. Nul renfort n’était prévu et leurs maigres ressources semblaient dérisoires face aux ennemis qui dévoraient la province au fil des mois. Depuis qu’il était en âge de suivre les opérations militaires de son père, Joscelin n’avait vu que des retraites et des replis, l’abandon de forteresses et de villes, au profit de leurs adversaires musulmans. On aurait dit que les montagnes en étaient emplies, tellement ils se pressaient, innombrables, à leurs portes.

« Tu viens préparer les défenses pour demain, fils ? »

La voix éraillée du comte Joscelin semblait déchirer les sons veloutés de la nuit. Emmitouflé dans un manteau doublé de fourrure, il s’avança vers son fils. Son haleine puait le vin à plusieurs pas.

« Tu aimes à cracher sur ces criquets, toi aussi ? Qu’ils viennent donc nous chercher, que je les transperce de ma hampe ! »

Il s’assit sur un des créneaux, s’adossa à un des merlons.

« Ils espèrent nous effrayer avec leurs lueurs nocturnes, mais c’est là ruse d’enfant. Les vrais adversaires avancent toujours dans l’ombre, mon garçon. Quand je pense que nous avons tendu la main à Kara Arslan, lorsque le prince d’Alep le menaçait… Voilà maintenant que l’héritier de Sanguin[^nuraldin] s’entremet pour nous aider face à Hisn kayfa et Kharput. »

Il se tourna vers son fils, le fit s’approcher.

« Tu étais là, ce tantôt, lorsque ce messager nous a déclamé l’offre du seigneur d’Alep. Qu’en dis-tu ?

— Allons-nous accepter ? Il semble bienveillant à notre égard, non ?

— N’aie aucune fiance envers ces serpents. Si Noreddin désire la paix avec nous, ce n’est pas pour nous sauver, mais pour éviter que son ennemi Kara Arslan ne devienne trop fort. Il n’est pas encore prêt à venir moissonner ici, tout à ses propres affaires. Il ne nous tend la main que pour nous étouffer en son temps.

— Allons-nous refuser, alors ? »

Le comte Joscelin soupira longuement, lança un sourire crispé à son fils.

« J’ai besoin de répit, même s’il m’est vendu fort cher. Antioche est bien bas, mais au moins sommes-nous débarrassés de ce maudit Raymond. Seulement, rien ne viendra de nos cousins du sud. Il me faut jouer l’un de mes ennemis contre l’autre, comme le vieil Anur[^anur] a su si bien le faire, se gaussant de nous comme de Sanguin[^zengi].

— Alors ne pourrons-nous poindre contre ces misérables, devrons-nous traiter comme Griffons[^griffon] tortueux ?

— Ah, que voilà bien mon sang qui s’exprime par tes lèvres ! Aurais-je une pleine échelle de bacheliers tels que toi et nous bouterions cul par-dessus tête tous nos adversaires. Mais la vaillance et le bras du noble guerrier ne suffisent plus en ces temps. Il faut y ajouter pointes de fourberie et de rouerie, marchandages comme ignoble négociant de fripes… »

Il se pencha, à presque toucher le visage de son fils de son front, chuchotant d’une voix lourde, empâtée par l’alcool.

« Ici, c’est Turbessel[^turbessel], la forteresse de ton grand-père, le fief de notre famille. De là nous déferlerons sur nos ennemis, lance au poing. Rien ne nous arrêtera, je te le jure. Et s’il me faut faire bonne figure face aux serpents durant un temps, que cela soit. Je leur ferai ravaler leurs sourires quand j’aurais retrouvé ma selle et ma hampe de frêne. »

Joscelin le jeune opina avec passion, grisé par le charisme de ce père dont il sentait bien les failles, malgré toute la vaillance et les bravades. Il l’avait déjà vu piquer des deux, renversant ses adversaires comme fétus insignifiants. Il savait que rien ne pouvait le vaincre quand il brandissait sa lance. C’était un homme de guerre, qui ne se plaisait que l’épée en main, le heaume sur la tête.

## Forteresse de Turbessel, matin du mardi 15 août 1150

La grande salle d’audience résonnait, vide de ses tapis, tentures, meubles et coffres. Seule demeurait la chaise curule sur l’estrade, siège du seigneur des lieux. Joscelin avait néanmoins insisté pour emporter celle de sa famille, la remplaçant par un fauteuil habituellement attribué aux invités de marque. Il faisait les cent pas, toujours réticent à obtempérer aux injonctions de sa mère Béatrice. En retrait, sa sœur Agnès s’efforçait de se tenir à l’écart de la tempête, l’ayant vu couver au fil des jours jusqu’à l’explosion d’aujourd’hui.

« N’y a-t-il donc plus que des tonsurés en ces provinces ? Baudoin[^baudoinIII] va-t’il passer le froc ? Qui est-il pour décider pour moi ce qu’il en est de ma terre ?

— Silence, tu n’es pas encore comte ! Ton père est toujours vif !

— Je le sais fort bien, mais au moins je ne m’empresse pas de le trahir, à peine est-il captif ! »

Béatrice foudroya son fils du regard et sa main partit avant même qu’elle ne le réalisât, claquant dans la salle vide comme le tonnerre. Abasourdi, le jeune prince écarquilla les yeux, outragé de l’affront, mais incapable d’y répondre, ne serait-ce que de la voix.

« Apprends à te taire, mon garçon ! Ton père a le sang bouillonnant et vois où cela l’a mené : en geôles sarrasines dont je le dois libérer. Et crois-tu que besants sortent du cul des vaches ? L’or byzantin servira à cela, à négocier son retour !

— De quoi me parlez-vous ? Vous vendez nos places fortes à l’encan, il ne nous restera rien à défendre quand il sera de retour. Pensez-vous qu’il vous remerciera ?

— *Tout fut à autrui, tout sera à autrui*. Je fais preuve du bon sens qui l’a déserté plus souvent qu’à son tour !

— Comment osez-vous…

— Je le dis, car j’en ai le droit. J’ai vu nos terres rongées par nos adversaires parce que ton père n’a jamais su faire profil bas, ne serait-ce qu’avec Antioche ! Puissé-je au moins t’enseigner cela : il est un temps pour chaque chose. »

Joscelin se frotta la joue, encore brûlante du soufflet. Il sentait crisser sous ses doigts le duvet qui affirmait aux yeux de tous qu’il devenait un homme.

« Je pourrais demander au roi quelques lances et soudoyer de quoi garnir nos forteresses, pendant que vous négocierez la libération de père.

— Crois-tu que cela n’ait pas été envisagé ? Penses-tu un seul instant que je laisse ces terres de gaieté de cœur ? Au moins le basile[^basileus] griffon pourra les garder face à nos ennemis et il sera plus aisé de lui prêter hommage le moment venu, aux fins de les récupérer.

— Prêter hommage pour franc-alleu que nous ne tenions de personne ? Père ne saurait tolérer cela !

— En ce cas, il n’avait pas à se faire prendre. Il n’y a nulle excuse à la défaite quand on est souverain de ses terres. »

Joscelin se mordit la joue, la lèvre. Béatrice avait raison et, au final, il partageait plus ou moins son point de vue. Pourtant, il bouillait de constater que la bravoure de son père ne les avait menés qu’à la ruine. Il ne voyait pas où des erreurs avaient été commises, quels faux pas ils avaient faits, à quel moment ils avaient déchu de leur rang.

« Quoi qu’il en soit, je ne saurais admettre que Turbessel demeure en mains foraines. Je fais serment que nous y reviendrons, en maîtres.

— Puissent Dieu et tous les saints t’exaucer, fils » souffla sa mère, soudain lasse.

Agnès s’approcha et attrapa la main de son frère, la lui serra en silence, un mince sourire sur son fin visage. Dans ses yeux, il vit qu’elle serait là, le jour où ils seraient de retour chez eux. Et qu’elle ne ménagerait pas sa peine pour faire advenir ce jour. Il la prit dans ses bras pour une longue étreinte.

## Jérusalem, palais des Courtenay, comtes d’Édesse, soir du jeudi 4 juillet 1157

La venaison en sauce caramélisée répandait ses effluves dans toute la pièce. Le jeune Josselin avait participé à une chasse les jours précédents et avait pu rapporter de l’antilope et quelques oiseaux. Cela le mettait toujours d’excellente humeur d’avoir parcouru la campagne, même s’il regrettait de ne pas galoper dans ses propres terres. Il s’était néanmoins fait à la vie de baron royal, membre de la Haute Cour pour les quelques domaines que Baudoin III lui avait données en fief autour d’Acre.

Il avait pourtant tout récemment perdu la Tour de Plomb, une forteresse du nord qu’il avait réussi à enlever aux musulmans d’Alep après un difficile siège. Il avait fait ensuite remonter les murs en scellant les maçonneries de plomb, espérant ainsi se garantir des assauts ultérieurs. Seulement, sa victoire fut de courte durée et la tête de pont depuis laquelle il escomptait lancer la reconquête du comté lui avait été rapidement reprise.

Leur mère ayant déserté le palais pour la côte, fuyant les grosses chaleurs du plus fort de l’été, il se retrouvait à table juste avec sa sœur Agnès. Bien qu’il ait toujours été proche d’elle, il la battait froid depuis quelques mois, depuis qu’il avait appris qu’elle fréquentait en secret le comte de Jaffa, Amaury, le frère du roi. Ce dernier était un homme porté sur le beau sexe, dont la réputation de coureur de jupons n’était plus à faire, et le fait qu’Agnès ait pu succomber à ses avances alors qu’elle était promise à un autre, c’en était trop pour le jeune Joscelin.

Ce soir, Agnès faisait pourtant de nombreux efforts pour nouer conversation, relançant la discussion aussi laborieusement que Joscelin la laissait s’éteindre. Elle finit par avouer pourquoi elle déployait tous ces trésors d’amabilité : elle avait une excellente nouvelle à annoncer à son frère.

« Tantôt, j’ai eu longue discussion avec le sire comte de Jaffa, il a été sensible à notre situation, et il va proposer à son frère de te nommer maréchal, Saint-Amand ayant été pris à Panéas le mois dernier[^voirqitaliaisonchetif]. »

Joscelin fit la grimace, avala un morceau de pain, puis grogna d’une voix maussade.

« Tu sais que je n’aime guère apprendre que tu entretiens le sire comte de Jaffa. On parle déjà bien assez dans mon dos. Même les valets se gaussent de toi et de…

— De quoi ? Que comprennent-ils à ma vie, à notre vie, Joscelin ? Il se trouve que le comte a quelque amitié pour moi, est-ce donc ignominieux que de le sensibiliser à nos soucis ?

— Tu sais très bien de quoi je parle ! Je croise souventes fois Hugues de Ramla, ou ses frères. Que puis-je leur dire ? Ils nous ont fort bien traités et voilà que tu leur fais terrible affront !

— Oui, eh bien, tu ne risques plus guère de voir Hugues…^[Fait prisonnier en même temps qu’Eudes de Saint-Amand, vers Panéas.]

— N’ajoute pas l’irrespect à la trahison !

— Et toi ne te crois pas sire en son fief, je suis ton aînée !

— En l’absence de père, j’ai devoir de veiller sur toi et ta réputation. »

Agnès lui lança un regard qui aurait paralysé de peur n’importe quel inféodé, mais Joscelin était immunisé contre les œillades assassines de sa sœur. Voyant cela, elle se contenta de faire mine de se concentrer sur le délicieux plat. Elle en picora deux ou trois bouchées avant de revenir à la charge, mais d’une voix plus douce.

« Joscelin, je sais que tu désapprouves ma conduite, mais sois assuré que je ne m’abaisse en rien. Hugues a parfaitement compris et demeure un ami proche de mon cœur. C’est juste qu’il n’a plus la première place. Elle est pour Amaury, et il a pour moi tendres sentiments. C’est pour cela qu’il pense à moi, à nous. Il nous considère comme siens, et il veut par cette offre nous faire témoignage de son souci pour nos intérêts.

— Alors pourquoi ne parle-t-il pas de mariage si ses intentions sont si nobles ?

— Peut-être que la Haute Cour ne sait pas tout, mon ami. Il se dit entre lui et moi certaines choses qui n’ont pas à tomber dans toutes les oreilles. »

Interloqué, Joscelin scruta le visage de sa sœur, dans l’espoir d’y lire quelque indication sur ce qu’elle insinuait, sans pour autant l’affirmer. Peut-être dans la crainte de tenter le destin et de se faire cruellement punir.

« Soit. Passons. Mais je n’ai pas désir de m’installer ici, au rebours de toi. Père ne saurait entendre que j’ai abandonné tout espoir de reprendre Turbessel et nos terres.

— Tu te lamentes souventes fois que tu n’as pas les moyens qu’il te faudrait. Apprends donc de l’expérience du vieil Onfroy[^onfroytoron]. Sa science des arts de la guerre te sera utile. Tu pourras te faire bons camarades de bannerets et soudoyés. Emplis ton grenier des viandes avant de partir en chevauchée ! Jamais je n’attendrais de toi que tu te contentes de vivre de tes fiefs d’Acre. Mais il nous faut apprendre la patience, toute chose que père n’a pas su nous enseigner. »

Joscelin acquiesça, souriant pour la première fois de la soirée. Il avait eu tort de désespérer d’Agnès. Elle demeurait une femme forte, ambitieuse, et sa sœur aimante. Ensemble, ils ne redouteraient jamais aucun ennemi, ensemble ils viendraient à bout de l’adversité. Et ils parcourraient de nouveau les sentiers des rives du Daysan.

## Jérusalem, palais royal, chambre du roi Baudoin, matin du mercredi 8 février 1161

Avant les audiences officielles et sessions de la Haute Cour, Baudoin avait pris l’habitude de voir ses proches pour échanger avec eux. Il profitait généralement du matin pour recevoir dans l’intimité de sa chambre, en présence de sa jeune épouse. Comme elle n’avait qu’une douzaine d’années lors de leur mariage, ils avaient longtemps dormi séparément. Les années passant, ils avaient décidé de partager le même lit depuis quelques mois et tout le royaume espérait qu’un enfant naîtrait bientôt, garantissant la continuité de la dynastie.

Affable et facile d’abord, Baudoin était très familier avec ses invités, se comportant généralement de façon amicale et ouverte, se levant de temps à autre pour servir son épouse ou appeler un valet. Il avait demandé la veille à Joscelin de le rejoindre pour son en-cas du matin, fruits au vin et biscuits au sésame, car il voulait l’entretenir d’une idée qui lui était venue.

Le temps d’avaler la collation, Baudoin se contenta de parler de menus détails de leur vie de palais, de choses sans importance. Il s’était entiché d’un cheval et, sachant parfaitement que Joscelin était adepte de la chasse, lui avait proposé de partir un de ces jours tester la valeur de la bête. Tout en se caressant la barbe du geste machinal qui ne le quittait que peu, il échangeait sur la vision qu’ils se faisaient de la monture idéale pour courir le lion ou la gazelle. Puis, repoussant sa coupe, il fit un grand sourire à celui qui l’avait servi comme maréchal durant plusieurs années.

« Tu te doutes que je ne t’ai pas demandé de venir pour simplement parler destrier et palefroi, même si je t’y sais fort savant. »

Il assortit sa remarque d’un sourire complice.

« L’affaire qui me préoccupe est bien plus grave pour la couronne, et pour les provinces d’ici. Tu as sans nul doute appris le départ de Renaud de Saint-Valéry, qui tenait Harenc[^harenc] ? »

Baudoin savait très bien que nulle information concernant le septentrion n’était ignorée de Joscelin et n’attendit pas la réponse.

« Avec la capture du prince Renaud[^renaudchatillon], les terres du Nord se trouvent bien dégarnies. Là-bas, tous les barons d’importance ont été pris, tués ou mis hors d’état. Ce me serait d’une grande aide que tu considères de devenir sire d’Harenc. J’ai bien conscience que ce serait déchoir quelque peu peu pour toi, car cela demande de prêter hommage à Antioche.

— Je n’ai pas le genou si roide que mon père, paix à sa mémoire.

— Voilà ce qui me pousse à te prier d’accepter : je te sais féal et de bonne sapience. Il me faut des hommes tels que toi là-bas, pour garantir nos frontières avec le sire d’Alep, fort entreprenant. J’ai l’intention de proposer au patriarche Aimery de devenir bayle[^bayle] d’Antioche, le temps pour le jeune Bohémond de prendre les rênes. Mais je crains que la princesse Constance soit de la même étoffe que sa mère et n’y entende guère des intérêts autres que les siens.

— Vous me faites grand honneur, mon sire. Je ne pourrais dire à quel point votre fiance me ravit le cœur.

— Nous sommes désormais presque frères, par le mariage de ta sœur. Il me semble de raison que nous nous gardions les uns les autres en ces temps troublés. Te savoir à Harenc me procurera quelque répit en mes soucis.

— Je ferai tout pour me montrer digne de vous, mon sire. »

Baudoin offrit un large sourire à Joscelin et leva sa coupe.

« Puisse cette première forteresse ouvrir la voie vers Roha, sire comte d’Édesse. » ❧

## Notes

L’histoire de la famille d’Édesse, les Courtenay, apparaît étroitement liée au destin de la couronne de Jérusalem à partir des années 1150. Pourtant, ce sont aussi les héritiers du premier territoire latin en Outremer, pétris de culture arménienne. La présentation qu’en fait Guillaume de Tyr en biaise la perception, et il me paraissait intéressant d’aller creuser un peu dans les vies de Joscelin et Agnès, dont les agissements seront si déterminants par la suite.

On brosse habituellement un portrait très noir de Josselin II, dont l’ombre a dû planer longtemps sur le destin du fils. Pourtant Josselin III ne donne pas l’impression d’avoir démérité au long de sa carrière, même s’il semble avoir été quelque peu en retrait. Il a néanmoins tenu des rôles importants, et le silence de Guillaume de Tyr sur ce point peut laisser penser qu’il n’y avait pas matière à critiquer, la plus petite occasion de le faire à l’encontre d’Agnès étant saisie avec empressement par l’évêque de Tyr. En outre, l’attitude de l’historien de la Terre sainte envers Renaud de Châtillon, dont il minimise les accomplissements, quand il ne les attribue pas carrément à d’autres, peut inciter à la prudence quant à l’effacement de Josselin lorsqu’il occupait des postes de premier plan. Quoiqu’il en soit, il me paraissait intéressant de brosser à grands traits quelques moments qui auraient pu être d’importance dans la jeunesse de cet éminent membre de la Haute Cour de Jérusalem.

## Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Amouroux-Mourad Monique, *Le comté d’Édesse 1098-1150*, Paris : Librairie orientaliste Geuthner, 1988.

Robert Lawrence Nicholson, *Joscelyn III and the Fall of the Crusader States 1134-1199*, Leiden : Brill, 1973.



