---
date: 2013-08-15
abstract: 1157. En plus de l’état de guerre quasi-permanent pendant plusieurs siècles, le Moyen-Orient connut de fréquents séismes durant les croisades. Les dévastations furent parfois gigantesques, aux dires des chroniqueurs. Bien évidemment, de telles catastrophes ne manquèrent pas d’inciter les adversaires qui se côtoyaient là-bas à saisir les opportunités offertes par le désarroi de leurs ennemis.
characters: Gosbert, Guilhem Torte, Toufik
---

# Branleterre

## Damas, sud de la Ghûta, fin de matinée du lundi 12 août 1157

Douleur. Dans les jambes. Visage écrasé sur des pierres. La bouche et le nez emplis de poussière. Un nuage de cendre voilait les environs.

Des bourdonnements à ses oreilles occultaient les cris, distants, étouffés. Guilhem tenta de soulever la tête, désorienté par la secousse, abruti par le choc.

Il grimaça lorsqu’il se mit sur les fesses, se frottant les joues comme pour en arracher le linceul poudreux. Il repoussa des mains les gravats qui avaient cherché à l’ensevelir. Il pouvait bouger les pieds, cette découverte le réconforta. Secouant son crâne doucement, il s’efforçait de comprendre ce qui se passait autour de lui.

Tout semblait calme, des voix criaient par moment, comme au ralenti. Quelques animaux braillaient, des chiens aboyaient au loin, et l’un d’eux hurlait à la mort. Le bâtiment sur lequel ils avaient travaillé ces dernières semaines n’était plus qu’un monceau de ruines éparpillées parmi les arbres fruitiers. Des silhouettes s’y discernaient avec peine, mêlées à la gangue de pierre, de mortier et de bois.

Il toussa, cracha à de nombreuses reprises. Il était couvert d’ecchymoses et se sentait trop désorienté encore pour se lever, mais il était sauf. À quelques pas de lui, il voyait un pied émerger des décombres, la jambe de son propriétaire disparaissant sous un des pans de mur écroulé. Un de ses compagnons d’infortune s’approcha à pas lents, une outre à la main. Un jeune captif à la peau noire, originaire du Soudan. Sans un mot, il lui tendit la gourde et s’accroupit à ses côtés, scrutant avec effarement devant lui.

L’eau fit le plus grand bien à Guilhem. Son cerveau se remettait en marche, méthodique, affairé. Il tourna la tête en tous sens, à la recherche des soldats qui les surveillaient. Ils n’étaient qu’une poignée d’esclaves employés sur ce bâtiment, aux abords des tombes de Suhayb al-Rumi et Umm’Atika, tout près du maydan al-Hasa. Les gardes s’étaient réfugiés à l’intérieur, profitant de l’ombre pour se reposer au plus fort des chaleurs.

Seul le petit groupe affecté à la préparation des repas et à l’approvisionnement en eau et lui, à la gâche du mortier, demeuraient dehors, les travaux se poursuivant à l’étage. Il sourit, heureux de son sort. Le soudanais le dévisageait désormais, effrayé, interdit. Il le rassura d’une voix rauque, expliquant qu’ils tenaient là une occasion unique. En se relevant, il tituba, surpris par la douleur qui rayonnait de ses jambes. Le gamin attendait, toujours aussi inquiet.

Retrouvant peu à peu son équilibre, Guilhem se traîna jusqu’aux décombres. C’est alors qu’il ressentit de nouveau un tremblement, une oscillation. Un nouveau séisme. Perdu au milieu des jardins, allongé sur le tas de gravats, il ne risquait plus rien. Mais la peur était la plus forte et il se figea, fermant les yeux tout en priant à voix basse. La secousse fut brève, peu importante et il reprit son examen sans tarder. Il cherchait tout ce qui pouvait lui servir, amassant les objets, les vêtements, sans discernement. Il ramenait tout à une pile qu’il faisait près de l’endroit où ils attachaient l’âne qui les accompagnait.

L’animal s’était enfui, mais il demeurait le panier empli des repas : pain, olives, quelques fruits séchés. Le gamin le voyait faire et se mit à l’imiter. Retrouvant son assurance coutumière, Guilhem dépouillait sans vergogne les corps pas trop exposés et récupéra même les couteaux qui les équipaient, ainsi qu’une masse de belle allure. Celle de Kalil. Il jetait parfois un regard inquiet autour de lui, dans la crainte que l’on vienne s’enquérir de ce qui s’était passé là. Il ne savait pas que, dans la ville, chacun courait après sa famille, s’extrayait des bâtiments écroulés avec l’aide des voisins, des amis. Nul ne se soucierait de son sort avant longtemps.

Il chercha un outil qui lui permettrait de se débarrasser de l’anneau de fer qui le marquait comme un esclave. Un burin, utilisé pour retailler les moellons fut sa seule trouvaille et il ne parvint pas à entamer le solide bracelet. Inquiet à l’idée de perdre du temps, il préféra l’attacher d’un chiffon le plus haut qu’il pouvait, et le dissimula sous la manche d’un large manteau. Il s’équipa de tous les vêtements qu’il pouvait dénicher, échangea ses souliers francs, usés et déchirés, contre des savates de belle facture. Puis termina par s’enrouler un turban sur la tête, sans grand talent, mais avec obstination.

Il espérait que cela suffirait pour qu’il rejoigne les territoires chrétiens. Il devait partir à l’ouest, sans être bien certain du temps qu’il lui faudrait pour cela. Son plan était simple : il était un marchand ruiné par le tremblement de terre à Damas qui s’empressait de retourner chez les siens, sur la côte. Il savait suffisamment bien parler arabe pour se faire comprendre. Tant qu’on ne voyait pas son bracelet, il pouvait faire illusion. Quand les autorités se rendraient compte qu’il avait filé et n’était pas enseveli parmi les vestiges du bâtiment, il serait loin.

Le garçon l’avait suivi, d’abord du regard, puis l’avait aidé à entasser tout ce qui pourrait lui être utile lors de son périple. Depuis un moment, il était invisible. Guilhem espérait qu’il n’était pas parti chercher du secours. Plus probablement, il avait tenté sa chance aussi. Mais que pouvait bien un gamin d’à peine une douzaine d’années livré à lui-même ? Il finirait brigand, ou serait repris. Il n’avait même rien récupéré dans les maigres bagages accumulés.

Tandis que Guilhem entassait avec fièvre dans le panier de nourriture tout ce qu’il pourrait échanger ou revendre, il entendit clopiner sur le chemin de graviers. Le garçon revenait avec le baudet en longe. Inquiet, apeuré, l’animal avait les oreilles baissées, et n’avançait qu’en raison des coups de trique obstinés. Guilhem esquissa un sourire. Un marchand avec un domestique et un âne serait certainement moins suspect qu’un homme seul.

## Damas, soirée du youm al joumouia, 12 ramadan 552^[Vendredi 18 octobre 1157.]

Une odeur d’épices et de coriandre flottait encore dans la pièce douillette. Abu Malik étendit un peu les jambes lorsqu’il s’assit de nouveau sur le kursi[^kursi] où il aimait s’installer pour rédiger son courrier. Il n’allait pas jusqu’à utiliser un large plateau de bois comme bureau, comme les Ifranjs, qu’il côtoyait tant, mais s’appuyait simplement sur une écritoire. Un brasero apportait un peu de chaleur en cette fraîche soirée, une lampe à huile à la flamme tremblotante complétait la chiche lumière des claustra. Il entendait dans une salle voisine son épouse jouer avec leurs filles. La rumeur de la rue était tenue à l’écart de la cour intérieure de sa demeure.

Il sortit un feuillet d’un étui de cuir ouvragé et le relit attentivement. C’était une missive à l’intention de frère Raymond, un membre de l’hôpital de Saint-Jean de Jérusalem. Ils étaient en contact régulier, car l’un comme l’autre œuvraient à la libération des captifs, en aidant au versement des rançons, à leur rachat par des âmes pieuses. Bien que fidèle pratiquant de l’Islam, Abu Malik n’était pas un homme belliqueux et préférait entretenir des rapports cordiaux avec les différentes congrégations. Issu d’une vieille famille de marchands damascènes, il savait que les dirigeants allaient et venaient et qu’il était plus sage de ne pas s’aliéner un puissant. Tant qu’il pouvait vivre sa foi en toute quiétude, il était satisfait.

D’ailleurs, il avait toujours plaisir à rencontrer des négociants de la côte, chrétiens nestoriens ou juifs d’Espagne. Il se sentait plus proche d’eux que des intolérants croisés arrivés voilà quelques dizaines d’années par armées immenses, ou même que des Turcs nomades, dont il déplorait la rudesse, jusque dans la langue. Tous ces nouveaux arrivants étaient imbus d’eux-mêmes, avides d’étendre leur pouvoir, et bataillaient sans cesse, plus pour leur propre gloire que dans le souci de propager leur foi, selon Abu Malik.

Il pointa les montants exigés pour la libération de certains prisonniers que leurs propriétaires étaient prêts à relâcher. Il indiquait également ceux qu’il savait avoir rendu l’âme lors de leur détention. Il en arrivait à un nom qui lui rappelait de mauvais souvenirs. Guilhem Torte ! Ce captif avait été tué lors des tremblements de terre de l’été, qui avaient frappé durement les territoires, autour de Damas, et encore plus au nord. Des quartiers entiers n’étaient plus que ruines, des familles broyées sous la pierre et les décombres, des vies anéanties en un séisme dévastateur. Colère divine hurlaient certains, fatalité pensaient d’autres. Abu Malik avait tressailli quand on lui avait raconté l’histoire de cette école effondrée sur ses élèves, ne laissant pour tout survivant que le professeur, sorti dans la cour. Il ne lui restait plus qu’à chercher le salut de son âme dans la prière et le dégoût de lui-même, à celui-là.

Le négociant soupira. Il s’estimait heureux de n’avoir que des filles en cette affaire, pour une fois. Elles n’allaient pas à l’école, et sa demeure était solidement bâtie. Ils n’avaient d’ailleurs eu que peu de dégâts : de la vaisselle cassée et un appentis écroulé sur le toit. Ses entrepôts ne contenaient que fort peu de marchandises, et ce n’était généralement que des étoffes, des tissus qu’il conservait le temps de leur faire prendre de la valeur, de les céder au bon moment. Sa fortune n’était que de papier, des parts dans des cargaisons, des investissements dans des négoces lointains…

Il voyageait de moins en moins, avec l’âge, et préférait demeurer de ce côté de la mer, désormais. À chaque départ, il craignait de ce qu’il trouverait à son retour. D’ailleurs, le matin, lorsqu’il était à la mosquée, il avait discuté avec ses amis de la difficile situation politique de la cité. Ils regrettaient tous le temps de leur indépendance, mais n’osaient guère le proclamer à haute voix. Nombreux avaient été ceux qui, mécontents de leurs précédents dirigeants, avaient placé leurs espoirs dans le chef turc.

L’appel à la prière incluait désormais le nom de ce farouche maître de guerre, Nūr ad-Dīn Mahmud ben Zengi ben aq Sunqur et ses émirs patrouillaient dans les rues. Damas s’éteignait, Abu Malik le sentait bien. Ce n’était qu’une des provinces pour l’homme qui tenait dans sa main aussi Alep et Mossoul. Il ne lui manquait que le Caire, disait-on, pour cueillir les territoires des Ifranjs comme des fruits murs. L’appétit des puissants ne connaissait nulle mesure, surtout quand leur bras maniait le sabre et la lance.

Yusuf ben Ahmed al-Ifriqiya, un de ses vieux amis, leur avait appris la nouvelle au matin : leur prince était tombé gravement malade alors qu’il était en campagne. Il se murmurait qu’il sentait sa fin proche et avait désigné son successeur, son frère Nusrat al-Din. Son fidèle entre tous, Asad al-Din Shirkuh, était arrivé récemment avec son askar pour prendre le contrôle de la cité. Abu Malik n’évoqua pas l’information dans son courrier. Après tout, frère Raymond appartenait à un des ordres les plus réfractaires à la puissance musulmane. Certains d’eux se battaient sous le même manteau que lui, portant le haubert et l’épée. De toute façon, il serait certainement au courant de la nouvelle rapidement. Peut-être qu’ils chercheraient à tirer avantage de la situation. Encore plus de batailles, plus de morts, de blessés et de captifs. N’étaient-ils pas enfin repus de tout ce sang versé, de toutes ces richesses accumulées ?

## Shayzar, soir du vendredi 22 novembre 1157

Le feu crépitait et lançait autant d’étincelles qu’il propageait de fumée. Ils brûlaient tout ce qui leur tombait sous la main et qui pouvait aider à se réchauffer. Jamais Gosbert n’aurait pensé qu’il regretterait aussi vite les chaudes journées de l’été. Depuis trois jours, il se croyait revenu dans sa Flandre natale, avec un ciel gris, bas, et un crachin fin et régulier. La morosité s’était abattue sur l’armée latine en même temps que les gouttes et, rapidement, des cabanes avaient été érigées dans les décombres de la cité.

Les trois osts, celui de Jérusalem, d’Antioche, et les troupes croisées de Thierry d’Alsace, comte de Flandre, avaient fait campagne vers Césarée de Syrie[^shayzar], espérant prendre le contrôle de son imposant pont de pierre enjambant le fleuve de Fer[^oronte]. Les récents séismes qui avaient frappé les territoires musulmans constituaient une vraie aubaine. L’opportunisme militaire faisait fi de toutes les autres considérations.

La quinzaine d’hommes dont Gosbert s’occupait commentait avec enthousiasme les opérations tout en plumant les poulets qu’ils avaient dénichés pour le repas du soir. Ils avaient aussi des pains plats, confisqués à des habitants, ainsi que divers légumes qui mijotaient dans une grosse oule.

« Je vous le dis, en vérite, compères, Dieu a décidé de favoriser le sire comte !

— Le père curé dit que c’est comme à Gerfaut, que le Seigneur fait corner ses troupes et la muraille s’en écroule d’elle-même !

— C’est Jéricho, crâne de piaf !

— Ouais, c’est tout comme. N’empêche que quand le sire comte sera seigneur du lieu, j’espère qu’il nous donnera quelques terres. J’ai repéré bel hostel où il ne me déplairait pas de m’installer.

— Écoutez-le, le père-la-regratte ! Il se croit déjà sire en son fief, le cul au parmi des coussins de duvet ! »

Une vague de rire parcourut l’assemblée, heureuse de son sort. Un des hommes, qui venait de porter du bois se tourna vers Gosbert :

« Je ne comprends pas que les gens d’ici n’aient pas encore bouté tous ces païens hors les murs. Nous n’avons eu guère de mal à les déloger.

— Ceux qui tenaient la ville n’étaient pas là depuis longtemps. Ils s’accrochent à la forteresse en plus, tu sembles l’oublier. »

Le soldat haussa les épaules, narquois.

« Ça sera tantôt réglé. »

Une voix l’interrompit, un autre sergent faisant irruption dans leur courette, secouant sa chape humide. Il avait l’air accablé, le regard bas.

« On doit se préparer à issir hors la cité très prochainement. »

Gosbert se leva, fronçant les sourcils. Tous les hommes s’étaient figés et attendaient d’avoir des explications.

« Mauvaises nouvelles ? Les Turcs font route sus nous ?

— Rien de ce genre. La faute en revient au prince Renaud. Il a exigé hommage du sire comte pour la cité.

— Comment ose-t-il ? Il n’est qu’à un roi que le sire comte doit préséance !

— C’est ce qu’il a répondu, mais le prince n’en démord pas. Il exige hommage, alors le sire comte a dit qu’il ne s’échinerait pas à prendre le chastel par force pour un autre. »

Un des soldats occupé à vider sa volaille la jeta au sol de dépit.

« La peste soit de ce prince fils de chienne ! N’a-t-il pas assez de terres et de cités ?

— La paix ! le réprimanda Gosbert. Mercions plutôt Dieu de nous avoir donne cité à piller. Nous avons jusqu’à demain pour lui faire rendre butin. La nuit sera longue, compères. »

Les sourires qui illuminèrent les faces n’avaient rien de réjouissant. On eut dit des loups affamés, les yeux brillants à l’idée de la chasse. Pourtant nulle fringale ne tenaillait leurs entrailles, à ces fauves-là. Le désir et la violence y prenaient toute la place. ❧

## Notes

Le Moyen-Orient connut de nombreux séismes durant la période des croisades. Ils furent doctement consignés par les chroniqueurs, tel que Ibn al-Qalânisî, parfois avec de grands détails. Selon les zones affectées, cela incita les dirigeants ennemis à tenter des coups de force pour prendre une ville dont les murailles s’étaient écroulées, une forteresse abattue.

La cité de Shayzar, qui abritait la famille du célèbre érudit et diplomate Usamah ibn Munqidh, fut très sévèrement touchée en 1157 et tous, ou quasiment, périrent lors de l’événement.

Des Ismaéliens (la secte appelée par la suite Assassins) et les Francs y virent une occasion unique de capturer l’imposante place forte située sur l’Oronte. Sans succès. Elle demeura finalement dans le giron de Nūr ad-Dīn. Cette période de forte activité sismique coïncida avec des épisodes de maladie pour le dirigeant musulman qui sentit sa fin proche. Il organisa sa succession et se préparait visiblement à décéder. Ses adversaires ne se doutaient pas qu’il vivrait encore longtemps, assez pour mourir la même année que l’héritier du roi Baudoin III de Jérusalem, 17 ans plus tard.

## Références

Élisséef, Nikita, *La description de Damas d’Ibn ’Asâkir*, Institut Français de Damas, Damas : 1959.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Nicolle, David, *The Second Crusade 1148: Disaster Outside Damascus*, Londres : Osprey Publishing, 2009.

Nicolle, David, Kervran, Yann (trad., adapt.), *La Seconde croisade et le siège de Damas en 1148 - Un pari manqué*, à paraître.
