---
date : 2018-05-15
abstract : 1157. Aux frontières des royaumes en guerre, récemment échappé d’un camp de travail, Guilhem Torte tente de recouvrer la liberté. Loin des territoires qui lui sont connu, sans personne à qui se fier, il espère renouer le lien avec son univers familier.
characters : Guilhem Torte, Toufik, Guy Scandélion, Kaspar Paneas, Frère Raymond
---

# Forain

## Mezze, ouest de Damas, fin d’après-midi du lundi 12 août 1157

Ouvrant la voie à Toufik et son âne, Guilhem veillait à s’écarter du chemin dès qu’il apercevait ou entendait des montures. Au plus chaud de la journée, ils s’étaient avancés dans un bosquet de palmiers de façon à ne pas attirer l’attention sur eux. Inquiet de ne pas être pris pour des pillards par les fellahs des environs, Guilhem avait jugé plus sûr de s’installer pour une sieste apparente, tout en gardant les yeux en éveil. Pendant ce temps, Toufik, toujours muet comme une carpe, s’était abandonné à un vrai repos. Ils avaient ensuite amassé un peu de bois mort qu’ils avaient entassé sur le dos de leur bête.

Ils avaient croisé avec crainte quelques cavaliers arrivés trop soudainement pour être évités et même une troupe de Turcs en armes, mais aucun n’avait fait attention à eux. Le tremblement de terre ne semblait pas avoir fait grand dégât[^voirqitabranleterre], mais avait déclenché une vive agitation. Les hameaux qu’ils avaient traversés bruissaient des travaux de remise en état et de l’angoisse qu’avait fait naître la catastrophe naturelle. De-ci de-là, des bêtes s’étaient échappées et s’aventuraient dans des cultures ou dans les zones désertes. Guilhem avait veillé à ne pas s’en approcher plus que nécessaire. Sa crainte était d’être pris à partie et de devoir justifier de son identité. Il pouvait passer pour un paysan aux yeux des voyageurs, mais ne saurait être reconnu comme l’un d’eux par les hommes de la région. Dans chaque lieu, ils se connaissaient tous et avaient l’habitude des visages et apparences des familiers du secteur.

Il avait décidé de s’éloigner le plus possible des faubourgs de Damas en direction de Baniyas et la frontière avec le royaume latin, profitant du temps moins étouffant de fin de journée. Il n’était pas sans croiser d’autres personnes qui s’activaient aussi, maintenant que les ardeurs du soleil étaient un peu retombées. Au détour d’un lacet dans les reliefs méridionaux du Ğabal Qasyun, il s’arrêta pour faire boire l’âne dans un mince filet qui sourdait d’une crevasse surplombant un petit bassin grossièrement aménagé. Il en profita pour jeter un regard vers la cité de Damas en contrebas. Noyée dans son oasis de verdure, elle semblait exhaler des fumées en longues traînées. De si loin, on n’y discernait nulle animation suspecte, pas plus que de patrouilles étirées sur un chemin ou l’autre.

Il allait reprendre la longe de son âne pour l’inciter à repartir quand il entendit soudain le vacarme d’une troupe. Avant qu’il n’ait le temps de trouver une échappatoire, il vit s’avancer à lui une belle caravane, guidée par un homme sur un vieux baudet au poils miteux. Sans se soucier de lui, celui-ci indiqua une halte d’un geste impérieux aux siens, certainement dans l’intention de profiter du point d’eau. Guilhem n’était pas trop inquiet, ils venaient du sud et allaient vers Damas. Il ne pouvait se rencontrer parmi eux des personnes à sa recherche. Il prit également conscience que ce n’étaient que des marchands, escortés de quelques gardes.

Il s’écarta du passage, inclinant respectueusement la tête et s’employa à se remettre en marche. Il n’avait pas fait trois pas que le chef caravanier avait placé sa monture en travers de son chemin. Il n’était pas armé, si ce n’était d’un immense fouet de cuir autour de la taille, et ne semblait pas animé de mauvaises intentions. Il le salua du bout des lèvres et lui demanda d’où il venait.

« Je suis de Berzé, venu à Dimashq pour y vendre quelques affaires, mais la terre y a tremblé si fort que j’ai préféré m’en éloigner.

— Nombreux ici ont leur famille en la cité. Compte-t-on beaucoup de morts ? Des dégâts ?

— Je ne saurais dire, j’étais à peine en la Ghûta et je n’ai pas été en les murs… »

Le caravanier l’examina plus étroitement. Guilhem savait que son accent n’était pas crédible pour la région, ayant appris à parler dans les quartiers d’Alexandrie et en Égypte. Il espérait malgré tout qu’il l’était suffisamment pour passer pour un arabe. Un latin en ces lieux, seul, ne pouvait être qu’un espion.

« Quel est est commerce ? Tu me sembles bien loin de chez toi !

— Je n’ai plus de chez-moi, les Ifranjs ont pris ma cité, Asqalan[^asqalan] et les soldats qui avaient fait jurement de nous faire escorte nous ont détroussés. Je n’ai gardé que ce gamin, seul à en avoir échappé avec moi. »

Son explication lui parut satisfaire la curiosité du guide, qui descendit de son âne et fit signe à tous d’abreuver les bêtes. Bientôt, plusieurs hommes vinrent poser le même genre de questions à Guilhem. Heureusement pour lui, aucun ne sembla connaître Ascalon ni y avoir des relations. C’étaient pour la plupart des marchands de victuailles qui suivaient l’armée de Damas dans les campagnes proches afin de ravitailler les soldats. Ils revenaient du fructueux assaut de l’émir sur la cité de Baniyas[^paneas] et se félicitaient du butin qui avait pour une bonne part fini dans leurs escarcelles. L’un d’eux, rond de visage et fort volubile, se présenta comme le mâalem[^maalem] d’une des zones du souk al-askar. Il était prêt à lui faire un prix sur un emplacement, s’il était intéressé, un de ses marchands ayant malheureusement péri de fièvres. Il adopta un court moment la face  empreinte de dépit qu’il convenait, mais ne s’attarda pas sur une trop longue commisération, conscient qu’il avait là une occasion de récupérer le montant d’une patente qu’il avait perdue par la faute de la maladie.

Comprenant que l’homme était plus enclin à parler de lui et de commerce qu’à s’inquiéter de son identité, Guilhem l’incita à bavarder, notant au passage les noms et les anecdotes dont il émaillait son discours. Quand il vit que la caravane allait reprendre sa route, certainement dans l’espoir d’arriver aux murailles à la nuit, il s’enquit respectueusement de l’endroit où il pourrait visiter le mâalem en ville, dans l’espoir qu’il était, prétendait-il, d’avoir des produits qui seraient utiles à l’armée de l’émir.

Plusieurs négociants l’incitèrent à faire demi-tour, aucune trêve n’ayant été signée avec les Latins, ce qui mettait en grand péril le bon croyant esseulé qu’ils pourraient capturer à leurs frontières. Il fit mine d’accéder à  leurs conseils et affirma être résolu à bifurquer vers Busrâ[^bosra] où il se trouvait d’autres rescapés d’Ascalon. Les marchands l’approuvèrent, ayant entendu que les troupes semblaient décidées à s’affronter plus au nord, là où le soleil ne les cuirait pas dans leurs armures, plaisantèrent-ils. Ils se quittèrent dans d’amicales dispositions, des bénédictions réciproques aux lèvres.

Lorsqu’ils se furent éloignés du petit groupe, de nouveau seuls avec leur âne sur le chemin caillouteux, le visage de Toufik s’illumina d’un timide sourire. Sans rien ajouter, Guilhem hocha la tête et lui en adressa un en retour. Devant eux, le soleil lançait ses derniers feux tout en s’effaçant derrière les reliefs. Il était temps de trouver un endroit pour la nuit, un peu en retrait du passage. La lune serait trop jeune pour leur permettre d’avancer en sécurité.

## Bait Jann, aube du vendredi 16 août 1157

Le chemin semblait n’en plus finir pour les deux voyageurs. Guilhem persévérait dans sa stratégie visant à s’écarter le plus possible de la voie principale, espérant être pris pour un simple fellah occupé dans la campagne. À chaque fois qu’ils le pouvaient, ils louvoyaient d’un côté à l’autre, n’avançant que fort peu dans la direction qu’ils suivaient réellement. Cela leur permit de voir passer plusieurs convois, dont la plupart semblaient n’être que des caravanes de marchandises ou de voyageurs. Guilhem savait qu’en rejoindre une, pour n’être pas forcément évident, aurait été idéal, quoiqu’il n’ait guère de quoi payer pour cela, mais il craignait la promiscuité. Il n’était pas impossible qu’on le vende à une patrouille au service d’un potentat local si on le découvrait esclave en fuite. Comme Toufik, le bracelet de fer qu’il avait dissimulé au mieux sous ses manches pouvait le trahir à chaque instant, sans parler même du baratin sur son identité, qui ne pourrait soutenir un examen serré.

Ils avaient dormi dans un petit abri sous roche prêt d’une oliveraie un peu en retrait du village de Bait Jann, le dernier sous le contrôle de Damas avant la frontière. Ils en étaient repartis avant la fin de la nuit, préférant comme la plupart des voyageurs avancer avant que la chaleur du jour ne fût trop forte. Jusqu’à longer cette combe où courait un léger torrent, ils avaient tout particulièrement souffert de la soif, n’ayant qu’une outre pour se désaltérer, qui plus est percée. Ils avaient encore un peu d’olives et du pain, mais leur estomac leur semblait bien creux pour parcourir toutes ces lieues.

L’avance se fit difficile lorsqu’ils abordèrent la montée d’un col. Cela faisait un bon moment qu’ils se trouvaient obligés de demeurer sur le chemin, d’abord au fond d’un vallon encaissé et peu large, qui serpentait en direction de l’ouest puis dans une boucle ascendante, vers le nord pour enfin redescendre ensuite vers le sud. Ils contournaient le Mont Hermon, où étaient certainement posté des éclaireurs surveillant la frontière. Travaillant son rôle en cas de mauvaise rencontre, Guilhem avait disposé sur le dessus de leurs paquets quelques outils qu’ils avaient ramassés avant de fuir les décombres du chantier détruit par le tremblement de terre. Il espérait juste qu’on ne lui ferait pas demande de montrer son savoir-faire en maçonnerie, ni même qu’on regarderait ses mains de trop près. La seule corne qui s’y trouvait était celle qu’il s’était faite sur le pouce à force de manier l’épée.

Un énorme chêne marquait normalement la frontière avec le royaume de Jérusalem, mais Guilhem n’était pas certain que ce fût toujours le cas. Il avait appris, pour en avoir parlé rapidement avec des voyageurs croisés, que Baniyas n’était pas tombée face à l’émir, mais il avait peut-être maintenu une forte présence militaire pour s’assurer de la zone. Sans trêve officielle, c’était là une marche bien dangereuse à fréquenter lorsqu’on ne faisait pas partie d’un imposant convoi.

À un détour du chemin, il aperçut un petit groupe de cavaliers qui venait à leur rencontre. Aucun animal de bât ne les accompagnait, c’était vraisemblablement des soldats, du moins des hommes en armes. Même en plissant les yeux, il ne sut dire si c’étaient des nomades, des Turcs, des Arabes ou des Latins. Lorsqu’il comprit que c’étaient apparemment une patrouille du pouvoir damascène, il ralentit le pas et se mit en devoir de leur céder le passage comme si de rien n’était. Il baissa la tête en signe de respect, dissimulant en partie son visage. Ce fut peine perdue, ils stoppèrent à son abord. La voix qui l’apostropha était rude et l’accent turc très marqué. Au moins seraient-ils plus aisément floués par son arabe.

« Que fais-tu ici sans guide ni caravane ? Ne sais-tu pas que des brigands ifranjs infestent ces contrées ?

— Je ne suis que modeste maçon, j’ai espoir de m’en retourner vers les miens, à Misr[^misr].

— En passant par ici, fou que tu es ? Tu es bien loin des tiens…

— J’avais fui avec ma famille lorsque Asqalan fut prise par les Celtes. J’ai renvoyé ma femme et mes enfants auprès des nôtres dès que j’ai pu leur payer le passage, voilà quelques mois. Il me fallait finir un chantier pour lequel je m’étais engagé. Je n’ai gardé que ce ghulam[^ghulam2] pour m’assister. »

Les membres du petit peloton ne faisaient guère attention à lui, laissant leur officier décider du sort à faire à ceux qu’ils rencontraient. Selon qu’ils étaient suffisamment bien payés et traités, ils obéissaient plus ou moins aux consignes données par les princes des territoires, mais ils ne dédaignaient parfois pas d’enfreindre les instructions, surtout si la perspective de butin était forte. Guilhem savait qu’il ne fallait pas leur suggérer l’idée qu’il possédait quoi que ce soit de valeur. Il poursuivit son récit en soupirant, l’air pitoyable.

« Las, j’ai été floué par ce mauvais maître, qui m’a chassé sans gages une fois mon ouvrage achevé. Me sachant sans famille, il a tenté de me dépouiller. Sans la fidélité de mon Toufik, qui m’a rejoint nuitamment avec quelques affaires qui m’appartenaient, je serais aussi nu qu’à ma naissance. J’ai donc décidé de prendre la route pour retrouver les miens, n’ayant que trop vécu au loin. »

Giulhem se demandait si on n’attendait pas de lui quelques monnaies. N’en ayant aucune à proposer, il ne pouvait compter que sur sa bonne mine et son histoire cousue de fil blanc. Au pire, il pourrait leur abandonner Toufik, mais le gamin n’aurait guère de valeur avec tous les captifs qui avaient été amassés dans la province depuis le début de l’année. Il inventoriait tout ce qu’ils avaient dans leurs sacoches dont il pourrait se défaire quand un des soldats siffla en indiquant la vallée derrière lui. Un épais nuage de fumée en montait, porté par un vent ascendant qui y dessinait des volutes.

En quelques instants, les chevaux furent remis en position. L’officier échangea quelques mots avec ses hommes et, sans plus jeter un regard à Guilhem, il lança sa petite troupe au trot. Guilhem lâcha un soupir de soulagement et reprit la route avec empressement. Un plus gros gibier avait fait diversion. En pressant un peu le pas, Guilhem espérait être à Baniyas avant la tombée de la nuit.

## Baniyas, soirée du vendredi 16 août 1157

Lorsqu’ils obliquèrent enfin sur le chemin qui menait, par-delà les sources du Jourdain et le pont permettant de les traverser, vers la cité de Baniyas, Guilhem et Toufik poussèrent un soupir de soulagement. Leurs pieds étaient brisés de souffrance à déambuler dans les oueds asséchés. Guilhem avait choisi le trajet le plus long, mais aussi celui qu’il espérait le plus sûr. Ils y avaient récolté plaies et bosses, griffures et lacérations sur tout le corps. Ils s’enveloppèrent une nouvelle fois grossièrement les pieds dans des chiffons et repartirent, pressés de se retrouver en sécurité derrière les murailles.

Au-dessus de leurs têtes, l’outremer gagnait peu à peu sur le cyan et la lune dans son premier quartier commençait à dispenser sa lumière argentée. Un ressaut du terrain leur offrit la vision sur la cité lovée dans les contreforts du Mont Hermon. De nombreuses lueurs la ponctuaient d’ambre, y compris aux abords de l’enceinte. Guilhem ne connaissait pas bien les lieux, mais savait seulement que la place avait été l’objet d’âpres combats ces derniers temps. Il ne fut pas surpris de voir qu’un avant-poste veillait sur le pont et s’y avança avec circonspection. La ville demeurait aux mains des Latins, mais les esprits devaient être échauffés des récents affrontements. Sans compter que les hommes de faction n’étaient pas toujours aussi honnêtes et dévoués à leurs maîtres qu’il aimaient à le faire croire. Dans les zones de frontière comme ici, nombreux étaient ceux qui avaient un ami, une relation, un cousin, à qui revendre des informations ou un esclave échappé.

Une barrière de sacs et de branches avait été placée au milieu du passage, et quelques mantelets garantissaient d’éventuels archers ceux qui montaient la garde. Guilhem compta presque une dizaine d’hommes, ce qui n’était pas rien et lui sembla mauvais signe. Par contre, seuls trois d’entre eux paraissaient s’intéresser à son équipage, ce qui le rasséréna. Ils étaient menés par un géant au dos voûté, qui possédait à la ceinture un large carquois en plus d’une épée et d’une masse. Il avait le visage long et les cheveux en bataille, la tenue fatiguée, mais soigneusement entretenue. Il portait un haubergeon de mailles sans manches et avait remonté les manches de sa tunique. Il arrêta Guilhem d’un ordre sec avant qu’il n’arrive à leur hauteur et s’enquit en arabe de qui il était et de ce qu’il voulait. La zone était encore en guerre contre l’émir de Damas et les errants devaient justifier de leur présence sous peine de finir engeôlés. Guilhem lui répondit en provençal, langue latine qu’il connaissait le mieux, puis dut répéter dans celle des Francs de Jérusalem pour se faire comprendre. À ses côtés, Toufik demeurait ainsi qu’une statue, ses yeux seuls allant d’un homme à l’autre tandis qu’il tentait de suivre ce qui se passait.

« Je suis Guilhem de Gibelet, féal d’un des plus éminents membres de la Compania de Gênes, mon sire Mauro Spinola. J’ai été tenu en geôle sarrazinoise des mois durant. J’ai pu m’échapper voilà quelques jours. je demande asile, dans l’espoir d’aller retrouver les miens. »

Le géant fit quelques pas en avant, et examina plus attentivement les voyageurs.

« Il se trouve moult espies avides de voir si la cité pourrait tomber. Qui me dit que tu ne mens point ? As-tu compère en nos murs, qui puisse jurer de toi ? Tu m’as tout l’air d’un gagne denier de misère.

— Je n’ai pas usance de venir en ces terres et j’encroie peu qu’on puisse jurer de moi ici. Mais je peux être mené à un bon père, qui saura vérifier que je connais l’*Ave*, le *Pater* et le *Credo*.

— Pff, je saurais bien jurer de ma Mahomerie si ma vie en dépendait ! Je ne laisserai clerc juger de cela. »

Il fit signe à quelques hommes de venir à lui puis alla chercher un grand arc qu’il avait laissé jusque-là à l’abri.

« Vous allez nous suivre. De plus avisés que moi sauront bien trouver s’il faut vous brancher ou vous laisser aller. »

Lorsqu’il vit qu’on le faisait passer la barrière pour le conduire en ville, Guilhem se laissa mener sans rechigner. Il n’était pas encore certain qu’on ne le dépouillerait pas, mais c’était sans importance pour lui. Tout ce qu’il espérait, c’était une façon de renouer le contact avec ceux pour qui il travaillait.

Il découvrit avec stupéfaction les dégâts des sièges qui s’étaient déroulés les semaines précédentes. Il n’osa pas en demander le détail, craignant qu’on ne trouve suspect son trop grand intérêt. Une des tours était complètement ruinée et des hommes, certainement des captifs, continuaient d’en dégager les alentours malgré l’heure tardive. De nombreux impacts attestaient de l’ampleur des opérations d’artillerie et de la détermination des assaillants. Bien que la ville semblât avoir été forcée, les portes avaient l’air d’avoir résisté, probablement ouvertes après une infiltration réussie, voire grâce à des complicités intérieures. 

La cité était calme, comme c’était l’habitude après des combats. Généralement, seuls les commerçants les plus audacieux osaient faire leurs affaires, parfois à la dérobée, en ne laissant pénétrer les clients dans les souks qu’au compte-goutte. La plupart des habitants demeuraient claquemurés dans leur maison, espérant que leurs huisseries suffiraient à repousser un éventuel assaut.

La patrouille qui le menait s’arrêta devant une forte porte puissamment ferrée, qui ne s’ouvrit qu’après quelques échanges à travers un guichet lui aussi grillagé de bon acier. Guilhem et Toufik se retrouvèrent dans une large cour où donnaient de hautes voûtes, surmontées d’une galerie couverte. On leur noua les bras à une perche à côté d’un abreuvoir avant de les laisser à la surveillance d’un des soldats. Celui-ci ne tarda pas à somnoler, échappant à la monotonie de sa tâche en tentant de cracher sur tous les cailloux environnants.

Lorsque le grand archer revint, il suivait un noble à l’allure nonchalante. Un pouce dans le baudrier d’armes, un morceau de viande dans l’autre main, il hochait le menton en écoutant le rapport, sans guère sembler y prêter garde. Son apparence martiale, quoique peu soignée, plut aussitôt à Guilhem. Il mit en genou en terre, certain qu’il s’agissait là d’un homme d’importance. La tête baissée, il n’eut pas longtemps à attendre pour qu’on l’incitât à se relever.

Le chevalier savourait un petit éclat d’os à grand renfort de bruits de succion. Il avait un visage lourd, mal rasé et la silhouette témoignait de son goût de la bonne chère. Mais rien en lui ne trahissait de mollesse ou de laisser-aller. Ses cheveux, pour être peu ordonnés, étaient proprement coupés à l’écuelle et ses mains avaient été correctement frottées avant de passer à table.

« Alors, mon brave, Kaspar me dit que tu prétends être Génois ?

— Je suis tripolitain, à leur service. Mon maître est sire Mauro Spinola. »

Le chevalier hocha la tête, lança quelques ordres pour qu’on fouille soigneusement les deux captifs.

« Je veux aussi avoir assurance que ce n’est pas sergent en fuite, porteur de quelque signe d’infamie. »

Il pointa de son os le bracelet qui apparut au poignet de Guilhem.

« Tu portes là cercle de fer qui peut indiquer que tu as été mis en servage. Comment savoir si tu n’es pas un simple fuyard ?

— Je le suis, mais pas d’un maître chrétien. Vous n’avez qu’à me questionner et vous verrez que je suis fidèle disciple de la sainte Église. »

Le chevalier le fixa un petit moment, apprécia longuement les affaires étalées devant lui, qui ne lui arrachèrent guère plus qu’un sourcillement désabusé.

« Je n’ai ni l’envie ni les moyens de nourrir des bouches inutiles ici, tu iras plaider ta cause en la Cité. Mon sire le connétable saura bien apprendre de toi ce dont il pourrait avoir nécessité.

— Je ne suis mie espie.

— De cela je n’ai cure. J’ai bien assez à faire ici déjà. Si tu es tel que tu le dis, tu seras plus proche de tes maîtres et pourra donner quelqu’utile renseignement. Et dans le cas inverse, que Dieu ait ton âme ! »

Puis, après avoir donné des instructions pour qu’on ligote solidement Guilhem et Toufik, il repartit d’un pas vif. Lorsque l’archer le fit se relever, Guilhem lui demanda :

« Ne pourrait-on au moins avoir quelque croûton ? La nuit est bien longue à celui qui s’encouche le ventre creux.

— J’irais voir si les pères n’ont pas de quoi te faire la charité. Ils ne sont plus tant riches depuis le pillage de la cité, mais s’ils voient en toi un de leur frère de religion, qui sait ? »

## Jérusalem, quartier Saint-Jean, midi du mercredi 4 septembre 1157

Le trajet depuis Baniyas s’était fait assez tranquillement, les hommes de l’escorte ayant jugé qu’il était plus agréable de flâner parmi les chemins du royaume que de tenir une place forte à la frontière. Il faisaient de suffisantes pauses régulièrement et, peu à peu, Guilhem sympathisa avec eux. S’ils continuaient à le surveiller et le maintenaient ligoté, ils n’étaient pas mauvais compagnons et acceptèrent même de lancer les dés avec lui, misant des cailloux pour passer le temps.

Avant leur départ, ils lui avaient pris toutes ses affaires, y compris ses hardes, pour les examiner en détail. On savait les espions habiles à dissimuler des messages en des endroits surprenants. Lors de cette fouille intégrale, ils avaient dû quitter tous leurs vêtements et, Guilhem avait découvert avec stupeur que Toufik n’était pas un garçon comme il le pensait, mais une jeune fille à la féminité naissante. Elle avait supporté la gêne et les regards égrillards ou moqueurs sans desserrer les dents, mais semblait encore plus repliée sur elle-même depuis ce moment. Elle avait dès lors refusé tout échange, se contentant de suivre Guilhem comme un petit chiot. Tous deux arboraient désormais des tenues latines, mais toujours pas de chaussures.

Une fois parvenus au palais à Jérusalem, ils avaient longuement attendu dans une étroite resserre emplie de meubles pourris. Sans repère, Guilhem compta trois repas, sans savoir si cela faisait un jour ou deux. Un des gardes qui vint les chercher lui expliqua qu’ils allaient chez les hospitaliers de Saint-Jean. Nul ne voulait prendre en charge cet espion possible dans l’hôtel du roi, pratiquement désert, avec Baudoin et son armée loin au Nord. Mais comme les frères de Saint-Jean avaient eu des droits sur la ville, peut-être pourraient-ils s’occuper de vérifier son histoire.

On les mena cette fois dans une petite salle qui avait longtemps servi de poulailler. De la paille moisie était entassée dans un coin et les déjections des volailles empuantissaient l’endroit. Mais du jour passait par-dessous la porte, ainsi que par le fenestron qui donnait sur la cour, ce qui rendait le lieu moins sordide. Guilhem retrouva le son familier des cloches et les bruissements d’une grande maison. Ils restèrent à patienter encore deux jours, correctement nourris cette fois. Les hommes qui venaient leur porter le brouet, le pain et l’eau n’étaient plus des soldats, mais de simples domestiques. Guilhem commençait à se dire qu’il serait possible de s’évader si les choses perduraient. Il demeurait la question du bracelet de fer qui le désignait à chaque rencontre comme un fugitif.

Il cherchait une façon d’en détacher le rivet lorsque des pas s’approchèrent de la porte. La barre en fut tirée et un grand gaillard, mince et les épaules larges vint se planter devant les captifs. Son visage maigre paraissait n’être que d’os et dans ses yeux brillait un regard fiévreux sous d’épais sourcils. À sa poitrine se voyait la croix blanche du manteau des frères profès de Saint-Jean. Il ne laissa pas à Guilhem le temps de saluer.

« Si vous êtes bien celui que vous prétendez, j’ai quelques questions pour vous !

— N’avez-vous pas fouillé mes affaires ? Je suis bon chrétien, Guilhem, de Gibelet. Si vous pouvez vous enquérir auprès de mon sire Mauro Spinola, il vous confirmera cela. »

L’hospitalier lui adressa un amical sourire.

« Moi je suis frère Raymond. Votre sire m’a justement chargé de verser rançon pour vous sortir des geôles sarrazines. »❧

## Notes

« Forain » : du latin *foranus*, étranger. La signification est demeurée en français contemporain, avec le terme de forain : ce qui n’est pas familier, ce qui est étrange, à rapprocher du mot *foreign* qui, en anglais désigne l’étranger, dans un semblable espace lexical.

La notion d’identité était bien différente de celle que nous pouvons éprouver, avec nos documents officiels et les listes administratives de recensement. Le Moyen Âge se basait avant tout sur une toile de confiance qui permettait de faire certifier par des témoins que l’on était bien celui que l’on prétendait. Il était extrêmement aventureux de se rendre en des lieux où personne ne pouvait se porter garant de vous, car cela signifiait aussi que vous ne pouviez garantir la sincérité de vos rencontres. Appartenir à un cercle relationnel même lâche ou distant offrait une sécurité à chacun des interlocuteurs. C’est pour cela que les négociants, y compris parmi les grands voyageurs, opéraient le plus souvent dans un espace social bien circonscrit, parfois étendu géographiquement, mais sans trop aller au-delà.

## Références

Broadhurst Roland, *The Travels of Ibn Jubayr*, London : Cape, 1952.

Greif Avner, « Reputation and Coalitions in Medieval trade: Evidence on the Maghribi Traders », dans *The Journal of Economic History*, vol. 49, No. 4 (Déc. 1989), p. 857-882.

Ellenblum Ronnie, « Who Built Qalʿat al-Ṣubayba? », dans *Dumbarton Oaks Papers*, vol. 43 (1989), p. 103-112.
