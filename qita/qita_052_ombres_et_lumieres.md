---
date: 2016-01-15
abstract: 1158. Remigio, artiste chevronné à la langue aussi infatigable que les jambes, est désormais assisté du jeune Vital pour aller de casal en cité présenter leurs spectacles d’ombres, jongleries et contes. Au fil des saisons, ils affrontent les poussières des chemins, fréquentent des hébergements aléatoires, les arrière-cours de châteaux.
characters: Remigio, Vital, Bassam, Daimbert le Scribe, Clarembaud, Anceline
---

# Ombres et lumières

## Jérusalem, porte de David, matin du mardi 18 mars 1158

La lumière d’un soleil frais habillait d’or les murailles de la cité, bleuissant les zones d’ombre. Dans le ciel limpide, quelques oiseaux volaient haut, occupés à se poursuivre en poussant des cris aigus. Le vacarme des cloches venait à peine de s’éteindre et les rues bruissaient de l’agitation du peuple industrieux, des animaux menés, des enfants baguenaudant. Près de la porte, une foule de chameaux de bât attendaient devant le moulin, dans une exubérance de poussière grise.

« La journée sera chaude, p’tiot, on ira pas bien loin ce jour. Mais vu le bon temps, je connais un coin où passer la nuit, peu avant le Toron des chevaliers. Vallon joliet, au parmi des oliviers, avec une source claire comme sourire de donzelle. »

Devant l’assurance de son compagnon, le jeune garçon hocha la tête en silence. Il n’attachait ses pas que depuis peu à ceux de Remigio, un saltimbanque, un jongleur comme on les appelait. Ses parents l’avaient mis en apprentissage disait-on. Mais il savait bien qu’on l’avait échangé contre quelques pièces brunies et un tas de vieux linges. Il n’était qu’une bouche à nourrir pour les siens et n’avait découvert sa valeur qu’au moment où il en voyait le montant sur la table râpée de sa maison. Il était parti sans un regard en arrière, mais le cœur gros de n’avoir personne à regretter, ni aucun signe d’affection pour le saluer.

De caractère moins ombrageux que son père, son nouveau compagnon avait également la main moins leste, mais les exigences plus fournies. Il l’avait même rebaptisé Vital, car il disait que cela plaisait aux moines chez qui il aimait à faire halte régulièrement. Sa maigre ménagerie était d’ailleurs affublée de patronymes similaires : le corniaud pelé qui faisait des cabrioles répondait au nom de Giusepe, ils avaient un perroquet, venu d’Alexandrie, Tomaso, et leur âne, une petite bête vaillante, mais dotée d’un esprit retors, avait été baptisé Martin. Remigio racontait qu’il l’avait appelé comme ça en mémoire d’un vieux compagnon, artiste comme lui, et qu’on surnommait le Bon à Rien. C’était mieux que d’être mauvais à tout, rétorquait-il invariablement.

À cette heure matinale, la porte de David était toujours encombrée par les visiteurs et marchands, désireux de pénétrer au plus vite dans la cité pour y faire affaire. Les sergents surveillaient ce qui entrait, sans se soucier de ce qui sortait, mais la cohue était telle qu’on devait se faufiler entre bêtes et gens, en prenant garde à ses pieds. Remigio tenait la longe de Martin d’une main ferme et se servait de sa badine pour se frayer un chemin, sans ménagement.

« Au sortir du faubourg, cria-t-il, y’a un hospice, où ils auront de certes du pain bis à nous donner. Ils reçoivent donations des frères de Saint-Jean pour faire héberge aux marcheurs de Dieu bloqués au dehors à la nuitée. Ils en ont bien assez pour nous aussi. Veille à prendre ta mine la plus triste, ils aiment à contenter maigres enfançons.

— Nous avons droit au pain des pérégrins, maître ?

— La charité s’offre à tous, c’est là maîtresse vertu, et il ne faut jamais manquer de l’exalter auprès de nos généreux hôtes. »

Le saltimbanque s’arrêta soudain, tendant la longe à son apprenti maintenant qu’ils avaient franchi la muraille. Il se pencha, adoptant un air sentencieux démenti par la lueur égrillarde dans son regard :

« Retiens bien ceci : le vénéré saint Paul disait que la plus grande des vertus, c’est la charité^[Première lettre de saint Paul Apôtre aux Corinthiens 12,31.13,1-13.]. »

Vital écarquilla les yeux, impressionné par le savoir de son maître. Cette naïveté tira un sourire amusé à Remigio. Le gamin lui plaisait.

« Nul besoin d’être grand clerc pour assavoir cela. Mais ça peut te valoir un quignon ou un brouet au plus froid de l’hiver de le rappeler à bon escient. »

Il assortit sa remarque d’un clin d’oeil complice et d’un rire forcé.

« Je t’en enseignerai bien d’autres. Si le fort baron aime à entendre les hauts faits des chevaliers fameux, la mégère et surtout le moine sont plus sensibles aux bondieuseries. Nous sommes là pour déclamer ce que chacun aime à entendre. »

Il assortit sa remarque d’un coup léger de badine pour remettre son équipage en route. Il se sentait d’humeur frivole, après plusieurs semaines dans la Cité sainte sans toutefois que les revenus aient été fameux. Jérusalem ne lui avait jamais réussi. Trop de religion pensait-il, et des miséreux comme s’il en pleuvait. Comment gagner sa pitance dans un tel endroit ? Mais le jeune seigneur Hugues de Césarée était un employeur généreux, et il avait décidé de passer les fêtes de Noël auprès du roi. Donc Remigio avait suivi.

Avec l’arrivée des beaux jours, les opérations militaires allaient sans doute reprendre, et il était temps de rejoindre la côte, où les périls étaient faibles et le climat plus propice. De façon générale, le jongleur aimait à rester le long de la Méditerranée et ne s’écartait guère du royaume. Il avait poussé une fois loin au nord, jusque dans le comté de Triple[^triple]. C’était un lieu magnifique, riche de ses cultures opulentes et de somptueuses cités, mais il s’y plaisait moins. Il avait ses habitudes entre Judée et Galilée, connaissait les bonnes héberges, les moines tatillons et les nobles généreux.

« On fera halte à Ramla. Le marché y est bien animé, surtout en cette période de l’année, avec la réouverture des mers. De là on ira peut-être à Ascalon, par chance on y encontrera maître Bassam. C’est lui qui m’a appris le truc des poupées d’ombre. Un sacré vieux goupil, celui-là. Jamais pu savoir s’il était mahométan, ermin[^ermin], romain[^romain] ou bon chrétien... »

Il sourit pour lui-même, retrouvant visiblement la mémoire de bons moments, et se tourna vers Vital.

« J’ai inventé de moi-même la plupart de mes pièces, mais lui en connaît tellement ! Il sait jouer pour quiconque peut le payer. Il sait même des morceaux pour les Pâques des mahométans, un pour chaque soir.

— Les mahométans fêtent Pâques ? s’étonna le gamin.

— De certes. Ils sont hérétiques, mais pas tant. Ils font carême et appellent ça Rame-dent, jamais su pourquoi. Ils font maigre tout au long du jour et, le soir, se dépêchent de gober ce qu’ils ont raté en journée. »

Il se gratta la tête, comme gêné par ce qu’il allait dire, puis ajouta :

« J’ai cheminé un temps avec Bassam. Il savait si bien leur langue qu’on faisait parfois halte en leurs casals. Moi j’y ai jamais rien compris en dehors de quelques mots. Si tu y arrives, ce serait bon pour nos affaires. Au nord du royaume, leurs villages sont légion, et on peut y faire bonne pitance. Juste que la plupart aiment pas le vin, encore moins la bière et que jamais tu y auras droit à une tranche de bacon. En dehors de ça, j’ai pas à dire qu’ils font mauvais hôtes. »

Il fit craquer son dos et s’étira lentement.

« Bon, avant tout, il te faut savoir tes lettres. Continue donc après moi :

> Charles le roi, notre empereur, le Grand,  
> Est resté sept ans tout pleins en Espagne :  
> Il a conquis jusqu’à la mer la terre haute.^[Début de la Chanson de Roland, traduction par Gérard Moignet, Paris, Bordas, 1969.]

## Bethléem, palais de l’évêque Raoul, midi du lundi 14 juillet 1158

Confortablement installés à l’ombre d’un palmier, dans une des cours privées, deux hommes profitaient de la chaleur en grignotant des pistaches qu’ils arrosaient de vin coupé d’eau. Parmi les domestiques de l’évêque, beaucoup s’étonnaient du bon accueil toujours réservé au jongleur Remigio. Et que Daimbert, le principal clerc de sa seigneurie, passât autant de temps avec lui, étonnait chaque fois, mais personne n’osait s’enquérir des relations qui unissaient ces deux-là. Les deux hommes auraient bien été en peine de s’en expliquer clairement, d’ailleurs, après toutes ces années.

« As-tu jamais eu désir de jouer pour le jeune comte de Jaffa ?

— Le frère du roi ? Non, pas eu l’occasion. On le dit fort versé dans tous les plaisirs.

— Ne t’y trompe pas, il est fort érudit et pourrait en apprendre, même à un vieux décloîtré comme toi.

— Au parmi du peuple, on le dit plus enclin à goûter l’abricot qu’à lécher les pages. »

Le clerc, intéressé, se tourna vers son ami :

« Ah, vraiment ? Il est pourtant de frais marié, et fort amoureux. D’aucuns le pensent ensorcelé par Agnès.

— Les gens s’amusent plutôt de le voir faire espousaille de la promise de son vassal si tôt ce dernier en fers sarrasins.

— Ils ne s’inquiètent pas de l’arrivée des Courtenay ? »

Le jongleur pouffa longuement.

« Ils se moquent de cela comme laboureur de ses abeilles. Cela ne peine que les barons qui se partissaient déjà les terres du Soudan d’Égypte.

— Tu as entendu rumeurs sur les ambitions du maréchal[^joscelinIII] ?

— Non, aucune. Mais certains pleurent, dit-on, alors qu’ils ont plein poing. Mais cela n’est qu’affaire de barons et permet à peine aux commères d’échauffer leurs langues. »

L’administrateur fit jouer le vin entre ses dents un petit moment, le regard fixé sur son verre. Remigio lui apportait des nouvelles du peuple, de ce que les amuseurs colportaient, que les négociants évoquaient, le soir, à la taverne ou dans les caravansérails. Il n’espionnait pas, à proprement parler, mais évoquait les rumeurs, les nouvelles, qui animaient cités et casaux. Les jongleurs de son genre étaient aussi importants pour célébrer la gloire d’un baron, selon qu’on les paie assez, que pour en apprendre l’état d’esprit des vilains et laboureurs, artisans et lavandières. Remigio était en outre assez malin pour savoir que cela lui ouvrait certaines portes que ses talents de conteur et marionnettiste n’auraient jamais forcées.

Daimbert avala quelques friandises, indolent, se laissant gagner par la torpeur.

« D’usage, tu n’aimes guère demeurer trop au chaud ou au froid, que viens-tu faire en les terres au mitan de l’été ?

— J’ai eu envie de montrer le pays au petit, mais je ne descends plus avant. Je prends la route de Naplouse en quittant la cité. Peut-être pour rejoindre la Galilée.

— Norredin aime à guerroyer par là-haut, tu devrais prendre garde.

— J’ai déjà croisé son sénéchal, Siracons[^shirkuh], il m’a suffit de me pousser du chemin et de les regarder passer...

— Ces démons ne t’ont pas détroussé ?

— On ne rase pas un oeuf ! Et on dit ces Turcs grands amateurs de pantins d’ombres, j’aurais même pu gagner quelque cliquaille, qui sait ? »

Daimbert lançait un regard aussi étonné que réprobateur lorsque des cris d’oies furieuses provenant de la basse-cour les firent sursauter. Sentant que sa plaisanterie n’avait pas été appréciée, Remigio décida de changer de sujet et passa une langue gourmande sur ses lèvres fines.

« Aura-t-on droit à un jars bien épicé en la veillée ?

— Pas toi, de certes, se gaussa Daimbert, acide. En as-tu jamais mangé, d’ailleurs ?

— Plus qu’à mon tour, j’en ai certeté. Certains barons ont la gourle percée et abreuvent aussi bien qu’ils nourrissent. »

Daimbert renifla, hochant la tête en silence. Non loin d’eux, à l’abri d’un appentis, le gamin cirait les personnages de cuir. Il donna un coup de coude à Remigio et lança, un peu moqueur :

— Dis donc, ton marmot, ni Turc ni baron n’a songé à le vêtir autrement que comme mendiant ? Il a une cotte que pas un chiffonnier ne voudrait comme toaille. Je vais m’enquérir auprès des celliers, voir s’il n’y a pas quelque vieille cotte de damoiseau entré en tonsure qui pourrait lui aller. »

Remigio sourit à Daimbert, en inclinant la tête en remerciement. Puis il ajouta, en faisant un clin d’oeil :

« Dieu connaît le bon pèlerin. »

## Casal de Saint-Gilles, taverne de Clarembaud, veillée du jeudi 11 septembre 1158

Les applaudissements crépitèrent dans l’obscurité de la petite salle voûtée où enfants, parents, curieux et badauds s’étaient entassés. D’un coup de menton, Remigio incita Vital à aller passer la sébile. Ce n’étaient que vilains et artisans, mais ils savaient parfois se montrer généreux, surtout quand, comme ce soir, ils avaient été enthousiasmés par le spectacle proposé.

Cela faisait plusieurs semaines que Remigio avait préparé son récit, dessiné et découpé les silhouettes de cuir pour les nouveaux personnages. À la flamme des lampes à huile, il évoquait le tragique et grandiose destin du Saut du Templier, comme il l’avait nommé. Il n’était pas certain que c’était une histoire véridique, et l’avait enjolivée à sa façon après l’avoir entendue répétée par d’autres. Mais l’essentiel était de frapper l’imaginaire, d’avoir des anecdotes passionnantes et des rebondissements maîtrisés. Il avait choisi de tester son nouveau spectacle ici, en Samarie, car il était censé se dérouler plus au nord, vers la Galilée, sur les côtes entre Saint-Jean d’Acre et Tyr. Là-bas il se devait d’être parfaitement à l’aise pour ne pas décevoir. Les réactions de ce soir le rassérénaient pleinement.

Le maître de céans, un gros barbu, Clarembaud, s’approcha, le visage tel celui d’un conspirateur. Il frottait sa main mutilée avec excitation comme s’il y voyait couler les pièces.

« Excellent spectacle, maître Remigio. Que voilà bonne idée de louer la milice du Temple. On les apprécie fort par ici. Surtout depuis leur martyr d’Ascalon !

— Eh, ne dit-on pas que ces valeureux martyrs ont protégé le reste de l’armée depuis les Cieux ? » répondit le marionnettiste.

Le tavernier n’était jamais très à l’aise avec lui, ne sachant s’il devait prendre ses remarques pour des plaisanteries ou des appréciations dévotes. Le jongleur s’en amusait fort et forçait le trait, trop heureux d’embrouiller un esprit à bon compte. Chez lui la mystification était un art de vivre.

« Tu nous verses un godet pour arroser ça ? »

L’éventualité de voir son bénéfice de la soirée amputé de la moindre portion annihila immédiatement la joie de vivre de Clarembaud. En un instant son visage avait retrouvé le masque du commerçant harcelé par les taxes diverses, les approvisionnements difficiles et les clients tatillons. Il tenait cette capacité instinctive de son passé paysan.

« Attendons de voir la collecte. On ne doit point louer la journée dont on n’a encore vu la nuit… » ajouta-t-il, circonspect.

Lorsque Vital arriva avec la gamelle, ils trièrent les quelques jetons et oboles, n’accusant qu’un très léger pécule. Par contre, de nombreux pichets avaient été commandés et vidés.

« Pas si mal pour un casal, grogna Remigio. Avec ce que tu nous accorderas, maître Clarembaud, nous aurons de quoi reprendre la route au chant du coq demain. »

Heureux de ne pas se voir soutirer un supplément avec l’affluence inespérée, le tavernier opina, tout en allant récupérer les récipients loués pour la soirée. Pendant ce temps, le jeune Vital aidait son maître à ranger soigneusement les figures dans un coffret de pin. Ils plièrent ensuite la grande toile de coton et les lampes, avant d’enfourner le tout dans une corbeille garnissant les flancs de Martin. Lorsque tout fut en place, il ne demeurait dans la salle que quelques lumignons tremblotant, peu vaillants, insuffisants pour faire plus que dessiner de traits ocres les ombres de chaque saillie.

Remigio fit claquer ses mains l’une dans l’autre.

« Alors, maître Clarembaud, qu’en est-il de cette porée tant vantée ? J’ai tel appétit que je pourrais goboyer comme un ours tout un troupeau, pâtre inclus ! »

Vital, désormais habitué aux éclats de son maître, échappa un rire enfantin, excité comme une puce. C’était la première fois qu’il avait eu la responsabilité de remuer quelques personnages et décors.

## Château de Naplouse, cuisines, soirée du samedi 27 décembre 1158

La tablée bruissait d’éclats de voix enjouées, de cris de surprise, de rires et de gloussements gaillards, de vociférations et de hurlements hilares. Le banquet servi dans la grande salle battait son plein et, dans les communs, les domestiques n’étaient pas en reste. La reine mère, Mélisende, n’était pas présente, mais il y avait tout de même une partie de sa cour demeurée là et suffisamment pour que les plats rapportés en cuisine fassent le bonheur de beaucoup.

Installé non loin de la porte d’entrée et donc dans les courants d’air, Vital ne se plaignait pas. Il n’avait jamais mangé ainsi. De la viande, rôtie comme pour un baron, avec de la sauce épicée digne d’un roi. Ce n’était que quelques miettes arrachées à un os, mais il savourait avec une joie non dissimulée. Il regardait avec des yeux ébahis les mets se succéder, depuis le haut bout où les principaux domestiques avaient droit aux plus beaux restes, jusqu’à sa table où les errants, musiciens, jongleurs, acrobates et bateleurs, avaient trouvé place. Nul besoin de se battre, chacun obtenait sa suffisance, dans les éclats de voix et les plaisanteries à la gloire de leurs hôtes.

Remigio avait trouvé avec qui baguenauder : Anceline, une jolie femme un peu usée par les ans, mais au sourire doux, qui se disait lavandière. Le saltimbanque en avait ri, avant d’ajouter qu’elle devait frotter les linges avec son derrière plus souvent qu’avec ses mains. Elle n’avait pas grimacé à la saillie, habituée aux rebuffades désobligeantes. Au moins cette fois-ci n’y avait-il pas méchanceté dans la remarque, voire même plutôt intérêt. En effet, Remigio n’en était pas rebuté pour autant. Il avait juste vérifié les monnaies de sa bourse, au cas où.

« Tu travailles au castel, ou pour un des barons de sa cour ?

— Je viens juste aider pour les fêtes. J’étais dans le coin, avec quelques amies, et sans ouvrage.

— Il ne fait pas bon temps, l’hiver, pour nous autres poudreux des chemins » acquiesça Remigio.

La remarque bienveillante ragaillardit Anceline, qui passa une main amicale sur le bras du jongleur.

« Et toi, tu espères demeurer un moment par ici ?

— Quelques jours au plus. Je fais découvrir l’art au gamin, là. »

D’un coup de menton, il désigna Vital, affairé à rogner son os avec l’enthousiasme d’un jeune dogue affamé.

« J’ai accepté de le prendre comme apprenti, et j’espère bien qu’il saura m’aider en mes vieux jours. J’ai espoir de m’attacher à puissant baron et de rimailler pour aider à célébrer sa gloire. Un jongleur qui irait déclamer pour moi serait de bon effet.

— Tu sais donc jouer avec les mots tel le poète ?

— J’ai étudié les arts libéraux et devais finir clerc. Mais j’aime trop la vie pour quitter le siècle. »

Anceline échappa un ricanement cristallin, revivant visiblement quelques souvenirs.

« Je connais nombre de prélats qui ne renoncent pas à certains plaisirs pour leur mitre.

— Ma défunte mère m’a trop bien inculqué la religion pour que je finisse évêque, c’est là tout mon malheur » s’esclaffa Remigio.

Ils partirent dans un grand éclat, qu’ils accompagnèrent d’une copieuse rasade à la bonne santé du châtelain de la reine. Puis une voix forte cria :

« Les jongleurs de pantins d’ombre, c’est à vous !

— _Fiat lux_^[_Que la lumière soit_, première parole de Dieu au début de la Genèse.] _et umbra fuit_^[_Et l’ombre fut_.], j’arrive ! » lâcha avec emphase et espièglerie Remigio, scellant sa promesse d’une caresse dans le dos de sa voisine tout en se levant. Puis il attrapa Vital par le col avant de s’avancer, majestueux comme un chevalier au behourd, jusqu’à l’escalier dans les ténèbres duquel il disparut dans une courbette adressée à Anceline. ❧

## Notes
Si le nom de mes deux personnages sont une référence à peine voilée à _Sans famille_ d’Hector Malot, mon envie était justement de prendre le contrepied de la description sociale résolument effrayante du XIXe qu’en faisait le romancier. Sans chercher à sous-estimer les difficultés et les aléas d’une vie d’errance, ni les périls à traverser des zones de guerre à l’époque des croisades, il apparaît que des voyageurs pouvaient connaître une existence décente.

Nous sommes beaucoup mieux renseignés sur les pèlerins de toute religion que sur les artistes, mais la vie culturelle était riche et se basait en grande part sur le spectacle vivant, même si le professionnalisme n’était pas toujours de mise. Quant au théâtre d’ombres, il était issu d’une longue tradition dans ces régions. Il a pu être christianisé pour plaire au public latin, bien que nous n’ayons aucune source sur le sujet.

## Références

Broadhurst Roland, _The Travels of Ibn Jubayr_, London : Cape, 1952.

Guo Li, _The performing Arts in Medieval Islam. SHadow Play and Popular Poetry in Ibn Daniyal’s Mamluk Cairo_, Leiden, Boston : Brill, 2012.

Moignet Gérard, _La chanson de Roland. Texte original et traduction_, Paris : Bibliothèque Bordas, 1969.
