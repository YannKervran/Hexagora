---
date : 2020-01-15
abstract : Habib, jeune homme originaire de Segor, revient dans sa ville natale où il va travailler dans l’industrie sucrière. Mais l’avenir de celle-ci risque d’être bouleversé avec l’arrivée d’un nouveau prince.
characters : Habib, Shabib, Halfon al Fustani abu Harith, Moreau, Virart, Abu Talib, Bachar al-Tabassum
---

# Le suc des roseaux

## Château du Fier, après-midi du samedi 10 octobre 1159

Indifférent à l’agitation autour de lui, un colporteur arrivé avec la caravane avait rapidement étalé quelques babioles sur une natte, un peu à l’écart des abreuvoirs et des bêtes. Il y avait disposé de menus breloques : rubans, lacets et aiguilles, boucles de métal, épingles et peignes. Il espérait faire quelques affaires avec les hommes de la garnison, allégeant son ballot pour la suite du chemin tout en empochant quelques piécettes ou méreaux.

Habib, jeune servant habillé d’un simple thawb ceinturé d’une toile, la tête ornée d’un turban soigneusement noué, hésitait sur quelques boutons qui lui seraient fort utiles, ainsi qu’un dé à coudre. Dans l’espoir de faire baisser le prix, il faisait mine de s’attarder sur des articles plus onéreux, plaisantait avec ses compagnons. Il savait que le convoi ne repartirait que le lendemain et que plus il attendrait, plus le marchand serait conciliant. La veillée serait également l’occasion de lui soutirer un éventuel rabais, en le jouant au dé, en lui offrant nourriture ou boisson.

Il se relevait en époussetant la poussière de ses genoux quand vint à lui un des voyageurs. Il reconnut immédiatement un des caravaniers qui faisaient souvent le trajet entre les cités du sud et la côte loin au nord, Bachar al-Tabassum. Fidèle à son surnom, celui-ci arborait un sourire d’une oreille à l’autre, dévoilant des dents d’une blancheur d’ivoire qui tranchaient dans son rude visage, prématurément ridé par le soleil. Ils se saluèrent aimablement et le voyageur l’invita de la main à se rapprocher.

« J’ai pour toi message de la part d’abu Salim. »

Cela signifiait en clair qu’il était porteur d’un message provenant de sa sœur Shukriya, épouse d’abu Salim. Mais il aurait été inconvenant de la part de Bachar de laisser entendre qu’il s’était entretenu avec elle directement, même si c’était le cas. Les fellahs des petites villes se mêlaient plus librement entre hommes et femmes, au grand désarroi de certains imams et cadis parmi les plus rigides sur les mœurs.

« J’ai moisson de nouvelles des tiens. Mais sache déjà qu’ils vont bien, les enfants courent comme gazelles et le vieil abu Talib conserve la vigueur du lion, même si sa boiterie est plus forte que jamais.

— J’ai toujours crainte pour lui, avec les chaleurs de l’été…

— Il dit que tu devrais revenir chez les tiens, qu’il y a de nombreux travaux à Zughar et du labeur pour qui en veut.

— J’ai entendu dire que Badawil faisait planter des cannes à sucre, mais cela n’a rien de nouveau. C’est le fait des princes.

— C’est qu’il met grands moyens pour ses terres : canaux de pierre pour l’eau, moulins solidement bâtis et murs chaulés. Quand je suis parti, ils avaient embauché tellement de laboureurs que les bêtes étaient serrées flanc à flanc, à tracer leurs sillons. »

Habib hocha la tête. Le calme relatif que connaissait la région depuis quelques années laissait entrevoir un développement économique qui pourrait profiter aux locaux. Cadet de famille sans fortune, il avait fui les abords de la Mer morte pour échapper à une probable condition de journalier, à récolter et trier le sel ou, pire encore, le soufre. Il s’était fait soldat, maniant la lance et l’arc de son mieux, et avait escorté des caravanes jusqu’à ce qu’il trouve cette opportunité dans une forteresse royale de seconde zone. Il y travaillait plus comme valet que comme guerrier, quoique la place fut plutôt bonne et correctement payée. Il n’allait pas lâcher la proie pour l’ombre, mais ne pouvait se permettre d’éconduire sans y mettre les formes une demande directe de son grand-père.

« Je vais demander congé au sire Gaston, peut-être pour les fêtes dans quelques semaines. Cela fait trop de mois que je n’ai vu les miens.

— Je leur porterai réponse à mon retour de Gaza. Si jamais tu veux leur en dire plus, je repasserai certainement par ici, même si on s’y arrête que le temps de faire boire les bêtes. »

Habib acquiesça, remerciant Bachar d’un sourire. Puis il l’invita d’un geste à le suivre dans la forteresse. Il trouverait bien quelque boisson à partager avec celui qui lui portait des nouvelles des siens.

## Palmeraie de Segor, matin du lundi 8 février 1160

Habib longeait le chemin qui menait à la ferme royale, son épée battant fièrement à sa hanche. Il y avait dépensé pratiquement toutes ses économies, mais il trouvait que cela lui donnait l’air plus martial que la lance, attribut du simple milicien. Or il était en charge de la surveillance du domaine, répétant les directives à ceux qui se contentaient de gourdins et de piques. Il partageait ce privilège avec quelques autres syriaques sous l’autorité d’un Latin à l’élocution rapide et au tempérament agité, Le Bourdon.

L’exploitation en plein agrandissement qui recrutait dépendait directement de l’hôtel royal, il avait donc aisément obtenu congé du châtelain du Fier, et avait été affecté près de sa ville natale. Ayant une légère expérience militaire, qu’il avait agrémentée de quelques détails reconstruits, il avait même décroché une meilleure solde. Il profitait juste beaucoup moins du gîte et du couvert, retrouvant fréquemment sa famille dans la cité voisine pour la nuit ou les jours de repos.

Il rejoignit la troupe dans la principale cour, tandis que le bras droit du Bourdon, la Plume, les faisait se rassembler pour les consignes du matin. Si le Bourdon, avec sa petite taille et ses cheveux hérissés, était volubile, la Plume le surclassait largement par son babil incessant. Parsemant à l’envi ses phrases de blagues salaces et de remarques souvent grossières, il semblait perpétuellement satisfait de ses saillies, sans se soucier de leur réception auprès de son public. Il avait ainsi acquis une inquiétante facilité à vexer les gens par des réparties qu’il pensait spirituelles.

Habib eut à peine le temps de saluer ses compagnons que la corne résonnait, marquant le début de leurs tâches de la journée. Le Bourdon sortit de la petite pièce qu’il utilisait comme bureau et logement, juste à l’entrée de la vaste cour. De là, il pouvait surveiller depuis une lucarne l’accès à toutes les dépendances qui lui faisaient face.

Quoique ses talents d’archer aient démontré son expérience militaire, il ne ressemblait guère à un soldat, avec ses pattes courtaudes, sa démarche bonhomme et sa silhouette empâtée. Il était généralement habillé comme un Latin, mais arborait souvent un détail local, turban ou foulard en guise de ceinture, voire ’aba[^aba] de laine si le temps était frais. De plus, il tenait parfois son épée tel un bâton, dans son fourreau gainé du baudrier.

Il se plaça face à la petite troupe, un sourire satisfait sur le visage devant assemblée de soldats dont aucun n’avait jamais eu à connaître un véritable affrontement. Mais ils étaient bien équipés et l’air correctement nourris, ce qui suffisait à éloigner maraudeurs, animaux sauvages et vagabonds. On ne leur en demandait pas plus.

« Alors, hui, nous avons mandement de guetter les journaliers qui œuvrent au tri et à la préparation des plants. Il ne s’agirait pas que les plus belles cannes finissent ailleurs. Vous avez vu ce qu’on a reçu, le lot de… »

Il chercha dans sa mémoire le terme qui désignait les boutures, mais n’y parvenant pas, il invita d’un geste Habib à compléter sa phrase.

« …de muqantar.

— Voilà ! Veillez à ce que ce soit bien celles avec le plus de nœuds qui sont choisies. Il faudra aussi que deux ou trois d’entre vous aillent faire le tour des jardins, il y a plusieurs caravanes dans le coin et donc toujours des marauds qui espèrent grapiner. »

Il allait s’éloigner, laissant les gardes s’organiser pour la suite, puis se ravisa et appela le silence d’un geste.

« Je ne sais ce qu’il en est ici, mais après-demain commence Carême, et chez moi les hommes ont usage de faire bombance à cette occasion. D’aucuns boivent plus que de raison ou s’abandonnent à la griserie de la fête. Cela n’est pas bien grave, mais il faut éviter que cela ne finisse par causer soucis. »

Autour de lui, les soldats retinrent un sourire devant sa méconnaissance du pays. Les chrétiens locaux observaient pour la plupart un Carême qui avait débuté la veille. Il n’allait donc guère y avoir de fauteurs de troubles, sauf si un pèlerin s’était perdu dans les parages, ce qui était peu probable. Les cités maudites qui entouraient la Mer morte n’étaient guère attrayantes pour les marcheurs de Dieu en quête des lieux saints.

## Segor, souk au sucre, début de soirée du samedi 8 juillet 1161

Il se trouvait toujours des négociants pour traîner le soir alors qu’ils avaient eu toute la journée pour faire leurs affaires. Ils continuaient à discuter, pinailler, argumenter tandis qu’on les poussait vers la sortie. Seuls les valets et les domestiques étaient pressés de rentrer chez eux, de fermer les éventaires et de voir les boutiques enfin closes.

Après une année à courir les jardins pour éviter les chapardages, Habib avait été promu au rang de premier gardien du souk au sucre par l’entremise d’un de ses oncles. L’épée toujours au côté, il assistait le nouveau mâalem[^maalem], qui supervisait les échanges, percevait les droits et taxes et surtout récupérait le fruit de la vente des exploitations royales, avant de le verser au châtelain. C’était un jeune homme au caractère fantasque, fils d’une bonne famille qui avait ses entrées auprès des seigneurs latins locaux. Il avait été amusé à l’idée d’avoir une personne du nom de Habib comme serviteur, vu que lui s’appelait Shabib. Leur duo n’avait pas tardé à s’attirer la sympathie des familiers des lieux, habitués aux routinières et peu aimables façons du vieil Ahmed, mort dans l’hiver précédent d’une mauvaise fièvre.

Habib suivait donc son chef, fermant les boutiques à l’aide de clefs passées sur un gros anneau de fer que le mâalem lui confiait le temps d’opérer. C’était aussi là que pendait la grande clef du souk qui ouvrait les serrures des larges vantaux qu’on verrouillait chaque soir. Nul ne pouvait alors plus pénétrer dans la zone, ses abords étant gardés par un concierge logé dans un guichet à l’angle de la belle demeure de Shabib, assisté d’un chien porté aux grognements, mais qui n’aboyait jamais.

La journée avait été calme, malgré la venue de quelques négociants en route pour les côtes levantines. C’était la période des plantations et les responsables d’exploitations se concentraient sur ce travail délicat. Les affaires se faisaient sans entrain, alimentées par les entrepôts des commerçants spécialisés et des spéculateurs tentés de jouer sur les variations saisonnières de prix.

Un scribe au service de Shabib les suivait, portant un coffret lourdement ferré où étaient conservés billets à ordre et lettres de créance, ainsi que quelques rares liquidités. La marchandise était bien trop onéreuse pour se négocier avec des monnaies, et tout se faisait généralement par échanges scripturaires, parfois validés par un gage de bourses scellées.

Habib laissa le mâalem et son assistant à la grande demeure puis prit la direction de la maison familiale. Il avait pu s’y aménager un coin à lui dans un des anciens débarras. Il n’y avait qu’une minuscule ouverture donnant par un claustra sur la cour intérieure, mais il était au rez-de-chaussée et pouvait aller et venir comme bon lui semblait. Surtout que désormais son grand-père avait estimé qu’il méritait sa propre clef de la petite porte latérale, étant donné son travail important dans la cité.

La nuit tombait vite, de trop rares éclairages autour de certains lieux publics, bains et mosquées, permettaient d’y voir par moment, mais Habib connaissait parfaitement le chemin. Il avait espéré avoir le temps de s’arrêter acheter quelques douceurs au miel pour leur dessert, mais une fois encore il était trop tard. Lorsqu’il referma la petite porte derrière lui en entrant dans la vaste demeure, les ténèbres avaient remplacé les ombres et la chaude lueur des lampes à huile l’accueillit.

Il salua ceux qu’il croisa, esquivant les enfants occupés à se courir après dans les couloirs malgré les récriminations des femmes. Il aperçut les lumières à l’étage, dans le majlis[^majlis], où les hommes de la famille, ses oncles, son grand-père abu Talib et ses cousins devaient échanger les nouvelles de la journée en attendant le souper. Il se dépêcha d’aller se rafraîchir au bassin avant de se savonner correctement les mains, puis il enfila des savates d’intérieur.

Lorsqu’il pénétra enfin dans la salle à manger, la nourriture était servie. Abu Talib, qui n’aimait pas être dérangé pendant son repas, lui lança un regard impératif, l’invitant à s’installer au plus vite s’il voulait éviter ses foudres. Il était à peine assis que la voix rocailleuse s’éleva.

« Mangeons ! »

## Segor, souk au sucre, début d’après-midi du mercredi 8 août 1161

Avec la chaleur assommante, Habib somnolait, installé sur un tabouret aux abords de l’office où Shabib recevait les négociants. Imitant le Bourdon à la sucrerie, il pouvait ainsi à la fois surveiller les entrées et sorties et ce qui se passait dans les boutiques. Le midi en plein été, chacun demeurait à l’ombre et certains marchands tendaient même une bâche devant leurs produits pour profiter du calme le temps d’une sieste.

Il fut donc surpris de voir s’avancer vers eux un petit homme à la longue barbe grise dont les atours trahissaient l’aisance. Il était écarlate en raison de la chaleur, mais ses manières et sa tenue montraient le personnage d’importance. Il était suivi d’un valet portant des paquets et un gros sac de cuir et se déplaçait avec énergie sans pour autant sembler agité. Il se figea devant le jeune homme, qui sauta sur ses pieds, tirant sur son thawb pour lui donner meilleure apparence. Il le salua avec respect et demanda qui il devait annoncer au mâalem.

« J’ai nom Halfon al Fustani abu Harith, ton maître me connaît. »

Sa voix était douce et ferme, mais sans cette inflexion de brusquerie qu’on rencontrait parfois chez les puissants. Il sembla immédiatement sympathique à Shabib. Il n’eut d’ailleurs pas le temps d’aller l’annoncer que Shabib s’avançait, les bras levés comme s’il recevait un frère. L’homme était visiblement plus que bienvenu et rapidement une petite collation amicale fut proposée et présentée. Ayant retrouvé son poste à l’entrée, Habib ne perdait pas une miette des échanges, même s’il aurait bien apprécié de pouvoir en partager les douceurs des mets.

« Quel plaisir de vous voir, abu ! Nous ne nous sommes encontrés depuis si longtemps !

— Depuis notre soirée en Asqalan[^asqalan], voilà plusieurs saisons de cela. Tu commerçais alors encens, étoffes et poivre.

— Plus besoin pour moi de courir les chemins et les mers, je gère le souk, ici, désormais. »

Le vieil homme approuva aimablement de la tête.

« Vous êtes en route vers al-Misr[^misr] ?

— Si seulement ! Non, je suis en route pour Eilat, où je dois prendre un navire pour quelques affaires au loin.

— Rien de funeste, j’espère.

— Non, juste des dépôts qui ne sont pas gérés ainsi qu’ils le devraient, mais rien d’inquiétant. Par contre, alors que je passais tantôt à al-Quds[^alquds], j’ai appris des choses qui devraient vous intéresser, ici.

— À propos du roi Badawil ?

— En partie, oui. Zughar n’est plus sienne, la haute Cour en a attesté voilà quelques jours. Il l’a donnée en fief au sire Philippe de Naples[^naples], qui lui a concédé ses terres au mitan du royaume en échange. Il faudrait donc d’ailleurs le nommer *Philippe de Montréal*, ou *prince d’Outrejourdain*. Cela sera certainement proclamé d’ici peu. »

Shabib en resta sans voix un moment, avala un peu de lebné avant de répondre.

« Il est de la parentèle du vieux Maurice, non ? Ce serait de coutume qu’il hérite de ces provinces, chez les Celtes.

— Aucune idée, mon ami.

— Quoi qu’il en soit, le roi a de nombreuses terres directement, qui lui procurent bons revenus, je n’encrois guère qu’il se déposséderait de pareils joyaux.

— Cela m’étonnerait aussi. Mais la justice sera en une autre main et certaines taxes iront à d’autres coffres… *Si vous entrez parmi les borgnes, fermez un œil*. »

Il assortit sa dernière remarque d’un clin d’œil appuyé. En tant que juif, il n’y avait aucun lieu où des coreligionnaires régnaient. Il était donc toujours d’une grande circonspection, particulièrement dans les zones sous contrôle des Latins, dont la tolérance n’était pas la plus avérée des qualités.

« Le roi a pourtant construit de nouvelles installations voilà peu, Habib y a travaillé quelques mois avant de me rejoindre. Badawil devait savoir qu’il allait donner à fief ces terres à ancien adversaire, partisan de la vieille Mélisende. »

Habib sursauta de s’entendre ainsi interpelé, mais il n’eut pas le temps de réagir que les deux hommes continuaient leur discussion. Il se contenta donc de hocher la tête ostensiblement.

« On la dit au plus mal depuis le printemps, indiqua abu Harith. Peut-être profite-t-il de voir sa mère grabataire pour régler ses comptes…

— *Qui a le pouvoir doit plus qu’un autre pardonner*. Le sire de Naples est grand guerrier, tout le monde le sait. Le voilà qui règne désormais sur importante marche, entre le bilad al-Sham[^bilad_al_Sham] et al-Misr.

— Il mènera pourtant moins de lances à la bataille avec ce moindre fief.

— Peut-être que tout cela vise à porter fer et feu dans nos provinces, à y lever plus de troupes ?

— Puissent le sabre et la flèche demeurer loin de vos murs, mon ami. Bien trop de sang est déjà versé pour leurs guerres sans fin. » ❧

## Notes

Le sucre est une denrée particulière au Moyen Orient à l’époque médiévale, car c’est une marchandise qui demande beaucoup d’investissements en termes d’infrastructure et de main d’œuvre tout en étant très rentable. Seuls les plus riches pouva^ient donc se lancer dans sa production, généralement les monarques.

L’échange de terres qui s’est déroulé pendant l’été 1161 entre Baudoin III et Philippe de Milly demeure un mystère. Pour quelle raison ce proche de la reine mourante aurait-il abandonné ses propriétés de Naplouse pour un fief deux fois moins puissant militairement ? Ceci en plus du fait que le roi avait amputé une partie des revenus de cette province, afin d’en bénéficier directement.

Cela pouvait être en rétorsion des affronts subis quelque dix ans plus tôt par le jeune monarque, quand il s’était affranchi de la tutelle de sa mère. Mais c’était aussi confier une zone stratégique à un homme dont personne, pas même ses opposants, ne contestait les qualités militaires. Il avait donc l’opportunité d’y briller par quelque haut fait d’armes. Je n’ai pas la réponse scientifique à cette question, mais ma proposition d’interprétation des sources historiques alimentera un des arcs narratifs dans le tome à venir des enquêtes d’Ernaut, *La rivière du diable*.

## Références

Taha Hamdan, « Some Aspects of Sugar Production in Jericho, Jordan Valley »

Taha Hamdan, « The Sugarcane Industry in Jericho, Jordan Valley »

Ouerfelli Mohamed, *Le sucre. Production, commercialisation et usages dans la Méditerranée médiévale*, *The Medieval Mediterranean Peoples, Economies and Cultures, 400—1500*, volume 71, Brill, Leiden-Boston, 2008.

*The Origins of the Sugar Industry and the Transmission of Ancient Greek and Medieval Arab Science and Technology from the Near East to Europe. Proceedings of the International Conference. Athens 23 May 2015*

RRR - Revised Regesta Regni Hierosolymitani Database, http://crusades-regesta.com no. 670 (RRH 366), accédé le 20 janvier 2020
