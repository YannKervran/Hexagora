---
date: 2013-12-15
abstract: 1157. Accomplissant fièrement son devoir au service du roi de Jérusalem, Ernaut commence à nouer des liens avec d’autres colons. La rencontre avec une jeune femme l’incite bien vite à envisager de fonder une famille. Aura-t-il seulement les moyens d’offrir à Libourc ce que sa famille attend d’un bon époux ? Le mariage n’est pas qu’une affaire de bons sentiments…
characters: Ernaut, Libourc, Sanson de Brie, Mahaut, Baset
---

# Jouvences

## Jérusalem, matin du mardi 16 avril 1157

Après l’afflux du matin, lorsqu’entraient tous les marchands de légumes, de fruits et de bestiaux à destination des foires et étals, le travail était plutôt calme à la porte de David. Les négociants arrivaient au compte-goutte, avec leurs caravanes de chameaux, de mules ou d’ânes, dans un nuage crayeux et les cris des convoyeurs. Ils étaient aiguillés vers le bâtiment des douanes, tandis que leur convoi emplissait l’air de poussière et d’odeurs animales.

Le péage ne concernait pas les simples voyageurs, les pèlerins ou les locaux. Donc le plus souvent, les sergents de faction se contentaient d’attendre que la foule et le temps passent, à l’ombre des murailles, espérant le passage du soleil au zénith, moment où ils seraient relevés. Certains jouaient au palet, d’autres encore fermaient les yeux, les fesses dans le gravier, cachés dans un recoin. Ernaut n’était pas de ceux-là.

Récemment nommé, il accomplissait son devoir avec zèle, sous les sourires et parfois les quolibets de ses compagnons. Sa haute stature marquait d’ombre le passage sous la voûte perçant la muraille. Il était l’écueil qui brisait le flot. Ou du moins s’imaginait comme tel, les pouces dans le baudrier, jambes écartées, le front haut et le regard inquisiteur. Eût-il été habillé de gris qu’on l’aurait pris pour une antique statue de Samson, oubliée là des siècles durant. Les yeux au loin, perdus dans les monts de Judée, parcourant les terrasses d’oliviers et d’aliboufiers, de cyprès et de figuiers, revenaient instantanément se poser sur quiconque se présentait à l’entrée du passage. Il n’esquissait pas le moindre geste pour les exempts de taxe.

Baset, un de ses collègues, petit homme au visage pointu, le doigt prompt à moquer ou à se réfugier dans l’une des cavités de son visage ricanait avec un de ses compères, un valet ventripotent qui traînait dans les environs, entre deux commissions pour un maître débonnaire.

« On a levé bon féal avec celui-là !

— Crédieu, y ferait pas bon l’inquiéter. Si la baffe te rate, la tempête à la suite t’enrhumera à coup sûr ! s’amusa l’autre.

— De certes je préfère l’avoir à mes côtés. Il a tant espoir d’œuvrer droitement qu’il aime à assurer pires corvées.

— Ça lui passera…

— Comme à tous » opina Baset gravement.

Il se tourna vers le valet, fouillant sous sa cotte pour détacher sa bourse du braiel.

« T’irais pas nous quérir cruchon de vin bien coupé ? J’ai grand soif et la vinasse d’ici a goût de pissat. »

Le joufflu attrapa les pièces et partit de sa démarche traînante, levant la main pour saluer Ernaut tandis qu’il pénétrait dans la cité. Le jeune homme le suivit d’un regard, tournant légèrement la tête, un demi-sourire sur les lèvres. C’est alors qu’il discerna des silhouettes connues qui s’approchaient depuis les rues de la ville. Son expression se fit soudain plus enjouée et toute sa silhouette se détendit. Sanson de Brie, sa femme Mahaut et, surtout, Libourc sa fille, s’avançaient droit vers lui.

Arrivés à quelques pas, le vieil homme salua amicalement, tandis que Libourc se contentait d’un sourire à son intention. La poitrine du jeune homme n’était que braises et il ne savait désormais plus quoi faire de ses mains, qu’il ne pouvait garder crânement sur ses armes. Embarrassé, il décida de les croiser dans son dos, histoire de les cacher.

« Le bon jour à toi, maître Ernaut, lança gaiement Sanson.

— Le bon jour à vous, maître Sanson. »

En disant cela, il ne s’attarda guère sur les deux parents mais échangea quelques regards avec la jeune fille, découvrant avec ravissement l’apparition des fossettes aux creux des joues. Il se rendit compte après un instant qu’il dévisageait Libourc alors même qu’il leur bloquait le passage à tous trois. Le visage de Mahaut n’aurait pu être plus acide lorsqu’elle déclara :

« Auriez-vous ordre de tenir les pèlerins en deçà des murs, sergent ? »

Ernaut écarquilla les yeux.

« Certes pas ! Je.. C’est juste que…

— Nous allons visiter un casal non loin, maître Ernaut, peut-être en as-tu entendu parler ? » glissa Sanson, de sa voix douce mais décidée.

Ernaut lui adressa un regard reconnaissant et réussit à enchaîner :

« Peut-être, un mien frère espère un casal à une poignée de lieues au septentrion, à la Mahomerie.

— Non, moi c’est à… »

Sanson fronça les sourcils, leva les yeux.

« Mahomeriola, mon ami, lui indiqua Mahaut.

— Voilà, sur la route de la côte. »

Ernaut ne put réprimer un soupir de mécontentement. Si Sanson s’était installé dans le même casal que son frère Lambert, il aurait eu toutes les excuses pour leur rendre visite, pour être en compagnie de Libourc.

« Nous cherchons loueur de montures pour nous y rendre, justement, y faire visite à l’intendant et se rendre compte des manses. En connais-tu un bon ?

— De certes ! Il y a Abdul Yasu, dont l’écurie est située en deçà du bosquet de palmiers où vous voyez la charette emplie de bois. Recommandez-vous de moi, il me connaît. »

Disant cela, Ernaut bomba le torse, heureux de se mettre en avant. Il n’osa risquer un œil vers Libourc, mais sentait bien qu’elle le fixait, et devina un sourire sur son visage.

« Grand merci, mon garçon. Je suis aise d’avoir compère en la sergenterie du roi Baudoin. J’ai espoir que tu viendras rompre le pain dans ma demeure quand je serai installé.

— Rien ne me ferait plus plaisir, maître Sanson.

— Alors c’est dit, grand merci à toi, maître Ernaut, je te souhaite le bon jour. »

Ernaut ne sut quoi répondre et bredouilla un salut incompréhensible, tandis qu’il dévisageait Libourc qui s’éloignait. Après quelques pas, elle fit voler sa natte d’un geste gracieux et en profita pour le fixer à son tour, le gratifiant d’un sourire porteur d’espoir.

## Mahomeriola, fin de matinée du dimanche 8 septembre 1157

À la sortie de la messe, les gens s’assemblaient devant le parvis, au frais des pins qui bordaient la placette, entre le gargouillis de la fontaine et le chant des cigales. Les enfants jouaient autour des buissons brunis par le soleil, couraient entre les hommes occupés à échanger les dernières nouvelles du royaume, des cultures ou, plus important encore, des vicissitudes du voisin. Ernaut avait pris l’habitude de venir partager le dîner dominical avec Sanson et les siens, profitant de l’occasion pour passer du temps avec Libourc. Comme à chaque fois, Mahaut était partie en avant pour finir de préparer le repas tandis que son mari, intarissable bavard, s’attardait à discuter.

Les deux jeunes gens profitaient de sa présence pour flâner également à l’ombre des arbres, comme d’autres de leur âge, déambulant aux alentours entre les bosquets. Ne sachant que faire de ses mains, Ernaut dépeçait une pomme de pin tout en parlant. Il se sentait de plus en plus en confiance avec Libourc et ne craignait plus les moments de silence, qu’ils comblaient de regards qui se suffisaient. Parfois, quand ils se trouvaient à l’écart d’un chemin, loin des regards toujours curieux, ils se laissaient aller à plus d’intimité, s’accordant baisers et chastes caresses. Ils jouissaient de leur jeunesse, de leur passion naissante, de ces promesses innocentes.

Ce jour-là, Libourc sentit bien qu’Ernaut n’était pas aussi dissert qu’habituellement. Il hésitait sur les mots et ne finissait pas ses phrases, comme si un souci le contrariait. Elle se rapprocha de lui, entrelaçant leurs bras et saisissant sa main.

« Tu me sembles d’humeur bien sombre, ce jour d’hui, Ernaut !

— Sombre ? Certes non. C’est juste que… Je n’ai pas grande habileté avec les mots.

— Alors déverse ton sac et nous ferons juste tri. »

Ernaut inspira lentement et déposa un baiser dans la chevelure de la jeune fille.

« Je pensais à Guillaume et Anseline…

— Que de bon temps nous avons pris à leur mariage, s’emporta Libourc. Je n’avais pas dansé ainsi depuis… Depuis jamais en fait ! Tu caroles si adroitement, mon bel ami. »

Elle se serra contre lui.

« Bon temps fait le pied léger… Je me demandais si… »

Il marqua un temps, se passant la langue sur les lèvres, hésitant. La jeune femme lui pressa le bras.

« Et quoi donc ? »

Ernaut lança tout à trac :

« Verrais-tu d’un bon œil que je parle mariage à ton père ? »

Libourc sentit son cœur s’emballer et ses joues se colorèrent tandis que la joie illuminait son visage. Elle s’arrêta brusquement. Il la dévisagea, surpris de sa propre hardiesse, soudain bien désemparé devant le silence.

« Eh bien alors quoi ?

— Bien sûr que oui, mon beau ! Je n’ai pas plus cher espoir que de m’unir à toi. »

Elle se tourna face à lui et enserra son cou de lutteur de ses bras minces, attirant son visage à elle. Sa voix se fit discrète, souffle léger couvrant les bruits alentours.

« Devenir ton épouse devant Dieu et les hommes, voilà mon rêve le plus doux ».

Puis elle scella sa déclaration d’un baiser d’abord très chaste puis empli de passion. Ils ne se séparèrent qu’à regret, lorsque des gamins qui les avaient espionnés se mirent à leur tourner autour, chantant et riant tout en se moquant des amoureux.

« Les petits démons » s’amusa Libourc tandis qu’elle faisait mine de les chasser de la main.

Ernaut la ramena contre son cœur et grogna contre les petits diables, d’un grondement d’ours empli de joie tandis qu’il goûtait ses lèvres une nouvelle fois. Il allait se marier.

## Jérusalem, midi du jeudi 6 février 1158

Un temps mitigé avait succédé à des journées froides et maussades. C’était la première fois depuis plusieurs jours que le soleil émergeait dans le ciel d’hiver. La chaleur de la mi-journée faisait oublier les frimas de la nuit, et à l’abri du vent, contre les murailles sud de la cité, il faisait bon s’alanguir sous les rayons bienfaisants.

Ernaut avait fini son service de la nuit, qu’il avait passé à patrouiller dans les rues et le long des remparts. Il était las, et avalait donc avec bonheur la tourte à la viande et aux herbes que Mahaut avait confectionnée. Il avait encore de nombreuses réticences à son sujet, ce qu’elle lui rendait bien, mais il reconnaissait qu’elle cuisinait divinement. Il espérait secrètement que Libourc avait hérité ce don de sa mère, dans la perspective de se délecter à chaque repas jusqu’à la fin de ses jours.

Il les avait rejoints au marché aux bestiaux, où Sanson était venu, espérant trouver de quoi se constituer un cheptel. Il avait besoin d’animaux de bât et envisageait d’investir dans quelques têtes d’ovins. Mais il n’avait pas trouvé son bonheur et repartait déçu. Cela n’avait néanmoins pas entamé son moral et il parlait de son établissement dans le casal de Mahomeriola avec les accents chantants d’un jeune homme. Il travaillait à avoir une presse à olives chez lui en plus d’un moulin à concasser.

« Une fois tout cela installé, il ne me restera plus rien de ce que j’ai amené de Champagne. Pas une maille ! Mais nous aurons beau manse.

— La terre vaut plus que monnaies d’or répétait mon père, et j’encrois qu’il disait vrai, abonda Mahaut.

— Vous ne saurez œuvrer seul en si vaste domaine, maître Sanson ? Cela me paraît bien grand, s’inquiéta Ernaut.

— Il y a abondance de valets. J’aurais bien acquis un esclave, mais…

— Il est hors de question qu’un de ces Mahométans vive sous mon toit, le coupa Mahaut, pincée. Et puis, j’ai doutance que Dieu approuve ce genre de choses. »

Ernaut fronça les sourcils, étonné de voir Mahaut proférer une telle remarque. Elle s’en aperçut et continua.

« Pour sûr que voilà païens malfaisants ! Mais il me semble qu’il faut en faire de bons chrétiens plutôt que des serviteurs qui flattent notre orgueil.

— Toi, tu écoutes trop les sermons des bons pères, ma douce » s’amusa Sanson.

La remarque éteignit la discussion et chacun s’employa pendant un moment à avaler son repas. Ernaut et Libourc échangeaient des regards complices lorsque leurs mains se frôlaient près du pain ou d’un pichet. Tandis qu’elle piochait parmi des dattes, Mahaut se tourna vers le jeune homme :

« Dis-moi, Ernaut, plutôt que d’avoir serviteurs inconnus, ne voudrais-tu pas labourer aux côtés de mon époux ? Tant qu’à se faire valet, autant le faire sur une terre de sa famille. »

Libourc soupira et Ernaut fit la moue, vexé de la remarque.

« Je ne suis nullement valet, j’ai prêté serment au roi de Jérusalem et le sers comme loyal sergent. Il n’y a là nul déshonneur, rien à comparer avec le valet qui s’emploie à récurer les auges à cochons ou à fendre le bois.

— Vois-tu déshonneur à fendre le bois de ton parent ?

— Je ne…

— La paix, vous deux, s’emporta Sanson. Mahaut, tu ne peux demander à Ernaut de venir en la maison de son espousée, cela ne se passe pas ainsi. »

Ernaut hocha la tête en accord, reconnaissant que Sanson ramène sa femme à la raison. Il avala un peu de vin, le temps de se calmer. Libourc lui serra la main en soutien, souriant avec douceur, inclinant de la tête à de la retenue de sa part. Il enrageait des remarques assassines de Mahaut, mais n’avait guère de moyens de s’en prémunir. Il choisit donc de se justifier, une fois encore.

« Je n’ai pas prétention de profiter des biens qui ne sont pas miens. J’ai conscience que Libourc aura belle dot et je saurai lui garantir un douaire à la mesure. Quand nous partagerons un toit, ce sera le nôtre, et j’ai espoir qu’il ne soit pas moins solide que le vôtre.

— J’ai nul doute à cela, indiqua Libourc, dans un sourire.

— Moi non plus, Ernaut, opina Sanson. Car même si j’ai bonne amitié pour toi, sache que je n’aurais jamais donné ma fille à un homme qui ne la vaille pas. Outre tu sais manier l’épée et cela peut être de quelque utilité en ce pays. L’affaire est dite et je n’y reviendrai pas. »

Il conclut sa déclaration d’un toat silencieux et avala son gobelet de vin tandis que des étoiles brillaient dans les yeux des jeunes gens, indifférents au visage maussade de Mahaut, aussi gris que son voile. ❧

## Notes

Au tournant du XIIe siècle, le mariage latin demeure une affaire essentiellement laïque. Si l’Église s’emploie depuis des années à en faire un moment important de la vie du fidèle (qui aboutira à en faire un sacrement quelques dizaines d’années plus tard), il constitue avant tout une union civile. Ce qui importe aux familles, c’est de réguler les transferts de biens que le départ des jeunes gens et leur installation peuvent provoquer. Généralement, c’est la femme qui quitte le foyer familial pour rejoindre son époux et de complexes échanges sont mis en place pour dédommager les différentes parties qui se voient lésées, amputées d’un membre productif.

Ce n’est d’ailleurs pas l’apanage de la société médiévale européenne et on retrouve de nombreux textes traitant des mêmes soucis dans la documentation légale juive ou musulmane.

L’ingérence de l’Église dans cette cérémonie sera aussi l’occasion de remettre en valeur le consentement des futurs époux, insistant sur l’engagement moral que représente l’union de deux âmes. Cela a certainement participé à renforcer l’importance du sentiment amoureux dans la négociation de telles cérémonies, au détriment des impératifs politiques et économiques familiaux, vraisemblablement prévalents jusque-là.

## Références

Ross David, « Allegory and Romance on a Mediaeval French Marriage Casket », dans *Journal of the Warburg and Courtauld Institutes *11, 1948, p.112-142.

Schnell Rudiger, Shields Andrew, « The Discourse on Marriage in the Middle Ages » dans *Medieval Academy of America* 73, 1998, p. 771-786.

Searle Eleanor, « Seigneurial Control of Women’s Marriage: A Rejoinder » dans *Past and* *Present Society *99, 1983, p. 148-160.

Sheehan Michael M., *Marriage, Family, and Law in Medieval Europe: Collected Studies*, Toronto: University of Toronto Press, 1996.
