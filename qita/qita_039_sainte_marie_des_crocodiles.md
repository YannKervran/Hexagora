---
date: 2015-02-15
abstract: 1157. Retournant chez lui, à Césarée, par la côte, le négociant Sawirus de Ramathes chemine avec un petit groupe de pèlerins, innocents de la réalité des voyages au Levant. Aux abords d’une petite église perdue au milieu des marais, ils vont faire une rencontre étonnante.
characters: Seguin Jacob, Sawirus de Ramathes, Mahran, Armand
---

# Sainte-Marie des Crocodiles

## Saint Jean d’Acre, après-midi du lundi 11 mars 1157

Confortablement installés sous une pergola embaumée de la délicate fragrance des cyclamens, les deux hommes sirotaient un vin frais tout en appréciant le calme de cette journée. Le plus âgé, Sawirus, était emmitouflé dans un épais ’abba[^aba] de laine rayée, une calotte de feutre ornant son crâne dépouillé. À côté de lui, également assis sur une banquette au matelas moelleux, son interlocuteur semblait noyé dans la masse de tissu qu’il arborait, d’amples drapés engloutissant son corps maigre, ne dévoilant que des mains osseuses et un visage décharné.

Il ne devait pourtant guère être âgé, aucun sel ne venant encore éclaircir les poils de sa barbe ou de ses cheveux bruns, soigneusement coupés et coiffés en arrière. Sous des arcades creusées par le manque de sommeil s’affairaient des yeux inquiets, comme s’ils redoutaient à chaque instant de découvrir un prédateur aux alentours. Il s’exprimait néanmoins d’une voix assurée et ses gestes n’avaient rien d’hésitant.

Plus d’un négociant s’était fait berner par l’aspect craintif de Seguin Jacob. Fils d’un juif converti, il avait transformé les maigres avoirs familiaux en un réseau de commerce tentaculaire, s’étendant de Byzance à Grenade. Il voyageait peu, mais accueillait volontiers ses contacts et ses partenaires dans sa vaste et luxueuse demeure, à peu de distance au nord de la Fonde[^fonde] d’Acre.

« Je n’ai espoir de vous garder encore long temps, alors ? J’attends la *Lionne des mers* et *Le beau soleil*, qui doivent décharger moult intéressantes marchandies.

— Je vous mercie de me faire si bel héberge, mais il est temps pour moi de m’en retourner. Mahran est là depuis presque une semaine déjà, et j’ai fait négoce de tout ce que j’avais. Il ne me reste qu’à passer à la Fonde régler les taxes en suspens.

— N’avez-vous pas déjà versé votre écot ?

— Si fait, j’ai payé sur les balles que le jeune Grec m’a achetées, toile et papier, mais il me restait encore à régler sur les verres et les épices qui ont mis à la voile ce jeudi. »

Seguin acquiesça d’un mouvement de tête, avalant un peu de vin tout en se tournant vers le petit jardin enclos orné d’une fontaine qui prolongeait son logis.

« Vous auriez hostel ici, ce serait fort plus aisé pour vous de commercer, mon ami.

— Mon épouse répugne à quitter Césaire. Moi-même j’y suis fort attaché. J’avais acheté cette demeure pour mes vieux jours, et ils sont là. Donc je me conforme à ce que j’avais prévu. »

Seguin n’insista pas. Il savait que le vieux marchand avait perdu son fils unique[^voirqitameilleurfils], des années plus tôt, alors qu’il commerçait. Il avait sûrement espéré profiter d’un repos bien mérité, tandis que son enfant aurait pris sur lui de faire fructifier les avoirs familiaux. Malgré le temps, la douleur était toujours vive, et Sawirus n’aimait guère à en parler.

« Dès que Mahran aura fait changer le fer de ma monture, je prendrai la route.

— Avec les naves arrivant chaque jour plus nombreuses, vous n’aurez guère de mal à trouver caravane à laquelle vous joindre.

— Nul besoin, s’amusa Sawirus. La côte est fort tranquille et je la connais bien assez. En deux journées de chevauchée, je serai rentré. Juste mon valet et moi, nous serons rapides. »

Seguin se leva, s’excusant un instant auprès de son invité. Il traversa la salle principale, vaste pièce aux murs chaulés rehaussés de tapis de prix, dont le sol en était aussi moelleux qu’une prairie à force d’y entasser les nattes et les textiles. Derrière une portière, un petit cabinet lui servait de bureau, et il y avait un coffre fort, bandé de fer, dont les aciers étaient noyés dans la maçonnerie. Au-dessus, de nombreuses niches accueillaient les documents sans valeur, les notes, les courriers dont Seguin était continuellement abreuvé. Il chercha parmi ceux-ci plusieurs papiers et les glissa dans une pochette de cuir qui se fermait d’un lacet.

Lorsqu’il revint vers Sawirus, celui-ci avait les yeux clos et il allait faire demi-tour quand ce dernier l’interpella.

« Restez, mon ami. Le sommeil me déserte, avec les ans, quand bien même mes paupières se closent. »

Il se tourna et souleva un sourcil lorsqu’il vit le paquet. Seguin lui fit un sourire et le lui tendit.

« Quelques messages à faire parvenir à des miens amis, à Jaffa. Peut-être aurez-vous possibilité de transmettre à des voyageurs de confiance.

— Bien évidemment. Surtout en cette saison, les frères de saint Jean et ceux aux blancs manteaux vont recommencer leurs incessants voyages entre les ports et la sainte cité. »

Seguin abonda et ajouta :

« Si seulement cela pouvait suffisamment les occuper pour que la trêve avec Damas ne cesse pas…

— Auriez-vous entendu rumeurs contraires ? Norredin a versé tribut de plusieurs milliers de dinars lors des accords, à l’automne.

— Avec l’attaque du roi Baudoin sur les bédouins du Golan et l’arrivée promise du comte de Flandre[^thierryalsace], je crains fort que les combats ne reprennent avec violence. Le Turc n’est pas homme à se laisser humilier, d’autant qu’il a désormais dans son escarcelle Damas, Mossoul et Alep. »

Sawirus soupira, conscient qu’il n’avait aucun pouvoir sur les incessants combats entre les dirigeants locaux. Quand ce n’étaient pas les Latins qui prenaient l’initiative, c’était un prince turc, ou un chef nomade, ou encore le vizir égyptien. Les armées allaient et venaient, saignant les terres de leur acier insatiable. À peine avaient-ils conquis une place qu’ils convoitaient la voisine ou entreprenaient de se venger d’une autre perdue peu auparavant au détriment d’un principicule[^principicule] aussi violent et avide qu’eux-mêmes.

L’essentiel était de ne pas se trouver dans les parages lorsque les lames étaient dégainées ou que les flèches volaient. Et pour ça, les plaines côtières de Samarie étaient relativement sûres. Il reposa son verre et sourit à son ami.

« D’ici deux ou trois jours, je ferai route pour ma demeure. J’ai voyagé bien assez pour attendre de voir depuis mon hostel comment cette saison va s’annoncer. »

## Saint-Jean de Tyre, après-midi du mercredi 13 mars 1157

Un ciel de plomb noyait de ses filaments cotonneux les pentes des contreforts du mont Carmel, écrasant une mer dont les vagues écumeuses se brisaient sans vigueur sur la côte sablonneuse en larges bandes mousseuses. Le sentier de gravier et de pierre se déroulait en ligne droite, n’esquissant que de timides lacets pour éviter des massifs de broussailles épineuses, des amas de rochers. Au pied des reliefs à leur gauche, un village ramassé auprès de bâtiments religieux et d’une petite tour laissait échapper les échos d’une activité humaine. Quelques barques tirées sur la côte devaient servir aux pêcheurs du lieu.

Sur le chemin qui fendait les parcelles ensemencées, un large groupe avançait : des marcheurs dont la voix portait les hymnes jusqu’aux oreilles de Sawirus. Autour d’eux gravitaient quelques cavaliers dont certains arboraient le blanc manteau des chevaliers du Christ. Ils allaient se rejoindre à une intersection un peu au sud, auprès de laquelle pourrissaient les vestiges de fourches patibulaires. La route était suffisamment fréquentée et patrouillée pour être sûre, et nul seigneur n’avait eu récemment à pendre des brigands pour servir d’avertissement. Même les pirates égyptiens ne venaient guère ici, aucune richesse ne valant la peine d’échouer son navire sur ces plages.

Par ce temps voilé, difficile de dire si des vaisseaux croisaient au large, galées ou simples embarcations de commerce ou de pêche. Chastel-Détroit, où il avait l’intention de passer la nuit, était généralement ravitaillé par voie de mer, comme beaucoup d’agglomérations littorales. Bien que ne disposant pas d’un port, il était plus rapide et facile de transborder sur de petites barges que d’acheminer par caravane ce dont l’hôpital avait besoin. C’était une héberge mise en place par les hommes au blanc manteau, parmi la lande côtière désolée, sur un saillant rocheux.

Lorsqu’ils dépassèrent l’affleurement qui marquait le croisement, un cavalier les attendait. Un Syrien, vêtu d’une armure d’étoffe crasseuse, sur un bidet fatigué. Un turban désordonné ornait son crâne et un sourire forcé barrait son visage. À sa hanche pendaient une longue épée et un petit bouclier. Il leva la main en guise de salut à leur approche et les interpella gaiement d’une voix rauque. Il était là certainement pour s’assurer que Sawirus et Mahran ne constituaient aucun danger pour les pèlerins. Les deux hommes stoppèrent leur monture et le valet en profita pour attraper la gourde qu’il avait trop solidement fixée derrière lui pour la prendre aisément pendant la marche.

Pendant ce temps, Sawirus échangea quelques formules polies avec le soldat. Bientôt la cohorte de marcheurs arriva, encadrée par ses bergers en armes. En queue venait une file de mules lourdement chargées de balles, de tonneaux et de filets. Un des chevaliers du Temple s’avança au trot, d’un air décontracté. Sawirus nota qu’il avait le bras gauche qui s’arrêtait au niveau du coude. Et, à son approche, il dévoila un visage dont les chairs avaient été violemment arrachées. Le nez ne se devinait plus que par les orifices déchirés d’où sortait une respiration sifflante et la bouche elle-même s’ouvrait en une crevasse absurde sur des moignons de chicots. Le regard par contre était aussi ferme et vif que celui d’un maître et la voix profonde portait par-dessus le fracas des pieds, des sabots et des caillasses roulées.

« Faites-vous route au sud, amis ? » lança-t-il alors même qu’il se trouvait encore à bonne distance.

Sawirus hocha la tête avec emphase et attendit que le chevalier soit plus prêt pour répondre complètement, indiquant qu’il allait à Césarée mais comptait faire halte à Chastel-Détroit. À ce nom, le templier afficha ce qui devait lui servir de sourire.

« Fort bien, nous y faisons route de même. Auriez-vous désir de vous joindre à nous ? Nous cheminons fort lentement, mais il est toujours bon de le faire de concert.

— Je ne suis guère empressé. Il y a bon temps pour arriver à Césaire, sans forcer le pas. Et aller en bonne compagnie raccourcit les voyages. »

Sawirus s’efforçait de ne pas examiner les blessures de l’homme, cherchant à attacher son regard ailleurs tandis qu’il lui parlait.

« À la bonne heure ! Ces pérégrins parlent fort peu ma langue, et j’ai toujours plaisir à deviser, en selle. Sur ces côtes, guère plus d’occupation à espérer.

— Je suis bien aise de ne pas avoir autre souci que celui de tenir en selle et d’espérer arriver sans endurer pluie ou mauvais temps. »

La remarque arracha un grognement qui devait être de joie.

« Je dois avouer que je m’ennuie un peu, maintenant qu’on m’a mandé en ces chemins. Je chemine depuis Acre jusqu’à Jaffa cette fois, plus souventes fois au travers de la plaine d’Esdrélon. »

Il soupira, levant son moignon.

« Mais bon, que puis-je être de plus qu’une nourrice en ces lieux abrités ?

— Rien que votre présence doit affermir cœurs et esprits craintifs.

— De certes, si on devait compter la vaillance en plaies et bosses, j’en aurais bons coffres emplis. »

Malgré sa laideur et son aspect brusque, le templier se révéla un agréable compagnon. Le chemin se déroula rapidement tandis qu’ils devisaient de banalités, de généralités sans importance. Le chevalier était fils cadet d’un nobliau antiochéen, d’ascendance nordique disait-il, farouche hériter des féroces païens qui terrifiaient jusqu’au grand Charles lui-même. Mais la guerre était terminée pour lui, ses blessures ne lui permettant plus de pointer la lance comme il seyait à un chevalier. Il finissait de blanchir sous le harnois, attendant de n’être plus bon qu’à cocher les inventaires sur des tablettes de cire quand son cul ne supporterait plus la rigueur de la vie en selle.

Il n’avait pourtant pas plus de trente printemps, estimait Sawirus.

## Abords de Castel-Fenouil, midi du jeudi 14 mars 1157

La pause annoncée, certains marcheurs s’étaient rapidement égaillés parmi les buissons marécageux, mais quelques-uns étaient demeurés près du groupe, aux alentours d’un bosquet entouré de palmiers aux troncs moussus. Une courte berge vaseuse disparaissait dans des flots croupis mangés de roseaux et d’algues aux relents de pourriture.

Dans un des méandres peu éloignés, un couple de grèbes pataugeait, plongeant à la recherche de nourriture. Le ciel bleu cendré accueillait de longues lanières grises pelucheuses qui occultaient le soleil en alternance. Une bise légère, venue du large, mordait les joues, les nez et faisait craquer les massettes et phragmites sans parvenir à rider les eaux stagnantes.

Sawirus profitait de cette pause pour ramasser quelques fleurs qu’il porterait à son épouse. Malgré les années et leur fréquent éloignement, il conservait beaucoup d’affection pour elle et ne manquait que rarement l’occasion de le lui manifester. Il avait déjà une belle moisson d’asphodèles et d’anémones quand un cri résonna, tandis qu’un couple de fauvettes piaillant passa près de lui.

Une des pèlerines arrivait d’un buisson, la robe encore en partie relevée, le voile en bataille et les bras agités comme des moulins à vent. En un instant, l’effervescence gagna le groupe et affola les montures. Le chevalier du Temple dégaina son épée avant d’aller jeter un œil dans la roselière d’où la femme avait jailli. Il revint rapidement, circonspect, et intima l’ordre à ses hommes de prendre leurs lances et de le suivre.

« Que se passe-t-il donc, frère ? s’inquiéta Sawirus.

— Un dragon était caché parmi les herbes hautes. Il est embusqué, mieux vaut l’occire avant qu’il ne nous attaque. »

Sawirus ne put retenir un sourire amusé. Il traversait les marécages si souvent qu’il ne s’inquiétait plus guère des crocodiles, surtout en cette saison.

« Vous faites escorte pérégrine pour la première fois en ces lieux, frère Armand, m’avez-vous confié.

— Si fait, confirma le soldat, tout en délaçant une botte de javelines d’un roncin.

— Alors, ne vous embarrassez pas de cette bestie. Elle ne nous causera nul dommage.

— Je sais bien qu’il ne s’agit nullement d’un démon ailé crachant le feu. Mais j’ai ouï dire que ces dragons étaient féroces et pouvaient tuer d’un coup de queue ou accrocher un homme pour le noyer. »

Sawirus s’approcha, posant une main sur le sac de toile qui contenait les armes.

« Il ne fait guère chaud en cette saison, c’est pourquoi la pauvresse est tombée sur ce crocodile en dehors de l’eau. Il aime à paresser sous le soleil, fort maigre en ces jours. Durant l’hiver et ses frimas, il n’a guère d’appétit. Il est dangereux au fort de l’été, quand il se cèle par les eaux et attend qu’un imprudent s’approche de la berge. »

Le chevalier se gratta ce qui lui tenait lieu de menton et éructa avant de s’esclaffer et d’abandonner le délaçage.

« J’aime autant, je n’ai guère envie de chasser en étant seul à savoir tenir lance et épée ! J’aurais néanmoins été curieux de savoir si sa peau vaut acier comme on le dit.

— Quelques-uns le chassent, depuis ma cité. Mais il ne s’en voit plus tant, et ils savent éviter les hommes. Ou alors n’en laissent aucune trace quand ils en croisent un. »

La remarque amusa le templier, qui se fendit d’une grimace éloquente, quoiqu’effrayante sur son visage fracassé. Sawirus se pencha pour arracher un asphodèle et ajouta :

« J’ai des miens amis qui ont œuvré avec le moulin que nous n’allons guère tarder à croiser. Je ne crois pas qu’aucun n’ait eu des ennuis avec ces serpents. Il suffit d’y prêter attention. »

Le chevalier claqua amicalement le cheval d’un geste automatique et retourna vers ses hommes, afin d’annuler ses ordres. La rencontre inopinée avait malgré tout affolé les marcheurs de Dieu, et ils attendaient tous de reprendre la route au plus vite, parlant dans leur dialecte tout en s’agitant. Un sifflement sec plus tard, les cavaliers étaient de nouveau en selle, et le prêtre dépenaillé qui encadrait les pèlerins levait son bourdon pour indiquer le départ.

Quelques enfants piaillèrent, tendant les bras dans l’espoir d’être portés. Des gorges fatiguées toussèrent en fluxions glaireuses, et des râles accompagnèrent les premières foulées, vite remplacés par la frappe régulière des bâtons, et bientôt couverts par le chant des Psaumes.

Une fois remonté, Sawirus tira sur son nez un des rafrafs de son turban, puis bâilla. Il était assommé, las des voyages, et trouvait la selle chaque fois plus inconfortable. Il repensa machinalement aux deux Yazid : le premier, son fils qui n’avait eu l’occasion de lui succéder, regretté chaque jour sur cette terre. Il aurait pu assurer à Sawirus de quoi vivre sur ses vieux jours, lui permettre de profiter de son épouse, de sa demeure, sans plus avoir à errer par chemins et ports. Le second Yazid, qu’il avait recueilli, il allait le revoir bientôt. Il l’avait confié à l’école cathédrale de Césarée pour lui inculquer quelques notions d’écriture et de comput[^comput]. Le garçon s’était étrangement bien intégré et se rendait avec enthousiasme à ses leçons.

Il se passionnait avec tellement d’entrain au cursus, au *quadrivium*[^quadrivium], que Sawirus avait cru un temps qu’il y avait anguille sous roche. Il connaissait suffisamment le caractère facétieux du jeune homme pour craindre qu’il passe ses journées à rimailler des chansons paillardes plus qu’à étudier. Mais les chanoines lui avaient confirmé qu’il était studieux et appliqué. Il avait redouté un instant qu’il ne soit tenté par une vie errante de jongleur, mais peut-être avait-il seulement besoin d’abreuver son âme. Ou alors, il avait compris tout l’intérêt qu’il y avait à devenir clerc. Sans y prendre garde, Sawirus se mit à fredonner :

>« Las, le père ne sut que faire  
>Fils à la tonsure te confère[^voirchansondouxmetier]… » ❧

## Notes

Malgré la situation de guerre endémique, de nombreuses personnes arpentaient les routes et chemins du Moyen-Orient. Marchands et pèlerins, essentiellement, allaient et venaient, parfois au moment où des combats se déroulaient à quelques dizaines de kilomètres de là. Il existe des relations de voyages qui ne font aucune allusion aux affrontements alors même que l’auteur s’était trouvé dans des zones de conflit. Il semble que, comme la météo et les attaques de bêtes sauvages, la présence de troupes ennemies soit intégrée parmi les maux possibles que le voyageur pouvait rencontrer. Il était donc normal de chercher à s’en prémunir et d’y porter attention, mais sans que cela n’empêche de se déplacer. Des commerces de location de montures et d’animaux de bât existaient d’ailleurs, et les textes de loi comportaient des rubriques pour gérer la blessure ou la disparition de l’animal loué.

En ce qui concerne la faune rencontrée lors de ces déplacements, le contact avec des créatures peu habituelles a permis en certains cas à l’imaginaire de prendre le pas. Mais même sans cela, on est souvent étonné de la méconnaissance à propos de bêtes pourtant répandues. De rares familiers, généralement ceux qui avaient à fréquenter au quotidien ou à éliminer les nuisibles, étaient plus au fait de la réalité. Les faibles connaissances zoologiques expliquent qu’un grand nombre d’approximations et d’erreurs grossières aient continué à être maintenues tout au long de la période, parfois en s’appuyant sur des aberrations remontant à l’Antiquité.

## Références

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. I & II.*, Cambridge : Cambridge University Press, 1993.

Mariño Ferro Xosé Ramón, Girard Christine, Grenet Gérard (trads.), *Symboles animaux, Un dictionnaire des représentations et croyances en Occident*, Paris : Desclée de Brouwer, 1996.
