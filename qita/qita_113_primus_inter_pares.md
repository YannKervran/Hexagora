---
date : 2021-03-15
abstract : 1143-1161. Au fil des ans, le roi Baudoin doit affirmer son pouvoir tout en ménageant les hommes compétents qui composent la Haute Cour. La moindre de ses décisions est influencée, commentée, jugée et validée par tous ceux et toutes celles qui doivent en subir l’application.
characters : Gembert, Baudoin III de Jérusalem, Foulque de Jérusalem, Aymar, Jehan, Droart, Ernaut, Libourc, Ysabelon, Geoffroy Foucher, Bertrand de Blanquefort
---

# Primus inter pares

## Acre, château royal, matin du mercredi 10 novembre 1143

La brume matinale s’était retirée rapidement, laissant un timide soleil chasser les ombres récalcitrantes. La cour, gorgée de l’eau des averses nocturnes, n’était que flaques et boue, mais cela ne semblait nullement gêner le jeune homme blond, richement habillé, agenouillé auprès de chiots. Il agaçait d’un bâton ceux qui voulaient jouer, éprouvant le mordant des plus vifs d’entre eux. À ses côtés, un valet aux jambes arquées, vêtu de toile solide, surveillait d’un œil distrait que les bêtes n’en prenaient pas trop à leur aise. Si le prince Baudoin était amical et peu enclin à reprocher à quiconque ses propres décisions, la reine était moins débonnaire vis-à-vis des serviteurs qui laissaient son fils revenir crotté comme un vilain. Il y avait des limites à la simplicité de mœurs estimait-elle.

« Je choisirais bien le petit marron pour diriger ma meute, il me semble plein d’allant et a le regard qui sied à bon chasseur !

— Sire, il est trop jeune. Il lui faudra faire ses crocs avant de prétendre mener la courre.

— Fi donc, il me paraît solidement taillé, jarret épais et large gueule. Il doit bien peser une demi-livre de plus que ses frères !

— Cela ne saurait suffire. Il ne servira de rien sans soumission des autres. La meute se fait ainsi que l’a voulu Dieu, et pas au souhait de l’homme, fut-il prince de grand royaume. »

Gembert attrapa le chiot pour l’examiner d’un œil sévère, tout en montrant une bête adulte enfermée avec ses pairs dans l’enclos de clayonnage peu loin.

« Regardez Tiens-Bon : comment il se tient… »

Baudoin releva la tête et étudia la meute qui se tenait au repos. Ce n’étaient pas les chiens qu’ils préféraient, ayant l’habitude des bêtes de Jérusalem. Sur la côte, il n’aimait rien tant que lancer ses rapaces, qu’il pouvait admirer à loisir tandis qu’ils survolaient les vagues. Il ne connaissait donc pas personnellement chacun des animaux. Ne voyant pas où voulait en venir le maître veneur, il haussa les épaules.

« Il me semble surtout qu’il a grand désir de sieste, allongé comme il est.

— Regardez plus avant : il garde les yeux ouverts et, surtout, il est en hauteur.

— Comme chacun, il n’aime guère s’affaler en la boue !

— Surtout, il peut voir tous les siens, guetter lesquels gagnent de l’assurance, lesquels se tiennent cois…

— Es-tu en train de me dire que simple cabot surveille sa mesnie comme un juif ses deniers ? Me prends-tu pour jouvenceau de cité ? »

Gembert laissa échapper un rire rauque et rendit l’animal au jeune prince, s’approchant pour lui montrer ce qui se déroulait dans l’enclos.

« Je leur ai donné quelques restes ce tantôt. Il en conserve plus bel os entre ses pattes, mais a laissé ce qui ne l’intéressait pas. Il surveille qui y arrive le premier et qui ose passer dans ses traces. Il sait que le défi viendra de là. S’il a désir de garder la tête, il lui faut être au fait de qui a volonté de le pousser hors. Demeurer plus haut lui garantit de ne rien manquer du spectacle. Outre, il se fait voir ainsi que le donjon d’un seigneur à ses vilains : il leur manifeste sa primauté. »

Baudoin acquiesça, caressant le chiot machinalement. Il n’avait jamais pensé que la constitution d’une meute répondait à autant de règles. Habitué à ce que les choses se conforment à ses souhaits, il découvrait la nécessité d’assimiler la façon dont les animaux fonctionnaient. Cela contrariait quelque peu ses désirs, mais il en éprouvait le bien fondé. Il n’était pas de ces jeunes nobliaux fats et trop imbus d’eux-mêmes pour ne jamais rien apprendre des plus modestes. Il y avait du bon sens à retirer de la main qui sème, du dos qui porte et même du chien qui ronge son os, il en était persuadé. Il se mit à étudier le petit animal qui, excité par les manipulations, tentait de lui mordre le pouce.

Une effervescence dans la cour le fit se retourner vers le logis, d’où venait de sortir la silhouette empâtée de leur seigneur à tous, le roi Foulque son père. Celui-ci s’avançait à grandes enjambées, agité ainsi qu’à son habitude, un sourire aux lèvres, appréciant la chaleur des rayons sur son visage. Vêtu d’une sobre tenue de toile solide, il héla son fils de sa voix habituée au fracas des lances et des chevaux :

« Fils, nous allons courir sus quelques bestes, si Dieu le veult. Ta mère a grand désir de sortir de cette cité pestilentielle, et j’aurai moi-même bon plaisir à galoper en nos terres avant que l’hiver ne vienne nous confiner entre quatre murs tels tonsurés. Va donc quérir ton cadet : aujourd’hui, nous chassons ! »

Heureux de cette perspective, Baudoin rendit le chiot au veneur et se hâta d’aller préparer quelques affaires. Il repenserait plus tard à sa propre meute, quand il serait en âge. Pour le moment, il se contenterait de profiter de celles de son père. Il n’était pas temps pour lui de s’encombrer de telles préoccupations.

Il ne le savait pas encore, mais c’était là le dernier jour d’insouciance de sa vie.

## Naplouse, cour du manoir seigneurial, matin du vendredi 23 juin 1161

Installé sur un banc près de la grande porte, Aymar gardait un œil sur les entrées et sorties. Occupé à graisser sangles et sacoches, il réfléchissait aux scénarios qui s’offraient à lui pour les semaines à venir. La maladie de Mélisende était dans tous les esprits, car le destin de leur maître à tous, Philippe de Milly, était trop imbriqué à celui de Mélisende pour que la disparition de cette dernière ne soit pas sans conséquence. Le seigneur de Naplouse était l’ultime grand baron qui avait fait fronde avec la reine quand Baudoin avait désiré s’affranchir de sa tutelle. Et si depuis pratiquement une dizaine d’années, il régnait quasiment sans partage, il avait sévèrement puni tous ceux qui avaient alors fait le mauvais choix. Tous, sauf Philippe de Milly, seigneur de la ville où la reine mère s’était retirée.

Aymar savait qu’un soldat capable comme lui trouverait toujours de l’embauche, mais il avait pris ses aises à Naplouse, où il logeait dans une petite maison avec femme et enfants depuis plusieurs années. Il s’était habitué à son figuier, ses poules et la nuée de chats qui baguenaudaient dans le quartier. Il avait déjà vécu l’exil lorsque son précédent maître, Manassès de Hierges, avait connu la disgrâce et n’avait aucune envie d’y être de nouveau confronté.

Il reconnut la silhouette empressée d’un des clercs de la reine qui sortait du grand bâtiment. Formé dans un monastère, il avançait à petits pas ainsi qu’il seyait à un digne lettré, mais il le faisait si vite qu’il marquait le rythme de la tête d’une façon semblable à un gallinacé. Aymar posa son pot de graisse et s’essuya les mains tout en venant à la rencontre du secrétaire.

« Le bon jour, Jehan. As-tu porté quelque nouvelle réjouissante à mon sire ?

— Il n’appartient pas au messager de choisir les oreilles qui doivent entendre ! » répondit le fin notaire, horrifié de se faire ainsi apostropher.

Puis, comprenant toute l’angoisse de la situation dans laquelle tous étaient plongés par la maladie de Mélisende, il se fendit d’un sourire bref avant d’ajouter d’une voix plus amène.

« Ton maître a grand amour de la reine, il est honnête qu’il sache matin et soir comment évolue sa santé…

— Nous prions tous pour sa guérison rapide, soyez-en assuré.

— De cela je ne doute. Seulement, je crains que celles-ci n’aident à sauver que son âme. Sa dépouille mortelle est au plus mal, malgré tous les bons soins de ses sœurs, de ses médecins… »

Aymar gratta son imposant nez, comme il le faisait chaque fois qu’il était ennuyé, se rapprocha du clerc et lui demanda à mi-voix :

« Le roi n’a mandé nul coursier ? »

Le visage du clerc d’austère devint pathétique.

« Rien. Pas même un message de circonstance. Nul n’ignore la détestation qu’il éprouve, mais c’eut été fort chrétien…

— Il se murmure qu’il affûte sa hache pour le col de l’ultime féal de Mélisende. Tout le monde ici ajoute à son désespoir de savoir la reine si mal l’angoisse d’imaginer l’hôtel de Milly être mis à bas.

— Nous serons de certes les derniers à découvrir les intentions de Baudoin. Nous en parlons souventes fois avec mestre Gui, qui a porte ouverte en la chambre de la reine. Il se murmure que son fils a gros appétit de ces terres et qu’il y verrait bien simple châtelain y percevoir les taxes en son nom.

— Tout de même, il ne peut ainsi congédier baron comme le sire Philippe !

— C’est le roi. Il a la main au Grand Conseil. »

Jehan se fendit d’un sourire matois, savourant ce qu’il savait comme une délicieuse sucrerie.

« Malgré tout, il doit compter avec ses tantes. La comtesse et l’abbesse ne laisseront pas les choses se faire sans peser de tout leur poids. Et qui les connaît ne peut ignorer combien elles ont l’oreille des prélats, pour le moins. »

Aymar n’était pas au fait des jeux d’alliance et de la subtilité des rapports de pouvoir dans la famille royale. Il avait entendu d’étranges rumeurs autour d’Hodierne de Tripoli, dont l’époux avec lequel elle était en conflit avait fini sous les coups d’un assassin. Et il connaissait de réputation l’abbesse de Béthanie, à la tête d’un des plus importants lieux de pèlerinage des territoires latins. Quel que soit le rôle qu’elles joueraient, si elles demeuraient loyales à leur sœur, ainsi qu’elles l’avaient toujours fait, les choses ne seraient peut-être pas si aisées pour Baudoin. Et, après tout, Philippe de Milly n’avait-il pas travaillé d’arrache-pied à protéger Jérusalem ces dernières années ? Son dévouement à Mélisende était avant tout fidélité à la couronne, et depuis que Baudoin avait pris seul les rênes du pouvoir, le seigneur de Naplouse n’avait jamais restreint son énergie pour soutenir les ambitions royales.

## Jérusalem, Temple de la Milice du Christ, veillée du mercredi 5 juillet 1161

En fin de journée, la terrasse préférée du maître de la Milice Bertrand de Blanquefort offrait une magnifique perspective sur le dôme du Temple. Il y venait pour assister à l’engourdissement progressif de la ville tandis que les lueurs plus vacillantes des flammes domestiques succédaient au flamboiement du soleil couchant. Il en profitait pour relire certains courriers, en espérant trouver l’inspiration pour des réponses durant son court sommeil. Il appréciait aussi de jouer aux tables, surtout aux échecs, qui lui permettaient de s’abstraire du monde pour focaliser sa pensée sur un problème ou l’autre. Il invitait donc à tour de rôle ses principaux officiers, ses intimes ou ses familiers pour passer agréablement ces soirées citadines.

Il avait une affection toute particulière pour Geoffroy Foucher, le neveu du maître précédent. Fils de bonne famille au tempérament batailleur, celui-ci s’était assagi en venant en Terre sainte. Bien qu’il fût tout à fait à l’aise à rompre des lances, il était rarement employé comme combattant, envoyé là où ses talents de mesure et de diplomatie pouvaient être plus efficaces. On le disait capable de parler plusieurs langues, de lire le latin aussi bien qu’un clerc et il avait en tête les relations et la généalogie de nombreuses lignées régnantes, y compris chez leurs adversaires musulmans. Mais son plus grand atout était de pousser le pion agréablement, sans chercher à gagner à tout prix, accordant la victoire non sans avoir opposé suffisamment de résistance pour qu’elle ne soit pas factice. Bertrand soupçonnait là quelque habileté de diplomate, mais appréciait de se les voir appliquer. Il n’était jamais insensible aux marques de respect.

Il allait donner la main à son adversaire d’un soir, certain de son succès prochain, quand il aborda une des questions qui lui tournait en tête depuis quelques semaines.

« Vous qui avez bonne affection pour la reine, vous devez être au fait des inquiétudes qui agitent la haute Cour. N’avez-vous rien à m’en dire qui puisse m’éclairer ?

— Mélisende est pour moi une amie fidèle et un soutien sans faille, mais je n’ai que vague connoissance de ces sœurs. Quant au sire de Milly, auquel vous pensez, nous nous côtoyons, sans que je ne puisse me prétendre son intime.

— C’est lui que j’ai en tête, de certes. Tout le monde sait que Mélisende vit ses derniers instants. Son confesseur l’entend chaque jour dit-on, la pauvre âme. Et sans le respect qu’elle inspirait encore à son fils, si ce n’était plus de la crainte, il y a de quoi s’inquiéter du destin de son plus indéfectible fidèle.

— La couronne a plus que jamais grand besoin d’hommes de sapience et de vaillance. Baudoin n’est pas irréfléchi…

— Certes pas, mais il a la rancune tenace et la mémoire longue, contrairement à son père. Milly compte-t-il quelques appuis qui pourraient s’exprimer en sa faveur ? Le connétable pourrait glisser quelques mots aimables propres à panser l’orgueil blessé de Baudoin…

— Onfroy n’est guère pressé de faire remembrance au roi de ces moments où lui-même a hésité. Mais il connaît la valeur du sire de Milly, et ne manquera de soutenir quiconque parlerait en son nom. »

Geofroy déplaça un de ses comtes, sachant qu’il exposait là quelques-uns de ses piétons. Mais cela ne lui importait guère, il n’aimait pas tant les échecs pour chercher à y remporter toutes les guerres. Y accomplir quelques belles manœuvres seyait mieux à son esthétique belliqueuse. Tandis que le maître réfléchissait à son tour, il passa en revue les éventuels soutiens que pouvait compter le baron vieillissant de Naplouse.

« Il est avéré que le sire évêque d’Acre a bonne estime de sire Guy, mais il n’a pas tant l’oreille du roi… »

Geoffroy marqua une pause tandis son cerveau passait en revue les relations interpersonnelles dont il avait connaissance autour de lui, et qui contenaient Philippe de Milly et le roi Baudoin dans leurs toiles. Puis il eut une soudaine inspiration.

« Mais, attendez, nous pourrions tout simplement en appeler au sire comte de Jaffa ! Il a toujours gardé bonne amitié aux proches de sa mère, et saura toucher par quelque parole tendre son frère ainé. Ce serait naturel ambassadeur.

— Vous l’en pensez capable ? On le décrit assez mol de caractère, irrésolu et peu volontaire.

— Ce sont là billevesées de médisants, je vous assure. Prêtées à lui en raison d’un bégaiement dont il a tardé à se débarrasser. C’est, à n’en pas douter, une belle âme, qui n’hésitera pas à faire droit à ceux qu’il a tant côtoyés avec sa mère, enfant.

— Il faudra que ce soit vous qui lui parliez, alors. Vous saurez trouver les mots qui feront mouche. Nous ne pouvons nous défaire d’un homme de la valeur de Philippe de Milly en ces temps troublés. Je n’ai guère envie de le regarder prendre les voiles pour la Flandre ou la Bourgogne. »

Geoffroy partageait l’analyse du vieux stratège. Il avait vu les principaux dirigeants des territoires latins se faire capturer ou mourir en l’espace de quelques années. Partout ce n’étaient que régence et mineurs. Et, pour leur plus grand malheur, il n’y avait plus de célèbre abbé par-delà les mers pour en appeler à sauver la cité du Christ.

## Jérusalem, demeure de Droart, soirée du vendredi 21 juillet 1161

La cité était encore agitée, ses rues emplies des pèlerins fébriles à l’idée de fêter la prise de la ville, quelques décennies plus tôt. Les habitants préféraient donc se calfeutrer chez eux pour y passer la veillée en bonne compagnie. Comme souvent, Droart recevait ses intimes, Ernaut et Libourc avaient une nouvelle fois pu goûter les excellents plats d’Ysabelon. Cette dernière se montrait plus enjouée depuis le mariage, ayant reconnu en Libourc les façons d’une de ses jeunes sœurs. Elle s’était depuis lors mis en tête de lui communiquer tous les secrets qui lui seraient nécessaires pour supporter les hommes, disait-elle.

Ayant abandonné les femmes et les enfants dans la cour, Ernaut et Droart étaient montés sur la terrasse pour y admirer l’apparition des étoiles. Malgré le brouhaha des quartiers environnants, le lieu respirait la sérénité. Avec les beaux jours, des matelas y étaient installés et il arrivait même que la famille y passe la nuit.

« Tu as déjà pensé à quitter l’hostel le roi, Ernaut ? »

La question, posée d’une voix calme par Droart, fit sursauter Ernaut, qui tourna un visage inquiet vers son ami.

« Que me chantes-tu là ? Je viens de me marier et je n’ai nulle famille ici, nul bien. Que pourrais-je faire d’autre ?

— Je me demande parfois si le roi est… bon maître.

— Tu as encontré quelques soucis ces derniers temps ? »

Ernaut n’ignorait pas que Droart était impliqué dans de nombreux trafics, mais cela restait dans les limites acceptables par tous. Sans qu’il se sente suffisamment confiant pour s’associer, il n’éprouvait nulle inquiétude pour son ami, sauf à vouloir faire un exemple, ce qui arrivait malgré tout de temps à autre afin de garder la mesure.

« Non, rien de personnel. Je pensais au sire de Milly. Mélisende n’est pas encore en son tombeau que Baudoin règle ses comptes. Il l’envoie au désert.

— De quoi parles-tu ?

— J’ai dîné tantôt avec Brun et Raoul. Une charte se prépare, qui fait échange de provinces entre la couronne et Philippe. Il va perdre tout ce qu’il possède à Naplouse et ne récupère que les pierres et la poussière d’Oultre Jourdain, la seigneurie de Montréal et de Kérak.

— Tout de même, voilà terres royales, non ? Bel exil pour qui lui a déplu. À ce compte je veux bien en être.

— Une charruée là-bas ne vaut pas courtil de Samarie, crois-m’en : au-delà de la Rivière du Diable, c’est le bout du monde. »

Ernaut ne s’expliquait toujours pas l’attachement que beaucoup éprouvaient envers Mélisende. Lui avait juré sa foi au roi Baudoin et il ne le trouvait pas mauvais maître. Chaque fois qu’il l’avait croisé ou aperçu, il lui avait fait l’impression d’être courtois et affable et nul ne le dépeignait en mal parmi les domestiques de l’hôtel qui le côtoyaient chaque jour.

Sa poitrine se comprima néanmoins à cause de la dernière remarque de Droart. Cela faisait plusieurs années qu’il était sans nouvelle de Droin, dont il avait appris les démêlés avec les moines près du Jourdain. Connaissant son compagnon d’enfance, il n’avait pu demeurer en retrait bien longtemps et, si on n’entendait plus parler de lui, c’était certainement parce qu’il était parti dans un endroit loin de tout.

« C’est toujours le royaume, tout de même !

— Chaudemar et les grottes des ladres au sud font partie de Jérusalem aussi… »

Ernaut se laissait contaminer par l’humeur nostalgique de Droart. Il se demandait si, à l’avenir, on lui reprocherait ses amitiés, ses liens avec l’un ou l’autre, et qu’on lui signifierait son exil parce qu’il avait accordé sa confiance à la mauvaise personne ? Il avait désormais une épouse, une famille dont il espérait voir les enfants grandir. Partageraient-ils le poids de ses fautes ? Lui si habitué aux fanfaronnades quand on parlait de son physique, il se sentait soudain les épaules bien lourdes. Sans y penser, il jouait avec son anneau d’or. ❧

## Notes

Au tournant de la décennie 1160, de nombreux changements eurent lieu au sein de la noblesse de Jérusalem, en partie causés par les batailles et guerres incessantes, mais aussi par voie légale. J’ai déjà évoqué la charte qui installa Guy de Milly au sud du royaume, après avoir abandonné ses terres. Certains y voient une disgrâce, d’autres un choix stratégique. J’ai pris l’option de considérer les deux hypothèses mêlées. Baudoin n’était pas un autocrate comme pouvait l’être Louis XIV et il devait sans cesse gérer les influences à la haute Cour pour arriver à ses fins. Ce qui pouvait parfois occasionner des dénouements complexes à appréhender.

Par ailleurs, le destin d’Ernaut est lié à ces événements et sa vie va connaître de profonds bouleversements dans la décennie qui s’ouvre, en partie à cause de cette charte, ce qui explique aussi son importance comme point de pivot de plusieurs Qit’a.

## Références

Barber Malcolm, « The career of Philip of Nablus in the kingdom of Jerusalem », in Peter W. Edbury et Jonathan P. Philips (dir.), The experience of crusading, Cambridge University Press, 2003, vol.2, p. 60‑78.

Burgtorf Jochen, *The Central Convent of Hospitallers and Templars: History, organization, and personnel (1099/1120-1310)*, Leiden - Boston, Brill, 2008, vol. 50.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », Dumbarton Oaks Papers, 1972, vol. 26, p. 93‑182.





