---
date : 2019-02-15
abstract : 1158-1159. Avançant en âge, Ernaut commence à s’organiser afin de pouvoir épouser la jeune femme qu’il aime. Pour cela, il lui faut oublier le garçon insouciant qu’il était et chercher à devenir un chef de famille responsable.
characters : Ernaut, Abdul Yasu, Arnulf, Droart, Sanson de Brie, Mahaut, Libourc
---

# Fondation

## Jérusalem, sud du mont Sion, après-midi du dimanche 27 juillet 1158

Malgré un soleil étouffant, une large foule s’était amassée sur les pentes desséchées du mont Sion. Beaucoup de visiteurs venus célébrer l’anniversaire de la prise de la cité étaient restés quelques jours et profitaient donc de la petite foire qui s’était agglutinée au fil des ans autour d’un concours estival d’archerie. On y déambulait autant pour admirer des bateleurs, des chanteurs et des montreurs de bêtes que pour y voir les prouesses des tireurs.

Fatigué de sa récente campagne militaire, Ernaut n’avait pas eu le courage de se rendre jusqu’à Mahomeriola pour y assister à la messe comme chaque fois qu’il en avait l’occasion le dimanche. Il était resté à se reposer tard dans la matinée et n’était venu flâner au dehors des murs que poussé par la curiosité envers les clameurs dont il entendait les échos.

Un village de toile et de cabanes s’était monté, hébergeant commerces de colifichets, étals de merciers et marchands de boissons. Des vendeurs ambulants proposaient de l’eau, du vin ou de la bière, des pâtés salés et des friandises. Des enfants couraient, battant des mains à la découverte de chiens cabriolant, de singes dansants ou de panthère acceptant les caresses. Le tout au milieu du tintamarre des instruments et des voix en concurrence pour attirer l’attention des badauds. La chaude poussière d’été, soulevée par les milliers de pieds, irritait la gorge et noyait de gris les vêtements de fête colorés.

Ernaut avait tenté de suivre un spectacle de théâtre d’ombres en langue locale, mais les moments chantés et l’usage de fréquents termes argotiques lui rendaient la compréhension difficile. Il en avait néanmoins bien saisi le thème, à la fois ordurier et pornographique, qui tournait autour des mésaventures d’un naïf fellah ayant fui sa campagne pour une opulente cité. Floué et trompé par tous ceux qu’il croisait, il en était à se lamenter sur les malheurs de sa condition lorsqu’Ernaut entendit la voix amicale d’Abdul Yasu qui, l’ayant reconnu, s’avançait vers lui, un large sourire sur les lèvres.

Ils se congratulèrent avec chaleur et, ne s’étant pas vus récemment, décidèrent d’aller quérir de quoi se désaltérer pour prendre des nouvelles l’un de l’autre. Ils espéraient trouver où s’asseoir sous les arbres pour partager leur boisson, mais les places étaient rares. Ils remontèrent donc jusqu’aux abords du mur de l’abbaye du Mont Sion, où ils s’affalèrent sur un rocher faisant saillie sur le périmètre. Abdul était, comme toujours, très bien habillé, et paraissait sortir de chez le barbier. Ernaut se sentit légèrement en défaut, vêtu sans recherche et mal rasé. Il n’était d’ailleurs pas allé aux bains depuis plusieurs jours.

Ils échangèrent à propos des événements récents, Ernaut profitant de sa fraîche expérience de soldat pour se mettre en avant. Il fut un peu déçu de voir qu’Abdul ne semblait pas accorder à la valeur militaire autant de prestige que ses amis latins. Il s’intéressait plus aux ragots autour du palais et aux dernières nouvelles du mariage royal. Il était très curieux de savoir si Ernaut avait pu découvrir à quoi ressemblait la jeune Théodora, future reine de Jérusalem, dont on vantait la beauté et la richesse. Abdul se rappelait qu’il avait été impressionné par Mélisende lors de son union avec Fouques. Il gardait un souvenir ému de l’avoir rencontrée au cours d’une session publique où elle avait accueilli son père parmi certains plaidants. Sans vraiment faire de politique, il regrettait qu’elle soit désormais retirée à Naplouse, les privant ainsi de sa présence bienfaisante. Il n’avait aucune sympathie pour le jeune roi friand de batailles et d’exploits, se risquant à l’évoquer en des termes qu’il n’attribuait en général qu’aux Turcs, auxiliaires militaires utiles, mais trop peu civilisés à son goût.

Ernaut se contentait de hocher la tête à ses remarques, découvrant une fois de plus la divergence d’affinités qu’il avait avec les locaux. Lui aimait à entendre parler des hauts faits d’armes des grands capitaines de guerre. Il avait d’ailleurs développé depuis peu un certain attrait pour le prince d’Antioche, Renaud de Châtillon, dont l’audace et la férocité nourrissaient la verve de nombreux conteurs de rue.

Avec la chaleur, leurs bouches se desséchèrent à tant discuter, et ils se turent un moment, profitant en silence de leur vin, admirant l’agitation de la foule autour du long champ de tir. Abdul avait aussi acheté quelques biscuits à la figue, qui laissait leurs doigts collants de miel.

« Dis-moi, Abdul, ta famille est dans la cité depuis de nombreuses années ?

— Que oui ! Quand Al-Hākim bi-Amr Allah[^alhakim] détruisit le tombeau du Christ, mes pères étaient déjà des anciens, ici.

— Saurais-tu me dire où je pourrais trouver bel hostel où installer femme et enfants ?

— Cela ne dépend que des besants que tu y pourras consacrer, mon ami, s’amusa Abdul. Vers les jardins du quartier du Patriarche, aux abords de la tour de Tancrède, mieux vaut être marchand de soie et de poivre. Mais vers la porte des Tanneurs, tu encontres de quoi t’abriter à faible prix. Êtes-vous nombreux à devoir y habiter ?

— Pour l’heure, je ne saurais dire. J’ai trouvé accord avec la famille de la femme que je vais épouser, et j’ai désir de la bien loger. Nous aurons peut-être valet et, j’espère, forte marmaille…

— Te voilà aussi prêt à porter le joug ! » ricana Abdul, dont les perspectives de noces semblaient moins enthousiasmantes[^voirqitatraditions].

Il avala un peu de vin, expira longuement et lança un coup d’œil vers les murs au Nord.

« Il y a pas mal de belles demeures à des prix corrects, dans le quartier d’ici, au près de la porte Beaucaire. Si tu n’as rien contre les Ermins[^ermin], du moins. Ils sont surtout par là, dans la Cité. C’est plus calme que la rue du Temple où tu résides, ou les environs du palais. »

Il réfléchit encore un petit moment, tirant les poils de sa barbe.

« Il n’y a que fort peu de belles demeures à prix correct auprès du Sépulcre et de l’hôpital Saint-Jean, mais en te rapprochant de la Juiverie, tu pourrais trouver de quoi te satisfaire. En plus tu ne serais pas loin de la taverne du vieux Josce !

— Je ne sais s’il est bien sage de vivre si près d’un tel endroit, s’amusa Ernaut. Mais il est vrai que le quartier est fort agréable. J’y ai vu de nombreux jardins et les rues bien closes à la nuit y rendent les périls fort rares.

— J’ai quelques amis d’amis qui résident par là-bas. De nouveaux arrivants amenés par le vieux Baudoin[^baudoinI] depuis l’Oultre-Jourdain lorsque les Juifs furent chassés. »

Ernaut leva son verre en un toast silencieux en guise de remerciement. Il n’avait pas escompté pouvoir si bien s’installer lorsqu’il avait commencé à penser à un possible mariage avec Libourc. Mais le vieux Sanson serait certainement sensible à un bel établissement et le dédommagement qu’il toucherait des imbéciles qui l’avaient agressé[^voirt3] serait bien investi, en offrant un accueillant cadre de vie à sa famille.

## Jérusalem, palais royal, après-midi du jeudi 25 juin 1159

C’était la première fois qu’Ernaut pénétrait dans les quartiers personnels du vicomte depuis qu’il était entré au service du roi. Ce n’était qu’une petite chambre, mais avec une belle fenêtre qui donnait sur une courette aménagée en jardin. On y entendait piailler quelques oiseaux malgré la chaleur de l’après-midi. Devant un lit imposant aux courtines de sobre toile de laine à la couleur passée, Arnulf était agenouillé face à un lourd coffre bien ferré et finissait de le cadenasser.

« Tu peux prendre cette huche, dès à présent, et la porter avec mes autres affaires près la grande cour. »

Lorsqu’Ernaut acquiesça, Arnulf vit bien que le jeune homme était contrarié. Il l’interrogea du regard sans un mot, ainsi qu’il le faisait souvent pour inviter un des sergents à s’exprimer. Ernaut n’hésita qu’un instant avant de prendre la parole d’une voix embarrassée.

« Est-ce bien vrai, mon sire vicomte, que vous quittez service le roi ?

— Je vois que la nouvelle a couru bien vite. Nous en ferons annonce ce dimanche, après messe, en la grande salle de la Haute Cour. »

Ernaut hocha la tête, atone. Sans pour autant s’estimer proche de lui, il s’était habitué à Arnulf et l’appréciait pour son abord franc, quoique parfois rude. Malgré tout, il était bon meneur d’hommes et savait exercer son autorité sans avoir à la démontrer. Il était aussi constamment à l’affût de la moindre rumeur. S’il était compétent en tant que vicomte, c’était dû à sa capacité à anticiper, encore plus qu’à son habileté à régler les problèmes.

« Et murmure-t-on le nom de qui va me succéder ?

— On dit que ce serait l’ancien maréchal le roi, sire Eudes de Saint-Amand.

— Je vois que les murs écoutent fort bien. Quoi de plus ? »

Ernaut inspira longuement. Arnulf était homme de palais depuis suffisamment d’années pour savoir que le personnel d’une maisonnée était toujours le premier à être informé des choses. Il n’était pas rare qu’un valet soit au courant avant son maître des nouvelles qu’il allait recevoir ou entendre. C’était de bonne fame, et il n’y avait pas à en avoir honte. Pourtant, il n’était pas à l’aise devant cet homme inquisiteur, dont les yeux sondaient l’âme, depuis leur abri de sourcils noirs.

« Que mestre Ucs s’en va également…

— C’est presque cela. Il te faudra d’ici peu lui donner du sire, à lui aussi. Il sera mon féal et recevra fief-rente, comme chevalier du comte qu’il sera. »

Arnulf s’assit sur son coffre, prenant son menton dans sa main durant un moment. Il savait Ernaut curieux à rendre jalouse la plus entreprenante des pies, mais il avait perçu en lui de grandes capacités. Peut-être que ce temps de transition lui serait profitable. Parmi les officiers royaux, on parlait parfois de ce jeune géant à l’intelligence surprenante qui officiait au palais. Il appréciait surtout son dévouement, sa sincérité à servir. Pour lui, c’était la principale qualité d’Ernaut.

« Je ne serai plus homme le roi, mais de l’arrière-ban, châtelain de son frère à Blanchegarde. Ucs me suit, car il me faut avoir à ma dextre quelqu’un à qui je puisse me fier. Outre cela, le sire Eudes sera châtelain le roi ici, à Jérusalem. Nul n’avait détenu ce titre en sus de celui de vicomte depuis le vieux Rohard, des années avant que tu ne viennes. Cela veut dire que beaucoup de choses vont changer, mon garçon. Pareils temps sont propices aux personnes de talent. »

Il fixa un moment le colosse face à lui, appréciant le visage massif et les épaules larges, les bras puissants. S’il savait utiliser sa tête aussi bien que ses muscles, Ernaut avait de quoi aller loin. Ne restait que la question du cœur, dont Arnulf se désintéressait généralement, laissant à Dieu et à ses clercs ce soin, se contentant d’une obéissance sans faille à ses instructions.

« Beaucoup ont frappé à mon huis ces derniers jours, afin de me vanter leurs mérites ou de chanter les louanges d’un de leurs bons compagnons parmi la sergenterie le roi. Mais il n’est de nul avantage à venir m’ennuyer ainsi. Le nouveau mathessep n’est pas issu du rang, c’est mestre Fouques, que tu croises tantôt à mon service. »

Ernaut ne put cacher sa déception au vicomte. Contre toute logique, il s’était persuadé qu’il aurait pu faire, à l’occasion, un mathessep tout à fait honnête. Ou, à défaut, qu’un de ses proches, Droart, ou plus probablement Eudes, soit désigné. Arnulf grimaça un court instant, peut-être un fugace sourire moqueur, à peine discernable sur sa mince bouche sans lèvres.

« Tu auras ton temps, mon garçon. *Tout fut a autrui, tout sera a autrui*. »

Ernaut comprit surtout qu’en plaçant son jeune protégé à ce poste, Arnulf gardait un pied dans le palais. Il faudrait à Ernaut attendre encore quelques années avant d’espérer meilleure solde. Pour les batailles, peut-être pourrait-il se faire nommer à cheval, vu que le vieux Gaston ne sortait plus guère. En ce cas, il serait nécessaire qu’il soit plus volontaire afin de participer aux campagnes royales. Il était rare qu’on oblige les moins valeureux à prendre part au combat. Un fantassin qui n’avait pas le cœur solide était un danger pour les siens. Seulement, partir en guerre à tout va n’était pas la meilleure façon de s’y prendre pour fonder une famille.

## Jérusalem, quartier de la Juiverie, matin du vendredi 27 novembre 1159

Le soleil commençait à peine à vider les rues de leurs ténèbres et les températures demeuraient fraîches. La nuit précédente, une fine bruine avait mouillé les terres et une odeur de salpêtre et de moisissure montait des gravats accumulés dans la petite cour où se tenaient Ernaut et Droart. Ce dernier avait déniché une magnifique borgesie à son ami, qui n’était redevable que d’un cens fort modéré à verser au Saint-Sépulcre. Située un peu au nord de Sainte-Anne, dans la partie la plus orientale de la Juiverie, elle était à un jet de flèche de la muraille nord. Les conditions en étaient avantageuses, car il y avait beaucoup d’aménagement à y faire.

Le lieu avait été abandonné lors de la prise de la ville et personne n’y avait habité depuis lors. Heureusement les toits avaient tenu et les planchers étaient solides. Mais portes et fenêtres étaient en piteux état et un aliboufier avait défoncé l’entrée de la petite cave, où se trouvait également une citerne. La rue qui desservait l’endroit n’était peuplée que de Latins et le chef de quartier était pompeusement surnommé « Le François », car ses ancêtres étaient de Paris. Son teint mat et sa plus grande habileté en arabe qu’en langue d’oïl le faisaient néanmoins plus semblable aux fellahs qu’on croisait dans certains casaux. C’était un homme agréable, aussi rond de caractère que d’aspect et il s’enthousiasmait à l’idée qu’un sergent du roi, un tel géant, s’installe près de chez lui. Cela n’en rendrait les lieux que plus tranquilles.

La maison en elle-même était spacieuse, avec un bel étage à deux pièces, agrémenté d’un magnifique rawshan[^rawshan] en bois marqueté qui ne demandait qu’à être remis en état. Au-dessous, une imposante cuisine occupait la moitié de la zone, avec une réserve adjacente. Sur le toit-terrasse, de petits appentis, anciens logements de domestiques certainement, permettaient de faire également du stockage, avec une vue superbe vers l’ensemble de la cité sainte. Enfin, une cour en terre battue, mangée de mauvaises herbes, accueillait en son fond la cave en partie enterrée, accolée de la citerne. C’était ce dernier point qui avait conquis Ernaut. L’idée de disposer à demeure d’eau à volonté lui semblait une mesure de bon sens dans ce pays où il pleuvait si peu durant de longs mois. Au-dessus de cette cave, un ancien entrepôt pourrait servir d’atelier ou d’abri pour une basse-cour.

« Il serait plus prudent de faire curer la citerne, expliquait Droart. Quand elles sont laissées à l’abandon durant des années, va savoir ce qui a fini dedans. Mais en ce cas, il faudrait le faire au plus vite. La saison pluvieuse est pratiquement sur nous et ce serait dommage de ne pas profiter de cette eau.

— As-tu connoissance de quelques manouvriers habiles à cela ?

— Pour cela, n’importe quel vidangeur suffira. On peut proposer de faire marché à moitié sur le produit de ce qu’on y trouvera, en plus de leur soudée, ça les motivera. »

Ernaut hocha la tête. Droart avait l’habitude de gérer des projets immobiliers et connaissait beaucoup de gens en ville. Son caractère affable et sa capacité à conclure des affaires, parfois douteuses, pour le plus grand profit de tous, y compris le sien, lui avaient permis de s’enrichir confortablement depuis quelques années. Son humeur, légère et primesautière, n’y avait néanmoins pas été gâtée et il demeurait le bon compagnon qu’il avait toujours été, chaussé de ses vieilles savates et revêtu de ses cottes perpétuellement froissées.

Ils montèrent l’escalier menant à l’étage et pénétrèrent dans la chambre à l’arrière du bâtiment où les fenêtres, modestes, donnaient sur la cour. Les murs récemment chaulés scintillaient dans les premières lueurs du jour.

« Je vais bientôt pouvoir loger ici, se réjouit Ernaut.

— Tu ne veux pas attendre d’y venir en homme marié ?

— Je pense qu’il serait mieux que je m’y installe au plus tôt afin de préparer les lieux. La maison ne fait pas tout. J’ai besoin d’un lit, de huches et d’une table, de bancs et d’escabeaux, de pots et cruches… tant à faire !

— Laisse donc à Libourc le soin de ce fatras! Elle saura étriller ta bourse bien plus vite qu’elle n’a gonflé, crois-m’en ! »

En disant cela, Droart prit conscience qu’Ernaut n’avait toujours pas annoncé de date pour son mariage. Les choses avançaient, clairement, mais jamais Ernaut ne parlait de la cérémonie à venir, comme si l’idée le mettait mal à l’aise. Il espérait que son ami ne s’emballait pas trop sur des perspectives mal assurées. Trop souvent, de modestes prétendants se voyaient écartés au dernier moment par une famille soucieuse de préserver les intérêts d’une de leurs filles. Plus jeune, il en avait cruellement fait les frais et ne souhaitait certes pas qu’Ernaut connaisse cette amère désillusion.

## Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159

Confortablement installé près du feu, Ernaut commençait à s’assoupir tranquillement en attendant le repas. Il avait les narines délicatement chatouillées par les odeurs de cuisine des derniers plats de fête que Mahaut concoctait. Lors des célébrations religieuses, elle mettait un point d’honneur à ne pas servir deux fois la même recette. Du coup, depuis son arrivée la veille au soir, il avait profité d’un véritable festin. Ce n’était pas mets de baron, comme il en goûtait de temps à autre les restes au palais les jours de grands banquets, mais d’habiles variations autour de préparations simples bien apprêtées. Cette abondance nourricière et la fatigue accumulée des dernières semaines l’incitaient à une petite sieste avant de se retrouver de nouveau à table.

Regardant sans le voir le feu dansant devant ses yeux, il se remémorait le dernier Noël qu’il avait passé à Vézelay. Comme cette période noire de sa vie lui paraissait loin ! Il avait franchi les mers en pénitence et avait trouvé ici un nouveau monde. Il accomplissait les pas qui achèveraient cette partie de son existence, d’enfant insouciant. Il s’estimait prêt à cela. Il avait désormais ses habitudes au sein de l’hôtel du roi, même s’il se sentait un peu à l’étroit dans les rôles où on le cantonnait pour l’instant. Il avait un petit cercle d’amis fidèles, son frère pas loin, et avait emménagé dans sa future demeure, où sa famille s’établirait d’ici peu.

Il aperçut Libourc qui assistait sa mère dans la mise en place du repas. Habillée de sa plus élégante cotte de laine moutarde, elle avait une longue natte qui voyageait sur ses épaules tandis qu’elle s’agitait pour les derniers préparatifs. En sentant le regard de son fiancé sur elle, elle lui adressa un rapide sourire affectueux. La table était presque prête, décorée de la plus belle vaisselle, dont les motifs verts et bruns apparaîtraient au fut et à mesure que les plats se videraient. Une nappe de lin clair, ornée de délicats tracés géométriques reflétait la lumière des chandelles sorties pour l’occasion. On n’attendait plus que le chef de famille, Sanson, occupé à quelque tâche personnelle dans le sous-sol.

Quand il parut enfin, le visage souligné de feu par sa lampe à graisse, il arborait un air malicieux, jouant avec une petite clef dans sa main libre. Mahaut les invita à prendre place et s’assit à son tour. Le vieil homme leur sourit alors et prit son temps avant de réciter l’habituel bénédicité, en un latin haché et mal maîtrisé.

« Benedic, Domine, nos et haec tua dona quae de tua largitate sumus sumpturi[^benedicite].

— Amen. »

Alors que chacun allait se servir, il leva la main et toussa légèrement, avant de poser sa petite clef de coffre sur la table.

« Si je n’ai pas mauvaise mémoire, il ne manque rien à la dot de ma fille que j’ai enserrée en cette huche. Et tu m’as dit que tout était prêt, de ton côté, Ernaut. Alors, mes enfants, il est plus que temps de fixer une date pour vos épousailles, non ? » ❧

## Notes

Le mariage au XIIe constitue un événement principalement civil. Si l’Église y avait part depuis des siècles, elle n’en a fait un sacrement qu’avec difficultés et précautions. Des tendances antagonistes au sein du clergé ont dû être maîtrisées pour permettre sa partie religieuse. Néanmoins, il demeure essentiellement profane, avec des conséquences civiles de lien de personne à personne. En outre, les usages semblent avoir varié parfois de façon importante selon les régions et les coutumes, et je n’ai rien trouvé de vraiment spécifique sur ce qui se faisait parmi les colons latins au Moyen-Orient. J’ai donc tenté de bâtir sur ce qui était perceptible d’autres zones européennes, en l’adaptant aux particularités démographique et sociale du royaume de Jérusalem que j’ai pu retracer.

La partie préparatoire de l’union entre Libourc et Ernaut va faire l’objet d’une petite trilogie de Qit’a, m’offrant l’occasion de dévoiler différents points de vue et diverses pratiques selon des éclairages croisés. J’espère aussi par ce biais mieux rendre sensible ce que pouvait être, symboliquement, le mariage de gens du peuple à ce moment de l’histoire.

## Références

d’Avray D. L., *Medieval marriage - Symbolism and Society*, Oxford : Oxford University Press, 2005.

Donahue, Charles Jr, *Law, Marriage, and Society in the Later Middle Ages*, Cambridge : Cambridge University Press, 2007.

Mc Carthy Conor, *Marriage in Medieval England: Law, Literature and Practice*, Woodbridge : The Boydell Press, 2004.
