---
date : 2017-08-15
abstract : 1157-1158. Le versement par Damas du tribut à Jérusalem pour la trêve annuelle va déboucher sur une crise majeure au sein de l’hôtel royal. Bernard Vacher, au centre de la tourmente, tente malgré tout de demeurer fidèle à son serment de servir loyalement la couronne.
characters : Bernard Vacher, Muhammad ben Ğafar, Baudoin III, Onfroy de Toron, Jean d’Acre, Baset, Eudes Larchier, Gaston, Brun, Isaac de Naalein
---

# Pour une poignée de dinars

## Damas, citadelle, début d’après-midi du jeudi 10 janvier 1157

La pièce dans laquelle Bernard Vacher venait d’entrer était de petite taille, austère, sans aucun décor et sentait l’humidité. On était bien loin du faste des salles de réception auxquelles les dirigeants bourides de la cité l’avaient jusqu’alors habitué. Leur temps avait passé, leur étoile s’était éteinte et c’était désormais celle de Nūr ad-Dīn qui brillait au firmament. Ce grand guerrier n’accordait pas la même importance au maintien de relations apaisées avec le trône de Jérusalem, qu’il voyait comme une verrue à éliminer. Encore plus farouchement opposé aux Latins que son père Zengī[^zengi], il le faisait comprendre de mille façons, entre autres en multipliant les vexations chaque fois que l’occasion lui en était donnée.

Le vieux chevalier s’efforçait de faire bonne figure. Il avait été chargé, une fois de plus, de récupérer le tribut versé en conséquence de la trêve. Il avait été accueilli par Muhammad ben Ğafar, un des nouveaux chambellans représentant le pouvoir damascène. Cela aussi constituait une première. Il avait l’habitude de traiter avec les princes ou leurs vizirs. Il se faisait l’impression d’être un lépreux et pressentait que les années à venir seraient rudes dans le Bilad al-Sham. Au moins cette année serait calme grâce à l’accord arraché à la fin de l’automne dernier.

Le petit fonctionnaire face à lui agitait les manches de son ample vêtement en se penchant vers les deux coffres cerclés de fer ouverts devant eux. La barbe poivre et sel taillée en pointe, le turban semblait à peine effleurer le haut de son crâne et paraissait prêt à verser à chaque fois qu’il inclinait la tête. Sa silhouette mince, ses mains délicates et ses épaules fines attestaient de son statut d’homme de robe et non d’un rôle militaire. Encore un affront envers le vieux soldat qu’était Bernard Vacher. On ne l’estimait pas suffisamment dangereux pour devoir lui opposer un émir de premier plan, comme Izz al-Din, nouveau gouverneur de la citadelle. Ce dernier avait néanmoins organisé une escorte pour accompagner les émissaires jusqu’aux confins de leur territoire, garantissant le bon acheminement de l’imposante somme.

« Tout y est, les huit mille dinars de Tyr sont là. »

Le fonctionnaire s’empara d’une bourse scellée d’un ruban cacheté à la cire.

« Chacune d’entre elles contient vingt dinars. Il y en a quatre cents, cachetées par la maison des monnaies de votre cité. Voulez-vous les compter ? Peser les pièces ? »

Une balance et des poids attendaient sur la table, au cas où le chevalier n’aurait pas confiance dans le versement. Bernard Vacher secoua la tête, se contentant de prendre la bourse et d’en rompre le cordon pour faire glisser les pièces d’or dans sa large paume.

« Il y a environ sept ratls et demi dans chaque huche^[Ce qui fait environ 27 kg d’or (en ratls damascènes).]. Nous avons préféré scinder en deux, pour plus d’aisance à mettre cela sur un animal. »

Muhammad ben Ğafar accompagnait chaque phrase d’un sourire qui se voulait engageant, mais qui finissait par sonner faux à force d’amabilité forcée.

« Vous avez bien fait » approuva le chevalier.

Il fit tinter un des dinars sur la plaque de marbre posée à côté de la balance, l’air de rien. Il faisait mine de jouer avec les pièces, mais tendait l’oreille pour s’assurer que le son lui convenait. Il n’avait jusqu’à présent jamais eu de mauvaises surprises de ce type. Mais avec une nouvelle administration, il fallait marquer son territoire, de façon discrète, mais compréhensible. Tant que les armes resteraient au fourreau, ce serait semblable jeu du chat et de la souris. C’était là, en amont, qu’une partie de la guerre se déroulait et, parfois, se gagnait.

« Une troupe vous escortera jusqu’à l’Arbre de la mesure[^arbremesure], aux fins de s’assurer que nul brigand ne soit tenté par cette fortune, précisa le chambellan.

— Une partie de mes hommes nous y attendent. Comme c’est l’usage à chaque versement » ajouta Bernard, perfidement, un sourire fallacieux sur les lèvres.

Le chambellan lui répondit par une crispation des joues qui ne ressemblait guère à une manifestation de joie, bien qu’ il s’efforçât péniblement de faire bonne figure. Après tout, il n’était qu’un sous-fifre et n’avait peut-être pas très envie de se trouver dans une salle close, avec un tas d’or et un vieil ambassadeur réputé pour son talent à la guerre. Bernard appréciait que cela soit connu chez ses adversaires. La valeur militaire était célébrée parmi les dirigeants turcs et une forme de respect pouvait en naître. Cela lui serait certainement utile dans les négociations à venir, qui s’annonçaient assez houleuses.

Il remit les pièces dans le sachet qu’il déposa avec les autres. Muhammad referma les rabats et verrouilla soigneusement les serrures, puis il tendit les deux clefs au chevalier, inclinant le buste. Enfin, il alla ouvrir la porte et frappa dans ses mains pour signifier le départ du tribut. Deux soldats latins, l’air mal à l’aise, accompagnés de deux miliciens damascènes pénétrèrent dans la salle et soulevèrent les coffres deux à deux.

Bernard soupira, la paix avait été encore plus difficile à obtenir qu’habituellement. Les tremblements de terre récents dans le nord aiguisaient les appétits des barons croisés, poussant à l’exploitation des opportunités que représentaient des murailles en partie abattues. Au moins cette région orientale serait tranquille pour quelques mois. Il prit la suite du petit convoi qui se dirigeait vers la cour où les attendaient leurs montures. Ils n’étaient pas restés une demi-journée dans la cité damascène.

## Jérusalem, palais royal, matinée du lundi 14 janvier 1157

Eudes tira le banc qu’il partageait avec Baset plus près de la table gravée de cases. Ils étaient installés dans une annexe de la vaste salle des monnaies, petite pièce carrée éclairée d’une haute fenêtre close de verre. Des barreaux épais en condamnaient l’accès. La porte elle-même était plaquée d’un décor métallique en fer forgé dont la fonction ornementale ne dissimulait que peu l’aspect massif.

Face à eux se trouvait Gaston, un sergent vétéran à qui on attribuait souvent les missions de confiance. Il était là pour vérifier qu’ils n’escamotaient rien tandis qu’ils triaient les dinars. Derrière lui, un clerc, Andri, notait ce qui était empilé dans les carrés de la table et les déplaçait une fois cela fait. Enfin, Brun, un des hommes de la Secrète[^grandsecrete], inscrivait le tout sur ses tablettes, nons sans contrôler le travail de chacun dans la pièce. Ce n’était pas des petites monnaies, billons de cuivre ou mailles pour payer un commerçant qu’ils allaient compter. C’était le versement damascène, près de huit mille disques d’or qui devaient passer entre leurs mains. C’était à la fois une marque de confiance et un motif de nervosité.

Si jamais il manquait ne serait-ce qu’un dinar, la tension serait à son comble. Le sénéchal n’était pas le pire des maîtres, mais il ne pouvait accepter que des larrons en prennent à leur aise avec le trésor royal. Surtout pour un aussi stratégique versement. Que des œufs, une poule ou quelques mesures de grain ne finissent pas dans le bon cellier n’était pas de grande importance et considéré dans la plupart des maisons comme faisant partie des pratiques normales. Mais le tribut de Damas devait absolument correspondre à ce qui était attendu. Quand politique et finances convergeaient, il ne faisait pas bon être le grain de sable inopportun.

Le clerc principal, Brun, fit jouer les deux serrures et ouvrit les coffres. Des dizaines de sachets crème les remplissaient, chacun fermé d’un cordon où se voyait le sceau de l’atelier tyrien, un château surplombant un fin chemin de terre entouré d’eau. Eudes et Baset prélevèrent une bourse, en coupèrent le lien avec des forces puis le présentèrent à Andri, qui en vérifiait la conformité. Ils comptaient ensuite les vingt pièces, en rangées de trois, ce qui faisait une valeur de quatre besants par ligne. Le premier, Baset prit un des dinars et le posa dans le petit plateau d’une balance dédiée au contrôle du poids. Ils avaient aussi une plaque de marbre pour y faire sonner les monnaies, ce qui donnait une indication sur leur aloi.

Eudes comprit très vite que Baset n’était pas à l’aise avec le trébuchet. Gaston lui-même fixait le sergent, qui paraissait hésiter sur l’équilibrage du fléau. Le malaise grandit encore lorsque Baset approcha son oreille du plateau pour y faire tinter la pièce. Il semblait indécis, s’attirant finalement une remarque de Gaston.

« Quelque souci, Baset ?

— Je ne sais… Cette balance est-elle juste ?

— Elle vient de l’atelier des monnaies, un peu qu’elle est juste ! »

Baset se mordit l’intérieur de la joue. Il n’aimait pas quand son quotidien était perturbé. Ses mains se mirent à trembler. Il prit une autre pièce, recommença les mêmes opérations avec plus de fébrilité encore. Eudes s’était interrompu et le regardait faire. Le malaise grandissait dans le petit groupe. Brun s’en émut et quitta son pupitre pour s’approcher, les yeux plissés et le nez froncé.

« Qu’est-ce donc, sergent ? »

Baset lança un regard inquiet à Eudes, qu’il connaissait le mieux, puis à Gaston. Il n’avait guère envie de répondre ce que tous avaient compris. Il secoua la tête, fit les mêmes tests avec un autre dinar, en prenant son temps. Chacun retenait son souffle.

« Hé bien, alors ? s’impatienta Brun.

— Ni sonnante, ni trébuchante, maître Brun.

— De quoi, de quoi ? s’emporta le clerc habituellement impassible.

— Ce ne sont pas monnaies de bon aloi, il y a certeté. »

Brun fronça les sourcils, se passa les mains sur le front.

« Vérifiez tout, au plus tôt ! S’il en est ainsi de chacune de ces monnaies… »

Il ne termina pas sa phrase, catastrophé. Tout le monde ici imaginait bien que les conséquences seraient terribles. Ce n’était pas un petit fraudeur qui trichait sur son cens[^cens] ou mentait au péage. Il s’agissait d’un versement en garantie de la paix. Eudes et Baset plongèrent du nez sur leur tâche, espérant, contre toute attente, que cela ne concernerait que de rares pièces.

## Jérusalem, palais royal, après-midi du mercredi 16 janvier 1157

Le roi Baudoin semblait avoir vieilli de plusieurs années en quelques jours. Les traits tirés, emmitouflé dans un épais manteau de laine écarlate fourrée, il était accoudé à une fenêtre, négligeant l’air froid venu du dehors. Il regardait un chat à l’affût des pigeons qui se dandinaient au faîtage du toit d’une galerie. Le ciel couleur de perle n’offrait guère de chaleur et il n’était pas exclu que de la neige blanchisse le paysage dans la nuit.

Il se retourna face aux trois hommes assis sur des chaises curules au pied de son lit. Ce n’était pas un réel conseil restreint, mais il avait besoin de formuler ses idées avant de les présenter à la Haute Cour. Depuis la prise d’Ascalon, quelques années plus tôt, le royaume ne s’était guère étendu et le trésor peinait à assumer toutes les dépenses. En outre, Baudoin était jeune, appréciait l’action et se languissait de retrouver la selle et la lance, de mener ses conrois à la bataille.

Devant lui se trouvaient ceux qu’il estimait le plus dans son hôtel, jusqu’à présent. Parmi ceux-ci, Bernard Vacher était en passe de perdre ce statut privilégié. Le porte-bannière royal avait gravement fauté, avec cette histoire de tribut en fausse monnaie. Son aspect piteux, ramassé et ses épaules affaissées marquaient son accablement. À ses côtés, en tenue d’une grande sobriété, le visage las, mais le regard affûté, le connétable Onfroy de Toron finissait de lire une tablette. Un rapport urgent dont il dévoilerait le contenu le cas échéant. Enfin, le sénéchal, Jean d’Acre, un homme fatigué, mais avisé, dont la tête dodelinait un peu en cette heure où il faisait habituellement la sieste.

« Barons, si je vous ai mandés, c’est pour échanger avec vous sur le sujet de ce tribut mensonger qui nous a été remis. J’ai besoin d’avoir vos avis avant d’arrêter ma décision et de la soumettre au Haut Conseil. »

Il s’approcha d’eux, s’installa sur un large siège recouvert de coussins, rajusta son manteau sur ses épaules. Il caressa sa longue barbe blonde, taillée à la mode, tout en parlant.

« Nous n’avons pas guerroyé encontre Damas depuis long temps. Le nouveau maître de la place a fort à faire dans le nord, les derniers messages en provenance de la princée d’Antioche en attestent. »

Il s’accorda un rictus en guise de sourire désabusé.

« Nous pouvons au moins accorder au bailli de la princée[^renaudchatillon] qu’il s’active fort contre nos ennemis. Cela ne laisse guère le temps au roi d’Alep de venir en nos terres. Pourtant, il nous fait injure de bien perfide façon, avec ses jeux de monnaies déloyales. Ce n’est pas là digne façon de s’opposer.

— C’est ce qui me questionne, en cette affaire, mon roi, intervint Toron. Quel serait l’intérêt de Nūr ad-Dīn de nous provoquer de la sorte ? Les Damascènes ont toujours préféré acheter la paix. Il les trahit autant qu’il nous ment, en cette affaire.

— Peut-être espérait-il que nous n’y verrions que du feu ! rétorqua le sénéchal. Je reconnais bien là les manières du fils de Sanguin[^zengi]. Il n’y a nul moyen de s’entendre avec eux, ce sont des sauvages qui vivent sous la tente, qui tètent les chèvres, quand ce ne sont pas les futailles ! Tant que la cité avait un prince comme Anur à sa tête, nous pouvions parlementer. Mais là, il nous faut conquérir le lieu, au plus vite ! »

Le connétable grimaça. Il avait beaucoup d’estime pour Jean d’Acre, mais il voyait le vieil administrateur se radicaliser avec les ans. Alors qu’il avait toujours été homme de mesure et de compromis, il se découvrait irascible et fougueux avec le temps. Il conservait malgré tout le talent de souvent pointer le bon argument.

« Je suis assez d’accord avec vous, sénéchal, approuva le jeune roi. Le fait demeure que nous ne saurions emporter la place sans quelque baron croisé à nos côtés.

— Ce fut leçon bien chèrement apprise que, parfois même, cela ne suffit pas, enchérit Bernard Vacher.

— Il n’y aurait pas eu insidieuses dissensions et un vrai chef à cet assaut, nous aurions emporté la place, Bernard. Cela au moins fut leçon, pour moi. »

La remarque avait été lancée avec morgue, incitant Vacher à rentrer la tête dans les épaules. Mal à l’aise avec le traitement infligé à un chevalier qu’il estimait, Onfroy intervint.

« Auriez-vous quelque courrier de votre frère[^thierryalsace2], mon roi ?

— Exactement ! s’enthousiasma soudain Baudoin. Ma sœur elle-même envisage de venir, m’a-t-elle écrit. Ils seraient là dès avant l’été. »

Le sénéchal s’agitait sur sa chaise. Il était chargé des finances du royaume, entre autres tâches, et il savait que la situation n’était pas au beau fixe. Le comte de Flandre allait venir avec des hommes, certes. Mais il faudrait les nourrir et pouvoir lui adjoindre une armée. Faute de quoi la campagne risquait de tourner court. Il n’eut pas le temps d’exprimer ses réserves quant à une expédition estivale que Baudoin leva la main, prévenant à son objection.

« Seulement, il nous faut reconstituer le trésor. Les récoltes n’ont pas été tant bonnes et les péages assez moyens, au final, de ce que j’en ai vu. J’ai donc une idée pour régler ce souci, tout en infligeant un camouflet à ce déshonnête turc. »

Il joignit les doigts en un geste un peu cérémonial, savourant à l’avance la subtilité de son idée.

« Chaque fin d’hiver, il est de coutume que de nombreux propriétaires de troupeaux les confient à valets et gardiens pour qu’ils profitent de la manne des premières pâtures, sur le Golan. »

Bernard Vacher comprit là où le roi voulait en venir et ouvrit des yeux ronds, effaré. Échaudé par la précédente rebuffade, il n’osa pas prendre la parole, bien que tout en lui se révoltât contre l’idée avancée.

« Il n’y a là qu’une poignée de sergents, des nomades et des servants. Un large butin s’offre à nous, pour laver l’outrage ! »

Le roi se tut, les yeux brillants d’excitation, attendant les commentaires, qu’il espérait élogieux, à son idée. Devant sa hardiesse, seul Onfroy osa s’exprimer.

« Mon roi, ce serait fâcheux précédent. Nous avons un peu partout sur les marches^[frontières.] usage de côtoyer les Damascènes. C’est là bien peu héroïque comme assaut.

— Et après ? Est-ce noble de payer son dû en méreaux de plomb ? gronda Baudoin. Je règle tous mes soucis en une fois. Réservons la gloire pour nos batailles de l’été. Avec des troupes que nous pourrons solder !

— Mon roi, si vous faites cela, plus aucun habitant de Damas n’acceptera de traiter avec nous, souligna Bernard. La confiance sera rompue à jamais. Nūr ad-Dīn n’aura qu’à souffler sur les braises du mécontentement pour s’en faire indéfectibles alliés.

— Il est bien question de cela, Vacher ! De cette confiance par trop dispendieuse que vous avez accordée à notre ennemi. Sans elle, nous n’en serions pas là ! » s’indigna Baudoin.

## Jérusalem, palais royal, soirée du mardi 18 novembre 1158

Isaac suivait un garde qui le menait à Bernard Vacher, son oncle, qui l’avait fait mander. Le vieux chevalier avait l’habitude d’être servi promptement et Isaac ne s’en formalisait guère. Depuis Naalein, au sud, il avait chevauché à bride abattue dès qu’il avait reçu le message. Il avait donc eu la surprise d’arriver le premier au palais et avait patienté un moment auprès des cuisines.

Isaac se perdait souvent dans le dédale de la résidence royale. Ce n’étaient que vastes bâtiments enchevêtrés, courettes et escaliers sans fin, alternant avec de splendides salles au décor oriental. Il avait eu l’occasion à de nombreuses reprises de venir, au service de son oncle ou lors de cérémonies et festivités officielles. Pourtant il n’arrivait toujours pas à se repérer correctement en dehors des endroits les plus éminents.

Il déboucha dans une des zones où de modestes écuries accueillaient les montures des nobles de première importance, ainsi que celles des hôtes de marque. Lui avait dû laisser son cheval à l’entrée de la cité et finir le trajet à pied. Il n’était pas étonnant que le prestigieux porte-bannière royal n’ait pas eu à mettre pied à terre avant d’être dans le palais.

La cour était calme, la forge de maréchalerie était éteinte depuis un moment. Quelques veilleuses, disposées bien à l’écart de la paille ou du foin permettaient de se repérer dans l’obscurité du crépuscule. L’odeur saline des bêtes, du crottin, rendait l’endroit familier aux narines d’Isaac, comme à tout jeune destiné à devenir chevalier. Il passait tellement de temps en selle qu’il ne savait plus s’il aimait ou haïssait ces animaux.

Une ombre plus épaisse que les autres se tenait à l’entrée des écuries. Bernard Vacher était assis sur un petit tabouret rustique, avalant le contenu d’une écuelle. Ses yeux brillèrent à la lueur des lampes quand il vit arriver Isaac. Il chassa le sergent d’un geste et se leva pour embrasser son neveu.

« Tu as bien fière allure, mon garçon. Je m’enjoie de te voir si promptement. J’ai à peine cru la vigie quand elle m’a indiqué que tu étais arrivé avant moi !

— Si j’avais idée de vous négliger, père aurait bon gré de me tancer à juste mesure, mon oncle. Vous me vouliez ici séance tenante, me voilà ! »

Bernard se laissa retomber lourdement sur son siège, invitant d’un geste Isaac à faire de même sur le banc où il avait posé sa gamelle et un pichet.

« Il sera bien temps d’ici peu de te faire mesurer tes éperons de chevalier.

— J’ai grande hâte à cela, mon oncle. Je pense que je suis prêt.

— Ah, je peux te jurer sur les Évangiles que tu ne l’es certes pas, nota sans joie le chevalier. Mais cela n’est d’aucune importance, on ne comprend que trop tard ce genre de chose ! »

Il avala encore quelques cuillers avec empressement. Isaac lui trouvait bien mauvaise mine, les traits tirés et les gestes lourds. On aurait dit que les années l’avaient rattrapé, ces derniers mois. Il soufflait régulièrement comme si le moindre de ses mouvements lui coûtait un effort incommensurable. Lui qui était considéré comme un des plus vaillants chevaliers de l’hôtel du roi paraissait désormais gauche et empoté. Finalement le vieil homme reposa son assiette, s’essuya de la manche et se rinça la bouche d’une longue gorgée de vin, avant de proposer le pichet à son neveu.

« C’est bon augure que tu sois là si tôt, je vais pouvoir prendre la route dès potron-minet.

— Où allons-nous ? s’enthousiasma Isaac, heureux à l’idée d’accompagner son oncle malgré sa fatigue.

— Toi, nulle part. Tu vas demeurer ici, dans l’attente de nouvelles de ma part. Tu les auras soit en personne, soit par un mien coursier. Il faut que tu te tiennes prêt à courir au Nord, pour rejoindre le roi. »

Il tapota une sacoche de cuir à sa hanche.

« J’ai ici de quoi subvenir à tes frais pour ce long voyage. N’épargne pas les besants, assure-toi d’arriver au plus vite en la princée d’Antioche.

— Quelque message secret à porter à Baudoin ? »

Le ton soudain enjoué à l’idée de toucher du doigt la réalité du service royal, Isaac se voyait déjà plénipotentiaire galopant à travers le pays.

« Si seulement ! Ce ne sera qu’à mon service que tu chevaucheras, mon garçon. Mais cette course peut nous valoir un retour en grâce… »

Bernard Vacher hésita un petit moment, sa voix s’étant cassée sur sa dernière remarque, brisant net l’enthousiasme de son jeune neveu.

« Tu sais que… le roi a quelques griefs à mon encontre, depuis quelques saisons.

— Père et vous ne me dîtes rien, comment le saurais-je ?

— J’ai assez d’estime envers toi pour ne point te savoir sot. Le roi a bonnes raisons de m’en vouloir. J’ai failli à son service et, par ma faute, de graves choses sont arrivées. Il me faut tenter à toutes forces de remédier à ce mal que j’ai causé.

— Ne puis-je vous assister ?

— C’est exactement cela que tu feras, je t’assure. Il me faut aller à Damas chercher la clef de ces mésaventures. Je crains qu’il n’y ait quelque terrible machination à l’œuvre.

— De qui ? Pour quoi ? »

Bernard sourit devant l’agitation de son neveu. Il haussa les épaules, ouvrit les mains et les joignit.

« Si je devais encroire ce que je sais, peut-être Nūr ad-Dīn a-t-il été trompé, comme nous. Il n’est de nul avantage pour lui de maintenir la guerre entre Damas et nous. Au contraire, il y trouve fourrage, bétail et impôts pour nourrir ses ambitions au nord. »

Isaac fronça les sourcils, hésitant à comprendre ce que cette révélation lui laissait entendre.

« Mon oncle, ces rumeurs seraient donc…

— Pour ça, tu peux te fier aux ragots de tour de garde. Le tribut était bien falsifié. Le roi a fait interdire qu’on en parle, mais les faits demeurent.

— Mais vous n’y êtes pour rien, ce sont eux qui…

— Calme-toi mon garçon. C’est bien le souci : nous ne savons pas qui ils sont, *eux*.

— Eh bien, nos ennemis, les Damascènes en l’occurrence ! »

Bernard secoua la tête d’un air désolé et posa une main imposante sur l’épaule de son neveu.

« Isaac, c’est là réponse sans usage. Il me faut savoir le nom de la personne qui a décidé de mettre ces fausses monnaies en la cassette. De là, je pourrais tenter de comprendre ses motifs.

— Il y a bien huit mille raisons qui ont pu la pousser !

— L’or ? Peut-être… Mais en ce cas, je n’aurais plus à m’en inquiéter. Le roi me maintiendra en déshonneur, à juste titre. J’aurais du temps à te consacrer pour m’assurer que tu romps la lance comme il sied ! »

Il marqua à nouveau une pause, serra le poing, qu’il leva devant son visage.

« Mais si de bien plus puissantes machinations sont à l’œuvre, je dois en informer mon roi, pour qu’il puisse les briser ! Et tu seras ma voix pour lui porter la nouvelle. »

Il se leva et saisit son neveu dans une embrassade bourrue. Il était épuisé de ces derniers mois, maintenu à l’écart après des dizaines d’années à servir la couronne. Il espérait comprendre quel feu lui avait ainsi brûlé les ailes. Plus que son honneur perdu, bien au-delà de l’arrêt des remarques acerbes qu’il entendait sur son passage, il avait besoin de voir la lueur de confiance renaître dans les yeux du roi Baudoin. ❧

## Notes

L’attaque de Baudoin III sur les plateaux du Golan en février 1157 n’a jamais reçu d’explication solidement étayée. Comme cela  déboucha sur la prise de Baniyas, ville frontière essentielle, par le pouvoir musulman, il m’a semblé intéressant de voir si des phénomènes souterrains inconnus de nous n’avaient pas joué. Une trêve, assortie d’un lourd tribut, avait été signée peu de temps auparavant avec le nouveau maître de Damas, je me suis donc amusé à proposer une hypothèse assez rocambolesque.

En outre, cela m’offre l’occasion de suivre un peu le personnage de Bernard Vacher, qui a progressivement disparu des chartes après plusieurs décennies au premier plan de l’hôtel royal. Cet effacement n’est d’ailleurs pas forcément dû à une relégation comme je l’indique ici, c’est là pure conjecture. Il demeure possible, plus simplement, que les sources qui ont pu comporter des mentions de cet important serviteur, réputé pour son travail diplomatique avec Damas, aient été en trop petit nombre pour nous parvenir, plus de huit siècles après les faits.


## Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tomes I, II et III, Damas : Institut Français de Damas, 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I, *Economic Foundations*, Berkeley et Los Angeles : University of California Press, 1999.
