---
date: 2012-01-15
abstract: 1155-1156. Sur les traces de Jacomoe Platealonga, un jeune marchand génois ambitieux, à qui tout réussit. Tandis que les navires s’élancent vers le Moyen-orient, il voit le monde s’offrir à lui.
characters: Jacomo Platealonga, Oberto Pedegola, Alda la Joliette, Benedetta Usura, Imberto Mallonus, Maître Embroni
---

# In pulverem reverteris

## Gênes, soirée du jeudi 4 septembre 1155

Les nombreuses lampes à huile disséminées dans l’opulente pièce éclairaient d’une lumière fauve le dos du jeune homme occupé à contempler au-dehors par la fenêtre vitrée. Sa grande taille était accentuée par une longue cotte de laine souple, de couleur sombre et profonde. Son visage, tourné vers la noirceur de la nuit, resplendissait de force. Bien que des traits réguliers en altérassent la sévérité, tout en lui inspirait l’autorité naturelle. Son regard noir, franc et inquisiteur, était pour l’heure perdu dans le lointain.

Il avait entrebâillé une des huisseries et humait l’air frais porteur de saveurs salées et d’embruns distants. Le vent soulevait les fines mèches de ses cheveux bouclés et faisait osciller les flammes. Il frissonna un instant, se frotta le nez et se tourna vers l’intérieur. Autour de la table se tenaient quatre hommes également habillés de beaux atours, dont l’un d’entre eux, le plus vieux, était assis sur un banc, occupé à lire à voix haute le texte d’un énorme ouvrage. Sa voix croassante donnait vie aux mots avec une étonnante vigueur, montrant l’habitude qu’il avait à déclamer les formules précises, les indications légales et les subtilités administratives. Son crâne chauve ressemblait au parchemin sur lequel son doigt courait, et ses cheveux ne formaient plus qu’une couronne argentée éparse. Régulièrement, il avalait sa salive en un puissant bruit de succion, faisant claquer ses lèvres d’un répugnant gargouillis. Parfois, il levait un index impérieux vers le ciel, le sol ou le mur, haussant le ton comme jongleur en foire. Assez rapidement, il s’arrêta et se redressa, grattant son oreille comme s’il s’y trouvait quelques puces. Il se tourna vers le jeune homme :

« Messire Jacomo, ce contrat vous sied-il en ces termes ?

— Si fait. Je n’y vois certes rien de fâcheux. Oncques n’ai été déçu de vos écritures, maître Embroni. »

La remarque arracha un couinement d’obséquieuse félicité au vieil homme.

Tout sourire, il se tourna vers le grand gaillard à ses côtés, large d’épaules, occupé à se suçoter les dents d’un air distrait, comme ennuyé d’être là.

« Et vous, maître Mallonus ? Êtes-vous en accord avec les déclarations ?

— Certes, cela me convient fort. J’aurais mauvaise grâce à m’en plaindre. »

Tout en répondant, il avait adressé de la main un petit merci au jeune homme. Ce dernier referma la fenêtre et vint prendre place sur un des bancs, incitant d’un geste les autres à faire de même.

« Si nos témoins en sont d’accord, il ne nous reste plus qu’à baptiser nos espoirs de vin épicé.

— Ce sera avec grande joie que je boirai aux vôtres succès. » répondit un petit homme au physique épais, surmonté d’un crâne orné de cheveux crépus, le visage flasque et fatigué.

« J’aurais fort goûté de participer à une telle aventure, même simple *socius tractans*[^1], vu mes maigres biens. »

La remarque soutira un juron à son voisin, un jeune homme blond aux manières brusques. Il s’esclaffa et lança un regard consterné à la cantonade, arrachant quelques rires polis.

« À qui pensez-vous faire accroire semblable chose, ami ? Vous avez si bien amassé la cliquaille depuis nos aventures en Espaigne qu’il ne doit demeurer aucun espace libre en vos celliers et entrepôts !

— Je ne saurai nier que le Seigneur m’a comblé de ses bienfaits, mais là, il s’agit de tout autre chose, par Dieu ! Plus de cent cinquante livres en poix, fer et charpenterie ! Jamais je n’ai fait pareil accord, même en mes plus brillants moments. »

Jacomo hocha la tête avec assurance, regardant son gobelet de verre avec attention.

« De sûr, certains me trouveront bien fol d’investir pareil montant. Depuis le temps que j’espérais faire commerce alexandrin, je suis fort aise d’avoir encontré Imberto ! Les louanges pleuvent sur lui comme sur la Vierge au jour de Noël. Pas un de mes compaignons d’affaires qui ne clame son habileté pour négocier avec l’Égypte.

— J’ai surtout usage d’éviter les ennuis en route. Les papistes ont beau jeu de nous promettre éternelle souffrance ou damnation. Quand bien même, qu’ils nous laissent œuvrer en paix. Est-ce que je vais leur dire comment bénir pain et vin, moi ? »

Sa saillie déclencha quelques rictus ennuyés et grimaces polies, et seul Jacomo s’esclaffa bruyamment.

« Puissent nos pères t’entendre, Imberto. C’est tout de même grande honterie que de se cacher pour établir contrat entre nous, comme larrons en maraude. Ces clercs me rendent malade, ils sont les premiers à se jeter sur les étoffes sarrasines, mais n’acceptent pas d’en payer le prix. »

Il marqua un temps, avalant une gorgée de vin, les sourcils froncés, prêt à mordre quiconque s’opposerait à lui. Il reposa son verre brutalement, faisant tinter le délicat gobelet de verre émaillé.

« Qu’ils nous laissent commercer en paix et embellissent leurs liturgies. Il n’entendent rien aux affaires de ce monde, qui les répugne visiblement. Alors qu’ils s’enclosent en leurs couvents, nous saurons bien mener le navire, et certainement pas en plus mauvais courant qu’eux.

— Amen ! » lança Imberto avant de s’envoyer une nouvelle gorgée de vin. Puis il se leva avec vigueur, empoignant un sac posé à ses côtés. Les deux témoins l’imitèrent, se dépêchant de finir leurs verres.

« Il est temps pour nous de vous laisser, messire Jacomo. Il me reste encore beaucoup de choses à faire avant de mettre les voiles. Et tôt parti, tôt rentré comme dit l’adage. »

Il tendit la main au jeune marchand, son associé.

Ce dernier l’accepta de bon cœur, un large sourire illuminant son visage.

« Je suis très heureux de vous voir prendre mes produits, maître Mallonus. Il y a tant à faire avec l’Égypte, et rares sont les associés assez aventureux pour y mener les denrées les plus demandées.

— Je ne doute mie que nous ferons d’autres beaux trajets à venir, ami. »

Il salua les présents d’un signe de tête et s’avança vers la porte menant à l’escalier, bientôt suivi par les deux témoins. Le vieux notaire caressait machinalement son registre en sirotant son vin, l’air béat. Réalisant qu’il venait d’en absorber la dernière goutte, ses traits mobiles virèrent à l’affliction, et il lança une œillade envieuse vers le pichet. L’air de rien, il se tourna vers Jacomo, tenant son verre vide devant lui.

« Messire, il me semble tout de même bien naturel de vous présenter force félicitations pour la bonne fortune dont vous jouissez. Et du courage dont vous faites montre. Il me semble vous voir encore tout marmouset, suivant votre père en ses affaires, il y a si peu.

— Je n’ai certes guère attendu avant de m’intéresser au négoce. Et n’ai pas perdu de temps à voyager pour enrichir d’autres. L’Espaigne fut pour moi la chance que d’aucuns espèrent toute une vie.

— Votre père fut bien avisé en cette affaire, je l’avoue. Quelle tragédie qu’on s’en soit pris à lui, un si grand homme ! »

Jacomo baissa la tête, le visage soudain fermé et les yeux brillants d’un éclair hargneux.

« Les émeutiers n’étaient que bêtes enragées ! Il aurait fallu en pendre quelques dizaines et le calme serait revenu bien vite en la cité. Les Consuls ne se sont guère montrés à la hauteur en cette question. Père en est mort de chagrin, j’en suis acertainé.

— Il faut les comprendre, beaucoup ont perdu en cette affaire d’Espaigne, les navires sont restés trop longtemps au loin…

— On dirait que vous êtes en accord avec ces miséreux, incapables d’œuvrer à la richesse des leurs. Étais-je si riche avant que ne mettions à la voile pour Almería ? Certes non. J’ai placé mes biens avec sagacité, comme nombre de mes amis. Et je moissonne ce jour fruit de mon labeur, de mes biens droitement gérés. »

Ému, le vieil homme sourit et prit quelques instants avant de déclarer :

« Je n’ai qu’un regret, messire, que votre père ne soit pas là en cet instant à nos côtés. Comme il serait fier de vous voir pareil au lion, ainsi qu’il l’était en ses jouvences. À travers son nom, vous portez sa mémoire bien haut. »

Embarrassé, Jacomo demeura coi un long moment, savourant l’instant et imaginant déjà les profits qu’il allait amasser d’ici peu.

Puis il tapota amicalement l’épaule du notaire avant de lui resservir une bonne quantité de vin.

« Pourriez-vous me faire copie de ce contrat, maître Embroni ? Je vous la paierai au tarif habituel.

— Je n’en ai que pour quelques minutes, messire Jacomo » affirma le vieil homme, le regard étincelant à la vision de son verre plein.

« Prenez votre temps, maître. Peut-être souhaiteriez-vous quelques douceurs, pour accompagner ce vin ? J’en partagerai volontiers quelques plats en votre compagnie. »

Certain que le vieux gourmand ne déclinerait pas l’invitation, Jacomo se dirigea vers la porte pour donner des ordres. La compagnie du vieil Embroni lui était plaisante. Il avait couché par écrit toutes les transactions de la famille Platealonga aussi loin que remontait sa mémoire. Son père avait toujours conseillé de s’en remettre à lui pour les affaires qu’il aurait à traiter. C’était un notaire fiable, scrupuleux, et surtout discret. Avec le temps, Jacomo avait fini par le considérer comme un vieil oncle, un confident qui l’avait chaque fois soutenu dans ses projets professionnels.

## Gênes, port, matin du mardi 28 février 1156

La brume qui recouvrait le port s’avançait jusqu’aux abords de la galerie longeant la façade de l’entrepôt. Un seul des deux grands battants avait été ouvert, de façon à apporter un peu de lumière, et une table disposée à l’entrée avec quelques tabourets était utilisée pour y déposer les documents de travail. De nombreuses balles, des sacs, des paniers, des tonneaux attendaient de partir pour de lointaines destinations. Aucune poussière ne les recouvrait, et il était évident que la halle ne conservait pas longtemps son stock. Chaque chose y était proprement entreposée, inventoriée, classée.

Mais pour l’heure, le petit attroupement qui discutait dans l’air glacial et humide ne s’intéressait guère aux marchandises. Au milieu du groupe se tenait Jacomo Platealonga, chaudement emmitouflé dans une chape de laine écarlate. Il brandissait une épée et un fourreau qui faisaient l’admiration de ses compagnons. La lame, ornée d’une inscription et de croix, étincelait comme un miroir. La garde de bronze était réhaussée de têtes d’animaux, et le pommeau gravé de décors marins mettait fin à une fusée enveloppée de cuir noir. L’étui, de bois recouvert de maroquin émeraude, n’était pas en reste, avec de nombreuses appliques métalliques et une bouterolle dorée ciselée de délicats feuillages. C’était une arme magnifique, digne d’un comte, que le marchand venait de se faire livrer. Ses compagnons l’étudiaient avec attention, envieux d’un si bel objet.

« Où as-tu trouvé si splendide lame, Jacomo ? »

Demanda un jeune homme à l’air efféminé, vêtu de bleu des pieds à la tête.

— Cédée par quelque Lombard, un négociant ami de Rustico. Fort étrange en ses affaires ! Il semble n’avoir jamais rien à négocier et se trouve pourtant à toutes les foires. Je devais l’encontrer à Milan pour la saint Michel et il n’y était pas. Il s’est présenté ici pour l’Avent alors que je ne l’attendais plus.

— Tu as tout de même conclu marché avec lui ? Périlleuse idée selon moi.

— Pour qui me prends-tu ? J’ai obtenu bon prix et ses produits étaient de première qualité, comme tu peux le voir. Tout n’était pas si bel et bon, mais il se trouve de quoi intéresser quelques contacts de mes amis. »

À sa gauche, un marchand tout en jambes, sec comme une brindille, l’air précieux, tendit les mains pour admirer le travail.

« Il est vrai, on ne peut qu’admirer semblable qualité. Même montée simplement, pareille lame peut se vendre sans problème 5 ou 6 sols. À quel prix les as-tu eues ?

— Si je te le dis, tu vas encore pester comme diable en eau bénite !

— Combien de pièces ?

— J’avais trois tonnelets, dont chacun tenait trois douzaines de lames. Et j’ai obtenu le tout pour 16 livres ! »

Écarquillant des yeux ronds, le marchand ne put retenir un sourire dépité.

« Comment fais-tu pour toujours arriver à trouver pareilles aubaines !

— Il était pressé de faire affaire me semble-t-il. Un projet important pour lequel il lui fallait toutes ses finances. J’étais là au bon moment. »

Un de leurs compagnons, qui avait un accent chantant, pouffa de rire.

« Il faudrait tout de même trouver un nom pour pareil négociant qui ne négocie guère ! »

Jacomo sourit à la remarque et rengaina son arme. Il ne lui restait plus qu’à faire accrocher l’ensemble à un baudrier et il pourrait l’arborer avec fierté.

« En quelle occasion pourras-tu la ceindre, ami ? Tu ne voyages guère et il est défendu d’en tenir une à sa hanche en la cité.

— Je sais bien, mais il me faut parfois m’assurer contre marauds. Je ne suis point toujours assuré lorsque je rentre à la nuit ou en certaines venelles.

— En ce cas, on va vouloir te dépouiller, rien que pour cette arme. Elle pourrait nourrir une famille plusieurs semaines, et pas de pain bis, mais de bonnes viandes ! »

Jacomo hocha la tête, reprenant son sérieux. Il renifla doucement et se tourna vers son ami à l’air goguenard.

« En ce cas, il leur faudra la prendre de mes doigts morts. Elle sera là pour tuer assaillants, pas seulement pour le paraître. Les consuls ont bien conscience qu’il nous faut être prêts à facer la vilenie si tôt hors de nos demeures. Il leur faudra bien accepter un jour que les membres de la Compagna ne restent sans défense. Il y a trop de jaloux, de fainéants avides de rapines… »

Rapidement se répandirent de sincères hochements de tête dans le groupe d’opulents marchands chaudement vêtus. Ils étaient tous conscients de la fragilité de leur situation, malgré tous leurs biens accumulés, voire à cause d’eux. Maints déshérités leur reprochaient leur insolente richesse, la comparant à leur propre détresse quotidienne. Les troubles qui avaient enfiévré la ville quelques années plus tôt, suite à l’expédition espagnole, leur avaient coûté, momentanément, le pouvoir. De nombreuses échauffourées avaient éclaté un peu partout et ils demeuraient inquiets à l’idée qu’on pouvait de nouveau s’en prendre à eux et aux leurs.

Jacomo n’était pas le seul à penser à s’armer.

## Gênes, cathédrale San Laurenzo, matin du dimanche 3 juin 1156

La foule était importante, comme à chaque messe dans la cathédrale. Les plus dévots étaient près du chœur, talonnés par les fort ambitieux. Car s’il était de mise pour un chrétien d’entendre l’office célébré par l’archevêque et les chanoines, il était indispensable pour un notable de la cité de s’y faire voir, avec ostentation. Les bousculades n’étaient pas rares, et il fallait souvent jouer des coudes pour se maintenir à un bon emplacement.

La richesse des tenues des fidèles faisait écho à la superbe des ecclésiastiques, et on retrouvait chez chacun soieries, broderies fines, passementerie et bijoux précieux. Certaines élégantes passaient une partie de la nuit à se préparer pour se montrer à leur avantage. Jacomo ricana à la vue de certaines femmes qui n’avaient plus l’âge, ni le physique, pour faire semblable étalage de leurs charmes, mais qui s’entêtaient pourtant. Il n’était d’ailleurs lui-même pas en reste. Il était vêtu d’un long bliaud de fine laine anglaise verte, dont les motifs en arêtes chatoyaient sous la lumière. Des bandes brodées de soie et d’or en ornaient plusieurs endroits. Ses chausses, d’un bleu profond, étaient parfaitement ajustées et ses pieds étaient protégés de souliers de souple cuir noir sur lesquels des fils de couleurs vives traçaient des entrelacs. Une cape de la même étoffe que son bliaud était posée sur ses épaules, doublée de fourrure courte. Il portait en outre de nombreux bijoux, bagues et broches. Sa ceinture, de fils de soie tissés en d’élégants motifs animaliers, arborait une aumônière brodée de belle taille, dans laquelle il avait placé quelques piécettes à distribuer aux pauvres et aux indigents.

Il était pour l’heure appuyé contre une des sombres colonnes de l’église, laissant son regard errer parmi les groupes de riches marchands devant lui. Il s’attardait sans vergogne sur les jeunes filles des puissantes familles patriciennes, espérant y trouver une future épouse. Un de ses compagnons le remarqua et le tança amicalement, le poussant du coude.

« Te voilà de nouveau en chasse ? As-tu trouvé jeune poulaille à ton goût ?

— Pas vraiment. J’ai encore temps. On m’a dit que Benedetto Spinola avait jolie jeunette d’à peine douze printemps. Cela pourrait faire mon affaire.

— Je ne te savais pas si fort porté sur la jouvence !

— Ce n’est pas tant la fille qui me semble joliette que sa famille de belle importance. J’ai besoin d’appuis si je souhaite mes affaires prospérer. Pour le reste, je sais bien où m’adresser. Quelques deniers donnés en la bonne maison me donnent occasion de m’épancher à plaisir. »

Badin, son voisin hocha la tête. Lui-même était encore jeune, trop pour espérer se marier avantageusement. Il lui fallait accroître sa richesse et améliorer sa position sociale. Mais pour l’heure, il songeait surtout à s’amuser. Il donna du coude dans les côtes de son ami.

« Avise la Pedegola qui arrive, voilà belle matrone qu’il me plairait fort de couvrir ! »

Jacomo fit une moue condescendante.

« Je ne sais si tu ferais là belle affaire. Oberto a fort à faire avec elle. Il lui faut souventes fois user de force pour la contraindre à son devoir. Il me l’a dit tantôt que nous étions chez Alda. »

L’autre soupira.

« Quelle pitié ! Elle a si beau minois et belles formes. Je m’ensouviens quand elle était jouvencelle, toute en cheveux, avec ses nattes brunes.

— Il en est de certaines femmes qui échaudent le sang, mais n’aiment à l’apaiser. Je n’en voudrais mie. Tant qu’elle me donne beaux enfants, sans trop rechigner, je n’en demande pas plus.

— Tu ne la voudrais tout de même pas comme la fille Boleti ? » se moqua son compère. La remarque arracha un rire surpris à Jacomo.

« Restons dans la mesure. Point trop belle, je m’en accommoderai, mais pareille à porcelette, certes pas. Je ne tiens pas à avoir hontage chaque dimanche pour la mener ici. »

Pendant qu’ils discutaient, comme tout le monde autour d’eux, la cérémonie se finissait lentement et ce n’est qu’au moment de l’« ite missa est »[^itemissa] qu’un sursaut religieux les parcourut, le temps de dire « Amen » à l’unisson. Puis la foule commença à sortir, doucement, chacun s’interpelant désormais sans plus aucune retenue.

Certains avaient fait mener leurs montures au bas des marches, ce qui aggravait la cohue. De nombreux marchands ambulants tentaient d’écouler leur bimbeloterie tandis que des défavorisés tendaient la main, dans l’espoir d’une aumône. Lorsqu’il déboucha sur le parvis, Jacomo discutait avec quelques confrères de la saison qui s’amorçait.

Malgré les événements dans le sud de la péninsule, entre le pape Adrien, Guillaume de Sicile et Manuel Comnène le basileus byzantin, les perspectives étaient bonnes. Une vaste flotte de navires était prévue pour l’Outremer[^outremer], et les notaires n’avaient guère chômé pour rédiger tous les contrats. Après quelques années maigres, la prospérité allait revenir. Beaucoup misaient sur les nouveaux royaumes latins au Levant, espérant un rapide développement avec la prise récente d’une cité comme Ascalon. En outre, la situation de guerre permanente qui y régnait permettait d’y écouler facilement certaines marchandises, difficiles à se procurer là-bas.

À cause des interdictions officielles et, surtout, cléricales, Jacomo avait longtemps hésité à commercer avec les musulmans. Cela tenait autant à une pieuse superstition qu’à son désir de fuir les ennuis. Il s’était laissé convaincre, emporté par l’enthousiasme de nombre de ses contacts, dont les profits amassés le rendaient songeur. À l’avenir, il serait toujours temps pour lui de verser quelques subsides à l’Église pour que des messes soient dites afin de racheter son âme. Une nuée de pauvres hères les assaillirent subitement, bras faméliques habillés de haillons crasseux, visages hâves et édentés, yeux caves et suppliants. Beaucoup n’avaient guère plus d’une dizaine d’années, trop jeunes pour se vendre et trop vieux pour avoir un parent qui s’occupe d’eux.

Jacomo porta la main à sa bourse et en sortit quelques pièces de cuivres, méreaux qui donneraient droit à un peu de pain et de soupe. Il distribuait volontiers et se trouva rapidement encerclé. Il haussa la voix pour ramener le calme et se faire un peu remarquer. Il ne donnait pas uniquement pour nourrir des bouches. Il savait que la parentèle Visconti appréciait qu’on soit prodigue envers les pauvres. Le vieil archevêque Siro de Porcello lui-même encourageait la charité, l’estimant comme la juste contribution des plus riches au bien commun. S’il souhaitait attirer un jour l’attention d’un de ces notables de premier plan, il devait leur montrer qu’il pouvait faire jeu égal avec eux, et qu’il souscrivait aux mêmes valeurs. Par ailleurs, il finissait par apprécier ces distributions dominicales, où il obtenait de la gratitude à bon compte. Il n’était pas encore suffisamment âgé, cynique et endurci pour ne pas affectionner des remerciements enthousiastes, bien que bruyants.

## Gênes, maison d’Alda la Joliette, nuit du mardi 31 juillet 1156

L’ambiance était plutôt braillarde lorsque la matrone, Alda, revint avec quelques jeunes filles. La vaste salle était emplie de chants inspirés par le vin, dérivés du poème qu’avait entamé un jongleur loué pour la soirée. Imperturbable, ce dernier tentait avec son psaltérion et bien peu de réussite à couvrir les beuglements. L’arrivée des femmes ramena le calme, chacun reprenant ses esprits tout en examinant d’un œil approbateur ce qui était proposé. Comme il s’agissait d’une maison réputée, où les hommes les plus riches aimaient à se délasser, elles étaient toutes assez jeunes, et pouvaient contenter tous les goûts.

Quelques-unes venaient visiblement de l’autre côté de la Méditerranée, ténébreuses beautés au regard éteint. Habillées simplement de leurs chemises et de leurs chausses, elles avaient toutes des coiffures soignées et le teint fardé. Selon leur visage, on les sentait bravaches, soumises ou perdues, indifférentes ou résignées. Alda, la seule entièrement vêtue, d’une cotte taillée dans une magnifique étoffe orange, les faisait marcher, danser quelques pas pour les mettre en valeur. Elle ne les brusquait pas, les invitant d’un geste gracieux. Pas encore assez vieille pour ne plus séduire, elle conservait de sa jeunesse une opulente chevelure blonde qui avait fait sa fortune. Et si quelques rides plissaient désormais les contours de ses yeux et de sa bouche, elle savait toujours enflammer les cœurs d’une œillade adroite, les corps d’une science experte.

Pourtant, lorsqu’elle dirigeait sa maison, nulle humanité n’habitait son regard. Son inflexible réputation suffisait généralement à ce qu’elle soit obéie sans retard. On lui prêtait de terribles vengeances sur les filles qui avaient eu le malheur de lui déplaire. Parmi les arrivantes, Jacomo crut reconnaître une jolie brune, le visage rond et les pommettes écarlates. Ses yeux étaient baissés, sans vie, mais évoquaient une pétulance qui l’intriguait. Il fit signe à la maquerelle de s’approcher tout en désignant la jeune femme.

« Dis-moi, Alda, voilà jolie novelté en ton hostel. Il me semble en avoir connoissance. »

La femme minauda un instant, cherchant toujours à plaire aux hommes.

« Fort possible. Tu ne l’as certes pas vue en chemise jusque-là, mais pour quelques pièces, elle sera à toi toute la nuit, bel étalon. »

Elle sourit, joueuse et s’approcha avec lascivité, effleurant de ses lèvres l’oreille du jeune homme.

« C’est la fille d’Anselmo Usura, la petite Benedetta. »

À ce nom, le visage de Jacomo s’éclaira, un rictus mauvais s’y déployant. Il susurra :

« On m’avait dit qu’il avait été mené au fétu[^menefetu], mais je ne le savais si bas. »

Alda se recula, s’appuyant langoureusement sur l’épaule. Elle plissa légèrement les yeux, se pourléchant comme un félin repu.

« Elle n’est pas fort experte, mais sa belle nature vaut le prix. Elle a fiers tétins et doux pertuis, rasé de frais.

— Sais-tu que je l’avais convoitée en mes jouvences ? Elle m’avait moqué devant tous mes amis, me trouvant en bien trop pauvre tenue pour l’aborder.

— Voilà belle occasion pour toi de savoir si le fruit a le goût de la fleur. »

Jacomo acquiesça silencieusement, souriant pour lui-même. Le vin l’abêtissait et il ricana à l’idée qui se formait dans sa tête.

« Il me plairait fort de la partager avec quelques miens amis, parmi mes compagnons. »

Alda le regarda, l’air faussement courroucé, faisant mine de le frapper d’un geste délicat.

« La pauvresse, que voilà bien méchante proposition !

— J’y mettrai bon prix, maîtresse, cela va sans dire. »

La femme se releva, laissant glisser sa main sur le bras de Jacomo. L’air mutin, elle penchait la tête, comme hésitante. Affectant une moue provocante, elle lâcha :

« Pas plus de trois alors, et tu paieras de plus une quarte part. »

Jacomo acquiesça d’un hochement de tête lourd, embrumé par l’alcool.

« Prépare-la moi, maîtresse, j’arrive ! »

Il se leva péniblement et tituba quelques pas, faisant signe à deux de ses compagnons d’approcher. Il leur expliqua ce qu’il avait concocté, s’attirant des remarques amusées et éveillant des regards libidineux.

Une fois l’affaire entendue, Jacomo s’excusa : il lui fallait d’abord s’absenter quelques instants, temps pour lui de se soulager de tout le vin ingurgité. Il ne se souvenait pas de l’endroit où pouvaient se trouver quelques lieux d’aisance, et décida donc d’aller au plus court, dans la ruelle voisine. Tandis qu’il avançait dans le couloir, il croisa un gros homme, qui le considéra d’un air enjoué. Reconnaissant immédiatement son ami Pedegola, il le salua joyeusement de la main.

« Alors, Oberto, on vient se mêler à canaille ?

— Tant qu’il n’y a que compaings pareils à toi, cela me sied, Jacomo. »

Ils se serrèrent la main chaleureusement. Le jeune homme fronça les sourcils, cherchant à arborer un profil sévère.

« Mais est-ce bien là lieu pour tôt marié ? Votre jeune épousée n’est-elle déjà plus à votre goût ? »

Franchement surpris, Oberto Pedegola fit une moue dégoûtée.

« Tout de même, ami, elle est là pour me donner enfançons ! Ces jouvencelles ont autre usage… »

Jacomo hocha la tête sans retenue.

« Comme l’a dit saint Augustin, compaing ! Le fier évêque d’Hippone lui-même ![^2]»

Il tapa joyeusement sur l’épaule de son confrère et ami et continua dans le couloir. Une lampe à huile éclairait le passage ouvrant sur une ruelle sombre et discrète. Au débouché dans la noirceur, Jacomo plissa les paupières, cherchant à discerner la venelle. La lune gibbeuse était cachée parmi d’épais nuages, lourds de pluie, et il était difficile de voir où on mettait les pieds. Titubant légèrement, il tenta d’avancer de quelques pas, histoire de ne pas se soulager juste à l’entrée. Les yeux baissés, il manqua de heurter une silhouette surgie devant lui. C’était le valet de Pedegola, chargé de surveiller la monture de son maître. L’homme s’excusa en reculant rapidement.

« Mordiable ! Veux-tu finir couvert de pissat ? » s’exclama Jacomo, énervé, dans une éruption de postillons. Finalement, la remarque l’amusa tant qu’il se mit à rire et commença à uriner en direction du domestique, qui détala sans demander son reste, peu désireux d’accomplir son devoir sous si sordide crachin.

« Voilà ! Il me plaît d’avoir mes aises lorsque je me soulage. »

Pour ne pas se montrer mesquin dans son ignonomie, il ajouta quelques rots sonores et un pet, puis cracha aux alentours le temps de soulager sa vessie. Après cela, il lui fallut quelques minutes pour remettre ses braies en place, gêné par la longueur de sa cotte et son degré d’alcoolémie. Il finissait son inspection, satisfait, et s’apprêtait à rentrer dans la maison lorsque des pas feutrés s’approchèrent de lui, furtivement. Trop éméché, il ne comprit rien à ce qui lui arrivait.

## Gênes, port, matin du lundi 6 août 1156

Berta avançait en trottinant, moitié dansant, moitié sautant. Elle aimait se rendre dans le port, où elle pouvait admirer les navires tout en pêchant des amorces pour son père. Bien sûr, comme toujours, ses deux frères l’accompagnaient, plus grands qu’elle et parfois un peu bêtes. Dans l’ensemble, ils étaient néanmoins assez gentils et la protégeaient quand quelqu’un faisait mine de l’ennuyer. Chemin faisant, elle essayait de se remémorer la chanson que le bateleur avait déclamée la veille lors d’un spectacle sur la placette à côté de chez elle. Il y était question d’oiseaux, de voyages et de bateaux, mais elle n’arrivait pas à se rappeler les paroles, se contentant donc de fredonner l’air.

Naviguant au milieu de la cohue habituelle avec aisance, les trois gamins retrouvèrent un coin où les courants menaient beaucoup de détritus. De nombreux poissons y étaient attirés, garantissant de belles prises à chaque fois. Il fallait de temps à autre jouer du poing avec quelques terreurs installées là, mais en général, la cohabitation se faisait assez paisiblement.

À peine arrivée, Berta s’assit sur la berge, les pieds ballants, et lança sa ligne d’un geste sûr. Ses petites mains maniaient avec expertise la canne, menant son appât dans les endroits les plus poissonneux. Appliquée, elle ne quittait que rarement des yeux son fil, simplement pour s’assurer que ses frères étaient toujours dans les environs. Finalement insatisfaite, elle se déplaça auprès des nefs d’où l’on déchargeait tout ce qui pouvait s’acheter et se vendre autour de la Méditerranée. D’un coup de poignet, elle ramena sa ligne, cherchant parmi les courants troubles les amas d’alevins où elle pourrait faire sa moisson.

Soudain elle poussa un cri strident. Elle venait de voir le dos d’un homme flottant entre deux eaux, juste contre la coque du navire. Une fois la foule entassée aux abords, quelques-uns prirent les choses en main. Il fallut l’intervention de plusieurs gaillards pour hisser la dépouille sur le quai car le corps était très abîmé, ayant passé un long moment dans les flots. Plusieurs animaux opportunistes y avaient d’ailleurs trouvé leur pitance. Après de nombreux palabres, et le concours de plusieurs hommes d’importance, on finit par l’identifier, à son riche bliaud, qui n’était, comme son porteur, plus que l’ombre de lui-même.

C’était un marchand prospère, un jeune qui promettait beaucoup, le fils du vieux Platealonga, lui-même décédé depuis peu. Il se nommait Jacomo. Quelques confrères attristés se signèrent et repartirent d’un pas vif vers leurs affaires, adoptant un éphémère masque d’affliction pour la circonstance. Finalement, deux manouvriers se mirent d’accord pour emmener la dépouille à la famille, espérant quelque récompense pour leur peine. Seule Berta restait, les yeux humides, reniflant à la vue du cadavre s’éloignant. Son frère s’approcha et la serra contre lui.

« T’inquiète, la môme. Il a de sûr eu son content avant d’aller. »

De sa manche, la petite fille s’essuya le nez et lança à son frère, l’air maussade :

« Et alors ?

— Il n’y a pas de quoi pleurer, sois acertainée, il a mangé bon pain chaque jour ce gars. Te mets pas en pleurs pour lui.

— C’est pas pour lui, c’est parce que moi, en le voyant, j’ai lâché la jolie canne que père m’avait donnée » ❧

## Notes

Les marchands génois employaient essentiellement deux types de contrats pour leurs investissements commerciaux. Un investisseur, le socius stans apportait les capitaux que le socius tractans se chargeait de faire fructifier au loin. Lorsque l’apport était uniquement le fait du socius stans, on parlait de commende, et le bénéfice était partagé, 25 % revenant au socius tractans. Dans le cas où ce dernier versait un tiers du capital, on parlait de societa et cette fois, il récupérait la moitié du bénéfice. La participation des forces génoises à la prise d’Almería et de Tortose, au moment où la Seconde croisade échouait devant Damas ne se fit pas sans douleur. L’éloignement des navires pendant de longs mois mit en grand danger beaucoup de marchands, incapables d’acheminer leurs denrées. Cela entraîna une crise politique dont les soubresauts durèrent un bon moment. La conversion des monnaies se faisait comme suit : 12 deniers pour un sou, 20 sous faisant une livre. À noter que les hautes valeurs n’existaient que sous forme d’unités de compte.

## Références

Bach, Erik, *La cité de Gênes au XIIe siècle*, Gyldendal, Copenhague : 1955.

Balard, Michel.« Les transports maritimes génois vers la Terre sainte », dans *I comuni italiani nel Regno crociato di Gerusalemme*, Airaldi G., Kedar B.Z. (éds.), Gênes, 1986, p.141-174.

Epstein, Steven A., *Genoa and the Genoese 958-1528*, Chapel Hill & Londres, The University of California Press, 1996.

Epstein, Steven A., « Secrecy and Genoese commercial practices », Journal of Medieval History, Vol. 20, Décembre 1994, p. 313-325.

[^1]: Pour tout ce qui a trait aux associations marchandes, voir les notes.

[^2]: On a longtemps propagé des citations pseudo-augustiniennes qui toléraient la prostitution, ce qui en légitimait peu ou prou l’encadrement et l’organisation.
