---
date : 2020-09-15
abstract : 1122-1153. Au fil des générations, la famille de Roupen, Mesrob et Gushak tente de maintenir son rang parmi la riche bourgeoisie arménienne, malgré les vicissitudes des guerres et des affaires.
characters : Gushak le Muletier, Roupen, Mesrob, Frère Matthieu, Clément, Missak, Frère Raoul
---

# Les fruits du labeur

## Édesse, demeure de Roupen, abords de la citadelle de Thoros, soir du vendredi 15 septembre 1122

L’excellente table du riche négociant avait attiré les estomacs plus sûrement que ses arguments, Roupen le savait. Il escomptait toujours que les mezzés ou les brochettes parviendraient à lui rallier des partisans et ne rechignait jamais à proposer vin et than, halvas et sari bourmas en abondance. Le temps que ses invités ingèrent tout ce qui leur était offert, il avait loisir de développer ses idées sans risquer la contradiction. Ils en étaient à s’échanger l’excellent choreg qui faisait la réputation de sa demeure, préparé en toute occasion. S’affichant plus mécréant qu’il n’était vraiment, Roupen aimait à dire que cette brioche lui avait apporté à elle seule plus de bienfaits que toutes ses prières et ses dons à l’église.

Depuis qu’il avait appris la capture du jeune comte par ses adversaires turks, il n’avait pas ménagé ses efforts pour manifester son soutien à la famille dirigeante des Courtenay. Il faisait partie des Arméniens acquis à la cause du nouveau souverain de ces terres. Il avait l’intention de collecter de quoi aider à payer la rançon, ou, à défaut, de quoi solder quelques hommes pour tenter de libérer le captif. Il savait que l’entreprise était fort hardie, et beaucoup de ses coreligionnaires ne voyaient pas d’un mauvais œil ce souverain trublion et batailleur quitter le devant de la scène. Comme venait de le lui faire remarquer un de ses invités, il n’avait pas hésité à convoler avec une princesse latine, Marie de Salerne, fille du régent d’Antioche après le décès de sa première épouse, princesse arménienne.

« Je pense que tu fais erreur, il a surtout marié la citadelle de Azaz^[La ville, au sud-ouest du comté d’Édesse, constituait la dot de la jeune femme.]. Un bon choix, à l’abri, près de ses terres de toujours, qui lui permet de se préparer au mieux pour conquérir Alep.

— Il aura besoin pour cela de retrouver sa liberté. L’Ortoqide[^balak] n’aura guère désir de lâcher si belle prise ! Son oncle Il-Ghazi va le noyer sous les paniers d’or et de joyaux.

— De cela tu ne devrais jurer. Balak est peu aimé à Alep, il est trop brutal, trop violent, trop impétueux. Je ne serais pas surpris qu’il préfère s’octroyer trésor pour prendre la place du vieux. Ce qui serait stupide, car si *l’avare perd un œil, l’ambitieux perd les deux^[Proverbe arménien.].

— J’entends bien ton désir de flatter les Celtes, Roupen, mais à quoi bon ? Les puissants vont et viennent, j’en ai connu tant depuis mes enfances… Ils ont de commun que jamais leur appétit n’est satisfait. Pourquoi dépenser son or pour l’un d’eux ?

— Vois tout ce qu’a fait Josselin pour les siens lorsqu’il tenait Tell Bashir[^turbessel2] ! Il a le sang du lion, mais un cœur d’agneau avec ses fidèles. Mon fils, Mesrob, qui se trouve à mes côtés, a soldé souventes fois de ses sergents qui pouvaient en jurer. Dis-leur, mon garçon ! »

Le jeune homme, habitué aux jeux rhétoriques de son père, hocha la tête avec métier avant de prendre la parole d’une voix posée.

« J’ai embauché qui-ci qui-là à mon service un Celte nommé Clément, qui n’a que louanges à faire du sire comte. Il est juste avec le faible et rabroue la superbe du fort. Féroce le fer en main, mais équitable et libéral avec les siens. Avec lui, tout semble possible.

— Ah ! Vous entendez ? Je ne suis pas seul à croire que sa bannière pourrait un jour flotter sur Alep. Le gris gagne dans la chevelure d’Il-Ghazi et regardez ses fils et ses neveux ! Ils se déchireront bientôt. La cité sera comme fruit mûr à prendre pour qui en aura l’audace.

— *Les chiens qui se battent entre eux s’unissent contre le loup*. Tu vas un peu vite en besogne.

— Le crois-tu vraiment ? J’ai vu, comme toi, comme nombre de nos amis, arriver ces Celtes d’outre les mers. J’ai pensé aussi que ce n’étaient que nouveaux mercenaires des Roums[^rum]. Mais nous avions tous tort. Ils sont venus tels barons en quête de royaumes. Et s’ils ont la vigueur de bêtes féroces, ils sont nos frères de religion… Mieux vaut eux pour nous diriger que des infidèles ou, pire, des syriaques hypocrites.

— Il n’y aucun bien à attendre d’eux, le père Matthieu de Kaysun[^matthieuedesse] l’a bien expliqué : ce sont des ingrats et des êtres cupides. N’ont-il pas pris la place de Thoros ? Tu as encontré parmi les premiers Baudoin[^baudoinIb], et tu as fait en sorte que notre prince en fasse son fils. Et depuis, il a remplacé tous les nôtres par des hommes à lui, sans égard pour nos barons.

— Thoros n’était même pas de notre Église, mais de celle des Romains[^byzantin].

— Baudoin et Joscelin n’en sont pas plus !

— *Prêtre à l’extérieur, Diable à l’intérieur* : comme beaucoup de clercs, le père Matthieu déteste les autres religions, car il voit taxes, impôts et offrandes partir ailleurs.

— Attention, Roupen, il est certaines bornes qui ne se franchissent pas. C’est là saint homme !

— Alors qu’il pratique un peu la compassion que lui et ses semblables professent à chaque messe ! »

Agacé de ces réticences imprévues, Roupen déchira de ses dents un gros morceau de choreg, maculant sa belle tenue de miettes tandis qu’il mâchait furieusement. Ce fut son fils qui rompit le silence tendu, de sa voix diplomate.

« Je sais par Clément qu’il est possible d’avoir audience auprès de la comtesse ou du sire de Marash, baillis des terres. Nous pourrions déjà lui faire part de nos regrets et indiquer que nous sommes prêts à aider à affranchir Joscelin. Sans entrer dans le détail.

— Excellente idée, mon fils. Il faudra lui dire que nous donnerons lampes et chandelles pour illuminer les prières pour sa libération. Qui nous porterait reproches de solliciter le Très-Haut ?

— De certes ! Joscelin a fait montre de grand cœur et d’amitié pour les nôtres depuis qu’il dirige ces terres. Montrons-lui que nous ne sommes pas des ingrats. Prier est bien le moins. »

Roupen se détendit. Si le vieux Missak l’approuvait ainsi, d’autres étaient certainement convaincus. Et si les quelques riches notables de leur communauté qu’il avait réunis là affichaient leur attachement au comte captif, nul doute que d’autres suivraient. Il n’en avait guère fallu plus lorsque les Latins avaient remplacé Thoros, quelques décennies plus tôt.

## Turbessel, écuries de Mesrob, après-midi du dimanche 20 août 1150

La chaude poussière poussée par le vent depuis la cour venait ajouter aux relents âcres de la terre battue. Le sol exhalait des senteurs chevalines, mélange de sueur animale, de déjections, de cuirs échauffés et de graisse, de foin entassé. Une vingtaine de riches négociants, influents sages ou prospères propriétaires, s’était installée au mieux pour débattre de la situation. Ils regardaient, impuissants, les musulmans s’emparer des cités les unes après les autres et le pouvoir arménien refluer au fur et à mesure. Leur collaboration avec la couronne latine leur portait dès lors fortement préjudice et leurs rivaux syriaques voyaient là une bonne occasion de se venger de décennies d’humiliation ou de rebuffades. 

Face au groupe, deux hommes se tenaient. L’un d’eux avait le physique un peu lourd, la barbe sombre négligée et il aurait pu passer pour un florissant laboureur s’il n’était habillé de la meilleure des soies, bien qu’elle soit maculée par endroit. Le second en aurait remontré à un piquet en termes de raideur et sa maigreur était renforcée par sa longue robe noire, ornée d’une croix à l’épaule. Il semblait s’impatienter à entendre les tergiversations des notables face à lui, remâchant son angoisse en lançant des regards assassins dans le vide.

Le petit homme à ses côtés finit par frapper dans ses mains, de l’air affable du maître débonnaire qui rassemble ses élèves.

« Mes amis, il ne sert de rien d’ainsi ergoter sur les intentions d’autrui. La chose demeure : Joscelin a vendu ses droits sur nos cités aux Romains.

— Le sire comte n’aurait jamais permis cela, c’est sa femme et le roi qui ont trahi…

— Narek, que tu croies cela ne change pas les faits. Nous n’avons pas loisir d’ordonner le monde, seulement d’y cheminer de notre mieux.

— Facile pour toi de dire cela, Mesrob ! J’ai déjà perdu beaucoup et je crains que mes dernières richesses ne disparaissent aux mains des Turks ! Toi, tu possèdes ânes, mules et chameaux. Il t’est commode d’y charger tes avoirs.

— Vais-je y mettre ma demeure ? La tombe de mon père pourra-t-elle y être fixée ? Mon frère, retiré en son monastère enfourchera-t-il une de mes bêtes ? Ne sois pas si cruel de me prêter indifférence à la situation. Frère Matthieu est venu nous prévenir sans tarder de la décision royale. Mettons ce temps à profit pour organiser cela au mieux pour tous. Au moins les humbles et les marchands que nous sommes pourront profiter de la vaillance des lances celtes ! Le chemin sera long jusqu’à Antioche, même sous leur protection. »

Il se tourna vers le jeune homme qui se tenait jusque-là en retrait, derrière lui.

« Mon fils Gushak revient d’Antioche, où il est établi depuis quelques années en mon nom. Il connaît les sentiers et les voies, il saura mener marcheurs et bêtes. »

Quelques têtes acquiescèrent sans enthousiasme, mais plusieurs visages demeuraient fermés, voire hostiles. Parmi les bourgeois rassemblés, certains voyaient leurs derniers avoirs en grand péril d’être pillés, dérobés ou simplement détruits. Et la perspective de quitter tout ce qu’ils avaient connu jusqu’alors ne les enchantait guère. Malgré tout, la crainte de rencontrer le même sort que les Édesséniens planait sur tous les esprits. Fuir et sauver ce qui pouvait s’emporter ou rester et risquer de perdre bien plus.

Mesrob échangea un regard un peu dépité avec le frère hospitalier.

« D’aucune façon, je ne saurais vous pousser à demeurer ou pas. Chacun décidera pour lui ! En ce qui me concerne, mon choix est fait, les miens finissent de préparer mes balles. Et j’ai disposé de quelques bêtes pour y mettre avoirs de ceux qui auront besoin. Nous organiserons cela dès l’aube à venir.

— Moi je reste, déclara avec arrogance un vieil homme au visage flasque. Les Romains sont bons guerriers. Rien ne dit qu’ils feront pis que les Celtes ! »

Indifférent à la pique, l’hospitalier prit la parole, à son rythme réfractaire aux aléas des temps.

« Je ne saurais parler pour les Griffons[^griffon], mais je n’ai nul connoissance de troupes en chemin. Les quelques forteresses et cités devront, je le crains fort, s’en remettre à leurs propres troupes, au moins pour les semaines à venir. Le roi a bien conscience de cela et c’est là charité de sa part que d’offrir à nos frères et sœurs en grand malheur de marcher à l’ombre de sa bannière, jusqu’en des terres plus assurées.

— Et pourquoi ne pas combattre et repousser nos ennemis ?

— Partout les barons tombent. Baudoin ne saurait tenir seul face à tous ces assaillants. Il lui a fallu faire terrible choix, que nul n’a loisir de commenter, entre un péril et l’autre. Nous ne sommes qu’hommes de peu à l’aune de tels géants, bien grands sont leur devoirs. »

Mesrob se rapprocha des notables, la démarche lourde, ses mains et ses bras s’agitant tandis que la ferveur le prenait.

« Il est vrai que nous ne sommes pas forcément tous du même avis ni que nul ici n’est ami de chacun. Mais il nous faut faire montre de fraternité en ces temps troublés. Il y a deux partis entre lesquels choisir. Ma décision est prise et, pour la plupart de vous, c’est aussi le cas. Accordons-nous au moins là-dessus et annonçons autour de nous cette division, sans chercher à en tirer influence ou fidèles. Laissons à chaque chef de famille le soin de se rallier à l’un ou l’autre, en lui indiquant juste où aller et comment se mettre en branle.

— Et puisse Dieu tous nous guider en ces périlleux chemins » précisa à voix basse l’ecclésiastique.

Mesrob n’était pas naïf au point de présumer que nul ne tenterait de tirer parti des circonstances. Il avait trop voyagé, négocié, pour conserver une opinion innocente sur le monde. Mais il avait aussi appris à ne pas toujours croire le pire inévitable. Il se trouvait quelques bonnes âmes au sein de ses supposés soutiens, et même parmi ses opposants, pour faire en sorte que la communauté s’en sorte au mieux. Lui-même avait pris ses précautions depuis des années, et son réseau de transport s’appuyait sur des partenaires de toutes les confessions. Il avait déjà des idées de la façon dont il pourrait gérer la situation sans trop y perdre si, comme il le pensait, les dernières cités arméniennes tombaient aux mains des dynasties turques. Cela lui donnait la tranquillité d’esprit requise pour prendre en charge les nécessités de sa communauté. Après tout, n’était-ce pas le rôle de l’élite, de s’occuper des plus modestes ?

## Jérusalem, rue de David, matinée du vendredi 3 juillet 1153

Les deux hospitaliers qui descendaient la rue de David étaient aussi dissemblables qu’il était possible. L’un d’eux était émacié et maladif, long et rigide dans tous ses gestes, les traits empreints de tristesse ou de sérieux. L’autre s’étendait quasiment autant en largeur qu’en hauteur, ondulant de toute sa rondeur à chaque pas. Son visage poupin était noyé de la graisse de ses excès alimentaires, son double menton prolongé par un goitre surplombant une bedaine achevant de supprimer tout angle de sa personne. Il avançait à petits pas, essoufflé du moindre effort, s’essuyant régulièrement la face ruisselante de sueur. Les chaleurs estivales l’incitaient généralement à demeurer au plus frais des caves, parmi les vivres qu’il inventoriait, classait et, parfois, goutait, pour le grand hôpital de Saint-Jean.

« N’était-il pas possible à ce négociant de venir en nos murs, mon frère ? Je n’ai pas usage d’aller ainsi quémander…

— J’ai cru comprendre que nos celliers sont fort dépourvus de fruits, malgré la saison. Et cela alors que l’afflux de pèlerins ne saurait tarder avec les fêtes de la mi-mois. Il y a urgence, chacun en a convenu au chapitre, ce tantôt.

— De certes, de certes, mais l’empressement est souventes fois mauvais conseiller. »

Frère Matthieu adressa un regard mi-surpris, mi-vexé au gros cellérier. S’il pouvait reconnaître le déplaisir de frère Raoul à subir les chaleurs, il n’appréciait guère d’être possiblement persiflé par un glouton victime de ses propres vices. Lui-même ne se plaignait que fort peu, et toujours raisonnablement, des humeurs fiévreuses qui l’accablaient régulièrement et l’incitaient au repos absolu.

Le message fut parfaitement perçu par le clerc habitué aux échanges muets et il fallut qu’ils bifurquent dans la rue des Arméniens pour que frère Raoul ose briser de nouveau leur silence gêné.

« Vous me disiez que cet homme n’est pas de notre église, c’est cela ?

— Il est de la meilleure famille qui soit, ermin des terres perdues du nord. Je l’ai connu tandis je compaignais le roi Baudoin après la capture du comte Joscelin. Sans lui, nul doute que beaucoup auraient péri lors de la périlleuse pérégrination depuis Édesse.

— Et vous dites qu’il aurait de quoi approvisionner nos réserves ?

— Je sais qu’il fait négoce de produits frais et secs, et nous devons en trouver suffisance. *La mer se fait de gouttes*. »

Le cellérier acquiesça, faisant ondoyer ses replis de graisses. Il était en charge d’une partie des stocks et depuis qu’il était jeune clerc, c’était la première fois qu’il avait si peu d’avance à disposition. Depuis quelques années, l’ordre fournissait chaque année plus de lances aux expéditions militaires et, vu que c’étaient des frères profès et pas de simples soudards employés temporairement, il fallait aussi les nourrir. Cadets de grandes familles et descendants de barons puissants, ils n’étaient guère enclins à se contenter de gruau insipide ou de hareng dur comme semelle. Un peu contrit de reprocher à d’autres son péché le plus flagrant, le moine s’éclaircit la gorge tandis qu’ils approchaient de la boutique.

À l’angle de la rue des Écrivains, la bâtisse était de belle apparence. Deux palmiers dépassaient du mur d’enceinte, apportant dans la cour une ombre bienvenue. Un des côtés était occupé par une écurie, flanquée d’une imposante citerne. En face, un large hôtel surplombait des arcades où le marchand faisait son négoce et entreposait ses balles et paniers. Alors que les deux hospitaliers entraient, un petit homme au turban bien ajusté s’avança, chassant de la main un valet qui s’approchait. Son visage, bien que replet, était anguleux, avec un nez en forme de bec et des orbites sombres où se noyaient ses yeux bruns. 

« Mestre Gushak, voici frère Raoul, le cellérier dont je vous ai entrenu hier.

— Soyez les bienvenus en ma demeure, frères » répondit l’arménien avec un léger accent.

Sa voix douce contrastait avec son attitude énergique. Il était habillé de façon sobre, fonctionnelle, et, les doigts tachés d’encre, paraissait occupé à quelque tâche d’écriture. Il appela rapidement pour qu’on leur apporte de quoi boire et manger. Ce faisant, il les guida jusqu’à une petite salle contigüe à ses entrepôts, où des suffah soigneusement disposés incitaient à la discussion ou à la sieste. Ils parlèrent un peu de généralités, du siège d’Ascalon qui durait depuis des mois, des craintes de hausse du prix du grain avec les récoltes mitigées au sud et des rumeurs de pirates dans le nord avant d’aborder ce qui les intéressait vraiment.

« Je voyage beaucoup pour mes affaires, au Bilad al-Sham[^bilad_al_Sham] essentiellement, mais aussi Triple[^triple], et al-Misr[^misr] à l’occasion. J’achète sec ou frais, mais transporte assez peu ainsi, car les pertes sont trop grandes. Je ne le fais qu’à la demande de confrères, souventes fois par gourmandise de baron ou de puissant…

— Nous avons toujours usage de raisins secs, de figues et d’abricots, et surtout de dattes. Principalement, mais sans se limiter à cela.

— De tout cela, je connais bons producteurs, et j’ai pas mal d’amis qui font ces négoces. Je pourrais envoyer quelques lettres et acheter en fonction de vos souhaits.

— Il me faudrait assavoir les prix usuels que vous pratiquez, avant de pouvoir me prononcer. »

Gushak ouvrit un petit coffre et en sortit un carnet de papier, épaisse liasse de feuillets où une écriture serrée avait noté des listes de chiffres.

« J’ai là quelque historique des tarifs que l’on me fait. Je suis prêt à acheter en votre nom à ces montants, sans y prendre la moindre obole en sus. »

Frère Raoul en eut le souffle coupé. Il n’était pas rare que des marchands négocient leur place au paradis par quelque rabais, mais jamais il n’avait entendu pareille proposition, qui lui paraissait farfelue, voire suspecte. Il fronça les sourcils.

« *L’homme doit gagner son pain à la sueur de son front*. Il me semble bien excessif de faire telle offre…

— Je serai franc avec vous, mon frère : si on sait que j’achète pour Saint-Jean de Jérusalem, cela ne sera pas pour une ou deux balles. Les prix seront d’autant plus généreux que le volume sera important. Je me fais fort d’obtenir pour moi appréciable escompte pour attribuer votre commande. »

Frère Raoul gloussa. L’homme était habile, tout en s’affichant assez droit. Cela lui plaisait. Si Dieu avait fait le marchand rusé, cela devait servir quelque dessein. Et puis avoir de nouveau accès aux dattes damascènes mettait en joie son palais. ❧

## Notes

La question de la mobilité sociale est relativement récente dans les travaux historiques. Néanmoins il s’agit selon moi d’un thème central de l’univers Hexagora, en ce qu’il concerne les rapports sociaux et les paysages mentaux des communautés. Plus encore que l’horizon géographique, cela conditionne les espoirs et les attentes des personnes. Même s’il n’est pas toujours aisé d’en tracer les contours pour les périodes médiévales, surtout les plus hautes, plusieurs études semblent aller dans le sens d’un assez grand conservatisme en ce qui concerne les groupes.

En effet, bien qu’il existe quelques destins ou trajets individuels qui parviennent à franchir les barrières, il est rare que cela fasse souche et que les lignées s’installent de façon pérenne dans un autre milieu que celui d’origine. Les cercles dominants, que ce soit au niveau politique, économique ou intellectuel, paraissent avoir été particulièrement réticents à l’intégration de nouveaux éléments sur le long terme. On pourra noter que certaines études indiquent que cet immobilisme s’est maintenu au moins jusqu’au XXe siècle dans certaines zones européennes.

## Références

Amouroux-Mourad Monique, *Le comté d’Édesse 1098-1150*, Paris : Librairie orientaliste Geuthner, 1988.

Barone G, Mocetti S, « Intergenerational mobility in the very long run: Florence 1427-2011 », Temi di Discussione, Bank of Italy working papers, No. 1060, 2016.

Clark Gregory, Cummins Neil, « Surname and Social Mobility in England, 1170-2012 », dans *Human Nature*, 2014, Vol. 25, p. 517-537.

Corak Miles, « Income Inequality, Equality of Opportunity, and Intergenerational Mobility », dans *The Journal of Economic Perspectives*, 2013, Vol. 27, No. 3,p. 79-102.

Dostourian Ara Edmond, *Armenia and the Crusades, Tenth to Twelfth Centuries - The Chronicle of Matthew of Edessa*, Lanham - New York - Londres : University Press of America, 1993.

Padgett John F., « Social Mobility, Marriage, and Family in Florence, 1282-1494 », dans *Renaissance Quaterly*, 2010, Vol. 63, No. 2, p. 357-411.

Perho Irmeli, « Climbing the ladder : Social Mobility in the Mamluk Period », dans *Mamluk Studies Review*, 2011, Vol. 2011 / XV, No. 1, p. 19-35.
