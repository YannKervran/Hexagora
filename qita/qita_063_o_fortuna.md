---
date: 2017-01-15
abstract: 1147-1159. Ra’ul, tailleur de pierre et captif d’origine latine, tente de trouver sa place dans la société moyen-orientale. Si les changements auxquels il doit se soumettre tournent parfois à son avantage, jamais la roue du destin ne s’arrête.
characters: Gerbaut, Shayma-Germaine, Thawabah-Thibaut, Ubayd-Yvain, al-Nadir al-Jurashi, al-Kabir
---

# O Fortuna

## Shayzar, pont sur l’Oronte, matin du samedi 8 mars 1147

Entamant la traversée du pont, Ra’ul tourna la tête vers les montagnes à l’ouest vers le jabāl al-ʾanṣarīya[^djebel_ansariyeh]. Cela faisait plusieurs années que les Nizāriyyūn[^nizarites] s’y implantaient, parfois au détriment des maîtres de Ra’ul, les puissants Banu Munqidh de Shayzar. Il n’avait toutefois nullement l’intention d’aller dans cette direction et, sans même y penser, il porta son regard vers la pointe méridionale du jabāl Banī-ʻUlaym[^djebel_zawiya], vers le nord. Surplombant le long plateau, des nuages d’altitude y traçaient des mosaïques d’ombre aux motifs changeants.

Inquiet à l’idée de dévoiler ses projets, il vérifia que les enfants étaient bien installés sur leur âne, replaça quelques attaches, ne sachant que faire de ses mains. De l’autre côté de l’animal, son épouse Shayma semblait indifférente. Elle marchait tranquillement, surveillant les deux garçons qui aimaient à se chamailler. Depuis l’aval, le chant des norias parvenait jusqu’à eux, sombre gémissement couvrant le grondement des flots.

Alors que le pont allait s’incurver vers l’est, ils approchèrent de la tour qui marquait la zone de péage. Un des soldats, indolent, était assis sur le parapet et s’occupait à jeter des cailloux dans l’eau. Personne d’autre n’était visible, mais Ra’ul savait qu’il y avait toujours plusieurs hommes dans l’édifice, généralement bons archers.

En les voyant, le garde se redressa et assujettit correctement sa ceinture où se balançait doucement un fourreau. Ce n’était pas un Turc, car on déléguait rarement de telles tâches subalternes à ces guerriers d’élite. Quant à l’abandonner à un Turkmène, nul émir n’avait suffisamment confiance en eux pour cela. Ra’ul savait que ce n’était qu’un fils de fellah à qui on avait attribué une arme. Al-‘Abbas n’était pas un ami, mais il le croisait depuis son enfance.

L’homme se gratta le menton lorsqu’ils arrivèrent à sa hauteur. Il plissa les yeux, indisposé par le soleil. Il inclina la tête en guise de salut et les apostropha d’un ton narquois, les invitant à s’arrêter d’un signe de la main.

« Salām ! Il va falloir que vous payiez la taxe sur les marchandises. »

Avant que Ra’ul n’eut le temps de répondre, désarçonné par l’entrée en matière, le soldat enchaîna.

« On dit que les petits enfants sont le mets préféré des Ifranjs, là-bas, au nord. Et vous en avez deux beaux dans vos paniers. »

Shayma sourit et baissa la tête modestement, invitant son époux à répondre.

« Si seulement ! Ces deux démons auraient tôt fait de réduire à néant leurs villes si on les y menait !

— Peut-être veulent-ils rejoindre l’aḥdāṯ[^ahdath], en ce cas ? »

Voyant que les deux enfants hésitaient à répondre et commençaient à s’agiter, Ra’ul s’efforça à sourire.

« J’ai espoir qu’ils suivent mes pas dans la taille de la pierre. C’est bon métier, moins risqué que de manier lance et bouclier. »

Le soldat acquiesça en silence puis se tourna vers la forteresse à l’est qui les dominait de toute sa hauteur depuis l’éperon rocheux où elle s’étendait.

« Tu as de la chance de pouvoir fréquenter les maîtres, là-haut. Je vois passer les carriers avec les pierres, mais je n’ai que rarement l’occasion de monter au qala’at.

— Abi Al-Asaker Sultan[^abi_al_asaker] s’intéresse peu à mon ouvrage, mais je croise souventes fois son frère Abi Salamah[^abi_salamah]. »

Hochant la tête, le garde se mit à bâiller puis leur fit signe d’avancer.

« Allez, je vous retiens pas plus longtemps. Salām ! »

Accompagnant son geste d’un sourire amical, Ra’ul salua et tira sur la longe de l’âne. Il se retint de tourner la tête vers Shayma, mais l’observa du coin de l’œil. Elle ne laissait toujours rien transparaître, avançant tranquillement au niveau des enfants. En peu de temps, ils rejoignirent le bosquet d’où partaient les routes vers le nord et vers l’ouest. Ce n’est qu’alors que Ra’ul échappa un soupir.

Face à eux s’étendait le plateau d’où naissaient les reliefs abritant l’endroit où ils se rendaient : Hisn Affamiya[^apamee], aux mains des Francs depuis bientôt un demi-siècle. Là d’où venait la famille de Ra’ul, capturé encore enfant.

Ils passèrent sans un mot la zone de jardins agglutinés autour des canaux d’irrigation distribuant l’eau des norias. Quelques gamins couraient dans les bosquets, adolescents impatients de se lancer le défi de sauter du haut de la roue dans les remous, le rite initiatique local officiellement réprimé par les adultes, mais secrètement admis par les hommes. Sauf par les charpentiers responsables de l’entretien des immenses godets et de leur complexe agencement de poutres et de planches. Jamais ses fils n’auraient l’occasion de s’éprouver ainsi, songea Ra’ul. Mais sa décision était prise : il devait retrouver les siens.

## Apamée, fin de matinée du jeudi 28 juillet 1149

Parcourant les rues pour rentrer chez lui, Ra’ul trouva la ville guère changée après sa conquête par les forces musulmanes. Les éventaires des commerces ouvraient de nouveau et, si les sabres et les sharbush[^sharbush] avaient remplacé les tenues latines, la présence militaire demeurait superficielle. Il avait été convoqué, comme tous les habitants, par les nouveaux notables en charge de la cité. Le fait d’avoir négocié la reddition avait permis d’éviter le saccage et les exactions multiples que ne manquait jamais d’occasionner un siège batailleur. Le šiḥna[^sihna] lui avait fait l’impression d’un homme davantage inquiet du péril extérieur que des troubles à l’ordre public. On le disait assez proche du pouvoir damascène et donc relativement bien disposé à l’encontre des populations franques, pour peu qu’elles s’affranchissent de leurs obligations légales et, surtout, fiscales.

Le muḥtasib, dont la fonction était, entre autres, de contrôler les mœurs et les pratiques religieuses, l’avait plus longuement interrogé. Vieil homme au profil de ṣūfī[^soufi], au visage émacié et à la voix douce, il s’était montré d’une grande bienveillance, mais avait fait preuve de perspicacité dans ses questions. Apprenant que Ra’ul avait été un captif franc converti enfant à l’islam, il lui avait demandé pour quelle raison il avait abjuré. Lorsque Ra’ul avait évoqué sa capture ultérieure par les Latins, il avait acquiescé sans un mot et avait proposé de l’entendre plus tard, pour envisager un retour à la vraie foi. L’apostasie était un crime grave, rarement considéré à la légère.

Parmi tous ces visages inconnus ne surnageait que celui du vieux al-Nadir al-Jurashi, un des riches commerçants d’étoffes de la ville, dont l’influence personnelle et familiale avait permis l’élévation au rang de ra’īs. Les compétences en avaient été grandement écornées par le pouvoir zenguide, ce qui n’était pas pour lui déplaire, anxieux qu’il était de devoir désormais superviser l’activité de ceux qui avaient été ses dirigeants. Durant tout l’entretien, il s’était débattu dans les manches de sa large ’aba, jetant régulièrement des regards inquiets vers les conquérants turcs et damascènes qui formaient la nouvelle administration locale.

Lorsqu’une fois entré, Ra’ul repoussa la porte de sa modeste demeure, il sentit la présence de Shayma derrière lui. Depuis la prise de la ville, elle se rongeait les sangs à l’idée qu’on les punisse pour avoir abjuré l’islam. Il se déchaussa et ôta son turban, ne gardant que son petit bonnet tricoté. Sa voix rauque lui fit l’effet d’un coup de tonnerre dans le silence de l’endroit.

« Pour eux, on est des ḏimmīs. Il faudra payer la taxe.

— Ils n’ont rien dit d’autre ?

— Le muḥtasib veut me revoir, on verra bien. »

Tout en répondant, il rejoignit la pièce principale et s’affala sur une banquette, marquant par là son intention de ne pas en parler plus. Il s’installa confortablement, dos au mur et demanda à Shayma de lui apporter de quoi se désaltérer. Il s’enquit aussi du repas, façon élégante de la congédier. Après un regard désapprobateur de se voir ainsi mise à l’écart, elle disparut dans la réserve utilisée en guise de cuisine.

Ra’ul entendait les garçons qui jouaient dans la cour qu’ils partageaient avec quelques autres familles. Des arbres desséchés par le soleil tentaient d’y survivre dans un décor de poussière et de gravier. Il repensa à son entretien et il trouvait qu’il s’en était plutôt bien passé au final. La plupart des nouveaux dirigeants s’inquiétaient avant tout de la capacité de la ville à payer ses impôts. Le šiḥna s’était même félicité de voir qu’un des ḏimmīs parlait si parfaitement l’arabe. Apprenant qu’il était tailleur de pierre, il avait décidé de lui confier l’approvisionnement en moellons depuis les ruines antiques voisines. Ra’ul n’avait certes pas mentionné qu’il était un fugitif, mais le fait qu’il se soit converti au christianisme n’avait ennuyé personne. Il n’avait pas eu l’impression d’avoir été traité de façon spéciale lors de son entretien, ayant pu assister à quelques autres auparavant.

Il se demandait même si cela ne constituait pas pour lui une occasion de voir sa situation s’améliorer. Lorsqu’ils étaient arrivés avec Shayma, il ne restait personne de sa famille et il n’avait jamais été véritablement accepté. Les Latins le considéraient comme un étranger, incapable qu’il était de parler leur langue sans accent, sans compter ses habitudes de vie orientales. Les musulmans, pour leur part, estimaient avoir affaire à un impur et préféraient l’éviter. Il n’avait pas franchement été mal accueilli, eu égard à ses aptitudes professionnelles, mais plusieurs années après, il admettait qu’il n’était pas vraiment intégré. Il échangeait un peu avec ses collègues au travail, fréquentait les gens de son quartier de loin en loin, mais il ne pouvait dire être satisfait de cela. Superviser la récupération de pierres dans les vestiges représentait une avancée par rapport à son ancien rôle de simple tailleur. Et s’il devait, pour être pleinement accepté, se convertir de nouveau à l’islam, il s’en sentait le cœur. Pour lui l’essentiel était de ne plus appartenir à un maître. Il devait, en outre, reconnaître qu’il était bien plus à l’aise avec les hadiths du Prophète qu’il récitait depuis l’enfance qu’avec ces prières latines dont il ne comprenait pas un traître mot.

Lorsque son épouse entra avec un plat et du pain à la main, elle le trouva apaisé et s’en étonna. Tout en lui répondant, il laissa même apparaître un sourire, faisant plisser son visage marqué de rides par le soleil.

« Je pensais à la situation. C’étaient les troupes de Hama qui ont assiégé la ville, mais le nouvel homme fort, ici, c’est Nūr ad-Dīn Maḥmūd[^nuraldin], le fils de Zengī. Il n’a sûrement que faire des fiers Banu Munqidh qui se refusèrent à son père. Alors des fugitifs comme nous…

— Mais le muḥtasib ?

— Je saurais bien lui prouver que nous avons agi par nécessité. Nous pourrions aussi aller voir l’imam pour discuter de cela.

— Mon père disait toujours “Qui se justifie sans être coupable, s’accuse.” Attendons de voir ce qu’ils veulent avant de le leur donner. »

Ra’ul hocha la tête.

« Tu parles de raison. »

Il s’assit plus correctement sur le bord de la banquette et remonta ses manches, soudain ragaillardi.

« Appelle donc les garçons et apporte-moi de quoi me laver les mains, nous allons manger. »

## Apamée, ruines de la cité antique, début d’après-midi du dimanche 14 juillet 1157

Installé sous un abri rudimentaire couvert de toile, Ra’ul sentait la sueur qui dévalait le long de sa colonne. Avec les ouvriers de la carrière, ils venaient de passer un moment à tenter de regrouper les mules qui leur servaient au quotidien pour transporter les matériaux. Elles étaient habituellement attachées à une longue corde près de l’endroit où ils travaillaient, sous la supervision des plus jeunes. La présence d’un prédateur avait dû les inquiéter, car elles avaient fini par détacher la longe et toutes s’étaient enfuies, causant le plus grand désordre. Bien évidemment, cela avait eu lieu au plus chaud de la journée et il avait fallu courir après ses fichues bêtes sur tout le plateau.

Ra’ul avala un peu d’eau et tendit l’outre à son compagnon Twabah al-Kabir, aussi transpirant que lui. Puis il s’assit sur une des nombreuses pierres abandonnées là par le temps.

« Maudites bêtes, elles vont nous faire courir toute la journée.

— Shaybah et Jibril en ont trouvé encore une ! »

Deux des ouvriers faisaient en effet de grands gestes pour tenter d’attraper une autre des bêtes qui courait en tous sens le long de la majestueuse colonnade qui s’étendait vers le sud. Le bruit de ses sabots sur l’antique pavage résonnait jusqu’à leurs oreilles. Indifférent à leur problème, Ra’ul trancha une large portion d’une pastèque et mordit dedans avec délectation. Il sentait le jus sucré lui couler sur le menton, traçant des sillons d’eau noire dans la poussière amassée là.

« Ça fait la troisième fois en moins d’une semaine… Je vais finir par demander si on ne pourrait pas avoir des chameaux !

— Un bon muletier serait plus utile. Le vieil ’Umarah est fort amiable, mais il y voit à peine plus qu’il n’entend. Elles sentent bien qu’elles peuvent faire ce qu’elles veulent. C’est intelligent une mule. Il n’y a qu’un cadi qui puisse leur faire entendre raison^[Allusion au fait que le cadi, juge musulman, se déplaçait traditionnellement à dos de mule.] ! »

Approchant sa puissante silhouette de la corbeille de fruits, Al-Kabir se coupa à son tour une belle part et se tint là, face à l’immense plateau hérissé de colonnes et de vestiges anciens, parmi les broussailles chenues et l’herbe rase roussies par l’été. Au loin, vers le sud-ouest, il apercevait les murailles de la cité perchée sur son tell, en bordure de relief. Les montagnes loin à l’occident dansaient dans la chaleur de l’après-midi sous un soleil de plomb.

« Ils sont partis où les gens qui vivaient là, à ton avis ?

— Ils sont allés dans le nord, là où il fait moins chaud et où les mules n’existent pas ! se railla Ra’ul.

— C’est pas le pays des Ifranjs que tu me présentes là ? ricana al-Kabir.

— Je sais pas, je n’y suis jamais allé. Mais en fait de mules, il y en a sûrement plus qu’à mon goût ! »

Ra’ul sourit et se leva pour aller tailler une autre tranche de fruit. Il éprouva soudain un vertige dans ses jambes, crut un moment qu’il avait eu trop chaud à courir au soleil puis il vit qu’al-Kabir semblait également troublé. L’instant d’après, il sentit comme des frissons monter dans tout son corps. Puis la pastèque tomba du tabouret, roula à terre, avant que le modeste appentis de bric et de broc ne s’écroulât sur sa tête.

Il sombra alors dans un univers de chaos et d’épouvante. Tout fuyait sous ses mains, il n’arrivait même pas à se maintenir dans une position quelconque, le sol échappant sans cesse à ses appuis. Il heurtait des objets, des choses le frappaient de partout, aveuglé qu’il était sous la toile abattue. Brassé dans un maelstrom furieux, il n’arrivait même pas à se recroqueviller. Un grondement assourdissant noyait le monde dans d’effroyables borborygmes, broyant ses tympans sans qu’il puisse n’y rien reconnaître.

Puis ce fut le silence. Total. Un court instant. Puis des cris, des appels.

Ra’ul réalisa qu’il avait les yeux fermés, les ouvrit pour trouver l’obscurité. Son cœur s’emballa à l’idée d’être devenu aveugle, avant qu’il ne comprenne qu’il était tête en bas, sous la toile. Il s’efforça de se redresser, sentit qu’on l’aidait. Des bras puissants le soulevaient. Il reconnut la voix d’al-Kabir.

« Le tremblement m’a fait rouler en contrebas. J’ai fini dans les buissons. Toi tu as reçu l’abri sur la tête ! »

Le soleil aveugla Ra’ul quand il se dégagea enfin, hagard, couvert de poussière et le corps endolori.

« Tu n’as rien, inch’Allah, s’enthousiasma al-Kabir.

— Twabah…

— Par contre je crois que tu auras besoin d’aller aux bains ce soir !

— Twabah…

— Quoi, tu as mal quelque part ?

— La cité… Elle a disparu ! »

Lorsqu’al-Kabir se tourna pour regarder au-delà du plateau, ses yeux s’écarquillèrent d’horreur. Une large portion du promontoire où la cité était implantée avait laissé la place à une gigantesque nuée poussiéreuse. ❧

## Notes

Le personnage de Ra’ul est inspiré des mémoires de Usamah ibn Munqidh. Il conte l’histoire de ce Latin captif, affranchi et converti qui finit par s’enfuir avec femme et enfants. Selon moi, elle illustre la plasticité des rapports sociaux qui existaient alors dans cette région du monde. On ne peut exclure l’adaptation littéraire du récit, car le vieil homme a un dessein à travers ses anecdotes, mais cela veut dire que de telles choses étaient envisageables, même si cela offre l’occasion de fustiger l’adversaire et ses nombreux défauts.

Les tremblements de terre étaient relativement fréquents au Moyen-Orient et certains chroniqueurs les mentionnaient dans leurs narrations, détaillant parfois le sort des populations durement affectées. Al-Qalānisī est de ceux-ci et j’ai d’ailleurs retenu la date qu’il indique dans ses textes pour celui qui aurait définitivement ruiné la cité d’Apamée.

Le titre, quant à lui, est directement inspiré du poème en latin écrit au début du XIIIe siècle dans la série dite *Carmina Burana* ou *Chants de Beuern*, l’abbaye où ils furent découverts. Sa renommée actuelle vient principalement de sa mise en musique par Carl Orff dans sa cantate en 1935-36. Le texte évoque un motif culturel médiéval très fréquent, la fortune qui tourne sa roue pour les puissants et les pauvres, frappant chacun sans ménagement.

Texte latin de *O Fortuna*

>O Fortuna  
velut luna  
statu variabilis,  
semper crescis  
aut decrescis,  
vita detestabilis  
nunc obdurat  
et tunc curat  
ludo mentis aciem,  
egestatem,  
potestatem  
dissolvit ut glaciem.  

>Sors immanis  
et inanis,  
rota tu volubilis,  
status malus,  
vana salus  
semper dissolubilis,  
obumbrata  
et velata  
michi quoque niteris;  
nunc per ludum  
dorsum nudum  
fero tui sceleris.  

>Sors salutis  
et virtutis  
michi nunc contraria,  
est affectus  
et defectus  
semper in angaria.  
Hac in hora  
sine mora  
corde pulsum tangite;  
quod per sortem  
sternit fortem,  
mecum omnes plangite!  


Traduction de *O Fortuna*

>Ô Fortune,  
comme la Lune  
de nature changeante,  
toujours croissant  
ou décroissant ;  
Vie détestable  
oppressante  
puis aimable  
par fantaisie ;  
Misère  
et puissance  
se mêlent comme la glace fondant.  

>Sort monstrueux  
et insensé,  
Roue qui tourne sans but,  
distribuant le malheur,  
et le bonheur en vain  
insaisissable toujours ;  
Ombrée  
et voilée  
pour moi sans but ;  
Maintenant par jeu,  
j’offre mon dos nu  
à ta méchanceté.  

>Les cours de la santé  
et du courage  
me sont contraires,  
affligé  
et défait  
toujours asservi.  
À cette heure  
sans plus tarder  
ses cordes vibrantes m’affectent ;  
Alors le destin  
comme moi frappe le fort  
et chacun se lamente !  

## Références

Büchler P., Joukovsky F., Micha A., *Carmina Burana*, Paris, Honoré Champion, Collection bilingue, volume 8, 2002.

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008

Al-Dbiyat Mohamed, « Les Norias de Hama sur l’Oronte. Un système traditionnel original de l’utilisation de l’eau fluviale » dans *Gestion durable et équitable de l’eau douce en Méditerranée. Mémoire et traditions, avenirs et solutions*, Actes des 5èmes Rencontres internationales Monaco et la Méditerranée, Monaco, 26-28 mars 2009, 2010, p. 191-210.

Gibb H. A. R., *The Damascus Chronicle of the Crusades. Extracted and translated from the chronicle of Ibn al-Qalanisi*, Londres : Luzac & Co., 1932, réédition : Mineola : Dover Publication, 2002.

Ṭāhir Muṣṭafā Anwār, « Les grandes zones sismiques du monde musulman à travers l’histoire. — I. L’Orient musulman », dans *Annales islamologiques*, volume 30, 1996, p.79-104.
