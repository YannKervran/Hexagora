--- Tome 1
Qit'a : Signifie littéralement « morceau » en persan

De Vézelay à Damas, Gênes, Ascalon ou Jérusalem, suivez pour quelques heures, quelques jours, le destin de femmes et d’hommes de tous rangs, de toutes conditions : puissante reine ou pauvre jongleur, enfant perdu, pâtissière ou tailleur de pierre…

Ils ont tous en commun de vivre et d’aimer, de travailler et de batailler dans l’univers qui accueille les enquêtes d’Ernaut de Jérusalem. Ils ont parfois croisé le jeune investigateur, ou ont pris des décisions qui ont pu en affecter le destin ou celui de ses proches. Tous sont liés, directement ou indirectement, les uns aux autres en une toile infiniment tissée : le monde d’Hexagora, reflet de notre passé au temps des Croisades.

 « Il n’est jamais inutile de dire prières et messes pour l’âme des vivants et des morts, quelles qu’aient été leurs bienfaits, mon fils.
— Malgré toutes ses largesses, il n’y a donc pas garantie de Paradis pour lui ?
— Nul n’est sûr de rien en ce bas Monde. Il nous faut espérer en la bienveillance divine, et se tenir prêt à la seule vérité connue de nous tous, l’amour de Dieu.
— Nulle certitude outre l’Amour de Dieu? », s’inquiéta André, le visage tendu.
« L’amour de Dieu… et les taxes ! » plaisanta son confrère Odo en lui tapant sur l’épaule, un sourire évanescent sur les lèvres.

--- Tome 2

Qit'a : Signifie littéralement « morceau » en persan

De Bethléem à Venise, Alexandrie, Beauvais ou Édesse, suivez pour quelques heures, quelques jours, le destin de femmes et d’hommes de tous rangs, de toutes conditions : modeste moine ou fonctionnaire zélé, pèlerine, enfant nomade ou prostituée…

Ils ont tous en commun de vivre et d’aimer, de travailler et de batailler dans l’univers qui accueille les enquêtes d’Ernaut de Jérusalem. Ils ont parfois croisé le jeune investigateur, ou ont pris des décisions qui ont pu en affecter le destin ou celui de ses proches. Tous sont liés, directement ou indirectement, les uns aux autres en une toile infiniment tissée : le monde d’Hexagora, reflet de notre passé au temps des Croisades.

« Vous savez, il se dit dans le hameau que vous seriez…
— Un meneur de loup ? Dame, je sais bien ! »
Il s’esclaffa et s’essuya la bouche dans sa manche.
« Il n’y a là que ramassis de crétins et familles d’imbéciles. Plus proches du bœuf que de tout autre bête, alors forcément, le loup…
— Vous en êtes un ? »
Le Vieux Loubeyre arbora un large sourire et avala un gros morceau de pain, l’air amusé, puis fit un clin d’œil.
« Si on te demande… »

--- Tome 3

Qit'a : Signifie littéralement « morceau » en persan

De Shayzar à Saint-Gilles, Naplouse, Colteja ou Antioche, suivez pour quelques heures, quelques jours, le destin de femmes et d’hommes de tous rangs, de toutes conditions : puissant baron ou chanoine désabusé, marionnettiste, princesse arménienne ou sergent royal…

Ils ont tous en commun de vivre et d’aimer, de travailler et de batailler dans l’univers qui accueille les enquêtes d’Ernaut de Jérusalem. Ils ont parfois croisé le jeune investigateur, ou ont pris des décisions qui ont pu en affecter le destin ou celui de ses proches. Tous sont liés, directement ou indirectement, les uns aux autres en une toile infiniment tissée : le monde d’Hexagora, reflet de notre passé au temps des Croisades.

« C’est que je dois forte somme, mestre. Et ne peux honorer ma dette.
— Que voilà vilaine affaire ! J’espère que ce n’est pas suite à funestes dévoiements.
— Certes pas, mestre, certes pas. C’était pour payer les soins de ma pauvre mère. Le médecin, les drogues d’apothicaire… Toutes mes économies y sont passées et je
dois encore une belle somme à celui qui m’a fait l’avance. »
Herbelot fit un rictus. Il avait la plus profonde antipathie pour ceux qui faisaient commerce de l’argent et d’autant plus lorsque c’était aux dépens d’un miséreux.
« Tu n’aurais jamais dû te fier à ces serpents qui vendent le temps de Dieu comme s’il leur appartenait.

--- Tome 4

Qit'a : Signifie littéralement « morceau » en persan

De Bruges à Mossoul, Ascalon, Baniyas ou Vézelay, suivez pour quelques heures, quelques jours, le destin de femmes et d’hommes de tous rangs, de toutes conditions : modeste moine ou artilleur inventif, chanteuse, ambassadeur ou orpheline…

Ils ont tous en commun de vivre et d’aimer, de travailler et de batailler dans l’univers qui accueille les enquêtes d’Ernaut de Jérusalem. Ils ont parfois croisé le jeune investigateur, ou ont pris des décisions qui ont pu en affecter le destin ou celui de ses proches. Tous sont liés, directement ou indirectement, les uns aux autres en une toile infiniment tissée : le monde d’Hexagora, reflet de notre passé au temps des Croisades.

« Quel abruti que ce vaunéant ! Il mériterait de se voir botter ainsi que son gamin !
— Voyons, frère Hémeri, est-ce là toute votre charité ? Le père la mérite tout autant que le fils et peut-être plus.
— Le gamin risque de finir boiteux. Il ne lui aurait rien coûté de nous le laisser quelques jours !
— Sa colère est peut-être à l’aune de son attachement, qu’en savons-nous ? Nos portes sont ouvertes, mais il ne serait guère amiable d’obliger à y entrer…
— Mouais, mon vieux aurait dit qu’on ne fait pas boire un âne qui n’a pas soif. Pour autant, un qui boit trop ne vaut guère. »
