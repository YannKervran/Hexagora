---
date: 2015-11-15
abstract: 1157. Une journée dans le nombril (omphalos) du monde, dans les contreforts montagneux du Moyen-orient, à Jérusalem. Les individus se croisent et se rencontrent, discutent ou s’invectivent, sans savoir que leurs destins s’entremêlent.
characters: Saïd, Ernaut, Baset, Guillemot, Garsende, Pierre, Stamatès Mpratos, Armand, Onfroy de Toron, Gringoire la Breite
---

# Omphalos

## Jérusalem, quartier Saint-Martin, aube du lundi 5 août 1157

Avant même que le soleil ne passe le Mont des Oliviers, Saïd ouvrit un œil. Quand le temps le permettait, comme cette nuit, il dormait sur la terrasse du bâtiment, tirant sa paillasse miteuse sous l’abri en feuille de palmier. Il respirait mieux ainsi, et avait plus de place, vu le bazar qu’il accumulait dans sa petite pièce depuis des années. Il habitait là depuis qu’il avait tout perdu, ses biens, sa famille, son argent, et un peu de son esprit. Il avait un long moment erré, jusqu’à finir dans la cité et avait commencé par mendier avant de dénicher ce logement, rudimentaire mais suffisant. Il avait aussitôt commencé son abracadabrante collection, alimentée par les arrière-cours abandonnées et les tas de détritus. Il veillait néanmoins sur le tout comme un cerbère et nul n’avait le droit de contempler son trésor.

Sauf peut-être le corniaud au poil jaunâtre, ras, dont la queue battait comme une trique sur le sol maintenant que Saïd était levé. Le chien passait toutes ses nuits avec lui depuis plusieurs mois, sans que Saïd n’ait cherché à l’attirer. Il ne lui avait d’ailleurs pratiquement jamais donné de nourriture. Peut-être lui aussi avait tout-il perdu et cherchait-il quelqu’un qui saurait le comprendre. Mais une chose dont Saïd n’était pas avare, c’était les caresses, et le cabot s’en contentait, l’œil humide.

Après s’être étiré, le vieil homme alla regarder où en étaient les dattes qu’il avait mises à sécher au soleil, en prévision de l’hiver. Il en avait obtenu une bonne quantité auprès d’un fermier des environs, contre quelques jours de travail. Il en tâta quelques-unes et les avala avec un gloussement de plaisir. Ces fruits lui rappelaient des souvenirs sucrés, anciens, flous et indistincts mais synonymes de plaisir. Un prénom lui revenait parfois, joie et douleur mêlés. Khadija.

Il frotta son thawb[^thawb] rapé et enfila ses savates. Aujourd’hui il allait voir si des commerçants n’avaient pas besoin de nettoyer autour de leur échoppe. S’enroulant le crâne d’un chiffon gris en guise de turban, Saïd empoigna son balai. Il posa également un sac sur son épaule, contenant son repas du midi, quignon de pain et reste de tourte, ainsi qu’une calebasse vide. Il passerait à la fontaine pour la remplir et en profiterait pour se frictionner un peu le visage, les bras et les mains, histoire d’être présentable. Le cabot tournait en tous sens, comprenant que le moment de rentrer dans la rue était venu. Le soleil affleurait à peine les reliefs orientaux que Saïd se dirigeait vers l’escalier. En passant vers la porte du garçon qui habitait à côté, il frappa deux coups secs du manche de son balai, comme le jeune le lui avait demandé auparavant. Il avait le sommeil plus lourd que le vieil homme.

Il glissa sans bruit dans l’escalier, devancé par le chien plein d’entrain. Sur un des paliers, deux hommes, des latins discutaient, l’un d’eux encore en chemise et pieds nus tandis que l’autre était habillé pour sortir.

« Salaam ! lança Saïd, souriant de ses quelques dents.

— Le bon jour, Saïd ! » répondit les deux hommes de concert.

Le chien, soucieux d’éviter un coup de pied qu’il savait possible avec ces deux-là, s’esquiva promptement dans la volée suivante. Arrivé au rez-de-chaussée, Saïd dégagea la clef qu’il portait toujours autour du cou et déverrouilla la serrure. L’animal jaillit comme une flèche, partant en courant dans la ruelle de terre, parmi la poussière et les cailloux. Des cloches sonnaient non loin lorsque le vieil homme tourna le coin pour rejoindre la grande artère.

Déjà des travailleurs cheminaient, outils et sacs à la main, parmi les animaux de bât, les cavaliers pressés et les pèlerins admiratifs. Une odeur de poussière, de sueur, de bouse et de sucre monta aux narines du vieil homme. Jérusalem se réveillait. Clopinant tranquillement, il descendait vers la ville basse, en direction du sud, vers la colline de Sion. Il s’y trouvait toujours beaucoup de voyageurs, et donc de boutiquiers, en période d’été. Surtout par un beau jour comme celui-là. Il arriva à la porte de la cité, tenue par deux sergents à la mine fatiguée. Ils regardaient passer les convois d’approvisionnement des marchés qui venaient des jardins méridionaux. L’un d’eux examinait avec attention les sacs d’une charrette menée par deux gamins. Aucun ne jeta un regard à la silhouette voûtée qui passa la porte en clopinant.

## Jérusalem, porte de Sion, matin du lundi 5 août 1157

Baset soupira et fit signe aux deux gamins qui menait le petit chariot d’entrer, puis il se rapprocha de Guillemot, l’air las.

« La peste de ces vilains qu’en font qu’à leur tête.

— Y z’avaient quoi ?

— Rien, juste pois et fèves. Mais j’aime pas quand on voit passer marchandies sans savoir de sûr d’où elles viennent.

— Pour des pois…» renifla l’autre, haussant les épaules.

Baset fronça le nez, mécontent, puis cracha dans la poussière. Il officiait comme sergent depuis bientôt vingt ans, et si son mauvais caractère et son peu de courage expliquaient qu’il soit toujours affecté à des tâches annexes, on ne pouvait lui reprocher de ne pas prendre son travail au sérieux. Il avait une haute opinion de tout ce qui avait trait à sa personne, et donc de son travail. Il se voyait comme le bras armé du roi. En l’occurrence, il n’était guère qu’un doigt de pied, et sûrement l’un des plus misérables, mais il avait le sentiment de faire réellement partie du corps royal.

Il s’étira le dos, le regard perdu vers le complexe de Sainte-Marie de Sion, puis dériva vers les pentes qui remontaient depuis la vallée de l’Hinnom. La verdeur du printemps n’était plus qu’un souvenir, sauf autour des zones soigneusement irriguées ici et là, ou aux abords des bassins, comme la piscine de Siloé. Baset aimait bien tenir cet accès. Il y avait nettement moins de passage qu’à l’ouest, à la porte de David, où la plupart des marchands se présentaient. Ici, c’étaient surtout des locaux, venus porter fruits et légumes aux marchés. Les bêtes passaient plutôt par la porte des Tanneurs, plus à l’est, pour rejoindre facilement le quartier des Bouchers.

Des pèlerins franchissaient aussi l’endroit en nombre, en direction des lieux de culte au sud de la cité, mais il n’y avait guère à s’en préoccuper. Eux-mêmes avaient l’esprit ailleurs et ils ne portaient ni armes ni marchandises à taxer. Baset revint s’appuyer contre le puissant vantail de bois, les bras croisés. Depuis la défaite humiliante du roi dans les environs de Panéas, la région était calme et cela faisait un moment qu’aucun voyageur n’avait porté de mauvaises nouvelles des confins du royaume.

Cela lui convenait parfaitement. Il avait suivi de temps à autre l’ost royal dans des campagnes, mais avait décliné chaque fois que c’était possible. Il était né à Jérusalem et aimait à y demeurer. Il y avait ses habitudes, sa famille. Sa femme, Garsende, était en outre une excellente cuisinière, et il n’aimait guère s’éloigner de sa table, sachant par avance qu’il devrait croquer du pain rassis et de vieux légumes, ou de la viande dont il pourrait réparer ses semelles.

Guillemot, tout en suivant du regard un âne bâté d’un chargement colossal de fagots de bois entrer sous l’arcade, se rapprocha de lui.

« Tu as entendu parler de la charte de casal proposée par les frères de Saint-Jean ?

— Pour où ?

— Un patelin loin au sud du royaume, Bête gibelin… »

Baset secoua la tête.

« Non. Ça t’intéresse ?

— J’ai le frère de ma mie qu’a bien envie d’avoir son hostel. Il œuvre ici, comme coutelier fèvre, mais pour un autre. Il a sufffisamment de sapience en son domaine pour avoir son propre étal.

— Fèvre ? Ça m’intéresse. Il est où ?

— Pas loin de Saint-Étienne. »

Guillemot fouilla dans une bourse sous sa cotte et en sortit un petit canif pliant, qu’il tendit à Baset.

« Bois de sycomore pour le manche. »

Baset déplia la lame et la regarda un instant avant d’en éprouver le fil du gras du pouce.

« Il y a toujours de l’ouvrage pour le bon fèvre mon vieux disait. Je demanderai à ma sœur si elle a entendu parler de ce casal.

— Tu la vois tantôt ?

— Je sais pas, j’ai pas eu nouvelles depuis la Saint-Bouillant[^saintbouillant], quand le Pierre est passé. »

Il rendit le couteau à Guillemot, qui le rangea aussitôt. Un groupe de pèlerins sortit de la cité, le chant des Psaumes s’attachant à eux tandis qu’ils descendaient la colline. Ils croisèrent un cavalier à l’allure martiale, sur un cheval de prix. L’homme était barbu, le teint cuivré à force de soleil, et un turban éclatant ornait son crâne, tandis que l’étoffe et la coupe de ses vêtements trahissaient le byzantin. Il avançait tranquillement, l’épée au côté. Baset prit sa lance pour se mettre en travers du chemin.

« Halte-là ! Que venez-vous faire en la cité le roi Baudoin ?

— J’ai nom Stamatès Mprato, je suis garde mandatôr porter message. »

L’homme exhiba un document scellé depuis un petit sac en bandoulière. Baset ne savait pas lire mais il connaissait très bien les sceaux des officiels, et c’était là un laisser-passer en bonne et dûe forme. Il glissa de côté et invita le cavalier à entrer, un sourire figé sur les lèvres, sans aucun naturel.

« La bonne jounée, messire. »

Le soldat opina, rangea son document et talonna doucement son cheval. Sous l’arcade, le claquement des sabots ferrés résonna sur la caillasse.

« Foutus Grecs » ronchonna Baset, avant de cracher par terre après le passage du messager.

## Jérusalem, rue du Mont Sion, fin de matinée du lundi 5 août 1157

Le dos et les fesses en compote d’avoir voyagé sans répit depuis la veille, Stamatès menait sa monture rênes lâches parmi la foule de la cité. Plus il approchait du cœur, vers le Change latin, plus la presse était forte. Il avait entendu dire que l’été voyait un grand afflux de voyageurs, mais que ce n’était pas comparable à la foule de Pâques. Il savait que les cavaliers n’étaient pas toujours appréciés mais il n’avait pas envie de mener son cheval à la bride à travers une foule pareille. Au moins pouvait-il se fier à la puissance de l’animal pour faire s’écarter les badauds.

Beaucoup de personnes profitaient de la moindre chaleur le matin pour faire leurs achats, bénéficiant également de la fraîcheur des produits et de la possibilité de choisir les plus belles pièces. Des valets affairés, porteurs de sacs, de paniers et de simples messages couraient en tous sens parmi les ménagères et les voyageurs en goguette. Des soldats, fort rares, se baladaient, les pouces dans le baudrier. Normalement les armes étaient interdites dans l’enceinte de la ville, mais nombreux étaient les hommes servant un baron ou l’autre qui étaient exemptés de cette interdiction ou qui s’en affranchissaient d’eux mêmes. La situation de guerre quasi permanente n’incitait pas à renoncer à son arme. En obliquant à l’ouest pour éviter Malquisinat, Stamatès sentit les bonnes odeurs venir lui chatouiller les narines. Depuis le matin, il n’avait avalé que des dattes et de l’eau croupie, et son ventre gargouilla à l’idée d’avaler autre chose. Il siffla un gamin qui passait au milieu d’une ribambelle et exhiba une petite pièce d’argent sous son nez, tout en montrant un étal proche.

« Petit, toi acheter fougasse pour moi et tu pourras en avoir partie. »

Le gamin exhiba un large sourire, tendit la main pour la pièce puis galopa jusqu’à l’éventaire de la marchande. Lorsqu’il revint, il tenait contre sa poitrine une impressionnante brioche dorée aux senteurs de beurre.

Stamatès en arracha une portion suffisante pour que le petit puisse faire preuve de largesse avec ses compagnons, la lui lança et mordit dans le reste, faisant avancer son cheval au pas. Alors qu’il remontait la rue de David, il vit au loin une troupe armée assez imposante, avec lances et bannières qui remontait en sa direction. Il tourna dès qu’il le put vers le nord, se rapprochant du Saint-Sépulcre, et donc du palais. Il ne trouvait pas l’endroit très pratique d’abord et aurait largement préféré être installé dans la forteresse dont les murailles étaient visibles près de la porte ouest. Il avait entendu parler du projet d’y déplacer la demeure royale, et il approuvait. Devoir sillonner ainsi les rues emplies de pèlerins n’était vraiment pas chose aisée. Sans compter qu’il se trouvait parmi eux les chrétiens les plus fanatiques, qui s’en prenaient à tous ceux qui n’étaient pas latins.

Stamatès eut à écoper plusieurs fois d’insultes lancées de loin et même de crachats, vite regrettés quand le soldat tourna ses yeux de vétéran en tout sens. Heureusement pour tous ces perturbateurs, il avait trop versé le sang pour y prendre du plaisir, et demeurait calme en toutes circonstances. C’était d’ailleurs peut-être la raison qui faisait qu’il voyageait souvent aux côtés d’un mandâtor. Il n’était pas un va-t-en guerre, malgré son allure martiale.

Lorsqu’il arriva enfin dans la cour où laisser son cheval, un chevalier au blanc manteau venait de démonter. L’homme avait le visage dévasté, il était difficile de reconnaître le nez de la bouche dans ce qui n’était plus qu’amas indistinct. De sa seule main, l’autre bras s’arrêtant au coude, il s’employait à dénouer un sac de sa selle et salua d’un signe de tête le Byzantin avant de s’engouffrer dans un couloir. Stamatès descendit de la selle avec un peu de raideur et jeta ses rênes à un palefernier, engloutissant les restes de brioche. Il en avait trop mangé, finalement, et se sentait un peu écœuré, le ventre en débâcle. Peut-être serait-il prudent de passer aux latrines avant d’aller délivrer son message aux officiels.

## Jérusalem, palais royal, après-midi du lundi 5 août 1157

Armand n’était jamais à l’aise dans les missions qui ne le menaient pas sur le terrain. Il était néanmoins évident qu’handicapé comme il l’était, il ne valait plus grand chose comme soldat. Il espérait se voir confier au moins des missions d’encadrement, à défaut de pouvoir charger lance au poing. Tout plutôt que de se voir relégué à l’arrière. Il regrettait parfois d’avoir été sauvé après sa blessure, condamné à survivre en étant moins qu’un homme, lui qui avait vécu comme un soldat depuis son enfance. Arrivé devant la porte qu’on lui avait indiquée, il pénétra sans attendre, découvrant le connétable Onfroy à sa table de travail. Celui-ci avait sa tête des mauvais jours, appuyé des deux mains devant une pile de papiers et de tablettes de cire. Plusieurs clercs et une poignée de chevaliers étaient présents. Armand salua de façon ostensible et tendit le document dont il était porteur au connétable qui le rompit et le parcourut rapidement, avant de le passer à l’homme à sa droite et de fixer le templier.

« Le roi fait tout ce qu’il peut pour aider à la libération du sire mestre Bertrand, soyez-en assuré ! Et je bouille personnellement d’assavoir que c’est à cause de Panéas qu’il est tombé.

— Certes pas, sire connestable. Ce sont les Mahométans les seuls à blâmer. »

Onfroy s’accorda un sourire triste, reconnaissant de l’indulgence du vétéran face à lui. Il aimait les soldats et les hommes d’armes, n’appréciait que ceux qui savaient se défendre l’arme à la main. Il n’avait aucune estime pour ceux qui se tonsuraient ou préféraient la balance du marchand. Pour lui, le combat était le révélateur de l’homme. Armand portait sur son visage, sur son corps, la preuve de sa vaillance. Le connétable se tourna vers un des clercs.

« Servez donc à boire à ce chevalier de la milice ! »

Il indiqua d’une main un escabeau et s’assit dans le même temps, poussant les papiers devant lui. Il semblait épuisé, mais prenait toujours plaisir à échanger avec ceux qui fréquentaient les champs de bataille. Il avait énormément de respect pour les soldats du Temple, dont il connaissait la valeur, malgré leur indépendance qui l’irritait parfois.

« Je suis revenu aussi vite que possible, le roi ne tardera pas. Norredin a levé le siège. Mais il n’y a aucune trêve signée. Nous avons encore l’avantage d’une forte troupe. Je n’ai pas le temps de rédiger missive, et le roi ne sera pas là avant plusieurs jours. Vous entendrez donc ce que je peux en dire.

— Les frères sont surtout inquiets du sort du sire mestre.

— J’entends bien. Nous n’avons pu avoir nulle garantie en ce sens, c’est une des raisons qui fait qu’aucun accord n’a été trouvé. Le roi n’acceptera rien sans certitude sur ce point. »

Armand hocha la tête, soulagé d’entendre cela. Le destin des Templiers pris par les adversaires musulmans était généralement scellé en quelques heures, sauf si on préférait auparavant les humilier par un défilé honteux, plutôt que d’emporter leurs têtes comme trophées. Mais le grand maître était un otage important, il pouvait s’avérer plus intéressant d’en négocier la libération, peut-être contre une formidable rançon. Armand se morigéna intérieurement de penser qu’il valait mieux Bertrand de Blanquefort que le vieil André de Montbard, un des fondateurs de leur ordre, retiré l’année précédente comme simple moine à l’abbaye de Clairvaux et mort avant la Noël. Au moins lui aura été épargnée la tristesse d’une pareille nouvelle. La voix du connétable le ramena à la réalité.

« Vous pouvez assurer les frères de notre soutien le plus entier. Le roi n’aura de cesse de faire libérer ceux qui sont tombés pour le sauver. Il a bonne mémoire ! Dès son retour, je pense que vous aurez des nouvelles du palais à ce sujet. »

Armand se leva, reposa son verre et salua les présents, avant d’aller rechercher sa monture. Il rentra tranquillement au Temple, par la grande artère qui traversait la cité d’est en ouest. Dans sa partie orientale, on la nommait la rue du Temple, et son ordre y possédait une très grande quantité de boutiques et de demeures, y apposant parfois son symbole dans la pierre. Il soupira. Il craignait un jour de n’être plus qu’un de ces collecteurs de rente, un de ces commandeurs chargés de faire converger les richesses de leurs domaines vers le cœur de la cité de Jérusalem, pour les transformer en une formidable machine de guerre.

Tandis qu’il avançait, il fut salué avec emphase par un gros bonhomme à la mine réjouie, à qui il répondit d’un discret signe de tête, s’attirant un large sourire et une litanie de bénédictions tandis qu’il s’éloignait. Le blanc mantel attirait toujours de vives réactions de la part de ses ennemis comme de ses amis.

## Jérusalem, rue de David, fin d’après-midi du lundi 5 août 1157

Gringoire la Breite postillonnait autant qu’il vociférait, le visage éclairé de joie, comme un enfant.

« Vous avez vu ce brave chevalier ? En voilà des héros chers à mon cœur. J’ai brûlé plusieurs bons cierges pour eux auprès du tombeau du Christ, loué soit son nom »

Il accompagna sa phrase d’un rapide signe de croix avant de se pencher de nouveau sur le morceau d’étoffe que le négociant venait de déplier depuis ses étagères. Cela faisait déjà un bon moment que le gros homme, son nez aviné penché sur les produits, demandait à voir chaque lot l’un après l’autre. Sa belle mise attestait de son aisance, sans quoi le boutiquier l’aurait sûrement envoyé au diable depuis longtemps. En outre, il portait une croix cousue sur la poitrine, et il n’était jamais bon de s’aliéner ceux qui venaient de fort loin, souvent en nombre et fort solidaires les uns des autres.

« C’est que mon Ermenjart va avoir petiot ! Alors je tiens à la vêtir comme princesse, ainsi qu’il sied à belle damoiselle de mon cœur ! »

Il se râcla la gorge et ajouta, d’une voix moins tonitruante :

« D’autant qu’on ne va pas repartir avec le marmouset en ses entrailles. Et pendant ces mois d’attente, il va sans dire que je ferai visitance à ceux des boutiquiers qui ont bons produits ! »

Tout en disant cela, Gringoire tâta l’étoffe, en apprécia la souplesse et la douceur. Il n’avait rien contre les textiles nouveaux, mais il les trouvait bien inférieur à ses étoffes de laine fétiches. Ici on n’en avait que pour le coton, le poil de chameau, de chèvre, des fibres sans grand intérêt. Il y avait bien la soie, qu’il trouvait moins dispendieuse que dans ses contrées. Mais bien qu’il en reconnût la brillance, la souplesse, la chatoyance, il préférait les belles productions anglaises, souples et chaudes, délicates et solides. Mais ici cela serait extraordinairement chanceux d’en trouver, et sûrement à des prix invraisemblables. Reniflant de dépit, il indiqua au vendeur de tout remballer. Rien ne lui convenait.

Il était heureux de se trouver à Jérusalem et impatient de serrer contre lui son enfant, mais il commençait à avoir le mal du pays. Rien qu’à l’idée qu’il devrait patienter un hiver ici, la nostalgie s’emparait de son âme. Il avait trouvé une jolie demeure à occuper, où il profitait d’un confort bien supérieur à ce qu’il connaissait chez lui. Mais justement, ce n’était pas chez lui, et il le ressentait vivement. Tout son être lui criait qu’il n’était pas fait pour ce pays de chaleur et de roc, de poussière et de sang. Il n’osait guère aller sur les chemins, maintenant que leurs visites aux lieux saints étaient accomplis, pour la plupart.

Chaque nouvelle qu’il entendait parlait de ces maudits Turcs qui tuaient, volaient, ravageaient. Pire que des sauterelles ! Non, décidément, pour Gringoire la Breite, Dieu avait choisi un drôle de pays pour venir au monde et mourir. Tout à ses pensées, il ne remarqua pas d’abord qu’il arrivait aux abords de Malquisinat, mais la senteur de pommes sucrées, de miel et d’épices le fit saliver. Il admettait que sur un point en particulier, il ne connaissait aucun égal à Jérusalem. Les mets y étaient fort variés, et richement assaisonnés, pour un coût modique. Sa cotte lui semblait d’ailleurs avoir rétréci depuis qu’ils étaient installés. Il faut dire qu’il ne ratait jamais l’occasion de s’offrir une petite friandise quand il passait aux abords de Malquisinat. Et, comme par hasard, le quartier était au cœur de la cité, là où l’on se devait de passer plusieurs fois par jour.

Tandis qu’il dévorait un pâté de viande au goût prononcé de poivre, il faillit buter contre un des corniauds qui traînaient toujours dans le coin. Les perspectives de dénicher de quoi se nourrir les attiraient depuis les coins les plus éloignés de la ville. Gringoire aimait bien les chiens, il arracha une petite part de sa tourte et la jeta au cabot au poil jaunâtre, qui le happa en plein vol.

« Que voilà bien habile mâtin » s’amusa Gringoire, avant de reprendre son chemin, dévorant à pleines bouchées un en-cas qui aurait fait le bonheur d’une famille pour un jour gras.

Comprenant qu’il n’obtiendrait pas plus, le chien s’arrêta, remuant toujours de la queue. Il se gratta l’oreille et regarda autour de lui. Puis il partit comme une flèche en direction du sud. Il savait à quelle heure Saïd rentrait habituellement, et ne tenait pas à rater l’accès à son coucher, ni sa pitance du soir. ❧

## Notes

Sans que cela ne soit exactement le même exercice, ce Qit’a est en rappel d’une formule que j’avais créée voilà des années pour le magazine Histoire Médiévale, sous le pseudonyme de Paul Frossenet (anagramme de « Pour les enfants ») : *La journée de Monsieur Bonhomme*. Ici nous voyons plusieurs personnages se croiser, mais cela se déroule aussi sur une seule journée. J’avais inventé cette rubrique car je savais les lecteurs friands de vie quotidienne, de celle qu’il est difficile de s’imaginer sans éplucher de nombreux ouvrages scientifiques ou des sources difficiles d’abord.

Je pensais que cela intéresserait tout particulièrement les enfants, sans imaginer que ce serait souvent la rubrique d’entrée dans le magazine, celle par laquelle la plupart des gens commençaient. La création d’Hexagora poursuit la dynamique initiée voilà bien longtemps pour la presse. Et peut-être qu’un jour, de nouveau, des images seront dessinées autour de ces récits, comme Callixte l’a fait pour quelques textes d’Ernaut au début, et comme Jean Trolley en a tant dessinés pour Monsieur Bonhomme.

## Références

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Boas Adrian J., *Domestic Settings. Sources on Domestic Architecture and Day-to-Day Activities in the Crusader States*, Leiden et Boston : Brill, 2010.
