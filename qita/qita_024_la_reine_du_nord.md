---
date: 2013-11-15
abstract: 1157. Jeune sergent au service du roi Baudoin III, Ernaut découvre les tâches de sa fonction. Rêvant parmi les puissants, il fait désormais partie d’une vaste administration, d’un gouvernement dont il n’est peut-être pas le plus héroïque représentant. Porté par l’enthousiasme juvénile et la curiosité, il est initié aux arcanes de ses missions grâce à un compagnon bien plus prosaïque.
characters: Ernaut, Droart, Ganelon, Jehan, Mélisende de Jérusalem, Onfroy de Toron, Régnier d’Eaucourt
---

# La reine du nord

## Jérusalem, matin du mardi 3 septembre 1157

Une langoureuse lumière orangée emplissait les rues abandonnées aux chiens, aux rats et aux lève-tôt. Ernaut était de ceux-là et clopinait, un sac de cuir à l’épaule, en direction de la forteresse de la tour de David. Il baillait à s’en décrocher la machoire, les yeux encore embués de sommeil. Inquiet de se présenter trop tard pour le départ, il avait peiné à s’endormir et s’était levé alors qu’un velours ambré commençait à peine d’éclaircir l’orient. Ses affaires prêtes depuis la veille, il s’était habillé et avait pris le chemin du départ en un instant.

Il avait été désigné comme escorte pour quelques émissaires se rendant à Naplouse. Là, dans la demeure de la vieillissante reine Mélisende[^melisende] allaient se retrouver des ambassadeurs et représentants de nombreux dirigeants. Parmi eux, on comptait Thoros d’Arménie, avec lequel les seigneurs latins espéraient organiser une campagne dans le nord, profitant de la désorganisation de leur ennemi commun Nūr ad-Dīn suite aux tremblements de terre de l’été. La mère de Baudoin[^baudoinIII] était fille des montagnes arméniennes et conservait un grand prestige, dont elle se servait désormais au service de son fils, et du royaume comme toujours.

Ernaut était curieux de découvrir cette femme qui avait bataillé ferme pour défendre Jérusalem contre tous ses adversaires, dont les sœurs avait épousé les princes des territoires voisins, et qui n’avait pas hésité à prendre les armes pour s’opposer à un fils qu’elle n’estimait pas encore assez mûr pour lui succéder. Aux abords de la citadelle, des hennissements, des appels, le tohu-bohu des valets empressés se déversaient dans les rues encore endormies. La vaste porte d’accès était largement ouverte et à l’intérieur, la fébrilité régnait.

Les magnifiques palefrois de la suite royale étaient apprêtés, les hongres des sergents bouchonnés et des paquets prenaient place aux flancs des roncins de bât. Jamais le jeune homme n’avait pris part à un tel équipage. Jusqu’alors, il se contentait de les regarder passer, au hasard de la route d’un évêque, d’un comte ou d’un abbé. Cette fois, il serait de ceux qui toiseraient fièrement les pieds-poudreux, les industrieux de la glèbe, qui n’oseraient lever sur eux les yeux qu’emplis de respect et de crainte.

Il rejoignit les compagnons avec qui il chevaucherait, le visage plein d’espoir et d’émerveillement. Droart l’accueillit d’une bourrade amicale mais rapide.

« Tu arrives à point, l’ami. Il faut aider les valets à lier les besaces et sacoches que tu vois là. »

Il s’arrêta et regarda Ernaut des pieds à la tête, le regard malicieux.

« Que te voilà en grand appareil, maistre Ernaut. Pourquoi donc t’endimancher pareillement ?

— Eh bien, n’allons-nous pas chevaucher au suivi de la bannière royale ? Il nous faut mener grand train, à errer ainsi au nom du souverain.

— Voilà bien vérité, par ma foi. Mais nous allons surtout chevaucher parmi caillasse et poussière, et ta cote flamboyante aura couleur de souris à notre arrivée. Roule-la donc en ta sacoche et enfile vêtement moins clinquant. Garde ta belle tenue pour Naplouse. »

Ernaut opina et se dépêcha d’accomplir ce qui lui avait été recommandé. Il passa donc, non sans regret, sa vieille cotte, d’un vert délavé, dont le chemin ne pourrait entamer la teinte, déjà fort passée. Peu après résonna un premier long coup de trompe, lancé depuis la maîtresse tour. Droart lui sourit :

« Au premier cornage, chacun se doit de s’aprêter. »

Ernaut acquiesça et noua ses affaires au troussequin de sa selle. Tandis qu’il s’affairait à régler la sous-ventrière, il aperçut le petit groupe de dignitaires qui sortait d’un des bâtiments. Autour du connétable Onfroy de Toron, dont la barbe majestueuse n’était pas sans rappeler celle du grand Charles[^charlemagne2], gravitaient plusieurs hommes de moindre envergure. Parmi ceux-ci, il reconnut le visage basané et le regard perçant de Régnier d’Eaucourt.

Le chevalier était vêtu d’une tenue de voyage très sobre et avançait à grandes foulées, remuant les mains tandis qu’il parlait. Peu de temps après, Ernaut eut l’occasion d’échanger quelques mots avec Ganelon, son valet. Il lui confirma qu’ils faisaient partie du convoi, son maître ayant de nombreux contacts avec les familiers de la reine, qu’il avait servie fidèlement pendant des années avant de se résoudre à rejoindre Baudoin. Lorsque la bannière portant les couleurs de Jérusalem sortit de la chapelle, fièrement brandie par un jeune homme en grand haubert, l’activité redoubla. Seul Ernaut perdit quelques instants à admirer le carré de soie ondoyant dans le vent.

Un second coup de trompe retentit, l’arrachant à sa rêverie.

« En selle ! lui cria Droart. Nous allons former conroi de route. »

Un chevalier au menton volontaire, le visage barré d’une cicatrice qui lui avait emporté une partie du nez allait et venait le long de la colonne, indiquant d’une voix sèche ou d’un index autoritaire les emplacements de chacun. Puis il rejoignit l’escorte du connétable.

Tout le monde attendait, les voix s’étaient tues. On n’entendait que le piétinement des sabots, le ronflement des étalons tenus serrés, le cliquetis des harnais. Onfroy de Toron leva le bras et la trompe sonna une dernière fois, indiquant le départ de la troupe. Ernaut admira les chevaliers menés par la bannière royale franchir le portail et talonna son cheval pour tenir sa place, parmi les valets et les bidets.

## Naplouse, fin de journée du mardi 3 septembre 1157

Lorsque la colonne était finalement parvenue à Naplouse, Ernaut se sentit soulagé. Il n’était pas habitué à chevaucher toute la journée et ses cuisses, ses fesses étaient endolories par la selle dont le matelassage lui semblait tout symbolique. Les dernières lieues lui avaient paru une éternité. La chaleur avait été assommante et, bien qu’ils eussent fait une longue halte à la mi-journée pour se protéger des moments les plus chauds, il avait la gorge sèche et la bouche comme du carton, ayant fini sa gourde depuis bien longtemps.

Lorsqu’il avait réalisé qu’ils prenaient la route de la Mahomerie, il avait secrètement cultivé l’espoir que son frère pourrait l’admirer tandis qu’il passerait, majestueux, dans la colonne royale. Mais, las, ils étaient passés au large du casal, sur la grande route, et nul paysan n’avait pointé son nez pour venir les soutenir de cris de joie ou, à défaut, les admirer en silence. Il avait remarqué que, si parfois quelques-uns levaient le nez de leurs travaux, la plupart se contentaient d’un rapide coup d’œil, habitués qu’ils étaient à voir passer des contingents d’hommes en armes à la suite d’une bannière. Le royaume était en guerre de façon quasi permanente depuis sa création et, lorsqu’il ne l’était pas, il s’employait à préparer la prochaine. L’enthousiasme d’Ernaut était donc retombé, au même rythme que ses épaules, tandis que la chevauchée pesait plus lourdement sur son corps.

La cité de Naplouse lui parut bien modeste, étant donnée sa réputation. Aucune enceinte n’en protégeait les habitations, et nulle forteresse de grande ampleur ne surplombait la verdoyante vallée où elle se lovait. Sur les pentes environnantes, des terrasses soigneusement entretenues accueillaient une grande variété de cultures, avec une prédominance de vignes et d’oliviers. Une vaste palmeraie enserrait les bâtiments, apportant ombre et fraicheur.

Ils stoppèrent à peine arrivés, la plupart des sergents ayant ordre d’attendre à l’entrée des quartiers commerçants, près d’une grande église. Le connétable et les chevaliers continuèrent, accompagnés de quelques valets. Plusieurs commerces étaient encore ouverts, des coups de maillets se faisaient entendre d’une échoppe non loin.

Un vieil homme à la barbe mitée, un bonnet tricoté de travers sur le crâne, s’arrêta devant eux, tenant ferme la longe de son âne qui disparaissait sous un impressionnant chargement de bois. Il resta à étudier la troupe un petit moment avant de s’effacer dans une des ruelles adjacentes. Ne demeuraient à les admirer que quelques gamins à l’allure espiègle qui dévisageaient, les yeux écarquillés, les valeureux hommes du roi.

Un frère convers de l’hôpital, reconnaissable à une tonsure peu récente et une tenue fatiguée, vint à eux et se présenta. Il avait pour charge de les mener là où ils seraient hébergés le temps qu’ils resteraient à Naplouse. Ernaut grimaça, il avait espéré demeurer auprès des puissants, et se trouvait relégué dans l’hospice des voyageurs, avec les pèlerins et les malades.

Le logis était non loin, massive construction aux bloc soigneusement appareillés. En peu de temps, la cour reprit vie, épouvantant les volailles qui péroraient jusque là. Un chien curieux profita de la pagaille pour venir les renifler puis, son inspection faite, retourna dans la dépendance dont il était sorti. Le moine courait partout, le cheveu fou et le regard inquiet. Il passait régulièrement un doigt agacé dans son encolure, comme si l’étoffe le serrait et il déglutissait bruyamment avant de dire la moindre chose.

Les hommes habitués ne mirent pas longtemps à décharger les affaires et à les porter dans la grande salle où les atttendaient des banquettes munies de matelas et de couvertures. Très vite, certains firent glisser leurs savates, s’allongeant sur la couche qui serait la leur pour les jours à venir. Quelques-uns déballèrent des affaires, d’autres encore allèrent s’enquérir des repas, des commodités ou des attractions que la ville offrait.

Ernaut et Droart furent de ceux qui se préoccupaient de leur estomac avant tout. Le souper était prévu d’ici peu, certains pèlerins assistant à l’office. Droart s’étonna de leur présence, en cette période si avancée de l’année. La plupart arrivaient au printemps et repartaient à la fin de l’été, avant la clôture des mers. Le frère hospitalier haussa les épaules et confirma qu’il était rare, même en plein hiver qu’il n’y ait pas quelques marcheurs qui se présentât.

Lorsqu’ils sortirent de la salle commune où ils venaient de prendre leur repas, Droart se frotta le ventre, soupirant.

« Il doit bien avoir taverne où se payer bon vin. J’ai souvenance de futailles à mon goût en cette bonne cité.

— Il faudra demander aux goujats plutôt qu’aux frères. Le valet a plus grande habitude de gouleyer le vin à la veillée.

— M’est avis que d’aucuns pourraient même apprécier de rouler les dés ! » confirma Droart, un sourire fendant son visage rond d’une oreille à l’autre.

Il fit quelques mouvement qui se voulaient d’assouplissement.

« J’ai le fondement comme beurre en baratte ! Espérons que nous aurons beau temps à demeurer à l’héberge.

— N’avons-nous pas labeur à faire ?

— Et quoi donc, cul-Dieu ? On nous mandera bien assez tôt. Demain, nous irons au castel de la reine et pourrons roupiller jusqu’à vêpres, j’en ai espoir.

— Pourquoi donc nous faire venir pour rien ?

— Parce qu’il plaît au sire roi et à son connétable de faire étalage de leur puissance en voyageant avec moult valets et sergents, quand bien même ils ne servent à rien. »

Il s’esclaffa et ajouta, narquois.

« Et puis Baudoin a peut-être bien appris la leçon de Safet[^meleha] ! Les démons ont failli lui ardre[^ardre] le cul. Donc nul voyage sans escorte ! »

Marquant son assentiment, Ernaut sourit. Il demeurait néanmoins fort circonspect quant à la valeur militaire d’une escorte d’hommes comme Droart.

## Naplouse, matin du vendredi 6 septembre 1157

« Cinq ! annonça Ernaut.

« Quatre » répliqua Droart, tout en ouvrant sa main. La mine déçue, le jeune homme s’esclaffa de dépit.

« Cul-Dieu, je ne m’y entendrai jamais à la mourre !

— C’est affaire de talent et j’ai le don depuis que je suis enfançon. T’y peux rien. »

Les deux compères étaient affalés contre des palmiers sur un talus face à l’entrée principale, dans la cour du château. Comme chaque matin, ils s’étaient présentés à l’aube, prêts à accepter toute mission, mais espérant bien y couper un jour de plus. Ils avaient désormais leurs habitudes et, quand ils ne jouaient pas ou en avaient assez de discuter, se contentaient d’attendre, tranquillement allongés, que le repas soit corné ou que la nuit arrive.

Une silhouette fine s’avança dans leur direction. Visage plat, yeux fatigués et blonds cheveux raides, c’était Jehan, le scribe de la reine. Persuadé de son importance, il avançait à petits pas pressés, ondulant du chef comme une poule anxieuse. De la main, il fit signe à Ernaut d’approcher.

« L’hostel du roi a message à faire porter en la cité.

— J’ai monture prête, il me faut juste la seller, maître Jehan.

— En ce cas, suivez-moi ! »

Le clerc repartit au même rythme affairé, comme s’il était le seul à besogner dans la demeure. Ils pénétrèrent dans plusieurs pièces, empruntèrent un escalier étroit pour finalement déboucher dans une grande salle, haute de plafond, aux murs peints de fresques colorées.

Ernaut n’était encore jamais entré là et il se figea, admirant les personnages empreints de majesté qui déambulaient autour de lui dans des vêtements richement ornés taillés dans des étoffes de prix. La pièce était emplie de petits groupes parlant entre eux, foulant des tapis dont le prix d’un seul aurait nourri une famille des années durant. Le vacarme était impressionnant et pourtant nul ne semblait s’en soucier.

Sous un dais brodé de couleurs vives, une femme se tenait debout à côté d’une chaise curule aux couleurs éclatantes. Sa tenue croulait sous les ors et les soieries. Il n’en apercevait guère le profil mais comprit bien vite que la frèle silhouette, à la main nerveuse, était la reine Mélisende. Elle parlait à un homme au physique épais, rond de ventre et de visage, dont la cotte chamarée se tendait chaque fois qu’il bougeait. Il se contentait d’hocher la tête, écoutant avec attention, un index sur les lèvres. Jehan l’amena jusqu’à un petit groupe qui faisait écran devant le connétable, assis sur un coffre, plusieurs documents étalés à côté de lui.

Ernaut fut accueilli dans un sourire par Régnier d’Eaucourt :

« Sergent, voici plusieurs messages à porter à la cité, à l’intention du roi ou du sénéchal, le premier que tu trouveras. Tu y attendras réponse, que tu nous porteras au plus vite. »

Tout en parlant, Régnier indiquait plusieurs documents cachetés, qu’il glissa dans une boîte métallique ornée des couleurs du roi. Le géant ne savait pas quoi dire, le cœur battant la chamade. C’était sa première mission d’importance. Il se contenta d’acquiescer en silence. Le chevalier lui sourit avec chaleur, et posa une main sur son bras, ajoutant à mi-voix :

« Cela signifie qu’il te faut te mettre en selle au plus vite, garçon. »

Ernaut lança un regard désolé, sourit avec reconnaissance et s’éloigna, la boîte à la main. Il en avait vu un certain nombre déjà, tandis qu’il officiait à la porte de la ville. Cette fois, c’est lui qui pourrait pénétrer bride abattue dans les rues de Jérusalem.

Avant de sortir, il jeta tout de même un dernier coup d’œil en direction du siège d’honneur. La reine était désormais assise, écoutant un homme habillé de soie verte, le crâne protégé d’un turban éclatant de blancheur. Son visage fin s’était asséché avec les ans et ses traits accusaient son caractère autoritaire, mais on devinait parfois un sourire fugace dans les séduisants yeux noirs.

Il ne quitta la grande salle qu’à contrecœur, d’un pas traînant, rasséréné néanmoins à l’idée de chevaucher librement jusqu’à la cité, représentant officiel du roi en mission, cavalier d’importance à la monture pressée. Lorsqu’il sortit, Droart s’approcha et remarqua la boîte de messager.

« Te voilà bon pour te tanner le cul jusqu’à la cité !

— Et retour, car je dois porter réponse.

— Viens-t’en donc, on va sangler ton roncin. »

Les deux hommes ne mirent pas longtemps à fixer les sangles et le filet, et Droart ajouta deux gourdes et un sac avec du pain et du fromage. Il les fixait à l’arçon de la selle tandis qu’Ernaut réglait ses rênes.

« J’ai souvenir de fringales lors de ces voyages, il vaut mieux prévoir. Et prends garde à la nuit, la lune n’est qu’un trait, tu y verras bien mal.

— Il me faut chevaucher à la nuitée ?, s’inquiéta Ernaut tout en prenant place en selle.

— Que crois-tu donc ? Que le connétable te mande à Jérusalem pour t’y prélasser la nuit durant en bordel pendant qu’il espère réponse du roi ? »

Puis il claqua la croupe du hongre, saluant son ami d’un signe de main. Ernaut talonna, donna une impulsion de reins et partit au petit galop, dans un crissement de gravier.

« Te voilà messagier royal, mon vieux ! » ❧

## Notes

Si la reine Mélisende s’est effectivement retirée du gouvernement après l’affrontement qui l’avait opposée à son fils au début des années 1150 (voir Qit’a « Épineuse couronne »), son statut lui permit de conserver une influence certaine. Il semble néanmoins qu’elle se soit contentée de s’en servir pour appuyer la politique de Baudoin, sans plus prendre de décision autonome. La plupart de son administration avait été de toute façon soit remplacée, soit exilée avec elle. La ville de Naplouse connut donc un regain d’activité alors qu’elle était installée là, jusqu’à sa mort au début de la décennie suivante.

Au service des puissants, les messagers pouvaient être de plusieurs types, le plus modeste d’entre eux étant le simple porteur de document, qui n’avait pour mission que de délivrer une lettre, dont il n’est pas certain qu’il ait connu la teneur. Bien qu’on ne connaisse pas de boîte de messager du XIIe siècle, l’iconographie en propose une représentation établie.

## Références

La Monte John, « The viscounts of Naplouse in the twelfth century », dans *Syria*, 1938, vol.19, no.3, p. 272-278.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem » dans *Dumbarton Oaks*, vol.26, 1972, p.93-182.

Weber R.E.J., *La boîte de messager en tant que signe distinctif du messager à pied*, Haarlem : Joh. Enschedé en Znen Grafiche Inrichting B.V., 1972.

Merceron Jacques, *Le messager et sa fiction. La communication par messager dans la littérature française des XIIe et XIIIe siècles*, Berkeley - Los Angeles - Londres : University of California Press, 1998.
