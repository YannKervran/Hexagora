---
date : 2021-01-15
abstract : 1146-1160. Partir en Croisade, c’est avant tout choisir de pèleriner à destination du lieu où le Christ aurait vécu. Pour des croyants à la foi simple, c’est un bouleversement qui transforme leur vie.
characters : Père Ligier, Pons de Mello, Gobert
---

# Histoire d’un aller et retour

## Saint-Vaast, parvis de l’église paroissiale, fin de matinée du dimanche 1er décembre 1146

Les averses matinales avaient cédé la place à un ciel bas, qui se reflétait en un gris acier dans les flaques parsemées sur le parvis bordé de frênes. Autour de la porte menant au jardin du presbytère, un petit tonneau avait été mis en perce par le sacristain, comme chaque semaine quand le temps le permettait. Le moment après la messe était toujours l’occasion pour le père Ligier d’offrir à boire de sa cave à ses paroissiens. Ce moment convivial, qu’il avait rapidement instauré après sa nomination, prolongeait le sermon en des sujets plus séculiers, plus immédiats et parfois plus matériels.

Les conversations allaient bon train, avec les soucis d’épizootie qui frappait certains troupeaux de moutons, et la rumeur d’une importante meute de loups aperçue dans les bois au nord du hameau. De nombreux villageois envisageaient d’envoyer une délégation au châtelain afin de demander à ce que des battues soient organisées. Les langues s’activaient aussi autour des commérages qui décrivaient le pèlerinage de grands barons pour aller délivrer la Terre sainte. Le père Ligier, qui avait pu échanger sur le sujet avec ses supérieurs, les chanoines de Saint-Lucien de Bury, était tout feu tout flammes à l’évocation de ce projet.

« Songez, mes amis, que le roi de France et le comte de Flandre eux-mêmes ont annoncé leur désir de prendre la croix ! Comme nos aïeux l’ont fait, ils vont délivrer le tombeau du Christ !

— En est-on là ? Que font donc les barons outremer ? Ne sont-ils pas puissants et bien armés pour garder ce que nos pères ont conquis pour eux ?

— Il se dit que les Turks sont véritables démons, aussi nombreux que criquets et sauvages comme des leus !

— Ils brûlent et pillent chacun des lieux bénis du Christ, y répandant sel et vitriol pour en effacer toute trace, que le Matthieu m’a narré !

— Il tient ça de qui, le Matthieu ? J’encrois pas qu’il ait jamais porté sa hotte plus loin que Beauvais ! moqua un des plus jeunes.

— Justement, à la grande foire de la Saint-Martin, il a encontré pérégrin qui revenait tout droit de la sainte ville : un marchand de toile de ses amis, prud’homme et juré de son métier. »

Le père Ligier hocha de sa grosse tête ornée de sa longue barbe. Lui aussi avait entendu les pires récits sur ce qui se passait au loin. Lors d’une visite de l’évêque chez les chanoines, on avait évoqué le destin des prélats orientaux, qui envoyaient lettre après lettre en appelant à l’aide pour reprendre les territoires perdus les uns après les autres. La situation paraissait catastrophique et que de puissants souverains d’ici se décident à fourbir leurs armes était un signe de la miséricorde divine : il demeurait encore bons chrétiens pour défendre les lieux saints face aux envahisseurs païens.

« Gardons espoir, mes enfants. Saint Vaast lui-même a dû faire face aux hordes de Huns. Que sont ces Turks par rapport aux démons anciens ?

— Vous avez dit qu’ils ont quand même pris le tombeau de l’apôtre Jean. C’est pas rien, ça ! 

— C’est au juste pour pas qu’il reste en leurs mains que les osts s’assemblent. Que pourront leurs lames dérisoires face aux lances et à la bravoure des guerriers d’ici ? On parle d’une troupe forte de plusieurs milliers de combattants.

— Nombreux sont les hommes de Dieu qui ont pris leur houlette pour porter la bonne parole et affermir les cœurs. La prière armera leurs bras aussi sûrement que leurs hasts. »

Un des anciens, à demi aveugle, frappa le sol de sa canne, postillonnant de ses mâchoires édentées tandis que la colère montait en lui.

« Mon fils m’a dit qu’un saint moine à l’est invitait à se purifier, que tout ça était dû au relâchement des mœurs au parmi des jeunes. On accepte trop la corruption chez nous. Commençons donc par nettoyer les mauvaises humeurs qui salissent nos diocèses ! »

Ligier avait entendu parler des événements de Mayence et des alentours. La population, soulevée par les propos d’un prêtre vagant, s’en était pris aux communautés juives, comme souvent lorsque la ferveur religieuse s’emparait d’eux. L’intervention des ecclésiastiques avait permis d’éviter un embrasement général, mais de nombreuses exactions, allant jusqu’au meurtre, étaient à déplorer. Pour n’être jamais confronté aux juifs et ne rien connaître d’eux, Ligier savait néanmoins qu’ils étaient sous la protection de l’Église. L’évêque l’avait rappelé en des termes clairs : il ne tolérerait aucune entorse à sa juridiction.

« Ne prêtez pas l’oreille à si méchantes rumeurs. Ce soi-disant clerc était mécréant et le saint père abbé de Citeaux l’a bien montré : nul ne sert de se purger de nos fautes au détriment des protégés de l’Église. Gardons nos forces pour soutenir les valeureux qui prendront la croix. C’est là bien plus vitale nécessité. »

Le vieux haussa les épaules, contrarié de se voir désavoué en public par l’autorité religieuse du hameau. Agacé de ce revers, il décida de s’octroyer un verre de plus en compensation et s’éloigna d’un pas trainant vers le fût.

« Pensez-vous qu’on va devoir payer la taille pour la Noël ? s’inquiéta l’un des jurés. C’est qu’avec les soucis en ce moment, il n’y a guère de monnaies en nos besaces.

— Les pères chanoines ne m’ont rien mentionné à ce sujet. En avez-vous parlé au châtelain ou à son intendant ? Ils seront les premiers à le dire si cela doit être.

— On n’a guère échangé depuis le paiement du cens, à la Toussaint. »

Depuis qu’il était jeune, Ligier avait assisté chaque année à la remise en cause et aux tractations sans fin autour des redevances. En dehors du cens, rarement discuté sauf lorsqu’on cherchait à l’augmenter, et la dîme, sur laquelle on ergotait plus dans son calcul que sur son bien-fondé, tous les prélèvements étaient l’objet d’âpres marchandages. Il était bien heureux de n’avoir pas à se confronter à de tels marchandages, comme son frère aîné dans la petite seigneurie familiale. En tant que vicaire de la paroisse, il n’était même pas le destinataire de la dîme et se contentait d’une prébende forfaitaire. Au moins ne pouvait-on pas le suspecter au sein du village de tenter de déposséder quiconque, ce qui lui facilitait les choses pour leur faire entendre raison.

« Le sire évêque Odon n’a rien déclaré pour ce qui ressort de ses châtellenies, c’est tout ce que je sais. J’ose encroire qu’annonce sera faite au préalable si cela devait être le cas à l’avenir. »

Autour de lui, les mentons acquiescèrent silencieusement. Chacun être au courant que, aussi sournoisement que les païens à l’assaut des lieux saints, les taxes s’annonçaient rarement longtemps à l’avance.

## Saint-Vaast, manse de Rémy de la Poterrie, début de soirée du lundi 16 juin 1152

La célébration du mariage du fils cadet du vieux Rémy avait rassemblé la moitié du village. Homme affable et débonnaire, le négociant en vases et céramiques avait prospéré au fil des ans et s’était toujours montré amical et accueillant. Chacun avait porté un plat ou une boisson, avait participé aux décorations, et on profitait donc d’une joyeuse occasion, un des derniers moments de fête avant le début des longs et harassants travaux de l’été. Malgré un soleil paresseux, la pluie n’était pas venue, et les danseurs et danseuses farandolaient depuis des heures, au milieu des cris des enfants et des discussions des aînés.

Le père Ligier que l’embonpoint, plus sûrement que la retenue ecclésiastique, empêchait de danser avait trouvé refuge entre une table où rissoles et pâtés étaient proposés à tous à côté des tonneaux de vin mis en perce. Il s’assurait ainsi la compagnie de nombreux autres villageois qui préféraient les délices du palais à celui des pieds et de la musique. Les langues s’activaient autant que les dents.

Se tenait parmi les présents Gobert, un carrier prospère, qui avait récemment perdu un bras dans un accident. C’était un homme taciturne et peu enclin aux plaisirs de la chère ou de la boisson dont Ligier sentait bien qu’il avait le désir de se confier. Il lui laissa le temps, sachant qu’il ne servait à rien de brusquer cet homme austère. Ce fut en lui versant un gobelet de vin que la glace fut finalement rompue.

« Dites-moi, père, comment ça se passe pour prendre l’habit ? »
 
La question sembla un peu incongrue à Ligier, qui prit le loisir d’avaler une gorgée avant de répondre.

« Tu parles de la cérémonie ou d’autre chose ?

— Comment se fait-on moine, en pratique ? Il suffit d’aller frapper à la porte d’un couvent ?

— Oh non, c’est bien importante décision, et il s’en faut d’un long temps entre le désir et la profession. Pourquoi cette question ? Aurais-tu souhait pour un de tes fils ? »

Gobert secoua la tête en dénégation.

« J’ai beaucoup réfléchi et prié alors que je gisais, après l’accident. Je me demandais si ce n’était pas là un signe de Dieu d’abandonner mes tâches et me vouer à plus saint office.

— Ne doute pas que Dieu est caché derrière toute chose. Mais pourquoi penses-tu qu’il veut que tu te fasses tonsurer ?

— Je ne sais, à dire le vrai. J’ai pourpensé ma vie et je suis peut-être trop revêche, excessivement dur avec mes œuvriers. J’en ai pas tant perdu, mais plusieurs sont tout de même morts depuis que j’ai la carrière en charge. Dieu m’est témoin que je ne faisais pas cela pour m’enrichir. J’ai toujours eu désir de bien faire, en chaque chose, exigeant souventes fois un peu plus que la suffisance. Était-ce là vanité, mon père ? »

Tout en parlant, il jetait de rapides coups d’œil en direction de Pons de Mello, un des anciens du village, cadet d’une longue fratrie qui avait été brisé par des années de labeur en tant que simple journalier. Conteur infatigable, il était aussi le confident silencieux et attentif de bien des âmes, qu’il savait guider adroitement vers le curé lorsque c’était nécessaire. Lui aurait fait un moine tout à fait honorable, estima Ligier.

Gobert était un des piliers de la communauté. Veuf depuis des années, il s’était consacré à son travail, d’extraction de pierres, avec une vigueur et une discipline qui n’avait rien à envier à la vie régulée la plus stricte. Il était aussi un organisateur de talent pour les corvées, habile à remblayer un chemin, drainer un marécage, fortifier un gué ou tracer une route. Ce serait une perte certaine pour le village que toutes ces compétences s’évanouissent derrière les murs d’un couvent.

« Se faire moine alors qu’on est ni enfançon ni vieillard, que voilà bien hasardeux choix, mon fils. Pourquoi ne pas te faire pérégrin plutôt ? Tu pourrais soutenir de tes deniers quelques indigents et vous faire compagnons pour un temps de foi et de prière. Et à ton retour, les tiens te retrouveraient, l’âme débarrassée de ces tourments, prêt à aider ainsi que tu l’as toujours fait par tes suggestions et conseils, forts avisés. »

Gobert plissa les yeux, fronçant les sourcils tandis qu’il analysait la proposition. Il demeura un certain temps ainsi figé, insensible à la musique et au brouhaha ambiant. Tout en sirotant son vin, Ligier pouvait presque entendre les rouages de sa réflexion qui s’activaient.

« N’avez-vous pas dit tantôt que plus méritant pérégrinage était aussi le plus périlleux ?

— De certes. La mortification du corps aide à la résipiscence, mon fils. Tu n’as pas besoin de prier pour guérison ou secours. Rome serait bonne destination…

— Je pensais plutôt à la Sainte Cité. À Jérusalem. »

Ligier s’accorda une moue stupéfaite. Il avait déjà assisté à des bénédictions dans la cathédrale de Beauvais, mais jamais un de ses paroissiens n’avait envisagé si ambitieux voyage. C’était aussi un signe de profonde religiosité auquel il ne pouvait demeurer insensible.

« Eh bien, eh bien, comme tu y vas, mon fils. C’est là bien formidable vœu, bien merveilleuse destination ! Prier sur les pas du Christ ! Sans doute aucun, tu en reviendrais transformé.

— Je pourrais laisser l’affaire au gamin, il est grand bien assez. Et j’ai de quoi payer pour quelques âmes qui souhaiteraient me compaigner. »

En disant cela, il regardait Pons, dont le sourire ravi dévoilait les gencives dégarnies. Le vieil homme avait les yeux larmoyants lorsqu’il plongea les doigts dans son col pour en sortir un anneau qu’il portait en sautoir.

« Je n’ai pu me résoudre à confier à la terre la bague de ma Beneoite. Le tremper dans le Jourdain serait mon souhait le plus cher… »

Gobert posa une main amicale sur le bras de Pons, confirmant d’un sourire discret la silencieuse promesse. Devant tant de sollicitude, le père Ligier perdit sa faconde habituelle. Il ne réfléchit guère avant de s’exprimer, emporté par son naturel débonnaire.

« Il faut raison garder, mes fils, mais j’en parlerai à l’évêque Henri. Quoiqu’avant cela avec les chanoines ! Vous aurez besoin de pasteur pour cheminer d’un bon pas tout ce temps. » 

## Bury, collégiale Saint-Lucien, veillée du mardi 16 novembre 1154

Le froid humide consécutif à de fréquentes journées brumeuses et pluvieuses avait incité les chanoines à se regrouper autour d’un imposant brasero de fer. Douillettement installés dans des couvertures, ils abordaient comme chaque soir différents points organisationnels ou liturgiques. Un des sujets les plus complexes qui les occupait depuis de nombreux mois était le départ prochain de leur vicaire de Saint-Vaast, le père Ligier. Il fallait lui trouver un remplaçant le temps de son absence, ce qui n’était pas une mince affaire. La prébende était correcte, mais sans excès, et ne pouvait attirer les meilleurs clercs. Les chanoines ne tenaient pas à rompre une tradition ancienne en relevant les montants, sans pour autant se résoudre à confier les âmes du hameau à n’importe quel prêtre mal dégrossi.

Malgré la surcharge de travail que cela entraînait, ils étaient plutôt satisfaits du pèlerinage en préparation. C’était un excellent signe envoyé aux autres paroisses et un indicateur de la rigueur de leur prédication, que ne manquerait pas de remarquer l’évêque de Beauvais. Celui-ci, le propre frère du roi de France, qui avait lui-même pris la croix quelques années plus tôt, était un ecclésiastique assez austère, dont les exigences morales étaient parfois pesantes. Il n’avait d’ailleurs accepté les honneurs de la charge qu’avec réticence, ayant longtemps préféré demeurer simple moine.

« J’ai envoyé quelque lettres à des amis que j’ai encontré alors que j’étudiais à Tours, expliqua un des chanoines, tout en essuyant son nez d’un revers de main. Il se trouve toujours des écoliers de valeur en ces lieux.

— Nous devrions peut-être insister sur le fait qu’il s’agit là d’une belle paroisse, dont l’église vient d’être rebâtie de bonnes pierres de taille, pas d’une petite chapelle retirée ? hasarda l’un d’eux, sans conviction.

— Je n’aurais guère confiance en un prêtre sensible à si séculier argument, mon frère, le corrigea un des autres, la voix pincée.

— Le souci nait de ce que les meilleurs d’entre tous préfèrent justement la règle au siècle. Il demeure pourtant que l’apostolat est de première importance. Le proclamer en lieux saints dignes de la célébration du Très-Haut me semble receler bel attrait pour le souligner. »

Les têtes hochèrent en cadence, doucement, tandis que chacun s’abîmait dans ses réflexions afin de découvrir une solution à leur problème. Ce fut la voix du plus âgé qui les sortit de leur marasme.

« Nous verrons bien si nos différents courriers portent fruit. Nous pourrons en reparler au père Pierre. Il doit se dénicher quelque novice en ses murs qui pourraient nous porter assistance à moindre mal. »

Il se racla la gorge en un long grincement avant de continuer.

« À propos de bel édifice, je voulais vous entretenir des travaux en cours ici. Je ne sais pas pour vous, mais j’ai grand mal à trouver le repos avec toute cette agitation… Il me serait amiable de m’accorder droit de coucher en ma petite maison à la sortie du hameau. Avec les froids hivernaux, je sens que je n’ai plus guère de vaillance, et mon cœur se serre à l’idée de pauvrement célébrer nos messes par cause de forte fatigue.

— Oui, je suis d’accord. Ces travaux, de nécessité, sont pénibles à la longue, et je souscris aussi à cette motion pour que nous puissions déroger à la règle, de façon provisoire, pour affermir nos corps.

— Hum. Nous devrions en référer au père abbé, ce me semble. C’est là contraire à nos devoirs.

— Ce ne serait que contingent, la période des frimas. Le service de Dieu me semble de plus grande importance que de dormir ici dans l’inconfort. Cela n’apporte rien, et risque de grever notre tâche divine.

— Sans compter que le temps d’envoyer missive et de recevoir son accord, dont je ne doute pas un instant, les beaux jours seront de retour et la situation aura changé. »

Un peu mal à l’aise, le chanoine opina. Lui aussi trouvait que les cellules de leur collégiale étaient bien austères, et ce sans que cela ne serve leur vocation. Il serait possible d’en améliorer le confort lorsque le printemps reviendrait. De façon à conserver leurs forces pour leur mission première, de célébration du mystère divin.

## Saint-Vaast, manse de Gobert le carrier, veillée du samedi 26 mars 1160

Comme cela devenait traditionnel depuis le retour des marcheurs de Dieu, les Vigiles de Pâques étaient prolongées par une veillée chez Gobert. C’était l’occasion pour ceux qui étaient allé en Terre sainte de montrer les palmes ou leurs vêtements usagés, encore ornés de la croix, ou leur besace, rafistolée et distendue.

De son côté, le père Ligier avait ramené un petit crucifix, taillé dans une pierre du Mont de la Transfiguration. On lui avait expliqué qu’il transformerait n’importe quel vin en un remède contre le poison si on le trempait dedans. Il était à demi convaincu de l’affirmation, partagé entre l’espoir de voir un miracle opérer et la crainte qu’on lui ait menti. Il chérissait malgré tout l’objet, souvenir de moments inoubliables. Il avait senti que sa prédication était désormais plus vivante, plus incarnée. Il maîtrisait d’autant mieux les récits des évangélistes qu’il avait foulé les lieux où tout cela s’était déroulé. De plus, il éprouvait au fond de lui la certitude que ses paroissiens accueillaient intensément la vérité contenue dans ces écrits. Il avait dépassé le stade de la récitation d’un savoir froid, il était devenu lui-même un apôtre en mettant ses pas dans ceux du Christ. À travers chaque parabole, il leur parlait de sa vie, de son expérience personnelle. Et ces mots empreints de lui touchaient d’autant plus juste, plus fort.

De son côté, Gobert s’était un peu déridé. Il avait gagné en souplesse d’esprit ce que son corps avait perdu en vigueur. Désormais moins taiseux, il ne se faisait généralement pas prier pour faire part de ses récits, mais ne les imposait jamais. Il semblait avoir cultivé une vie intérieure plus harmonieuse et moins inquiète. Ayant retrouvé la carrière dirigée de main de maître par son fils, il s’était mis en retrait, se contentant de superviser les comptes ou les acheminements sans plus trop se rendre dans les zones d’extractions. Il avait de nouveau évoqué avec le père Ligier le projet de quitter le siècle. Mais sa demande était moins angoissée, et semblait naître du désir de prolonger le voyage intérieur qu’il avait entamé. Il faisait peu de doute qu’il rejoindrait une abbaye d’ici quelques mois, quelques années au plus.

Le vieux Pons, dont l’esprit vagabondait de plus en plus, émerveillait les enfants par ses récits fantastiques. À l’entendre, chaque lieu regorgeait de miracles et de créatures plus incroyables les unes que les autres. Et il achevait ces veillées en brandissant l’anneau dont il disait qu’il avait touché toutes les places les plus saintes de la terre, avant d’être purifié de tout mal par un baptême à l’endroit même où le Christ s’était baigné.

Malgré les inventions dont il parsemait ses récits, le père Ligier n’avait pas le cœur de reprendre le vieil homme. Il avait cru longtemps qu’il était trop fragile pour affronter un tel trajet, et pensait qu’il demeurerait là-bas, une fois son vœu accompli. Mais Pons avait choisi de revenir, car il voulait montrer la bague à son épouse. Il avait laissé un peu de son esprit en chemin, mais n’en était devenu que plus attachant et plus simple dans les manifestations de sa foi. Le visage tendu vers un au-delà qu’il était seul à percevoir dans la pénombre des lampes à huile, il brandissait devant les enfants son anneau tel un prêtre l’hostie durant l’Eucharistie. ❧

## Notes

Les motivations des croyants médiévaux qui décidaient de tout abandonner pour prendre le chemin de lointaines destinations sans certitude de retour ou d’accomplissement de leur vœu nous demeureront à tout jamais inaccessibles. Alors que leur paysage mental ne dépassait guère leur paroisse, ou leur évêché, pour la plupart semblable parcours devait ébranler profondément leur rapport au monde. En outre, la découverte de la réalité matérielle de ce qui n’était jusque là que des récits vaguement compris ou connus, leur permettait de créer un lien plus immédiat, personnel, avec la complexe narration chrétienne.

En même temps qu’ils incarnaient plus pleinement leur foi au sein de leur communauté, ils se faisaient les propagateurs d’histoires qui n’avaient que peu à voir avec la religion telle que professée par les prédicateurs orthodoxes. Il me semble qu’ils représentent une parfaite illustration de cette ambivalence de la pratique catholique médiévale, nourrie d’une liturgie et de commandements très encadrés par des préceptes prêchés avec une sévérité de plus en plus stricte et d’un folklore narratif environnant basé sur un fantastique riche et contradictoire dans ses développements.

Pour l’anecdote, le titre de ce Qit’a est une référence directe au récit que Bilbo Sacquet fait de son voyage dans le roman de J.R.R Tolkien « Bilbo le Hobbit ». Toute présence d’un anneau dans mon Qit’a n’est certainement pas fortuite.


## Références

Graboïs Aryeh, *Le pèlerin occidental en Terre sainte au Moyen Âge*, De Boeck Univers, Paris & Bruxelles : 1998.

Tolkien John Ronald Reuel, Lauzon Daniel (trad.), *Le hobbit*, Paris : Christian Bourgeois, 2012.

Ward Benedicta, *Miracles and the medieval mind : Theory, record, and event, 1000-1215*, Revised edition, Philadelphia, University of Pennsylvania Press, 1987.
