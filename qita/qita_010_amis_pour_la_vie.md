---
date: 2012-09-15
abstract: 1148-1156. Retrouvez Ernaut, de son enfance à la veille de son départ, quelques jours avant le début de *La nef des loups*. Si les moyens de transports étaient peu développés, les voies de communication étaient néanmoins nombreuses. Les cours d’eau, tout particulièrement étaient régulièrement empruntés. C’est ainsi que des liens se tissaient avec de lointaines provinces, que des amitiés naissaient parfois.
characters: Ernaut, Ermont de la Treille, Lambert, Robert le Tonnelier, Gillot
---

# Amis pour la vie

## Abords du Bourg-l’Évêque, Bayeux, mercredi 3 novembre 1148

La presse était si dense sur le champ de foire de la Toussaint qu’il fallait sans cesse se contorsionner ne serait-ce que pour demeurer en place. On se bousculait, on se gênait, on piétinait dans l’herbe devenue boue en raison de la pluie et de la cohue agglutinée parmi les étals et les cabanes. Des éclats de voix amicaux, intrigués, interrogateurs, moqueurs fusaient depuis les groupes de marchands industrieux, jaillissant des échoppes le long des allées. Des soldats parcouraient la foule, d’un pas alerte, le visage goguenard au milieu des hommes d’affaires. Ils fronçaient les sourcils, dévisageaient quiconque aurait pu se prêter à un larcin. De temps à autre, un notable, fier chevalier à l’allure martiale, ou ecclésiastique à la tenue chatoyante s’avançait, impérieux, généralement environné de parasites bourdonnants et obséquieux, saluant d’une main condescendante les boutiquiers dont ils venaient admirer les produits.

Devant un stand où plusieurs tonneaux avaient été mis en perce, un trio de négociants devisait de leurs marchandises respectives. Le premier, grisonnant à l’imposante bedaine drapée dans une riche cotte de laine orange, s’exprimait avec une voix forte.

« Entendez bien, maître Robert, je peux acheminer plusieurs douzaines de tonnels de ces vins. Chacun est de six muids parisiens, de bonne mesure.

— Certes, maître Ermont, seulement douze sols la futaille me semble bien dispendieux, même si le vin est fort gouteux.

— Mon souci, ce sont toutes les nouvelles taxes depuis Vézelay. Chaque seigneur fait son pont et y installe péage en ses piles !

— Ne pourrions-nous trouver accord à quelques deniers rabattus ? Se rapprocher de onze sols ? »

Ne portant aucun intérêt à la discussion, un jeune garçon à l’impressionnante carrure, qui n’avait pas dix ans, le cheveu châtain coupé court et la mâchoire forte encadrant un regard clair, écarquillait de grands yeux sans s’éloigner de son père Ermont, le Bourguignon. Il n’avait jamais vu autant de denrées en un seul lieu. Des étoffes, surtout, de toutes les épaisseurs, dans un ébahissant foisonnement de coloris et de motifs.

En bons camelots, certains vendeurs interpelaient les passants devant eux en déployant en gestes amples les toiles, faisant miroiter les finitions, vantant la finesse des tissages. La plupart proposaient de la laine, bien sûr, mais on pouvait aussi trouver d’autres fibres, du chanvre, du lin voire du coton, ou des assemblages mélangeant l’une et l’autre. La plus désirable néanmoins, la plus attirante, c’était la soie, que bien peu présentaient à l’envi. Il fallait s’afficher comme acheteur potentiel pour pouvoir admirer les riches brocards, les pièces brillantes gardées bien à l’abri, dans un écrin, dans un coffre discret. Un coupon nécessaire à la confection d’un vêtement aurait permis à deux ou trois familles de vivre une année entière, alors forcément, on ne les dévoilait pas à n’importe qui, on se méfiait des curieux qui auraient pu salir de leurs doigts fureteurs, endommager de leurs regards envieux ces articles de prix.

Par ailleurs, beaucoup de marchands n’avaient avec eux que des échantillons, car ils étaient là pour chercher des partenaires, comme son père. Il produisait du vin, achetait et revendait celui de nombreux autres vignerons, qu’il acheminait jusqu’aux lieux de foire. Pour l’heure, il avait quitté son emplacement, laissant Lambert, un de ses fils, tenir seul l’endroit, avec les quelques tonneaux qu’ils avaient apportés avec eux depuis Vézelay.

Ermont de la Treille était un négociant opulent, en biens comme en volume. Son ventre impressionnant était d’ailleurs en accord avec son teint couperosé. Il aimait son vin, celui des autres, et tout ce qui pouvait les accompagner lors de bons repas. Si ce n’était son dos épais et ses larges épaules, on aurait pu le croire abbé ou évêque tant il semblait empâté. La rouerie qui allumait parfois son regard et sa voix chaleureuse constituaient autant de signes évidents de son habitude au marchandage. Pour l’heure, il discutait âprement avec deux clients potentiels, dont le premier, aussi ventru qu’Ermont, remontait sans relâche une longue mèche malmenée par le vent sur son crâne. Le second paraissait presque maigre, enserré entre ces deux panses rebondies. Mais il appartenait sans doute aucun au même commerce qu’Ermont, son nez pouvait en témoigner. À ses côtés, un autre garçon s’ennuyait ferme, piaffant d’impatience, le regard au loin, s’attardant parfois sur la silhouette imposante d’Ernaut, cherchant à en sonder les intentions. Le visage brun et le poil frisé, un sourire espiègle apparaissait par intermittence, dévoilant deux dents cassées. Se rapprochant du jeune Bourguignon sans pour autant perdre le contact avec son père, il lui lança :

« Vous avez cheminé long temps ?

— Pour sûr ! Par barge et chariot, il nous a fallu plusieurs semaines. Mais père a grand usage de cela… »

Le garçonnet hocha la tête, non sans avoir jaugé d’un œil expert le gabarit d’Ermont. Puis il reporta son attention sur Ernaut et lui sourit de nouveau.

« On me nomme Gillot, fils de Robert le Tonnelier. Et toi ?

— Ernaut, de Vézelay.

— Ton père fait aussi négoce de vin ?

— Oui-da. Nos coteaux ont bonne fame par chez nous, et père s’emploie à les faire connaître au loin. »

Le jeune Normand secoua une nouvelle fois la tête en accord, bien décidé à ne rien céder en ce qui concernait son expertise du sujet. Son seul souci était qu’il n’y connaissait pas grand-chose. Prudent, il choisit donc de se rabattre sur un autre, mieux maîtrisé.

« As-tu parcouru la foire ? C’est peut-être la prime fois que tu t’y trouves ?

— Je n’ai guère eu occasion à cela, je suis mon père en ses affaires, voilà tout. Mais j’ai pu apercevoir quelques attrayantes boutiques.

— Il te faut absolument me compaigner… »

Il se pencha et souffla, le regard malicieux :

« Il y a au nord du pré plusieurs bains installés pour les voyageurs. Et ils ne dédaignent pas d’accueillir en leur baquet ravissantes pucelles. Je connais des lieux par où faire l’espie ! »

Ernaut ne put réprimer un gloussement de ravissement à l’idée d’aller voir. Pas tant pour ce qu’il pourrait y admirer, mais simplement enthousiasmé par la perspective de s’abandonner à un coupable amusement. Gillot tira sur la manche de son père, assurant d’un regard à son désormais complice qu’il saurait obtenir l’autorisation de baguenauder parmi les étals.

## Bayeux, vendredi 5 novembre 1148

Les deux enfants couraient, armés de bâtons pour tenter d’effrayer ceux qui se mettaient en travers de leur route. Pour l’heure ils n’avaient eu à affronter que quelques volailles, une poignée de chats et de rares chiens, dont certains n’hésitaient pas à montrer les dents s’ils étaient affairés après sordide repas trouvé dans le caniveau. Le seul qui s’était opposé avec indifférence à leur avancée était un énorme goret au poil hirsute, dont la plus repoussante qualité était d’être surveillé par un porcher dont la méchanceté du regard n’était battue en brèche que par l’odeur pestilentielle s’exhalant de ses vêtements déguenillés. Les farouches aventuriers avaient opté pour une retraite stratégique.

Depuis deux jours qu’ils écumaient les meilleurs coins connus par Gillot, les deux garnements étaient parvenus à se faire une réputation satisfaisante auprès de badauds indignés ou d’habitants agacés, voire de commerçants chagrinés. Pour l’heure, ils caracolaient en direction du château, à l’angle sud-ouest de la cité enclose, fouettant de leurs badines leurs montures imaginaires. Les nombreuses flaques en chemin étaient autant d’occasions de salir leurs propres chausses que les personnes environnantes, dans un vaste éclat de rire bien entendu. Essoufflés, ils parvinrent aux abords de la rue qui partait de la cathédrale, depuis la petite place accueillant le parvis.

Un groupe de badauds discutait devant la colossale façade de pierre, entourant une poignée de voyageurs arborant la tenue des pèlerins, bâton en main et croix sur l’épaule. Gillot, intrigué, se rapprocha l’air de rien, tendant l’oreille. Il était question du passage Outremer du roi, que ces marcheurs de la Foi espéraient rejoindre. Ils venaient du Cotentin et prévoyaient d’arriver en Terre sainte d’ici l’été. Des curieux et des promeneurs leur demandaient de porter des nouvelles du pays à plusieurs d’entre eux partis depuis de nombreux mois, dont certains avec l’ost[^ost] de France. Les informations étaient rares et la progression semblait moins aisée qu’il ne l’avait été supposé. De fréquentes bénédictions et pieuses invocations accompagnaient ceux qui allaient à leur tour entreprendre ce périlleux voyage.

Reprenant leur avancée vers le château, le jeune normand se tourna vers Ernaut, impatient de lui dévoiler le prestige de sa nation.

« Tu sais que le père de notre duc, il est devenu roi à Jérusalem ?

— C’est pas Godefroy[^godefroy] le preux, le suzerain du royaume Outremer plutôt ?

— Ah non, le père du nouveau duc, Geoffroy[^geoffroyplantagenet], est parti pour faire épousailles de la princesse, voilà déjà long temps.

— Et il est toujours roi ?

— Aucune idée. Il a certainement dû batailler ferme contre les païens ! »

Joignant le geste à la parole, Gillot faisait de grands moulinets avec son bâton, aussi dangereux pour les alentours que pour son propre visage.

« S’il est aussi adroit et rusé que le grand Guillaume[^guillaumeconq], il est de certes encore en selle, lance en main, et boute ses ennemis droitement…

— Le grand Guillaume ? Je n’en ai point connoissance. C’était fort chevalier normand ? »

Gillot écarquilla les yeux, amusé et sauta à pieds joints dans une flaque.

« Le plus grand ! Il devint roi par ses prouesses !

— Je ne te crois point, il n’est nul roi de France avec pareil nom, c’est là fable d’enfançon… »

S’arrêtant brutalement, Gillot posa les deux mains sur les hanches et commença à entonner à tue-tête une chanson égrillarde :

>« Fils à diable plus fort qu’ourson,  
>Il était fort bel enfançon.  
>Poil soufré bien dru sur son chief,  
>De sa mère tira bon lait  
>Qui le fit haut et bien épais  
>Mais de son père n’eut point fief. »

Secouant la tête comme un dément, il s’attirait quelques sourires convenus parmi les passants, du moins ceux qui comprenaient sans doute fort bien l’air qu’il déclamait.

>« D’un pet repoussait les saxons  
>Rotait flammèches sur bataillons,  
>Les seigneurs n’en voulurent baron  
>Mais devint roi à sa façon »

Tout en continuant à lancer ses vers avec entrain, le jeune garçon reprit son chemin en sautillant, accompagnant parfois son chant de gestes éloquents. Ce fut donc haletant qu’il parvint au débouché de la rue, face à l’impressionnante muraille. Mais il avait gardé suffisamment de souffle pour lancer avec emphase :

«Voici chastiaux que le roi Henri[^roihenri] prit voilà bien longtemps, quand il fit brûler la ville. »

Devant le regard admiratif d’Ernaut, le jeune guide sourit.

« Ça te dirait de voir la belle demeure de pierre du chanoine Conan ? Elle aussi a brûlé alors, mais elle demeura assez belle pour faire envie au roi de France dit-on… »

Sans même attendre la réponse, Gillot se mit à galoper avec entrain en direction d’une ruelle qui s’ouvrait entre deux boutiques à la belle devanture. Le sourire aux lèvres, Ernaut le rejoignit, faisant claquer son bâton contre les murs des maisons.

## Bayeux, tôt le matin du jeudi 1er mai 1152

La cohue effroyable avait attiré un grand nombre de curieux et fait fuir les chats et chiens errants du quartier. Toute la jeunesse environnante s’était assemblée sous les fenêtres du vieil orfèvre récemment remarié. L’épouse venue de Bretagne, disait-on, était aussi sage et timorée qu’agréable à regarder, et les adolescents estimaient que cela était prétexte à aller les réveiller bien tôt en ce matin du premier mai. Nombreux parmi ceux présents auraient volontiers tressé une couronne pour orner son joli front.

Au milieu des agités, frappant comme beau diable ses deux louches en bois sur tout ce qui passait à sa portée se trouvait Ernaut. Lui n’avait aucune idée de l’aspect de la dame, mais cognait avec autant d’enthousiasme que les autres, hurlant et vociférant comme un animal en rut. Lorsque le vieil époux et sa nouvelle femme finirent par se montrer à la fenêtre, ils eurent droit à plusieurs chants aussi passionnés que grossiers. De bonne grâce, ils saluèrent la foule, et l’orfèvre, en bon stratège, leur jeta même quelques monnaies pour aller s’offrir des pichets de vin à leur santé. Les chansons grivoises se transformèrent quasi instantanément en bénédictions et souhaits de long bonheur. Puis la colonne reprit sa route, continuant un peu de tapage, histoire de ne pas faire oublier que ce jour, ils avaient bien l’intention de célébrer la jeunesse. Les matrones qui les croisaient arboraient un franc sourire ou un froncement de sourcils hostile, selon qu’elles aient une fille dans leur maison ou pas. En peu de temps, le charivari parvint à la porte du faubourg Saint-Jean.

L’enthousiasme n’avait guère baissé, mais les voix s’étaient calmées le temps pour eux de se concerter. Le guet n’allait guère tarder à déverrouiller les accès, il fallait se dépêcher de se confectionner des couronnes pour offrir aux jeunes filles, ainsi qu’amasser de nombreux rameaux à accrocher aux portes des belles. Ernaut n’en connaissait guère, mais il faisait confiance à Gillot, lui-même comptant parmi les adolescents les plus exubérants.

« Je te ferai montrance des plus belles, n’aie mie crainte. Aucune ne saurait résister à beau Bourguignon comme toi ! »

Disant cela, le jeune Normand lança une bourrade dans les côtes de son ami. Il était vrai que la stature d’Ernaut attirait souvent les regards féminins. Déjà plus grand que la plupart des adultes, presque d’une tête, beaucoup enviaient son impressionnante carrure. Il manipulait les tonneaux de son père comme d’autres de simples tonnelets, et rien ne semblait jamais pouvoir le fatiguer. Son inépuisable énergie n’était limitée que par sa malheureuse propension à la gaspiller en vains efforts ou geste maladroits. Sans que cela n’entame en rien son enthousiasme, d’ailleurs.

À la fin de l’après-midi, une partie de la population s’était assemblée aux abords du champ de foire pour y achever la journée en musique et en danse. Chacun souriait, heureux de voir que les traditions étaient respectées, et que la jeunesse était pleine de fougue et d’entrain. On avait ri de la mésaventure de quelques parents, ayant découvert un de leurs enfants parmi les couples à l’ardeur un peu trop prononcée. On avait chanté, dansé et joué sans se soucier des remontrances d’une poignée de chanoines révoltés par le paganisme latent de ces pratiques. Une longue farandole serpentait, ondulant au rythme des musiques et des cris, emportée par des adolescents bondissants.

Transpirants, Ernaut et Gillot lâchèrent les mains au détour d’un virage et rejoignirent un tavernier qui avait fort opportunément mené quelques tonneaux. C’était de la petite bière, pleine d’amertume, mais fort rafraichissante.

Tandis qu’ils dégustaient leur breuvage, s’essuyant le front de leur manche, un troupeau de jeunes filles gloussantes et ricanantes s’approcha des deux garçons. La moins farouche, jolie brune à l’air espiègle, esquissa une courbette moqueuse et sourit malicieusement à Gillot.

« Dis-moi, le Tonnelier, comptes-tu tenir à l’écart ton compaing ainsi toutes les danses ?

— La paix, Osane, nous sommes en eau à force de sauts et de cabrioles !

— Déjà épuisés ? Puis elle ajouta, le regard lourd de sous-entendus à destination d’Ernaut : quel dommage, pourtant on le croirait bâti comme ours sauvage… »

La remarque déclencha une salve de ricanements chez ses compagnes. Ernaut ne savait plus où donner de la tête, un sourire barrant son visage. Au cas où, il bomba légèrement le torse tandis qu’il avalait la dernière goutte de son gobelet, dévisageant la jeune fille aux longs cheveux nattés.

« Je ne voudrais certes pas affliger si joliets minois ! »

Osane offrit un large sourire en réponse, faisant plisser son nez de séduisante façon. Bousculée par ses camarades lors de cet échange, elle en perdit sa couronne de fleurs. Avant qu’elle n’ait eu le temps de réagir, Ernaut l’avait ramassée et la lui tendait, avec une joyeuse révérence. Les jeunes filles se mirent à pousser de petits cris et Gillot lui-même tapa dans ses mains tandis qu’elles commencèrent à scander :

« Le baiser de Mai ! Le baiser de Mai ! »

Un peu interdit, le colosse interrogea Gillot du regard. Amusé, celui-ci lui expliqua qu’à Bayeux, il était coutume d’embrasser celle à qui l’on offrait la couronne de mai. Les joues empourprées, Osane avait un peu perdu de sa superbe et espérait visiblement qu’il s’acquitte de son devoir. Bien peu embarrassé, Ernaut approcha son visage et déposa un rapide baiser sur les lèvres de la jeune fille, ce qui déclencha une explosion de rires et d’exclamations joyeuses. Puis, rejoignant la farandole, les mains s’accrochèrent et les jambes s’élancèrent en rythme. Ernaut eut à peine le temps de rendre son gobelet qu’Osane l’entraînait dans une danse échevelée, le regard énamouré, illuminé par un sourire radieux.

## Vézelay, après-midi du mardi 29 mai 1156

Ermont de la Treille était en train de superviser un chargement de tonneaux à destination de Cravant, où ils seraient placés sur des barges. Il avait ajouté à ses propres fûts ceux d’autres petits récoltants des environs, et il expédiait tout cela, par voie d’eau, jusqu’à Paris, la Normandie et même au-delà. Revêtu de son habituelle cotte de laine fatiguée à la couleur indéfinissable, il mettait souvent la main à la pâte, travaillant aussi dur que ses employés.

Pour l’heure, la chaleur était telle qu’il prenait le frais tout en discutant avec le charretier qui mènerait son bien par les chemins. Ayant son cellier ouvert directement sur la grande rue qui montait vers la basilique de La Madeleine, il aperçut bien vite le petit groupe de voyageurs, dont il reconnut immédiatement plusieurs membres. C’étaient comme lui des habitués des foires normandes et parisiennes, des négociants avec lesquels il faisait parfois affaire. Mais il était surprenant de les voir venir jusqu’à Vézelay. Levant la main en guise de salut, il leur sourit, déclenchant en retour d’éloquentes démonstrations amicales.

« Le bon jour à toi, Ermont !

— La bien venue en notre fière cité de Vézelay, compaings. Heureuse surprise que voilà, je n’ai pas usage de vous y encontrer. »

Le visage de son interlocuteur se ferma instantanément tandis qu’il serrait avec enthousiasme la main offerte.

« C’est que je suis porteur de bien tristes nouvelles. J’ai donc décidé de pousser jusque ici, les donner de moi-même.

— Quelles sont-elles ? Quelque guerre en France de nouveau ? Ou en Normandie ?

— Rien de ce genre, même si cela n’est jamais exclu…

— Vous m’intriguez fort. »

La face couperosée et débonnaire d’Ermont devint inquiète tandis que son front se plissait. D’un geste, il fit signe à la cantonade d’entrer chez lui. Une fois dans la cave, parmi les fraîches odeurs de vin, il redemanda ce qu’il en était.

« Je suis venu vous faire savoir que le Tonnelier est passé, voilà plusieurs semaines de cela.

— Robert ? Mais comment cela ? Il était bien de dix ans mon cadet, et aussi solide que beffroi d’église !

— De certes. Mais il a péri, avec ses biens et son fils Gillot. Emportés en estuaire de Seine avec leur embarcation. »

Ermont remua la tête, secoué.

« Je n’ai jamais aimé ces lieux. Trop mouvants, trop capricieux… Cette maudite brume !

— Il portait à Rouen marchandises. Son pilote habituel, le père Berthelot n’a pas passé l’hiver, à cause de fièvres quartes. Alors il a pris un jeune, mais nul n’en est revenu.

— Un pilote jeunot pour frayer parmi l’estuaire, voilà bien folle idée… Le Ratier[^ratier] n’est guère enclin à pardon. »

Les deux hommes gardèrent le silence un long moment. Puis Ermont remplit un pichet avant de servir l’assemblée, les lèvres toujours closes. Seules des paroles venues de la rue ou de l’arrière-cour, les aboiements de chien excités ou amusés résonnaient par-delà la porte. Un chant se fit soudain entendre, poignée de strophes sans queue ni tête lancées sur un air entraînant par une voix grave. Le regard d’Ermont se fit sévère.

« Voilà mon cadet, Ernaut. Il est fort compaing du jeune Gillot. Son passage Outremer est prévu pour dans quelques jours, à destination de la Sainte Cité. Ne lui en soufflez mot, il en aurait grande peine et y verrait peut-être ténébreux présage. »

Lorsque le jeune géant entra dans la salle voûtée, il marqua un temps, avant de reconnaître certains des visages. Puis il salua l’assemblée d’un geste nonchalant, son imposante silhouette se découpant sur la lumière extérieure. N’obtenant qu’une réponse peu enthousiaste, il s’esclaffa.

« Hé bien, compaings, n’y a-t-il pas toujours joie à arriver sauf à destination ? » ❧

## Notes

Le commerce médiéval n’était pas que local et de nombreux produits voyageaient au loin. On pense généralement aux épices venues d’Orient, par-delà la Méditerranée, mais d’autres denrées bien moins exotiques traversaient l’Europe. Le vin de Bourgogne était ainsi acheminé par voie d’eau tout au long de la Seine, ce qui lui permettait de toucher les régions nordiques, les ports normands offrant bien évidemment des débouchés vers l’Angleterre et au-delà.

Les foires et marchés étaient des lieux de rencontre et de sociabilisation où les réseaux se formaient. Cela pouvait être à la fois des zones de contact pour gros négociants, qui faisaient voyager les produits de loin en loin, mais aussi pour les revendeurs et colporteurs locaux. Ceux-ci approvisionnaient ensuite les villes et villages environnants.

Ces échanges expliquent la forte mobilité de nombreux hommes d’affaires, rompus au commerce à longue distance, aux taux de change et aux poids et mesures, ainsi qu’aux frais de douane divers. La batellerie était importante, beaucoup plus fréquemment utilisée que le transport par voie de terre. Ceci malgré les conditions de navigation parfois difficiles, souvent dangereuses.

## Références

Alexandre-Bidon, Danièle, Lett Didier, *Les enfants au Moyen Âge*, Paris : Hachette, 1997.

Neveux François, *La Normandie des ducs auxs rois Xe-XIIe*, Rennes : Ouest-France Université, 1998.

Sadourny Alain, « Les transports sur la Seine aux XIIIe et XIVe siècles », dans *Actes des congrès de la Société des historiens médiévistes de l’enseignement supérieur public. 7e Congrès*, Rennes : 1976, p.231-244.
