---
date: 2014-08-15
abstract: 1155. Alors qu’ils traversent des étendues sauvages en Auvergne, Ernaut et Lambert font une rencontre peu amicale. Sans même avoir atteint les lointains rivages de l’Outremer, la nature se rappelle à eux de façon brutale. La main de l’homme n’est pas encore maîtresse de tous les espaces, malgré la fébrilité de son activité en ce douzième siècle florissant.
characters: Ernaut, Lambert, Aubert d’Auvergne, Bénech, Bénédite, Gui, Loubeyre
---

# Les trois loups

## Apchon, fin d’après-midi du mercredi 25 juillet 1156

La pierre des murets du chemin était mangée de mousse, de joubarbe, de capillaire et de lichen, et de hautes herbes frémissaient aux abords, dans l’attente d’être fauchées. Agrippé à un massif rocheux surplombant le plateau, un imposant château s’élançait à l’assaut du ciel. Forteresse de moellons sombres et de mortier solide, ses courtines déchiraient les nuages.

Dans son ombre, à ses pieds, un hameau se blottissait entre deux ressauts du terrain. Le village était animé, visiblement florissant, et de nombreux artisans tenaient leur étal sur la place jouxtant la robuste chapelle, autour d’un couderc. Après avoir laissé leurs montures près d’un abreuvoir, Ernaut et Lambert se dirigèrent vers une bâtisse de bonne taille, aux portes grand ouvertes.

Le maréchal-ferrant était présentement occupé à parer le sabot d’une vache avant d’y poser un fer. Un jeune valet nettoyait les abords, un balai de genêt à la main. Lambert salua, dans un dialecte mélangeant son bourguignon natal et le peu d’occitan des montagnes qu’il connaissait. Il n’était pas aussi à l’aise qu’Ernaut pour apprendre les langues. L’artisan répondit poliment, mais brièvement, attentif à son ouvrage. Il était gris de poil, avec une belle moustache et une barbe courte. De grands yeux bruns, francs et volontaires, un sourire amical aux dents irrégulières ornaient un visage lourd. Sa silhouette, empâtée, trahissait l’homme aisé, malgré des mains et des épaules de travailleur. Il remarqua immédiatement les croix cousues sur les vêtements.

« Nous avons usage d’accueillir des hôtes, je peux vous offrir de dormir en mon fenil si vous le souhaitez. Nous serons heureux de faire héberge à des marcheurs de Dieu pour la nuitée. J’ai nom Aubert.

— Grâces vous en soient rendues, je suis Lambert et voilà mon frère Ernaut. Nous serions heureux d’apporter de quoi compaigner le pain en ce cas. Il nous reste fromage et biscuits.

— Foin de cela, gardez vos provendes pour mauvais chemins. J’ai celliers garnis bien assez pour nourrir le pèlerin. »

Il détailla Ernaut de la tête aux pieds et ajouta, en souriant :

« Même s’il a taille d’un ours et semble capable d’avaler génisse et son veau pour le souper ! »

Il invita Ernaut et Lambert à le suivre dans la demeure jouxtant la grange. Une robuste bâtisse, solidement assemblée, avec un étage coiffé de chaume. Des volailles gambadaient aux alentours et un chien de belle taille dormait sur le seuil. Aubert poussa la porte et disparut dans l’obscurité. Des voix d’enfants l’accueillirent tandis que, d’un geste, il incitait ses hôtes à pénétrer. La vaste pièce était occupée par une table imposante, des coffres et un foyer dans un angle.

Assise sur un banc, une femme était affairée à découper des légumes et une tranche de lard. Des jouets épars sur le sol étaient manipulés par une demi-douzaine de gamins dont le plus vieux n’avait pas dix ans.

Aubert présenta sa femme Bénédite et, d’un geste, proposa de s’installer pour « ôter la poussière du chemin ». Puis il se lança dans d’interminables questions sur les voyageurs, d’où ils venaient, ce qu’ils faisaient. Il était aussi curieux que disert sur lui-même et, lorsque le valet rentra après avoir rangé l’atelier, il semblait à Ernaut qu’il connaissait Aubert depuis des années. Bénédite était également accueillante, quoique bien plus discrète, n’ayant guère le choix de toute façon vu la faconde de son époux. En outre, le jeune pèlerin avait sympathisé avec Gui, un des gamins, et il le ravissait avec ses réflexions joviales et ses anecdotes étranges. Le jour tirait à sa fin, les ombres grandissaient à l’extérieur lorsqu’Aubert remarqua que sa femme avait enfermé les poules à l’abri. Il alla chercher le pain et y découpa quelques généreuses tranches d’un canif bien affûté.

« C’est de mon grain, vous m’en conterez du bien. Fraîchement moulu de la moisson.

— Vous avez eu bonne année ?

— De certes, les silos sont emplis et le courtil donne bien. Je vois là bonne saison qui commence, si les orages ne viennent tout gâcher. »

Sa femme distribua une écuelle et une cuiller à chacun, et une généreuse portion de soupe rehaussée de lard y fut versée sur le pain émietté. Les plus jeunes eurent droit à un surplus de lait crémeux. Aubert apporta un pichet de vin avec fierté.

« Il vient d’un coteau de Galvaing, un mien voisin. Moi j’y entends rien à la vigne. »

Le breuvage parut bien léger aux deux frères habitués aux capiteuses liqueurs de Vézelay. Mais ils firent néanmoins bon accueil à une boisson si généreusement partagée. Ils n’étaient pas toujours hébergés avec autant d’égards, se contentant souvent d’une soupe diluée servie dans une grange malodorante ou un appentis venteux. Lorsqu’ils en eurent fini avec le repas, ils s’installèrent tous à la porte de la demeure. Les journées étaient encore assez longues pour profiter de la veillée sans devoir se serrer près du feu. Les enfants étaient couchés et les discussions voisines résonnaient autour du couderc. Aubert triait des pois tout en devisant, tandis que Bénédite reprisait une chemise, à la lueur d’une petite lampe à graisse. Ils en vinrent à parler du départ.

« Si vous partez pour Saint-Flour, le mieux est par le Limon, mais c’est là terre à saignes[^saignes] et barthes[^barthes]. Soyez fort prudents et suivez bien le passage indiqué par les quirous[^quirou].

— C’est terre gaste[^gaste] ?

— Un peu, par endroit, mais il s’y trouvent chabannes[^chabanne] et hameaux donc ne craignez pas trop. Vous trouverez la terre de Diane[^dienne] en l’autre ribier[^ribier]. La voie est bien connue des charreyres, vous ne cheminerez pas seuls. »

Il se tut un moment puis, voyant que personne ne parlait, préféra ne pas laisser le silence s’installer.

« Je vous ai dit que, chemin faisant, vous pourriez aller faire visitance à la Font-Sainte ? Peu au-delà de Saint-Hyppolite ? On y prie belle statue de la sainte mère de Dieu que les comptours[^comtour] ont rapporté d’outremer justement… »

Ernaut s’étira lentement. La nuit serait courte après une telle veillée mais pour une fois qu’il n’avait pas à se contenter des vêpres suivies d’un repas silencieux au sein de religieux moribonds, il n’allait pas s’en plaindre. Il eut un sourire quand il s’aperçut que Lambert oscillait régulièrement sur son tabouret, les yeux papillotant de fatigue.

## Plateau du Limon, jeudi 26 juillet 1156

Le temps superbe s’était imposé au fil de la journée, et le passage du fond de vallée encore brumeux était désormais loin derrière. Ernaut et Lambert cheminaient tranquillement parmi les doux reliefs d’un vaste plateau qui rejoignait les paysages accidentés au sud, où pointaient de nombreux pics. Les cris des milans et le chant des oiseaux dans les broussailles avaient accompagné leur avancée. Ils croisaient régulièrement des paysans brandissant leurs grandes faux pour la fenaison. Plusieurs fois, des rémouleurs ambulants, colporteurs, charretiers, les avaient aiguillés sur les bifurcations à prendre, les sentes à éviter.

Ils arrivaient dans une zone où l’empreinte de l’homme se faisait moins sentir. Aucun muret n’était visible pour délimiter les parcelles, seules de rares croix ou des pierres crossées indiquaient les frontières. De nombreuses tourbières parsemaient les creux du relief. Les bois devenaient forêts, où nul ne pénétrait sans bonne raison. Il demeurait malgré tout de vastes étendues herbeuses, vers le nord, où des troupeaux paissaient sous la garde de vachers. Peu de moutons et de chèvres, mais pas mal de bovins, signe indiscutable de la richesse de ces lieux. L’Auvergne était bénie de Dieu, et sa paysannerie y prospérait malgré les rudes mois d’hiver qu’on lui prêtait.

« Nous ne saurons arriver à Saint-Flour avant la nuit, la route est trop fort escarpée, se lamenta Lambert.

— En ce cas, préfères-tu pousser au plus loin ou nous contenter de Diane? ou de Murat ?

— Les jours sont longs assez pour… »

Un sursaut de sa monture stoppa net Lambert dans sa phrase. Le pauvre cheval roulait des yeux alarmés en tirant sur ses rênes. Le cavalier avait bien de la peine à garder le contrôle, sans parler de rester en selle. Ernaut connut rapidement les mêmes difficultés, et les hennissements effrayés ajoutèrent à la panique générale.

Jusqu’au surgissement d’une forme noire, oblongue, vive et insaisissable. Un loup. Un énorme loup au poil hirsute, tout en crocs et griffes, la gorge crachant sa colère et sa haine des hommes. Il s’affola un moment parmi les jambes des chevaux puis bondit dans un taillis, aussi vite évanoui qu’il était apparu.

Sa disparition ne calma pas le cheval de Lambert, qui se mit à tourner en rond, entraîné par la main maladroite crispée à une des rênes. D’une ruade il se débarrassa de ce poids gênant et partit, en saut de puces alternant avec un galop désordonné, droit sur le chemin. Ernaut avait lâché ses guides et s’accrochait des deux mains à la selle, les jambes crochetées autour du ventre de sa pauvre monture telles les mâchoires d’une tenaille. Il rentra la tête dans les épaules quand il se retrouva parmi les branches d’un bosquet de noisetier. Mais il tint bon, et son cheval finit par se calmer.

Cherchant où était Lambert, il descendit rapidement, flattant l’encolure de la bête affolée tout en la menant par la bride avec autorité. Son frère était à terre, le visage contracté, et se massait la cheville droite. Ernaut se pencha vers lui.

« La peste de cette malebeste ! Ça va ?

— Je ne sais, j’ai grand dol qui me gagne la jambe. Rien de cassé néanmoins. »

Ernaut hocha la tête, soulagé. Il s’agenouilla vers son frère et l’aida à ôter sa chausse. La cheville présentait une marque rouge violacé qui s’assombrissait à vue d’œil.

« Demeure ici le temps que je trouve ton cheval. Il n’a pas dû aller bien loin. »

Il aida Lambert à s’assoir sur un tronc couché là par un ancien orage et s’éloigna, non sans s’être muni de son bâton de marche. Si jamais le loup repointait son museau, il pourrait bien lui en coûter.

Gravissant un talus, il plissa les yeux, s’efforçant d’apercevoir le cheval. Il ne voyait que la lande, les hautes graminées ondoyant sous le vent, les bosquets qui, peu à peu, se rejoignaient en une forêt cheminant sur les versants escarpés. À deux ou trois portées d’arc, il repéra un troupeau surveillé par plusieurs chiens et vachers, et se dit qu’ils avaient peut-être remarqué leur monture. Il prit donc dans cette direction, droit au nord, fouettant l’herbe de son bâton tout en avançant.

« Désolé l’ami, nul cheval par ici, il a dû s’égarer vers Diane ou au parmi des bosquets au sud.

— C’te maudit leu mène la danse depuis de nombreuses saisons. Un gros, dru de poil, noir comme charbon. De la semence de démon ! Il doit avoir sa tanière dans la forêt, mais nul ne l’a trouvée. »

Un des pâtres proposa de venir chercher Lambert et le cheval restant, pour qu’ils soient en sécurité avec eux. Et si Ernaut tardait trop, ils le mèneraient à leur hameau. Ils revinrent donc à plusieurs vers le chemin et expliquèrent la situation. Lambert n’était guère enthousiaste à l’idée de laisser son frère partir seul.

« Tu n’es pas louvier, Ernaut, et les terrains sont traîtres ici, Aubert nous l’a bien dit.

— Nous n’avons guère le choix, frère. Nous ne pouvons cheminer avec un seul cheval. Et la remonte nous coûterait bien trop cher.

— Tu parles de raison. Mais prends garde à toi.

— Ne t’enfrissonne pas, je ne serai pas long. Cette maudite carne a dû galoper un temps, jusqu’à ce qu’elle comprenne qu’elle était à l’abri. »

## Plateau du Limon, après-midi du jeudi 26 juillet 1156

Ernaut sentait sa gorge sèche et regrettait à présent son optimisme. Il n’avait pas pris de gourde et les rus qu’il croisait ne l’inspiraient guère. Le terrain ondulait régulièrement, abritant en ses creux des tourbières qui gargouillaient sous ses pas. Il prenait garde à ne pas se faire piéger et avançait prudemment, appelant la monture de Lambert de temps à autre. La lande de bruyère se transformait parfois en mer de graminées dansant sous le vent léger, cachant les reliefs, dérobant au guetteur le tracé des sentes.

Lorsqu’il parvenait à un bosquet de pins, depuis l’ombre bienvenue, il portait son regard au loin, cherchant à deviner la silhouette de l’animal. À plusieurs occasions, il s’était donné de faux espoirs, trompé par quelques chevreuils dont la robe évoquait la couleur alezan du cheval. Seuls les chants des grillons et le craquètement des geais répondaient à ses appels, ou le cri d’un milan en chasse qui tournoyait loin au-dessus de sa tête.

Il longeait la vieille forêt, là où les troupeaux s’aventuraient peu. Parfois il rencontrait les restes de cabanes grossièrement assemblées, anciens refuges pour des charbonniers, des bûcherons ou des tourneurs. Sur les pentes d’un de ces hameaux improvisés, il découvrit des groseilles et des myrtilles, une poignée de framboises. Il dévora avec plaisir les fruits, assis face au paysage qu’il surplombait. Devant lui, loin au nord un mont s’élançait vers les cieux brumeux, noyé de bleu dans la chaleur de l’après-midi. Partout autour, ce n’étaient que trouées et vallons, ressauts et collines sur les plateaux, vallée profonde qui l’en séparait. L’homme n’était pas à sa place ici, et la nature le faisait sentir aux voyageurs égarés, aux pâtres aventureux. Parfois le vent portait à ses oreilles le tintement des sonnailles des troupeaux gardés au loin. Il lui arrivait de discerner les taches beiges et brunes, rousses et noires des bêtes à cornes, parmi les nuances de jade, de céladon et d’émeraude.

Cela faisait un bon moment désormais qu’il battait la campagne, sans résultat, et il commençait à perdre patience. La chaleur et le chant des insectes lui donnaient la suée. En dehors des bêtes aperçues au loin, il n’avait remarqué aucune trace et se demandait si le cheval n’avait pas fui en direction des habitations. Il serait bien temps de le découvrir plus tard. Pour l’instant, il voulait être certain que le loup ne l’avait pas attaqué ou qu’une fondrière ne l’avait pas englué.

Profitant de l’ombre d’un bosquet de pin, il ôta son chapeau de paille et passa sa manche sur son front moite. L’odeur de résine et l’humidité lui faisaient du bien. Il soupira, demeura un long moment ainsi, les yeux mi-clos à regarder sans voir devant lui. Il se sentait mal à l’aise dans ce lieu. Ce n’était pas que l’absence d’activité humaine, l’hostilité d’un monde sauvage peu désireux de se faire domestiquer. Il discernait une présence à l’orée de sa vision. Jamais assez présente pour se dévoiler mais suffisamment pour inquiéter.

On lui avait conté tellement d’histoires de loup qu’il ne doutait pas que ce fut la male bête qui les avait mis en si mauvaise posture. Certains récits en faisaient un animal stupide, infiniment moins rusé que le renard. Mais tout le monde savait que c’était le roi des espaces sauvages. Certainement moins fort que l’ours puissant, au cri rauque, et moins discret que le goupil au museau pointu. Pourtant il demeurait le prince de ces lieux, et ne se privait pas de le rappeler à l’homme qui se croyait invulnérable.

Ernaut commençait à se demander si ce vieux loup n’était pas un changeur de forme, un de ces sorciers qui adoptaient l’apparence de la bête pour tourmenter à plaisir. Il sourit à l’idée, peu désireux de s’épouvanter tout seul. Il regarda le soleil, autour duquel moutonnaient langoureusement quelques nuages cotonneux. Il était temps de se diriger vers les hameaux s’il ne voulait pas se faire prendre par les ténèbres. Aventureux, peut-être, mais certes pas au point d’errer nuitamment en des lieux appréciés des démons.

## La Bedice, soir du jeudi 26 juillet 1156

La chaleur de la journée se sentait malgré l’heure avancée. Installés devant leurs modestes cabanes, les manouvriers qui avaient invité Ernaut et Lambert discutaient dans leur dialecte occitan tout en se partageant une soupe épaisse de gruau et de légumes verts, dans laquelle ils puisaient à l’aide de lambeaux de pain gris. Ils cuisinaient dans un appentis grossièrement couvert de branchages, appuyé contre les cases mitoyennes où chacun resserrait ses affaires. La plupart étaient célibataires et d’autres avaient laissé leur femme le temps de la saison, dans une vallée en contrebas. Ils venaient aider aux champs là où le travail était plus important que les bras qualifiés pour le faire.

La cabane qu’ils partageaient avec Bénech, leur hôte, n’avait que la porte pour ouverture. Épaulée à droite et à gauche par une case semblable, en pierre sèche, elle était couverte de chaume pelé envahi de mousse grise. Sur le sol, des paillasses réalisées avec des feuilles de hêtre, les « plumes de bois » constituaient les seuls meubles. Une perche accueillait le rechange, et deux-trois sacs les modestes biens de l’habitant. Une lampe à graisse fournissait une maigre lumière, empruntée au feu extérieur.

Appuyés au mur chauffé par le soleil, les deux frères goûtaient un peu de calme. La cheville de Lambert n’était pas cassée, et du repos suffirait à remettre les choses en place. Un des villageois avait réalisé un emplâtre et, le pied désormais enrobé de feuilles de chou, Lambert ne sentait plus la douleur. Il profitait de la quiétude de la soirée.

Dans le hameau, tout était paisible, la longue journée de travail était terminée et parfois des voix s’échappaient des courtils ou du couderc. Des silhouettes dansaient autour des lampes tandis qu’on s’aventurait aux latrines ou qu’on rentrait après une veillée chez un voisin. Il n’y avait guère d’habitants ici, simple lieu-dit où des colons venaient dans l’espoir d’arracher un peu de bonne terre à la nature hostile, malgré le froid hivernal et les marécages omniprésents. Ernaut se demanda un instant si les conditions seraient aussi difficiles une fois l’outremer atteint. Il avait l’impression que ces gens étaient encore plus pauvres que la famille de Droin le Groin[^voirporon].

Un des manouvriers les apostropha soudain :

« On se disait qu’il faudrait voir le Vieux Loubeyre, lui saura bien trouver vot’ monture.

— Il a bonne connoissance des lieux ?

— De certes, et surtout des leus ! Il chasse pour les seigneurs de Diane, pour les comptours… »

Les visages hochèrent gravement la tête, comme s’ils hésitaient à en dire plus. Puis un des hommes, aux joues mangées de barbe grisonnante, ajouta :

« Il arrive qu’il vende une patte à clouer sur l’étable, pour conjurer le démon. Mais d’aucuns disent qu’il serait un peu sorcier.

— Dame, renchérit un autre, à vivre comme les loups, il en est devenu un.

— Ou a toujours été l’un d’eux ! C’est des bêtes féroces, sans pitié entre elles. »

L’ouvrier le plus proche d’Ernaut lui donna un coup de coude.

« C’est un vieux machin, aussi âgé que les pierres que tu vois par les terres gastes. Il a usé de sorts pour enlever une donzelle de Murat. »

Ernaut en écarquilla les yeux.

« Il en a fait quoi ?

— Dame, à ton avis ? répliqua l’autre, la voix emplie de sous-entendus. C’était une vraie beauté en plus, une de ces filles dont les rois rêvent de voir ne serait-ce que la cheville.

— Des menteries tout ça, Raoux. J’ai encontré un Muratais qui m’a dit que la famille était d’accord. Courir sus le leu, ça nourrit bien, et la donzelle avait maigre dot, voilà ce qu’on m’a dit.

— M’ouais, pourtant frais minois fait dot assez ! »

Les hommes ricanèrent, émoustillés à la mention de jeunes beautés qu’ils ne voyaient que peu, isolés dans la montagne pendant de longs mois. Certains parmi eux couraient les filles quand ils rentraient chez eux, dans l’espoir de fonder une famille le jour où ils auraient assez épargné pour cela. Le voisin d’Ernaut se tourna vers lui et lui administra une bourrade amicale tout en se levant.

« Demain, je te montrerai comment trouver la cabane du Vieux Loubeyre. Si lui ne sait pas trouver ton canasson, personne le pourra ! »

## Plateau du Limon, fin de matinée du vendredi 27 juillet 1156

Le vent agitait les branches des bouleaux et des pins, faisait frémir les épais buissons de houx. Pourtant, le soleil ne semblait guère décidé à vaincre la couche de nuages laiteux. Quelques geais craquetaient en volant d’un arbre à l’autre, inquiets de voir un intrus dans leur domaine.

Ernaut avançait sur l’étroit sentier qui ondulait parmi les roches lisses, les hautes fleurs jaunes et les bosquets de petits genêts. L’endroit paraissait oublié de l’homme malgré le chemin bien tracé. Des pierres avaient été disposées dans les vallons humides, permettant de traverser sans risque les zones marécageuses. Il n’osait pas imaginer ce qui arriverait à un voyageur imprudent dans une telle région. Il était heureux que la route principale ait été jalonnée de quirous^[Voir les notes] dressés de loin en loin.

Devant lui, l’orée de la forêt marquait de ténèbres la limite des espaces où l’homme tentait de s’imposer. Il pénétra sous le couvert frais sans ralentir, soucieux de ne pas montrer ses réticences. Le vrombissement des mouches emplit bientôt ses oreilles et il devait faire attention à fermer la bouche pour ne pas gober par inadvertance un de ces satanés insectes. Quelques chants d’oiseaux, le bruit de pics à l’œuvre s’aventuraient parfois entre les troncs. Son bourdon frappant le sol souple, le crissement de son pied sur les aiguilles et la mousse lui semblaient résonner jusqu’au plus loin des reliefs, alertant les présences maléfiques de son arrivée. Les senteurs résinées, généralement agréables, lui irritaient le nez et il trouvait un air lugubre à toutes ces branches dévorées de lichen. On aurait dit la cendre recouvrant des os sans âge.

Le fracas d’une course précipitée le fit sursauter. Il s’efforça au sourire en pensant que ce devait être une biche ou un cervidé quelconque, encore plus effrayé que lui. Posant son bâton contre l’épaule, il déboucha sa gourde et s’humecta les lèvres. Cette fois, il avait pris de quoi se restaurer. Il croqua un morceau de pain dur et replaça le tout dans sa besace. Lorsqu’il entendit une voix rauque résonner derrière lui.

« Bien dangereuses pour pérégrin que ces voies ! »

Il fit volte-face et chercha qui avait prononcé ces mots. Il ne vit que des formes indistinctes, branches et feuillages. Puis ce qu’il avait pris pour un amas en contrebas s’anima.

Un petit homme, recouvert d’une épaisse chape velue s’avançait. Il avait les cheveux mi-longs éparpillés autour d’un crâne en partie chauve et une barbe hirsute. Bien que son visage ait l’air usé et fatigué, sa démarche était pleine de vigueur et ses mains respiraient la puissance. Il arborait une robuste ceinture où était enfiché un couteau à lame grossière, et un bâton ferré de trois pieds de haut. Il ne fit presque aucun bruit lorsqu’il s’approcha. Il exhalait une odeur forte, animale, et pas la simple sueur du travailleur. Aucun relent d’oignon ou d’ail, si fréquent. Il semblait être parfaitement à sa place, empuanti des miasmes de la forêt, vêtu d’un gris semblable aux troncs. Malgré cela, il souriait, de tous ses chicots marron, les yeux plissés, rendus invisibles sous les sourcils touffus.

« Es-tu Loubeyre ? »

Le visage de l’autre devint carrément amusé.

« Ai-je donc telle fame que les forains me connaissent déjà ?

— Je viens de la Bedice, on m’a dit que je te trouverai en ces lieux.

— Tu me cherches donc ? »

Il inclina la tête, frémissant des narines comme une bête curieuse.

« Et pourquoi donc, l’ami ?

« J’ai nom Ernaut, et mon frère et moi, qui cheminons pour Jérusalem, avons fait mâle rencontre avec un gros leu, qui a fort effrayé un de nos chevaux. Je lui fais donc chasse.

— Au leu ou à ta bête ?

— À mon cheval. »

Le sourire de Loubeyre devint carnassier et il se pourlécha lentement.

« Tu es homme de raison. C’est un grand vieux loup, rusé et perfide, qui ne se prendra pas aisément.

— Je n’ai que faire de cette male beste, retrouver le cheval me suffira.

— On va le pister. Tu allais par la sente des Quiroux ?

— Oui-da, nous avions franchi modeste gué peu avant. »

Le louvetier hocha la tête doucement et fit signe à Ernaut de le suivre. Il avançait d’un pas rapide mais léger, arrivant à se déplacer sans heurter la moindre branche, sans faire craquer la plus modeste brindille. Derrière lui, Ernaut se sentait comme un ours mal dégrossi. On aurait dit qu’un troupeau de bovins furieux pourchassait Loubeyre.

Une fois les traces de la bête fuyarde retrouvées, il ne fallut guère de temps au vieux chasseur pour la localiser. Elle paissait tranquillement dans un vallon, à l’abri d’un bosquet de hêtres, la selle sur le flanc et le filet perdu. Elle ne portait aucune marque de morsure et ne boitait visiblement pas. Un peu inquiète, elle hésita à l’approche des deux hommes mais en peu de temps, un licol improvisé lui était passé autour du cou et elle suivait docilement.

Le Vieux Loubeyre souriait de satisfaction.

« Je te l’avais dit, c’est pas un chevalier[^loupchevalier], ce leu. Il fait parfois du dégât, mais il sait que les chevaux, ça porte souvent des gens d’armes. Alors il les évite.

— Mille grâces de votre aide. Je ne sais comment vous mercier.

— Moi j’aurais bien une idée, et ça te coûtera qu’un peu de temps. »

Ernaut s’arrêta pour flatter le cheval et attendre que son compagnon en dise plus. Le chasseur tourna la tête de-ci de-là, comme s’il auscultait les environs, à la recherche d’indices qu’il serait le seul à percevoir.

« On va mener ta bête à mon hostel. Elle y sera en sauveté. Pis on ira faire un boulot où j’ai besoin d’un gars costaud

— Quel genre ?

— Ça va te plaire. On va se venger de ce démon. »

## Plateau du Limon, après-midi du vendredi 27 juillet 1156

Les deux hommes progressaient rapidement, courbés pour éviter les branches. Le louvetier s’abaissait parfois tellement qu’on aurait dit une bête se déplaçant à quatre pattes. Il avait échangé son bâton contre une lance de bon frêne, épaisse et courte, au fer large d’une paume, affilé comme un rasoir. Il s’arrêtait régulièrement pour renifler, écouter, indiquant d’un geste impérieux le silence à son compagnon. Puis il finit par se poser dans une sente et fit signe à Ernaut de s’approcher pour lui murmurer :

« Nous ne sommes plus guère éloignés de la tanière. On a pris au large, car le vent nous est pas favorable. Si elle nous sent trop tôt, la nichée aura tôt fait d’être vidée. Attends-moi là. »

Puis il rampa jusqu’à la crête, vague indistincte ondulant sur les bruyères mauves. Il resta un moment à étudier par-delà le relief puis redescendit.

« Tu me suis exactement, ne fais rien que je fais pas. »

Puis il s’avança avec rapidité en directement d’un bosquet de pins appuyé sur plusieurs gros rochers. Parmi leurs racines entremêlées de pierre, on devinait un puits de noirceur. Loubeyre s’assit à proximité et renifla un instant à l’intérieur, tendant l’oreille.

« Vérole, la mère a filé !

— Quoi ?

— Les petits sont là, mais la mère est dans le coin, à attendre qu’on se dévoile pour attaquer, j’en ai certeté. »

Il soupira.

« Bon, pas grave, pousse la grosse pierre qui bloque le passage, on va dénicher les oisillons tant qu’on est là.

— Et s’ils sont plusieurs à venir ? »

Ernaut n’était que peu motivé à l’idée de s’attaquer à des travaux de terrassement alors qu’une horde de loups risquait d’arriver à tout moment.

« Aucune chance. J’ai toujours cru que le vieux était seul, et pis j’ai découvert qu’il avait une femelle. J’ai mis du temps à trouver cette tanière, alors vu qu’elle est pleine, on va les zigouiller. Avant que la meute se forme. »

D’un mouvement de tête, il invita Ernaut à se mettre au travail. Une grosse pierre enchevêtrée dans les racines empêchait quiconque de pénétrer dans le tunnel d’où s’exhalait une forte odeur. Tandis qu’Ernaut s’efforçait de débloquer le comblement rocheux, il lui sembla entendre comme des glapissements. Plusieurs fois, il s’arrêta, le souffle court, et s’attira un regard courroucé du louvetier. Et, finalement, il parvint à faire rouler le bouchon, dans un grognement libérateur. Loubeyre se retourna, l’air satisfait. Il glissa son couteau hors de son étui et tendit sa lance à Ernaut.

« Tu vas demeurer ici. Peu de chance qu’ils viennent. Ils nous savent déjà là pourtant. Mais ils ont trop peur.

— Vous êtes sûr ?

— Tu n’as pas la taille pour aller dans ce fin boyel chercher les petits, alors on a pas le choix. »

Ernaut opina tandis que le louvetier plongeait les mains en avant dans le trou, sa lame devant lui. Il lui fit un clin d’oeil avant de passer la tête. Puis, enfin, les pieds disparurent complètement et seuls des glissements et râles d’effort parvinrent aux oreilles d’Ernaut. Celui-ci choisit de s’appuyer contre le talus, inquiet de se faire assaillir de dos par une bête courroucée de le voir s’attaquer à ses petits. Il savait ces animaux forts discrets et capables de s’approcher à quelque pas de vous sans un bruit. Jusqu’à ce que leur mâchoire se referme sur votre gorge.

À un moment des glapissements et des halètements s’échappèrent de la tanière mais il n’osa appeler Loubeyre, de peur d’attirer l’attention sur lui. Plusieurs fois, il lui sembla reconnaître la forme d’un loup dans les broussailles environnantes. Pourtant rien n’approchait et il n’entendait rien de particulier. Jusqu’à la voix du louvetier, par la bouche de l’antre.

« Tiens, jeune, attrape ces louveteaux ! »

Et Ernaut vit apparaitre, pêle-mêle, les cadavres ensanglantés des petits que le chasseur était allé traquer jusque dans leur tanière. Il en éprouva un pincement au cœur, loin de reconnaître de futurs terribles mangeurs d’hommes, démons à forme animale.

Loubeyre ressortit de là couvert de poussière et de sang, avec une vilaine griffure sur la joue et la main pleine d’ecchymoses. Il ne semblait pas y prêter la moindre attention et capta le regard d’Ernaut mais n’ajouta rien. Il s’épousseta tranquillement, nettoya le sang et détacha une ficelle de sa besace, avec laquelle il noua la demi-douzaine de petits cadavres. Nulle joie ne s’exprimait sur son visage malgré la tâche menée à bien. Il balança son fardeau sur son épaule, reprit sa lance et partit, sans un mot pour Ernaut.

## Plateau du Limon, soirée du vendredi 27 juillet 1156

Le petit feu de brindilles crépitait, les flammèches coloraient d’ocre les visages et les mains. Ernaut était tranquillement installé sur un tabouret au coin de l’âtre, face au vieux Loubeyre. L’un et l’autre regardaient la danse incandescente tandis que les braises chauffaient le ventre des oules pansues où la soupe mijotait.

Derrière le chasseur, dans l’ombre, un galetas de bonne taille accueillait trois garçons que leur mère, une jeune femme, s’efforçait de coucher. Les deux hommes prenaient de temps à autre une gorgée de vin dans le gobelet qu’ils tenaient à la main. Dans un coin de la pièce, accrochées à une poutre, les dépouilles des louveteaux oscillaient. Le louvier les regarda un moment puis porta ses yeux sur Ernaut.

« Difficile de les voir comme des démons, hein ?

— Je n’avais jamais vu de petits…

— Et des gros, en as-tu jamais vu ?

— De loin, on les entendait parfois. »

Le vieux secoua la tête.

« Tu sais pourquoi on les tue, jeune ?

— À cause des attaques ? Ils causent tant de dommages…

— Sornettes ! Quelques bons chiens et ils n’osent approcher. »

Il s’éclaircit la gorge un long moment avant de continuer :

« On jalouse leur liberté. Elle nous broie le cœur… »

Ernaut fronça les sourcils, se pencha en avant, interloqué. Le Vieux continua.

« J’ai pas toujours chassé. J’ai porté le fer et le feu pour le compte de puissants barons, qui en remontraient à pas mal de loups question sauvagerie.

— Pourquoi vous les chassez alors ?

— On me fout la paix, ici, et on me paie bien.

— Pourtant, vous ne les détestez pas, c’est ça ?

— J’ai peine à croire que Dieu aurait pas mis quelque qualité en une de ses créatures. Même chez l’homme, il en a mis, alors… »

Ernaut soupira, laissa son regard errer dans la modeste chaumière. La jeune femme du louvetier vint s’assoir entre eux, en silence. Elle déposa les écuelles sur un banc et les servit. Ernaut admira le profil doux, le visage fin. Sa fragilité apparente tranchait avec l’aspect hirsute de son époux. Mais ses mains étaient bien celles d’une travailleuse de la terre, aux ongles abimés et aux doigts calleux.

Pendant ce temps, le louvier avait découpé quelques longues lanières de pain épais. Le vieux en tendit une à Ernaut en ajoutant :

« J’ai entendu voilà quelques années un jongleur qui contait l’histoire d’un gars, un Breton, le Bisclavret. C’était un homme qui devenait loup. Tu en as déjà entendu parler ?

— De certes ! Les garous ! Les plus terribles des démons !

— Pas celui-là. Lui avait le cœur pur et c’est son épouse qui avait comploté pour lui nuire. À la fin il se fait reconnaître ses droits et le roi lui pardonne.

— Que voulez-vous dire ? Que certaines de ces males bestes ne sont pas méchantes ?

— Dame, que crois-tu ? J’aimerais voir mes trois fils, ces louveteaux, aussi braves et vaillants que les bêtes que je pourchiace saison après saison. »

Ernaut hocha la tête et déchira son pain dans la soupe, puis il attrapa sa cuiller et commença son repas. Pendant un temps, on n’entendit plus que les longs lapements et les bruits de mastication. Puis, alors qu’il sauçait le fond de son écuelle, Ernaut ne put se retenir :

« Vous savez, il se dit dans le hameau que vous seriez…

— Un meneur de loup ? Dame, je sais bien ! »

Il s’esclaffa et s’essuya la bouche dans sa manche.

« Il n’y a là que ramassis de crétins et familles d’imbéciles. Plus proches du bœuf que de tout autre bête, alors forcément, le loup…

— Vous en êtes un ? »

Le Vieux Loubeyre arbora un large sourire et avala un gros morceau de pain, l’air amusé, puis fit un clin d’œil.

« Si on te demande… » ❧

## Notes

Les longs trajets à l’époque médiévale, s’ils se faisaient sur des routes et des voies relativement fréquentées, comportaient leur lot de surprises et de problèmes. On n’était jamais sûr de la direction qu’on prenait, aucune signalétique précise ne jalonnant l’avancée. Il fallait donc se fier aux indications des autochtones et de ceux avec qui on partageait le chemin un temps durant. Le soir, même pour les pèlerins, l’accueil pouvait être problématique, sans parler de la météorologie. Les textes abondent d’anecdotes sur les aléas des voyages et on disait que nul ne pouvait se prétendre voyageur s’il n’avait pas connu l’inconfort d’une nuit dans un fossé humide.

Le rapport à la nature sauvage avait évolué depuis le Haut Moyen Âge, en ce douzième siècle, et partout l’homme apposait sa marque, domptait les forces qu’il avait subies depuis des siècles. On asséchait les marais, on déboisait, on défrichait et ensemençait chaque fois que c’était possible. Même les zones de moyenne montagne furent investies à l’année, profitant d’un redoux commencé peu après l’an Mil et qui durera jusqu’à la moitié du XIVe siècle. À partir de là de nombreux hameaux en Auvergne furent abandonnés en Auvergne sur les plateaux entourant le Puy Mary.

Si d’aventure vos pas vous mènent dans cette région, ne manquez pas d’aller visiter la Maison de la Pinatelle à Chalinargues, qui explique la domestication de ces zones par l’homme au fil du temps.

Si vous avez envie d’admirer les paysages qu’Ernaut et Lambert traversent lors de ce récit, vous pouvez en avoir un aperçu en parcourant le [sentier de découverte du Frau de Vial](http://www.tracegps.com/fr/parcours/circuit6058.htm), sur la commune de Ségur-les-Villas. Quant au chemin entre Apchon et Murat, sur le plateau du Limon, il existe toujours, c’est [le sentier des Quirous](http://www.puymary.fr/sites/www.puymary.fr/files/fiche\_individuelle\_rando\_le\_sentier\_des\_quirous\_v2013.pdf).

## Références

Ad. de Chalvet de Rochemonteix, *La maison de Graule. Étude sur la vie et les œuvres des convers de Citeaux en Auvergne au Moyen-Âge*, Paris : Alphonse Picard, 1888.

Collectif, *Voyages et voyageurs au Moyen Âge*, Paris:Publications de la Sorbonne, 1996.[En ligne], consulté le 23 septembre 2011, Adresse URL: <http://www.persee.fr/issue/shmes_1261-9078_1996_act_26_1>

Heath Sydney, *Pilgrim Life in the Middle Ages,* London, Leipsic: T. Fisher Unwin, 1911.

Mallouet Jacques, * Auvergne de nos racines*, Barembach : Éditions J.-P. Gyss, 1985.

Verdon Jean, *Voyager au Moyen Âge*, Paris : Perrin, 2003.

Webb Diana, *Pilgrims and Pilgrimage in the Medieval West*, London - New York: I.B. Tauris, 1999.

Harf-Lancner Laurence, « Bisclavret » dans *Lais de Marie de France*, Paris : Librairie générale française - Le livre de poche, Collection Lettres gothiques, 1990, p. 117-135.
