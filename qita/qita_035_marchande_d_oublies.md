---
date: 2014-10-15
abstract: 1149. Alors que les troupes européennes venues pour assiéger Damas repartent désappointées, Margue vit dans le casal de Baisan, accolé à la forteresse du même nom en Galilée. Mais le siège de la ville musulmane a rompu une trêve ancienne et les territoires chrétiens sont désormais exposés à une attaque du seigneur damascène Anur.
characters: Ancelin, Eberhard, Geoffroy, Hugues de Baisan, Margue l’Allemande, Père Martin, Robert l’Allemand, Thibaut
---

# Marchande d’oublies

## Baisan, matin du jeudi 24 mars 1149

Les mouvements de son compagnon sur la paillasse réveillèrent Margue. Recroquevillée contre le mur, elle enfouit la tête dans un moelleux coussin de laine pour ne pas voir le jour s’insinuant par les volets de bois. Ancelin bâilla bruyamment puis la secoua sans ménagement. La poussière scintillait dans les rais de lumière vive.

« J’ai déjà ouï l’père Martin sortir ses poules. »

Elle ne réussit qu’à marmonner sans émettre un son intelligible, mais se redressa néanmoins. Au bout de leur couche dormait à poings fermés leur enfant, Robert, petit garçonnet d’une paire d’années. Le seul qui ait survécu de tous ceux qu’elle avait eus jusqu’alors.

Elle soupira, étendit les bras, souriante. Le soleil étincelant mettait fin à plusieurs jours de pluie ininterrompue. Même si c’était excellent pour les cultures, elle était fatiguée de déambuler sous des averses. Et aujourd’hui, elle devait se rendre au château du seigneur Hugues. Il avait annoncé son retour, avec quelques attardés des grandes armées allemandes et françaises. Ils prévoyaient de faire route vers le nord, après avoir envisagé de participer à une vaste offensive sur Ascalon, au sud du royaume.

L’intendant avait mandé de nombreux domestiques pour organiser un impressionnant banquet. Cuisinière renommée, Margue se louait à chaque fois pour aider à préparer les mets. Pâtés, tourtes et oublies, venaisons, chapons et rôts n’avaient aucun secret pour elle. Ayant grandi dans la demeure du sire de Picquigny[^gormond], que ses parents avaient rejoint lors de sa venue outremer, elle était à son aise pour régaler nobliaux et damoiselles.

Elle s’étira comme un chat alangui, à plusieurs reprises, puis fouailla parmi son épaisse chevelure brune histoire d’achever de se réveiller. Son époux en était déjà à lacer ses souliers qu’elle était toujours en chemise, sous les couvertures. Il toussa puis cracha avant de se tourner vers elle.

« T’amèneras l’eau avant de partir. Pour le dîner, je m’arrangerai.

— Il se trouve du fromage frais encore, ainsi que du brouet que tu n’auras qu’à chauffer.

— Je sais pas si j’aurais désir de monter le feu à la vêprée. J’irai p’t-être chez l’Thibaut. »

Margue retint une grimace de déconvenue. Elle n’aimait guère ce célibataire, récemment arrivé au casal, qui semblait passer son temps à boire et jurer. On lui prêtait des mœurs dissolues et une fâcheuse tendance à s’emporter. Depuis qu’il le fréquentait, Ancelin était d’ailleurs devenu plus bourru, plus distant. Il ne levait guère la main sur elle ou le petit, sauf à de rares exceptions, mais il bougonnait de plus en plus fréquemment et parlait peu, si ce n’était pour se plaindre ou critiquer ses voisins.

Malgré tout, Margue sourit. Une journée au château, à préparer un repas de fête lui changerait les idées. Les quelques deniers qu’elle rapporterait lui permettaient de n’avoir à subir aucun reproche de son mari, bien qu’elle sache pertinemment qu’il détestait la voir quitter leur foyer. Il aimait que le souper soit prêt, au soir, le feu allumé et l’eau tirée. Rien ne l’emportait plus que de découvrir qu’elle avait pris du retard, à discuter avec les autres femmes, au lavoir ou au four.

Elle s’assit au bord de la banquette tandis qu’il finissait de lacer sa ceinture. Entre temps, il avait poussé la barre de la fenêtre et le jour, ainsi que la fraîcheur, pénétraient largement dans la pièce. Margue frissonna et se dépêcha d’attraper ses chausses, accrochées avec sa vieille cotte à la perche à la tête du lit. Elle avait cette robe de bonne laine depuis des années. Elle l’avait agrandie avec le temps de bandes de tissu de moindre qualité, tandis que son corps s’empâtait à chaque grossesse. Elle avait toujours eu tendance à l’embonpoint mais la maternité avait encore forcé le trait, et elle arborait désormais une silhouette opulente, hanches lourdes, cuisses larges et ventre tombant, ainsi qu’une poitrine qui ne manquait pas d’attirer l’œil des hommes.

Elle finit de s’habiller en démêlant son épaisse toison qu’elle noua avec habitude avant de la dissimuler sous une coiffe de lin grisonnant. Son époux était déjà sorti et le bruit qu’il faisait dans l’appentis voisin lui indiquait qu’il prenait les outils pour se rendre à leur courtil, au sein de la palmeraie irriguée. Le boucan qu’il faisait pour s’emparer d’une houe et d’une bêche ne manquait jamais de la surprendre. Ces bruits réveillaient presque chaque fois le petit Robert, qu’elle n’allaitait plus elle-même. Elle ne lui donnait plus le sein depuis quelques mois, son lait s’étant tari à la fin de l’automne. Une voisine l’avait aidée au sevrage, tandis qu’elle habituait son fils aux bouillies et gruaux. Le voir ainsi allongé, offert et paisible pendant son sommeil, lui arrachait toujours un large sourire. Il était de bonne constitution et avait déjà survécu à une crise de fortes fièvres. Doté d’un solide appétit, il grandissait et grossissait de façon correcte.

Comme chaque mère, Margue vivait avec la peur qu’une maladie ou un accident n’emporte son enfant avant qu’il ne soit adulte. Mais cette fois, elle sentait bien qu’il était robuste et vaillant. Le choix de son prénom n’y était pas pour rien, elle en était convaincue. En Normandie, on racontait aux petits les aventures du duc Robert, qui s’y entendait si bien pour berner les foules qu’on le surnommait le Diable.

C’était tout le mal qu’elle souhaitait à son enfant, de tracer ainsi son chemin dans ce monde ardu et farouche. Si Ancelin ne s’y opposait pas, elle envisageait de proposer au sire Hugues de prendre leur fils comme domestique, dans l’espoir d’en faire un sergent. Peut-être un de ceux qui portaient haubergeon et empoignaient l’épée. Au moins pourrait-il se défendre contre les nombreux périls qui assaillaient le commun. Mais, pour l’heure, avant d’en faire un grand guerrier, il fallait lui préparer son brouet du matin.

## Baisan, fin de matinée du jeudi 24 mars 1149

La cuisine était de proportion modeste, et on s’y frottait, s’y coinçait, s’y gênait lors des jours affairés. Assise sur un escabeau, près de la porte vers les réserves, la cellérière veillait, si pingre qu’on aurait cru lui tirer une pinte de sang à chaque mesure de farine qu’elle concédait.

Malgré cela, l’endroit était agréable, et la chaleur du vaste foyer était bienvenue en ce mois encore frais. Les odeurs de légumes, de jus grésillant, de pâtes levant, de fruits macérant, d’épices parsemées, de vin assaisonné se mêlaient en une éblouissante et appétissante symphonie. La seule exception à ce tourbillon de saveurs alléchantes demeurait la zone où l’on préparait les poissons. Corvée dont on se déchargeait sur les plus grognons, les moins habiles, les plus détestés. Ils devaient éviscérer, trancher, écailler, dresser les filets sans discontinuer, jusqu’à être imbibés à leur tour des senteurs de pêche.

Margue était occupée à tamiser de la fleur de farine pour les pâtisseries les plus délicates lorsque l’activité de la cuisine se ralentit soudain. Une des servantes à l’entrée, son panier de raves à la main, venait de demander le silence. Un cor résonnait depuis l’enceinte. Un appel, puis un second. La femme écarquilla des yeux affolés.

« Ennemis aux abords! »

En un souffle, la cuisine se vida. Même l’intendante, dans son empressement, oublia de pousser la porte du cellier avant de sortir dans la cour. Depuis les autres bâtiments, les serviteurs s’étaient aussi avancés, apeurés. Ils se blotissaient tous auprès de la tour centrale, bien que leur maître ne soit pas encore arrivé. Une poignée de soldats couraient le long des courtines. Le portail avait été clos rapidement et, depuis la vaste esplanade, Margue ne pouvait voir que les communs, le donjon et l’immense ciel gris pommelé, menaçant à tout instant de lâcher quelques gouttes.

Un des gamins affectés aux écuries grimpa sans demander l’autorisation jusqu’au rempart pour jeter un coup d’œil, vers le nord. Il cria de sa voix aigrelette :

« Une troupe d’infidèles sur la grand colline !

— Nombreux ?

— Guère, répliqua un des soldats. Mais trop peu pour n’être plus que des éclaireurs. »

L’absence de sire Hugues et la faible garnison présente semèrent l’inquiétude parmi la foule. Jusqu’à ce qu’un chevalier sortît de la tour. Les épaules larges, avec un cou de taureau et un air renfrogné, Geoffroy était un homme d’expérience, circonspect et posé. Sa simple apparition suffit à calmer les esprits. Il dévala les marches et traversa la cour sans un mot, la main sur son épée. Il lâcha un sourire forcé au fèvre avant de grimper sur le mur d’enceinte. Puis il donna des instructions aux quelques sergents. Un des soldats courut aux écuries, bientôt rejoint par des valets. Enfin il se tourna vers la foule amassée en contrebas.

« Quelques cavaliers qui se massent sur la colline au-dessus de l’Harrod[^harrod]. Je vais faire prévenir le sire Hugues, qui ne doit plus être fort éloigné. Ce peut n’être que des nomades qui espèrent faire leurs choux gras après le départ des armées de Louis et de Conrad[^conradlouis].

— Et les nôtres ? Ceux du casal ?

— Je vais faire ouvrir la porte pour ceux qui pourront nous rejoindre. Nulle inquiétude à avoir en pareilles murailles. »

Il sourit, puis se gratta le menton un petit moment.

« Je ne crois pas qu’il puisse s’agir de puissant ost. Belvoir nous aurait dépêché messager en tel cas.

— Sauf si les infidèles l’ont pris, hasarda une voix anxieuse. »

Geffroy s’emporta, et lâcha quelques postillons.

« Ne dites pas pareilles sornettes ! Il s’agit là d’un puissant castel, malaisé à forcer. Et qui a bien temps de lancer quelques coursiers prévenir à l’entour. »

Margue jetait de temps à autre un coup d’œil vers le portail, voir si son époux s’y présentait. Elle avait bien amené Robert avec elle, pour le confier à une amie servante au château, comme d’autres gosses. Avec la peur qui s’était répandue, chaque mère aspirait à tenir ses enfants dans ses bras. Elle savait qu’Ancelin n’était pas le genre d’homme à prendre des risques. Il lâcherait sa houe à la première silhouette turque pour s’enfuir à toutes jambes.

Ce n’était pas tant qu’elle l’aimait tendrement, mais elle s’y était habituée au fil des ans. Il n’était pas le compagnon dont elle avait pu rêver secrètement, enfant, mais il y en avait de pires. Il travaillait avec application, si ce n’était avec acharnement, et n’avait pas la main trop leste. Sans cette tendance grandissante à lever le coude, elle se serait estimée assez chanceuse. Suffisamment en tout cas pour espérer de lui qu’il arrive à la rejoindre au plus vite.

## Baisan, matin du vendredi 25 mars 1149

Chacun avait dormi comme il l’avait pu. Margue avait trouvé refuge avec son fils dans les cuisines. Elle y avait traîné une paillasse rudimentaire, et bénéficiait ainsi de la chaleur du foyer. Si le repas n’avait pas été aussi festif que prévu, au moins avait-il donné l’occasion d’un feu conséquent.

Sire Hugues était arrivé en milieu d’après-midi, avec une troupe de presque cinquante soldats, ce qui avait grandement remonté le moral des réfugiés. Entretemps, de nombreux habitants du casal avaient rejoint la forteresse, portant leurs maigres biens dans des sacs, menant leurs quelques bêtes afin de les mettre à l’abri. Tout ce qui ne serait pas serré entre ces murs risquait le pillage ou le feu. À la fin de la journée, la majeure partie des femmes était dans le château, ainsi que les hommes dont les terres étaient les plus proches. Au grand dam de Margue, Ancelin n’était pas de ceux-là. Leurs parcelles étaient parmi les plus septentrionales, vers l’occident.

Cela les mettait aussi les plus loin des soldats musulmans qu’ils avaient aperçus, mais n’offrait guère de chance de le voir arriver sain et sauf. Lorsqu’elle s’assit sur sa couche, Robert dormant toujours, Margue obtint un sourire sans joie de sa voisine, une vieille domestique édentée qui s’occupait des linges de sire Hugues. Elles se levèrent de concert et se rendirent dehors. Là, plusieurs avaient construit des cabanes rudimentaires de leurs maigres affaires, pour ceux du moins qui n’avaient pu trouver refuge dans un galetas quelconque. Margue constata avec soulagement que les remparts étaient régulièrement garnis de soldats, parfois affalés la tête sur les coudes appuyés au mur, à regarder au loin. Elle apostropha un gamin qui cavalait, une outre à la main.

« Quelles nouvelles de la nuit ?

— Y’a forte troupe qui passe dans la vallée au nord, mais y se sont pas arrêtés pour nous bouter fer et feu. Y z’ont peur de sire Hugues !

— Des pillards comme disait sire Geoffroy, alors ?

— Oui-da, semblerait bien »

Puis il décampa sans demander son reste. Alors qu’elle allait rentrer se chercher de quoi manger, une troupe pénétrait par la porte principale, à peine une demi-douzaine d’hommes bien équipés. Des Allemands qui étaient arrivés hier et un chevalier de l’ost personnel du seigneur de Baisan.

Elle s’approcha d’eux tandis qu’ils discutaient en descendant de cheval. L’un des croisés, un grand blond au nez camus, lui sourit, comprenant qu’elle venait aux nouvelles. Il lança ses rênes à un gamin et, ôtant son casque, s’avança vers elle. Il parlait avec un fort accent germanique malgré son vocabulaire plus que complet.

« Rien dans les parages…

— Les mahométans sont partis ?

— Ils ont continué par la vallée droit au couchant. Ils sont campés à quelques lieues amont.

— Dieu soit loué ! Nous voilà saufs !

— Je ne sais. Ils ont fourragé assez loin et vos hostels sont fort pillés. Ils n’ont pas laissé le moindre grain… »

Margue haussa les épaules.

« Nous ne sommes pas si fort éloignés de la moisson. Et il n’est rien que nous ne puissions reconstruire. Et des habitants du casal ? Sont-ils saufs ?

— Comme vous le voyez, ici même ! »

Margue sentit son cœur défaillir.

« Nul autre ?

— Nous n’avons encontré nulle âme.

— Et à l’église ? Certains y ont de certes trouvé asile ! »

Le soldat secoua la tête.

« Ils ont même emporté la sainte Croix qui s’y trouvait, après en avoir brisé les portes.

— Et vous n’avez ?… »

L’Allemand baissa le nez, désolé tandis que les yeux de Margue s’embrumaient.

## Jérusalem, midi du mardi 9 août 1149

Eberhard accompagna Margue jusqu’à la porte de Sainte-Marie des Allemands. La pierre couleur de miel rayonnait de chaleur et les parfums sucrés des jardins alentour embaumaient l’air lourd. Des enfants passèrent en criant, attirant l’attention de Robert qui aurait bien quitté les bras de sa mère pour les rejoindre. Un peu au nord, quelques commerces encadraient une taverne, devant laquelle traînaient toujours des ivrognes.

« Alors tu es acertainée, tu demeures céans ?

— Oui-da. J’ai eu commerce avec les frères de Saint-Jean qui ont boutiques à proposer. Je suis habile assez au fournil pour proposer pâtés et oublies. »

Il s’approcha d’elle, lui prenant la main.

« Je ne suis pas riche, mais ma famille possède du bien, chez moi, à Rothenburg. Nous pourrions y prendre commerce, comme tu en as désir ici.

— Je te mercie de ta proposition, mais je n’ai nul désir de franchir les mers comme je l’ai fait enfant. »

Elle caressa la joue rugueuse de l’homme, lui souriant avec chaleur.

« Il te faut repartir avec ton sire, et moi ma vie est ici…

— Je pourrais voir à prêter serment à un quelconque baron et demeurer.

— Devenir félon à celui que tu as servi jusqu’alors ? Je ne te le demande pas et ne l’accepterait point. Demeure féal, ainsi que tu l’as toujours été, mon bel ami. »

Elle déposa un baiser évanescent sur la pommette de son compagnon et s’écarta, à regret.

« Mes prières auprès du tombeau du Sauveur te porteront chance. »

Puis elle s’éloigna d’un pas qu’elle espérait assuré en direction du nord, vers la rue du Temple. Elle devait retrouver le frère hospitalier au change latin. Elle n’avait pour tout bien que ce qu’elle portait sur elle, dans ses baluchons.

C’était son mari qui avait prêté serment au sire de Baisan, et elle n’avait nul désir d’y demeurer après sa disparition. Elle avait vendu ce qu’elle pouvait, donné ce qui restait et avait pris le chemin avec les Allemands qui avaient souhaité se rendre à Jérusalem, avant leur départ.

Eberhard avait été un compagnon attentionné tout au long des dernières semaines, mais elle ne voulait pas finir femme à soldat. Et il y avait Robert, qu’elle espérait bien voir grandir en un lieu protégé et non pas déambuler sur les routes au milieu des soudards. Peut-être que les frères de Saint-Jean accepteraient de l’éduquer, d’en faire un lettré, qui sait ?

Elle retrouva le petit homme tonsuré dans l’échoppe qu’elle se proposait de louer. L’endroit sentait la poussière et l’humidité, mais il était assez spacieux et à proximité du four, ce qui lui faciliterait la tâche. Le clerc dévorait à belles dents une oublie bien garnie, dont le jus coulait sur ses doigts. Il l’aperçut et sourit, dévoilant une partie de son déjeuner :

« Ah ! Notre amie l’Allemande ! » ❧

## Notes

Le peuplement des zones rurales des territoires latins du Levant se faisait beaucoup par des chartes qui garantissaient certains droits en échange de redevances. Néanmoins, de façon à attirer les populations, celles-ci étaient souvent bien moins lourdes qu’en Europe. Lors de l’installation, les chefs de famille prêtaient généralement serment auprès du propriétaire des terres. Certaines coutumes d’alors sont parvenues jusqu’à nous, parfois partiellement.

Il faut concevoir les foyers comme des unités de vie et de production, et leur constitution ne nécessitait pas forcément l’amour des conjoints. Des considérations économiques entraient en ligne de compte : il était essentiel d’avoir suffisamment de bien pour garantir la survie du groupe. On a donc longtemps dépeint ces couples comme basés sur la raison plus que sur le cœur. Pourtant, on découvre dans les sources suffisamment d’exemples qui prennent à contrepied cette image d’Épinal.

Non pas que tous les mariages (qui étaient alors des unions essentiellement civiles, l’Église n’ayant pas encore complètement intégré cette cérémonie dans ses sacrements) aient tenu compte de l’avis des deux époux, mais il ne faut pas les caricaturer à l’inverse. Par ailleurs, les sentiments envers les enfants étaient réels et profonds, comme ils peuvent l’être aujourd’hui, malgré la très forte mortalité infantile et l’espoir de l’aide de leur force de travail pour les vieux jours.

Les oublies sont une pâtisserie du genre des crêpes, et leur nom est à rattacher à la même racine qu’oblat et oblation, *don*.

## Références

Ellenblum Ronnie, *Frankish Rural Settlement in the Latin Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1998.

Schaus Margaret (éd.), *Women and Gender in Medieval Europe, an Encyclopedia*, Routledge, New York & Londres : 2006.

Schnell Rudiger, Shields Andrew, « The Discourse on Marriage in the Middle Ages » dans *Medieval Academy of America* 73, 1998, p. 771-786.

Sheehan Michael M., *Marriage, Family, and Law in Medieval Europe: Collected Studies*, Toronto: University of Toronto Press, 1996.

Rey Emmanuel Guillaume, *Les colonies franques de Syrie aux XIIe et XIIIe siècles*, Paris : A. Picard, 1883.
