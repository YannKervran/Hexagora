---
date: 2012-11-15
abstract: 1142-1146. Rêver de nouveaux horizons a toujours été l’apanage de la jeunesse. Mais un monde ouvert, dont le centre était au loin, risquait d’exaucer au-delà de ses espérances de telles aspirations. Le jeune Ucs, fils de bonne famille paysanne de Haute-Auvergne, a de grandes aspirations dans la vie. Saura-t-il les suivre ? Apprenez en un peu plus sur celui qui sera amené à jouer un rôle important dans *Les Pâques de sang*.
characters: Ucs de Monthels, Peyre le Roulier, Ysane
---

# Là-bas

## Le Monteil, mercredi 4 novembre 1142

Le bruit des cognées résonnant sur les troncs emplissait le bosquet. Peu d’animaux avaient osé demeurer dans les environs tandis que les hommes dévoraient la forêt, élargissaient la clairière. L’air était sec, les visages rouges. Le sol craquait sous les pas lorsqu’un des bûcherons s’aventurait à l’ombre. Une poignée de cabanes rapidement édifiées abritait des artisans œuvrant le bois encore vert, transformant les billots en écuelles, les perches en manches, les grumes en poutres. Sous un ciel moutonneux, les bras s’affairaient, réchauffaient les corps dont l’haleine s’échappait parfois en fumée.

Près d’un char robuste, attelé de deux bœufs au poil roux et aux longues cornes en lyre, un petit groupe s’employait à entasser un chargement de rondins.

« Hardi compaings, voilà bonne chauffe que celle de ce bois. La première à l’abattre et le trancher, et une autre à le porter ! » lança la voix joviale d’un jeune homme à l’allure soignée.

Bien qu’il ne soit revêtu que d’une tenue de laine épaisse, grossièrement teintée et plusieurs fois reprisée, il se dégageait de sa personne une élégance innée. Sa chevelure bien coupée à l’écuelle et son visage avenant tranchaient avec les rudes faciès des valets qu’il dirigeait. L’un d’eux possédait d’ailleurs un nez qui tenait plus du champignon violacé, pas tant en raison de la froidure que des larges rasades de vin qu’il s’accordait avec générosité. Ses joues flasques et sa barbe miteuse n’annonçaient que superficiellement la crasse qui le caractérisait, compagne d’effluves dont on disait dans tout le Monteil qu’elles auraient fait reculer plus d’un blaireau. Néanmoins Peyre était un homme apprécié, vacher reconnu et roulier expérimenté. Sa réputation moqueuse était aussi connue que son odeur corporelle et son rire enthousiaste résonnait souvent parmi les cols tandis qu’il menait son attelage.

« Pour ma part, je préfère bonne vinasse qui échauffe le sang par en dedans ! La meilleure des flambées est celle qui me compaigne en ma gourde.

— Soit assuré que le père aura tiré quelques pichets de la réserve quand nous arriverons, Peyre. La mère aura aussi sorti provendes pour rassasier nos bouches.

— Je n’ai guère frayeur à ce sujet, Ucs. La tienne maisonnée est bien pourvue et n’hésite guère à ouvrir sa tablée. J’ai souvenance de plusieurs veillées à chauffer ma panse en votre ostel après bonne chère !

— Nous n’avons pas à nous plaindre, l’année a été bonne en notre domaine. Père a pleines réserves de toisons encore à faire filer. Nous avons de quoi faire œuvrer toutes les femmes de la vallée, je crois bien ! »

Le vieux bouvier sourit, flattant ses bêtes tandis que les hommes finissaient de charger quelques épais rondins et plusieurs souches noueuses. Une fois le tout correctement arrimé, Ucs s’avança vers les valets.

« Je vais vous laisser mener l’équipage jusqu’à l’ostel, j’aimerais surveiller ce que font les autres œuvriers. »

Ses compagnons sourirent d’un air entendu, ramassèrent leurs sacs et paniers et prirent le sentier du retour, s’épongeant le front de leurs manches, bavardant entre eux à propos de la foire à venir en la vallée voisine, auprès du château des comtours[^comtour] d’Apchon.

Ucs les suivit du regard un petit moment, jusqu’à ce qu’ils aient bifurqué dans une laie[^laie] dissimulée parmi les broussailles. Puis il emprunta à son tour le chemin du bois, se faufilant avec célérité entre les branches en direction de la berge de la Santoire. Il progressa ensuite vers l’aval, s’efforçant de ne pas se faire remarquer lorsqu’il s’approcha du gué, aux abords du hameau.

Plusieurs femmes étaient là à discuter, leur panier de linge à la main, les bras rougis par l’eau vive du torrent qui descendait du col de Cabre. Un pêcheur était occupé à relever ses nasses. Bondissant plus que marchant, Ucs traversa la Santoire avec légèreté, familier des pierres que chaque année les villageois remettaient en place après les crues du printemps. Quelques frênes s’accrochaient sur les pentes menant au petit hameau ramassé aux abords de la vieille tour qui veillait cette zone de la vallée. De taille dérisoire, ancienne et peu glorieuse construction, elle proclamait néanmoins la puissance de son propriétaire, seigneur du lieu, même si sa richesse n’était que toute relative, à proportion du modeste amas de cabanes pelotonnées contre les palissades de bois.

Tandis qu’il progressait, le souffle laborieux, il tendait l’oreille. Il se dirigeait vers une clairière légèrement pentue orientée vers le soleil, abritée des vents et un peu à l’écart des habitations. Là ne résonnaient aucune cognée ni aucun cri de travailleurs, mais les voix légères et enjouées de jeunes filles.

Aussi discret qu’un traqueur à l’affût, Ucs s’approcha et se fit plus attentif encore, espérant entendre une personne en particulier. Il n’eut pas longtemps à patienter et s’annonça alors plus franchement, un sourire charmant sur le visage. Les jouvencelles, occupées à coudre et broder, confortablement installées sur d’épaisses couvertures, s’esclaffèrent de le voir s’avancer vers elles. D’un geste de la main, d’un signe de tête ou juste d’un regard, toutes le saluèrent et l’une d’elles se leva, l’air faussement embarrassé et les joues en feu.

Vêtue d’une robe simple, mais d’une étoffe de qualité teinte d’un ton fauve mettant en valeur ses longs cheveux clairs nattés, elle souriait largement, creusant des fossettes sur son visage rond. Elle lui prit la main lorsqu’elle le rejoignit et chuchota, tout en l’entraînant à l’écart :

« Ucs ! Que voilà belle surprise ! Je n’espérais plus guère, depuis tous ces jours.

— Nous avons grand ouvrage à abattre à l’approche de l’hivernage, Ysane. Je ne peux m’éloigner pour long, car les valets prendraient vite leurs aises. »

Souriant à la jeune fille, Ucs lui vola un rapide baiser, qu’elle rendit, les yeux emplis de désir.

« Le temps m’a paru si long depuis ce dimanche. J’avais désir de t’enlacer, et de pouvoir baiser ces lèvres bienaimées. »

Le jeune homme hocha la tête et la serra contre lui, effleurant son dos à travers les nombreuses et épaisses couches de vêtements. Leurs souffles emmêlés se firent plus haletants, leurs gestes plus empressés et durant un moment plus rien n’exista pour chacun d’eux que ce corps avide de caresses pressé contre le leur, cette bouche insatiable. Lorsqu’ils s’écartèrent un peu, les yeux noyés dans le regard de l’autre, il leur fallut un peu de temps pour réaliser que des voix masculines s’étaient mêlées à celles des jeunes filles de la clairière. Ysane en reconnut une en particulier, emplie de colère et d’agacement. Elle repoussa Ucs doucement et lui sourit avant de chuchoter.

« Je ferai mieux de retrouver mes amies, Ucs. Mon frère est là.

— Il m’enfriçonne guère. Je lui ai déjà fait ravaler sa morgue.

— Il ne t’en déteste que plus fort. Il se pensait redoutable lutteur et tu l’as mis au sol si aisément !

— Il confond force et adresse, ton frère ! Ce n’est qu’un benêt empli de vent. »

Ysane soupira, l’air soudain affligé.

« De certes, mais il a l’oreille de père et je sais qu’ils me prévoient des épousailles avec des cousins du sire de Mercoeur… Il a peur de ce que nous pourrions faire avant cela. »

Elle eut un petit rire espiègle aux pensées qui la traversèrent. Caressant la joue de Ucs, elle s’éloigna lentement, un sourire tendre illuminant ses traits. Revenu à lui, le jeune homme ressentit soudain le froid, entendit le cri d’un milan au-dessus de la vallée. Il se frotta le nez, passa sa main sur la joue qu’elle avait effleurée. Puis il reprit le sentier qui menait vers la Santoire.

## Église de Dienne, dimanche 2 juillet 1144

Un vent léger balayait la vallée, faisant trembler les frênes du bosquet aux abords de l’église. Ucs était appuyé contre la porte de l’édifice et regardait d’un air absent le ciel où quelques massifs nuages immaculés glissaient lentement. De temps à autre, il reportait son attention sur le petit groupe de moineaux qui sautillaient aux alentours. Ce n’était que bien rarement qu’il se penchait vers l’intérieur du lieu sacré pour y suivre ce qui s’y passait. La plupart des villageois assemblés là n’étaient d’ailleurs guère plus assidus. Certains allaient et venaient en discutant, d’autres étaient assis, occupés à marchander avec un colporteur le contenu de sa balle.

Seule une poignée était attentive, massée auprès du clerc qui officiait à l’entrée de l’abside, leur tournant le dos. Parmi ceux-ci, Léon de Diane[^dienne], le puissant seigneur du lieu, se remarquait aisément à sa riche tenue de soie colorée, son port altier. Sa bedaine, imposante malgré sa jeunesse, trahissait son goût pour la bonne chère. Pour l’instant, il était en grande discussion avec son épouse, petite femme au visage guère attrayant, mais dont la chaleur et l’amabilité étaient renommées dans toute la vallée. Ucs était là pour voir si des voyageurs seraient intéressés par un guide pour franchir les plateaux, les cols.

Depuis plusieurs années il louait ses bras, ses jambes et sa connaissance des montagnes en accompagnant des marchands ou des étrangers fortunés par les chemins. Sa réputation était flatteuse et on louait à mots couverts sa vivacité à prendre le parti de ceux qui le payaient.

Récemment encore, il s’était violemment opposé à un sergent à l’entrée de Saint-Flour parce qu’il demandait un péage exorbitant à ses clients. La discussion avait dégénéré et les lames auraient vite été sorties si des paysans des alentours ne s’étaient interposés. Beaucoup de villageois du Monteil étaient d’ailleurs soulagés de savoir qu’Ucs avait trouvé de nouveaux endroits où laisser s’exprimer ses poings. Sa réputation de bagarreur n’était plus à faire. Et si on reconnaissait qu’il n’était que rarement à l’origine des altercations, et presque jamais le premier à frapper, beaucoup estimaient qu’il était un peu trop souvent impliqué dans de tels affrontements. Il semblait même y prendre goût, habitué à finir encore debout, et parfois éclaboussé d’un sang qui n’était pas le sien. En outre, il arborait fièrement à sa hanche une longue épée d’acier. Personne ne savait s’il était vraiment efficace avec, mais peu se seraient aventurés à le lui demander, craignant d’en subir la démonstration.

Des éclats de voix jaillis de la nef l’arrachèrent à sa rêverie. L’assemblée commençait à bruisser d’une bousculade. Il faut dire que la messe était l’occasion de parler affaires et plusieurs des paroissiens étaient venus avec des animaux, une poule pour certains, mais carrément un mouton pour d’autres. C’était autour d’une de ces bêtes que l’empoignade semblait s’envenimer.

« Fils à moine, tu voix bien qu’il a patte fol, cet ouaille !

— Miladiu[^miladiu]! Il était bien vif quand je te l’ai cédé.

— Tu l’avais celé au parmi du troupeau, que je ne le voie pas, sale bougre de rat vérolé… »

Les invectives ne semblant plus suffire aux deux compères, ils en étaient venus à se secouer l’un l’autre, attrapant la cotte élimée de leur adversaire d’une poigne mal assurée. L’officiant avait dû cesser la messe, cherchant à ramener le calme, sans aucun résultat.

Le seigneur de Diane, lui, assistait à la scène non sans une lueur joyeuse dans le regard. Les querelles n’étaient pas chose rare, et il savait qu’en cas de blessure, on aurait recours à son jugement. Et on le disait toujours prêt à lever une amende, à se faire payer son droit de justice. Pour l’heure, il semblait surtout amusé par les insultes salaces que les deux hommes se lançaient au visage, incapables de faire plus que se tirer et se pousser, le malheureux mouton affolé coincé entre eux deux.

Lorsqu’un couteau fut libéré de sa gaine, des cris de frayeur accompagnèrent le mouvement de reflux. Visiblement, l’un des deux était prêt à faire violence pour voir ses arguments triompher. Le prêtre avait beau agiter les bras comme un épouvantail, personne ne lui prêtait attention. Les deux paysans avaient de vieux contentieux à régler, héritiers de litiges anciens, aussi graves et sérieux que des abus de droits de pacage ou du bois traitreusement ramassé. Le sang allait parler et chacun suivait la scène, fasciné.

Mais le dénouement ne fut pas celui attendu, sinon espéré, par la foule. En quelques pas, Ucs était sur l’homme au couteau et le lui avait fait lâcher d’un coup sec sur le bras. Ne laissant pas à son adversaire le temps de réagir, il chassa la lame du pied et s’interposa, le regard sévère.

« Qu’as-tu en tête, le Peyre ? Saigner Geraud en pleine messe pour un vieux mouton mité ! »

Décontenancés, les deux bagarreurs échangeaient des regards circonspects, hésitant à passer outre le vigoureux arbitre qui s’interposait. On les frustrait d’une bonne empoignade, et cela les contrariait visiblement. Mais ils n’étaient pas prêts pour autant à se frotter à celui dont on disait qu’il ne craignait pas de tenir tête à un chevalier en armes.

« C’est pas ton affaire, Ucs. Il m’a robé, il doit rembourser.

— Face d’étron ! T’as qu’à mieux soigner tes bêtes. Elle était saine quand je te l’ai changée pour ta terre…

— La Paix ! tonna Ucs. Je me demande si cette pauvre bête n’a pas plus de cervelle que vous deux réunis. Il y a d’autres lieux et d’autres moments pour s’entrebattre ! Si vous n’arrivez à vous entendre, allez donc visiter un prud’homme. Il saura démêler le bon grain de vos criements. »

Le calme revint soudainement dans l’église. Brièvement on entendit au dehors des aboiements de chiens, le bêlement des troupeaux proches, le cri d’un rapace qui surveillait son territoire. Les deux paysans se jaugèrent un instant et s’écartèrent, l’air mauvais. Ils n’étaient pas forcément convaincus, mais s’accordèrent d’un regard pour liquider leur différend un autre jour. Maussade, Peyre ramassa son canif et s’éloigna avec son mouton, suivi de sa femme et de ses enfants.

Le prêtre toussa pour attirer l’attention sur lui et se prépara à terminer la cérémonie, sans que personne n’y prête le moindre intérêt. Tout le monde était tourné vers le jeune homme, qu’avait rejoint Léon de Diane en quelques enjambées. Souriant à Ucs, il le jaugea de la tête aux pieds en un rapide coup d’œil, s’attardant quelques instants sur le fourreau qui pendait crânement à sa taille.

« Tu es bien téméraire, l’ami. Quel est ton nom ?

— Je suis Ucs, du Monthel, fils d’Aymon.

— Le jeune Ucs ? Dont on parle tant durant les veillées ? Je m’enjoie de te voir ennemi de la bagarre. »

Ucs sourit d’un air bravache, tentant de se montrer plus assuré qu’il n’était face à un si puissant seigneur et répondit :

« J’ai appris à préférer le calme à la tourmente. Nul ne gagne à chercher noise à son voisin, et je regrette d’y avoir eu ma part en mes jouvences.

— Parole frappée au sceau de la vérité… »

Léon de Diane se gratta le menton un petit moment, un sourire mystérieux planant sur ses lèvres.

« J’ai toujours emploi d’hommes tels que toi, Ucs du Monthel. Si tu sais te montrer fidèle à ton seigneur, tu récolteras bonne moisson à mon service. Viens donc au castel un de ces jours, nous parlerons de cela. »

Le visage rayonnant, Ucs inclina la tête en acceptation, et salua par la même occasion. Il avait toujours rêvé de s’affranchir de sa condition de fils de riche paysan. Malgré tous ses biens, il n’avait pu prétendre à la jeune femme de ses pensées quelques mois plus tôt.

Sa vocation pour les armes était en partie née de son désir de devenir quelqu’un de valeur, qui n’aurait plus à subir la morgue des puissants, des seigneurs titrés. Mais il n’aurait jamais espéré qu’un homme aussi important que le comtour de Diane lui fasse l’honneur de l’inviter à se mettre à son service devant toute la paroisse assemblée. Alors que le noble rejoignait sa femme pour assister à la fin de la cérémonie, Ucs toisait les villageoises et villageois réunis là, le regard envieux, jaloux, parfois admiratif, voire craintif. Il sentait qu’ils le dévisageaient comme s’il n’était plus des leurs. Et cela le réjouissait.

## Le Monteil, jeudi 19 décembre 1146

L’écir[^ecir] qui balayait la vallée, soulevait la neige légère et l’amassait en épaisses congères contre les haies, les maisons, les troncs des arbres. Impitoyable, le vent cinglait le visage des voyageurs attardés. Levant vers le nord un nez rougi par le froid, Ucs toussa, retenant in extremis sa capuche qui allait être emportée par une rafale. Il venait d’installer sa monture dans l’étable familiale et n’avait que quelques pas à faire pour rejoindre le chaud foyer où les siens se tenaient. Comme à chaque hiver, une fois les volets colmatés de paille, ils attendraient patiemment que le temps s’améliore au coin du feu, échangeant les nouvelles avec des voisins, cousant, filant, fabricant des paniers, réparant des outils ou profitant juste d’un peu de bon temps.

Lorsqu’il poussa la porte après avoir frappé, toutes les têtes se tournèrent dans sa direction, inquiètes de voir ce voyageur bravant les intempéries qui osait entrer sans y avoir été invité. La chaleur le happa, en même temps que l’odeur forte, mélange de fumée, de sueur, de poussière et de potages mijotés. Il lui fallut quelques instants pour discerner les formes qui se tenaient là dans la pénombre, chichement éclairées par les flammes léchant les bûches.

Son père était déjà debout, le visage empli de joie. Ils étaient tous installés autour de l’âtre central, se chauffant les pieds, cherchant la clarté dans la chaumière barricadée contre les éléments. Ils plissaient les yeux, aveuglés par la vive lumière venant du dehors, accompagnée de vent froid et de quelques flocons aventureux.

« Le bonjour à tous, les parents ! » lança gaiement Ucs tandis qu’il battait un peu des pieds avant d’entrer et de refermer la porte derrière lui.

Les visages s’illuminèrent et, en un instant, le voyageur était entouré de ses jeunes frères et sœurs, invité à s’asseoir par sa mère, serré contre son cœur par son père.

« Que voilà bien folle idée d’aller par les chemins en ce temps, Ucs ! Le sire Léon est bien dur avec toi !

— Nous avions tous grand désir d’être de retour pour la Noël. Et le chemin n’est pas si mauvais plus au nord. Nous n’avons quasi pas vu de neige avant le Limon. »

Son père dodelina de la tête, peu convaincu.

« Tu ne m’ôteras pas de la tête que le Bon Dieu n’a pas voulu que le voyageur s’aventure au-dehors en pleine froidure.

— La paix, Aymon, le fils s’en revient à peine et tu le sermonnes déjà ! »

L’épouse embrassa son fils et l’aida à se dévêtir de son épaisse chape de voyage.

« As-tu appétit pour un fond de brouet ? Histoire de te chauffer par en dedans ? »

Sans même se soucier d’une réponse, elle s’élança pour touiller dans le pot qui mijotait au coin de l’âtre, indiquant d’un geste impérieux à un de ses enfants plus jeunes d’aller chercher une écuelle.

Ucs souriait, heureux de se retrouver parmi les siens. Il s’assit sur un tabouret et ôta ses chaussures mouillées, se frictionna les pieds. Son père avait relancé le feu, y ajoutant une belle bûche pour que les flammes donnent plus de clarté. Chacun avait repris sa place et attendait désormais qu’il leur conte ses dernières aventures, ou au moins rapporte les nouvelles les plus importantes du monde.

À chaque retour, le rituel était immuable : il savourait d’abord un rapide en-cas, ménageant l’intérêt de ses proches pour le récit qu’il allait faire. Ses yeux couraient de l’un à l’autre : ses deux petits frères, adolescents, installés sur un banc contre le mur d’entrée, façonnaient le bois pour en faire des cuillers, des manches. Ses trois sœurs, plus jeunes, assises à même le sol devant le feu parmi leurs poupées et leurs jouets lui lançaient des regards envieux. Sa mère, à droite du foyer, près du lit, reprisait des chausses d’un geste tranquille après lui avoir servi de la soupe fumante, avec une épaisse tranche de pain noir.

Derrière l’âtre, entouré de débris d’osiers, de branches entassées et de paniers en cours de fabrication, son père le dévisageait, les mains allant et venant d’elles-mêmes dans les entrelacs de tiges souples. À l’approche de Noël, les valets s’en étaient retournés dans leur parentèle et il ne restait donc que la famille dans l’ostal. Ce serait la première fois que Bertrande, l’ancêtre, ne leur conterait pas ses histoires à la veillée.

Grand-mère de son père, elle avait toujours égayé les soirées par ses récits. Ucs n’avait appris son décès qu’à son retour l’été dernier. Une fièvre de printemps avait emporté la vieille dame et il avait dû se contenter d’aller saluer la tombe fraîchement refermée aux abords de l’église. Il était néanmoins heureux de pouvoir passer une nouvelle fois les fêtes avec les siens, d’autant qu’il ne savait jamais ce que l’avenir lui réservait.

Le comtour de Diane, Léon, était un seigneur important et se comportait comme tel. Il visitait souvent ses proches, en particulier la puissante famille de Mercœur. Ucs avait aussi pris l’habitude de résider en une propriété ou l’autre de l’évêque de Clermont. Et, présentement, il revenait d’une assemblée du comte d’Auvergne, Robert[^robert3auvergne].

Ragaillardi par le potage, Ucs fouilla dans ses affaires. Il vérifia que sa courte cotte de mailles n’avait pas trop souffert de la neige, solidement roulée dans un sac de peau huilée pour l’empêcher de rouiller. Il n’était pas peu fier de ce vêtement, qui complétait son casque de fer, lui aussi emmailloté dans une poche graissée. Sa valeur à elle seule lui aurait permis de s’acheter de nombreuses bêtes, mais elle n’était pas vraiment sienne, confiée à lui par le seigneur qu’il servait. Il la portait avec orgueil, assuré de faire son effet auprès des jeunes filles et attirant les regards envieux des hommes condamnés à pousser la charrue, à mener le bœuf.

Il sortit une petite bourse et fit un signe de tête à son père, lui montrant qu’il avait l’intention, comme à son habitude, de lui donner une part de ses gages. Il avait encore touché une belle somme récemment, et partageait chaque fois avec sa famille. Leur prospérité à tous était déjà reconnue au village, mais depuis qu’il s’était fait sergent d’armes, elle n’en était que plus éclatante. Contrairement à la plupart des paysans, une partie de leur richesse était maintenant en monnaies sonnantes et trébuchantes, ce qui leur permettait d’acheter et de vendre comme des négociants de la ville.

Son père faisait désormais commerce de bêtes entre les vallées et prêtait parfois à des voisins impécunieux de quoi ensemencer leurs terrains, contre redevance rémunératrice. Ucs allait s’approcher pour leur parler à tous lorsque des coups légers résonnèrent sur la porte. Un des fils ouvrit et un garçon se faufila prestement à l’intérieur. C’était un des gamins des environs, le visage radieux, les joues rougies par sa course dans le froid.

« Le comtour Léon va à Jérusalem ! » cria-t-il d’une voix enthousiaste sans même saluer.

La stupeur fut générale à l’évocation de ce nom, à la fois si familier et si exotique : la ville au centre du Monde, là où les miracles du Seigneur étaient si fréquents. Ce fut le père d’Ucs qui réagit le premier.

« J’avais ouï parler que le roi de la France[^louis7] avait fait serment de s’y rendre. Il doit chercher à avoir grand ost. »

Tout en parlant, il se tourna vers son grand fils, le visage inquiet. Ucs hocha gravement la tête.

« Le seigneur comte de Clermont, Robert, ira avec le roi et le comtour Léon a également pris la croix.

— Tu veux dire que… souffla sa mère, soudain blanche de frayeur.

— Oui, la mère. Je vais prendre les chemins pour aller libérer le tombeau du Christ en la Sainte Cité. Je pars pour Jérusalem. » ❧

## Notes

La Haute-Auvergne du XIIe siècle était assez densément peuplée, au regard de la situation ultérieure. Les conditions climatiques favorables avaient permis l’implantation de nombreux villages sur les plateaux, en des lieux qui seraient désertés par la suite (avec la baisse des températures au XIVe siècle). De nombreuses familles nobles détenaient là des territoires assez importants, qui ne dépendaient pas du lien féodo-vassalique plus développé dans le monde septentrional. Beaucoup de nobles avaient hérité de leurs biens en alleux - en totale indépendance - (de même que certains paysans), et tenaient farouchement à leur statut, refusant de s’inféoder à d’autres.

Les relations qui s’instauraient entre les puissants et les petits propriétaires étaient donc bien différentes, plus complexes que celles qui unissaient le vassal et son seigneur au nord de la France.

## Références

Phalip Bruno, *Seigneurs et bâtisseurs. Le château et l’habitat seigneurial en Haute-Auvergne et Brivadois entre le XIe et le XVe siècle*, Clermont-Ferrand : Publication du l’Institut d’Études du Massif-Central, Collection prestige, n°III, 1993.

Ribier du Châtelet Jean-Baptiste de , *Dictionnaire statistique, ou Histoire, description et statistique du département du Cantal*, 5 volumes, Aurillac : 1852-1857

Sassier Yves, *Louis VII*, Paris : Fayard, 1991.
