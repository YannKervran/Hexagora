---
date: 2015-04-15
abstract: 1157. Sur la route entre Jaffa et Jérusalem, l’ordre de Saint-Jean s’occupe des pèlerins, par ses dortoirs et réfectoires, mais aussi avec ses forteresses. Dans les montagnes de Judée, les hommes en charge du lieu s’affairent autour du nouvel édifice, destiné à accueillir les blessés et les vieillards parmi les frères : Aqua Bella.
characters: Frère Chrétien d’Emmaüs, Père Daniel d’Aqua Bella, Frère Guillaume de Calanson, Frère Pons de Belmont, Frère Raoul
---

# Retraite

## Château Emmaüs, fin de matinée du jeudi 9 mai 1157

Frère Raoul descendit de sa monture, un petit âne au tempérament placide, avec aussi peu de grâce qu’il y avait grimpé. Sa large bedaine et son manque d’activité physique rendaient la tâche bien ardue, faisant de l’exercice un spectacle ridicule. Ce faisant, il nota avec agacement l’hilarité mal dissimulée des deux valets qui l’avaient accompagné avec le train de mules et les deux chameaux. Dans le nuage de poussière soulevé par ses efforts, il remit un peu d’ordre dans sa tenue et ajusta sa ceinture par-dessus la coule.

Il n’aimait guère sortir de l’enceinte de l’hôpital de Jérusalem, et encore moins de la ville. Néanmoins, ses futures attributions l’obligeraient à voyager fréquemment, ce qu’il déplorait à grands soupirs à chaque fois qu’il y pensait. Peut-être avait-on estimé parmi ses supérieurs que l’ampleur de son estomac ne seyait guère à un serviteur de Dieu humble et parcimonieux. Quelques mois passés à se tanner les fesses sur le dos d’un bourricot, le long des chemins empoussiérés de Judée allaient vite mettre bon ordre à cela.

Il souleva son chapeau de paille et s’épongea le front, coiffa sa chevelure tonsurée d’une main moite. Ses bajoues luisaient de sueur grasse, coulant dans son cou irrité par la rude toile de ses vêtements. La chaleur l’achèverait bien si sa foutue monture ne le précipitait pas d’abord dans un ravin. Faisant craquer son dos contusionné, il dodelina péniblement vers l’entrée principale du caravansérail accolé à l’église.

Le bâtiment s’organisait autour d’une cour ceinturée de galeries couvertes. Il y apprécia la fraîcheur, non sans frissonner du soudain changement de température, mais grimaça en y voyant un imposant groupe de pèlerins discuter bruyamment. Dans une langue inconnue de lui, en plus. Grommelant, il les dépassa, jetant un œil habitué quoique distant aux silhouettes harassées, aux épaules voûtées.

Une petite porte ouvrait directement sur l’endroit où il pensait dénicher le cellérier responsable du lieu. La pièce exiguë n’abritait qu’un tabouret devant une modeste table à tréteaux, des étagères couvertes de tablettes et de rouleaux poussiéreux, ainsi qu’une lampe éteinte. Mais pas d’hospitalier en vue. Il continua à avancer vers les cuisines, où il savait trouver quelqu’un à toute heure.

Une odeur de brouet s’en échappait, rappelant à l’infortuné voyageur que son repas du matin était bien loin. Plissant les yeux à cause de l’obscurité relative, il manqua de peu de renverser un moine qui sortait du lieu, se contentant de le heurter de son ventre moelleux.

« Mon frère ! échappa l’autre, surpris.

— La paix de Dieu, frère. Je suis Raoul, de Bethléem, et je m’en viens depuis le chef hospice, en la sainte cité.

— J’ai nom Chrétien, céllérier de ce lieu. »

Tout en parlant, il l’invita à le suivre d’un geste de la main.

« Nous n’avons certes pas de salle capitulaire, mais notre réfectoire sera tout aussi bien. »

L’endroit était petit, chaulé de frais, avec un imposant crucifix de bois peint accroché au mur, qui semblait surveiller les tables sombres et les tomettes de terre cuite. Cela sentait l’humidité, mais la lumière issue des fenêtres jouait dans la poussière, apportant un peu de gaieté sous la morne figure du crucifié. Frère Chrétien tira un banc et s’assit tandis que Raoul s’affalait tel une baleine, expirant comme soufflet de forge. Il s’essuya une nouvelle fois le front pendant que son compagnon envoyait quérir du vin et du fromage. Puis il se tourna vers son visiteur.

«Alors, quelles sont les nouvelles en la Cité ?

— Pèlerins et marcheurs s’en viennent, et nous ramassons toujours autant de miséreux. À croire qu’il s’en fabrique tout exprès pour nous chaque jour. »

Chrétien acquiesça, tout en accueillant le pichet, les verres et le plat. Il servit son frère en silence puis ajouta :

« Quelles nouvelles du nord ? De nos frères tombés ? »

Raoul secoua la tête ostensiblement, le visage renfrogné, déchirant un morceau de pain.

« Ce fut là tragédie comme seuls les Anciens en chantaient jusqu’à aujourd’hui. Tant de pertes ! Nous n’avions pas besoin de cela… »

Puis il se consacra à l’engloutissement exhaustif et minutieux de tout ce qui pouvait être comestible sur la table, sans plus rien ajouter. Il accordait néanmoins de temps à autre un regard méfiant au fils de Dieu accroché au mur, comme s’il craignait que ce dernier lui pique sa part. L’autre frère attendait, son verre à la main. Ce ne fut qu’après avoir avalé l’ultime bouchée, suivie d’un rot discret, que Raoul fit de nouveau entendre sa voix.

« J’ai pour tâche de vous demander de préparer les bois d’œuvre d’Aqua Bella. Nous allons les envoyer, comme beaucoup de choses, à Beit Gibelin. Vous n’en avez plus usage ici, de toute façon ?

— Non, ils sont remisés ici depuis plusieurs semaines déjà.

— C’est parfait, préparez-les pour un transport jusqu’à la Cité, nous ferons convoi d’ici peu.

— Quels projets pour Beit Gibelin ?

— Avec la prise d’Ascalon, nous devrions attirer colons en masse. Nous allons leur bâtir une église paroissiale pour inciter à cela. »

Frère Chrétien hocha la tête, enthousiaste, sans rien ajouter.

« Nous avons par mal heurt perdu moult provendes avec cette histoire de Panéas, alors je bats le rappel d’un peu partout.

— Il demeure de quoi bien œuvrer, je n’ai guère de craintes en cela.

— Puisse Dieu vous entendre, nous avons tant à faire. Et tellement à accueillir chaque jour !

— Il est vrai que cette année voit grande foule de pérégrins. Je ne crois pas avoir jamais autant accueilli de marcheurs de Dieu. »

Raoul siffla la dernière goutte de son verre et regarda avec dépit dans la cruche vide, avant d’ajouter.

« Je vous mène d’ailleurs les quelques draps que vous avez mandés pour Aqua Bella.

— Parfait, peu à peu, le cellier s’emplit. J’ai grande hâte de voir l’endroit accueillir nos premiers frères. »

Tentant d’avaler les traces d’humidité persistant dans son gobelet, Raoul hocha la tête. Il espérait d’ailleurs bien être un des premiers à bénéficier de l’hospitalité de l’endroit. N’avait-il pas l’âge de se reposer un peu ? Il n’était pas loin de ses quarante cinq ans après tout. Et s’il devait cheminer par les routes du royaume jour après jour, il ne faudrait guère plus de quelques semaines avant qu’il ne soit brisé de partout.

## Château Emmaüs, après-midi du samedi 11 mai 1157

Frère Chrétien ne remarqua pas l’hospitalier au sein de la foule des pèlerins qui sortaient de l’église. Les murmures de leurs conversations avaient éclipsé le chant des cigales et des grillons, et l’attroupement avançait bruyamment parmi les pins et les palmiers. Il les salua, de façon automatique, son panier à la main tout en se dirigeant vers l’accès au lieu de culte. Il avait cueilli quelques fleurs dans le jardin pour orner les autels de frais. Le sourire de son confrère le stoppa net, un peu confus.

« Je ne vous avais pas vu au parmi de la foule, frère.

— Nulle offense ! J’ai profité de mon passage ici pour aller faire oraisons. »

Frère Chrétien sourit. Il était toujours touché par les manifestations de foi simple, surtout parmi ceux de son ordre, où on rencontrait aussi pas mal d’ambitieux et de carriéristes.

« Vous compaignez ces pérégrins ? s’enquit-il.

— Non pas. Il se trouve juste que j’ai cheminé de concert un peu avec eux. Je m’en viens de Calanson, suite à vos demandes. »

Le cellérier écarquilla les yeux.

« Oh, mais en ce cas, vous êtes…

— Frère Guillaume, oui. La paix sur vous, frère. »

Un large sourire déformant désormais ses traits, Chrétien attrapa la manche de son compagnon et partit en direction de la porte de l’hôpital, obstruée par les pèlerins. Il échangeait régulièrement des denrées avec le cellier de Calanson, propriété de l’ordre située dans la plaine, sur la route de la côte.

Ils se faufilèrent parmi les marcheurs et traversèrent la cour. Chrétien confia ses fleurs à un jeune valet pour qu’il s’occupe de changer les bouquets puis invita son confrère à le rejoindre dans la petite salle où il conservait ses documents. L’air était chaud, empli de la poussière soulevée, et aucun vent ne venait rafraichir les nuques échauffées. La douceur de l’ombre leur fit donc grand bien.

En s’asseyant, Guillaume nota le pupitre sur lequel plusieurs tablettes étaient posées, la corne à encre accueillant une poignée de plumes, les rouleaux de papier entassés. Tout cela lui rappelait sa propre petite cellule. Il sourit, dévoilant une dentition irrégulière qui cadrait assez bien avec son faciès rustique. Il n’était certainement pas fils de nobliau confié à l’institution pour y faire carrière.

« Je fais chemin pour le maître hospice, je me suis dit que ce serait bonne occasion pour vous encontrer. Et voir de mes yeux le castel qu’on réserve aux anciens.

— Aqua Bella est quasiment fini, nous œuvrons à le meubler. Pour l’heure, je fais office de cellérier pour eux aussi, mais je ne doute pas qu’un frère sera spécialement désigné pour cela. »

Guillaume acquiesça en silence, le coude appuyé sur la table, le regard traînant ici et là.

« Vous faudra-t-il quelque chose de mes greniers ? Les récoltes sont assez bonnes et nous avons larges provendes.

— Si vous avez tiges de fraiche moisson, je ne dis pas non. Il va falloir faire paillasses assez pour garnir les lits. D’ailleurs ceux-ci sont en train d’être bâtis en la sainte Cité.

— Je pense que cela peut se faire. »

Frère Chrétien s’excusa, le temps d’aller régler un problème d’hébergement pour les arrivants. Il revint une cruche et deux gobelets à la main. Pendant ce temps, frère Guillaume s’était appuyé contre le mur et commençait à dodeliner de la tête.

« Auriez-vous désir de vous allonger avant vêpres, mon frère ?

— Je ne dis pas non, j’ai fort veillé ces derniers jours, et les voyages en pleine chaleur sont toujours éreintants.

— Des soucis en votre casal ? »

Guillaume soupira, attendant d’être servi en vin et d’en avoir avalé une gorgée pour répondre.

« Nous avons eu quelques troubles avec des soudards descendus de Panéas.

— Des Turcs ? s’inquiéta soudain Chrétien.

— Certes non. Des baptisés, comme vous et moi. Mais l’un d’eux, pris de boisson, s’est attaqué à une pucelle et l’a forcée. La pauvresse n’ayant personne pour la défendre à la cour du vicomte, il nous a fallu l’y représenter.

— Le coquin a été condamné?

— Oui et non. J’ai demandé, comme de juste, qu’il fournisse de quoi la faire nonette, qu’elle puisse laver cet affront sous le voile. Mais elle avait disparu entretemps…

— La pauvrette.

— L’homme a payé l’amende au vicomte et j’ai obtenu qu’on l’escouille, tout de même ! Mais il a fait appel à son baron, espérant grâce pour ses bourses. »

Chrétien renifla, décontenancé. Comment la justice pouvait-elle s’exercer si les condamnés pouvaient échapper aux sanctions par l’aide de leurs patrons ? Il serait néanmoins malaisé d’obtenir rémission de l’amende, les puissants ayant les doigts encore plus crochus qu’usuriers lombards. Mais payer quelques sous ne saurait effacer la faute, et l’enfant serait à jamais marquée du sceau de l’infamie, sans espoir de rédemption si elle fuyait le voile.

Il fut ramené à la réalité par le bruit du gobelet heurtant la table.

« Je ne suis que fils de vilain, et n’y entends guère à ces choses, mais cela me désole de voir que même notre ordre ne peut porter bon secours à une gamine.

— Vous savez ce qu’elle est devenue ?

— Aucune idée. Peut-être qu’un de ces soudards l’aura trucidée pour la celer dans un coin… Dans l’espoir que cela permettrait à son compère d’échapper au jugement. »

Chrétien se mordit la lèvre, ennuyé. Il était également trop souvent confronté au malheur, à la mort et la tristesse pour ne pas en être affecté.

« Les affaires de sang ne sont pas pour nous, mon frère. Laissons cela aux barons.

— C’est quand même dommage d’accueillir le voyageur comme nous le faisons et d’en oublier la protection des miséreux sans feu ni lieu. »

Il se renfrogna, se frottant le nez de sa grosse paluche avec une vigueur telle qu’il semblait vouloir l’arracher de son visage. Son compagnon lui posa une main amicale sur le poignet.

« Nous pouvons tout de même prier pour elle, frère.

— Ouais, prier, ça on sait faire. »

## Aqua Bella, matin du samedi 25 mai 1157

Les chaussures ornées d’éperons de frère Pons résonnaient sous les voûtes tandis qu’il appréciait d’un œil connaisseur le travail des maçons. La pièce était largement éclairée par les fenêtres au sud, et le soleil pénétrait à flot en cette belle journée. Il s’avança vers l’abside à l’est et en caressa les murs finement lissés. Puis, se grattant le cou, il se tourna vers le père Daniel qui était resté à l’entrée, admirant la salle dans toute son ampleur. Leurs voix résonnaient dans l’endroit vide.

« Voilà belle salle où prendre repos, mon père. Je n’avais pas fait visite depuis long temps, et je m’enjoie de voir si bel ouvrage.

— Nos frères apprécieront de certes le confort du lieu. Malades ou blessés trouveront leur place ici.

— Vous aurez quelque docte médecin pour veiller sur eux ?

— Nous avons un frère fort versé en chirurgie qui sera des nôtres, et des médecins du chef hospice viendront faire visite chacun à leur tour. Ainsi nous bénéficierons de leur science à tous. Le temps d’en trouver un qui nous rejoigne. »

Le chevalier hochait la tête en assentiment, déambulant tout en devisant. Il avait vu sortir l’édifice de terre au fil des semaines, des mois, depuis le château voisin de Belmont dont il avait la charge. Il s’était tout d’abord emporté contre ce projet, qu’il jugeait préjudiciable à son domaine, mais finalement, il comprenait l’intérêt d’un tel lieu, où les frères malades, âgés ou blessés, pourraient être soignés tranquillement. Un bel endroit où finir ses jours, à l’occasion, auprès des eaux gazouillantes qui surgissaient des sources environnantes, parmi les chênes et les pistachiers.

« Nul doute qu’il y aura usage de l’endroit avec Norredin qui s’est remis en chasse…

— De bien obscurs moments nous attendent, je le crois aussi.

— Obscurs, je ne sais. Il est acertainé que nous ne pouvons pas laisser ce païen fouler nos droits ainsi. Le roi va sûrement lever le ban pour aller bouter hors ce Turc. »

Daniel opina en silence. Il n’était pas de ceux qui avaient vu avec enthousiasme le développement de la fonction militaire au sein de leur ordre. Il était prêtre, un de ceux qui se sentaient le plus poussés vers une vie contemplative, comme un simple moine. Il avait d’ailleurs un long moment été tenté de rejoindre les frères de Bernard de Clairvaux.

Puis on lui avait parlé du projet d’Aqua Bella, un lieu de retraite et de repos, où un prieur serait nécessaire. Il avait accepté avec joie, trop heureux de concilier ses appétits de retrait du siècle avec la possibilité de demeurer hospitalier. Mais c’était avant de faire connaissance avec frère Pons, le châtelain de Belmont, forteresse voisine, également propriété de l’ordre. Lui était un fils de banneret, cadet sans avenirs qui avait vu dans le développement du bras armé des frères de saint Jean la perspective de faire carrière, lance au poing, garantissant son âme et son renom de concert.

« Il faudra me confier un ou deux de vos sergents quand ils seront là, que je les prenne en main une poignée de jours… »

Voyant le visage étonné du prieur, frère Pons afficha un sourire sarcastique.

« Ne vous enfrissonnez pas, je ne saurais en faire des chevaliers en si peu de temps. C’est juste pour leur apprendre les signaux à connaître, vu que nous sommes en vue les uns des autres. Cela pourra servir.

— Ne sommes-nous pas en sécurité ici, au cœur du royaume ?

— La flèche vole par-dessus n’importe quelle muraille, mon père. C’est la prudence, plus que la force qui fait de nous de bons soldats de Dieu. »

Le clerc se renfrogna. Il s’estimait un soldat de Dieu lui aussi, mais combattait différemment. Et certainement pas en versant le sang. Ce n’était pas là un rôle décent pour qui espérait apporter son âme pure au jour au Jugement Dernier, il en était persuadé.

« Vous comptez accueillir quand les premiers frères ?

— Dès que la consécration aura eu lieu…, soupira le prieur.

— La peste du patriarche ! Il fait encore problème ?

— Il use de la moindre miette de son pouvoir pour nous nuire. J’aurais vu bon signe d’avoir la messe solennelle au jour de la Pentecôte^[Le 19 mai cette année là.], mais cela n’a pas été possible. Il n’y a donc désormais plus d’urgence, mais j’ai espoir avant la saint Jean d’été[^saintejeanete]. »

Le soldat acquiesça en silence, laissant son regard admirer la pièce une dernière fois, avant qu’elle ne s’emplisse de toux, d’éructations diverses et d’odeurs de corps fatigués et abîmés.

« Puisse le Seigneur vous entendre. L’été se passera en selle, à pourchacier le païen, et nul doute que nous y gagnerons horions et plaies. »

En entendant cela, le prieur se signa, dans l’espoir que le Seigneur les épargnerait, à défaut de leur éviter les combats. ❧

## Notes

L’ordre de Saint-Jean de Jérusalem, appelé communément l’ordre de l’hôpital représentait une énorme institution, qui comprenait un vaste domaine s’étendant sur de nombreux territoires. La gestion s’en faisait de façon à assurer la mission d’accueil initialement prévue, à laquelle s’était ajoutée au fil des ans une tâche de protection des pèlerins. Pour aboutir à la création d’une branche armée, similaire à la milice du Christ, les Templiers.

Il faut se garder néanmoins de penser l’ensemble comme un monolithe, et des tensions existèrent tout au long de son existence entre les différentes tendances. On y rencontrait aussi bien des prêtres que des laïcs, des hommes de guerre comme de simples serviteurs manuels. On a heureusement conservé pas mal d’archives, grâce à la survie de l’ordre jusqu’à nos jours (désormais appelé Ordre de Malte), mais il demeure difficile de dépeindre exactement la vie au jour le jour dans les établissements qui composaient cette mosaïque.

Le long de la route de Jérusalem depuis la côte, Château Emmaüs (Abu Ghosh), Aqua Bella (Khirbat Iqbala) et Belmont (Suba) synthétisent bien en peu d’espace toutes ces tendances : accueil des pèlerins, forteresse pour protéger la zone, lieu possible de retraite (hypothèse proposée par Denys Pringle, que j’ai retenue). Pour ceux qui se rendent en Israël, les trois endroits sont encore visitables, les deux premiers en assez bon état, et au sein d’un parc naturel pour le second, où les sources sont mises en valeur. Le tout à quelques kilomètres du centre de Jérusalem.

## Références

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. I & II.*, Cambridge : Cambridge University Press, 1993.

Riley-Smith Jonathan, *The knights of St John in Jerusalem and Cyprus c. 1050-1310*, Londres : McMillan, 1967.
