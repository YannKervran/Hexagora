---
date : 2020-05-15
abstract : 1162. Karpis, soldat arménien au service du prince d’Antioche, est en garnison dans la forteresse frontière d’Harim. Vétéran des affrontements avec les forces d’Alep, il se voit une nouvelle fois contraint à vivre un siège éprouvant, en compagnie de recrues moins expérimentées.
characters : Karpis, Hugues d’Harenc, Renart, Guymar, Sevrin
---

# La vigie du Levant

## Château d’Harim, début de soirée du jeudi 15 novembre 1162

Les doigts collés de son torchis sommaire, Karpis tentait de boucher les courants d’air qui s’infiltraient par les volets de bois de la chambrée qu’il partageait avec quelques camarades. Grommelant face au très relatif succès de son entreprise, il finit par enduire d’une épaisse couche brunâtre pratiquement tous les rebords. Les hivers précédents il s’était contenté de bloquer l’ouverture de bouchons de paille, mais cette fois le vent froid qui descendait des plateaux orientaux s’immisçait partout depuis plusieurs jours.

« Nous n’aurons plus guère de lumière, mais au moins aurons-nous chaud pour dormir ! »

Il se rinça les mains dans un seau et s’essuya rapidement d’une touaille avant de se chauffer au brasero improvisé dans une vieille jarre. Il soupira en voyant le maigre tas de bois.

« Les bûches apparaissent pas là par magie, compères. Il faudrait s’activer à en porter suffisance… »

Nul n’osa lui répondre ni même le regarder directement. Ses compagnons avaient appris à respecter son statut de vétéran, mais aussi à connaître son humeur maussade. Jamais rien ne semblait le réjouir. Il s’installa sur son lit, sobre matelas de paille garni d’une couche de couvertures élimées. Le repas du soir approchait et il espérait que ce serait enfin une épaisse soupe d’orge, d’oignons et de lentilles, comme il en demandait régulièrement aux cuisines. Il n’était pas le seul arménien de la garnison, et chacun à son tour réclamait ce plat simple et roboratif qui leur rappelait à tous leur enfance dans les reliefs de l’Anti-Taurus.

« Karpis, on te mande en la basse cour ! »

La voix qui l’avait interpelé était celle d’un Latin, un jeune archer à la dégaine maladive, Renart. Récemment recruté, il servait un peu d’homme à tout faire aux anciens, acceptant sans broncher les tâches les plus humiliantes. Il savait que son intégration était à ce prix et qu’il lui serait loisible d’en faire autant lorsqu’un nouveau prendrait sa place. À l’encontre de ce qui se faisait habituellement, et pourtant sans se soucier outre mesure de la sensibilité de son prochain, Karpis n’aimait pas qu’on rabaisse par plaisir ou, pire encore, par conformisme. Il s’efforça donc de répondre sans colère malgré son agacement à devoir ainsi redescendre alors qu’il s’installait pour la soirée.

« Quelque urgence ? Ou cela peut attendre ? »

Le jeune haussa les épaules.

« Un vilain s’est présenté à la porte et déclare avoir important message. Mais il ne veut rien nous dire, à *nous*. Sevrin a pensé que tu saurais le faire parler sans qu’on ait besoin d’en appeler au sire Hugues. »

Karpis grogna. Les fellahs des environs , tout en acceptant de verser les impôts sans trop oser relever la tête, n’appréciaient guère les envahisseurs latins qui avaient pris possession des lieux. Les pauvres cultivateurs devaient renâcler tout autant face aux guerriers turcs et, lorsqu’ils avaient une requête, préféraient toujours s’adresser aux Syriaques, ou, à défaut, aux Arméniens, avec lesquels certains partageaient la religion orthodoxe. D’un signe de menton, il marqua son accord et entreprit d’ajouter une chaude veste fourrée à ses couches de vêtements. Puis il se coiffa d’un bonnet de laine épaisse. Enfin, il attacha son baudrier, symbole de son statut d’homme d’armes.

Lorsqu’il fut prêt, Renart avait décampé, et il prit donc son temps pour traverser les salles, courettes et galeries qui conduisaient en bas vers la porterie. La vaste entrée était maintenue fermée à toute heure et on ne pouvait s’approcher que par une mince poterne et un passage piéton. La cité d’Alep n’était pas si éloignée et on avait conscience d’être là comme une avancée en territoire ennemi, en deça du Pont du Fer[^oronte] qui menait à Antioche et ses puissantes murailles.

La cour était boueuse des pluies récentes et, sous le ciel plombé de fin de journée, la couleur avait déserté les lieux. La tête dans les épaules, quelques hommes s’étaient regroupés autour d’un fellah et de son gamin, pieds nus dans la fange. Ils accueillirent Karpis avec contentement. Sevrin, le gros rouquin qui commandait habituellement la troupe fit signe qu’on laisse l’Arménien s’approcher. Le paysan, en l’apercevant, fut visiblement soulagé et salua d’un air désolé. Il attendit l’accord du Franc et prit la parole dans un salmigondis essentiellement syriaque.

« Je suis heureux encontrer guerrier des Montagnes. Toi ne voleras pas Mu’adh !

— De quoi parles-tu ?

— Il est usage donner belle pièce d’argent à premier à voir soldats d’Alep. Fils à moi les a vus, proches ! »

Fronçant les sourcils, Karpis dévisagea le gamin. Dépenaillé, le petit était habillé de ce qui ressemblait à différentes couches de sacs, unis par la crasse et leur aspect mité. Jambes nues, il était crotté de boue jusqu’à mi-mollet et pourtant souriait de toutes ses dents d’un ivoire étincelant. Il ne paraissait avoir guère plus de 8 ou 9 ans.

« Qu’as-tu vu ? Et où ?

— Dans les terres nord, j’ai cherché chèvre enfuie. Force Turkmènes, pas nomades, soldats…

— Vers la route du Pont ?

— Vers Levant. »

Karpis traduisit pour Sevrin, qui trahit sa pensée en tordant la bouche.

« On n’a guère encontré de troupes là-haut depuis long moment. C’est peut-être juste brigandage…

— J’en serais fort heureux ! Avisons le sire Hugues sans tarder.

— Ouais, qu’il puisse mander patrouille dans le froid et la pluie. On va encore se tremper la couenne… J’aimerais bien y échapper cette fois ! »

Karpis acquiesça. Le châtelain, Hugues d’Harenc, était un homme prévisible qui appliquait toujours les mêmes stratégies. Il enverrait un petit groupe de cavaliers vérifier l’information, en y intégrant le sergent auquel il se fiait le plus, ainsi que l’autochtone dont il se méfiait le moins : Sevrin et Karpis.

Avant que ce dernier ne puisse s’éloigner, le fellah l’interpella respectueusement.

« Pas oublier, mon fils a gagné belle pièce en argent ?

— Quoi encore ? s’interrogea Sevrin.

— Il était de tradition de mercier d’un besant celui qui signalait le premier des forces armées ennemies. Il veut s’assurer que nous la lui donnerons.

— Peuh ! Argent de bon aloi pour lui et frimas et averses pour nous ? C’est le monde cul par-dessus tête ! »

Sans un mot de plus, Karpis sourit au fellah et emboîta le pas à Sevrin. Il veillerait à ce que le gamin ait sa pièce.

## Château d’Harim, soir du vendredi 16 novembre 1162

Le déluge furieux et incessant avait lessivé la terrasse où le bûcher du feu d’alerte était empilé et on n’en discernait les formes que grâce à la lueur rousse échappée par la porte donnant dans la tour. Indifférent à la pluie qui l’avait détrempé depuis son départ au matin, Karpis essayait de creuser un petit tunnel sous le tas de bois, de paille et de copeaux. Il espérait qu’en y versant de l’huile il pourrait ensuite arriver à lancer le feu malgré les intempéries.

Avec un tel temps, il n’était même pas certain que cela soit vu par les vigies à l’ouest et que le message soit répercuté jusqu’à Antioche. Par précaution, le châtelain avait décidé de lâcher des pigeons porteurs de missives en plus de l’alerte visuelle habituelle par flamme. Sentant les gouttes perler dans son dos, Karpis frissonnait malgré lui, s’écorchant les doigts tremblants et engourdis tandis qu’il préparait l’allumage.

Derrière lui, quelques sentinelles le soutenaient moralement ou lui lançaient d’inutiles conseils. Il leur avait ordonné de rester à l’abri, estimant ridicule d’être plusieurs à se tremper alors que lui l’était déjà, d’avoir chevauché depuis le matin. À grand-peine, il réussit à constituer une sorte de caverne, où il imbiba quelques copeaux d’huile avant d’y glisser une lampe, tirant la mèche pour en obtenir une longue flamme. Il espérait que la réserve de combustible serait suffisante pour initier le départ de feu.

Il se recula, jaugeant ses chances de succès aux arabesques de la flammèche qui oscillait dans sa cabane précaire. Puis il rejoignit le couvert, rappelant aux hommes de surveiller la petite lueur et de la remplacer si jamais elle venait à s’éteindre. Leur sécurité valait bien quelques lampes sacrifiées, estimait-il. Puis il descendit dans une quasi-obscurité, familier des recoins et des marches, ses pieds faisant un bruit de succion à chaque pas. Il sentait les gouttes se regrouper, s’échapper de ses vêtements, de ses membres, avant de frapper la poussière et la pierre d’un plic plic régulier.

Lorsqu’il souleva la portière d’épaisse toile qui donnait sur la grande salle, il faillit en suffoquer tellement l’air lui parut étouffant. Au milieu d’une myriade de lampes, le seigneur Hugues d’Harenc était occupé à parcourir des tablettes et des documents, assisté par un de ses clercs. Près du feu qui brulait largement, son épouse, ses enfants et quelques familiers jouaient aux tables, discutaient ou tiraient l’aiguille paisiblement. La vocifération du vent ne parvenait pas jusque-là.

Le châtelain leva la tête à son entrée et lui fit signe d’approcher.

« Prends donc un verre de ce vin chaud, tu m’as tout l’air d’un vieux chien abandonné sous l’orage ! Il ne serait pas bon que tu attrapes fluxion de poitrine en ces temps difficiles ! »

Puis il reposa sa tablette, prenant soin de la disposer à sa place, et rangeant d’un geste mécanique ce qui se trouvait devant lui.

« Le feu d’alerte est-il lancé ?

— Je l’ose croire, mon sire. Je venais en rendre compte. Mais l’humidité rend la chose malaisée et je vais remonter vérifier qu’il ne puisse céder sous l’assaut de la pluie.

— Certes. Certes. Tu agis de raison. J’ai de toute façon lâché deux autres oiseaux pour porter le message. Il ne faut guère économiser ces volatiles quand l’ennemi est à nos portes ! »

Karpis garda le silence, il avait vu de ses yeux les patrouilles du prince d’Alep, Nūr ad-Dīn, en trop grand nombre pour que cela ne soit pas une avant-garde. Pour autant, il trouvait que le châtelain s’agitait bien trop. Ils connaissaient les risques, à tenir une forteresse de frontière, et Karpis estimait dangereux de montrer de l’anxiété quand on devait diriger des hommes.

« J’ai vérifié les listes et inventaires demandés pour nos celliers. Nous avons bonnes réserves, et enserrés dans nos murs, nous n’avons guère de souci à nous faire. »

C’était là une déclaration que le châtelain d’Harenc s’adressait à lui-même, Karpis le comprit. Il se retint d’évoquer la prise de la citadelle une douzaine d’années plus tôt environ. Puis sa recapture par les forces franques même pas dix ans plus tard. Aucune forteresse n’était inviolable. Ce n’était qu’une question de temps et d’habileté. Mais il était habitué aux soliloques de son seigneur, plus destinés à lui-même qu’à son auditoire.

« Tu me confirmes de nouvel que l’accès au Pont du fer nous est scellé ? Quelque prompt coursier ne pourrait s’y faufiler ?

— Les Turkmènes ont rapides montures, meilleures que toutes celles que nous avons en nos écuries. Et leur premier soin sera sans doute aucun de se garantir que nul ne court à Antioche. Et au vu de leur avancée, je dirais que demain les principaux corps de l’askar seront dans la plaine, leurs fourrageurs et leurs patrouilles vaquant comme en leurs provinces. »

Hugues d’Harenc hocha la tête. Fouillant dans sa mémoire, il vérifiait s’il n’avait pas négligé un point ou l’autre des instructions qu’il avait reçues. Le légitime seigneur du lieu, Josselin de Courtenay, était absent, comme souvent, et il savait que le jeune baron prenait ombrage du moindre manquement, qu’il avait tendance à interpréter comme une insulte personnelle. Homme de guerre, Hugues était plus à l’aise à mener un conroi, maniant la lance et dirigeant les soldats lors des assauts qu’à naviguer dans les eaux troubles des conseils nobiliaires. Dans la bataille, les choses étaient simples, et les conséquences rapides. Le doute n’avait pas le temps de fouailler son âme.

## Château d’Harim, nuit du mardi 27 novembre 1162

La cuisine, où sommeillait à toute heure un feu léger, bruissait des échanges entre les hommes installés autour de la table habituellement réservée à la préparation des repas. Un marmiton à moitié réveillé veillait à maintenir un épais gruau chaud à toute heure pour les vigies qui, sans relâche, parcouraient les murs d’enceinte afin de surveiller l’ennemi. Depuis plusieurs jours, l’armée de Nūr ad-Dīn s’était déployée aux abords du tell, occupant le village voisin et disposant ses lourds engins de siège. Le pilonnage du massif d’entrée avait commencé en fin de matinée, marquant de ses chocs sourds et répétés le tempo vers leur inévitable reddition.

La nuit avait une nouvelle fois été accueillie par une forte averse et un déluge s’abattait désormais sur le pays, bouchant la vue depuis les murailles. Le seul réconfort de la garnison était de profiter de salles chauffées et d’abris solides tandis que leurs assaillants devaient certainement se contenter de cabanes précaires, en toile ou de branchages ramassés alentour.

Néanmoins, en l’absence de nouvelles, le châtelain avait décidé d’envoyer un messager. Il devait également informer d’éventuels renforts qu’il ne leur faudrait pas trop tarder, la citadelle n’ayant guère les moyens de tenir plus de quelques semaines face à l’armée d’un prince aussi déterminé.

Sevrin avait  désigné Renart  pour la mission et lui avait indiqué de se préparer en ce sens. Karpis avait alors invité le sergent à boire un coup, espérant le dissuader d’un tel choix. Mais le vieil entêté n’en démordait pas.

« C’est la coutume, compère. On ne va pas mander sochon[^sochon] quand un béjaune[^bejaune] est là à attendre de faire ses preuves !

— Fariboles, Sevrin, tu sais comme moi qu’il n’a pas une chance. Il ne connaît pas le pays ! Il ne découvre qu’à peine le castel et tu l’envoies errer dans la nuit noire, en espérant qu’il pourra trouver le Pont. Norreddin en fera son serf avant le lever du jour, c’est acertainé.

— Et quoi ? Tu le sais comme moi, ils sont là, au-dehors, tels criquets. Sortir, c’est aller à la mort ! J’ai bien triste rôle, de choisir qui tantôt goûtera du fer…

— Il en est d’aucuns ici mieux armés à cela, cul-Dieu ! Qui auraient au moins une chance !

— Hé quoi ? Aucun de mes compères ne serait enjoyé à l’idée de se jeter dans la tanière du lion…

— Je n’irais pas le cœur en joie, mais je le ferais si tu me l’ordonnais… »

Le rouquin fronça un sourcil, avala une gorgée, se suçotant les dents tandis qu’il réfléchissait. Il semblait chercher l’entourloupe dans le marché qu’on lui proposait.

« Tu es prêt à risquer ta couenne pour ce freluquet ?

— C’est surtout que moi j’ai une chance de passer. J’ai usage de ça, je connais cette terre et, si on me surprend, je peux toujours mentir. Lui aura les épaules libérées de son chef dès qu’on l’apercevra. »

Sevrin se gratta la tête, inspira longuement.

« Si tu tiens tant que ça, vas-y donc, bougre d’âne ! Moi je m’en lave les mains si tu y laisses ta peau. »

Karpis hocha le menton et se leva, les yeux rivés dans ceux du sergent.

« N’aie crainte, je serais bientôt de retour pour botter tes grosses fesses, le Celte ! »

Puis, sur un clin d’œil, il sortit de la pièce, se dépêchant de retrouver Renart. Il le trouva en train de ranger ses affaires dans son coffre, équipant un long chaperon qui le désignerait à la moindre sentinelle comme un franc.

« Sevrin a changé d’avis, c’est moi qui irais. »

Le jeune homme en resta coi, balbutiant sans arriver à s’exprimer. Karpis ne lui en laissa pas le temps.

« Tu es archer, c’est stupide de t’envoyer au-dehors alors que tu es utile ici. Moi je ne sais ni l’arc ni la fronde… »

Il sortit la clef qui ouvrait son propre coffre afin de s’équiper en prévision de son périple nocturne.

« Par contre, le temps que je descendrais la muraille par la corde, j’apprécierais fort que tu sois là, pour flécher quiconque risquerait de s’en prendre à moi ! »

## Château d’Harim, nuit du jeudi 29 novembre 1162

Transi de froid, Guymar allait et venait sur la coursive orientale, emmitouflé dans une couverture. Au crépuscule, la pluie s’était arrêtée et la lune jouait depuis lors à cache-cache avec les nuages effilochés, apportant par moment une lueur bienvenue pour observer les environs. Il était impatient d’aller se coucher, épuisé d’avoir participé au renfort de la muraille sud, affaiblie par les tirs répétés des engins. Toute la journée il avait porté de la terre, des cailloux, des gravats, afin de consolider le mur où de longues lézardes couraient. Et le repos avait été de courte durée avant qu’il vienne prendre son tour de guet.

Il y avait peu de chance que les ennemis décident d’écheler en pleine nuit du côté le plus haut, mais il fallait surveiller tous les abords. Au moins y avait-il quelque lumière lunaire intermittente désormais, qui permettait de discerner à plus de dix pas. Mais l’exercice restait pénible et résister au sommeil qui montait n’en était pas la plus petite des difficultés. Sans même parler de la peur de servir de cible à ces maudits archers turcs, capables selon les rumeurs de percer l’œil d’un oiseau en plein vol à plus de deux cents+ coudées.

Alors qu’il battait des pieds pour réchauffer tout en baillant, il lui sembla entendre un bruit au bout de la terrasse. Circonspect, il s’approcha prudemment.

« Souris ou quelque bestiole » se rassura-t-il.

Mais cela recommença, comme si un petit animal dérangeait le gravier de la plateforme. Pourtant Guymarl ne voyait rien. Écarquillant les yeux dans l’obscurité, il restait à bonne distance du parapet, soucieux de ne pas offrir de cible à un éventuel tireur. La nuit n’arrêtait pas ces démons, lui avait-on expliqué.

Il entendit de nouveau le bruit, plus près. Il fit un pas prudent… et reçut un choc sur le front, qui le fit reculer. Un instant, son cœur s’affola à l’idée d’avoir reçu une balle de fronde et d’en mourir, mais il se passa la main sur le visage. Il n’y sentit aucune blessure, ce n’était qu’un minuscule projectile.

À pas de loup, il vint plus près du rebord , risqua un œil dans le vide, pour n’y découvrir que ténèbres. C’est alors qu’il entendit un petit sifflement. Qu’il aperçut du mouvement parmi la noirceur. Il appela, doucement.

« Qui va là ?

— Chut ! C’est moi, Karpis, envoyez une corde ! »

En peu de temps, le messager fut hissé par quelques hommes rapidement réveillés et il se retrouva à l’abri des murs, essoufflé, mais souriant.

« J’ai bonnes nouvelles, compères. Une forte armée arrive et ce diable de Norreddin lève le camp ! » ❧

## Notes

La période médiévale invoque immédiatement en nous des images de forteresses assiégées. Il est néanmoins difficile d’en évoquer la réalité matérielle, tant les pratiques et les conditions variaient énormément d’un lieu à l’autre, d’un moment à l’autre. Durant les croisades, les citadelles étaient généralement intégrées à un habitat (ou l’incorporaient). Au final ces forteresses se retrouvaient souvent garnies d’assez peu de défenseurs et comptaient sur une armée de campagne pour être délivrées. Le renseignement, les informateurs étaient alors essentiels, ainsi que des moyens de communication afin de prévenir ses alliés de la situation dans laquelle on se trouvait. L’usage de pigeons ou de feux est avéré dans les territoires musulmans, j’ai donc estimé qu’il était probable que les Latins en soient dotés également.

## Références

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Rogers Randall, *Latin siege warfare in the twelfth century*, Oxford : Oxford University Press, 1997.
