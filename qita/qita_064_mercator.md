---
date: 2017-02-15
abstract: 1157. Guido, riche marchand pisan installé dans le fondaco d’Alexandrie voit ses opérations commerciales gravement compromises par les autorités franques du royaume de Jérusalem. Dans son univers de négoce, saura-t-il faire les bons choix ?
characters: Guido Scudari, Elijah ben Moses, Jahm al Aswad, Muqatil ibn Sa’id al-Arabi, Francesco Caetani, Jaffa al-Akka, Henri le Buffle
---

# Mercator

## Alexandrie, fondaco des Pisans, matin du lundi 1er avril 1157

Les pas feutrés du serviteur venant tirer les rideaux de la chambre réveillèrent Guido. Il grogna puis se retourna sur ses coussins de plume. La lumière qui se déversa soudain à travers les claustras révéla au marchand que le jour était bien avancé. Il ronfla de déplaisir à l’idée de s’arracher à sa couche douillette. La veille, il avait organisé un banquet pour célébrer dignement le dimanche de Pâques. Ses invités avaient pu y apprécier la richesse de sa demeure, même si la taille en était réduite. Ils avaient d’ailleurs largement débordé dans la cour centrale et la fête avait duré bien avant dans la nuit.

Guido passait la plupart de son temps de ce côté de la Méditerranée, n’étant pas rentré à Pise depuis belle lurette. La création d’un fondaco[^fondaco] à Alexandrie quatre ans plus tôt était arrivée à point nommé pour lui. Il était las de naviguer et avait toujours apprécié le climat égyptien. Par ailleurs, sa demeure était désormais bien plus luxueuse que tout ce qu’il avait pu posséder jusqu’alors : des meubles marquetés, du stuc et des mosaïques sur tous les murs, agrémentés de tentures de soie importées de Byzance et d’Aden. Des tapis épais du Khorasan habillaient les sols ouvragés et des senteurs épicées s’échappaient de brûle-parfums damascènes en bronze et en céramique dans chacune des pièces. Il ne pouvait résider en dehors du fondaco, certes, mais il y amassait tout ce dont il pouvait rêver. Et, surtout, de là il se rendait aisément à Fustat[^fustat], le véritable cœur économique de la région.

Enfilant ses mules, il passa une chemise d’étoffe fine et un bliaud richement orné, de velours de soie rebrodé, puis pénétra dans la salle principale, ouverte par une large arcature sur la cour centrale, au-dessus des entrepôts. Il aimait à prendre ses repas en admirant tout ce petit monde qui s’affairait, en partie à son service. Un valet s’empressa de lui apporter des fruits frais, un peu de vin de Chypre et du fromage de brebis, le tout disposé dans de magnifiques coupes en céramique sicilienne décorée.

Des balles d’un de ses amis étaient actuellement transportées dans une des galeries. Il s’agissait apparemment de tissus et de papier, deux denrées hautement rentables. Mais Guido faisait commerce de produits encore plus intéressants : il vendait du bois d’œuvre, souvent destiné à des engins de guerre, ainsi que des armes et des matériaux qui servaient à les fabriquer. Il importait essentiellement d’Europe et y renvoyait ses navires emplis de marchandises en provenance d’Aden. En outre, il ne dédaignait pas quelques petits investissements ici et là, sur les étoffes d’Ifriqiyah[^ifriqiya] en particulier, pour lesquels il ne faisait généralement que s’associer. Cela lui permettait par ailleurs de justifier ses activités sans avoir à s’étendre sur ses principales négociations, maintes fois condamnées par l’Église et les puissants monarques chrétiens.

Finissant de picorer dans les fruits, il rentra dans sa maison et prit son qalansuwa tawila[^tawila] qu’il recouvrit comme à son habitude d’un turban lâche qui lui repassait sous le cou, à l’égyptienne. Enfin, il enfila une ’aba[^aba] à larges manches, en soie d’Alep. Ainsi équipé, il descendit faire le point avec ses clercs sur les affaires en cours. Il avait le temps de flâner avant le déjeuner. Avec la reprise des échanges commerciaux après l’hivernage, il attendait l’arrivée sous peu de produits acheminés depuis Chypre par le biais d’un de ses amis.

## Alexandrie, fondaco des Pisans, midi du mercredi 3 avril 1157

Des soucis avaient empêché Guido de profiter de son repas. Le navire qu’il attendait, en provenance du nord, était à quai depuis la veille, mais les douaniers semblaient trouver un malin plaisir à retarder la sortie des passagers. Un de ses associés, Elijah ben Moses, était venu chez lui, affolé par la tournure que prenaient les affaires. Guido décida donc de l’accompagner sur place, dans les bâtiments de la douane. Il fit préparer une monture pour s’y rendre et réquisitionna deux de ses serviteurs et un scribe.

Très vite ils se retrouvèrent noyés dans le flot de portefaix, vendeurs ambulants, voyageurs et négociants qui encombraient les abords du port de commerce. L’odeur marine se mêlait subtilement aux relents de sueur, de poisson et de marchandises qui imprégnaient les quais. Au-delà du mouillage, sur l’île, l’immense construction du phare écrasait de sa masse tous les navires ancrés là. Même le vaste enclos des douanes paraissait minuscule en comparaison.

La cohue était indicible aux abords de la porte et on s’y invectivait dans une bousculade générale. Deux soldats se tenaient devant le petit guichet, les vantaux principaux étant fermés. De nombreux voyageurs étaient retardés, retenus, questionnés, et cela provoquait toutes sortes de problèmes pour les familles et les amis qui les attendaient. Guido fut un peu soulagé de découvrir que nul n’était épargné par les ergotages administratifs du pouvoir fatimide. En tant que Latin, il était toujours un peu inquiet de voir les autorités s’intéresser de trop près à ses activités. Il savait pouvoir en appeler au vizir Ṭalā’i‘ ben Ruzzīk en cas de souci, mais il était conscient que les réseaux d’influence étaient mouvants.

Finalement avant même de pouvoir accéder au bâtiment, il vit s’approcher son contact, Jahm al Aswad, un maghrébin au service de son associé tunisien Muqatil ibn Sa’id al-Arabi. Le jeune homme semblait consterné et essuyait d’un fin mouchoir son visage trempé de sueur. À la vue de Guido, il agita la main amicalement, l’air épuisé. Il salua poliment de la tête Elijah et se présenta à lui avant de s’adresser à Guido.

« Je ne voyais pas la fin de ces jours, mon ami. Que les démons emportent tous ces douaniers !

— Vous avez eu graves soucis ? demanda poliment le Pisan.

— Pas vraiment, seulement des désagréments. Mon père m’avait prévenu que les douanes d’Alexandrie étaient tatillonnes, mais je n’aurais pas cru à ce point-là. Ça a commencé hier : des hommes sont montés inspecter toute la cargaison, avec minutie. Puis nous avons été convoqués un par un, et avons dû indiquer nos noms et notre pays d’origine ainsi que les marchandises et monnaies en notre possession, pour déterminer la zakat[^zakat]. Et ceci sans même s’inquiéter du temps pendant lequel nous avons eu ces biens ! Un homme avec lequel j’ai sympathisé en route, simple pèlerin de la foi, a été obligé de régler la taxe sur ses provisions de bouche, vous vous rendez compte ? »

Agitant la tête en tout sens, sans cesser de frotter son mouchoir, il ne pouvait retenir le flot de paroles indignées qui avaient macéré dans son esprit.

« Ils ont ensuite longuement questionné certains d’entre nous sur les Ifranjs, et ils m’ont mené, comme un malfaiteur, pour répondre au gouverneur, au cadi, à nombre de questions insidieuses, notant par le menu toutes mes déclarations pour voir si je ne me contredisais pas. Finalement, on nous a autorisés à débarquer il y a peu et nous avons dû passer chacun à notre tour devant des fonctionnaires mesquins pour voir nos affaires personnelles être fouillées sans pudeur. Ils ont même regardé dans ma ceinture si je n’y cachais rien ! Outre, les porteurs étaient tellement mal organisés que ce sera une chance s’ils n’ont pas mélangé mes affaires avec celles d’un autre.

— Vos soucis sont finis, je me suis arrangé pour que vous soyez bien installés, dans l’hôtellerie des Dinandiers, près de la Savonnerie. Vous verrez qu’Alexandrie sait se montrer accueillante. Avec Elijah, nous allons vous conduire immédiatement si vous le souhaitez. »

Voyant le visage soulagé du jeune marchand, Guido donna quelques instructions à son scribe et à un de ses serviteurs pour qu’ils retournent au fondaco puis il prit le chemin de l’hôtellerie à pied. Tandis qu’ils avançaient dans les rues, Elijah et lui indiquaient à leur hôte les bâtiments de renom, montraient les grandes colonnades de l’ancien temps, désignaient les places dignes d’intérêt. Ils ne récoltèrent que des moues peu éloquentes et des hochements de tête sans enthousiasme. C’était la première fois que Jahm parcourait ces lieux, mais il ne semblait guère s’en émouvoir. La mention d’un hammam tout à côté de son hébergement illumina malgré tout le visage du nouvel arrivant. Finalement parvenus devant l’hôtellerie, Guido allait prendre congé lorsqu’il sentit que le jeune homme hésitait à dire une dernière chose.

« Il demeure autre souci que vous n’avez mentionné, mon ami ? Dites et nous ferons au mieux. Nous avons usage des diwans de la cité…

— J’aurais préféré vous l’annoncer autrement, mais je n’ai trouvé de belle façon pour cela. Cela me soucie depuis un moment : lorsque nous avons fait escale à Ascalon, la douane des Ifranjs a fait débarquer des balles.

— Dont certaines à moi ? Combien ?

— À dire le vrai, seulement les vôtres. Toutes les vôtres. »

## Alexandrie, fondaco des Pisans, fin de soirée du samedi 6 avril 1157

Des lots d’étoffes en provenance du Khwarezm[^khwarezm] venaient d’être ouverts et Guido avait un doute sur les sceaux de plomb identifiant les tissus. Il se trouvait donc de bien méchante humeur, la qualité n’étant pas celle des pièces habituelles. Déjà très soucieux de la saisie de sa cargaison à Ascalon, il craignait des contrefaçons. Comme il avait reçu cette marchandise en paiement d’une livraison d’ivoire à un contact sicilien, il était contrarié parce que cela pouvait entacher la réputation de cet associé et, par conséquent, la sienne. Il connaissait un Génois de ses relations qui avait failli être ruiné car on racontait partout qu’il avait longtemps collaboré avec un faussaire qui recachetait les bourses de dinars après en avoir gratté les pièces[^boursescellee].

Guido avait donc envoyé quérir à l’étage un de ses amis et associés, Francesco Caetani, pour avoir son avis. Ce farouche gaillard était un marchand âpre au gain, d’une réputation sans faille, ayant l’appui de sa puissante famille dans leur ville natale, mais aussi un homme affable et serviable. Francesco était un habitué du Khwarezm, ayant même visité la capitale, à la frontière avec le désert oriental. Il saurait immédiatement reconnaître une copie d’une vraie production.

Le riche négociant arriva dans l’entrepôt, un éventail à la main pour se faire de l’air. Il portait une ample tenue typiquement musulmane, jubba[^jubba] et ’aba, avec un petit chapeau de feutre brodé. Il était tellement ventru qu’il avançait en se dandinant, faisant ondoyer les étoffes à chaque pas. Guido remarqua immédiatement sa mine également contrariée.

« Que se passe-t-il mon cher ami ?

— J’ai reçu dans mes balles du papier qui a été abîmé lors du transport. Les bâches étaient défaillantes et de l’eau de mer a gâté une bonne partie de la marchandise. J’aurais dû m’écouter et faire faire le transport intégralement par voie de terre. La saison commerciale commence fort mal cette année. »

Guido hocha la tête, dépité. Il lui semblait que tout allait mal depuis quelques jours. Francesco essaya de combattre l’amertume qui s’installait en reprenant d’une voix plus enjouée.

« Fort heureusement, de nombreux contrats de *societas maris*[^societasmaris] que nous avons mis en place devraient être profitables. Notre envoyé du Rhin a pu se procurer auprès d’armuriers un bon nombre de lames d’épée de qualité, qui intéresseront certainement le vizir. »

La nouvelle tira un sourire crispé à Guido, qui désigna les étoffes déballées devant lui sur une table.

« Puissiez-vous parler de raison, que ces ennuis se fassent bien vite oublier… Ce qui m’inquiète pour l’instant, ce sont ces marchandises, qui me semblent suspectes. »

Accommodant, Francesco se pencha sans plus un mot et inspecta d’un œil avisé les pièces présentées, n’hésitant pas à les porter au-dehors pour les voir à la lumière naturelle. Il tourna ensuite son attention sur les sceaux qu’il détailla en marmonnant pour lui-même. Puis il reposa le tout en vrac en déclarant :

« Ce sont bien des pièces khwarezmiennes, à n’en pas douter. Mais il est vrai que ce ne sont pas les plus belles que j’aie pu voir.

— Vous me rassurez. Je redoutais grave problème avec ces produits.

— À votre place, je les expédierais à l’occident, où elles auront plus de prix qu’ici. »

Rasséréné par la nouvelle, Guido invita Francesco à le suivre.

« Faites-moi le plaisir de venir partager un peu de vin grec pour vous remercier de votre conseil.

— Avec grand plaisir, s’exclama son compagnon en souriant. Vous avez reçu une nouvelle cargaison de nabidh[^nabidh] ? Je n’arrive pas à croire qu’il vous reste encore quelques tamawiya[^tamawhiya] après la veillée de l’autre jour ! »

Les deux hommes repassèrent dans la grande cour tandis que des membres de l’ahdath[^ahdath] étaient en train de clore les portes du fondaco pour la nuit. À l’intérieur, un valet tirait les verrous du petit guichet qui permettait aux individus identifiés d’aller et venir. La sécurité des biens et des personnes était à ce prix et aucun des marchands enfermés là n’y prêtait attention.

Une fois confortablement installé sur des coussins, Guido attendit que son compagnon ait eu le temps de déguster un peu le vin avant de lancer la discussion. Francesco était un homme de ressource, qui avait de nombreux contacts dans les territoires latins.

« Dites-moi, mon ami,vous avez entendu parler de mon souci avec le comte de Jaffa ?

— Certes oui ! Et je le déplore. Avez-vous avec eux quelque contentieux non réglé ?

— Pas à ma connaissance. Sans compter que ces balles ne sont pas miennes, sur les documents. Elles sont toutes identifiées appartenant à différents miens contacts. Il leur aura fallu grande habileté pour les saisir. »

Francesco sirota son vin en silence, réfléchissant aux implications d’une telle opération. Il savait qu’en théorie ce commerce était passible des condamnations les plus féroces de la part des chrétiens. D’autant plus que l’Égypte se montrait de plus en plus belliqueuse sous le nouveau vizir.

« J’ai parmi mes associés un certain Jaffa al-Akka, je ne sais si vous avez déjà traité avec lui…

— Ce nom ne m’est rien.

— Jeune, prometteur. Il a beaucoup commercé avec les Génois, moins depuis quelques années. Grâce à un de ses amis, il m’a aidé plusieurs fois à régler de petits soucis avec la Chaîne[^chaine] d’Acre ou celle de Césarée. Il est en ce moment à Misr[^misr], si jamais il repasse par ici, je lui demanderai avis. »

Guido acquiesça. Il jouait très gros dans cette histoire. En plus des marchandises perdues, dont le montant s’élevait à plusieurs centaines de dinars, il risquait de se voir interdire l’abord des côtes chrétiennes. Il ne tenait pas à être frappé d’une marque d’infamie, voire de finir au bout d’une corde. Il n’avait pourtant guère envie de devoir quitter ces rivages qu’il appréciait tant. Le climat de la Méditerranée occidentale lui convenait beaucoup moins, sans même parler de retourner à Pise, qu’il n’envisageait nullement.

## Alexandrie, fondaco pisan, midi du mercredi 24 avril 1157

Guido était épuisé, il avait passé une partie de la nuit à peaufiner des courriers que Jahm al Aswad allait emporter avec lui. Il lui avait fallu temporiser avec de nombreux contacts, en raison de la perte sèche que représentait l’immobilisation de ses marchandises à Ascalon. Il avait auparavant rédigé plusieurs autres lettres destinées à obtenir des informations de la part des administrations latines, faisant suite à celles parties depuis une dizaine de jours.

Ce blocage intervenait au plus mauvais moment. La navigation en Méditerranée venait à peine de reprendre après le long arrêt hivernal et il serait contraint de s’abstenir d’investir dans les trafics de cette saison. Ce qui représentait un gros manque à gagner, d’autant que Ṭalā’i‘ ben Ruzzīk achetait tous les produits nécessaires à son effort de guerre à un bon prix. Cela n’allait certainement pas durer, que le vizir voie ses plans couronnés de succès ou pas.

Guido avait fait l’inventaire des marchandises qu’il conservait en magasin et c’était maigre. Comme tous ses collègues négociants, il estimait que le moindre stock contrevenait à l’esprit de lucre qui l’animait. Il ne possédait même pas beaucoup de pièces, tout étant placé en participations, en cargaisons ou en balles. Il gérait des flux et des ressacs de biens et de contrats, qui s’alimentaient les uns les autres, prenant au passage sa commission. Il suffisait que l’une des sources se tarisse pour que le fragile ensemble se déséquilibre. Ce qui était en train de lui arriver.

Il finissait de classer ses documents lorsqu’il entendit des voix résonner sous les voûtes devant son logement. Il reconnaissait bien évidemment Francesco, mais pas les intonations graves qui lui répondaient. Il verrouilla la cassette où il resserrait toutes ses notes administratives importantes et s’avança vers l’entrée.

Il remarqua immédiatement la silhouette gracieuse qui accompagnait son ami pisan. Bien que d’un physique délié et impressionnant, l’homme se déplaçait avec élégance. Vêtu d’un simple thwab et d’un durrâ’a aux couleurs bigarrées, il portait sur la tête un turban de soie bleue orné d’un bijou d’argent. Tout en lui respirait l’aisance et la maîtrise. Son faciès ne semblait pas égyptien, mais il était difficile de dire s’il venait de l’Orient ou d’Europe. Son teint mat, son regard sombre et sa barbe noire l’auraient fait passer pour le cousin de n’importe qui autour de la Méditerranée. Guido bomba la poitrine, rentra le ventre et offrit son plus beau sourire, laissant Francesco faire les présentations.

« Guido, mon frère, je te présente al Jāmūs, l’ami d’un ami. »

En indiquant cela, il confirmait que ce marchand faisait partie d’un cercle de confiance, que d’autres étaient prêts à se porter garant pour lui.

Très rapidement ils s’installèrent sur des coussins, confortablement répartis sur des banquettes. Des domestiques s’empressèrent d’aller chercher quelques friandises et disposèrent du leben[^leben]. Ne sachant pas si ce négociant était un fidèle scrupuleux, Guido préférait ne pas le provoquer en proposant du vin. Ils commencèrent par discuter des affaires en général, du regain d’activité militaire voulu par les émirs, des conquêtes du nouvel homme fort de Syrie, Nūr ad-Dīn. Al Jāmūs semblait très au fait de ce qui passait dans les ports latins, ce qui était de bon augure.

Francesco finit par s’esquiver, non sans avoir évoqué en partant les ennuis que causait la Chaîne d’Ascalon à certains marchands. Guido put alors enfin aborder le sujet qui l’intéressait.

« Vous avez aussi quelques soucis avec eux ?

— J’ai déjà eu à verser de l’huile sur les bons verrous.

— Auriez-vous idée des motifs qui les auraient incités à séquestrer ainsi mes biens ? »

Al Jāmūs réprima un sourire et avala une boulette au sésame grillé avant de répondre. Guido remarqua alors que l’homme n’avait pas tous ses doigts à la main droite, chose qu’il dissimulait adroitement dans les manches de son ample vêtement.

« Croyez-vous que le sire comte de Jaffa aime à savoir des armes pour ses ennemis passer en ses ports ?

— Comment aurait-il pu apprendre cela ? C’est commerce fort discret…

— Peut-être parce que je lui ai dit ? »

Guido éprouva un léger vertige. Il n’était même pas certain d’avoir bien entendu. Son invité continuait à picorer d’un air affable comme s’il venait de parler de la météo. Le Pisan avala difficilement sa salive avant de reprendre.

« Que voulez-vous dire ? Je croyais que vous étiez…

— L’ami d’un ami. De certes. Du moins ce sera à vous d’en décider. Je suis là pour négoce. Comme vous !

— Je ne comprends rien à ce que vous me dites.

— C’est pourtant très simple. Vous voulez acheter libre passage dans les ports du comté de Jaffa. Moi je peux vous le vendre. Le commerce dans sa forme la plus pure. »

Guido se sentait soudain en proie à des nausées. L’homme, malgré son ton aimable et son indolence, ou peut-être plus précisément à cause d’eux, lui faisait l’effet d’un serpent prêt à mordre. Il se redressa sur son siège, se frotta le visage.

« Vous avez moyen d’acheter quelque officiel ?

— Je peux aider certains à regarder ailleurs.

— Combien cela me coûterait-il ?

— Quelques papiers, rien de plus.

— Vous voulez devenir mon associé ? »

La question fit ricaner l’homme, qui secoua la tête en dénégation.

« Je ne vois nul intérêt à vos babioles, mon ami. De simples courriers, mais réguliers m’iraient mieux…

— Et que vous écrirais-je ?

— Tout, mon bon. Tout.

— Qu’est-ce à dire ?

— Que pour ne plus froisser le comte de Jaffa, frère le roi de Jérusalem, vous ferez l’espion pour lui. » ❧

## Notes

Malgré la situation de guerre endémique au Moyen-Orient, le commerce florissait. On accueillait volontiers tous les négociants, pour peu qu’ils s’affranchissent des taxes et droits. Certaines denrées faisaient néanmoins l’objet de restrictions, permanentes ou temporaires, ce qui n’empêchait pas les plus audacieux de se risquer dans des aventures de contrebande. Si les profits étaient attrayants, les peines encourues, pour être parfois symboliques, étaient suffisamment répulsives pour être prises en compte par la plupart des marchands.

Par ailleurs, le renseignement florissait aussi, bien évidemment dans ces milieux qui avaient accès aux deux camps de façon quasi libre. Tous les opposants s’appuyaient donc sur des espions et informateurs, épisodiques ou réguliers. Même si la lenteur des moyens de communication et la difficulté de contact en rendaient l’efficacité toute relative, chacun tissait sa toile de façon à en savoir le plus possible sur ce qui se passait chez ses adversaires. La discrétion nécessaire de ces pratiques fait qu’il est désormais extrêmement compliqué d’en esquisser l’importance et l’impact réel, surtout en dehors du renseignement purement militaire lors des campagnes.

## Références

Broadhurst Roland, *The Travels of Ibn Jubayr*, London : Cape, 1952.

Elisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.
