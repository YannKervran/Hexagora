---
date : 2021-02-15
abstract : 1137-1160. Herman, jeune clerc érudit et talentueux cherche à gravir les échelons des dignités ecclésiastiques.
characters : Herman de Meissen, Wibald de Stavelot, Rahewin de Freising
---

# Curiales

## Stavelot, abbaye, soir du lundi 20 décembre 1137

Les pas du frère qui menait Herman claquaient sur les dalles de pierre en rythme avec le balancement de sa lampe. Herman découvrait pour la première fois cette zone du couvent, où circulaient habituellement princes et magnats de ce monde, parmi lesquels le puissant abbé des lieux, Wibald. Ce dernier était récemment rentré de ses voyages et tenait à rencontrer personnellement tous les novices qui étaient arrivés en son absence. Herman n’était donc pas particulièrement inquiet, même s’il redoutait de ne pas faire bonne impression. Le prélat était un homme à la réputation de rigueur et de labeur. On disait son esprit nourri d’une érudition sans faille encore plus acéré que sa langue.

La cellule qui tenait lieu de chambre et de bureau surprit Herman par sa simplicité. Bien que les murs en fussent ornés de fresques aux scènes édifiantes, le mobilier en demeurait sobre, fonctionnel et sans ostentation. Assis à une vaste table de travail encombrée de missives, lettres, parchemins et tablettes, Wibald était perdu dans ses pensées quand ils pénétrèrent dans ses quartiers. Le visage extrait des ténèbres ambiantes par quelques lampes disséminées sur le plateau offrait une apparence bonhomme. La faéce ronde, presque lunaire aurait pu facilement engendrer un aspect poupin sans la rigueur de l’ecclésiastique en ce qui concernait les plaisirs de la chère. Sa tonsure était nette, tracée aussi strictement que sa barbe fine et ne laissait qu’une mince bande de cheveux. À son annulaire brillait une imposante bague abbatiale, écho de la lourde croix d’émaux qui pendait à son cou.

Bien qu’il n’ait pu ignorer leur entrée, il demeura un instant les doigts sur les tempes, perdu en une introspection née de la lecture des documents devant lui. Puis il leva les yeux vers les moines qui attendaient, immobiles. Sans un mot de plus, son secrétaire se retira, laissant Herman seul face à l’examen, toujours silencieux, du maître des lieux. Il finit par joindre les mains, dissimulant ses lèvres fines.

« Voilà donc notre jeune rossignol ! J’ai eu grand plaisir à t’entendre, mon fils. Ce n’est pas tous les jours que Dieu met tant de grâce en l’un de nous. »

Herman sourit en remerciement, attendant une invitation formelle avant d’oser prendre la parole.

« J’ai également appris que tu es passé par Saint-Pancrace[^saintpancrace]. Sais-tu que l’admirable Hugues de Saint-Victor de Paris y débuta ses études ? »

Herman hésita à répondre, n’étant pas habitué à converser avec l’abbé. Mais, à l’attitude de son supérieur, il comprit bien vite que le grand homme appréciait fort les monologues, et attendit patiemment une fois de plus.

« Tu l’as sans nul doute appris, mais la mort récente du roi Lothaire risque de provoquer remous et troubles. Il nous faut tout faire pour que les provinces demeurent en une main ferme, mais point cruelle. Il en va des intérêts de l’Église ici, à Rome et au-delà. As-tu connoissance du duc de Franconie ? »

La question était cette fois directe et Herman répondit d’une voix qu’il espérait assurée.

« C’est le neveu de l’empereur Henri[^henriv] auquel il a souhaité succédé, mais ses attentes ont été déçues. Il s’est opposé un temps à Lothaire, pour ne se soumettre, avec son frère, qu’après la Diète de Bamberg.

« C’est là savoir du commun, je pensais que tu avais plus fortes attaches avec lui. On dit que ta famille est de celles qui n’hésitent pas à parler en bien des Souabes. »

Herman sentit son cœur s’affoler à ce qui était peut-être l’échec à un test. Mais Wibald ne lui laissa pas l’occasion de développer, il se pencha en avant, posant le menton sur ses mains croisées.

« Les temps qui s’annoncent vont être difficiles, mon fils. Malgré la campagne victorieuse contre les Siciliens, bien des menaces demeurent. Il faut à nos princes des fanaux pour les guider dans ces océans de ténèbres, des hommes issus des meilleures écoles, familiers des Pères, habiles à résoudre les problèmes et solides en leur Foi. Ce n’est plus un temps où la vaillance des lames peut suffire, quoi qu’en disent nos intrépides reitres. »

Il fit mine de prendre une des missives, laissant le silence s’installer, puis se recula dans son siège. Les lueurs dansantes creusaient son visage d’inquiétants traits d’ombre tandis qu’il réfléchissait.

« Tu vas quitter le service du chantre et entrer au mien. Tu recevras l’ordination sacerdotale au plus vite, car il ne sied pas que simple frère soit mon calligraphe. »

Herman se retint d’exprimer sa surprise. Il avait de prime abord attendu une ascension rapide, mais patientait depuis de si longs mois qu’il avait peu à peu perdu espoir. Il lui sembla fugacement enfin apercevoir la première des marches qui le conduiraient peut-être à finir évêque ou mieux encore. Il hocha la tête sans un mot, s’efforçant à la digne réserve bénédictine.

« Tu iras voir le frère drapier, pour te fournir de quoi voyager tantôt au parmi des frimas à mes côtés. Dès après la Noël, nous prendrons la route de Coblence. Il va nous falloir honorable successeur de Lothaire… »

## Freising, palais épiscopal, matinée du mercredi 12 mars 1147

Le temps humide avait incité plusieurs clercs de l’administration épiscopale à se retrouver dans le chauffoir pour y travailler plus confortablement. Dehors, une pluie intermittente noyait de gris ardoise l’Isar. Dans ce havre douillet, aux craquements du bois sec répondait le grattement des plumes sur le papier ou le parchemin. Tous étaient issus d’une stricte éducation monastique où le bavardage ne cohabitait que peu avec le labeur. Et il y avait fort à faire en ce printemps maussade. De nombreux courriers étaient échangés en cette période de préparation d’un long voyage outremer pour l’évêque Othon, en compagnie de son demi-frère le roi de Germanie Conrad.

Herman appréciait cette ambiance studieuse familière, savourant également le fait de n’avoir que peu à faire. Il profitait de son séjour pour parcourir les ouvrages de l’imposante bibliothèque d’Othon de Freising. Il dévorait pour le moment une copie du *De unitate et trinitate divina, sive Theologia Summi Boni* ^[« De l’unité et de la trinité divine, ou Théologie du Bien suprême », vers 1120.], texte d’Abélard le sulfureux écolâtre parisien, récemment décédé, qui s’était durablement opposé au non moins célèbre abbé de Citeaux Bernard.

Comme celui-ci était actuellement en train de sillonner les territoires alémaniques pour prêcher la prise de la croix par le plus grand nombre, Herman savourait le délicieux paradoxe. S’il souscrivait pleinement à la vision de la Foi défendue par Bernard, le génie rhétorique et la puissance conceptuelle du savant parisien résonnaient fort parmi les jeunes générations de lettrés dont il était, formés dans ses traces, au moins intellectuelles, si ce n’était spirituelles. Le seul désaccord qu’il avait avec les théories de Bernard concernait les Juifs. Lui-même n’appréciait guère leur présence et les aurait volontiers forcés à la conversion ou au départ. Adepte d’une moindre violence, il n’approuvait pas les récents massacres, mais comprenait la colère qui avait animé leurs instigateurs.

Hermann avait été envoyé par son abbé auprès de l’évêque de Freising, car il intégrerait son administration le temps du voyage vers la Terre sainte. Wibald ne pouvait prendre la route, mais il avait tenu à adjoindre au roi de Germanie un clerc de confiance, dont il ne doutait pas des fréquents rapports à lui destinés. Il était particulièrement impatient d’en apprendre plus sur le basileus byzantin, dont il estimait qu’il s’agissait, avec le pape et l’empereur germanique, d’un des piliers de la chrétienté. Dans la conjonction de ces trois puissances résidait l’avenir de l’humanité, Wibald de Stavelot en était persuadé. Et cela avait suffi pour qu’Hermann se voie contraint à un long, périlleux et bien inconfortable périple jusqu’à Jérusalem.

Il craignait que cela ne mine définitivement ses chances de promotion, à courir ainsi loin des centres de pouvoir. S’il se trouverait au plus près du prince régnant actuellement sur l’empire, sans toutefois en porter le titre, il ne pourrait que difficilement rencontrer d’occasions de se hisser à l’épiscopat ou l’abbatiat, qu’il visait depuis tant d’années. Il redoutait de finir semblable à Rahewin, confident et secrétaire du puissant évêque de Freising. Homme d’importance, mais dont l’existence était subordonnée à un maître. Hermann découvrait chaque jour avec horreur un nouveau point de ressemblance avec le discret fonctionnaire, qu’il commençait à considérer comme un ami.

Depuis le matin, Rahewin établissait une liste d’ouvrages et de documents que le prélat emporterait avec lui. Fin connaisseur des volumes de la bibliothèque et des archives, il notait en un long inventaire ce que les valets devraient soigneusement empaqueter. Il convenait de n’omettre aucun livre d’importance, de façon à avoir sous la main tous les arguments et les preuves tangibles de la supériorité de leur foi et de leur liturgie quand ils seraient dans les provinces byzantines.

Rahewin marqua finalement une pause, reposant sa plume avant de bâiller ostensiblement.

« Il me semble que la lumière baisse, je vais mander quelques lampes, déclara-t-il. Et aussi un pot ou deux de fruits au vin, le souper demeure fort loin et nous n’avons diné que légèrement. »

Sans pour autant succomber à une gourmandise effrénée dont on moquait bien des clercs, Rahewin aimait manger à toute heure, se nourrissant autant de la vue des autres grignotant que de ses propres bouchées. Doux et chaleureux, il avait à cœur de se faire apprécier de tous, quand bien même ce n’étaient que de ses subordonnés. Hermann y établissait la différence fondamentale entre eux : le secrétaire de l’évêque de Freising n’avait aucune ambition, satisfait du rang auquel il était parvenu.

La proposition relâcha la discipline implicite et les bavardages naquirent tandis que chacun prenait place pour l’encas. Hermann lui-même approcha son tabouret du grand feu de la cheminée, tendant ses mains vers les flammes. Rahewin vint s’installer près de lui et il se dit qu’il pourrait en profiter pour lui soutirer quelques informations. Sans pour autant être un espion, il veillait à en collecter le plus possible pour son abbé.

« Était-ce coursier du roi qui est arrivé ce matin ? J’ai vu son train en la cour. Importantes nouvelles ?

— Rien de bien nouveau. Il confirme que le grand départ sera fait depuis Ratisbonne, où se tient ces jours-ci la Diète[^diete]. Avant toute chose, il souhaite garantir la couronne à son fils. Nous ferons donc route certainement peu après les Pâques. »

Il se rapprocha insensiblement du clerc, avant de sortir d’une de ses manches une chute de parchemin sur laquelle son écriture précise emplissait les moindres recoins.

« Mais foin de ces soucis de labeur ! Je vous sais fort amateur de poétique et j’ai espoir de porter en satisfaisants hexamètres l’hagiographie de saint Théophile. J’y vois là sujet d’édification propre à répandre l’amour de la Vierge, bienheureuse mère de Dieu. Écoutez donc et dites-moi si ces paroles résonnent en vous… »

## Damas, oasis de la Ghuta, chapelle royale de Conrad, matin du mercredi 28 juillet 1148

Le vent faisait craquer les membrures de la toile et se balancer les lampes suspendues tandis qu’Hermann et Rahewin finissaient de ranger leurs tenues de célébration. Une fois encore, Conrad avait écourté sa présence à la messe, emportant avec lui son aréopage de soldats et conseillers. Les deux prêtres déploraient que le combat spirituel soit ainsi négligé, et voyaient là un accroc au prestige de leur fonction. Et ils s’accordaient parfaitement sur le résultat redouté de ces façons de faire cavalières envers leur Créateur.

« Le roi est furieux que de si grands feudataires commencent à faire désaffection, se lamenta Rahewin. J’espère qu’il saura les ramener à la raison.

— Le souci, c’est que les barons d’ici aiment trop à écouter le son de leur propre voix. Et ils n’ont goût que pour batailles et lances rompues ! Nulle surprise qu’ils aient si peu entendu ce que narrait l’évêque Geoffroy^[de Langres.] !

— Il se dit au parmi des hommes que des dinars infidèles auraient convaincu certains seigneurs poulains…

— Cela expliquerait l’empressement de certains à vanter la puissance des armées ennemies en marche pour libérer la ville. J’ai bien vu combien cette nouvelle avait assombri l’humeur du roi ! »

Rahewin finit de plier son étole et la rangea dans une solide malle dont il ferma les serrures d’une clef à sa ceinture.

« Souvenez-vous des conseils de l’impératrice à son père[^irene] ! Les fils des intrépides conquérants menés par l’évêque Adhémar[^adhemarmonteil] ne sont que des nains par rapport à leurs illustres devanciers. Ils se querellent la moindre terre, la plus petite province.

— De certes, leur tempérament batailleur les incite à chercher renommée et vaillance, alors qu’ils n’obtiennent que gloriole et témérité. Sans mesurés conseillers pour les assister, toute leur force ne devient que bravache. »

Rahewin opina du chef longuement, un peu dépité.

« L’évêque Othon m’a confié que le roi a espoir de rallier de nouvelles échelles, pour revenir en force. Et il escompte trouver en la sainte cité plus de courageux capitaines, bien fournis en viandes et en armes, plutôt que crasseux pérégrins. »

N’ayant aucun intérêt pour la campagne en cours, Hermann ne se sentait que peu dépité à l’idée de quitter le campement de toiles, même si c’était pour quelques semaines. Il avait espéré, comme tous les membres de l’hôtel royal de Germanie, que le siège se déroulerait aisément. Mais il avait appris, depuis leur effroyable traversée de l’Anatolie, que les périls étaient nombreux dans ces régions. Il attendait juste avec impatience le moment où il pourrait prendre le chemin du retour. Néanmoins, conscient que son compagnon s’était plus enflammé pour leur sainte mission, il gardait ses réflexions pour lui.

## Pavie, palais épiscopal, midi du vendredi 4 mars 1160

D’une humeur sombre, Hermann sélectionnait les tenues qu’il emporterait avec lui lpour ce nouveau long périple. Il ne pouvait prendre qu’un coffre et devrait subir les périls de la haute mer, dans une galère spécialement affrétée. Il était malade rien qu’à l’idée de devoir supporter d’interminables journées sans voir aucune côte. Lorsque le concile avait élu le pape Victor, il était évident que son opposant, qui se faisait appeler Alexandre, n’accepterait jamais d’abdiquer. Il avait déjà refusé de se présenter devant l’empereur Frédéric pour se soumettre à son jugement. La suite logique de ces affrontements était l’envoi de diplomates auprès de tous les monarques afin de se garantir leur soutien et la reconnaissance du souverain pontife légitime.

Et la curie impériale l’avait désigné, lui, comme première ambassade en Outremer, auprès du roi Baudoin, le gamin écervelé qui avait assiégé en vain une cité au milieu du désert une douzaine d’années plus tôt. Il ne doutait pas un seul instant que c’était là le résultat de quelques manœuvres de jeunes clercs de son entourage, Erlung ou bien Burchard. Il avait vu arriver ces petits ambitieux et les observait se hisser dans les meilleurs postes avec une célérité qui le consumait de jalousie. Une telle mise à l’écart à son âge avancé risquait de le contraindre à finir simple moine dans un couvent de Silésie ou de Poméranie.

Pour une fois, Hermann n’aurait pas désapprouvé l’envoi d’un homme de guerre, plus enclin à affronter les pirates siciliens. Néanmoins, il savait l’empereur souverain habile et ne pouvait que se rendre à sa froide logique. Il utilisait chacun tel un des pions aux échecs afin de garantir sa victoire. Et il avait besoin de toutes les lances disponibles pour abattre Milan. En outre, le roi de Jérusalem, encore jeune et sans enfant, n’était pas vraiment intégré dans les jeux diplomatiques entre France, Angleterre et empire. Le rallier serait un avantage, mais son appui n’était pas stratégique. Ce qui renvoyait Herman à l’étendue de son importance à la curie. Il savait qu’il n’y allait que dans l’urgence, pour préparer le terrain à plus éminent plénipotentiaire, désigné par la suite, mais il ressentait cela comme une certaine rebuffade qui n’améliorait guère son humeur.

Faisant le tri dans ses affaires, il se remémora avec aigreur d’un des poèmes qui circulait dans les écoles qu’il avait fréquentées, enfant : 

> *Pontificum causas regumque negocia tractes*  
> *Qui libi divicias deliciasque parant*.^[« Vous vous occuperez de la cause des évêques et des affaires des rois // Qui vous préparent à la richesse et au plaisir ».] ❧

## Notes

Le XIIe siècle a vu l’accession de nouveaux administrateurs à des postes d’influence, qui n’étaient pas issus de la noblesse héréditaire ou des hommes de guerre. Il s’agissait de clercs, des lettrés formés dans les meilleures écoles, qui prirent peu à peu leur place dans les structures de gouvernement des différents monarques, dans des proportions assez variables. Le dédain avec lequel ils étaient accueillis par les tenants d’un pouvoir viriliste ou traditionnel n’était égalé que par leur superbe, nourrie de savantes références et de maîtrise d’abstractions érudites. Ils apportèrent néanmoins de nouvelles manières de faire dans la gestion des provinces et renouvelèrent le périmètre de recrutement habituel des cours seigneuriales.

Le terme de *curiales* était à l’origine utilisé de façon péjorative, comme par Pierre Damien en 1072, pour désigner ceux qui se servaient de leurs cercles relationnels pour obtenir des avantages.

La citation finale est rapportée à l’origine par Étienne de Tournai, qui cite un poème qu’il a connu alors qu’il étudiait à Bologne avec Richard Barre.

## Références

Casagrande Maria, Vecchio Silvana, « Clercs et jongleurs dans la société médiévale (XIIe et XIIIe siècles) », Annales, 1979, vol. 34, num. 5, p. 913‑928.

Foulon Jean Hervé, « Le clerc et son image dans la prédication synodale de Geoffroy Babion, archevêque de Bordeaux (1136-1158) », Amiens, 1991.

Turner Ralph V, « Toward a Definition of the Curialis: Educated Court Cleric, Courtier, Administrator, or “New Man”? », Medieval Prosopography, 1994, vol. 15, num. 2, p. 3‑35.
