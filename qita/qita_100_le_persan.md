---
date : 2020-02-15
abstract : Membre de l’élite sociale de la capitale abbasside, Barmak se voit parfois contraint de voyager en des terres désolées, où rien ne trouve grâce à ses yeux de civilisé. 
characters : Barmak al-Fayyad al-Baghdadi, Sufyan al-Katib, Abu al-Kabir
---

# Le persan

## Abords de Qasr-e Chirin, matin du youm el itnine 11 rabi’ al-awwal 555^[Lundi 21 mars 1160.]

Barmak fut réveillé en sursaut par le bruit d’une cavalcade aux alentours de la tente. Au peu de lumière qui filtrait au travers de la toile, l’aurore pointait à peine. Il perçut le froid sur son visage, n’osa pas s’extraire du doux cocon de ses couvertures. Le braséro au milieu de la pièce crépitait, mais était bien loin de suffire à maintenir une température agréable. Il aperçut un de ses domestiques qui s’avançait pour lui signaler l’heure du réveil et apporter ses vêtements chauffés, prêts à être passés. Sans un mot, il le congédia d’un regard peu amène. Les voyages le mettaient toujours dans une humeur massacrante et il détestait qu’on le voie au lever avant qu’il n’ait le temps de s’apprêter.

Prenant son courage à deux mains, il sortit de son lit et enfila rapidement un confortable kaftan de laine par-dessus sa qamis. Il passa également des chaussettes épaisses puis des bottes fourrées, finement ouvragées. Enfin, il posa sur sa tête un bonnet de feutre recouvert d’un long turban de soie rouge à rafraf brodés. Il détestait encore plus faire ses ablutions dans le froid qu’y renoncer. Il se contenta donc de brosser sa barbe et sa moustache, de se laver les mains et de se rincer le visage. Tandis qu’il finissait ses préparatifs, il appela son serviteur et donna des ordres pour qu’on lui apporte de quoi se restaurer après la prière. Puis il se rendit rapidement à la zone utilisée comme mosquée dans la tente de son maître Sufyan al-Katib.

Tandis qu’il traversait les espaces non chauffés, son souffle dansait en volutes de fumée, le gel lui mordait les joues. Ils étaient récemment arrivés aux abords des montagnes ; ils avaient quitté Bagdad juste à la fin de safar pour accomplir une mission au nom du calife Al-Mustanjid. Barmak était un fonctionnaire, un secrétaire au service de Sufyan al-Katib, lui-même un familier du grand vizir. Ils avaient pris la route pour Hamadan[^hamadan1], car l’angoisse rongeait le palais depuis la mort du précédent sultan seldjoukide Muhammad ibn Mahmud. Théoriquement au service du calife, ce puissant dirigeant était en fait indépendant et pouvait devenir un réel ennemi, comme l’avait prouvé le siège de Bagdad quelques années plus tôt. Le choix du candidat était donc crucial et, si Al-Mustanjid ne pouvait le nommer ni même influencer sa désignation, il lui fallait surtout apprendre à le connaître afin de le circonvenir au mieux.

La famille de Barmak était originaire d’Eṣfahân[^ispahan], mais avait fui devant les armées conquérantes seldjoukides au siècle précédent, pour se réfugier à Bagdad. Il faisait partie de la nouvelle vague de fonctionnaires qui ne voyait pas d’un très bon œil le contrôle qu’exerçaient les différents peuples turcs sur le pouvoir central abbasside. Il méprisait autant les Seldjoukides que les Kurdes ou les Turcmènes, qu’ils estimait soit barbares, soit dégénérés ou décadents, et se sentait plus d’affinités avec les anciennes dynasties arabes, qu’il trouvait plus civilisées, quoique trop attachées à leurs valeurs nomades.

Quand il entra dans le lieu de prière, il se déchaussa et prit sa place au milieu des familiers du puissant al-Katib. Celui-ci était assez âgé et sa longue carrière aux côtés du grand vizir Awn al-Din ibn Hubayra inspirait le respect à tous les jeunes ambitieux qui rêvaient d’occuper son poste. Barmak appréciait ce privilège, même s’il devait supporter les crises de colère lorsque le travail n’était pas accompli exactement comme le vieil homme l’avait décidé. C’était un lettré hors pair, habile dialecticien, qui savait tourner les phrases de belle manière et qui connaissait les usages et la titulature sur le bout des doigts. Barmak apprenait beaucoup depuis qu’il était à son service et espérait qu’on lui confierait un jour la rédaction complète d’un texte et pas simplement la retranscription de ce qui lui était dicté.

La prière était un instant privilégié entre tous, où le temps se suspendait, que Barmak goûtait particulièrement, loin de la frénésie bureaucratique et des intrigues de palais. Mais le moment de grâce était toujours trop court à son goût. Les chaussures à peine enfilées, les discussions politiques renaissaient. Un courrier était arrivé dans la nuit et ils savaient désormais qui était pressenti pour devenir le futur sultan. C’était Sulayman Shah et il y avait un petit souci à sa prise de fonction : il était prisonnier de Qutb ad-Dīn de Mossoul, le frère de Nūr ad-Dīn, l’émir de Damas et Alep, qui menait le jihad contre les envahisseurs celtes, vers les côtes occidentales. Il fallait repartir au plus vite et découvrir comment les choses allaient se mettre en place.

## Abords de Tikrit, bords du Tigre, midi du youm al had 4 jadjab 555^[Dimanche 10 juillet 1160.]

Le repas était très animé, les participants se divisant en deux factions aux opinions divergentes. Barmak était de ceux qui estimaient que l’atabeg Nūr ad-Dīn avait des visées sur les territoires à l’est et qu’il allait pour cela s’appuyer sur son frère Qutb ad-Dīn, mais un autre groupe de fonctionnaires, dont Abu al-Kabir était le meneur, était persuadé qu’il n’y avait rien à craindre de lui, qu’il se soumettrait au légitime dirigeant de tous les musulmans, Al-Mustanjid.

« Je n’arrive pas à vous comprendre, vous soutenez ce Turk alors qu’il tente d’interférer avec nos ennemis ! Il a bien fourni des hommes pour nous assaillir, voilà peu, non ? 

— Il ne pourrait désobéir au calife, tout atabeg qu’il soit. Il ne peut facilement s’aliéner ceux qui ont des familles influentes.

— Et pour quels motifs entretient-il des relations avec les Kurdes et les Turcomans de la frontière ? Il sait pourtant qu’ils ne tombent pas sous son pouvoir.

— Ces barbares sont tous parents, et se reproduisent comme des chacals en rut, mais de là à penser qu’ils peuvent s’allier… Les deux frères n’ont-ils pas failli s’entretuer pour le contrôle de Mossoul à la mort de Saif al-Din Ghazi ?

— Ne disent-ils pas *Moi contre mes frères ; mes frères et moi contre mon cousin ; mes cousins, mes frères et moi contre le monde* ?

Son adversaire fit la moue à cette habile réplique, ne trouvant pas quoi objecter. Barmak se détendit. Avisant d’un coup d’œil circulaire ses collègues, il savait qu’il avait gagné. Il s’empara d’une bonne portion de haljamiyya[^haljamiyya] en souriant d’un air satisfait.

Tandis qu’il savourait sa victoire, un serviteur vint lui dire quelques mots à l’oreille. Il devait rejoindre la salle principale au plus vite, car al-Kabir avait besoin de lui. Se rinçant les mains dans une cuvette d’eau parfumée, Barmak donna quelques ordres pour qu’on lui apporte de quoi écrire au cas où il lui faudrait prendre des notes. Il se demandait ce que cette rencontre cachait.

Alors qu’il s’asseyait à sa place, légèrement en retrait sur le côté du salon d’audience de la tente, il jeta un coup d’œil au groupe qui entrait. Il fut soulagé de voir que ce n’étaient que des Bagdadis, certainement porteurs d’instructions et de messages personnels de leurs familles. Il n’aurait pas à entendre encore ces langues gutturales et ces phrasés râpeux des soldats de métier.

Les nouvelles qu’al Kabir transmit aux émissaires étaient un peu déconcertantes pour eux, mais pas inquiétantes pour autant. Qutb ad-Dīn, émir de Mossoul et frère du grand Nūr ad-Dīn, avait accepté de libérer son prisonnier, Sulayman Shah, s’il était lui-même nommé atabeg de ce dernier, avec son fidèle Zayn ad-Dīn comme chef des armées. Il comptait donc devenir l’homme fort du régime, agissant dans l’ombre de sa marionnette. Mais celui qui se voyait déjà généralissime avait finalement été contraint d’abandonner son otage, alors qu’il l’escortait pour Hamadan. Tellement de partisans du futur sultan avaient rejoint sa caravane que Zayn ad-Dīn avait craint pour sa sécurité et avait rebroussé chemin, trahissant les espoirs de son maître. Les Bagdadis et les Persans ne cachèrent que peu leur soulagement à l’idée que la famille du zengîde ne s’empare pas du pouvoir aussi aisément.

## Jalula, bords du Tigre, soir du youm al sebt 23 jha'ban 555^[Samedi 3 septembre 1160.]

Barmak retira sa jubba doublée de fourrure qu’il abandonna à un serviteur et s’assit sur un tabouret pour qu’on lui ôte ses bottes, qu’il remplaça par des savates délicatement brodées. Il était furieux d’avoir dû sortir sous la pluie, dans la boue et le froid. Il n’appréciait déjà pas de devoir vivre sous tente, mais aller se balader dans le camp malodorant et fangeux était au-dessus de ses forces.

Il avait dû calmer les esprits, car une querelle avait éclaté entre des membres de leur escorte et des soldats damascènes. Le ton avait monté et un homme avait été frappé au visage d’un coup de savate^[La savate est considérée comme un élément impur, frapper quelqu’un avec constitue une insulte grave, surtout au visage.], ce qui avait entraîné une bagarre, heureusement sans conséquence grâce au sang-froid des émirs des deux côtés. Les coupables avaient été punis et la situation était redevenue calme.

Barmak laissa s’échapper un soupir en s’asseyant sur son suffah[^suffah], tirant un manteau de laine sur ses épaules. Il repensa à tous les pourparlers qui s’éternisaient et sentait bien l’hostilité des généraux mossouliotes, aleppins et damascènes à leur égard. Sous les formules aimables, il percevait la tension, car les Turcs prétendaient servir de leur mieux le pouvoir du calife et l’orthodoxie sunnite. Ils mettaient en avant leurs créations de nombreuses écoles pour assurer l’encadrement des étudiants, les donations en waqf[^waqf] qu’ils multipliaient, leur engagement dans le Jihad contre les infidèles. Mais pour lui leur ambition crevait les yeux, Nūr ad-Dīn et les siens s’appuyaient sur la religion pour justifier leur expansion.

Alors qu’ils n’étaient que des nomades va-nu-pieds il y a encore peu, ils se pensaient égaux, voire supérieurs, à des familles prestigieuses de Bagdad. Leur arrivisme déconcertait Barmak et le rendait furieux en même temps. Il savait qu’il était nécessaire de recourir à des peuples naturellement belliqueux pour faire les guerres, mais il répugnait à devoir les fréquenter. Il aurait fallu pouvoir se débarrasser d’eux après usage et ne pas leur concéder la moindre parcelle de pouvoir.

En fin d’après-midi, un messager rapide était venu informer al-Katib de la mort du nouveau sultan Sulayman Shah. Celui-ci, qui faisait déjà proclamer la khutba en son nom, s’était comporté de façon tellement odieuse qu’il s’était vite aliéné tout son entourage. On disait qu’il avait été empoisonné, l’assassin ayant pris soin de le punir par là où il péchait, en abusant du vin, et ce en pleine journée de ramadan. Barmak y voyait la dégénérescence de ces peuples turbulents, incapables de savourer les bonheurs de la vie avec parcimonie et expertise, ne goûtant les breuvages que dans l’ivresse et le moindre plaisir que dans la violence des passions. Mais au moins cela provoquerait-il peut-être le retour prochain à Bagdad de son maître Sufyan al-Katib.

Il était las de ces déplacements et n’aspirait qu’à retrouver sa magnifique demeure. Il ne se lassait pas d’y recevoir musiciens et poètes, au milieu d’objets commandés à des artisans de renom. Il avait fait spécialement aménager une partie du qa’a[^qaa] pour y héberger une immense volière où il gardait de splendides oiseaux aux plumages colorés et aimait entendre les exclamations de surprise et de joie de ses invités lorsqu’il leur dévoilait ses plus belles créatures.

Il appela pour indiquer qu’il prendrait son repas du soir dans la section de tente qui lui était réservée. Et il demanda à ce qu’on trouve un autre braséro pour la nuit, qu’un seul ne lui suffisait décidément pas. Il se pencha et sortit d’un petit coffre marqueté son nécessaire à écriture qu’il déploya devant lui. Puis il glissa hors de sa ceinture le mince carnet où il notait les vers qui venaient parfois à son esprit, inspirés par les grands auteurs. La journée passée, de nouveau pleine d’inconnu, lui remémora un de ses poètes favoris, dont les textes de temps à autre licencieux choquaient autant qu’ils ravissaient, al-Khayyam Nishabouri :

« *Contente-toi de savoir que tout est mystère :

la création du monde et la tienne,

la destinée du monde et la tienne.

Souris à ces mystères comme à un danger que tu mépriserais.* »^[Pièce 141 des Poèmes d’Omar Khayyam (v. 1048 - 1131), savant, mathématicien, astronome, poète et philosophe. Traduction de Franz Toussaint.]

Souriant pour lui-même tandis qu’il récitait les vers, il se versa une coupe de douqh[^douqh] bien frais. Il commençait à avoir faim et entama des petits gâteaux au riz, au safran, à la pistache et au miel en attendant qu’on lui apporte le souper. Il s’allongea nonchalamment sur les coussins, réfléchissant aux rimes du quatrain qu’il s’efforçait de composer. ❧

## Notes

J’avais toujours eu en tête de ne faire intervenir les Persans et les proches du pouvoir califal que tardivement, apportant un éclairage oriental à Hexagora, ainsi qu’un changement de focale. Les différents princes musulmans n’étaient pas forcément inquiets des croisés arrivés à la fin du XIe siècle et il me semblait intéressant de montrer que les chrétiens n’étaient qu’une des nombreuses composantes de la géopolitique multilatérale du Moyen-Orient médiéval. Barmak me fait en outre la joie de me permettre un jeu de mot tellement facile et puéril pour cette centième publication de Qit’a que je n’ai pu me l’interdire.

Le monde musulman est en proie à de nombreux changements en ce début de seconde moitié du XIIe siècle, avec la dislocation du grand sultanat seldjoukide qui fait écho à la déliquescence de la puissance califale abbasside et l’effondrement fatimide. Les peuples turcophones, seldjoukides, kurdes… constituent les nouvelles forces vives et s’insèrent peu à peu aux échelons principaux du pouvoir. Les populations inféodées restent bien évidemment les mêmes, comme cela se passe sur les territoires sous contrôle latin.

## Références

Bosworth Clifford Edmund, *The History of the Seljuq State. A translation with commentary of the Akhbar al-dawla al-saljuqiyya*, Routledge : Oxon & New York, 2011.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

