---
date: 2013-04-15
abstract: 1148. La Seconde croisade, qui mena au siège de Damas, entraîna une rupture dans les rapports entre l’Europe et le Moyen-Orient latin. Son échec marqua les esprits durablement et scinda les forces catholiques, bien loin de l’esprit croisé des premier conquérants. Lors d’une guerre, les combattants n’ont pas que l’ennemi auquel se confronter. Les adversaires sont légion, parfois parmi vos propres alliés et peuvent même se cacher en vous.
characters: Régnier d’Eaucourt, Ganelon, Aymar, Manassès de Hierges, Bernard Vacher, Élinand de Galilée
---

# L’ennemi intime

## Abords du village de Rabwé, environ de Damas, matin du lundi 26 juillet 1148

Le hennissement d’un destrier fit sursauter le chevalier, qui arracha en un geste vif sa couverture, prêt à bondir sur ses pieds. Les yeux hagards, il scruta les alentours. Tout semblait calme. Son valet était occupé à alimenter en bois un petit feu appuyé à un muret. Sous des arbres fruitiers, une corde avait été tendue pour y attacher les montures. Le soleil, déjà vaillant, perçait de ses flèches dorées la voûte d’émeraude. Le cliquetis des armes, le frottement des cuirs de sellerie, l’odeur des chevaux, s’efforçaient de vaincre les saveurs sucrées et délicates des jardins où les Latins avaient pris position, dans le faubourg occidental de Damas, l’oasis artificielle de la Gûta.

Rassuré, Régnier d’Eaucourt bailla, s’étira. La nuit avait été tranquille après une journée harassante. Il faisait partie d’une échelle[^echelle] menée par le connétable du royaume latin de Jérusalem, Manassès de Hierges. Tout le jour durant, la veille, ils s’étaient opposés à des forces jaillies de la ville, au nord du canal de Bânâs.

L’arrivée de contingents ennemis depuis les régions septentrionales les avait obligés à refluer vers leurs positions, non sans avoir durement malmené les musulmans. Il s’assit, frottant d’une main lasse son visage gris de poussière, orné d’une barbe vieille de plusieurs jours. Il sentait la sueur déjà couler dans son dos quand bien même il n’avait pas encore enfilé son haubert de mailles par-dessus sa cotte gamboisée[^gamboise].

Ganelon, son valet, vint s’enquérir de lui et repartit lui préparer une collation. Une agitation laborieuse parcourait les environs. Des hommes en chemise et chausses s’activaient à abattre les petits édifices des jardins, tailler les arbres, rebâtir les murets, de façon à fortifier leur position. D’innombrables canaux d’irrigation leur causaient de grands soucis pour déployer leur cavalerie et ils espéraient ainsi avoir un endroit d’où ils pourraient s’élancer contre les unités ennemies. Il entendit des soldats évoquer l’installation des troupes du roi Conrad sur une large zone herbeuse découverte, plus au sud.

Après avoir rompu le jeûne, le chevalier se dirigea vers une maisonnette érigée par un notable damascène dans son jardin d’agrément. Modeste par la taille, elle était néanmoins bâtie avec goût et des structures légères en bois décoré ornaient l’étage où le propriétaire des lieux devait venir prendre le frais au plus fort de l’été. Pour l’heure, elle abritait les quartiers du connétable du royaume, le maître de Régnier. Un auvent avait été tiré contre, de façon à accueillir quelques chevaux. Aucun soldat ne s’opposa à l’entrée de Régnier, chevalier connu et respecté.

Il monta directement l’escalier, laissant la petite cuisine où s’activaient deux valets. De si bon matin, l’odeur de plats épicés le saisit à la gorge. Ce fut donc en toussant qu’il acheva de grimper les marches menant à l’étage.

« Notre féal picard ! Qui corne bien curieusement son entrée ! »

Le connétable était assis sur un siège curule, face à une table où s’étalait son harnois de guerre, mailles et lames, casque et baudrier. Un verre de vin à la main, il était jusqu’alors en train de discuter avec deux hommes debout devant lui, un gros chevalier à la barbe fleurie, les yeux torves animant un faciès cassé, le crâne orné d’une coupe à l’écuelle, flanqué d’un maigre civil, habillé de toile fatiguée, poussiéreuse, occupé à gratter d’un pouce énervé le pavillon de son nez fort imposant. Tous deux tournèrent leurs regards vers le nouvel arrivant, qui se fendit d’un salut, les sourcils froncés de ne pas les reconnaître. Le connétable n’en dit pas plus et ne prit que quelques instants pour congédier le premier homme, puis il invita Régnier à se joindre à lui, en montrant le civil de la main.

« Aymar, que voilà, me porte nouvelles de la cité. Elle semble en proie à quelque panique, et c’est fort bon pour nous. Chaque jour nous renforce et les habitants en sont moult inquiets.

— Les contingents arrivés hier étaient pourtant de belle importance, sire. »

Le connétable fit un geste vague de la main.

« Renforcements tardifs ! Ils s’échinent à relever leur muraille fatiguée, ils craignent notre assaut imminent. »

Il fit une grimace.

« Ce qui me pose fort souci, ce ne sont pas combats en champ mais embuscades fort coûteuses en hommes et en moral. Encore cette nuit, plusieurs patrouilles ont subi violent assaut et je ne m’enjoie guère de voir les têtes de mes hommes orner les murs de la ville. »

Il se tourna vers Régnier, soudain rembruni.

« Ils vont et viennent à leur aise en ces jardins, familiers des sentiers, des détours de canaux et du tracé des murets. Nos coursiers se font souventes fois harceler, voire tuer. Il nous faut avoir nouvelles des différents points de l’assaut, sans quoi nous ne pouvons frapper adroitement ! »

Comme faisant écho à ses paroles, au-dehors une trompe sonna plusieurs coups répétés, attirant le regard des trois hommes vers la fenêtre, dont les volets délicatement ouvragés avaient été ouverts. Rapidement, l’effervescence gagna la cour. Un sergent cria :

« Aux armes ! Aux armes ! »

Régnier n’attendit pas les instructions, il savait ce qu’il avait à faire. Après un salut silencieux, il s’engouffra dans les escaliers, rejoignant à pas rapides son valet, qui devait avoir préparé son fourniment de guerre.

« Biens vaillants, pour des assiégés démoralisés ! » pensa-t-il en son for intérieur.

## Village de Rabwé, soir du lundi 26 juillet

Les flammes des lampes dessinaient des combats d’ombre sur les murs de toile. Avachi sur son siège, face à plusieurs autres qui attendaient, anxieux, ce qu’il allait leur dire, le connétable Manassès réfléchissait. Tous arboraient encore le harnois de guerre, haubert de mailles, baudrier et épée sur la cuisse. Les visages fatigués, collés de sueur et de poussière, n’avaient été que grossièrement lavés, et les rides qui creusaient leurs traits n’étaient pas que de vieillesse. Manassès tenait à la main un document de papier plié que le dernier arrivant, un jeune coursier aux yeux caves, la mine défaite, venait de lui porter. Il s’empara d’un gobelet de verre émaillé et, d’une gorgée, s’éclaircit la voix rendue rauque par l’inquiétude.

« Anur n’a pas menti, le soudan d’Alep[^nuraldin] nous court sus. Il a descendu l’Oronte à marche forcée et se trouve sûrement dès avant Hims. »

La colère faisait trembler ses mots. Il se tourna vers le clerc lui servant de secrétaire et le congédia d’un geste impatient. Il répéta le mouvement à l’adresse du messager, puis invita les trois hommes demeurés là à se rapprocher de lui. Il reprit d’une voix moins forte.

« Ce démon païen joue avec nous, je ne le crois pas assez fol pour risquer de voir les Turcs devenir maîtres chez lui. Mais leur arrivée mettrait à mal nos positions, de certes.

— Nous sommes fort exposés, et devons nous opposer à la citadelle depuis ces lieux, sire. Déplaçons-nous plus au sud, et assaillons la ville depuis là. Nous n’y serons pas proie facile, la ville sera entre nous et les Turcs.

— Le roi pourrait y consentir, mais Conrad et Louis[^conradlouis] ne verront certes pas cela d’un bon oeil. Ils frémissent déjà de colère de ne pas avoir encore pris la cité !

— Pour se la partir entre eux, comme si nous n’étions pas là. J’ai ouï dire qu’elle serait destinée au comte Thierry[^thierryalsace]. N’a-t-il pas déjà assez de terres en Flandre ?

— Silence, foin de ces jérémiades ! » tonna le connétable, agacé.

Il se leva, commençant à faire les cent pas tout en grondant et vociférant à mi-voix. La campagne ne s’avérait pas aussi facile qu’escomptée, malgré l’arrivée des forces armées venues d’Europe. Il se tourna vers Elinand, prince de Galilée, qui patientait tranquillement, le regard dans le vague. C’était un homme d’action, capable des coups de main les plus audacieux lorsqu’il était en selle, mais n’aimant guère à se perdre en longs palabres. Il était un partisan fervent de la reine Mélisende, dont Manassès était le plus sûr soutien.

Les deux hommes pouvaient parler à coeur ouvert de leurs projets pour éviter que le jeune Baudoin[^baudoinIIIc] ne prenne trop d’assurance dans cette campagne. En outre, en tant que seigneur des terres les plus proches de Damas, il était certainement un des plus capables militairement parmi les conseillers auxquels le connétable pouvait se fier.

« Que t’inspirent ces nouvelles, Elinand ?

— Je ne saurais dire que cela m’enjoie le cœur, sire, mais nous devons faire avec. Il est de meilleur choix de garder Anur à nos portes que de livrer Damas à Norredin. »

Le connétable plissa les yeux de ruse, reprenant place dans son siège.

« Que ferais-tu à ma place, donc ?

— Le sire Anur a offert de reconnaître notre suzeraineté sur Panéas, acceptons cela, déjà. Puis voyons ce que nous pouvons exiger outre. Il sera bien temps de revenir autre fois.

— Jamais le roi Louis n’acceptera de se retirer ! »

Le prince de Galilée soupira, excédé.

« Si l’ost de l’émir n’est qu’à quatre ou cinq jours comme le dit votre message, cela ne nous laisse guère de temps pour prendre la cité et la fortifier de nouvel. Nous n’arrivons à rien face à la citadelle et n’empêchons guère renforts d’entrer par le nord, ou l’est.

— Le sud alors, voila ton conseil ? »

Le chevalier haussa les épaules.

« De prime, il faudrait voir comment se présentent les choses. Ici, en les jardins, nous sommes protégés des ardeurs du soleil, et avons plein fourrage pour hommes et bêtes. Si nous faisons route, ce sera pour assaillir à grand force et prendre la cité vite, et bien, sans quoi nous serons en fâcheuse posture. »

Le dernier homme, Bernard Vacher, avança d’un pas, hésitant à formuler ce qu’il avait en tête. Il l’énonça avec difficulté alors même que cela prenait corps dans son esprit.

« Nous avons tenu conseil au moment des Pâques, sire. Aussi ne saurais-je croire que le seigneur Anur n’a pas eu vent de nos préparatifs. Il aurait pu appeler à l’aide bien plus tôt. Je crois qu’il nous préfère comme voisins que les Turcs. Rappelez-vous qu’il n’a pas cherché à détruire l’ost lors de la rébellion d’Altuntash[^altuntash].

— C’est oublier bien vite que tu as failli, et que nous n’y avons rien gagné.

— Peut-être qu’il a pu éprouver déjà le poids du joug turc sur ses épaules à cette occasion. Et ne l’a point goûté », répliqua l’autre, indifférent à la rebuffade.

Le connétable acquiesça sans conviction. Bernard Vacher était un grand connaisseur de l’âme des musulmans. Il avait mené de nombreuses ambassades auprès de Damas. Parfois, il se disait qu’il était peut-être trop familier avec eux, et des rumeurs circulaient comme quoi il ne dédaignait pas les dinars d’or que le seigneur de Damas distribuait avec prodigalité. Il gardait pourtant la faveur de Mélisende et du connétable, et ne s’était jamais montré déloyal.

Manassès avala encore un peu de vin et mit fin à leur entretien. Il demeura un long moment, seul, à méditer sur la décision à prendre. Il s’agissait de bien choisir parmi les différents chemins qui se traçaient devant lui. Il était là pour conseiller le roi et l’influencer au mieux de ses intérêts, mais il était surtout un fidèle de la reine. Si elle pouvait apprécier une victoire sur les Damascènes, elle ne priserait guère que son fils puisse en tirer gloire, et s’affirme alors indépendant de son pouvoir.

Organiser une défaite révoltait tout son être, mais contribuer à la réussite ne devait se faire qu’avec doigté, afin que le mérite en revienne à d’autres que Baudoin. Familier de la cour française, il se dit qu’il devrait souffler l’idée de faire mouvement au sud à quelque baron de l’entourage du roi Louis. Ou plutôt, moins suspect de complaisance envers les musulmans, à un seigneur ecclésiastique. Quelqu’un que jamais personne ne soupçonnerait de vouloir déplacer l’attaque pour une autre raison que de meilleures chances de succès. Il appela un valet : il allait demander audience auprès de l’évêque Geoffroy de Langres, un des intimes du fameux Bernard de Clairvaux[^saintbernard].

## Oasis sud-ouest de Damas, matin du 29 juillet

Le campement abandonné ressemblait au squelette de toile et de bois d’un grand animal décomposé. Déjà, telles des fourmis patientes, les unités musulmanes reprenaient sans faillir les jardins, investissaient zone après zone, s’abritant derrière les remparts durement assemblés pour se protéger d’eux. À l’ombre d’un bosquet de palmiers, Ganelon finissait de ramasser des sacs et entreprit de caler un blessé sur sa mule lorsqu’une voix forte tonna dans son dos :

« Fais mouvement, bougre de ribaud, ou tu finiras le col percé d’un trait sarrasin ! »

Le chevalier qui l’avait invectivé poussait sa monture transpirante, soulevant graviers et poussière dans son agitation. Son écu orné de fers plantés, de fûts brisés, attestait de la proximité des archers musulmans. Ils étaient les derniers à partir, l’amertume empoisonnant leur âme. Ganelon frappa de la trique son animal et avança au petit trot rejoindre le gros du bagage.

Un peu hébété, il avait le regard éteint des hommes qui croyaient avoir tout perdu et qui venaient d’être spoliés une nouvelle fois. Il avait marché des mois durant, vu ses amis mourir, renoncé à tout pour se trouver là, aux côtés de son seigneur Régnier d’Eaucourt. Tout cela en pure perte. La fière armée croisée avait échoué. Elle n’avait pas tenu plus de quelques jours, à tenter de prendre une misérable cité abritée dans ses jardins, au milieu du désert. Son âme se grossissait de fiel tandis qu’il s’éloignait, baissant la tête sous les cris, bousculant ses compagnons, trébuchant sur le sol caillouteux mille fois malmené devant lui. Qu’elle avait goût de poussière, dans sa bouche, la Terre promise. Il cracha une salive épaisse, fronçant les sourcils et tira un foulard devant son visage. Tandis qu’il trottinait, inquiet, hagard, affolé, il ne cessait de se répéter :

« Honnis soient ces Orientaux et leur pays maudit ». ❧

## Notes

L’assaut sur Damas lors de la Seconde croisade constitue toujours une gageure à expliquer. Les relations, bien que tendues, entre les deux puissances (Damas et Jérusalem), se maintenaient plus ou moins stables, et aucune des deux n’avait envie de voir les forces aleppines (Nur al -Dîn) venir si loin dans le sud. Néanmoins, la pression étrangère finit par pousser à l’attaque, peut-être relayée par le désir du jeune Baudoin III de briller au cours d’une campagne militaire dont sa mère serait forcément exclue. Toujours est-il que, même si on arrive à comprendre les raisons qui ont mené les armées chrétiennes dans l’oasis, les hypothèses demeurent nombreuses quant aux motivations ultérieures.

Après des combats très difficiles, où la victoire n’était jamais franche, le siège fut rapidement abandonné au bout de quelques jours, peut-être sous la menace des troupes nord-syriennes et en raison d’un manque de ravitaillement. La retraite se déroula en bon ordre et Damas versa par la suite régulièrement un tribut pour s’épargner un nouvel assaut.

Néanmoins, le désastre moral entraîna le délitement des forces croisées et aucun succès militaire d’envergure ne couronna la venue des deux puissants souverains européens (Louis VII de France et Conrad d’Allemagne), bénie par saint Bernard lui-même. Cet échec pesa lourdement sur les consciences, à comparer aux victoires de leurs prédécesseurs, et l’esprit de croisade ne fut désormais plus jamais le même.

## Références

Élisséef, Nikita, *La description de Damas d’Ibn ’Asâkir*, Institut Français de Damas, Damas : 1959.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972, p.93-182.

Nicolle David, Kervran Yann (trad., adapt.), *La Seconde croisade et le siège de Damas en 1148 - Un pari manqué*, à paraître.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.
