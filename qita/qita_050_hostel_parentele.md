---
date: 2015-12-15
abstract: 1148-1155. Au fil des ans, à l’ombre du vieux Temple de Jérusalem, le petit Raoul grandit. Ses parents laissent la place à l’aîné, ses sœurs se marient et un de ses frères part étudier au loin. En ses vertes années, comment voit-il le monde ? Quels sont ses espoirs ?
characters: Raoul le Scribe, Emlot, Père Thibault, Frogier de Jérusalem, Durant, Lancelot, Renaudin, Lorette, Gaude, Clarice
---

# Hostel et parentèle

## Jérusalem, parvis du Temple, fin de matinée du samedi 9 janvier 1148

Yeux fermés, tête en arrière, Raoul laissait les flocons caresser son visage, effleurer sa langue où ils se dissolvaient en une rosée glacée. Il entendait le crissement des pas autour de lui, dans la neige légère fraîchement tombée. Ses camarades s’affairaient à construire des cibles qu’ils mitraillaient ensuite de projectiles soigneusement façonnés. Il cligna plusieurs fois des yeux, fixant toujours l’immensité crayeuse au-dessus de lui. Puis frotta de la manche son nez rougi, souriant aux anges. La ville, habituellement bruyante, disparaissait dans la blancheur des cieux et des flocons et se terrait, silencieuse, camouflant la vie dans ses entrailles réchauffées à grand renfort de braseros et de foyers crépitants. Le petit garçon secoua la tête, débarrassa la capuche de sa chape du froid duvet, puis rejoignit ses compagnons de jeu.

Les cibles étaient en place, certaines ornées d’hastes aussi impressionnants que dérisoires. Le jardin environnant le Temple du Seigneur offrait quantité d’arbustes devenus, le temps d’un matin, pourvoyeurs d’armes en bois et de supports ennemis. Emlot, son aîné de plusieurs années, posa une main amicale sur son épaule :

« Tu n’as pas envie de mettre bas tous ces félons, frérot ? »

Raoul haussa les épaules. Il se sentait mélancolique, des sentiments confus l’habitaient depuis plusieurs jours, et il ne savait comment exprimer ce qui l’inquiétait. Son grand frère, qu’il percevait comme un géant débonnaire, arrivait toujours à le réconforter, à découvrir l’histoire qu’il aimait à entendre avant de s’endormir. Pourtant, il ne trouvait comment lui décrire son malaise.

« De coutume, tu goûtes fort les jeux de neige. Aurais-tu désir de rentrer ?

— Je sais pas. Non. »

Emlot s’accroupit face à lui, le tenant par les épaules.

« Me diras-tu seulement ce qui pèse sur ton cœur ? Est-ce à propos de Guy ? »

Raoul se mordit la lèvre, puis acquiesça doucement.

« Je me disais qu’il a jamais connu la neige…

— Tu crois qu’y a pas occasion de faire des bonhommes au parmi du Ciel ?

— J’ai jamais entendu le curé dire ça.

— Auquel cas il faudrait peut-être lui demander. J’encroie volontiers que Dieu te permet si simples plaisirs en son Paradis. »

Le garçonnet fit claquer sa langue, les sourcils froncés.

— Y va être perdu.

— Moult enfançons sont là-haut avec lui. Il saura se faire des amis. C’était fort amiable bébé, tu le sais bien. »

L’adolescent se releva et soupira. Leur petit frère était mort de fièvres soudaines à la fin de l’automne, alors qu’il n’avait pas trois ans. En quelques jours, il s’était éteint et avait été porté en terre aux côtés de leurs ancêtres, dans les reliefs à l’orient de la ville. De telles cérémonies pour des nourrissons, de très jeunes enfants, étaient malheureusement fréquentes, répandant l’angoisse dans les familles.

« Nous pourrions faire un joli bonhomme et ensuite prier la Vierge pour qu’elle le montre à Guy. Qu’en dis-tu ? »

Un timide sourire plissa les joues rosies de froid.

« Je vais aller quérir jolis cailloux pour les yeux alors ! »

Puis sous le regard attendri de son frère, Raoul s’élança vers les escaliers, en quête de pierres brillantes, ou colorées, comme il s’en trouvait parfois au pied de l’ancienne église. Emlot toussa pour s’éclaircir la voix et chercha un tas de neige qui n’ait pas été encore piétiné ou dévasté par la horde enthousiaste d’enfants. Puisse Dieu leur faire connaître à tous de nombreux autres hivers, songea-t-il en se mettant en marche.

## Jérusalem, cloître du *Templum Domini*, matin du mercredi 6 septembre 1150

« *Alpha Beatus Creator Deus Eternitas*… »

Les petites voix aigrelettes ânonnaient avec sérieux, si ce n’est enthousiasme, tandis que la férule du chanoine désignait les différents signes peints sur une large pièce de cuir. Il hochait la tête avec emphase, le visage fermé et austère malgré la malice pétillant dans ses yeux clairs. Il savait qu’il ne faisait pas bon montrer trop de complaisance à une bande d’enfants quand on s’efforçait de les éduquer. Lorsque toutes les lignes furent énumérées, avec plus ou moins de bonheur, le clerc félicita ses élèves et pointa de nouveau la première lettre.

« Qui d’entre vous connaît un mot ou un texte commençant par cette lettre. Parmi les plus petits… »

Son regard se porta aux premiers rangs, où se tenaient les plus jeunes. Sa question provoqua de grands écarquillements d’yeux, des bouches ouvertes et des mordillements de joue sans pour autant générer quoi que ce soit de valide. Peu à peu, il se tourna vers les plus âgés, parmi lesquels se trouvait Raoul, sautillant sur son escabeau, impatient de lâcher la réponse. Le maître sourit et le désigna.

« Achard. C’est le nom du bébé de ma sœur. »

La sincérité de la réponse fit jaillir un sourire sur le visage du chanoine.

« De certes, mais j’attendais autre réponse, garçon. Quelqu’un saura-t-il parmi vous ?

— Moi !, échappa avec vigueur un petit blond au regard renfrogné.

— Alors, parle, nous t’écoutons.

— *Ave Maria, grâce à pleina, dominuste cum, bene dicte à tout*

— Juste, mon garçon. Encore que je ne sois certain que tu le possèdes parfaitement.

— Si, père Thibault. Je le sais fort bien, comme le *Paterne austère*, car c’est moi qui surveille la cuisson des œufs !^[Voir les notes de fin.] »

Le chanoine renifla, légèrement agacé. L’apprentissage du latin constituait une seconde étape dans l’enseignement, et bien des chrétiens étaient incapables de le comprendre, voire de le prononcer correctement. Ce qui ne lui rendait pas moins désagréables à l’oreille les déformations incessantes des formules écorchées.

« Nous répéterons vos prières ainsi qu’il sied à la fin du cours, garçon. Veille à bien entendre quand je prononce et à répéter avec soin.

— Moi je serai le parrain d’Achard pour son baptême, Lorette elle a dit. Je réciterai le *Credo*, précisa Raoul.

— Voilà bien sérieux office. Sais-tu qu’il apprendra de ta bouche cette prière essentielle entre toutes ? »

Raoul hocha la tête avec gravité, conscient de l’importance de la tâche qui lui était dévolue, sans pour autant en saisir les implications qu’y mettait le clerc.

« Saurais-tu nous montrer la lettre par laquelle commence le *Credo*, en ce cas ? Vu que tu as parlé sans en avoir licence… »

Un peu confus de se faire rabrouer, même gentiment, devant ses camarades, le garçon se leva et vint pointer du doigt, sans aucune hésitation, la troisième lettre de la première rangée. Le maître approuva d’un signe de tête silencieux et attendit qu’il soit retourné à sa place pour continuer. Il distribua aux plus petits de vieilles tablettes de cire fatiguées, ainsi que des stylets métalliques pour qu’ils s’exercent à tracer les motifs tandis qu’il s’occuperait des plus âgés.

Parmi ceux-ci, un grand nombre était capable de lire correctement, et une partie montrait des dispositions pour passer au latin. Il demeurait la question de savoir si leurs parents accepteraient d’en faire des clercs. Ils étaient en majeure partie fils d’artisans et de commerçants, de bonne naissance, mais pour qui le destin était décidé depuis longtemps. Ils hériteraient de l’activité familiale ou y apporteraient leur concours.

L’apprentissage des lettres dispensé par les religieux visait à compléter la découverte des chiffres faite en famille, de façon à commercer plus adroitement. Quasi jamais n’était-il envisagé d’en faire un érudit ou un serviteur de Dieu. Quelques exceptions demeuraient, lorsqu’un membre de la famille était lui-même tonsuré. Il avait parfois assez d’influence pour arracher à sa condition le jeune homme prometteur. Comme le chanoine Pépin, parrain d’un des aînés de Raoul, Guillaume.

Tibault l’avait eu comme élève jusqu’à son départ pour les prestigieuses écoles d’Europe. Chartres, Paris… Des noms qui sonnaient comme des chants angéliques aux oreilles du vieux tuteur, plus habitué à des langues râpant le latin le plus élémentaire. C’était sa plus grande fierté, et il n’avait plus eu, depuis près de dix ans, l’occasion de voir un de ses pupilles en si bonne voie. Il s’accommodait néanmoins fort bien de sa tâche, qu’il appréciait bien plus que les activités foncières et administratives que nombre de ses confrères devaient affronter. Au moins il avait le plaisir d’être confronté à de jeunes âmes innocentes, pétries de fraîcheur et de naïveté. Et si Dieu les avait faits maladroits à l’énoncé de la langue latine, cela l’incitait à redoubler d’efforts, en usant de la badine ou du pot de miel selon les caractères.

## Jérusalem, piscine de Siloé, soir du mardi 20 mars 1151

Un ciel gris chargé de nuages sales menaçait de déverser son fardeau humide à tout moment, mais Raoul n’en avait cure. Il était revenu du cimetière du Lion sans ouvrir la bouche et s’était esquivé avant de rentrer à la maison. Des familiers, des amis, y seraient reçus pour évoquer une dernière fois la mémoire de Rose. Sa mère. C’était une femme âgée, il le savait, mais cela ne rendait pas sa douleur moins prégnante. Il l’avait vue s’éteindre peu à peu, devenue immobile avant de se faire voler la parole, pour finalement quitter ce monde sans un mot.

Il refusait de garder d’elle un tel souvenir. Elle était généralement pleine d’entrain, affrontant les corvées sans fatigue, et les coups du sort sans se plaindre. Elle insufflait la vie dans leur maison, savait consoler d’un sourire ou réprimander d’un regard.

La religion n’était pas d’un grand réconfort pour un gamin d’une dizaine d’années, et il ruminait son malheur en jetant des pierres dans le bassin de Siloé, adossé à une des colonnes du portique.

« Tiens, t’es là, le tanneur ? »

La voix qui l’avait arraché à sa rêverie mélancolique était celle de Frogier, un fils de négociant spécialisé dans les peaux. Il traitait beaucoup avec la famille de Raoul et les deux enfants, qui avaient le même âge à quelques mois près, appartenaient à la même bande depuis des années.

« J’ai appris pour ta mère. Désolé. »

Il vint s’asseoir à côté de son camarade et entreprit de s’associer à ses lancers de cailloux.

« Tu veux pas venir avec moi ? Les frères de la Milice font entrer leur troupeau. Y’a des milliers de bêtes, pour le moins!

— Ils sont déjà de retour des pâtures ?

— L’Annonciation est pas si loin. Et pis y se murmure que la trêve avec Damas pourrait bien finir ce printemps. »

Pour ces jeunes gens de Jérusalem, la guerre était familière, sans qu’ils aient eu à la subir directement. Ce n’était que péripéties qui touchaient les puissants, et parfois une connaissance malchanceuse qui avait croisé le chemin de la mauvaise troupe. La plupart du temps, ils n’en avaient qu’une idée floue et confuse, tout en estimant que cela concernait avant tout ceux qui faisaient des armes leur profession. Pour l’attrait de l’aventure tout autant que le désir des richesses selon certains.

« On verra peut-être bel étendard, allons-y ! » lança soudain Raoul.

Ils galopèrent rapidement sur le flanc oriental des reliefs, en direction de la porte au débouché de la vallée du Cédron. Avant même d’y parvenir, ils entendirent le raffut et goûtèrent la poussière des milliers de sabots qui frappaient le sol rocailleux.

« Comment font-ils pour que tout tienne en leur castel ?

— On dit qu’ils ont le Temple du roi Salomon.

— Ouais, et même y conservent son trésor d’or et de soie, rétorqua Raoul, sarcastique.

— Nan, sérieux. Même que mon frère y est déjà entré. C’était le palais du roi Godefroy, il est immense. »

La richesse et la réputation des frères de la Milice du Temple, qu’on commençait à appeler les Templiers, n’avaient d’égale que la curiosité dont ils faisaient l’objet. En quelques décennies, ils étaient devenus une puissance majeure du royaume, et leurs rangs comptaient parmi les plus vaillants combattants. Leur renommée faisait parfois briller d’espoir les yeux des rêveurs, comme ceux de Frogier.

« Plus tard, je goûterais fort de combattre parmi eux, lance au poing et heaume en chef.

— Aucune chance à ça. Tu y finirais responsable des latrines, au mieux. Il faut être chevalier pour tenir le glaive en leur sein.

— On m’a dit qu’ils acceptaient qui voulait les rejoindre.

— Peut-être bien, mais il me serait surprise que ton vieux voie ça d’un bon œil. »

Frogier soupira longuement puis cracha dans la poussière, les yeux toujours rivés sur le vaste troupeau qui n’en finissait pas de défiler en contrebas.

« N’empêche, un jour, je porterai le blanc mantel. »

Raoul jeta un coup d’œil jaloux à son ami. Malgré sa cotte usée et recousue, ses chaussures fatiguées, il enviait cette certitude. Lui aussi rêvait de grands espaces, de voyages et de hauts faits. Mais il n’arrivait jamais à franchir le pas, ni même exprimer à voix haute ces désirs qu’il pensait coupables. Il était fils de tanneur, et n’osait souhaiter autre chose pour son avenir. Sa mère seule aurait su comprendre et encourager ses timides initiatives. Mais elle gisait désormais dans un linceul, couverte de terre froide, à quelques lieues de là. Et lui finirait donc simple travailleur dans la tannerie familiale, comme la plupart de ses frères.

## Jérusalem, quartier des tanneurs, veillée du lundi 4 avril 1155

La chaleur de la journée s’était évanouie avec les derniers filaments carmin du ciel et on s’était empressé de fermer les volets de bois. La clameur des rues laissait la place aux rumeurs domestiques, le chant criard des porteurs d’eau avait disparu au profit du murmure des berceuses enfantines. Dans la vaste pièce chaulée au sol de brique, les senteurs d’huile d’olive des lampes s’ajoutaient aux relents du repas, gros pain de froment rassis, potage épais et fromage fort.

Autour de la grande table, c’était dorénavant Durant qui tenait le haut bout. Il ne restait que la fratrie, une partie d’entre elle du moins. Emlot était assis à sa droite, à côté de Raoul, face à Lancelot et Renaudin.

Les bancs paraissaient désormais bien vides. Avec le mariage de Lorette, puis celui de Gaude, l’année précédente, ils s’étaient résolus à embaucher du personnel domestique. Deux sœurs, vieilles filles trop pauvres pour être dotées, s’occupaient de leur maison, de leur feu et de leurs vêtements. Elles partageaient leur repas, sans jamais se risquer à prononcer un mot, courbées par les ans et la vie difficile. Puis elles s’esquivaient en silence, abandonnant la famille pour la veillée.

Ce soir, malgré le décès récent du père, l’ambiance était plutôt festive. Ils avaient reçu une lettre de Guillaume, parti des années plus tôt en France se former auprès de grands maîtres. Emlot et Durant étaient seuls à s’en souvenir, mais le destin exceptionnel de l’étudiant rejaillissait sur chacun, qui s’en faisait un héros à sa mesure. Et, pour ajouter à cela, Durant avait cédé à l’insistance d’Emlot et Raoul allait commencer d’ici peu son apprentissage chez un écrivain de la Cour des Bourgeois du roi.

Durant renâclait, comme leur père avant lui, à se séparer d’un travailleur gratuit, mais il avait fini par admettre qu’un frère bien introduit dans l’administration royale constituerait un atout pour la famille. Raoul avait appris la nouvelle ce soir, lors du repas, et l’avait accueillie avec allégresse. Puis il s’était vu confier la tâche de leur lire à tous la prose de Guillaume, tandis qu’ils dégustaient un vin légèrement aromatisé en guise de digestif, avec des biscuits secs à la cannelle.

Raoul déplia le document avec soin, comme s’il craignait de l’abîmer. Ils ne recevaient que rarement des lettres, sans toujours savoir si c’était dû à la faible fréquence des envois ou à la disparition des missives sur le trajet. Il connaissait néanmoins bien l’écriture élégante de Guillaume, ses formules ampoulées et ses constructions alambiquées. Érudit, il avait été façonné par ces années à étudier dans l’ombre des cloîtres et cela transpirait à travers ses mots. D’ailleurs, parmi ses frères, nul ne doutait qu’il finirait abbé voire évêque. Son parrain, le chanoine Pépin, ne ménageait pas ses efforts pour soutenir la carrière à venir du jeune homme.

« Il parle de quitter le royaume de France, qu’on lui conseille d’aller suivre les enseignements de maîtres en droit civil à Bologne…

— Bologne, c’est où, ça ?, grogna Renaudin. Y va encore falloir lui envoyer de quoi se payer long voyage !

— Chaque besant envoyé à notre frère est utilisé à bon escient, le réprimanda Durant. Tu saurais cela si tu avais pris la peine d’étudier plus sérieusement.

— Tu parles, y’avait plus guère de monnaie pour m’envoyer à Châtre ou Paris, comme lui, quand j’ai été en âge.

— C’est Chartres, la ville où il a commencé, répliqua Durant avec dédain. Ferme donc le vidoir qui te sert de bouche. »

Puis il fit signe de la main à Raoul de continuer la lecture. Guillaume détaillait les maîtres dont il découvrait les enseignements, leur donnant des indications sur les cursus qu’il suivait, les préceptes qu’on lui inculquait. Il tentait de les rendre compréhensibles, sans se rendre compte qu’ils n’étaient que bien peu à appréhender autant de savoir conceptuel.

« Il nous remercie de l’avoir prévenu du décès de père, il prie pour son âme chaque jour et a fait dire des messes pour nos parents. »

Puis il esquissa un sourire taquin et lança une œillade à son aîné :

« Il pense aussi qu’il faudrait que tu ne perdes pas de temps avant de te marier. Qu’un hostel n’a pas d’âme sans épouse qui s’en occupe. Seulement, selon lui, la difficulté est de trouver une femme qui ait les bonnes qualités et le tempérament qui sied à sa fonction. Il nous conseille d’aller en discuter avec le chanoine Pépin.

— Un chanoine pour conseiller en femmes, notre frère est bien un clerc » se gaussa Emlot.

La saillie fit rire la tablée, et Raoul allait enchaîner lorsque Durant confirma ce que chacun savait sans jamais en parler.

« J’ai déjà pourpensé la question de toute façon, et j’ai bon espoir de conclure accord prochainement.

— Alors ce sera Clarice ? risqua Emlot.

— Si son père en est d’accord, oui. »

Chacun acquiesça à sa façon, sachant que sa vie serait bouleversée. Une maîtresse de maison allait prendre les clés du lieu, veiller sur le cellier et les linges, assurerait la lignée. Eux tous, d’héritiers possibles, ils deviendraient des auxiliaires, plus ou moins acceptés, appréciés.

Raoul était heureux de ne pas avoir à rester ici, une autre carrière s’offrant à lui. Demeurer dans l’hôtel familial équivalait à renoncer au mariage, très certainement, ainsi qu’à espérer une descendance à qui léguer ses biens. Lequel de ses trois autres frères endurererait un tel destin ? ❧

## Notes

Au Moyen Âge, il était de coutume de réciter trois *Ave Maria* et trois *Pater Noster* pour estimer le temps de cuire un œuf à la coque. L’apprentissage de ces deux prières se faisait donc assez rapidement en milieu domestique, sans que la signification en soit bien claire.

Les termes d’hôtel et de parentèle désignaient les personnes de la famille qui, respectivement, habitaient sous le même toit et qui étaient en dehors. Tout cela était regroupé sous le concept de la familiarité. Ce terme pouvait englober des individus avec qui les liens de sang étaient assez distants, voire inexistants, pour peu qu’il y ait peu ou prou des relations symboliques (belle-famille, parrains/marraines, etc.) ou que la personne soit considérée comme faisant partie de la maisonnée (comme un serviteur, sa descendance, etc.).

## Références

Alexandre-Bidon Danièle, « La lettre volée. Apprendre à lire à l’enfant au Moyen Âge » dans *Annales. Histoire, Sciences Sociales*, 1989, vol.44, no 4, p.953-992.

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Boas Adrian J., *Domestic Settings. Sources on Domestic Architecture and Day-to-Day Activities in the Crusader States*, Leiden et Boston : Brill, 2010.
