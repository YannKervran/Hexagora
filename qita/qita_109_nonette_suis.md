---
date : 2020-11-15
abstract : 1129-1161. Iveta, de très haute naissance, a choisi une vie monastique, à l’abri des murs d’un couvent. Malgré cela, elle ne saurait demeurer à l’écart du siècle…
characters : Iveta de Béthanie, Mélisende de Jérusalem,  Sybille de Flandre, Eudeline
---

# Nonette suis

## Jérusalem, palais royal, après-midi du vendredi 24 mai 1129

Le petit jardin embaumait les jacinthes, offrant l’abri d’une tonnelle où le jasmin prospérait. Quelques massifs de roses, amoureusement entretenus, s’épanouissaient sous le soleil déjà chaud.  Assise sur un banc, une fillette attendait, l’air grave. Vêtue d’une simple robe de laine grise, les cheveux noirs nattés avec soin, elle regardait ses pieds se balancer entre l’ombre et la lumière, fredonnant paisiblement. À ses côtés, une poupée de chiffon habillée d’une superbe étoffe de soie contemplait de ses yeux de bois les abeilles occupées à butiner.

Une porte grinça, sortant l’enfant de sa torpeur. Elle tendit le cou afin de voir dans la pénombre du petit couloir bordé de colonnes : c’était sa sœur aînée, la princesse Mélisende, qui venait à elle, environnée comme toujours de quelques suivantes, servantes ou filles de baron. Elle se déplaçait toujours avec un certain aréopage qui soulignait son importance, concédant un intérêt poli aux pépiements qui l’entouraient sans cesse. Mais son regard était rivé sur Iveta, qui l’attendait patiemment.

La fillette sauta sur ses pieds et fit une révérence des plus formelles. Mélisende lui accorda en retour un sourire gracieux qui illumina son visage tandis qu’elle s’avançait pour serrer contre elle l’enfant qu’elle n’avait pas vu depuis des semaines.

« Ma chère Iveta. Chaque fois que je te regarde, j’ai l’impression que des années ont passé, à te découvrir si bel !

— Merci, ma sœur.

— Je t’en prie, foin de ces balivernes qu’on a a dû te mettre en tête. Je vais prendre époux et être couronnée reine, de certes, mais je demeure avant tout ton amie. Rien n’a changé entre nous. »

Iveta adressa un sourire plein de fossettes à son aînée et se laissa approcher plus gracieusement, non sans avoir attrapé sa poupée. Sans un mot, d’un simple geste de la main, Mélisende chassa ses compagnes et vint s’installer à l’ombre des feuillages.

« As-tu encontré celui qui sera à mes côtés sur le trône[^foulque] ?

— Pas encore. »

La fillette hésita un instant.

« Il est si vieux qu’on le dit ?

— Pas tant, se mit à rire Mélisende. Mais il a déjà fort bataillé et gouverné.

— Il est vrai qu’il a renoncé à devenir roi d’Angleterre ?

— Pas du tout ! Il a renoncé à ses terres là-bas, en Anjou, mais c’est son aîné qui a épousé celle qui sera reine. »

La jeune fille hocha la tête gravement. Puis se tint coi un long moment avant de reprendre d’une voix faible, hésitante.

« Il y aura un prince pour moi aussi ?

— Pourquoi t’en inquiéter, mon enfant ? C’est souci de père, et le mien.

— Matilda dit que je vais devoir épouser mon cousin Joscelin[^joscelinII] vu que c’est le seul qui reste de mon rang.

— Matilda n’est qu’une niaise qui n’y connaît rien. Nous te trouverons bon mari en France, en Normandie ou chez les Germains. »

Iveta se renfrogna, lissant la tenue de sa poupée de façon machinale.

« Pourquoi je ne pourrais pas demeurer, comme mère, en l’abbaye Sainte-Marie ?

— Ne dis pas de sottises. Elle était fatiguée et âgée, elle a souhaité y finir ses jours. Mais elle a fait son devoir avant cela.

— Prier pour vous, mes sœurs, et demander aux saints et aux saintes de protéger le royaume, ne voilà pas une tâche digne de moi ?

— Tu ne sais pas encore, mon enfant. Tu es trop jeune. »

Iveta acquiesça poliment, mais sans se défaire d’une moue éloquente. Elle dévisagea sa poupée, traçant du doigt les motifs sur le tissu chatoyant.

« J’ai souvenance d’un gentil prince, quand j’étais petite. Usamah. Il riait à tout instant, heureux de ses chasses ou du bon temps qu’il prenait. Pourquoi ne pourrais-je pas vivre selon mes désirs ainsi que lui ?

— S’il était prince, il avait de certes moult devoirs, Iveta. »

Mélisende étudia le profil de sa jeune sœur, qu’elle considérait presque comme sa fille. Si leur différence d’âge n’était pas si importante, en tant qu’aînée, elle avait dû grandir vite. Et Iveta avait servi d’otages quelques années plus tôt, le temps que leur père paie sa rançon, ce qui les avait séparées de longs mois. Mélisende acceptait ce difficile destin de princesse, et l’embrassait volontiers, mais elle conservait une pointe d’affection pour sa benjamine, qu’elle espérait protéger des aspects les plus rudes de la vie.

« Entrer en religion te fermera les portes du monde, ma mie. Est-ce bien l’inclination de ton cœur ?

— Je me sens bien plus à mon aise priant au parmi des sœurs que lorsque je vois père gouverner les barons de la Haute Cour. Je n’ai que faire de porter belles robes ou d’avoir un époux. Pour cela le Christ me semble le meilleur choix à faire. »

Mélisende acquiesça gravement. Puis elle se releva, épousseta machinalement sa tenue qui n’avait pas un pli et sourit à Iveta.

« Si ton désir persiste de prendre voile, j’en parlerai à père. Laisse-moi du temps pour trouver le bon moment. Et, quoi qu’il arrive, tu ne saurais demeurer simple nonette dans l’ombre d’une abbesse, fut-elle mère de Saint-Anne ! »

## Jérusalem, palais royal, matin du mardi 8 octobre 1146

La chambre de la reine était baignée du soleil matinal, faisant resplendir les riches couleurs des tapis, des étoffes, briller les bronzes et les bois cirés. Les fenêtres ouvertes laissaient pénétrer les bruits de la cité, du palais, des pèlerins tout proches. Assise sur un magnifique coffre peint au pied du lit, Ivetta détonnait par sa sobre tenue : une simple robe de laine sombre et un voile clair qui cachait sa chevelure. D’un air absent, elle feuilletait le somptueux et délicat psautier que Foulques avait fait réaliser pour son épouse quelques années plus tôt. Face à elle, la reine lisait quelques courriers, sans y prêter grande attention.

« Ce n’était pas si mauvais époux, tout compte fait… »

La voix de Mélisende semblait moins autoritaire dans ce cadre domestique, mais son ton gardait un aspect péremptoire. Iveta y était habituée et ne s’en formalisait pas. De toute façon, elle partageait fréquemment les vues de celle qu’elle considérait comme une mère, plus que comme une sœur.

« Il t’aurait sûrement richement dotée. Mais il n’avait jamais envisagé que Matilde puisse lui survivre et que tu ne deviennes pas abbesse de son vivant. Pauvre Foulques… c’est bien de lui, de penser toute femme en inférieure.

— N’est-ce pas méprise qu’il a souventes fois commise ? s’amusa Iveta, un sourire complice sur les lèvres.

— C’est l’erreur que tout homme fait » soupira Mélisende.

Elle reposa un des feuillets.

« En dehors du père abbé de Citeaux…[^saintbernard]

— As-tu nouvelles de lui ? Du pérégrinage du roi Louis ?

— Pas de lui directement, mais il s’active fort à prêcher pour nous. Le très Saint-Père a fulminé de nouvel la bulle apostolique qui appelle à l’aide. L’espoir est grand, si les barons de France et d’Aquitaine viennent nous porter secours. »

Iveta pinça les lèvres, admirant le ciel bleu extérieur un instant.

« Je ne saurais dire si cela m’enjoie ou m’horrifie, d’apprendre que le fer et le feu vont encore dévaster le pays.

— Nous ne pouvons laisser nos ennemis détruire ce qui importe à nos yeux !

— Je prie souventes fois pour que le Seigneur leur décille les leurs.

— Mieux vaut le faire à l’abri de solides murailles, ma sœur. Garder le siècle en deçà ne se peut que si courtines et tours nous protègent. »

Iveta opina doucement. Elle regrettait de temps à autre de ne pouvoir vivre comme une simple moniale. Elle enviait alors Eudeline, sa prieure, qui ne sortait que fort peu de la clôture et s’occupait avant tout de la vie réglée des religieuses. Chaque jour les mêmes prières, les mêmes tâches, en une perpétuelle et réconfortante récapitulation. Mais elle était fille de souverain, abbesse et confidente de la reine de Jérusalem. Elle ne pouvait se retirer du monde aussi commodément qu’elle en avait le désir.

« Il te faudrait faire visite du monastère, découvrir comme toutes choses avancent bien, d’ailleurs. Tu n’es pas venue depuis des semaines.

— Ce serait avec grand plaisir. Mais trop de requêtes à examiner, de soucis à traiter, qui me retiennent ici ou qui me font voyager en des lieux où je n’ai guère envie d’aller. La mort de Zengi[^zengi] nous offre beaux espoirs, mais Baudoin n’est que garcelet à peine dégrossi, tout juste bon à trousser tout ce qu’il goûte. Un vrai bélier en rut !

— Manassès disait ce tantôt qu’il apprécie les choses de la guerre et y montre quelque aptitude.

— Cela ne suffirait à faire de lui un roi. Il se sait destiné à cela depuis la naissance. J’ai eu moins de temps pour cela que lui ! »

Mélisende se replongea dans ses lectures, laissant Iveta seule dans son examen du livre pieux. Bien que supérieure d’un riche couvent, elle n’avait pas de tels ouvrages personnels, même plus modestes d’apparence, préférant doter l’abbaye en elle-même de tous les objets liturgiques et ostentatoires qui louaient la grandeur divine. Elle avait entendu parler de l’austérité prônée par le dirigeant de Citeaux, qui raillait le décorum usuel des bénédictins, mais il lui semblait logique de célébrer le Tout-Puissant avec les réalisations les plus magnifiques qui soient. Peut-être était-elle trop habituée au luxe oriental pour envisager que Dieu n’y soit pas sensible.

« N’a-t-on pas de nouvelles de l’ambassadeur damascène ? »

Mélisende retint un sourire. Malgré son retrait du monde, Iveta conservait quelques souvenirs infantiles de sa période de captivité à Shayzar. Et son affection, distante, un peu fantasmée, mais réelle, avec le  prince munqidhite demeurait un de ces fils ténus.

« Je n’imagine pas qu’on le reverra de sitôt. Unur n’est guère tendre avec les félons. Il serait toujours au Caire, à ce qu’on dit. »

Mélisende leva la tête, soudain songeuse.

« Il nous faudrait d’ailleurs envoyer quelque émissaire à Damas, rappeler à Unur combien nous sommes attachés à son amitié… »

Elle attrapa une tablette de cire et entreprit d’y noter quelques idées, laissant Iveta de nouveau dans ses pensées, plongée dans les détails de la miniature présentant la résurrection de Lazare, Marie et Marthe à ses pieds.  Si Iveta se sentait proche de la première, à n’en pas douter Mélisende s’activait autant que la seconde.

## Béthanie, couvent des Saint-Lazare-Marthe-et-Marie, après-midi du lundi 7 octobre 1157

Iveta relisait la charte d’échange de terres avec attention. Le scriptorium en avait fait copie, et elle y avait fait apposer son sceau le matin même. Elle était de temps à autre surprise de l’attrait que représentait sa congrégation et du volume des dons, offrandes et légations qui lui étaient faits. Il était vrai que, situé sur le trajet entre le Jourdain, Jéricho et Jérusalem, aucun pèlerin ne manquait l’occasion de s’arrêter sur le tombeau qui avait accueilli un miracle du Christ. Mais il lui semblait que tout cela était irréel quand elle se trouvait là, dans la simplicité de sa cellule qu’elle avait souhaité dépouillée dans son mobilier et peu ornée sur ses murs. Son seul luxe était la vue qu’elle avait sur le cloître, minuscule paradis terrestre où les massifs de fleurs et la verdure des arbres abritaient de nombreux oiseaux.

Elle en était à vérifier la liste des témoins quand on frappa à sa porte. Eudeline entra sans bruit, suivie par une visiteuse en tenue de novice. Iveta reconnut immédiatement Sybille, sa nièce, la femme du comte de Flandre. Celui-ci s’était de nouveau croisé et son épouse l’avait cette fois accompagné. Mais elle avait choisi de ne pas participer aux campagnes militaires, préférant le calme du monastère pour y attendre le retour de son conjoint. Sybille était la fille d’un premier lit du défunt roi Foulques, et donc la nièce d’Iveta. Mais elle était un peu plus âgée, de quelques années.

« Sybille sollicite audience, mère Iveta. Comme vous m’aviez indiqué que vous gardiez votre porte ouverte pour de possibles entretiens, j’ai pris la liberté de venir directement avec elle. »

D’un sourire, Iveta acquiesça et invita Sybille à s’assoir face à elle. À peine eut-elle eu le temps de le faire qu’Eudeline avait disparu sans un bruit, tirant l’épaisse huisserie derrière elle.

« Tout va bien, j’espère, Sybille ? Il n’est nulle contrariété qui trouble ton séjour parmi nous ?

— Oh non, bien à rebours ! Et c’est de cela qu’il s’agit. J’aimerais savoir ce que vous penseriez si je devenais professe en vos murs. M’accepteriez-vous ? »

D’étonnement, Ivetta demeura coite un temps, ce que Sybille prit pour une réticence.

« Je suis lasse du siècle et n’aspire à rien de plus qu’au silence et à la prière. Et vous avez ici si bel endroit, épargné de la fureur du monde.

— J’en serais très heureuse, ma mie, assurément ! Mais en avez-vous parlé à votre époux ? Sans son accord, rien ne se pourra faire.

— Je voulais d’abord avoir votre assentiment, mais nul doute que mon cher Thierry approuvera. J’ai fait mon devoir, il a fils et filles en suffisance. J’ai même tenu le bayle[^bayle2] de Flandre un temps. Il me voue trop belle amitié pour m’opposer refus. De toute façon, il a bien plus appétit de terres ici que de ma présence. »

Iveta avait entendu des rumeurs comme quoi le bouillant comte de Flandre avait espoir de s’arroger le territoire de Césarée sur l’Oronte[^cesareeoronte] une fois la ville prise. Là où elle avait passé plusieurs années, lorsqu’enfant, elle avait servi d’otage le temps pour son père de payer sa rançon. Il pourrait être ironique qu’elle y retourne un jour pour y fonder un établissement religieux que dirigerait sa nièce. Elle jeta un regard vers la charte, posée entre elles.

« Je confesse que vous avoir à mes côtés serait grand bonheur. Il y a tant à faire ici !

— Si cela vous était possible, je préférerais n’être que modeste nonette. J’ai bien assez bataillé en ma vie, je n’ai plus goût à toutes ces choses de gouvernement. Prier me suffira pour les jours qui me restent.

— Si vous m’autorisez à vous demander qui-ci qui-là conseil, cela me conviendra tout à fait. Je comprends combien simple sceau pèse dans la main qui doit le manier. Il m’est souventes fois arrivé d’espérer m’en décharger. Mais Dieu en a voulu autrement.

— *Amen*. »

Bien qu’elle éprouvât une relative déception de ne pouvoir s’adjoindre Sybille pour les affaires du couvent, Ivetta était heureuse d’imaginer qu’elles partageraient leurs jours dans le monastère désormais achevé. Adolescente, elle avait pris plaisir à fréquenter cette jeune femme, venue avec son père loin de chez elle, dans l’attente d’un époux qui lui serait désigné. Elle se remémora alors combien Sybille avait apprécié la vie dans la Cité. Elle n’était d’ailleurs peut-être pas étrangère à l’attrait de la Terre sainte sur le comte de Flandre.

« Avez-vous eu quelque nouvelle du nord ? Le siège se déroule-t-il comme espéré ?

— Aucun message depuis le départ de la Bocquée. Je prie chaque jour que Dieu leur prête victoire.

— Nous demanderons au père de célébrer une messe pour appeler sur eux la miséricorde du Christ. »

## Béthanie, couvent des Saint-Lazare-Marthe-et-Marie, matin du jeudi 11 mai 1161

La basse-cour du monastère grouillait de valets et de serviteurs, de palefreniers et de gardes. Un baron en route pour la guerre n’aurait pas provoqué plus d’agitation. Au-dessus de cette effervescence, dans le couvent, Iveta donnait ses instructions à la prieure Eudeline qui la remplacerait une nouvelle fois. Pendant ce temps, Sybille supervisait les préparatifs personnels de l’abbesse. Son confesseur avait été prévenu et les bagages s’apprêtaient en concertation avec la drapière. Il fallait également organiser le secrétariat, qui devrait suivre et assurer la correspondance. Nul ne savait combien de temps Iveta s’absenterait.

Lorsqu’Eudeline sortit de la cellule, Iveta s’accorda un peu de relâchement dans sa posture et s’affaissa sur le lit. Elle avait veillé une partie de la nuit et n’avait pas encore pris de repas depuis que le messager était arrivé la veille au soir. Seule Sybille pouvait alors la voir ainsi dans son intimité, sans fard.

« Ne veux-tu pas un bouillon ou un léger gruau, ma mère ?

— Rien qu’à l’idée d’avaler la moindre bouchée, mon estomac se révolte. J’ai trop grande hâte de parvenir à Naplouse.

— Le voyage sera éprouvant, il serait plus raisonnable de l’affronter le ventre plein. »

Iveta accorda un sourire triste à sa nièce.

« Ma chère, ma très chère amie. Dieu t’a envoyée à moi pour m’éviter le désespoir… Fais ainsi que tu le désires, je ne suis qu’effroi et appréhension. Tant que je n’aurais pas la main de Mélisende en la mienne, je serai telle une de ces Vierges folles…

— C’est là bien le moindre. Mon angoisse à moi est partagée entre toi et la reine. »

Iveta se redressa, caressant de ses doigts décharnés la joue de Sybille.

« Ne crains rien pour moi. Ce n’est que mauvais moment à passer. Cela fait si longtemps que je l’appréhende.

— Est-elle si mal ?

— Le coursier a dit qu’elle s’est effondrée quand elle a appris que Baudoin avait évincé Constance de la régence de la princée ^[En 1161, Baudoin III choisit d’écarter sa cousine de la régence d’Antioche. Comme sa mère Alice, la sœur de Mélisende, avant elle, Constance était une femme entreprenante, cherchant à diriger la principauté en toute indépendance.]. Elle était déjà si faible, ce maudit garnement n’en aura donc jamais assez de lui faire tort !

— Ne succombe pas à la même colère que ta sœur !

— Oh, je suis bien moins sanguine qu’elle, mais je le trouve bien mal aimant avec celle qui l’a fait tel qu’il est. »

Sybille ne répondit pas, se contentant de caresser doucement la main de son amie.

« Quoiqu’il en soit, Antioche n’est pas la question pour le moment. Constance a l’âme si fort trempée que sa mère. Je dois veiller avant tout sur mon aînée.

— Nous serons prêts à partir avant sixte m’a assuré la cellérière. Elle a mandé quelques sergents depuis la cité, afin de nous faire escorte.

— Le coursier m’a dit que des chevaucheurs avait aussi été envoyés à Triple prévenir Hodierne[^hodierne]. Je crois que Baudoin y réside, après son séjour à Antioche et dans le nord. Si seulement il acceptait de se rendre au chevet de Mélisende…

— N’escomptez en rien cela, ma mie. Il est trop empli de colère envers elle, nul homme ne vient ainsi à résipiscence.

— Alors nous prierons aussi pour lui, car son âme est en bien grand péril de ne pas honorer une mère comme Mélisende. » ❧

## Notes

L’histoire médiévale s’attarde souvent sur les individus dominants d’une époque, et il est toujours malaisé de reconstruire le parcours, et encore moins la personnalité des plus modestes. Et parfois même de figures de premier plan, mais qui ont été occultés par les hasards des études et récits ultérieurs. On ne retient donc généralement des filles de Baudoin II que Mélisende, dont l’éclat ternit la visibilité de ses sœurs, d’Iveta en premier lieu. Celle-ci avait pourtant fait un choix de vie radical, pour n’être pas si rare au XIIe siècle.

Le fait de devenir religieuse lui permettait d’acquérir une autonomie et un pouvoir qui lui aurait été bien disputé si elle avait du se marier, l’histoire de ses sœurs le montre à l’envi. Par ses origines, elle a pu se hisser à un niveau d’influence certain, dont on peut voir la réalité par quelques signes, tel l’usage d’un sceau (elle est la seule femme en dehors de la reine à en posséder un à cette époque). En outre, elle dirigeait un établissement réputé parmi les pèlerins, dont le souvenir aujourd’hui ne rend pas compte de l’importance d’alors.

Par ailleurs, le lien avec Usamah ibn Munqidh est une pure invention de ma part, mais je me suis imaginé qu’Iveta ayant passé plusieurs mois comme otage à Shayzar alors qu’elle était très jeune, elle avait pu y apercevoir le prince. Et le retrouver des années plus tard, quand il représentait Unur de Damas à la cour de Jérusalem.

Le titre est une référence au texte de Christine de Pisan « Seulette suis » :

>Seulette suis et seulette veux être,  
>Seulette m’a mon doux ami laissée,  
>Seulette suis sans compagnon ni maître,  
>Seulette suis, dolente et courroucée,  
>Seulette suis en langueur malaisée,  
>Seulette suis plus que nulle égarée,  
>Seulette suis, sans ami demeurée.

>Seulette suis à huis ou à fenêtre,  
>Seulette suis en un angle mussée,  
>Seulette suis pour de pleurs me repaître,  
>Seulette suis, dolente ou apaisée,  
>Seulette suis, rien n’est qui tant me siée,  
>Seulette suis en ma chambre enserrée,  
>Seulette suis, sans ami demeurée.

>Seulette suis partout en tout est.  
>Seulette suis où j’aille et où je siée  
>Seulette suis plus qu’autre rien terrestre,  
>Seulette suis de chacun délaissée,  
>Seulette suis durement abaissée,  
>Seulette suis souvent toute éplorée,  
>Seulette suis, sans ami demeurée.  

>Princes, or est ma douleur commencée :  
>Seulette suis de tout deuil menacée,  
>Seulette suis plus sombre que morée  
>Seulette suis, sans ami demeurée.

Texte extrait du site du Centre National pour la Poésie : https://www.printempsdespoetes.com/Seulette-de-Christine-de-Pisan


## Références

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Jordan, Erin L., « Hostage, Sister, Abbess: The Life of Iveta of Jerusalem », Medieval Prosopography, vol. 32, 2017, pp. 66‑86.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », Dumbarton Oaks Papers, 1972, vol. 26, p. 93‑182.



