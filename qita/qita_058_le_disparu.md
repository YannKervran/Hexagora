---
date : 2016-08-15
abstract : 1158. Tout en vaquant à ses occupations quotidiennes, Constantin, évêque de Lydda s’efforce d’élucider le mystère d’une disparition.
characters : Évêque Constantin, Josce, Seguin de Brebon, Maugier du Toron, Heimart
---

# Le disparu

## Lydda, chambre de l’évêque, matin du mardi 23 septembre 1158

L’évêque Constantin était assis à sa petite table face aux jardins. La fenêtre largement ouverte laissait entrer la chaleur naissante de la journée, accompagnée des effluves capiteux des fleurs et fruits cultivés en contrebas. En arrière-plan, les bruits du chantier de la basilique commençaient à se faire entendre. Une brise de mer agonisante apportait depuis l’occident quelques nuances salées, parfois un peu de fraîcheur. L’évêque se dit que bientôt la récolte du coton débuterait dans les campagnes de Saint-Jean d’Acre.

Bien qu’il soit relativement âgé, Constantin demeurait en assez bonne forme. Un physique robuste, empâté avec les ans, lui donnait l’allure d’un vieux chevalier. Il l’avait acquis en selle, en armure, auprès des saintes reliques qui accompagnaient généralement le roi de Jérusalem dans ses déplacements. Ses mains avaient aussi souvent servi à bénir les étendards et les combattants de la foi qu’à tenir les rênes de sa monture. Il n’était pas pour autant un guerrier, incapable de la moindre stratégie militaire, qu’il abandonnait à d’autres. Son devoir était de garantir aux troupes l’appui divin dans leurs affrontements, pas de manier une épée.

De fréquents voyages, à la tête de sa propre compagnie d’hommes en armes, avaient tanné son visage, creusé de nombreux sillons dans sa face ronde, aux larges oreilles. La bonne chère emplissait ses traits et le faisait paraître moins âgé qu’il n’était en réalité, mais le regard parfois las trahissait le poids des ans. Si les sourcils étaient à peine parsemés de blanc, son crâne ne recevait plus aucune tonsure, tellement la couronne de cheveux gris se réduisait à un souvenir. Les lèvres fines étaient rarement déformées par un sourire, dévoilant des dents usées et gâtées, mais il n’en était pas pour autant acerbe ni autoritaire. Ses domestiques n’étaient que peu confrontés à des sautes d’humeur ou des caprices et lui accordaient volontiers un caractère débonnaire.

Il avait gardé de ses années d’étude au monastère et de canonicat l’habitude de se lever au son des heures, et priait avec régularité, dans son oratoire. Son sommeil se découpait donc en plusieurs plages, dont la sieste, ce qui entraînait des en-cas assez fréquents, avec des repas plutôt légers, sauf lorsqu’il recevait des invités.

Comme souvent, il était vêtu d’un bliaud de soie claire à motifs d’oiseaux, sans ceinture, avec un impressionnant crucifix émaillé autour du cou. À sa main, l’anneau de sa fonction étincelait tandis qu’il brassait les fruits secs trempés dans du vin à l’aide d’une cuiller en argent. Il n’avait même pas touché à sa compote fraîche ni aux quelques beignets qui l’accompagnaient. Il se sentait morose.

Les pas d’un valet venu chercher son vase de nuit le tirèrent de sa rêverie. Il apostropha le domestique et lui fit signe d’approcher.

« Avez-vous eu quelques nouvelles de Josce ? Ce démon est absent depuis plusieurs jours désormais.

— Nenni, sire. Nul ne l’a vu ni aux communs, ni en l’héberge, ni en l’hostel.

— Il n’est pourtant pas de ses habitudes de disparaître ainsi. Crois-tu qu’il aurait quelque raison d’avoir fui ?

— Je suis persuadé qu’il n’a que louanges à votre égard, sire. Il serait bien ingrat de ne pas vous savoir gré d’une telle amitié. »

L’évêque hocha la tête, à demi convaincu.

« Savoir ce qui passe dans la tête de telle jouvence… »

Il congédia le valet d’un geste et entreprit d’avaler des fruits. Il n’aimait pas gâcher la nourriture, d’autant qu’il avait assez goûté au pain dur de campagne ou au biscuit de mer pour apprécier les plats de ses cuisines. Une gorgée de vin légèrement coupé lui mit un peu de baume au cœur. Certainement un cru de Samarie, estima-t-il.

Le chantier résonnait désormais de cris de manutentionnaires. Il reconnaissait le grincement d’une énorme grue hissant les pierres, le tintement du marteau du forgeron. Il se dit qu’une visite impromptue de la construction lui ferait le plus grand bien. Il n’était pas toujours intéressé par les explications détaillées du maître de l’œuvre, mais appréciait fort de superviser de loin les thèmes iconographiques prévus par le chapitre des chanoines, ou de valider d’un signe de tête encourageant une décision architecturale.

C’était sa façon de faire, déléguer au maximum tout en insistant bien sur son nécessaire assentiment à toute chose. Sans pour autant s’embarrasser à vérifier si le déroulement était conforme à ce qu’il en avait demandé. Ça aurait été admettre qu’il envisage qu’on ne lui obéisse pas à la lettre. Nul n’aurait pu dire s’il s’aveuglait volontairement ou ne tenait aucun compte des manquements, mais chacun s’en félicitait, trop heureux de pouvoir en décider à sa guise, sous sa supervision purement nominale.

## Lydda, sacristie de l’église Saint-Georges, matinée du mardi 23 septembre 1158

L’épaisse porte de bois ferré menant au petit trésor était largement ouverte et le père Heimart, trésorier de la congrégation augustinienne, y penchait sa délicate silhouette pour y prendre les différents éléments qu’il présentait à l’évêque. Il espérait le convaincre de la nécessité d’étoffer leur collection d’objets liturgiques.

Manipulant un coffret, il en fit jouer la serrure et en sortit plusieurs patènes, soigneusement rangées dans des bourses de soie. Il les compta avant de les extraire avec précaution de leur étui. Il ne pouvait s’empêcher de tirer la langue tandis qu’il faisait cela.

« Voyez, père, comme nous manquons cruellement de tout. En cas de messe solennelle, nous n’aurons jamais assez de ciboires.

— N’en avons-nous pas commandé à cet artisan d’Acre ?

— Nous en avons seulement parlé, mais rien n’est fait. »

Il se releva brusquement, les courtes mèches frisées de sa chevelure blonde dansant autour de sa tonsure.

« D’ailleurs nous n’avons pris nulle décision encore en ce qui concerne les pyxides[^pyxide]. Nous pourrons avoir plusieurs autels en la basilique neuve, et autant de tabernacles. Outre, il est fort probable que moult pèlerins viendront faire visitance à Saint George. »

Tout à ses réflexions, il ne prit pas garde au fait que l’évêque Constantin s’était assis sur un escabeau et regardait vers la nef. Heimart prenait sa charge de trésorier très à cœur et s’inquiétait devant l’ampleur du chantier. Il s’attendait à voir déferler dans les années futures des hordes de fidèles, et tenait à avoir tout le nécessaire pour célébrer le culte de façon appropriée.

Plus influencé par la liturgie clunisienne que par celle des cisterciens, il se plaisait à imaginer les travées emplies de chefs-d’œuvre d’orfèvrerie et d’étoffes plus somptueuses les unes que les autres. Pour cela, il n’hésitait pas à voyager dans tout le royaume pour rencontrer les artisans les plus habiles. Les convois de négociants de tissu étaient également avertis qu’il faisait bon faire halte à Lydda lorsqu’on était sur le chemin de Jaffa, car le frère trésorier était chaque fois désireux d’acquérir les belles pièces de soie qu’on possédait. Il dépensait d’ailleurs une large partie de sa prébende canoniale en ce sens.

Constantin finit par reporter son attention sur le chanoine, toujours occupé à sortir et montrer le mobilier liturgique. Il ne semblait pas décontenancé par le fait que l’évêque ne l’écoutait guère. Il en était à présenter plusieurs étoffes finement ouvragées.

« J’ai pris sur moi de faire broder ces pavillons, nous en aurons ainsi plus qu’assez. Ma seule inquiétude concerne la réalisation de corporaux »

Sa voix s’éteignit lorsqu’il vit que l’évêque Constantin faisait la moue.

« N’êtes-vous pas d’accord avec moi, père ? s’émut le jeune homme.

— Que si, mon fils. Je suis désolé, je n’ai guère la tête à ces questions. Je vous avais promis d’en discuter, mais je n’arrive pas à y trouver intérêt. »

Devant le visage décomposé d’Heimart, il s’empressa d’ajouter :

« Ceci malgré la haute importance de ces questions. C’est vous dire comme je suis troublé.

— Auriez-vous souci ? Voulez-vous que je fasse appeler votre chapelain confesseur ?

— Ce n’est rien de cet ordre. Juste de l’inquiétude à propos de mon petit Josce…

— Il a encore fugué ?

— Il semblerait bien. Et nul ne l’a vu s’éloigner. Je crains fort pour lui, il n’est pas si habile pour se débrouiller par lui-même. Il est encore un peu jeune. »

Heimart hocha la tête doucement. Comme tout le monde au palais, il était habitué aux facéties de Josce, protégé par la main bienveillante de l’évêque. Sa disparition n’était pas pour lui déplaire, mais le désarroi que cela causait chez le vieil homme le touchait malgré tout. Il se gratta le cou, réfléchit un petit instant et arbora un franc sourire. Il avait trouvé ce qui pouvait rasséréner le prélat.

« Vous ai-je parlé de ce nouvel habit de grande fame chez nos frères de France et d’Angleterre ? »

Il s’empara d’un fin voile de lin soigneusement plié et le déploya sur son épaule en un geste frivole.

« On le nomme surplis, et il est fort apprécié pour les offices. Il serait bien avisé d’en user en lieu et place de nos aubes, lourdes et incommodantes vêtures de laine. »

## Lydda, chantier de la nouvelle basilique Saint-Georges, fin de matinée du mardi 23 septembre 1158

La chaleur déversée par le soleil commençait à se répandre, et l’évêque se félicita d’avoir pris un chapeau de paille et un éventail. Il avait décidé de voir l’avancée des travaux de la future église, sans pour autant déranger les ingénieurs et ouvriers. Il entretenait également l’espoir que Josce aurait trouvé là un espace de jeu à la hauteur de son imagination. Il ne pouvait guère définir exactement pourquoi il y était autant attaché. Enfant, il avait été confié à un établissement ecclésiastique à peine savait-il marcher. L’amusement lui était donc alors inconnu, et peut-être compensait-il sur ses vieux jours.

Il était parfaitement conscient du désordre qu’engendrait Josce et se promettait chaque fois de le morigéner comme il convenait. Il n’aurait accepté de quiconque le dixième des bêtises de Josce. Et pourtant subsistait cette affection sans jugement.

Il stoppa soudain, impressionné par l’ascension d’un chapiteau décoré, suspendu dans un écrin de toile et de cordes au bout d’un épais câble tracté par un âne à travers un astucieux enchevêtrement de poulies. Parmi les hommes nerveux qui criaient, guidaient à l’aide de filins la montée de la sculpture, se trouvait l’imagier responsable de l’œuvre. Un morceau de robuste bâche en guise de tablier, les poings sur les hanches, il admirait l’envol de son travail.

Au sein même de ce qui serait la future nef, une loge hébergeait ses collègues, dont les burins résonnaient en cadence tandis qu’ils libéraient de leur gangue de pierre les feuillages, personnages et créatures qui orneraient l’édifice. Autour d’eux, sur des planches de bois, de vieux cuirs ou du papier, des indications avaient été tracées par la main experte de Régnier, le chanoine responsable du programme iconographique.

L’évêque aperçut des pèlerins admiratifs du chantier, essaimés le long du chemin. Appuyés sur leurs bourdons, ils écarquillaient les yeux, émerveillés par la silhouette élancée des colonnes jaillissant vers le ciel. Ils ne semblaient guère prêter attention au fait qu’ils étaient au milieu du passage et gênaient les portefaix transportant mortier et moellons. Ils se poussèrent néanmoins en entendant le grincement de l’essieu d’un lourd charroi de pierre tiré par deux bœufs.

À l’entrée du chantier, la loge des ingénieurs prolongeait le bâtiment de la forge. Cette dernière était le seul élément édifié en dur. Le tintement qui en émanait marquait le rythme des journées de travail. Deux artisans et leurs aides s’employaient chaque jour à réparer les outils, à en façonner de neufs. Ils produisaient également les accessoires métalliques nécessaires à la construction. Les pièces d’art étaient commandées à des ateliers spécialisés, chargés de composer portails à motifs complexes, treillis de ferronnerie et serrures décoratives.

Tout en déambulant, l’évêque espérait apercevoir la silhouette de Josce, sachant que l’endroit recelait mille cachettes où s’amuser. Malgré toute la prévenance du prélat et de ses domestiques, le lieu offrait trop d’occasions de jeu pour en réfréner l’attirance. Lorsque, transpirant, il rejoignit les bâtiments de son palais, son estomac commençait à se manifester. Peut-être que l’appel du ventre serait assez fort pour faire rentrer le fugueur. Avant de passer la porte, il jeta un dernier coup d’œil circulaire à l’édifice qui s’annonçait somptueux. La perspective d’avoir ainsi contribué à ériger un tel monument de la Foi arracha un soupir d’apaisement à son esprit chagrin.

## Lydda, salle d’audience du palais de l’évêque, après-midi du mardi 23 septembre 1158

Constantin avait passé une partie de son après-midi à vérifier et faire sceller plusieurs chartes en attente depuis quelques jours. En tant que dirigeant de la cité de Lydda, il avait des devoirs en plus de la charge spirituelle et foncière de l’évêché. Il ne s’opposait plus aussi violemment aux prétentions des seigneurs de Ramla que ses prédécesseurs, mais il tenait à manifester la réalité de son pouvoir temporel. D’ailleurs, il devait pour celui-ci service à Baudoin un contingent de dix chevaliers et vingt sergents d’armes, dont l’étendard au Saint George était connu dans tous les territoires.

On lui avait signalé l’arrivée de son banneret, Seguin de Brebon, à la mi-journée. Celui-ci avait préféré se rafraîchir avant de venir présenter les dernières informations de l’ost royal. Cela signifiait donc qu’elles étaient plutôt bonnes. Par les fenêtres ouvertes, donnant sur la cour d’entrée, il entendait le remue-ménage engendré par le retour de la troupe. Tandis que les montures et animaux de bât étaient pris en charge, les hommes manifestaient bruyamment leur enthousiasme d’être rentrés sains et saufs en discutant de leurs aventures avec les domestiques.

Il y aurait un grand repas ce soir pour les recevoir comme il seyait. L’évêque aurait préféré demeurer tranquille dans sa chambre, mais il ne pouvait décemment pas faire un pareil accueil à ceux qui étaient allés le représenter les armes à la main. Il savait que certains d’entre eux s’inquiétaient toujours de devoir verser le sang, quand bien même ils soient bénis et au service d’une juste cause. D’ailleurs, certains respectaient scrupuleusement les pénitences infligées, espérant racheter leur violence par des pratiques spirituelles redoublées.

La grande porte grinça en s’ouvrant, laissant passage à la haute silhouette voûtée de Seguin. Manifestement sorti d’un bain, il était rasé de frais, le cheveu propre bien coupé à l’écuelle comme il l’affectionnait. Ses joues semblaient encore plus creuses qu’à l’ordinaire, reflet des semaines occupée à guerroyer, et on ne voyait de lui que ses lèvres charnues et son nez busqué. En apercevant la silhouette de l’évêque installée sur son trône, un large sourire égaya son visage austère et il s’approcha d’une démarche empressée. Il baisa la bague du prélat et attendit qu’on lui donne la parole.

« Je m’enjoie de vous voir sauf auprès de moi, mon fils. J’espère que vous n’avez que bonnes nouvelles à m’adresser.

— Il y a tant de choses qu’il me faudra long moment pour ne rien en oublier. Tout d’abord notre sire Baudoin vous envoie son salut, et vous remercie, par son connétable, d’avoir si promptement répondu à son appel une fois de plus.

— Nous verrons ces questions par la suite. Dis-m’en plus sur les hommes. J’espère que tous sont saufs.

— Seulement des blessés et quelques malades à déplorer, mais tous sont revenus, sire. Depuis la bataille peu après la saint Martin^[Bataille vers Buṭayḥa, 4 jours après la Saint Martin d’été, soit le 8 juillet.], nous n’avons que peu entrebattu l’armée du Soudan.

— Le roi est-il satisfait ?

— J’ose le croire. Nous avons affranchi les Caves de Suète[^suete] et le Soudan est parti sans oser batailler. »

L’évêque approuva sans un mot puis invita d’un geste son vassal à prendre place sur un petit tabouret. Un valet leur servit du vin tandis que le silence s’installait. Ce fut le chevalier qui reprit la parole, après avoir longuement savouré la boisson.

« J’ai ouï parler des assauts faits depuis l’Égypte. Espérons que l’ost parti les combattre y mettra bon ordre.

— Penses-tu que le ban sera levé pour cela ?

— Nul ne m’en a parlé. Le roi est de retour en son palais et nous avons même cheminé un temps avec le sire comte Amaury[^amaury], qui s’en est allé dans ses terres également.

— Parfait. Fais assavoir aux hommes qu’un banquet sera donné ce soir. Ils l’ont sûrement appris par les cuisines, mais que cela soit officiel. Ta femme et toi serez servis à ma table. »

Seguin inclina la tête en remerciement, et avala ce qu’il restait de son vin. Puis il fouilla dans une besace pour en sortir de nombreux plis. En découvrant la quantité, Constantin souffla.

« Fais donc appeler mon secrétaire, veux-tu ? Nous étions justement dans la paperasserie. »

## Lydda, chambre de l’évêque, veillée du mardi 23 septembre 1158

La voix forte de Maugier du Toron résonnait sous le haut plafond tandis qu’il évoquait les quelques soucis de police qu’il rencontrait. La ville était située sur le chemin de la côte, et les gens de passage y occasionnaient pas mal de troubles. Rien que le vicomte n’arriva à mâter, mais suffisamment pour qu’il estimât nécessaire d’en avertir le seigneur en titre du lieu.

Sous un physique gras et indolent, Maugier dissimulait un esprit vif et une volonté inébranlable. Dédaignant le cheval, il arpentait les rues et le domaine d’un pas infatigable malgré son embonpoint. On moquait d’ailleurs son habitude de toujours s’éponger le front d’un linge qui ne quittait jamais sa main, à tel point qu’on le surnommait « sire la Toaille[^toaille] ». Son autre caractéristique était d’être perpétuellement infesté de poux et de puces, ce qui le faisait se gratter sans cesse, incapable de demeurer immobile.

Ce soir, l’évêque espérait de lui un rapport des jours passés, et certainement quelques nouvelles de Josce. Le rusé vicomte était au courant de la disparition et avait donc chargé quelques hommes d’y être particulièrement attentifs. Ce qu’il craignait le plus, c’était qu’une troupe de jongleurs n’ait attiré le malheureux pour l’emmener au loin. Au final, tout ce qu’il savait, c’est qu’il n’avait rien appris susceptible de dérider le vieil ecclésiastique. Sans pour autant dire qu’il avait de l’amitié pour Constantin, qu’il trouvait parfois trop irrésolu, il éprouvait un certain attachement à une personne qu’il devinait finalement peu désireuse de diriger, bien que déterminée à se conformer à son devoir.

Il conclut son récit en se raclant la gorge, tout en se grattant un sourcil. Il allait avaler un peu d’hypocras et ajouta, l’air désolé :

« Il y a aussi les soucis avec ces nouveaux colons au village de Séphorie. La cour des Bourgeois doit statuer prochainement.

— As-tu idée de qu’ils décideront ?

— Je leur ai soufflé l’idée que le jurement sur la nouvelle charte confirmait les anciennes coutumes, ce qui leur convient fort bien. Je ne vois pas pourquoi le champart serait moindre pour quelques-uns. C’était là usage des sires de Rama[^rama], pas les nôtres. »

L’évêque grogna en assentiment. Les chicaneries fiscales et légales étaient légion, certains villageois prétextant de coutumes différentes pour tenter d’échapper au versement de certains impôts. Il fallait sans cesse réaffirmer leur position pour éviter de voir disparaître certaines redevances. Il avala quelques amandes effilées, espérant que le vicomte aurait des choses à ajouter. Il se doutait bien que son inquiétude était connue, et qu’on lui aurait annoncé dès le début de l’entretien tout ce qui aurait pu soulager son angoisse, dans un sens ou l’autre. Mais il ne put s’empêcher de demander.

« N’avez vous rien découvert à propos de Josce ?

— Nenni, et j’en suis fort déçu. Il est suffisamment bien connu des hommes, mais nul ne l’a vu.

— Il faut dire qu’il n’a pas son pareil pour se faire discret… »

Le gros vicomte échappa un rire et agita son large ventre, goguenard :

« Il a certes grand don pour moquer avec astuce, heureusement qu’il est aussi vif pour s’échapper. »

L’évêque savait que Maugier appréciait fort Josce, qu’il gâtait de friandises jusqu’à en être déraisonnable. Cela lui garantissait en outre une totale impunité pour les bêtises qu’il accumulait dans la ville et les alentours lorsqu’il lui prenait l’envie d’y baguenauder. Le vicomte reprit une contenance et ajouta, sentencieux :

« Je suis acertainé qu’il va revenir tout penaud de la peine qu’il nous a infligée, et certes avec à ses trousses de nombreuses victimes de ses facéties »

## Lydda, chambre de l’évêque, nuit du mardi 23 septembre 1158

Simplement vêtu de sa chemise de fine laine, agenouillé devant le beau crucifix peint qui ornait le petit autel portatif qui le suivait en campagne, l’évêque Constantin susurrait les psaumes en s’efforçant de demeurer concentré. Cela lui apportait un sentiment de paix, de stabilité. Il n’avait plus guère besoin de vérifier les temps liturgiques, nourri d’une pratique ancienne, et se contentait parfois de contrôler avec les chanoines qu’ils étaient bien d’accord sur les textes à réciter.

Entendant un bruit étrange derrière lui, il se retourna soudainement, intrigué.

Sur la petite table où il avait toujours quelques fruits et friandises à portée de main pour une de ses fringales se tenait un singe vervet. Sans quitter le prélat des yeux, il grignotait avidement tout ce qui lui tombait sous la main. Sa fourrure claire maculée de poussière prenait des teintes fauves à la lueur des lampes.

Lorsque Constantin se leva, le primate fit quelques bonds souples, déliant sa longue queue, et atterrit dans les bras de l’évêque.

« Josce, petit fripon ! Où donc étais-tu caché tout ce temps ? » ❧

## Notes

Certains prélats jouaient parfois le rôle de seigneurs territoriaux, avec tous les devoirs que cela impliquait. Bien qu’ils n’aient pas à prêter hommage au roi, ce qui aurait posé souci en raison de leur statut de clerc, ils devaient soit le fournir en homme, soit, plus souvent, verser un dédommagement monétaire pour solder des troupes. De telles seigneuries ecclésiastiques n’étaient pas rares et compliquaient encore l’emboîtement des autorités légales, administratives et fiscales. Dans le cas de Lydda, des dissensions nombreuses mirent plusieurs dizaines d’années à s’éteindre face aux prétentions des dirigeants, laïcs, de Ramla tout proche.

La maison d’un évêque de ce type comportait donc à la fois les officiers et serviteurs d’un hôtel noble, en plus de l’organisation religieuse du diocèse, qui pouvait à son tour être détentrice à un titre ou l’autre de propriétés foncières ou fiscales.

En ce qui concerne le petit familier, Josce, on sait qu’il existait un commerce d’animaux pour les plus riches, dont certains aimaient à s’entourer d’une faune exotique. Les malheureuses bêtes avaient souvent une espérance de vie très courte, quand elles ne mouraient pas en chemin. Malgré tout, un certain nombre d’espèces, comme les oiseaux ou les singes, supportaient assez bien la captivité ou la domestication. On cite même des amuseurs qui usaient de tels compagnons.

## Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Mayer Hans Eberhard, « The Origins of the Lordship of Ramla and Lydda in the Latin Kingdom of Jerusalem » dans *Speculum*, vol. 60, No 3, Juillet 1985, p. 537-552.

Mariño Ferro Xosé Ramón, *Symboles animaux*, Paris : Desclée de Brouwer, 1996.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. I & II.*, Cambridge : Cambridge University Press, 1993.
