---
date : 2018-08-15
abstract : 1158. Gobin, ancien soldat désœuvré, vit de petits boulots et d’expédients dans la cité portuaire de Jaffa. Lorsque des galères égyptiennes s’annoncent au large des côtes, il saisit l’occasion de tourner la roue de la Fortune dans le sens qu’il espérera favorable.
characters : Gobin, Jonas, Bonado, Federico
---

# Écueils

## Jaffa, bassin de la lune, fin de matinée du lundi 24 mars 1158

Un soleil plus audacieux que les dernières semaines avait fini de déchirer les brumes matinales, inondant les reliefs côtiers d’une chaude lumière. Quelques barques de pêcheurs attardés fainéantaient aux abords des plages tandis que les plus chanceux étaient déjà le long des murailles de la cité, occupés à nettoyer leurs prises et à les vendre aux commères, valets et marchands. Gobin avait aidé à acheminer quelques corbeilles, essentiellement du bar, jusqu’aux commerces qui se trouvaient en ville. Il y avait gagné une odeur entêtante et la promesse de quelques piécettes à la fin de la semaine.

Il s’était arrêté pour avaler une brochette de poisson frais, glissée dans du pain, admirant les vaisseaux à l’ancre dans le port. La ville de Jaffa n’avait pas de quai, se contentant de débarquer les navires à l’aide de petits porteurs depuis une anse abritée par une ligne de rochers, où la légende disait qu’une princesse avait été livrée à un monstre marin pour mettre fin à ses attaques.

Il avait un modeste canot qu’il maniait avec habileté, se proposant au déchargement dès qu’un bateau se présentait. Avec la fin de l’hiver, les affaires reprenaient peu à peu, la saison de mauvaise mer finissant enfin. Il n’aimait guère devoir accepter des tâches qu’il estimait dégradantes et mener sa barque à son bon plaisir lui semblait plus prestigieux. Il tirait grande fierté de son indépendance et de son expérience au service du roi de Jérusalem. Il n’avait fait partie de son hôtel qu’à peine plus de deux ans, mais à l’entendre, il y avait passé sa vie.

Habitué des tavernes de bric et de broc qu’on rencontrait dans les environs du port et sur les plages même, il y contait sans lassitude la façon dont il avait dû se retirer du service après avoir été gravement blessé au pied. Il avait dû se battre pour qu’on ne l’ampute pas et il traînait désormais sa claudication, lançant son pied à chaque pas. Il prenait la chose avec philosophie et soignait ses douleurs avec de grandes quantités d’alcool, de toute provenance. On l’appelait plus fréquemment la Patte que Gobin.

Figure familière du quartier, il était arrivé qu’il soit recruté un temps pour prêter main-forte aux hommes affectés à la surveillance sur les murailles. Sa formation militaire, bien que modeste, lui offrait l’avantage de connaître les missions habituelles. Il conservait d’ailleurs précieusement son casque de fer et son épée, qu’il astiquait régulièrement pour en maintenir le brillant. Même au plus fort des coups du sort, il n’avait jamais voulu se séparer de ces symboles de sa compétence guerrière.

Il s’apprêtait à retourner en ville, où il espérait retrouver quelques compagnons désœuvrés comme lui, histoire de passer la journée à discuter et faire rouler les dés. Il n’était pas tant joueur, mais appréciait de commenter l’actualité avec une poignée de commensaux. Ils avaient leurs habitudes dans une ruelle qui serpentait depuis les hauteurs de la citadelle, ondulant sur les reliefs en amphithéâtre en direction du rivage.

Il franchissait les portes de la Mer lorsqu’une rumeur se mit à enfler. Une rapide agitation grondait depuis la berge au sud, pour parvenir jusqu’à lui. Des navires arrivaient, en assez grand nombre. Leur formation, leur allure, tout indiquait que ce n’était pas de simples vaisseaux de commerce, mais des galées taillées pour la course et la guerre. Et si elles montaient depuis les zones méridionales, elles étaient probablement égyptiennes, donc ennemies.

En peu de temps, le grouillement devint maelström. Chacun s’agitait pour porter à l’abri ses maigres biens dans l’enceinte de la ville. Les hommes dans les barques ramaient aussi vite qu’ils le pouvaient et échouaient leur embarcation au plus haut. Des cors résonnaient depuis les murailles tandis que les églises s’étaient mises à carillonner, propageant l’alerte.

Inquiet de n’avoir pas tiré son canot suffisamment loin, Gobin s’élança au rebours de la foule, en direction du sud. Il eut tout le temps de voir que l’escadre était forte de nombreux navires, qui ne laisseraient aucune chance aux quelques vaisseaux de commerce ancrés en deçà des rochers d’Andromède. Ce n’étaient peut-être que des pillards, les Égyptiens n’ayant jamais lancé de véritables attaques sur les cités fortifiées. Dans le doute, il ne tenait pas à ce que son seul moyen de subsistance finisse en bois d’œuvre pour un mantelet ou une bricole à l’assaut de la ville.

Il tira l’esquif aussi loin qu’il le put jusqu’au couvert des premiers jardins. Quelques autres avaient suivi son exemple, avant de disparaître dans les frondaisons. Voyant que la flotte se divisait désormais, certaines galères venant à la manœuvre pour passer dans le port, il opta pour un repli stratégique vers l’est, espérant franchir les murailles par Bab el-Belad, du côté opposé à la mer. Il avançait aussi vite qu’il le pouvait, mais sans s’abandonner à la terreur. Il savait qu’il faudrait du temps aux musulmans pour s’emparer des navires stationnés et débarquer éventuellement leurs hommes, dans le cas où ils envisageraient de s’attaquer à la ville. Ils ne pouvaient de toute façon certainement pas accoster sous les tirs des archers sur les chemins de ronde. Tout ce que la cité comptait d’arbalétriers et de frondeurs avait dû se poster sur les courtines, et peut-être même les bricoles avaient-elles été pointées sur le large. Les jeux nautiques pendant lesquels les quartiers, confréries et paroisses s’affrontaient à viser des tonneaux allaient finalement dévoiler leur utilité.

Il croisa quelques laboureurs et jardiniers, inquiets du tintamarre, leur expliqua en quelques mots que des navires ennemis étaient en vue. Même si personne ne croyait qu’une armée serait assez folle pour établir un camp de siège en plein cœur du royaume, tous craignaient le pillage. Ils emportaient donc tout ce qui pouvait avoir de la valeur à leurs yeux, qui ne pouvait se cacher et qui n’était pas trop lourd et encombrant.

L’oasis qui entourait la cité était florissante, les arbres fruitiers bénéficiant d’une irrigation incessante, apportée par des norias où tournaient poneys et mules. Les propriétaires des bêtes se hâtaient de les libérer de leur harnachement afin de les mener à l’abri dans les murs. Gobin prêta la main à un vague compagnon de soirée qui s’était mis en devoir de rapatrier deux ânes et quelques moutons qui appartenaient à son maître. Il avait abandonné l’espoir de rassembler la volaille.

Comprenant qu’il y aurait peut-être quelqu’avantage à ne pas se précipiter, Gobin le laissa filer devant, indiquant qu’il souhaitait s’assurer d’un autre compère. Il fit demi-tour, dans l’espoir de pouvoir profiter du désordre pour s’emparer de quelques victuailles. Que ces maudits musulmans lui permettent au moins de manger correctement dans les jours qui viendraient ! Lorsqu’il quitta la zone des jardins, il portait à l’épaule un chiffon noué en sac où il gardait deux poules rapidement étranglées, une poignée d’œufs et des légumes pour quelques bons repas.

## Jaffa, porte orientale sur la route de Rama, début d’après-midi du lundi 24 mars 1158

Lorsqu’il parvint enfin à la porte orientale, une foule s’était massée devant les vantaux clos. Ceux qui n’avaient pu entrer interpellaient avec colère les hommes de faction sur la muraille au-dessus. Gobin reconnut quelques visages parmi les personnes du guet et siffla pour se signaler à eux.

« Pourquoi donc fermer les portes ici, compère ? Ils sont en galées sur les plages, ce ne sont pas cavaliers !

— Ordre du château ! Toutes les portes ont été barrées. Seul un des chevaliers du comte peut nous ordonner la réouverture !

— Ne peut-on pas au moins passer par le guichet ?

— On l’aurait ouvert si on le pouvait !

— Vous allez nous laisser là dehors ?

— Restez dans le coin, on vous fera entrer dès qu’on pourra. »

Le petit groupe de refoulés se mit à pester de plus en plus fort, certains ayant avec eux bêtes et bagages. Ils estimaient injuste de se trouver séparés des leurs, risquant de se faire prendre par les attaquants. Curieux d’en savoir plus et inquiet de se voir questionné sur son chargement, Gobin continua de longer la muraille à main gauche, de façon à rejoindre la partie septentrionale des jardins. Il pourrait éventuellement y apercevoir ce qui se passait en avançant prudemment. De toute façon, en restant près des fortifications, il avait peu de chance de croiser des assaillants. Même s’ils étaient en quête d’esclaves, ils n’allaient pas débarquer ici. Il était bien plus facile de déposer les hommes à terre discrètement au large d’une petite agglomération et de faire un raid éclair sur des territoires sans protection. Ils en voulaient plus certainement aux navires à l’ancre.

L’oasis était étrangement calme, les cultivateurs ayant abandonné la zone comme au sud. Il croisa quelques chiens, ainsi qu’un âne qui avait dû échapper à son propriétaire. Il tenta de l’amadouer pour s’en emparer, en pure perte. Il continuait d’avancer sous les frondaisons en ne s’écartant guère des remparts, attentif au moindre bruit. Une brise de mer apportait les senteurs salines et lui permettrait d’entendre assez clairement ce qui se déroulait sur la plage. Quand il estima n’en être plus guère éloigné, il bifurqua vers le nord, en direction de l’église Saint-Nicolas. Elle était située en plein milieu de la zone de cimetières, sur un surplomb au-dessus du rivage et offrirait une belle vue sur ce qui s’y passait.

Il pourrait aussi s’assurer de Jonas, le vieux gardien, qu’on disait à demi fou. Gobin le connaissait depuis des années et savait qu’il avait néanmoins suffisamment de bon sens pour se mettre à l’abri. Il était original, mais pas téméraire. Sa nonchalance n’était pas si forte qu’il ne reconnaissait pas le danger. L’église, ancienne, était entourée de quelques bâtiments annexes. La nef y était suffisamment vaste pour y accueillir pas mal de personnes et servait surtout pour les cérémonies funéraires et quelques bénédictions en rapport avec la mer. Le prêtre titulaire n’y était que peu et c’était Jonas qui faisait la plupart des corvées, assisté parfois de quelques manouvriers. Il n’était pas rare de le voir sur le toit réparer les dégâts d’un fort coup de vent.

Lorsqu’il parvint aux abords de la clôture, Gobin y perçut les bruits familiers : quelques poules caquetaient et des bêtes pâturaient derrière les barricades. On y trouvait aussi un petit troupeau de chèvres et un âne. Ce dernier était généralement attaché à la roue du moulin qui puisait l’eau. La porte était fermée et Gobin ne chercha pas à frapper, sachant que Jonas ne répondrait certainement pas aux sollicitations. Il vint en rampant jusqu’en bordure du ressaut qui surplombait la plage.

Il ne discernait que peu la rade, masquée en partie par l’enceinte, mais put compter les nombreuses galères qui se tenaient au large, prêtes à intervenir si le besoin s’en faisait sentir. Il en voyait trois qui avaient pénétré dans le port par le nord, s’exposant au tir depuis les murailles. Ayant contourné les rochers d’Andromède, deux d’entre elles avaient abordé un navire à l’ancre et semblaient chercher à s’en emparer. Une fois l’équipage fait prisonnier ou tué, il s’agissait de mettre à la voile au plus vite avant de subir trop de dommages. Il était possible qu’ils tentent la même opération plus au sud, quoique les dangers y fussent plus grands, l’endroit se trouvant directement en contrebas de la citadelle, zone la mieux pourvue en défenseurs.

Gobin nota avec soulagement que nul attaquant n’avait posé les pieds à terre. C’était un raid de pillage comme les Égyptiens les affectionnaient, une razzia sur les mers qui ne prenait pas le risque de s’enliser dans de longs affrontements. Le temps qu’une réaction puisse s’organiser, ils étaient généralement déjà partis, comme les Bédouins du désert. Revenant sur ses pas, il osa alors appeler Jonas depuis l’entrée.

Le vieil homme mit un moment à lui répondre, d’une voix moins forte que la sienne. Il lui permit de se faufiler rapidement, reposant rapidement la barre du portail. Dans la cour se trouvaient quelques jardiniers, des enfants et une dizaine de marins et de pêcheurs.

« Il faut juste attendre, ils ne s’en prennent qu’aux navires de commerce.

— Nous avons veillé leur attaque depuis le beffroi, indiqua Jonas. Nous regardons l’orage passer. »

La voix d’un gamin qui faisait la vigie les interrompit.

« Ils tentent de hisser les voiles ! Leurs galées semblent s’éloigner du *Luna*. Ils doivent l’avoir pris ! »

Gobin en grogna de mécontentement.

« Je vais aller voir ce qu’il en est de plus près.

— Demeure à l’abri, tu ne sauras rien faire d’utile au-dehors.

— Ils ne s’occuperont pas de moi s’ils sont là pour les marchandies.

— Ils ne cracheront certes pas sur un esclave de plus ! »

Négligeant l’avertissement, Gobin fit un sourire encourageant à Jonas puis ressortit du petit enclos. Il n’aimait pas se sentir enfermé, quand bien même c’était pour être à l’abri.

## Jaffa, plages septentrionales, après-midi du lundi 24 mars 1158

Demeurant sous le couvert des arbres, Gobin avait lentement longé la côte jusqu’à retrouver les murailles. Il pouvait ainsi avoir une meilleure vue sur ce qui se passait. Quelques galées se maintenaient au loin, à l’abri des armes de tir de la cité, tandis qu’une demi-douzaine s’affairait autour de trois gros navires amarrés là. Comme l’avait indiqué le gamin, le *Luna*, une nef ventrue en provenance de Pise manœuvrait pour prendre le vent en direction du large. Sur les autres bateaux, les choses semblaient moins claires, soit les affrontements y continuaient, soit les marins avaient saboté suffisamment les dispositifs de gouverne pour éviter qu’on s’en empare.

Les tirs depuis les murailles visaient sporadiquement les galères à distance des embarcations latines, sans grands effets. Gobin huma la brise : elle avait tourné et favorisait désormais les musulmans s’ils souhaitaient s’éloigner. Ils avaient finalement hissé les voiles sur un navire supplémentaire outre le *Luna* et les galées pivotaient pour laisser le champ libre. Ils allaient rejoindre le reste de l’escadre avant de rapatrier ce butin dans leurs ports. Soulagé de les voir partir, Gobin héla un des soldats sur la muraille et lui demanda quand ils ouvriraient les portes. Le guetteur ne savait pas, il fallait attendre les ordres de la citadelle.

Gobin envisageait de rebrousser chemin, estimant que ce serait certainement à l’orient qu’il serait possible d’entrer en premier, quand il aperçut des gerbes d’eau autour du *Luna*. Des matelots sautaient à la mer alors que le vaisseau glissait en prenant de la vitesse. Des cris montaient depuis la nef, en pure perte. Les hommes battaient des bras et des jambes dans les vagues avec l’énergie du désespoir, inquiets de recevoir une flèche tandis qu’ils s’échappaient. Les razzieurs n’étaient généralement pas féroces et craignaient les représailles, ne versaient le sang que lorsqu’ils y étaient contraints. Si les matelots arrivaient à rejoindre la rive, ils seraient sauvés.

Incapables de contrôler leur nage désordonnée, plusieurs d’entre eux se contentèrent d’aller jusqu’aux récifs, déjà heureux de sentir sous leurs pieds une assise solide. Quelques-uns parvinrent, épuisés et suffocants, à la berge. Pendant ce temps, les galères et leurs prises formaient une escadre qui gagnait de la vitesse en direction du nord-ouest. Comprenant que le danger était quasiment écarté, Gobin avança vers les rescapés et les aida à se mettre au sec. Puis il détacha un des canots tirés à l’abri et le hala jusque dans les flots pour aller chercher ceux qui s’étaient accrochés aux rochers. Avant même d’y parvenir, il reçut de leur part suffisamment de bénédictions et de remerciements pour se garantir un Salut dans l’Au-delà.

Les hommes sautèrent vers lui et se hissèrent en soufflant dans la petite embarcation, manquant de la faire chavirer dans leur agitation. Sans sa connaissance des lieux et sa maîtrise des rames, ils auraient tous fini à l’eau. Une fois installés dans la chaloupe, ils laissèrent éclater leur soulagement en vociférant blagues et invectives grossières, à la mesure de leur frayeur passée.

Le temps qu’ils reviennent au rivage, la porte de la Mer avait été ouverte et des secours s’étaient avancés pour accueillir les naufragés. Des pêcheurs sortaient aussi, soucieux de leurs embarcations. Gobin salua aimablement l’un d’eux, à qui il avait emprunté la barque et l’aida à la remettre en place. L’homme n’était qu’à demi content qu’on ait ainsi disposé de son bien sans lui en faire la demande, mais il se laissa convaincre de mauvaise grâce quand les remerciements et les félicitations fusèrent autour de Gobin. La solidarité en mer était une vertu louée et le risque, même mineur, qu’avait pris Gobin, offrait l’occasion de se réjouir en un jour plutôt malheureux. Les Égyptiens avaient saisi trois navires, dont un qui avait une belle cargaison de bois.

Gobin savourait ce petit moment de grâce, tout en espérant que cela lui redonnerait du crédit auprès de quelques commerçants. Il avait souvent la pépie et l’appétit plus conséquents que sa bourse, sans même parler de ses égarements au jeu. Il remerciait chaleureusement les bénédictions, souriant d’une oreille à l’autre tandis qu’il avançait avec les rescapés qu’il avait secourus. Ceux-ci tenaient à le convier à une veillée dans le quartier pisan, où on fêterait le salut des quelques matelots. Convaincu par ces agréables perspectives, Gobin se laissa mener, comprenant de moins en moins les échanges alors qu’ils se faisaient de plus en plus en italien.

## Jaffa, quartier pisan, veillée du lundi 24 mars 1158

Malgré tous les désagréments de la journée, l’ambiance était joyeuse avec les marins réchappés, et il y avait même un héros à célébrer : Gobin. C’étaient plutôt les familles humbles qui se félicitaient, n’ayant rien perdu dans l’affaire. Les maisons des patriciens, des négociants qui avaient vu partir leurs biens et leurs navires ne brillaient guère des feux de la fête. Pour une fois que les motifs de réjouissance touchaient davantage les plus modestes, ceux-ci ne boudaient pas leur plaisir. En plein Carême, chacun y allait de son cellier pour participer : pâtés, tartes et boissons jaillissaient comme si une corne d’abondance avait soudain surgi dans la cité. On avait sorti quelques instruments et, rapidement, branles et sardanes avaient agité bras et jambes.

Gobin profitait de l’instant, avalant tout ce qui se présentait à sa portée. Il avait posé dans un coin mal éclairé son butin de l’après-midi, en espérant qu’il serait toujours là lorsqu’il rentrerait chez lui. Avec son pied paralysé, il ne dansait pas, mais frappait des mains en sifflant pour accompagner la musique. Cela faisait des années qu’il n’avait pas passé une si bonne veillée. Il regretta que sa vieille mère n’ait manqué cette célébration que de quelques mois. Son frère, qui était resté enfermé dans leur maigre masure, avait fini par le rejoindre, amusé et curieux de voir Gobin pareillement admiré. Il craignait, disait-il, que l’avenir leur réserve une pluie de grenouilles ou de la neige en été, au rythme où allaient les choses. Il participa néanmoins de bon cœur à la fête, profitant du sillage de renommée pour se faire valoir à son tour.

Un moment que la musique retombait un peu et que les plus essoufflés avalaient de quoi récupérer l’énergie pour une nouvelle farandole, Gobin retrouva deux des hommes qu’il avait recueillis. Ils n’avaient guère eu le temps de discuter depuis l’après-midi et ils se présentèrent un peu. Bonado et Federico étaient Génois, embauchés depuis quelques mois sur le *Leo* après avoir vu le navire où ils étaient subir tellement d’avaries après une tempête qu’il risquait le désarmement. Loin de chez eux, à Saint-Syméon dans la principauté d’Antioche, ils avaient accepté le premier engagement venu. Ils se trouvaient donc une nouvelle fois désœuvrés, avec pour tout bien ce qu’ils portaient sur eux.

« Il se trouve ici quelques Génois, rassurez-vous, et au pire vous pourrez cheminer au nord, où ils tiennent plusieurs quartiers. Les mers viennent de s’ouvrir, de bons marins trouvent toujours de l’emploi.

— J’avais acquis un peu de poivre et d’encens, tout ça va finir chez le Soudan[^soudanbabylone], grogna Bonado.

— *Fortune tourne sa roue*, compère. Tu as sauvé ta vie, c’est déjà ça. Il ne fait pas bon finir en les geôles babyloniennes.

— Mes derniers voyages m’ont rendu plus pauvre qu’à mon départ… »

Gobin haussa les épaules et avala un peu de vin d’une outre qu’il fit passer aux deux marins. Il n’avait guère envie de se lamenter et préférait profiter de l’instant. Il avait trop d’occasions de se plaindre et n’allait pas refuser pareil rayon de soleil. Demain il serait bien assez tôt pour retrouver sa vie ordinaire.

« Réjouis-toi de ta bonne fortune pour ce soir, compère. Demain nous verrons ce qu’on peut faire pour vous. Il ne sera pas dit que notre bonne cité de Jaffa ne sait faire charité au bon pèlerin ! »

Il accorda une bourrade amicale aux deux matelots et leur reprit la gourde. S’il ne s’enivrait pas ce soir, il aurait le sentiment de faillir à sa réputation. Il était encore temps pour cela.

« Puisse le Seigneur faire que récifs où nous frappons être garni de quelque belle garcelette à la voix mélodieuse pour nous réconforter ! » ❧

## Notes

Les opérations guerrières, latines ou musulmanes, lors des croisades n’étaient parfois que des raids de brigandage, que ce soit à terre ou en mer. Durant le mois de mars 1158, des émirs mécontents de la politique de soumission apparente du vizir égyptien au pouvoir latin décidèrent de braver ses ordres et entreprirent de mettre au pillage la côte franque. Ils en ramenèrent un confortable butin, après avoir remporté quelques succès dans les affrontements et créé une certaine pagaille sur les rivages.

Même si c’était là une campagne conçue par les autorités militaires, ce n’était pas des actions avec une visée stratégique d’ampleur. Le but était de déstabiliser le voisin tout en s’enrichissant. En l’occurrence, c’était peut-être aussi lié à un souhait de venger l’affront de la prise d’Ascalon quelques années plus tôt. Tout n’était pas motivé uniquement par le désir de conquête et l’esprit de lucre avait sa part dans les décisions.

## Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem. A corpus. Volume I. A-K*, Cambridge : Cambridge University Press, 2008.

Riley-Smith Jonathan (ed.), Ellenblum Ronnie, Kedar Benjamin, Shagrir Iris, Gutgarts Anna, Edbury Peter, Phillips Jonathan et Bom Myra, *Revised Regesta Regni Hierosolymitani Database* http://crusades-regesta.com/ (consulté le 24/01/2018)
