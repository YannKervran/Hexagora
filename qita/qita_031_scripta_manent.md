---
date: 2014-06-15
abstract: 1154-1188. Le puissant et riche émir Usamah ibn Munqidh découvre à son grand désarroi la faible valeur que certains accordent à leur parole, toute formelle. Ceci d’autant plus cruellement, et plus ironiquement, que cela concerne surtout les écrits qu’il aime tant…
characters: Gaston, Gobin, Nusayba, Al-’Abbas
---

# Scripta manent

## Saint Jean d’Acre, fin de matinée du jeudi 14 octobre 1154

La matinée tirait à sa fin, les canots de pêcheurs étaient déjà pour la plupart rentrés et les quais empestaient les prises fraîchement libérées des filets. Une nuée de mouettes et de goélands tournait aux alentours, plongeant dans les vagues emplies des déchets rejetés par des mains expertes. Dans la zone réservée aux poissonniers, sur la rive ouest, les chalands se pressaient, inquiets d’obtenir les meilleures pièces. Certains poissons étaient encaqués directement, au milieu des barques échouées. Les derniers navires en partance pour l’Europe faisaient le plein avant la clôture hivernale des mers, et les affaires allaient bon train.

Affalés contre la coque d’un imposant canot à voile, quelques hommes se partageaient des brochettes, du pain et une outre de vin tout en jaugeant les passants. L’un d’eux, le torse vigoureux, taillé comme un lutteur, conservait le regard au loin, les yeux plissés. Nullement en repos, il était à l’affût, aux aguets comme un chien de chasse. Un de ses voisins lui donna un coup de coude dans les côtes :

« Le *Leo*, c’est pas un bateau de Césaire ? Les païens l’ont pris, Gaston ? »

Le costaud grogna et avala sa bouchée avant de répondre d’une voix enrouée :

« Aucune idée. Sûrement. »

La réponse laconique ne satisfit pas son compère qui insista, après avoir avalé une goulée de vin.

« Quand même, on est pas beaucoup pour assaillir pareille nave ! Je l’ai déjà vue à quai.

— Le sire connétable a dit qu’on n’aurait pas de souci.

— Couler si beau navire, quand même… »

Gaston se rembrunit et haussa le ton :

« Bougre de cul de lapin ! T’es là pour la double solde, nan ? Alors tu fermes ta grand’ goule ! »

Il attrapa une des larges cognées assemblées en un tas à leurs côtés et la balança d’un geste énervé aux pieds de son interlocuteur.

« Tiens, affile plutôt ça, on en aura besoin. »

Puis il tourna la tête vers chacun d’eux, l’air mauvais, mâchonnant ses lèvres minces, avant de lancer :

« Si jamais y vient à l’idée d’un de vous de parler une fois sur le *Leo*, il a intérêt à savoir nager, j’vous le promets ! »

Un des hommes, grisonnant, la barbe drue et le nez couperosé cracha dans le sable avant de déclarer, l’air inquiet.

« Moi ce qui m’plaît pas, c’est qu’le vent tourne et qu’y va pas faire bon à prendre la mer si on tarde trop.

— Tu veux qu’on mette à l’eau maintenant, Gobin ?

— On serait pas plus mal. De toute façon, le *Leo* va pas tarder. Il a dépassé Césaire hier. Et pis, vu les courants du golfe, au plus tôt on le coince, au mieux on sera. Y’a quelques hauts-fonds qui iront bien mais s’agit pas de les louper…

— On doit pas le couler ? intervint une voix hachée.

— Certes pas ! éructa Gaston. On doit l’échouer, mais si y va par le fond, son contenu sera perdu. Et nous on aura droit aux verges au lieu de la cliquaille. »

Il se tourna vers Gobin, plissant les yeux après avoir regardé le ciel, en direction de la cité :

« On attend jusqu’à ce que les frères de Saint-Jean sonnent sixte, à la mitan du jour. »

Chacun médita sur ces paroles en silence pendant un temps.

Une légère brise soulevait le sable sec, le soleil était encore chaud en cette saison. L’outre se vida peu à peu sans bruit. Quelques-uns sortirent leurs dés tandis que deux autres chantaient une chanson à boire, accompagnés de la guimbarde d’un troisième. Gaston s’installa les mains derrière la tête, les yeux mi-clos, jouant avec un morceau de bois entre ses chicots. Ce furent les cris d’un gamin qui le réveillèrent en sursaut. Le navire tant espéré était annoncé, ils avaient juste le temps de le rejoindre.

Sans attendre, le canot fut mis à l’eau et le mât dressé. La petite voile latine permettait de naviguer avec souplesse et rapidité. En peu de temps ils dépassaient la pointe où se bâtissait le quartier du Temple, en perpétuel chantier. Gobin manœuvrait avec talent le frêle esquif, louvoyant sans donner l’air d’y réfléchir. À bord, les visages s’étaient fermés, les mains se crispaient. Gaston examina les trois arbalètes qu’ils emportaient. L’humidité ne leur convenait pas trop, leurs arcs collés, en tendon et corne, avaient tendance à se fragiliser. Mais cela avait l’avantage de calmer tout de suite les ardeurs combatives de beaucoup. Un carreau dans le ventre signifiait la plupart du temps une morte lente et douloureuse. Puis, comme ses camarades, il vérifia son gambison matelassé, assujettit sa ceinture.

Jusque là, pas un mot ne fut échangé à voix haute tandis qu’ils avançaient vers leur proie. La voile ondoyant, les cordages grinçant sous la main experte du marin, la vague fendue en embruns par la proue couvraient les chuchotements. Gobin décrivait un large cercle pour aborder par bâbord. La nef ventrue était bien visible, et on pouvait même discerner les gens à bord. La silhouette d’un lion jaune se dessinait sur le château avant, griffes et crocs brandis en avertissement.

D’un sifflement, Gaston invita les hommes à se rapprocher de lui. Tous tenaient leurs armes désormais, essentiellement des haches.

« Bon, je le répète : on leur dit rien. On monte à bord, on les rassemble et pis on échoue la nave. Compris ? »

Il n’obtint en réponse que des acquiescements graves, un silence empreint de crainte. Mener un combat était toujours hasardeux, et le faire en mer encore plus dangereux. La plupart ne savaient pas nager, et le poids de leur équipement pouvait les entraîner au fond bien vite. Il distribua les grappins à ceux qu’il pensait le moins timorés.

« On fait ça vite, et bien. On s’accroche, on monte et on fait ce qu’y a à faire.

— Et si des soldats nous attendent ?

— Je vous l’ai dit : y z’ont dû avoir le mot à bord. Faut juste avoir doutance des passagers. Il y a peut-être quelques Turcs qui compaignent les femmes et les enfants, mais on est trop nombreux pour qu’ils puissent faire quelque chose. »

Un cri venu du *Leo* attira leur attention, les visages curieux se tournèrent à l’unisson. Derrière eux, Gobin lâcha d’une voix grave :

« Tenez-vous prêts, on va toucher leur coque ! »

## Plages au sud de Saint Jean d’Acre, après-midi du jeudi 14 octobre 1154

Abrité du vent par le ressaut herbacé d’une dune, le chevalier écoutait tranquillement tout en déambulant. Attentif, il gardait les yeux au sol la plupart du temps, ne les relevant que furtivement, par intermittence. Son interlocuteur parlait avec un fort accent égyptien, mais de façon assez fluide. Son débit était néanmoins saccadé, en grande partie en raison de l’émotion certainement, ainsi que de l’effort qu’il avait fait pour venir à la nage depuis l’épave échouée parmi les vagues.

Ses vêtements étaient encore humides de la traversée et nul n’avait fait le moindre geste pour lui proposer une couverture ou de quoi se changer. Ils avançaient pourtant au sein d’un groupe d’hommes affairés. Une dizaine de chevaliers et nombre de serviteurs semblaient bien occupés entre la plage, l’épave et la dune. C’était en ce dernier endroit, sous un dais rapidement tendu que se tenait la silhouette royale de Baudoin III. C’était justement de lui qu’il était question dans les récriminations du naufragé.

« Tu as vu l’*aman* scellé de ton sire, nous ne sommes pas ennemis.

— J’en suis d’accord et lui aussi. Tu n’es pas traité en tant que tel d’ailleurs, ni aucun de ton hostel.

— Mes maîtres sont inquiets, car ils n’ont vu venir à eux que des pillards, et nulle aide ne leur a été offerte. D’aucuns attendent encore dans la carcasse, inquiets de leur sort. »

Le chevalier hocha la tête avec circonspection.

« Voilà remarque de fidèle serviteur, j’en conviens. Il va me falloir t’expliquer certaines choses… »

Le chevalier inspira longuement, retenant à grand-peine un sourire narquois. Il étendit le bras autour d’eux, de façon grandiloquente.

« Sais-tu où nous sommes ? »

L’Égyptien écarquilla les yeux, décontenancé par la question. Il balbutia quelques mots incompréhensibles avant de répondre, hésitant :

« Sur une plage non loin d’Akko[^acre] ?

— De certes, en bord de mer. Mais en des terres du roi de Jérusalem, tu en conviens ?

— Oui, concéda l’autre du bout des lèvres.

— Fort bien, donc tu dois connaître le droit d’épave alors ? »

Le silence qui accueillit sa question le fit sourire, avec un manque de naturel évident.

« Non ? Voilà bien grande surprise ! Laisse-moi donc t’en conter la teneur en ce cas. »

Il se gratta le menton un petit moment, levant les yeux au ciel. Entretemps, ils étaient parvenus au sommet de la dune et voyaient l’épave échouée du *Leo*, autour de laquelle plusieurs canots étaient emplis de sacs, de ballots et de paniers.

« Il s’agit d’une coutume fort ancienne en ces pays comme en d’autres, qui font que le sire d’un lieu peut se servir à volonté au parmi des épaves échouées en ses terres depuis la mer. Le sire Baudoin ne fait donc là que suivre bien ancienne loi de vos pays. »

Le jeune homme fronça les sourcils, interloqué. Il entrevoyait où la discussion le menait mais semblait encore refuser d’y croire. La situation était par trop artificielle pour qu’il l’accepte.

« Mais… la nef n’a pas échoué. Nous avons été attaqués et le *Leo* a été volontairement sabordé par…

— Par qui ? Vois-tu ces hommes autour de nous ? Le sire Baudoin saurait les punir comme pillards qu’ils sont. Il vous protégera, toi et l’hostel du sire Usama comme il l’a promis à Norredin, je m’en porte garant ! Tu es en sécurité désormais.

— J’ai pourtant l’impression que… »

L’Égyptien hésitait, impressionné par le déploiement des forces en présence. On lui donnait toutes les garanties, apparemment, mais ce dont il était témoin, c’était un simple pillage, de ceux qui se déroulent après les batailles, lorsque le perdant est dépouillé par le vainqueur. Les sergents, les valets, ne mettaient pas les biens de l’émir Usama en sécurité, ils les rapportaient à leur maître Baudoin.

« Allez-vous nous aider à former une caravane ?

— Et comment ! Nous vous fournirons même vesture et de quoi manger pour cheminer.

— Grâce vous en soit rendue, mais nous saurons faire avec ce que nous avons emporté. »

Le chevalier s’arrêta brusquement et se pencha, parlant soudain d’une voix sourde.

« Le sire Baudoin y tient. Vous avez tout perdu dans votre naufrage, mais nous vous fournirons de quoi rejoindre votre maître. »

Le domestique ferma les yeux et hocha la tête en silence. Désormais il craignait pour les femmes, emmenées à l’écart par quelques sergents. Il imaginait le pire pour la famille de l’émir, et n’osait s’aventurer à présumer du destin des servantes. Le chevalier lui tapa avec allégresse sur l’épaule.

« Porte message de la sauveté de tous. Nul n’attentera à vos vies, le sire Baudoin de Jérusalem en est garant. Et nous vous fournirons escorte pour cheminer, une fois que nous aurons récupéré la cargaison de cette épave. »

Puis il planta le jeune serviteur là, au milieu de la plage.

Un canot venait de s’échouer, mené par trois hommes. Ils se faisaient aider pour tirer plus au sec l’embarcation lourdement chargée. Des ballots de toile cirée, certainement une partie de la bibliothèque du maître. Il possédait plusieurs milliers de tomes, et chaque ouvrage, que ce soit de poésie, de science, de législation ou de religion, faisait sa fierté.

Il détenait plusieurs exemplaires du Coran, dont certains avaient été copiés par des membres prestigieux de sa famille, les seigneurs de Shayzar, sur l’Oronte. Un de ses plus précieux venait d’Ifriqiya[^ifriqiya], où des calligraphes experts avaient tracé les sourates sacrées dans le style coufique qu’il affectionnait tant. Yusuf ne put s’empêcher de frémir quand il vit un des lourds paquets tomber dans une gerbe écumeuse parmi les vagues. Comme son maître allait se maudire d’avoir cru en la parole des Ifranjs !

## Damas, fin d’après-midi de youm al arbia 3 rabi’ al-thani 584^[Mercredi 1er juin 1188.]

Abu al-’Abbas soupira d’aise lorsqu’il sortit du bain de la rue du Palmier. Il se sentait détendu, propre et plein d’entrain. Pour une fois, il n’était pas gêné par la cohue habituelle qui régnait aux abords de Bab as-Sagir, hochant la tête d’un air satisfait tout en admirant les allées et venues depuis l’entrée du hammam. Il portait une de ses plus belles jubba, en lin fin d’Égypte, réhaussée de bandes colorées, et un turban soigneusement monté ornait son chef. Ce soir il devait recevoir quelques amis chez lui, et avait même commandé des musiciens pour l’occasion.

Il n’était pas particulièrement amateur de festivités mais la perspective d’un moment à discuter de tout et de rien en bonne compagnie le rassérénait. Il avait perdu son unique fils plusieurs années auparavant et, depuis, sa demeure était toujours un peu triste malgré les cris de ses petites filles. Son épouse Nusayba était encore jeune, et il priait Allah régulièrement dans l’espoir qu’il bénisse leur union d’un nouveau garçon.

Il finit par s’aventurer dans la foule, en direction de l’est. Habituellement il ne venait pas aux bains si loin de chez lui, vers darb ad-Daylam, mais il avait décidé de passer voir un négociant en livres. Cela faisait des années qu’il enrichissait peu à peu sa collection d’ouvrages. Il privilégiait les œuvres littéraires, poésie et contes essentiellement. Et, bien sûr, des exemplaires du Coran. Il avait eu l’occasion d’en acquérir un fragment lors d’un voyage lointain, quand il n’était qu’un jeune commissionnaire, à Kairouan. Depuis, il tentait de compléter ses volumes pour avoir en sa demeure l’intégralité des sourates du livre sacré. Mais c’était là un luxe rare, étant donné le prix exorbitant de ces ouvrages prestigieux.

La boutique ouvrait directement sur la rue par un couloir peu large encombré d’étagères où s’amassaient les recueils les plus modestes, qu’on pouvait s’offrir pour moins d’un dinar. De la poésie parfois, mais surtout des manuels scientifiques, utilisés par les savants et les hommes cultivés : astronomie, mathématique, médecine. Al-’Abbas en avait quelques-uns chez lui, comme les *Axiomes* d’Euclide ou l’*Introduction à l’astrologie* de Abu Ma’shar, achetés dans un lot pour une poignée de pièces. Le marchand, un jeune qui avait hérité l’affaire de son oncle, connaissait bien ses clients et allait de temps à autre chercher des ouvrages fort loin. Il s’avança en s’inclinant, les mains jointes en signe de respect, saluant avec emphase son visiteur.

« Vous arrivez fort bien, j’allais prendre une collation. Faites-moi le plaisir de m’accompagner. »

N’escomptant aucune réponse, il lança quelques ordres d’une voix autoritaire en direction de la cour à l’arrière de la petite pièce sombre. Puis il invita d’un geste son visiteur à prendre place sur un des tapis disposés sous un claustra par où s’insinuait la chiche lumière. Ils ne parlèrent que de banalités en attendant qu’on leur serve les plats : abricots cuits aux pistaches grillées, prunes séchées, le tout accompagné de lait miellé à l’eau de rose. Le boutiquier faisait des efforts visibles pour mettre l’éventuel acheteur en confiance.

Al-’Abbas était lui-même négociant, spécialisé dans les vivres, à destination de l’armée essentiellement, et il savait reconnaître un bon professionnel. Il appréciait d’être traité avec les égards dus à un client d’importance. Une fois rassasiés, les deux se turent un instant jusqu’à ce que le marchand, dans un sourire, glissât :

« Peut-être voudriez-vous voir le volume que j’ai déniché ? J’ai espoir qu’il convienne bien à vos goûts. »

Al-’Abbas acquiesça, s’essuyant les mains avec soin tandis que son interlocuteur sortait un volume de bonne taille, plus large que haut, enveloppé dans un sac d’étoffe. Il le dévoila avec un art consommé, le posant entre eux sur un lutrin avant d’en révéler le contenu. C’était indéniablement un ouvrage de premier plan, réalisé sur parchemin. La page qu’admirait al-’Abbas comportait trois lignes magnifiquement tracées dans une encre noire, ponctuées de ronds bornant la fin des versets. Les voyelles brèves y étaient indiquées en rouge, et des diagonales élégantes marquaient les points diacritiques. Al-’Abbas ne résista pas à l’envie de lire avec respect le texte devant ses yeux.

« Et quiconque croit en Allah, Allah guide son cœur[^sourate64]… »

Il demeura muet un petit moment, en admiration devant la page, fasciné. Le vendeur avala plusieurs gorgées de boisson en silence, souriant, confiant. Il attendait la question, qui mit un certain temps à venir.

« Quel serait le prix que vous demandez ?

— Il s’agit là d’une œuvre d’exception, même si elle ne contient que quelques sourates. Il me semble que six dinars serait prix honnête. »

Al’-Abbas souffla. C’était une belle somme, plus que ce qu’il dépensait habituellement pour sa famille en un mois. Mais c’était indiscutablement un très bel ouvrage, peut-être destiné à l’origine à une mosquée.

«  Cela me semble cher. J’ai déjà beaucoup de dépenses à faire en ce moment, avec les trousseaux de mes filles : plusieurs bijoux à réaliser, boucles d’oreilles et bagues.

— Oui, je comprends, mais il s’agit là d’un travail sur parchemin, pas sur simple papier. Admirez le tracé, c’est une main élégante et talentueuse qui a fait ce travail. On ne voit pas beaucoup de ce genre d’ouvrage par ici, c’est un style de l’Ifriqiya.

— Oui, j’avais reconnu. Il est vrai que c’est un bel ouvrage, mais aussi une belle somme !

— Vous aviez quel montant à consacrer à un livre sacré ? »

Le marchand retint un sourire intérieur. La discussion était lancée, en espérant qu’elle aboutisse à un tarif juste. Il conservait quelques arguments de poids en réserve, pour maintenir le montant élevé. Mais quoi qu’il arrive, il ferait une bonne affaire. Il avait récupéré le livre dans un lot vendu une bouchée de pain par des Turkmènes de retour du pillage des terres Ifranjs. Ces barbares incultes ne savaient pas différencier un manuscrit rare d’un recueil de fables populaires. Quels arriérés que ces nomades qui n’accordaient aucun prix aux ouvrages de qualité, préférant l’évanescence de leurs récits contés. ❧

## Notes

Le titre est la deuxième partie de l’expression *Verba volant, scripta manent*, attribuée à Caius Titus lors d’un discours au sénat romain et qu’on traduit en français par « Les paroles s’envolent, les écrits restent. »

Je n’ai pas résisté à l’envie de lier un souvenir conté par Usama ibn Munqidh dans ses mémoires (le pillage de ses biens par le roi Baudoin III malgré le sauf-conduit octroyé à sa famille) avec une page de Coran présentée lors de l’exposition « Les trésors fatimides du Caire ». Il est toujours émouvant de supposer les différents commanditaires, possesseurs successifs de ces ouvrages multiséculaires, témoins palpables de notre passé commun.

Cet émir, Usamah ibn Munqidh, personnage de première importance dans le Moyen-Orient des croisades était un érudit, un poète reconnu, bien qu’il soit désormais plus connu pour ses anecdotes historiques. J’ai donc imaginé lui attribuer un livre parmi les milliers qu’il indique avoir détenus. Cette perte fut d’ailleurs celle qui lui fut la plus pénible et il en parle encore avec affliction plusieurs dizaines d’années après.

## Références

Ashtor Eliyahu, *Histoire des prix et des salaires dans l’orient médiéval*, Paris : École Pratique des Hautes Études, 1969.

Cobb Paul M., *Usama ibn Munqidh. The book of Contemplation. Islam and the Crusades*, Londres : Penguin Books, 2008.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem. A corpus. Vol. IV The Cities of Acre and Tyre with Addenda and Corrigenda to Volumes I-III*, Cambridge : Cambridge University Press, 2008.

Collectif, *Trésors fatimides du Caire*, Catalogue de l’exposition présentée à l’Institut du monde arabe du 28 avril au 30 août 1998, Paris : Institut du monde arabe, Gand : Snoeck-Ducaju & Zoon : 1998.
