---
date: 2012-03-15
abstract: 1146. Être enfant à Vézelay au XIIe siècle n’est pas forcément synonyme de vie difficile, sauf lorsque l’on a de farouches adversaires à abattre… Souvenirs d’enfance d’Ernaut…
characters: Ernaut, Raoul de Vézelay, Droin le Groin, Ermont de la Treille, Laurette
---

# Les croisés du Poron

## Lieu-dit du Poron, environs de Vézelay, après-midi du jeudi 6 juin 1146

Une brise parfumée parcourait les feuillages des saules et des bouleaux parsemant la rive de la Cure. Quelques moineaux jouaient à se poursuivre en piaillant, indifférents aux hommes occupés à tirer l’eau salée des sources proches pour en extraire le sel. Deux manouvriers exténués faisaient la sieste à l’ombre des arbres, ronflant de concert, les pieds à l’air après un bain revigorant. Non loin de là, d’un taillis plus serré fusaient quelques voix enfantines. L’une d’elles, plus forte et plus assurée se fit entendre une nouvelle fois par-dessus le clapotis :

« J’ai dents sans tête et queue sans cul. Qui suis-je ? »

La petite assemblée d’enfants groupée autour du tronc couché par-dessus la rivière regardait d’un air interrogateur Ernaut, l’auteur de la devinette.

Celui-ci arborait une coupe à l’écuelle récente, le cheveu châtain raide dessinant comme un casque sur son crâne. Son visage, assez massif, pétillait d’intelligence et ses yeux couleur azur scintillaient tandis qu’il évaluait son public. Bien qu’ayant passé l’âge de raison seulement depuis peu, il dépassait tous ses camarades d’une bonne tête, même ceux qui avaient deux à trois ans de plus que lui. Ses joues rebondies et sa cotte ventrue lui donnaient un aspect bonhomme, démenti par l’éclat sauvage qui habitait son regard. Le sourcil froncé, il toisait son petit monde l’air narquois, lançant de temps à autre un caillou dans le cours d’eau. Il attendit que quelques propositions fausses soient lancées avant de s’écrier, l’air triomphant :

« C’est un râteau, bande de nigauds ! »

Une fois encore, il triomphait. Satisfait de cette victoire, il descendit de son perchoir et proposa à ses compagnons d’aller s’aventurer vers les sources salées, où se trouveraient bien quelques valets dont ils pourraient se moquer. Un petit brun frisé à l’air renfrogné, peu emballé par la proposition, avança une autre idée :

« Et si on jouait plutôt à la dîme ? »

Agacé par cette remise en cause de son autorité, mais voyant là une occasion de la conforter, le jeune géant acquiesça de la tête.

« Si fait ! Je serai l’abbé et toi mon prieur. Et vous mes serfs, qui devrez aller me quérir de quoi payer. »

Traversant une clairière bordant les étendues où s’activaient quelques travailleurs sous le chaud soleil de printemps, il rejoignit leur repaire. Ce n’était qu’un roncier recelant un espace découvert, mais il devenait selon les jours monastère, château fort, voire navire en route pour le lointain.

Avec toujours à la tête de ce petit monde Ernaut, le fier-à-bras le plus imposant du groupe. Il choisit au passage un bâton et se tint de façon solennelle au centre, attendant ses serviteurs. Puis il distribua avec gravité des tâches diverses, depuis le ramassage de quelques feuilles jusqu’à la collecte de cailloux de couleurs variées. Tandis qu’il organisait les travaux, Raoul, son prieur désigné l’interpella :

« Ce serait quoi ton nom ? Pons comme le père abbé ? »

À l’évocation de ce maître peu apprécié de la population locale, Ernaut grimaça.

« Certes non ! Bernard, voilà joli nom de clerc. Comme le prud’homme qui vint à Pâques parler au roi et aux barons de la croisade outre les mers.

— Je l’ai ouï dire. Le père dit que c’était bien étrange d’encontrer clerc parlant d’autres choses que de cens et de taxes… »

Voyant une nouvelle occasion de briller, Ernaut se rengorgea, l’air précieux :

« Et à moi il a parlé ! »

Raoul ouvrit des yeux émerveillés.

« Ça alors, quelle bonne fortune ! Que t’a-t-il dit ?

— Eh bien…

— Menteries que voilà, il est resté en l’abbaye à tout moment » l’interrompit un grand gaillard maigrelet à qui il manquait deux dents.

— Et qu’en sais-tu, Droin-le-Groin ? rétorqua Ernaut, l’air rogue.

— J’en sais que tu racontes que niaiseries, et puis cet idiot de Raoul gobe tout comme goujon crétin. »

Ernaut s’avança vers son détracteur, le visage haineux, le bâton en main. Il ne s’arrêta qu’une fois collé à lui et le provoqua d’un mouvement de menton.

« Tu as souhait de perdre d’autres dents, crapaud sans cul ? »

Puis il le poussa en arrière d’un geste brusque. Mais Droin s’était préparé à la rebuffade et résista, attrapant son adversaire à l’épaule. Bientôt ils roulèrent au sol, parmi les caillasses et la poussière, s’invectivant de tous les noms d’oiseaux qu’ils connaissaient, ahanant comme bucherons à l’abattage, encouragés chacun par quelques partisans.

Lorsqu’Ernaut rentra chez lui, il se dirigea droit dans la cuisine. Il avait tenté de rendre sa tenue plus présentable, mais ne se faisait guère d’espoir. Sa cotte était déchirée à l’épaule et une de ses joues était mal en point, avec une large griffure qui courait depuis son nez d’où l’on voyait que du sang avait coulé. La servante qui lui avait ouvert la porte avait levé silencieusement les bras au ciel et était partie bien vite, certainement chercher la maîtresse de maison.

Heureusement pour le garçon, ce fut son père qui le trouva le premier. La cinquantaine passée, Ermont de la Treille marchait avec difficulté, gêné par son embonpoint. Son visage ingrat, veiné de couperose, s’illumina à la vue de son enfant et il s’avança, un franc sourire aux lèvres. Il lui ébouriffa les cheveux affectueusement et s’esclaffa tandis qu’il s’asseyait à son tour sur le banc.

« Encore quelques comptes à régler avec de méchantes gens, petit lion ? »

Ernaut n’osa répondre, se contentant de renifler en silence. Il savait que la tempête n’allait pas tarder, même s’il se sentait plus confiant pour l’affronter désormais. Son père le fit asseoir à côté de lui et attira à eux un pichet et deux gobelets.

Ce fut à cet instant qu’entra dans la pièce Laurette, l’épouse du fils aîné, qui régnait sur la maison depuis son mariage. Les manches relevées, elle devait être occupée à quelque lessive dans l’arrière-cour de la demeure, avec des domestiques. Encadré de son voile de travail un peu passé, son visage était barré d’épais sourcils noirs, froncés comme bien souvent. Voyant son beau-père, elle marqua un temps, mais s’avança tout de même vers l’enfant, l’air franchement contrariée. Détaillant son aspect misérable, elle manifestait sa désapprobation par des mouvements de tête exaspérés.

« Ernaut ! Où as-tu été te rouler encore ? »

L’enfant demeura coi, baissant les yeux, espérant de prompts secours.

« Et bien ? J’attends ! Tu as vu ta cotte ? Crois-tu que les vêtements poussent sur les arbres, qu’il nous suffit de les cueillir ? »

Ernaut se rapprocha ostensiblement de son père.

« As-tu seulement idée du coût de tes bêtises ? Il va bientôt nous falloir te faire neuve cotte à chaque saison !

— Paix ma bru ! Ce n’est qu’un enfant ! » répondit doucement le vieil homme, posant un bras protecteur autour de son fils.

La jeune femme pinça les lèvres.

« J’entends bien, père, mais il est désormais en âge de comprendre qu’il fait mal. Je suis fatiguée de devoir repriser sans cesse ses tenues, il use le linge si vite !

— Ce n’est pas quelques aiguillées de fil qui nous mèneront à la ruine, voyons ! Avec tout ce que je vous ai légué, vous devriez bien trouver de quoi vêtir mon dernier né. »

Les épaules de la jeune femme s’affaissèrent. Elle inspira lentement, vexée par cette rebuffade puis quitta la pièce, le visage sombre. Le vieil homme fit un clin d’oeil à son enfant puis se leva du banc, en direction du cellier.

« Quelques morceaux de fromage nous feront oublier cette mégère, qu’en penses-tu, garçon ? »

## Abords du village de Saint-Pierre, matin du mardi 11 juin 1146

La bande s’était donné rendez-vous aux abords de la vigne de l’Alemant, où le père d’un des gamins était employé comme garde. En attendant les retardataires, Ernaut rongeait son frein, impatient d’en découdre. Non content de lui chercher querelle, Droin s’était mis en tête de constituer son propre groupe. Comble des problèmes, il avait revendiqué, et occupé, le carré de roncier qu’Ernaut considérait comme son fief. La guerre était inévitable, il ne fallait pas laisser une telle rébellion s’installer.

Raoul et les amis fidèles avaient donc retrouvé Ernaut au départ du sentier qui rejoignait la clairière des sources salées afin d’organiser un assaut. Pour l’heure, ils patientaient aux abords du chemin caillouteux menant à Saint-Pierre et de là à Vézelay, perché sur sa butte, à une demie lieue à peine. Un colporteur au visage ridé comme une pomme accompagné d’un petit âne lourdement chargé les dépassa, amusé de voir leurs préparatifs guerriers. Une poignée parmi les plus courageux s’était munie de bâtons, transformés en épées avec leurs gardes lacées d’un cordon de cuir. Les autres se contenteraient de lancer des cailloux de loin, voire de seulement invectiver leurs adversaires.

Équipé d’un morceau de charbon, Raoul s’appliquait à tracer une croix sur chaque épaule, afin de bien marquer le caractère sacré de leur entreprise. Ernaut avait insisté pour qu’ils arborent ce symbole, car il avait entendu le sermon qu’avait fait le fameux Bernard, le moine présent quelques semaines auparavant pour Pâques. Il en avait retenu que ceux qui se battaient avec un tel signe sur eux ne pouvaient connaître la défaite, que Dieu était de leur côté. Bien que confiant dans ses capacités, le jeune garçon jugeait plus prudent de s’assurer d’un allié de poids.

Tandis qu’il lorgnait au loin dans l’espoir que ses dernières recrues allaient enfin arriver, Ernaut vit une troupe de cavaliers armés qui avançaient au pas, descendant visiblement de Vézelay. Il les avait croisés plusieurs fois alors que leur chef s’entretenait avec son père. C’étaient des soldats du comte de Nevers, venus en mission. Ernaut n’avait guère compris de quoi il relevait, mais il avait entendu dire que cela amènerait certainement encore des problèmes avec l’abbé Pons, qui s’était toujours opposé au vieux Guillaume de Nevers[^guillaumenevers]. Ernaut s’en moquait, il admirait la morgue de ces hommes en haubert de mailles, l’épée au côté et le casque sur la tête. Lorsqu’ils laçaient la ventaille de leur coiffe, on ne voyait plus que leurs yeux intrépides, encadrant le nasal d’acier.

Les autres gamins vinrent regarder passer la petite troupe, bouche bée devant ces anges de la mort à l’attitude désinvolte. Le chef de l’escadron, monté sur un superbe étalon bai à l’allure anxieuse, reconnut Ernaut et lui fit un rapide geste amical. Il s’esclaffa, en désignant les enfants à ses hommes :

« Voyez donc compaings, que voilà féroce troupe prête à massacrer les Turcs ! »

Ernaut ne pouvait laisser croire qu’il ne s’agissait là que de déguisements. Il fit un pas et lança à la cantonade :

« Nous allons à l’assaut de quelques méchants garçons qui nous cherchent noise. Le Seigneur est avec nous, car nous sommes dans notre droit. »

Le chevalier arrêta sa monture et examina le garçon devant lui.

« Par la Barbedieu, que voilà marmot plus fier qu’un pet ! Qui a osé s’en prendre à toi et les tiens ?

— Il a pour nom Droin, mais ne vaut guère plus qu’étron de loup. Une tête vérolée posée sur un tas pourri, voilà ce qu’il est. »

Le cavalier hocha la tête, un sourire aux lèvres.

« Tu parles de vrai, marmouset. Il en faudrait plus comme toi dans l’ost ! Rappelle-moi donc quel nom est tien, que je prenne garde à ne jamais croiser ta route qu’en ami. »

Indifférent à l’ironie, qu’il avait à peine perçue, Ernaut bomba le torse et leva le menton.

«On me nomme Ernaut, sire, Ernaut de Vézelay, fils d’Ermont de la Treille.

— Et bien, Ernaut, fils d’Ermon, je suis aise de te connaître mieux. Si d’aventure dans les années qui viennent, tu as désir d’un peu d’action, demande donc après moi. Je suis Crestien de Breban, chevalier en l’ost le comte Guillaume de Nevers. Je suis assez souvent en votre cité de Vézelay… »

Puis le chevalier fit pivoter sa monture et salua de la main levée. Ernaut inclina la tête et regarda la troupe de soldats reprendre sa route d’un pas lent. La poussière du chemin était d’un blanc éclatant sous le soleil montant. Il se retourna vers ses hommes, aussi déterminé qu’un taureau d’arène. Les autres le dévisageaient, mi-effrayés, mi-admiratif.

« Il est temps de nous rendre au repaire, compaings. Qu’ils broutent la boue une fois pour toutes ! »

Puis il s’enfonça sur le sentier ombragé qui partait en direction des sources salées. Sentant qu’il fallait donner de l’entrain à ses hommes, il avait prévu de chanter une comptine qu’ils entendaient de temps à autre braillée par quelques soudards avinés ou impertinents. Ernaut entonna donc le début de la chanson, qui les avait fait tant rire la première fois qu’ils l’avaient déclamée aux moines de l’abbaye. Intrigués, quelques journaliers occupés à palisser les vignes levèrent la tête. Ils reprirent bien vite l’ouvrage, amusés par la petite troupe armée qui déclamaient d’une voix vaillante bien qu’aigrelette les couplets salaces :

>« J’ai outil bel et adroit,  
>Tantôt courbe tantôt droit.  
>Dieu qu’il est beau quand il s’étend,  
>Et ne vaut rien si ne se tend.  
>Je pousse aval, je tire amont,  
>Je touche au loin, et bien profond.  
>Fier-Archer voilà mon nom ! »  

Sous le chaud soleil de juin, parmi l’herbe jaunie du chemin, les graviers crissaient sous les souliers. D’un pas martial, ils frappaient le sol en cadence, effrayant papillons, étourneaux et musaraignes. Braillant un hymne qui n’avait que peu à voir avec les psaumes, ils avançaient fièrement vers la bataille. Les combattants de la Foi étaient sur le sentier de guerre, bien déterminés à reprendre leur territoire sacré, le roncier du Poron. ❧

## Notes

Depuis quelques années, la conception traditionnelle de la condition des enfants au Moyen Âge a bien évolué. À travers les sources scripturaires et les découvertes archéologiques, les chercheurs nous brossent un portrait beaucoup moins noir qu’envisagé depuis les travaux de Philippe Ariès. Il existait un temps pour s’amuser, y compris dans les couches les plus populaires, et si certaines situations décrites (surtout dans les sources judiciaires) attestent de maltraitance ou d’exploitation, voire de viols, on ne peut négliger qu’il s’agisse là d’un biais de perception.

Les enfants étaient très nombreux, représentant une partie importante de la population à une époque où la mortalité infantile était dévastatrice. Malgré ces décès fréquents, l’attachement des parents était bien réel, et même les clercs, parmi les plus prompts à la punition, faisaient parfois montre de clémence envers ceux dont l’innocence était sans cesse vantée. La figure de saint Augustin, petit chenapan voleur de poires devenu Père de l’Église demeurait une référence incontournable.

Cette nouvelle est parue dans le numéro 41 (décembre 2011 — janvier 2012) du magazine Histoire et images Médiévales. Des illustrations ont été réalisées à cette occasion par Callixte, qu’on peut retrouver sur [son site](http://www.callixte.com/index.php?2011/12/01/80-les-croises-du-poron).

Retrouvez la chanson du *Fier archer* en intégralité sur le site Hexagora.

## Références

Alexandre-Bidon, Danièle, Lett Didier, *Les enfants au Moyen Âge*, Paris : Hachette, 1997.

Alexandre-Bidon Danièle, Riché Pierre, *L’enfance au Moyen-âge*, Paris :Seuil, 1994.

De Bastard DʼEstang, Léon, « Recherches sur lʼinsurrection communale de Vézelay, au XIIe siècle », Bibliothèque de l’École des Chartes 12, no. 1 (1851): 339-365.
