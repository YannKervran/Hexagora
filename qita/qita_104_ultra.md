---
date : 2020-06-15
abstract : 1160. Une nouvelle campagne militaire en territoire musulman offre l’occasion à chacun de progresser sur le chemin de ses choix professionnels, personnels voire intimes.
characters : Ernaut, Droart, Baset, Eudes, Raguenel, Gosbert, Onfroy de Toron, Isaac de Naalein, Brun, Eudes de Saint-Amand
---

# Ultra

## Jérusalem, hostel d’Ernaut, début de soirée du vendredi 5 février 1160

Dans un coin de pièce, un maigre feu couvait sous l’épaisse marmite de céramique où frémissait un pâteux brouet au lard. Au centre, autour d’une table improvisée de planches, quatre silhouettes avalaient le consistant mélange avec de longs soupirs de satisfaction. Droart, Eudes, Baset et Ernaut avaient passé la journée à s’activer dans la maison que ce dernier occuperait bientôt avec Libourc. Même si des artisans avaient fait le plus gros du travail, il demeurait de nombreuses tâches annexes, petits détails qui ne pouvaient s’abandonner aux soins d’inconnus.

Les lieux commençaient à avoir fière allure et le mobilier s’entassait dans la grande pièce, dans l’attente d’être installé de façon définitive. Plusieurs coffres, jarres et tonneaux serviraient à entreposer nourriture et effets. Du linge de maison, couvertures et draps pour faire les couchages, était méticuleusement roulé, protégé d’herbes aromatiques pour repousser les insectes. Ernaut savait que ses choix initiaux seraient certainement remis en cause par sa future épouse, mais il estimait que c’était dans l’ordre des choses. Leur logement serait le royaume de Libourc et cela lui convenait parfaitement. Il espérait simplement que l’endroit serait à la hauteur des attentes de sa fiancée. Sanson était passé plusieurs fois vérifier les installations et Mahaut avait tenu à inspecter linge et matériel de cuisine. Malgré ses habitudes pincées, elle avait lâché quelques remarques, qui pour n’être pas enthousiastes, ne marquaient aucun désaccord ni critique, ce qui mettait Ernaut en joie. Il s’enorgueillissait de clouer le bec de sa belle-mère, toujours prompte à le dévaloriser selon lui.

Baset, s’essuyant la main de son revers de manche après une longue rasade de vin, servit ses compagnons.

« Vous avez ouï rumeurs de la chevauchée pour tantôt ?

— Si tu parles de celle qui se déroule en lit royal chaque nuit, je suis prêt à en entendre ! ricana Droart.

— Oui-da, le sire vicomte en discutait l’autre jour avec Brun confirma Eudes, sans prendre note de la moquerie de son ami. Le sire Baudoin a reçu coursiers disant que Norredin[^nuraldin] est fort occupé au septentrion. Ses appétits pour le Cham[^bilad_al_Sham] vont de nouvel s’aiguiser…

— Que voilà belle possibilité, il y aura de certes quelques mailles à se partir ! » approuva Ernaut.

Droart fit une moue, retrouvant son sérieux.

« Si les rois et princes n’ont pu cueillir le fruit alors qu’ils étaient nombreux, cela me semble hasardeux[^croisade2b].

— Connaissant Baudoin, il ne désire pas à toute force prendre l’arbre quand il peut se contenter du fruit. Il a toujours grand appétit de monnaies, qui lui filent entre les doigts comme l’eau claire.

— J’espère qu’ils n’en appelleront qu’à volontaires, grommela Baset. Je n’ai nul goût à coucher sur la pierre, à manger de la poussière et me dessoiffer de flaques croupies.

— Pour ça, je te dis *Amen*, compain, renchérit Droart. Je suis bien aise de demeurer en nos murs plutôt que de marcher sous le soleil, avec les flèches turques pointant sur mon cul comme récompense.

— Je pense que j’en serai » déclara Eudes, un peu solennel. 

Un silence se fit autour de la table, le petit monde de la sergenterie avait eu de grands espoirs lorsque le vicomte Arnulf était parti pour la forteresse de Blanchegarde, quelques mois plus tôt, emmenant Ucs de Monthels avec lui. Mais c’était Fouques, jeune homme ambitieux, familier d’Arnulf, qui avait eu le poste de mathessep. Et c’était le tonitruant Eudes de Saint-Amand qui était désormais vicomte, en plus d’être châtelain de Jérusalem. Ses imprécations et sa brusquerie avaient bouleversé la vie dans la Cour des bourgeois et l’hôtel royal, habitués depuis des années aux manières feutrées et diplomates d’Arnulf.

« Il me semble douteux que le sire vicomte choisisse Gaston, malgré son expérience, car il n’apprécie rien tant que bras solides et pieds vaillants, indiqua Ernaut. Je compte bien aussi être de ceux qui porteront le fer pour cette marche.

— Tu devrais plutôt penser à tes accordailles qui ne sauraient tarder, Ernaut. Pâques sera tôt passé. Libourc aura besoin de toi pour la cérémonie, as-tu oublié ? ironisa Droart.

— De vrai, compère. Mais si j’encroie l’usage du sire Baudoin, ainsi qu’il a fait voilà quelques années à Panéas, cela ne demandera guère long temps. D’ici là, j’aurais bel écot à verser en mon hostel.

— Baudoin a bien failli y laisser sa couenne, rétorqua Baset. Et grands barons ont lourd payé quelques destriers et menues monnaies pour la Secrète[^grandsecrete].

— Remarque que cette fois, ce n’est pas piétaille qui a subi le fer, que voilà bon augure ! » s’amusa Ernaut.

Eudes se leva et alla puiser quelques cuillérées de potage, prenant la parole à son tour.

« Fouques nous le montre bien, compères. Si on ne se fait pas connaître de barons, les meilleures places vont à d’autres, parfois plus jeunes, mais de certe plus habiles à vanter leurs talents. Quand le trésor royal est à sec et que la solde est reportée, Tiphaine a toujours grand mal à obtenir délais pour grain, vin ou viandes[^viandes]. Surtout que c’est souventes fois le moment où les accapareurs font leur gras. Alors si je peux aider à rentrer cliquailles en l’hostel Baudoin et y ajouter ma propre picorée, cela me semble bon choix. »

Ernaut piqua du nez dans son écuelle. Il avait en tête surtout la perspective de chevaleresques exploits à conter, de prouesses dignes du grand Charles[^charlemagne3]. Il appréciait le confort et le butin accumulé jusque-là lui avait offert l’occasion d’acquérir un bel hôtel où installer sa famille. Pourtant, ce qui faisait battre son cœur était plutôt l’aventure qui frappait à sa porte. Inconsciemment il jouait sans cesse de son pouce contre l’anneau donné par le prince d’Antioche lui-même quelques mois plus tôt. Il aurait bien échangé tous les trésors de la terre pour une autre soirée comme celle qu’il avait alors vécu.

## Damas, sud de la Ghûta, veillée du jeudi 25 février 1160

Les gouttes d’une pluie fine pointillaient les tuiles de l’appentis en contrebas de la pièce où s’étaient installés Raguenel et Gosbert, deux sergents montés responsables d’unités de fantassins. Depuis ce salon, ils bénéficiaient d’une magnifique vue sur les environs, la verdoyante oasis qui baignait de jardins et de canaux les abords de Damas. Quelques cahutes branlantes, des cabanes hâtivement assemblées, des parcs à chevaux et des monceaux de sacs de toile cirée s’égayaient partout dans la palmeraie. Parmi les arbres, on entendait l’activité d’un camp affairé, où les hommes goûtaient le bonheur d’avoir amassé un large butin sans trop avoir à ferrailler, comptant et recomptant à l’envi les sommes accumulées jusque-là. Beaucoup avaient d’ailleurs commencé à en dilapider le fruit en plaisirs éphémères, impatients de siroter le suc de la vie après en avoir enduré l’ivraie.

Le lieu était certainement une maison de repos, agrémenté de quelques matelas et nattes, ses propriétaires y apportant avec eux le confort de tapis et coussins lors de leur venue. Mais pour les deux hommes habitués aux nuits dans les buissons et aux haltes le long de sentiers caillouteux et arides, cela avait déjà un goût de paradis. Ils partageaient même un repas chaud, ragoût insipide réalisé avec les découvertes du jour, épaissi de blé concassé. Allongé d’un vin rubis, le souper leur faisait l’impression d’un festin, mangé à l’abri, ce qui en renforçait les saveurs, somme toute bien ordinaires.

Déchirant un pain plat, Raguenel en tendit un morceau à son compagnon.

« Si on arrive à mener tout ça jusqu’à nos murs, je me verrais bien laisser l’épée au fourreau. Finir sur belle moisson, c’est pas tant l’usage, pour nous autres…

— Tu comptes les œufs dans le cul de la poule… Il n’y aura pas de quoi vivre bien vieux.

— Je demande juste de quoi m’installer bon commerce de lames, de fers d’hasts et fourniment de guerre. Entre ceux qui partent sans guère de place pour emporter plus que simple palme et ceux qui arrivent avec désir de trancher du col mahométan, je pense que j’aurais de quoi cuire mon pain. Et avoir assez pour me trouver garcelette pouvant pétrir le nécessaire, pour mes vieux jours. »

Gosbert acquiesça, engloutissant sa cuillerée en expirant bruyamment à cause du liquide encore brûlant. Raguenel souffla sur sa bouchée avant de l’avaler. Il savait que le sujet de se retirer des combats était toujours délicat avec son compagnon. Celui-ci se racla la gorge et entama son couplet habituel.

« Je te souhaite d’avoir tout ce qu’il te faut, compain. Rien ne vaut enfançon pour garantir la vieillesse. Puisse ta future avoir ne serait-ce que la moitié des qualités de ma Peironelle.

— Et toi, tu n’as pas désir d’enfin lâcher le fer ? Profiter un peu de ton temps ?

— Je ne sais. Il y a encore la benjamine à doter, qu’elle trouve bon mari. Pas un vaut-rien comme nous autres ! » ricana-t-il.

Il lécha rapidement sa cuiller puis reprit. 

« Je t’ai dit que le Merle m’avait passé nouvelles, juste après la Noël ? Ça date, c’est un pérégrin parti avant Pâques dernier qui l’a encontré. Un gentil gars, le Merle, il t’aurait plu. Il s’occupe bien des miens. Je lui ferai porter ce que j’aurai grappillé ici. Je ferai peut-être encore une saison ou deux, puis je passerai outre la mer ultime fois. Revoir mes filles et mes fils, embrasser mon espousée. Que de bonnes années à venir ! Mais je tiens à achever ma promesse, que mes vœux soient parfaits. »

Raguenel hocha la tête, avalant une autre bouchée. Il avait appris voilà des mois que Merle était mort depuis bien longtemps et chacun savait que Gosbert dépensait sa solde et ses butins dans les bordels et les tavernes de Jérusalem, faisant rouler les dés à la moindre occasion. Il parlait pourtant à qui voulait l’entendre de sa famille, à qui il envoyait la moindre miette amassée, tandis qu’il accomplissait son devoir de pèlerin. Raguenel jeta un coup d’œil à la croix de tissu cousue au col du vêtement de Gosbert. Elle n’avait plus de couleur et avait été tant reprisée qu’elle ressemblait à un chiffon, lointain souvenir du ruban écarlate de son serment.

« Ça te dirait pas de t’associer avec moi ? On sait évaluer la qualité d’une lame au premier coup d’œil, et à tous deux, on a les noms de toute la sergenterie d’Antioche à Ascalon ! Fini la fatigue et les mauvaises nuits, les repas manqués et les flux de ventre…

— Je ferais quoi d’un commerce en la Terre ? Les miens sont au loin, je n’ai rien à bâtir ici, juste devoir de pérégrin défenseur de la Foi. Non, c’est amiable à toi, compère, mais tant que je serai en ces provinces, ce sera avec le glaive en main pour protéger la Croix. Une fois mon ouvrage accompli, je m’en retournerai. »

Gosbert acheva sa réponse d’un clin d’œil et se rinça la bouche d’une longue goulée d’une outre sentant la piquette. Il s’étira alors le dos puis se frotta le ventre de contentement.

« Crois-m’en, tu m’envieras bien vite si tu te retires dans ta modeste échoppe de ferraille. Plus que deux-trois Pâques, c’est bien le moins pour nettoyer méchante âme comme la mienne, disent les bons pères. Et je profiterai tantôt de la vie, tel soudan[^soudan2] en son palais ! »

Raguenel se dit qu’il devrait peut-être en parler à un ecclésiastique, justement. Peut-être les frères de Saint-Jean sauraient quoi faire. On racontait qu’ils guérissaient les corps et les cœurs, ou prenaient soin de ceux qui vivaient comme dans un rêve. Gosbert était un compagnon agréable, un soldat efficace et un bon meneur d’hommes, mais avec le temps, il semblait s’enliser dans un monde qui n’avait que peu à voir avec la réalité. Les moines auraient peut-être une réponse, dans leurs prières si ce n’est dans leurs potions.

## Damas, ouest de la Ghûta, veillée du vendredi 10 juin 1160

Dans les feuillages, quelques oiseaux nocturnes invectivaient les soldats entassés dans leur territoire. Une nouvelle fois l’armée de Jérusalem avait envahi le riche faubourg damascène et ses hommes y avaient pillé allègrement, infligeant aux munificents jardins plus de dégâts qu’une horde de criquets.

Parti se dégourdir les jambes après de trop longues heures assis à sa table dans sa tente à gérer des inventaires, transmettre des messages ou donner des ordres, Onfroy de Toron, connétable de Jérusalem, passait le long des cordes où on avait attaché les montures de l’hôtel royal. Quelques palefreniers dormaient là, dans le foin, partageant leurs nuits avec les bêtes qu’ils soignaient le jour. Onfroy aimait l’odeur piquante des chevaux, synonyme pour lui de liberté juvénile, perdue depuis qu’il avait pris en charge son office auprès de Baudoin. Il ne regrettait nullement les choses, mais les senteurs éveillaient en lui le souvenir de ses années d’apprentissage, où il vivait quasiment tout le temps en selle.

Il déambulait en dégustant quelques maamouls[^maamoul] qu’un de ses valets avait dénichés au marché. Cette dernière campagne lui offrait également la chance de savourer des dattes parmi les meilleures, au plus frais de leur ramassage. Tout ce qui en était tiré comme pâtisserie ou boisson lui agréait fort et, s’il ne cédait que rarement à la gloutonnerie, il ne résistait que peu à profiter d’un plat apprécié, ni à s’initier à de nouveaux goûts.

Il vit passer Isaac Vacher et l’apostropha amicalement. C’était le neveu du regretté Bernard, qui servait désormais depuis un moment dans l’hôtel royal. Un solide chevalier, habile à la lance, mais parfois trop impétueux, comme tous les bacheliers[^bachelier]. Son air préoccupé, en ces temps prodigues, avait incité Onfroy à le héler. Le jeune homme s’approcha, prit meilleure contenance, faisant bon accueil à un puissant baron qui l’avait toujours traité en ami, voire en parent.

« Je te vois bien aigre alors que tout nous sourit depuis des mois, mon garçon. Quelque souci dont je pourrais te soulager ?

— J’ai demandé congé pour aller en la cité et il m’a été refusé.

— C’est sage décision. Nous n’avons nulle trêve avec Damas, ils attendent juste que nous partions. Et, de toute façon, vu que nous avons couru sus à leurs murailles à peine le dernier traité obsolète, tu aurais fort mauvais accueil là-bas.

— J’ai quelques noms qui sauraient me faire ouvrir des portes, rassurez-vous. Et je parle leur idiome parfaitement.

— Ainsi que moi ! Mais pour autant, je n’ai guère désir de voir fils de bonne famille se perdre en leur pouvoir en ces temps. »

Onfroy se doutait de ce qui motivait le jeune homme, mais il lui fallait l’entendre afin d’en combattre le bien fondé, aussi posa-t-il la question en proposant nonchalamment un maamoul.

« Et qu’irais-tu faire là-bas ? Nous les avons rançonnés et pillés deux fois en l’espace de quelques mois, ils ne risquent guère d’avoir attentive oreille à tes demandes. »

Isaac hésita à choisir un biscuit, se décida finalement à en avaler un, prenant son temps pour le mâcher tandis qu’il réfléchissait. Il savait que c’était impoli de faire attendre une personne aussi importante que le connétable, mais il avait compris que ce dernier lui avait accordé un délai en même temps que la friandise. Chacun savait exactement ce dont il s’agissait, au fond.

« C’est rapport à mon oncle. Peut-être certains ici en connaissent plus qu’on a voulu nous en dire[^voirqitavacher]. Il mérite qu’on voit s’il n’est pas à espérer qu’on paie sa rançon…

— Penses-tu que nous n’aurions pas obtenu la libération du porte-bannière du roi s’il avait été en geôle quand Ayyoub a versé les dinars, avant Pâques ?

— Peut-être ne sait-il pas qu’il est détenu ? Des brigands auraient pu s’en emparer.

— Que voilà bien redoutable captif pour simples malandrins de chemin. Outre le fait qu’il aurait fallu force soldats aguerris pour prendre Bernard, nul maraudeur n’oserait se brûler les doigts avec si dangereux marron. »

Il se rapprocha d’Isaac, baissant la voix et adoptant un ton paternaliste.

« Tu sais comme j’étais lié à ton oncle. Penses-tu que je n’ai pas cherché ? Que je n’ai pas lancé coursiers et ambassadeurs ? Si le roi l’a battu froid pour cette sombre histoire, il est resté féal en mon cœur. »

Il posa une main robuste sur l’épaule du jeune homme.

« Ton oncle, malgré toute la hardiesse qui l’habitait, a quitté ce monde de douleurs. Admets-le. Sinon tu courras après des ombres. Cela ne diminue en rien l’éclat de sa vie, sa droiture et sa vaillance !

— Il mérite bien chrétienne sépulture, pour tout cela, non ? Sinon comment pourra-t-il connaître la Résurrection à la fin des temps ? »

Onfroy soupira longuement, retira sa main.

« Il y a bien assez de souffrances en ce monde pour ne pas porchacier les diables, mon garçon. » ❧

## Notes

Le début de l’année 1160 fut relativement faste pour le royaume de Jérusalem. Profitant des difficultés de Nūr ad-Dīn au nord, Baudoin pilla de nombreuses zones entre la terre de Suète, par delà le Jourdain, jusqu’aux abords de Damas. Après une première trêve de trois mois qui lui permit de rapporter son butin en toute sécurité, il revint une nouvelle fois ravager les territoires musulmans avant l’été et le retour du prince d’Alep.

Ultra : *latin*. Au-delà de, par delà, en avant, plus loin.


## Références

Crawley Charles, *Medieval lands. A prosopography of medieval European noble and royal families*, « Jerusalem nobility » http://fmg.ac/Projects/MedLands/JERUSALEM.htm ( v4.0 Updated 27 February 2019, consultée le 11/06/202)

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972.
