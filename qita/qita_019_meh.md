---
date: 2013-06-15
abstract: 1157. Si désormais se vêtir n’est plus forcément onéreux, cela n’était pas le cas pendant des siècles. Les moutons et la laine constituaient les premiers éléments d’une chaîne de production éminemment complexe, qui aboutissait à une grande variété d’étoffes. À travers les destins de personnages qui ne sont pas sans évoquer les vicissitudes de la roue de la Fortune, si chère aux auteurs médiévaux, une première découverte à la rencontre d’une des industries les plus abouties du Moyen Âge.
characters: Aymar, Frère Chrétien d’Emmaüs, Droin le Groin, Enrico Maza, Gringoire la Breite
---

# Mêh

## Plateau du Golan, fin d’après-midi du mardi 12 février 1157

La poussière soulevée par les troupeaux agités embrumait les buissons gris. Hommes et bêtes respiraient de la craie, la face poudreuse collée de sueur. Cris, hennissements, bêlements, insultes, hurlements. Le chaos s’était abattu sur les paisibles pâturages au nord-est de Baniyas. Là-dessus, un ciel bleu acier hébergeait un soleil triste, sans chaleur. Un étalon paniqué roulait des yeux affolés, dressé sur ses postérieurs, prêt à frapper de ses sabots quiconque s’approcherait de lui. Des moutons à queue grasse, imbéciles et inquiets, tentaient de se faufiler entre ses jambes et celles de ses compagnons, indifférents aux coups. Plusieurs soldats, les bras écartés, s’efforçaient de les regrouper en sifflant, hurlant, ajoutant à l’effervescence.

Il fallut un long moment avant que le plateau retrouve un peu de calme. Une centaine de têtes de bétail avaient été rassemblées près d’une zone buissonneuse et la présence de nourriture avait fait miracle. Une quinzaine de chevaux de belle allure, dont l’étalon, étaient surveillés par une troupe d’une vingtaine d’hommes, le front las. De temps à autre, un cavalier en arme venait donner des ordres, emmenant avec lui une poignée de fantassins pour une mission. Près des moutons, assis autour d’un rocher, plusieurs soldats soufflaient, partageant l’eau croupie mêlée de vin de leurs outres, massant les corps fatigués, nettoyant leurs ecchymoses ainsi que font les loups après la chasse. L’un d’eux arborait une ancienne cicatrice au visage et parlait avec un fort accent génois. Il n’était pas dans la troupe depuis longtemps et s’était pourtant déjà taillé une belle réputation. Tout en se reposant, il graissait la corde de son arbalète, frottait et rangeait les carreaux qu’il avait récupérés après quelques tirs. Un de ses compagnons lui tendit la gourde, regardant les nombreuses brebis en train de paître à leurs côtés.

« J’espère qu’on pourra s’en gloutoyer une tantôt, après tous nos efforts. J’ai si grand faim que je pourrais avaler l’étalon !

— Le capitaine n’a rien dit en ce sens. Le butin est au roi et à ses féaux. T’es pas baron que je sache. »

Un sourire hargneux fendit la face craquelée du soldat.

« Si une ouaille a grande navrure, elle ne saura marcher jusqu’à nos terres. On pourrait la manger pour ne pas perdre sa viande.

— Certes, s’il s’agit de ne pas gaspiller, c’est bien différente chose ! »

Le petit groupe s’esclaffa. Ils savaient que le butin était considérable : au moins des centaines de moutons, de chèvres et de chevaux avaient été pris, sans compter les nombreux captifs. Quelques Turcomans avaient tenté de résister les armes à la main, vite abattus. La bataille s’était résumée à une moisson grasse sans que le bras n’ait guère à se fatiguer à faucher.

Au milieu du groupe, appuyé contre le rocher, un garçon au poil brun se rinçait le flanc avec le contenu de son outre, tout en grimaçant, le visage en sueur. Le rouge de son sang se mêlait à la boisson et il tentait d’en endiguer le flot en y appliquant un chiffon sale, vite imbibé. À côté de lui, des vêtements déchirés, souillés, voisinaient avec d’autres, prêts à être utilisés. Il serrait les dents, les yeux fébriles tandis qu’il compressait la plaie.

L’arbalétrier se tourna vers lui, curieux, examinant la blessure.

« Sans emplâtre, ça ne s’arrêtera pas de saigner, compère…

— Je sais bien, Maza, seulement il n’y a rien ici pour ça » croassa l’autre.

Se penchant plus près, Enrico Maza souleva doucement le linge. Les chairs étaient tranchées profondément et des bulles éclataient tandis que le liquide vital s’écoulait à gros bouillons. Il échappa une grimace mécontente.

« Je vais voir si je ne trouve pas quelque chirurgien pour t’arranger ça ! »

Il se leva, empoignant ses armes et son carquois, sans jeter un regard à son compagnon. Il savait que la seule chose qui serait utile à celui-ci était un prêtre. La lame avait touché les entrailles du malheureux et ce n’était plus qu’un mort en sursis, un cadavre inconscient de son état. Maza n’avait pas envie d’assister à sa lente agonie. Il en avait vu assez. Il était là depuis trop peu de temps pour s’être fait des amis, et avait appris à se défier des liens qui se tissaient entre les hommes, alors même que leur destin à tous se jouait les armes à la main. Il déambula jusqu’à un autre groupe où il fut accueilli par des regards méfiants. Voyant que les soldats faisaient cercle, il comprit qu’on se partageait un butin en catimini, certainement prélevé au nez et à la barbe des chevaliers et des barons.

Aucun guerrier ne trouvait la solde suffisante et chacun agrippait ce qu’il pouvait dès qu’il en avait l’occasion. Cela devait juste demeurer discret, pour ne pas insulter les puissants et éviter de se faire dépouiller par plus fort. Il arriva dans une clairière bordée de peupliers, où des captifs attendaient, les bras noués dans le dos à une longue corde les reliant les uns aux autres, sous le regard attentif de sentinelles. Dans un bosquet, un chevalier se délassait en discutant avec un guerrier portant la cape noire à croix rouge des frères de Saint-Jean. Ils riaient et parlaient fort, heureux de leur bonne fortune.

Enrico avança, sans considération pour les silhouettes misérables qui piétinaient ou patientaient, écroulées dans la poussière et les graviers, le visage fermé. La plupart des prisonniers n’étaient pas des soldats, juste de simples bergers, des nomades qui faisaient paître les bêtes qu’on leur confiait. Rassurés par la trêve avec le royaume de Jérusalem, ils étaient venus dans les riches pâturages du Golan, aux abords de Baniyas. Seulement le roi Baudoin avait besoin d’argent. Le parjure plutôt que la ruine.

Maza ne retrouva ses compagnons que bien plus tard, sans avoir pu trouver quelqu’un qui aurait pu soulager un peu le blessé, ni prêtre ni chirurgien. Entretemps, ils s’étaient attaqués à un mouton qu’ils s’employaient à préparer pour le repas du soir. Comme lui, ils savaient tous qu’il n’y avait rien à faire pour le malheureux. Enrico Maza se pencha et examina le jeune homme, dont la tête avait roulé sur la poitrine. Il approcha son oreille des lèvres, écoutant avec attention. Il voyait la tache brune qui s’était répandue sous le blessé, avait imbibé toutes les étoffes. Il soupira, tourna les yeux vers les autres, qui taillaient dans la viande avec des rires gourmands. Alors qu’il tendait la main vers la sacoche encore accrochée à l’épaule, une voix l’interpela :

« Hé ! Maza ! Tu crois quoi ?

— Il aura pas usage de ça là où il va, Aymar !

— Je dis pas le contraire. T’avise pas de tout garder pour toi ! »

## Chateau Emmaüs, matinée du jeudi 16 mai 1157

Lâchant ses forces[^forces], Blayves se redressa en grimaçant, le dos rompu par l’effort. D’un signe de tête, il indiqua à son aide de libérer le mouton, qui s’éloigna en quelques bonds, surpris de se voir si léger et fort rafraichi. Beaucoup de bêtes, inquiètes, poussaient des bêlements anxieux, contrariées d’être séparées de leurs congénères.

Certaines paissaient sous la surveillance de valets et de chiens jusqu’aux abords du caravansérail, où un peu de fraicheur naissait au pied du mur, près de l’église. Il en demeurait encore un grand nombre à tondre. Le berger attrapa une calebasse et avala une gorgée, s’essuyant le front de la manche. Le soleil était déjà chaud et, malgré les frondaisons, la suée venait vite.

Contrôlant rapidement d’un œil que tout se passait bien chez les autres ouvriers, il se pencha et commença à plier soigneusement la toison. Il en profitait toujours pour couper les parties sales, les plus souillées et en arracher les brindilles. Le Boiteux avait bonne réputation, et c’était pour cela qu’il ne peinait jamais à trouver de l’embauche. Un père dur à la tâche et prompt à la colère et aux coups lui avait inculqué le goût du travail bien fait. Il n’hésitait pas lui-même à réprimander vertement ses manouvriers, maniant parfois la trique sur les échines des plus réfractaires.

Il porta la toison sur le chariot que les frères de l’Hôpital avaient placé à son intention puis descendit la pente vers le gros du troupeau. La sueur mouillait son cou, ses aisselles, son dos, brûlant de son sel la peau tannée par le soleil. Il s’essuya la nuque d’une main crispée, toute de tendons et de muscles. À peine fut-il près des bêtes que l’un des chiens vint le sentir et s’assit un court instant devant lui. Il sourit, caressa le crâne de l’animal sans s’arrêter pour autant. Il suivit le haut édifice à main droite, contournant l’église pour rejoindre la rue qui longeait la façade de l’hébergement destiné aux pèlerins. Deux chameaux lourdement bâtés de pièces de bois patientaient, le regard absent, mâchant une herbe rare qu’ils attrapaient de leurs lèvres mobiles.

Le village était quasi-désert, tout le monde étant affairé dans sa parcelle ou à l’abri de la chaleur. On entendait seulement le bruit d’un chantier de construction non loin : de nouvelles maisons se bâtissaient à l’entrée du hameau. Près de la fontaine, au centre de la place principale, une vieille femme filait, surveillant distraitement une marmaille agitée qui semblait la laisser indifférente.

Blayves pénétra dans le caravansérail, retrouvant la fraicheur du passage couvert menant à la cour autour de laquelle les entrepôts et hébergements se déployaient. Les pèlerins avaient pratiquement tous repris la route, et un grand calme régnait. Un chaton traversa en miaulant, un instant tenté d’effrayer des pigeons occupés à gratter à l’endroit où on attachait les montures. Il disparut en un éclair à l’approche de Blayves. Celui-ci se dirigea droit vers le côté ouest, au fond des arcades, où il savait probable de trouver le frère cellérier.

Celui-ci, un jeune homme d’origine syrienne, le cheveu aussi noir que sa tenue, recopiait depuis sa tablette de cire plusieurs chiffres sur un abaque à calcul tracé à même la table. Il accueillit le berger d’un sourire, reposant les tesselles servant de marqueurs. Blayves le salua d’un imperceptible signe de tête.

« Il faudrait envoyer valet vider le char, frère.

— Il est déjà empli des tontes du matin ?

— Certes, et bientôt nous devrons les poser au sol si rien n’est fait prestement. »

Le jeune clerc se mordit les lèvres.

« C’est que je n’ai personne de disponible pour le moment. »

Il hésita un moment, puis proposa d’une voix douce :

« Aucun de vos valets ne saurait mener la mule ? »

Blayves soupira, contrarié. Chaque fois, on tentait de l’abuser.

« Je ne saurais dire, et puis ils sont là pour les toisons, ils n’ont guère le temps de s’occuper de l’attelage.

— Je comprends, cela ne serait pas fort long.

— Si vous aviez voulu que je prévoie valet d’attelage, il aurait fallu me prévenir, frère Chrestien. Nous avons contrat pour la tonte, rien de plus, je ne peux payer mes gens à œuvrer pour votre part de l’accord. »

Le cellérier acquiesça en silence, comprenant que le berger ne céderait rien.

« Je vais voir ce que je peux faire pour vous envoyer valet d’ici peu.

— Fort bien. D’ici là si nous n’avons plus de place, nous entasserons les ballots devant le char.

— Je comprends, faites ainsi. »

Le Boiteux repartit d’un pas lent, longeant la cour à l’abri des arcatures, le visage toujours fermé. Le chien, qui l’avait suivi en catimini, l’attendait assis devant la porte, la queue balayant la poussière du chemin. Il sourit et lui caressa de nouveau la tête, l’invitant d’un geste à avancer. Finalement, il préférait la compagnie des bêtes.

## Jérusalem, entrepôts des hospitaliers de Saint-Jean, après-midi du jeudi 20 juin 1157

Debout face à une montagne de toisons empilées, Gringoire la Breite souriait. Les mains sur les hanches, il toisait l’or blanc, beige et brun entassé devant lui, à la lueur des lampes à huile. Il n’avait jamais vu semblable amas de laine de sa vie. Des doigts fébriles fouillèrent sa chevelure argentée, et il ne put retenir un gloussement de joie.

« Foutrecul ! Que voilà superbe vision, mon frère ! »

Le petit moine qui l’accompagnait lui adressa un regard courroucé en entendant le juron. Il n’eut pas le temps de lui en faire reproche que l’autre continuait sur sa lancée, postillonnant et gesticulant.

« Je suis fort aise de me rendre utile, d’autant que vos gens ne me semblent guère s’y entendre en cet ouvrage ! »

Il s’approcha d’un premier paquet et froissa quelques fibres de la main.

« Déjà, même s’ils ne savent partir poil de brebis de celui du bélier, ils auraient pu éviter de mêler bruns et ocres, blancs et sables ! Du travail de foutriquet, ça ! »

Il fit quelques pas et renifla un autre amas.

« Vous ne lavez pas les laines avant la tonte ce me semble ? J’avais appris ainsi avec mon père, moi. D’autant que chez vous, ça sécherait vite !

— Hé bien…

— Remarquez, ça dépend aussi de la bête, bien sûr, je ne voudrais pas vous faire affront. Je ne m’y entends guère à vos types d’ouailles. Ces animaux à grosse queue, ça me parait fort étrange tout de même ! Le poil en est-il correct ?

— Nous avons… »

Attrapant une touffe, le grand gaillard la porta à son oreille et fit un signe impératif de silence au clerc. Puis il fit crisser les fibres avec attention, échappant un sourire, le regard perdu au loin dans les ténèbres des magasins non éclairés. Puis il lâcha un gloussement, qui s’entacha d’un rot.

« M’ouais, pas si mal. Seulement vous avez là de tout, j’en jouerai mes braies ! N’indiquez-vous pas à vos valets de séparer au moins les laines de bêtes mortes des autres ?

— Je ne…

— C’est très important, ça, vous savez. Sinon on perd du temps. Les meilleurs drapiers savent faire la différence. Bien faire les choses dès le départ, ça évite suées tardives comme disait mon vieux. »

L’hospitalier soupira, s’efforçant de faire bonne figure. Il s’occupait de la draperie de Saint-Jean depuis bientôt quinze ans et s’y entendait au moins autant que son interlocuteur, pourvu qu’on lui laissât le temps de s’en expliquer. L’homme, un pèlerin dont la femme était en couche dans la section des soeurs, avait insisté pour se rendre utile, en remerciement. Le pauvre moine commençait à croire que les autres avaient surtout cherché un moyen de s’en débarrasser.

Néanmoins, sa bonne volonté attirait la sympathie, malgré un comportement agaçant. Le frère drapier rongeait donc son frein, attendant patiemment que Gringoire ait fini sa diatribe pour pouvoir se mettre au travail. Gringoire s’avisa d’un paquet et l’examina plus précisément, faisant soudain silence. Le fait étonna suffisamment le clerc pour qu’il s’approche, intrigué. Il évita de justesse d’être bousculé quand l’autre se releva brusquement.

« Et voilà, j’en étais sûr ! On vous a mis poil à carder et bonne fibre à peigner en vrac ! »

L’hospitalier fronça les sourcils, contrarié.

« Êtes-vous sûr ?

— Ça, je ne m’y entends peut-être guère pour dire le Pater et l’Ave sans faute, par contre on ne me fait pas prendre toison de bélier pour fourrure d’agnel. Vous avez là de la méchante laine de second choix qui se cache parmi les belles fibres longues. On vous aura trompé sur la marchandise !

— Il ne peut être question de cela, mon frère.

— Par Dieu, dites-vous que je mens ? »

Le clerc sourit, amusé malgré le nouveau blasphème.

« Certes pas, c’est juste qu’il ne peut y avoir tromperie, car tout cela n’a pas été acheté, ce sont toisons de nos domaines. Simple erreur de tri, voilà tout. »

Gringoire écarquilla les yeux, le souffle coupé.

« Tout ça vient de vos terres ? Toutes… »

Pour la peine, il en perdait la voix, ce qui amusa d’autant plus le clerc.

« Enfin, pas exactement. La grande majorité, oui. D’aucunes toisons nous ont été données en obole aussi, perçues comme butin des batailles que nous menons. Ce sont paiements de redevances, et aussi et surtout lainages de nos bêtes. »

Gringoire acquiesça, impressionné.

« Il y a là de quoi tisser des vêtements pour le royaume entier !

— Nous donnons tout à peigner ou carder puis filer, et revendons ensuite beaucoup. Nous ne gardons que modeste part pour nous, pour nos besoins de draps dans les différentes héberges.

— C’est là véritable trésor royal, par la jambe-Dieu !

— Il nous faut bien payer le pain et les lits des pèlerins, mon frère. Nous commerçons nos biens pour pouvoir offrir bon accueil au pérégrin qui frappe à notre porte. »

Le grand costaud continuait de hocher la tête, admirant les ballots qu’il avait devant les yeux, imaginant des pyramides de laine qui s’étendaient à l’infini. Pour le coup, son silence inquiéta le clerc, qui s’avança à son tour, touchant le bras de Gringoire.

« Vous vouliez donc m’aider à faire le tri des meilleures toisons ? »

## Plaine d’Esdrelon, midi du mardi 15 octobre 1157

Le groupe de voyageurs avait fait halte auprès d’une citerne abritée dans un bosquet. Alentour, les broussailles et les arbustes finissaient de ronger les ruines de bâtisses éventrées. Deux cavaliers en cotte de mailles, dont l’un portait le manteau noir frappé de la croix rouge, discutaient avec un frère de Saint-Jean vêtu de bure. Ils étaient tranquillement accoudés à un muret tandis que les marcheurs, une trentaine de personnes arborant la croix sur l’épaule, la besace en bandoulière et le bourdon à la main, se désaltéraient à l’eau fraîche.

Certains s’étaient assis pour avaler un peu de pain. Le ciel était d’un blanc gris laiteux, sans soleil et un vent venu de la côte caressait les frondaisons. L’été arrivait à sa fin et, dans un champ voisin, quelques paysans attardés finissaient de récolter le coton. L’un d’eux leva son chapeau de paille en guise de salut.

Un peu à l’écart du groupe, abrité des bourrasques par un buisson, Droin mâchait son pain sans plaisir, la bouche pâteuse. Il jouait à soulever la poussière de la pointe de sa chaussure. Il savait qu’ils ne s’arrêteraient pas longtemps, souffla de dépit et s’apprêtait à s’allonger tout de même lorsqu’il aperçut une silhouette qui entrait dans le hameau ruiné d’un pas lourd, fatigué. Il plissa les yeux, détaillant l’arrivant. La tenue ne lui était guère familière, le teint olivâtre et l’aspect général de l’individu lui firent froncer les sourcils.

Il siffla et, de la main, fit signe à l’homme de s’éloigner, sans résultat. Il se releva et entreprit de le chasser avec de grands gestes, toujours sans que cela ralentît le miséreux vêtu de haillons. Droin s’approcha et lui jeta une poignée de gravier et de poussière en vociférant. Les visages s’étaient tournés vers lui maintenant et chacun s’interrogeait. Le cavalier au manteau noir s’avança à son tour, interpellant Droin. Le vagabond se figea en voyant le soldat sans pour autant reculer.

« Que se passe-t-il ?

— C’est ce païen ! Il n’a rien à faire là ! » s’emporta Droin.

L’hospitalier prit une mine surprise.

« Comment savez-vous qu’il n’est pas bon chrétien ?, s’amusa le chevalier.

La question parut si saugrenue à Droin qu’il faillit s’en étouffer. Il parvint à articuler quelques mots sans cohérence, crachant sa haine des infidèles qui souillaient le royaume de Dieu. L’homme, clairement un natif de Syrie, gardait les yeux fixés au sol, passif, indifférent à ce qui se jouait autour de lui. L’hospitalier le tira un peu à l’écart et lui demanda son nom, et où il se rendait, sans obtenir de réponse. Il fit alors signe à son compagnon sans arme, qui le rejoignit rapidement. Ils lui répétèrent les mêmes questions dans la langue des Syriens, échangeant péniblement quelques mots avec le vagabond. Prenant un quignon de pain dans son sac, le frère le tendit au malheureux, et l’invita à se désaltérer à la citerne, sous le regard étonné des pèlerins. Le miséreux hésita un long moment, s’exécuta sans hâte, de façon mécanique, en prenant garde à ne dévisager personne.

Alors qu’il s’éloignait de nouveau, le moine l’apostropha dans sa langue une dernière fois, n’obtenant qu’un souffle en guise de réponse. Hagard, l’homme reprit ensuite le chemin vers le sud.

Droin, qui s’était un peu calmé, s’approcha du frère hospitalier :

« C’était pas un infidèle ?

— Je ne sais.

— Il aurait pas fallu lui donner du pain, si c’en était un. »

Le clerc se tourna vers le pèlerin, désapprobateur.

« La charité est devoir chrétien, je n’ai pas souvenance qu’il faille choisir à qui la proposer.

— S’il veut du pain, il n’a qu’à semer et suer comme nous tous.

— Il ne voulait pas notre aide, j’ai dû insister. Il a refusé de nous suivre jusqu’à l’hôpital, où on aurait pu l’accueillir.

— C’est ce qu’il a dit à la fin ?

— Il a dit qu’il était berger, pas mendiant, et qu’un jour on lui rendrait ses brebis volées.

— Des brigands lui ont dérobé ses bêtes? » s’inquiéta Droin, instinctivement plus proche de celui qui était, comme lui, un travailleur de la terre.

L’hospitalier haussa les épaules, dépité. Il allait pour parler, se reprit et lança finalement à la cantonade d’une voix lasse :

« Il est temps de se remettre en route, mes frères! »❧

## Notes

Le pillage par le roi Baudoin des plateaux du Golan en début d’année 1157 fournit un bon prétexte pour aborder de façon transversale un aspect essentiel de l’économie médiévale : le tissu. Il ne s’agit ici que d’effleurer une première fois le parcours de la matière première, à travers le destin de quelques-uns amenés à croiser ce matériau, source de richesse et parfois objet de luxe, d’un prix élevé quelle que soit sa qualité. Les sociétés modernes ont tellement optimisé la production industrielle des tissus que leur valeur s’est effondrée. Pourtant pendant des siècles, encore plus qu’aujourd’hui, le vêtement et les étoffes ont constitué un élément marqueur de la classe sociale et représentaient un budget très important pour les familles. La chaîne de production des lainages était éminemment complexe et organisée, réglementée, en Europe comme au Moyen-Orient.

## Références

Cardon Dominique, *La draperie au Moyen Âge. Essor d’une grande industrie européenne*, Paris : CNRS Éditions, 1999.

Lombard Maurice, *Les textiles dans le monde musulman du VIIe au XIIe siècle*, Paris: Éditions de l’EHESS, 2002, réédition.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.
