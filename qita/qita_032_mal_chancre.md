---
date: 2014-07-15
abstract: 1154-1188. Certaines infections de l’âme, de la vie, proviennent de symptômes physiques, comme le pensent les Latins installés en Terre sainte. La jeune Anceline découvre qu’il n’est même pas besoin qu’elles touchent son propre corps ou son âme, pour qu’elles empoisonnent l’existence. Quelques années avant qu’elle ne rencontre Mile.
characters: Anceline, Fouques, Frère Aymon
---

# Mal chancre

## Césarée, midi du mercredi 3 août 1149

Le ressac couvrait la rumeur déversée depuis les rues agitées. La plage, encombrée de barques et de filets, s’étendait jusqu’aux pieds de la muraille couleur de miel. S’avançant dans la mer comme une épée dans les chairs, le château des seigneurs comtes de Césarée s’affranchissait de l’enceinte de la cité et défiait les vagues qui venaient lécher ses fondations. Par-delà les dunes de sable couvertes d’arroche, la palmeraie se déployait à perte de vue, abritant jardins et vergers, cabanons et potagers. Installés dans un creux du relief, deux jeunes gens flemmardaient, alanguis, écrasés par la chaleur d’un soleil radieux, dont l’ardeur était à peine entamée par la brise marine.

« Le jeune sire Eustache a fait serment de me prendre en sa mesnie comme valet d’armes, Anceline, tu imagines ? »

La jeune fille, petite silhouette élancée aux longs cheveux bruns emmêlés, plissa ses yeux clairs, de plaisir.

« N’a-t-il pas déjà bel hostel ? Es-tu bien certain qu’il ne se dédira pas, Fouques ?

— Certes non, le vieux sire comte Gauthier l’a bien confirmé à mon père. Il pense équiper son fils droitement, qu’il lui fasse honneur en combat. Pour cela, il lui faut fidèles soudards. »

Le garçon étira les bras, soupirant d’aise tout en imaginant l’avenir radieux qui s’offrait à lui.

« Nous aurons notre propre demeure, avec une citerne et un jardin !

— Ne devras-tu pas suivre ton maître partout ? Dormir en son hostel ?

— Pas chaque jour, et j’aurais bien assez de besants pour avoir mon feu. Nos enfants y naîtront, mes fils, qui me succéderont. Peut-être que l’un d’eux finira clerc ou chevalier…

— Un clerc ? Que vas-tu inventer là ? »

Le jeune homme éclata de rire.

« Bein quoi, Anceline ? Voilà vie fort tranquille ! »

Il fredonna, le sourire aux lèvres:

>« D’un tel fils oiseux et rétif  
>Du moindre labeur si craintif  
>Las, le père ne sut que faire  
>Fils à la tonsure te confère  
>En village tu seras curé  
>Le fils ne put rien opposer. »

La jeune fille termina avec lui, de sa voix vibrante et chaleureuse, le visage rayonnant. Au fonds de son âme, elle demeurait pourtant inquiète, sachant que la famille de son bien-aimé ne l’appréciait guère. Elle était enfant de la rue, perdue après la mort de ses parents, pèlerins sans fortune échoués sur les rivages du royaume. Mendiant, chantant sur les places, elle vendait ses bras comme lavandière. Certains disaient qu’elle louait plus, à l’occasion, quand le froid se faisait trop pressant ou que son ventre sonnait creux. Elle n’avait pas vingt ans, mais son regard était déjà fatigué, éteint.

Jusqu’à ce que ses yeux croisent le beau Fouques, jeune homme plein d’ambition et de courage qui était tombé éperdument amoureux d’elle. Ils avaient passé le printemps et l’été à batifoler dans les dunes, s’enlaçant à la moindre occasion, se volant des baisers à l’ombre de la cathédrale au jour des grandes cérémonies.

« Ton père ne dira rien de nous voir ?

— Il ferait beau le voir me critiquer sous mon toit. Si je gagne ma soudée, je n’aurai aucun compte à lui rendre. »

La jeune femme acquiesça, inquiète.

« Et une fois que tu lui auras donné des petits-fils, il comprendra, ajouta Fouques, le sourire aux lèvres.

— Comment nous marier, alors que je n’ai nulle dot, rien à t’apporter…

— Nous ne serons pas les premiers à qui cela arrive. En travaillant dur, on peut fort bien réussir en ces lieux. »

Il releva le buste, dévisageant la jeune fille, la couvrant d’un regard tendre.

« Ne t’enfrissonne pas ainsi, tout ira bien. Ce n’est affaire que de quelques semaines, de mois tout au plus. À la Noël, nous serons chez nous. »

Il caressait doucement le bel ovale du visage, traçant des chemins de tendresse du bout des doigts. Plusieurs appels de cor résonnèrent au loin, lui faisant tourner la tête vers la ville.

« Des voyageurs arrivent au castel du sire comte. Peut-être aurons-nous plaisir de voir quelques jongleurs de talent ! »

Il se leva avec vigueur, tendant la main pour aider sa compagne.

« Ne tardons pas. Si les visiteurs sont d’importance, il y aura banquet ce soir et nous pourrons profiter des reliefs à la veillée, et goûter les acrobaties. »

Main dans la main, les deux jeunes gens s’élancèrent vers la porte de Jaffa, traversant les bosquets d’aloès bordant le chemin d’accès. Le soldat de faction ne tourna même pas la tête, occupé qu’il était à fouiller dans les sacs et les besaces de deux Bédouins accompagnés d’un chameau croulant sous les paquets. Arrivés dans les rues ombragées, ils se lâchèrent la main, mais continuèrent à progresser avec enthousiasme, indifférents aux boutiques et aux étals.

Peu de badauds s’attardaient en cette période de la journée et la plupart des échoppes d’artisans ne laissaient échapper aucun son, à l’heure où la chaleur incitait à la sieste. Ils rirent en égayant quelques volailles outragées, avant de déboucher sur la placette longeant le port, devant les tours qui donnaient accès au château. Une impressionnante caravane attendait que les valets aient ouvert les lourds vantaux pour pouvoir avancer. Seuls demeuraient les domestiques et les serviteurs, les personnages d’importance ayant déjà franchi les portails. Fouques apostropha un petit rouquin armé d’un épais bâton ferré, le saluant avec enthousiasme avec de le questionner sur son maître.

« C’est le sire comte de Jaffa, fils le roi Foulques.

— Il s’en vient lever l’ost ?

— Pas que je sache, non. »

Fouques allait le presser davantage de questions, mais son interlocuteur s’éloigna pour réprimander une bête rétive, tout en admonestant un jeune valet chargé de s’en occuper.

« S’il n’y a pas la guerre, nous aurons sûrement des réjouissances, viens-t’en donc, ma douce, faufilons-nous outre la porte en la cour du sire vicomte. Profitons de cette soirée de fête. »

Puis, après avoir déposé un baiser sur la joue d’Anceline, il l’entraîna parmi les domestiques.

## Césarée, après-midi du dimanche 7 août 1149

« Nul doute sur le mal qui frappe le jeune sire Eustache, Anceline. »

La voix triste de Fouques n’était qu’un souffle, à peine discernable dans le bruissement des branches de palmiers au-dessus de leurs têtes. Il tenait Anceline dans ses bras, par l’épaule, assis sur un ressaut du terrain parmi les bosquets environnant la porte orientale. La jeune fille gardait les yeux mi-clos, fixés sur le sol, abasourdie. Elle ne répondit que tardivement.

« Il ne tiendra pas le comté de son père, alors ?

— Non, cela a été clairement expliqué en l’assemblée de la cort. Il est prévu qu’il rejoigne les frères de Lazare.

— Il se retire du monde. »

L’angoisse lui fit monter les larmes aux yeux et elle continua, d’une voix brisée :

« Cela veut dire que tu vas devoir œuvrer avec ton père et que nous ne pourrons célébrer nos espousailles ?

— Certes non. C’est juste que… »

Le jeune homme était ennuyé, il se mordit la lèvre à plusieurs reprises, cherchant à rassembler ses pensées.

« Le sire comte de Jaffa est frère le roi comme tu le sais, et il y aurait peut-être besoin de sergents en la sainte cité. Sergent le roi, moi, tu imagines ? »

Anceline écarquilla les yeux de frayeur, se tournant vers lui.

« Tu vas partir, tu me quittes ? C’est cela que tu voulais me conter ?

— Non, je veux que… J’ai désir que tu me compaignes en la sainte cité. Nous pourrons y être heureux aussi bien qu’ici.

— Partir avec toi ? Mais je n’ai rien pour le voyage et je ne connais personne là-bas, comment je ferai ?

— Tu m’auras, moi. Et mon père m’a fait promesses de quelques besants pour m’installer. »

Le jeune femme hocha la tête, mais son visage demeurait fermé. Finalement, elle se frotta les yeux, les joues, de ses paumes avant de longuement soupirer.

« Je ne sais pas, Fouques, ça me parait fort périlleux.

— Crois en moi, Anceline. Père est convaincu que je trouverai bonne place là-bas, sinon il ne dessererrait pas les cordons de sa bourse aussi aisément, tu le sais bien. »

Il esquissa un sourire amusé, auquel la jeune femme répondit, sans conviction.

« Le départ du frère le roi est prévu pour dans trois jours, tu n’auras qu’à venir à la caravane de départ, au matin, et nous cheminerons ensemble. Prends toutes tes affaires, car nous ne viendrons pas ici de si tôt, je crains. »

Se rapprochant de Fouques, elle lui prit le bras et plongea son regard dans le sien.

« Je n’ai jamais quitté Césaire depuis mes enfances, Fouques. Je n’ai rien ici, pas de feu ni de famille, mais j’y ai des amis, mes habitudes. Je n’ai ja osé cheminer ailleurs. C’est bien trop dangereux pour femme seule.

— Tu n’as donc pas désir de me suivre ? Nous aurons la vie dont nous rêvions, mais en une autre cité. Et sergent le roi, c’est excellent travail. D’être mon épousée fera de toi une femme honorable, plus personne n’osera se moquer. J’aurais l’épée à la hanche et chacun nous respectera. »

Anceline sourit timidement, attendrie par la douceur de son compagnon. Elle vint cueillir ses lèvres d’un délicat baiser.

« Je serai ta femme, Fouques, je n’ai nul désir d’autre chose.

— Tu m’attendras à la porte de la cité ce mercredi ?

— Je serai là, avec ma besace et mes maigres avoirs. Toute à toi, mon bel ami. »

Vibrant de tout son être, elle scella sa promesse d’une étreinte passionnée.

## Césarée, matin du lundi 8 août 1149

Fouques sursauta quand il sentit une main lui secouer l’épaule. Il voyait l’ombre de son père, penché sur sa paillasse, venu le réveiller. Ses frères et sœurs, amassés comme lui dans un coin de la réserve, dormaient encore à point fermé. Le jour pénétrait pourtant par la porte ouverte de la grande pièce à vivre.

« Il te faut te lever, mon garçon, tu as force ouvrage à faire ! »

Fouques s’étira et repoussa sa couverture. Il attrapa rapidement sa cotte et ses chausses, puis glissa ses pieds dans ses savates. La poussière volait dans les rais de lumière du matin jeune, par les ouvertures donnant sur la cour. Il cligna des yeux en y débouchant, se protégeant de la main. Arrivé devant la citerne, il y lança le seau qu’il remonta en bâillant, avant de s’asperger le visage et les bras. La fraîcheur de l’eau le réveilla tout à fait. Ce fut en finissant d’enfiler sa cotte qu’il rejoignit son père et sa mère à table. Du pain et du lait, ainsi que des fruits l’attendaient. Il sourit, ragaillardi à la perspective du repas.

« Vous avez nécessité de mon aide ce jour d’hui, père ? Quelque belle vente en perspective ? »

Il se figea en voyant ses parents assis côte à côte, l’air sérieux et renfrogné.

« Tu n’as pas à t’en inquiéter, fils. Ta mère a préparé tes habits et quelques douceurs comme en concèdent les femmes aimantes. »

Il marqua une pause, indiquant du doigt un sac de toile grise posé sur un des bancs. Puis il sortit de ses replis de vêtement une petite bourse.

« Je ne suis qu’un modeste blatier[^blatier], et je n’ai donc que peu à t’offrir. Alors voilà tout ce que nous pourrons jamais te donner pour t’installer. »

Il posa la modeste escarcelle, peu ventrue, sur la table.

« Je sais que tu es vaillant à la tâche, et j’ai donc bon espoir que tu sauras t’en débrouiller. »

Fouques n’était pas certain de comprendre et son regard allait d’un de ses parents à l’autre, hésitant entre espoir et angoisse.

« Vous avez donc eu réponse du sire comte pour moi, père ?

— Si fait, je ne te l’ai pas dit hier, mais tu pars avec le frère le roi ce jour d’hui. Tu prêteras serment au roi en arrivant à la cité et le servira comme sergent. Te savoir fils d’un ancien mesureur de blé[^mesureurble] a fait bonne impression. »

Le jeune homme sentit son cœur s’arrêter, toute vie abandonna son visage.

« Ce jour ? Mais je croyais que c’était…

— Il semble que le sire comte de Jaffa fasse selon ses envies, et on m’a dit de te mener à la première heure au castel. Il fait route tantôt. »

Le vieil homme se leva pesamment et se dirigea vers la porte, s’emparant du bâton qu’il ne quittait jamais. Assujettissant un bonnet de toile sur son crâne dégarni, il lança, d’un air sec :

« Salue ta mère promptement et rejoins-moi dans la cour, il ne s’agit pas d’être en retard. »

Fouques se frotta le visage puis se lança auprès de sa mère, s’agenouillant.

« Mère, saviez-vous que je devais partir ce jour ? »

Le visage las échappa un soupir consterné.

« Tu sais combien ton père n’aime pas la désobéissance, Fouques.

— Mère, je vous en conjure ! Vous savez que je ne peux partir ainsi, dès potron-minet ! Je dois prévenir… »

Sa voix mourut tandis que sa mère l’attirait à lui. Elle l’enlaça avec douceur un court instant et lui chuchota :

« Tu es désormais un homme, fils, ne laisse plus quiconque voir tes larmes. Que Dieu te garde, mon enfant, je prierai pour toi. »

L’éloignant d’elle lentement mais avec fermeté, elle traça un signe de croix sur le front de son fils, essuya ses larmes d’une main décidée et concéda un baiser.

« Brûle une chandelle de bonne cire pour tes vieux parents auprès du tombeau du Christ quand tu en auras le loisir. »

La silhouette du père se traçait dans l’embrasure de la porte, profil courbé mais déterminé, impassible.

« Femme, laisse notre aîné partir comme un homme, foin des jérémiades ! »

## Césarée, midi du lundi 8 août 1149

Anceline, les bras encombrés d’une énorme corbeille de linge, avançait en trébuchant dans la ruelle depuis le lavoir jusqu’aux bâtiments des frères de Saint-Jean. Ils comptaient parmi les clients de lavandières les plus fréquents, ayant besoin de changer souvent les draps de leurs hôtes, sans négliger toute la vêture qui passait entre leurs mains. Elle déposa avec plaisir le lourd paquet devant la porterie. Les femmes n’étaient pas les bienvenues dans l’enclos et des valets venaient donc généralement prendre les affaires sous un auvent protégeant des intempéries. Le frère cellérier ou l’hospitalier se chargeait habituellement de noter, comptabiliser, en s’installant parfois sur une petite table lorsque l’ouvrage était conséquent.

Anceline escomptait voir le cellérier, car elle voulait demander son compte. Elle était réglée chaque semaine, selon son travail. Mais elle préférait récupérer au plus tôt l’argent qu’on lui devait ici et là, de façon à pouvoir partir avec toute sa fortune. Le vieux moine espéré sortit, dandinant sa silhouette rebondie.

Anceline aimait bien ce frère à l’esprit fin, amateur de bons mots. Même s’il laissait parfois courir ses mains en des endroits qui lui étaient défendus. On lui prêtait une liste de maîtresses longue comme le bras mais jamais il n’avait fait d’avances à Anceline, bien qu’il ait été manifeste qu’il la trouvait à son goût. Le sourire ravi qu’il arborait là encore en était une nouvelle preuve évidente.

« Alors, belle Anceline, tu as désir de toucher ton compte ? Quelques soucis impérieux ? Nous pouvons t’aider si tu en as le désir.

— Mille grâces, frère Aymon, c’est juste que je vais quitter Césaire. »

Le visage avenant se fit soudain contrarié.

« Que voilà cruelle nouvelle ! Tu ne te plais plus en notre cité ? »

Le visage rayonnant, Anceline lança, pleine d’entrain :

« Je vais me marier, mon frère, et mon futur époux part tantôt pour Jérusalem, alors je vais le suivre.

— Jambe-Dieu ! Tant de cœurs vont se briser ici à cette nouvelle ! »

Il sourit, s’avançant pour demander d’une voie pateline :

« Qui est l’heureux élu ?

— Fouques, un fils de blatier qui part pour rejoindre la sergenterie du roi avec le sire comte de Jaffa. Nous faisons route ce mercredi.

— Mercredi ? Tu dois faire erreur, ma belle. Le sire comte a corné son départ dès la première heure. Il doit déjà apercevoir la tour de Caco en ce moment. »

Anceline sentit ses jambes flageoler, son regard se brouilla. Frère Aymon s’en émut et tendit une main compatissante, inquiet.

« Que t’arrive-t-il ? »

Trop bouleversée pour répondre, Anceline abandonna le frère et détala à toutes jambes. Elle passait comme une furie dans les rues, les robes relevées pour courir plus à son aise, indifférente aux regards curieux, courroucés ou désapprobateurs. Elle filait pour sa vie, n’avait qu’une idée en tête. Jamais elle n’avait osé s’approcher trop de la boutique du père de Fouques, car elle le savait hostile à son égard. Mais cette fois, elle ne reculerait pas.

Elle s’arrêta brusquement devant la double porte par laquelle s’exhalaient les senteurs poussiéreuses du grain. Le vieil homme était là, assis sur un escabel, gardant l’entrée comme un cerbère tapi dans l’ombre. Il la toisa lentement puis agita la main comme pour l’effrayer :

« Va t-en, maudite ! Mon fils n’est plus là ! Tu ne l’auras jamais ! »

Elle hésita un instant, fit mine d’avancer, pour le défier. Elle demeurait indécise. Elle aperçut le bâton qui ne le quittait jamais, et fantasma sur ce qu’elle pouvait en faire. Elle se délectait de voir le détestable crâne ridé se fendre comme un fruit pourri, d’imaginer la silhouette fracassée sur le sol. Mais elle se contenta de renifler et tourna talon. Quand elle disparut à l’angle de la rue, le vieux sortit la tête de l’ombre, comme une gargouille curieuse, puis cracha avec morgue. ❧

## Notes

La lèpre posait un double problème aux sociétés médiévales. Tout d’abord, bien évidemment, il y avait l’aspect sanitaire. Certaines formes étaient contagieuses et il fallait résoudre la question du soin à prodiguer aux malades. Mais il s’y ajoutait un autre souci : on estimait que le corps était le reflet de l’âme, et que donc le dépérissement de l’un était signe d’une affection de l’autre. C’est une des raisons qui poussait à l’ostracisme des lépreux, quand bien même ils appartenaient à la noblesse. L’ordre de saint Lazare fut créé pour leur permettre de conserver une utilité sociale tout en préservant les gens sains. Le cas de Baudoin IV, à la fin du siècle, questionnera grandement cette vision des malades souffrant d’affections graves - car tous les « lépreux » n’étaient pas véritablement atteints de cette maladie, mais pouvaient présenter des symptômes similaires.

Le mariage, au milieu du XIIe siècle, demeure avant tout un contrat civil en vue de former une unité économique viable. Bien que les sentiments n’en soient pas forcément exclus, il est essentiel pour les futurs époux de pouvoir s’installer dans de bonnes conditions. C’est pourquoi le rôle des familles est généralement important, car elles contribuent (via la dot, le trousseau, le douaire, etc.) à la constitution des premiers apports. Ne pas pouvoir se rattacher à un réseau familial ou l’autre est donc un réel souci, d’autant plus pour les femmes, qui sont souvent considérées comme des mineures et qui, donc, peinent à exister en dehors de tout mâle référent (père, mari, frère, cousin, oncle).

## Références

La Monte John L. , « The lords of Caesarea in the Period of the Crusades », dans *Speculum*, vol.22, n°2, avril 1947, p.145-16

Mitchell Piers D., « An evaluation of the leprosy of King Baldwin IV of Jerusalem in the context of the medieval world », dans Hamilton Bernard, *The leper king and his heirs. Baldwin IV and the crusader kingdom of Jerusalem*, Cambridge : Cambridge University Press, 2000, p. 245-258

Sheehan Michael M., *Marriage, Family, and Law in Medieval Europe: Collected Studies*, Toronto: University of Toronto Press, 1996.
