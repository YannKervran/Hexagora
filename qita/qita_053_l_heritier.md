---
date: 2016-02-27
abstract: 1147-1158. Isaac de Naalein, jeune garçon de bonne famille, découvre peu à peu les responsabilités à travers les espoirs qu’on met en lui. Soutenu par les ambitions familiales, il tente de se montrer à la hauteur des attentes. Et fera une rencontre inopinée...
characters: Ernaut, Isaac de Naalein, Bernard Vacher, Jehan Vacher, Maître Baruch
---

# L’héritier

## Naalein, maison-forte, fin d’après-midi du mardi 20 mai 1147

La petite cour de la maison forte résonnait des cris des chameliers tentant d’organiser le déchargement. Les bêtes blatéraient parfois de mécontentement, ce qui ajoutait encore à la pagaille. Malgré le vacarme, le seigneur du lieu, Jehan, regardait avec plaisir les pièces de bois que l’on portait dans ses celliers. Il avait prévu depuis un moment de surélever le bâtiment et de lui adjoindre une tour, d’où il pourrait surveiller les confins de la terre que le roi lui avait confiée. Il savait parfaitement qu’il était en relative sécurité, coincé dans une vallée sur la route entre Bethgibelin et Bethléem. Il ne se l’avouait guère, mais il avait surtout envie de marquer la présence d’un officiel, de montrer aux environs que le lieu était sous la coupe d’un homme. Pour le compte d’un autre, en attendant mieux.

Installé à l’une des fenêtres de la grande salle à l’étage, Isaac, son aîné, s’amusait avec quelques figurines de bois et de toile. Ils avaient reçu quelques jours plus tôt un jongleur en route vers la côte depuis Bethléem, et il recréait les exploits devenus mythiques de leurs devanciers, Godefroy de Bouillon en particulier, narrés à cette occasion. À ses pieds, ses deux sœurs jouaient également, accroupies sur un tapis aux couleurs vives. Assise un peu en retrait, leur mère, une jeune femme aux traits lourds, tirait l’aiguille, pour orner de motifs chamarrés le col du bliaud de leur père. Dans la pièce, deux servantes s’activaient à nettoyer et préparer la veillée, remplissaient les lampes, balayaient. Un clerc installé en bout de table compulsait des livres, bougeait des jetons sur un échiquier, marmonnait tout en calculant avec son abaque.

Isaac plongea le regard dans la cour en entendant un bruit de cavalcade. Plusieurs hommes venaient de pénétrer dans l’enceinte, tenant ferme leurs chevaux effrayés de trouver là tant d’animation. Il écarquilla les yeux de plaisir en reconnaissant son oncle et lâcha ses jouets pour s’élancer vers l’escalier.

« Oncle Bernard est là ! Mère ! »

Le voyageur n’eut qu’à peine le temps de mettre pied à terre que la petite silhouette se serrait contre lui, à sa plus grande joie. Il était gris de poussière, avait le visage las, mais il étrilla le gamin avec entrain.

« Alors, que devient mon futur page ? Il serait bien temps de t’initier à tout cela !

— Laisse donc enfançons profiter un peu avant de les lancer dans le monde, frère » lui rétorqua, souriant, Jehan.

Le cadet s’avança afin d’embrasser son aîné, imposante silhouette ventrue. Le cavalier souleva son chapeau de paille et essuya la sueur coulant sur son front dégarni. Bernard Vacher n’était pas un bel homme. Ses traits empâtés, son nez tordu, sa bouche sans lèvre, et ses yeux fatigués accusaient son âge. Il savait encore manier l’épée avec dextérité et sa monture adroitement, et arborait en tout moment ses éperons dorés de chevalier. Mais les épaules commençaient à tomber, ainsi que le ventre et le fort poitrail à la suite, aujourd’hui plus qu’habituellement. Jehan, l’invitant d’un geste à le précéder, s’en inquiéta alors qu’ils rejoignaient la fraîcheur de la grande salle.

« Mauvaises nouvelles, mon frère ?

— De certes. La Cour a décidé d’aller encontre Damas et de soutenir ce fou d’Altuntash dans sa révolte. »

Il n’en dit rien de plus, saluant d’une accolade distraite sa belle-soeur puis s’affala sur le banc. Rapidement, du vin, de la bière, et quelques fruits, accompagnés de pain, d’olives et de fromage, furent disposés devant lui. Jehan prit place sur un escabeau, tandis qu’Isaac s’installa auprès de son oncle, le servant tel un page. Bernard avala plusieurs gorgées d’un rouge capiteux avant de reprendre.

« Le jeune Baudoin est entouré de sots, et il a grand désir de marquer sa force. Il sait que la reine ne peut le suivre sur pareil terrain. Il ira donc au secours de Bosra contre Damas.

— Il est prêt à rompre la trêve ? Il va pousser Aynar[^anur] dans les bras de Noradin[^nuraldin] !

— J’ai grand regret à le dire, mais le roi est comme jeune dogue, impatient de courir et fort mal entouré. On m’a presque accusé d’avoir reçu dinars et soieries pour trahir. »

Jehan en frappa sur le plateau de colère, mais Bernard l’arrêta de la main, une moue cynique sur le visage.

« Pas de quoi s’emporter pour autant. Le connétable et la reine savent bien qu’il n’en est rien, et l’ont fait assavoir. »

Il haussa les épaules et reprit un peu de vin, avant d’ajouter.

« Au final, il faut donc que tu convoques l’ost, pour le mener à Tibériade où le roi a fait semonce. De là, nous irons en Hauran pour porter secours à Altuntash. »

Jehan se leva avec lenteur, secouant la tête comme pour s’extraire d’un mauvais rêve. Tandis qu’il sortait, pour annoncer leur départ imminent, Bernard se tourna vers Isaac, gobant quelques olives dont il recrachait les noyaux dans son poing.

« Et toi, gamin, as-tu quelques exploits à narrer à ton vieil oncle ?

— Père m’a fait faire une lame de bois et, avec, je malmène souvent le pal. Et j’ai sauté un muret qui faisait plus de deux pieds l’autre jour, en selle.

— Te voilà en bon chemin pour manier la lance comme il sied à décent chevalier ! Et où en es-tu de la lecture, de l’écriture ?

— Mère me fait réciter chaque jour, mais c’est là ouvrage de clerc. Matthieu dit qu’on finit tonsuré à trop étudier.

— Qui est donc ce Matthieu ?

— Le fils d’un des chevaliers ici. Il est grand et costaud, mais je le bats parfois. »

Le soldat se passa une main sur le visage, gratta son oreille et attrapa son neveu qu’il installa sur son genou. Il souriait devant la candeur de l’enfant. Il aurait aimé trouver lui aussi une fille de bonne famille et avoir un héritier, mais le destin en avait voulu autrement. De toute façon, il appréciait trop sa liberté pour s’attacher à une terre.

« Tu diras à ce Matthieu qu’il n’a que du vent entre les oreilles, et que c’est un chevalier du roi qui le lui dit.

— Pourtant, il est plus important d’apprendre à batailler ses ennemis. La prière et l’étude, ce n’est pas pour nous.

— Tu parles de vrai. Mais le fer de l’homme doit servir avec discernement. Nombreux sont ceux qui pensent qu’il n’y a que ténèbres et lumière. Et les clercs ne sont pas les derniers à ce jeu-là. Voilà la plus grande des erreurs, mon garçon. Ici-bas rien que des ombres, changeantes, mouvantes. Apprends à raisonner avant toute chose, ne laisse pas tes impressions l’emporter. Ton discernement sera toujours la lame la plus affilée à ton service. »

Lorsqu’il reposa Isaac au sol, Jehan entrait.

« J’ai annoncé la convocation du roi. Nous partons demain matin, à l’aurore.

— Au moins pourrons-nous faire bonne veillée ce tantôt, avant de manger la poussière derechef » opina Bernard.

## Naalein, maison forte, fin de matinée du dimanche 28 août 1149

Au retour de la messe, alors que la famille Vacher allait prendre place à table, un messager était apparu à l’intention de Bernard. Un de ses contacts avait envoyé un pli auprès de la Cour, et il avait fallu encore qu’un sergent soit désigné pour prévenir le chevalier, plus loin au sud chez son frère. Entre-temps, le contenu en avait été lu, bien évidemment, car le connétable ne faisait guère preuve de réserve avec les hommes de soudée. Même s’il leur reconnaissait parfois du talent, ce n’étaient que des domestiques à son sens. Il avait donc joint un second feuillet, indiquant que Bernard devait se présenter au plus tôt à la Cour pour exposer la situation en détail.

Le chevalier parcourut rapidement les notes puis les tendit à Jehan, le maître des lieux. Autour d’eux, les valets commençaient de servir le plat de viande aux fèves. Seul Isaac, assis à la droite de son oncle, suivait avec attention les échanges entre les deux frères. Chacun ici était accoutumé à ce que les hommes partent en guerre, reviennent parfois blessés, répondent aux convocations. Nul ne s’inquiétait outre mesure d’un message ou des discussions à table.

« Ainsi donc le vieil Aynar est mort, sembla regretter Jehan.

— Je ne saurais dire si je m’enjoie de la nouvelle ou pas. D’aucuns verront là bon moment pour prendre la cité...

— Attendons de voir qui le remplacera. En fait, si ce n’est en titre. Ton billet n’en parle pas ?

— Ce serait Mugîn ad-Dîn le nouvel homme fort. Nous pourrions avoir pire. Il n’aura nul désir de se jeter dans les bras des Turcs. »

Depuis des années, Damas jouait ses adversaires les uns contre les autres et n’avait jamais écarté l’aide franque quand il s’agissait de s’affranchir de la puissance aleppine du nord, celle de Zengui puis Nūr ad-Dīn. L’inverse était également vrai, comme le roi Baudoin l’apprit sévèrement lors de la prise manquée de Bosrah, deux ans plus tôt.

« Je ne sais quoi dire aux Barons lors du conseil. Le comte Thierry aura sûrement l’appétit aiguisé.

— La cité a résisté aux plus puissants rois, l’ont-ils déjà oublié ?

— Toi et moi savons bien que le fruit n’est pas si mûr qu’il tombera en nos mains aisément. Mais ce n’est pas là héroïque geste qu’il plaît à un jeune roi d’ouïr. »

Il avala quelques cuillers en silence, lichant avec un plaisir évident la sauce puissamment miellée. S’il devait confesser un péché, ce serait volontiers celui de gourmandise, et ses contacts de part et d’autre de la frontière savaient que le plus court chemin vers son cerveau passait par son estomac.

« Je demanderai audience à la reine pour m’assurer qu’elle a bonne connoissance des faits.

— Peut-être devrais-tu en toucher aussi quelques mots au sénéchal. Il sait fléchir les barons de la Cour.

— Je ne sais jamais sur quel pied danser avec celui-là. Il est plus glissant que serpent.

— Il ne veut pas se trouver entre marteau et enclume et veille donc à demeurer équitable dans ses relations avec chacun. Mais il m’a fait bonne impression, je le pense prud’homme. »

Bernard hocha la tête sans conviction puis se resservit une large portion. Les sourcils froncés, il avalait désormais sans joie ses cuillerées, anticipant les jours prochains. Il lui faudrait également se rendre à Damas, il en était certain. Ne serait-ce que pour se rendre compte par lui-même des intentions du nouvel homme fort, Mugîn ad-Dîn.

Il l’avait rencontré de nombreuses fois et ne lui concédait pas le même crédit qu’à son beau-père. Il lui reconnaissait certes de l’autorité et de la volonté, mais diriger la cité de Damas demandait bien plus que cela. Il regrettait presque l’absence du vieil Usamah ibn Munqidh, réfugié en Égypte. Le diplomate était un fieffé chenapan, il n’en disconvenait pas, mais il lui accordait de grandes qualités humaines, malgré son opportunisme politique et un vrai talent pour l’intrigue. Le nouveau maître de Damas en aurait bien besoin, pour résister aux pressions des Aleppins.

Malgré tout son dévouement à la couronne de Jérusalem, Bernard rechignait à imaginer la cité dans le giron latin. La voir entre les mains d’un Thierry de Flandre ou, pire encore, d’un aventurier fraîchement débarqué et qui aurait eu l’heur de plaire au jeune roi, lui arrachait le coeur. Peut-être que ses adversaires avaient raison, il finissait par se laisser corrompre de ses trop fréquents voyages chez les musulmans.

La voix immature de son neveu le tira de ses sombres tribulations :

« Mon oncle, je pourrais venir avec vous en la Cité ? Comme l’autre fois ? »

Bernard sourit et se tourna vers son frère. Il avait beaucoup d’affection pour Isaac, qu’il traitait comme son fils, mais il prenait garde à ne pas porter ombrage à son père. L’enfant hériterait peut-être un jour de la terre de Naalein, tous deux l’espéraient, mais il devait savoir à qui il le devrait. Bernard servait d’autres seigneurs depuis si longtemps qu’il avait grande habitude de s’effacer. Lorsque Jehan hocha la tête, le gamin se mit à se trémousser sur le banc, fébrile.

« Tu y seras mon page, donc apprends bien à ouvrir tes yeux et tes oreilles et, surtout, garde ta bouche fermée et tes mains calmes. La Cour le roi, ce n’est pas manse de vilain.

— Je ne vous ferai pas hontage, mon oncle. Je vous le promets.

— Je le sais bien, et je compte sur toi pour qu’il en soit toujours ainsi. »

## Jérusalem, rue de Josaphat, fin d’après-midi du mercredi 22 octobre 1152

Isaac avançait d’un bon pas, négligeant la sueur qui commençait à lui parcourir la colonne vertébrale. Il ressassait la leçon du jour, l’esprit enflammé par les aventures du jeune marin qu’il venait de découvrir pendant son cours. Bien qu’héritier d’un opulent marchand, il avait dilapidé sa fortune en peu de temps et espérait se refaire par le commerce maritime. Mais le destin avait voulu qu’il prenne une énorme baleine pour une île et il fut abandonné là, peu avant que l’animal ne plonge. Son professeur, le vieux Baruch, l’avait rassuré : le héros allait connaître mille péripéties, mais finirait fort riche.

Il bifurqua sur sa gauche, empruntant la grande voie de Saint-Jean l’Évangéliste puis la rue du Temple. Il préférait demeurer au loin du Saint-Sépulcre, toujours encombré de dévots. Il envisageait juste de passer par Malquisinat, salivant à l’avance de ce qu’il pensait s’offrir comme récompense après une après-midi studieuse. Il avait eu les plus grandes peines à consacrer une partie de ses journées à étudier encore et encore, et n’avait accepté que de mauvaise grâce au début. Mais au fil des mois et des années, il appréciait de plus en plus les capacités que ce savoir mettait à sa portée. Il lui arrivait de surprendre des échanges dans la rue entre des commerçants, de reconnaître des mots lancés entre deux fellahs tandis qu’ils traversaient un casal. Et si ses amis se moquaient de lui, il leur démontrait sur le champ qu’il n’en négligeait pas les armes ni la lutte pour autant.

En pleine croissance, il s’était un peu affiné et avait fait oublier sa réputation de petit gros, qu’il avait traîné des années durant. Il n’était néanmoins pas très grand, ce qui était un avantage pour la lutte, semblait-il. Mais il n’était jamais à l’aise quand il devait monter un haut animal, avec tout le fourniment. Leur jeu, de sauter en selle depuis l’arrière équipés de pied en cap, de la maille au casque, ne lui plaisait que moyennement. Son aine gardait la trace douloureuse d’un troussequin vindicatif lors d’une tentative avortée.

Dans la rue du Temple, le cortège habituel de pèlerins s’étirait d’un lieu saint à l’autre, bigarré de soldats en armes, de marchands en goguette et de boutiquiers affairés. Parfois un train de mules, de poneys ou d’ânes bloquait le passage et il fallait jouer des coudes, en veillant à ne pas y laisser sa bourse. Certaines boutiques attiraient les curieux, comme les huchiers qui faisaient de magnifiques coffrets ouvragés, dans les senteurs de pin et de cèdre. À un endroit, plusieurs artisans martelaient le cuivre et le laiton, pour décorer plateaux, aiguières et coupes. Mais la plupart des évents se contentaient de revendre des produits fabriqués ailleurs. Parfois les sacs et les paniers s’amoncelaient tellement en devanture que le marchand devait se balancer à une corde pour entrer et sortir de son échoppe.

Il laissa passer un groupe de femmes, des ballots de linge propre sur la hanche, sans se priver d’en admirer quelques-unes au passage. Il récolta quelques œillades amusées, peut-être intéressées. Il savait qu’il n’était pas séduisant, mais sa mise trahissait le jeune homme de bonne naissance et c’était généralement suffisant pour attirer les regards des filles, du moins parmi les plus humbles.

Il stoppa chez un marchand de boulettes de viande, qu’il dévora dans une tranche de pain tout en reprenant son chemin. Il avait fait porter cet achat sur son compte, comme habituellement, et avait noté qu’il lui faudrait très prochainement payer son dû. La Toussaint marquait souvent le terme de nombreuses taxes, loyers et impôts et les commerçants avaient besoin de toutes les pièces qu’ils pouvaient trouver. Verser ce qui était attendu avant qu’on ne nous le rappelle était toujours apprécié, et Isaac se targuait de n’avoir jamais abusé du système.

Lorsqu’il pénétra dans la fraîcheur de la cour intérieure, il aperçut la silhouette massive du maître de maison et se dirigea vers lui, tout sourire.

« Mon oncle ! Vous voilà enfin de retour en l’hostel ! Quelle bonne surprise !

— Comment te portes-tu, mon grand ? J’ai l’impression que tu prends un pouce à chaque fois que je pars quelques jours.

— Vous êtes absent depuis avant la saint Michel, mon oncle ! »

Le chevalier grimaça et haussa les épaules, visiblement fatigué malgré sa mine réjouie.

« Le service du roi, garçon. Nul ne peut s’y soustraire ! »

Tout en avançant vers la grande salle, il étudia son neveu des pieds à la tête, satisfait de ce qu’il voyait. Il avait convaincu Jehan de le lui confier quelques mois pour qu’il se familiarise avec la capitale. Bernard n’était que chevalier de soudée, et Jehan châtelain, pour le compte du roi. Mais tous les deux espéraient qu’Isaac finirait seigneur du lieu, en plein titre. Avoir des amis au sein de la Haute Cour serait un atout, et il pourrait nouer des relations avantageuses pour faire carrière. Après tout, Bernard était un familier de la domesticité de Baudoin. il était souvent désigné pour tenir la bannière royale et son neveu pouvait en profiter. Un office, même temporaire, serait du meilleur effet. Un bon mariage plus encore, pour s’assurer de soutiens puissants.

Bernard s’affala sur une chaise curule dans la petite chambre qui lui servait de bureau, indépendante de la salle de réception. Il s’était habitué à cette façon de faire orientale, utilisant de nombreuses pièces de sa demeure à des fonctions précises. La seule zone dont il n’avait jamais eu besoin était le gynécée. Il avait bien des aventures de temps à autre, mais il n’avait aucunement la fibre romantique et peu le désir de s’attacher une famille. Il était entièrement dévoué à son frère et aux siens. Avant même qu’il n’ait eu le temps de demander, son valet lui apporta de quoi se rafraîchir, une bière légère, de l’ail confit et des pistaches.

Isaac s’installa sur un escabeau face à lui, impatient de lui apprendre ses derniers progrès. Bernard sortit d’une besace plusieurs documents et portfolios, les tria en tas sur son bureau tout en grignotant puis lâcha enfin la question tant attendue.

« Alors, quelles nouvelles en l’hostel en mon absence ?

— Tout va pour le mieux, mon oncle. Guéraud a porté quelques vivres de Naalein et nous avons aussi pu acheter du bois pour l’hiver.

— De la cire avec les vivres ? J’aimerais faire quelques dons aux autels d’ici.

— Oui, les mouches à miel ont encore bien donné et vous avez plusieurs bottes de chandelles de bonne cire. »

Bernard avala une longue rasade, essuyant la mousse d’un revers de main.

« Et toi, ton apprentissage ?

— J’ai réussi plusieurs passages à la lance à travers les annels la semaine dernière !

— Tu vas bientôt me battre, voilà des années que je n’ai poigné le fer, ironisa le chevalier.

— J’ai également pu chasser avec le comte de Jaffa. Nous avons traqué un lion par-delà le Jourdain. »

Bernard sourit. Il n’avait pas eu l’occasion de participer à ce genre de réjouissances depuis si longtemps ! Dès avant même la mort du vieux Foulques, son expertise des territoires musulmans était sans cesse mise à contribution, et il se faisait parfois l’impression de n’être qu’un clerc, un de ces évêques ambassadeurs toujours par monts et par vaux. Isaac commença soudain à parler arabe, avec un accent et des hésitations, mais ses progrès étaient évidents :

« J’ai aussi beaucoup pratiqué avec maître Baruch. Il dit que j’apprends bien.

— Il est donc meilleur professeur que je ne le pensais. J’ai toujours eu méfiance des Juifs.

— Maître Baruch est homme de savoir, et assez aimable avec moi.

— J’aurais préféré un mahométan, tant qu’à faire.

— Ne m’avez-vous pas enseigné de juger selon ma raison et non mes sentiments, mon oncle ?

— Touché ! », opina Bernard en français avant de reprendre un peu de bière, goguenard.

## Jérusalem, palais royal, soirée du mardi 18 novembre 1158

Ernaut traînait dans les cuisines, comme souvent, dans l’espoir de grappiller quelques gourmandises avant le repas. Il n’était pas de service ce soir, mais avait passé la journée avec d’autres sergents, pour assister le sénéchal dans des tâches comptables. En l’occurrence, c’était plutôt de la manutention car ils avaient apporté plusieurs caisses de monnaie depuis l’atelier royal de frappe monétaire. Avec sa stature hors norme, Ernaut était fréquemment désigné pour ces travaux fatigants. Il ne s’en plaignait pas, profitant de l’occasion pour se pousser auprès des responsables. En un peu plus d’un an, il n’avait guère progressé dans la hiérarchie, chacun veillant à protéger ses intérêts propres, mais il était néanmoins assez connu. Le roi lui-même avait remarqué sa présence, disait-on.

« Si tu demeures dans les environs alors que le repas va être servi, je vais t’arracher les boyels pour en faire des saucisses » lui lança Clarembaud, un des maîtres queux.

Il ne força pas sa chance et s’esquiva, un quignon de pain à la main. En tant que sergent, il avait droit d’être nourri, en bas bout, parmi les domestiques, mais il lui fallait attendre encore qu’on ait fait corner le repas. Il décida de s’accorder un peu de repos dans une des cours voisines où des bancs appuyés contre les murs servaient parfois aux commis de cuisine pour plumer et vider les animaux. À cette heure, il y aurait de la place.

L’air de fin de journée était chargé des senteurs de l’activité diurne, mêlées des parfums des plantes aromatiques cultivées là. Et si la clameur des rues proches, incessante étant donné la proximité des lieux de pèlerinage, ne s’entendait guère, c’était parce qu’elle était remplacée par l’effervescence du palais. D’autant qu’il communiquait avec celui du Patriarche et que le tout ne formait qu’un vaste îlot au sein de la Cité, accolé au monastère et à l’église du Saint-Sépulcre.

La meilleure place était déjà occupée par un jeune homme visiblement épuisé. Sa tenue de voyage était pleine de poussière et il s’appuyait contre le mur, paupières fermées, jambes tendues et bras croisés. Ernaut allait s’éloigner pour ne pas le réveiller lorsque celui-ci ouvrit les yeux et le salua de la tête après avoir soulevé un sourcil, surpris de la masse devant lui.

« J’ai cru un instant qu’on m’envoyait quérir...

— Garde repos. Je fais pareil que toi, compère, je cherche un endroit où souffler un peu. Tu es de quel hostel ? »

Ernaut s’affala à ses côtés tandis que l’autre lui répondait.

« Vacher.

— Ah oui, je vois. Bernard, c’est ça ? C’est un familier de la Cour, souventes fois porte-bannière. Je le vois souvent aller et venir. On le dit grand voyageur et fort habile avec les Mahométans. Tu dois en voir du pays... »

La remarque était faite avec une envie non dissimulée, ce qui fit sourire Isaac.

« Pas tant que j’en aurais espoir. Je suis plus souvent en son hostel en la Cité que chevauchant au loin.

— Souvent Fortune tourne sa roue. Notre temps viendra, compère... »

Ils furent interrompus par l’irruption d’un soldat, épée au côté.

« Maître Isaac, votre oncle vous mande auprès de lui. Il vous veut à ses côtés pour le repas de ce soir. »

Le jeune homme acquiesça en silence et commença à se lever tandis qu’Ernaut fronça les sourcils :

« Ton oncle ? Heu, je veux dire : *votre* oncle ? Je fais excuse, je vous ai pris pour un valet.

— Nulle offense, je ne suis rien ici. Même pas encore chevalier... Tu as certainement plus important statut que le mien.

— Je ne suis que sergent le roi, et pas un des plus renommés.

— Voilà déjà bel office, que de servir son roi. Je ne peux en dire autant. Puissions-nous partager ce destin à l’avenir... »

Il tendit sa main amicalement :

« Je suis Isaac Vacher.

— Ernaut de Vézelay. »

La réponse fit sourire Isaac, qui lui adressa un clin d’oeil.

« Vu où nous sommes, je te nommerais plus volontiers Ernaut de Jérusalem. » ❧

## Notes

Si par son besoin même de migrants latins pour maintenir ses structures (impérativement catholiques), les territoires francs levantins offraient des perspectives de promotion sociale, rien n’était pour autant simple. On cite souvent Renaud de Châtillon comme l’archétype du jeune noble porté aux plus hautes sphères par ses mérites, une politique matrimoniale audacieuse et une volonté inébranlable. Mais pour un homme tel que lui, il devait s’en trouver des centaines qui échouaient dans les sables du désert syrien, ou végétaient dans des cercles périphériques du pouvoir.

Il existait un statut de chevalier dit de « soudée », qui désignait un noble, mais payé ainsi qu’un soldat. Pour un montant annuel très élevé, mais il demeurait rien moins qu’on combattant rémunéré pour ses services, dont la pension pouvait être révoquée à tout moment. D’autres pouvaient également se voir confier les possessions d’autrui, et le roi de Jérusalem avait l’habitude de nommer des châtelains pour superviser les fortifications dépendant directement de son pouvoir. Bien que cela permît parfois de faire souche, on peut suivre le destin de tels chevaliers qui n’eurent jamais la possibilité d’aller plus haut dans la hiérarchie. Ils étaient des centaines à servir le roi de Jérusalem de façon plus ou moins directe par les levées de troupe alors que les places prestigieuses au Haut Conseil étaient rares.

## Références

Mayer Hans Eberhard,  »Angevins versus Normans: The New Men of King Fulk of Jerusalem », dans *Proceedings of the American Philosophical Society*, vol.133, n°1, Mars 1989, p. 1-25.

Prawer Joshua, *Crusader Institutions*, Oxford University Press, Oxford, New York : 1980.

Shagrir Iris, *Naming Patterns in the Latin Kingdom of Jerusalem*, Prosopographica et Genealogica, Oxford : 2003.

Smail R.C. , *Crusading Warfare, 1097-1193*, 2nde édition, Cambridge University Press, Cambridge : 1995.
