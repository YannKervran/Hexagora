---
date: 2015-07-15
abstract: 1096-1993. Le destin d’un morceau d’étoffe de qualité, depuis son tissage jusqu’à sa redécouverte à l’aube du XXIe siècle. Quels destins peut croiser un simple amas de fil teinté ?
characters: Yusuf al-Yama al-Bandaniji, Abu Muqatil, Sabiha, Yasmina, Shahid, Clément Ferpieds, ’Ali nomade, Salih, Abu Hamzah, Fazila la Nomade
---

# Fil du temps

## Alexandrie, midi du youm al talata 12 jha'ban 489^[Mardi 5 août 1096.]

Yusuf al-Yama al-Bandaniji suait abondamment, les gouttes salées lui irritant le cou. Il avait beau s’aérer avec un large éventail, il lui semblait respirer à l’entrée d’un four. Retranché dans la pièce la plus fraîche de la maison de son ami Abu Muqatil, il désespérait de pouvoir rentrer bientôt à al-Quds[^alquds]. Sa demeure y disposait d’une courette ombragée en été, où gazouillait une fontaine recouverte de mosaïque. Le simple fait d’y penser faisait frissonner sa nuque épaisse. Il avala quelques longues gorgées de sirop de canne fortement dilué.

Devant lui s’étalaient de nombreuses étoffes qu’un négociant avait déballées à son intention. À l’approche de ramadan, avec toutes ses festivités, il tenait à ce que son épouse soit dignement habillée. Il voulait donc lui offrir une robe de soie, voire une pièce de tissu neuve pour qu’elle se fabrique le vêtement. Le marchand était un petit homme à la peau bronzée, légèrement cuivrée, un de ces Égyptiens hautains qui méprisaient ceux qui n’avaient pas vu le jour sur les rives du Nil. Sa lippe dédaigneuse claquait régulièrement sous son nez aquilin et ses yeux de faucon paraissaient de jais dans la pénombre des claustra. Yusuf se pencha vers une pièce qui l’intriguait et la montra du doigt.

« Qu’est ceci ?

— Une magnifique toile issue des ateliers de Fustat, parmi les plus rigoureux. Connais-tu celui qu’on nomme al-Najm ?

— Pas que je sache…

— C’est un homme empli de sapience et de talent. Il dirige un des ateliers les plus réputés. Cette étoffe est née là-bas, entre les doigts experts de ceux qu’il dirige. »

Yusuf fit signe de la main qu’il voulait éprouver le tissu par lui-même. Le drapier déploya le pan en un geste emphatique, le gonflant telle une voile de navire pour en démontrer toute la finesse et la légèreté. Les motifs bleus chatoyèrent un instant dans les rais de lumière.

« Vois cette superbe étoffe intacte ! Elle est de belle taille, et l’on pourrait y couper plus d’un vêtement. »

En disant cela, il retint un sourire en réalisant que l’acheteur était d’une circonférence telle qu’on aurait été bien en peine d’y faire une simple ’aba[^aba] pour lui. Heureusement, Al-Bandaniji semblait concentré sur l’examen minutieux des fibres, admirait les motifs qui se déployaient en assemblages géométriques.

« Quel serait le prix pour cette pièce ?

— Il me faudrait la peser. Trois dinars le ratl[^ratl] généralement. »

Yusuf manqua s’étrangler.

« Tu te moques de moi ! Tu me prends pour un fellah stupide ou quoi ! Ce n’est que de l’*harrir*[^harrir], je ne vais tout de même pas la payer comme si elle venait de Perse !

— Ah, je me suis trompé en effet, tu parles de juste. Ce morceau coûte seulement 25 dinars les 10 ratls.

— Je ne mettrai jamais plus de deux dinars le ratl pour une pareille étoffe. »

Le marchand se renfrogna, faisant mine de replier la toile.

« Tu négliges la qualité des décors. Ils sont de bonne tenue, ne partiront pas au lavage ou avec le temps. Nos teinturiers savent faire des couleurs qui résistent à notre beau soleil égyptien.

— Je n’ai guère envie de payer un tel prix, moi qui n’ai pas à supporter cette fournaise. Je t’en propose 21 dinars les 10 ratls.

— À 22 je peux encore m’en sortir. Si je vais en dessous, mes associés penseront que je les ai volés. Un si beau travail ne peut être vendu à moins.

— 22 alors, c’est dit » accepta Yusuf, hochant la tête avant d’avaler une nouvelle longue rasade.

« Aurais-tu beaux turbans à me proposer ? Pas de ces vulgaires chiffons à deux-trois dinars, un vrai beau couvre-chef, que je sois fier d’arborer à la mosquée pour youm al joumouia[^aljumah]. »

En disant cela, il eut soudain une pensée pour Sabiha. Si jamais il ramenait à son épouse de quoi l’habiller de neuf et qu’il n’avait rien prévu pour elle, celle qu’il aimait appeler sa petite gazelle risquait bien de se montrer lionne. Elle n’était généralement que douceur et tendresse, mais jamais Allah n’avait fait de femme plus jalouse. Elle mesurait l’amour que lui portait Yusuf à l’aune des ratls de cadeaux qu’il lui faisait plus qu’à ses démonstrations d’affection empressées. Pas question de se contenter d’un simple *mindîl*[^mindil] pour la satisfaire, s’il achetait cette belle étoffe de soie bleue pour son épouse, il faudrait qu’il fasse au moins aussi bien pour elle. Il soupira, la nuque moite. L’Égypte n’était vraiment qu’une vaste fournaise.

## Jérusalem, après midi du youm al khemis 22 jha'ban 492^[Jeudi 14 juillet 1099.]

Yasmina aperçut du mouvement dans la chambre des maîtres tandis qu’elle se rendait sur la terrasse, histoire de voir ce qui se passait dans les alentours. Intriguée, elle souleva la portière d’épais tissu. Elle découvrit Shahid qui s’acharnait auprès d’un des gros coffres avec un énorme couteau de cuisine.

« Mais qu’est-ce que tu fais ? »

Le domestique sursauta, en échappa son outil, manquant de se trancher un doigt.

« Yasmina ! Tais-toi, tu vas attirer les maîtres ! »

Elle s’approcha, les sourcils froncés, la voix grave comme si elle s’adressait à un enfant.

« Tu as perdu la tête ? Qu’essaies-tu de faire ?

— Il faut se sauver. Viens avec moi, Yasmina, les Ifranjs vont tuer tout le monde ici. Il faut se sauver ! »

Elle secoua la tête en dénégation.

« Nous sommes à l’abri ici. Si tu sors, tu vas y laisser ta tête.

— Rester, c’est mourir. Prenons ce qu’on peut et fuyons. »

Il soupira, hésitant à se lancer. Il avait toujours trouvé Yasmina à son goût, même si elle était un peu plus âgée que lui. Elle était parfois un peu trop infantilisante, mais elle le voyait comme le domestique qu’il était. Une fois dehors, dans leur propre demeure, elle apprendrait à le respecter comme un homme. Du moins l’espérait-il. Il lui prit la main, et la fixa, ce qui déclencha un malaise immédiat.

« Viens avec moi, Yasmina. Je te protégerai et, inch’Allah, nous nous marierons une fois tout ça fini. »

Elle s’esclaffa sans aménité.

« Tu comptes me protéger avec ton couteau ridicule ? Ils ont des épées d’acier et des armures de fer dehors. Nous ne ferons pas deux pas que tes entrailles seront versées dans la rue et moi emmenée par ces chacals puants. »

Elle retira sa main sans brusquerie, mais avec fermeté.

« Nous n’allons pas fuir tous les deux, Shahid. Je t’aime bien, comme un petit frère, alors je t’en conjure, reste ici avec moi et les maîtres. »

Le jeune homme haussa le menton de défi, blessé dans son orgueil.

« Tu crois que je ne saurai pas me défendre, *te* défendre ?

— Je crois que ce n’est pas le moment de sortir surtout ! »

Ignorant sa réponse, il reprit sa lame et s’acharna brièvement sur la délicate serrure du coffre, révélant un ensemble de vêtements de prix, d’étoffes brodées, teintes et décorées.

Il s’empara d’une robe de soie chatoyante, aux motifs bleus géométriques.

« Regarde, rien qu’avec ça, on a de quoi vivre un long moment.

— Tu es fou… »

Démontrant d’un geste qu’elle n’avait pas forcément tort, il lui brandit le couteau sous la gorge. Les yeux écarquillés d’horreur, elle tenta de s’écarter mais il la retint par le poignet.

« Si tu ne viens pas, je…

— Je ne dirai rien, j’en fais serment, Shahid. Je ne trahirai pas. Mais ne me fais pas de mal, je t’en conjure… »

Hésitant un instant, il la relâcha lentement, jouant avec ses les lèvres comme un enfant pris en faute.

« Viens avec moi, Yasmina. Nous pourrions mieux vivre qu’ici. Damas n’est pas si loin.

— Comment aller jusque là bas ? Comme de vulgaires vagabonds ? »

Il s’empara d’une poignée d’étoffe qu’il entreprit de se nouer autour de la taille, sous ses propres vêtements de travail. Une voix résonna dans l’escalier, depuis le rez-de-chaussée, attisant la fébrilité des deux domestiques.

On appelait en bas.

« Range ça, intima Yasmina sans succès, tentant de reprendre les tissus qu’il sortait désormais sans plus de retenue.

— Va voir ce qu’ils veulent ! cracha-t-il d’un air offensé. Si tu ne viens pas avec moi, va te terrer avec eux. Et mourir quand les Ifranjs forceront la porte. »

La jeune femme se mordit la lèvre, affolée. Elle hésita à caresser la joue de son compagnon de domesticité, mais elle craignait qu’il comprenne de travers cette démonstration d’affection. Il courait à la mort et ne voulait rien entendre. Comme elle se levait pour aller répondre aux maîtres, Shahid nouait le beau vêtement en soie à motifs bleux sur sa poitrine. Et soudain un bruit formidable retentit dans le couloir d’entrée. On venait de forcer la porte dans un fracas épouvantable.

## Clepsta, après-midi du samedi 8 septembre 1156

Clément fit couler la dernière goutte de son verre dans son gosier desséché. Il n’avait pas marché longtemps mais la chaleur des collines de Judée était assommante pour le colporteur qu’il était. Il allait d’un casal à l’autre, porteur de babioles : rubans, boutons, colifichets et petits accessoires, épingles, pendentifs en étain. Il approvisionnait ainsi les habitants sans qu’ils n’aient à se déplacer jusqu’à Jérusalem, distante de plusieurs lieues. Il profitait de l’allègement progressif de sa balle pour acheter de vieux vêtements qu’il recyclait avec l’aide de sa mère.

La vie n’était pas toujours aisée mais il aimait cette liberté qu’elle lui offrait, et parfois il savourait de bons moments, comme en cet instant. Il était arrivé la veille tardivement et n’avait pas eu le temps de présenter ses articles, ce qu’il avait fait au matin. Une de ses acheteuses lui avait proposé de venir examiner des habits dans l’hôtel de ses parents, où il profitait pour l’instant du confort d’un matelas, dans une niche ombragée. La femme était allée chercher les pièces qui pourraient l’intéresser, qu’elle avait mises immédiatement à l’abri au décès de son père, avait-elle expliqué. Elle demeurait avec son époux et ses enfants quelques rues plus loin.

Sa silhouette se découpa bientôt dans la porte inondée de lumière, suivie d’un gamin portant une corbeille bien remplie. La matrone n’était guère jolie, le visage rougeaud et plat, avec des yeux écartés et trop grands, mais elle était accueillante et sympathique, assez bavarde. Sa tenue soignée attestait en outre d’une bonne maîtresse de maison. Clément estima qu’il ne serait guère judicieux de faire des offres trop basses, elle ne serait certainement pas dupe et pourrait s’en offenser. Il s’approcha de la table où elle disposait les vêtements les uns après les autres.

« Je ne vois là que de bien ordinaires toiles. Mais la façon en est belle, et la trame encore solide » approuva-t-il.

Tout en examinant les morceaux de tissu, il les triait un à un, dans une sélection devant lui. Il voyait les pupilles de la femme s’allumer d’une lueur avide tandis que le tas augmentait. Puis un peu de malice s’y mêla lorsqu’elle sortit le dernier vêtement. Clément en écarquilla les yeux. Elle tenait à la main une cotte de soie claire aux motifs bleux géométriques. Assurément une pièce de choix, quoiqu’un peu courte et sans grande ampleur.

« Cette pièce aussi est à vendre ?

— Oui-da. Mon père y tenait fort, car il en avait hérité du sien. C’était un vêtement gagné par mon aïeul lors de la prise de la sainte Cité. Nous sommes croisés de la première heure dans ma famille. »

Sa voix modulée avec fierté accusait le prestige qu’elle accordait à une telle affirmation et Clément ne voulut pas la vexer, se contentant d’opiner mollement du chef tout en tendant la main vers la cotte. Immédiatement, il vit que le bas était composé d’une myriade de petits morceaux adroitement assemblés.

« Elle est fort abîmée, c’est dommage.

— J’ai fait de mon mieux, on ne voit guère les coutures.

— Certes, mais des pans de grande ampleur donnent plus de valeur. On peut plus facilement y retailler à son gré. Qu’est-il arrivé à la toile ?

— Je ne sais guère. Mon père ne m’en a jamais rien dit. Je me suis contenté de repriser. »

Elle n’allait certainement pas avouer à un pied poudreux, un quasi-inconnu, que son père s’était fait mordre par un mâtin un jour qu’il sortait de l’église dans sa belle cotte. Il en avait été si contrarié qu’il avait refusé de se rendre à la messe pendant de longs mois, se houspillant avec le curé à la moindre occasion à propos de cette attaque qu’il estimait démoniaque.

« Je crains ne de pouvoir revendre la cotte en elle-même, et y retailler fera perdre beaucoup de valeur. Vous en espériez bon prix, je pense.

— Je ne le céderai certes pas comme vulgaire coutil, je n’ai aucune urgence. C’est l’héritage de mon père après tout. »

Clément fit mine d’examiner l’étoffe avec dépit, mais il savait qu’il pouvait le transformer. Un tissu de si belle qualité pourrait être découpé pour en faire des pochettes à vendre aux pèlerins, pour qu’ils y resserrent les précieuses reliques qu’ils ramenaient de Terre sainte. Parfois ce n’était qu’un caillou de la ville où Jésus avait marché, mais le fait de le confier à la soie en rehaussait la sainteté, quand on le montrait à ses amis et familiers restés en France, en Normandie ou en Auvergne. Il y avait même de quoi faire quelques petits couvre-chefs, simples bonnets circulaires qui protégeaient le crâne du lourd turban posé au-dessus.

Reprenant les pièces qu’il avait sélectionnées auparavant, il additionna rapidement. Quelques chausses, une cotte de solide chanvre et une autre de laine, des chemises plus ou moins râpées, en coton…

« Je vous proposerai pour le tout dix besants de mes marchandises, ou huit en monnaie sonnante et trébuchante. J’ajoute encore cinq besants d’affaires pour la cotte de soie, ou trois et deux rabouins en monnaies. »

La paysanne compta sur ses doigts et remua le paquet de linge qu’il avait devant lui, de façon à en vérifier le contenu.

« Cela ne fait tout de même pas lourd pour cotte de soie.

— Elle est fort usée, et trop petite pour la plupart des hommes. Sans compter que le bas en est bien abîmé.

— Iriez-vous jusqu’à dix-sept besants de marchandies pour le tout ? En sus, je vous promets le gîte et le couvert chaque fois que vous passerez jusqu’à Noël prochain. »

La garantie d’une héberge, et de repas certainement de qualité, vu la matrone, convainquirent Clément. Ne pas avoir à sortir de monnaies d’argent lui plaisait, car il évitait d’en trop avoir sur lui, par peur des détrousseurs. Il était rare qu’on mentionne des voleurs aussi près de la ville, maintenant que les musulmans n’y venaient plus guère, mais il savait par expérience que des gens pouvaient être attaqués et disparaître sans laisser aucune trace pour peu qu’ils aient trop de bien sur eux.

« Marché conclu, répondit-il avec enthousiasme.

Puis il ajouta, un large sourire aux lèvres : cela comprend aussi le souper de ce jour ? »

## Abords du Jourdain, près de Jéricho, soir du youm al sebt 27 jha'ban 658^[Samedi 7 août 1260.]

Salih fut réveillé par sa mère qui le secouait doucement. Il jeta un œil par-dessus la couverture et vit que le jour était levé ; plusieurs de ses sœurs étaient déjà affairées. Parmi elles, Fazila lui fit un sourire, tout en préparant les galettes de pain qu’elle mettait à cuire dans le petit four de terre. Il entendait derrière le *qata*[^qata] la voix de son père mêlée à celle de ses oncles.

Il se leva, encore un peu ensommeillé et regarda à travers un des trous de la toile de séparation. Il vit que son frère aîné était là lui aussi et que les hommes discutaient des événements récents. Des djinns terribles venaient du nord, de par delà les montagnes, et ils s’affrontaient aux seigneurs des villes voisines, sans que Salih comprît bien en quoi cela les concernait, eux[^mongols].

Se frottant les yeux, il revint chercher de quoi se restaurer, se faisant réprimander par les femmes plus âgées, car il encombrait le passage. Il prit un gobelet de lait frais, un morceau de pain et sortit devant la tente, pour s’asseoir sur une pierre à l’écart.

Tout en grignotant son repas, il finit de se réveiller en regardant ses sœurs et sa mère traire les chèvres à l’ombre des arbres de l’oasis. En partant, son père vint lui frictionner affectueusement la tête, accompagnant son geste d’un sourire. C’était un des hommes les plus influents de la tribu, bien que n’étant pas le scheik. Il était néanmoins assez âgé, tout le monde respectait son expérience. Et lui avait une tendresse particulière pour son dernier fils, Salih, auquel il pardonnait bien des bêtises qu’il n’aurait jamais tolérées d’un autre de ses enfants.

Après avoir ramené son gobelet, Salih partit en quête de ses amis, avec lesquels il aimait passer le temps. Sa mère, le voyant s’éloigner, l’apostropha :

« Salih, ne t’éloigne pas trop. Tu dois aussi aller à la corvée de bois ce matin, n’oublie pas.

— Non non je reviens vite, je vais voir ’Ali et Zaid » répondit-il en se mettant à courir.

Il ne lui fallut pas longtemps pour retrouver ses deux camarades de jeu. Ils étaient occupés à ramener un chevreau, l’air sérieux et affairés.

« Qu’est-ce qui se passe ?

— C’est Abu Hamzah qui nous a demandé de ramener ce chevreau. Il s’était coincé dans un paquet de broussailles. »

Tout en parlant, ils aiguillaient l’animal d’un air attentif, le guidant vers le reste du troupeau, sous la surveillance de plusieurs hommes dont Abu Hamzah, le chef des pâtres. Les enfants partirent ensuite vers la rive du Jourdain pour s’amuser à faire des ricochets. Ils y retrouvèrent plusieurs femmes de corvée d’eau, dont ils se cachèrent parmi les bosquets pour échapper à des tâches domestiques. Tandis qu’ils choisissaient avec soin les pierres qui glisseraient le mieux sur les flots, ’Ali leur proposa :

« Et si on allait fouiller les ruines après manger ?

— Tu sais qu’on n’a pas le droit, c’est dangereux là-bas répliqua Zaid.

— C’est une réponse de fillette ça. Je suis sûr qu’on pourrait y trouver des choses intéressantes. Mon frère ’Umar, y m’a dit qu’il y avait ramassé des pointes de flèches quand il était plus jeune. On pourrait se faire des arcs comme les djinns, et s’entraîner à tirer, pour aller chasser.

— Oui ! Cherchons de quoi se faire des arcs et des flèches. Je sais où trouver la ficelle dont on aura besoin. »

Les trois enfants se mirent alors à la recherche de ce qui pourrait leur servir dans leurs desseins guerriers, oubliant qu’ils devaient aller à une autre recherche, de bois, avec les femmes.

Plus tard dans la journée, Salih était assis à l’abri de l’auvent, appuyé contre le *qach*[^qach], sanglotant piteusement lorsque son père arriva pour manger. Il s’enquit de ce qui se passait, interrogeant du regard son épouse. Elle lui répondit d’un air pincé, sachant par avance que son autorité serait désavouée.

« Il a oublié encore une fois qu’il avait du travail à faire ce matin. Il a disparu avec ses petits copains et n’est reparu que pour manger. Alors, il est puni, il n’a rien pour son repas. S’il veut manger, il n’a qu’à s’acquitter de ses corvées.

— Tu faisais quoi au lieu de faire ce que ta mère t’avait dit ? »

Salih répondit entre deux hoquets :

« On cherchait de quoi se faire des arcs, pour aller chasser. On a trouvé des sacs de trésor…

— Quelle chance ! Viens avec moi, fils, raconte-moi ce que tu voulais faire. »

Son père l’entraîna dans ses quartiers, où il fut rapidement servi, ainsi que ses deux cadets pas encore mariés. Contrevenant à ce que sa femme avait décidé, il partagea son repas avec Salih. La bonne humeur du gamin revint aussi vite qu’elle était partie. Il convint de ramener son petit trésor dans l’espoir qu’il ait de la valeur.

Tandis que les adultes s’allongeaient pour prendre un peu de repos après manger, Salih s’esquiva avant de croiser de nouveau sa mère. Il retrouva ses compagnons de jeu dans la tente voisine, où se trouvait également sa sœur Fazila, qui y apprenait de nouvelles techniques de tissage auprès d’une femme expérimentée. Zaid et ’Ali discutaient de ce qu’ils allaient faire des petites bourses de toile qu’ils avaient dénichées, emplies de poussière des bords du Jourdain.

Ils en avaient vidé le contenu et prévoyaient de s’en faire chacun un sac à merveilles. Les trois gamins eurent tôt fait de retourner vers les ruines. Le terrain montait régulièrement jusqu’au plateau qui accueillait le village abandonné, parmi les palmiers. Une végétation rase avait commencé à envahir ce qui devait être à l’origine un des fleurons de la présence franque en Terre sainte.

Les enfants croisèrent deux cavaliers probablement venus de la grande cité voisine. Ils avaient stoppé là, car l’endroit offrait un excellent point de vue sur les environs. Indifférents aux gamins, ils s’approchèrent ensuite nonchalamment du camp, où ils restèrent un moment à discuter, avant de mettre pied à terre et de se rendre dans la tente du scheik. Un certain nombre de têtes de bétail fut rapidement séparé du troupeau par Abu Hamzah et ses hommes. Les trois compagnons réunirent ensuite leurs trouvailles et repartirent tranquillement vers le camp.

Le soir, Salih était tout excité, courant de-ci de-là avec ses amis. Ils trottaient d’une tente à l’autre, se cachant parmi les broussailles qui les protégeaient du vent. Une excellente vente avait été conclue par un de ses oncles aujourd’hui et un repas de fête était prévu. Salih avait aidé ses sœurs et sa mère à préparer le *mensaf*[^mensaf], et c’est lui qui avait eu l’honneur de poser la tête de mouton sur le plat prêt à être servi. Son arrivée fut accompagnée d’exclamations de plaisir.

Il rejoignit ensuite les femmes avec lesquelles il dînait en compagnie de quelques cousins. Il était encore trop jeune pour partager le repas des hommes. On discuta essentiellement tissage, car l’une d’entre elles était en train de finir une nouvelle tente. Fazila y apprenait le travail en longs panneaux des fils en poils de chèvre. Cette toile était pour un jeune qui allait bientôt se marier. Il avait assez de têtes dans son troupeau et avait conclu un accord avec une famille d’une tribu voisine. La cérémonie était prévue pour la fin de l’hiver. On évoqua aussi la situation de Fazila. Elle ne tarderait pas à se voir dotée d’un époux. Leur père s’en occupait avec sérieux, ayant déjà éconduit plusieurs prétendants qu’il ne trouvait pas à son goût.

Durant tout le temps du repas, Salih couvait des yeux un panier où les petites bourses de tissu coloré avaient été mises à l’écart. Stockées avec d’autres guenilles dans une grotte proche, à l’abri, elles seraient ensuite revendues à des négociants pour les fabricants de papier. Salih regrettait juste de ne pas avoir pu conserver une des jolies petites bourses pour lui. Lorsque sa mère le déposa sur sa couche pour la nuit, il l’attrapa par le cou en un geste affectueux un peu outrancier. Puis il lui murmura :

« Mère, je pourrais garder un des petits sacs, celui avec les dessins bleus ? Je promets de faire mes corvées aussi longtemps que tu voudras. »

Mais sa mère avait élevé trop d’enfants pour se laisser prendre à ce jeu. Elle tira la couverture sur lui et répondit.

« Fais d’abord tes corvées, et Allah veillera à te récompenser. »

## Caverne 38, falaise du Qarantal, près de Jéricho, mercredi 7 juillet 1993

La truelle de l’étudiant grattait soigneusement le sol, dévoilant les uns après les autres les vestiges qu’il dégageait ensuite à l’aide de pinceaux et d’outils fins et précis. Depuis quelques jours, c’était l’effervescence dans le petit groupe qui participait à la campagne de fouilles. La découverte d’un trésor était toujours grandement excitante, et il ne fallait nullement qu’il soit d’or pour provoquer l’enthousiasme.

En l’occurrence, il s’agissait de simples fibres, de fragments textiles, mais en telle quantité que cela permettrait de mieux connaître les usages médiévaux. L’endroit était daté des alentours du XIIIe siècle et la formidable documentation écrite qu’on avait pour la période saurait éclairer avec pertinence le moindre brin exhumé. À peine reconnu et extrait de sa gangue, le plus petit morceau d’étoffe était soigneusement numéroté, empaqueté, aux fins de nettoyage et de conservation avant l’analyse. Si le climat sec du lieu pouvait préserver miraculeusement des fibres sur des centaines d’années, cela pouvait être ruiné par une mauvaise manipulation, un traitement inadapté.

Le jeune étudiant de l’université d’Haifa qui fouillait tout à côté jeta un coup d’œil curieux par-dessus l’épaule de la veinarde qui s’employait à dégager un lambeau de tissu. La zone F, où ils se trouvaient avait jusqu’à présent dévoilé le plus de trésors, mais lui n’avait eu droit qu’à des morceaux de ce qui devait être de la corde ou, du moins, de la vannerie de ce type. Ils s’interrompirent un instant tandis que le responsable de leur secteur, qui était allé aux nouvelles, revenait l’air ravi.

« Il y a plusieurs morceaux de qualité dans ce qu’on a trouvé l’autre jour. De la soie, en particulier un gros morceau à motifs bleus, géométrique, le 704. »

Voyant le visage émerveillé des jeunes étudiants archéologues qui avaient participé à la découverte de ce trésor, le responsable ajouta « Il va y avoir de la saisie à faire, avec tous ces fragments. »

La perspective de longues heures à saisir des données informatiques fastidieuses : taille et densité des fils, sens de torsion, nature des poils, technique de tissage, teintures…, rien ne pouvait entamer la joie des ouvriers de fouille enthousiastes devant la richesse de leur trésor. Pas même le fait que seule une poignée de spécialistes obscurs se pencherait sur l’étude qui en sortirait. ❧

## Notes

Ce texte est inspiré par mes études d’archéologie, au cours desquelles j’ai eu la chance de pouvoir manipuler de nombreux objets de vie quotidienne, essentiellement de la céramique, pour laquelle j’ai toujours eu une tendresse particulière. J’ai rapidement éprouvé un sentiment d’intimité avec les fabricants et les utilisateurs passés, simplement en raison du fait que je tenais par moment des artefacts dont ils usaient chaque jour.

Le dernier passage est aussi un clin d’œil au travail pas facile et souvent méconnu des archéologues, dont la réalité est à des années-lumière des aventures dépeintes dans les films à grand spectacle. Par contre, j’ai toujours rencontré chez eux une véritable jubilation à découvrir des artefacts, non pas pour les objets en eux-mêmes, car cela concerne plus les pilleurs et/ou les collectionneurs, mais à cause du formidable potentiel que chaque vestige recèle quant à notre histoire. Je ne prétends aucunement avoir présenté là une image fidèle de la campagne de fouilles dirigée par Orit Shamir et Alisa Baginski, mais il me semblait que sans cela, le récit aurait été incomplet

## Références

Baginski Alisa, Shamir Orit, « Textiles’ Treasure from Jericho Cave 38 in the Qarantal Cliff Compared to other Early Medieval Sites in Israel » (2012), *Textile Society of America Symposium Proceedings*, Paper 742. En ligne : <http://digitalcommons.unl.edu/tsaconf/742> (consulté le 12 janvier 2015).

Baginski Alisa, Shamir Orit, « Textiles’ Hoard from Jericho Cave 38 », dans *The Qarantal Cliff, Hoards and Genizot as Chapters in History*, Haifa, 2013, p.77-88. En ligne : <https://www.academia.edu/3659997/Shamir_O._and_Baginski_A._2013._Textiles_Hoard_from_Jericho_Cave_38_in_the_Qarantal_Cliff._Hoards_and_Genizot_as_Chapters_in_History._Haifa._Pp.77_-88_> (consulté le 12 janvier 2015).
