---
date: 2015-06-15
abstract: 1156. Marcel l’Espéron, notable beauvaisien, a fait le choix de rejoindre Jérusalem en marchant, porteur des symboles de sa foi. Le chemin qui le mènera au cœur de la chrétienté sera l’occasion d’en apprendre sur lui-même.
characters: Aubertin, Emeline, Frogier, Gillot  le Charpentier, Honfroi, Liénart, Luce, Mainet, Marcel l’Espéron, Remiggio
---

# Marcheurs de lumière

## Cathédrale de Beauvais, après-midi du dimanche 3 juin 1156

La pierre froide frigorifiait la joue et l’oreille de Marcel, allongé sur les dalles, les bras en croix. À côté de lui, les vitraux traçaient des arabesques colorées sur le sol, tandis que les voix des chanoines et des fidèles montaient dans la nef. Il sentit des pas se rapprocher, devinant les souliers brodés de l’évêque Henri[^henrifrance] à la périphérie de son champ de vision.

« *Domine deus pater omnipotens cuius sempiterne divinitatis potentiam et inmense maiestatis dominationem*… »

L’encens se répandait sur lui tandis que l’oraison s’élançait vers les cieux. Ce matin, la messe de Pentecôte avait été longue, étendu qu’il était à l’entrée du chœur, mais le chanoine lui avait expliqué que c’était un moment très favorable à la remise du bourdon, de la besace et de la croix et qu’il serait bon d’y assister en humble pénitent. Puis d’attendre ainsi, en oraison, que l’office de célébration pour leur départ se déroule également, jusqu’au moment où on leur distribuerait leurs attributs. Que son voyage en serait d’autant plus sanctifié.

Pour l’instant, la dureté du dallage lui meurtrissait les genoux et il ne se sentait guère en mesure de laisser son esprit accompagner les prières. Il réprimait régulièrement l’envie d’éternuer et reniflait pour faire disparaître la goutte qui s’amassait lentement au bout de son nez.

« …*peracto presentis uite cursu, suorum mercedem laborum recepturus feliciter ualeat*. »

Les mains de jeunes clercs l’aidèrent à se relever, à se mettre à genoux, la tête baissée. Il frissonna et se gratta rapidement le visage, tandis que l’évêque s’éloignait de lui dans un froufroutement de soie rouge. Lorsqu’il revint, un garçon présentait sur un coussin devant lui les croix qu’avec ses compagnons de route ils arboreraient sur leur poitrine tout au long du pèlerinage. Apposant ses doigts gantés et bagués, l’évêque susurra quelques mots, et un autre officiant s’approcha, tenant un ouvrage ouvert. La voix forte résonna de nouveau sous la haute voûte.

« …*Benedic domine hanc crucem quam famulus tuus pro devotione sancti domini nostri Jesu Christi…* »

Profitant de ce que l’évêque passait entre les rangs des croyants qui s’apprêtaient à partir pour Jérusalem, Marcel se dandina un peu d’un genou sur l’autre. Ils étaient presque une vingtaine à recevoir croix, besace et bourdon ce jour, et la cérémonie n’en finissait pas. Il entendait le vague brouhaha issu de la nef cathédrale, où la foule de leurs proches amassés discutait sûrement de tout et de rien alors que les prêtres officiaient. Il se souvint des innombrables affaires qu’il avait traitées à l’abri de ces murs, tandis que les divins mystères se déroulaient.

Il y avait même échangé son premier baiser, encore enfant. Avec la jeune Laïs, désormais mariée à un négociant en vin parisien. Peut-être était-elle morte, d’ailleurs, depuis toutes ces années. La vision de son visage rond parsemé de son, du charmant petit nez retroussé, de ses boucles retenues par un ruban vermillon, tout lui revint soudain en mémoire. Il revoyait la poussière dansant dans la lumière tandis qu’il goûtait aux lèvres de son amie. La voix autoritaire de l’évêque l’arracha brutalement à ses rêveries lorsqu’on lui tendit une croix d’étoffe rouge.

« *Suscipe frater mi Marcellus victoriosissimum sancte crucis vexillum per quod secure possis omnium inimicorum tuorum maliciam superare…* »

Tandis que la distribution continuait, Marcel caressa de l’index la douce étoffe de fine laine teinte. Deux bandes croisées, qu’il allait coudre sur son vêtement, censées le garder de tous les malheurs, pour peu que sa Foi soit pure et sincère. À son retour, il la conserverait dans un coffret.

Il l’en sortirait à la veillée, en même temps que la palme rapportée de Jéricho, devant les enfants ébahis et admiratifs, comme son oncle quand Marcel était enfant. Pierre Deux Croix l’appelait-on, car il s’était rendu deux fois en Terre sainte. La première un peu par curiosité, et aussi par défi. Et la seconde, pour y porter une mèche de cheveux de son épouse défunte et de ses deux filles mortes en bas âge.

Les bâtons et les besaces furent rapidement disposés devant les pèlerins, aux pieds de l’évêque. Le prélat s’empara d’un aspersoir et relut de nouveau le lourd volume que lui présentait le jeune clerc. Il secoua la main, faisant s’envoler quelques gouttes du liquide béni à chaque oscillation du poignet.

« *…ut digneris hanc sportam et fustem istum benedicere, quatinus eos qui illos in signum peregrinationis et suorum corporum sustentationem sunt receptori…* »

Marcel se sentait rajeuni par l’office, malgré l’inconfort. Il s’était lavé, fait couper les cheveux, raser la barbe et portait des vêtements neufs. Ses souliers aux coutures récentes lui meurtrissaient d’ailleurs un peu les orteils, à demeurer ainsi agenouillé. Oubliant la gêne, il s’enchantait des odeurs de savon montant à ses narines, mélangées aux volutes d’encens et à la saveur douceâtre des cierges colossaux qui l’entouraient.

Lorsque l’évêque et ses officiants se présentèrent devant lui, il tendit les bras comme on lui avait dit de le faire, prêt à recevoir les attributs qui l’accompagneraient sur les chemins jusqu’en Outremer. Un jeune clerc lui remit la besace que la femme de son frère lui avait confectionnée. Elle l’avait taillée dans une étoffe de prix, achetée dans une balle pendant la foire Saint-Martin à Pontoise, quelques mois plus tôt. Elle avait brodé le rabat et la bandoulière de croix de laine colorée.

« *In nomine domini nostri Jesu Christi, accipe hanc sportam peregrinationis habitum…* »

Le sac posé sur l’épaule, ses yeux coulèrent vers le solide bâton de marche en frêne qu’il avait fait tourner par Guéraud, un des fils de ses anciens voisins. La pointe ferrée ne risquait pas de glisser sur le sol et la tête s’ornait d’une petite croix de bronze qui scintillerait au soleil, illuminant ses journées lieue après lieue. Il se sentait en outre suffisamment aguerri pour en user au besoin contre les crânes de ceux qui tenteraient de s’opposer à son périple ou, plus prosaïquement, de le détrousser.

« *Accipe hunc baculum sustentationis itineris ac laboris uie peregrinationis habitum*… »

Lorsqu’il se releva, un peu bancal, Marcel arborait un sourire rayonnant. Les prêtres s’étaient évanouis par une porte dérobée, et seuls des acolytes demeuraient, occupés à ranger le matériel liturgique. Une odeur d’encens persistait, porteuse d’élan sacré. On se félicitait, on s’embrassait sans trop de ménagement.

Marcel rejoignit un neveu et sa femme et les enlaça de concert, avant d’attraper leurs enfants en une brassée enthousiaste et rieuse. Les gamins, sans trop savoir pourquoi, étaient gagnés par l’effervescence et couraient en tout sens en criant. Ils singeaient la cérémonie et les formules sacrées entendues plus tôt, et que personne ou presque n’avait compris en dehors des célébrants.

Les marcheurs convinrent entre eux de se retrouver à l’aurore le lendemain matin devant la cathédrale, afin de prier une dernière fois saint Pierre, à qui l’église était dédiée, de les mener sains et saufs jusqu’au tombeau du Christ. Puis ils franchirent les portes en une foule compacte, agitée, enthousiaste.

## Abords de Salzburg, midi du vendredi 6 juillet 1156

Installé sur un talus surplombant un ruisseau gargouillant, le petit groupe de marcheurs faisait une pause sous un bosquet de saules et de noisetiers, en contrebas du sentier. Un peu plus loin au sud s’étendait une vaste cité, aux puissants remparts, abritée sous un imposant château bâti sur une éminence. Au-delà, les reliefs des Alpes ornaient de crêtes l’horizon.

Les champs aux alentours étaient emplis de froment dont la couleur dorée annonçait la moisson prochaine. Depuis le chemin s’entendait le grincement de l’essieu d’un char lourdement chargé de pierres, tiré par un bœuf. Le paysan, baguette sur l’épaule sifflait paisiblement, le visage protégé par un chapeau de paille. Loin devant lui, un convoi de poneys disparaissait dans les frondaisons d’un bosquet ombragé. La plaine respirait l’opulence et la tranquillité, à si peu de distance d’une puissante cité.

Marcel massait ses pieds après les avoir trempés dans le petit cours d’eau. Il appréciait chaque fois de pouvoir ainsi se délasser, dès que l’occasion s’en présentait. Il attribuait à cette habitude le fait qu’il n’avait eu à déplorer aucun souci depuis leur départ, un mois plus tôt. Ils avaient marché d’un bon pas, parcourant les territoires alémaniques sans encombre, trouvant chaque soir une héberge confortable et de quoi se nourrir. Seulement, depuis quelques jours, des dissensions avaient surgi au sein du petit groupe.

Aubertin, un des plus jeunes, qui les avait rejoints à Strasbourg, maintenait qu’ils auraient dû couper par les montagnes et suivre la côte. Il savait que Venise, la célèbre ville marchande se rencontrait par là-bas et prétendait qu’il pourrait être avisé d’envisager de traverser par mer.

Profitant de l’arrêt, il expliquait à Emeline et Frogier, deux laboureurs des environs de Bailleul sur le Thérain, à Froidmont, que cette cité représentait le dernier moment où cela serait encore pertinent d’obliquer au sud. Son attitude agaçait Marcel, qui avait en quelque sorte pris la tête du groupe. N’ayant aucun clerc parmi eux vers qui se tourner, son statut de doyen l’avait naturellement désigné. Il interpela Aubertin d’une voix cassante.

« La paix, garçon, on continue au levant. »

L’autre leva la tête, fronçant le nez comme s’il allait mordre.

« Tu n’es point baron de cet ost, le Marcel. Il n’y a qu’hommes libres ici, nul serf !

— Moi j’y vois au moins un bec-jaune, désireux d’apporter discorde en notre groupe. Si tu es acertainé de ton idée, prends ton bourdon et pars au sud.

— Cheminer seul n’est pas bonne chose pour le pérégrin, tu le sais comme moi.

— N’as-tu pas licence du sire évêque en ta besace ? le railla Marcel.

— Il n’est point question de ça. Tu le sais fort bien. »

Marcel enfila et noua son soulier et se releva, s’approchant du jeune homme.

« Nous avons moult palabré de cela. Nous suivons au plus droit par voie de terre. Nous envisageons le retour par mer, plus rapide, pour ne pas trop se perdre en chemin. La discussion est close. »

Aubertin baissa le regard, mais pinça les lèvres. Il n’avait pas les moyens de se payer la traversée, Marcel le savait. En faisant partie d’un groupe, il pouvait espérer que son passage serait pris en charge avec les autres. Mais son attitude rebelle lui avait attiré pas mal d’inimitiés au sein des marcheurs.

Ils étaient partis une vingtaine de Beauvais, désormais presque trente-cinq à avancer, bâton en main. Un prêtre les avait suivis un temps, jusqu’à ce que des coliques l’obligent à rester dans un monastère, quand ils avaient commencé à longer la chaîne des Alpes. Aubertin s’était rapproché du petit groupe des Beauvaisiens et Marcel suspectait que c’était parce qu’il avait été condamné à faire ce pèlerinage pour un crime quelconque dans sa cité d’origine. Il n’aimait pas du tout son attitude. Mais n’avait guère de moyens de s’en débarrasser.

## Col de Chipka, fin de journée du vendredi 14 septembre 1156

Le majestueux panorama qui s’offrait aux yeux des marcheurs harassés les récompensait de la fatigue accumulée à gravir les pentes. Jusqu’à se noyer dans les nuages au loin, près de nouveaux contreforts montagneux, les plaines au sud s’étendaient. Des villages semblaient si petits qu’on aurait pu les écraser d’un doigt. De part et d’autre, les monts du Grand Balkan, couverts de forêts, se découpaient en alternance de vallées et de crêtes verdoyantes.

Assis auprès du rocher orné d’une croix, Marcel se reposait un peu en attendant les retardataires. Peu à peu, le groupe s’était étiré tandis que l’effort creusait les visages et épuisait les souffles. Ils avaient décidé de passer ce col aujourd’hui malgré la fatigue accumulée. Il le regrettait un peu désormais. Ils avaient encore pas mal de chemin à parcourir et le soleil baissait déjà.

Les derniers voyageurs croisés, une caravane de marchands de toile byzantins qui se rendaient à Craiova, étaient loin derrière eux. Aucun local ne déambulait plus par les routes si tard, ce qui leur poserait problème s’ils se trouvaient bloqués en montagne en pleine nuit. Dormir parmi fossés et haies n’était jamais agréable, devoir le faire sur un relief était bien plus dangereux. En dehors du risque, toujours présent, de finir dans une crevasse, la météo était bien trop changeante pour que cela soit prudent de le faire. D’un autre côté, continuer à marcher alors même qu’on ne voyait plus ses pieds ne l’enchantait guère. D’autant que la lune, en fin de son dernier quartier, n’apporterait pas de lumière, si elle parvenait à percer les nuages qui s’amoncelaient.

Lorsque les retardataires se montrèrent, essoufflés et écarlates, il ne leur laissa guère le temps de se reposer, leur expliquant qu’il leur fallait hâter le pas s’ils espéraient arriver à une halte pour la nuit. Maugréant et pestant, les marcheurs entamèrent donc la descente le visage maussade.

Les bourdons frappaient le sol en silence, tandis que les pieds fatigués faisaient rouler les cailloux, soulevaient la poussière du chemin. Les lacets se succédaient, chaque virage les rapprochait de la plaine d’où les fumées des villages montaient vers le ciel désormais bouché. Les arbres se mirent à frissonner sous le vent, incitant les plus frileux à se couvrir la tête de leur capuche. Au détour d’un des ultimes tronçons, le groupe s’immobilisa soudain.

Devant eux, au milieu du chemin, une douzaine d’hommes attendait. Tous avec des lances, des haches, et des arcs pour quatre d’entre eux. Assis sur une souche, appuyé sur le manche de sa cognée, un grand gaillard souriait d’un air sadique. Voyant que les pèlerins s’étaient arrêtés à quelque trente mètres d’eux, il se leva nonchalamment et s’approcha. Autour de lui, tous étaient nerveux. Les tireurs avaient encoché et deux d’entre eux semblaient prêts à bander. La voix du chef résonna dans le silence.

Il parlait avec une intonation rauque, dans une langue que personne ne connaissait. Mais nul n’en était besoin, pour deviner qu’il avait l’intention de les détrousser, voire pire. Marcel hésitait sur la décision à prendre. Il ne pouvait parlementer avec les brigands, ne comprenant rien à leur dialecte. Il savait que sur leur groupe d’une vingtaine d’hommes, seule une dizaine était apte à se défendre correctement et tous n’en auraient pas le cœur.

Tandis qu’il réfléchissait, le truand continuait à pérorer et semblait s’agacer du manque de réaction face à lui. Les détrousseurs étaient désormais tous tendus, crispés sur leurs armes. Marcel échangea un coup d’œil avec Gillot, un charpentier qui avait perdu une main dans un accident, mais dont la force était demeurée intacte. Ce dernier hocha la tête lentement, empoignant son bâton plus serré. Marcel hésitait à cause des arcs. Avant qu’ils n’arrivent au contact, plusieurs flèches auraient volé vers eux. D’un autre côté, rien ne lui garantissait qu’ils seraient laissés sains et saufs s’ils obtempéraient.

Il n’avait jamais été de ceux qui pliaient sans se battre, l’évêque Henri pouvait en attester. Il ne pouvait admettre que de simples larrons des montagnes allaient réussir là où le propre frère du roi n’y parvenait pas. Il leva la main en un signe amical et fit quelques pas en avant, pas trop pour ne pas inquiéter les brigands, mais suffisamment pour attirer leur attention sur lui. En passant parmi ses compagnons, il interpela à voix basse les hommes qu’il jugeait les plus aptes à se battre.

Arrivé devant le groupe, il fit mine de fouiller dans sa besace. Il n’y conservait pas grand-chose, les monnaies qu’il avait l’intention de dépenser pour la traversée de retour étant cousues dans ses vêtements. Mais il faisait semblant d’y chercher quelque richesse à donner. Le chef commença à rire, mais Marcel n’aurait pas juré qu’il était dupe de son jeu. Un des archers s’était relâché, mais les autres larrons paraissaient plus nerveux que jamais.

Marcel exhuma le parchemin que l’évêché lui avait remis, attestant de son statut de pèlerin. Il le leva bien haut, comme si c’était là un important document, souriant aux brigands. Il faisait mine de marcher comme un souffreteux, avançant avec hésitation. Il savait que sa barbe poivre et sel le vieillissait, et il espérait que la nuit tombante accentuerait cette impression.

Quand il ne fut plus qu’à une dizaine de pas des détrousseurs, il se rua subitement en avant, son bourdon brandi droit sur le meneur. Il hurla tel un dément, en appelant à la rescousse ses compagnons. Avant même que quiconque réagisse, il abattait sa croix de bronze en direction du sourire détestable.

## Faubourgs de Byzance, matin du samedi 29 septembre 1156

Le petit hospice accolé à la modeste église bénéficiait d’un jardin bien entretenu, d’agrément autant que potager. Marcel y déambulait tranquillement, goûtant les premières lueurs de l’aube, avant que le vacarme de la cité toute proche n’emplisse les oreilles. Ils étaient bien hébergés, et la nourriture était, sinon de qualité, au moins roborative. De plus, un savetier voisin s’employait à réparer leurs semelles et à leur garantir une paire de secours, le tout pour un montant honnête.

Ils étaient désormais une quarantaine, fragments agglomérés de marcheurs épars. La plupart étaient originaires de France, mais ils avaient rencontré ici des Pisans, dont le groupe avait été décimé par des fièvres et des coliques lors de leur traversée. La longue route avait clairsemé le noyau de Beauvaisiens. Frogier avait été grièvement blessé par une flèche à Cipka, et Emeline était resté auprès de lui, dans un village qu’ils avaient croisé peu après. Honfroi, le vieux changeur, était décédé, épuisé, avant même qu’ils n’arrivent au pied des Alpes. Aubertin, le trublion, avait disparu une nuit, alors qu’ils étaient dans la vaste plaine des Huns. Il ne le regrettait guère, celui-là.

Pour le moment, ce qui l’inquiétait, c’était l’état de Lienart. Fils d’un des principaux drapiers de la cité, membre actif de la commune il le connaissait depuis des années. Le jeune s’était épris de religion et avait souhaité prendre la coule plusieurs fois, sans parvenir à convaincre son père. Finalement, il s’était rabattu sur un pèlerinage au cœur de la chrétienté, peut-être dans l’espoir d’y trouver sa voie. Il était alité depuis deux jours, après s’être plaint de flux de ventre depuis un bon moment.

Les Pisans lui avaient dit qu’ils étaient bloqués ici depuis plusieurs semaines. Ils avaient espéré voyager par bateau jusqu’à Antioche, au moins, mais aucun navire ne les avait acceptés. Ils proposaient donc de se regrouper pour suivre la côte anatolienne. Les effroyables récits qu’ils avaient entendus sur les reliefs des plateaux les avaient incités à la prudence. Une troupe d’Allemands bien armés et équipés, avec plusieurs petits barons, y avait laissé une demi-douzaine d’hommes . Ils avaient cru y rester à vouloir braver la fureur des Turcs belliqueux qui contrôlaient la zone.

Luce, une des Strasbourgeoises, sortit alors qu’il allait rentrer. Sa silhouette empâtée s’était amincie au fil des lieues et son visage fin trahissait la fatigue. Elle s’était cassé le nez et avait perdu deux dents en chutant dans un ravin, mais il demeurait en elle une certaine grâce qui rappelait à Marcel sa propre fille. Il n’en avait plus aucune nouvelle depuis qu’elle était partie avec ce Normand mal embouché.

Il sentait qu’il reportait sur la jeune femme un peu de l’affection qu’il ne savait plus guère exprimer depuis qu’il était veuf, sans enfant dont s’occuper. La mort de son benjamin, Martin, d’une mauvaise fièvre, avant même qu’il ne trouve une épouse, avait été le déclencheur. Plus rien ne le retenait à Beauvais, si ce n’était ses devoirs, et il aspirait à autre chose. Au grand dam de ses amis et relations, il avait annoncé qu’il prendrait la besace et le bourdon à la Noël dernier.

On avait ri de sa résolution, se moquant de ses habitudes de bonne chère, d’amateur de putains, de belles étoffes et de lit de plume. Mais il avait tenu bon, peut-être renforcé dans sa décision par les railleries. Il avait vendu tout ce qu’il possédait, en avait confié une grande partie aux frères de saint Jean de l’Hôpital, dans l’espoir soit de revenir, soit de demeurer en Outremer. Il avait presque cinquante ans, et ne regrettait rien de sa longue vie. Il était juste déterminé à ne pas gâcher le peu qui lui restait, ne s’intéressant plus guère à ce bas monde.

Un moment, il avait hésité à rejoindre un couvent, mais l’austérité au quotidien le rebutait un peu. Le pèlerinage était aussi l’occasion de découvrir un ailleurs, et mourir en faisant cela lui convenait. Et le sourire de Luce, même en partie édenté, éveillait en lui des démons qu’il pensait depuis longtemps endormis. Échappant un rictus amical, il s’esquiva pour la laisser passer. Coquette, elle plissa les yeux en remerciement, se cachant la bouche d’une main pudique. Elle n’acceptait pas de bonne grâce sa nouvelle apparence.

Le regard caressant la silhouette qui s’éloignait, il soupira puis alla chercher ses affaires. Il fallait qu’il voit Remiggio, le Pisan le plus mature, pour discuter du voyage à venir.

## Faubourg de l’Asnerie, Jérusalem, veillée du jeudi 20 décembre 1156

Luce frissonnait malgré l’épaisse couverture à la puissante odeur de chèvre. Ils étaient tous assis ou allongés, affalés, dans la paille d’un bâtiment annexe de l’hôpital de Saint-Jean. Arrivés en fin d’après-midi, ils avaient préféré reporter leur entrée dans la cité sainte au lendemain, avec le jour nouveau. Ils avaient soupé d’un gruau poivré qui avait des effluves d’ambroisie sous leurs palais enthousiastes. Ils étaient enfin au terme de leur voyage, le but de ces mois de souffrance et de dévotion !

Marcel n’était plus le gros homme pansu de naguère, désormais sec comme un chêne en hiver, et les joues moussues de barbe grise. Ses cheveux, qu’il n’avait guère coupés depuis son départ, étaient noués en une dérisoire queue de cheval. Mais il souriait, satisfait. De Beauvais, il ne restait que neuf personnes.

Certains arriveraient peut-être plus tard, retardés par la maladie ou les blessures. Il pensait en particulier à Mainet, un potier dont les rhumatismes le tourmentaient tellement qu’il n’avait guère pu suivre leur rythme de marche. Avec un peu de chance, il serait là pour les Pâques. Plein de son bonheur, Marcel en souhaitait à tous les absents.

Dans quelques jours, il assisterait à la grande messe de Noël dans l’église qui abritait le tombeau du Christ. Puis il aurait de nombreuses semaines pour aller découvrir les endroits que les curés avaient évoqués devant lui, depuis son enfance. Il était surtout curieux de voir le Jourdain, là où on pouvait être baptisé de nouveau au même endroit que le Christ.

Luce lui avait avoué, un des soirs derniers, qu’elle aurait aimé prendre le voile dans une des abbayes de Jérusalem, mais elle avait envie de visiter les lieux saints avec lui auparavant. Elle avait apprécié assez justement la lueur qui habitait le regard de Marcel, mais s’y était refusée, n’accordant que son amitié. Elle ne souhaitait plus que le Christ pour époux, lui avait-elle confié.

Tout ce qu’il avait obtenu d’elle, c’était un baiser bien chaste sur le front et une caresse de la joue. Elle avait en elle des blessures qui ne pouvaient se guérir dans le siècle apparemment. Quelques années plus tôt, Marcel aurait peut-être balayé tout cela d’un geste moqueur et tenté d’embrasser les lèvres de force. Mais il était trop épuisé, trop calme, pour s’essayer à cela désormais.

Il ne savait guère ce qu’il allait faire par la suite, car il ne s’était jamais projeté au-delà de son arrivée en Terre sainte. Il avait d’ailleurs finalement dépensé toutes les monnaies dissimulées sur lui, depuis bien longtemps. La dernière avait fini chez un apothicaire de la côte contre quelques remèdes pour soigner un des Pisans. Il avait gardé la lettre des frères de Saint-Jean, énumérant les biens par lui confiés à leur garde. Peut-être pourrait-il l’échanger contre quelques besants ici. Cela lui éviterait le retour interminable et fastidieux. Il avait même assez d’argent pour entrer fort honorablement dans un couvent. ❧

## Notes

Le départ en pélerinage, y compris pour une destination proche, n’était pas une mince affaire. La plupart se contentaient de cultes locaux, de saints pas trop éloignés, au maximum d’une ou deux semaines de marche. Mais l’attrait pour Jérusalem ne se démentit pas, même au sein des couches populaires.

Être pèlerin donnait droit à l’assistance des établissements hospitaliers au long du chemin et, plus généralement, d’un accueil au moins minimal de la part des coreligionnaires. Des lettres rédigées par les autorités ecclésiastiques étaient remises aux marcheurs, de façon à leur permettre de prouver leur état, pour bénéficier de l’exemption de taxes, d’hébergements gratuits, etc. Malgré le statut qui en théorie garantissait la charité, il était plus sage de porter avec soi de quoi acheter de la nourriture ou les bonnes grâces de potentats locaux.

Il demeure malheureusement difficile de connaître exactement les conditions de voyage des pèlerins les plus modestes justement parce que ce sont ceux qui sont passés le plus inaperçus dans les sources. Ce n’est généralement qu’à travers des mentions indirectes qu’on peut les suivre.

Au XIIe siècle, le trajet vers la Terre sainte se faisait souvent par voie de terre, les marcheurs se regroupant pour affronter les périls. En outre, ils croisaient de nombreux autres voyageurs, suffisamment pour s’assurer la plupart du temps un cheminement en relative sécurité.

Le cérémonial encore balbutiant au XIIe siècle est attesté bien qu’apparemment conçu selon les besoins *ad hoc* de la congrégation où il se déroulait. Celui présenté dans la première partie s’appuie sur un texte extrait du pontifical de Bari (Ms. Graz Univer. 239, fol. 143v-146v), publié par Kenneth Pennington (que je remercie ici de m’avoir aimablement fourni un tiré à part), traduit et interprété par Joël Schmitz, qui m’avait déjà bien éclairé lors de la recherche documentaire pour le tome 2 des aventures d’Ernaut. N’ayant que partiellement utilisé la matière par lui traduite, nul doute que je la réemploierai à l’avenir.

Vous trouverez ci-dessous l’intégralité de son travail, ainsi que le renvoi vers la transcription de Kenneth Pennington.

Le titre « Marcheurs de lumière » est inspiré de l’évangile selon saint Jean, chap.XI, versets 9-10, lorsqu’il évoque la résurrection de Lazare.

**Ms Graz. Univer. 239 fol. 143V — 146v**

Traduction de J. Schmitz, d’après Pennington Kenneth, *Traditio, Studies in ancient and medieval history, trought, and religion*, volume XXX, 1974, Fordham University Press, « The rite for taking le cross in the twelfth century », p.431-32.

*Office concernant ceux qui se rendent à Jérusalem*

Qui demeure à l’abri du Très Haut… (Ps 90). Heureux ceux dont la conduite est intègre… (et qui marchent dans la loi de Dieu… Ps 118). Enseigne-moi, Seigneur, ta voie… (guide-moi au droit chemin… Ps 26,11). Gloire au Père, etc… Seigneur aie pitié. Christ aie pitié. Notre Père… et ne nous laisse pas succomber à la tentation… J’ai dit, Seigneur, aie pitié. Guéris mon âme (car j’ai péché contre toi… Ps 40,5). Envoie-leur, Seigneur, ton secours de ton sanctuaire depuis Sion (Ps 19,3). L’ennemi jamais ne l’emportera sur lui ni le fils d’iniquité (Ps 88,23). Le Seigneur soit avec vous… (etc.) (Oraison) : Seigneur Dieu et Père tout puissant, dont le pouvoir divin est éternel et la souveraine majesté infinie, et vers qui nous nous rendons maintentant dans la foi, nous te demandons de regarder avec bienveillance la dévotion de ton serviteur qui espère en l’immensité de ta miséricore et qui souhaite humblement la grâce ineffable de ta tendresse, et qui, à cause des manquements à l’amour de ton nom, et par refus des tentations du monde, a voulu se rendre avec empressement, là où ton Fils, Notre Seigneur Jésus-christ, est né par ta volonté d’une vierge, selon la chair, est mort et ressuscité avant de monter au ciel.

D’après l’oracle du prophète qui a dit de lui (Jésus-Christ)…  
« Nous prierons en un lieu où il a séjourné », et parce que la fragilité humaine ne peut donner aucun fruit digne de pénitence sans ton aide, mais que grâce au don et à la largesse de tous tes biens, il va marchant vers toi au travers du chemin de tes commandements de toujours, Toi secours de ton peuple, protège le, pour son mérite qu’il doit conserver, et garde le pour ce mérite qui doit être protégé, dans l’accomplissement de la vie présente. Qu’il soit fort, lui qui est sur le point de recevoir avec bonheur la récompense de son labeur. Par ce même (Christ NS, etc.)

*Bénédiction de la croix*  
Dieu éternellement tout puissant, toi qui, par la soumission et le supplice de la sainte croix de ton fils Notre Seigneur Jésus Christ, as permis de racheter l’homme déchu par la faute diabolique, dans ta clémence porte ton regard sur cet étendard de la sainte croix, et dans ta tendresse daigne la bénir, afin que, à la lumière de la grâce de l’Esprit Saint, soit donné le mérite d’obtenir la rémission de tous ses péchés par le digne fruit de la pénitence, et la vie éternelle, à quiconque aura pris la croix en l’honneur de ton saint nom. Par (etc.)

*Autre*.  
Bénis, Seigneur, cette croix que ton serviteur est sur le point de prendre sur lui par dévotion de ton saint nom. Accorde lui, nous te le demandons, de montrer extérieurement de l’humilité, tant dans le regret que l’aveu de son repentir, de sorte que s’en suive pour lui une véritable purification de son âme, par l’effet de ta très grande largesse, ainsi que l’expiation de ses péchés. Par (etc.)

*Lorsque la croix est remise*  
Reçois cette croix signe de ton pélerinage au nom de notre Seigneur Jésus Christ, afin que, préservé par elle de toutes mauvaises rencontres (de tous malheurs), tu parviennes à ta destination, en temps opportun, et mérites de l’atteindre avec l’approbation du Christ. Qui avec le Père (etc.)

*Autre*.  
Reçois cette croix, signe de ton pélerinage, au nom de la rédemption de notre Seigneur Jésus Christ, et qu’à la manière de l’ange de Tobie lui-même, elle te précède sur ton chemin comme guide et compagnon, et de même que son ange le dirigeait, qu’elle te conduise à destination, te soit un compagnon joyeux, et qu’elle te préserve des incommodités du chemin. Que l’Esprit Saint daigne t’être un compagnon de voyage et écarte les méchants loin de toi.

Par lui-meme. En unité avec lui (Jésus-Christ)…

*Autre*.  
Reçois, mon frère N… l’étendard très victorieux de la sainte croix par lequel, comme d’une hache, tu puisses l’emporter sur la malice de tes ennemis, et te dresser victorieux en soldat, avec tous ceux qui suivent fidèlement Jésus-Christ, jusqu’au temps de l’ultime rétribution, lors de ton retour de la guerre, après avoir terrassé les ennemis et orné de la palme de la victoire, tu puisses recevoir, de notre seigneur Jésus-Christ qui est le chef suprême, des couronnes impérissables de gloire, et régner sans fin avec lui, dans son palais éternel. Qui avec le Père (etc.)

*Bénédiction de la besace et du bâton*  
Seigneur Jésus Christ, rédempteur et créateur du monde, toi qui as recommandé à tes bienheureux apôtres qui partaient prêcher de prendre avec eux seulement un bâton, nous te demandons avec insistance et dévotion de daigner bénir cette besace et ce bâton, afin que ceux qui sont sur le point de les recevoir en signe de pérégrination et de sustentation de leurs corps, reçoivent la plénitude de ta grâce céleste, et éprouvent la force de ta bénédiction, et que, de même que le bâton d’Aaron se désunit lui-même des Juifs rebelles en fleurissant dans le temple du Seigneur (Nb 17,10-11), tu les absolves de tous péchés, eux qui doivent être ornés de la marque des serviteurs du bienheureux Pierre pour que, au jour du jugement, ils soient séparés et placés à ta droite, en vue d’être couronnés. Par (etc.)

*Quand il reçoit la besace*  
Au nom de notre Seigneur Jésus Christ, reçois cette besace de voyage, afin que tu puisses accéder, en bonne santé et corrigé de tes fautes, au pays des bienheureux apôtres Pierre et Paul et des autres saints où tu souhaites parvenir et que, ton voyage accompli, tu puisses faire ton retour vers nous, sain et sauf. Par (etc.)

*À la remise du bâton*  
Reçois ce bâton pour soutenir ta marche et t’aider dans les moments pénibles de ton voyage, afin d’être fort pour soumettre toute bande d’ennemis et de parvenir ainsi sans danger au pays des bienheureux apôtres Pierre et Paul et des autres saints où tu souhaites te rendre, puis revenir vers nous, sain et sauf.

## Références

Ciggaar Krijnie N., *Western travalers to Constantinople. The West and Byzantium, 962-1204: Cultural and Political Relations*, Leiden - New York - Cologne : Brill, 1996.

﻿Collectif, *Voyages et voyageurs au Moyen Âge*, Paris : Publications de la Sorbonne, 1996.[En ligne], consulté le 23 septembre 2011, Adresse URL: <http://www.persee.fr/web/revues/home/prescript/issue/shmes\_1261-9078\_1996\_act\_26\_1>

Heath Sydney, *Pilgrim Life in the Middle Ages*, London, Leipsic: T. Fisher Unwin, 1911.

Pennington Kenneth, « The rite for taking le cross in the twelfth century », dans *Traditio, Studies in ancient and medieval history, trought, and religion*, volume XXX, 1974, Fordham University Press, p.429-35.

Verdon Jean, *Voyager au Moyen Âge*, Paris : Perrin, 2003.

Webb Diana, *Pilgrims and Pilgrimage in the Medieval West*, Londres - New York: I.B. Tauris, 1999.
