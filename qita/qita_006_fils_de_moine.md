---
date: 2012-04-15
abstract: 1133. Petite visite dans le monastère de la Charité, où un intrigant visiteur fait une découverte bien surprenante. Les communautés monastiques pouvaient éveiller des espoirs pour les enfants de familles désargentés.
characters: Herbelot, Herbelot Gonteux, Humbaud, Ymarus
---

# Fils de moine

## Bords de Loire, matinée du mardi 9 mai 1133

Sous un ciel invisible, revêtu d’un voile gris, les lambeaux de nuage semblaient piégés par les branches de saule. Au loin on entendait quelques sternes lançant des cris rauques, alarmées par un prédateur. Un héron passa silencieusement, frôlant l’eau de ses longues ailes. Menée d’un geste sûr par le passeur, la barge résonna doucement contre le ponton rudimentaire, quelques troncs assemblés en bord de plage. Prenant son temps pour se préparer, un des voyageurs descendit de l’embarcation avec précaution, guidant attentivement sa monture. Il était soulagé de n’avoir pas eu à traverser à gué, vu le haut niveau des eaux ce printemps. Le grand pont de bois était en pleine construction et il n’était pas possible de traverser autrement qu’en bac pour le moment.

Frissonnant sous sa chape de voyage, il frotta son nez rougi par le froid et l’humidité. Il n’avait décidé de quitter le palais de l’archevêque[^vulgrin] Vulgrin qu’à contrecœur, satisfait du confort douillet de l’endroit. Il soupira, le regard perdu dans le vague parmi l’épaisse brume du val. Une bourrasque de vent souleva sa capuche, révélant la fine tonsure marquant son état de clerc. En dehors de ce détail, il était difficile de le croire homme de Dieu. Élégamment vêtu d’étoffes de laine colorée, chaussé de délicats souliers guère adaptés à la marche, il tenait la bride d’un cheval de prix, dont le harnachement aurait fait pâlir d’envie de nombreux chevaliers. La voix du passeur le ramena à la réalité :

« Le bon jour, biau sire, puisse la charité des moines de Notre-Dame vous faire bon accueil. »

Sans lui répondre, le passager le salua d’un sourire. En quelques grandes enjambées, il avait rejoint le petit groupe qu’il avait côtoyé pour franchir le premier bras de Loire : deux hommes dirigeant un convoi de mules, au regard moins vif que celui de leurs animaux, un pâtre rigolard menant trois moutons, aidé d’un chien galeux et d’un garçonnet à l’air espiègle. Ils dépassèrent en silence les quelques maisons qui s’étaient installées sur l’île, modestes cabanes de pêcheurs ou de mariniers, avançant en direction du vaste bourg appuyé sur l’autre rive de la Loire, accolé à l’imposante prieurale Notre-Dame, fille aînée de Cluny.

Comme sur le premier bras du fleuve, un imposant pont de bois était en train d’être refait, mais le passage n’en était possible que pour de simples piétons, pas trop lourdement encombrés. Le long des piles, des pontons accueillaient des moulins, en partie achevés également. Le cavalier n’était guère rassuré par les flots écumants et ronflants qu’il découvrait. Le fin crachin virevoltant ajoutait à son humeur maussade. Les mules stoppèrent brutalement, apparemment peu convaincues par la perspective d’une nouvelle traversée. Les moutons se rallièrent à cet avis et n’avancèrent sur le bac que contraints par les morsures du chien et la crosse du berger. Le voyageur flatta sa monture sur l’encolure afin de l’apaiser et la fit progresser en lui parlant calmement. Une fois à bord, il s’employa à la tenir. Tout à son effort, il ne jeta que de rapides coups d’œil à l’édifice qui perçait la brume, vaste vaisseau de pierre déchirant le brouillard, à l’assaut du ciel. Le passeur chantonnait pour lui-même, indifférent aux tourbillons. Voyant le regard angoissé des voyageurs, il cracha dans l’eau, l’air goguenard, avant de leur crier :

« D’meurez sans peur, c’est quand l’eau manque qu’il faut craindre. Là, avec si bon courant, on ira pas s’écueiller en une souche, et pis on traverse en un rien de temps… »

De fait, l’embarcation arriva rapidement sur la berge orientale, sans dommage, au grand soulagement du voyageur. Descendant le premier, il entreprit de se diriger au plus vite vers l’entrée de l’hôtellerie du monastère. Traversant d’un bon pas les rives affairées, où les nombreux chalands déchargeaient ou installaient leurs cargaisons, il n’eut pas un regard pour les pêcheurs hissant depuis leurs fûtreaux[^futreau] leurs paniers de loches, d’aloses et d’anguilles.

Une fois en ville, il entendit distinctement les ouvriers s’activer sur la vaste église et n’avait qu’une hâte : admirer de ses yeux le nouveau chœur, les chapelles récentes, les dernières travées. On lui avait longtemps vanté l’édifice, parmi les plus grandioses que la chrétienté ait jamais réalisés. La prieurale Notre-Dame avait reçu toute l’attention du riche ordre clunisien, et les dons avaient afflué depuis un demi-siècle.

La façade occidentale, en plein travaux, était parsemée d’échafaudages improbables, de cordages pendants et de poulies grinçantes. Quelques portefaix grimpaient sur des échelles, courbés sous le poids du mortier ou des mœllons. La place était bordée des loges des sculpteurs et des tailleurs de pierre ainsi que d’une forge où tintait le marteau sur l’enclume.

Sous la direction du vieux prieur Eudes[^eudesarpin], récemment décédé, l’endroit avait connu la prospérité, à l’image du puissant ordre auquel il appartenait. L’entrée du monastère se trouvait sur la gauche, les battants grand ouverts. Un convers[^convers], à la bure fatiguée et dépenaillée, avisa le bel aspect du nouvel arrivant et partit en courant prévenir le frère hôtelier de l’arrivée d’un visiteur de marque.

Les départs terminés, la cour était calme. Seuls quelques moines s’affairaient autour du cellier, entreposant à l’abri les denrées collectées à Pâques. L’hôtelier, un petit homme au visage fripé, s’approcha rapidement, ordonnant d’un geste de s’occuper du cheval. Il était accompagné d’un jeune moine à la face rubiconde, porteur d’une aiguière et d’une serviette de lin. Ils s’inclinèrent devant le nouvel arrivant, qui les salua à son tour en ôtant sa capuche, révélant ainsi son état et se présentant rapidement. L’hôtelier esquissa un rapide sourire, dévoilant de trop rares dents tandis que le voyageur se lavait rapidement les mains avant de les sécher.

« Grand honneur de vous avoir en nos murs, frère. Escomptez-vous rencontrer le père prieur Ymarus ?

— Selon son désir. Pour ma part, je n’ai nulle tâche auprès de lui, je suis porteur de courriers à destination de Paris. »

Le vieil homme hocha la tête, prenant un air grave.

« Avec tous ces travaux, il est fort occupé. Je lui ferai savoir que vous êtes présent, et il avisera. »

Le messager hocha la tête en assentiment.

« Pourriez-vous me faire savoir le chemin pour se rendre en la grande prieurale ? J’aurai grande joie à me recueillir sur vos autels… »

Souriant de toutes ses gencives, le moine indiqua son jeune compagnon, qui ne les avait pas lâchés d’une semelle tandis qu’ils s’approchaient du bâtiment réservé aux hôtes de marque.

« Humbaud est là pour vous servir, il vous indiquera le chemin, aisé à trouver par ailleurs. Vous resterez pour un long temps ?

— Certes pas plus d’une poignée de jours. Il me faut juste trouver une nave pour descendre la Loire jusqu’à Orléans. J’ai quelques missives pour Fleury en chemin.

— Et pour quel sire vous m’avez dit porter messages ?

— Je ne l’ai pas dit, rétorqua le mystérieux clerc avec un grand sourire.

## Prieurale Notre-Dame de la Charité-sur-Loire, midi du jeudi 11 mai 1133

Le soleil cru frappait durement la pierre blanche de l’édifice, obligeant à plisser les yeux pour en admirer l’ordonnancement des volumes. Sur les toits, les couvreurs travaillaient à la protection du nouveau chœur. En tant que clerc, le voyageur avait été autorisé à se rendre dans le cloître, tant qu’il ne perturbait pas les frères.

Néanmoins, avec le chahut des jeunes novices à qui l’on faisait classe, il aurait été difficile de se faire remarquer à moins d’y hurler comme un dément. Il passait par là pour se rendre dans l’église, par le bras septentrional du transept. En y entrant, il ne pouvait s’empêcher de s’émerveiller devant le magnifique ordonnancement de l’endroit. Bien que le chœur en fût de nouveau en travaux, augmenté en raison de l’affluence des pèlerins, il s’en dégageait une telle paix qu’il aurait apprécié de pouvoir s’y retirer. Seulement, son devoir l’appelait ailleurs, et il n’était pas encore prêt à renoncer au siècle. Peut-être après quelques années encore, lorsque les horreurs de ses semblables auraient fini de briser l’espoir en lui.

Les maçons et les sculpteurs avaient laissé la place aux peintres et aux charpentiers, et quelques frères surveillaient d’un œil expert la réalisation des décors figurés, selon l’enseignement qu’ils avaient reçu. Se faufilant parmi les groupes de pèlerins aux tenues fatiguées, le messager fit le tour du déambulatoire en flânant, prenant le temps de se recueillir à chaque chapelle. Puis il se rendit par une porte latérale dans le passage qui menait vers l’est, au-delà du chevet et de la vieille église Saint-Laurent dont il ne restait plus guère que le chœur et un tronçon de nef. Il avait envie de rejoindre les jardins du prieuré, de se délasser un peu à l’écart, au calme.

Comme dans tous les monastères bénédictins, les jardins étaient très soignés, les vergers faisaient de nombreux envieux, et les fleurs cultivées servaient à embellir l’édifice. Le visiteur se pencha, intrigué par la taille d’un parterre de lys. Il sourit pour lui-même : logique, dans une prieurale dédiée à la Vierge. Tandis qu’il se relevait, il lui sembla entendre un miaulement dans un buisson adjacent. Intrigué, il s’approcha, curieux de voir le matou qui se cachait par là. Il ne vit qu’un panier, oublié par quelque jardinier distrait. Le miaulement reprit, différemment cette fois. Inquiet de ce qu’il croyait reconnaître, il s’approcha et souleva le couvercle d’osier, révélant un bébé, âgé de quelques mois tout au plus, bien emmitouflé dans un vieux drap de laine. Il gazouillait, à demi endormi. Le clerc se releva brusquement, cherchant la mère aux alentours. Sûrement une chapardeuse, vu que les femmes n’avaient rien à faire dans l’enceinte du monastère.

Un peu éberlué, il chercha parmi les bosquets, derrière les claies, voir si personne ne se dissimulait, attendant son départ. Il appela plusieurs fois, en vain. Puis il se décida : il attrapa délicatement le panier et prit la direction de l’hôtellerie.

## Cellier du prieuré Notre-Dame de la Charité-sur-Loire, après-midi du jeudi 11 mai 1133

Le couffin improvisé trônait sur la table du cellier, autour de laquelle s’interrogeaient plusieurs officiers du monastère : l’hôtelier, le cellérier, le maître des novices, en plus du messager. Tous se demandaient d’où venait cet enfant et, surtout, ce qu’il convenait d’en faire. Après un long silence, le maître des novices osa la première remarque, de sa voix chantante de méridional :

« Il me paraît fort jeune, il ne saurait se passer du lait maternel… »

Le cellérier, un vieux barbu au nez couperosé, hocha la tête en assentiment. Attendri, il sourit au bébé avant de reprendre contenance.

« Il faudra dire au prêtre de la paroisse de faire annonce au prochain dimanche, si ses parents le cherchent. »

L’hôtelier oscilla de la tête, désapprobateur.

« Je ne vois guère comment on pourrait l’égarer en notre courtil[^courtil]. Il leur a fallu grimper outre le mur de clôture. Non, je suis assuré que cet enfançon ne peut compter que sur nous, les siens n’ont pu le garder pour un motif qui sera jugé par le Seigneur. »

Le cellérier souleva le tissu qui enveloppait l’enfant, déclenchant une gesticulation qui le fit bien vite reculer, non sans avoir vérifié ce qu’il voulait voir.

« Au moins c’est un garçonnet, sa présence n’est donc pas déplacée. »

L’hôtelier haussa un sourcil narquois.

« Et alors ? Le voilà bien trop jeunot pour demeurer en la clôture…

— Nous pouvons toujours le confier à une vilaine de nos terres, le temps pour lui de se passer de lait » rétorqua le maître des novices en souriant à l’enfant.

Le voyageur renifla pour attirer l’attention sur lui et adopta un air surpris :

« Vous ne comptez donc pas chercher sa famille ? »

L’air désolé, le cellérier hocha la tête.

« Nous ferons annonce, comme il se doit, sans trop en espérer ni perdre notre temps en vaines chasses . M’est avis qu’elle espérait que nous le prendrions en garde.

— Si fait, ajouta le maître des novices. Ce n’est là ni premier ni dernier. Nous avons l’usage des oblats[^oblat], plusieurs parmi nous le sommes d’ailleurs. »

L’enfant portait son regard alternativement vers les uns et les autres, babillant d’un air gracieux comme s’il trouvait normal de se trouver entouré de vieux messieurs habillés de noir, dans une cave mal éclairée. Le messager lui sourit à son tour, s’attirant quelques gazouillis aimables. Le maître des novices se frotta le visage avant de déclarer :

« Je vais voir comment démêler cet écheveau prestement. Je vais quérir permission du père prieur pour sortir confier l’enfançon à une famille ou l’autre.

— Demandez aux visiteurs usuels de l’hôtellerie, ils sauront bien vous donner utile conseil en ce sens. »

L’affaire était donc entendue, et chacun s’apprêtait à repartir à ses devoirs lorsque le messager s’exclama :

« Attendez, nous ne savons même pas si cet enfant a reçu baptême chrétien, ni quel nom est sien…

— De vrai, acquiesça l’hôtelier. Il faudrait remédier à cela au plus vite, non ? »

Le maître des novices sourit avec candeur.

« Cela sera facile, nous pourrons le porter au bassin ce dimanche, pour la Pentecôte. Plusieurs petits n’ont pu être baptisés pour les Pâques. Un de plus ou de moins…

— Il lui faudra des parents dans la Foi pour le tenir au bassin, riposta le cellérier.

— Nous n’aurons qu’à demander à la nourrice. Elle sera de sûr bien aise de rendre ce service de rien au prieuré. »

L’étranger était resté silencieux tandis que les trois moines traçaient la destinée de l’enfant. D’un doigt, il chatouillait la main qui cherchait à l’agripper. Sans bien réfléchir, il répondit :

« Je serais heureux de le tenir pour son entrée parmi les fidèles.

— Voilà magnifique idée ! s’exclama le maître des novices, dans un jet de postillons. Nous le nommerons donc… Mais quel est votre nom, mon frère ?

— On me nomme Herbelot.

— Eh bien, il en sera de même pour lui, en mémoire de son parrain. La chose est dite ! Grande chance pour lui que vous l’ayez découvert. Un homme de votre rang pour le mener en la maison du Seigneur… »

Le jeune clerc sourit à l’enfant babillant, indifférent au destin qui se scellait pour lui, dans cette cave humide aux relents de moisi. Peut-être que la chance n’y était pour rien, que c’était la main de Dieu qui l’avait guidé là. Dans un nouvel élan d’humanité, il ajouta :

« J’aimerais qu’il soit confié au prieuré comme novice dès qu’il sera en âge, je ferai donation en ce sens, qu’il ne soit une charge pour la communauté.

— *Deo gratias*[^deogratias] s’exclama le frère cellérier, rasséréné par l’idée d’un don à son établissement. ❧

## Notes

Les abandons d’enfants n’étaient pas si fréquents que cela, et se faisaient généralement aux abords d’édifices religieux. Les parents désespérés faisaient confiance à la charité de ces endroits pour garantir à leurs enfants une vie qu’ils ne pouvaient leur assurer. Par ailleurs, l’intégration de jeunes garçons au sein des communautés monastiques était suffisamment importante pour qu’on s’en inquiète. Des recommandations furent donc données par les autorités cléricales pour que cela ne soit plus irréversible. Il demeurait important de s’assurer que la vie monastique était un choix véritable, fait par un adulte, qui devait donc répéter l’engagement pris pour lui quand il était enfant..

Par ailleurs, le baptême, qui marquait l’entrée dans la communauté des Chrétiens était donnée de plus en plus tôt, de façon à s’assurer du salut des enfants morts en bas âge. La mortalité infantile était telle que les parents étaient généralement pressés de voir leur progéniture sous la protection divine. Cela se faisait habituellement avant le troisième anniversaire, lors d’une cérémonie d’importance (généralement Pâques). Cela pouvait se doubler d’une confirmation, vers les sept ans, qui indiquait la reconnaissance et l’acceptation par l’enfant de tous ses devoirs de chrétien. Il commençait en outre à communier vers cet âge là.

## Références

Alexandre-Bidon, Danièle, Lett Didier, *Les enfants au Moyen Âge*, Paris : Hachette, 1997.

Alexandre-Bidon Danièle, Riché Pierre, *L’enfance au Moyen-âge*, Paris :Seuil, 1994.

Anonyme, Collectif (éd.), *Histoire Cronologique du Prieuré de La Charité sur Loire*, La Charité-sur-Loire : Les Amis de la Charité-sur-Loire, 1991.
