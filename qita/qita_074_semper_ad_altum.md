---
date : 2017-12-15
abstract : 1153-1157. Simple sergent d’armes de l’hôtel du roi, Fouques voit son destin bouleversé lorsque le chevalier Arnulf le prend à son service. Mais une autre rencontre lui fera prendre son envol dans les instances dirigeantes du royaume…
characters : Fouques, Arnulf, Ameline, Gace
---

# Semper ad altum

## Ascalon, camp de siège, après-midi du samedi 15 août 1153

Craquements et grincements du bois donnaient l’impression à Arnulf qu’il était à bord d’un navire. Appuyé contre un des bastaings de structure du beffroi d’assaut, il faisait évacuer les hommes, dont certains sautaient carrément depuis la plateforme la moins élevée, négligeant les échelles. La chaleur n’était pas encore perceptible à l’intérieur, protégés qu’ils étaient par les parois de vannerie et de boue, mais certaines flammes s’insinuaient parfois. Autour de lui, au second étage, étaient entassés des tonneaux de flèches que deux sergents balançaient par brassées le plus loin possible. Il fallait tenter de sauver ce qui pouvait l’être.

Arnulf sentait l’approche des fumées épaisses plus qu’il ne les voyait et se frotta le nez. Devant lui, au sol, se répandait l’arsenal qu’ils avaient patiemment amassé avant de faire avancer la tour vers les murailles. La haute plateforme leur donnait un avantage pour contrôler le chemin de ronde de la cité d’Ascalon sur tout un pan de l’enceinte. Mais les défenseurs avaient réussi à embraser le bois, grâce à un quelconque mélange secret de leurs ingénieurs. Arnulf enrageait de n’avoir pu mener l’assaut.

Lorsque les dernières armes furent jetées au loin, il fit signe aux deux hommes de descendre au plus vite. Le plafond au-dessus d’eux se voyait léché de flammèches de plus en plus aventureuses. La chaleur serait bientôt intenable. De là où il était, il surplombait les abris et les tentes, les mantelets mobiles et les bricoles du camp royal. Il espérait qu’il restait assez de bois sur le bateau dont la carcasse gisait sur la plage. Qu’ils pourraient y puiser de nouveaux matériaux.

Équipé de son haubert, casque en tête et bouclier au dos, il dévala lourdement les échelles, retrouva le sol, où la température n’était plus aussi intense. Avançant rapidement parmi les barricades légères, il rejoignit un bosquet de palmiers non loin de là, d’où il avait supervisé l’assaut. Il soupira, s’empara d’une outre qu’on lui tendait et délaça son heaume, avala une longue rasade. Il se rinça le visage, les mains noires.

Un violent incendie grondait désormais au niveau le plus haut de la tour, accroissant encore la sensation de chaleur. Sur sa gauche, une unité de cavalerie et des fantassins lourdement équipés patientaient depuis la zone affectée à la Milice du temple. Le roi et le connétable craignaient que les assiégés ne profitent de l’occasion pour tenter une sortie. Il lança un coup d’œil sur sa droite, vers l’horizon maritime. Les voiles de la flotte latine étaient toujours en place, faseyant mollement dans la légère brise de terre.

Les hommes se hâtaient de récupérer l’armement et de le regrouper à l’abri. Les plus audacieux se risquaient un peu trop près des murailles à son goût. Il grogna quelques ordres à un des soldats à son service, qu’il fasse reculer ces inconscients. Le sergent qui l’avait suivi jusque là sans un mot haussa les épaules.

« Le vent pousse au sud, mon sire. Les mahométans auront le cuir grillé s’ils s’approchent pour flécher nos gens. »

Arnulf dévisagea le jeune homme. C’était un des derniers à avoir quitté le beffroi avec lui. Il l’avait remarqué déjà, pour son application et sa vaillance, qui confinait parfois à la témérité. Plissant les yeux, il tourna le regard de nouveau vers les murailles ennemies. Elles étaient en effet vierges de défenseurs au plus près de la tour, dont le brasier s’intensifiait. Un sourire naquit sur ses lèvres. Il tapa sur l’épaule du jeune sergent :

« Garçon, tu parles de raison. Dieu nous offre l’occasion de leur rendre la politesse. Fais assavoir aux hommes du sire connétable que nous allons tenter de faire de cet échec un bienfait. »

Il fit corner le rassemblement et expliqua en quelques phrases lapidaires son plan : pousser la tour au plus près des murs avant qu’elle ne soit complètement en flammes. Avec le vent du nord qui se levait, l’incendie deviendrait leur allié, éclatant les pierres et écartant les défenseurs. Au moment d’entraîner les hommes avec lui, il fit signe au jeune sergent de s’approcher.

« Quel est nom, garçon ?

— Fouques, mon sire.

— D’où viens-tu ?

— Je suis né à Césaire[^cesaire], sur la côte. Mon père était mesureur de blé[^mesureurble].

— Un enfant du royaume… Tu sers l’hostel du roi depuis longtemps ?

— Cela fait bientôt quatre ans, mon sire. J’ai suivi le sire comte de Jaffa[^amaury] pour ensuite entrer en la sergenterie le roi. J’y ai tout de suite été volontaire pour porter les armes.

— Le royaume a fort besoin de jeunes désireux de servir. Et moi j’aime que mes gens emploient leur tête pour autre chose que tenir leur chapel en place. Tu tu présenteras à ma tente ce soir, après souper. Nous verrons si nous pouvons faire bon mâtin du jeune chiot que tu es. »

Fouques offrit en réponse son sourire le plus éclatant. C’était la première fois que dame Fortune le bénissait. Mais le chevalier se contenta d’un rictus en retour, fronçant les sourcils. La journée était encore loin d’être finie.

## Bethléem, parvis de la basilique de la Nativité, veillée du vendredi 24 décembre 1154

Une vaste foule occupait le parvis, humains et montures. Des lampes dansaient au gré des rafales de vent, ponctuant d’or les ombres bleues tracées par la lune quasiment à son plein. Une courte averse avait mouillé le sol tandis que les fidèles entendaient la messe, mais les nuages avaient rapidement déserté le ciel, poussés vers l’est.

Fouques avait sa place dans l’entourage d’Arnulf, tenant la bride de son destrier. Une large partie de la Haute Cour avait décidé de faire une procession depuis Bethléem jusqu’à Jérusalem. La distance n’était pas si grande, mais avec la cohorte de vieillards et d’enfants, les fréquentes stations pour prier, il était prévu d’y passer la nuit. C’était la première fois que Fouques allait y assister. Il n’était pas particulièrement porté sur la foi et se contentait de suivre le mouvement lorsqu’il y avait des célébrations. Il savait que le jeune roi, Baudoin, aimait tout particulièrement les festivités de Noël, car c’était là qu’il avait affirmé son pouvoir quelques années plus tôt^[Cf. Qit’a *Épineuse couronne*].

De nombreuses filles gravitaient autour du prince, dans l’espoir qu’elles susciteraient des projets maritaux. Las pour leurs familles, si Baudoin avait grand appétit de chairs tendres et dociles, il ne semblait guère pressé de prendre femme. Les épousailles du roi de Jérusalem étaient un programme politique en soi, dont chacun à la Haute Cour estimait qu’il lui revenait le droit de se mêler. Fouques, dont le goût pour les ébats égalait au moins celui de Baudoin, appréciait d’autant plus le spectacle. Il avait beaucoup gagné en assurance depuis qu’il suivait Arnulf en toutes choses, bénéficiait de gages plus conséquents et de dons de vêtements qui lui faisaient l’allure d’un jeune bachelier[^bachelier]. En outre, il arborait habituellement crânement son épée à la hanche.

Depuis le siège d’Ascalon, il avait obtenu le droit de servir exclusivement Arnulf, chevalier du roi, en renfort d’un jeune écuyer, Gace. Celui-ci, un gamin d’à peine douze ans, était absent pour les fêtes, dans sa famille à Antioche. Il logeait désormais dans la demeure d’Arnulf, où il partageait la soupente des valets, mais possédait son propre coffre, avec une clef. Il était fréquent qu’il dorme sur une paillasse devant la porte ou aux pieds du lit où reposait son maître, signe de confiance dont il s’enorgueillissait.

Il n’avait eu l’opportunité de retourner voir ses parents qu’en deux occasions depuis qu’il avait quitté Césaire. Lors de la première, si les retrouvailles avec sa mère avaient été chaleureuses, son père avait à peine décroché quelques mots. Il avait grogné un borborygme de satisfaction en apprenant que son fils servait le roi, mais n’avait pas concédé une parole amène. Fouques n’était resté qu’un court moment et leur avait remis une bourse contenant bien plus que la somme qu’ils lui avaient confiée à son départ. Sa mère avait remercié avec chaleur, mais son père semblait vexé d’un tel geste, sans rendre les pièces pour autant. La seconde fois qu’il avait pu passer les voir, il avait croisé une de ses belles-sœurs, occupée à prodiguer les soins à leur père, bien diminué et alité. Sa mère lui avait fait l’impression d’une folle, le regard perdu et la bouche édentée laissant échapper un filet de salive quand elle parlait, sans rien dire de sensé. Il avait rapidement pris les jambes à son cou, dans l’espoir d’oublier d’où il était issu. Cela faisait désormais deux ans et il y pensait de moins en moins.

Un parfum fleuri s’amusa à chatouiller ses narines et il se tourna, intrigué. Une femme dans une magnifique robe de laine sombre, chaudement emmitouflée dans un manteau de voyage fourré, venait de passer près de lui. Il n’eut le temps que d’en capter le regard intense, la moue moqueuse. Il s’empourpra immédiatement, heureux à l’idée que la nuit cacherait son trouble et piqua du nez tel un domestique respectueux. Il vit qu’elle s’était arrêtée et échangeait quelques mots à voix basse avec un des chanoines qui allaient les accompagner. Tout en parlant, elle semblait l’étudier avec attention, un léger sourire sur les lèvres. Il ne savait plus où se mettre.

La voix d’Arnulf le fit trembler, ce qui amusa fort son maître.

« Eh bien, Fouques, on s’oublie devant les dames ? »

La voix était plus moqueuse que sévère et Fouques avait appris à déchiffrer les subtiles humeurs de l’homme. Il se composa un sourire confus.

« Je ne pensais pas à mal, mon sire… Pardonnez mes manières, je n’ai pas usage de fréquenter damoiselles de la Haute Cour. »

Arnulf s’autorisa un éclat de rire, bref et soudain.

« Te voilà à flatter comme ménestrel ! Elle serait fort aise de s’entendre ainsi appelée à son âge.

— N’est-elle pas jeune fille ?

— J’ai entendu bien des noms pour désigner Ameline d’Abraham[^abraham], mais rarement celui-ci, j’en fais confesse. »

Fouques fronça les sourcils, intrigué. Il étudia discrètement la silhouette qui s’éloignait, dont les ondulations lui semblaient tout à coup fort aguicheuses.

« Elle pourrait être ta mère, de peu. Et le scandale s’attache à elle depuis sa naissance. Elle est fille naturelle du sire châtelain d’Abraham et a manœuvré pour éviter le couvent. Son époux a fini par prendre l’habit, lui aussi, espérant, en pure perte, qu’elle ferait de même. »

Arnulf accorda une bourrade à son domestique.

« On la prétend fort intempérante. Mais elle a quelques appuis à la Cour, dont je ne sais exactement par quels intérêts ils sont mus. Si tu as moyen d’en savoir plus, j’en serais fort aise. »

Fouques hocha la tête. Il ne pouvait s’empêcher de lancer des regards en direction de la femme qui avait désormais pris place sur une splendide monture non loin d’eux. Il n’était qu’un modeste fils cadet d’une famille populaire. Et même si elle était bâtarde et de bien sulfureuse renommée, elle appartenait à un autre monde. Pourtant, il aurait juré qu’elle tournait parfois la tête dans sa direction. La sienne, ou celle d’Arnulf ?

## Jérusalem, demeure d’Ameline d’Abraham, soirée du vendredi 15 juillet 1155

« Arnulf vicomte ! Le vieux Rohard va s’en étrangler de rage ! »

Le rire de gorge d’Ameline se déversa dans la vaste chambre. Elle avait toujours abhorré la clique des anciens qui voulaient décider de son destin. Elle n’avait aucun grief personnel contre l’un ou l’autre, mais les détestait tous en bloc, sans discrimination. Voir l’un quelconque d’entre eux connaître une rebuffade la mettait chaque fois en joie. Elle se tourna vers son jeune amant, blotti contre elle dans les draps.

« Quand donc le roi l’annoncera-t-il ? J’ai grand désir d’être là quand le vieux boira la soupe.

— De certes ces jours-ci. Il y a grande affluence pour l’anniversaire de la prise de la cité. Le roi Baudoin saura en profiter pour le faire assavoir. »

Ameline se releva, offrant son buste à la vue du jeune homme. Sa chemise au large col dévoilait largement sa gorge et une partie de sa poitrine. Elle prit une pose mutine, caressant sa longue tresse. Bien qu’elle jouât à l’enfant, elle démontrait dans ses postures un savoir consommé, apte à maintenir l’intérêt de son compagnon.

« Vas-tu demeurer à son service ? Il aura besoin d’une plus large domesticité !

— Il semble que oui. Il m’a indiqué que je pourrai apprendre bien plus de choses, désormais.

— C’est merveilleux. Avec Arnulf pour te montrer la voie et mes conseils pour ne faire aucun impair, la voie qui s’offre à toi te mènera loin ! »

Fouques se rapprocha d’elle, la peau moite et encore frissonnante de leurs ébats.

« Qu’as-tu donc en tête ?

— Sois bien assuré que des embûches vont s’abattre sur le pauvre Arnulf. Et s’il peut compter sur toi pour l’en dégager, il pourrait faire de toi plus qu’un simple valet.

— Je suis déjà bien aise de ma place !

— Pourquoi te contenter des miettes quand tu pourrais mordre la miche ? N’est-ce pas le vicomte qui désigne le *mathessep*[^mathessep] ? »

Fouques écarquilla les yeux. Il n’avait aucune idée des habiles manœuvres qu’Ameline semblait tracer quotidiennement.

« Il va te falloir y mettre plus d’entrain, jeune homme, le morigéna son amante.

— Mais… que veux-tu que je fasse ?

— N’as-tu pas désir d’étreindre plus qu’il ne t’est assigné ? J’ai toujours enragé de me voir cantonnée à la pénombre alors que d’autres, moins bien pourvus, avançaient en pleine lumière !

— *Dieu dispose*. Je ne suis que fils de blatier, d’autres sont là pour diriger la terre, ainsi qu’il sied. »

De mutine, la voix d’Ameline se fit plus virulente.

« Crois-tu que je saurais m’en contenter ? Tu as bien frais minois et bonnes dispositions. Mais si je t’ai ouvert mes draps, ce n’est pas pour y finir femme de… blatier ! »

Comprenant sa bourde, Fouques se redressa à son tour, caressant les épaules de sa compagne.

« Accorde-moi pardon. Je n’ai guère usage de rêver si haut. Ce n’est qu’à ton contact que j’apprends.

— Il te reste encore beaucoup à apprendre » rétorqua-t-elle, apaisée et faussement agacée.

Pour toute réponse, il ferma ses lèvres d’un long baiser, ses mains commençant à flatter ses courbes. Elle se refusa aux caresses et ne le suivit pas sur le matelas, se redressant d’un bond.

« Il me faut m’apprêter pour le banquet de ce soir, sait-on jamais ! Et toi tu dois te vêtir pour aller prendre ton service. »

Taquine, elle lui lança ses habits ainsi qu’une enfant. Un peu vexé de voir ses avances repoussées, il se plia néanmoins au jeu de bonne grâce. Au moment de lui envoyer sa cotte, elle en caressa l’étoffe.

« Nous regarderons s’il ne se trouve pas meilleure vêture pour toi dans les coffres de mon époux. Il ne me plaît guère de te voir mal habillé le jour où nous triomphons.

— C’est là cadeau de la Noël de sire Arnulf. Il sera surpris de me voir porter autre tenue pour si importante soirée !

— Crois-tu vraiment que ton maître ne sait pas tout de toi, et de nous ? »

Fouques se figea, un frisson peu agréable lui caressant l’échine. Devant sa mine de chien battu, Ameline ne put retenir de s’esclaffer une nouvelle fois.

« Allons, Fouques, un peu d’entrain ! Si ton maître voyait à redire sur ce que tu fais ici, il t’aurait renvoyé dans ton galetas depuis longtemps ! »

Elle se rapprocha de lui et déposa un baiser rapide sur ses lèvres avant de se diriger vers la porte pour appeler une de ses servantes.

« Il a vu comme moi que tu es du bois dont on fait bon chevalier ! »

## Jérusalem, palais royal, début de matinée du mardi 26 mars 1157

Le vicomte  Arnulf dégustait un verre de vin épicé avec quelques oublies, confortablement installé dans une galerie couverte des jardins royaux. Il avait chevauché toute la nuit, s’assurant du calme dans la cité envahie par les pèlerins venus pour Pâques. Il n’avait pas envisagé que sa charge serait aussi exigeante, mais comme c’était un homme scrupuleux, il s’astreignait à patrouiller en personne régulièrement, au moins lors des périodes d’affluence.

Tandis qu’il avalait son repas en silence, il regardait les oiseaux sautiller et pépier autour des valets en train de semer et planter des fleurs et des aromatiques. Il songea un moment qu’il aurait aimé avoir un domaine où ses gens travailleraient la terre pour les nourrir. Il n’était guère heureux de n’être que chevalier de soudée[^fiefdebesant], tout vicomte qu’il était. Jamais il n’avait obtenu la concession d’un fief, fût-il modeste.

Fouques se présenta à lui alors qu’il avalait sa troisième crêpe. Le jeune homme arborait une tenue impeccable et, si ce n’était les éperons dorés qui manquaient, on l’aurait pris volontiers pour un bachelier. Quoi qu’en ait pensé Arnulf au début, sa fréquentation de la bâtarde d’Abraham réussissait plutôt au sergent. Il le salua d’un mouvement de tête et lui indiqua un escabeau à ses côtés.

« Tu vas devoir me représenter ce matin, il nous faut échanger avec les frères de Saint-Jean et les chanoines du Saint-Sépulcre. Une autre pauvresse a été retrouvée alors même que j’achevais ma ronde de nuit. »

Les yeux de Fouques s’élargirent insensiblement. Il n’était pas tant affecté par la découverte d’un corps que par le fait de s’entendre une mission pareille confiée à lui seul. Arnulf poursuivit sans attendre de réponse.

« J’ai importante entrevue avec le sénéchal à laquelle je ne peux surseoir. Mais il nous faut absolument éviter que cela ne sème nouvelle discorde. Ces maudits clercs guettent la moindre occasion de se bestancier. Veille donc à ce que cela n’arrive pas. Outre, il ne faudrait pas que les pérégrins prennent peur ou, pis encore ne s’échauffent les sangs avec cette histoire. *Vilain courroucé est à demi enragé*…

— Nulle famille n’est venue demander justice pour l’heure. Nous ne pouvons rien proposer en la Cour des Bourgeois !

— C’est bien le souci. C’est à la main royale de s’abattre, mais elle ne le fera que si une demande est faite. Sans compter que nous manquons de bras en cette période de l’année. Le murdrier a fort mal choisi son moment. Il aurait voulu cracher à la face du roi qu’il ne s’y serait pas pris autrement ! »

Il avala une gorgée du vin, s’accordant un rictus tellement il était fort en épice et en sucre.

« Le corps a été mené à l’hôpital Saint-Jean, ainsi qu’il sied, et j’ai fait porter message aux chanoines. Tu les retrouveras tous là-bas. Ne tarde pas, tu risquerais de les trouver en train de s’étrangler les uns les autres ! Nous ne pouvons tolérer de pareils désordres dans la Cité, surtout avec l’affluence des Pâques. La France, la Provence, les Flandres, toutes les provinces amies bruisseraient bien vite de malfaisantes rumeurs. Le royaume n’a guère besoin de nouvelles dissensions. »

Il n’eut pas besoin de congédier Fouques que celui-ci se levait, s’inclinait cérémonieusement.

« J’y vais de ce pas, mon sire. Je vous rendrai compte dès l’entrevue achevée. Vous pouvez compter sur moi. »

Arnulf lui accorda un sourire amical, ce qui se résumait à une fente peu expressive chez lui, puis replongea le nez dans sa boisson. Fouques partit alors à grandes enjambées en direction de l’Hôpital de Saint-Jean. Sa poitrine lui semblait prête à éclater tellement il exultait. Il se sentait de taille à affronter ces nouvelles responsabilités, qu’il espérait depuis des mois. Il était bien loin le jeune garçon mal dégrossi qui quittait Césaire le cœur gros. Il jubilait à l’idée qu’il pourrait un jour se présenter à la demeure de ses parents semblable à un seigneur visitant ses paysans. Il hésitait quant au traitement qu’il réserverait à son père : dédain, colère ou pardon ? Il était impatient d’être au soir pour parler de tout cela avec Ameline. Elle serait ravie pour lui, il en était convaincu. Si elle le tançait parfois de ses inconséquences ou de son manque d’ambition, elle s’enthousiasmait encore plus que lui à chacune de ses avancées dans la bonne société. Tandis qu’il traversait la rue du Sépulcre en direction de l’Hôpital de Saint-Jean, il lui semblait entendre tinter des éperons à ses pieds. ❧

## Notes

La mobilité sociale durant les croisades est un phénomène assez difficile à aborder, la documentation lacunaire permettant assez peu de suivre le destin des personnes, fussent-elles de premier plan. On relève malgré tout qu’il existait un certain nombre d’individus dont l’ascension fut assez importante. Il n’est que de constater le parcours de Renaud de Châtillon, dont on connaît mal les ascendants, de petite noblesse apparemment, voire de Guillaume de Tyr dont l’origine bourgeoise semble assez assurée (et que je tiens pour acquise dans mes récits). Pour autant, les élites non cléricales demeuraient étanches.

On peut malgré tout suivre les itinéraires de personnages secondaires à travers les chartes et documents officiels, et en délimiter les stades d’évolution. Au prix parfois de nombreuses interprétations, car les désignations et titulatures peuvent grandement changer d’un rédacteur à l’autre. L’image qui en ressort n’est pas du tout celle d’une société complètement figée. Des familles apparaissent et disparaissent, des noms semblent passer de la catégorie *bourgeois* à l’environnement des officiers royaux dans les chartes. C’est sur cela que je m’appuie pour proposer une vision d’une société féodale extrêmement conservatrice et traditionaliste, mais capable d’assimiler, voire de réformer, les éléments les plus réfractaires aux règles en place. Il ne faut pas oublier que le pouvoir y était partagé par de nombreux groupes, même au sein des unités territoriales cohérentes comme le royaume de Jérusalem et que le consensus, informel, s’établissait en fonction des négociations et tensions toujours actives entre eux.

*Semper ad altum* : « Toujours plus haut. »

## Références

Crawley Charles, *Medieval lands. A prosopography of medieval European noble and royal families*, « Jerusalem nobility » http://fmg.ac/Projects/MedLands/JERUSALEM.htm#_Toc423186882 (  v3.2 du 02 June 2017, consultée le 10/12/2017)

Mayer Hans Eberhard, "Angevins versus Normans: The New Men of King Fulk of Jerusalem", dans *Proceedings of the American Philosophical Society*, vol.133, n°1, Mars 1989, p. 1-25.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972.

Prawer Joshua, *Crusader Institutions*, Oxford University Press, Oxford, New York : 1980.
