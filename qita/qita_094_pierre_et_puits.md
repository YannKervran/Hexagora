---
date : 2019-08-15
abstract : 1099-1157. Malgré ses déficiences, le fils de Sabiha, une prostituée, arrive à trouver au fil des ans une place dans Jérusalem conquise par les Latins.
characters : Said, Sabiha, Nivelet, Moryse, Ernaut, Lambert, père Josce, frère Garin, Chartain, Shihab al-Khwarizmi
---

# La pierre et le puits

## Jérusalem, abords de la rue des Arméniens, après-midi du youm al joumouia 23 jha'ban 492^[vendredi 15 juillet 1099]

Des appels, des bruits de course, des invectives résonnaient parfois dans la ruelle qui menait au petit quartier où résidait Sabiha. Un peu plus tôt, un gamin était passé en criant avec angoisse « Ils sont entrés ! Ils sont entrés ! ». Depuis, un calme relatif régnait. Sabiha n’avait que très rarement eu affaire à des Celtes, ces hommes venus d’au delà les mers. On racontait qu’ils étaient sales, bruyants et violents, barbares et cannibales. Depuis plusieurs saisons, les récits de leurs assauts, de leurs pillages émaillaient toutes les discussions. Et si beaucoup pensaient au départ qu’ils n’étaient qu’un fléau de plus parmi les catastrophes usuelles, la plupart s’effrayaient désormais de les voir approcher depuis les territoires septentrionaux.

Habituée à se débrouiller par elle-même, Sabiha avait prévu  de larges réserves de nourriture.  Au moins pouvait-elle tenir encore plusieurs jours sans ouvrir sa porte, puisant l’eau dans sa petite citerne. Elle possédait même une chèvre qui lui donnait du lait pour son enfant né quelques mois plus tôt. Elle avait rassemblé ses biens les plus précieux, tissus, céramiques et bijoux dans sa chambre et passait le temps à de menus travaux de couture. Elle regrettait juste de n’avoir pu garder avec elle la vieille Safwah. Sa servante avait demandé à fuir plusieurs semaines auparavant, espérant rejoindre le village où un de ses fils était installé. Elle estimait que la campagne serait un plus sûr refuge.

Sabiha avait préféré se fier à la solidité des murailles de la cité. Elle avait mis des années à gagner sa place ici et n’avait nulle part où aller. Ses protecteurs, en particulier le riche Yusuf al-Yama al-Bandaniji, habitaient tous de Jérusalem. Et si elle arrivait à se faire respecter de son voisinage, elle ne se faisait aucune illusion sur le sort qui lui serait réservé si elle partait à l’aventure.

Elle sursauta, se piqua le doigt quand un fracas résonna au bout de la rue : des coups puissants et répétés, certainement sur la grille d’accès. Un temps de silence, puis le vacarme revint, cris et appels, imprécations et hurlements. Les Celtes étaient parvenus à entrer, ils échangeaient bruyamment, peut-être avec le vieil al-Jayhani, le portier dont la voix portait si loin. Terrifié, il implorait leur pitié. Étaient-ils en train de le torturer ? L’avaient-ils frappé de leurs lames ? Parmi le brouhaha, Sabiha crut entendre une autre voix compréhensible. Elle se rapprocha de la fenêtre partiellement occultée de tressage de bois, non sans un regard vers son enfant, tranquillement étendu sur la couche. Il dormait à poings fermés.

Elle aperçut des Latins qui secouaient al-Jayhani. Ils arboraient des tenues de guerre, casque sur la tête et lame ou lance en main. Elle vit aussi un combattant musulman avec eux, le visage tuméfié, ensanglanté et les poignets ligotés. C’était lui qui parlait, échangeant laborieusement avec les soldats autour de lui : une dizaine d’hommes, occupés à frapper sur les portes. On leur intimait à tous de sortir, en présentant les armes qu’ils pourraient détenir.

Elle hésita, son regard faisant des allers et retours entre la petite fenêtre à claustra et son enfant. Elle ne possédait aucune arme et craignait qu’on ne lui dérobe sa maigre fortune amassée au fil des ans. Elle souleva le bébé, glissa rapidement quelques-unes de ses plus belles étoffes sous son matelas. Elle s’enveloppa ensuite dans un large izār assez sobre, dont elle ramena un pan sur son visage en un geste coutumier. Puis elle descendit l’escalier qui menait à la petite cour par laquelle on pénétrait chez elle. Elle quittait la dernière marche lorsque des coups redoublés firent frémir son huis. Malgré son courage, elle ne put retenir un frisson d’angoisse.

Elle s’approcha avec crainte, déverrouilla la serrure et repoussa la barre. Elle n’eut pas le temps de tirer la porte que celle-ci s’ouvrit, révélant un grand gaillard hirsute, sale comme un pou, barbu en diable, son épée brandie, rougie de sang. Il se figea un instant, brailla derrière lui tout en lui ordonnant, d’un geste, de ne pas bouger. Rapidement l’interprète musulman ligoté fut traîné vers eux et le Celte s’adressa à lui dans son dialecte. L’homme traduisit avec lenteur, peinant visiblement à bien comprendre ce qu’on lui demandait.

« Es-tu seule ? As-tu armes ou soldats cachés chez toi ? »

Elle fit non de la tête, rivant ses yeux sombres dans ceux du guerrier.

« Appartiens-tu à riche famille ? Quelqu’un souhaitera-t-il payer rançon pour toi ? »

Sabiha réfléchit un moment, s’efforçant à retenir l’effroi qui la gagnait. Elle n’eut besoin de guère de temps pour prendre sa décision. Après avoir libéré son visage des replis de toile, révélant son profil net et sa chevelure d’ébène dans un geste qu’elle espérait gracieux, elle tendit sa main libre vers le soldat. Elle se forçait à un sourire engageant qu’elle enrichit d’une œillade experte.

« Dis à cet homme que je suis sa servante s’il souhaite fouiller ma demeure. Il aura accès à tout s’il passe ma porte. »

Le prisonnier fronça les sourcils, lui adressa un regard courroucé. Impérieuse, elle lui intima d’un coup de menton de traduire. Pendant ce temps, le grand soldat latin ne la quittait pas des yeux. Il semblait impatient de comprendre ce qu’elle voulait. Quand il entendit, il rugit dans un sourire carnassier et lança une plaisanterie à la cantonade. Puis il poussa Sabiha sans ménagement en direction de sa maison. Il tenait toujours son épée à la main.

## Jérusalem, quartier de Malquisinat, matin du lundi 7 juillet 1113

« Dès ces quelques marchandises vendues, nous te ferons parvenir ce qui te revient. J’ai usage de passer par là plusieurs fois l’an. »

Tout en parlant au jeune homme, Shihab al-Khwarizmi, lointain parent de Sabiha finissait de sangler un petit sac sur la mule où il s’apprêtait à prendre place. Il n’avait découvert que par hasard l’existence de cette branche de la famille et avait saisi la chance d’être présent peu après le décès de sa cousine. D’autorité il s’était fait connaître et avait pris en charge le jeune homme indolent qui n’avait jusque là jamais quitté le giron de sa mère. Celle-ci portée en terre, il ne demeurait personne pour s’occuper du gamin et il errait en ville, dilapidant les quelques avoirs que Sabiha avait accumulés.

Shihab y mit bon ordre et s’empara de tout ce qui avait de la valeur, logeant son petit cousin dans une misérable mansarde. Il lui ouvrit également un compte dans plusieurs commerces de bouche. Il assurait être de retour très prochainement pour mieux installer son parent. Un temps le garçon avait craint qu’on ne l’oblige à quitter sa ville, mais jamais le sujet n’avait été abordé.

Après un dernier au revoir, il prit la direction d’un établissement réputé pour ses beignets. Au milieu des échoppes où cuisaient mille délices, il se laissait bercer par les saveurs grasses et caramélisées propagées par cette chaude journée d’été. Alors qu’il  passait devant une petite taverne où se vendait une piquette vinaigrée que seuls les estomacs les plus solides appréciaient, il entendit un de ses compagnons le héler. C’était Josce Tête Folle, un gamin des rues à l’aspect ébouriffé, qui alternait entre comportement agressif, moqueries sans méchanceté et attitude protectrice envers lui. Il lui adressa un sourire en guise de salut et continua son chemin, mais Josce se leva, son gobelet de terre à la main.

« Viens donc partager lippée, compère !

— Vous ne mangez que vin au matin, mère dit pas bon !

— Eh bien, amène ce qui te sied ! »

Le jeune homme acquiesça et revint avec quelques pâtisseries croustillantes. Les doigts luisants d’huile, le petit groupe se régala alors avec avidité. Un des plus gourmands remercia plusieurs fois en avalant presque sans mâcher. À en juger par sa tenue élimée et son aspect rachitique, il ne se goûtait pas souvent de pareilles friandises.

« As-tu donc cliquailles plein les mains pour t’offrir si belles bouchées ?

— Cousin venu arranger les affaires de mère, a payé pour moi, le temps pour lui de revenir.

— Le grand gars qui te quittait pas ces jours derniers ? Je l’avais jamais vu par nos rues.

— Je le pas connaître. Il voulait aider moi, maintenant que mère… »

Il n’acheva pas sa phrase et engloutit un beignet en silence, le visage soudain éteint. Josce lui asséna une tape amicale à l’épaule.

« Mange, sochon, ça remplit le ventre à défaut du cœur ! »

Puis le petit groupe continua son festin sans l’atmosphère d’allégresse qui s’était installée jusqu’alors.

Autour d’eux, des pèlerins, des visiteurs des autres régions, Samarie, Galilée, Antioche et Édesse, étaient présents pour commémorer la prise de la ville une quinzaine d’années plus tôt. Pourtant l’ambiance était morose : le royaume était assailli de toutes parts et nul grand seigneur ne s’était présenté dans les ports depuis des mois.

« Comment qu’y s’appelle, ton cousin, au fait ?

— Shihab al-Laqit al-Khwarizmi[^khwarizmi].

— Avec un nom pareil, il est pas du coin.

— Non, ici pour commerce. Lui aider. S’occuper de moi.

— Tu vas nous quitter ? »

Les yeux effarés, le jeune homme secoua la tête.

« Oh non. Lui prendre biens de mère, les vendre puis revenir donner moi tout ce dont besoin.

— Il t’a dit ça ?

— Oui. Lui promis faire de moi grand marchand, comme lui. »

Le visage soudain suspicieux, Josce avala une longue rasade de vin. Puis il se leva et, sans un mot d’explication, alla faire un petit tour dans le quartier. À son retour, il paraissait fort mécontent. Il tapa sur l’épaule du jeune homme d’un geste autoritaire.

« Viens avec moi, qu’on te trouve un bon père pour t’aider. Ton cousin, il n’a guère versé pour ton manger. Rien que ces friands ont coûté la moitié de ton avoir ! »

Quand Josce disparut avec son infortuné compagnon, le petit groupe finit le vin et les beignets en commentant la stupidité du pauvre enfant. Couvé comme un coq en pâte par Sabiha depuis sa naissance, il était souvent sujet de moquerie de la part des plus féroces. Personne ne s’étonnait qu’il se soit fait voler à peine sa mère décédée. 

## Jérusalem, quartier de l’hôpital Saint-Jean, matin du lundi 24 juin 1140

Comme chaque jour, l’ouverture des portes de la cité marquait le début véritable de l’activité artisanale et commerçante. Des files d’ânes, de chameaux, de mules déambulaient un peu partout, ondulant entre les groupes de portefaix et les valets chargés de paquets. Pour les établissements religieux, c’était aussi le moment où on attribuait des corvées aux volontaires venus quémander de la nourriture contre un peu de travail. Beaucoup de visages étaient familiers des lieux et c’était comme une grande famille de traîne-misère, qui échouait là, dans l’attente des distributions de vêtements l’hiver ou de tâches au frais l’été.

Le fils de Sabiha était parmi eux, sourire aux lèvres. Habillé d’un vieux thawb de couleur sale, sa barbe et ses cheveux avaient désormais la teinte de la poussière et des rides sillonnaient ses joues minces. Il conservait pourtant cette nonchalance d’enfant, s’émerveillant du soleil, des rencontres, du moindre bienfait dont il était le témoin, voire le destinataire.

Chartain, le valet de l’hôpital qui distribuait les corvées, l’avait au départ pris en grippe, estimant inutile de venir en aide à un mécréant qu’on ne voyait jamais à l’église. Les frères de Saint-Jean le rabrouèrent et lui interdirent de trier la faim d’un homme selon sa foi. Il lui avait alors attribué les travaux les plus salissants ou les plus pénibles, sans jamais obtenir autre chose qu’un consentement indifférent, voire enthousiaste. Chartain avait finalement admis ne voir aucune malice face à lui. Les années passant, il en était venu à considérer le mendiant comme un compagnon, qu’il saluait même comme un ami quand il le croisait en ville.

Au fil du temps, Chartain avait d’ailleurs découvert qu’il n’était pas le seul à s’être laissé gagné par l’affection inconditionnelle de ce misérable hère. Il rendait d’innombrables services contre une piécette, une obole pour un repas, ou, plus souvent encore, en échange d’un cadeau dérisoire. Il vouait un véritable culte à tout ce qui était verroterie et objets de décoration, aimait bricoler ceux qu’on lui donnait, les vendant ensuite à l’encan, à même le sol, sur les placettes.

Il semblait connaître tout le monde, mais saluait en fait tous ceux dont il croisait le regard, pèlerins nouvellement débarqués ou figures d’importance de la cité. Jamais il ne paraissait se lasser ou n’échappait une parole bien longue, ou une remarque désagréable.

Sachant le pauvre homme de toute confiance et peu difficile sur les tâches, Chartain le menait à la chapelle ardente où des dépouilles attendaient le dernier repos. Chaque jour, le grand hôpital accompagnait des croyants jusqu’à leur ultime demeure. Et comme certains demandaient à être ensevelis avec un bijou, un souvenir, une croix, le valet craignait toujours qu’on ne les vole avant de les mettre en terre.

Garin, le frère de Saint-Jean en charge ce jour-là bénit les défunts une nouvelle fois avant de les laisser partir. Chartain se rapprocha de lui, tout en se signant ostensiblement.

« Encore neuf ! Cela n’en finira donc jamais de c’te fièvre quarte !

— Puissent nos prières apporter la paix à ceux-ci, mon fils. Le reste n’est pas en notre pouvoir. »

Chartain hocha la tête, tandis que frère Garin saluait les aides venus emporter les corps.

« Ce sont là toujours les mêmes, n’ont-ils aucune crainte de respirer les miasmes ?

— Y disent que les morts n’ont plus de souffle, enserrés en leur linceul. Et que ça vaut bien le pain de l’abbaye…

— J’ai chaque fois chaud réconfort à découvrir ainsi parmi indigentes-personnes de tels trésors de bonté. *Beati pauperes, quia vestrum est regnum Dei*[^luc6-20]. »

Chartain acquiesça d’un air las. Il aimait bien le frère hospitalier qui se dévouait chaque jour dans la grande salle des malades, mais il ne partageait guère ses vues optimistes sur l’humanité. Lui qui avait connu la vie en dehors de la clôture monastique n’y avait guère rencontré de pitié et de prévenance. En outre, il n’avait aucune notion de Latin et n’avait rien compris de la fin de la phrase.

« J’encrois surtout que certains, comme le vieux, là, y sont un peu simplets. Lui a bien tourné, par chance, mais y peuvent devenir vicieux si on n’y prend garde. Y se passe de drôles de choses en leur caboche.

— Un des anciens ici, que tu n’as pas encontré, disait souventes fois que si le fou jette une pierre dans un puits, mille sages ne pourront l’en tirer. »

Le valet adopta un regard hébété qui fit naître un soupçon d’amusement sur le visage du moine.

« Ce que je veux dire, c’est que nul ne saurait dire quels sont les desseins de Dieu à travers ce simplet, mon fils. »

## Jérusalem, quartier de l’hôpital Saint-Jean, midi du samedi 16 février 1157

Arborant la croix de pèlerin sur leur épaule, Ernaut et Lambert étaient arrivés tout récemment à Jérusalem. Ils n’avaient pas pu résister à l’envie de voir au plus vite la sainte ville dès qu’ils avaient quitté le Falconus avant de faire un long périple vers les lieux de culte périphériques. Ils étaient de retour pour les Pâques, but de leur voyage et objet de leur vœu sacré de marcheur de Dieu. Encore fébriles de mettre leurs pas dans ceux du Christ, des apôtres et de martyrs, ils souriaient presque en permanence, s’émerveillant du moindre détail. Ernaut en particulier semblait intarissable d’admiration.

Ne connaissant personne, ils étaient venus s’adresser à l’hôpital de Saint-Jean pour trouver un hébergement. Lambert préférait loger en un lieu à eux où ils pourraient s’imprégner de l’atmosphère du pays avant de prendre une décision pour leur installation définitive. Ernaut avait vanté les collines de Samarie, où s’étageaient les merveilleux coteaux de vigne dont ils avaient goûté le fruit pressé. Lambert était plus réservé, comme toujours, et ne dévoilait que peu ses intentions. Il douchait régulièrement l’enthousiasme de son cadet en lui rappelant que leur trésor n’était pas infini et qu’il estimait plus raisonnable de rechercher les casaux où l’on faisait les meilleures conditions aux colons.

Ils patientaient en haut du grand escalier qui ouvrait sur le parvis du Saint-Sépulcre, battant la pierre de leurs fines semelles de savates usées. Le portier, un jeune Latin blond aux cheveux frisés, leur avait fait un temps la conversation, mais il avait fort à faire à surveiller les allées et venues. Il leur désigna néanmoins celui qu’ils attendaient. Il avait dit que c’était un guide de confiance qui leur permettrait de trouver un logement à leur goût et adapté à leur bourse.

L’homme, assez âgé, était habillé d’une vieille robe réparée en de nombreux endroits et s’était enveloppé les pieds de chiffons pour se protéger du froid. Il avait sur la tête une chappe de voyage élimée, fixée avec un turban mal ficelé. Autour de ses jambes frétillait un petit chien jaune au poil ras, la queue fendant l’air avec vigueur. Lambert avait espéré qu’on les dirigerait vers un colon ou un ancien pèlerin. Il retint donc de justesse une grimace de dépit quand il vit que le portier de l’hôpital saluait amicalement ce gueux à l’allure misérable. Un sourire sur le visage, leur guide monta à eux et s’inclina poliment, s’exprimant dans leur langue malgré une légère pointe d’accent.

« J’ai nom Saïd. Je peux montrer à vous bon abri, pas cher. J’ai grande joie à dévoiler cité de Dieu à marcheurs des pays lointains. » ❧

## Notes

Même si Hexagora est un projet de découverte de l’altérité de façon générale, à travers le temps et l’espace, j’ai eu aussi envie de creuser un peu le sujet des personnes à l’esprit foncièrement autre, qu’on a désigné de toutes sortes de termes, le plus souvent *folie*. Le rapport à cette partie de l’humanité n’a pas été constant au fil des siècles et si Michel Foucault en a rédigé une histoire à l’âge classique, elle demeure, à ma connaissance, à faire pour les périodes antérieures.

Quoi qu’il en soit, les institutions charitables orientales ou occidentales médiévales paraissent avoir eu une capacité d’accueil qui permettait de canaliser les cas les plus graves et les structures sociales traditionnelles arrivaient souvent à inclure les éléments les plus dociles. Malgré la prévention qu’on pouvait avoir envers les actes de ces personnes différentes du commun, on leur conservait une place au sein de la société, avec plus ou moins de facilité. L’ambivalence du dicton arabe que cite frère Garin me semble une excellente illustration de cette tension, qui pouvait se résoudre par un abandon à une transcendance.

## Références

Foucault Michel, *Histoire de la folie à l’âge classique*, Paris : Gallimard, 1972
