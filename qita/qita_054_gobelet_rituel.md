---
date : 2016-04-15
abstract : 1157. Récemment promu sergent du roi de Jérusalem, Ernaut découvre les légendes et traditions dans lesquelles ses confrères nés en Terre sainte ont baigné depuis leur enfance, ainsi que la complexité de l’administration dans laquelle il vient de s’engager.
characters: Ernaut, Baset, Droart, Bertaud, Brun, Clarembaud, Raoul, Gelta, Père Josce
---

# Gobelet rituel

## Jérusalem, porte de David, matin du lundi 3 juin 1157

La chaleur sèche venue de l’est semblait décidée à s’éterniser dans les monts de Judée, et la nuit n’avait pas apporté la moindre fraîcheur. Hommes et bêtes étaient suants, maculés de poussière, irrités par le sel de leur transpiration, agacés par les mouches. L’ombre des murailles ne suffisait pas à faire oublier l’impitoyable morsure solaire de l’Orient.

Dans un calme relatif, les caravanes pénétraient sous le regard des sergents et les contrôles des officiers de la douane. À côté, comme toujours, des pèlerins avançaient, le dos courbé, mais les yeux levés, un air béat sur leurs visages fatigués. De temps à autre, des messagers apportaient des nouvelles de l’ost royal, en campagne vers Panéas[^paneas] pour l’en délivrer d’un siège mis en place par les Turcs d’Alep.

Parmi les hommes occupés à trier les arrivants et à surveiller d’éventuels fraudeurs, Ernaut demeurait un des rares à faire montre de zèle. Il était encore peu fréquent qu’il ne soit pas affecté à des tâches secondaires et tenait à montrer son sérieux et son engagement à chaque fois qu’il en avait l’occasion. Le moindre paquet, la plus petite sacoche qu’il pensait pouvoir assujettir à une taxe était soumis à une inspection tatillonne. Amusés, plusieurs de ses collègues l’observaient tracasser tous ceux qui passaient à sa portée. Ils échangeaient des regards conspirateurs, impatients de le voir se confronter à un récalcitrant. Mais ce fut l’arrivée d’un trio hétéroclite qui les fit reprendre leur sérieux.

Les trois hommes arboraient des tenues grises d’avoir longtemps marché, peinaient comme s’ils allaient s’effondrer à chaque pas. Leur mise modeste et la légèreté de leurs bagages auraient pu inciter à la mansuétude de la sergenterie. Mais Ernaut n’y discernait nulle croix cousue, pas plus que de besaces à leurs flancs. Ils n’étaient pas pèlerins et pouvaient donc se voir exiger quelques taxes à leur entrée en ville. On ne voyageait pas sans raison, et la première d’entre elles était le commerce, quand bien même il s’agissait de denrées bien discrètes.

« Le bon jour, voyageurs. Avez-vous marchandie soumise à taxes ?

— Nous sommes sergents le comte d’Édesse, porteurs d’importantes nouvelles. »

L’homme qui avait répondu paraissait anxieux, les yeux incapables de se fixer sur un point quelconque. Ses deux comparses arboraient un air contrit, peu soucieux de décoller le regard de leurs savates.

« Je ne vois nulle boîte à ta ceinture[^boitemessage]. Tu as bien quelque document ou sceau à montrer pour que je te laisse passer outre ? »

L’homme sortit un anneau où étaient gravés quelques symboles. Certainement les armes du comté d’Édesse, mais Ernaut n’y avait jamais été confronté. Si l’on évoquait encore le territoire, il n’était plus aux mains des catholiques depuis plusieurs années et la plupart des Courtenay étaient désormais exilés loin de leurs terres quand ils n’étaient pas dans des geôles musulmanes. Inquiet de faire une bévue, il lança un coup d’œil vers ses collègues, malheureusement tous occupés. Il hésitait, se mordit la lèvre, passant une manche sur son front en sueur. Le soi-disant valet avait tout de même un gros paquet au côté.

« Et que portes-tu en ce havresac ? Si tu fais commerce de marchandies, valet ou pas, tu dois t’acquitter des droits. »

L’autre arbora un air de conspirateur et se rapprocha d’Ernaut, un peu gêné.

« Il s’agit du chef de Fils-Foulque le Lépreux... Durement déniché après moult difficultés. Le sire maréchal a grand désir de la récupérer.

— Tu trimballes une tête dans ton baluchon, compère ? Tu te moques de moi ? Mouches et vers en dégorgeraient, vu la chaleur. »

Sa réponse alluma une lueur narquoise dans l’œil du serviteur, qui le toisa alors avec amusement.

« Sire Fils-Foulque est mort voilà bon nombre d’années, tué par Dodequin[^tughtekin]. »

Le valet se rapprocha encore.

« Le farouche turc ne s’est pas contenté de lui trancher le col, il a fait de son crâne son gobelet favori. »

Ernaut fit une grimace à la mention de l’exaction. Tout en parlant, l’autre avait entrouvert son sac, y découvrant un paquet de linge. Il semblait inquiet à l’éventualité de trop en dire ou trop en montrer parmi la foule. Ernaut renâcla à l’idée de contrarier le maréchal, dont le caractère difficile était connu. Il était actuellement absent, au nord avec le roi, mais n’apprécierait certainement pas qu’un jeune sergent récemment promu ait pu gêner l’exécution de ses instructions. Il fit un signe de la main.

« Vous pouvez entrer. Et la bien venue en la sainte Cité ! »

En disant cela, il risqua encore un regard vers Baset et les autres, mais aucun ne semblait lui prêter la moindre attention. Seul Droart était inoccupé, à l’entrée du bâtiment des officiers de la douane, arrachant d’ultimes gorgées à une outre. Il s’en approcha, l’air faussement badin.

« Dis-moi, Droart, as-tu déjà ouï parler du sire Fils-Foulque et de Dodequin ?

— Bien sûr, voilà histoire souventes fois narrée à la veillée, pour bien rappeler la férocité et la duplicité des Turcs. Pourquoi donc t’en soucier ? »

Ernaut haussa les épaules et s’empara de l’outre, sans plus rien en dire, laissant Droart dans l’expectative.

## Jérusalem, palais royal, entrepôts, midi du vendredi 7 juin 1157

La lumière qui entrait par les portes largement ouvertes du cellier dessinait des volutes dans les tourbillons de poussière dansante. Un des intendants, tablette de cire et stylet à la main, inspectait chacune des amphores qu’on lui présentait. Les sergents entreposaient délicatement celles qui pouvaient encore être utilisées sur des banquettes spécialement aménagées, celles à nettoyer dans un appentis extérieur et, enfin, celles qui étaient fissurées ou cassées dans des paniers pour être recyclées ou jetées.

Ernaut trouvait la tâche aisée, lui qui avait grandi en manipulant les encombrants tonneaux de vin chez son père. Il s’étonnait tout de même de se voir affecté à de tels travaux. Le service royal lui apparaissait soudain bien moins prestigieux que dans ses rêves. Au fin fond des méandres du palais, dans un bâtiment sombre et malodorant où s’entassaient des céramiques de stockage de toutes tailles. Droart, responsable du tri, lui avait expliqué qu’ils appartenaient avant tout à la domesticité, et se trouvaient au services du sénéchal avant celui du maréchal. Ils maniaient donc plus souvent les approvisionnements que les armes.

« Je n’aurais cru qu’on puisse tenir tant de pots en celliers, roi ou pas, souffla Ernaut en s’abattant sur une banquette.

— Il faut bien mener les denrées depuis casals et chastiaux. Entre huile et vin, il y a bon usage de tous ces vaisseaux, lui répondit Droart, tout en lui tendant la miche apportée par un des gamins de cuisine.

— Grand dépit qu’il n’y ait nul fleuve pour acheminer barrels.

— Et de quel bois les ferais-tu ? Ici on a plus de terre à pots que d’arbres. »

Ernaut opina en silence et déchira avec appétit le pain qu’il trempa dans la purée froide aux oignons. Il appréciait les repas au palais, sans jamais avoir réalisé ce que cela impliquait en terme de denrées à conserver. Il découvrait les gigantesques entrepôts nécessaires à alimenter une population aussi nombreuse.

Bertaud, un des scribes, pénétra soudain dans le local, l’air désemparé. Il avait le visage rouge d’avoir couru, lui habituellement si calme.

« Il est là, le nouveau ? »

Puis, l’apercevant, il se dirigea vers Ernaut, plissant les yeux pour y voir dans la pénombre après la clarté de la cour.

« C’est toi qui a vu passer le crâne du sire Fils-Foulque ?

— Voilà quelques jours, à la porte de David, oui-da.

— Tu es acertainé que c’était bien la coupe de Dodequin ?

— Le valet me l’a affirmé, mais je ne saurais dire...

— Il faut t’en assurer ! On ne retrouve plus le paquet ! Si le maréchal apprend cela, il est capable de nous en faire percer le poing ! Surtout à toi, tu es le dernier à l’avoir vu. »

Droart, surpris par la véhémence des propos, s’approcha et secoua la tête.

« Comme tu y vas ! Il n’y a pas larcin !

— Tu iras le dire au sire Joscelin, on verra bien s’il te laisse ta langue... »

Ernaut commençait à s’inquiéter, angoissé à l’idée d’avoir gaffé si tôt après son embauche. Il se leva, frottant sa cotte pour l’en débarrasser des miettes et de la poussière.

« Que puis-je faire pour remédier à cela ?

— Il te faut mettre la main sur ce maudit vase !

— Je ne sais même pas...

— Va donc voir Brun, de la Secrète[^grandsecrete]. C’est un mien ami. Il saura si on aurait pu celer le gobelet en le trésor. »

Hochant le menton avec énergie, Ernaut s’efforça de prendre un air plus assuré qu’il n’était vraiment. Puis il s’élança vers l’aile du bâtiment abritant les services fiscaux. Il ne savait pas qui était ce Brun, mais questionna tous les clercs qu’il croisait, traversant les vastes salles de compte, les escaliers aux marches usées, les petits réduits d’archives, pour finalement dénicher l’administrateur dans une alcôve d’une large pièce voûtée d’arêtes. Il travaillait sur un lutrin à la lumière du jour, recopiant des tablettes de cire posées sur un plateau à ses côtés. Les traits lourds, la panse rebondie, entre deux âges, il manifestait l’entrain d’un animal mené à l’abattoir et la vigueur d’un centenaire. Mais sa main traçait les symboles avec légèreté, sans trembler un instant lorsque la grosse voix d’Ernaut résonna dans le tranquille endroit.

« Mestre Brun, je viens de la part de Bertaud, au sujet de la coupe... »

Des sourcils froncés avec vigueur et une moue horrifiée le stoppèrent net dans sa déclaration. Le clerc susurra alors :

« Si c’est la fameuse coupe à laquelle je pense, il serait bon de ne pas hurler comme pâtre en herbage.

— De vrai, reprit Ernaut plus doucement. Auriez-vous quelques nouvelles à son propos ?

— J’ai bien vérifié les inventaires, elle n’a certes pas été posée au trésor. Je ne l’ai vue consignée nulle part. »

Devant l’air catastrophé d’Ernaut, il ajouta d’un ton se voulant engageant :

« Mais j’y ai pourpensé : si ces valets sont bien venus en le castel, après si long voyage, il est fort probable qu’ils ont quémandé écuelle et gorgées de vin. Tu devrais t’enquérir aux cuisines. »

## Jérusalem, palais royal, cuisines, début d’après-midi du vendredi 7 juin 1157

Pour rejoindre l’aile où étaient installés les foyers et ateliers de préparation des repas, Ernaut dut refaire tout le tour des bâtiments, craignant de devoir traverser les salles principales. Il y avait toujours le risque de tomber sur un haut baron dans la grande aula ou aux environs des chambres princières et royales. Il n’était pas naïf au point de penser qu’il pourrait passer inaperçu tandis qu’il parcourrait les lieux l’air empressé et le souffle court.

Ce fut Clarembaud, un des coqs responsables, qui l’interpella alors même qu’il venait à peine de poser un pied dans l’endroit. Quelle que soit l’heure du jour, et parfois de la nuit, l’activité était telle que le moindre intrus était immédiatement détecté et généralement vertement tancé. Cette fois ne fit pas exception, Ernaut gênait le passage de plusieurs valets trimballant des paniers emplis de poissons que des marmitons s’empressaient de vider, riant de la puanteur des entrailles qu’ils entassaient sur l’immense table.

 Ernaut s’efforça de montrer sa bonne foi en se disant à la recherche des hommes du comté d’Édesse, se prétendant mandé en cela par le vicomte. Il était peu probable que personne n’ose jamais en demander vérification auprès de ce dernier. La mention d’Arnulf calma un peu les ardeurs de Clarembaud qui réfléchit un petit moment, tout en distribuant quelques tâches et remarques bien senties autour de lui comme s’il n’y prêtait pas attention.

« Je crois avoir souvenance de ces trois drôles. Maigres comme belette et gloutons comme goupils. Ils m’auraient dévoré dîner d’évêque si je les avais écoutés. Mais ils sont partis, de ce que j’en sais. Ils avaient à faire avec le sire maréchal et sont allés tout droit demander après lui.

— Mais il est parti depuis un moment. Ont-ils pris la route pour le trouver ?

— Et comment j’le saurais, crâne de puce ? Va donc voir auprès de son hostel. »

Ernaut s’en figea de surprise. Aller ainsi se faire connaître auprès des serviteurs de la famille Courtenay ne lui semblait guère judicieux. Voyant son indécision, Clarembaud s’agita un peu, frappant la table devant lui de l’épaisse louche tenant lieu de sceptre en cette salle.

« Sinon y’aura bien un des jeunes là-haut qui saura quoi. Reste pas là, j’ai l’impression d’avoir une grosse vache perdue au parmi de mes cuisines. Va donc demander à Raoul, le petit scribe, y devrait pas te faire peur çui-là ! »

Ernaut voyait à peine de qui Clarembaud voulait parler, mais la mention d’une petite personne l’incitait à la confiance. On rencontrait plusieurs apprentis dans les salles notariales et de compte, porteurs de messages et de tablettes, aussi discrets que des souris et à peu près aussi vaillants.

Après s’être fait refouler de la grande pièce de l’échiquier, il apprit que le gamin était aux archives. Régulièrement, les sacs de cuir contenant chartes et documents anciens et importants étaient inspectés pour vérifier leur bon état. On en profitait pour nettoyer et balayer les lieux de fond en comble, à la recherche de la moindre crotte de rongeur pouvant indiquer une future infestation. Une armée de chats était dorlotée pour prévenir toute invasion, mais cela demeurait, avec les lampes, et donc le feu, la principale angoisse des archivistes.

Raoul s’avéra n’être plus un gamin et, installé derrière une table à tréteaux dans une salle aux murs éclatants de blancheur, il supervisait le dépoussiérage des sacs avant d’en vérifier le bon état. Dans un coin de la pièce, une pile soigneusement façonnée était disposée sur une banquette carrelée de terre cuite.

« Je me souviens fort bien de ces trois hommes. »

Il baissa d’un ton et se rapprocha d’Ernaut.

« Ils ont réussi à remettre la main sur le chef du Fils-Foulque, dérobé aux héritiers de Dodequin.

— Ils te l’ont confié ?

— Certes pas ! C’est là coupe sertie de pierreries, d’or et d’argent. Ne le sais-tu pas ?

— Je n’en avais jamais entendu parler. »

Raoul tira Ernaut par le bras jusque vers la porte donnant sur un petit corridor, s’exprimant à voix basse tout en surveillant ses subordonnés.

« Fils-Foulque était grand baron d’Antioche, au courage sans pareil malgré la lèpre qui le rongeait. Il connaissait fort bien les Mahométans qu’il affrontait sans relâche. Capturé par Gazzi en même temps que le prince Roger, il a été vendu ensuite à son plus farouche adversaire, Dodequin. »

Raoul marqua une pause, comme hésitant à continuer, puis reprit.

« C’était là un Turc de fort sinistre mémoire. Il aimait à se baigner dans le sang de ses ennemis vaincus, et on le disait soûlard sans limites. Il a tranché la tête de Fils-Foulque de son sabre et en a fait son gobelet préféré pour s’enivrer. »

Ernaut fit une moue dégoûtée, encouragé en cela par un hochement de tête de Raoul.

« Récupérer sa tête permettra de l’inhumer chrétiennement.

— Crois-tu qu’ils l’ont déjà mise en terre ?

— Certes pas. Ils avaient désir de voir le maréchal, mais tout son hostel est parti avec lui. Et ils ne vont pas s’aventurer au-dehors avec pareil trésor.

— Ils sont encore au palais ?

— Je leur ai dit de voir avec un des hospitaliers du chambellan, moi je ne réside pas ici. »

Ernaut était bon pour se rendre dans la cour de l’hôpital, où on accueillait les visiteurs que Gauvain de la Roche se devait de gérer, avec leur maisonnée. Gauvain régnait sur un empire de valets, domestiques et pages divers, pour l’hôtel du roi, à l’exclusion des services administratifs généraux, des finances ou de la guerre. Il avait également la haute main sur l’organisation des couchages et des hébergements.

## Jérusalem, palais royal, hôtellerie, après-midi du vendredi 7 juin 1157

Peu désireux de se confronter aux responsables les plus importants de la maison royale, Ernaut avait dirigé ses pas vers le lavoir où les lingères travaillaient le plus souvent. Il y a avait tellement de drap et de toiles à nettoyer ou repriser qu’il s’en trouvait toujours quelques-unes à battre l’étoffe ou à tirer l’aiguille. Une autre de leurs activités favorites était la propagation des rumeurs, cancans et potins. L’arrivée de ce jeune homme au physique impressionnant déclencha des sourires entendus et des regards égrillards. On n’accordait que peu de vertu à la plupart de ces femmes, qui en jouaient donc.

Il n’eut pas plus tôt posé le pied sous l’auvent que crépita une voix de crécelle au débit rapide :

« Y’a pas de miel ici, l’ours, tourne tes pas ! »

La saillie déclencha de nombreux rires, mais Ernaut, bien qu’empourpré, ne recula pas. L’agresseur ne devait mesurer guère plus de cinq pieds et se tordait le cou, aussi maigre que le reste, pour défier Ernaut du regard.

« Je cherche trois valets du sire comte d’Édesse qui font héberge au palais.

— Et vois-tu l’un d’eux ici ? Crois-tu qu’on le cache sous nos robes ?

— Je me suis dit que vous sauriez peut-être où je pourrais les trouver. »

La commère lança à la cantonade, sans lâcher son aiguille et son fil :

« Et v’là qu’y nous prend pour les clercs du chambellan !

— Allons, Gelta, effraie pas ce pauvre poussin ! »

La lingère haussa ses maigres épaules sans désarmer.

« Et y f’ra quoi pour moi si je réponds à sa question ? J’y gagne quoi ?

— Je pourrai vous aider à tordre les draps !

— Ah, voilà qui sonne comme lai de damoiseau amoureux à mes vieilles oreilles... Avec tes bras de Samson, t’auras guère de peine. Tu t’en viendras pour les grandes lessives nous porter assistance, je compte dessus.

— J’en fais serment !, confirma Ernaut avec espoir.

— Y sont partis se désoiffer chez le père Josce. Le chambellan aime guère voir valets traîner savates sans trimer alors y s’sont esquivé au matin. »

Ernaut se rua vers la cour en levant la main pour remercier, sans s’apercevoir des rires qui saluèrent son départ.

## Jérusalem, taverne du père Josce, fin d’après-midi du vendredi 7 juin 1157

La taverne fit l’effet d’une douche froide sur les épaules d’Ernaut, qui avait quasiment couru depuis le palais. Essoufflé, transpirant, il parcourait du regard la vaste salle au pilier, faiblement éclairée par les étroites fenêtres. Quelques hommes se reposaient là, soldats entre deux corvées, valets se cachant d’un maître trop exigeant. Mais en journée on n’y voyait que peu de monde. Les gens venaient chercher leur vin et emportaient leurs pichets sans s’attarder. Ce n’était qu’en soirée que parfois la sergenterie du palais s’acoquinait avec celle des grandes maisons, bavardant et faisant rouler les dés, écoutant à l’occasion un jongleur de passage.

Ernaut reconnut immédiatement les trois hommes, dont l’aspect semblait moins miteux. Ils étaient installés autour d’un tabouret où deux d’entre eux déplaçaient des méreaux sur un quadrillage. Il s’approcha d’un pas décidé, attira le regard de celui qui ne jouait pas.

« Le bon jour, j’ai couru après vous depuis un moment !

— Y a-t-il souci, sergent ?

— Il se trouve que l’hostel le roi aimerait bien assavoir où se balade le chef du sire Fils-Foulque ! »

L’autre baissa d’un ton, l’air affolé, tandis que ses deux acolytes levaient la tête, interloqués.

« Nul besoin de crier ! Il demeure à mon flanc, comme il est depuis nombre de jours. Il y demeurera jusqu’à ce que je le remette au sire maréchal !

— Il serait mieux gardé au castel.

— J’en crois rien. Qui pourrait savoir qu’il est là ? En dehors d’une poignée, dont tu es... »

Il se recula d’un pas, appréciant la stature impressionnante du jeune homme.

« T’as pas idée de me le dérober j’espère ? Tu viens le prendre ?

— Nenni, je suis féal servant le roi. »

L’autre paraissait à demi rassuré, jetant un coup d’œil autour de lui. Ses deux compagnons s’étaient levés. Puis il souffla, posa une main amicale sur l’épaule d’Ernaut.

« De vrai, tu sembles bon garçon, tout respire droiture et franchise chez toi. »

Il l’invita d’un geste à s’asseoir avec eux et lui remplit un gobelet.

« Aurais-tu désir de le voir ? Il n’y a pas grand monde, c’est l’occasion. »

Ernaut n’hésita qu’un instant. Chez lui la curiosité l’emportait généralement sur tout. Il posa son verre et avança la main vers le sac. Le valet lui tendit une boule d’étoffe sale, l’invitant à dégager la coupe avec délicatesse.

Et il découvrit dans les linges une poupée de bois et de chiffon grossièrement peinte de couleurs vives, au visage hilare.

« Qu’est-ce... »

Les trois hommes éclatèrent de rire.

« C’est toi qu’a la vache, mon gars, désormais. Et tu devras payer la tournée à tous, ce soir, comme le béjaune que tu es ! »

Les rires redoublèrent et le père Josce se joignit à eux quand on lui montra le jouet. Ernaut secouait la tête, ne sachant quoi penser. Ce fut le tavernier qui lui expliqua.

« C’est la tradition dans la sergenterie, garçon. On fait courir le béjaune nouvellement arrivé après la Vache, et quand il la trouve, il doit payer godets à tous les anciens. Puis il se charge de la passer à celui qui arrive après lui !

— J’ai eu la Vache à la Noël, moi. Ils m’ont fait croire que c’était les langes de l’Enfant Jésus lui-même » confirma le valet conspirateur.

Ernaut soupesait le jouet dans ses énormes mains, ne sachant s’il devait sourire ou s’emporter. Puis il porta un verre de vin à ses lèvres après avoir adressé un toast silencieux à son tourmenteur de la journée, pensant « Une coupe dans un crâne de chevalier, mais quel crétin je suis ! » ❧

## Notes

Le bizutage est une pratique connue dès le XIIe siècle dans les universités européennes, où les « béjaunes » sont soumis à toutes sortes d’épreuves ou de vexations avant de devoir être mis à l’amende en abreuvant leurs aînés. Avec le temps, les débordements furent de plus en plus fréquents bien que la coutume demeurât vivace jusqu’à notre époque, pour donner ce qu’on appelle désormais des Week-End d’Intégration. On n’a aucune source attestant cet usage au sein d’autres confréries qu’étudiantes/cléricales à cette période, mais il me semblait que cela ne serait pas incongru dans une administration en partie militaire. Dans l’armée, la recherche de la clef du champ de tir par les nouvelles recrues était devenue un classique au siècle dernier.

La légende qui fait courir Ernaut est basée sur des récits autour de la disparition de Robert Fitz Fulk, seigneur de Zerdana (dans la principauté d’Antioche), capturé lors de la fameuse bataille des Champs de Sang qui vit la mort de Roger de Salerne, prince d’Antioche en 1119. Robert a vraisemblablement été livré par Il-Ghazi à Tughtekin, seigneur de Damas, avec lequel il entretenait selon Usamah ibn Munqidh des relations plus ou moins amicales. Mais celui-ci, après l’avoir décapité, aurait fait réaliser une coupe avec son crâne. Comme on prête énormément de vices au dirigeant damascène ainsi qu’une cruauté sans égale, il est difficile de se prononcer sur le sérieux de ces allégations. Mais de telles pratiques sont attestées parmi les populations nomades de la steppe eurasienne, comme la coupe fabriquée par le khan petchénègue Kurya avec le crâne de Sviatoslav Igorevitch de Kiev peu avant l’an mille.

Enfin, le terme utilisé pour désigner le jouet m’a été inspiré par une anecdote familiale du début du XXe siècle que m’a rapporté mon grand-père, lorsqu’on cherchait à interdire l’usage des langues régionales. Dans son école primaire, si un enfant parlait breton, on lui donnait la « Vache », morceau de bois avec une ficelle et qu’il s’efforçait à son tour de refiler à celui qu’il entendait parler breton après lui. L’enfant qui restait avec la Vache le vendredi se voyait puni, contraint de conjuguer « ne pas parler breton » à tous les temps, toutes les personnes, durant son week-end. Autre pratique vexatoire d’intégration, forcée, celle-là…

## Références

Asbridge Thomas, *The creation of the Principality of Antioch 1098-1130*, Rochester: The Boydell Press, 2000.

Asbridge Thomas, Edgington Susan B., *Walter the Chancellor’s The Antiochene Wars*, Aldershot : Ashgate Publishing Limited, 1999, p.159-163.

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008, p.131-2.
