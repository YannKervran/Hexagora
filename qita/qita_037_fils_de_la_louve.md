---
date: 2014-12-15
abstract: 1151-1157. Le quotidien d’un soldat n’est pas que de batailles l’arme au point et celui qui sert le basileus Manuel voyage beaucoup. Stamatès découvre ainsi les franges du grand territoire, depuis les confins gelés d’Anatolie jusqu’à la cité sainte entre toutes, Jérusalem. Vétéran des combats, intrépide, il a pleinement conscience de faire partie d’un puissant empire, celui auprès duquel se jaugent tous les petits royaumes barbares.
characters: Stamatès Mpratos, Nicéphore, Arcadius Zonaras, Ernaut, Katarodon, Nicéphore, Zeheb
---

# Fils de la louve

## Rive de la Save, fin de matinée du mercredi 21 novembre 1151

Les toiles de l’armée byzantine avaient rongé le paysage le long de la Save[^save], engloutissant un petit village et la dérisoire forteresse de rondins censée le protéger. La fumée des feux de camp traçait des colonnes grises jusqu’à la voûte laiteuse du ciel. L’humidité du matin avait reflué en même temps que la brume, mais le froid maintenait son emprise, marquant par endroit le sol de gelée blanche. Tout ce qui pouvait brûler avait été découpé, arraché et livré aux flammes sur un vaste périmètre. Les fourrageurs devaient désormais s’aventurer à plusieurs lieues pour trouver de quoi réchauffer les articulations craquantes des soldats.

À perte de vue, la plaine s’étendait, les reliefs lointains mangés dans les lambeaux de brouillard qui persistaient dans les creux des champs et des pâtures. Des hameaux parsemaient le paysage, reliés par de maigres sentiers bordés de haies et de murets, séparés de bosquets aux branches nues. Une large bande de terre boueuse coupait au travers, sombre ruban qui s’attachait aux basques de l’armée byzantine.

Un peu au nord du cantonnement, vers l’entrée orientale, des soldats avaient réquisitionné des canots et des pirogues pour tenter de pêcher de quoi améliorer l’ordinaire. Près de là, assis sur un moignon de mur dans ce qui devait être à l’origine un enclos à bétail, un cavalier s’employait à réchauffer de la graisse qu’il appliquait avec soin sur sa selle. Le cheveu hirsute coincé sous un bonnet cylindrique de feutre, emmitouflé dans un épais kaftan en peau de mouton, il soufflait dans ses mains tout en regardant les apprentis pêcheurs s’escrimer sans guère de matériel ni plus de succès. Un de ses compagnons, à la démarche pesante, se rapprocha de lui, ses cuirs sous le bras. Il le gratifia d’une tape amicale sur l’épaule et s’installa à son tour.

« On devrait traverser d’ici à deux jours au plus tard. L’empereur a grand désir de fustiger ces maudits Huns[^hun].

— Je goûterai fort d’être en selle, à profiter de la chaleur de ma monture. »

L’autre acquiesça, tout en démontant les sangles et étrivières de sa selle.

« Au moins ici ils auront compris ce qu’il en coûte de se rebeller.

— Le crois-tu ? Le pansebaste sebaste[^sebaste] Kantakouzenos y a laissé la main, à ce qu’on dit.

— Mais l’empereur a occis lui-même leur grand chef, Bakchinos.

— Ce n’était que le meneur des Huns, et Manuel a décidé de les poursuivre de sa colère. »

Le premier cavalier posa son chiffon et se frotta les mains sous les aisselles afin de les réchauffer tout en se tournant vers son compagnon.

« Tu sais, Stamatès, j’ai pas mal guerroyé toutes ces années, et j’commence à me demander si ça sert à quelque chose, tout ça. »

Son compagnon étira les lèvres en un sourire gercé.

« Par l’archange Michel, que t’arrive-t-il ? T’as la lance qui s’amollit avec les ans ?

— J’suis sérieux, l’âge me gagne et je fatigue. J’ai porté le fer partout où le basileus[^basileus] le souhaitait, et j’le ferai jusqu’à mon dernier souffle. Pourtant je me demande si ça aura une fin.

— Tu penses au gamin, c’est ça ? »

L’autre ne répondit pas, les lèvres soudainement pincées, reniflant tout en fixant les remous et les tourbillons de la Save. Il avait perdu son aîné quelques mois plus tôt, alors qu’il s’entraînait à cheval pour intégrer à son tour l’armée impériale. Stamatès souleva sa pesante carcasse et s’étira lentement, s’ébrouant et grognant comme un ours. Il s’approcha de son ami et lui confia doucement :

« La Polis[^polis] fait tellement d’envieux. Nos richesses, notre pouvoir, notre puissance sèment la faim dans les cœurs. Qui ne rêve pas d’être Romain ?

— Les morts. Eux ne rêvent plus. »

## Pélagonie, soirée du lundi 10 janvier 1155

La neige tombée récemment s’était mélangée à la boue et à la poussière ; la plaine ne comportait désormais plus que des traînées grises, aux reflets bruns. Seuls quelques toits conservaient la mémoire des flocons blancs. Devant la modeste cité de Pélagonie[^pelagonie], une ville de toiles de tente avait pris place, entourée de barrières, d’épieux et de fossés. Des feux scintillaient dans des fosses, apportant chaleur et lumière en cette fin de journée.

Tout au long des méandres des voies qui permettaient d’approcher de l’enceinte, des gardes contrôlaient, inspectaient ceux qui se présentaient. En sa qualité de cavalier vétéran, Stamatès avait hérité de la responsabilité de la porte de la ville, avec une poignée de fantassins à encadrer. Bien qu’il soit parfaitement conscient qu’il ne s’agissait là que d’une tâche annexe, jugée servile par beaucoup, il mettait un point d’honneur à paraître sous son meilleur jour. Il était amené à croiser beaucoup de hauts dignitaires, des fonctionnaires impériaux et des officiers qui allaient et venaient au service du basileus, installé dans la forteresse.

Il avait brossé sa barbe fournie et ses cheveux ondulés et arborait, par-dessus son klibanion[^klibanion], un court manteau de laine rouge qu’il avait acheté à prix d’or avant de partir de Byzance. Une large écharpe verte, de bel aspect, pendait sous son baudrier d’arme. En outre, il veillait à ce que ses hautes bottes de cavalier demeurent propres, malgré la boue et la neige.

Sa fonction avait par ailleurs un énorme avantage : il dormait dans une des tours de la ville, dans une petite salle qu’il partageait avec ses hommes et où se trouvait une partie de l’arsenal, conservé dans des coffres scellés aux murs. Le matelas était de paille, mais au moins n’était-elle pas humide, voire pourrie, comme pour bon nombre de ses compatriotes condamnés à dormir sous toile.

La journée tirait à sa fin et il allait être temps de clore les portes pour la nuit. Stamatès déambulait sous la grande arche, soucieux de vérifier qu’aucun groupe ne se présentait pour entrer, depuis le camp, ou pour sortir, depuis les rues adjacentes. Il n’en vit surgir que son vieux compagnon de combat Nicéphore, au petit trot. D’un mouvement sec du poignet, celui-ci arrêta sa monture et se pencha, son haleine alourdie de vin traçant de la buée dans la fraîcheur de l’air vespéral.

« Le basileus a fait arrêter son cousin » lança-t-il tout à trac.

Stamatès fronça les sourcils. Il avait vu arriver en début d’après-midi Andronic Comnène en grand appareil, tel le fier duc de Branicevo et Belgrade qu’il était. L’homme traînait une réputation sulfureuse derrière lui mais sa vaillance, sa force physique, avaient toujours attiré l’admiration des soldats.

« Que s’est-il donc passé ?

— Il semblerait que Manuel ait épuisé sa patience avec son cher cousin. Déjà que son aventure avec la fille d’Alexis l’avait mise à mal.

— On ne démet pas duc comme Andronic pour histoires de coucheries.

— Il a ridiculisé le neveu du basileus et le pansebaste sebaste Jean avec son histoire d’évasion. »

Le souvenir de l’anecdote amena un sourire sur les lèvres de Stamatès, et un hochement de tête de désapprobation de la part de Nicéphore.

« Ris pas, on n’humilie pas ainsi des dignitaires de leur rang.

— Que ne sont-ils pas aussi braves et habiles que lui ?

— Plaire aux soldats ne lui sera d’aucun secours. Et s’il est enferré, c’est parce qu’on l’accuse de vouloir s’allier aux Huns. Il avait déjà été envoyé ici, loin de la Polis, pour ne pas succomber aux tentations des intrigues de palais. Il descend du grand Alexis[^alexiscomnene], comme le basileus, et pourrait avoir des prétentions dangereuses.

— Que va-t-il devenir ?

— Il repart pour la Mère des cités, mais chargé de fers et non plus d’honneurs, pour y méditer dans une geôle impériale. »

Nicéphore salua son ami et relança son cheval au trot en direction du camp. La cathédrale se mit à sonner les vêpres. Stamatès huma l’air froid, tapa des pieds puis donna l’ordre de barrer les portes pour la nuit. Soufflant dans ses mains gelées, il tourna son regard vers la forteresse dont les ouvertures s’ornaient de lumières vacillantes. Machinalement, il lissa son foulard vert, heureux de n’avoir pas à naviguer parmi les puissants et les dignitaires. Au moins pouvait-il compter sur quelques amis.

## Venise, après-midi du mardi 31 mai 1155

Assis sur le pont de la galée, sous la voile déployée en tente, Stamatès regardait l’ondée s’abattre sur les eaux de la lagune, sous une voûte aussi grise que les vagues. Les bâtiments eux-mêmes semblaient avoir perdu toute couleur, mangés par la brume. Un des marins lui avait expliqué qu’il n’y avait que trois ciels possibles à Venise : le brouillard, la pluie ou le brouillard *et* la pluie.

En tout cas, la traversée de l’Adriatique s’était déroulée sans trop de mal malgré une mer agitée et un temps maussade tout du long. L’humeur des hommes s’en ressentait et certains grognaient qu’il aurait mieux valu se fier aux conditions météorologiques pour leur départ qu’aux auspices astrologiques. De son point de vue, Stamatès trouvait la situation assez enviable. Accompagner le sebaste Michel Paléologue à Venise pour une mission diplomatique revenait pour lui à dormir et manger dans un endroit tranquille, sans risquer de finir dans la boue.

Il avait espéré pouvoir visiter la cité lacustre, mais n’en avait guère eu le temps. À peine étaient-ils arrivés que Stamatès était désigné pour repartir bientôt, comme escorte à un *mandâtor*[^mandator], Arcadius Zonaras, à bord d’un des navires les plus rapides. Le basileus attendait la réponse avec impatience, disait-on. Ses émissaires étaient là pour tenter de contrer les visées expansionnistes des rois barbares de Sicile, qui infligeaient de cuisantes défaites depuis des années aux troupes impériales. La vanité sans borne et l’opportunisme des Siciliens entravaient la politique outremer, en obligeant l’armée et la flotte à se montrer sans cesse sur le qui-vive sur ses frontières occidentales.

Un sifflement lui fit tourner la tête. Sur le quai, Katarodon lui faisait de grands signes, l’invitant à descendre à terre. N’ayant aucun supérieur à qui rendre compte en l’absence du diplomate, Stamatès franchit la passerelle en quelques enjambées.

« Quelles nouvelles du palais ?

— On doit aller retrouver le mandâtor plus tard, ils tiennent banquet. Puis on monte à bord pour mettre au large demain. »

Stamatès avisa le large sourire de son compagnon.

« Les nouvelles sont bonnes on dirait.

— J’en sais trop rien. Je crois oui, tout le monde a l’air de joyeuse humeur. Mais c’est pas pour ça que je m’enjoie. »

Stamatès pencha la tête, intrigué. Il ne connaissait guère Katarodon, jeune recrue appartenant à une famille aisée de Byzance. Il avait à peine vingt ans mais se trouvait déjà là où le vétéran avait mis plus d’une dizaine d’années à arriver, après de nombreuses campagnes. En outre, ses tenues d’apparat généralement bleues et blanches trahissaient son appartenance à la haute société ou, du moins, son désir de s’y intégrer.

« J’ai eu bonne fortune d’encontrer un lointain cousin à moi, Loukas, un marchand. Il est ici pour négoce, assez bien installé. Il propose de nous inviter à souper. »

Stamatès saliva rien qu’à l’idée de manger autre chose que des biscuits de mer, du poisson salé et du gruau insipide. Aucune chance néanmoins de pouvoir déguster du kavourmas[^kavourmas] comme sa famille le préparait à Radolibos[^radolibos]. La perspective était pourtant engageante et son amitié naissante avec Katarodon lui permettait d’espérer pouvoir se faire des relations qui aideraient à améliorer sa condition. Il aimait son statut de soldat, mais pour devenir officier, il savait que la vaillance et la compétence ne suffiraient pas. Des appuis politiques et des amis bien placés étaient indispensables. Il ajusta la capuche de son manteau et étendit un bras pour flatter l’épaule de Katarodon.

« Au moins aurons-nous bonne chère avant de mettre à la rame. Hâtons-nous d’aller dévorer avant de retrouver le mandâtor.

— Le soleil est encore haut, ami. Nous avons même temps d’aller flâner près des rues des boutiquiers. J’aimerais fort trouver de ces bijoux francs qu’on dit si beaux, décorés d’émail brillant. »

Stamatès échappa un rire gras.

« Quelque jeunette à séduire en la Polis ?

— Si seulement ! Mes parents ont décidé de m’unir à gamine dont la famille pourrait grandement nous aider. Elle n’a pas dix ans et il me faudra attendre pour profiter de cette union. Mais cela n’empêche nullement de leur témoigner de mon respect par quelques cadeaux. Je sais qu’ils apprécient fort ces bijoux, si chers à Byzance.

— Voilà judicieuse idée, jeune. Allons donc dénicher cette précieuse babiole puis nous lamperons quelques goulées de bon nectar en une taverne, pour choisir lequel porter à ton cousin. »

Il s’esclaffa, tout en tirant son manteau sur ses épaules tandis que la pluie redoublait. Ils avançaient dans les flaques claquant sous leurs semelles, indifférents aux visages fatigués des domestiques affairés, bavardant bruyamment, plaisantant l’un de l’autre. Heureux comme des enfants, ils goûtaient le plaisir d’être l’élite du monde d’alors, s’amusant de leur voyage dans les confins barbares des territoires. Il étaient des Romains de la Polis.

## Abords de Sivas, fin d’après-midi du lundi 12 décembre 1155

Le verglas faisait craquer l’herbe sous les pieds des chevaux et la glace dévorait les eaux du Kizilirmak, saupoudrant les bras morts d’un givre pulvérulent. La vallée s’élargissait peu à peu, en direction de l’est, et bientôt ils allaient déboucher sur la vaste esplanade où la route depuis Charsianon juqu’à Mélitène croisait, dans la cité de Sivas, celle qui traversait le plateau depuis le nord. Le froid habillait de fumée les paroles et chacun économisait son souffle.

Le responsable de l’intendance avait demandé à ce qu’on leur apporte au plus vite un mouton, afin de préparer le souper pour les hommes et l’ambassadeur. Seulement, malgré le temps, un pâtre avait sorti les ouailles du village où ils avaient fait halte, histoire d’épargner au maximum les réserves de foin.

Stamatès avait été désigné pour accompagner un de leurs escorteurs, Zeheb, auprès du paysan pour lui acheter une bête. Stamatès appréciait le Turc car il employait sans trop de difficulté le grec, suffisamment en tout cas pour parler de tout et de rien, comme on le faisait pour s’occuper lors des longs trajets. Pas très grand, il avait un œil fou à demi aveugle qui le faisait ressembler à un démon. Son nez fin, cassé à de multiples reprises, son sourire en partie édenté et son épaisse toison brune aux minces nattes ornées de brins de tissu colorés travestissaient en fait un esprit allègre, un sens de l’absurde et de l’humour, quoique parfois très rustique, qui ne manquait jamais de surprendre les Byzantins.

Il rappelait à Stamatès un compagnon de combat qu’il avait fréquenté tandis qu’il guerroyait dans les septentrions de l’empire, un auxiliaire venu peut-être des mêmes confins que Zeheb. Ce dernier arborait une impressionnante masse de métal, d’un seul bloc d’acier, à tête de félin, très semblable à celle qu’il voyait présentement à la ceinture de son guide. Ils avançaient au petit trot en direction d’un relief derrière lequel le berger devait emmener les bêtes, à l’opposé du fleuve, sur les contreforts des pentes qui les enserraient depuis qu’ils avaient pris la route.

Un soleil blanc sans chaleur se faisait manger peu à peu par les crêtes, laissant les doigts d’ombre progresser dans les creux et les bosquets. Stamatès frissonna et rentra le menton dans son vêtement doublé de mouton. Zeheb se racla la gorge et désigna le troupeau qui cheminait vers eux, à une poignée de portées d’arc.

« Tu prendras garde aux chiens. Gros et pas faciles, déclara-t-il sans ambages. Les gros trucs jaunes. Pas méchants mais méfiants. »

Stamatès comprit alors que ce qu’il avait pris pour des moutons de loin était en fait de gros molosses de couleur similaire, à la gueule noire. Leurs cous puissants s’ornaient de colliers cloutés et leur démarche pesante indiquait des bêtes lourdes, résistantes, vigoureuses.

« Ça peut te tuer un loup d’un claquement de mâchoire et te broyer la jambe pareil. Plus méchant, ça ferait des bons guerriers. »

Il agrémenta sa remarque d’un large sourire connaisseur puis renifla, s’essuyant le nez de sa manche. Les ténèbres bleutées commençaient à l’emporter sur les lueurs orangées des pentes méridionales. Le bruit des cloches sourdes, les bêlements leur parvinrent peu à peu.

L’affaire fut rondement menée, sous le contrôle scrupuleux des mâtins qui allaient et venaient autour des ovins, obéissant aux sifflements de leur maître. Trois grosses bêtes dont la placidité rendait la puissance encore plus menaçante. Zeheb proposa d’égorger leur achat non loin de là, pour se faciliter le retour, et il accomplit le geste machinalement, sans même demander de l’aide. Immobilisant le mouton entre ses jambes, presque assis dessus, il lui tira la tête en arrière et dégaina un long couteau. Avant qu’elle n’ait eu le temps de comprendre, la bête était morte, son sang s’écoulant à gros bouillons dans les broussailles. Zeheb sourit, tout en maintenant le cou.

« Tu entends pester l’autre ? Il dit on va attirer les loups avec sang. Il a pourtant fait payer assez cher pour qu’on en fasse à notre volonté. »

Ils lièrent la dépouille à l’arrière de sa monture, guère effrayée par l’odeur du sang puis repartirent prestement, pressés de retrouver la chaleur des bâtiments où les attendaient un bon feu.

« À Sivas, je te mènerai chez une vieille qui prépare un des plats d’ici. Avec foie et oignons[^tjvjik]. »

Il fit claquer ses lèvres en un bruit de succion.

« Dommage ton chef nous laisse pas prendre du vin du malik. Ça irait bien avec, pour y tremper leur lavash[^lavash]…

— Je crains que ton malik n’apprécie pas qu’on boive son vin, non plus.

— Ça, c’est sûr ! Et si ton chef se montre sans cadeau, vous devoir retourner chez vous sans tête. »

La réponse se voulait sans malice, et Stamatès ne s’en offusqua pas. Il savait que leur venue auprès du prince de Sivas, Yagi Basan, était dangereuse. Mais le basileus avait besoin de l’aide des dirigeants danishmendides[^danishmendide] pour occuper le sultan Qilig Arslan, qui menaçait sans cesse ses possessions anatoliennes. Convaincre l’adversaire de son ennemi qu’on était son allié demandait en général beaucoup de présents. Il rit de bon cœur, conscient que l’amitié entre soldats n’empêchait nullement de tirer le sabre ou l’épée pour décapiter celui avec qui on avait trinqué la veille. Il espérait ne pas avoir à défendre sa peau contre Zeheb mais savait qu’il pourrait lui trancher la tête si son devoir l’exigeait. Sans haine, mais sans regret.

## Jérusalem, soirée du jeudi 1er août 1157

Stamatès revenait tranquillement du mont du Temple, qu’il avait enfin eu l’occasion d’aller visiter. C’était la première fois que ses pas le menaient à Jérusalem et il ne voulait pas manquer de voir de ses yeux les lieux saints. Il était loin d’être dévot, mais la curiosité s’ajoutant à sa maigre foi, il avait insisté auprès du mandâtor pour obtenir congé de temps à autre. De toute façon, ils étaient en sécurité dans la cité. Le roi Baudoin avait toujours manifesté de grands égards pour le basileus et on disait même qu’il s’était récemment brouillé avec le prince Renaud d’Antioche, qui posait tant de problèmes aux Byzantins.

Dans le ciel, de la pourpre et de l’ocre se déversaient dans l’azur jusque là invaincu de la journée et l’activité des rues changeait. Les tavernes débordaient plus largement, les commerces et les artisans fermaient les uns après les autres. La zone qu’il venait de franchir ne présentait pratiquement que des arcades closes et des volets tirés. Comme l’odeur métallique l’indiquait, cette partie de la rue du Temple était investie par les bouchers, ainsi qu’en attestaient les traces de sang, les abats que se disputaient des chiens errants.

Déambulant le nez en l’air, il s’amusait de ce que les Celtes pensaient que cette petite ville était le centre du monde. Byzance, la Polis, comme il l’appelait, la Mère des Cités, était d’une taille bien plus considérable. Elle s’étendait sur les deux rives du Bosphore et abritait des palais et des monuments autrement plus impressionnants, s’ils n’étaient pas aussi saints.

« Hé, le Grec ! »

L’appel le tira de ses rêveries et il aperçut le jeune colosse avec qui il avait sympathisé, depuis quelques jours. Heureux de rencontrer un visage ami, il s’approcha, un sourire aux lèvres.

« Es-tu là à effectuer mission pour ton sire ?

— Non, je acheter manger.

— Que te dirait d’aller plutôt verser bonne goulées de vin en gosier ? Je cherchais justement compère pour aller à la taverne. »

Le Byzantin acquiesça et mit ses pas dans ceux d’Ernaut, en direction des rues où étaient installées la plupart des échoppes de nourriture toute prête. Ils arrivèrent bientôt devant l’arcade qui desservait le lieu de boisson espéré.

Descendant quelques marches, ils se trouvèrent dans une longue salle où étaient stockés les fûts, le long d’un mur. Plusieurs lampes à huile éclairaient la zone d’une lumière dansante. En entrant, Ernaut attrapa le chat qui était venu se frotter à ses jambes, amenant l’animal à ronronner bruyamment.

« Ho là, maître Garnier ! Y a-t-il mesures de vin pour gosiers empoussierés ? »

Le tenancier, grand gaillard au dos voûté, les rejoignit depuis la cour où il discutait avec quelques acheteurs.

« Le bon soir, ami Ernaut ! Tu viens quérir un pichet de Galilée ?

— Pas ce soir, fais-nous donc savourer ton Bezek. Mon ami qui est là vient de Byzance, il a droit au meilleur. »

Le tavernier glissa un sourire et attrapa un pichet qu’il remplit auprès d’un tonneau, avant de le tendre aux deux hommes, empochant les piécettes données par Ernaut.

« Si tu veux t’installer dans la cour, y’a de la place. »

Sans lâcher le chat, le jeune homme s’empara du pichet et alla s’asseoir sur un des bancs, à côté de voyageurs qui parlaient un dialecte italien, peut-être des Pisans. Stamatès s’installa face à lui puis ils se mirent en devoir de taster le nectar qui leur avait été servi.

« Tu sais quand vous repartez, compère ? demanda Ernaut au bout d’un moment.

— Non. Je ne sais pas pourquoi être venus. Juste accompagner mandâtor.

— Il y a force voyageurs qui viennent ces temps, j’aurais jamais cru que même des mahométans allaient et venaient ici, porteurs de sauf-conduits. »

Stamatès haussa les épaules.

« Affaires de puissants, pas pour nous…

— Moi je serais roi de Jérusalem, j’aurais vite fait d’assaillir villes ennemies et d’instaurer la paix du Christ comme il sied à bon monarque.

— Pas si simple, s’amusa Stamatès. Toujours compliqué faire choses.

— Il suffit d’une bonne armée et de savoir la mener ! »

Stamatès délaça la petite masse à tête de bronze qu’il arborait toujours à sa ceinture et la posa sur la table.

« C’est comme ça, être basileus, ou roi, ou baron.

— Que veux-tu dire, l’ami ?

— Arme puissante et semble aisée, habile à fracasser têtes et bras… »

Il avala une lampée de vin, l’air mystérieux puis continua, se penchant en avant, soudain espiègle.

« Mais très difficile bien user, car coup doit être subtil. Jamais simple de toucher et toujours grand risque à bien manier. »

Ernaut fronça les sourcils, penchant la tête.

« Je ne vois pas le rapport.

— Être pareil. Bien manier force n’est jamais juste taper fort. Habile est celui qui sait l’employer juste. »

Il tourna la poignée de la masse vers Ernaut, l’invitant à la prendre en main.

« Cadeau pour toi, compère Ernaut. Pour souvenir leçons d’un Romain. Force n’est rien sans habileté. »

Le jeune homme écarquilla les yeux, surpris du présent, mais touché du geste. Il fit voler la petite tête de bronze dans l’air. On lui avait dit que les Grecs étaient bizarres, hautains et précieux. Il lui semblait pourtant qu’il faudrait de peu qu’il y ait une entente réelle entre leurs deux peuples. Pour peu qu’il y ait plus de gens comme Stamatès. ❧

## Notes

Byzance au milieu du XIIe siècle est une puissance incontournable au Moyen-Orient, mais il ne faut pas oublier qu’elle entretient de nombreux liens avec l’Europe. Une partie de ce qui est désormais l’Italie, la Sicile, était d’anciennes provinces byzantines.

De nombreux territoires des Balkans étaient pleinement intégrés à l’empire, ou sous son contrôle en titre. Malgré sa formidable puissance, le basileus était souvent contraint de mener des campagnes sur plusieurs fronts à la fois, maritimes et terrestres. La diplomatie requérait donc une grande finesse et une subtilité qui donnaient une image assez trouble de l’attitude byzantine dans l’échiquier des forces en présence.

On ne compte plus les allusions à la duplicité grecque dans les sources latines. Bien que les croisades aient été lancées à l’origine pour venir en aide à ce frère chrétien oriental, l’arrivée massive de contingents latins qui se taillèrent leurs propres royaumes peut, a contrario, être perçue comme une trahison à leur endroit, ou du moins une déloyauté. Le basileus alors à la tête de l’Empire byzantin, Manuel Comnène, était néanmoins partisan de l’entente avec les territoires latins d’outremer, et multiplia les gestes de bonne volonté, malgré quelques manœuvres diplomatiques parfois contrariantes. Son ambition, très compréhensible, était de demeurer la puissance référente de la région.

## Références

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Lilie Ralph-Johannes, (trad. Par Morris J. C., Ridings Jean E.), *Byzantium and the Crusader States, 1096-1204*, Oxford : Clarendom Press, 1993.

Magoulias Harry J. (trad), *O City of Byzantium: Annals of Niketas Choniates*, Détroit : Wayne State University, 1984.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.
