---
date: 2014-09-15
abstract: 1157. Peu après l’arrivée du Falconus à Gibelet, une silhouette décharnée erre dans les massifs libanais, à la recherche de son destin. Frappé du sceau de l’infamie, saura-t-elle trouver une rédemption ?
characters: Ugolino, Amaury de Sidon, Boutoum, Gibran, Josef, Massina, Pakâlin, Sayed
---

# Par delà les ombres

## Monts du Liban, mardi 10 décembre 1157

Le vent descendu des cimes hurlait sur les reliefs, crachant des embruns de neige sale dans un grondement rageur. Saturant l’atmosphère de grésil immaculé, le fin blizzard griffait les yeux, mordait la peau exposée, ployait les arbres, brisait les branches. Sur un sentier malingre, parmi les caillasses roulantes s’avançait pourtant une silhouette. Une main décharnée s’employait à retenir une couverture mitée. Les pieds à demi nus, amas de poussière, de plaies et de gerçures, butaient à chaque pas. Rien ne pouvait les faire renoncer.

Le marcheur s’arrêtait régulièrement, ajustait sa dérisoire protection face aux assauts furieux des éléments et reprenait sa route. Squelette maintenu debout par sa simple volonté, il ne sentait plus la faim ni le froid, la soif ni la douleur. Il n’avait qu’un but : atteindre les montagnes où se tenait celui qui le libérerait, l’homme qui avait les réponses. Il en avait entendu parler alors qu’il s’éloignait de la côte, un jour où il tentait de mendier, camouflant les flétrissures qui suppuraient encore sur son visage.

Des fers portés au rouge avaient été apposés sur ses joues et son front, arrachant dans la douleur ses derniers lambeaux de raison. Des passants s’entretenaient d’un homme, dans la montagne, qui avait trouvé la solution à tous les maux de ce monde. Ils l’évoquaient avec révérence, comme s’ils craignaient de le voir apparaître au coin de la rue, descendu de sa cache rocailleuse. Le cœur soudain empli d’allégresse, il n’avait pas attendu qu’on le chasse une nouvelle fois, sous les lazzis des enfants et les pierres des parents. Une fois, même, on avait suggéré de le pendre sans plus tarder. Ils ne voyaient de lui que les signes de son infamie, sans plus reconnaître dans le corps décharné un frère, un compagnon affamé, malade, blessé. Un humain.

Il avait alors pris la route, chapardant dans les champs et les jardins à la nuit tombée, avançant sans relâche en direction des monts où la délivrance l’attendait. Un jeune berger, au cœur plus tendre, l’avait hébergé un temps. Il l’avait remercié en lui volant sa couverture, qui le protégeait désormais des assauts de la tempête. Il lui fallait continuer, atteindre le prochain col, dépasser le relief suivant, jusqu’à le trouver. Le vieux de la Montagne.

## Monts du Liban, mercredi 18 décembre 1157

Sayed referma la porte du pied, les bras encombrés de bûches. Il les lâcha en un tas désordonné dans un fracas d’écroulement qui amusa les deux bambins assis près des flammes. Inspirant bruyamment tandis qu’il se relevait en faisant craquer son dos, il se dépêcha d’aller mettre la barre à l’huis et assujettit un boudin de paille à sa base.

« Le vieux Gibran dit que ça va durer encore trois jours, le vent a tourné par-delà le Barouk[^barouk]. C’est jamais bon qu’y dit. »

Sa femme, occupée à touiller un pot où fumait un épais gruau hocha la tête sans un mot. Il fouilla dans un repli de son vêtement et en sortit un morceau de markouk[^markouk] enveloppant un peu de viande fumée.

« Tiens, Josef a donné ça aussi, pour l’inconnu. Il veut pas qu’on soit seuls à devoir le nourrir.

— Tu as accepté ? Ils croient que je sais pas prévoir ? Qu’on est des mendiants ?

— Non, on nous a confié sa garde mais les anciens ont décidé que c’était le village qui l’accueillait, et c’est à tous de le nourrir. »

Elle pinça les lèvres et acquiesça de mauvaise grâce. Puis prit la viande. Au fond, cela la soulageait. Ils n’étaient pas si riches qu’ils pouvaient refuser cette aide au plus fort de l’hiver. Ce n’était pas que l’inconnu mangeait tant que ça, il avalait à peine quelques cuillers de kichk[^kichk] chaque jour. Mais la vie était rude pour une famille modeste comme la sienne. Elle s’arrangerait pour que la viande serve à ses enfants avant tout.

Sayed s’assit près du feu et se frotta les mains. Il épousseta les traces de neige fondue qui subsistaient sur ses épaules. Fascinés par leur père, les deux petits le regardaient, les yeux écarquillés. Dehors, le vent sifflait sur les toits, faisait vibrer les planches des fenêtres étanchées de torons d’herbe sèche. Son épouse, Massina, avait pris place à ses côtés après avoir posé un pichet de sahlab[^sahlab] entre eux. Il s’en servit une tasse qu’il se mit à boire avec lenteur, le regard tourné vers l’obscurité du fond de la salle, vers la paillasse aménagée pour leur visiteur. Les enfants avaient repris leurs jeux, remuant et poussant des jouets de bois grossiers tout en arpentant leurs contrées imaginaires. Massina reprisait une toile, chantonnant une de leurs chansons favorites.

Blottis autour du feu, ils étaient tranquilles, à l’abri. Si la tempête les séparait du monde extérieur, elle les en protégeait aussi. Nul brigand, nul seigneur pillard n’osait s’aventurer dehors par un temps pareil. Seuls les fous le faisaient, comme cet homme dont Sayed avait la garde. Un de ces Celtes, venus de par-delà les mers, le visage dévasté de brûlures infamantes.

Sayed n’aimait guère avoir affaire à eux. En fait, il n’aimait avoir affaire à personne. Surveiller son maigre troupeau de chèvres dans la montagne ou sarcler ses cultures, voilà ce qui le contentait. Il se sentait libre, dans ces moments-là, et se surprenait à fredonner la même chanson que Massina, qu’ils avaient tant hurlé, enfants, dans les monts environnants. Il regarda ses mains tavelées, crevassées. Il n’avait pas trente ans et déjà son dos se voûtait. Sa belle Massina ne souriait plus que de quelques dents. Et il y voyait de moins en moins bien d’un œil, depuis quelque mois, quand les fièvres avaient bien failli l’emporter en même temps que leur fille cadette.

La vie n’était pas tendre pour les montagnards comme eux, et il n’avait pas besoin qu’un Celte à l’esprit dérangé s’ajoute à cela. Dès la tempête terminée, il proposerait qu’on le laisse à son destin. Un de ceux qui comprenaient leur langue disait que ce fou parlait d’un vieux scheik. C’était peut-être un sage, un ermite qui aspirait à la sainteté dans l’abnégation, l’oubli de soi. Sayed secoua la tête, dépité. Il méprisait ces hommes égoïstes, qui cherchaient leur bonheur seuls, sans se soucier des autres. Lui avait une famille à sa charge, il se comportait comme il fallait. Et ce n’était pas un vagabond de passage qui mettrait tous ses efforts en danger. Certainement pas.

## Monts du Liban, vendredi 20 décembre 1157

Le grincement de la porte forcée par une soudaine bourrasque réveilla Sayed en sursaut. La blancheur de la neige qui commençait à s’accumuler à l’entrée l’éblouit lorsqu’il ouvrit les paupières. En un instant, il fut hors du lit, se ruant vers la table où il empoigna un solide couteau, à manche épais. Le froid mordait ses jambes nues. Il avança néanmoins jusqu’à l’huis et risqua un coup d’oeil au-dehors.

L’aube naissait à peine et le village, noyé de neige, dormait encore paisiblement. Aucune maison n’avait allumé le feu, personne n’était en vue. C’est là qu’il aperçut les traces. Légères, en partie effacées, s’éloignant de sa demeure. L’angoisse étreignit son cœur et il se retourna, affolé. Le renfoncement qu’il partageait avec Massina était calme, il distinguait la silhouette de sa femme allongée, endormie. À leurs pieds, les matelas des enfants, également paisibles.

Il resserra sa prise sur le couteau et s’approcha de l’endroit où le Celte se reposait. Il n’y avait plus rien, l’homme était parti, avec la couverture. Sayed grogna, soulagé. Si l’inconnu avait envie de mourir, ça ne le concernait pas. Il était malgré tout contrarié de voir qu’il s’était fait voler, et en demanderait réparation au conseil du village. Apaisé, il lâcha la lame sur la table, tout en allant refermer la porte. Le froid commençait à le gagner et il frissonna quand ses pieds effleurèrent la neige. Il bloqua la barre et ajusta le boudin de paille, soupirant une fois cela fait. Il n’avait jamais aimé le vagabond.

« Sayed ? Que se passe-t-il ? s’inquiéta Massina dans son dos.

— Rien. Le Celte s’en est allé.

— Tu l’as laissé faire ? Il va mourir !

— Il est parti depuis un moment, ses traces dans la neige sont à peine visibles.

— Il faut prévenir les autres, aller le chercher. »

Il revint à sa couche sans un mot, et en retrouva la chaleur avec un frissonnement de plaisir. Il se frotta le visage.

« Il nous a volé la couverture et peut-être plus. Crois-tu qu’il vaille la peine qu’on se risque là dehors en cette saison ? »

Massina ne répondit rien, le visage plongé dans les ténèbres. Elle lâcha un soupir.

« C’était serpent malfaisant, soyons heureux qu’il ne nous ait pas fait grand mal. Que ses pas le mènent au loin, ça me va. »

Tirant la couverture sur ses épaules, il se rapprocha de sa femme dont la chaleur le réconfortait.

« Pour l’heure, le jour est à peine levé, il n’est pas temps. Dormons. »

## Monts du Liban, vendredi 3 janvier 1158

La petite troupe cheminait en file indienne sur l’étroite sente serpentant à flanc de falaise. Le temps était froid, mais, le blizzard retombé, ils tentaient enfin de retourner chez eux. Du moins le lieu qu’ils nommaient ainsi pour cet hiver, un abri sous roche qu’ils avaient fermé par des murs de pierres sèches, du torchis et des branchages.

Ils ne restaient jamais plus d’une saison ou deux au même endroit, craignant qu’un potentat local ne vienne les en déloger l’épée à la main. Ils n’étaient pas si nombreux et guère vaillants lorsqu’il fallait s’affronter à une troupe entraînée et bien équipée. Pakâlin, leur chef, était un déserteur de l’armée damascène, jeté sur les routes quand ses maîtres avaient perdu la ville. C’était le seul qui sache vraiment se battre, à cheval comme à pied. Et il tirait à l’arc avec talent.

Malgré tout, là, il ressemblait à un loqueteux, emmitouflé dans des hardes pour se protéger du froid, menant leur dernier cheval. Il ne restait que son casque d’acier, à la peinture fort écaillée, pour attester de son statut. Ils n’étaient qu’une dizaine, avec un maigre butin. Partis fourrager quelques jours plus tôt, ils n’avaient guère pu extorquer aux paysans misérables du coin.

L’hiver était rude pour tous, et on ne pouvait prendre ce que les gens n’avaient pas. Ils avaient déjà mangé plusieurs de leurs montures au fil des semaines. Le groupe stoppa soudain. Pakâlin avait levé le poing et semblait intrigué. Un de ses lieutenants, Boutoum, un solide anatolien aux mains puissantes, s’avança, tirant sur son foulard pour parler.

« Un souci, chef ? »

Le Turc ne répondit pas, les yeux fixés devant lui. Une maigre silhouette se tenait là, un squelette décharné à peine protégé de lambeaux sales, le visage brûlé par le froid, couvert de boue. Pakâlin dégaina doucement son sabre, tendant les rênes de son cheval à Boutoum.

« Qui es-tu ? Que fais-tu là ? »

L’homme ne bougea pas, se contentant de tourner la tête vers le vide de la vallée en contrebas.

« Pousse-toi de mon chemin ! »

Pakâlin s’avança, tout en se dépêtrant de ses vêtements chauds afin d’être plus à l’aise pour frapper.

« Tu m’as pas compris, l’homme ? »

Il se retourna vers sa troupe :

« Nous aurions bien besoin d’un nouvel esclave, qu’en dites-vous ? Ça m’éviterait de devoir le tuer pour qu’on passe ! »

Quelques hochements de têtes et rires nerveux approuvèrent. Le Turc fit encore un pas et se figea quand il entendit la voix forte :

« Es-tu le Vieux ?

— Quoi ?

— Es-tu le Vieux de la Montagne ? Celui qui apaise les cœurs ? »

Pakâlin s’esclaffa.

« Ouais, de juste, c’est moi. Viens me servir, vieux débris, et tu connaîtras le bonheur ! »

Le visage décharné s’éclaira un instant puis se déforma sous l’emprise de la colère, de la haine. Sa voix se brisa lorsqu’il hurla :

« Tu mens ! Tu n’es pas… »

Il se tut lorsqu’il vit le brigand s’avancer, le sabre au clair, le visage déterminé, il hésita un instant et cria :

« Tu n’as pas le droit, tu ne sais pas qui je suis ! Je suis le gên… »

Son cri se perdit dans un grondement gigantesque, accompagné d’un tremblement qui alla en s’amplifiant. Tous les hommes se jetèrent au sol ou y tombèrent sans le vouloir. Le cheval rua subitement, affolé, arrachant les rênes des mains de Boutoum. Il roulait des yeux paniqués tandis qu’il sentait la terre se dérober sous ses sabots. Il sautait, tentant de se détacher de ce roc fuyant, traître, se cabrant et frappant dans le vide. Jusqu’à trouver le dos de Pakâlin.

Le Turc n’eut pas le temps de réagir qu’il volait déjà dans le profond précipice, bientôt rejoint par la pauvre bête assassine de son maître. Un hurlement de terreur fut la dernière chose que les hommes de Pakâlin entendirent de lui. Terrés la face contre le sol, épouvantés, aucun n’osait bouger. Il fallut plusieurs minutes après la fin des secousses pour que Boutoum se relève. Il fit quelques pas hésitants, ramassa le sabre demeuré dans la poussière. La silhouette était toujours là, désormais agenouillée. Le brigand s’interrogeait, sa lame tremblait tandis qu’il la brandissait vers l’inconnu.

« Par l’épée de saint Michel, mais t’es qui toi ? »

## Sidon, mardi 16 septembre 1158

L’évêque Amaury faisait les cent pas, visiblement ennuyé. L’archevêque était à ses côtés, assis sur un des petits sièges pliants disposés là pour qu’ils profitent de la douceur du midi. Dans le cloître ombragé, le vent léger faisait frissonner les arbustes et les buissons taillés, mêlant les fragrances des fleurs aux effluves marines. Face à eux, un clerc boiteux au visage triste attendait qu’on le questionne plus avant, semblant indifférent à sa tâche, les yeux dans le vague.

L’évêque reprit :

« Il n’a donc rien dit ? Tu ne sais même pas son nom ?

— J’en suis désolé, mon sire, il n’a même pas paru me voir. D’aucuns le nomment al Djinn, le démon, ou le génie. Mais lui ne dit rien. Il était là, assis à même le sol, à marmonner, le poil hirsute collé de boue et de crasse.

— Était-ce là quelque prière ?

— Je n’en ai reconnu aucune, sire archevêque. Je ne crois pas qu’il s’agissait de latin non plus. »

L’évêque s’agita :

« Serait-ce là des appels en mahométan ?

— Difficile à dire, mais il ne m’a pas semblé, même si je ne suis pas spécialiste. Je ne connais que quelques mots, pour parler avec les gens au-dehors… »

L’archevêque Pierre toussa pour intervenir de sa voix faible :

« Les gens des villages le vénèrent-ils ? N’est-ce pas celui qu’on appelle le Vieux de la Montagne ?

— Il ne me semble pas. On ne parle pas de lui depuis très longtemps, et il ne semble pas s’intéresser aux gens. Quelques paysans superstitieux viennent lui porter nourriture et offrandes, mais je n’ai jamais entendu qu’il ait fait quoi que ce soit. En dehors des miracles.

— Des miracles ? Quel type de miracles ?

— Il protège les lieux des brigands on dit. Il parle à la terre, elle lui obéit. »

L’évêque reprit place dans son siège et soupira.

« S’il ne prêche pas, il n’y a guère à s’en inquiéter. Ce n’est qu’un ermite, sans grand intérêt.

— Il semble bien, en effet. Mais je m’inquiète toujours de voir ces anachorètes incontrôlables aller de par le pays. Ils peuvent jeter le trouble dans les cœurs.

— De toute façon, nous n’avons guère de paroisses en ces lieux. Cela touche plus les Grecs.

— Si fait, vous avez raison, mon fils. Aucune crainte à avoir pour la fréquentation de nos églises ou le versement de la dîme. Mais je m’inquiète toujours de voir des idées étranges se propager dans les cœurs. »

L’évêque opina, réfléchit un instant.

« Voulez-vous que je fasse en sorte de le déloger ?

— Non, cela ne ferait que déplacer le souci. Gardez-le juste à l’œil. Peut-être n’est-il qu’un saint homme s’efforçant au bien. »

L’évêque esquissa un sourire, amusé. Il congédia le clerc d’un geste de la main et tourna les yeux vers le délicat jardin de la cour, orné de mille couleurs, où voletaient des papillons et quelques passereaux. Le ciel accueillait quelques nuages paresseux qui jouaient avec la lumière. Un saint, dans son diocèse ? Et pourquoi pas après tout ? La manne des pèlerins se déversait jusqu’à présent surtout en Galilée, Samarie et Judée. Quelques miracles plus au nord pourraient les y attirer, ainsi que leurs dons et offrandes. Qui était-il pour s’opposer à la volonté divine ? s’amusa-t-il en son for intérieur. Il sourit à l’archevêque et murmura :

« *O altitudo divitiarum sapientiæ, et scientiæ Dei*[^oaltitudo] » ❧

## Notes

La justice médiévale latine utilisait régulièrement les signes d’infamies (marques au fer, section du nez, des oreilles…), de façon à pouvoir aisément identifier les fauteurs de trouble et condamner très sévèrement les récidivistes (souvent à la peine capitale). À cela s’ajoutait la mise au ban de la communauté, par l’exil forcé. Le coupable était ainsi placé à l’écart de la société dont il avait transgressé les règles, et perdait l’accès à toutes les solidarités. Cela pouvait revenir au final à une condamnation à mort.

Le clergé au XIIe siècle n’a pas encore investi tous les pans de la société médiévale et les mentalités demeurent généralement assez souples malgré les mouvements de réforme ecclésiastiques qui appellent à plus de rigueur. À la fois dans l’observance des règles pour les clercs eux-mêmes mais aussi pour les fidèles. Beaucoup de prélats se contentent de voir leurs ouailles assister aux offices et verser la dîme. Ils n’interfèrent que peu dans leurs croyances, cherchant à canaliser les pratiques plutôt qu’à contrôler les esprits. Beaucoup de saints personnages accèdent ainsi à une reconnaissance officielle de l’Église alors même qu’ils ne se présentent pas comme très orthodoxes au départ.

## Références

Régnier-Bohler Danielle, *Croisades et pèlerinages. Récits, chroniques et voyages en Terre Sainte XIIe-XVIe siècle*, Paris : Robert Laffont, 2002.

Le Goff Jacques, Rémond René (dir.), *Histoire de la France religieuse. Des origines au XIVe siècle*, Paris : Éditions du Seuil, 1988.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.
