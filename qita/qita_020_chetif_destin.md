---
date: 2013-07-15
abstract: 1157. Lorsque la guerre endémique s’installe en une région, la survie des plus modestes devient un combat quotidien contre la fatalité. Choix moraux, choix de vie, espoirs pour les siens, chaque décision se fait dans une urgence vitale et façonne l’individu. Pendant les croisades, le turcople, ou turcopole, est à la jonction de deux mondes, musulman et chrétien, européen et moyen-oriental, qui ne s’excluent et ne s’ignorent pas tant qu’on l’aime à croire.
characters: Jehan le Batié, Guilhem Torte, Raoul
---

# Chétif destin

## Meleha, abords du lac de Houla, soirée du mardi 18 juin 1157

Le moustique vrombissait aux oreilles de Jehan le Batié. Agacé, pestant fort rageusement, le soldat agitait les doigts en tout sens autour de sa tête.

« Le démon saisisse ces infernales bestioles ! Que dévorent-elles donc quand nul sergent de la Milice du Christ n’est là ? »

Un de ses compagnons, occupé à souffler sur leur maigre feu de camp, lui sourit, protégé par l’épaisse fumée s’échappant du bois vert.

« S’enduire de boue est fort efficace, ainsi que le font les buffles d’eau ! »

Jehan s’esclaffa sans vraiment rire puis vint se placer à son tour sous le vent. Il ajouta dans la marmite quelques poignées de sorgho au riz acheté aux paysans du lieu. Autour d’eux, l’escorte royale s’était installée sous les frondaisons qui habillaient les pentes où s’alanguissait le vieux village aux maisons grises. L’éminence jaillissait des terres marécageuses alentour.

La route menant vers Tibériade, depuis Panéas[^paneas], avançait au pied des reliefs occidentaux, près des champs de canne à sucre. Le paysage au levant n’était que roselières, cultures et étendues d’eau jusqu’aux contreforts du Golan. Des colonies d’oiseau tachetaient la vaste plaine de couleurs vives et chamarrées, et leurs cris et chants résonnaient sur les paluds. Les hautes tiges souples des plantes aquatiques murmuraient dans le vent faible, sous un ciel d’un cyan immaculé. Le feu du soir n’avait pas commencé à ronger l’horizon.

Jehan étendit ses jambes, fourbu après une journée de monte. S’il savait bien tenir en selle, il n’était pas habitué à y rester longtemps. Il servait, en tant que fantassin, la milice du Christ qu’on appelait désormais du lieu où se trouvait le quartier général : le Temple. Escortant Baudoin de Jérusalem[^baudoinIII], ils revenaient de Panéas, attaquée récemment par les soldats turcs de Nūr ad-Dīn[^nuraldin]. Voyant que son adversaire avait fui à son approche, le roi s’était contenté de libérer la forteresse puis s’était employé à faire relever maisons et fortifications détruites. La plupart des hommes étaient demeurés sur place de façon à achever le travail, tandis que la cavalerie reprenait le chemin de la cité sainte.

Quelques sergents, parmi lesquels Jehan, avaient été désignés pour encadrer les animaux de bât. Comme son nom, Le Batié, l’indiquait, il n’était pas chrétien depuis fort longtemps. C’était un captif converti pour échapper à l’esclavage. Tout jeune, il avait pris les armes par désoeuvrement, trop pauvre pour pouvoir acheter des bêtes, trop fier pour se faire fellah. Devenu brigand, il pillait et rançonnait les voyageurs imprudents. Jusqu’à tomber aux mains des féroces cavaliers au blanc manteau. Peu versé dans la religion, il avait rapidement compris qu’une meilleure vie pouvait s’offrir à lui. Il abjura la foi de ses ancêtres, qu’il ne pratiquait que des lèvres, pour celle qu’il ne connaissait guère plus de ses conquérants.

On l’avait aspergé de quelques gouttes, il avait prononcé des mots mystérieux et avait retrouvé sa liberté. Toute relative, car il suait désormais sous le harnais, installant le campement, prenant soin des montures et tenant la lance et le pavois à l’occasion. Il n’y était pas maladroit, mais préférait son petit bouclier rond et son épée, une bonne lame d’Europe dont il avait refait la poignée lui-même. Il ne goûtait finalement guère le repos, mais dormir le ventre plein était un luxe pour qui avait connu des années où la poussière des chemins servait de souper et les broussailles rêches de matelas. Le regard embrassant le paysage, il jouait machinalement avec la croix qu’il portait autour du cou, symbole de son émancipation et garantie pour lui de son statut. Sur un ressaut du terrain, quelques hommes nourrissaient d’avoine les montures des chevaliers, tandis que du foin était prodigué aux autres.

Le village, simple amas de cabanes de pisé entourant une solide bâtisse où l’on extrayait le sucre des cannes, n’avait aucune fortification. Des paysans, de retour des champs de canne à sucre, venaient aux nouvelles, s’informant de ce qui se passait au-delà des frontières de leur petit monde routinier. Ils se contentaient de travailler la terre, de payer les impôts à celui qui se proclamait maître des lieux. La guerre pouvait en changer le visage, mais n’en rendait pas la poigne plus douce et ne perturbait pas le lent déroulement des saisons, du labeur pour arracher aux sillons sa subsistance.

Jehan n’avait que dépit pour ces hommes serviles, selon lui tout juste bons à être pressurés. Il était d’une ascendance nomade, un enfant de pasteurs, pillards à l’occasion. Il aspirait à retourner sous la tente de poil, d’y boire le leben en chantant les poèmes des exploits de ses ancêtres, dans l’attente de ses fils qui porteraient haut la gloire de son nom. Il était affligé de l’avoir abandonné pour cette appellation latine et chrétienne. Mais il n’était plus un homme du désert de toute façon, et ses rêves de grandeur n’étaient désormais que fantasmes. Il soupira et se tourna vers ses compagnons de route. Le repas était prêt.

## Abords du Jourdain, soir du mercredi 19 juin 1157

Germant sous le pas des montures et des soldats affairés, la poussière tourbillonnait en volutes de cendre. Les visages transpirants se maculaient, les plaies devenaient grises, la boue buvait le sang. Parmi les hennissements des chevaux énervés, les cris des hommes agacés, fébriles, on discernait des râles, des gémissements, des prières.

Brisée, la superbe escorte du roi de Jérusalem était répandue dans une clairière, les corps des agonisants servants de matelas aux blessés. Les yeux inquiets, brûlés par la sueur et le soleil, cherchaient un sens à la folie environnante. De temps à autre, une escouade turque venait prélever quelques captifs et les emportait, sans qu’on ne puisse tirer d’eux autre chose que des coups et des ordres lancés d’une voix dure.

Les chevaliers gisaient tels des mendiants, affalés dans les broussailles épineuses. On avait forcé les plus fiers à s’agenouiller, garroté serré les arrogants, humiliant leur morgue de rires moqueurs.

Sans blessure sérieuse, Jehan avait été désigné pour aider à porter les plus mal en point. Rapidement interrogé par un émir au sharbush[^sharbush] crasseux, il avait prétendu être un habitant de la côte, chrétien depuis toujours. Il savait quelle punition attendait généralement ceux qui avaient abjuré la foi musulmane. Le tranchant d’une lame raccourcissait de façon définitive la tête qui avait osé se dresser contre les traditions. Il avait expliqué que sa famille était dans le commerce, espérant se donner ainsi de la valeur.

Fouillant sous les mottes de phalagnon, cherchant un galet à sucer, il passa une langue pâteuse sur ses lèvres sèches. Le vent les rafraichissait enfin, mais la journée avait été torride, les affrontements impitoyables. Partis de Meleha au matin, ils avaient suivi tranquillement la route vers le sud, jusqu’à ce que des unités de Nūr ad-Dīn fondent sur eux au détour d’un relief, en contrebas de Saphet[^saphet].

La cavalerie, rompue aux exercices, n’avait pas cédé et s’était rapidement organisée en plusieurs corps. Mais la valeur n’avait pas suffi à compenser le nombre. L’embuscade avait été bien préparée et l’émir était venu avec tous les hommes qui l’avaient accompagné à l’assaut de Panéas. Quelques escadrons de chevaliers, sans infanterie pour les soutenir, ne pouvaient s’opposer à pareille force. La vaillance des coeurs et la robustesse des lances ne fit que rendre plus pénible l’inéluctable. Jehan avait compris que c’était la fin lorsque la bannière du roi s’était affaissée dans la cohue. Peu avant, il avait vu le grand maître, Bertrand de Blanquefort, s’écrouler, son cheval tué sous lui, tandis qu’il menait une charge intrépide.

Les hommes du Temple capturés avaient été mis à l’écart, étroitement surveillés avant d’être emmenés sous les lazzis des combattants musulmans. Certains leur crachaient dessus, d’autres leur jetaient pierres et gravillons, volaient leur manteau pour lui faire subir tous les outrages. Adversaires irréductibles de l’Islam, ils constituaient des captifs de valeur. Leur colonne avait disparu derrière les bosquets de pins et de pistachiers descendant vers le gué sur le Jourdain. On murmurait qu’ils allaient être décapités devant les officiers musulmans, offrant un spectacle de choix aux vainqueurs méritants. Jehan avait été soulagé de ne pas être intégré à leur groupe. Captif, esclave, il pouvait encore espérer en réchapper. La survie était inscrite dans son âme.

Déroulant le foulard qui lui servait de ceinture, il s’essuya le front, le cou. Croisant le regard d’un chevalier assis face à lui, il le lui proposa d’un geste, en silence. L’homme acquiesça et prit l’étoffe, la passant sur son visage souillé. On lui avait retiré son haubert de mailles, mais son gambison de soie attestait encore de son statut. La trentaine, il avait le faciès rude du guerrier, les mains puissantes du cavalier amateur de joutes. Il riva ses yeux couleur d’eau sur le sergent.

« Tu n’es pas sergent le roi ? Ton visage ne m’est guère familier… »

Jehan toussota et répondit d’une voix faible.

« Certes pas, mais je préfère ne pas faire assavoir à ceux-là qui je servais. »

Le chevalier hocha la tête, esquissant un rictus amer.

« As-tu vu si d’aucuns ont pu s’en réchapper ?

— Je ne saurais dire, j’étais un peu en retrait. J’ai vu les bannières tomber les unes après les autres.

— As-tu vu le roi se faire prendre ? »

Jehan secoua la tête.

« J’ai vu son étendard flancher, rien de plus. Par contre, le sire de Blanquefort a été saisi. »

Le chevalier serra la mâchoire et hocha la tête en silence avant de lâcher, d’une voix brisée :

« Nous voilà entre les mains de Dieu. »

Jehan baissa la tête, jouant de son caillou avec la langue.

« Plutôt dans celles des diables turcs » murmura-t-il.

## Damas, matin du lundi 24 juin 1157

Jeté en pleine lumière, le troupeau humain émit un gémissement. L’ardeur du soleil piquait les yeux habitués aux ténèbres. Plissant les paupières, ils se dévisageaient, hagards, après les jours passés dans une quasi-obscurité, entassés parmi la paille moisie, les excréments, les rats et les cadavres de leurs compagnons. Les vêtements pendaient sur les silhouettes amaigries, fatiguées et un linceul de cendre recouvrait leur corps. Rendus dociles par la faim, l’épuisement et la lassitude, ils marchaient en traînant des pieds, les bras ballants, la mine égarée.

On les fit s’asseoir et quelques serviteurs leur distribuèrent du pain et des pots emplis d’une eau croupie. Chacun s’en fit un festin et il fallut donner de la voix et du fouet pour que certains n’accaparent la modeste pitance. Jehan mâchait en silence, se grattant frénétiquement le ventre et le cou dévorés de vermine. Le dos lui cuisait encore des coups de bâtons reçus pendant leur venue jusqu’à la ville, sans qu’il n’en ait su le motif. Il s’efforçait d’être aussi coopératif que possible, obéissant avec tout le zèle qu’il pouvait insuffler dans son corps affaibli. Le captif à sa gauche, issu d’une autre geôle, lui proposa la cruche après en avoir avalé une longue gorgée qu’il faisait jouer entre ses dents, la savourant comme nectar de Chypre. Il but rapidement et passa le récipient à son tour, engloutissant goulûment les lambeaux de pain qu’il avait pu saisir. Il réalisa que son voisin le dévisageait et l’interrogea d’un regard.

L’homme pencha la tête et répondit à voix basse, bougeant à peine les lèvres.

« Tu as été pris où ?

— En Galilée, voilà quelques jours.

— Grosse bataille ?

— Non, une embuscade. Nous étions une poignée. Le roi n’en a échappé que de peu. »

L’homme hoqueta de surprise et déchira d’un coup de dent une portion de pain.

« Le roi ? En Galilée ? Avons-nous tant perdu de terres ?

— Non, c’était l’armée du soudan qui rôdait après l’assaut sur Panéas. Nous avons joué de male chance. »

Jehan garda le silence un moment et demanda finalement.

« Sais-tu ce que nous faisons ici ?

— Ils nous préparent pour nous montrer au peuple. Le soudan aime à fêter ses victoires. Cela fait plusieurs fois qu’on me tire de mes labeurs pour ça.

— Tu es là depuis longtemps ? »

L’homme soupira, mais ne répondit pas.

« J’avais nom Guilhem, mais je ne suis plus qu’un esclave parmi les ombres.

— Au moins ils nous gardent vifs. Mieux vaut serf que moribond. »

Guilhem leva le menton, indiquant quelque chose derrière Jehan. Celui-ci hésita à se tourner, voyant l’air abattu de son compagnon d’infortune. Inquiété, il mit un moment à se décider avant de porter son regard vers l’arrière. Puis ses yeux s’emplirent d’horreur. Sur plusieurs chameaux, des chevaliers étaient installés deux par deux, et on fournissait à chacun un étendard pris aux chrétiens. Sans oublier d’y suspendre par les cheveux la tête de quelques captifs. ❧

## Notes

Le terme de « chétif » joue sur le sens commun et son usage ancien, qui signifie « captif ». Son emploi le plus célèbre est celui qui désigne le texte que Graindor de Douai a intercalé au XIIe siècle entre la *Chanson d’Antioche* et la *Conquête de Jérusalem*. Il y développe le récit de chevaliers capturés, mettant en avant leurs aventures d’une façon romanesque qui tranche avec la tradition plus descriptive et historique de la chanson précédente.

Les sources médiévales font régulièrement allusion aux turcoples/turcopoles, et toutes les hypothèses ont été échafaudées sur leur réalité. Il est désormais illusoire de penser pouvoir la définir de façon monolithique, tant le mot est polysémique. Toujours est-il qu’on retrouve de ces combattants dans l’armée franque, parfois affublés de noms, comme Jehan le Batié, Jean le Baptisé, qui semblent indiquer des convertis récents.

Mais pour autant, est-ce que cela veut dire que tous les turcoples, du moins tous ceux désignés sous ce terme étaient ainsi ? Apparemment ils étaient de culture et de tenue moyen-orientale, mais on n’est même pas certain de devoir les rattacher aux traditions locales, syriennes et arabes, ou turques nomades.

## Références

Gibb H. A. R., *The Damascus Chronicle of the Crusades. Extracted and translated from the chronicle of Ibn al-Qalanisi*, Londres : Luzac & Co., 1932, réédition : Mineola : Dover Publication, 2002.

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Richard Jean, « Les turcoples au service des royaumes de Jérusalem et de Chypre: musulmans convertis ou chrétiens orientaux? », dans Richard Jean, *Croisades et Etats latins d’Orient. Points de vue et Documents*, Aldershot : Ashgate, 1992.
