---
date: 2014-11-15
abstract: 1156. Un prisonnier et son geôlier, dans les faubourgs d’une ville la nuit. Situation simple d’où ne peut naître que la confrontation. Lequel des deux se révélera le plus humain ?
characters: Maciot Point l’Asne, Mustafâ al-Dimashqi
---

# Liberté

## Baalbek, nuit du youm al sebt 26 dhu al-qi’dah 550^[Samedi 21 janvier 1156.]

Ombre parmi les ténèbres, accroupi près d’un imposant bouquet de genévrier, un homme en haillons attendait, immobile. La brise glissait sur lui, sur ses hardes, pourtant il n’avait pas froid. Son esprit était entièrement tourné vers son but.

Ses yeux hagards épiaient fièvreusement le garde censé surveiller les esclaves. Le prisonnier avait réussi, à force de patience, à creuser un trou dans le mur de la masure où on les entassait, mais il avait été le seul à oser s’y faufiler. Les autres captifs étaient trop fatigués, trop désespérés pour s’y risquer. Pas lui ! Il avait une furieuse soif de liberté.

Ni le fouet ni les privations n’avaient altéré sa volonté. Il se sentait plus fort que jamais, en rage et plein d’ardeur. On lui avait retiré la femme qu’il aimait, trainée loin de lui. Puis on l’avait fouetté, durement, avant de l’amener dans cette ville maudite pour y travailler comme un forçat. Tandis qu’il s’épuisait à porter les gravats, acheminer les pierres, il se répétait sans cesse le nom de sa bien-aimée, se raccrochant à son souvenir pour ne pas sombrer. Il était décidé à s’enfuir pour la retrouver, la libérer de ses geôliers infidèles. La Terre sainte s’était révélé un enfer emplis de démons et d’ordures dont ils devaient s’échapper.

Malgré la fureur qui l’habitait, il restait tapi, semblable à une pierre dont il avait la couleur poussiéreuse. Un hamster s’y trompa, passant devant lui ainsi qu’il l’aurait fait devant un amas rocheux, indifférent. Inquiété par l’effluve que le vent venait de rabattre, l’animal fit un détour. Sur son trajet se trouvait un autre humain, celui-là même que le fugitif surveillait avec attention.

Mustafâ al-Dimashqi tapait des pieds pour se réchauffer, agacé de rester seul à surveiller le chantier dans cette glaciale nuit noire où la rare lumière des étoiles ne lui était d’aucun réconfort. Il entretenait les flammes qui lui réchaufaient les articulations et éclairaient une petite zone autour de lui, mais sans cesser de râler contre cette tâche injuste.

Cela faisait plusieurs années qu’il avait rejoint l’aḥdāṯ[^ahdath] de sa cité et on ne lui confiait jamais que des tâches secondaires. Garder les montures, approvisionner le camp, encadrer les prisonniers, voilà tout ce qu’on acceptait de lui ! Il avait espéré s’enrichir en devenant soldat, en détroussant les riches voyageurs Ifranjs[^ifranj], mais au final il n’était que le commis des véritables soldats, ces maudits Turcs qui s’accaparaient les riches butins.

Il craignait d’avoir des problèmes avec sa femme, car il ne ramenait guère d’argent au foyer. Elle ne lui en faisait pas reproche, elle n’aurait jamais osé. Néanmoins il voyait bien dans ses yeux qu’elle le considérait comme un raté. Elle n’arrêtait pas de lui parler de ses frères, qui réussissaient dans les affaires, de ses cousins qui habitaient de magnifiques demeures.

Il s’en était ouvert à son amir[^amir] et avait demandé à se voir confier de plus grandes responsabilités. Voilà pourquoi il se retrouvait là, toutes les nuits, à garder un œil sur le baraquement des esclaves affectés à la réfection des murs d’enceinte de la ville. Il y avait plusieurs dizaines de captifs et leur valeur était importante, lui avait-on assuré. Mais ce n’était pas ainsi qu’il ferait fortune ! Abu Bakr, son supérieur, s’était moqué de lui et lui avait refilé la corvée dont personne ne voulait, sous prétexte de mission de confiance. Alors Mustafâ se gelait les pieds et les mains en râlant contre son sort.

Le prisonnier espérait pouvoir se faufiler hors de la cour sans être vu. Il fallait juste que cette maudite sentinelle se décide à lui tourner le dos et il pourrait tenter de se glisser silencieusement jusqu’à l’entrée sud. Il avait repéré l’autre côté, mais il y était trop visible depuis les remparts de la ville. Malgré la nuit sans lune, il était facile pour une sentinelle de l’apercevoir dans la vaste étendue vide de toute végétation. Depuis le temps qu’il patientait, il commençait à sentir des crampes, des fourmillements dans son corps fourbu. Il ne pouvait se permettre de perdre trop de temps. Plus tôt il s’échapperait, plus il aurait de chance d’échapper à des poursuivants lancés sur ses traces dès la reprise du travail au matin.

Il réfléchissait à un moyen de faire diversion, peut-être en lançant un caillou. Son regard glissa dans la pénombre et s’arrêta sur une pierre, bien trop grosse pour être lancée, mais tout à fait indiquée pour s’en faire une arme. Il fronça les yeux d’un air approbateur, un sourire malsain déformant ses traits. À défaut de contourner la vigie, il pouvait s’en débarasser. Se pourléchant les babines comme un prédateur embusqué, il s’empara à tâtons de son arme improvisée, l’assujettit avec application dans sa paume, la soupesant et, de petits gestes vifs, fit mine de frapper.

Mustafâ commençait à s’endormir debout et se donna quelques gifles sur les joues pour se réveiller. Il était impatient de voir la relève arriver. Il attrapa l’outre accrochée à une branche et se désaltéra puis entreprit de fouiller dans sa sacoche où il avait un peu de pain et des fruits secs. Il poussa du pied une bûche qui lui servirait de siège, au plus près du feu puis s’assit, engloutissant sans plaisir son en-cas, mâchant bruyamment. Une rafale de vent souleva de la poussière et la propagea sur son campement sommaire, le faisant frissonner. Il étendit ses pieds bottés à proximité des braises, soupirant d’aise à la sensation de chaleur.

Au loin, les hurlements des chacals, entrecoupés d’aboiements, indiquaient qu’une meute partait en chasse. Mustafâ était heureux de n’avoir pas un troupeau à surveiller. Les humains étaient plus faciles à garder, généralement assez dociles. Et plus aptes à éviter les prédateurs.

Il allait cracher un nouveau noyau de datte dans les flammes lorsqu’il perçut du coin de l’œil un mouvement. Il se retourna trop tard, l’homme était déjà sur lui. Il leva tout de même les bras pour ne pas recevoir le coup sur la tête, roulant sur le sol dans le choc, entraînant son agresseur dans sa chute. L’empoignade était aussi violente que désordonnée. Aucun des deux adversaires n’était habitué à la bagarre. Ils se tiraient, se repoussaient, s’arrachaient les cheveux ou se frappaient des coudes avec la hargne de deux bêtes furieuses.

Tout en se débattant, le captif tentait désespérément de retrouver sa pierre, qui lui permettrait de frapper un coup décisif. Mustafâ espérait pouvoir dégainer son petit couteau, passé dans le foulard qui lui tenait lieu de ceinture. Mais le Franc eut plus de chance. Il posa les doigts sur ce qu’il recherchait tandis qu’il était allongé sur le dos, écrasé par son adversaire. Frappant avec l’énergie du désespoir, il sentit le choc sourd contre le crâne et son ennemi s’écroula sur lui comme un tas de guenilles.

Se dégageant rapidement, le fugitif s’agenouilla sur le corps désormais inerte, du sang s’écoulant de la plaie derrière l’oreille. Interdit, il examina la pierre sanguinolente puis son adversaire, sentant le souffle qui s’exhalait encore lentement des lèvres. À plusieurs reprises, il fit mine de soulever son arme pour l’abattre sur le visage de l’homme, mais ne put s’y résoudre.

La tension était retombée. D’un geste dégoûté, il jeta son arme dans les broussailles où il se dissimula bien vite, non sans avoir reniflé de colère une dernière fois. La voie était libre, il ne lui restait qu’à courir. Instinctivement, il attrapa au passage l’outre en peau de chèvre et la besace du gardien et détala comme une bête traquée, se propulsant parfois à l’aide de ses mains lorsqu’il était déséquilibré par sa course sur la route caillouteuse. Par moments, il jetait derrière lui un regard affolé tel un renard à la chasse, comme si des chiens allaient le pourchasser. Mais tout demeurait calme.

Essouflé par son départ précipité, il s’arrêta près de la grande pierre que ses geôliers appelaient Hajar al-Houbla. Elle était tellement énorme qu’une fois taillée, les anciens bâtisseurs n’avaient pu la bouger. Elle serait suffisamment haute pour offrir un point de vue sur les alentours. Il grimpa dessus en un éclair. D’autres feux illuminaient le campement qu’il avait fui, certains plus hauts que d’autres : des cavaliers étaient lancés à sa poursuite ! De colère, il se mordit la lèvre. Il aurait dû achever le gardien. Sa faiblesse allait lui coûter cher.

Lorsque Mustafâ ouvrit les yeux, il avait l’impression que sa tête allait exploser. Contemplant sa main après l’avoir portée à son crâne, il s’aperçut qu’elle était couverte de sang, de poussière et de graviers agglomérés. Il s’assit lentement puis regarda autour de lui, encore sonné par le coup. Il s’efforçait de remettre ses idées en place, conscient que de sa rapidité dépendait son avenir : un des esclaves sous sa garde venait de s’échapper. Cela voulait dire de gros ennuis en perspective ! Il chercha son sac pour en sortir la trompe de cuivre qui lui servirait à sonner l’alerte, mais il ne la trouva pas. En plus le fugitif l’avait détroussé !

Il se releva à grand peine, se déplaçant lentement vers un appenti près de l’entrée. Là, il se mit à taper sur le bois de la porte de toute ses forces en hurlant « Évasion ! Alerte ! Un esclave s’est sauvé ! Alerte ! » en essayant de ne pas succomber à l’onde de douleur qui se propageait depuis sa tête, jusqu’à lui ébranler les viscères.

Les sentinelles de la cité ne furent pas longues à dépêcher trois cavaliers, des membres de l’aḥdāṯ eux aussi, que Mustafâ connaissait bien. Ils étaient légèrement équipés, seulement d’épées et de masses, et sans armure. Deux d’entre eux brandissaient des torches.

« Il m’a attaqué, y’a pas longtemps, je suis demeuré à terre, assommé. Il est de sûr parti vers le bosquet de sapins, au couchant, pour s’y cacher, ou sur la route d’Anjar. »

Un des cavaliers lança un regard noir vers la sentinelle défaillante.

« Il y a long chemin d’ici le Wadi al-Tayim, il n’est pas prêt de rejoindre les terres ifranjs ! »

Les chevaux s’élancèrent sans guère attendre, projetant les gravillons en tous sens tandis qu’ils galopaient sur le chemin poussiéreux. Le calme revint soudain, après les hennissements des bêtes, les cris des hommes. Mustafâ porta de nouveau la main sur sa blessure et contempla sa paume recouverte de sang, aussi noire que la nuit dans l’obscurité sans lune. Il fit une grimace, qui de douleur devint de colère. Il pensait avoir atteint le fond lorsqu’il s’était trouvé seul à garder les captifs. Mais il enrageait de découvrir que sa situation pouvait encore empirer, à cause de ces maudits Ifranjs ! Impuissant, il leva vers le ciel une main implorante. Qu’avait-il fait de si terrible pour être ainsi perpétuellement châtié ?

L’évadé vit les cavaliers prendre la route du sud vers lui. Il redescendit précipitamment de la pierre et, avisant qu’elle était penchée et offrait un petit espace dégagé à sa base, il se glissa dessous. Il espérait laisser passer les chevaux et reprendrait son chemin par la suite, en les sachant devant lui. S’écorchant et se meurtrissant les membres, il réussit à disparaître totalement sous l’énorme roche, comme un animal craintif terré dans l’ombre de son antre.

Léchant ses mains griffées, il essayait de calmer son souffle, au cas où les poursuivants viendraient inspecter les environs. Il s’efforçait aussi de ne pas succomber à la fureur qui montait en lui. Il s’était montré faible en laissant la vie sauve au gardien. Ce dernier avait pu rapidement donner l’alerte. Crachant et pestant en silence, il se jura de ne jamais refaire ce genre d’erreur. Quoiqu’il lui en coûte, sa main ne faiblirait plus ! ❧

## Notes

Les fréquents combats en Terre sainte ne donnaient pas lieu à des massacres systématiques, et les populations étaient plus souvent razziées qu’exécutées. Parmi les soldats, beaucoup voyaient dans les combats l’occasion de s’enrichir.

La prise de captifs constituait une source estimée de revenus, qu’ils soient ensuite relâchés contre rançon, généralement pour les plus riches et puissants ou avec le plus de relations, ou simplement utilisés comme esclaves, éventuellement pour alimenter un marché aux ramifications internationales.

Les pèlerins, voyageurs isolés loin de leurs familles, souvent mal équipés et peu fortunés rentraient logiquement dans cette dernière catégorie et les récits évoquent parfois la disparition de groupes entiers, sans plus de précisions. Parfois certains chroniqueurs évoquaient le destin de tous ces sans-grades qui, à l’occasion d’un traité ou de la libération de captifs prestigieux, recouvraient alors la liberté.

## Références

Friedman Yvonne, *Encounters between enemies. Captivity and ransom in the Latin Kingdom of Jerusalem*, Leiden : Brill, 2002.

Gravelle Yves, *Le problème des prisonniers de guerre pendant les Croisades Orientales 1095-1192*, Maîtrise ès Arts en Histoire, Université de Sherbrooke, 1999.
