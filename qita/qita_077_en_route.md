---
date : 2018-03-15
abstract : 1153-1158. Jeune fille sans famille et sans attache, Anceline s’efforce de vivre au mieux dans les territoires latins. Alternant des moments de désespoir avec de courtes embellies, elle tente de trouver sa place avec les maigres moyens dont elle dispose.
characters : Anceline, Margue l’Allemande, Robert, Belek, Houdoin, Baset, Gerbaut, Gelta, Mile
---

# En route

## Tyr, parvis de la cathédrale Sainte-Croix, fin de matinée du dimanche 18 février 1151

Poussée par la brise de mer, l’averse matinale avait refroidi l’atmosphère. Le sol, marqué de quelques flaques, devenait de la boue sous le piétinement des fidèles au sortir de la messe. Un ciel cendré promettait de nouvelles ondées, sans que cela inquiète la foule occupée à discuter. Des bandes d’enfants, s’affranchissant de la tutelle de leurs aînés en plein commérage, en profitaient pour se faufiler de-ci de-là en jouant et piaillant.

Plusieurs saltimbanques avaient pris place sur le parvis, espérant une prodigalité renforcée par l’office religieux. Plusieurs montreurs d’animaux faisaient cabrioler leur bête, s’attirant des cris d’émerveillement ou de surprise. Jongleurs et acrobates n’étaient pas en reste, apostrophant le chaland à grand renfort de tambours et d’exclamations outrées.

Au plus près de l’édifice sacré se tenaient des musiciens, profitant des marches pour se poster ainsi qu’en chaire face à leur auditoire. Parmi eux, tout au nord de l’endroit, une jeune femme chantait des psaumes de sa voix fatiguée. Elle n’avait guère de vêtements sur elle pour se prémunir du froid et se frottait continuellement les bras afin de se réchauffer. Son répertoire attirait à elle les plus dévots, pas pour autant toujours les plus charitables. Elle savait que c’était là sa plus belle quête de la semaine et mettait donc tout son cœur dans un latin approximatif, n’ayant guère idée de ce qu’elle déclamait. Elle se contentait de copier à l’oreille les chants des pèlerins et des clercs.

Anceline avait pris l’habitude de suivre la côte, quittant une cité où la générosité s’érodait pour une autre, le temps de faire oublier les largesses reçues. Elle s’était constitué un petit répertoire sacré qui ne recueillait pas autant de pièces que les habiles tours des artistes plus inventifs, mais qui lui permettait d’avoir bon accueil auprès des établissements religieux. Elle était une fidèle des hospices, y goûtant la soupe légère et le pain noir qu’elle engloutissait comme mets de choix.

Une fois encore, la récolte était maigre et ne lui donnerait guère l’occasion de festoyer. En outre, en ce mois de février, le plus frais de l’année, elle était frigorifiée. Elle n’avait pour tout vêtement que deux chemises, sa cotte bien fatiguée et une chape lui servant souvent de couverture lorsqu’elle était sur les chemins. Elle avait des chausses sans pieds, ayant depuis longtemps oublié l’usage des savates qu’il fallait sans cesse faire ressemeler.

Elle avait à peine vingt ans, mais en paraissait dix de plus, fanée par la trop fréquente exposition aux éléments et les durs labeurs. Elle se louait comme lavandière là où elle pouvait trouver de l’ouvrage, bien rarement et payé à la journée. Elle y avait gagné de nombreuses engelures et des mains douloureuses lorsque le temps tournait à la pluie. Il lui était aussi arrivé de négocier les attraits de son corps quand la faim se faisait trop pressante, mais elle évitait d’en faire une habitude.

Souvent, après avoir cédé au découragement, elle quittait la ville, désireuse d’oublier et craignant d’être ainsi connue. Malgré tout, à force de fréquenter les mêmes lieux, on la voyait de moins en moins comme une lavandière. Elle refusait néanmoins de s’abandonner à ce commerce et de suivre les troupes de soldats où il était plus aisé de s’y adonner. Sans plus vraiment y croire, elle ne désespérait pas de retrouver celui qui lui avait fait promesse de l’épouser. Elle ne voulait pas se présenter à lui irrémédiablement souillée, chérissant ce rêve sans jamais oser faire en sorte qu’il puisse éclore.

Elle comptait les quelques piécettes, oboles et méreaux, qu’elle avait reçus après son tour de chant quand elle fut apostrophée par une voix à l’intonation rieuse.

« Alors, belle amie, la moisson est-elle à la hauteur de ces divines strophes ? »

Celui qui avait parlé était un petit homme, plusieurs degrés au-dessus d’elle et pourtant les yeux à peine à sa hauteur. Son corps et sa tête étaient normaux, mais ses membres semblaient ceux d’un enfant. Tout en s’exprimant, il secouait ses mains, amplifiant chaque mot d’un geste démonstratif. Clairement, il était comédien, de nature si ce n’était de profession. Son visage aux traits réguliers était mangé de barbe et ses vêtements, pour être de bonne qualité, avaient connu de meilleurs jours. Il bondit à côté d’elle et fit une révérence entre deux sautillements.

« J’ai nom Mile, comédien, chanteur et acrobate ! Mes compères et moi t’avons entendue. Ce qui s’échappe de ta gorge est à l’aune de celle-ci et je tenais à te faire compliment pour le tout !

— Je n’ai nulle fierté à tirer de cela, *Dieu dispose*.

— Le sire patriarche en sa chaire n’aurait pas mieux dit, mais d’une voix moins doucette à mes oreilles ! »

Le visage barré d’un franc sourire inspira confiance à Anceline, qui empocha rapidement sa collecte et ramassa son petit sac posé au sol. Le nain avait visiblement une idée derrière la tête et elle s’approcha un peu de lui.

« Et que puis-je pour toi, compère Mile, que ne pourrait t’accorder beau clerc en son chœur ? »

La saillie éclaira le visage du nain plus encore et son regard en devint outrageusement égrillard.

« Rien que je ne saurais demander à mienne sœur, je t’en fais jurement. Avec mes compagnons, nous pensons aller au sud, vers Acre et sa chaude fournée de pérégrins qui ne saurait tarder à se déverser en nos terres. Une belle voix de plus en notre groupe ferait pousser cliquaille comme fleurs en mai. Outre, tu pourrais nous apprendre tes chants, plus propices à flatter l’oreille du dévot que mes paillardises. »

Tout en parlant, il désigna un petit groupe d’à peine une demi-douzaine, occupé à des acrobaties. Une femme mendiait auprès des chalands tout en leur lançant des plaisanteries dont Anceline devinait qu’elles n’étaient guère pieuses, à voir les mines mi indignées mi-enthousiastes autour d’elle. La présence d’une commère la rassurait un peu, bien qu’elle sache que cela ne garantissait pas toujours d’être en sécurité. Néanmoins, l’offre du petit homme était intéressante. Les pèlerins fraîchement débarqués pouvaient se montrer généreux. Peut-être s’y trouverait même un riche baron désireux de s’entourer de saltimbanques pour égayer ses soirées. Elle accorda à Mile son plus beau sourire.

« Eh bien, nomme-moi donc tes sochons. J’aurais usance de leur nom si nous devons compaigner un temps ! »

## Jérusalem, échoppe de Margue l’Allemande, matin du mardi 3 novembre 1153

De chaudes odeurs briochées vinrent titiller les narines d’Anceline avant même qu’elle ne réalise le bruit autour d’elle. Elle était épuisée, vermoulue, nauséeuse, incapable d’ouvrir les yeux. Elle était installée sur une couche et bien couverte. Dans sa bouche raide comme du carton, elle sentait des remugles désagréables, héritiers de la bile qu’elle soupçonnait encore désireuse de jaillir de ses entrailles.

Des personnes discutaient aimablement, une voix féminine dominant les autres, donnant parfois quelques ordres secs ou flattant ce qu’Anceline devinait être des chalands. Elle remua un peu, émit un grognement et força pour entrouvrir les paupières.

Elle était sur une modeste plateforme, installée sur une paillasse. À ses côtés, un coffre cadenassé. Le lieu n’était pas très large, mais assez long, couvert d’une voûte chaulée. Tandis qu’elle clignait des yeux, une petite silhouette s’approcha d’elle. Un garçon, emmitouflé dans une épaisse chemise de laine. Il semblait aussi circonspect qu’elle. Avant qu’elle ne puisse émettre un son, il avait bondi vers le bord de la galerie et crié :

« Mère, mère ! Elle est éveillée ! »

Anceline déglutit lentement, sa gorge lui paraissant de feu. Elle sentit qu’on montait à l’échelle et une tête apparut bientôt. Un visage rond, laiteux, serré dans un foulard d’où s’échappaient quelques mèches brunes. La femme lui sourit.

« Alors, ma mie, comment te sens-tu ? Nous avons eu grand effroi et craignions de devoir te livrer à la terre sans même connaître ton nom.

— Anceline me nomme. »

Sa voix lui fit l’effet d’un terrible croassement, elle qui aimait tant chanter.

« Bien contente de t’ouïr, ma belle. J’ai nom Margue et voici mon enfançon, Robert.

— Où suis-je ? Qu’est-ce que… »

Sa question mourut dans une toux violente qui l’abattit. Margue finit de grimper l’échelle, faufilant son imposante masse auprès de la couche pour la border.

« Dors, il sera bien temps plus tard. J’ai aussi moult questions pour toi. »

Les jours qui s’égrenèrent ensuite furent très flous pour Anceline. Elle se réveillait de temps à autre, nourrie de bouillons et de soupes au vin par Margue. Elle percevait parfois sa chaleur pendant la nuit, lorsqu’elles partageaient le grabat. Elle entendait les berceuses chantées pour Robert, fredonnées à mi-voix. La journée, le commerce qui se déroulait sous elle lui apportait des effluves sucrés et capiteux, incitant son estomac à la rébellion. Elle se sentait néanmoins chaque jour plus forte que le précédent.

Un soir qu’elle lui servait son écuelle de potage, Margue lui expliqua que c’était son valet Belek qui l’avait trouvée, inconsciente, dans le faubourg de la porte de David.

« J’avais plusieurs compères et commères. Que sont-ils devenus ?

— Je ne sais. Tu étais seulette en la ruelle. Peut-être t’ont-ils abandonnée pour morte ? »

Elle haussa ses fortes épaules tout en s’efforçant au sourire. Anceline se souvenait qu’ils avaient tous été atteints d’effroyables flux de ventre après des jours à se nourrir d’expédients. Les derniers mois avaient été particulièrement difficiles pour les bateleurs. Après les violents affrontements entre la reine Mélisende et son fils Baudoin, puis la prise de pouvoir effective et indisputée de celui-ci, il y avait eu de grands bouleversements dans le royaume.

Les barons n’étaient guère enclins à faire la fête, pas plus que les bourgeois à distribuer leurs aumônes. Et les moines préféraient les pèlerins aux saltimbanques, toujours suspects de mille fraudes et abus. Peut-être ses compagnons l’avaient-ils cru morte. Même Mile, qu’elle appréciait assez pour avoir partagé sa couche quelques fois, n’était plus si enjoué qu’à leurs débuts. Le désespoir les avait tous gagnés.

« S’ils t’ont laissée, c’est qu’ils ne méritent guère que tu te biles pour leur sort. Pense donc déjà à te bien porter !

— Pourquoi m’avoir ramassée ? Nous ne sommes pas…

— Suis-je si male chrétienne que je ne peux aider plus nécessiteuse que moi ? Nul ne sait quand il aura besoin d’une main tendue, mais il peut décider de la tendre à qui lui chante.

— Je ne pourrai jamais payer de retour.

— Tu pourras bien passer le balai ou tamiser la farine quand tu seras mieux. Belek est parfois si gauche que j’ai doutance qu’il fasse exprès dans l’espoir que je l’affranchisse de certaines corvées ! »

Elle récupéra l’écuelle et se releva, non sans remettre en place la couverture au plus près d’Anceline.

« Laisse donc simple femme te porter charité à sa mesure. On gagne ainsi Paradis à bon compte. »

## Jérusalem, quartier du palais royal, matin du lundi 16 novembre 1153

Désormais plus assurée sur ses jambes, Anceline allait de temps à autre en ville faire quelques commissions et livraisons afin d’aider Margue à faire tourner son commerce. Ce jour-là, ses pas l’avaient menée aux abords du Saint-Sépulcre et du palais royal. Elle avait longuement hésité, baguenaudant parmi les boutiques aux alentours, avant de se décider à s’enquérir de Fouques^[Voir le Qit’a _Mal chancre_.] auprès de la domesticité de Baudoin.

Des hommes de faction à l’entrée lui en interdirent l’accès. Elle n’avait selon eux aucun motif valable d’y pénétrer. Les profiteurs et larrons étaient légion et, si l’hôtel royal était généreux, il n’avait pas vocation à accueillir tous les miséreux de la terre. Devant le grand portail de la cour, un des sergents, moins méchant que son allure austère ne le laissait supposer la prit un peu en pitié et lui demanda après qui elle en avait.

« C’est jeune valet qui a trouvé service auprès du frère le roi, le comte de Jaffa. Il a nom Fouques et vient de Césaire, sur la côte.

— Pourquoi donc le recherches-tu ? T’aurait-il fait subir mal sort ? Abandonnée avec enfançon ? »

À sa question, Anceline comprit qu’il n’envisageait pas l’idée avec mansuétude.

« Rien de cela. Nous nous étions promis l’un à l’autre en nos jouvences et la vie nous a séparés. J’aurais néanmoins grande joie à l’encontrer de nouvel. »

L’homme hocha la tête avec gravité, visiblement soulagé de n’avoir pas à rechercher un pernicieux inconséquent. Il héla une petite femme à l’allure nerveuse qui s’approcha de lui, les bras encombrés d’une corbeille de draps. Il lui expliqua rapidement l’histoire et en appela à son jugement. Lingère du palais, elle voyait aller et venir la plupart de la domesticité pour leur fournir le couchage et, parfois, les vêtements.

« Je crois voir ce gamin là, indiqua-t-elle de sa voix haut perchée. Damoisel de belle allure, mais il n’est pas en nos murs. Le sire comte de Jaffa a tant attendu pour tenir Ascalon, il s’y est rendu tantôt. »

Elle adressa à Anceline un rictus désolé.

« Le sire Amaury[^amaury] va et vient souventes fois au palais, mais il risque de demeurer en ses terres le temps de s’assurer que nul ne vient lui contester sa prise. Ses ribauds seront avec lui, il y a sûrement fort ouvrage à accomplir là-bas. »

Anceline hocha la tête en silence et remercia d’un sourire. Puis elle prit congé des deux domestiques et reprit son chemin vers la proche boutique de Margue. Elle ne savait pas ce qu’elle avait imaginé. Même si elle avait retrouvé Fouques, il devait l’avoir oubliée. Cela faisait plusieurs années qu’ils s’étaient séparés. Elle ne maintenait son souvenir vivace que parce qu’il avait représenté un espoir de quitter sa vie de misère. Ce n’était qu’une chimère qu’elle s’entêtait à poursuivre, sans que cela ne lui apporte aucun réconfort.

Pourtant, elle ne pouvait s’empêcher de réfléchir au temps que lui prendrait un voyage vers le sud du royaume. Ascalon n’était pas si éloignée et de fréquents convois de marchands allaient sûrement s’y rendre depuis Jaffa. Elle pourrait y faire un tour, dans l’improbable espoir que Fouques y soit. Et qu’il accueille avec soulagement son amour de jeunesse. Elle pourrait tout à fait trouver de l’embauche comme lingère ou simple lavandière. Avec leurs deux soldes, ils pourraient vivre une belle vie et, même, avoir des enfants. Elle n’était pas si vieille pour ne pas en rêver. Elle aussi aurait plaisir à voir grandir un petit Robert auprès d’elle.

## Ascalon, abords de la porte de Jaffa, veillée du dimanche 13 décembre 1153

Anceline n’avait jamais vu une cité aux murailles pareillement dévastée. Une large partie de la courtine était abattue, objet de tous les soins d’une foule de maçons et de charpentiers. Elle venait chaque jour y trouver de l’ouvrage, en recherche de manouvriers désireux d’avoir leur vêtement nettoyé ou réparé. Elle avait renoncé à l’espoir d’obtenir de quoi manger après la messe, la population étant principalement constituée de voyageurs et de négociants. Lors de la prise de la ville, les musulmans avaient eu l’opportunité de fuir, abandonnant leurs maisons et leurs biens aux vainqueurs. Quelques familles de chrétiens orientaux, de juifs, étaient demeurées, mais elle n’était pas de leurs communautés pour oser leur demander l’aumône.

On parlait partout de l’ouvrage qui ne manquerait pas : cathédrale, château, fortifications. Amaury, seigneur d’Ascalon avait tout à faire en sa cité et les ouvriers se frottaient les mains des années de labeur qui s’annonçaient. Selon leur spécialité, ils allaient d’un chantier à l’autre, vendant leur savoir-faire aux plus offrants. Anceline leur enviait cette indépendance née d’une maîtrise, d’un talent dont ils défendaient jalousement la valeur.

Elle avait tenté d’entrer en contact avec les gens au service du comte, espérant enfin retrouver Fouques, pour apprendre qu’il n’était plus simple ribaud, mais valet d’armes et avait trouvé sa place dans l’ost royal au service d’un chevalier. Il pouvait désormais être n’importe où, tenant garnison pour l’hiver, portant des messages ou escortant un émissaire. La nouvelle l’avait abattue, ses dernières illusions lui étant arrachées, elle s’était alors abandonnée au commerce qu’elle déprisait fort jusque là. Mais au moins n’avait-elle guère à y penser, son fin visage et son regard doux suffisaient à lui attirer les hommes. Elle avait sombré un temps dans le désespoir le plus total.

Un matin, elle avait été prise à partie par d’autres prostituées qui lui reprochaient de brader ses charmes, exigeant à peine de quoi survivre, indifférente à ce qui était requis d’elle. La plus hargneuse l’avait si violemment secouée qu’elle était tombée à terre, la lèvre fendue par le choc contre une pierre. Puis les coups avaient plu sur elle, la plongeant dans une léthargie dont elle n’était sortie qu’avec difficulté, le corps endolori et l’esprit absent. Elle avait alors constaté que ses maigres affaires avaient disparu pendant son inconscience.

Alors qu’elle gisait là, hébétée et sans plus aucune attente, un manouvrier l’avait reconnue : Houdoin, un gâcheur de mortier aux mains cornées et brûlées par la chaux, dont elle avait déjà réparé quelques effets et rassasié les chairs. Il l’avait relevée et l’avait ramené avec lui, lui laissant l’usage de ses draps la journée contre l’abandon de son corps la nuit. Il n’était pas tant attentionné, mais pas brutal pour autant et lui apportait de quoi se restaurer chichement. En plus du gîte et du couchage, elle n’en attendait pas plus.

Houdoin et quelques compagnons s’étaient arrogé une échoppe avec une petite cour à l’arrière où ils pouvaient cuisiner sur un feu leurs soupes et brouets. Tandis qu’elle reprenait des forces, elle se proposa de leur entretenir le linge, tout en profitant de leur protection, de leur nourriture et de leur abri. Bien vite, elle partagea à l’occasion le grabat de l’un ou l’autre. Leurs épouses au loin, ils avaient des besoins à satisfaire, sans égards, mais sans violence. Elle s’en contenta et finit par s’habituer à son état.

Noël approchant, presque tous s’absentèrent pour retrouver leur famille, et Anceline demeura seule avec Houdoin. Un soir qu’ils savouraient un épais potage aux fèves agrémenté de lard, ce dernier se laissa aller à évoquer sa vie et ses projets. Sous ses dehors frustes, il n’était pas mauvais homme, quoique parfois empressé et autoritaire, héritage d’une existence de rude labeur.

« Et toi, commère, qu’as-tu en tête ? Tu ne saurais demeurer ici ta vie durant. Nous allons de certes prendre le chemin d’ici l’été. Tu pourrais nous compaigner.

— Cela fait long temps que tu erres en quête d’ouvrage, Houdoin ?

— Je ne sais de juste, je suis né l’année où Foulques reçut la couronne^[En 1131]. Je cours les chantiers depuis bien longtemps, mis en apprentissage que le lait me coulait encore du nez. Mais j’ai espoir de pouvoir fonder ma famille d’ici quelques paires d’ans, j’ai amassé cliquaille assez pour décider un père à me donner sa fille. Pour peu qu’elle soit honnête et bien dotée… »

Le regard qu’ils échangèrent alors fit naître un rictus sur le visage tavelé de l’ouvrier.

« Qui voudrait espouser fille des rues telle que toi ? Tu as mignonne face assez, mais c’est là bien faible dot pour fonder hostel.

— Je sais cela. À défaut d’en marier un, au moins serais-je la femme de plusieurs pour manger assez. »

À sa grande surprise, Houdoin lui accorda une caresse sur l’épaule. Puis il avala une longue rasade de vin, le regard perdu dans le vague.

« La nuit s’en vient, commère, et la lune n’est qu’un filet. Il est temps de se glisser sous la couverte. Dormons ensemble, il ne fait pas tant chaud. »

## Lydda, quartier de Samarie, matin du lundi 15 décembre 1158

Anceline frappait le sol de ses pieds afin de se réchauffer. Malgré le soleil, la température était fraîche et elle était impatiente de prendre la route. Elle avait posé à côté d’elle la hotte où elle rangeait toute sa fortune. Elle était désormais habituée des chantiers et suivait des équipes d’artisans, ne restant jamais plus de quelques mois en un lieu, s’efforçant de ne pas faire naître des espoirs qu’elle savait sans cesse déçus. Il lui était arrivé plus d’une fois de se consacrer à un seul homme, lui réservant ses caresses, mais jamais aucun n’avait eu le désir d’officialiser cela, préférant s’en tenir à des échanges commerciaux là où elle recherchait de la stabilité si ce n’était de l’affection.

Le dernier de ses réguliers, Gerbaut, l’avait accompagnée pour ce départ du chantier de la basilique Saint-Georges. Silencieux, il profitait de la proximité d’une fontaine pour puiser l’eau qu’il distribuerait ensuite aux ouvriers. C’était un portefaix aux épaules robustes, durement blessé quelques jours auparavant, un bras cassé en écharpe.

Son humeur, déjà fort maussade au naturel, s’en était encore dégradée et cela avait convaincu Anceline de faire son baluchon une fois de plus. Elle avait appris qu’une belle fête se préparait à Naplouse, à la cour de la reine Mélisende. Peut-être aurait-elle l’occasion d’y trouver de l’emploi durant l’hiver. Malgré ses défauts, elle hésitait à quitter Gerbaut. Il s’était révélé un compagnon décent quoique fort taciturne. Il était peu causant et, les quelques mois qu’ils avaient vécu l’un à côté de l’autre, il ne s’était jamais confié. Elle l’avait deviné bien plus adroit et cultivé que son embauche comme portefaix ne le suggérait et elle avait découvert dans ses affaires des outils de tailleur de pierre. Mais jamais il n’avait abordé le sujet.

Il savait malgré tout montrer de la prévenance, à l’occasion, pensant à elle pour la distribution de vêtements des chanoines au début de l’Avent ou lui offrant de temps à autre une pâtisserie. Mais à chaque fois il n’avait accompagné ces attentions d’aucune explication ni d’aucun geste qui aurait pu laisser croire qu’il avait envie d’aller plus loin. Il n’avait guère manifesté de réaction lorsqu’elle avait évoqué son désir de partir pour Naplouse. Il avait simplement indiqué qu’il s’enquerrait des caravanes afin qu’elle voyage en sécurité.

Elle n’avait pas le cœur à lever les mystères de cet homme et se contentait de survivre un jour après l’autre. Elle avait abandonné l’espoir de trouver quelqu’un à marier et savait qu’aucun établissement religieux ne voudrait d’une novice qui viendrait prononcer ses vœux sans apporter un minimum de biens à la congrégation ni pouvoir attester d’une bonne moralité. Elle s’efforçait juste de ne pas penser au lendemain.

Enfin le maître caravanier donna le signal du départ, faisant naître une agitation bruyante parmi les voyageurs. Les chameaux et les mules de bat, les chevaux des plus aisés se mirent en branle dans un grand vacarme et un dense nuage de poussière. Anceline se tourna vers Gerbaut et l’accola tendrement. Malgré toutes ses préventions, elle ne pouvait s’empêcher de s’attacher.

« Que la paix de Dieu soit avec toi, Gerbaut.

— À Dieu te mande, Commère. Garde-toi bien des périls de la route. J’espère te revoir aux beaux jours. »

Elle se figea, surprise, mais elle n’eut pas le temps d’en demander plus. Il s’écarta d’elle et, après lui avoir pressé le bras en signe d’adieu, il s’en fut sans plus un regard. Lorsqu’elle franchit la porte de la cité, elle essuya avec vigueur les larmes qui perlaient de ses paupières, oscillant entre agacement et espérance. ❧

## Notes

Route : XIIe siècle, du latin *[via] rupta* (pour un chemin percé à travers une forêt), du participe passé féminin *rupta* du verbe *rumpere*, « épuiser en imposant un effort, rompre, casser, percer ».

Le titre de ce Qit’a fait bien évidemment référence à l’excellent roman *La route* de Cormac McCarthy dont fut tiré le film éponyme avec Viggo Mortensen dans le rôle principal. Je me suis dit qu’il était intéressant de suivre les pas et cheminements intérieurs d’une femme en proie à un monde violent. Leurs traces sont souvent faibles, aussi bien dans le texte de McCarthy que dans les sources médiévales.

La suite de l’histoire d’Anceline est très largement influencée par des lectures que j’ai pu faire autour des enquêtes de Jacques Fournier lors des ultimes soubresauts de l’hérésie cathare, environ 150 ans plus tard dans le sud de la France. Aucune existence scrutée par l’inquisiteur n’est telle que ce que j’écris. Cela m’a néanmoins donné une image assez dure, mais précise, des conditions de survie des femmes qui se trouvaient reléguées à la marge de la société. Je n’ai cependant aucune certitude sur la véracité d’une pareille vision pour la Terre sainte du XIIe siècle, quoiqu’en disent les sources pour d’autres lieux et périodes proches.

Pour avoir une illustration précise et documentée d’une vie de jeune fille marginale au Moyen Âge, je vous conseille l’excellent livre de Anne Brénon, *L’impénitente*, qui suit le destin d’une hérétique à la naissance du XIVe siècle.

## Références

Brénon Anne, *L’impéninente. le roman vrai de Guillelme maury, de Montaillou*, Cahors : La Louve éditions, 2002.

Karras Ruth Mazo, *Common Women. Prostitution and Sexuality in Medieval England*, New York, Oxford : Oxford University press, 1996.

Le Roy Ladurie Emmanuel, *Montaillou, village occitan de 1294 à 1324*, Paris : Folio Histoire, 2008.

McCarthy Cormac, *The road*, New York : Alfred A. Knopf, 2006 traduit *La route*, Paris : Éditions de l’Olivier, 2008.
