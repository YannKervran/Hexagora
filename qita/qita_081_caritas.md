---
date : 2018-07-15
abstract : 1158-1159. L’ordre de Saint-Jean de Jérusalem est issu d’une initiative charitable avant même la naissance du royaume latin. Ses membres ont à cœur d’appliquer ces principes considérés comme fondamentaux par les Pères de l’Église. Est-ce que ces préceptes résisteront à la réalité d’un territoire colonisé ?
characters : Hashim, Hémeri, Frère Guillaume de Calanson, Dame Sédille, Guillemot, Frère Aimon, Frère Chrétien, Lambequin Breton, Alison, ’Unaynah al-’Abbas, Père Daniel d’Aqua Bella
---

# Caritas

## Calanson, halte caravanière, après-midi du mardi 13 mai 1158

L’impressionnante file de chameaux, mules, ânes et chevaux avait soulevé un nuage ocre gris qui faisait de toutes choses des statues. Les aboiements des chiens, les cris des hommes se perdaient dans les volutes de poussière qui partaient à l’assaut des maisons. L’agitation se concentrait autour des bassins où les bêtes pouvaient boire à tour de rôle. Se frayant un passage à force de coude, des muletiers puisaient de quoi abreuver leurs animaux, tenus à l’écart par le maître de la caravane principale.

Hashim, un garçon d’une dizaine d’années allait et venait, une vaste calebasse dans les mains, depuis la petite file distraitement surveillée par son père ‘Unaynah al-‘Abbas, des ânes, pour l’essentiel, qui portaient des sacs de pois et fèves. Ils avaient rejoint le convoi à Lydda et l’accompagnaient jusqu’à Césarée, où leur chargement était attendu. Ils bénéficiaient de l’escorte d’un fort groupe de marchands, de quelques pèlerins et voyageurs et même d’une demi-douzaine d’hommes en armes, habillés du noir manteau de Saint-Jean de Jérusalem.

Le garçon trottinait sans relâche tandis que ‘Unaynah échangeait gaiement avec ses compagnons. Il avait sympathisé avec quelques muletiers, partageant avec certains d’entre eux le goût du vin et des plaisanteries grasses. Une nouvelle fois Hashim se retrouvait donc obligé de surveiller les bêtes et de donner des instructions à ses deux petits frères. Il avait l’habitude de cela, qu’il préférait à devoir demeurer aux abords de son père, qui avait souvent la main leste quand il lui semblait devoir faire montre d’autorité. Il n’était en général qu’indifférence envers ses enfants, sauf lorsqu’il craignait qu’on ne reconnaisse pas sa valeur. Frapper les membres de sa famille lui paraissait le moyen le plus pertinent pour valider auprès des autres la bonne opinion qu’il se forgeait pour lui-même à travers l’oisiveté et la boisson.

Lors des haltes, il se trouvait toujours quelques mains curieuses qui tentaient de délacer des liens, de trancher des courroies ou de percer les toiles dans l’espoir de récupérer des denrées. Les grosses balles, les amphores de grande taille et les tonneaux ne craignaient rien, mais les cargaisons de moindre importance, en sacs, constituaient la cible de prédilection des chapardeurs. Il fallait donc porter attention aux personnes qui approchaient des animaux. Tout en allant et venant depuis l’auge de pierre, Hashim s’assurait que nul ne venait trop près de son petit groupe.

Un instant il lui sembla qu’une ombre s’était infiltrée au milieu d’eux et il se retourna précipitamment, donnant sans le vouloir un coup de coude à un des ânes. Celui-ci répliqua d’un mouvement de tête agacé, le projetant contre son voisin, qui répondit à cela à l’aide d’un sabot postérieur, propulsant Hashim dans les graviers, cul par-dessus tête, dans une gerbe d’eau et un envol de calebasse.

L’animal ayant accompagné sa frappe d’un long braiment d’avertissement, tout le monde vit le garçon le nez dans la poussière. Il voulut se relever immédiatement, mais, sonné, il n’y parvint pas. En outre, une violente douleur à la jambe le fit lourdement retomber au sol à demi inconscient. Deux caravaniers vinrent à lui et l’aidèrent à reprendre ses esprits, lui proposant de l’eau. Un des membres de l’hôpital, sorti de la commanderie voisine pour échanger avec ses frères, s’approcha à son tour. Il était habillé comme un Latin, cotte et chausses, mais portait le manteau distinctif et une tonsure burinée de clerc. Son fin visage en lame de couteau, prolongé d’une barbe drue aussi noire qu’il était possible, reflétait une austérité démentie par sa silhouette empâtée. Il s’agenouilla près de l’enfant, parlant sans difficulté dans la langue des locaux.

« Demeure ainsi un temps, garçon. As-tu douleur en aucune partie de ton corps ?

— Mon genou me cause grand dol ! »

D’un geste expert, le moine releva le vieux thawb qui faisait office de vêtement au gamin, en prenant bien garde de ne pas le remonter trop haut. Il connaissait la pudeur des Orientaux. La jambe était griffée et un gros hématome ornait le bas de la cuisse. Avec le gonflement, on ne voyait déjà plus la rotule. Le clerc grimaça.

« Il te faudrait patienter un temps au repos, alité de préférence. Si rien n’est cassé, tu seras guéri dans quelques jours. »

Hashim fit une moue, luttant contre la douleur lorsqu’une ombre vint leur cacher le soleil.

« Allez, debout, vaurien. Les bêtes ont encore soif ! »

Le père de Hashim ne semblait nullement ému de la blessure de son fils et ne lui accorda pas plus d’un regard, tournant rapidement les talons pour aller se poster à l’ombre. L’hospitalier le rejoignit en quelques foulées.

« Pardonnez-moi, mestre. Peut-être n’avez vous pas vu que l’enfant est blessé, il vient de recevoir mauvais coup d’une des bêtes.

— Et alors ? J’y peux quoi ? Il faut que les ânes boivent avant qu’on reparte !

— Il lui faudrait quelques jours alité, le temps que sa jambe guérisse.

— Peuh ! On se remet mieux en forçant, c’est connu. Sans quoi on devient mou et plus bon à rien.

— Laissez-moi au moins le faire voir à un de nos frères savant en simples et onguents. »

Leur altercation commençait à attirer les regards. ‘Unaynah ne se sentait jamais en confiance dans ces cas-là, ce qui faisait monter sa voix dans les aigus et agitait son corps de tremblements.

« Je vous connais, les frères de Yahya[^yahya] ! Vous ensorcelez les esprits et voulez me voler mon fils ! Pas de ça avec moi ! Laissez-le tranquille ! »

L’hospitalier en fut muet de stupéfaction, ne sachant que répondre. Il laissa l’homme repartir tandis que le gamin s’esquivait en boitillant. Un autre frère, son doyen de plusieurs décennies s’approcha, les sourcils froncés.

« Certains n’aiment guère la charité qu’on leur propose…

— Quel abruti que ce vaunéant ! Il mériterait de se voir botter ainsi que son gamin !

— Voyons, frère Hémeri, est-ce là toute votre charité ? Le père la mérite tout autant que le fils et peut-être plus.

— Le gamin risque de finir boiteux. Il ne lui aurait rien coûté de nous le laisser quelques jours !

— Sa colère est peut-être à l’aune de son attachement, qu’en savons-nous ? Nos portes sont ouvertes, mais il ne serait guère amiable d’obliger à y entrer…

— Mouais, mon vieux aurait dit qu’on ne fait pas boire un âne qui n’a pas soif. Pour autant, un qui boit trop ne vaut guère. »


## Beith-Gibelin, église paroissiale, fin d’après-midi du mercredi 12 novembre 1158

Une pluie fine avait rabattu la foule amassée devant le château dans l’église paroissiale. Les voûtes y résonnaient des discussions animées, l’arrivée d’une belle caravane depuis Jérusalem étant toujours un événement. Dame Sédille avait accueilli avec sa faconde habituelle son beau-frère, récemment veuf de sa sœur, avec un bébé en très bas âge. Comme elle venait aussi d’accoucher et se savait suffisamment allaitante, elle avait proposé à Guillemot de lui apporter les enfants. Pour elle, quatre bouches en plus ne posaient aucun problème. Malgré sa petite taille, elle accomplissait toutes ses tâches ménagères avec l’énergie et l’obstination obtuse d’un sanglier, sans en avoir le caractère acariâtre.

Ils avaient suivi la foule dans le bâtiment, échangeant les nouvelles des derniers mois. La jeune femme s’ébaudissait des grands enfants qu’elle accueillait et se disait impatiente de les voir batifoler avec les siens dans les jardins. Comme ils ne savaient pas quand la caravane arriverait, elle avait laissé ses deux garçons accompagner leur père. Ils aimaient à se rendre au terrain qu’ils cultivaient, surtout quand c’était, comme aujourd’hui, avec la perspective de monter quelques murs.

« Peut-être finiront-ils maçons ou carrier, plaisanta Guillemot.

— J’espère en tout cas que ce sera solides travaux, car ils n’y entendent guère aux choses délicates. Je suis bien aise de voir arriver Esdeline et cette pauvre petite Perronele. Je me sens parfois bien seule au milieu de tous ces hommes. »

Tout en parlant, elle berçait l’enfant qu’elle venait d’allaiter. N’ayant aucune nourrice qui puisse l’accompagner, Guillemot avait pris du lait de chèvre pour le voyage. Découvrant le biberon donné par des mains bien peu expertes, le bébé n’avait pas avalé grand-chose. Après deux journées difficiles, il était désormais enfin repu et dormait à poings fermés malgré le vacarme ambiant. L’hôpital de Saint-Jean avait fait venir un large groupe d’orphelins et déshérités divers, dont la garde serait confiée à des colons du casal. On attendait donc Frère Aimon, le commandeur local, qui indiquerait quels enfants étaient destinés à quelle maison. Dans l’intervalle, les familles d’accueil et les gamins échangeaient des regards circonspects, espérant anticiper qui ils rejoindraient parmi ces inconnus.

Frère Pons arriva accompagné du cellérier, Gilebin. Ils allaient souvent par paire, l’athlétique silhouette de frère Pons dominant la petite stature de son compagnon. Ce dernier compensait la différence de gabarit par une agitation perpétuelle des membres, des yeux et, surtout, de la langue. Il était réputé pour aimer discuter et le vide se faisait généralement devant lui lorsqu’il s’adonnait à une balade dans les environs. Pour être agréable d’abord, il n’en était pas pour autant capable de s’intéresser à autre chose que ses propres soucis et s’efforçait d’en partager le détail avec quiconque avait au moins une oreille en assez bon état.

Le commandeur appela au calme de sa voix habituée aux combats. Il n’était pas des frères issus du couvent, mais un fils de noble qui avait choisi d’embrasser la carrière ecclésiastique. Il se murmurait que c’était suite à un chagrin d’amour, mais personne n’en avait l’assurance.

« Frères et sœurs du casal, nous allons procéder à la répartition des enfançons. Avant cela, je tenais à vous rappeler quelques détails. »

Il invita Gilebin à le rejoindre devant l’autel, celui-ci enchaîna de sa voix grave.

« De prime, n’oubliez pas de passer au cellier après, nous vous donnerons du linge de lit pour chacun, un drap de laine, un coussin et un sac de coton. Chaque année, pour les enfants ayant à peu près dix ans révolus, nous fournirons à la Toussaint du linge de corps, robe et chemise. »

Il fit traîner un regard lourd de reproches.

« Il ne s’agit pas d’en dessaisir les enfants pour vendre leur toile au fripier comme certains ont tentation à le faire. Le drapier connaît ses toiles et saura si un gamin n’a pas sur le dos ce qui lui appartient. »

Le commandeur enchaîna de façon naturelle, d’une voix tout aussi autoritaire.

« Je vous fais également remembrance que ce sont enfantelets de Dieu et pas esclaves païens ! Les abus seront punis. Qu’un garçon dont la taille approche celle d’un homme soit envoyé aux champs pour y apprendre le labeur des terres, soit, mais j’espère bien voir courir et s’amuser en notre casal les plus jeunes que nous voyons ici. Ce que vous faites au plus modeste de ces garcelets, vous le faites à Dieu ! »

Sedille chuchota à Guillemot que certaines familles avaient cru qu’accueillir quelques enfants leur ferait de la main-d’œuvre à pas cher, mais l’Hôpital veillait généralement au grain. Un couple s’était vu infliger une forte amende dans un casal au nord, pour avoir profité d’une fratrie qui leur avait été confiée. Malgré cela, les abus étaient fréquents et les plus résolus des gamins placés s’enfuyaient, nourrissant la misère des gens de peu qui erraient par les routes.

Guillemot écoutait d’une oreille distraite les commentaires de sa belle-sœur. Effrayé par le destin réservé aux orphelins sans famille, il comptait bien trouver rapidement une nouvelle épouse, de façon à reprendre ses enfants et leur garantir un avenir. Le mari de Sédille était forgeron et s’était installé de belle façon. Basyle, Andri, Esdeline et Perronele s’épanouiraient certainement chez eux, surtout avec leurs cousins et cousines. Mais Guillemot craignait toujours un malheur. Il estimait l’endroit trop près de la frontière, ne s’était pas encore fait à l’idée que la prise d’Ascalon avait reporté celle-ci par-delà le désert. Les assauts égyptiens récents lui donnaient d’ailleurs raison, mais il n’avait guère le choix. En outre, il avait pu constater de ses yeux que l’Hôpital de Saint-Jean dépensait sans compter pour fortifier la zone et s’employait à renforcer leur propre établissement. Cela ne le rassurait qu’à moitié.

Frère Aimon achevait son sermon et le cellérier appelait désormais les enfants pour leur indiquer la famille où ils iraient. Certaines des femmes s’indignèrent lorsqu’elles apprirent que des fratries avaient été divisées, les plus benjamins étant demeurés à Jérusalem chez des nourrices familières de Saint-Jean. Le commandeur leur garantit qu’on les réunirait dès que les plus jeunes seraient sevrés. L’ordre estimait peu pratique de faire voyager des bébés dans leurs langes.

En voyant les regards inquiets, les gestes maladroits, les visages apeurés, Guillemot soupira. Il n’avait toujours pas osé demander audience au vicomte pour s’enquérir de tâches subalternes non militaires auxquelles il pourrait être affecté en priorité. Il n’avait pas souvent été envoyé au combat, mais cela lui était arrivé de temps à autre. Désormais, il craignait que ses enfants ne connaissent le sort des orphelins autour de lui.

## Château Emmaüs, salle capitulaire des frères de Saint-Jean de Jérusalem, après-midi du dimanche 7 juin 1159

Bien que l’été ne soit pas encore là, le soleil s’abattait avec force dans le modeste cloître de Château Emmaüs. Reculant devant la chaleur, Frère Chrétien avait invité ses visiteurs âgés, Lambequin le Breton et son épouse Alison, à venir s’asseoir dans la petite salle capitulaire. La lumière pénétrait par la porte ouverte, mais la pierre conservait de l’hiver fraîcheur et humidité. L’hospitalier leur avait indiqué les bancs et s’était installé sur un escabeau face à eux. Il avait l’habitude de les voir passer lors de leurs fréquents périples sur les traces du Christ et de ses disciples. Année après année, ils venaient, désormais en palanquin après avoir longtemps erré en mules.

Il connaissait le couple depuis son enfance. Riches artisans, ils avaient employé son père dès son adolescence dans leur établissement de travail des peaux. Lambequin avait également été le parrain de chacun des membres de la famille lors de la conversion de son commis au catholicisme. Et, pour finir, ils avaient permis à Chrétien de poursuivre de belles études quand ils avaient appris qu’il souhaitait devenir clerc. Ils le considéraient comme un des leurs, d’autant qu’aucun de leurs enfants ou petits-enfants ne partageait leur passion religieuse ni n’avait choisi la voie sacerdotale. Ils pèlerinaient presque chaque année, parfois pendant plusieurs semaines et ne regrettaient qu’une chose : de n’avoir pas pu aller à Rome plus tôt. Ils s’estimaient désormais trop âgés pour partir si loin sur les routes. Ils venaient consulter Daniel à propos du dernier voyage qu’ils souhaitaient faire, chacun de leur côté, quoiqu’unis en esprit.

Ils avaient l’intention de remettre à l’Hôpital tous leurs biens immobiliers restants, un bel hôtel à Jérusalem et une propriété rurale dans un casal des environs, avec tous les droits qui y étaient attachés. Ils ne savaient pas s’il était plus adéquat de vendre le tout et de faire don de la somme ou s’il valait mieux faire offrande des biens. En échange, ils demandaient à être tous deux reçus dans l’ordre afin de terminer leurs vies en prière.

Frère Chrétien les avait écoutés attentivement, sans mot dire, hochant la tête.

« C’est faire là grande charité à Saint-Jean que de vouloir faire si généreux don… Ne craignez-vous pas que Pierre ou Jacques n’y trouve occasion de querelle ?

— Chacun de mes enfants a eu sa suffisance. Il me semble que bien peu peuvent se targuer de partir avec autant de biens dans la vie. Le surplus ne ferait que les gâter. Je préfère en faire la part à Dieu, pour les nécessiteux et les malades. »

Lambequin postillonnait quand il parlait et sa surdité l’incitait à parler plus fort que nécessaire. Alison, tout aussi malentendante que lui, hochait la tête en remâchant les mots de son époux. Elle l’avait suffisamment pratiqué pour savoir ce qu’il disait quand bien même elle n’aurait plus eu de son du tout.

Chrétien avait beaucoup d’affection pour les deux vieillards et se trouvait gêné de les inviter à plus de modération dans leur générosité. Se dessaisir de tous les biens qu’il leur restait serait certainement fort apprécié, mais leur demande d’être reçus dans l’ordre en retour risquait de poser problème.

« La seule difficulté que je vois en tout cela, c’est votre désir de rejoindre notre ordre. Non pas que vous n’en soyez pas dignes, loin de là. Mais parce qu’il est fort rare que cela se fasse en ses vieux jours. Nous sommes tournés vers le siècle assez, ce qui demande régulier et important labeur.

— N’y a-t-il pas frères et sœurs d’oraison en vostre maison ?

— Nous avons les chapelains, prêtres ordonnés, mais ils s’adonnent aux offices des frères et parfois de nos paroisses. Il s’agit de servir bien différemment des frères noirs ou blancs… »

Le vieux Lambequin se tut un moment, plus ennuyé que contrarié. Il avait attendu de cette entrevue qu’elle lui permettrait d’aborder les derniers détails pratiques qui lui ouvriraient les portes d’un couvent où finir ses jours. Son désappointement se mesurait à l’aune de son silence. Frère Chrétien lisait leur détresse à l’idée de se voir refuser l’avenir céleste qu’ils espéraient, par une vie achevée en perpétuelles oraisons.

« Si j’étais vous, je conserverais une part de mes biens pour me doter en vue de joindre une communauté de Cluny, voire de Citeaux. Ainsi garnis, vous aurez bon accueil de leur part et pourrez vous cloîtrer ainsi que vous en avez désir.

— Ont-ils quelque établissement en ces lieux ? Nous ne sommes pas tant vaillants pour traverser les mers.

— De certes, il s’en trouve de très bonne fame, à Jérusalem même et aux abords.

— Mais ils ont moins grande pratique envers les pauvres et les nécessiteux. J’aurais préféré que cela soulage les maux des plus modestes, plutôt que d’engraisser un abbé. »

La remarque fit sourire Chrétien. Il était de bon ton de critiquer les chefs d’établissement religieux dont les appétits terrestres étaient quelquefois amplement nourris, au détriment, craignait-on, de leurs aspirations célestes. Selon les endroits, la discipline était un peu relâchée, mais il demeurait difficile de faire la part entre légende et réalité, le mur de la clôture protégeant les frères des regards du monde. Quoi qu’il en soit, l’hospitalier conservait une certaine confiance dans la rigueur des maisons bénédictines.

« Ne vous y trompez pas, beaucoup ont un hospice digne des nôtres. Il s’en trouve moins en ces lieux, voilà tout. Certains sont peu désireux de se voir obligés de frayer avec des hommes de guerre, ainsi que nous le faisons. Ils se contentent donc de s’ouvrir un peu au monde en des terres moins dangereuses. »

Le vieux Lambequin hocha la tête, à demi convaincu. Il n’était guère habitué à devoir remettre en question ses décisions, surtout pour un point aussi essentiel. Il sourit poliment à son filleul, lui tapotant la main en un geste familier.

« Aurais-tu quelque frère à me conseiller dans un de ces ordres ? Il va me falloir pourpenser tout cela. »

## Calanson, cellier des frères de Saint-Jean de Jérusalem, matin du jeudi 25 juin 1159

Frère Guillaume, encore cellérier en titre pour quelques jours, claudiquait, sa canne à la main. Les violentes fièvres de l’hiver l’avaient bien diminué et il toussait régulièrement, s’essoufflant au moindre effort. Il avait été convenu de le transférer à Aqua Bella, en charge du cellier une nouvelle fois, mais en sachant qu’il n’y avait là-bas que fort peu de travail par rapport à Calanson. C’était un lieu de retraite, de repos pour les blessés et malades d’importance de l’ordre. Il avait accueilli cette promotion avec hésitation. Il reconnaissait et appréciait l’honneur qui lui était fait, lui qui n’était que modeste fils de paysan, mais il n’avait jamais envisagé de quitter Calanson et pensait y finir ses jours tranquillement.

Il avait fait de sa cellule la maison dont il avait toujours rêvé, y entreposant au fil des ans de menus objets, sans valeur pour la plupart, mais auxquels il était fort attaché. Il lui avait donc fallu opérer un tri cruel, persuadé qu’il ne pourrait guère en emporter avec lui, l’ordre prônant la pauvreté de ses membres. Il ne savait pas comment serait son supérieur, mais il doutait qu’il soit bien vu d’arriver avec malles et coffres ainsi qu’un baron en campagne.

Il avait passé les derniers jours à énumérer ses tâches à frère Hémeri, son successeur. Cette nomination était connue depuis longtemps et frère Guillaume avait été consulté à ce propos, mais cela demeurait néanmoins un déchirement pour lui d’abandonner ces bâtiments si familiers, qu’il parcourait une ultime fois en boitillant, espérant se remémorer tous les détails négligés en voyant le lieu de son labeur. Il désigna une vaste amphore, enterrée dans une petite réserve où ils conservaient de la céramique commune, des jarres de manutention et des vases de moindre valeur.

« J’ai usage de verser là les restes d’huile de l’année, pour en faire usage d’éclairage. S’il s’en vient quelque vilain des manses environnants qui en aurait usage, on peut y puiser quelques pintes. Il ne faut pas en user pour les lampes d’autel ou les logis d’importance, elle est trop rancie pour cela. »

À ses côtés, Hémeri suivait sans mot dire. Frère Guillaume se félicitait de ce choix, persuadé que le jeune homme était organisé et pointilleux. Il était malgré tout un peu étonné de le voir manifester si peu d’enthousiasme à recevoir un tel office. Beaucoup se seraient enorgueillis d’une pareille distinction et Guillaume préféra attribuer cette placidité à une modestie de bon aloi. Il lui aurait été insupportable de penser qu’il serait relayé par un clerc de peu de foi, de talent ou d’abnégation.

Ils passèrent devant la bergerie où quelques brebis et leurs agneaux étaient gardés pour garantir du lait.

« Je ne sais plus si je te l’ai déjà dit, mais j’ai usage d’accepter de la laine de la part des Bédouins qui ne peuvent donner ouailles en suffisance pour les droits de pacage. Nous n’avons pas tant d’herbe, j’aime autant des toisons pour nos linges. »

Il marqua une pause, levant un doigt tandis qu’il réfléchissait.

« À ce propos, il me faut t’établir la liste des manses à qui nous commandons le fil à tisser. Je t’y indiquerai le poids de laine donné à chacun. »

Il stoppa un instant, reprenant son souffle alors qu’ils arrivaient devant le cellier principal.

« Parlant de laines, ne laisse pas partir toutes tes couvertures à l’approche de l’hiver. Avec la clôture des mers, il s’en vient moins de voyageurs et le frère cellérier à Jérusalem te fera certement pressantes demandes. Mais à trop te dégarnir, tu ne sauras accueillir comme il se doit tous ceux qui débarqueront au printemps. J’ai usage de retrancher au moins le décime de ce que demande Jérusalem. Idem pour le foin. Il se trouve toujours plus de besoins que de demande, comme la paille, donc je vais parfois jusqu’à deux décimes, selon les années. »

Ils arrivèrent près des greniers, silos en céramique semi-enterrés, où ils stockaient les grains, semences de l’année à venir et fournitures pour le pain et les bouillies quotidiennes.

« Je ne sais plus si tu en as remembrance, mais l’an passé, j’ai dû rappeler à quelques ra’is[^rais] de nos casals que les grains à glaner après foulage et battage doivent être abandonnés aux plus pauvres. Garde-les à l’œil, certains ont la cupidité rivée au corps. Si jamais tu as des soucis de bornage avec eux, n’hésite pas à aller consulter les shaykhs[^shaykh], ils sont souvent moins avides et plus soucieux de leur communauté. »

Ils revinrent à la porte principale, plissant les yeux devant la forte lumière, annonciatrice d’une chaude journée. Les mains sur les hanches, Hémeri envisageait le territoire dont il aurait désormais la supervision économique. Il soupira, soucieux du travail que cela demanderait et inquiet à l’idée de ne pas être à la hauteur. Il ne s’était fait clerc que par dépit et choisi l’Hôpital pour ne pas se voir cloîtré. Il fréquentait d’ailleurs de temps à autre des établissements où il dissimulait sa tonsure sous un bonnet, un peu honteux de s’adonner à ses vices alors qu’il était censé montrer l’exemple. Il ne se sentait juste pas à sa place et aurait préféré devenir simple paysan, avec femmes et enfants. Mais sa famille était trop pauvre pour qu’il puisse avoir sa terre et il avait connu trop de mauvais maîtres pour se faire valet. Au moins bénéficiait-il d’une relative liberté tant qu’il donnait les bons signes de soumission.

Frère Guillaume montra un des bâtiments du casal accolé à leur maison forte.

« Il faudra penser à faire retailler la meule du moulin Saint-Jean. J’en ai fait déjà la demande par trois fois, sans que cela n’aboutisse. Quelques vilains grognent qu’elle donne plus de poussière que de farine. Et ils n’ont pas tout à fait tort. »

Il cherchait quelque chose à dire encore, souhaitant prolonger le moment le plus possible. Puis il baissa la tête, dénoua de sa ceinture le lourd anneau de fer où les clefs étaient amoncelées et le tendit à Hémeri.

« Tu as déjà été nommé par le chapitre et je n’ai gardé cela que par déférence pour mon labeur passé. À toi de porter l’annel du cellérier désormais. Puisse-t-il ne pas trop peser !»

## Aqua-Bella, cellule de père Daniel, veillée du mercredi 18 novembre 1159

Le froid descendu des monts de Judée avait gagné le cœur du vaisseau de pierre attaché à ses pentes. Des braseros avaient été disposés dans la chambre des malades et les frères avaient revêtu une seconde robe par-dessus la première. Malgré les frimas, le père Daniel conservait l’habitude de porter des sandales, même avec des chausses. Il ne s’aventurait de toute façon que fort peu au-dehors et ne risquait pas de se mouiller ou de se salir les pieds.

Il avait malgré tout installé une couverture sur ses genoux. Il ne faisait apporter aucun chauffage dans sa cellule, persuadé que la rigueur qu’il s’infligeait, bien que son corps y rechignât, serait de bonne influence sur ses frères. Pour le coup, cela incitait la plupart à décliner les séances de travail à ses côtés, préférant la douceur de l’infirmerie.

Frère Guillaume, le nouveau cellérier parcourait à mi-voix une tablette de fournitures demandées par le château de Belmont, dont ils dépendaient. Daniel l’avait convoqué, car il avait besoin d’un avis plus posé que le sien. Il trouvait chaque fois les réquisitions de frère Pons plus gourmandes que précédemment, sans que cela soit au service d’une cause qu’il appréciait. Son supérieur était en effet un soldat, un chevalier cadet de famille qui s’était engagé dans l’ordre pour y mener bataille, lance en main. Le père Daniel était un religieux plus soucieux de charité et, plus encore, d’oraisons. Il se méfiait de ses agacements épidermiques, dont il craignait que cela ne devienne préjudiciable à des relations, sinon cordiales, du moins apaisées.

Frère Guillaume reposa la tablette, écartant la lampe à huile qu’il avait approchée pour gagner en clarté.

« Il y a certes forte quantité de paille pour notre maigre domaine. Il ne s’agirait pas que cela aille croissant chaque année. Il nous faut garder de quoi faire les grabats de nos valets, garnir nos écuries et refaire paillasses des lits de temps à autre. Si nous devons par la suite débourser pour nos besoins, cela serait de nul intérêt.

— J’en suis bien d’accord. C’est là demande pour forte cavalerie, plus qu’il n’en a pour le moment. Adoncques, je ne serais guère surpris qu’il escompte l’augmenter.

— Il demeure qu’il assure nombreuses patrouilles depuis la route maritime jusqu’à la Cité, sans même parler des chevauchées dans les montagnes. »

Le prieur lança un regard soupçonneux à son cellérier. Se pouvait-il que ce soit un homme de frère Pons ? Ce dernier évoquait si souvent les patrouilles comme justification à toutes ses demandes que Daniel en venait à ne plus supporter d’en entendre parler. Inconscient des pensées qui agitaient son supérieur, frère Guillaume poursuivait ses réflexions à voix haute.

« Cela étant, je ne vois néanmoins pas pourquoi nous devrions fournir la toile d’un pavillon et de deux grebeleures[^grebeleures]. Les tentes sont de nul usage en escorte.

— C’est ce qui me fait dire que c’est là demandes nouvelles pour fournir une échelle[^echelle]. »

Frère Guillaume hocha la tête. Il n’était pas aussi détaché du siècle que le prieur et avait pu apprécier l’importance des hommes d’armes dans leur ordre. Il les considérait comme un mal nécessaire qui permettait de ne pas avoir recours à des expédients soudoyés, dont on n’était jamais certain ni de la vaillance ni de la tempérance. Malgré tout, il appréhendait les choses en gestionnaire et s’inquiétait de voir dépossédé un domaine dont il avait en partie la charge. Pour être moins prestigieuse, la pratique de la charité, y compris envers leurs frères indigents, ne devait pas céder le pas devant les menées guerrières. À ses yeux, c’était la vocation à accueillir qui avait entraîné la fondation de leur ordre, et tout le reste n’en était qu’un des aspects plus ou moins utiles, mais toujours subordonnés.

Prenant le silence de Guillaume pour un acquiescement à sa remarque, Daniel continua.

« Bientôt il nous fera demande de hauberts ou de glaives, j’en ai la crainte. Allons-nous acheter harnois de guerre avec les pieuses offrandes faites à nous ? Christ ressuscité en le chemin d’Emmaüs a-t-il demandé à ses disciples un destrier pour aller semer la mort au parmi de ses assassins ? Non, il a demandé l’héberge ! J’ai fort grandes craintes pour l’avenir que certains dans notre ordre aient oublié ce fait essentiel…

— Je ne suis pas tant versé que vous en ces points de foi ou en la connaissance des textes saints. Je sais juste que j’ai vu plus de bien naître des repas que j’ai pu distribuer que des violences nées des batailles.

— Voilà sages paroles. Fort heureusement, il s’en trouve de fort savants qui opinent en ce sens à Paris ou Chartres. Puissent leurs voix porter plus fort que les tambours et cors de guerre ! Pierre Damien professait que l’épée de l’esprit, la parole de Dieu, était supérieure au glaive du siècle et que l’Église ne devait pas se soucier de batailleuses querelles. Je crains que le grand abbé Bernard[^saintbernard] n’ait eu tort sur ce point. Il n’est de si grand esprit qu’il ne puisse se fourvoyer jamais.

— Amen » opina Guillaume. ❧

## Notes

La notion de charité était au cœur de nombreuses institutions ecclésiastiques au XIIe siècle et bien sûr en particulier des frères de l’Hôpital. Il ne faut néanmoins pas y mettre le sens commun d’aujourd’hui, car cela correspondait à un aspect essentiel et plus vaste de la doctrine catholique. Il ne s’agissait pas du tout de *faire la charité* comme on peut le comprendre maintenant, avec toute la condescendance et le mépris qui peuvent s’en dégager, mais plutôt de se montrer *charitable*, bienveillant envers son prochain, considéré comme à l’image de Dieu. Les notions d’accueil, de soin aux plus pauvres et aux démunis étaient donc dépendants de ce précepte tant vanté par Paul de Tarse (1 Corinthiens 13:1-7). C’était lié également aux valeurs de solidarité et de fraternité au sein de la communauté. Les frères de Saint-Jean pratiquèrent cette vertu avec ampleur, avant même de devenir un ordre militaire.

Des voix s’élevaient d’ailleurs dans l’Église, à la fois pour s’inquiéter de ce mélange des genres peu aisé, mais aussi plus largement contre la violence, quelle qu’elle soit, y compris envers des croyants d’autres religions, voire contre des puissances guerrières adverses. Cet évangélisme prônait parfois l’exemplarité du martyr comme une voie vers un monde meilleur, préférable à l’adhésion aux règles brutales d’un environnement violent. Cette résistance à l’hypothèse que la guerre pouvait être juste, mais certainement pas sainte, participa à la désaffection progressive de l’idée de croisade.


## Références

Aurell Martin, *Des Chrétiens contre les croisades. XIIe-XIIIe siècles*, Paris : Fayard, 2013.

Aurell Martin, (page consultée le 15 juillet 2018), *Des pacifistes au temps des croisades*, conférence donnée le 2 mai 2017 à l’auditorium de la Cité des sciences et de l’industrie, [En ligne]. Adresse URL : https://www.dailymotion.com/video/x5oesuc

Riley-Smith Jonathan, *The knights of St John in Jerusalem and Cyprus c. 1050-1310*, Londres : McMillan, 1967.
