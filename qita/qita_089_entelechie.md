---
date : 2019-03-15
abstract : 1157-1159. Jeune fille, Libourc ne voit pas son avenir autrement que liée à un homme dont elle tiendra la maison. Malgré toutes ses prévenances, son sérieux et son application, la temporalité de son établissement en tant que femme maîtresse de son foyer est loin de s’accorder avec ce dont elle rêve depuis son enfance.
characters : Libourc, Ernaut, Sanson de Brie, Mahaut, Marguerite, Père Giriaume
---

# Entéléchie

## Casal de Mahomeriola, fin d’après-midi du mardi 30 juillet 1157

À l’ombre d’un colossal jujubier, un groupe de jeunes filles s’activait à peigner et carder des écheveaux de laine tout en échangeant potins, nouvelles, blagues et chansons. Les tremblements de terre dont on rapportait, au hasard des sources, qu’ils avaient détruit la plupart des villes du Nord ou n’avaient en rien affecté la moindre des cités, n’avaient que rapidement occupé leurs discussions. Elles étaient bien plus intéressées par les frasques de quelques séducteurs locaux ou le passage des colporteurs proposant colifichets, rubans et boutons ornés. Sans forcément l’être en leur cœur, elles jouaient aux frivoles, s’éprouvant les unes les autres à l’aune de leur audace à enfreindre, soi-disant, les règles de vie qu’on leur imposait.

Quelques jeunes mariées fréquentaient leur cercle, les premiers mois de leur union, mais bien vite elles s’excluaient d’elle-même, abandonnant cette période d’insouciance pour la dure réalité de la vie domestique. Avoir « pot et feu » à soi obligeait à un dur labeur de l’aube au crépuscule. Elles avaient néanmoins le temps de venir alimenter d’anecdotes fort peu chastes leurs premières semaines à partager chaque jour la couche d’un homme.

Certaines, comme Marguerite, traînaient une réputation sulfureuse, qu’elles entretenaient par leurs confidences, mi-réelles, mi-fantasmées, et par leur impudence dans le récit qu’elles en tissaient. Libourc, bien qu’effacée, appréciait ces moments de licence où elle pouvait enfin s’affranchir de la tutelle parentale. Même si elle était finalement parfois un peu choquée par ce qu’elle entendait, elle se risquait de temps à autre à une répartie qui lui faisait flamber les joues.

Marguerite ne se cachait pas de ses nombreuses entorses à la conduite qu’on attendait d’une jeune femme et, malgré ses seize printemps, elle revendiquait une expérience qui affolait autant qu’elle piquait la curiosité. Benjamine d’une longue fratrie, elle savait sa dot fort maigre et semblait compter sur d’autres appâts pour ferrer le mari qui lui donnerait des enfants et son consentement devant l’église, de préférence en ordre inverse.

Après une période de calme relatif, quand Mahaut était passée déposer de nouvelles toisons de laine à sa fille, Marguerite se tourna vers Libourc, l’air malicieux.

« Ma jolie, tu ne nous en a guère conté sur le beau colosse qui s’en vient te voir fréquentes fois à la messe ?

— Qu’aurais-je à en dire ?

— Eh bien, le plus petit détail nous siérait fort ! Du moins, j’ose encroire qu’il n’est pas si menu… »

Sa saillie déclencha l’hilarité du groupe, y compris de Libourc dont les joues s’ornèrent d’un fard gêné.

« Que vas-tu là penser ? C’est juste bon ami, qui s’en vient nous visiter, mes parents et moi.

— Allez, ne te fais pas prier, compaigne ! Ce sont détails de ses visites qu’il te fait dont j’ai appétance. Si bel ouvrier doit avoir outil adroit ! N’arrivez-vous jamais à échapper à la veille de la Mahaut ? S’il ne te plait pas, j’en connais qui pourraient en croquer ! Et quand je dis croquer…

— Eh là, tout doux, ce n’est pas bestiau qu’on achète à la criée, s’anima Libourc. Il est…

— Ah, la voilà goupil qui défend son poulet ! Donne-nous donc des détails ! Le mât est-il proportionné à la nef ? Ce serait misère qu’il n’y ait pas bon carillon dans les braies de si beau beffroi ! »

Encore une fois, les rires ne laissèrent que peu de possibilités à Libouc de répondre. Elle-même oscillait entre amusement et agacement. Elle savait Marguerite sans malice, mais était mal à l’aise d’être ainsi exposée. Surtout que, si elle trouvait en effet Ernaut bien à son goût, c’était essentiellement parce qu’il lui témoignait un grand respect et une tendresse qu’on n’aurait pu soupçonner sous des dehors d’ours parfois mal dégrossi. Et si elle y avait pensé à l’occasion, jamais ils n’avaient eu l’occasion d’avoir le moindre rapport intime. Rien qu’à l’idée, son cœur s’affolait, de frisson autant que de crainte.

Elle savait de façon très théorique comment cela se passait, mais ne faisait pas bien le lien entre ce qui devait se dérouler pour elle et ce qu’elle pouvait en voir parmi les bêtes  chaque jour autour d’elle. Sans être vraiment effrayée des sermons qu’elle avait pu écouter de la part des prédicateurs ou des sous-entendus maternels, elle conservait une appréhension pour le moment où les choses se dérouleraient en pratique. Pareils sujets lui semblaient tellement difficiles à évoquer qu’elle en savait gré, finalement, à Marguerite, de les rendre moins angoissants, en les faisant objets de plaisanterie. La question lui paraissait délicate à aborder en termes clairs et précis avec quiconque en dehors de ce petit cercle d’amies.

## Casal de Mahomeriola, église paroissiale, matin du lundi 8 décembre 1158

Libourc allait et venait autour de l’église depuis le matin, après avoir prétexté quelques tâches à l’extérieur. Sa corbeille désormais vide sous le bras, elle hésitait à franchir le porche. De plus en plus inquiète à chaque pas, elle était angoissée qu’on la voie justement ainsi atermoyer. Elle allait s’en retourner quand la voix douce du prêtre, le père Giriaume, l’arrêta.

« As-tu quelque ouvrage à faire en l’église, ma fille ? »

Le clerc était un petit homme à la tonsure mal peignée, dont l’épaisse barbe brune ne s’ornait que de quelques dents marron quand il souriait. Quoique vêtu de laine de qualité, il n’était guère soigneux et on moquait volontiers les effluves corsés qui s’attachaient à lui. En place depuis peu, il ne faisait guère l’unanimité parmi ses ouailles en raison de son caractère changeant. Un jour il prônait la séparation des sexes pour la messe et, le lendemain, il bénissait sans honte une union fortement suspectée d’adultère. On disait d’ailleurs que cela expliquait qu’un homme si lettré ait échoué ici au lieu de finir opulent chanoine prébendé, comme nombre de ses frères du Saint-Sépulcre. Il s’était néanmoins toujours montré aimable avec Libourc, ainsi qu’il avait l’habitude de l’être avec les plus jeunes. 

« C’est à dire que je… »

La honte de ce qu’elle souhaitait évoquer mit ses joues en feu et elle n’osa pas aller plus loin. Tandis qu’elle demeurait là à chercher ses mots, le père Giriaume repoussa la porte de l’église et l’invita à entrer.

« M’est avis qu’il serait mieux d’entendre cela en confession. Va donc réciter quelques *Ave* le temps pour moi de me préparer. Nous irons la chapelle nord pour cela. »

Incapable de se rétracter, Libourc hocha la tête et alla attendre, agenouillée en position d’orante, que le prêtre puisse l’entendre. Lorsqu’il revint, son air n’était plus guère aimable, mais empreint de sérieux et de solennité, si ce n’était qu’il avait posé son étole à l’envers. La jeune fille n’eut pas le courage de le lui faire remarquer. Elle récita les quelques répons puis demeura muette un moment avant d’oser se lancer à avouer ses péchés.

« Je suis céans ce jour car on m’a dit que l’église était fermée aux âmes impures…

— De certes, de certes. Mais encore faut-il dénicher quel mal est en ton âme, afin de l’en bien purger par adéquates pénitences.

— Eh bien, il y a ce garçon, que vous avez souventes fois vu avec moi à la messe.

— Ce jeune Sanson ? Qui ne l’aurait vu ?

— Vous savez que nous sommes promis l’un à l’autre ? Nous allons nous unir… »

Le bruit de gorge du prêtre arrêta Libourc dans sa difficile confession. Elle se demanda si c’était un ricanement ou juste une toux contenue. De la main, il l’invita à continuer.

« Il se trouve que voilà quelques semaines, mes parents ont dû quitter la maison, me laissant seule une partie de la journée. Et Ernaut est arrivé alors qu’on ne l’attendait pas… Et nous avons… commis le péché de chair ensemble. Tout est allé si vite… Je n’ai pas d’excuse…

— C’est là en effet bien terrible outrage à la loi divine, qui veut que seuls l’époux et l’épouse s’accouplent. Et toujours dans la continence. Mais tu me dis que vous êtes promis l’un à l’autre ? Avez-vous fait solennelle promesse, de votre plein gré ? Y avait-il des témoins de cela ?

— Oui, mes parents attendent juste qu’Ernaut ait suffisamment de bien pour correctement nous établir. Ce n’est qu’alors que nous pourrons faire bénir notre union. »

Le clerc fit la moue, réfléchit un instant.

« Et comment vous êtes-vous accouplé ? Selon la nature ou d’une façon contraire à celle-ci ? »

Libourc était écarlate de confusion à l’idée de conter par le menu tout ce qui s’était passé, mais le regard attentif du prêtre ne lui laissait guère d’échappatoire.

« Nous nous sommes baisés sur la bouche, puis avons commencé à nous dévêtir.

— Oui, de cela je me doute, mais après ?

— …Il s’est couché sur moi et nous nous sommes ensuite… agité, lui en moi.

— Je vois. Tu étais sur le dos et vous étiez ventre contre ventre ? C’est bien cela ? »

Libourc opina en silence. Le prêtre semblait réfléchir, lançant de rapides coups d’œil à la jeune fille, sans laisse présager de ce qui se passait dans sa tête. Elle appréhendait le pire, en particulier une pénitence qui la stigmatiserait aux yeux de la congrégation. Elle craignait que sa famille ne supporte pas un tel déshonneur et battait sa coulpe de s’être ainsi abandonnée à son désir.

« Il me faut savoir si tu as eu quelque plaisir… intense. Ou rien de la sorte ? »

Surprise par la question, Libourc bafouilla et répondit d’une petite voix.

« Je ne pourrais dire. Il y a eu d’abord quelque douleur…

— Était-ce pour toi la prime fois ?

— Oh oui ! Jamais je n’avais… »

Le prêtre se laissa à un sourire fugace et gratta sa barbe.

« Tu as bien fait de venir me voir, ma fille. Tu y trouveras la paix de l’esprit, à défaut de celle de l’âme. Sois rassurée, il n’y a là nul péché de chair. Mais te voilà mariée ! »

La poitrine prise en étau par ce maelstrom émotionnel, Libourc écarquilla les yeux, la bouche ouverte, incapable de respirer. Voyant son désarroi, le prêtre s’expliqua.

« C’est très simple : ce jeune homme et toi, vous avez échangé vos promesses au vu et au su de témoins et vous avez par la suite consommé cette union déclarée. Vous voilà donc liés par le mariage. Et comme vous vous êtes accouplés ainsi qu’il sied, de façon naturelle et sans abandon aux turpitudes de la chair qui engendrent le plaisir impie, il n’y a là que le devoir des époux l’un envers l’autre. Votre union est éternellement scellée, rien de plus. *Matrimonium consummatun est*[^mariage1]. »

Libourc se frotta le visage, incapable de réagir à une telle annonce. Sans lui laisser le temps de se reprendre, le père Giriaume enchaîna.

« Tant que tu es là, il y a sûrement d’autres péchés qu’il serait bon que tu me narres… »

## Casal de Mahomeriola, demeure de Sanson de Brie, matin du jeudi 24 septembre 1159

Sanson de Brie avait l’habitude de faire un tri des graines qu’il avait mises de côté pour leur alimentation hivernale juste avant que les frimas n’arrivent, vers la Saint-Michel. Malgré la plus faible humidité en Judée qu’en Brie, il avait toujours la crainte que leurs provisions moisissent et qu’il faille entamer les réserves de semences de l’année à venir. Il n’avait donc jamais laissé cette tâche domestique à Mahaut, qu’il aurait de toute façon harcelée si elle l’avait faite par elle-même.

Les jarres pour l’accès régulier étaient enterrées au fond de la réserve close, couvertes d’un épais rabat de bois étanché de tresses et on se servait d’une barre marquée pour contrôler le volume de graines restant. On y conservait froment et seigle, ainsi qu’un peu d’avoine. Ce dernier était surtout utilisé pour pousser les bêtes qui travaillaient, mais Sanson avait toujours apprécié cette céréale de luxe, avec laquelle on faisait galette et bouillies légères. Libourc l’accompagnait, profitant de l’ouverture du cellier pour remplir sacs et récipients qui seraient portés au moulin pour les semaines à venir.

Il fallait se mouvoir avec précaution au milieu des étagères, des pots et des céramiques entreposées et la jeune fille se rendit rapidement compte que quelque chose n’allait pas avec son père. Il avait la tête ailleurs. Lui d’habitude si serein et assuré avait manqué de renverser plusieurs contenants et s’était cogné aux montants des meubles. Il grognait sans cesse et prenait un temps fou à puiser dans les grains qu’il versait dans les sacs que tenait Libourc. Elle savait que ce n’était guère le moment de l’interroger quand il était dans une humeur aussi exécrable, mais elle craignait qu’il n’ait quelque grief à son encontre et s’en émut auprès de lui, d’une voix fluette.

« Nenni, ma douce, je n’ai nulle colère envers toi. C’est juste que je me fais gâteux et que cela m’agace.

— Est-ce fardeau que je pourrais vous aider à porter ? »

Le vieil homme soupira douloureusement, s’assit sur un  escabeau branlant et prit un long temps avant de répondre.

« Ne va pas dire cela à ta mère, elle en mourrait ! Mais je crains que ma vue décline ces temps-ci.

— C’est bien normal avec les ans…

— Je ne te parle pas de ça. Tu n’as guère connu ma tante Emeline, ma marraine.

— Je ne me souviens que d’une vieille nonne que nous visitions à l’occasion.

— C’est cela. Si elle a rejoint les sœurs, c’est parce qu’elle a vécu ses dernières années dans le noir. Je crains d’avoir le même mal qu’elle…

— Vous voulez dire que vous allez devenir…

— Aveugle. Si fait. Je n’arrive déjà plus à reconnaître le seigle du froment si ce n’est au toucher. Et sans forte lumière, je ne discerne que des formes vagues. »

Libourc ne put s’empêcher de prendre la main de son père et de la serrer dans les siennes.

« Les valets ne sont bons à rien si on ne les houspille pas en permanence et ils auront beau jeu à en faire à leur tête, avec un maître aveugle. Je ne peux demander à ta mère de s’occuper des terres en plus de la maison. Elle en fait déjà tant…

— Je suis là, père. Vous pouvez compter sur moi.

— Que non pas, ma fille. Tu as ta vie avec le jeune Ernaut et elle te mènera loin d’ici, dans la ville. Nous ne pouvons nous dédire maintenant.

— Ernaut a bon cœur, père. Il ne saurait souffrir de vous voir esseulé et en difficulté, j’en suis certaine. Le moment venu, vous pourrez vous installer avec nous, avec des valets pour s’occuper de vous. »

Sanson sourit faiblement, attendri par la spontanéité de sa fille. Mais il n’aimait guère l’idée de finir vieillard grabataire dans la maison d’un autre. Il lui caressa la main doucement.

« De cela j’aviserai si besoin s’en fait sentir. Mais, surtout, ne va pas en discuter avec qui que ce soit ! Ta mère a le cœur fragile et s’enfrissonne d’un rien. Je vous dirai ma décision quand je l’aurai prise. »

## Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159

Occupée à découper un petit pâté à la viande, Libourc laissait ses pensées vagabonder. Elle appréciait ce moment simple, en famille, avec ses parents et Ernaut. Elle déplorait l’absence de ses frères et sœurs demeurés en Brie, qui devaient désormais être envahis d’enfants. Avec les fêtes, les valets et servantes étaient chez eux, et ne restaient que les personnes dont elle se sentait le plus proche. Elle regrettait aussi que Lambert et Osanne n’aient pu se joindre à eux. Elle ne les connaissait guère, mais souhaitait renforcer les liens qui les unissaient. Elle avait l’impression que son frère aîné exerçait une bonne influence sur Ernaut. Il était parfois  sentencieux, mais aussi posé et prudent, ce qui pondérait l’enthousiasme de temps à autre irréfléchi d’Ernaut.

Elle n’avait pas abordé avec son fiancé le sujet de la santé de son père, consciente qu’elle risquait de contrarier Sanson en le faisant sans son accord. Elle se sentait néanmoins tiraillée par son devoir entre les deux hommes. Ce que le prêtre lui avait dit l’avait bouleversée et quand elle en avait parlé à Ernaut, il avait semblé ennuyé lui aussi. Depuis lors, il s’était mis en tête de leur permettre de s’établir au plus vite, redoublant d’efforts en cela. Libourc avait bien senti que leur relation avait changé et qu’ils se comportaient l’un envers l’autre déjà comme des époux. Elle s’estimait donc un peu en faute de ne pas lui avouer ce qu’elle avait sur le cœur et en projet.

Elle lui lança un petit sourire, en le voyant assis au coin du feu, certainement occupé à échafauder des plans pour être promu dans l’hôtel du roi. Elle était très fière de tout ce qu’il faisait et hésitait parfois à l’en féliciter. Elle craignait qu’il n’en devienne fanfaron et veillait donc à ne pas lui ouvrir ainsi la voie au péché d’orgueil. Elle serait plus à l’aise pour aborder tous ces sujets avec lui quand ils partageraient un logement et leur couche. Jusqu’à présent, leurs moments d’intimité étaient bien trop rares et précieux pour qu’elle les gaspille. Étrangement, après les assurances du prêtre, ils n’avaient plus osé aller au-delà de quelques baisers, redoutant peut-être de rompre le délicat équilibre actuel. Libourc était également soulagée de n’avoir pas à gérer une possible grossesse prématurée. Ils étaient néanmoins tous deux impatients que les choses avancent.

La table prête, Sanson mit un moment à venir s’installer. Il arborait son visage des bons jours, légèrement espiègle et content de lui. Lorsqu’après le bénédicité, il prit la parole doucement, elle n’en fut qu’à demi étonnée. Enfin il donnait son accord définitif pour leur union ! Le cœur de Libourc se mit à battre la chamade, surtout quand elle vit Ernaut sourire de bonheur, incapable de répondre. Sanson paraissait fier de son effet et enchaîna, tout joyeux.

« Il me semble que passer Carême nous laissera le temps de provisionner pour offrir beau banquet à compères et commères. Qu’en dites-vous ? Les Pâques ne sont que dans trois mois, ce pourrait être peu après. » ❧

## Notes

À l’époque d’Ernaut, la femme européenne est considérée comme une mineure de façon quasi naturelle, que ce soit au niveau religieux ou d’un point de vue social, économique et politique. En gros, elle passe de la sujétion à son père à celle à son mari et ne peut vraiment devenir indépendante, complètement majeure qu’une fois veuve. Sa voie est donc toute tracée dès sa naissance et en dehors de l’intégration à un couvent, impossible à éviter. Une structure sociale traditionaliste faisait que cela n’empêchait pas certaines de parfaitement s’épanouir dans un système si oppressif, en adhérant dès l’enfance à un fonctionnement dont rien n’indiquait qu’il pouvait en exister un autre.

Il devait se rencontrer des réfractaires à ce schéma : on pense tout de suite à des femmes d’influence comme Mélisende de Jérusalem ou, bien sûr, Aliénor d’Aquitaine. Mais on a aussi quelques traces infimes de leur existence parmi les couches les plus populaires. Je fais là référence à Guillelme Maury, de Montaillou, dont Anne Brenon nous narre le roman vrai dans son magnifique récit « L’impénitente ». Je n’ai néanmoins pas choisi de me baser sur elle pour Libourc, que je voulais plus semblable à ce que je perçois et crois comprendre de beaucoup de femmes de cette époque, pour peu qu’on puisse définir sans restreindre et appauvrir ou se projeter sans trahir exagérément.

En ce qui concerne la confession de Libourc, c’est l’occasion d’évoquer les traditions légales en droit canon qui s’affrontaient alors autour de l’importance du consentement, de sa temporalité et de la question des relations sexuelles. Les pénitentiaires qui nous sont parvenus sont très complets et semblent indiquer que les ecclésiastiques exigeaient les détails que Libourc peine à dévoiler. Les glossateurs sur les péchés avaient ainsi défini des catégories précises, selon les moindres faits et gestes, y compris dans les rapports intimes, et y appliquaient des pénitences en fonction de leur cadre d’analyse de la société. Avec parfois un décalage avec ce qu’on pourrait imaginer, en laïc du XXIe siècle. Le Qit’a « Maître de chapelle », à venir, reviendra sur cette question.

## Références

Brénon Anne, *L’impéninente. le roman vrai de Guillelme maury, de Montaillou*, Cahors : La Louve éditions, 2002.

Brundage James A., *Law, Sex, and Christian Society in Medieval Europe*, Chicago : The University of Chicago Press, 1987.

d’Avray David, *Medieval marriage - Symbolism and Society*, Oxford : Oxford University Press, 2005.

Donahue, Charles Jr, *Law, Marriage, and Society in the Later Middle Ages*, Cambridge : Cambridge University Press, 2007.

Schaus Margaret (dir.), *Women and Gender in Medieval Europe - An Encyclopedia*, New York : Routledge, 2006.

Shahar Shulamith, *The Fourth Estate - A history of women in the Middle Ages*, nouvelle édition, New York : Routledge, 2003.
