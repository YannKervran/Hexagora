---
date : 2016-06-15
abstract : 1157. Alors que le jeune roi Baudoin s’efforce de stabiliser son pouvoir au sein du royaume, son frère Amaury succombe aux charmes de la jeune Agnès de Courtenay. Au grand désarroi d’une partie de la noblesse, qui voit ses aspirations balayées par l’établissement d’une nouvelle coterie.
characters : Droart, Herbelot Gonteux, Régnier d’Eaucourt, Eudes Larchier, Frédéric de la Roche, Amaury
---

# Liaisons dangereuses

## Palais royal, salle basse, après-midi du samedi 2 février 1157

Noyé de lumière, le vaste lieu voûté présentait un aspect riant malgré le froid extérieur. Se déversant par les croisées de bonne taille, un vigoureux soleil d’hiver rebondissait sur les murs étincelants de blancheur. Avec le banquet offert par Baudoin à de nombreux membres de la Haute Cour, une large assemblée de pauvres, pèlerins et nécessiteux avaient été accueillis dans une salle basse pour s’y voir proposé les reliefs du repas royal.

Le vicomte ayant l’expérience de ces groupes où tire-goussets et crocheteurs savaient se fondre dans la masse, plusieurs sergents vérifiaient qu’aucun larron ne profitait de l’occasion pour opérer larcin. Eudes et Droart étaient postés à l’entrée de la salle de l’Échiquier, adjacente, où se déroulaient habituellement les activités comptables de l’administration de Jérusalem. Tranquillement adossés au mur, les bras croisés, ils discutaient nonchalamment. L’hiver avait été calme, une trêve courant avec le farouche Nūr ad-Dīn. Les frimas et les inévitables fluctuations du prix du pain en période de jointure constituaient les principaux sujets de conversation. Avec l’arrivée prochaine du carême, les gens auraient prétexte à faire maigre.

Deux valets redescendant les tranchoirs imbibés de sauce poivrée s’approchèrent des sergents, l’air hilare. Ils étaient de leurs compagnons de taverne, aimant à faire rouler les dés ou à jacasser des rumeurs animant le palais de temps à autre. Ils semblaient fort s’amuser et se délectaient par avance de ce qu’ils allaient apprendre à leurs amis.

« Un nouveau jongleur aurait-il manqué de respect au sire prince de Galilée ? ricana par avance Droart.

— Ben pu drôle que ça, compain. De peu qu’le sire Courtenay rende son âme, occis par une tourte mal goboyée !

— Si fait, je crois que le jeune comte frère le roi[^amaury] a assailli mauvaise forteresse, cette fois.

— Que nous contez-vous là ? » s’intéressa Eudes, hésitant à lâcher du regard un des festoyeurs amateur de gamelles autres que la sienne.

Les deux valets posèrent leurs corbeilles et se rapprochèrent, la mine réjouie et le ton conspirateur.

« Le jeune comte aime ben noyer l’épinette un peu partout, mais y l’a trempée en un lieu promis à un autre.

— La jeune princesse d’Édesse, veuve au frais minois, a préféré fils à roi que sire d’Ibelin. On aurait surpris la charmante issir de la couche comtale.

— Et la nouvelle est arrivée aux oreilles du sire Joscelin croquant son repas. J’ai ben cru qu’le vin allait lui jaillir hors le groin tant y s’étouffait. »

La remarque fit jaillir quelques rires enthousiastes. Si chacun reconnaissait la valeur du jeune Courtenay comme homme de guerre, personne n’en ignorait pour autant le naturel féroce et les exigences strictes. La perte du domaine familial ne lui laissait que sa réputation et sa vaillance pour faire son chemin. Ses espoirs de retrouver une principauté, même tenue en fief, motivaient chacune de ses actions et son impétuosité convenait à merveille à son poste militaire.

Il avait fait venir sa sœur dans l’espoir d’un mariage avec l’héritier Ibelin, famille en faveur depuis le roi Foulques. Mais si elle se déshonorait avec Amaury, ses ambitions seraient définitivement ruinées. Il avait brûlé tous ses vaisseaux et ne voyait pas d’autre échappatoire que de faire carrière, et faire souche, dans l’orbite du pouvoir royal. Sa superbe en était durement éprouvée, et sa méchante humeur en sortait renforcée.

« Y f’ra pas bon œuvrer aux écuries les jours à v’nir.

— J’encrois qu’les exercices au conroi vont faire pleuvoir horions et bosses !

— D’un autre côté, comment en vouloir au jeune sire Amaury ? Lui qui n’a besoin que d’une œillade amicale ou d’une croupe audacieuse pour succomber. »

Droart ajouta à sa remarque un geste de la main, dessinant les courbes audacieuses qu’il prêtait à la jeune femme. À son arrivée, chacun avait pu admirer la joliesse de la princesse d’Édesse, habilement mise en scène par des atours et des apprêts attirant sur sa tête les imprécations cléricales autant que les regards appréciateurs. Les serviteurs goûtaient sa splendeur, dont ils savaient qu’il n’en seraient que spectateurs, mais la noblesse bruissait de cette beauté en liberté, dont le caractère n’avait que peu à envier à celui de son frère.

Eudes ajouta finalement :

« On ne peut blâmer la princesse. Pour naviguer entre les coteries et tracer son sillon, elle n’a que son frais minois et un nom envié.

— Comme l’araignée, de son cul elle tire sa toile ! » opina Droart, hilare.

Des éclats de rire fusèrent derechef puis les valets se décidèrent à reprendre leurs corbeilles. Au moment de partir, l’un d’eux lâcha :

« De certes, j’donnerai ben trente deniers pour assavoir c’que l’sire Hugues[^huguesibelin] dira quand y se saura deux fois floué. Cornard avant espousaille et promis à offrir sienne sœur à çui qu’a orné ainsi son front. »

## Jérusalem, palais royal, chambre du comte de Jaffa, matin du jeudi 20 juin 1157

Les pas du chevalier résonnaient sur les sols dallés de pierre, de mosaïque ou de carreaux vernissés, ses éperons rythmant sa marche précipitée autant que le cliquetis de ses armes. Il tenait son casque sous le bras et n’avait que grossièrement délacé sa coiffe de mailles. Arrivant à la porte de sa destination, il fit un signe au valet qui l’avait précédé. Celui-ci entra pour l’annoncer puis ouvrit peu après largement l’huis.

Régnier n’avait jamais été reçu par le jeune comte de Jaffa, le frère du roi. Il découvrait donc la somptueuse chambre où Amaury accueillait ses familiers et amis lorsque, fréquemment, il était dans la Cité. En l’absence du souverain, il avait la charge des affaires courantes au sein du palais, mais la tâche était suffisamment tranquille pour qu’il prenne ses aises.

Les parois de la vaste pièce s’ornaient de complexes décors céramiques, abritant plusieurs niches aux profils découpés dans un style arabisant. Le sol, de marbre clair, accueillait par endroit d’épais tapis aux motifs bariolés. Un lit gigantesque, à l’occidentale, était placé au centre, nimbé de voilages et de tentures, noyé de coussins, édredons et couettes de brocard. Des coffres peints de scènes guerrières s’alignaient sur le mur opposé aux larges fenêtres de verre coloré, en partie obturées de volets ajourés. Devant la couche, un haut siège, mélange de divan à l’orientale et d’un trône européen était recouvert de somptueuses étoffes négligemment disposées. Il était flanqué d’une table où s’amassaient de branlantes piles de livres, rouleaux, tablettes, dans un désordre précaire.

Amaury était debout, face au monticule et se tenait de trois quarts par rapport à Régnier. De belle taille, sa silhouette un peu épaisse était affinée par un bliaud de soie aux motifs syriens dorés. Il dardait un regard clair, attentif sur l’arrivant, se mordant les lèvres. Il était réputé pour ne pas être homme à la parole facile et son haut front se barrait souvent de rides quand il cherchait comment s’exprimer. La chevelure blonde rappelait son frère, et le nez quasi aquilin le profil de son père. Mais il portait la barbe, courte et bien taillée, ainsi que les Templiers dont il partageait le goût pour la chasse et le dédain des jeux.

Régnier aperçut un petit christ en croix dans le capharnaüm de la table, juste là où se tenait le comte de Jaffa en fait. Il fléchit un genou et patienta, n’osant prendre la parole sans y avoir été invité, et se contentait d’attendre, retrouvant son souffle après sa folle chevauchée et sa course dans le palais.

La voix hésitante d’Amaury finit par rompre le silence, annoncée par un imperceptible bégaiement dont il avait mis des années à se défaire.

«  On vous annonce porteur de mauvaises nouvelles. Quelles sont-elles ?

— Mauvaises, mais pas dramatiques, sire. Nous avons subi male défaite, mais le roi est sauf, réfugié en l’éperon de Saphet. »

Le comte s’assit doucement tout en invitant d’un geste Régnier à se relever.

« Qu’est-il arrivé au juste ?

— Nous repartions de Panéas[^paneas] quand nous avons été pris à partie par fort contingent de troupes. Damascènes, turcs, aleppins, turcomans, on aurait dit des sauterelles. Le soleil était voilé de leurs traits. »

Le regard d’Amaury erra sur la tenue de Régnier, s’attarda sur la maille rompue par endroit, la poussière. Son expression demeurait impénétrable.

« Avons-nous perdu hommes de valeur ?

— Je le crains fort. Nous n’avons été qu’une poignée à rejoindre le refuge du castel, pas même une échelle[^echelle]. Seuls ceux qui percèrent la ligne ennemie pour y glisser le roi en ont réchappé.

— Est-ce à dire que le royaume n’a plus d’ost ?

— Valetaille et piétons demeuraient presque tous à Panéas, pour aider à remettre en état. Seule la cavalerie a été défaite. Mais il s’y trouvait tous les grands présents, peu ou prou. »

Amaury réfléchit un petit moment, les yeux rivés sur le chevalier. Puis il se redressa soudain. Il faisait penser à son frère quand il adoptait ainsi une tenue plus hiératique, masquant son léger embonpoint.

« Nous allons constituer petite escorte rapidement. Je vous veux parmi eux, que vous les meniez au roi. Voyez avec les frères de la Milice[^templiers] s’ils ne peuvent divertir quelques hommes. Une partie du ban d’Ascalon est ici avec moi, je peux me garantir sans eux, et ferai appeler d’Hébron en remplacement. »

Voyant que Régnier levait la main pour demander la parole, il la lui concéda d’un geste.

« Le sire roi avait l’intention de rejoindre Acre à franc étrier dès les troupes musulmanes éloignées.

— Vous l’y joindrez donc. Passez par Naplouse, La Fève et Séphorie. Je vous donnerai mandements pour y récolter des hommes en chemin. La première chose à faire est de garantir le roi et le mener sauf ici. Je vais informer le sire comte Raymond et le prince Renaud, qu’ils soient prêts à nous appuyer si besoin était. »

Il se leva et s’approcha de Régnier, parlant de façon moins formelle.

« Allez donc prendre repos le temps pour la troupe de se préparer. Je veux son départ dès avant none. »

Puis, d’un geste amical de la main, il invita Régnier à sortir. Lorsque la porte se referma, il tira l’épaisse toile qui s’y ajoutait et fit quelques pas, jusqu’à une des fenêtres, jetant un regard vers la cour en contrebas. Des ombres allaient et venaient parmi les orangers et les massifs de fleurs multicolores.

Il hésitait sur ce qu’il devait faire. Il avait longuement prié que le Seigneur daigne lui accorder la main d’Agnès. Ce qui signifiait empêcher son union avec Hugues d’Ibelin, son vassal. Leur idylle commençait à inquiéter au plus haut niveau, sans compter les moqueries colportées par le peuple. Sa propre mère Mélisende l’avait durement tancé, lui reprochant de s’aliéner les Ibelin, une puissante famille, alliée et soutien sans faille de son défunt père.

Fallait-il qu’il obtienne satisfaction à un tel prix ? Hugues d’Ibelin était de ceux qui avaient pris part à la chevauchée pour délivrer Panéas. Était-il sauf ? Était-il libre ? Amaury n’avait pas osé poser la question qui lui brûlait les lèvres. Il hésitait également à prévenir celle qu’il chérissait, inquiet d’éveiller en elle des espoirs qu’il ne saurait combler.

Il se mordillait l’intérieur des joues, les yeux dans le vague quand on frappa à la porte. Un jeune clerc au regard myope se présenta, son écritoire sous le bras, une sacoche de notaire au côté, un de ces anonymes discrets dont il n’arrivait jamais à se souvenir du nom. Derrière lui, une cohorte de familiers s’entassaient, dans l’attente des nouvelles.

« Nous allons nous rendre en la Grande salle. J’y ferai annonce et dicterai plusieurs libelles et annonces. Nous lancerons force coursiers avant sixte, au moins une douzaine. Faites-le assavoir au plus tôt. »

Comme lors des chasses, il sentait son courage s’affermir dans l’action. Pour l’heure, le royaume avait besoin de lui, et il comptait bien ne pas manquer à son frère. Ses affaires de cœur sauraient attendre jusqu’à la veillée.

## Jérusalem, église du Saint-Sépulcre, matin du vendredi 16 août 1157

Herbelot marchait à petits pas, le visage recueilli comme il seyait à un clerc respectueux déambulant dans le Saint des Saints. Il suivait sans mot dire la haute silhouette de l’évêque Frédéric de la Roche au travers des détours et recoins du Saint-Sépulcre. Avançant rapidement, ils débouchèrent au niveau de la tribune de la rotonde, d’où la vue sur le tombeau du Christ embrasait toujours la Foi du jeune homme. Mais l’ambiance n’était guère aux oraisons et aux transports religieux. Frédéric de la Roche était plus porté sur les discussions de Cour et la politique que sur l’exégèse évangélique. On le disait d’ailleurs plus souvent coiffé du heaume que de la mitre.

Herbelot lui reconnaissait un courage physique impressionnant, mais désapprouvait ce mélange des genres. En silence, bien sûr, car il n’était pas dans ses façons de s’épancher ainsi à propos d’un supérieur, fut-il farci de tous les vices. Ce qui n’était pas le cas de Frédéric, qui lui imposait par sa vivacité d’esprit, si ce n’était par sa culture. Sans pour autant dire qu’il appréciait de devoir le servir parfois, sur les instructions de son vrai maître, l’archevêque Pierre de Tyr, il ne s’en effrayait plus. D’autant que le grand prélat lui manifestait des égards marqués. C’était en partie parce qu’il était soucieux de ne jamais abaisser le prestige d’un clerc devant les gens du commun. Il conservait néanmoins cette attitude pleine de respect, bien que souvent assez directe, dans l’intimité.

Un des serviteurs des chanoines fit tourner sa clef dans la serrure d’une petite porte et ils continuèrent leur périple dans une galerie bordant une terrasse, surplombant des jardins. Ils se trouvaient désormais dans le palais royal. Le soleil matinal commençait à en chauffer durement les pierres et, rebondissant sur les murailles, obligea les deux hommes à plisser les yeux. Ils croisèrent plusieurs domestiques affairés, qui saluèrent sans s’attarder. Ils étaient habitués à côtoyer des têtes couronnées et tonsurées de la plus grande importance et ne s’éternisaient pas en politesses.

Un large escalier déroulant ses degrés lentement les mena jusqu’au jardin, où chantait une fontaine au milieu de massifs arborés. Un vieux valet au dos plus voûté qu’une galerie de cloître binait tranquillement un parterre de roses ombragé, tandis que des gamins d’une dizaine d’années s’activaient à remplir un tonneau depuis le point d’eau à l’aide de seaux aussi lourds qu’eux.

Sur un banc à l’abri d’un petit kiosque léger en vannerie, le jeune comte de Jaffa, Amaury, était assis, un stylet à la main. L’air concentré, il notait parfois sur une tablette ce qu’un lecteur lui récitait d’un épais volume posé sur un lutrin. Les deux hommes s’interrompirent en voyant arriver le couple de clercs. L’évêque salua poliment.

« Sire comte de Jaffa, la paix du Christ soit avec vous

— Sire évêque Frédéric, le bon jour à vous. Que vous voilà fort à point, Isaac me lisait un texte griffon que vous goûteriez fort. »

Il se tourna vers le lecteur, qui s’inclina respectueusement sans mot dire, puis continua, d’un ton badin.

« Il s’agit de la *Présentation et la composition de l’art de la guerre selon le sire Nikephore*[^praecepta]. Empli d’instructives idées sur leur usage de la cavalerie et de la piétaille. Il faudrait le faire traduire en latin pour l’édification de nos gens.

— Je ne serais que trop heureux de mettre mon scriptorium et quelque savant lettré de grec à votre service, sire comte. »

L’évêque marqua une pause puis se rapprocha avant d’ajouter d’une voix feutrée.

« J’aurais espoir, néanmoins de prendre entretien avec vous d’une affaire qui vous touche de fort près. Vous assavez combien je vous tiens en affection et estime. C’est donc à ce titre que je me permets de venir, en tant qu’ami.

— Amitié dont je n’ai eu qu’à me louer, sire évêque. Vous me voyez bien aise vous compter parmi mes intimes. »

Il fit un signe de la main au traducteur pour qu’il s’éloigne et invita le prélat à s’asseoir à ses côtés. Il ne fit guère de cas d’Herbelot, sachant que les seigneurs de l’Église ne se déplaçaient jamais sans quelque séide tonsuré, dont l’usage et l’utilité lui paraissaient toujours obscurs. Il avait appris à ne pas s’en préoccuper, voire à les ignorer.

« Vous assavez que la reine votre mère m’honore d’accepter mes avis en sa Cour. Outre, même si je n’ai plus l’heur de servir directement la couronne, mes pensées, et mon cœur ne s’en éloignent guère.

— Chacun sait dans ma famille combien votre présence est précieuse à la couronne, moi le premier.

— C’est également à ce titre que j’ai l’audace de venir vous parler des rumeurs qui courent au parmi du royaume, et qui inquiètent fort votre mère. »

Amaury soupira et redressa le buste, lançant un regard aussi méfiant que s’il avait soudain affaire à un serpent. Mais l’évêque était opiniâtre et tenait à aller au bout de sa pensée.

« J’ai souvenance que l’union de votre famille à celle des Ibelin était déjà un projet de votre père. Ce serait là fort habile et fructueuse union. Ils se sont montrés indéfectibles dans leur service.

— Ils servent déjà ma bannière. Qu’espérer de plus ? Il est de plus illustres lignées dont nous pourrions nous honorer.

— Il en est de certaines familles dont l’étoile a fort brillé, mais dont l’éclat est désormais terni.

— Le mariage n’est donc t-il là que pour fournir sergents et lances ? Péages et octrois ? s’emporta Amaury.

— Ai-je jamais dit cela ?, s’effraya Frédéric. Je me demande seulement si vous n’avez pas cédé un peu vite à votre sang, fort jeune et bien amiable. Il faut raison porter à ces importantes matières. Ne pas trop vite céder aux naturelles bienveillances de votre cœur. »

Le jeune prince se renfrognait de plus en plus, bien qu’il agita le menton en timide assentiment. Réalisant qu’il risquait d’arriver à l’inverse de ce qu’il escomptait, l’évêque se releva, un sourire matois sur le visage.

« Ne prenez pas en mâle part ce que je vous ai confié, c’est là inquiétude d’un féal serviteur, et d’un ami. Pourpensez à tout cela et voyez de qui vous viennent les avis, je n’en demande guère plus. »

Tout en parlant, Frédéric se leva, lissant ses habits comme s’il craignait d’en avoir dérangé la superbe. Amaury hocha la tête, visiblement toujours agacé. Il lâcha un salut poli, mais froid et reprit une de ses tablettes. L’entretien était fini.

Alors qu’ils s’éloignaient, Herbelot entendit maugréer l’évêque entre ses dents.

« La peste soit de cette Courtenay. Est-elle sorcière pour le prendre ainsi dans ses rets ! »

Puis il se tourna vers Herbelot, trop énervé pour s’empêcher de le prendre à témoin.

« Le saint père Foucher ne va jamais tolérer cela. J’espérais lui éviter pareil souci. »

Puis, reprenant sa route avec vigueur, il ajouta une dernière fois mezzo-vocce :

« Que n’ont-ils péri avec leur comté, ces maudits ! » ❧

## Notes

L’union du jeune prince Amaury, alors simple comte de Jaffa (mais futur roi) avec Agnès de Courtenay fut à l’origine d’un véritable drame. En dehors du fait qu’il aurait volé la fiancée (certains dirent même l’épouse) d’un de ses vassaux, seigneur d’Ibelin et chef d’une famille en pleine ascension, une forte hostilité d’une grande partie de la Cour et des clercs aboutit à son divorce pour qu’il accède à la couronne. Il se remaria à une princesse byzantine, mais on légitima ses précédents enfants. Il s’agit de Baudoin, le roi lépreux tant célébré et de Sybille, qui fit roi Guy de Lusignan, qu’on accable de la perte du royaume à Hattîn en 1187.

Les raisons de cette animosité demeurent mystérieuses, et la mauvaise réputation d’Agnès de Courtenay tient en grande partie au portrait qu’en dresse Guillaume de Tyr (et son continuateur Ernoul, qu’on pense être de la domesticité des Ibelin). Principal historien pour notre connaissance de cette époque, il fait souvent preuve de partialité et j’offre l’hypothèse qu’il se comporte ainsi en raison de ses liens avec Frédéric de la Roche, qui le fit archidiacre lorsqu’il revint en Terre sainte. Ce prélat faisait partie des nouveaux venus en ce milieu de siècle, promu sous l’administration du roi Foulques, comme le patriarche Foucher d’Angoulême par exemple. Je lui prête donc une certaine inimitié à l’égard des anciennes familles, de souche plutôt normande, dont faisaient partie les Courtenay, comtes d’Édesse.

Il s’agit d’une simple hypothèse de ma part sur un événement difficilement compréhensible, à la portée politique complexe. Le royaume de Jérusalem était régulièrement déchiré de conflits internes qui le menèrent plus d’une fois au bord du précipice, jusqu’à sa destruction en 1187. Les dissensions entre les coteries de familles anciennement implantées et les vagues d’arrivants avides d’une ascension sociale inespérée en Europe ont, me semble-t-il, structuré le fonctionnement des états latins tout au long du XIIe siècle. Selon moi, la chute d’une famille aussi riche et puissante que les Courtenay ne pouvait demeurer sans implications dans le jeu politique.


## Références

Amouroux-Mourad Monique, *Le comté d’Édesse 1098-1150*, Paris : Librairie orientaliste Geuthner, 1988.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. III.*, Cambridge : Cambridge University Press, 2007.
