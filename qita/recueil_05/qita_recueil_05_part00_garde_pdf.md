# Qit’a

Recueil

**Textes courts dans l’univers Hexagora**  
**Tome 5**

![Framabook, le pari du livre libre](images/Logo_framabook_grand.png)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d’informations sur Framasoft, consultez [http://www.framasoft.org](http://www.framasoft.org)

Se démarquant de l’édition classique, les Framabooks sont dits « livres libres » parce qu’ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu’un utilisateur de logiciels libres. Les Framabooks s’inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l’appropriation collective de la connaissance.

Pour plus d’informations sur le projet Framabook, consultez [http://framabook.org](http://framabook.org)

Copyright 2020 : Yann Kervran, Framasoft (coll. Framabook)

Ce recueil de textes Qit’a est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

ISBN :  
Dépôt légal :    
Couverture : Saint Luc évangéliste, Évangéliaire de Dobrila, Bibliothèque d'État russe, 1164. Domaine Public - [commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:Dobrilovo_Gospel.jpg)

