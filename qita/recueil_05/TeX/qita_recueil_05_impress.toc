\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Basileia romaion}{1}{chapter.0.1}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la Juiverie, matin du jeudi 20 juin 1157}{1}{section.0.1.1}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la Juiverie, après-midi du mercredi 26 février 1158}{3}{section.0.1.2}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la Juiverie, fin d'après-midi du mardi 14 octobre 1158}{6}{section.0.1.3}%
\contentsline {section}{\numberline {}Notes}{9}{section.0.1.4}%
\contentsline {section}{\numberline {}Références}{10}{section.0.1.5}%
\contentsline {chapter}{\numberline {2}Fondation}{11}{chapter.0.2}%
\contentsline {section}{\numberline {}Jérusalem, sud du mont Sion, après-midi du dimanche 27 juillet 1158}{11}{section.0.2.1}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, après-midi du jeudi 25 juin 1159}{15}{section.0.2.2}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la Juiverie, matin du vendredi 27 novembre 1159}{18}{section.0.2.3}%
\contentsline {section}{\numberline {}Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159}{20}{section.0.2.4}%
\contentsline {section}{\numberline {}Notes}{22}{section.0.2.5}%
\contentsline {section}{\numberline {}Références}{23}{section.0.2.6}%
\contentsline {chapter}{\numberline {3}Entéléchie}{25}{chapter.0.3}%
\contentsline {section}{\numberline {}Casal de Mahomeriola, fin d'après-midi du mardi 30 juillet 1157}{25}{section.0.3.1}%
\contentsline {section}{\numberline {}Casal de Mahomeriola, église paroissiale, matin du lundi 8 décembre 1158}{28}{section.0.3.2}%
\contentsline {section}{\numberline {}Casal de Mahomeriola, demeure de Sanson de Brie, matin du jeudi 24 septembre 1159}{31}{section.0.3.3}%
\contentsline {section}{\numberline {}Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159}{33}{section.0.3.4}%
\contentsline {section}{\numberline {}Notes}{35}{section.0.3.5}%
\contentsline {section}{\numberline {}Références}{36}{section.0.3.6}%
\contentsline {chapter}{\numberline {4}Passage}{37}{chapter.0.4}%
\contentsline {section}{\numberline {}Casal de Mahomeriola, fin d'après-midi du mercredi 29 janvier 1158}{37}{section.0.4.1}%
\contentsline {section}{\numberline {}Casal de Mahomeriola, demeure de Pariset, veillée du dimanche 7 septembre 1158}{40}{section.0.4.2}%
\contentsline {section}{\numberline {}Casal de Mahomeriola, demeure de Sanson de Brie, fin d'après-midi du mardi 28 juillet 1159}{43}{section.0.4.3}%
\contentsline {section}{\numberline {}Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159}{45}{section.0.4.4}%
\contentsline {section}{\numberline {}Notes}{47}{section.0.4.5}%
\contentsline {section}{\numberline {}Références}{48}{section.0.4.6}%
\contentsline {chapter}{\numberline {5}Maître de chapelle}{49}{chapter.0.5}%
\contentsline {section}{\numberline {}Lydda, palais de l'évêque, matin du lundi 9 mars 1159}{49}{section.0.5.1}%
\contentsline {section}{\numberline {}Château de Beaufort, après-midi du dimanche 14 juin 1159}{53}{section.0.5.2}%
\contentsline {section}{\numberline {}Sagette, château de La Tour-Baudoin, veillée du jeudi 10 septembre 1159}{56}{section.0.5.3}%
\contentsline {section}{\numberline {}Sagette, château de La Tour-Baudoin, matinée du mardi 15 décembre 1159}{58}{section.0.5.4}%
\contentsline {section}{\numberline {}Notes}{60}{section.0.5.5}%
\contentsline {section}{\numberline {}Références}{61}{section.0.5.6}%
\contentsline {chapter}{\numberline {6}Le lion et l'éléphant}{63}{chapter.0.6}%
\contentsline {section}{\numberline {}Jérusalem, rue du Temple, aube du mercredi 15 avril 1159}{63}{section.0.6.1}%
\contentsline {section}{\numberline {}Village de Hammam, gué de la Balanée, fin d'après-midi du lundi 25 mai 1159}{66}{section.0.6.2}%
\contentsline {section}{\numberline {}Village de Hammam, camp latin du gué de la Balanée, après-midi du samedi 30 mai 1159}{68}{section.0.6.3}%
\contentsline {section}{\numberline {}Village de Hammam, camp latin du gué de la Balanée, veillée du samedi 30 mai 1159}{70}{section.0.6.4}%
\contentsline {section}{\numberline {}Notes}{73}{section.0.6.5}%
\contentsline {section}{\numberline {}Références}{74}{section.0.6.6}%
\contentsline {chapter}{\numberline {7}En marche}{75}{chapter.0.7}%
\contentsline {section}{\numberline {}Chêne d'Abraham, faubourgs de Hébron, midi du jeudi 3 mai 1151}{75}{section.0.7.1}%
\contentsline {section}{\numberline {}Ascalon, camp de siège, veillée du lundi 17 août 1153}{77}{section.0.7.2}%
\contentsline {section}{\numberline {}Région de Gaza, zone d'el Djifar, veillée du samedi 6 septembre 1158}{80}{section.0.7.3}%
\contentsline {section}{\numberline {}Désert d'al-Gifar, fin d'après-midi du vendredi 19 septembre 1158}{82}{section.0.7.4}%
\contentsline {section}{\numberline {}Notes}{84}{section.0.7.5}%
\contentsline {section}{\numberline {}Références}{84}{section.0.7.6}%
\contentsline {chapter}{\numberline {8}La pierre et le puits}{87}{chapter.0.8}%
\contentsline {section}{\numberline {}Jérusalem, abords de la rue des Arméniens, après-midi du youm al joumouia 23 jha'ban 492}{87}{section.0.8.1}%
\contentsline {section}{\numberline {}Jérusalem, quartier de Malquisinat, matin du lundi 7 juillet 1113}{90}{section.0.8.2}%
\contentsline {section}{\numberline {}Jérusalem, quartier de l'hôpital Saint-Jean, matin du lundi 24 juin 1140}{93}{section.0.8.3}%
\contentsline {section}{\numberline {}Jérusalem, quartier de l'hôpital Saint-Jean, midi du samedi 16 février 1157}{95}{section.0.8.4}%
\contentsline {section}{\numberline {}Notes}{97}{section.0.8.5}%
\contentsline {section}{\numberline {}Références}{97}{section.0.8.6}%
\contentsline {chapter}{\numberline {9}Mon ennemi, mon frère}{99}{chapter.0.9}%
\contentsline {section}{\numberline {}Jérusalem, mont du Temple, après-midi du mardi 14 avril 1142}{99}{section.0.9.1}%
\contentsline {section}{\numberline {}Faubourg damascène, camp du Temple, matin du mardi 27 juillet 1148}{102}{section.0.9.2}%
\contentsline {section}{\numberline {}Wadi Arabah, camp de voyage, soir du mardi 10 août 1154}{105}{section.0.9.3}%
\contentsline {section}{\numberline {}Jaffa, maison du Temple, après-midi du mardi 14 octobre 1158}{108}{section.0.9.4}%
\contentsline {section}{\numberline {}Notes}{111}{section.0.9.5}%
\contentsline {section}{\numberline {}Références}{112}{section.0.9.6}%
\contentsline {chapter}{\numberline {10}Valeureux prince}{113}{chapter.0.10}%
\contentsline {section}{\numberline {}Taverne du père Josce, veillée du jeudi 19 décembre 1157}{113}{section.0.10.1}%
\contentsline {section}{\numberline {}Palais royal, soir du vendredi 17 janvier 1158}{116}{section.0.10.2}%
\contentsline {section}{\numberline {}Environs de la rue des Tanneurs, veillée du jeudi 30 janvier 1158}{117}{section.0.10.3}%
\contentsline {section}{\numberline {}Notes}{120}{section.0.10.4}%
\contentsline {section}{\numberline {}Références}{121}{section.0.10.5}%
\contentsline {chapter}{\numberline {11}Les chaussures du mort}{123}{chapter.0.11}%
\contentsline {section}{\numberline {}Damas, maison d'al-Kāla, midi du youm el itnine 20 dhu al-hijjah 553}{123}{section.0.11.1}%
\contentsline {section}{\numberline {}Damas, porte de Bāb al-Ğābiya, fin d'après-midi du youm al khemis 30 dhu al-hijjah 553}{126}{section.0.11.2}%
\contentsline {section}{\numberline {}Damas, quartier du Marché aux oiseaux, soirée du youm al talata 12 muharram 553}{128}{section.0.11.3}%
\contentsline {section}{\numberline {}Damas, citadelle, matin du youm al khemis 19 safar 554}{131}{section.0.11.4}%
\contentsline {section}{\numberline {}Notes}{133}{section.0.11.5}%
\contentsline {section}{\numberline {}Références}{134}{section.0.11.6}%
\contentsline {chapter}{\numberline {12}Retour au siècle}{135}{chapter.0.12}%
\contentsline {section}{\numberline {}Calanson, cimetière paroissial des frères de Saint-Jean, après-midi du dimanche 3 avril 1160}{135}{section.0.12.1}%
\contentsline {section}{\numberline {}Aqua Bella, cellier des frères de Saint-Jean, soirée du jeudi 28 avril 1160}{137}{section.0.12.2}%
\contentsline {section}{\numberline {}Calanson, cellier des frères de Saint-Jean, fin de veillée du jeudi 5 mai 1160}{140}{section.0.12.3}%
\contentsline {section}{\numberline {}Césarée, hôtel de Garin la Sonnaille, veillée du mardi 21 juin 1160}{142}{section.0.12.4}%
\contentsline {section}{\numberline {}Notes}{144}{section.0.12.5}%
\contentsline {section}{\numberline {}Références}{145}{section.0.12.6}%
\contentsline {chapter}{\numberline {13}Le suc des roseaux}{147}{chapter.0.13}%
\contentsline {section}{\numberline {}Château du Fier, après-midi du samedi 10 octobre 1159}{147}{section.0.13.1}%
\contentsline {section}{\numberline {}Palmeraie de Segor, matin du lundi 8 février 1160}{149}{section.0.13.2}%
\contentsline {section}{\numberline {}Segor, souk au sucre, début de soirée du samedi 8 juillet 1161}{152}{section.0.13.3}%
\contentsline {section}{\numberline {}Segor, souk au sucre, début d'après-midi du mercredi 8 août 1161}{154}{section.0.13.4}%
\contentsline {section}{\numberline {}Notes}{157}{section.0.13.5}%
\contentsline {section}{\numberline {}Références}{157}{section.0.13.6}%
\contentsline {chapter}{\numberline {14}Le persan}{159}{chapter.0.14}%
\contentsline {section}{\numberline {}Abords de Qasr-e Chirin, matin du youm el itnine 11 rabi' al-awwal 555}{159}{section.0.14.1}%
\contentsline {section}{\numberline {}Abords de Tikrit, bords du Tigre, midi du youm al had 4 jadjab 555}{161}{section.0.14.2}%
\contentsline {section}{\numberline {}Jalula, bords du Tigre, soir du youm al sebt 23 jha'ban 555}{163}{section.0.14.3}%
\contentsline {section}{\numberline {}Notes}{166}{section.0.14.4}%
\contentsline {section}{\numberline {}Références}{167}{section.0.14.5}%
\contentsline {chapter}{\numberline {15}Les murailles de Turbessel}{169}{chapter.0.15}%
\contentsline {section}{\numberline {}Forteresse de Turbessel, fin d'après-midi du jeudi 22 avril 1148}{169}{section.0.15.1}%
\contentsline {section}{\numberline {}Forteresse de Turbessel, veillée du lundi 26 septembre 1149}{171}{section.0.15.2}%
\contentsline {section}{\numberline {}Forteresse de Turbessel, matin du mardi 15 août 1150}{174}{section.0.15.3}%
\contentsline {section}{\numberline {}Jérusalem, palais des Courtenay, comtes d'Édesse, soir du jeudi 4 juillet 1157}{176}{section.0.15.4}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, chambre du roi Baudoin, matin du mercredi 8 février 1161}{179}{section.0.15.5}%
\contentsline {section}{\numberline {}Notes}{182}{section.0.15.6}%
\contentsline {section}{\numberline {}Références}{182}{section.0.15.7}%
\contentsline {chapter}{\numberline {16}Accordailles}{185}{chapter.0.16}%
\contentsline {section}{\numberline {}Jérusalem, cour de l'église Sainte-Agnès, matin du dimanche 24 avril 1160}{185}{section.0.16.1}%
\contentsline {section}{\numberline {}Jérusalem, hostel d'Ernaut et Libourc, veillée du dimanche 24 avril 1160}{190}{section.0.16.2}%
\contentsline {section}{\numberline {}Notes}{193}{section.0.16.3}%
\contentsline {section}{\numberline {}Références}{194}{section.0.16.4}%
\contentsline {chapter}{\numberline {17}La vigie du Levant}{195}{chapter.0.17}%
\contentsline {section}{\numberline {}Château d'Harim, début de soirée du jeudi 15 novembre 1162}{195}{section.0.17.1}%
\contentsline {section}{\numberline {}Château d'Harim, soir du vendredi 16 novembre 1162}{198}{section.0.17.2}%
\contentsline {section}{\numberline {}Château d'Harim, nuit du mardi 27 novembre 1162}{201}{section.0.17.3}%
\contentsline {section}{\numberline {}Château d'Harim, nuit du jeudi 29 novembre 1162}{204}{section.0.17.4}%
\contentsline {section}{\numberline {}Notes}{205}{section.0.17.5}%
\contentsline {section}{\numberline {}Références}{206}{section.0.17.6}%
\contentsline {chapter}{\numberline {18}Ultra}{207}{chapter.0.18}%
\contentsline {section}{\numberline {}Jérusalem, hostel d'Ernaut, début de soirée du vendredi 5 février 1160}{207}{section.0.18.1}%
\contentsline {section}{\numberline {}Damas, sud de la Ghûta, veillée du jeudi 25 février 1160}{210}{section.0.18.2}%
\contentsline {section}{\numberline {}Damas, ouest de la Ghûta, veillée du vendredi 10 juin 1160}{213}{section.0.18.3}%
\contentsline {section}{\numberline {}Notes}{216}{section.0.18.4}%
\contentsline {section}{\numberline {}Références}{217}{section.0.18.5}%
\contentsline {chapter}{\numberline {19}Flamme, fumée et cendre}{219}{chapter.0.19}%
\contentsline {section}{\numberline {}Bilbais, berges du Nil, fin de journée du jeudi 21 septembre 1161}{219}{section.0.19.1}%
\contentsline {section}{\numberline {}Faubourgs de Gaza, camp nomade, matin du vendredi 9 novembre 1162}{223}{section.0.19.2}%
\contentsline {section}{\numberline {}Désert du Negev, abords du canyon du Nahal Zin, nuit du youm al arbia 10 chawwal 558}{226}{section.0.19.3}%
\contentsline {section}{\numberline {}Notes}{228}{section.0.19.4}%
\contentsline {section}{\numberline {}Références}{229}{section.0.19.5}%
\contentsline {chapter}{\numberline {20}La main gauche de la nuit}{231}{chapter.0.20}%
\contentsline {section}{\numberline {}Jaffa, faubourgs, abords du quartier hospitalier, tôt le matin du mercredi 16 novembre 1138}{231}{section.0.20.1}%
\contentsline {section}{\numberline {}Tyr, plage septentrionale, début d'après-midi du lundi 6 janvier 1141}{234}{section.0.20.2}%
\contentsline {section}{\numberline {}Jérusalem, parvis du Saint-Sépulcre, fin de matinée du vendredi 13 avril 1145}{236}{section.0.20.3}%
\contentsline {section}{\numberline {}La Mahomerie, église paroissiale Sainte-Marie, veillée du mercredi 17 mai 1150}{239}{section.0.20.4}%
\contentsline {section}{\numberline {}Notes}{240}{section.0.20.5}%
\contentsline {section}{\numberline {}Références}{241}{section.0.20.6}%
\contentsline {chapter}{\numberline {21}Les fruits du labeur}{243}{chapter.0.21}%
\contentsline {section}{\numberline {}Édesse, demeure de Roupen, abords de la citadelle de Thoros, soir du vendredi 15 septembre 1122}{243}{section.0.21.1}%
\contentsline {section}{\numberline {}Turbessel, écuries de Mesrob, après-midi du dimanche 20 août 1150}{247}{section.0.21.2}%
\contentsline {section}{\numberline {}Jérusalem, rue de David, matinée du vendredi 3 juillet 1153}{250}{section.0.21.3}%
\contentsline {section}{\numberline {}Notes}{254}{section.0.21.4}%
\contentsline {section}{\numberline {}Références}{255}{section.0.21.5}%
\contentsline {chapter}{\numberline {22}Commère}{257}{chapter.0.22}%
\contentsline {section}{\numberline {}Clepsta, hostel de Girout et Clarin, soirée du dimanche 6 avril 1158}{257}{section.0.22.1}%
\contentsline {section}{\numberline {}Mirabel, manse des Terres d'Épines, matin du mercredi 3 septembre 1158}{260}{section.0.22.2}%
\contentsline {section}{\numberline {}Clepsta, terres de Girout et Clarin, midi du jeudi 2 octobre 1158}{263}{section.0.22.3}%
\contentsline {section}{\numberline {}Mirabel, manse des Terres d'Épines, soir du mardi 27 janvier 1159}{265}{section.0.22.4}%
\contentsline {section}{\numberline {}Notes}{268}{section.0.22.5}%
\contentsline {section}{\numberline {}Références}{269}{section.0.22.6}%
\contentsline {chapter}{\numberline {23}Nonette suis}{271}{chapter.0.23}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, après-midi du vendredi 24 mai 1129}{271}{section.0.23.1}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, matin du mardi 8 octobre 1146}{274}{section.0.23.2}%
\contentsline {section}{\numberline {}Béthanie, couvent des Saint-Lazare-Marthe-et-Marie, après-midi du lundi 7 octobre 1157}{277}{section.0.23.3}%
\contentsline {section}{\numberline {}Béthanie, couvent des Saint-Lazare-Marthe-et-Marie, matin du jeudi 11 mai 1161}{280}{section.0.23.4}%
\contentsline {section}{\numberline {}Notes}{282}{section.0.23.5}%
\contentsline {section}{\numberline {}Références}{283}{section.0.23.6}%
\contentsline {chapter}{\numberline {24}La raison des femmes}{285}{chapter.0.24}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la porte d'Hérode, après-midi du mercredi 7 avril 1148}{285}{section.0.24.1}%
\contentsline {section}{\numberline {}Encharim, casal d'al-Najjar, matin du jeudi 26 décembre 1157}{288}{section.0.24.2}%
\contentsline {section}{\numberline {}Jérusalem, maison de bains, matin du mardi 21 avril 1159}{290}{section.0.24.3}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la porte d'Hérode, midi du jeudi 6 octobre 1160}{292}{section.0.24.4}%
\contentsline {section}{\numberline {}Notes}{294}{section.0.24.5}%
\contentsline {section}{\numberline {}Références}{295}{section.0.24.6}%
\contentsline {chapter}{\numberline {25}Histoire d'un aller et retour}{297}{chapter.0.25}%
\contentsline {section}{\numberline {}Saint-Vaast, parvis de l'église paroissiale, fin de matinée du dimanche 1er décembre 1146}{297}{section.0.25.1}%
\contentsline {section}{\numberline {}Saint-Vaast, manse de Rémy de la Poterrie, début de soirée du lundi 16 juin 1152}{301}{section.0.25.2}%
\contentsline {section}{\numberline {}Bury, collégiale Saint-Lucien, veillée du mardi 16 novembre 1154}{304}{section.0.25.3}%
\contentsline {section}{\numberline {}Saint-Vaast, manse de Gobert le carrier, veillée du samedi 26 mars 1160}{306}{section.0.25.4}%
\contentsline {section}{\numberline {}Notes}{308}{section.0.25.5}%
\contentsline {section}{\numberline {}Références}{309}{section.0.25.6}%
\contentsline {chapter}{\numberline {26}Curiales}{311}{chapter.0.26}%
\contentsline {section}{\numberline {}Stavelot, abbaye, soir du lundi 20 décembre 1137}{311}{section.0.26.1}%
\contentsline {section}{\numberline {}Freising, palais épiscopal, matinée du mercredi 12 mars 1147}{314}{section.0.26.2}%
\contentsline {section}{\numberline {}Damas, oasis de la Ghuta, chapelle royale de Conrad, matin du mercredi 28 juillet 1148}{317}{section.0.26.3}%
\contentsline {section}{\numberline {}Pavie, palais épiscopal, midi du vendredi 4 mars 1160}{319}{section.0.26.4}%
\contentsline {section}{\numberline {}Notes}{320}{section.0.26.5}%
\contentsline {section}{\numberline {}Références}{321}{section.0.26.6}%
\contentsline {chapter}{\numberline {27}Primus inter pares}{323}{chapter.0.27}%
\contentsline {section}{\numberline {}Acre, château royal, matin du mercredi 10 novembre 1143}{323}{section.0.27.1}%
\contentsline {section}{\numberline {}Naplouse, cour du manoir seigneurial, matin du vendredi 23 juin 1161}{326}{section.0.27.2}%
\contentsline {section}{\numberline {}Jérusalem, Temple de la Milice du Christ, veillée du mercredi 5 juillet 1161}{328}{section.0.27.3}%
\contentsline {section}{\numberline {}Jérusalem, demeure de Droart, soirée du vendredi 21 juillet 1161}{331}{section.0.27.4}%
\contentsline {section}{\numberline {}Notes}{333}{section.0.27.5}%
\contentsline {section}{\numberline {}Références}{334}{section.0.27.6}%
\contentsline {chapter}{\numberline {28}Apostat}{335}{chapter.0.28}%
\contentsline {section}{\numberline {}Antioche, casernement près de la porte du Chien, matin du lundi 4 juillet 1149}{335}{section.0.28.1}%
\contentsline {section}{\numberline {}Antioche, porte du Fer, soir du jeudi 7 juillet 1149}{337}{section.0.28.2}%
\contentsline {section}{\numberline {}Harran, baraquements des soldats, soirée du youm al joumouia 29 jumada al-thani 549}{340}{section.0.28.3}%
\contentsline {section}{\numberline {}Terre de Suète, abords du Yarmouk près des caves de Suète, matin du vendredi 11 juillet 1158}{343}{section.0.28.4}%
\contentsline {section}{\numberline {}Notes}{345}{section.0.28.5}%
\contentsline {section}{\numberline {}Références}{346}{section.0.28.6}%
\contentsline {chapter}{\numberline {29}Modus vivendi}{347}{chapter.0.29}%
\contentsline {section}{\numberline {}Casal de Salomé, demeure d'Abu Qasim, début d'après-midi du youm al joumouia 14 muharram 551}{347}{section.0.29.1}%
\contentsline {section}{\numberline {}Casal de Salomé, demeure d'Abu Mahmud, soirée du youm al talata 17 jha'ban 552}{350}{section.0.29.2}%
\contentsline {section}{\numberline {}Casal de Salomé, mosquée, après-midi du youm el itnine 1 dhu al-qi'dah 553}{352}{section.0.29.3}%
\contentsline {section}{\numberline {}Château de Salomé, grande salle, après-midi du youm al had 19 ramadan 554}{355}{section.0.29.4}%
\contentsline {section}{\numberline {}Notes}{357}{section.0.29.5}%
\contentsline {section}{\numberline {}Références}{358}{section.0.29.6}%
\contentsline {chapter}{\numberline {30}La lettre cachée}{359}{chapter.0.30}%
\contentsline {section}{\numberline {}Sidon, château seigneurial, veillée du vendredi 12 février 1160}{359}{section.0.30.1}%
\contentsline {section}{\numberline {}Sidon, demeure de maître Benjamin, après-midi du mercredi 9 mars 1160}{362}{section.0.30.2}%
\contentsline {section}{\numberline {}Sidon, château seigneurial, fin d'après-midi du lundi 27 juin 1160}{364}{section.0.30.3}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, fin de matinée du samedi 16 juillet 1160}{366}{section.0.30.4}%
\contentsline {section}{\numberline {}Notes}{369}{section.0.30.5}%
\contentsline {section}{\numberline {}Références}{370}{section.0.30.6}%
\contentsfinish 
