Basileia romaion
----------------

### Jérusalem, quartier de la Juiverie, matin du jeudi 20 juin 1157

Tohu-bohu dans la rue. Vacarme de la foire proche. Trop proche. La nuit avait été courte pour Hypatios. Il ouvrit les paupières avec peine. Vite refermés, lumière excessive. Il lui semblait qu'un démon cherchait à lui enfoncer les yeux dans le crâne, tout en le pressant dans son énorme poigne. À chaque mouvement, le sang pulsait dans tout son chef. Il résonnait dans ses cavités oculaires, ses tempes, son front. Il avait toujours eu du mal à se restreindre lorsque le vin était bon, mais il sentait que l'âge lui rendait les lendemains de plus en plus difficiles.

La veille, il avait entrepris d'abreuver un groupe de pèlerins qui revenait de Galilée pour obtenir les informations qui l'intéressaient. L'armée de Jérusalem avait été défaite dans cette région et la plus grande confusion régnait quant au sort du roi lui-même, le jeune Baudoin. Il était de son devoir d'en apprendre le plus possible afin de nourrir son rapport.

Avec précaution, il s'assit péniblement sur le bord de son lit, le regard perdu dans le vague. Dehors, la ville était déjà en effervescence. La nouvelle avait dû se répandre comme un feu de broussailles.

Il appela Felix, son valet, d'une voix hésitante. Ce dernier arriva aussitôt, portant en silence les effets de son maître. Originaire du Soudan, le vieil homme avait réchappé des règlements de compte entre factions fâtimides grâce à l'intervention d'Hypatios. Celui-ci avait profité du chaos ambiant pour revendiquer le guerrier comme étant Felix, son esclave personnel. Dénommé ainsi depuis lors, le soldat s'était par la suite toujours comporté comme tel, ombre mutique au port de tête solennel. Par ce geste généreux et spontané, Hypatios s'était adjoint un ancien combattant d'élite de l'armée musulmane.

Avec peine, il enfila son makhlamion[^1] de soie bleue brocardée, ajusta lentement ses chaussettes de coton puis laça ses souliers bas, ouverts sur le coup de pied. Tandis qu'il fixait son turban par-dessus son bonnet de feutre, Felix lui apporta de quoi se restaurer : des beignets, du raisin, des dattes fourrées, des pistaches, le tout arrosé d'une boisson au miel. Il se sentait un peu nauséeux, mais savait qu'avaler une légère collation aidait parfois. En outre, il avait tout son temps ce matin : il s'attendait juste à recevoir un confrère qui lui avait promis quelques courriers quand ils s'étaient croisés plusieurs jours auparavant.

Hypatios faisait essentiellement du négoce de poivre et d'étoffes et avait surtout des participations dans des transports de marchandises entre la Tunisie, la Sicile, Alexandrie, Acre et Byzance. C'était grâce à ce réseau de partenariats commerciaux qu'il était très bien informé de ce qui se passait dans les territoires francs. Et ses lointaines origines syriennes lui permettaient de nouer d'excellents contacts avec les populations locales de Jérusalem, Antioche et Tripoli.

Ayant achevé son repas, l'esprit désormais plus clair, il commença à chiffrer un rapport à destination des autorités byzantines, tout en jetant de temps à autre un coup d'œil par les claustras qui surplombaient la rue, depuis son rawshan[^2]. Aucune agitation n'était visible dans son quartier, peuplé essentiellement de chrétiens orthodoxes, coptes ou jacobites dont beaucoup se sentaient plus proches des indigènes musulmans que des Francs venus d'Europe qui les dirigeaient actuellement.

Il vit arriver Halfon al Fustani Abu Harith, son collègue et ami égyptien, entouré d'une bonne demi-douzaine de serviteurs et assis sur une mule magnifiquement parée. Juif de confession, c'était un petit personnage au visage émacié dissimulé derrière une belle barbe grise, dont l'apparence austère cachait un cœur d'or. Banquier et gros négociant avec les Indes, il comptait parmi les plus riches donateurs de sa communauté au Caire et à Fustat et rachetait régulièrement des prisonniers. Peu lui importaient leurs origines dès l'instant où ils appartenaient à un de ses cercles de relation; il s'efforçait de les libérer de leur geôle latine, turque, arabe, byzantine ou égyptienne. Quelles que soient les nouvelles officielles, le petit homme en aurait la primeur. Le statut des Juifs était toujours précaire sous l'administration franque, et les plus éminents d'entre eux se tenaient en permanence informés des remous qui agitaient le pouvoir latin.

### Jérusalem, quartier de la Juiverie, après-midi du mercredi 26 février 1158

Son déjeuner à peine avalé, Hypatios avait revêtu son epilorikion[^3] de la plus belle facture. Comme toujours accompagné de Felix, il se rendait à la maison de Rafold de Trêves. Ce dernier était très bien implanté à Ascalon et entretenait des relations régulières avec des membres de la famille d'Ibelin. Felix et lui étaient en affaires depuis pas mal d'années et avaient plaisir à se fréquenter. Ils ne manquaient pas de s'informer l'un l'autre de toutes les nouvelles qui pouvaient contrarier ou faciliter leurs investissements respectifs.

Malgré un abord plutôt distant, Rafold était fidèle en amitié, sans jamais y concéder de démonstration bien claire. Il se rendait régulièrement par delà les mers, en Champagne, où il rencontrait des marchands hanséates[^4]. Il en rapportait des récits merveilleux sur les coutumes des peuples francs, qui faisaient souvent sourire ou s'esclaffer le Byzantin. L'allemand avait une façon bien à lui de raconter les choses, marmonnant parfois et semblant indifférent à la drôlerie ironique de nombre de ses remarques. Il était capable d'accorder autant de sérieux à l'évocation d'une anecdote autour d'un âne et d'un chien qu'à des comptes-rendus d'opérations commerciales conjointes. Il narrait tout cela d'une voix monocorde et inarticulée, sans montrer d'amusement devant l'émoi que suscitaient ses propos.

C'était également pour le Byzantin la source principale sur ce qui se passait dans les terres allemandes. En échange, Hypatios le régalait des nouvelles venues de Méditerranée orientale et des Indes. Hypatios avait reçu de façon générale la consigne de se renseigner sur les relations avec l'Égypte. Depuis peu s'y ajoutait aussi une curiosité pour les sentiments du jeune roi des Romains Frédéric[^5] à l'encontre du récent maître de la Sicile, le très belliqueux Guillaume qui s'en prenait violemment aux intérêts byzantins à l'ouest. Rafold aurait certainement des informations de première main, lisibles à travers les aléas du commerce dans les territoires occidentaux.

Comme toujours, Hypatios collecterait et rendrait compte sans préjuger de ce qui pourrait en être décidé. La diplomatie byzantine était tellement alambiquée que même ses exécutants n'en voyaient guère le plan d'ensemble. Le négociant avait parfois l'impression que ce n'était qu'une grosse bête aiguillonnée en tous sens par une légion de bouviers, et qui bringuebalait sans trop savoir vers où elle avançait.

Chemin faisant, assis sur sa mule menée par Felix, Hypatios remarqua immédiatement qu'au dehors de la partie nord-est, la ville était effervescente. Des hommes d'armes étaient plus fréquemment visibles qu'à l'accoutumée. Depuis le quartier syrien, où il résidait, il lui fallait traverser la rue de Josaphat, toujours emplie de pèlerins. Il devait après cela longer à main gauche le quartier du Temple ; la Porte des douleurs puis l'entrée principale, la Belle Porte si magnifiquement décorée, étaient encadrées par des sergents à l'allure sévère, revêtus de leur armure. Ils dévisageaient tous les badauds d'un regard soupçonneux. Il tourna sur sa droite en direction du passage qui enjambait la vallée des Fromagers, vers le centre de la ville.

Le roi était annoncé, de retour d'une campagne victorieuse dans le nord, avec la prise d'Harim. Malgré l'échec à Césarée sur l'Oronte[^6], tout le monde semblait satisfait et les soldats déjà de retour dépensaient leur butin avec prodigalité. C'était toujours l'occasion d'une certaine fébrilité, le vin et l'inactivité après des semaines, voire des mois de combats rendaient les hommes plastronneurs et querelleurs. Il n'était alors jamais loin le temps des massacres des communautés locales, dont les autochtones comme Hypatios avaient entendu parler toute leur enfance.

La rue du Temple qui menait à l'entrée du quartier allemand était elle-même plus remuante qu'à l'ordinaire, les gens discutaient plus qu'ils ne faisaient affaires. Avec de grands gestes et force exclamations, on commentait abondamment la querelle entre barons qui avaient entraîné l'abandon du siège de la cité sur l'Oronte. Hypatios aperçut quelques sergents du Temple, habillés de leur manteau noir, qui étaient apparemment en train de percevoir les loyers de magasins qui leur appartenaient. Il sourit pour lui-même, voyant que le commerce continuait en toutes circonstances. Enfin il arriva dans la rue qui menait à l'Hôpital de Sainte-Marie-des-Allemands, ainsi qu'au quartier où résidait Rafold.

Légèrement en retrait de la voie principale, accessible par une étroite venelle qui serpentait sous les balcons ajourés, l'habitation de son ami était assez petite. Il avait acheté quelques pièces sous les toits, qui donnaient sur la cour intérieure d'une grande demeure. Au moins, ils étaient au calme pour discuter des événements. Après les salutations d'usage, une fois confortablement installés sur les coussins du suffah[^7], ils parlèrent un peu de tout et de rien, s'enquérant poliment de leurs familles respectives avant d'en venir au fait de leurs affaires. Au moment de faire son rapport sur des balles de tissus parties à Gênes, Rafold bifurqua pour évoquer une relation commune dont on racontait qu'il avait perdu ses souliers lors d'une épopée qui l'avait conduit dans une fosse de vidange dans les bas-fonds de Rome. Chaussures dont on disait qu'elles valaient plusieurs livres...

### Jérusalem, quartier de la Juiverie, fin d'après-midi du mardi 14 octobre 1158

Rendez-vous avait été fixé en fin de journée à Eudoxius Eirenikos pour parler affaires, comme chaque fois que celui-ci passait dans la cité sainte. Hypatios avait également l'intention de transmettre toutes les informations glanées à Nicéphore, un des suivants du riche négociant d'orfèvrerie et de bronze. Manchot à l'allure et à la démarche de soldat, celui-ci accompagnait depuis quelques mois Eudoxius. Il s'était fait connaître à Hypatios lors d'une précédente entrevue, au hasard d'un couloir, et lui avait donné tous les mots de contact qui faisaient de lui un nouveau collecteur de ses rapports. Ils prenaient chaque fois l'apparence de lettres commerciales, recelant un message chiffré destiné aux yeux du renseignement, que Nicéphore mêlait à d'autres documents sans valeur. Tout cela se faisait à l'insu, semblait-il, d'Eudoxius, garantissant ainsi sa totale sincérité en cas d'accusation.

Le riche marchand était arrivé de Damas deux ou trois jours auparavant, hébergé dans un comptoir d'une de ses relations, et devait rentrer d'ici peu à Byzance avant que le froid et les intempéries ne rendent la chose impossible pour l'hiver. Avant toute chose, ils échangèrent quelques informations sur le commerce et firent le point sur certains accords et partenariats qu'ils avaient ensemble. Un lot de balles de feutre byzantin livré depuis plusieurs mois aux frères de l'Hôpital de Saint-Jean était venu à terme pour le règlement, ce que Halfon al Fustani Abu Harith avait appris à Hypatios une poignée de semaines plus tôt. Il avait délivré deux bourses de dinars scellées en paiement, mais la plupart des fonds avaient été immédiatement réinvestis par les associés.

Une fois ces choses triviales achevées, ils se firent apporter de quoi dîner. Bien qu'ils soient des exilés loin de leur patrie, il était possible de trouver quelques plats de leur pays : keftedes[^8] et pain levé, ainsi que des pastfelis[^9] en dessert, accompagnant quelques fruits, le tout arrosé de vin de Chypre. Après avoir écouté attentivement les nouvelles du royaume latin, Eudoxius fit part de ses sentiments à son hôte. Sans faire partie du réseau de renseignement, ses allées et venues le rendaient précieux pour comprendre ce qui se passait dans les territoires musulmans et il ne manquait pas d'avis et de savoir sur les intrigues de palais byzantines. Être au fait de ce qui se tramait dans les cercles de pouvoir semblait vital à Hypatios afin de ne pas commettre d'impair. Si le métier d'espion avait des avantages, il n'était pas sans risque, même dans son propre camp.

« Le basileus Manuel a décidé de montrer sa puissance à tous ces jeunes Celtes. On dit qu'il a levé tant d'hommes qu'ont ne voit la fin de la colonne depuis sa tête !

--- Je le croyais fort incliné à leur concéder amitié et soutien. Penses-tu que nos affaires pourraient en souffrir ?

--- Certes pas. Il veut juste leur rendre sensible la magnificence de Rome. Toute grande âme qu'il soit, il ne saurait tolérer les affronts du jeune prince d'Antioche. Et personne n'a oublié qu'ils s'ajoutent à la longue liste initiée par le parjure Bohémond. »

Hypatios acquiesça. Nul Byzantin n'avait pardonné la parole donnée puis trahie de restituer Antioche à l'empire, une fois délivrée du joug musulman, lors de la Première croisade. Eudoxius sourit rapidement, avant de continuer avec affabilité.

« Quoi qu'il en soit, le jeune Baudoin est maintenant de sa famille, du fait du mariage avec sa nièce Théodora. Désormais cousin cher à son cœur, il aura sûrement désir de l'instruire.

--- Si fait. Il a d'ailleurs toujours eu grande amabilité avec les Celtes. »

Tout en continuant à discuter, les deux hommes picoraient dans les friandises sucrées qui servaient de désert. La nuit commençait à couvrir la ville, le bruit ambiant retombait tandis que les habitants se renfermaient chez eux.

Hypatios finit par prendre congé. Il ne résidait qu'à quelques pâtés de maison de là et rentra à pied, suivi de Felix, porteur d'une lampe. Il profita de la fin de cette belle soirée pour monter sur le toit de sa demeure admirer le paysage. Il aimait voir les derniers rayons de soleil éclairer les jardins qui longeaient la muraille au nord, tandis que les cours intérieures et les fenêtres barrées de claustras commençaient à scintiller de mille feux.

À l'ouest il observa un temps le massif ensemble de la Tour de David, se détachant derrière le Saint-Sépulcre et le quartier du Patriarche. On disait que de grands travaux y étaient prévus, afin de renforcer ce point de défense crucial de la cité. Il était curieux de savoir ce que l'avenir réservait à Jérusalem. Elle était loin de receler tous les attraits de Byzance, la mère de toutes les villes, et n'égalait même pas en taille la plupart des agglomérations côtières, plus animées. Il n'était pas particulièrement religieux et n'expliquait pas son attachement à des raisons de foi. Mais il ne quittait jamais sans regret cette cité parmi les arides collines de Judée.

Il était heureux que l'empire et le royaume tissent des relations de plus en plus fortes et fondait beaucoup d'espoir sur l'importante rencontre qui allait se dérouler dans le nord, entre Manuel et les seigneurs latins.Des tensions demeuraient, de nombreux barons francs voyaient avec dépit la tutelle byzantine se profiler, mais le jeune Baudoin semblait plutôt enthousiaste à l'idée de s'allier à pareille puissance. Il fallait juste qu'il comprenne qu'il ne saurait être question d'égalité quand on traitait avec un empire millénaire dirigé par le basileus porphyrogénète. ❧

### Notes

Les relations entre le gigantesque Empire byzantin, dont on oublie souvent la prépondérance au Moyen-Orient médiéval, et ses voisins latins étaient loin d'être univoques. Sous le règne de Manuel Comnène, on peut parler d'une bienveillance mutuelle, mais cela ne se faisait pas sans arrière-pensée. Le pouvoir byzantin se posait en référence et considérait généralement les autres comme des satellites. Il avait clairement la puissance militaire, commerciale et culturelle pour déployer ses ambitions, mais s'étendait sur un tel territoire que cela ne se faisait jamais sans batailles ou manœuvres diplomatiques.

Pour arriver à ses fins, il lui était nécessaire de recueillir des informations sur ses adversaires, partenaires, vassaux. Cela se faisait au travers de correspondants, espions dont certains étaient parfois des locaux corrompus, auxquels il était versé des émoluments. Le tout était ensuite collecté et centralisé par l'administration supervisée par le logothète du drome, sorte de ministre des communications et responsable du « bureau des étrangers », en relation avec les ambassades. On sait que les messages étaient transmis de façon dissimulée ou codée, sans avoir des détails sur l'étendue des méthodes employées.

Basileia : du grec Βασιλεία , désigne un pouvoir, une autorité, un royaume. Βασιλεία Ρωμαίων, *basileia romaion*, désigne l'Empire byzantin.

### Références

Dvorník François, *Origins of Intelligence Services : The Ancient Near East, Persia, Greece, Rome, Byzantium, the Arab Muslim Empires, the Mongol Empire, China, Muscovy*, Rutgers University Press, New York : 1974

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Fondation
---------

### Jérusalem, sud du mont Sion, après-midi du dimanche 27 juillet 1158

Malgré un soleil étouffant, une large foule s'était amassée sur les pentes desséchées du mont Sion. Beaucoup de visiteurs venus célébrer l'anniversaire de la prise de la cité étaient restés quelques jours et profitaient donc de la petite foire qui s'était agglutinée au fil des ans autour d'un concours estival d'archerie. On y déambulait autant pour admirer des bateleurs, des chanteurs et des montreurs de bêtes que pour y voir les prouesses des tireurs.

Fatigué de sa récente campagne militaire, Ernaut n'avait pas eu le courage de se rendre jusqu'à Mahomeriola pour y assister à la messe comme chaque fois qu'il en avait l'occasion le dimanche. Il était resté à se reposer tard dans la matinée et n'était venu flâner au dehors des murs que poussé par la curiosité envers les clameurs dont il entendait les échos.

Un village de toile et de cabanes s'était monté, hébergeant commerces de colifichets, étals de merciers et marchands de boissons. Des vendeurs ambulants proposaient de l'eau, du vin ou de la bière, des pâtés salés et des friandises. Des enfants couraient, battant des mains à la découverte de chiens cabriolant, de singes dansants ou de panthère acceptant les caresses. Le tout au milieu du tintamarre des instruments et des voix en concurrence pour attirer l'attention des badauds. La chaude poussière d'été, soulevée par les milliers de pieds, irritait la gorge et noyait de gris les vêtements de fête colorés.

Ernaut avait tenté de suivre un spectacle de théâtre d'ombres en langue locale, mais les moments chantés et l'usage de fréquents termes argotiques lui rendaient la compréhension difficile. Il en avait néanmoins bien saisi le thème, à la fois ordurier et pornographique, qui tournait autour des mésaventures d'un naïf fellah ayant fui sa campagne pour une opulente cité. Floué et trompé par tous ceux qu'il croisait, il en était à se lamenter sur les malheurs de sa condition lorsqu'Ernaut entendit la voix amicale d'Abdul Yasu qui, l'ayant reconnu, s'avançait vers lui, un large sourire sur les lèvres.

Ils se congratulèrent avec chaleur et, ne s'étant pas vus récemment, décidèrent d'aller quérir de quoi se désaltérer pour prendre des nouvelles l'un de l'autre. Ils espéraient trouver où s'asseoir sous les arbres pour partager leur boisson, mais les places étaient rares. Ils remontèrent donc jusqu'aux abords du mur de l'abbaye du Mont Sion, où ils s'affalèrent sur un rocher faisant saillie sur le périmètre. Abdul était, comme toujours, très bien habillé, et paraissait sortir de chez le barbier. Ernaut se sentit légèrement en défaut, vêtu sans recherche et mal rasé. Il n'était d'ailleurs pas allé aux bains depuis plusieurs jours.

Ils échangèrent à propos des événements récents, Ernaut profitant de sa fraîche expérience de soldat pour se mettre en avant. Il fut un peu déçu de voir qu'Abdul ne semblait pas accorder à la valeur militaire autant de prestige que ses amis latins. Il s'intéressait plus aux ragots autour du palais et aux dernières nouvelles du mariage royal. Il était très curieux de savoir si Ernaut avait pu découvrir à quoi ressemblait la jeune Théodora, future reine de Jérusalem, dont on vantait la beauté et la richesse. Abdul se rappelait qu'il avait été impressionné par Mélisende lors de son union avec Fouques. Il gardait un souvenir ému de l'avoir rencontrée au cours d'une session publique où elle avait accueilli son père parmi certains plaidants. Sans vraiment faire de politique, il regrettait qu'elle soit désormais retirée à Naplouse, les privant ainsi de sa présence bienfaisante. Il n'avait aucune sympathie pour le jeune roi friand de batailles et d'exploits, se risquant à l'évoquer en des termes qu'il n'attribuait en général qu'aux Turcs, auxiliaires militaires utiles, mais trop peu civilisés à son goût.

Ernaut se contentait de hocher la tête à ses remarques, découvrant une fois de plus la divergence d'affinités qu'il avait avec les locaux. Lui aimait à entendre parler des hauts faits d'armes des grands capitaines de guerre. Il avait d'ailleurs développé depuis peu un certain attrait pour le prince d'Antioche, Renaud de Châtillon, dont l'audace et la férocité nourrissaient la verve de nombreux conteurs de rue.

Avec la chaleur, leurs bouches se desséchèrent à tant discuter, et ils se turent un moment, profitant en silence de leur vin, admirant l'agitation de la foule autour du long champ de tir. Abdul avait aussi acheté quelques biscuits à la figue, qui laissait leurs doigts collants de miel.

« Dis-moi, Abdul, ta famille est dans la cité depuis de nombreuses années ?

--- Que oui ! Quand Al-Hākim bi-Amr Allah[^10] détruisit le tombeau du Christ, mes pères étaient déjà des anciens, ici.

--- Saurais-tu me dire où je pourrais trouver bel hostel où installer femme et enfants ?

--- Cela ne dépend que des besants que tu y pourras consacrer, mon ami, s'amusa Abdul. Vers les jardins du quartier du Patriarche, aux abords de la tour de Tancrède, mieux vaut être marchand de soie et de poivre. Mais vers la porte des Tanneurs, tu encontres de quoi t'abriter à faible prix. Êtes-vous nombreux à devoir y habiter ?

--- Pour l'heure, je ne saurais dire. J'ai trouvé accord avec la famille de la femme que je vais épouser, et j'ai désir de la bien loger. Nous aurons peut-être valet et, j'espère, forte marmaille...

--- Te voilà aussi prêt à porter le joug ! » ricana Abdul, dont les perspectives de noces semblaient moins enthousiasmantes[^11].

Il avala un peu de vin, expira longuement et lança un coup d'œil vers les murs au Nord.

« Il y a pas mal de belles demeures à des prix corrects, dans le quartier d'ici, au près de la porte Beaucaire. Si tu n'as rien contre les Ermins[^12], du moins. Ils sont surtout par là, dans la Cité. C'est plus calme que la rue du Temple où tu résides, ou les environs du palais. »

Il réfléchit encore un petit moment, tirant les poils de sa barbe.

« Il n'y a que fort peu de belles demeures à prix correct auprès du Sépulcre et de l'hôpital Saint-Jean, mais en te rapprochant de la Juiverie, tu pourrais trouver de quoi te satisfaire. En plus tu ne serais pas loin de la taverne du vieux Josce !

--- Je ne sais s'il est bien sage de vivre si près d'un tel endroit, s'amusa Ernaut. Mais il est vrai que le quartier est fort agréable. J'y ai vu de nombreux jardins et les rues bien closes à la nuit y rendent les périls fort rares.

--- J'ai quelques amis d'amis qui résident par là-bas. De nouveaux arrivants amenés par le vieux Baudoin[^13] depuis l'Oultre-Jourdain lorsque les Juifs furent chassés. »

Ernaut leva son verre en un toast silencieux en guise de remerciement. Il n'avait pas escompté pouvoir si bien s'installer lorsqu'il avait commencé à penser à un possible mariage avec Libourc. Mais le vieux Sanson serait certainement sensible à un bel établissement et le dédommagement qu'il toucherait des imbéciles qui l'avaient agressé[^14] serait bien investi, en offrant un accueillant cadre de vie à sa famille.

### Jérusalem, palais royal, après-midi du jeudi 25 juin 1159

C'était la première fois qu'Ernaut pénétrait dans les quartiers personnels du vicomte depuis qu'il était entré au service du roi. Ce n'était qu'une petite chambre, mais avec une belle fenêtre qui donnait sur une courette aménagée en jardin. On y entendait piailler quelques oiseaux malgré la chaleur de l'après-midi. Devant un lit imposant aux courtines de sobre toile de laine à la couleur passée, Arnulf était agenouillé face à un lourd coffre bien ferré et finissait de le cadenasser.

« Tu peux prendre cette huche, dès à présent, et la porter avec mes autres affaires près la grande cour. »

Lorsqu'Ernaut acquiesça, Arnulf vit bien que le jeune homme était contrarié. Il l'interrogea du regard sans un mot, ainsi qu'il le faisait souvent pour inviter un des sergents à s'exprimer. Ernaut n'hésita qu'un instant avant de prendre la parole d'une voix embarrassée.

« Est-ce bien vrai, mon sire vicomte, que vous quittez service le roi ?

--- Je vois que la nouvelle a couru bien vite. Nous en ferons annonce ce dimanche, après messe, en la grande salle de la Haute Cour. »

Ernaut hocha la tête, atone. Sans pour autant s'estimer proche de lui, il s'était habitué à Arnulf et l'appréciait pour son abord franc, quoique parfois rude. Malgré tout, il était bon meneur d'hommes et savait exercer son autorité sans avoir à la démontrer. Il était aussi constamment à l'affût de la moindre rumeur. S'il était compétent en tant que vicomte, c'était dû à sa capacité à anticiper, encore plus qu'à son habileté à régler les problèmes.

« Et murmure-t-on le nom de qui va me succéder ?

--- On dit que ce serait l'ancien maréchal le roi, sire Eudes de Saint-Amand.

--- Je vois que les murs écoutent fort bien. Quoi de plus ? »

Ernaut inspira longuement. Arnulf était homme de palais depuis suffisamment d'années pour savoir que le personnel d'une maisonnée était toujours le premier à être informé des choses. Il n'était pas rare qu'un valet soit au courant avant son maître des nouvelles qu'il allait recevoir ou entendre. C'était de bonne fame, et il n'y avait pas à en avoir honte. Pourtant, il n'était pas à l'aise devant cet homme inquisiteur, dont les yeux sondaient l'âme, depuis leur abri de sourcils noirs.

« Que mestre Ucs s'en va également...

--- C'est presque cela. Il te faudra d'ici peu lui donner du sire, à lui aussi. Il sera mon féal et recevra fief-rente, comme chevalier du comte qu'il sera. »

Arnulf s'assit sur son coffre, prenant son menton dans sa main durant un moment. Il savait Ernaut curieux à rendre jalouse la plus entreprenante des pies, mais il avait perçu en lui de grandes capacités. Peut-être que ce temps de transition lui serait profitable. Parmi les officiers royaux, on parlait parfois de ce jeune géant à l'intelligence surprenante qui officiait au palais. Il appréciait surtout son dévouement, sa sincérité à servir. Pour lui, c'était la principale qualité d'Ernaut.

« Je ne serai plus homme le roi, mais de l'arrière-ban, châtelain de son frère à Blanchegarde. Ucs me suit, car il me faut avoir à ma dextre quelqu'un à qui je puisse me fier. Outre cela, le sire Eudes sera châtelain le roi ici, à Jérusalem. Nul n'avait détenu ce titre en sus de celui de vicomte depuis le vieux Rohard, des années avant que tu ne viennes. Cela veut dire que beaucoup de choses vont changer, mon garçon. Pareils temps sont propices aux personnes de talent. »

Il fixa un moment le colosse face à lui, appréciant le visage massif et les épaules larges, les bras puissants. S'il savait utiliser sa tête aussi bien que ses muscles, Ernaut avait de quoi aller loin. Ne restait que la question du cœur, dont Arnulf se désintéressait généralement, laissant à Dieu et à ses clercs ce soin, se contentant d'une obéissance sans faille à ses instructions.

« Beaucoup ont frappé à mon huis ces derniers jours, afin de me vanter leurs mérites ou de chanter les louanges d'un de leurs bons compagnons parmi la sergenterie le roi. Mais il n'est de nul avantage à venir m'ennuyer ainsi. Le nouveau mathessep n'est pas issu du rang, c'est mestre Fouques, que tu croises tantôt à mon service. »

Ernaut ne put cacher sa déception au vicomte. Contre toute logique, il s'était persuadé qu'il aurait pu faire, à l'occasion, un mathessep tout à fait honnête. Ou, à défaut, qu'un de ses proches, Droart, ou plus probablement Eudes, soit désigné. Arnulf grimaça un court instant, peut-être un fugace sourire moqueur, à peine discernable sur sa mince bouche sans lèvres.

« Tu auras ton temps, mon garçon. *Tout fut a autrui, tout sera a autrui*. »

Ernaut comprit surtout qu'en plaçant son jeune protégé à ce poste, Arnulf gardait un pied dans le palais. Il faudrait à Ernaut attendre encore quelques années avant d'espérer meilleure solde. Pour les batailles, peut-être pourrait-il se faire nommer à cheval, vu que le vieux Gaston ne sortait plus guère. En ce cas, il serait nécessaire qu'il soit plus volontaire afin de participer aux campagnes royales. Il était rare qu'on oblige les moins valeureux à prendre part au combat. Un fantassin qui n'avait pas le cœur solide était un danger pour les siens. Seulement, partir en guerre à tout va n'était pas la meilleure façon de s'y prendre pour fonder une famille.

### Jérusalem, quartier de la Juiverie, matin du vendredi 27 novembre 1159

Le soleil commençait à peine à vider les rues de leurs ténèbres et les températures demeuraient fraîches. La nuit précédente, une fine bruine avait mouillé les terres et une odeur de salpêtre et de moisissure montait des gravats accumulés dans la petite cour où se tenaient Ernaut et Droart. Ce dernier avait déniché une magnifique borgesie à son ami, qui n'était redevable que d'un cens fort modéré à verser au Saint-Sépulcre. Située un peu au nord de Sainte-Anne, dans la partie la plus orientale de la Juiverie, elle était à un jet de flèche de la muraille nord. Les conditions en étaient avantageuses, car il y avait beaucoup d'aménagement à y faire.

Le lieu avait été abandonné lors de la prise de la ville et personne n'y avait habité depuis lors. Heureusement les toits avaient tenu et les planchers étaient solides. Mais portes et fenêtres étaient en piteux état et un aliboufier avait défoncé l'entrée de la petite cave, où se trouvait également une citerne. La rue qui desservait l'endroit n'était peuplée que de Latins et le chef de quartier était pompeusement surnommé « Le François », car ses ancêtres étaient de Paris. Son teint mat et sa plus grande habileté en arabe qu'en langue d'oïl le faisaient néanmoins plus semblable aux fellahs qu'on croisait dans certains casaux. C'était un homme agréable, aussi rond de caractère que d'aspect et il s'enthousiasmait à l'idée qu'un sergent du roi, un tel géant, s'installe près de chez lui. Cela n'en rendrait les lieux que plus tranquilles.

La maison en elle-même était spacieuse, avec un bel étage à deux pièces, agrémenté d'un magnifique rawshan[^15] en bois marqueté qui ne demandait qu'à être remis en état. Au-dessous, une imposante cuisine occupait la moitié de la zone, avec une réserve adjacente. Sur le toit-terrasse, de petits appentis, anciens logements de domestiques certainement, permettaient de faire également du stockage, avec une vue superbe vers l'ensemble de la cité sainte. Enfin, une cour en terre battue, mangée de mauvaises herbes, accueillait en son fond la cave en partie enterrée, accolée de la citerne. C'était ce dernier point qui avait conquis Ernaut. L'idée de disposer à demeure d'eau à volonté lui semblait une mesure de bon sens dans ce pays où il pleuvait si peu durant de longs mois. Au-dessus de cette cave, un ancien entrepôt pourrait servir d'atelier ou d'abri pour une basse-cour.

« Il serait plus prudent de faire curer la citerne, expliquait Droart. Quand elles sont laissées à l'abandon durant des années, va savoir ce qui a fini dedans. Mais en ce cas, il faudrait le faire au plus vite. La saison pluvieuse est pratiquement sur nous et ce serait dommage de ne pas profiter de cette eau.

--- As-tu connoissance de quelques manouvriers habiles à cela ?

--- Pour cela, n'importe quel vidangeur suffira. On peut proposer de faire marché à moitié sur le produit de ce qu'on y trouvera, en plus de leur soudée, ça les motivera. »

Ernaut hocha la tête. Droart avait l'habitude de gérer des projets immobiliers et connaissait beaucoup de gens en ville. Son caractère affable et sa capacité à conclure des affaires, parfois douteuses, pour le plus grand profit de tous, y compris le sien, lui avaient permis de s'enrichir confortablement depuis quelques années. Son humeur, légère et primesautière, n'y avait néanmoins pas été gâtée et il demeurait le bon compagnon qu'il avait toujours été, chaussé de ses vieilles savates et revêtu de ses cottes perpétuellement froissées.

Ils montèrent l'escalier menant à l'étage et pénétrèrent dans la chambre à l'arrière du bâtiment où les fenêtres, modestes, donnaient sur la cour. Les murs récemment chaulés scintillaient dans les premières lueurs du jour.

« Je vais bientôt pouvoir loger ici, se réjouit Ernaut.

--- Tu ne veux pas attendre d'y venir en homme marié ?

--- Je pense qu'il serait mieux que je m'y installe au plus tôt afin de préparer les lieux. La maison ne fait pas tout. J'ai besoin d'un lit, de huches et d'une table, de bancs et d'escabeaux, de pots et cruches... tant à faire !

--- Laisse donc à Libourc le soin de ce fatras! Elle saura étriller ta bourse bien plus vite qu'elle n'a gonflé, crois-m'en ! »

En disant cela, Droart prit conscience qu'Ernaut n'avait toujours pas annoncé de date pour son mariage. Les choses avançaient, clairement, mais jamais Ernaut ne parlait de la cérémonie à venir, comme si l'idée le mettait mal à l'aise. Il espérait que son ami ne s'emballait pas trop sur des perspectives mal assurées. Trop souvent, de modestes prétendants se voyaient écartés au dernier moment par une famille soucieuse de préserver les intérêts d'une de leurs filles. Plus jeune, il en avait cruellement fait les frais et ne souhaitait certes pas qu'Ernaut connaisse cette amère désillusion.

### Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159

Confortablement installé près du feu, Ernaut commençait à s'assoupir tranquillement en attendant le repas. Il avait les narines délicatement chatouillées par les odeurs de cuisine des derniers plats de fête que Mahaut concoctait. Lors des célébrations religieuses, elle mettait un point d'honneur à ne pas servir deux fois la même recette. Du coup, depuis son arrivée la veille au soir, il avait profité d'un véritable festin. Ce n'était pas mets de baron, comme il en goûtait de temps à autre les restes au palais les jours de grands banquets, mais d'habiles variations autour de préparations simples bien apprêtées. Cette abondance nourricière et la fatigue accumulée des dernières semaines l'incitaient à une petite sieste avant de se retrouver de nouveau à table.

Regardant sans le voir le feu dansant devant ses yeux, il se remémorait le dernier Noël qu'il avait passé à Vézelay. Comme cette période noire de sa vie lui paraissait loin ! Il avait franchi les mers en pénitence et avait trouvé ici un nouveau monde. Il accomplissait les pas qui achèveraient cette partie de son existence, d'enfant insouciant. Il s'estimait prêt à cela. Il avait désormais ses habitudes au sein de l'hôtel du roi, même s'il se sentait un peu à l'étroit dans les rôles où on le cantonnait pour l'instant. Il avait un petit cercle d'amis fidèles, son frère pas loin, et avait emménagé dans sa future demeure, où sa famille s'établirait d'ici peu.

Il aperçut Libourc qui assistait sa mère dans la mise en place du repas. Habillée de sa plus élégante cotte de laine moutarde, elle avait une longue natte qui voyageait sur ses épaules tandis qu'elle s'agitait pour les derniers préparatifs. En sentant le regard de son fiancé sur elle, elle lui adressa un rapide sourire affectueux. La table était presque prête, décorée de la plus belle vaisselle, dont les motifs verts et bruns apparaîtraient au fut et à mesure que les plats se videraient. Une nappe de lin clair, ornée de délicats tracés géométriques reflétait la lumière des chandelles sorties pour l'occasion. On n'attendait plus que le chef de famille, Sanson, occupé à quelque tâche personnelle dans le sous-sol.

Quand il parut enfin, le visage souligné de feu par sa lampe à graisse, il arborait un air malicieux, jouant avec une petite clef dans sa main libre. Mahaut les invita à prendre place et s'assit à son tour. Le vieil homme leur sourit alors et prit son temps avant de réciter l'habituel bénédicité, en un latin haché et mal maîtrisé.

« Benedic, Domine, nos et haec tua dona quae de tua largitate sumus sumpturi[^16].

--- Amen. »

Alors que chacun allait se servir, il leva la main et toussa légèrement, avant de poser sa petite clef de coffre sur la table.

« Si je n'ai pas mauvaise mémoire, il ne manque rien à la dot de ma fille que j'ai enserrée en cette huche. Et tu m'as dit que tout était prêt, de ton côté, Ernaut. Alors, mes enfants, il est plus que temps de fixer une date pour vos épousailles, non ? » ❧

### Notes

Le mariage au XIIe constitue un événement principalement civil. Si l'Église y avait part depuis des siècles, elle n'en a fait un sacrement qu'avec difficultés et précautions. Des tendances antagonistes au sein du clergé ont dû être maîtrisées pour permettre sa partie religieuse. Néanmoins, il demeure essentiellement profane, avec des conséquences civiles de lien de personne à personne. En outre, les usages semblent avoir varié parfois de façon importante selon les régions et les coutumes, et je n'ai rien trouvé de vraiment spécifique sur ce qui se faisait parmi les colons latins au Moyen-Orient. J'ai donc tenté de bâtir sur ce qui était perceptible d'autres zones européennes, en l'adaptant aux particularités démographique et sociale du royaume de Jérusalem que j'ai pu retracer.

La partie préparatoire de l'union entre Libourc et Ernaut va faire l'objet d'une petite trilogie de Qit'a, m'offrant l'occasion de dévoiler différents points de vue et diverses pratiques selon des éclairages croisés. J'espère aussi par ce biais mieux rendre sensible ce que pouvait être, symboliquement, le mariage de gens du peuple à ce moment de l'histoire.

### Références

d'Avray D. L., *Medieval marriage - Symbolism and Society*, Oxford : Oxford University Press, 2005.

Donahue, Charles Jr, *Law, Marriage, and Society in the Later Middle Ages*, Cambridge : Cambridge University Press, 2007.

Mc Carthy Conor, *Marriage in Medieval England: Law, Literature and Practice*, Woodbridge : The Boydell Press, 2004.

Entéléchie
----------

### Casal de Mahomeriola, fin d'après-midi du mardi 30 juillet 1157

À l'ombre d'un colossal jujubier, un groupe de jeunes filles s'activait à peigner et carder des écheveaux de laine tout en échangeant potins, nouvelles, blagues et chansons. Les tremblements de terre dont on rapportait, au hasard des sources, qu'ils avaient détruit la plupart des villes du Nord ou n'avaient en rien affecté la moindre des cités, n'avaient que rapidement occupé leurs discussions. Elles étaient bien plus intéressées par les frasques de quelques séducteurs locaux ou le passage des colporteurs proposant colifichets, rubans et boutons ornés. Sans forcément l'être en leur cœur, elles jouaient aux frivoles, s'éprouvant les unes les autres à l'aune de leur audace à enfreindre, soi-disant, les règles de vie qu'on leur imposait.

Quelques jeunes mariées fréquentaient leur cercle, les premiers mois de leur union, mais bien vite elles s'excluaient d'elle-même, abandonnant cette période d'insouciance pour la dure réalité de la vie domestique. Avoir « pot et feu » à soi obligeait à un dur labeur de l'aube au crépuscule. Elles avaient néanmoins le temps de venir alimenter d'anecdotes fort peu chastes leurs premières semaines à partager chaque jour la couche d'un homme.

Certaines, comme Marguerite, traînaient une réputation sulfureuse, qu'elles entretenaient par leurs confidences, mi-réelles, mi-fantasmées, et par leur impudence dans le récit qu'elles en tissaient. Libourc, bien qu'effacée, appréciait ces moments de licence où elle pouvait enfin s'affranchir de la tutelle parentale. Même si elle était finalement parfois un peu choquée par ce qu'elle entendait, elle se risquait de temps à autre à une répartie qui lui faisait flamber les joues.

Marguerite ne se cachait pas de ses nombreuses entorses à la conduite qu'on attendait d'une jeune femme et, malgré ses seize printemps, elle revendiquait une expérience qui affolait autant qu'elle piquait la curiosité. Benjamine d'une longue fratrie, elle savait sa dot fort maigre et semblait compter sur d'autres appâts pour ferrer le mari qui lui donnerait des enfants et son consentement devant l'église, de préférence en ordre inverse.

Après une période de calme relatif, quand Mahaut était passée déposer de nouvelles toisons de laine à sa fille, Marguerite se tourna vers Libourc, l'air malicieux.

« Ma jolie, tu ne nous en a guère conté sur le beau colosse qui s'en vient te voir fréquentes fois à la messe ?

--- Qu'aurais-je à en dire ?

--- Eh bien, le plus petit détail nous siérait fort ! Du moins, j'ose encroire qu'il n'est pas si menu... »

Sa saillie déclencha l'hilarité du groupe, y compris de Libourc dont les joues s'ornèrent d'un fard gêné.

« Que vas-tu là penser ? C'est juste bon ami, qui s'en vient nous visiter, mes parents et moi.

--- Allez, ne te fais pas prier, compaigne ! Ce sont détails de ses visites qu'il te fait dont j'ai appétance. Si bel ouvrier doit avoir outil adroit ! N'arrivez-vous jamais à échapper à la veille de la Mahaut ? S'il ne te plait pas, j'en connais qui pourraient en croquer ! Et quand je dis croquer...

--- Eh là, tout doux, ce n'est pas bestiau qu'on achète à la criée, s'anima Libourc. Il est...

--- Ah, la voilà goupil qui défend son poulet ! Donne-nous donc des détails ! Le mât est-il proportionné à la nef ? Ce serait misère qu'il n'y ait pas bon carillon dans les braies de si beau beffroi ! »

Encore une fois, les rires ne laissèrent que peu de possibilités à Libouc de répondre. Elle-même oscillait entre amusement et agacement. Elle savait Marguerite sans malice, mais était mal à l'aise d'être ainsi exposée. Surtout que, si elle trouvait en effet Ernaut bien à son goût, c'était essentiellement parce qu'il lui témoignait un grand respect et une tendresse qu'on n'aurait pu soupçonner sous des dehors d'ours parfois mal dégrossi. Et si elle y avait pensé à l'occasion, jamais ils n'avaient eu l'occasion d'avoir le moindre rapport intime. Rien qu'à l'idée, son cœur s'affolait, de frisson autant que de crainte.

Elle savait de façon très théorique comment cela se passait, mais ne faisait pas bien le lien entre ce qui devait se dérouler pour elle et ce qu'elle pouvait en voir parmi les bêtes chaque jour autour d'elle. Sans être vraiment effrayée des sermons qu'elle avait pu écouter de la part des prédicateurs ou des sous-entendus maternels, elle conservait une appréhension pour le moment où les choses se dérouleraient en pratique. Pareils sujets lui semblaient tellement difficiles à évoquer qu'elle en savait gré, finalement, à Marguerite, de les rendre moins angoissants, en les faisant objets de plaisanterie. La question lui paraissait délicate à aborder en termes clairs et précis avec quiconque en dehors de ce petit cercle d'amies.

### Casal de Mahomeriola, église paroissiale, matin du lundi 8 décembre 1158

Libourc allait et venait autour de l'église depuis le matin, après avoir prétexté quelques tâches à l'extérieur. Sa corbeille désormais vide sous le bras, elle hésitait à franchir le porche. De plus en plus inquiète à chaque pas, elle était angoissée qu'on la voie justement ainsi atermoyer. Elle allait s'en retourner quand la voix douce du prêtre, le père Giriaume, l'arrêta.

« As-tu quelque ouvrage à faire en l'église, ma fille ? »

Le clerc était un petit homme à la tonsure mal peignée, dont l'épaisse barbe brune ne s'ornait que de quelques dents marron quand il souriait. Quoique vêtu de laine de qualité, il n'était guère soigneux et on moquait volontiers les effluves corsés qui s'attachaient à lui. En place depuis peu, il ne faisait guère l'unanimité parmi ses ouailles en raison de son caractère changeant. Un jour il prônait la séparation des sexes pour la messe et, le lendemain, il bénissait sans honte une union fortement suspectée d'adultère. On disait d'ailleurs que cela expliquait qu'un homme si lettré ait échoué ici au lieu de finir opulent chanoine prébendé, comme nombre de ses frères du Saint-Sépulcre. Il s'était néanmoins toujours montré aimable avec Libourc, ainsi qu'il avait l'habitude de l'être avec les plus jeunes.

« C'est à dire que je... »

La honte de ce qu'elle souhaitait évoquer mit ses joues en feu et elle n'osa pas aller plus loin. Tandis qu'elle demeurait là à chercher ses mots, le père Giriaume repoussa la porte de l'église et l'invita à entrer.

« M'est avis qu'il serait mieux d'entendre cela en confession. Va donc réciter quelques *Ave* le temps pour moi de me préparer. Nous irons la chapelle nord pour cela. »

Incapable de se rétracter, Libourc hocha la tête et alla attendre, agenouillée en position d'orante, que le prêtre puisse l'entendre. Lorsqu'il revint, son air n'était plus guère aimable, mais empreint de sérieux et de solennité, si ce n'était qu'il avait posé son étole à l'envers. La jeune fille n'eut pas le courage de le lui faire remarquer. Elle récita les quelques répons puis demeura muette un moment avant d'oser se lancer à avouer ses péchés.

« Je suis céans ce jour car on m'a dit que l'église était fermée aux âmes impures...

--- De certes, de certes. Mais encore faut-il dénicher quel mal est en ton âme, afin de l'en bien purger par adéquates pénitences.

--- Eh bien, il y a ce garçon, que vous avez souventes fois vu avec moi à la messe.

--- Ce jeune Sanson ? Qui ne l'aurait vu ?

--- Vous savez que nous sommes promis l'un à l'autre ? Nous allons nous unir... »

Le bruit de gorge du prêtre arrêta Libourc dans sa difficile confession. Elle se demanda si c'était un ricanement ou juste une toux contenue. De la main, il l'invita à continuer.

« Il se trouve que voilà quelques semaines, mes parents ont dû quitter la maison, me laissant seule une partie de la journée. Et Ernaut est arrivé alors qu'on ne l'attendait pas... Et nous avons... commis le péché de chair ensemble. Tout est allé si vite... Je n'ai pas d'excuse...

--- C'est là en effet bien terrible outrage à la loi divine, qui veut que seuls l'époux et l'épouse s'accouplent. Et toujours dans la continence. Mais tu me dis que vous êtes promis l'un à l'autre ? Avez-vous fait solennelle promesse, de votre plein gré ? Y avait-il des témoins de cela ?

--- Oui, mes parents attendent juste qu'Ernaut ait suffisamment de bien pour correctement nous établir. Ce n'est qu'alors que nous pourrons faire bénir notre union. »

Le clerc fit la moue, réfléchit un instant.

« Et comment vous êtes-vous accouplé ? Selon la nature ou d'une façon contraire à celle-ci ? »

Libourc était écarlate de confusion à l'idée de conter par le menu tout ce qui s'était passé, mais le regard attentif du prêtre ne lui laissait guère d'échappatoire.

« Nous nous sommes baisés sur la bouche, puis avons commencé à nous dévêtir.

--- Oui, de cela je me doute, mais après ?

--- ...Il s'est couché sur moi et nous nous sommes ensuite... agité, lui en moi.

--- Je vois. Tu étais sur le dos et vous étiez ventre contre ventre ? C'est bien cela ? »

Libourc opina en silence. Le prêtre semblait réfléchir, lançant de rapides coups d'œil à la jeune fille, sans laisse présager de ce qui se passait dans sa tête. Elle appréhendait le pire, en particulier une pénitence qui la stigmatiserait aux yeux de la congrégation. Elle craignait que sa famille ne supporte pas un tel déshonneur et battait sa coulpe de s'être ainsi abandonnée à son désir.

« Il me faut savoir si tu as eu quelque plaisir... intense. Ou rien de la sorte ? »

Surprise par la question, Libourc bafouilla et répondit d'une petite voix.

« Je ne pourrais dire. Il y a eu d'abord quelque douleur...

--- Était-ce pour toi la prime fois ?

--- Oh oui ! Jamais je n'avais... »

Le prêtre se laissa à un sourire fugace et gratta sa barbe.

« Tu as bien fait de venir me voir, ma fille. Tu y trouveras la paix de l'esprit, à défaut de celle de l'âme. Sois rassurée, il n'y a là nul péché de chair. Mais te voilà mariée ! »

La poitrine prise en étau par ce maelstrom émotionnel, Libourc écarquilla les yeux, la bouche ouverte, incapable de respirer. Voyant son désarroi, le prêtre s'expliqua.

« C'est très simple : ce jeune homme et toi, vous avez échangé vos promesses au vu et au su de témoins et vous avez par la suite consommé cette union déclarée. Vous voilà donc liés par le mariage. Et comme vous vous êtes accouplés ainsi qu'il sied, de façon naturelle et sans abandon aux turpitudes de la chair qui engendrent le plaisir impie, il n'y a là que le devoir des époux l'un envers l'autre. Votre union est éternellement scellée, rien de plus. *Matrimonium consummatun est*[^17]. »

Libourc se frotta le visage, incapable de réagir à une telle annonce. Sans lui laisser le temps de se reprendre, le père Giriaume enchaîna.

« Tant que tu es là, il y a sûrement d'autres péchés qu'il serait bon que tu me narres... »

### Casal de Mahomeriola, demeure de Sanson de Brie, matin du jeudi 24 septembre 1159

Sanson de Brie avait l'habitude de faire un tri des graines qu'il avait mises de côté pour leur alimentation hivernale juste avant que les frimas n'arrivent, vers la Saint-Michel. Malgré la plus faible humidité en Judée qu'en Brie, il avait toujours la crainte que leurs provisions moisissent et qu'il faille entamer les réserves de semences de l'année à venir. Il n'avait donc jamais laissé cette tâche domestique à Mahaut, qu'il aurait de toute façon harcelée si elle l'avait faite par elle-même.

Les jarres pour l'accès régulier étaient enterrées au fond de la réserve close, couvertes d'un épais rabat de bois étanché de tresses et on se servait d'une barre marquée pour contrôler le volume de graines restant. On y conservait froment et seigle, ainsi qu'un peu d'avoine. Ce dernier était surtout utilisé pour pousser les bêtes qui travaillaient, mais Sanson avait toujours apprécié cette céréale de luxe, avec laquelle on faisait galette et bouillies légères. Libourc l'accompagnait, profitant de l'ouverture du cellier pour remplir sacs et récipients qui seraient portés au moulin pour les semaines à venir.

Il fallait se mouvoir avec précaution au milieu des étagères, des pots et des céramiques entreposées et la jeune fille se rendit rapidement compte que quelque chose n'allait pas avec son père. Il avait la tête ailleurs. Lui d'habitude si serein et assuré avait manqué de renverser plusieurs contenants et s'était cogné aux montants des meubles. Il grognait sans cesse et prenait un temps fou à puiser dans les grains qu'il versait dans les sacs que tenait Libourc. Elle savait que ce n'était guère le moment de l'interroger quand il était dans une humeur aussi exécrable, mais elle craignait qu'il n'ait quelque grief à son encontre et s'en émut auprès de lui, d'une voix fluette.

« Nenni, ma douce, je n'ai nulle colère envers toi. C'est juste que je me fais gâteux et que cela m'agace.

--- Est-ce fardeau que je pourrais vous aider à porter ? »

Le vieil homme soupira douloureusement, s'assit sur un escabeau branlant et prit un long temps avant de répondre.

« Ne va pas dire cela à ta mère, elle en mourrait ! Mais je crains que ma vue décline ces temps-ci.

--- C'est bien normal avec les ans...

--- Je ne te parle pas de ça. Tu n'as guère connu ma tante Emeline, ma marraine.

--- Je ne me souviens que d'une vieille nonne que nous visitions à l'occasion.

--- C'est cela. Si elle a rejoint les sœurs, c'est parce qu'elle a vécu ses dernières années dans le noir. Je crains d'avoir le même mal qu'elle...

--- Vous voulez dire que vous allez devenir...

--- Aveugle. Si fait. Je n'arrive déjà plus à reconnaître le seigle du froment si ce n'est au toucher. Et sans forte lumière, je ne discerne que des formes vagues. »

Libourc ne put s'empêcher de prendre la main de son père et de la serrer dans les siennes.

« Les valets ne sont bons à rien si on ne les houspille pas en permanence et ils auront beau jeu à en faire à leur tête, avec un maître aveugle. Je ne peux demander à ta mère de s'occuper des terres en plus de la maison. Elle en fait déjà tant...

--- Je suis là, père. Vous pouvez compter sur moi.

--- Que non pas, ma fille. Tu as ta vie avec le jeune Ernaut et elle te mènera loin d'ici, dans la ville. Nous ne pouvons nous dédire maintenant.

--- Ernaut a bon cœur, père. Il ne saurait souffrir de vous voir esseulé et en difficulté, j'en suis certaine. Le moment venu, vous pourrez vous installer avec nous, avec des valets pour s'occuper de vous. »

Sanson sourit faiblement, attendri par la spontanéité de sa fille. Mais il n'aimait guère l'idée de finir vieillard grabataire dans la maison d'un autre. Il lui caressa la main doucement.

« De cela j'aviserai si besoin s'en fait sentir. Mais, surtout, ne va pas en discuter avec qui que ce soit ! Ta mère a le cœur fragile et s'enfrissonne d'un rien. Je vous dirai ma décision quand je l'aurai prise. »

### Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159

Occupée à découper un petit pâté à la viande, Libourc laissait ses pensées vagabonder. Elle appréciait ce moment simple, en famille, avec ses parents et Ernaut. Elle déplorait l'absence de ses frères et sœurs demeurés en Brie, qui devaient désormais être envahis d'enfants. Avec les fêtes, les valets et servantes étaient chez eux, et ne restaient que les personnes dont elle se sentait le plus proche. Elle regrettait aussi que Lambert et Osanne n'aient pu se joindre à eux. Elle ne les connaissait guère, mais souhaitait renforcer les liens qui les unissaient. Elle avait l'impression que son frère aîné exerçait une bonne influence sur Ernaut. Il était parfois sentencieux, mais aussi posé et prudent, ce qui pondérait l'enthousiasme de temps à autre irréfléchi d'Ernaut.

Elle n'avait pas abordé avec son fiancé le sujet de la santé de son père, consciente qu'elle risquait de contrarier Sanson en le faisant sans son accord. Elle se sentait néanmoins tiraillée par son devoir entre les deux hommes. Ce que le prêtre lui avait dit l'avait bouleversée et quand elle en avait parlé à Ernaut, il avait semblé ennuyé lui aussi. Depuis lors, il s'était mis en tête de leur permettre de s'établir au plus vite, redoublant d'efforts en cela. Libourc avait bien senti que leur relation avait changé et qu'ils se comportaient l'un envers l'autre déjà comme des époux. Elle s'estimait donc un peu en faute de ne pas lui avouer ce qu'elle avait sur le cœur et en projet.

Elle lui lança un petit sourire, en le voyant assis au coin du feu, certainement occupé à échafauder des plans pour être promu dans l'hôtel du roi. Elle était très fière de tout ce qu'il faisait et hésitait parfois à l'en féliciter. Elle craignait qu'il n'en devienne fanfaron et veillait donc à ne pas lui ouvrir ainsi la voie au péché d'orgueil. Elle serait plus à l'aise pour aborder tous ces sujets avec lui quand ils partageraient un logement et leur couche. Jusqu'à présent, leurs moments d'intimité étaient bien trop rares et précieux pour qu'elle les gaspille. Étrangement, après les assurances du prêtre, ils n'avaient plus osé aller au-delà de quelques baisers, redoutant peut-être de rompre le délicat équilibre actuel. Libourc était également soulagée de n'avoir pas à gérer une possible grossesse prématurée. Ils étaient néanmoins tous deux impatients que les choses avancent.

La table prête, Sanson mit un moment à venir s'installer. Il arborait son visage des bons jours, légèrement espiègle et content de lui. Lorsqu'après le bénédicité, il prit la parole doucement, elle n'en fut qu'à demi étonnée. Enfin il donnait son accord définitif pour leur union ! Le cœur de Libourc se mit à battre la chamade, surtout quand elle vit Ernaut sourire de bonheur, incapable de répondre. Sanson paraissait fier de son effet et enchaîna, tout joyeux.

« Il me semble que passer Carême nous laissera le temps de provisionner pour offrir beau banquet à compères et commères. Qu'en dites-vous ? Les Pâques ne sont que dans trois mois, ce pourrait être peu après. » ❧

### Notes

À l'époque d'Ernaut, la femme européenne est considérée comme une mineure de façon quasi naturelle, que ce soit au niveau religieux ou d'un point de vue social, économique et politique. En gros, elle passe de la sujétion à son père à celle à son mari et ne peut vraiment devenir indépendante, complètement majeure qu'une fois veuve. Sa voie est donc toute tracée dès sa naissance et en dehors de l'intégration à un couvent, impossible à éviter. Une structure sociale traditionaliste faisait que cela n'empêchait pas certaines de parfaitement s'épanouir dans un système si oppressif, en adhérant dès l'enfance à un fonctionnement dont rien n'indiquait qu'il pouvait en exister un autre.

Il devait se rencontrer des réfractaires à ce schéma : on pense tout de suite à des femmes d'influence comme Mélisende de Jérusalem ou, bien sûr, Aliénor d'Aquitaine. Mais on a aussi quelques traces infimes de leur existence parmi les couches les plus populaires. Je fais là référence à Guillelme Maury, de Montaillou, dont Anne Brenon nous narre le roman vrai dans son magnifique récit « L'impénitente ». Je n'ai néanmoins pas choisi de me baser sur elle pour Libourc, que je voulais plus semblable à ce que je perçois et crois comprendre de beaucoup de femmes de cette époque, pour peu qu'on puisse définir sans restreindre et appauvrir ou se projeter sans trahir exagérément.

En ce qui concerne la confession de Libourc, c'est l'occasion d'évoquer les traditions légales en droit canon qui s'affrontaient alors autour de l'importance du consentement, de sa temporalité et de la question des relations sexuelles. Les pénitentiaires qui nous sont parvenus sont très complets et semblent indiquer que les ecclésiastiques exigeaient les détails que Libourc peine à dévoiler. Les glossateurs sur les péchés avaient ainsi défini des catégories précises, selon les moindres faits et gestes, y compris dans les rapports intimes, et y appliquaient des pénitences en fonction de leur cadre d'analyse de la société. Avec parfois un décalage avec ce qu'on pourrait imaginer, en laïc du XXIe siècle. Le Qit'a « Maître de chapelle », à venir, reviendra sur cette question.

### Références

Brénon Anne, *L'impéninente. le roman vrai de Guillelme maury, de Montaillou*, Cahors : La Louve éditions, 2002.

Brundage James A., *Law, Sex, and Christian Society in Medieval Europe*, Chicago : The University of Chicago Press, 1987.

d'Avray David, *Medieval marriage - Symbolism and Society*, Oxford : Oxford University Press, 2005.

Donahue, Charles Jr, *Law, Marriage, and Society in the Later Middle Ages*, Cambridge : Cambridge University Press, 2007.

Schaus Margaret (dir.), *Women and Gender in Medieval Europe - An Encyclopedia*, New York : Routledge, 2006.

Shahar Shulamith, *The Fourth Estate - A history of women in the Middle Ages*, nouvelle édition, New York : Routledge, 2003.

Passage
-------

### Casal de Mahomeriola, fin d'après-midi du mercredi 29 janvier 1158

Un ciel chargé de houle noire avait chassé le jour encore plus tôt qu'habituellement. Des maisons claquemurées s'élevaient avec difficulté quelques fumerolles stagnantes, alourdissant l'atmosphère. Parti au matin de la Grande Mahomerie sur une monture de location, Lambert arrivait enfin à Mahomeriola alors que des attardés rentraient les poules ou enclosaient leurs bêtes pour la nuit. En ce crépusucule annonciateur de pluie froide, même les chiens errants avaient trouvé un abri. À son grand réconfort, Sanson lui fit bon accueil et l'invita à se reposer tandis que Mahaut et Libourc préparaient le potage du soir. Près du feu, tout en dégustant un vin léger coupé d'eau, ils discutèrent semailles et récoltes, jointure et cheptel, comparant les terroirs de leurs deux manses. Ils avaient fait connaissance lors de leur séjour à l'hôpital de Saint-Jean[^18], mais se découvraient vraiment pour la première fois.

Ce ne fut qu'une fois le repas avalé que Sanson invita son épouse et sa fille à se retirer derrière les courtines du lit pour que les deux hommes puissent aborder ce qui amenait Lambert en ces lieux. Car il n'était pas là pour une simple visite de courtoisie. Empreint de sérieux, Sanson leur servit de la petite bière et posa un bol d'amandes entre eux sur la table.

« Alors, Lambert, que peux-tu me dire des intentions de ton frère ?

--- Elles sont limpides : il m'a mandé afin de voir quelles seraient vos prétentions pour son mariage avec Libourc. Il veut que cela soit fait ainsi qu'il sied. »

Sanson fit une grimace. Le mot avait été lâché et l'entendre ainsi prononcé lui meurtrissait le cœur. Il s'y était préparé, le jeune Ernaut l'avait déjà dit précédemment. Mais cette fois, les choses devenaient concrètes, officielles, inévitables. Il se passa une main fatiguée sur le visage, se recomposant un aspect plus engageant. Il aimait bien ce qu'il voyait de Lambert jusqu'à présent. Et il avait de la sympathie pour Ernaut. Sans pour autant que cela soit suffisant pour qu'il lui concède sa fille sans prendre des garanties.

« Avant tout, je dois te dire que je ne pensais pas m'établir ici et Libourc n'a nul désir de s'en retourner vers les miens, à Meaux. Il me faut donc m'assurer qu'elle aura bon parti pour ne pas se trouver seulette et dépourvue.

--- C'est ce que j'ai expliqué à mon frère, et il l'a fort bien entendu. Il est prêt à lui accorder douaire de bon aloi. Je me porte garant de ce qu'il promettra. Elle sera de ma famille et pourra compter sur mon aide et ma protection. À Vézelay, notre parole est écoutée et entendue : le père a une voix d'importance au parmi des doyens, y compris près le sire abbé. »

Sanson hocha la tête. Tout devenait compliqué quand on était loin des siens. Comme Lambert, il était un nouvel arrivant dans son casal et nul n'avait tissé de liens assez forts avec lui pour lui offrir la sécurité d'une communauté ancienne et éprouvée comme en Brie. Il avait fait écrire une lettre à des amis pour se renseigner sur la réputation de la famille d'Ernaut, là-bas, en Bourgogne, mais nul n'était jamais sûr de recevoir une réponse.

Prenant son silence pour une invitation à développer, Lambert enchaîna.

« Outre, j'ai projet de faire bon mariage moi aussi, d'ici peu. Une voisine mienne, Osanne fille Godefroy.

--- Deux mariages en si peu de temps ?

--- Père nous a bien garni, nous avions désir de nous établir ici. Ernaut m'a laissé usage de sa part pour le moment, mais je lui paierais son dû avec la récolte. Cela s'ajoutera outre sa solde de sergent.

--- De cela aussi j'ai quelque crainte. Il n'est pas maître de son bien et, surtout, s'expose à grands dangers chaque jour, même s'il sert prestigieux seigneur. »

Lambert retint de justesse un sourire ironique. Ernaut avait dû dresser un tableau héroïque de ses activités dans l'hôtel royal. Il n'était pourtant que simple sergent affecté à des tâches administratives et du labeur routinier. Il n'encourait guère plus de risques qu'un valet de ferme aux prises avec un troupeau de vaches. Jusqu'à présent il avait toujours été tenu en dehors des opérations militaires, même s'il bouillait d'impatience d'y prendre part. Lambert faisait tout son possible pour l'en dissuader, car il n'était pas bon selon lui que le vigneron ou le laboureur se fasse guerrier. Chacun devait demeurer à sa place.

« Il ne s'est pas juré définitivement au roi, mais renouvelle chaque année son serment. Il envisage aussi de faire quelque négoce avec des amis à lui. Il a désir de s'établir en ville, ce qui offre solides garanties de sauveté.

--- Et comment donc ? Il n'a pas de métier ! Même si je sais qu'ici il est de bon usage de jouer avec la monnaie comme si c'en était un.

--- De nombreux offices sont proposés à ceux qui servent bien. Ernaut a déjà soudée suffisante pour nourrir les siens.

--- Oui, cela j'entends bien, mais *amour de seigneur n'est pas fief !* »

À voir la mine renfrognée de Lambert, Sanson comprit qu'il était allé trop loin dans ses récriminations. Il avait parlé un peu trop librement, dévoilant ses sentiments intimes au lieu d'avancer des arguments de négociation. Il ne souhaitait néanmoins pas laisser croire à un refus. Il ne serait pleinement rassuré que lorsque Libourc aurait un époux pour veiller sur elle, et Ernaut semblait un bon parti. Il tenta un sourire plus engageant.

« Foin de tout ceci, parlons clair. Tu me disais qu'Ernaut allait mieux s'établir ? Est-ce logement pour fonder son foyer ?

--- Non pas, juste deux pièces près le centre de la Cité. De là, il voudrait chercher meilleure demeure pour les siens et l'acheter. Avec ses compaings en la sergenterie, il ne devrait guère avoir de peine à dénicher bel endroit. Mais en attendant, il épargne un cens inutile. »

Sanson approuva, se demandant si cette attitude réfléchie était bien du fait d'Ernaut. S'il le trouvait très sympathique et appréciait les égards dont il entourait Libourc, il lui semblait voir là plus des traits de Lambert que du jeune homme. Quoiqu'il en soit, c'était néanmoins un signe rassurant qu'il suive les préceptes d'un aîné pétri de bon sens. Sanson lança un regard aux mains de Lambert : tavelées, crevassées, emplies de corne, celles d'un paysan dur à la tâche. Cela lui plaisait. Il ne faudrait plus longtemps avant qu'ils ne parviennent à un accord formel.

### Casal de Mahomeriola, demeure de Pariset, veillée du dimanche 7 septembre 1158

Le manse de Pariset était parmi les plus vastes et les plus prospères du casal. Établi autour d'une cour enclose de murs maçonnés, il rivalisait de dépendances et d'annexes avec le manoir du Saint-Sépulcre. Installée là depuis les origines, sa famille se vantait d'avoir juré serment à Godefroy de Bouillon lui-même avant qu'il ne fasse don du lieu au Saint-Sépulcre. Chaque année pour les Rameaux, leur doyen faisait bénir une branche de buis qu'ils prétendaient venir de cet hommage illustre, brindille rabougrie et jaunie qui trônait derrière un crucifix dans leur grande salle le reste de l'année.

C'était désormais Pariset, vieil homme malingre et maladif, qui s'asseyait au haut bout de la table. Père d'une considérable fratrie, il avait abandonné la gestion quotidienne de l'exploitation à son aîné pour se consacrer à l'administration du bourg. Son élégance et sa beauté, conservées au fil des ans, attiraient le regard et entretenaient les espoirs de nombreuses femmes, sans aucun succès jusqu'à présent. Veuf sans aucun attrait pour le sexe ou la religion, il avait un tempérament calme et méthodique qui faisait merveille pour concilier des voisins fâchés par des querelles de bornage ou d'animal errant.

Au sein du conseil siégeait aussi Géraud, dit *Le roi*. Imposant octogénaire, sourd à en rendre jalouses bien des pierres, il répondait d'une voix grasseyante tonitruante quand on lui posait une question, mais demeurait coi la plupart du temps, perdu dans ses rêveries et son monde de silence. Arrivé là au temps du premier roi Baudoin[^19], il était la mémoire vivante du casal, ayant gardé en ses méninges le souvenir des bornages, péages, coutumes et loyers de la plus infime parcelle de terre. Il était aussi un conteur enjoué, narrant avec brio, et pas mal d'humour, le demi-siècle auquel il avait assisté dans les lieux. On lui savait gré de ne jamais se risquer à dévoiler de faits trop fâcheux ou d'histoire trop grivoise, sauf en compagnie choisie.

Le troisième homme était Plat-Pié le Carrier. Cet ancien maçon au dos voûté et aux gigantesques mains percluses d'arthrose s'était mis en tête d'aider et de superviser le moindre aménagement dans le périmètre du casal. Apportant plus que volontiers son savoir-faire bénévolement, il s'attirait régulièrement les foudres de son épouse qui l'accusait de négliger son bien jusque dans l'église durant les offices. Il ne semblait guère en tenir compte et parcourait les lieux assis sur son vieil âne d'un air bonasse, accordant à chacun autant de temps qu'il l'estimait nécessaire. Cet empressement à aider autrui se doublait d'une curiosité maladive, qui lui donnait accès à bien des informations utiles pour ensuite délibérer au sein du conseil des jurés.

Sanson était venu les voir afin de discuter avec eux des habitudes de pacage des bêtes dans les communs. Il était habituel pour les habitants du casal de laisser déambuler leurs troupeaux sur des terres libres, dont la collectivité avait la charge. En tant que nouvel arrivant, Sanson souhaitait découvrir ce à quoi il avait droit, par coutume, et les devoirs qui s'y rattachaient. Il s'était par ailleurs résolu à demander conseil pour le mariage de Libourc, sujet qu'il aborda une fois la question des animaux réglée.

« Si vous le permettez, j'aurais également une demande fort personnelle, vous sachant hommes de ces lieux. Nous avons convenu de marier notre fille Libourc à un jeune sergent le roi, en la Cité. Tout se fait selon les règles et je n'ai nulle raison de m'opposer à cette union. Mais mon épouse Mahaut est fort marrie de le voir porteur de l'épée. Je me disais que vous auriez peut-être possibilité d'apprendre de quoi la rassurer ? »

Pariset inclina son étroit visage avant de prendre la parole.

« Déjà, il me semble que servir le roi est bon gage. Il est au parmi de l'hostel depuis long temps ?

--- Certes pas. Fraîchement débarqué avec son aîné Lambert, désormais établi à la Grande Mahomerie, il vient de Bourgogne. »

Le Carrier risqua un regard à son collègue avant de s'exprimer à son tour.

« Nous pouvons poser quelques questions au frère Géraud, le préchantre du Saint-Sépulcre. Nous avons eu maintes fois affaire à lui, avec grand bonheur. Il me serait étonnant qu'il n'ait pas moyen d'en savoir plus sur ce jeune homme, s'il ne le connaît pas déjà. Comment se nomme-t-il ?

--- Ernaut. C'est un géant, bâti à chaux et sable, le verbe haut et la poigne ferme. Pourtant gentil garçon, de ce que j'ai pu en juger.

--- Je pourrai aussi entreparler avec mestre Sicherius. Juré le roi en la Cour des Bourgeois de la cité, je le tiens pour bon ami. Il aura sûrement quelques savoirs propres à rassurer les vôtres, mestre Sanson. »

Sanson acquiesça. Sous couvert des inquiétudes de Mahaut, qu'elle avait clairement formulées, il n'estimait pas inutile de découvrir ce qu'on disait d'Ernaut autour de lui. Il n'avait guère pu en apprendre auprès de son curé de paroisse, vu que le jeune homme n'était pas là depuis très longtemps. Et Sanson ne connaissait personne dans son entourage qui puisse lui montrer une autre image que celle qu'il percevait de son probable futur beau-fils. L'excellente impression qu'il avait de Lambert, et donc de l'éducation qu'avait reçue Ernaut, ne pouvait suffire à rassurer un vieux père quant au destin de sa benjamine.

### Casal de Mahomeriola, demeure de Sanson de Brie, fin d'après-midi du mardi 28 juillet 1159

Sanson aimait à se rafraîchir après un long labeur. Il venait d'envoyer son valet Huet récupérer au pâturage les vaches pour les traire et s'accordait ce bref moment pour souffler. Torse nu, en braies, il se frottait de linges qu'il trempait dans un cuvier laissé à chauffer dehors toute la journée. Le soleil avait noirci sa peau, traçant sur son front dégarni une marque avec son crâne blanc, préservé sous son bonnet et son chapeau.

Lorsqu'il se pencha pour se rincer le visage dans l'eau claire, il grimaça. Il n'aimait guère s'y confronter avec ce qu'il était devenu avec les ans. Il gardait en tête les traits de sa jeunesse et ne faisait que difficilement le lien avec ce que l'impitoyable reflet lui proposait. Il détailla ses bras maigrelets, son ventre avachi... puis inspira longuement. La vie avait filé si vite ! Quand il regardait Mahaut, il la voyait toujours comme la belle femme qu'elle était au jour de leur mariage, plaquant sur son austère faciès ridé les minauderies de la jouvencelle d'alors. Cela le fit sourire, évoqua en lui le visage de sa petite Libourc.

Il chercha autour du cuvier le pot où était posé le savon et, ne le trouvant pas, se mit à pester contre Huet qui ne rangeait jamais rien. Il se leva, agacé, et alla dans la réserve. En entrant, il se cogna contre un tabouret appliqué là pour maintenir la porte grande ouverte. Il n'en ronchonna qu'avec plus de vigueur, se frottant le tibia. Avant de se prendre les pieds dans un amas de ficelle. Cette fois il ne pouvait s'en plaindre qu'à lui-même, qui l'avait jeté là. Évitant de peu de se retrouver au sol, il se résolut à laisser ses pupilles accommoder un moment, sans grand succès.

Cela faisait maintenant plusieurs mois qu'il avait remarqué qu'il y voyait de moins en moins bien, surtout quand la lumière se faisait rare. Sa main était également plus hésitante, et il montrait une maladresse inhabituelle dans ses gestes, ce que ne manquait pas de relever Mahaut. Il mettait à chaque fois cela sur le compte de la fatigue, mais il commençait à accepter le fait que c'était un problème d'yeux. Il n'était pas le premier de sa famille à qui ça arrivait. Rien n'était aussi net que dans son souvenir et il avait beau se frotter le visage, écarquiller les paupières, loucher... rien n'y faisait, il n'y voyait qu'à travers un voile de plus en plus épais.

Il finit par retrouver le petit pot de savon et retourna à la cuve, l'air renfrogné. Mahaut, qui revenait du jardin avec des légumes dans un panier, en fut frappée et s'inquiéta de ce qui le tourmentait.

« Rien d'importance, juste trop longue journée pour mes vieux os. On louera peut-être un autre valet pour la fin des travaux...

--- Dis-tu cela pour quémander meilleure chère au souper ? J'ai prévu un bouillon de viande, mais je peux l'épaissir et y adjoindre une bonne compotée d'oignons frais... »

Devant le sourire complice de son épouse, dont elle n'usait que fort rarement en dehors du cercle intime, il n'eut pas le cœur de maintenir sa mauvaise humeur. Il hocha la tête avec enthousiasme.

« Ma douce, tu es comme la pluie sur une terre desséchée...

--- Écoutez-le, beau damoiseau, le voilà à me conter fleurette ! »

Puis elle disparut avec gaieté dans l'escalier, chantonnant pour elle-même. Sanson se redressa, plissa les yeux. Il fallait vraiment qu'il se dépêche de marier sa fille avec un bon parti, en cas de malheur. Ernaut avait récemment prouvé à quel point il était prêt à s'investir pour défendre les siens[^20] et cela confortait Sanson dans son acceptation de son union avec Libourc. Mais il était également conscient que Mahaut pouvait se montrer aussi épineuse qu'une rose. Et elle ne portait guère le jeune garçon dans son cœur, Dieu seul savait pourquoi.

### Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159

Sanson avait battu en retraite vers le sous-sol, chassé par sa femme durant la préparation du repas. D'humeur particulièrement taquine, il avait fini par s'attirer des reproches de ses agaceries. Il était resté un temps assis à table, inquiet de provoquer une catastrophe à cause de sa maladresse, avec le peu de lumière. Puis finalement, muni d'une lampe, il était descendu. Il avançait précautionneusement jusqu'à la huche qu'il avait confectionnée pour Libourc. Il y enfermait tous les objets de prix, tissus et vêtements essentiellement, dont il était convenu qu'il les apporterait en dot pour le mariage. Mais il s'y trouvait un autre élément auquel il était tout particulièrement attaché.

Il déverrouilla le coffre avec la clef qui ne le quittait jamais et prit sur le paquet d'étoffes le petit cheval à roulettes qu'il avait sculpté sans le dire à personne. C'était, autant qu'il pouvait s'en souvenir, une copie de celui que ses enfants avaient poussé dans la cour de l'échoppe à Meaux. Libourc le reconnaîtrait sans aucun doute. C'était une façon pour lui de leur souhaiter une famille nombreuse, tout en créant un peu de lien avec ceux qui étaient demeurés en Champagne.

Il sourit en caressant la crinière qu'il avait tracée de son couteau, vérifia le bon fonctionnement des roulettes puis replaça le tout avec soin. Incapable de distinguer les motifs des magnifiques tissus serrés là, parmi les feuilles de menthe, il y laissa flâner la main avant de refermer le lourd couvercle. Il était content de cette huche, avait hésité un temps à faire peindre le devant avec décors locaux, mais la sobriété du meuble lui plaisait davantage.

Il se leva péniblement puis retrouva les escaliers, qu'il monta et se garantissant de la paume le long du mur, jouant de la clef entre ses doigts. Tout le monde l'attendait et il s'installa directement à sa place, heureux de n'avoir rien heurté en chemin. Il récita le bénédicité l'esprit ailleurs puis annonça qu'il était plus que temps selon lui de célébrer ce mariage tant espéré, dès après Pâques. Les deux jeunes gens en furent muets de stupeur et il sourit à la cantonade, satisfait de lui-même.

Il montra la clef et la tendit à Mahaut.

« Cette clef que je remets à ta mère sera la première qui ornera ta ceinture de femme, ma fille. Puisse-t-elle ouvrir la porte vers une vie joyeuse. »

Il se tourna vers Ernaut, le visage plus grave.

« Ce n'est pas en ce coffre que se tient mon plus précieux bien en cette terre, mon garçon. J'ose encroire que tu sais de quoi je parle. Sois bon mari pour elle, c'est tout ce qui importe. »

Inquiet de son ton pontifiant et de l'atmosphère solennelle qu'il venait d'instaurer, il sortit son petit canif et le déplia, l'air soudain exagérément enjoué.

« Mais en attendant, si nous faisions honneur à tous ces bons plats ? » ❧

### Notes

Le rôle des familles dans le mariage médiéval n'était pas forcément aussi prégnant qu'on pourrait le croire de prime abord. L'influence de l'Église, qui insistait sur le consentement des époux comme constituant essentiel de toute union, fut un moteur très important de cette évolution. Il demeurait malgré tout le fait que la création de nouveaux foyers périphériques intervenait dans un réseau social plus ou moins dense, en interaction avec les groupes impliqués. Chaque parent voyait son rôle bouleversé, la relation à son enfant devant être renégocié par le changement de statut que le mariage engendrait. Avec la naissance de la famille parentale au Moyen Âge en occident, cela signifiait aussi une évolution du rapport à la vieillesse, celle-ci ne se déroulant plus dans le cadre d'une famille élargie.

Il m'a donc semblé intéressant de réfléchir à la façon dont on pouvait évoquer cette dialectique entre ce que l'union consacrée entraînait pour le rapport entre l'enfant et le parent en complément de la vision que les fiancés pouvaient en avoir. Cela m'a paru d'autant plus pertinent que le fait d'être colon dans une région éloignée des cercles de relations habituels accentuait certainement les difficultés. Je n'ai malheureusement pas trouvé de travaux universitaires spécifiques sur cette notion de passage dans les âges de la vie pour nourrir plus précisément ce Qit'a au delà de ce que je pouvais extrapoler des recherches collatérales sur le mariage ou sur l'appréhension de la mort.

### Références

Brundage James A., *Law, Sex, and Christian Society in Medieval Europe*, Chicago : The University of Chicago Press, 1987.

d'Avray D. L., *Medieval marriage - Symbolism and Society*, Oxford : Oxford University Press, 2005.

Donahue, Charles Jr, *Law, Marriage, and Society in the Later Middle Ages*, Cambridge : Cambridge University Press, 2007.

Maître de chapelle
------------------

### Lydda, palais de l'évêque, matin du lundi 9 mars 1159

Les yeux à demi ouverts, Gilbert s'étira longuement, emmitouflé sous les couvertures. La lumière du jour entrait à flots dans la petite salle qu'il partageait avec une demi-douzaine d'autres serviteurs de l'évêque. Comme souvent, il attendait pour sortir du lit le claquement de la porte de son dernier compagnon. Il savait pourtant qu'en tardant ainsi, il n'aurait peut-être même pas le temps de grignoter quoi que ce soit avant de se rendre à la messe matinale.

La capacité à se lever à toute heure était la première chose qu'il avait perdue en quittant le monastère clunisien où il avait grandi. À son arrivée à Lydda, il avait malgré tout mis un point d'honneur à ne manquer aucun office et continuait à suivre la stricte discipline qu'il avait toujours connue. Puis il avait vite remarqué que nombre de chanoines étaient souvent absents, surtout pour les heures nocturnes. De plus, étant simple clerc sans charge liturgique, Gilbert ne semblait tenu à aucune exigence sur ce plan. Il en conclut donc qu'il n'avait pas à être plus assidu que ses aînés dont c'était la mission. Il avait ainsi commencé à se relâcher.

Tout en bâillant, il enfila sa longue cotte de laine, ses sandales et se dirigea vers le sanctuaire. De sa voix chantante enrouée de sommeil, il saluait du bout des lèvres tous ceux qu'il croisait. En revenant de l'église, il passa au réfectoire des religieux et demanda pour sa collation du matin un peu de pain et une compotée de fruits divers, avec un fromage sec. Puis il s'installa à table, le regard dans le vague.

Il fixait sans le voir le grand crucifix accroché au bout de la pièce, à côté du lutrin où, certains jours, un des chanoines faisait la lecture. Il aimait à s'imaginer parfois que ce pourrait être lui. C'était là une des fonctions du préchantre, dont on estimait généralement qu'il était le successeur désigné du chantre lorsque celui-ci était rappelé à Dieu. Gilbert raffolait de musique et, par-dessus tout, chantait avec ferveur, de sa voix forte et pure. Oblat formé dès ses plus jeunes années à la complexe liturgie clunisienne, il ne se sentait jamais si bien que quand il laissait ses vocalises s'unir à celles de ses frères, en célébration du Très-Haut. Souvent il fredonnait des hymnes pieux pour lui-même, enchaînant les mélismes avec un ravissement qui se dessinait sur ses traits joufflus.

Lorsqu'il arriva dans sa salle de travail au petit lutrin où il officiait généralement, il inspecta machinalement ses plumes, pots d'encre, réglets et mines de plomb. C'était pour lui un rituel chaque matin, qu'il exécutait avec gravité, tout en saluant ses compagnons. Il avait progressivement pris l'habitude du papier, dont le comportement à l'écriture était différent du vélin. Il avait découvert de nouvelles recettes de couleur, plus adaptées, retenant avec application les dosages. Jaque, le scribe principal de l'évêque était généralement satisfait de son travail, appréciant de voir un élève aussi discipliné, même s'il le trouvait trop souvent perdu dans ses rêveries.

Ce fut d'ailleurs la toux insistante de son maître qui tira Gilbert de ses songes éveillés. Plusieurs rouleaux à la main, Jaque le toisait de ses yeux myopes.

« Sire Constantin te fait mander, Gilbert. »

Puis, voyant que le jeune clerc s'emparait d'une tablette de cire pour prendre des notes, il secoua la tête.

« Non, tu n'as besoin de rien, mais tu dois y aller séance tenante. »

Gilbert acquiesça et se dirigea avec célérité vers la grande salle d'audience. Chemin faisant, il ajusta sa tenue, se recoiffa d'un geste. Il était toujours inquiet de se faire ainsi convoquer, même s'il était rare qu'on l'admonestât pour quoi que ce fût. De toute façon, l'évêque était assez débonnaire avec les gens de son hôtel, et ses exigences étaient bien moins fortes que celles de l'écolâtre qui avait éduqué le jeune novice. La férule qui l'avait dressé tout au long de son enfance se rappelait néanmoins à sa mémoire chaque fois qu'il était mis en présence d'un supérieur d'une telle importance.

Lorsque Gilbert pénétra dans la grande salle, Constantin de Lydda n'était pas sur son trône, mais assis près d'une petite table, sur une chaise curule, occupé à discuter avec un homme aux cheveux gris coupés courts, à l'allure martiale, installé à côté de lui. Ils partageaient une collation de beignets aux fruits dont le parfum vint chatouiller les narines du clerc. Josce, le vervet de l'ecclésiastique, avait pris la place d'honneur sous le dais et s'activait à grignoter des amandes tout en surveillant les alentours. Il ne réagit pas à l'entrée de Gilbert, auquel il était habitué. Le clerc s'approcha sans bruit, puis inclina la tête, attendant respectueusement qu'on s'adresse à lui.

Les deux hommes parlaient de navigation maritime et du passage de certaines nefs espérées d'ici peu. Ils achevèrent rapidement et Constantin indiqua à Gilbert de venir plus près. L'hôte de l'évêque, d'un certain âge, le détailla des pieds à la tête. Il avait le regard perçant, mais dénué de violence et de méchanceté, attentif plus qu'inquisiteur. Et si ses gestes étaient martiaux, il n'avait pas sur le visage la sécheresse et la dureté de traits de certains barons autoritaires. Il arbora même un sourire amical.

« Voici donc notre jeune merle !

--- Comme je vous l'ai dit, mon fils, il a l'usage des chants les plus complexes. Comme tout *nutriti* enfant de Cluny. »

Puis Constantin se tourna vers Gilbert.

« Tu as étudié assez pour les ordres mineurs et majeurs, si j'ai bonne souvenance ?

--- De certes, mon sire. Mais n'ai pas été ordonné...

--- Cela pourrait se faire vite assez. Géraud, que tu encontres ce jour, est seigneur, entre autres de la ville de Sagette, au nord du royaume. Il t'a ouï à vespres hier et désire te prendre à son service. Son chapelain, fort fatigué, a dû revêtir l'habit, et il n'est pas d'usage qu'une chapelle de telle importance demeure vide. Cela serait belle charge pour jeune clerc, qu'en dis-tu ? »

Familier des questions qui n'étaient que des injonctions formulées aimablement, Gilbert hocha la tête sans faire entendre sa voix.

« Je vais rédiger une lettre pour l'évêque Amaury, que tu puisses recevoir les ordres de sa main. D'ici là, tu pourras tout de même célébrer quelques offices, des morts en particulier. Sire Géraud tient à honorer ses ancêtres. Un clunisien tel que toi doit savoir cela parfaitement ! Va prévenir Jaque des tâches que tu avais en cours et sois prêt à partir avec l'hostel du sire Sagette demain dès potron jaquet. »

Tout en signifiant son congé d'une main distraite à Gilbert, l'évêque se tourna vers le seigneur de Sagette et lui sourit aimablement.

« Vous voilà de nouveau garanti pour votre âme, mon fils. Et par bon clunisien dont je ne me sépare qu'à regret... »

Gilbert salua d'une inclinaison du buste et se retira en silence. Puis il dirigea ses pas vers le scriptorium. Il voyait son univers familier se dissoudre autour de lui, ce qui l'angoissait au plus haut point. Il n'était à Lydda que depuis quelques années, mais son tempérament routinier avait vite tracé un sillon dont il ne s'écartait que peu. Devenir chapelain, cela voulait dire vivre sans clôture, dans le monde séculier. Et surtout, sans supérieur direct auquel obéir chaque jour. Rien que d'y penser, il sentait le trouble gagner son cœur, chavirer son estomac. Il lui faudrait visiter les cuisines au plus vite, voir s'il ne se trouvait pas un petit quignon sur lequel passer ses inquiétudes.

### Château de Beaufort, après-midi du dimanche 14 juin 1159

Installé sur un des petits bancs de la chapelle, Gilbert jouait doucement de son psaltérion. La chaude lumière pénétrait par les fines ouvertures, faisant danser la poussière grise soulevée par le balayage récent qu'il avait demandé. L'autel était paré, surmonté d'une jolie fresque un peu naïve représentant un christ en majesté. Il se sentait désormais moins angoissé à l'idée d'être en charge du service divin. En outre, si les célébrations qui allaient commencer au soir étaient d'importance, messe des morts en mémoire d'un ancêtre particulièrement vénéré, Eustache, c'était là un cérémonial que Gilbert connaissait parfaitement. Il regrettait juste que les participants ne fassent qu'ânonner à sa suite, incapables qu'ils étaient d'accompagner ses envolées vocales.

Il aimait bien la petite chapelle du château, profondément enchâssée dans l'énorme massif de pierre. S'il avait appréhendé la montée jusqu'à ce nid d'aigle, il se sentait à l'abri tant qu'il n'approchait pas des murs d'enceinte. Il s'était installé une couche dans le fond de la salle et avait son coffre personnel, qui lui servait souvent de table. Il avait trouvé étrange de disposer ainsi de biens propres en si grande quantité. De plus, il percevait une dotation qui lui permettrait de s'acheter des instruments de musique de qualité. Au début, il avait été horrifié de découvrir qu'on lui donnait de l'argent pour le service de Dieu, puis il avait compris que c'était nécessaire, vu que certaines choses ne lui étaient plus fournies. Il distribuait néanmoins beaucoup en aumônes.

Il avait aussi constaté qu'on lui conférait une importance à laquelle il n'était pas habitué. Il était désormais servi au haut bout de la table, là où une nappe de lin venait habiller le bois ciré. Il était même juste à côté du maître lorsque celui-ci n'avait pas ses fils avec lui. Cela le mettait encore plus mal à l'aise, et il gardait généralement les yeux rivés sur son tranchoir, ne répondant qu'aux questions directes, et toujours avec quelques propos laconiques. Il aimait que Renaud, l'un des cadets, soit présent. Plus âgé que lui d'une dizaine d'années, il était beaucoup moins affecté dans ses façons, sachant qu'il n'avait aucune responsabilité officielle. C'était un homme de guerre, à l'aise avec l'épée au côté, mais il s'était montré également le plus curieux et le plus amical avec Gilbert. Il avait réussi à l'attirer à quelques veillées, lui apprenant des jeux de table.

Par sa fonction, Gilbert célébrait des offices réguliers pour le seigneur Géraut et sa famille et les entendait en confession. Il avait été horriblement gêné de devoir se charger aussi des femmes, et rougissait de les écouter ainsi, obligées de lui dévoiler leur intimité. Heureusement pour lui, aucune faute avouée ne lui avait jusque là posé de problème, rien de grave ou de non répertorié dans les pénitentiels n'ayant été porté à sa connaissance. Il se doutait par ailleurs qu'il lui faudrait gagner la confiance de chacune des personnes avant qu'elles n'osent lui en dire plus. Il allait devoir apprendre à écouter.

Un autre avantage de la forteresse de Beaufort était de se situer loin de tout, avec une faible garnison. Gilbert se sentait souvent perdu dans la grande ville de Sidon, et il n'aimait guère devoir sortir de la Tour-Baudoin, le château seigneurial. En outre, cela signifiait qu'ici il ne partageait le lieu de culte avec personne. En ville, il devait composer avec un prêtre plus âgé que lui, qui semblait n'avoir accueilli un jeune collègue qu'avec réticence. Peut-être avait-il espéré la place, plus prestigieuse et vraisemblablement mieux dotée que sa cure. Gilbert s'efforçait pourtant de bien marquer son respect des formes, allant à confesse chaque semaine et suivant les messes de son confrère avec application.

Lorsque la porte grinça sur ses gonds, il suspendit son plectre, prêt à saluer quiconque se présentait là. En l'absence de paroisse, les permanents de la garnison tombaient sous sa juridiction spirituelle, mais la plupart montraient bien peu d'appétit pour les questions de cet ordre. Ils se contentaient d'être présents au moins le dimanche à la messe, s'abstenant de bavarder uniquement quand les maîtres étaient aux environs.

En voyant Renaud, il se leva et l'accueillit d'un sourire. C'était un homme assez svelte et tout en muscle, le port bien droit né de toutes ses années en selle, avec un visage extraordinairement laid. Gilbert savait que c'était souvent là le signe d'un esprit retors, ou du moins de quelque infirmité d'âme, mais il n'arrivait pas à en faire grief à Renaud. Celui-ci était par ailleurs certainement le plus instruit de la maisonnée.

« Vous allez finir par prendre la poussière à demeurer ainsi à l'ombre, mon père ! lança le jeune chevalier, un sourire aux lèvres.

--- Je n'ai pas grand plaisir à aller au soleil, cela échauffe les sangs.

--- Surtout ce jourd'hui. La chaleur coule comme plomb fondu. J'ai épuisé trois montures à la chasse ce matin. Mais au moins j'ai ramené tendre gazelle qui égayera nos soupers ! »

Gilbert ne put retenir un sourire gourmand. Il savait qu'il serait parmi les premiers à se servir dans les beaux morceaux et les plats de sauce. Renaud avait vite compris la façon de lui plaire.

« Je venais vous chercher pour vous proposer une partie d'échecs. Je ne désespère pas de vous y faire prendre goût ! Père déteste ça et nul ici n'a votre jugeote.

--- Je ne suis pas encore certain que ce soit là jeu bien chrétien...

--- Allons donc. On n'y lance nul dé ! Et on dit que le saint prédicateur Bernard lui-même ne les dédaignait guère. N'était-ce pas votre abbé ?

--- Il était abbé de Clairvaux et j'ai grandi dans une fille de Cluny !

--- Que voilà bien cléricale finasserie ! N'est-ce pas couvent les deux fois ?

--- C'est aussi différent pour moi qu'un alesan et un pie pour vous » répliqua Gilbert, tout content de replacer une nuance qu'il n'avait intégrée que depuis peu.

Renaud éclata de son rire joyeux et ouvrit en grand la porte, invitant le jeune chapelain à le suivre. Malgré ses manières familières et amicales, il était bien entendu que ses demandes ne devaient jamais être comprises autrement que comme des ordres.

### Sagette, château de La Tour-Baudoin, veillée du jeudi 10 septembre 1159

La grande salle bruissait des activités joyeuses d'une fin de veillée festive. Géraud Granier, seigneur de Sagette, recevait quelques chevaliers croisés qui allaient reprendre la mer d'ici quelques jours et une magnifique soirée d'adieu leur était offerte. Quelques molles promesses d'union matrimoniale avaient été évoquées, mais sans que personne n'y croie vraiment. Géraud tenait néanmoins à ce que la réputation de la terre de Sagette soit bonne jusqu'au cœur des plus lointaines principautés. Il avait encore des enfants à placer, dont Renaud était le plus bouillonnant exemple.

Gilbert avait eu le plaisir de passer du temps avec des voyageurs venus de Saxe, se replongeant dans son dialecte natal avec bonheur. Il était rare de voir des croisés originaires de cette région, leur suzerain Henri le Lion étant généralement plus favorable à des expéditions contre Niklot, un seigneur païen abodrite établi non loin de ses territoires. C'était aussi pour Gilbert la première fois qu'il assistait à un spectacle de théâtre d'ombres. Le sujet racontait les exploits de Godefroy de Bouillon lors de la première croisade, en un abrégé des Chansons habituelles. L'accent y était mis sur le caractère exceptionnel de ce héros et certaines de ses aventures semblaient proprement incroyables. Gilbert tint donc à s'en entretenir avec les jongleurs, un homme d'une trentaine d'années accompagné d'un petit garçon. Ils rangeaient leurs marionnettes de cuir quand le prêtre vint les chercher.

Il savait qu'il entrait dans un univers inconnu, ayant été prévenu contre les artistes ambulants depuis sa plus tendre enfance. Il était néanmoins intrigué de voir qu'ils étaient capables, malgré toute l'infamie de leur condition, de célébrer des sujets inspirants. S'il était un thème qui rassemblait tout le monde en Terre sainte, c'était bien la figure de Godefroy.

L'homme l'accueillit d'un sourire poli, sans crainte, puis inclina la tête avec respect, le reconnaissant à sa tonsure comme un ecclésiastique. Il donna un coup de coude au gamin pour que celui-ci exécute également un salut dans les formes.

« Pouvons-nous vous aider, mon père ?

--- De certes. De qui tenez-vous ces amiables récits ? J'y ai trouvé quelque matière propre à édifier. Et l'exemple venant de si haut, il n'en est que plus motivant.

--- La plupart sont des extraits des chansons de celui qu'on nomme Richard, pérégrin des premiers temps. Un certain nombre est né de choses que j'ai entendues de différents jongleurs, quoique je confesse avoir qui-ci qui-là fleuri de menus détails, souventes fois pour la rime.

--- Oh, vous versifiez ?

--- J'ai ce plaisir, certes. J'ai étudié quelques années, pour savoir mes arts assez. Du moins suffisamment pour tirer de cette pauvre caboche de quoi enjoyer laboureurs et barons. »

Gilbert était tellement ébahi de voir que certains saltimbanques avaient donc quelques lettres qu'il en oublia sa déception à la nouvelle que les exploits de Godefroy étaient en grande partie imaginaires.

« Concevez-vous également les mélodies de vos chants ?

--- Certes. J'emprunte ici et là de quoi garnir ma besace, mais ce qui en sort est bien de moi. »

Gilbert sourit de toutes ses dents. C'était la première fois qu'il s'entretenait avec un compositeur qui créait des pièces originales, selon sa propre inspiration. Il crut un instant se trouver au paradis.

« Auriez-vous un peu de temps pour parler musique ? »

### Sagette, château de La Tour-Baudoin, matinée du mardi 15 décembre 1159

Une large table était disposée dans un des coins de la grande salle d'audience lorsque Géraud ne recevait pas. Le personnel de son administration s'y installait pour établir les comptes, rédiger les missives ou recopier les chartes. Gilbert avait pris l'habitude d'y travailler, profitant de la tiédeur de la pièce et de la calme activité qui y régnait. C'était l'endroit qui lui rappelait le plus le monastère. En dehors du seigneur de Sagette et de sa famille, nul n'élevait la voix et bien peu parlaient haut.

Il y préparait avec application ses célébrations sur de petites tablettes de cire et y venait lire les quelques ouvrages dont la chapelle était dotée. Il s'y trouvait un épitomé du pénitentiel de Burchard de Worms, qu'il avait toujours connu. Il regrettait juste que c'en soit une forme abrégée, car il se sentait de temps à autre décontenancé par les péchés auxquels il était confronté. Tout ce qui ressortissait des soucis de conflit armé ou des relations avec la femme lui était difficile à appréhender car il n'avait jamais eu à se frotter à ces domaines jusque là. Il avait donc emprunté à la bibliothèque de l'évêque de Sagette une version plus complète des écrits de Burchard et se confectionnait un addendum pour ces questions.

Il en était à un point qui le tracassait au plus haut point, et qui concernait les interdits alimentaires : *Comedisti de cibo Judaeorum vel aliorum paganorum quem ipsi sibi prapeparverunt*[^21]. Ce *aliorum paganorum* semblait indiquer que chaque fois que Géraud ou, plus souvent Renaud, partageait un repas avec les mahométans en tant qu'émissaire, il devait faire ensuite pénitence dix jours au pain et à l'eau. Ce qui n'était pas sans poser problème, d'autant qu'ils n'avaient ni l'un ni l'autre jamais avoué encore pareil péché. Quand bien même Gilbert savait pertinemment qu'ils s'étaient déjà trouvés dans ce cas. Il nota la question pour l'aborder avec son propre confesseur, voire avec l'évêque.

Estimant qu'il avait assez travaillé pour le matin, il alla marcher un peu dans la grande cour. Malgré le soleil, un vent descendu des montagnes apportait de la fraîcheur et il avait enfilé des chausses de laine épaisses. C'étaient là une des commodités de la vie séculière qu'il aimait assez, de pouvoir se vêtir de façon confortable. Il continuait néanmoins de porter de modestes et informes cottes longues, incapable d'assumer l'indécence de les couper au genou. Il prenait par contre grand soin de sa tonsure, indicatrice de son état de clerc. Au milieu du peuple, il était toujours inquiet qu'on l'aborde comme un simple quidam et pas avec le respect dû à sa fonction. Ce n'était nullement par orgueil, mais par crainte de ne pas réagir correctement au milieu de laïcs méconnaissant son état.

Lorsqu'il revint dans la salle, il vit qu'on commençait à dresser la table pour le repas. Géraud était présent et discutait avec plusieurs de ses chevaliers des campagnes de l'année. Il était à la fois heureux de n'y avoir rien perdu, mais aussi un peu déçu qu'aucune conquête n'ait pu se faire malgré la rencontre avec le basileus byzantin. Il trouvait qu'on faisait bien grand cas d'un souverain qui n'avait pas à cœur les intérêts des territoires latins. Il expliquait que son père, l'éminent Eustache, *l'épée et le bouclier du royaume*, avait toujours eu profonde méfiance de ceux qu'il considérait à peine moins que des hérétiques. Géraud n'était pas loin de partager son avis.

Sans se faire remarquer, Gilbert alla ranger ses affaires, glissant soigneusement les codex dans des étuis de cuir et classant ses feuillets dans un portfolio. Dans un coin du coffre où il laissait tous ses écrits, il aperçut les notes qu'il tenait de Herbelot. Il n'y avait guère travaillé, manquant d'inspiration, trouvant l'entreprise trop ambitieuse pour lui. Il s'en voulait un peu de conserver ainsi un tel projet sans rien en faire. Puis il eut soudain une idée : il pouvait se servir de ces idées édifiantes jetées pêle-mêle pour des conseils à son seigneur. Mais de façon détournée peut-être, en s'adressant justement à ce fameux roi byzantin, ce Manuel Comnène, en le prenant à parti. Ses propos n'en auraient que plus de facilité à se faire accepter par ceux dont il avait la charge d'âme. ❧

### Notes

Le statut des clercs est extrêmement varié et leurs fonctions au sein de la population pouvaient prendre de nombreuses formes. Vu qu'ils constituaient la frange la plus littéraire de la société, ils nous ont laissé, par chance, une documentation assez conséquente sur cette société parallèle dans laquelle ils évoluaient. Cela n'en rend pas forcément la découverte et le parcours aisé pour les néophytes séculiers que nous sommes pour la plupart. On ne peut néanmoins pas se résoudre à accepter comme suffisamment évocatrices les maigres caricatures que l'on nous assène sans discontinuer du moine paillard, du curé inquisiteur ou de l'évêque politicien.

La pléthore de fonctions qu'ils avaient à remplir créait autant de caractères qu'il y avait de situation, et la lecture des sources nous dévoile une infinie variation de vies. Malgré toutes les tentatives de réforme, de contrôle par les hauts dignitaires, il n'existait aucune homogénéité et des traditions dynamiques s'opposaient parfois, régionalement ou culturellement. J'ai trouvé intéressant de montrer le parcours intellectuel que pouvait connaître un jeune oblat dont le destin aurait voulu qu'il soit forcé de sortir de la clôture qui l'avait toujours protégé. Car c'est aussi la période où l'on estime que le service de Dieu ne peut être envisagé que par choix librement consenti.

### Références

Avril Joseph, « En marge du clergé paroissial : les chapelains de chapellenies (fin XIIe-XIIIe siècles) » dans *Actes des congrès de la Société des historiens médiévistes de l'enseignement supérieur public. 22e congrès*, Amiens, 1991. pp. 121-133

Casagrande Caria, Vecchio Silvana, « Clercs et jongleurs dans la société médiévale (XIIe et XIIIe siècles) », dans *Annales*, Année 1979, Volume 34, Numéro 5, p. 913-928

de Miramon Charles, « Embrasser l'état monastique à l'âge adulte (1050-1200). Étude sur la conversion tardive », dans *Annales*, Année 1999, Volume 54, Numéro 4, p. 825-849

Devailly Guy, « Le clergé régulier et le ministère paroissial » dans *Actes des congrès de la Société des historiens médiévistes de l'enseignement supérieur public. 5e congrès*, Saint-Étienne, 1974, p. 151-164.

Gagnon François, *Le Corrector sive Medicus de Burchard de Worms (1000-1025) : présentation, traduction et commentaire ethno-historique*, Mémoire de Master, Université de Montréal, Département d'histoire, Faculté des arts et des sciences, 2010. \[En ligne\], consulté le 19 mars 2019, Adresse URL : <https://papyrus.bib.umontreal.ca/xmlui/handle/1866/4915>

Hilton Suzanne M., *A Cluniac office of the Dead*, Thèse de Master of Arts, Université du Maryland, College Park, 2005.

Le lion et l'éléphant
---------------------

### Jérusalem, rue du Temple, aube du mercredi 15 avril 1159

Dans la fraîcheur de l'aurore, son porte-manteau sur l'épaule, Ernaut avançait gaiement dans la rue commerçante encore à demi ensommeillée. La plupart des échoppes étaient fermées et les plus matinaux des vendeurs s'affairaient à encombrer le passage avec leurs sacs de fèves et de pois, leurs paniers d'épices ou de grain, leurs pots de condiments. Il leur prêtait des remarques appréciatrices sur sa fière allure, son allure martiale impressionnante et cela le mettait en joie.

Il s'était porté volontaire pour rejoindre le roi Baudoin à Antioche. On y rassemblait tout ce que les territoires latins pouvaient compter de forces militaires afin d'aller assiéger la ville de Nūr al-Dīn[^22], la prestigieuse Alep. Le basileus byzantin Manuel Comnène était descendu avec une considérable armée dont on disait qu'elle recouvrait les reliefs. De nombreux croisés, venus pour Pâques, avaient décidé de se joindre également à l'expédition. Depuis plus de dix ans et l'assaut manqué sur Damas, on n'avait pas vu un pareil enthousiasme, une telle effervescence.

Ernaut estimait que c'était là une occasion rêvée de faire ses preuves. Il avait déjà tout son équipement de combat, et n'avait eu qu'à se munir d'un grand sac en cuir huilé qui supporterait le voyage par bateau jusqu'au nord. La perspective de parcourir de nouveaux lieux l'excitait au plus haut point. On lui avait tant vanté la cité de saint Pierre, les montagnes de Cilicie... Et La mystérieuse Alep, qui représentait depuis des dizaines d'années le repaire des adversaires les plus acharnés du royaume latin. Il était plus que temps de s'en emparer et d'en faire une fidèle baronnie. En souriant, Ernaut s'imaginait prêtant hommage à Baudoin pour ce nouveau fief qui n'allait pas tarder à naître, en récompense de ses futurs exploits sur les murailles.

Les abords du palais étaient encore plus agités qu'à l'ordinaire. Même si la plupart des officiers et des hommes du roi étaient déjà au nord depuis des mois, c'était là que beaucoup de voyageurs se regroupaient ou venaient simplement recevoir leurs instructions. On assemblait généralement la caravane à la porte de David, à l'ouest de la cité avant de prendre la route pour Jaffa. Gaston, vétéran qui avait initié Ernaut au combat d'infanterie, s'avançait vers lui alors qu'il franchissait à peine le portail.

« Tu arrives à point, gamin. Viens-t'en avec moi à l'arsenal. »

Sans un mot de plus, il partit en direction de la citadelle de David, où la plupart des armes et armures étaient stockées. Les fantassins royaux avaient leur équipement de guerre en propre, mais ils étaient tenus de les laisser à l'abri. Ils ne conservaient que leurs protections de corps et leur épée. Le reste était distribué au moment des campagnes, ou pour les exercices. À côté du sergent boiteux, Ernaut avançait d'un pas allègre, affichant toujours sa bonne humeur du matin. Gaston la remarqua et arbora un rictus dont on ne savait s'il était gai ou cynique.

« Le voilà ce bel assaut dont tu rêvais, jeune, on dirait bien !

--- J'ose l'encroire ! répliqua Ernaut, un large sourire aux lèvres.

--- Ici, ils disent que *la vie est semblable au feu, elle commence par la fumée et finit par la cendre*. C'est aussi vrai de tous les combats. Garde-toi bien de trop en espérer... »

Lançant un rapide regard en coin au petit homme noueux qui trottinait à ses côtés, Ernaut se rembrunit. Gaston était souvent acide, voire carrément rabat-joie. Peut-être était-il vexé de s'être fait écarter de la troupe qui allait s'emparer d'Alep. À son âge, qu'Ernaut estimait autour de la quarantaine, il était bon d'avoir accumulé un pécule pour sa retraite, afin de profiter de la vie. Sa situation actuelle démontrait qu'à l'évidence il n'y était pas parvenu. Et il allait rater une des plus belles occasions de s'enrichir que les soldats aient connue depuis de nombreuses années.

Reconnus par les sentinelles de faction, ils pénétrèrent sans halte dans la cour où une interminable cohue d'ânes et de mulets attendaient dans les cris et l'agitation que le chargement soit fini. Des valets attachaient boucliers, lances et toiles pour les hommes du rang, sous la supervision de clercs à l'air concentré, stylets et tablettes de cire en main. D'autres sergents patientaient, aidaient à fixer les matériels. Ni Droart ni Eudes n'étaient là, ce qu'Ernaut regrettait un peu. Il n'avait aucun proche parmi les soldats avec qui il allait passer les semaines, voire les mois, à venir. Il aurait même supporté la présence de Baset, mais ce dernier arrivait à esquiver tous les enrôlements avec talent et il était exceptionnel qu'il aille bien loin de l'enceinte de la cité.

De la voix, de la main, Gaston pressait le mouvement. Il était prévu de corner le départ quand le couvent du Saint-Sépulcre marquerait tierce. D'ici là, toutes les bêtes devaient être assemblées en file à l'extérieur des murailles. Le cortège d'animaux franchissait à peine les portes de la ville que la grosse cloche se mit à sonner. Les cavaliers étaient en place loin devant, chacun sous la bannière qu'il servirait lors des affrontements. Venaient ensuite les contingents de fantassins qui patientaient en une longue colonne derrière eux. Enfin, en dernier étaient disposés les bagages, où chameaux et mules étaient menés par une légion de valets.

De nombreux habitants, et même des voyageurs, des pèlerins, s'étaient amassés pour admirer l'impressionnante procession militaire, encourageant ces valeureux combattants de la foi avec des chants, des prières, des appels de voix. Quelques familles disaient aussi au revoir à ceux des leurs qui partaient pour une périlleuse aventure. Ernaut regrettait de ne pas voir Libourc parmi tous ces badauds, tandis qu'il rejoignait son groupe. Il aurait aimé la serrer dans ses bras une dernière fois, il avait à peine eu le temps de lui faire savoir qu'il allait au loin. Il n'eut pas le loisir de laisser ces pensées sombres s'installer : la grande bannière à l'avant venait de s'incliner, ordre bien vite repris par les voix fortes des responsables d'unité. En un instant, le faubourg fut noyé de la poussière de leur avancée.

### Village de Hammam, gué de la Balanée, fin d'après-midi du lundi 25 mai 1159

De faction au gué qui traversait le Nahr Ifrin, Ernaut fut parmi les premiers à voir arriver la file des marcheurs blancs de poussière. Quelques-uns, certainement des barons d'importance, avaient reçu une monture de leur escorte byzantine. Les autres avançaient, hagards, pieds nus et en sous-vêtements sales, la barbe longue et le cheveu hirsute. On aurait dit une armée de mendiants. C'étaient les prisonniers francs que le basileus Manuel avait pu faire libérer suite à des échanges diplomatiques avec l'émir Nūr al-Dīn : plusieurs centaines d'hommes du commun et une poignée de détenus notables.

Fragiles sur leurs jambes, certains hésitèrent à entrer dans l'eau du gué malgré la corde qui avait été tendue au long pour justement les aider. Sur la rive occupée par l'armée latine, c'était la consternation. Dans un silence de mort, quelques-uns tentaient de reconnaître des visages, des silhouettes.

Au milieu des plénipotentiaires byzantins se tenait un petit groupe de Turcs, coiffés du sharbush[^23], le sabre et l'arc au côté. Ils accompagnaient un civil, un arabe monté sur un magnifique étalon, qu'il ne semblait guère maîtriser. Un érudit quelconque capable de parler plusieurs langues, mais peu familier des voyages en selle. Sa posture fit ricaner les Latins, soulagés de trouver un exutoire à la tension qui s'était établie. Certains parièrent même sur sa chute probable dans le vif courant, mais ils en furent pour leurs frais, et se contentèrent de cracher dans sa poussière une fois qu'il les avait dépassés.

Parmi son groupe, Ernaut avait bien sympathisé avec Daimbert. Sergent du prince de Galilée, c'était lui qui était en charge du petit comité qui surveillait le passage à travers le cours d'eau. Il était monté sur son cheval pour faire bon accueil aux arrivants et les regardait défiler, faisant signe à tous de continuer à avancer sans ralentir.

Tandis que le flot des captifs s'étendait inlassablement devant eux, Ernaut fronça les sourcils. Il comprenait ce que signifiait la défaite, quand on n'était pas tué durant les affrontements, mais se voyait confronté pour la première fois à la réalité du destin des prisonniers. Il tentait de se montrer aimable, encourageant de la voix les hommes à ne pas ralentir, leur vantant le confort qui les attendait. À côté de lui Daimbert demeurait muet, suçotant un morceau de bois qui lui servait de cure-dent. Il ne rompit le silence que lorsque la colonne eut entièrement franchi le gué, incitant chacun à un redoublement de prudence.

« Tu crois qu'ils pourraient nous assaillir alors que leur envoyé est au parmi du camp ?

--- Ces diables ont toujours quelque ruse en leur besace...

--- Ils ne doivent pas se sentir si vaillants, qu'ils acceptent toutes les demandes qu'on leur fait !

--- C'est surtout la vaillance des Griffons[^24] qui va pas tarder à manquer, m'est avis. »

Ernaut dévisagea son chef, surpris de l'entendre émettre un commentaire désobligeant sur leurs alliés. Il était plutôt fort en gueule, mais semblait n'avoir que peu de colère en lui, hurlant juste pour se faire comprendre des plus rustiques de ses hommes.

« Il se dit des choses ?

--- Certes oui. Il y a eu force messages échangés et certains des Griffons du souk racontent qu'ils vont bientôt remballer. »

Ernaut n'en crut pas ses oreilles. Jamais les négociants byzantins ne quitteraient le camp sans l'armée. Cela signifiait forcément que le basileus prévoyait de prendre la route.

« On va enfin lancer l'assaut ?

--- Nous, je sais pas, mais il semblerait que le roi Manuel ait décidé de s'en retourner chez lui.

--- Comment ça ? Va-t-on courir sus Halepe sans lui ?

--- Je serais pas surpris qu'on reparte sans même en avoir vu la muraille. »

Daimbert soupira longuement avant d'accorder un sourire amer à Ernaut.

### Village de Hammam, camp latin du gué de la Balanée, après-midi du samedi 30 mai 1159

Le petit hameau depuis longtemps déserté de Hammam accueillait des sources chaudes qui avaient permis l'installation de bains, à la jonction du camp militaire et des marchés périphériques, en bordure des aménagements de toile, de corde et de bric-à-brac qui caractérisaient tous les établissements de campagne. L'accès y était assez difficile pour les hommes du peuple, qui se contentaient souvent de se baigner dans le Nahr Ifrin. Comme beaucoup d'autres, Ernaut avait pris le parti de se rafraîchir dans une boucle de la rivière en amont des endroits où l'on risquait de souiller l'eau. Il y avait ses habitudes, avec quelques compagnons dont certains y faisaient parfois leur lessive.

Au milieu des flots, il se savonnait activement les cheveux et les oreilles, les yeux fermés, quand il perçut soudain une grande agitation autour de lui. Il plongea la tête dans le courant pour se rincer et comprendre ce qui se passait. Lorsqu'il tourna son regard vers la rive, il se figea.

Un lion, ou une lionne, à son absence de crinière, déambulait nonchalamment parmi les affaires du petit groupe, la queue ondulant tandis qu'elle reniflait les objets. Elle n'était qu'à quelques pas d'Ernaut, qu'elle considéra soudain avec curiosité. La tête penchée, la gueule à demi ouverte, elle semblait surprise par ce qu'elle voyait. Face à elle, Ernaut était nu, le corps en partie couvert de mousse, une savonnette à la main.

C'était la première fois qu'il était face à une telle bête et il percevait les remugles acides de sa transpiration ramper jusqu'à lui, il entendait son souffle alors qu'elle l'étudiait. Il n'avait aucune idée des habitudes de ces animaux et se prit à prier pour qu'elle n'eût pas faim.

Le temps parut s'étirer tandis que la lionne le fixait de ses yeux attentifs. Puis elle bâilla, dévoilant des crocs propres à briser l'échine du plus colossal des humains. Ernaut sentit ses jambes faiblir, son bas-ventre se crisper. Il n'y avait aucun bruit aux alentours et il espérait que ses compagnons étaient partis chercher de quoi faire fuir le fauve. Il serra fort son poing libre et se raidit soudain. Puis il s'élança en hurlant vers la lionne, lui balançant sa savonnette en plein museau tandis qu'il projetait de grandes gerbes d'eau en avançant. En deux bonds, l'animal s'esquiva dans les buissons, laissant Ernaut essoufflé, dégoulinant, le cœur à cent à l'heure, tout seul et nu sur la berge.

Il entendit alors de nouveau le vent dans les branchages et des voix qui s'approchaient. Une demi-douzaine de soldats accouraient avec des lances et des arcs. En voyant Ernaut ainsi immobile, indemne et ébahi, ils éclatèrent de rire.

### Village de Hammam, camp latin du gué de la Balanée, veillée du samedi 30 mai 1159

L'histoire du sergent du roi qui chassait le lion entièrement nu ne mit que fort peu de temps à parcourir le camp, apportant un peu de légèreté dans l'atmosphère oppressante qui régnait jusque là. L'annonce du départ byzantin avait plongé les troupes dans la morosité, et chacun accueillit avec soulagement l'amusante anecdote. Ernaut goûtait fort son succès et raconta la scène une bonne dizaine de fois durant son souper, agrémentant chaque nouveau récit de détails imaginatifs. Il ne fut donc que modérément surpris qu'on vienne le chercher pour aller narrer son exploit ailleurs. Il ne s'attendait néanmoins pas à être guidé jusqu'à la tente maîtresse du prince d'Antioche, Renaud de Châtillon.

Ce chevalier ombrageux dont le courage forçait l'admiration des hommes se tenait un peu en retrait après avoir dû s'humilier devant le basileus byzantin Manuel, en demande de pardon de ses exactions passées. Ernaut se souvenait que lors de sa venue en Terre sainte, ils avaient dû éviter l'île de Chypre à cause de son raid[^25]. Il se racontait d'ailleurs que le féroce prince n'avait que fort peu d'égard pour ceux qui le contrariaient. Le patriarche de sa cité avait fini nu et enduit de miel en haut d'une tour pour avoir osé s'opposer à lui.

Avançant tout en frottant sa cotte de la main, Ernaut échangea quelques mots avec le valet, histoire de ne pas commettre d'impairs. Celui-ci, un jeune arménien à l'accent chantant, lui expliqua que le prince était de bonne humeur, ayant entendu des jongleurs toute la soirée. Lorsqu'il avait appris l'anecdote, il avait immédiatement désiré rencontrer le sergent qui chassait les lions sans arme, sans préciser rien de plus.

Lorsqu'Ernaut pénétra dans la tente, il fut surpris du luxe qu'il y découvrit. En dehors des mâts et des grincements des cordages, on se serait cru dans un palais : épais tapis sur le sol, nombreuses lampes à huile, tentures bariolées dont les motifs de soie accrochaient la lumière. Une dizaine de sièges confortablement rembourrés de coussin étaient disposés en demi-cercle face à l'espace servant de scène, où se produisaient un musicien et un conteur.

Au centre de l'hémicycle ainsi formé, sur un trône plus imposant que ses voisins, se tenait le prince d'Antioche. Ernaut l'avait déjà aperçu au loin, mais il le voyait véritablement pour la première fois. Il était assez grand, brun de poil et de peau. D'une trentaine d'années, il avait un visage carré d'où partait un nez légèrement busqué, fermé d'un menton en galoche. Une barbe très courte et des cheveux frisés coupés à l'écuelle indiquaient l'homme de guerre qu'il était, ainsi que les fines rides au coin des yeux qui plissaient son teint buriné. Il n'était plus si imposant une fois dépouillé de son harnois, mais ses gestes trahissaient son goût pour l'action, un corps préparé à tous les efforts.

En voyant Ernaut entrer, il rugit littéralement, interrompant sans égard le spectacle pour permettre au jeune sergent de s'avancer.

« Ah, voilà donc notre héros ! Comment t'appelles-tu, mon garçon ? Goliath ? Samson ?

--- Ernaut, sire prince.

--- Ah, que ce nom sonne doux à mes oreilles ! On dirait le mien ! Peut-être m'attribuera-t-on ton exploit ? »

Il éclata de rire, satisfait de sa propre blague.

« Cela ne serait pas de trop, il y en a bien assez à dégoiser sur moi en ce moment, cela leur clouerait le bec à tous. »

D'un geste, il intima à un de ses voisins de laisser son siège et indiqua à Ernaut d'y rendre place. Le jeune homme en rougit de plaisir et de gêne tandis que le chevalier ainsi mis à l'écart lançait un regard hautain à Ernaut, ce que Renaud aperçut.

« N'ayez qu'amitié pour ce sergent le roi, compères. Voilà vaillant qui vaut bien cent Griffons, dont il nous faudrait pleines échelles ! Il chasse le lion avec un savon ! »

Puis, se tournant, hilare, vers Ernaut, il ajouta : « Avais-tu l'intention de lui frictionner le museau ? »

N'attendant même pas sa réponse, il répartit d'un grand rire et fit signe qu'on leur serve à tous du vin. Emportés par le plaisir de leur seigneur, les chevaliers firent un relatif bon accueil à Ernaut. S'ils n'étaient pas toujours à l'aise avec le peu de sens des convenances de leur prince, ils en partageaient le goût pour la bravoure.

« Figure-toi que j'ai mandé ces jongleurs, car ils sont Englois et ils connaissent un conte de leur pays qui parle d'un chevalier tel que toi. »

D'un geste, il apostropha le ménestrel, qui attendait patiemment qu'on lui demande de reprendre sa narration.

« Et comment s'appelle donc le brave qui se fait compaigner d'un lion ?

--- Owein, sire prince. »

Renaud de Châtillon se tourna alors vers Ernaut et, faisant glisser un petit anneau d'or d'un de ses doigts, il le donna au géant.

« Voilà, ce sera désormais ton nom auprès de moi. Si tu as désir de venir dans les terres du Nord, si tu ne te plais plus à Jérusalem.... Quels que soient tes besoins, cet anel sera ton sauf-conduit jusqu'à moi, *Yvain*. »

Puis il demanda à Ernaut de leur raconter à tous comment on s'y prenait pour chasser le fauve quand on n'avait pas d'armes en dehors d'une savonnette. Il était espiègle et enjoué comme un enfant au premier jour de vacances et riait aux éclats à la moindre occasion. Un vrai chevalier comme Ernaut avait toujours rêvé d'en rencontrer. Peut-être ne serait-il pas de trop d'ajouter encore quelques détails propres à amuser son auditoire, se dit-il. ❧

### Notes

Le titre de ce Qit'a est issu de l'expression « L'éléphant du Christ » par lequel les musulmans désignaient alors Renaud de Châtillon, en référence à une sourate du Coran. Il s'agit ici de la première apparition dans Hexagora d'un personnage de premier plan dans mon évocation du royaume de Jérusalem. J'espère avec le temps arriver à en tracer une image plus complexe, riche et intéressante que les caricatures romantiques ou brouillonnes qu'on en trouve généralement. Influencé par les travaux de Bernard Hamilton, j'avais déjà tenté de donner une lecture nuancée de son action dans un livre édité voilà quelques années chez Histoire et Collections (« La bataille de Hattîn »), dont j'ai récupéré récemment les droits et que je prévois de republier sous licence libre.

L'anecdote autour du lion est née des très fréquentes mentions de ce fauve dans les mémoires d'Usamah ibn Munqidh, qui se vantait d'en avoir chassé sans relâche toute sa vie, sans jamais avoir été blessé par l'un d'eux. Cet animal, désormais disparu au Moyen-Orient, était alors assez répandu et causait de nombreux dommages aux cheptels et n'hésitait pas à s'attaquer parfois à l'homme. On en rencontrait aussi quelques spécimens apprivoisés, dont Usamah nous raconte qu'il en vit un mis en fuite par un mouton particulièrement belliqueux.

Le surnom que donne Renaud à Ernaut est tiré d'une possible source d'inspiration galloise de Chrétien de Troyes pour son Yvain, *Le Chevalier au lion* (écrit vers 1175-80), et qui se baserait sur la vie de saint Mungo. Le fait de placer cette référence cultuelle presque vingt ans avant la rédaction du roman champenois est une façon de mettre en lumière l'existence préalable à toute création culturelle d'un substrat qui le nourrit et en permet la floraison. Il s'agit là d'un concept qui sous-tend une grande partie des textes d'Hexagora en général et les Qit'a en particulier.

### Références

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008.

Hamilton Bernard, « The Elephant of Christ: Reynald of Chatillon » dans *Studies in Church History*, vol. 15, 1978, p. 97-108.

Schlumberger Gustave, *Renaud de Châtillon, prince d'Antioche, seigneur de la terre d'Outre-Jourdain*, Paris, Plon : 1898.

En marche
---------

### Chêne d'Abraham, faubourgs de Hébron, midi du jeudi 3 mai 1151

Installé contre un tronc du bosquet, à l'abri de sa ramure, Garin grignotait un peu du quignon de pain plat qui lui restait. Il admirait le vieux chêne tortueux qui déroulait ses branches face à lui, ses ramilles feuillues ondulant mollement dans une impalpable brise. Il était seul à profiter de l'endroit, la chaleur du soleil ayant chassé les pèlerins qui s'étaient égayés dans la campagne après la fin des Pâques à Jérusalem. Cela faisait deux jours qu'il contemplait le vénérable tronc noueux. Il avait vu passer des fidèles, qui se faisaient conter les miracles qu'avait opérés la sainte plante : c'était là qu'Abraham avait vécu et installé sa tente.

Il avait une petite provision de bouche, essentiellement de poisson séché et de figues et une source peu éloignée l'abreuvait à satiété. Il n'en demandait pas plus. Pour dormir, il se roulait dans le manteau qui lui servait contre le froid et la pluie, profitant de la douceur du climat. Depuis des mois, il prenait la route dès qu'il sentait qu'on tentait de lui causer des problèmes ou qu'on cherchait à le retenir. Il avait passé un temps comme manouvrier au service des frères de Saint-Jean, dans un casal non loin d'Acre, mais il n'arrivait pas à rester en place.

Les signes d'infamie qui avaient percé ses paumes à Naplouse[^26], le désignant comme un serviteur parjure, ne lui laissaient guère d'occasion de s'installer, de toute façon. À peine ces stigmates étaient aperçus, les visages se fermaient, l'accueil se faisait plus réservé et les offres de travail moins enthousiastes, moins longues.

Cela lui convenait. Il errait d'une saison à l'autre entre Césarée et Jaffa sur la côte et s'aventurait de temps en temps dans les reliefs intérieurs jusqu'aux berges méridionales de la Mer Morte. Il ramassait les olives, transportait le sel, curait les fossés, portait pierres, mortier, bagages et matériaux. Sans jamais rester en place. Un petit chien l'avait suivi plusieurs semaines, mais il avait disparu une nuit sans lune, alors qu'ils dormaient dans les collines de Samarie. Garin se demandait s'il avait trouvé un meilleur maître ou fini sous la dent d'un chacal voire d'un lion.

Parfois il tressait des chapeaux avec des pailles négociées en salaire. Puis il les revendait aux ouvriers des champs, contre nourriture et boisson, en échange de vieux vêtements usés. Il avait aussi guidé des caravanes de pèlerins entre Acre ou Jaffa et Jérusalem. Il y avait appris d'autres langues, de nouvelles coutumes, avait sympathisé avec ces voyageurs venus d'outremer. Mais il demeurait toujours en marge, refusant de se plier à l'usage d'un lieu, de s'engager à suivre les codes d'un groupe. Il veillait surtout à ne jamais s'aventurer trop au nord, où on aurait pu le reconnaître. Il s'était laissé pousser la barbe, se faisait tailler les cheveux de temps en temps à l'écuelle et portait une tenue aussi latine que possible, oubliant l'esclave qu'il avait été, dans les territoires musulmans. Il avait même un petit pendentif en forme de croix, et avait appris les prières indispensables pour avoir bon accueil dans les couvents. Mais dans son cœur, il n'éprouvait aucun attrait pour cette religion, pas plus que pour celle qu'il avait pratiquée durant des années quand il servait Zengī[^27].

Il hésitait à passer le Jourdain, à aller admirer les forteresses les plus éloignées au sud, rêvant de la cité méridionale dont on disait qu'on pouvait y trouver des navires commerçant avec les Indes. Il gardait un bon souvenir des bateaux, quand il était encore avec sa famille. La traversée du Bosphore avait été particulièrement rapide, mais il avait aimé le goût des embruns sur sa langue, la caresse du vent dans ses cheveux. Le grincement des voiles et des cordages lui avait donné l'impression qu'ils allaient s'envoler.

Il entendit sonner sixte dans le couvent voisin, appelant les frères à la prière. La chaleur allait croître, avec le soleil au mitan du ciel. Il épousseta ses mains à destination des fourmis avant de se faire un oreiller de sa besace et s'étendit dans l'herbe déjà sèche, mains sous la tête. Puis il s'endormit en fredonnant une comptine que, jadis, sa mère lui chantait.

### Ascalon, camp de siège, veillée du lundi 17 août 1153

Assis en bordure des jardins, Garin tressait des cordons avec de vieux brins glanés près des navires démantelés pour faire les engins de siège. Il faisait cela en écoutant distraitement les aventures militaires d'un vétéran. Il avait l'impression d'avoir déjà entendu la moindre anecdote d'intérêt, les récits les plus inventifs. Chaque histoire était la même que la précédente. Les langues changeaient, les conteurs avaient leurs tics propres, mais leurs souvenirs ne variaient que peu.

Il avait rejoint la grande armée qui attaquait Ascalon, et gravitait autour du souk al-askar, le marché qui s'accrochait aux soldats en campagne. Il donnait la main en échange de nourriture, faisait ses petites affaires, menus trafics et services sans lendemain. Il veillait à ne pas trop s'aventurer au milieu du village de toile des guerriers, où les chemins étaient tracés de corde, mais aimait venir admirer les assauts, à l'abri depuis les arrières. Cela le ramenait quelques années en arrière sauf que désormais il vivait tout cela en homme libre.

Si la ville tombait, il avait l'intention de s'infiltrer à la suite des conquérants. Mais ce n'était pas dans l'espoir de piller, juste de pouvoir entrer et déambuler à son aise. Les maisons abandonnées à la hâte lui semblaient comme autant de magnifiques promesses, de merveilleux territoires à visiter. Il pourrait peut-être s'attribuer un logement, non loin du port, d'où il pourrait voir chaque matin les vagues se briser sur la plage.

Mais pour le moment, l'ambiance était plutôt morose dans le camp chrétien. La principale tour d'assaut avait été incendiée deux jours plus tôt. Et si le vent avait finalement rabattu les flammes sur la muraille, entrainant l'effondrement d'un pan de courtine la veille, la joie n'avait que peu duré. Un peloton des hommes de la Milice du temple s'était engouffré dans la brèche, s'octroyant le prestige d'être les premiers à entrer à la suite de leur grand maître. Leurs corps pendus décoraient désormais les remparts, les fātimides ayant veillé à illuminer le chemin de ronde de lampes en ces nuits obscures de dernier quartier.

Un sergent, occupé aux dés jusque là vint s'assoir en silence près de Garin, un pichet de bière à la main. Tout en sirotant sa boisson, il regardait la cité qui les narguait, l'air buté. Il se tourna au bout d'un moment vers son voisin, dont les doigts dansaient sans même qu'il y prenne garde.

« Bien des années que ces démons s'abritent en leurs murailles. Ne pourra-t-on les mettre à bas ?

--- J'ai guère la science de ces choses, mestre. L'ost le roi me semble puissant assez à cela.

--- Si fait ! Aucun autre ne pourrait assembler tant de bannière et d'engins. Et pourtant nous voilà envasés dans les sables depuis Carême ! »

Garin haussa les épaules, conciliant. Le soldat lui tendit son pichet.

« J'ai nom Fouques, sergent Baudoin. Je n'ai pas souvenance de t'avoir vu en son hostel...

--- Moi c'est Garin, je ne sers personne, et ne suis là que pour aider au marché ».

Puis il s'offrit une lampée avant de rendre le récipient et de reprendre son ouvrage en silence. Des voix leur parvenaient depuis les murailles, portées par le vent du large. Une des phrases fit grogner Garin, ce qui éveilla l'intérêt de Fouques. D'un regard, il s'enquit de ce qui se passait.

« Ils nous insultent et nous maudissent...

--- Tu comprends leur langue ? Moi qui suis né au royaume je n'en connais que des bribes.

--- Je travaille souvent pour des Judéens, des Samaritains : fellahs, marchants ou artisans. Alors j'ai usage de leur parler, oui. »

Fouques acquiesça, s'offrant une nouvelle gorgée tout en tendant l'oreille. Il entendit quelques phonèmes, qu'il fut incapable de reconnaître.

« Et là, ils disaient quoi ?

--- Des choses pas terribles pour nos épouses et nos mères, nos filles... »

Il finit sur un rictus moqueur, Fouques hocha le menton.

« Certes, nous en avons tout autant à leur intention. Sauf que nos femmes sont loin d'ici, et les leurs sont à portée de nos mains...

--- Leurs bras n'en sont que plus vaillants ! Ce ne sont pas des Turks qui se battent pour un butin, mais des citadins qui ont peur.

--- Il est vrai que chaque fois que la ville fut soumise, ils se sont rebellés, même contre leurs barons quand ceux-ci s'étaient livrés à nous. »

Garin savait ce que cela signifiait dans l'esprit du jeune sergent : soit les habitants se rendaient et fuyaient, soit ils seraient impitoyablement massacrés. Dans un cas comme dans l'autre, il pourrait explorer à loisir les lieux. Il sourit doucement à Fouques, acquiesçant à son alternative silencieuse.

### Région de Gaza, zone d'el Djifar, veillée du samedi 6 septembre 1158

Au milieu de l'oasis, au pied d'un palmier, Garin s'activait à détacher des dattes de leur branchette, les avalant pour tout dîner. Il gardait en permanence sur lui un petit sac de toile fine où il conservait des vivres de voyage. Autour de lui, tout était calme, les abris avaient été dressés pour la nuit et des feux parsemaient les environs.

Il sursauta quand il entendit qu'on l'appelait dans le camp voisin. Il avait été recruté comme caravanier et interprète par un modeste baron qui prenait part à une attaque dans les territoires frontaliers avec l'Égypte : un coup de main avec quelques centaines d'hommes. Garin espérait ainsi pouvoir traverser les zones désertiques et rejoindre la vallée du Nil dont on disait qu'elle était une merveille à voir.

Hugues, le chevalier à leur tête, était un robuste gaillard à la voix tonitruante, nasillarde à cause de son nez cassé. Débarqué depuis quelques mois en Terre sainte, il piaffait d'impatience de mener la charge contre les infidèles. Il avait levé une petite troupe pour l'accompagner dans son périple et ne connaissait rien du pays où il évoluait. Curieux et papillonnant comme un enfant, il était tout le temps en train de poser des questions.

Lorsque Garin se présenta à lui, il était assis sous l'auvent qui lui servait de tente et partageait un repas avec un des shaykh bédouins. Persuadé qu'il s'agissait là d'un des barons locaux, il s'efforçait de faire bonne figure face à leurs coutumes. Il avait très vite compris que c'étaient, comme lui, des hommes de chevaux et appréciait leurs poésies qui en parlaient. Il demandait donc souvent à ce qu'on les lui traduise. Mais pour le moment, il voulait en apprendre plus sur les nouvelles des éclaireurs. Le vieux Bédouin expliqua à Garin, en termes laconiques, ce qu'il en était.

« Ils ont vu une belle caravane qui avance en notre direction. Nous devrions pouvoir les surprendre d'ici deux ou trois jours, pensent-ils.

--- Ils ne nous attendent pas ? Et leurs espies ?

--- Le shaykh dit qu'il n'y a aucune autre tribu dans les parages. Ils doivent se contenter de leurs yeux. Et ce ne sont pas ceux de l'aigle. »

Le chevalier tonna d'un gloussement propre à effrayer un loup.

« Sont-ce des cavaliers ? Des archers ? De la piétaille ?

--- Il dit : une poignée de soldats des bords du Nil, mous comme de la glaise et craintifs comme des chatons. Ils fuiront comme le sable sous le vent dès qu'ils nous verront, abandonnant leurs bagages et leurs vivres. »

Hugues opina. Il avait entendu parler de ces païens qui faisaient demi-tour dès qu'ils apercevaient une charge de bonnes lances de frêne. Il espérait néanmoins que quelques courageux oseraient lui tenir tête, histoire qu'il puisse briser une hampe ou deux. Il n'était pas contre le butin, mais il portait la croix du Christ sur la poitrine afin de combattre en son nom. Manquer à verser le sang à cette occasion lui semblait presque une insulte à la face de Dieu.

Garin passa une bonne partie de la veillée à servir de truchement au chevalier, dont l'enthousiasme était partagé par le Bédouin qui s'amusait de l'aspect infantile de son hôte. Les étoiles scintillaient depuis longtemps dans le ciel quand il fut autorisé à aller se coucher.

Il déambula dans le camp jusqu'à trouver un espace où s'installer pour la nuit. Quelques soldats ronflaient déjà, tandis que les plus anxieux échangeaient à voix basse autour des braises des petits feux qui avait chauffé leur brouet. Il s'emmitoufla dans son vieux manteau élimé non loin d'eux, mais sans être trop près. Le matin du combat était toujours bruyant et affairé pour les guerriers. Il aimait les regarder s'activer, à distance, tout en finissant de se réveiller.

La lune était presque pleine et il la vit qui arborait un sourire tandis qu'elle veillait sur le soleil des hommes. Au contraire d'eux, elle savait certainement quel serait leur destin le lendemain. Il lui fit un clin d'œil et se tourna pour s'endormir.

### Désert d'al-Gifar, fin d'après-midi du vendredi 19 septembre 1158

En très peu de temps, l'attaque avait tourné au désastre et les Latins s'étaient rapidement dispersés, harcelés par des contingents d'archers montés qui bourdonnaient autour d'eux sans relâche. Lorsque les premiers fuyards étaient revenus vers les bagages, Garin avait décidé de tenter sa chance. Il avait ôté et jeté sa croix dans la poussière et s'était mis en marche tranquillement en direction des dunes où avait lieu l'affrontement.

Il avait avancé d'un bon pas, sans se dissimuler, la main sur la tête pour tenir son chapeau de paille, veillant à ne pas trop s'approcher des endroits où il voyait des unités évoluer. Il craignait surtout d'être pris pour un combattant en train de fuir et d'être abattu d'une flèche.

Il venait de rejoindre une piste moins meuble que la zone où il était jusque là quand il aperçut un petit contingent de cavaliers qui trottaient dans sa direction. Il lui était impossible de se cacher d'eux et il estima que ce serait le moment de tenter sa chance. Au moment où le soldat de tête, un émir armuré de soie, faisait stopper son superbe étalon alezan d'un geste sûr, il leva la main et les salua d'une voix allègre, appelant la bénédiction d'Allah sur eux.

L'officier lui lança un regard étonné, découvrant ses cheveux blonds et sa vêture latine. Sans lui laisser le temps de la réflexion, Garin s'inclina respectueusement et demanda dans son meilleur arabe s'ils pouvaient le sauver et le ramener en terre d'Islam.

« Qu'es-tu donc pour parler ainsi ? N'es-tu pas de ces infidèles que nous finissons de débander ?

--- Ils m'avaient forcé à les servir, c'est vrai, mais je ne suis pas l'un de ces polythéistes. Et je n'aspire qu'à fuir loin d'eux.

--- D'où es-tu ? Quel est ton nom ?

--- Ahmed al-Dimashqi. J'ai longtemps vécu par-delà le Bilad al-Sham[^28]...

--- Tu as le poil bien clair pour un Turk, et guère l'allure d'un Perse ! Es-tu en train de mentir ?

--- J'ai été pris enfant et élevé dans la religion par mon maître, qui me fit libre au jour où je me convertis. J'ai ensuite travaillé à ses côtés, dans les périples qu'il faisait pour ses affaires. J'ai été capturé voilà deux hivers, par ce chien de Badawil[^29] alors que je m'occupais de ses montures, vers Baniyas. »

L'Égyptien se détendit, mais ne semblait pas totalement convaincu.

« Tu as de la chance que ce soit jour de prière et que je n'aime pas condamner le voyageur sans raison. Tu vas nous accompagner et répondre à quelques questions. Tu auras sûrement grand désir de nous narrer tout ce que tu sais sur les troupes que nous avons vaincues.

--- C'est pour moi un honneur que de mettre mes pas dans les vôtres. Puis-je apprendre quel nom je dois louer auprès d'Allah pour cette bonne fortune ? »

Un des cavaliers répondit d'une voix rogue, faisant avancer son cheval d'un air autoritaire.

« C'est l'émir Muhammad ben Mukhtār Shams al Khilāfa qui t'offre l'aman, veille à t'en souvenir ! »

Garin hocha respectueusement la tête, prêt à suivre avec empressement ceux qui lui ouvraient les portes d'un nouveau monde. ❧

### Notes

Le titre de ce Qit'a m'a été inspiré par mes lectures adolescentes, quand je dévorai la série de bandes dessinées de science-fiction *Laureline et Valérian* de Pierre Christin et Jean-Claude Mézières. Un des tomes, *Sur les frontières*, parlait de ces lieux de transition, de connexion dans lesquels erraient ceux qui n'appartenaient plus à aucun monde. Je me suis dit que cela conviendrait parfaitement à Yarankash, tout en jouant sur la polysémie du terme « marche », entre son acception médiévale et celle d'aujourd'hui, de mise en mouvement.

Il y a bien sûr fort peu de documentation sur les marginaux, qui sont, par nature, exclus des récits habituels, si ce n'est pour en illustrer parfois les chroniques judiciaires. Mais j'avais envie de suivre le destin possible d'un de ces déracinés qu'on évoque à mi-mots dans certaines sources.

### Références

Christin Pierre, Mézières Jean-Claude, *Sur les frontières*, Tome 13 de la série *Valérian*, Paris : Dargaud, 1988.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Jackson David Edwards, Lyons Malcolm Cameron, *Saladin. The Politics of Holy War*, Cambridge : Cambridge University Press, Édition Canto, 1997.

La pierre et le puits
---------------------

### Jérusalem, abords de la rue des Arméniens, après-midi du youm al joumouia 23 jha'ban 492[^30]

Des appels, des bruits de course, des invectives résonnaient parfois dans la ruelle qui menait au petit quartier où résidait Sabiha. Un peu plus tôt, un gamin était passé en criant avec angoisse « Ils sont entrés ! Ils sont entrés ! ». Depuis, un calme relatif régnait. Sabiha n'avait que très rarement eu affaire à des Celtes, ces hommes venus d'au delà les mers. On racontait qu'ils étaient sales, bruyants et violents, barbares et cannibales. Depuis plusieurs saisons, les récits de leurs assauts, de leurs pillages émaillaient toutes les discussions. Et si beaucoup pensaient au départ qu'ils n'étaient qu'un fléau de plus parmi les catastrophes usuelles, la plupart s'effrayaient désormais de les voir approcher depuis les territoires septentrionaux.

Habituée à se débrouiller par elle-même, Sabiha avait prévu de larges réserves de nourriture. Au moins pouvait-elle tenir encore plusieurs jours sans ouvrir sa porte, puisant l'eau dans sa petite citerne. Elle possédait même une chèvre qui lui donnait du lait pour son enfant né quelques mois plus tôt. Elle avait rassemblé ses biens les plus précieux, tissus, céramiques et bijoux dans sa chambre et passait le temps à de menus travaux de couture. Elle regrettait juste de n'avoir pu garder avec elle la vieille Safwah. Sa servante avait demandé à fuir plusieurs semaines auparavant, espérant rejoindre le village où un de ses fils était installé. Elle estimait que la campagne serait un plus sûr refuge.

Sabiha avait préféré se fier à la solidité des murailles de la cité. Elle avait mis des années à gagner sa place ici et n'avait nulle part où aller. Ses protecteurs, en particulier le riche Yusuf al-Yama al-Bandaniji, habitaient tous de Jérusalem. Et si elle arrivait à se faire respecter de son voisinage, elle ne se faisait aucune illusion sur le sort qui lui serait réservé si elle partait à l'aventure.

Elle sursauta, se piqua le doigt quand un fracas résonna au bout de la rue : des coups puissants et répétés, certainement sur la grille d'accès. Un temps de silence, puis le vacarme revint, cris et appels, imprécations et hurlements. Les Celtes étaient parvenus à entrer, ils échangeaient bruyamment, peut-être avec le vieil al-Jayhani, le portier dont la voix portait si loin. Terrifié, il implorait leur pitié. Étaient-ils en train de le torturer ? L'avaient-ils frappé de leurs lames ? Parmi le brouhaha, Sabiha crut entendre une autre voix compréhensible. Elle se rapprocha de la fenêtre partiellement occultée de tressage de bois, non sans un regard vers son enfant, tranquillement étendu sur la couche. Il dormait à poings fermés.

Elle aperçut des Latins qui secouaient al-Jayhani. Ils arboraient des tenues de guerre, casque sur la tête et lame ou lance en main. Elle vit aussi un combattant musulman avec eux, le visage tuméfié, ensanglanté et les poignets ligotés. C'était lui qui parlait, échangeant laborieusement avec les soldats autour de lui : une dizaine d'hommes, occupés à frapper sur les portes. On leur intimait à tous de sortir, en présentant les armes qu'ils pourraient détenir.

Elle hésita, son regard faisant des allers et retours entre la petite fenêtre à claustra et son enfant. Elle ne possédait aucune arme et craignait qu'on ne lui dérobe sa maigre fortune amassée au fil des ans. Elle souleva le bébé, glissa rapidement quelques-unes de ses plus belles étoffes sous son matelas. Elle s'enveloppa ensuite dans un large izār assez sobre, dont elle ramena un pan sur son visage en un geste coutumier. Puis elle descendit l'escalier qui menait à la petite cour par laquelle on pénétrait chez elle. Elle quittait la dernière marche lorsque des coups redoublés firent frémir son huis. Malgré son courage, elle ne put retenir un frisson d'angoisse.

Elle s'approcha avec crainte, déverrouilla la serrure et repoussa la barre. Elle n'eut pas le temps de tirer la porte que celle-ci s'ouvrit, révélant un grand gaillard hirsute, sale comme un pou, barbu en diable, son épée brandie, rougie de sang. Il se figea un instant, brailla derrière lui tout en lui ordonnant, d'un geste, de ne pas bouger. Rapidement l'interprète musulman ligoté fut traîné vers eux et le Celte s'adressa à lui dans son dialecte. L'homme traduisit avec lenteur, peinant visiblement à bien comprendre ce qu'on lui demandait.

« Es-tu seule ? As-tu armes ou soldats cachés chez toi ? »

Elle fit non de la tête, rivant ses yeux sombres dans ceux du guerrier.

« Appartiens-tu à riche famille ? Quelqu'un souhaitera-t-il payer rançon pour toi ? »

Sabiha réfléchit un moment, s'efforçant à retenir l'effroi qui la gagnait. Elle n'eut besoin de guère de temps pour prendre sa décision. Après avoir libéré son visage des replis de toile, révélant son profil net et sa chevelure d'ébène dans un geste qu'elle espérait gracieux, elle tendit sa main libre vers le soldat. Elle se forçait à un sourire engageant qu'elle enrichit d'une œillade experte.

« Dis à cet homme que je suis sa servante s'il souhaite fouiller ma demeure. Il aura accès à tout s'il passe ma porte. »

Le prisonnier fronça les sourcils, lui adressa un regard courroucé. Impérieuse, elle lui intima d'un coup de menton de traduire. Pendant ce temps, le grand soldat latin ne la quittait pas des yeux. Il semblait impatient de comprendre ce qu'elle voulait. Quand il entendit, il rugit dans un sourire carnassier et lança une plaisanterie à la cantonade. Puis il poussa Sabiha sans ménagement en direction de sa maison. Il tenait toujours son épée à la main.

### Jérusalem, quartier de Malquisinat, matin du lundi 7 juillet 1113

« Dès ces quelques marchandises vendues, nous te ferons parvenir ce qui te revient. J'ai usage de passer par là plusieurs fois l'an. »

Tout en parlant au jeune homme, Shihab al-Khwarizmi, lointain parent de Sabiha finissait de sangler un petit sac sur la mule où il s'apprêtait à prendre place. Il n'avait découvert que par hasard l'existence de cette branche de la famille et avait saisi la chance d'être présent peu après le décès de sa cousine. D'autorité il s'était fait connaître et avait pris en charge le jeune homme indolent qui n'avait jusque là jamais quitté le giron de sa mère. Celle-ci portée en terre, il ne demeurait personne pour s'occuper du gamin et il errait en ville, dilapidant les quelques avoirs que Sabiha avait accumulés.

Shihab y mit bon ordre et s'empara de tout ce qui avait de la valeur, logeant son petit cousin dans une misérable mansarde. Il lui ouvrit également un compte dans plusieurs commerces de bouche. Il assurait être de retour très prochainement pour mieux installer son parent. Un temps le garçon avait craint qu'on ne l'oblige à quitter sa ville, mais jamais le sujet n'avait été abordé.

Après un dernier au revoir, il prit la direction d'un établissement réputé pour ses beignets. Au milieu des échoppes où cuisaient mille délices, il se laissait bercer par les saveurs grasses et caramélisées propagées par cette chaude journée d'été. Alors qu'il passait devant une petite taverne où se vendait une piquette vinaigrée que seuls les estomacs les plus solides appréciaient, il entendit un de ses compagnons le héler. C'était Josce Tête Folle, un gamin des rues à l'aspect ébouriffé, qui alternait entre comportement agressif, moqueries sans méchanceté et attitude protectrice envers lui. Il lui adressa un sourire en guise de salut et continua son chemin, mais Josce se leva, son gobelet de terre à la main.

« Viens donc partager lippée, compère !

--- Vous ne mangez que vin au matin, mère dit pas bon !

--- Eh bien, amène ce qui te sied ! »

Le jeune homme acquiesça et revint avec quelques pâtisseries croustillantes. Les doigts luisants d'huile, le petit groupe se régala alors avec avidité. Un des plus gourmands remercia plusieurs fois en avalant presque sans mâcher. À en juger par sa tenue élimée et son aspect rachitique, il ne se goûtait pas souvent de pareilles friandises.

« As-tu donc cliquailles plein les mains pour t'offrir si belles bouchées ?

--- Cousin venu arranger les affaires de mère, a payé pour moi, le temps pour lui de revenir.

--- Le grand gars qui te quittait pas ces jours derniers ? Je l'avais jamais vu par nos rues.

--- Je le pas connaître. Il voulait aider moi, maintenant que mère... »

Il n'acheva pas sa phrase et engloutit un beignet en silence, le visage soudain éteint. Josce lui asséna une tape amicale à l'épaule.

« Mange, sochon, ça remplit le ventre à défaut du cœur ! »

Puis le petit groupe continua son festin sans l'atmosphère d'allégresse qui s'était installée jusqu'alors.

Autour d'eux, des pèlerins, des visiteurs des autres régions, Samarie, Galilée, Antioche et Édesse, étaient présents pour commémorer la prise de la ville une quinzaine d'années plus tôt. Pourtant l'ambiance était morose : le royaume était assailli de toutes parts et nul grand seigneur ne s'était présenté dans les ports depuis des mois.

« Comment qu'y s'appelle, ton cousin, au fait ?

--- Shihab al-Laqit al-Khwarizmi[^31].

--- Avec un nom pareil, il est pas du coin.

--- Non, ici pour commerce. Lui aider. S'occuper de moi.

--- Tu vas nous quitter ? »

Les yeux effarés, le jeune homme secoua la tête.

« Oh non. Lui prendre biens de mère, les vendre puis revenir donner moi tout ce dont besoin.

--- Il t'a dit ça ?

--- Oui. Lui promis faire de moi grand marchand, comme lui. »

Le visage soudain suspicieux, Josce avala une longue rasade de vin. Puis il se leva et, sans un mot d'explication, alla faire un petit tour dans le quartier. À son retour, il paraissait fort mécontent. Il tapa sur l'épaule du jeune homme d'un geste autoritaire.

« Viens avec moi, qu'on te trouve un bon père pour t'aider. Ton cousin, il n'a guère versé pour ton manger. Rien que ces friands ont coûté la moitié de ton avoir ! »

Quand Josce disparut avec son infortuné compagnon, le petit groupe finit le vin et les beignets en commentant la stupidité du pauvre enfant. Couvé comme un coq en pâte par Sabiha depuis sa naissance, il était souvent sujet de moquerie de la part des plus féroces. Personne ne s'étonnait qu'il se soit fait voler à peine sa mère décédée.

### Jérusalem, quartier de l'hôpital Saint-Jean, matin du lundi 24 juin 1140

Comme chaque jour, l'ouverture des portes de la cité marquait le début véritable de l'activité artisanale et commerçante. Des files d'ânes, de chameaux, de mules déambulaient un peu partout, ondulant entre les groupes de portefaix et les valets chargés de paquets. Pour les établissements religieux, c'était aussi le moment où on attribuait des corvées aux volontaires venus quémander de la nourriture contre un peu de travail. Beaucoup de visages étaient familiers des lieux et c'était comme une grande famille de traîne-misère, qui échouait là, dans l'attente des distributions de vêtements l'hiver ou de tâches au frais l'été.

Le fils de Sabiha était parmi eux, sourire aux lèvres. Habillé d'un vieux thawb de couleur sale, sa barbe et ses cheveux avaient désormais la teinte de la poussière et des rides sillonnaient ses joues minces. Il conservait pourtant cette nonchalance d'enfant, s'émerveillant du soleil, des rencontres, du moindre bienfait dont il était le témoin, voire le destinataire.

Chartain, le valet de l'hôpital qui distribuait les corvées, l'avait au départ pris en grippe, estimant inutile de venir en aide à un mécréant qu'on ne voyait jamais à l'église. Les frères de Saint-Jean le rabrouèrent et lui interdirent de trier la faim d'un homme selon sa foi. Il lui avait alors attribué les travaux les plus salissants ou les plus pénibles, sans jamais obtenir autre chose qu'un consentement indifférent, voire enthousiaste. Chartain avait finalement admis ne voir aucune malice face à lui. Les années passant, il en était venu à considérer le mendiant comme un compagnon, qu'il saluait même comme un ami quand il le croisait en ville.

Au fil du temps, Chartain avait d'ailleurs découvert qu'il n'était pas le seul à s'être laissé gagné par l'affection inconditionnelle de ce misérable hère. Il rendait d'innombrables services contre une piécette, une obole pour un repas, ou, plus souvent encore, en échange d'un cadeau dérisoire. Il vouait un véritable culte à tout ce qui était verroterie et objets de décoration, aimait bricoler ceux qu'on lui donnait, les vendant ensuite à l'encan, à même le sol, sur les placettes.

Il semblait connaître tout le monde, mais saluait en fait tous ceux dont il croisait le regard, pèlerins nouvellement débarqués ou figures d'importance de la cité. Jamais il ne paraissait se lasser ou n'échappait une parole bien longue, ou une remarque désagréable.

Sachant le pauvre homme de toute confiance et peu difficile sur les tâches, Chartain le menait à la chapelle ardente où des dépouilles attendaient le dernier repos. Chaque jour, le grand hôpital accompagnait des croyants jusqu'à leur ultime demeure. Et comme certains demandaient à être ensevelis avec un bijou, un souvenir, une croix, le valet craignait toujours qu'on ne les vole avant de les mettre en terre.

Garin, le frère de Saint-Jean en charge ce jour-là bénit les défunts une nouvelle fois avant de les laisser partir. Chartain se rapprocha de lui, tout en se signant ostensiblement.

« Encore neuf ! Cela n'en finira donc jamais de c'te fièvre quarte !

--- Puissent nos prières apporter la paix à ceux-ci, mon fils. Le reste n'est pas en notre pouvoir. »

Chartain hocha la tête, tandis que frère Garin saluait les aides venus emporter les corps.

« Ce sont là toujours les mêmes, n'ont-ils aucune crainte de respirer les miasmes ?

--- Y disent que les morts n'ont plus de souffle, enserrés en leur linceul. Et que ça vaut bien le pain de l'abbaye...

--- J'ai chaque fois chaud réconfort à découvrir ainsi parmi indigentes-personnes de tels trésors de bonté. *Beati pauperes, quia vestrum est regnum Dei*[^32]. »

Chartain acquiesça d'un air las. Il aimait bien le frère hospitalier qui se dévouait chaque jour dans la grande salle des malades, mais il ne partageait guère ses vues optimistes sur l'humanité. Lui qui avait connu la vie en dehors de la clôture monastique n'y avait guère rencontré de pitié et de prévenance. En outre, il n'avait aucune notion de Latin et n'avait rien compris de la fin de la phrase.

« J'encrois surtout que certains, comme le vieux, là, y sont un peu simplets. Lui a bien tourné, par chance, mais y peuvent devenir vicieux si on n'y prend garde. Y se passe de drôles de choses en leur caboche.

--- Un des anciens ici, que tu n'as pas encontré, disait souventes fois que si le fou jette une pierre dans un puits, mille sages ne pourront l'en tirer. »

Le valet adopta un regard hébété qui fit naître un soupçon d'amusement sur le visage du moine.

« Ce que je veux dire, c'est que nul ne saurait dire quels sont les desseins de Dieu à travers ce simplet, mon fils. »

### Jérusalem, quartier de l'hôpital Saint-Jean, midi du samedi 16 février 1157

Arborant la croix de pèlerin sur leur épaule, Ernaut et Lambert étaient arrivés tout récemment à Jérusalem. Ils n'avaient pas pu résister à l'envie de voir au plus vite la sainte ville dès qu'ils avaient quitté le Falconus avant de faire un long périple vers les lieux de culte périphériques. Ils étaient de retour pour les Pâques, but de leur voyage et objet de leur vœu sacré de marcheur de Dieu. Encore fébriles de mettre leurs pas dans ceux du Christ, des apôtres et de martyrs, ils souriaient presque en permanence, s'émerveillant du moindre détail. Ernaut en particulier semblait intarissable d'admiration.

Ne connaissant personne, ils étaient venus s'adresser à l'hôpital de Saint-Jean pour trouver un hébergement. Lambert préférait loger en un lieu à eux où ils pourraient s'imprégner de l'atmosphère du pays avant de prendre une décision pour leur installation définitive. Ernaut avait vanté les collines de Samarie, où s'étageaient les merveilleux coteaux de vigne dont ils avaient goûté le fruit pressé. Lambert était plus réservé, comme toujours, et ne dévoilait que peu ses intentions. Il douchait régulièrement l'enthousiasme de son cadet en lui rappelant que leur trésor n'était pas infini et qu'il estimait plus raisonnable de rechercher les casaux où l'on faisait les meilleures conditions aux colons.

Ils patientaient en haut du grand escalier qui ouvrait sur le parvis du Saint-Sépulcre, battant la pierre de leurs fines semelles de savates usées. Le portier, un jeune Latin blond aux cheveux frisés, leur avait fait un temps la conversation, mais il avait fort à faire à surveiller les allées et venues. Il leur désigna néanmoins celui qu'ils attendaient. Il avait dit que c'était un guide de confiance qui leur permettrait de trouver un logement à leur goût et adapté à leur bourse.

L'homme, assez âgé, était habillé d'une vieille robe réparée en de nombreux endroits et s'était enveloppé les pieds de chiffons pour se protéger du froid. Il avait sur la tête une chappe de voyage élimée, fixée avec un turban mal ficelé. Autour de ses jambes frétillait un petit chien jaune au poil ras, la queue fendant l'air avec vigueur. Lambert avait espéré qu'on les dirigerait vers un colon ou un ancien pèlerin. Il retint donc de justesse une grimace de dépit quand il vit que le portier de l'hôpital saluait amicalement ce gueux à l'allure misérable. Un sourire sur le visage, leur guide monta à eux et s'inclina poliment, s'exprimant dans leur langue malgré une légère pointe d'accent.

« J'ai nom Saïd. Je peux montrer à vous bon abri, pas cher. J'ai grande joie à dévoiler cité de Dieu à marcheurs des pays lointains. » ❧

### Notes

Même si Hexagora est un projet de découverte de l'altérité de façon générale, à travers le temps et l'espace, j'ai eu aussi envie de creuser un peu le sujet des personnes à l'esprit foncièrement autre, qu'on a désigné de toutes sortes de termes, le plus souvent *folie*. Le rapport à cette partie de l'humanité n'a pas été constant au fil des siècles et si Michel Foucault en a rédigé une histoire à l'âge classique, elle demeure, à ma connaissance, à faire pour les périodes antérieures.

Quoi qu'il en soit, les institutions charitables orientales ou occidentales médiévales paraissent avoir eu une capacité d'accueil qui permettait de canaliser les cas les plus graves et les structures sociales traditionnelles arrivaient souvent à inclure les éléments les plus dociles. Malgré la prévention qu'on pouvait avoir envers les actes de ces personnes différentes du commun, on leur conservait une place au sein de la société, avec plus ou moins de facilité. L'ambivalence du dicton arabe que cite frère Garin me semble une excellente illustration de cette tension, qui pouvait se résoudre par un abandon à une transcendance.

### Références

Foucault Michel, *Histoire de la folie à l'âge classique*, Paris : Gallimard, 1972

Mon ennemi, mon frère
---------------------

### Jérusalem, mont du Temple, après-midi du mardi 14 avril 1142

La cité s'était brusquement dévoilée au sortir de la route qui courait dans les gorges des monts de Judée depuis la plaine côtière. Énorme, solidement bâtie, elle présentait aux nouveaux arrivants son entrée la plus imposante, celle où s'adossait la forteresse. En plus de cela, elle était précédée d'un faubourg grouillant de voyageurs et d'animaux, bruissant d'activité. Geoffroy, originaire d'un petit village de Bourgogne, avait traversé ces lieux comme dans un rêve. La porte rapidement passée grâce au blanc manteau qu'il portait avec ses compagnons, il avait découvert la longue rue, bordée d'échoppes animées dont les éventaires mangeaient l'espace, qui menait vers le Temple dont il apercevait le dôme entre deux auvents, parmi les avancées des terrasses et les rawshans[^33].

Lorsqu'il était enfin parvenu sur la vaste esplanade où battait le cœur de toute la chrétienté, où ses frères de la milice, porteurs de la cape blanche depuis quelques années, s'étaient installés, il avait poussé un soupir de soulagement, émerveillé du spectacle qui s'offrait à ses yeux. Autour de lui, ce n'étaient que lieux saints, prêtres et soldats de Dieu, bâtiments de la foi...

« Ne veux-tu pas descendre de ton roncin, frère ? »

La voix le ramena à la réalité. Un chevalier, habillé en civil, mais l'épée au côté, lui tenait la bride et attendait qu'il démonte.

« j'ai nom Armand, et on m'a donné pour mission de t'accueillir en nos murs.

--- Grand merci, frère. Je suis Geoffroy Foucher, de Bourgogne.

--- Oh, je sais fort bien qui tu es. Tu es espéré depuis long temps. Le sénéchal ne fait que deviser de toi, sien neveu, dont il apprécie tant les courriers. »

Geoffroy sourit en entendant parler d'André de Montbard, l'oncle qui l'avait convaincu de s'enrôler dans la milice du Temple. Pas tant par des discours recruteurs, mais bien plus par ses récits de vaillance, de don de soi et de guerre pour la vraie foi. Pour le jeune Geoffroy, il était l'idéal chevaleresque incarné. Bernard, l'abbé de Clairvaux qui avait tant fait pour la chrétienté, ne tarissait pas d'éloges sur ce cousin qui avait embrassé un état supérieur. Geoffroy et lui correspondaient depuis plusieurs années, et c'était André qui l'avait convaincu de demeurer dans ses terres, aux soins des cisterciens, le temps de devenir suffisamment lettré pour ne plus être considéré comme un esprit pesant par les clercs épris de culture. Il avait donc rongé son frein trois années, où il avait alterné pratiques militaires et tournois afin d'éprouver son bras et études littéraires et liturgiques pour aguerrir son intelligence. Ce ne fut qu'une fois prêt, au moment du départ pour Jérusalem qu'il avait prononcé ses vœux.

« Est-il visible ? Je n'ai guère eu l'occasion de le voir depuis si long temps !

--- Il n'est pas en nos murs ces jours, mais il saura d'ici peu que tu es parmi nous, pour sûr. »

Armand invita d'un geste Geoffroy à le suivre, laissant sa monture à des valets, dont une nuée était à l'ouvrage afin de décharger les bêtes de la caravane.

« Nous allons voir tout de suite le frère drapier, que tu aies de quoi te vêtir et dormir ce soir. Nous avons un peu de temps avant la prochaine heure. »

Il fit une grimace de dépit avant de poursuivre d'un ton chagrin.

« Tu arrives malheureusement peu après la pitance. Les Pâques sont encore loin, et nous avons pris la collation de jeûne.

--- Nous avons eu du pain et des olives en chemin.

--- Tant mieux ! Sans cela, l'attente aurait été longue jusqu'à demain. »

Tout en échangeant, Geoffroy découvrait une enfilade de salles, de couloirs, de patios et de cours. Partout des hommes s'affairaient, discutaient, s'entraînaient. La plupart étaient porteurs d'un manteau blanc comme le sien, mais de nombreux subalternes n'étaient vêtus que de bure, de toile grossière. Armand passait d'un endroit à l'autre, indifférent à l'agitation des maçons et charpentiers occupés à agrandir et renforcer le lieu, aux serviteurs omniprésents et aux chevaliers qui déambulaient en tous sens. Au contraire, le quartier général du Temple faisait l'effet d'une ruche à Geoffroy : la forteresse d'où ils allaient essaimer afin de répandre le message du Christ. Tout au long du parcours, un sourire béat ornait son jeune visage.

« Nous avons ici tout le nécessaire, bains et fours, cuisines, dortoirs et réfectoires, forges et moult loges d'artisans. Nul besoin de sortir dans la cité. Et, surtout, nous avons des écuries... Une merveille que nul sur terre n'a pu voir depuis l'époque des Romains, pour sûr. Je te montrerai ça. Mais avant cela, il nous faut aller à la quarravane.

--- Qu'est-ce donc ?

--- C'est un peu comme notre maître cellier, mais pour tout ce qui n'est pas viandes. C'est là-bas que les drapiers t'équiperont. Veille à ne pas fâcher frère Remondin. Sans quoi tu n'auras droit qu'à braies percées et lame rouillée... »

Devant l'air dépité de son compagnon, Armand partit d'un grand rire.

« Je te moque, pardonne-moi ! Nous n'avons que bon harnois ici. Mais il est de fait que le frère Remondin a caractère bien trempé, comme ses glaives. Veille bien à ne jamais le contrarier ou tes oreilles pourraient bien chauffer. »

Geoffroy sourit à cette innocente plaisanterie. Il avait le cœur si léger que rien ne pouvait le rembrunir ce jour. Il était enfin parvenu à la destination de ses rêves et chevaucherait d'ici peu, lance au poing et casque en chef, à l'assaut des forces ennemies. Il sentait son sang bouillonner dans son jeune corps empli de vigueur.

### Faubourg damascène, camp du Temple, matin du mardi 27 juillet 1148

La réunion d'état-major était à peine achevée que les ordres avaient fusé : tout devait être démonté et chargé sur les bêtes au plus vite. La décision de lever le siège de la ville était prise et il ne fallait pas laisser à l'ennemi le temps de s'organiser pour harceler la colonne au départ. Des échelles de cavalerie allaient se mettre en place, appuyées de quelques fantassins, afin de patrouiller aux abords et éviter que des actions de guérilla ne viennent transformer la retraite en déroute.

Geoffroy n'avait pas été désigné pour le combat en selle, ce qu'il avait perçu comme une rebuffade personnelle, en plus de l'humiliant repli. La mine basse et le regard enflammé de courroux, il accompagnait son oncle jusqu'à sa tente afin de l'assister dans la rédaction de plusieurs courriers qui devaient partir sans délai.

Grisonnant depuis des années, le sénéchal du Temple André de Montbard était solidement bâti, la mâchoire large et les pommettes saillantes, le cheveu court et la barbe maintenue rase. Ses yeux rieurs étaient enfoncés sous des arcades proéminentes où jouaient ses sourcils mobiles. La bouche fine, comme une fente discrète, ne s'ouvrait que peu pour autre chose que le service, de Dieu ou des hommes.

« Je te vois bien en rage, neveu. Livre-moi donc ce qui te ronge le cœur. »

Geoffroy hésita un instant. Il avait énormément d'affection pour cet homme qui l'avait pris sous son aile depuis des années. Inlassable, à l'occasion autoritaire, il était malgré tout généralement bienveillant et ne s'emportait guère, même si son regard en disait parfois long sur les sentiments qui l'habitaient. Toujours sa voix demeurait égale et ses mots choisis. Geoffroy s'efforça de se montrer à l'image de son modèle et retint la bile qui lui brulait les entrailles.

« Ne pouvions-nous refuser ce honteux repli ?

--- Les rois ont décidé avec leurs conseils. Toute notre vaillance ne saurait suffire à abattre ces murailles seuls.

--- Nous avions pourtant immense troupe, le plus bel ost assemblé en ces lieux depuis les illustres souverains de nos pères.

--- Tu parles vrai. Mais force soudards implique aussi grands appétits de barons. La tourte avait été coupée avant de l'enfourner, ce qui ne fait pas bonne pitance. »

Geoffroy savait que son oncle désapprouvait la répartition qui avait été faite des terres et de Damas, alors que la campagne n'avait même pas commencé. Selon lui, cela brisait la vigueur des jaloux mis de côté et rendait cupides les chanceux, peu désireux de démolir des fortifications qu'ils auraient ensuite à rebâtir. S'il appréciait la vaillance et l'engagement de Thierry d'Alsace, le comte de Flandre, il n'en aimait pas les ambitions parfois mesquines qu'il estimait contraires aux intérêts du royaume.

Ils pénétrèrent dans le petit pavillon qui servait de bureau et de chambre au sénéchal. Un valet s'y employait à ranger dans un coffre les objets usuels, sans s'approcher du plateau où étaient disposés les matériels d'écriture, les sacs d'archives et de correspondance. André de Montbard s'installa dans sa chaise curule et fit un signe à Geoffroy pour qu'il prenne note.

« De prime, j'aimerais discuter avec le sire Vacher, porte-bannière Baudoin. Il aura peut-être utiles nouvelles à propos de l'arrivée du sire d'Alep[^34]. »

Voyant Geoffroy froncer les sourcils, le sénéchal expliqua son idée.

« C'est de certes le plus informé et sage des hommes du roi. Il a usage de s'entretenir avec les mahométans. Je n'accorde nulle foi aux calomnies qui circulent sur lui, je le connais assez pour n'avoir aucun doute sur sa droiture. »

Il marqua une pause et esquissa un éphémère sourire, d'où la gaieté était absente.

« D'ailleurs, il serait bon que tu t'efforces de mieux le connaître, toi aussi. Il saura te donner les clefs qui te manquent parfois pour comprendre ce qui se passe en nos provinces. »

Il se tut, joua un instant avec une plume oubliée sur son bureau.

« Ce jour, mon neveu, il est temps pour toi d'apprendre à voir plus loin, plus large, plus grand. Tu disais tout à l'heure que nous faisions honteux repli. Il n'en est rien... Mais certaines batailles ne se gagnent pas que le fer en main. »

Puis il expliqua succinctement à Geoffroy que des échanges de courriers avaient eu lieu avec le seigneur de Damas, Mu'īn ad-dīn Anur. Celui-ci, qui jouait ses adversaires les uns contre les autres afin de maintenir son indépendance, avait démontré que si les Latins s'entêtaient à assiéger sa cité, il se verrait contraint de se livrer à Nūr ad-Dīn, le nouvel homme fort du nord de la Syrie. Et l'union d'Alep et de Damas serait une catastrophe pour l'équilibre des puissances en présence. Faute de ne pas s'être emparés de la ville rapidement, les croisés devaient se résoudre à quitter la place pour ne pas empirer la situation à leurs frontières.

« Ce mouvement de retrait que tu déplores vaut bien des centaines de charges lance au poing, mon neveu. À la vaillance du soldat et à la pureté du moine, il est parfois roué d'ajouter le bon sens du paysan qui sait quand semer et quand récolter. »

### Wadi Arabah, camp de voyage, soir du mardi 10 août 1154

Sur les conseils des bédouins qui les guidaient, la modeste troupe de chevaliers du Temple s'était installée au pied d'une falaise, dans un ressaut qui apportait un peu d'ombre. Ils n'avaient eu que peu de temps pour mettre en place le bivouac avant que la nuit ne tombe. Ils avaient rapidement dessellé et brossé les montures, avaient nourri les bêtes. Un feu chiche crépitait désormais au milieu de leur petit cercle, tandis qu'ils avalaient leur repas du soir, poisson séché, pain et bière.

Un peu à l'écart, près de leurs chameaux, les bédouins discutaient d'une voix forte, certains chantaient de temps à autre, tandis que les étoiles piquaient d'éclats diamantés le ciel de velours. Les porteurs du manteau blanc demeuraient silencieux, habitués à manger sans proférer une parole, utilisant parfois quelques signes de la main ainsi qu'il était d'usage dans les monastères. Ils savaient qu'un temps d'échange était prévu une fois le repas terminé, où il était même permis de jouer ou de faire de la musique sans s'attirer de regards sentencieux.

Geoffroy commandait cette petite unité, qui acheminait des messages à Ayla[^35], patrouillant loin dans le sud, vers le seul port tenu sur la mer Rouge par le royaume de Jérusalem. Il s'était porté volontaire afin de mieux connaître cette région. Depuis quelques semaines, il ressassait l'insistance de la reine Mélisende à parler des zones méridionales. Depuis qu'il était arrivé et ses premiers entretiens avec elle, grâce aux courriers de recommandation envoyés par l'abbé Bernard de Clairvaux, il avait la chance de la rencontrer de temps à autre. Il avait toujours été impressionné par l'intelligence de cette femme de pouvoir. Malgré une apparence fragile, qui s'accentuait au fil des ans, il n'avait qu'exceptionnellement découvert autant de force et de détermination dans une personne, y compris chez ses frères du Temple.

Récemment, il avait été frappé par son insistance à étudier ce qui se passait dans les zones méridionales, alors que leurs principaux ennemis étaient au nord et à l'est. Il avait donc eu envie de voir ces régions de ses yeux. Il n'avait eu jusque là que rarement l'occasion de chevaucher si loin dans le désert.

C'était la première fois que Gaufroi Morisse, son nouvel écuyer, prenait la route et il admirait tout avec des yeux ronds. Sa candeur et son élan juvénile amusaient Geoffroy, même s'il craignait aussi que cela ne dénote un esprit mal discipliné. Il estimait de toute façon que des sorties en pleine nature, loin du couvent, constituaient le moment idéal pour éprouver une âme et se faire une idée du caractère d'un homme. Surtout dans des circonstances pareilles, où ils n'étaient qu'une poignée, isolés au milieu de la rocaille, des acacias et des gazelles.

Sachant que la troupe attendait de lui qu'il donne le signal de début de soirée, il brisa le silence en s'adressant à son écuyer.

« Alors, Gaufroi, trouves-tu le voyage à ton goût ?

--- C'est... incroyable, mon sire Geoffroy ! Ici, tout n'est que poussière et roches. N'y a-t-il aucune bête, aucune plante ? Est-ce là un endroit oublié de Dieu ?

--- Que non pas. Il te faut apprendre à voir le lézard qui se tapit dans l'ombre, l'antilope qui se dissimule dans les failles. Et surtout, le lion, qui fait la sieste dans un bosquet.

--- Mais où trouvent-ils de l'eau ? Nous n'avons croisé nulle source depuis deux jours.

--- C'est l'été, les rivières s'assèchent, et les mares se cachent. Mais les bêtes, et les bédouins, savent où regarder. »

Le jeune écuyer hocha la tête sans ajouter un mot. Geoffroy l'envoya chercher un portfolio de cuir où il gardait du papier, ainsi qu'une écritoire. Il avait vite remplacé ses tablettes de cire par un support plus résistant à la chaleur quand il était arrivé dans le royaume. Depuis, il conservait tous les documents de peu d'importance pour pouvoir y griffonner des remarques dans les moindres marges. Bien qu'ayant une excellente mémoire, il trouvait utile de coucher par écrit des annotations pour pouvoir organiser ses pensées.

Et là, il s'était fixé pour tâche de relever tout ce qui pouvait être d'un quelconque usage militaire dans la zone. Si jamais la guerre devait se déplacer vers le sud, ces lieux seraient l'unique point de passage possible pour les armées de Nūr ad-Dīn en direction de l'Égypte. Ce démon d'homme leur avait soufflé Damas quelques années plus tôt, et Geoffroy était désormais convaincu que c'était en partie dû à leur mauvaise préparation. Il comptait bien fournir au Temple suffisamment d'éléments pour ne pas laisser s'épanouir une nouvelle union, qui placerait le royaume au sein de gigantesques tenailles qu'une main ferme n'aurait que peu de peine à serrer.

À la pensée que cela pourrait signifier pour l'avenir de Jérusalem, il se signa silencieusement. Puis il se mit en devoir de noter ses souvenirs du jour de son écriture minuscule.

### Jaffa, maison du Temple, après-midi du mardi 14 octobre 1158

Un vent du large apportait de la fraîcheur et de l'humidité dans la petite cour qui avait été aménagée en succédané de cloître au centre des bâtiments conventuels de l'ordre. Installé dans une des salles ouvrant sur la galerie, Geoffroy Foucher était de passage pour quelques jours. Il s'employait à un recensement des matériels et combattants capables de participer à des affrontements navals alors que la plupart des des hommes se préparaient à partir vers le nord. Ils devaient se joindre à l'armée latine qui accueillerait le basileus byzantin Manuel Comnène avec lequel il était prévu de lancer ensuite une vaste offensive dans les territoires aleppins.

Malgré cela, Geoffroy s'employait toujours à réfléchir à la façon dont des opérations militaires d'ampleur pourraient être déployées dans la direction exactement opposée. Certains de ses frères en étaient venus à se moquer de cette lubie, parfois sans aménité, sans que cela n'entame sa détermination. Du reste, il avait été soutenu en ces travaux par son oncle, quand il avait accédé à la magistrature suprême de l'ordre après le siège d'Ascalon et la mort désastreuse de l'éphémère Bernard de Tramelay.

Le décès d'André, auquel avait succédé Bertrand de Blanquefort, n'avait rien changé à ces habitudes. Le nouveau maître, homme de guerre, mais aussi ouvert à la réflexion d'ampleur, lui avait confirmé cette mission d'analyse et d'étude. Geoffroy regrettait de ne plus autant chevaucher que par le passé, mais il estimait n'être plus en âge de s'amuser à chasser le lion, et s'ennuyait à escorter les pèlerins sur des chemins désormais bien balisés et protégés. Il avait même commencé à tisser un réseau d'informateurs venus d'Alexandrie, voire du Caire. Voyageurs et négociants qui s'y rendaient ou en venaient se voyaient régulièrement convoqués pour lui en apprendre plus sur le territoire voisin. Geoffroy se sentait parfois handicapé de ne pas savoir s'exprimer en arabe, à l'image de Bernard Vacher, dont l'absence se faisait si cruelle.

Il était dans sa petite cellule, à compulser des listes de marins et de sergents, d'équipements nautiques et de nolissement de navires, quand on frappa à sa porte. Sans qu'il n'ait le temps de répondre, une haute silhouette s'avança, emmitouflée dans une chape de voyage coupée dans une magnifique laine anglaise. Ce n'était certes pas un frère qui aurait pu s'autoriser une telle débauche de luxe, mais, pour être ami et proche du temple, Philippe de Milly n'était pas un profès. Sa vaillance et sa réputation le désignaient néanmoins comme un modèle naturel pour de nombreux chevaliers. Geoffroy reconnut immédiatement le seigneur de Naplouse et l'accueillit avec chaleur. Il l'avait croisé de temps à autre, tous les deux orbitaient dans les mêmes cercles, mais jamais ils n'avaient eu l'occasion d'échanger plus de quelques mots convenus.

D'autorité, le baron s'empara d'un escabeau et se laissa tomber dessus, invitant d'un geste le frère du Temple à demeurer assis. Son relatif mépris des marques ostentatoires de respect lui valait aussi une excellente réputation parmi les hommes de guerre. Il considérait volontiers les gens vaillants comme des égaux, sans attacher d'importance à son titre et son rôle éminent au sein du Haut Conseil.

« Je suis bien aise de vous encontrer alors même que je vais devoir quitter nos provinces de longs mois, avec ce qui se prépare à Antioche !

--- Vous compaignez Baudoin ?

--- Pas complètement. Disons que j'ai moindre presse que lui à me livrer au roi des Griffons.

--- Quelque mésentente avec le basileus Manuel ? »

Philippe de Naplouse retint un sourire. Le templier faisait usage de la bonne titulature, signe de sa connaissance du sujet. Un bon point pour lui, nota-t-il en silence. Il balaya la question de la main.

« Si j'encrois les discussions, vous-mêmes n'accordez que peu d'importance à toutes ces affaires au septentrion, d'aucune façon. On dit de vous à la Haute Cour que vous arpentez les chemins du sud comme un pèlerin en quête du Salut.

--- On parle donc de moi parmi barons ? s'amusa Geoffroy.

--- De cela et de bien d'autres choses. On n'arrive pas ici recommandé par le grand Bernard lui-même sans attirer à soi les yeux et les oreilles. Mais si c'est bien vrai, j'encrois que vous devriez faire connoissance de Guy, mon frère sénéchal le comte de Jaffa, Amaury.

--- Je ne savais pas qu'il était de votre fratrie. Je l'ai vu, à l'occasion, au palais. Ainsi que chez la reine. »

Philippe acquiesça. Il appréciait le fait que Geoffroy parle de Mélisende comme si elle était encore la seule et unique femme à porter ce titre. Pourtant elle avait abdiqué de toutes ses fonctions de gouvernement des années plus tôt. Et le jeune Baudoin avait désormais une épouse, la princesse byzantine Théodora, la propre nièce du basileus.

« Disons que mon frère assaille la muraille d'Amaury aux côtés de Mélisende, pour le convaincre de l'importance du Caire. Ainsi que vous le faites auprès des vôtres.

--- Certes. Et il serait de plus de profit encore d'en convaincre le roi lui-même.

--- Pour le moment, nul de nous n'a son oreille, toute tournée vers le nord, la Cilicie, l'Oronte et l'Euphrate. Mais si Amaury nous rejoint, si la milice lui apporte son soutien, nous pouvons peut-être infléchir les choses.

--- Avec notre sire maistre par la grâce de Dieu dans les geôles mahométanes[^36], nous ne pouvons guère prendre pareilles décisions.

--- De cela Dieu tranchera : s'il doit recouvrer la liberté, et le roi s'emploie à cela, ou pas. À défaut, celui qui le remplacera pour tenir le bâton de commandement saura peut-être entendre nos arguments. Il nous faut juste nous montrer prêts. »

Geoffroy hocha la tête et embrassa d'un mouvement de bras tous les documents empilés sur sa table de travail.

« Nous ne répéterons pas l'erreur de Damas, voilà chose assurée ! » ❧

### Notes

Il est rare d'avoir des renseignements sur les personnages de second plan et le templier Geoffroy Foucher figure parmi ceux-ci. On sait qu'il devint avec le temps un diplomate reconnu, envoyé négocier lors de difficiles conciliabules. Par ailleurs, on connaît grossièrement ses origines, étant lié par sa famille aux fondateurs de l'ordre où il fera carrière, jusqu'à atteindre un rôle d'importance.

Je me suis alors demandé, tout en cherchant à fournir une plausible réponse à la problématique, comment un tel homme, élevé dans une ambiance dévote voire zélote, avait pu, au fil des ans, se montrer si mesuré qu'on ne craignait pas de lui confier des missions où la retenue et la subtilité constituaient des clefs essentielles de réussite, autant qu'une nécessaire relative connaissance de ses interlocuteurs. J'ai donc eu envie de tracer une ligne générale, d'esquisser un arc narratif assez large, sur lequel je pourrai bâtir certaines intrigues à venir dans l'univers d'Hexagora. Car, comme les férus d'histoire le savent déjà, la décennie 1160 sera entièrement tournée vers l'Égypte et sa conquête.

### Références

Burgtof Jochen, *The Central Convent of Hospitallers and Templars. History, Organization, and Personnel (1099/1120-1310)*, History of Warfare, Vol. 50, Leiden - Boston, Brill, 2008.

Forey A. J., « Novitiate and Instruction in the Military Orders during the Twelfth and Thirteenth Centuries », dans *Speculum*, volume 61, numéro 1, 1986, p. 1-17.

Bottazi Marialuisa, « La lettre de Saint Bernard à la reine Mélisende », dans *La lettre-miroir dans l'Occident latin et vernaculaire du V au XV s.*, sous la direction de D. Demartini, S. Shimahara et Ch. Veyrard-Cosme, Paris : Institut d'Études Augustiniennes, 2018, p. 67-80.

Valeureux prince
----------------

### Taverne du père Josce, veillée du jeudi 19 décembre 1157

Emplie de monde, la pièce au pilier résonnait des conversations murmurées, des dés lancés avec espoir, des interjections graveleuses, des rires avinés ou des appels insultants. On y croisait plus de sergents que partout ailleurs en ville, la soldatesque aimant à s'y retrouver. Grâce à la foule qui s'y entassait, l'endroit était chaleureux malgré les frimas. On n'y voyait guère de pèlerins et de voyageurs, l'aspect décati des lieux n'incitant guère à y entrer. La qualité y était correcte, les prix modestes et, parfois, les filles accueillantes.

En cette période de fêtes hivernales, la taverne offrait l'occasion de se réchauffer le cœur et le corps à bon compte. Sueur, boissons et plats apportés depuis Malquisinat proche se mêlaient à la fragrance de paille humide suintant du sol et de salpêtre des murs.

Le père Josce lui-même ne faisait commerce que de vin, mais il laissait entrer librement les servantes des bains peu éloignés. De temps à autre, il acceptait aussi d'accueillir des jongleurs ou des musiciens, pour animer l'endroit. On le disait très vieux, eu égard à son absence quasi totale de cheveux, ses rides profondes et son dos voûté qui le faisait ressembler à une tortue. De plus, un mauvais œil faisait qu'il regardait toujours de côté comme un animal intrigué. Mais son ouïe était excellente, ainsi que sa mémoire, et il laissait les naïfs s'amuser de son apparence inoffensive sans s'émouvoir le moins du monde. On parlait souvent de l'Allemand châtré pour s'être mal conduit, sans savoir si c'était une vraie histoire. À chaque évocation, ceux qui la racontaient assuraient la tenir d'un témoin direct.

Sous une fine fenêtre, bouchée d'un volet coincé de paille, Ernaut discutait avec ses collègues de leur journée. Ils avaient appris depuis peu l'abandon du siège de Césarée[^37] par les armées royales. Les raisons de cet échec faisaient l'objet de commentaires enflammés et partiaux, parfois aussi mal informés que peu avisés.

« Je ne vois pas en quoi le sire comte a démérité, 'scuse moi, rétorqua à Ernaut un sergent au visage creusé, le nez comme un soc.

--- Il est plus fier qu'étron de pape, Robert de Flandre ! Refuser hommage à prince, pour qui se prend-il ?

--- Bein pour un grand sire comte, c'est bien le moins ! »

Ernaut manqua s'étouffer devant la provocation.

« Il n'y a pas ahonterie à jurer sa foi à prince d'Antioche quand on est simple comte !

--- Bein quand ledit prince est cadet de famille chez lui, ça doit rendre les genoux moins souples...

--- Il a refusé de faire hommage au petit prince[^38], aussi. C'est bien là vanité ! »

Droart, affalé contre le mur, ne prenait pas part à la conversation. Il savait qu'Ernaut s'était mis à entretenir une sorte de vénération pour le prince d'Antioche Renaud de Châtillon. Il se demandait si c'était lié au fait que tous les deux étaient Bourguignons ou à l'espoir qu'Ernaut caressait de connaître le même parcours. Renaud de Châtillon était un petit chevalier au service du roi Baudoin quand l'annonce de son mariage avec la veuve de Raymond d'Antioche avait surpris tout le monde. On le disait vaillant et bien de sa personne et il incarnait le rêve de beaucoup : seigneur d'un vaste territoire par ses mérites et son amour, habile à manier la lance et jamais en reste pour aller poindre des deux. Par contre, on le dépeignait également comme irascible, coléreux et violent. Un jeune bachelier convaincu que le monde n'était qu'un verger sans fin où cueillir les fruits qu'il dévorait à belles dents.

« Vu l'importance de la ville, le prince Renaud aurait pu accepter que le sire Thierry jure sa foi au roi, non ? Ainsi ils auraient pris la cité.

--- Césaire dépendait d'Antioche dans les temps anciens, pourquoi rompre coutume pour caprice de baron ? Ce n'est pas l'usage ! N'en plaise au comte !

--- Souventes fois choses varient, Ernaut.

--- Doit-on ployer le genou parce que c'est désir de puissant ? Renaud est preux chevalier tel qu'il en faudrait quelques échelles[^39] pour le royaume. Les mahométans auraient tôt fait de se convertir ou de disparaître. »

Il conclut sa diatribe d'une longue rasade avant de claquer son verre sur la table, puis se leva.

« Moi je dis que le sire prince Renaud est homme de bien, on le rabaisse, car il n'est pas fils de comte. Sa vaillance est son droit ! »

Il jeta quelques piécettes de cuivre pour son écot de vin et s'en fut, enfilant sa chape tout en sortant. Droart sourit et se pencha vers son collègue, mi-figue, mi-raisin :

« Faut pas le chercher sur le prince d'Antioche. Renaud, c'est son héros ! »

### Palais royal, soir du vendredi 17 janvier 1158

La senteur de bois brûlé des braseros se mêlait à l'odeur de soupe et de sauce. Les cuillers claquaient dans les écuelles et on entendait les cruches heurter les gobelets dans le brouhaha des conversations. Des marmitons allaient et venaient, les bras chargés de pains et de plats, bourdonnant autour des serviteurs occupés à se restaurer. La haute table, sur l'estrade était vide, aucun hôte de marque n'étant présent. Le roi Baudoin guerroyait au nord, la plupart des gens de son hôtel l'avaient suivi. Et le vicomte Arnulf ne s'était pas montré ce soir. Malgré tout, il demeurait une foule de domestiques à sustenter : sergents de ville et hommes d'armes, voyageurs, invités et messagers de moindre rang. Même lorsque ce n'était pas fête, Baudoin nourrissait amplement sa maisonnée, comme il seyait à tout baron digne de ce nom.

En l'absence des officiers, on s'assemblait un peu selon ses affinités et Ernaut avait pris place près d'un cavalier arrivé dans la journée. Son turban étrangement noué, sa tenue et son visage indiquaient clairement qu'il n'était pas latin. Mais il n'était pas Syrien ou Turc pour autant et il aurait pour sûr accueilli comme une insulte qu'on l'appela Grec. Karpis était un Arménien, un montagnard farouche au geste sûr et au verbe haut. Il avait fait voyage depuis Harenc[^40] comme escorte d'un messager. Il décrivait le siège qui se poursuivait, malgré les difficiles conditions hivernales.

Tout en parlant, l'Arménien engloutissait tout ce qui lui était présenté. Il avait confié ne pas avoir si bien mangé depuis des semaines.

« Voilà long temps que tu t'es juré au service du prince ?

--- Je l'ai fait au retour de Chypre. Jusque là, je ne servais aucun maître. Mais j'ai su en le voyant que c'était grand homme. »

Ernaut se redressa, arborant un large sourire. Il s'était rapproché de Karpis dans l'espoir d'en apprendre plus sur Renaud et il découvrait avec plaisir qu'il n'était pas le seul à l'admirer.

« Est-il si preux qu'on le dit ?

--- Plus que ça ! Bien des hommes n'ont pas quart de sa fougue. Il fallait le voir, sur l'île, chasser les Griffons comme vulgaires poulets. Son épée tranchait les têtes comme on moissonne les blés.

--- J'aurais grand plaisir à l'encontrer !

--- Il emploie volontiers qui a désir de le bien servir. Et toi, avec ta stature d'ours des montagnes, tu serais accueilli à bras ouverts dans l'hôtel de n'importe quel baron. »

Celui qui disait ça était pourtant solidement charpenté, les membres épais et les mains puissantes, carrées, aux doigts noueux. La tête semblait émerger directement des larges épaules et sa tenue se tendait à chaque geste malgré l'aisance de la toile. Il avait un physique de lutteur de foire, sauf qu'il ne le mettait pas en avant. Ce qu'on voyait, c'était ses deux yeux bruns moqueurs, qui brillaient sous des sourcils broussailleux, aussi noirs que sa barbe raide.

« Je suis juré au roi pour le moment, mais j'ai espoir de ne pas demeurer ici, en la cité, et de le servir au-dehors, l'arme en main. Avec un peu de chance... »

L'Arménien avala un morceau de pain et lui sourit.

« Crois-m'en, il y a toujours bonne moisson à faire lorsque les princes font bataille. »

### Environs de la rue des Tanneurs, veillée du jeudi 30 janvier 1158

Depuis la maison voisine, l'ombre se faufila sans un bruit en quelques bonds agiles et discrets. Longeant la terrasse, elle se laissa couler souplement jusqu'à un encadrement de fenêtre avant d'atterrir mollement au sol. Elle se figea un instant, scrutant l'obscurité dans l'attente d'un éventuel danger. La petite cour était déserte, mais des voix filtraient depuis un volet entrebâillé. De la lumière en sortait également, ainsi qu'un effluve de nourriture. De viande, même. Intriguée, la silhouette bondit sur le rebord et risqua un œil vers l'intérieur. Elle y découvrit plusieurs personnes, les familiers de l'endroit, mais aussi le géant aux gestes amples qu'elle croisait de temps à autre. Agréable odeur que la sienne, sans compter les caresses qu'il prodiguait. Elle lui sauta donc dessus, dans l'espoir de s'en attirer quelques-unes.

« Prince ! » hurla un des gamins d'Eudes, tandis que le chat venait se frotter à Ernaut. Tiphaine, la femme d'Eudes, allait le chasser d'un coup de torchon, mais elle se retint, voyant que son invité semblait apprécier l'animal. Il ne perdait néanmoins rien pour attendre, ce petit chapardeur.

« C'est ta bête ? demanda Ernaut à Perrin, le cadet qui avait crié.

--- Nan, il est juste souventes fois dans le coin.

--- C'est moi qu'ai trouvé son nom, indiqua Rufus, l'aîné âgé de six ans. Il a comme couronne sur le chef, ajouta-t-il en riant et en se tortillant sur le banc

--- Je vois ça ! » confirma Ernaut.

Il flatta un peu l'animal qui se mit à ronronner puis le posa sur ses genoux tandis qu'Eudes revenait avec une cruche de bière légère. À table, il préférait ça au vin et en servit une généreuse rasade à son invité.

« Grand merci, Eudes. J'ai fort bien mangé. J'avais pas avalé compotée de choux depuis bien longtemps !

--- Tu auras bientôt un foyer où mijoteront les plats » s'amusa Tiphaine.

La jeune femme avait semblé un peu distante à Ernaut au début, mais il avait fini par l'apprécier. Elle demeurait habituellement légèrement en retrait, mais savait échapper quelques remarques amicales de temps à autre. Ses grands yeux bruns vous regardaient franchement et on voyait qu'elle était sincèrement attachée à Eudes, qui le lui rendait bien. Il aimait manger chez eux, car l'ambiance y était bien meilleure que chez Droart, même si la chère n'y était pas si bonne. Ils lui donnaient l'image du foyer qu'il espérait prochainement fonder avec Libourc.

« Toujours pas de date prévue ? demanda Eudes.

--- Pour le mariage ? Certes non. Lambert a convenu d'en parler avec le vieux Sanson avant Carême. Mais je n'ai nul bien encore assez, et Sanson n'est pas homme à accepter un douaire de journalier. J'amasse mon pécule, trop lentement à mon goût. Il me faudra peut-être joindre l'ost, espérer bon butin.

--- Tu as trop palabré avec l'Arménien, toi. Il t'a mis de drôles d'idées en tête mon ami. »

Ernaut haussa les épaules, avalant sa dernière bouchée avec une large portion de pain.

« J'aime à discuter avec les voyageurs, cela me donne l'impression de parcourir le monde.

--- Sois prudent, il en est de certains qui avancent derrière un masque. Karpis est de ceux-là.

--- Que veux-tu dire ?

--- J'ai encontré un vétéran du vieux prince Raymond, désormais retiré en un casal vers Hébron. Il racontait certaines choses pas bien belles sur Chypre et le nom de Karpis était dans sa bouche en ces moments.

--- Y'a pas qu'un âne qui s'appelle Martin ! » le coupa Ernaut, péremptoire.

Eudes commençait à bien connaître son ami et avait compris qu'il était illusoire de lui faire entendre raison quand il avait une idée en tête. Il liait à un physique de géant la détermination d'un sanglier. Mieux valait l'aborder par le flanc et laisser les choses se faire à leur rythme.

« Je sais bien que tu brûles d'aller batailler, mais c'est un endroit d'où on revient différent. Karpis est de ceux qui se sont perdus là-bas, je peux le voir, et tu t'en rendras compte un jour. Je pense que le prince Renaud est pareil. Trop de vaillance n'est pas forcément un don.

--- Si j'ai envie d'écouter un prêche, je mange habituellement chez mon frère, le railla Ernaut.

--- Cul-Dieu, si je me mets à deviser comme Lambert, je vais bientôt finir moine ! éclata de rire Eudes.

--- Sers-moi donc de ta bière, frère Eudes, avant que d'aller te faire tonsurer le crâne. Moi je n'ai pas l'intention de quitter le siècle. »

Il allait ajouter une remarque grivoise, mais il se retint au dernier moment. Il n'était ni à la taverne ni avec d'autres gardes, et Tiphaine, malgré sa gentillesse, n'apprécierait guère qu'on parle légèrement des femmes sous son toit. Il se contenta de choquer son verre contre celui de son ami. Il repensa soudain à son héros, qu'il brûlait tant de rencontrer.

« Puisse Dieu me permettre de saluer un jour le beau sire prince d'Antioche, et me donner sa vaillance.

--- Puisse Dieu te faire présent de plus de prudence » ajouta Eudes. ❧

### Notes

Ce Qit'a était le premier que j'avais commencé pour introduire Renaud de Châtillon dans Hexagora et je l'ai reporté maintes fois, espérant le conserver pour une passerelle vers la cinquième enquête d'Ernaut, dont le titre envisagé, « Les noces de porphyre » indiquent que l'intrigue va se déplacer vers le Nord, plus près des territoires byzantins. Le prince d'Antioche y est bien sûr un personnage incontournable. Ne sachant pas à quel moment j'aurai le temps de m'attaquer à ce nouvel opus, il m'a semblé préférable de publier sans attendre plus.

L'idée qui sous-tend ce court texte est celle de la propagation des nouvelles, qui se fait uniquement par voie orale. Comment les réputations se faisaient autour des échanges, en un rythme lent, qui ciselait au fil des mois et des années une image auprès d'un public qui recevait par la même voie les récits des figures légendaires et mythiques. Il m'a semblé intéressant de voir la façon dont l'identification pouvait alors être hybride, s'accrochant à une représentation qui n'avait pas besoin d'être complètement intégrée au réel. Cela vient donc en complément et miroir de ce qui s'est déroulé dans « Le lion et l'éléphant ».

### Références

Hamilton Bernard, « The Elephant of Christ: Reynald of Chatillon » dans *Studies in Church History*, vol. 15, 1978, p. 97-108.

Schlumberger Gustave, *Renaud de Châtillon, prince d'Antioche, seigneur de la terre d'Outre-Jourdain*, Paris, Plon : 1898.

Les chaussures du mort
----------------------

### Damas, maison d'al-Kāla, midi du youm el itnine 20 dhu al-hijjah 553[^41]

Avec le mauvais temps et le mois des pèlerinages à la Mecque, la ville bourdonnait tranquillement de ses activités quotidiennes sans l'agitation frénétique du reste de l'année. Razin et al-Misri, deux Soudanais exilés en Syrie depuis des années, prenaient un repas léger chez une vieille femme de leur voisinage. Désargentée, elle proposait mezzés et plats chauds à des prix modestes dans son étroit rez-de-chaussée accolé à une minuscule mosquée de la rue de l'Eau, en plein centre de la puissante cité. On y croisait une clientèle d'habitués, généralement des porteurs d'outres, qui traitaient al-Kāla comme une parente.

Les deux frères avaient invité à manger l'imam de leur congrégation, car ils avaient besoin d'un avis autorisé, à la fois sur des questions de foi, mais aussi sur certains points politiques, dont les religieux étaient souvent au fait. Ils étaient un petit nombre de vendeurs d'eau qui envisageaient de faire un présent à l'émir Nūr ad-Dīn afin de lui souhaiter meilleure santé. Ils espéraient par la même occasion appeler sur eux sa protection, voire être gratifiés de quelques privilèges. Nūr ad-Dīn était en effet réputé pour se montrer généreux avec les humbles, surtout ceux qui pratiquaient un pieux sunnisme. Les deux frères avaient pris sur eux de gérer la question du cadeau, mais, Soudanais d'origine et Égyptiens de culture, ils voulaient se doter de toutes les garanties pour ne commettre aucun impair.

Leur choix s'était porté sur le don d'un bol magique de guérison, dont on disait qu'il apportait la bonne santé à qui y buvait régulièrement eau, huile ou lait. La sélection des motifs et des symboles adaptés avait fait l'objet de nombreux et longs débats au sein de leur confrérie, et il était important de s'assurer que les formules de bénédiction soient adroites et de circonstance. L'émir avait connu de fréquents accès de maladie ces dernières années et il ne fallait pas non plus être trop obséquieux au cas où il décéderait et verrait lui succéder un ancien opposant. Flatter et glorifier sans trop s'engager était un exercice périlleux quand le pouvoir pouvait basculer d'un jour à l'autre. Les deux Soudanais l'avaient cruellement découvert quelques années plus tôt en Égypte.

Leur imam, un jeune érudit aux joues rebondies issu d'une vieille famille de Damas, était toujours partant pour partager plats et boissons, comme en attestaient ses formes replètes et son ventre généreux. En dépit de l'air débonnaire que cela lui donnait, il était très strict et peu enclin aux compromis, malgré une faconde dans le verbe qui pouvait suggérer l'illusion de la légèreté. Son engagement volontaire auprès des plus modestes alors qu'il aurait pu connaître une vie opulente de négociant lui valait une réputation flatteuse, dont il essayait de ne pas s'enorgueillir.

Il avait apporté quelques feuillets sur lesquels il avait tracé de son élégante écriture de lettré des formules dont il pensait qu'elles seraient adaptées. Le choix des sourates lui avait demandé un long travail et il venait d'en achever la lecture, commentée, à l'intention des deux frères. Razin hochait la tête avec conviction, bien que n'étant pas complètement au fait des subtilités évoquées. Il lui semblait néanmoins que c'était là l'attitude à présenter. Ce fut al-Misri, volubile comme à son habitude, qui prit la parole le premier.

« Nous te sommes redevables de cette parfaite sélection, qu'Allah te bénisse.

--- Amine. Je suis heureux que la santé d'al Malik al-Ādil Mahmud[^42] vous préoccupe. Beaucoup se réjouissent, alors qu'il a amené paix et prospérité sur cette cité.

--- A-t-on nouvelles récentes ?

--- Bien peu, et les prières ne seront pas inutiles pour en appeler à la bénédiction d'Allah. Les meilleurs médecins sont près de lui. Il aurait même déjà désigné son successeur si son séjour parmi nous devait s'achever.

--- La rumeur disait vrai alors ? Le prince de Mossoul ?

--- Oui, les principaux émirs sont venus. Il leur a fait jurer de soutenir Qutb ad-Dīn Mawdūd comme son seul et unique héritier. »

L'imam marqua une pause, tirant sur sa longue barbe, puis ajouta avec une allégresse un peu forcée.

« Mais avons-nous inquiétude à avoir lorsque tous les bons croyants s'associent pour en appeler à la Miséricorde d'Allah pour notre protecteur ? Il n'est pas encore dans la tombe qu'il faille se lamenter ! »

Al-Misri acquiesça avec enthousiasme. La présence d'un homme fort amenait paix et stabilité dans la ville depuis plusieurs années. Même si les Ifranjs savouraient ses ennuis récents et lançaient des assauts de façon régulière sur ses provinces, ils étaient chaque fois repoussés dans leurs territoires.

### Damas, porte de Bāb al-Ğābiya, fin d'après-midi du youm al khemis 30 dhu al-hijjah 553[^43]

Idris était descendu de sa monture afin de passer la porte, peu désireux d'attirer l'attention sur lui. Il avait quitté la caravane avec qui il avait cheminé depuis Baalbek au moment de pénétrer dans la Ghuta, l'oasis qui enserrait la ville. Il n'avait gardé avec lui que son valet, Ahmed, un jeune alexandrin qu'il formait à la pratique du commerce. Il n'avait plus foulé le sol de sa cité natale depuis bien longtemps et se réjouissait de faire à ses proches la surprise de son retour pour la nouvelle année.

Son inquiétude avait néanmoins enflé au fur et à mesure qu'il avançait dans le territoire de son enfance. En mer depuis des mois, il n'était pas préparé à la situation de siège qu'il avait découverte en revenant : des soldats en armes partout, des unités de Turkmènes amassés autour des baraquements de toiles sur le Mīdaān vert, près du Baradā, et la présence de contingents venus du nord en grand nombre. Il avait appris que des opérations militaires étaient en cours dans les zones septentrionales, les Latins se regroupant avec l'empereur byzantin afin de prendre à l'émir son joyau, la fière cité aleppine. Il s'étonnait d'autant plus de cet état de guerre si loin à l'est.

Il fut arrêté par un milicien à l'aspect malingre, qui voulait savoir s'il détenait des biens de valeur susceptibles d'intéresser la douane.

« Je n'ai rien d'autre que ce que je porte, ainsi que mon jeune valet. Je suis dimashqi et de retour parmi les miens. »

L'homme ne parut guère impressionné et le toisa sans aménité.

« D'où venez-vous ?

--- Baalbek, où j'ai fait visite à des amis.

--- Et avant cela ? Il n'y a guère de commerce en cette saison.

--- Voilà parole bien avisée. C'est justement car nul ne prend la route en ces mois que je souhaite retrouver la douceur de ma cité natale, la belle *colline bien stable et dotée d'une source*[^44]. »

Le garde se dérida à la mention de ce passage si apprécié des Damascènes et se fendit même d'un sourire disgracieux. Son visage revêche semblait réticent à l'idéer de présenter la moindre amabilité malgré ses tentatives. Il ajouta un hochement de tête, jaugeant Idris et son valet avant de s'adresser à son émir, lui demandant s'il pouvait laisser entrer le voyageur qui revenait chez lui. Occupé à échanger avec un caravanier ombrageux, l'officier acquiesça d'un geste sans trop y regarder.

Avant de pénétrer dans la cité, Idris hésita puis sourit au milicien devenu conciliant.

« À peine arrivé à Rabwé, j'ai commencé à croiser plus d'hommes de sabre et d'arc que de fellahs. Y a-t-il rumeur de guerre ? Doit-on craindre pour les siens ?

--- Cela fait des semaines que l'émir garde le lit, dans la forteresse. Les chacals rôdent pour enfiler les bottes du moribond...

--- *Moi contre mes frères ; mes frères et moi contre mon cousin ; mes cousins, mes frères et moi contre le monde*.

--- Inch'allah ! Peut-être en saura-t-on plus à la grande prière... À nouvelle année, nouvelle moisson. »

Idris hocha le menton. Il serait sans faute le lendemain dans la mosquée cathédrale. S'il n'était pas particulièrement dévot, il n'ignorait pas que ce serait le lieu où apprendre les dernières nouvelles. Il avait eu tellement hâte de retrouver les siens, et s'étonnait d'être soudainement impatient de repartir en mer, loin des intrigues politiciennes qui rongeaient le cœur de sa ville bien aimée. Il disait souvent à ses amis qui se languissaient de lui lors de ses fréquentes et longues absences : *L'exil avec la richesse, c'est une patrie. La pauvreté chez soi, c'est un exil*. Il commençait à croire que l'exilé n'avait en outre pas à subir la cruelle désillusion de la mort lente de sa cité.

### Damas, quartier du Marché aux oiseaux, soirée du youm al talata 12 muharram 553[^45]

Sa'd ad-Dīn 'Utman profitait de la soirée en lisant un de ses ouvrages d'hippiatrie. Il se targuait d'être un fin connaisseur des chevaux, qu'il élevait, comme son père et ses aïeux avant lui. Ses revenus tirés du commerce et de sa participation à différents dîwâns de l'administration damascène lui donnaient l'aisance suffisante pour se consacrer essentiellement à ce passe-temps d'importance.

Au fil des ans, il avait amassé une bibliothèque digne d'éloges, qu'il ne dévoilait qu'à ses proches ou à ceux qui lui étaient recommandés. Cela lui permettait de détenir au sein même de sa riche demeure quelques liens avec sa dispendieuse écurie installée en périphérie de la ville, sur le chemin de Ğawbar. Fraîchement marié, il espérait avoir rapidement un fils à qui communiquer l'importance de cette tradition familiale.

Il venait de se faire servir un peu de leben et des pâtisseries aux dattes, et se lavait les mains avant sa collation quand son valet entra et lui annonça l'arrivée d'un visiteur. À ce moment de la nuit, ce ne pouvait être qu'un importun ou un porteur de mauvaises nouvelles. Lorsqu'il apprit que c'était son jeune cousin Hakim, il ne put réprimer un frisson. Son parent aurait dû se trouver loin au nord, sur la route de Harrān. Il fit signe de l'introduire sans perdre de temps.

La tenue débraillée et fatiguée, sale de poussière et de boue, humide et sentant le crottin et le feu de bois augurait du pire, quoique plus angoissant encore était le regard de chien battu. Hakim bafouilla en guise de salut, inquiétant d'autant plus Sa'd ad-Dīn.

« Que s'est-il passé ? Tu n'as pu délivrer les lettres si vite !

--- Nous avons été pris, mon cousin. Nous avions franchi Alep sans souci, mais une patrouille près de Manbiğ nous a reconnus comme *dimashqi*, a refusé le passage et voulait nous saisir. Allah soit loué que vous m'ayez donné un de vos rapides coursiers. Aucun de ces maudits Turkmènes n'a pu me rattraper.

--- Et Ibn Magzū ? Est-il avec toi ?

--- Il n'est pas si bon cavalier, je crains qu'ils ne l'aient pris.

--- Au moins n'est-il pas de la famille. Espérons qu'il saura tenir sa langue !

--- C'est qu'il avait les lettres cachées dans ses chaussures, mon cousin... »

Sa'd ad-Dīn éprouva un léger vertige, s'affaissant dans les moelleux coussins de son siège. Il se racla la gorge, cherchant à retrouver ses esprits.

« A-t-il la mienne aussi ? Ou juste celles des autres ?

--- Le gouverneur 'Izz ad-Dīn avait insisté que son gulham garde les missives, je n'avais rien sur moi. »

Sa'd ad-Dīn secoua la tête. Son nom figurait sur le document qu'il avait écrit à l'intention de Nusrat ad-Dīn, le seigneur de Harrān et frère de l'émir. Comme ses complices, il l'incitait à venir prendre le pouvoir à Damas contre les souhaits de Nur ad-Dīn dès que celui-ci décéderait. Mais le vieux soldat semblait se remettre de ses dernières fièvres et il n'apprécierait certainement pas ce nouvel appel à trahison. Même s'il n'était pas sanguinaire comme son père, il n'était pas homme à traiter à la légère les tentatives de sédition.

« J'ai galopé comme le vent. Nous avons un jour ou deux avant que la nouvelle parvienne ici, mon cousin. Dans le cas où ils trouvent les lettres...

--- De cela je ne doute pas un instant. Et si je me trompe, il est de moindre importance que je parte à la mauvaise saison pour rien. Qui sait ce que tu viens de m'apprendre ?

--- Personne. J'ai voyagé tel un fantôme depuis des jours, gardant le silence avec les étrangers, évitant patrouilles et cités, dormant quelques heures à peine chaque nuit, poussant ta magnifique monture à la limite de l'épuisement à chaque étape.

--- De cela je ne te remercierai jamais assez. Va prendre un peu de repos, fais-toi donner un bain. Pendant ce temps, je vais lancer des ordres, nous partons demain pour al-Misr. Je prétexterai des ennuis avec des fournisseurs. La main de l'émir ne porte pas là-bas. »

Hakim approuva, l'air las. Il allait se retirer puis, hésitant à vexer son cousin et protecteur, il fit une moue inquiète avant de livrer le fond de sa pensée.

« Ne devrions-nous pas prévenir le gouverneur ou l'intendant de la Chancellerie ?

--- Ce sont des proches de Nūr ad-Dīn, il leur pardonnera. Je n'ai pas tant d'appuis, cela me vaudra d'avoir la tête coupée ! Et à toi aussi si on apprend que tu les as visités juste avant leur fuite. »

À voir l'air dépité de son cousin, Sa'd ad-Dīn eut un pincement au cœur. Il savait que sa décision pouvait paraître cruelle, mais il ne pouvait risquer d'être pris. Les hommes de Nūr ad-Dīn avaient une chance d'en réchapper en faisant jouer leurs relations. Pour sa part, si on dévoilait ses amitiés shi'ites, il était certain d'y laisser la vie. Il avait espéré attirer un prince plus tolérant, souhaitant ouvrir ainsi des ponts vers l'Égypte, mais son rêve avait vécu. Il ne lui restait plus qu'à fuir pour sauver ce qui pouvait encore l'être. Il jeta un coup d'œil à sa merveilleuse bibliothèque et poussa un long soupir de dépit à l'idée de la perdre.

### Damas, citadelle, matin du youm al khemis 19 safar 554[^46]

Soldat d'élite formé depuis sa plus tendre enfance, Sawwar avançait avec la démarche chaloupée des familiers de la selle. Comme les marins, il trouvait le sol trop dur pour le fouler de ses pieds. Il aimait mieux le soulever en nuages de poussière, un cheval entre ses cuisses. Porté aux excès, il fêtait depuis plusieurs jours le possible départ prochain en campagne. C'était pour lui une tradition que de se délester de tout ce qu'il possédait avant de prendre le sabre et l'arc. Il voyageait léger dans l'espoir d'aller loin. Et là il venait d'apprendre une nouvelle qui lui mettait le cœur en joie : il allait découvrir al-Qahira[^47], les sables du désert percés du fleuve majestueux. Il pourrait goûter aux courtisanes noires comme l'ébène dont on disait l'étreinte subtile et enfiévrée. Son sang bouillonnait littéralement, encore embrumé des vapeurs alcoolisées de la veille.

Il se figea au moment de frapper à la porte du cabinet où les officiers se retrouvaient pour préparer les opérations. Il patienta un instant puis pénétra, le visage soudain emprunt de sérieux et de solennité. La pièce, sobrement garnie de coffres, de sièges bas et de petites tables, voyait son mobilier évoluer selon les besoins et les envies des présents. Pour le moment, seul son émir Yalim al-Buzzjani était occupé à lire des courriers, sirotant une boisson épicée dont les vapeurs venaient danser jusqu'aux narines du soldat. Il s'approcha et salua de sa voix rauque.

L'officier lui accorda un sourire sans le regarder, finit sa missive puis répondit enfin au bonjour. Comme toujours, il était environné de papiers divers, livres et feuillets. Sawwar ne comprenait pas l'intérêt qu'il y avait à s'épuiser les yeux dans de telles fariboles de citadin décadent. Mais il conservait la plus haute estime pour son maître, dont il savait la science et la sagesse de grande importance. En outre, ils avaient développé une relative amitié au fil des ans, née de la camaraderie à porter le fer et le feu en selle, ou à subir des assauts ensemble.

« Tu pourrais au moins te rendre aux bains avant de te présenter à moi. Les puanteurs de tes excès te suivent comme des mouches. J'espère qu'il en est fini de ceux-ci jusqu'à l'année prochaine.

--- Je suis tel le faucon dont on ne dénoue pas le lacet... Avez-vous appris quand nous partons ?

--- Cela ne devrait plus tarder, l'émir a donné ses ordres hier. Tu le saurais si tu avais été présent à l'audience plénière. Demain la cité priera pour la bonne fortune du chambellan al-Muwallad al-Mustašīdī pour un départ un ou deux jours après. Temps pour nous de préparer la caravane. »

Sawwar opina avec enthousiasme. La traversée serait certainement tranquille. Les Ifranjs étant pour la plupart occupés loin au nord, aucune troupe n'aurait la puissance de s'opposer à leur avancée. Il le regrettait presque, n'aimant rien tant que le son clair du choc de l'acier ou le bruit sourd des flèches touchant au but.

« Garde aussi en tête que nous devons estimer la force des armées du vizir, voir comment ses marches sont tenues et si ses hommes sont de qualité.

--- J'ai commencé à me faire mon idée en montrant à certains de ces Misri mes coins préférés ! »

Il marqua une pause, puis reprit, de façon moins goguenarde.

« A-t-on projet de s'attaquer à eux ?

--- On ne m'en a rien dit, un tel secret serait forcément gardé bien serré. Mais cela m'étonnerait. L'émir a déjà fort à faire ici. Il doit surtout espérer que nos frères en religion, même s'ils ne reconnaissent pas le bon Calife, auront la force de nous venir en aide si besoin, ou, au pire, la capacité de résister aux assauts des infidèles, ou aux tentations de nous nuire.

--- Du genre soutenir le mauvais prétendant à l'héritage ?

--- Par exemple. Même si al-Malik semble désormais aussi vaillant qu'un jeune étalon. »

Sawwar acquiesça, mais il n'était qu'à demi convaincu du retour en santé du puissant dirigeant. Il était certain que ce dernier s'était amolli au fil des ans, à loger entre quatre murs, sous un toit de pierre. L'homme était fait pour vivre sous la toile, son arc à portée de main et sa monture attachée à son arbre. Puis il disparaissait emporté comme le sable dans la tempête. Il fallait être bien infantile pour croire que l'on pouvait tenir quoi que ce soit en cette vie. ❧

### Notes

Si nous sommes plus ou moins familiers des tergiversations du pouvoir chrétien en général et latin en particulier pendant les croisades, j'ai constaté que bon nombre de personnes imaginent encore les puissances musulmanes comme un bloc uni. Il n'est pourtant que de parcourir les sources pour voir combien les communautés et cultures étaient fragmentées, malgré une foi commune, quoique très divisée en de nombreux courants et sectes, dont le shi'isme et le sunnisme ne constituent que les ensembles les plus généraux. À cela s'ajoutaient des ambitions personnelles des princes, qui n'hésitaient pas à nouer et trahir des alliances au fil des ans, dans le but d'amasser toujours plus d'influence et de pouvoir. Ils n'étaient en somme pas plus unis ou solidaires que les barons latins. Au XIIe siècle, cela n'arrivera que brièvement, quand Saladin réussira à tenir dans sa main tous les territoires durant un temps. Mais ceci est une autre histoire...

Le titre de ce Qit'a est tiré d'une expression qui dit que *Celui qui espère après les chaussures d'un mort ira longtemps pieds nus*. Cruelle prophétie pour décourager les intrigants qui patientent auprès d'un mourant dans l'espoir de s'arroger son héritage. L'idée de ce texte, de parler de façon périphérique des tentatives de soulèvement ou de trahison autour de Nūr ad-Dīn, est née en découvrant les bols magiques du type que Razin et Al-misri veulent offrir à l'émir. De magnifiques exemplaires contemporains de ce Qit'a en avaient été présentés dans l'exposition *L'orient de Saladin. L'art des ayyoubides*, en particulier la pièce numéro 223 du catalogue.

### Références

Alaoui Brahim (commissaire), *L'orient de Saladin. L'art des ayyoubides*, catalogue de l'exposition présentée à l'institut du monde arabe, Paris : Institut du monde arabe / Gallimard, 2001.

Élisséef, Nikita, *La description de Damas d'Ibn 'Asâkir*, Institut Français de Damas, Damas : 1959.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Retour au siècle
----------------

### Calanson, cimetière paroissial des frères de Saint-Jean, après-midi du dimanche 3 avril 1160

Moite de la touffeur de cette chaude journée de printemps, Frère Hémeri s'était retiré dans un bosquet de pistachiers et de caroubiers qui avaient peu à peu colonisé l'aire sépulcrale. Au fil des années, les frères de Saint-Jean y avaient disposé quelques sièges, bancs rustiques et rochers plats, pour y profiter de l'air pur tout en goûtant l'ombre bienfaisante en été. Il était même arrivé que des réunions capitulaires s'y déroulent. Hémeri y avait pris ses habitudes après la collation de sixte. Il s'y installait pour une quasi-sieste, bercé par l'agitation du village dont il n'était séparé que par un mur en pierres sèches.

Des pas firent rouler le gravier aux abords de sa retraite, mais il s'abstint d'ouvrir les yeux, estimant qu'il devait s'agir d'une famille souhaitant se recueillir sur la tombe de quelque défunt. Peu après Pâques, il était fréquent qu'on vienne orner les croix de morceaux de rameaux bénis, espérant attirer sur les proches la bienveillance du Seigneur.

Il entendit le frottement des branches et des feuilles sur un vêtement, signe que la personne pénétrait dans le bosquet. Peut-être était-ce finalement un frère qui avait besoin d'accéder à quelque denrée... Il porta machinalement la main à son trousseau de clefs et ouvrit les paupières. Il sursauta car il ne s'attendait pas à découvrir la robuste silhouette de Tabitha, jeune fille des environs, fréquemment employée comme servante au sein du petit hôpital. Un rapide coup d'œil circulaire lui confirma que personne d'autre ne se trouvait près d'eux.

« Que viens-tu faire ici ? On ne doit ja nous voir ensemble en pareil endroit !

--- J'en demande pardon, frère, mais... »

Sa voix s'éteignit, et son regard alla voguer au sol. Hémeri se leva, reculant instinctivement afin de conserver entre eux une distance garante de respectabilité. Les mains dans les manches, il se tordait les doigts sans plus parvenir à articuler pendant un long moment. En pleine lumière, habillée de sa plus belle tenue, elle lui apparaissait si féminine, toute en courbes et en vagues, les cheveux sagement maintenus par un foulard lâche qui dévoilaient une nuque sensuelle. Il se morigéna intérieurement de laisser son esprit divaguer à de telles pensées, à quelques mètres à peine du sanctuaire.

« Que me veux-tu ? Reviens me voir demain, au cellier, avec les autres. Il y aura de l'ouvrage. Avec tous les pèlerins de ces dernières semaines, les lessives et le raccommodage ne manquent guère.

--- Je ne suis pas là pour obtenir labeur, c'est à propos de... nous. »

Hémeri tressaillit à l'écoute de ce mot terrible. Voilà qu'ils n'étaient plus deux personnes séparées, distinctes et sans rapport, mais une seule entité, une créature nouvelle, dont la simple évocation le faisait trembler d'effroi.

« Il n'y a rien de tel, nous avons juste... Ne te méprends pas, j'ai réelle amitié pour toi, mais il ne peut y avoir plus que ce que je t'offre. »

Tabitha hocha la tête tristement.

« Tout cela était bel et bon, et je ne saurais dire que vous n'avez pas été de grande générosité, mais il se trouve que les choses sont plus compliquées maintenant.

--- En quoi ? T'ai-je trahi à un quelconque moment ? Ou te sens-tu flouée de nos accords ? Je ne peux te garantir quoi que ce soit, mais chaque fois que... Eh bien, à chaque fois, il y aura quelque chose en dédommagement. »

La jeune femme acquiesça, de nouveau en silence, cherchant péniblement en elle le courage de s'expliquer.

« C'est que mon ventre s'arrondit de belle façon, je ne saurais le cacher encore bien longtemps. »

L'annonce résonna en Hémeri comme la foudre, brûlant ses entrailles et broyant ses os. Il lança une main vers le tronc le plus proche pour s'y appuyer. Le monde autour de lui manqua de disparaître.

### Aqua Bella, cellier des frères de Saint-Jean, soirée du jeudi 28 avril 1160

La chiche lumière du soir en ce jour nuageux n'éclairait que les abords de la grande porte, laissant les cuves, barils, doliums et caisses dans les ténèbres. Dans une lampe à huile, une minuscule flammèche battue par une main invisible s'efforçait de ponctuer d'ambre les reliefs. Dans un recoin, deux moines s'étaient installés autour de la petite table de travail où s'étalaient échiquiers, jetons, balance et bâtons de taille. Au fatras habituel de frère Guillaume s'ajoutaient deux gobelets de terre exhalant de chaudes senteurs épicées. Les deux hommes goûtaient en silence leurs boissons, absorbés en eux-mêmes. Il fallut l'irruption d'un chat, ondoyant entre leurs jambes pour les ramener à la réalité.

« Tu n'es certes pas venu jusqu'en mes montagnes pour simplement parler de draps et de boisseaux de blé, Hémeri. Je vois bien une ombre gagner en toi. Non que tu aies jamais été fort jovial, mais un poids semble peser en ton âme. »

Hémeri avala longuement une gorgée, cherchant une contenance et du courage dans le fort breuvage. Il reposa le verre lentement, le faisant rouler entre ses doigts avant de se résoudre à répondre.

« J'ai grave souci, en effet. Je ne sais même pas comment en parler à mon confesseur...

--- Quelque faute ? Péché véniel j'ose encroire !

--- Bien terrible est le fardeau, et chaque jour en accroît le poids sur mes épaules.

--- Conte-moi donc ce qu'il en est, nous portons plus à notre aise avec sochon à nos côtés.

--- J'ai forte amitié pour vous, frère Guillaume, et j'ai souffrance à vous souiller ainsi de mes errements. Mais je ne vois nul autre qui puisse apporter lumière en cette noirceur. »

Il inspira un grand coup et parla avec vigueur, comme s'il cherchait à se délester au plus vite, s'arrêtant à peine pour reprendre son souffle entre deux phrases.

« Nous avons usage de recourir à quelques lavandières pour nos linges, comme de coutume, et j'ai succombé à la chair en les côtoyant. Il m'est arrivé, certaines fois, d'encontrer l'une d'elles dans nos resserres, et de m'y abandonner à bien terrible péché. Elle y était fort consentante, je ne l'ai jamais forcée, et j'ai veillé à la dédommager de ce bien funeste péché.

--- Il n'y en a eu qu'une ?

--- Oh oui, je ne saurais dire le pourquoi, si ce n'est qu'elle m'a séduit. De bien innocente façon, la faute est autant mienne que sienne. Nous avons succombé à la violence de la chair.

--- Tu n'es par malheur pas le premier clerc à ainsi rompre ses vœux. Au moins n'as-tu pas ajouté à cette violation de ton serment quelque autre péché plus grave. Il va te falloir mettre fin à tout cela et recevoir de ton confesseur le châtiment approprié. Et l'enjoindre elle aussi à se purger de sa faute... »

Hémeri soupira.

« Si je m'en viens jusqu'ici, c'est que je ne le peux guère aisément. Il se trouve que la garcelette est grosse de moi. »

Frère Guillaume écarquilla les yeux, se reculant sur son tabouret, en un repli instinctif face au péché.

« Par la Croix-Dieu, que voilà funeste situation, mon frère...

--- Vous êtes le seul en qui j'ai confiance, j'ai besoin de votre aide.

--- Comme tu y vas ! Je veux de certes t'assister, mais je ne porterai le poids de tes choix. »

Il se tut un moment, étudiant son compagnon, s'accordant une petite lippée pour s'éclaircir les idées.

« De ce que je vois, tu as enfreint des promesses faites à Dieu et à tes frères, et tu as souillé cette jeune garce par tes manquements. J'ai souvenir que dans mes jouvences, il y avait un clerc, où je faisais mon noviciat, qui avait pareillement fauté. Il a fait en sorte que le fruit de ce manquement soit confié aux moines de Cluny. De ce que j'ai appris, le gamin est désormais prêtre de bonne fame. D'un mal peut naître un bien.

--- Me conseillez-vous de faire don de cet enfant à venir à l'Église, en rémission de mes péchés ?

--- Certes non, je me garde bien de donner conseil ! N'écoute d'ailleurs que distraitement ceux qui les dispensent de bon gré alors qu'ils ne connaissent pas les affres de tes tourments ! Ce que je veux te dire, c'est qu'il te faut prendre une juste décision pour que de tout cela quelque bien puisse advenir. Dieu a confiance en toi. Veille simplement à ne pas répandre le mal en justifiant un manquement par une nouvelle erreur. Tu as gravement failli, mais c'est là épreuve dont tu peux sortir grandi. »

Hémeri lança un regard perdu vers son compagnon. Il avait beaucoup espéré de cette entrevue, et elle n'apportait que plus de questions. Il sentait pourtant de façon confuse qu'il y voyait une lumière y naître. Avec l'aide de Dieu, de la prière, il arriverait à trouver en lui les ressources pour faire face à son malheur. Il lui fallait l'affronter sans aucun autre recours terrestre. Le premier vrai combat de sa vie.

### Calanson, cellier des frères de Saint-Jean, fin de veillée du jeudi 5 mai 1160

Hémeri sursauta lorsque le petit guichet s'ouvrit en grinçant puis, reconnaissant les formes de Tabitha, il l'invita d'un geste à entrer rapidement. La lanterne à plaques de corne qu'il avait prise avec lui ne permettait guère d'y voir, n'étant prévue que pour se repérer grossièrement quand on déambulait de nuit dans les bâtiments. Plus sûre que les simples lampes à huile, elle était aussi moins efficace.

Le frère de Saint-Jean se sentait mal à l'aise à l'idée de cette rencontre. Ils renouaient avec leurs usages clandestins, de se retrouver nuitamment, mais c'était désormais dans un but bien moins grisant que leurs étreintes fébriles. Depuis son retour d'Aqua Bella, Hémeri avait beaucoup réfléchi et il voulait livrer le fond de sa pensée à celle qui portait le fruit de leur union défendue.

« Personne ne t'a suivie ?

--- J'ai été prudente ainsi que les fois précédentes. Le petit portail du verger puis au travers des jardins. Pas même un chat n'aurait pu me pister sans que je le voie.

--- Parfait, parfait. Je t'ai demandé de me retrouver, car j'ai pris ma décision. »

À s'entendre aussi pontifiant, Hémeri réalisa qu'il alarmait la jeune fille. Elle le regardait d'un air qu'elle s'employait à rendre détaché, mais la peur se lisait sur son visage.

« Je ne peux te laisser seule affronter cette épreuve, mais je ne peux demeurer en cette maison, ayant profané mon statut de clerc de façon définitive. Je pense que nous pouvons aller dans ma famille, à Césarée. Je vais prendre de quoi faire le voyage aux frères et je les rembourserai après. »

le soulagement transfigura le visage buté de Tabitha, qui s'accorda même un timide sourire, les yeux emplis de reconnaissance.

« Que voulez-vous dire ? Vous allez m'abandonner aux vôtres ?

--- Certes pas. Je ne serai pas le premier moine qui perd le froc. Il me semble que ce serait grande honte que de te laisser porter le poids de notre faute tout en continuant à profiter de la vie ici, à l'abri. Dieu m'a envoyé cette épreuve, je dois m'y confronter. »

Tabitha se rapprocha, rassérénée.

« Vous seriez prêt à tout quitter ? Pour moi ?

--- Il faut boire le vin tiré. Il y a plus déshonnête labeur que de gagner le pain des siens à la sueur de son front. »

Émue de cette réaction, Tabitha esquissa un mouvement vers lui, mais s'arrêta en le voyant si raide, si guindé. Elle se contenta de lui prendre la main.

« Je vous pensais homme de bien, mais vous êtes encore plus que cela. Merci de...

--- Nous savions tous deux que cela pouvait arriver. Dieu nous met à l'épreuve. »

La jeune femme avait espéré un peu plus de chaleur dans leurs échanges. Elle avait tout d'abord monnayé ses charmes contre quelques menus avantages, dont elle faisait profiter sa famille désargentée. Ses parents faisaient comme s'ils ne voyaient rien, trop impatients de partager cette manne pour se risquer à la perdre par des considérations morales. Mais avec le temps, elle avait trouvé Hémeri attachant. Au contraire des quelques autres auxquelles elle s'était vendue, il n'avait jamais été brutal. Empressé et maladroit, certes, mais par inexpérience et impétuosité. Elle s'était imaginé qu'il aurait pu développer des sentiments. Mais il n'agissait qu'en homme de devoir. Elle retint un soupir, trop heureuse déjà de n'avoir pas à gérer la situation seule. Elle estimait s'en sortir à bon compte.

« Mais que vais-je dire aux miens ? Si nous disparaissons de concert, ils vont comprendre. Grande honte va s'abattre sur eux !

--- Feindront-ils l'étonnement ? Alors même que tu leur portais beaux quartiers de viande ou sacs de grain sans commune mesure avec ton labeur en notre couvent ? Nous ne pouvons rester ici... »

Tabitha hocha la tête, elle sentait son cœur se comprimer à l'idée de quitter ce casal à jamais, aux côtés d'un homme qu'elle ne connaissait que fort peu. Elle n'était pas spécialement attachée à sa famille, qui n'avaient jamais cherché à la marier bien qu'elle allât sur ses seize ans. Pas assez jolie et trop pauvre devaient-ils estimer, ils privilégiaient leur fils aîné et ses deux sœurs cadettes. L'alternative à la fuite, finir servante de ses parents puis de son frère ne lui paraissait guère plus enviable. Au moins aurait-elle une chance de vivre quelques belles années.

### Césarée, hôtel de Garin la Sonnaille, veillée du mardi 21 juin 1160

Au travers des gobelets de verre clair, le nectar de Chypre tirait du soleil tardif des éclats rubis qui dansaient sur les murs. Garin jouait avec sa boisson, la humant et la respirant autant qu'il la goûtait. Il n'accordait que peu de valeur au fait qu'il l'avait payé une fortune ; il était vraiment amateur de bons crus et pouvait passer une veillée à savourer les différents arômes d'un cépage de qualité correctement vinifié. Il sentait juste que la soirée ne serait pas aussi agréable qu'escomptée, avec l'arrivée impromptue de son jeune frère Hémeri.

Celui-ci, face à lui, avalait son nectar comme une petite bière, l'air las, misérable et pathétique. S'il était un péché que Garin ne pouvait supporter, c'était bien l'acédie. Il s'adonnait à ses propres vices avec une désinvolture qui lui rendait incompréhensible qu'on puisse en tirer la moindre tristesse.

« Si tu restes ainsi à te répandre en lamentations, je ne sais si je t'accorderai long temps avant de te retourner à la rue, mon frère !

--- Je suis désolé de faire si grise mine alors que tu m'ouvres grandes tes portes en ces temps troublés.

--- Que me chantes-tu là ? Tu as trouvé joli pot où tremper ta cuiller et garcelet va en jaillir, rose et poupon, prêt à s'activer pour tes vieux jours. Où vois-tu le mal ?

--- J'avais prononcé quelques vœux, si tu en fais souvenance. Père et mère doivent me maudire...

--- Ne préjuge donc pas des morts. Dieu seul sait. »

Garin fit une moue appréciatrice en avalant une petite gorgée, se laissant distraire un instant par le délicieux breuvage.

« Père t'a farci le crâne de ses idées, à faire de toi un clerc ! Mais quand il a fui au monastère, il pensait surtout à lui.

--- Là, c'est toi qui médis des défunts.

--- Certes non. Je souhaite que père soit heureux au Ciel, avec tous les Saints, mais il t'a montré bien lamentable exemple, à s'adonner en oraison à tout moment. Tu ne seras pas malheureux ici, crois-m'en ! »

Hémeri acquiesça, pitoyable. Il était encore couvert de la poussière du voyage, car il avait finalement fait la route à pieds avec Tabitha, n'emportant que quelques hardes qu'il avait prises dans les dons pour les pauvres. Un chaperon usé dissimulait tant bien que mal la tonsure cléricale. En chemin, lors des héberges, ils avaient prétendu être mari et femme, retournant chez eux après une visite faite à la famille. Arrivés à Césarée la veille, ils ne s'étaient présentés à Garin que ce midi.

Content de revoir son frère, celui-ci les avait accueillis sans problème, saluant Hémeri d'un sourire narquois en découvrant la jeune Tabitha, dont il déchiffrait les moindres courbes avec l'assurance d'un expert. Les questions n'étaient venues qu'à la veillée, dans le petit cabinet de l'étage qui ouvrait à l'ouest, laissant entrer les odeurs salines et le bruit des vagues.

Garin picora quelques amandes, chercha une position plus confortable sur ses coussins, puis rompit de nouveau le silence qui s'était installé avec la nuit qui se déversait depuis la rue.

« Il est malgré tout une affaire que tu ne dois pas surseoir. Il te faut l'autorisation de ton père abbé, ou évêque, ou ce genre de chose. Je ne saurais attirer mauvaise fame sur ma maison en donnant héberge à un défroqué en butte aux frères de Saint-Jean. Tu dois obtenir bon et licite consentement.

--- Comment veux-tu que je fasse ? Je n'ai nul appui qui pourrait me permettre de demander pareille licence.

--- Je vais faire écrire à Elainne. Elle s'y connait bien mieux en bondieuseries que nous autres. Si elle n'était si donzelle, j'aurais même cru à un moment qu'elle avait passé sa main sous la coule de son petit curé...

--- Tu ne respectes donc rien, ni les clercs, ni ta propre sœur ?

--- Les frères ont la réputation qu'ils méritent, il n'y a qu'à te voir bouffi d'orgueil alors que tu trempes au pot comme tout le monde ! Quant à Élainne, pour te dire ce que j'en pense : tu n'as jamais trouvé qu'elle ne ressemblait guère à père ? » ❧

### Notes

Cela faisait un moment que j'avais envie de parler du possible changement d'état d'un personnage adulte. L'idée en est née voilà des années, à la lecture d'une anecdote de la vie de Guillaume le Maréchal, relevée par Georges Duby dans sa biographie (pages 1081-1083 de son volume *Féodalité*). Mon souhait était à la base de montrer le défrocage, mais, s'il est évoqué dans les textes, il n'est jamais décrit en détail. J'ai donc dû abandonner cet espoir, et j'ai préféré me concentrer sur le cheminement intérieur du clerc devant affronter cette épreuve.

En outre, ce Qit'a est un de ceux que je désire voir advenir depuis un moment, car il resserre des fils que j'ai placés au fil du temps. Il me fallait avoir un large éventail de personnages, de situations, de récits, pour avoir suffisamment de matière afin de tracer des motifs distants, mettre en valeur de subtiles jonctions esquissées ça et là. La toile de vie s'en trouve densifiée, réalisée au sens premier. J'espère ainsi permettre l'advenue d'un monde véritable, celui que j'ai choisi d'appeler Hexagora et que je vous invite à parcourir depuis quelques années.

Rq : on parle couramment du *siècle* pour désigner le lieu où vivent les laïcs, à opposer à l'espace régulier, *sous la loi d'une règle*, où sont cantonnés les religieux cloitrés.

### Références

Augé Isabelle, *Byzantins, Arméniens et Francs au temps de la croisade. Politique religieuse et reconquête en orient sous la dynastie des Comnène 1081-1185*, Paris : Geuthner, 2007.

Duby Georges, *Féodalité*, Paris : Gallimard, 1996.

Thibodeaux Jennifer D. *Negotiating Clerical Identities. Priests, Monks and Masculinity in the Middle Ages*, New York : Palgrave MacMillan, 2010.

Le suc des roseaux
------------------

### Château du Fier, après-midi du samedi 10 octobre 1159

Indifférent à l'agitation autour de lui, un colporteur arrivé avec la caravane avait rapidement étalé quelques babioles sur une natte, un peu à l'écart des abreuvoirs et des bêtes. Il y avait disposé de menus breloques : rubans, lacets et aiguilles, boucles de métal, épingles et peignes. Il espérait faire quelques affaires avec les hommes de la garnison, allégeant son ballot pour la suite du chemin tout en empochant quelques piécettes ou méreaux.

Habib, jeune servant habillé d'un simple thawb ceinturé d'une toile, la tête ornée d'un turban soigneusement noué, hésitait sur quelques boutons qui lui seraient fort utiles, ainsi qu'un dé à coudre. Dans l'espoir de faire baisser le prix, il faisait mine de s'attarder sur des articles plus onéreux, plaisantait avec ses compagnons. Il savait que le convoi ne repartirait que le lendemain et que plus il attendrait, plus le marchand serait conciliant. La veillée serait également l'occasion de lui soutirer un éventuel rabais, en le jouant au dé, en lui offrant nourriture ou boisson.

Il se relevait en époussetant la poussière de ses genoux quand vint à lui un des voyageurs. Il reconnut immédiatement un des caravaniers qui faisaient souvent le trajet entre les cités du sud et la côte loin au nord, Bachar al-Tabassum. Fidèle à son surnom, celui-ci arborait un sourire d'une oreille à l'autre, dévoilant des dents d'une blancheur d'ivoire qui tranchaient dans son rude visage, prématurément ridé par le soleil. Ils se saluèrent aimablement et le voyageur l'invita de la main à se rapprocher.

« J'ai pour toi message de la part d'abu Salim. »

Cela signifiait en clair qu'il était porteur d'un message provenant de sa sœur Shukriya, épouse d'abu Salim. Mais il aurait été inconvenant de la part de Bachar de laisser entendre qu'il s'était entretenu avec elle directement, même si c'était le cas. Les fellahs des petites villes se mêlaient plus librement entre hommes et femmes, au grand désarroi de certains imams et cadis parmi les plus rigides sur les mœurs.

« J'ai moisson de nouvelles des tiens. Mais sache déjà qu'ils vont bien, les enfants courent comme gazelles et le vieil abu Talib conserve la vigueur du lion, même si sa boiterie est plus forte que jamais.

--- J'ai toujours crainte pour lui, avec les chaleurs de l'été...

--- Il dit que tu devrais revenir chez les tiens, qu'il y a de nombreux travaux à Zughar et du labeur pour qui en veut.

--- J'ai entendu dire que Badawil faisait planter des cannes à sucre, mais cela n'a rien de nouveau. C'est le fait des princes.

--- C'est qu'il met grands moyens pour ses terres : canaux de pierre pour l'eau, moulins solidement bâtis et murs chaulés. Quand je suis parti, ils avaient embauché tellement de laboureurs que les bêtes étaient serrées flanc à flanc, à tracer leurs sillons. »

Habib hocha la tête. Le calme relatif que connaissait la région depuis quelques années laissait entrevoir un développement économique qui pourrait profiter aux locaux. Cadet de famille sans fortune, il avait fui les abords de la Mer morte pour échapper à une probable condition de journalier, à récolter et trier le sel ou, pire encore, le soufre. Il s'était fait soldat, maniant la lance et l'arc de son mieux, et avait escorté des caravanes jusqu'à ce qu'il trouve cette opportunité dans une forteresse royale de seconde zone. Il y travaillait plus comme valet que comme guerrier, quoique la place fut plutôt bonne et correctement payée. Il n'allait pas lâcher la proie pour l'ombre, mais ne pouvait se permettre d'éconduire sans y mettre les formes une demande directe de son grand-père.

« Je vais demander congé au sire Gaston, peut-être pour les fêtes dans quelques semaines. Cela fait trop de mois que je n'ai vu les miens.

--- Je leur porterai réponse à mon retour de Gaza. Si jamais tu veux leur en dire plus, je repasserai certainement par ici, même si on s'y arrête que le temps de faire boire les bêtes. »

Habib acquiesça, remerciant Bachar d'un sourire. Puis il l'invita d'un geste à le suivre dans la forteresse. Il trouverait bien quelque boisson à partager avec celui qui lui portait des nouvelles des siens.

### Palmeraie de Segor, matin du lundi 8 février 1160

Habib longeait le chemin qui menait à la ferme royale, son épée battant fièrement à sa hanche. Il y avait dépensé pratiquement toutes ses économies, mais il trouvait que cela lui donnait l'air plus martial que la lance, attribut du simple milicien. Or il était en charge de la surveillance du domaine, répétant les directives à ceux qui se contentaient de gourdins et de piques. Il partageait ce privilège avec quelques autres syriaques sous l'autorité d'un Latin à l'élocution rapide et au tempérament agité, Le Bourdon.

L'exploitation en plein agrandissement qui recrutait dépendait directement de l'hôtel royal, il avait donc aisément obtenu congé du châtelain du Fier, et avait été affecté près de sa ville natale. Ayant une légère expérience militaire, qu'il avait agrémentée de quelques détails reconstruits, il avait même décroché une meilleure solde. Il profitait juste beaucoup moins du gîte et du couvert, retrouvant fréquemment sa famille dans la cité voisine pour la nuit ou les jours de repos.

Il rejoignit la troupe dans la principale cour, tandis que le bras droit du Bourdon, la Plume, les faisait se rassembler pour les consignes du matin. Si le Bourdon, avec sa petite taille et ses cheveux hérissés, était volubile, la Plume le surclassait largement par son babil incessant. Parsemant à l'envi ses phrases de blagues salaces et de remarques souvent grossières, il semblait perpétuellement satisfait de ses saillies, sans se soucier de leur réception auprès de son public. Il avait ainsi acquis une inquiétante facilité à vexer les gens par des réparties qu'il pensait spirituelles.

Habib eut à peine le temps de saluer ses compagnons que la corne résonnait, marquant le début de leurs tâches de la journée. Le Bourdon sortit de la petite pièce qu'il utilisait comme bureau et logement, juste à l'entrée de la vaste cour. De là, il pouvait surveiller depuis une lucarne l'accès à toutes les dépendances qui lui faisaient face.

Quoique ses talents d'archer aient démontré son expérience militaire, il ne ressemblait guère à un soldat, avec ses pattes courtaudes, sa démarche bonhomme et sa silhouette empâtée. Il était généralement habillé comme un Latin, mais arborait souvent un détail local, turban ou foulard en guise de ceinture, voire 'aba[^48] de laine si le temps était frais. De plus, il tenait parfois son épée tel un bâton, dans son fourreau gainé du baudrier.

Il se plaça face à la petite troupe, un sourire satisfait sur le visage devant assemblée de soldats dont aucun n'avait jamais eu à connaître un véritable affrontement. Mais ils étaient bien équipés et l'air correctement nourris, ce qui suffisait à éloigner maraudeurs, animaux sauvages et vagabonds. On ne leur en demandait pas plus.

« Alors, hui, nous avons mandement de guetter les journaliers qui œuvrent au tri et à la préparation des plants. Il ne s'agirait pas que les plus belles cannes finissent ailleurs. Vous avez vu ce qu'on a reçu, le lot de... »

Il chercha dans sa mémoire le terme qui désignait les boutures, mais n'y parvenant pas, il invita d'un geste Habib à compléter sa phrase.

« ...de muqantar.

--- Voilà ! Veillez à ce que ce soit bien celles avec le plus de nœuds qui sont choisies. Il faudra aussi que deux ou trois d'entre vous aillent faire le tour des jardins, il y a plusieurs caravanes dans le coin et donc toujours des marauds qui espèrent grapiner. »

Il allait s'éloigner, laissant les gardes s'organiser pour la suite, puis se ravisa et appela le silence d'un geste.

« Je ne sais ce qu'il en est ici, mais après-demain commence Carême, et chez moi les hommes ont usage de faire bombance à cette occasion. D'aucuns boivent plus que de raison ou s'abandonnent à la griserie de la fête. Cela n'est pas bien grave, mais il faut éviter que cela ne finisse par causer soucis. »

Autour de lui, les soldats retinrent un sourire devant sa méconnaissance du pays. Les chrétiens locaux observaient pour la plupart un Carême qui avait débuté la veille. Il n'allait donc guère y avoir de fauteurs de troubles, sauf si un pèlerin s'était perdu dans les parages, ce qui était peu probable. Les cités maudites qui entouraient la Mer morte n'étaient guère attrayantes pour les marcheurs de Dieu en quête des lieux saints.

### Segor, souk au sucre, début de soirée du samedi 8 juillet 1161

Il se trouvait toujours des négociants pour traîner le soir alors qu'ils avaient eu toute la journée pour faire leurs affaires. Ils continuaient à discuter, pinailler, argumenter tandis qu'on les poussait vers la sortie. Seuls les valets et les domestiques étaient pressés de rentrer chez eux, de fermer les éventaires et de voir les boutiques enfin closes.

Après une année à courir les jardins pour éviter les chapardages, Habib avait été promu au rang de premier gardien du souk au sucre par l'entremise d'un de ses oncles. L'épée toujours au côté, il assistait le nouveau mâalem[^49], qui supervisait les échanges, percevait les droits et taxes et surtout récupérait le fruit de la vente des exploitations royales, avant de le verser au châtelain. C'était un jeune homme au caractère fantasque, fils d'une bonne famille qui avait ses entrées auprès des seigneurs latins locaux. Il avait été amusé à l'idée d'avoir une personne du nom de Habib comme serviteur, vu que lui s'appelait Shabib. Leur duo n'avait pas tardé à s'attirer la sympathie des familiers des lieux, habitués aux routinières et peu aimables façons du vieil Ahmed, mort dans l'hiver précédent d'une mauvaise fièvre.

Habib suivait donc son chef, fermant les boutiques à l'aide de clefs passées sur un gros anneau de fer que le mâalem lui confiait le temps d'opérer. C'était aussi là que pendait la grande clef du souk qui ouvrait les serrures des larges vantaux qu'on verrouillait chaque soir. Nul ne pouvait alors plus pénétrer dans la zone, ses abords étant gardés par un concierge logé dans un guichet à l'angle de la belle demeure de Shabib, assisté d'un chien porté aux grognements, mais qui n'aboyait jamais.

La journée avait été calme, malgré la venue de quelques négociants en route pour les côtes levantines. C'était la période des plantations et les responsables d'exploitations se concentraient sur ce travail délicat. Les affaires se faisaient sans entrain, alimentées par les entrepôts des commerçants spécialisés et des spéculateurs tentés de jouer sur les variations saisonnières de prix.

Un scribe au service de Shabib les suivait, portant un coffret lourdement ferré où étaient conservés billets à ordre et lettres de créance, ainsi que quelques rares liquidités. La marchandise était bien trop onéreuse pour se négocier avec des monnaies, et tout se faisait généralement par échanges scripturaires, parfois validés par un gage de bourses scellées.

Habib laissa le mâalem et son assistant à la grande demeure puis prit la direction de la maison familiale. Il avait pu s'y aménager un coin à lui dans un des anciens débarras. Il n'y avait qu'une minuscule ouverture donnant par un claustra sur la cour intérieure, mais il était au rez-de-chaussée et pouvait aller et venir comme bon lui semblait. Surtout que désormais son grand-père avait estimé qu'il méritait sa propre clef de la petite porte latérale, étant donné son travail important dans la cité.

La nuit tombait vite, de trop rares éclairages autour de certains lieux publics, bains et mosquées, permettaient d'y voir par moment, mais Habib connaissait parfaitement le chemin. Il avait espéré avoir le temps de s'arrêter acheter quelques douceurs au miel pour leur dessert, mais une fois encore il était trop tard. Lorsqu'il referma la petite porte derrière lui en entrant dans la vaste demeure, les ténèbres avaient remplacé les ombres et la chaude lueur des lampes à huile l'accueillit.

Il salua ceux qu'il croisa, esquivant les enfants occupés à se courir après dans les couloirs malgré les récriminations des femmes. Il aperçut les lumières à l'étage, dans le majlis[^50], où les hommes de la famille, ses oncles, son grand-père abu Talib et ses cousins devaient échanger les nouvelles de la journée en attendant le souper. Il se dépêcha d'aller se rafraîchir au bassin avant de se savonner correctement les mains, puis il enfila des savates d'intérieur.

Lorsqu'il pénétra enfin dans la salle à manger, la nourriture était servie. Abu Talib, qui n'aimait pas être dérangé pendant son repas, lui lança un regard impératif, l'invitant à s'installer au plus vite s'il voulait éviter ses foudres. Il était à peine assis que la voix rocailleuse s'éleva.

« Mangeons ! »

### Segor, souk au sucre, début d'après-midi du mercredi 8 août 1161

Avec la chaleur assommante, Habib somnolait, installé sur un tabouret aux abords de l'office où Shabib recevait les négociants. Imitant le Bourdon à la sucrerie, il pouvait ainsi à la fois surveiller les entrées et sorties et ce qui se passait dans les boutiques. Le midi en plein été, chacun demeurait à l'ombre et certains marchands tendaient même une bâche devant leurs produits pour profiter du calme le temps d'une sieste.

Il fut donc surpris de voir s'avancer vers eux un petit homme à la longue barbe grise dont les atours trahissaient l'aisance. Il était écarlate en raison de la chaleur, mais ses manières et sa tenue montraient le personnage d'importance. Il était suivi d'un valet portant des paquets et un gros sac de cuir et se déplaçait avec énergie sans pour autant sembler agité. Il se figea devant le jeune homme, qui sauta sur ses pieds, tirant sur son thawb pour lui donner meilleure apparence. Il le salua avec respect et demanda qui il devait annoncer au mâalem.

« J'ai nom Halfon al Fustani abu Harith, ton maître me connaît. »

Sa voix était douce et ferme, mais sans cette inflexion de brusquerie qu'on rencontrait parfois chez les puissants. Il sembla immédiatement sympathique à Shabib. Il n'eut d'ailleurs pas le temps d'aller l'annoncer que Shabib s'avançait, les bras levés comme s'il recevait un frère. L'homme était visiblement plus que bienvenu et rapidement une petite collation amicale fut proposée et présentée. Ayant retrouvé son poste à l'entrée, Habib ne perdait pas une miette des échanges, même s'il aurait bien apprécié de pouvoir en partager les douceurs des mets.

« Quel plaisir de vous voir, abu ! Nous ne nous sommes encontrés depuis si longtemps !

--- Depuis notre soirée en Asqalan[^51], voilà plusieurs saisons de cela. Tu commerçais alors encens, étoffes et poivre.

--- Plus besoin pour moi de courir les chemins et les mers, je gère le souk, ici, désormais. »

Le vieil homme approuva aimablement de la tête.

« Vous êtes en route vers al-Misr[^52] ?

--- Si seulement ! Non, je suis en route pour Eilat, où je dois prendre un navire pour quelques affaires au loin.

--- Rien de funeste, j'espère.

--- Non, juste des dépôts qui ne sont pas gérés ainsi qu'ils le devraient, mais rien d'inquiétant. Par contre, alors que je passais tantôt à al-Quds[^53], j'ai appris des choses qui devraient vous intéresser, ici.

--- À propos du roi Badawil ?

--- En partie, oui. Zughar n'est plus sienne, la haute Cour en a attesté voilà quelques jours. Il l'a donnée en fief au sire Philippe de Naples[^54], qui lui a concédé ses terres au mitan du royaume en échange. Il faudrait donc d'ailleurs le nommer *Philippe de Montréal*, ou *prince d'Outrejourdain*. Cela sera certainement proclamé d'ici peu. »

Shabib en resta sans voix un moment, avala un peu de lebné avant de répondre.

« Il est de la parentèle du vieux Maurice, non ? Ce serait de coutume qu'il hérite de ces provinces, chez les Celtes.

--- Aucune idée, mon ami.

--- Quoi qu'il en soit, le roi a de nombreuses terres directement, qui lui procurent bons revenus, je n'encrois guère qu'il se déposséderait de pareils joyaux.

--- Cela m'étonnerait aussi. Mais la justice sera en une autre main et certaines taxes iront à d'autres coffres... *Si vous entrez parmi les borgnes, fermez un œil*. »

Il assortit sa dernière remarque d'un clin d'œil appuyé. En tant que juif, il n'y avait aucun lieu où des coreligionnaires régnaient. Il était donc toujours d'une grande circonspection, particulièrement dans les zones sous contrôle des Latins, dont la tolérance n'était pas la plus avérée des qualités.

« Le roi a pourtant construit de nouvelles installations voilà peu, Habib y a travaillé quelques mois avant de me rejoindre. Badawil devait savoir qu'il allait donner à fief ces terres à ancien adversaire, partisan de la vieille Mélisende. »

Habib sursauta de s'entendre ainsi interpelé, mais il n'eut pas le temps de réagir que les deux hommes continuaient leur discussion. Il se contenta donc de hocher la tête ostensiblement.

« On la dit au plus mal depuis le printemps, indiqua abu Harith. Peut-être profite-t-il de voir sa mère grabataire pour régler ses comptes...

--- *Qui a le pouvoir doit plus qu'un autre pardonner*. Le sire de Naples est grand guerrier, tout le monde le sait. Le voilà qui règne désormais sur importante marche, entre le bilad al-Sham[^55] et al-Misr.

--- Il mènera pourtant moins de lances à la bataille avec ce moindre fief.

--- Peut-être que tout cela vise à porter fer et feu dans nos provinces, à y lever plus de troupes ?

--- Puissent le sabre et la flèche demeurer loin de vos murs, mon ami. Bien trop de sang est déjà versé pour leurs guerres sans fin. » ❧

### Notes

Le sucre est une denrée particulière au Moyen Orient à l'époque médiévale, car c'est une marchandise qui demande beaucoup d'investissements en termes d'infrastructure et de main d'œuvre tout en étant très rentable. Seuls les plus riches pouva\^ient donc se lancer dans sa production, généralement les monarques.

L'échange de terres qui s'est déroulé pendant l'été 1161 entre Baudoin III et Philippe de Milly demeure un mystère. Pour quelle raison ce proche de la reine mourante aurait-il abandonné ses propriétés de Naplouse pour un fief deux fois moins puissant militairement ? Ceci en plus du fait que le roi avait amputé une partie des revenus de cette province, afin d'en bénéficier directement.

Cela pouvait être en rétorsion des affronts subis quelque dix ans plus tôt par le jeune monarque, quand il s'était affranchi de la tutelle de sa mère. Mais c'était aussi confier une zone stratégique à un homme dont personne, pas même ses opposants, ne contestait les qualités militaires. Il avait donc l'opportunité d'y briller par quelque haut fait d'armes. Je n'ai pas la réponse scientifique à cette question, mais ma proposition d'interprétation des sources historiques alimentera un des arcs narratifs dans le tome à venir des enquêtes d'Ernaut, *La rivière du diable*.

### Références

Taha Hamdan, « Some Aspects of Sugar Production in Jericho, Jordan Valley »

Taha Hamdan, « The Sugarcane Industry in Jericho, Jordan Valley »

Ouerfelli Mohamed, *Le sucre. Production, commercialisation et usages dans la Méditerranée médiévale*, *The Medieval Mediterranean Peoples, Economies and Cultures, 400---1500*, volume 71, Brill, Leiden-Boston, 2008.

*The Origins of the Sugar Industry and the Transmission of Ancient Greek and Medieval Arab Science and Technology from the Near East to Europe. Proceedings of the International Conference. Athens 23 May 2015*

RRR - Revised Regesta Regni Hierosolymitani Database, http://crusades-regesta.com no. 670 (RRH 366), accédé le 20 janvier 2020

Le persan
---------

### Abords de Qasr-e Chirin, matin du youm el itnine 11 rabi' al-awwal 555[^56]

Barmak fut réveillé en sursaut par le bruit d'une cavalcade aux alentours de la tente. Au peu de lumière qui filtrait au travers de la toile, l'aurore pointait à peine. Il perçut le froid sur son visage, n'osa pas s'extraire du doux cocon de ses couvertures. Le braséro au milieu de la pièce crépitait, mais était bien loin de suffire à maintenir une température agréable. Il aperçut un de ses domestiques qui s'avançait pour lui signaler l'heure du réveil et apporter ses vêtements chauffés, prêts à être passés. Sans un mot, il le congédia d'un regard peu amène. Les voyages le mettaient toujours dans une humeur massacrante et il détestait qu'on le voie au lever avant qu'il n'ait le temps de s'apprêter.

Prenant son courage à deux mains, il sortit de son lit et enfila rapidement un confortable kaftan de laine par-dessus sa qamis. Il passa également des chaussettes épaisses puis des bottes fourrées, finement ouvragées. Enfin, il posa sur sa tête un bonnet de feutre recouvert d'un long turban de soie rouge à rafraf brodés. Il détestait encore plus faire ses ablutions dans le froid qu'y renoncer. Il se contenta donc de brosser sa barbe et sa moustache, de se laver les mains et de se rincer le visage. Tandis qu'il finissait ses préparatifs, il appela son serviteur et donna des ordres pour qu'on lui apporte de quoi se restaurer après la prière. Puis il se rendit rapidement à la zone utilisée comme mosquée dans la tente de son maître Sufyan al-Katib.

Tandis qu'il traversait les espaces non chauffés, son souffle dansait en volutes de fumée, le gel lui mordait les joues. Ils étaient récemment arrivés aux abords des montagnes ; ils avaient quitté Bagdad juste à la fin de safar pour accomplir une mission au nom du calife Al-Mustanjid. Barmak était un fonctionnaire, un secrétaire au service de Sufyan al-Katib, lui-même un familier du grand vizir. Ils avaient pris la route pour Hamadan[^57], car l'angoisse rongeait le palais depuis la mort du précédent sultan seldjoukide Muhammad ibn Mahmud. Théoriquement au service du calife, ce puissant dirigeant était en fait indépendant et pouvait devenir un réel ennemi, comme l'avait prouvé le siège de Bagdad quelques années plus tôt. Le choix du candidat était donc crucial et, si Al-Mustanjid ne pouvait le nommer ni même influencer sa désignation, il lui fallait surtout apprendre à le connaître afin de le circonvenir au mieux.

La famille de Barmak était originaire d'Eṣfahân[^58], mais avait fui devant les armées conquérantes seldjoukides au siècle précédent, pour se réfugier à Bagdad. Il faisait partie de la nouvelle vague de fonctionnaires qui ne voyait pas d'un très bon œil le contrôle qu'exerçaient les différents peuples turcs sur le pouvoir central abbasside. Il méprisait autant les Seldjoukides que les Kurdes ou les Turcmènes, qu'ils estimait soit barbares, soit dégénérés ou décadents, et se sentait plus d'affinités avec les anciennes dynasties arabes, qu'il trouvait plus civilisées, quoique trop attachées à leurs valeurs nomades.

Quand il entra dans le lieu de prière, il se déchaussa et prit sa place au milieu des familiers du puissant al-Katib. Celui-ci était assez âgé et sa longue carrière aux côtés du grand vizir Awn al-Din ibn Hubayra inspirait le respect à tous les jeunes ambitieux qui rêvaient d'occuper son poste. Barmak appréciait ce privilège, même s'il devait supporter les crises de colère lorsque le travail n'était pas accompli exactement comme le vieil homme l'avait décidé. C'était un lettré hors pair, habile dialecticien, qui savait tourner les phrases de belle manière et qui connaissait les usages et la titulature sur le bout des doigts. Barmak apprenait beaucoup depuis qu'il était à son service et espérait qu'on lui confierait un jour la rédaction complète d'un texte et pas simplement la retranscription de ce qui lui était dicté.

La prière était un instant privilégié entre tous, où le temps se suspendait, que Barmak goûtait particulièrement, loin de la frénésie bureaucratique et des intrigues de palais. Mais le moment de grâce était toujours trop court à son goût. Les chaussures à peine enfilées, les discussions politiques renaissaient. Un courrier était arrivé dans la nuit et ils savaient désormais qui était pressenti pour devenir le futur sultan. C'était Sulayman Shah et il y avait un petit souci à sa prise de fonction : il était prisonnier de Qutb ad-Dīn de Mossoul, le frère de Nūr ad-Dīn, l'émir de Damas et Alep, qui menait le jihad contre les envahisseurs celtes, vers les côtes occidentales. Il fallait repartir au plus vite et découvrir comment les choses allaient se mettre en place.

### Abords de Tikrit, bords du Tigre, midi du youm al had 4 jadjab 555[^59]

Le repas était très animé, les participants se divisant en deux factions aux opinions divergentes. Barmak était de ceux qui estimaient que l'atabeg Nūr ad-Dīn avait des visées sur les territoires à l'est et qu'il allait pour cela s'appuyer sur son frère Qutb ad-Dīn, mais un autre groupe de fonctionnaires, dont Abu al-Kabir était le meneur, était persuadé qu'il n'y avait rien à craindre de lui, qu'il se soumettrait au légitime dirigeant de tous les musulmans, Al-Mustanjid.

« Je n'arrive pas à vous comprendre, vous soutenez ce Turk alors qu'il tente d'interférer avec nos ennemis ! Il a bien fourni des hommes pour nous assaillir, voilà peu, non ?

--- Il ne pourrait désobéir au calife, tout atabeg qu'il soit. Il ne peut facilement s'aliéner ceux qui ont des familles influentes.

--- Et pour quels motifs entretient-il des relations avec les Kurdes et les Turcomans de la frontière ? Il sait pourtant qu'ils ne tombent pas sous son pouvoir.

--- Ces barbares sont tous parents, et se reproduisent comme des chacals en rut, mais de là à penser qu'ils peuvent s'allier... Les deux frères n'ont-ils pas failli s'entretuer pour le contrôle de Mossoul à la mort de Saif al-Din Ghazi ?

--- Ne disent-ils pas *Moi contre mes frères ; mes frères et moi contre mon cousin ; mes cousins, mes frères et moi contre le monde* ?

Son adversaire fit la moue à cette habile réplique, ne trouvant pas quoi objecter. Barmak se détendit. Avisant d'un coup d'œil circulaire ses collègues, il savait qu'il avait gagné. Il s'empara d'une bonne portion de haljamiyya[^60] en souriant d'un air satisfait.

Tandis qu'il savourait sa victoire, un serviteur vint lui dire quelques mots à l'oreille. Il devait rejoindre la salle principale au plus vite, car al-Kabir avait besoin de lui. Se rinçant les mains dans une cuvette d'eau parfumée, Barmak donna quelques ordres pour qu'on lui apporte de quoi écrire au cas où il lui faudrait prendre des notes. Il se demandait ce que cette rencontre cachait.

Alors qu'il s'asseyait à sa place, légèrement en retrait sur le côté du salon d'audience de la tente, il jeta un coup d'œil au groupe qui entrait. Il fut soulagé de voir que ce n'étaient que des Bagdadis, certainement porteurs d'instructions et de messages personnels de leurs familles. Il n'aurait pas à entendre encore ces langues gutturales et ces phrasés râpeux des soldats de métier.

Les nouvelles qu'al Kabir transmit aux émissaires étaient un peu déconcertantes pour eux, mais pas inquiétantes pour autant. Qutb ad-Dīn, émir de Mossoul et frère du grand Nūr ad-Dīn, avait accepté de libérer son prisonnier, Sulayman Shah, s'il était lui-même nommé atabeg de ce dernier, avec son fidèle Zayn ad-Dīn comme chef des armées. Il comptait donc devenir l'homme fort du régime, agissant dans l'ombre de sa marionnette. Mais celui qui se voyait déjà généralissime avait finalement été contraint d'abandonner son otage, alors qu'il l'escortait pour Hamadan. Tellement de partisans du futur sultan avaient rejoint sa caravane que Zayn ad-Dīn avait craint pour sa sécurité et avait rebroussé chemin, trahissant les espoirs de son maître. Les Bagdadis et les Persans ne cachèrent que peu leur soulagement à l'idée que la famille du zengîde ne s'empare pas du pouvoir aussi aisément.

### Jalula, bords du Tigre, soir du youm al sebt 23 jha'ban 555[^61]

Barmak retira sa jubba doublée de fourrure qu'il abandonna à un serviteur et s'assit sur un tabouret pour qu'on lui ôte ses bottes, qu'il remplaça par des savates délicatement brodées. Il était furieux d'avoir dû sortir sous la pluie, dans la boue et le froid. Il n'appréciait déjà pas de devoir vivre sous tente, mais aller se balader dans le camp malodorant et fangeux était au-dessus de ses forces.

Il avait dû calmer les esprits, car une querelle avait éclaté entre des membres de leur escorte et des soldats damascènes. Le ton avait monté et un homme avait été frappé au visage d'un coup de savate[^62], ce qui avait entraîné une bagarre, heureusement sans conséquence grâce au sang-froid des émirs des deux côtés. Les coupables avaient été punis et la situation était redevenue calme.

Barmak laissa s'échapper un soupir en s'asseyant sur son suffah[^63], tirant un manteau de laine sur ses épaules. Il repensa à tous les pourparlers qui s'éternisaient et sentait bien l'hostilité des généraux mossouliotes, aleppins et damascènes à leur égard. Sous les formules aimables, il percevait la tension, car les Turcs prétendaient servir de leur mieux le pouvoir du calife et l'orthodoxie sunnite. Ils mettaient en avant leurs créations de nombreuses écoles pour assurer l'encadrement des étudiants, les donations en waqf[^64] qu'ils multipliaient, leur engagement dans le Jihad contre les infidèles. Mais pour lui leur ambition crevait les yeux, Nūr ad-Dīn et les siens s'appuyaient sur la religion pour justifier leur expansion.

Alors qu'ils n'étaient que des nomades va-nu-pieds il y a encore peu, ils se pensaient égaux, voire supérieurs, à des familles prestigieuses de Bagdad. Leur arrivisme déconcertait Barmak et le rendait furieux en même temps. Il savait qu'il était nécessaire de recourir à des peuples naturellement belliqueux pour faire les guerres, mais il répugnait à devoir les fréquenter. Il aurait fallu pouvoir se débarrasser d'eux après usage et ne pas leur concéder la moindre parcelle de pouvoir.

En fin d'après-midi, un messager rapide était venu informer al-Katib de la mort du nouveau sultan Sulayman Shah. Celui-ci, qui faisait déjà proclamer la khutba en son nom, s'était comporté de façon tellement odieuse qu'il s'était vite aliéné tout son entourage. On disait qu'il avait été empoisonné, l'assassin ayant pris soin de le punir par là où il péchait, en abusant du vin, et ce en pleine journée de ramadan. Barmak y voyait la dégénérescence de ces peuples turbulents, incapables de savourer les bonheurs de la vie avec parcimonie et expertise, ne goûtant les breuvages que dans l'ivresse et le moindre plaisir que dans la violence des passions. Mais au moins cela provoquerait-il peut-être le retour prochain à Bagdad de son maître Sufyan al-Katib.

Il était las de ces déplacements et n'aspirait qu'à retrouver sa magnifique demeure. Il ne se lassait pas d'y recevoir musiciens et poètes, au milieu d'objets commandés à des artisans de renom. Il avait fait spécialement aménager une partie du qa'a[^65] pour y héberger une immense volière où il gardait de splendides oiseaux aux plumages colorés et aimait entendre les exclamations de surprise et de joie de ses invités lorsqu'il leur dévoilait ses plus belles créatures.

Il appela pour indiquer qu'il prendrait son repas du soir dans la section de tente qui lui était réservée. Et il demanda à ce qu'on trouve un autre braséro pour la nuit, qu'un seul ne lui suffisait décidément pas. Il se pencha et sortit d'un petit coffre marqueté son nécessaire à écriture qu'il déploya devant lui. Puis il glissa hors de sa ceinture le mince carnet où il notait les vers qui venaient parfois à son esprit, inspirés par les grands auteurs. La journée passée, de nouveau pleine d'inconnu, lui remémora un de ses poètes favoris, dont les textes de temps à autre licencieux choquaient autant qu'ils ravissaient, al-Khayyam Nishabouri :

« \*Contente-toi de savoir que tout est mystère :

la création du monde et la tienne,

la destinée du monde et la tienne.

Souris à ces mystères comme à un danger que tu mépriserais.\* »[^66]

Souriant pour lui-même tandis qu'il récitait les vers, il se versa une coupe de douqh[^67] bien frais. Il commençait à avoir faim et entama des petits gâteaux au riz, au safran, à la pistache et au miel en attendant qu'on lui apporte le souper. Il s'allongea nonchalamment sur les coussins, réfléchissant aux rimes du quatrain qu'il s'efforçait de composer. ❧

### Notes

J'avais toujours eu en tête de ne faire intervenir les Persans et les proches du pouvoir califal que tardivement, apportant un éclairage oriental à Hexagora, ainsi qu'un changement de focale. Les différents princes musulmans n'étaient pas forcément inquiets des croisés arrivés à la fin du XIe siècle et il me semblait intéressant de montrer que les chrétiens n'étaient qu'une des nombreuses composantes de la géopolitique multilatérale du Moyen-Orient médiéval. Barmak me fait en outre la joie de me permettre un jeu de mot tellement facile et puéril pour cette centième publication de Qit'a que je n'ai pu me l'interdire.

Le monde musulman est en proie à de nombreux changements en ce début de seconde moitié du XIIe siècle, avec la dislocation du grand sultanat seldjoukide qui fait écho à la déliquescence de la puissance califale abbasside et l'effondrement fatimide. Les peuples turcophones, seldjoukides, kurdes... constituent les nouvelles forces vives et s'insèrent peu à peu aux échelons principaux du pouvoir. Les populations inféodées restent bien évidemment les mêmes, comme cela se passe sur les territoires sous contrôle latin.

### Références

Bosworth Clifford Edmund, *The History of the Seljuq State. A translation with commentary of the Akhbar al-dawla al-saljuqiyya*, Routledge : Oxon & New York, 2011.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Les murailles de Turbessel
--------------------------

### Forteresse de Turbessel, fin d'après-midi du jeudi 22 avril 1148

Le jeune homme avait les joues en feu d'avoir couru tout l'après-midi, occupé à s'affronter à divers jeux de force dans les environs avec ses camarades. Il grimpa la volée d'escalier qui menait au passage longeant les créneaux sans effort. Il ne ralentit qu'en découvrant son père Joscelin, le comte d'Édesse, appuyé sur la muraille grise, le regard perdu au loin. À côté de lui se tenait, immobile, le messager arrivé quelques instants plus tôt sur un roncin éreinté. La mine austère et l'air pincé du comte n'auguraient rien de bon, d'autant qu'il avait près de lui un de ses cruchons de vin qui ne le quittait guère depuis quelques mois.

Joscelin le jeune prit le temps de retrouver son souffle, ajusta sa cotte, espérant lui redonner un aspect décent en tirant dessus et en la frottant rapidement des plus grosses taches de poussière. Puis il s'avança, d'un pas prudent.

« Enfin tu te présentes à moi ! Je t'ai fait mander voilà bien long temps ! Penses-tu qu'il sied à fils de comte de courir les chemins comme manant ? »

Son père le toisait d'un regard courroucé, ses yeux injectés de sang et son dodelinement de tête trahissant l'abus de boisson. Le jeune homme se contenta de baisser le menton, se préparant à affronter la tempête sans un mot.

« Ta mère me tance que tu t'adonnes à jeux de vilains ! À quoi as-tu donc passé ta journée ? Que faisais-tu ?

--- Nous avons joué au cheval fondu avec Kostan, Godebert et Sempad. »

Le comte le foudroya d'un œil sévère.

« Me prends-tu pour nigaud à ne citer que fils de loyaux chevaliers de mon hostel ? Il s'en trouve bien plus de compères pour pareil amusement... »

Il grogna, s'accorda une gorgée de son pichet, lança un regard sur le messager, toujours immobile à ses côtés, comme s'il le découvrait. Puis il le chassa d'un geste agacé de la main, avant d'avaler encore un peu de vin. Sa voix se fit moins dure quand il reprit la parole.

« J'ai espoir que tu as gagné, au moins. Que nos gens sachent qui sont les maîtres. »

Le jeune homme acquiesça, gardant le silence. Son père l'invita à ses côtés et lui montra le paysage qui s'ouvrait devant eux à l'est, par delà les collines qui bordaient la vallée du Sadjur.

« J'ai parfois l'impression que le vent me porte les senteurs des jardins de l'étang[^68]... Ce matin il m'a bien semblé entendre sonner la grande cloche de Saint-Jean... »

Il se tourna vers son héritier, le visage déformé par la colère et le dégoût.

« Au final, les barons de France, de Flandre, de Franconie ne vont rien faire pour nous ! Ce mâtin de Raymond[^69] a réussi à faire fuir ce moine de Louis[^70]. Il aurait besogné sa propre nièce, le fils à putain ! Ils préfèrent les jardins de Damas à nos montagnes, dirait-on, et ils escomptent se tailler beau royaume plutôt que nous venir en aide. Seule Mélisende, qui a du sang des nôtres, comprenait cela, mais elle n'a rien pu faire...[^71] »

Il secoua la tête, s'abandonnant au dépit, les yeux larmoyants. Il se chercha un peu de réconfort dans la boisson, s'essuya la bouche avec agacement, comme si ses lèvres le démangeaient.

« Je ne vais pas me contenter de Marasse, Cresson, Bile et Hazart, que croient-ils ? Je vais repasser l'Euphrate et reprendre ce qui est mien. Et tu chevaucheras à mes côtés, fils. Qu'ils voient que nous n'allons rien céder. Bientôt nous presserons les grappes de Sarudj entre nos doigts pour en goûter le suc ! »

Il posa un bras protecteur sur les épaules de Joscelin, le serrant contre lui avec vigueur.

« Bientôt nous arpenterons les doubles murailles de Roha[^72], notre ville... »

Le jeune homme souriait de voir son père reprendre ainsi confiance, défiant leurs ennemis invisibles, cachés dans les replis montagneux vers l'orient et le nord. Il avait déjà chevauché à ses côtés et n'aspirait à rien de plus que de se montrer à la hauteur de cet homme irascible et vindicatif, batailleur et sanguin, au courage indomptable.

### Forteresse de Turbessel, veillée du lundi 26 septembre 1149

Le jeune Joscelin aimait venir en soirée échanger avec les sentinelles, partager les anecdotes des vieux guerriers, faire rouler le dé à l'occasion, fraternisant avec les simples soldats. La lune presque inexistante en son dernier quartier n'occultait que de rares étoiles dans son entourage proche et un air sec et froid permettait d'admirer leur scintillement. Quelques nuages effilochés depuis l'ouest grignotaient de ténèbres le ciel et annonçaient de la pluie. Au sommet d'une des tours, un brasero maintenu bas offrait l'occasion de se réchauffer les mains. On y plongeait les flèches destinées à embraser les machines des attaquants s'ils osaient se mettre à portée, ce qui n'était guère arrivé jusqu'à présent.

Cela faisait plusieurs jours que les seldjoukides de Konya, menés par le fils du prince Mas'ud, Kilig Arslan, établissaient leur tentes et leurs engins d'assaut autour des murailles de Turbessel. Ils avaient déjà pris Marash et semblaient bien décidés à ne rien laisser du comté aux souverains latins. Des feux de camp parsemaient la plaine et les rives du cours d'eau. Il était d'usage d'en entourer les abords d'une citadelle assiégée afin de paraître plus nombreux qu'on ne l'était vraiment. Même sans cela, le moral des défenseurs était au plus bas. Nul renfort n'était prévu et leurs maigres ressources semblaient dérisoires face aux ennemis qui dévoraient la province au fil des mois. Depuis qu'il était en âge de suivre les opérations militaires de son père, Joscelin n'avait vu que des retraites et des replis, l'abandon de forteresses et de villes, au profit de leurs adversaires musulmans. On aurait dit que les montagnes en étaient emplies, tellement ils se pressaient, innombrables, à leurs portes.

« Tu viens préparer les défenses pour demain, fils ? »

La voix éraillée du comte Joscelin semblait déchirer les sons veloutés de la nuit. Emmitouflé dans un manteau doublé de fourrure, il s'avança vers son fils. Son haleine puait le vin à plusieurs pas.

« Tu aimes à cracher sur ces criquets, toi aussi ? Qu'ils viennent donc nous chercher, que je les transperce de ma hampe ! »

Il s'assit sur un des créneaux, s'adossa à un des merlons.

« Ils espèrent nous effrayer avec leurs lueurs nocturnes, mais c'est là ruse d'enfant. Les vrais adversaires avancent toujours dans l'ombre, mon garçon. Quand je pense que nous avons tendu la main à Kara Arslan, lorsque le prince d'Alep le menaçait... Voilà maintenant que l'héritier de Sanguin[^73] s'entremet pour nous aider face à Hisn kayfa et Kharput. »

Il se tourna vers son fils, le fit s'approcher.

« Tu étais là, ce tantôt, lorsque ce messager nous a déclamé l'offre du seigneur d'Alep. Qu'en dis-tu ?

--- Allons-nous accepter ? Il semble bienveillant à notre égard, non ?

--- N'aie aucune fiance envers ces serpents. Si Noreddin désire la paix avec nous, ce n'est pas pour nous sauver, mais pour éviter que son ennemi Kara Arslan ne devienne trop fort. Il n'est pas encore prêt à venir moissonner ici, tout à ses propres affaires. Il ne nous tend la main que pour nous étouffer en son temps.

--- Allons-nous refuser, alors ? »

Le comte Joscelin soupira longuement, lança un sourire crispé à son fils.

« J'ai besoin de répit, même s'il m'est vendu fort cher. Antioche est bien bas, mais au moins sommes-nous débarrassés de ce maudit Raymond. Seulement, rien ne viendra de nos cousins du sud. Il me faut jouer l'un de mes ennemis contre l'autre, comme le vieil Anur[^74] a su si bien le faire, se gaussant de nous comme de Sanguin[^75].

--- Alors ne pourrons-nous poindre contre ces misérables, devrons-nous traiter comme Griffons[^76] tortueux ?

--- Ah, que voilà bien mon sang qui s'exprime par tes lèvres ! Aurais-je une pleine échelle de bacheliers tels que toi et nous bouterions cul par-dessus tête tous nos adversaires. Mais la vaillance et le bras du noble guerrier ne suffisent plus en ces temps. Il faut y ajouter pointes de fourberie et de rouerie, marchandages comme ignoble négociant de fripes... »

Il se pencha, à presque toucher le visage de son fils de son front, chuchotant d'une voix lourde, empâtée par l'alcool.

« Ici, c'est Turbessel[^77], la forteresse de ton grand-père, le fief de notre famille. De là nous déferlerons sur nos ennemis, lance au poing. Rien ne nous arrêtera, je te le jure. Et s'il me faut faire bonne figure face aux serpents durant un temps, que cela soit. Je leur ferai ravaler leurs sourires quand j'aurais retrouvé ma selle et ma hampe de frêne. »

Joscelin le jeune opina avec passion, grisé par le charisme de ce père dont il sentait bien les failles, malgré toute la vaillance et les bravades. Il l'avait déjà vu piquer des deux, renversant ses adversaires comme fétus insignifiants. Il savait que rien ne pouvait le vaincre quand il brandissait sa lance. C'était un homme de guerre, qui ne se plaisait que l'épée en main, le heaume sur la tête.

### Forteresse de Turbessel, matin du mardi 15 août 1150

La grande salle d'audience résonnait, vide de ses tapis, tentures, meubles et coffres. Seule demeurait la chaise curule sur l'estrade, siège du seigneur des lieux. Joscelin avait néanmoins insisté pour emporter celle de sa famille, la remplaçant par un fauteuil habituellement attribué aux invités de marque. Il faisait les cent pas, toujours réticent à obtempérer aux injonctions de sa mère Béatrice. En retrait, sa sœur Agnès s'efforçait de se tenir à l'écart de la tempête, l'ayant vu couver au fil des jours jusqu'à l'explosion d'aujourd'hui.

« N'y a-t-il donc plus que des tonsurés en ces provinces ? Baudoin[^78] va-t'il passer le froc ? Qui est-il pour décider pour moi ce qu'il en est de ma terre ?

--- Silence, tu n'es pas encore comte ! Ton père est toujours vif !

--- Je le sais fort bien, mais au moins je ne m'empresse pas de le trahir, à peine est-il captif ! »

Béatrice foudroya son fils du regard et sa main partit avant même qu'elle ne le réalisât, claquant dans la salle vide comme le tonnerre. Abasourdi, le jeune prince écarquilla les yeux, outragé de l'affront, mais incapable d'y répondre, ne serait-ce que de la voix.

« Apprends à te taire, mon garçon ! Ton père a le sang bouillonnant et vois où cela l'a mené : en geôles sarrasines dont je le dois libérer. Et crois-tu que besants sortent du cul des vaches ? L'or byzantin servira à cela, à négocier son retour !

--- De quoi me parlez-vous ? Vous vendez nos places fortes à l'encan, il ne nous restera rien à défendre quand il sera de retour. Pensez-vous qu'il vous remerciera ?

--- *Tout fut à autrui, tout sera à autrui*. Je fais preuve du bon sens qui l'a déserté plus souvent qu'à son tour !

--- Comment osez-vous...

--- Je le dis, car j'en ai le droit. J'ai vu nos terres rongées par nos adversaires parce que ton père n'a jamais su faire profil bas, ne serait-ce qu'avec Antioche ! Puissé-je au moins t'enseigner cela : il est un temps pour chaque chose. »

Joscelin se frotta la joue, encore brûlante du soufflet. Il sentait crisser sous ses doigts le duvet qui affirmait aux yeux de tous qu'il devenait un homme.

« Je pourrais demander au roi quelques lances et soudoyer de quoi garnir nos forteresses, pendant que vous négocierez la libération de père.

--- Crois-tu que cela n'ait pas été envisagé ? Penses-tu un seul instant que je laisse ces terres de gaieté de cœur ? Au moins le basile[^79] griffon pourra les garder face à nos ennemis et il sera plus aisé de lui prêter hommage le moment venu, aux fins de les récupérer.

--- Prêter hommage pour franc-alleu que nous ne tenions de personne ? Père ne saurait tolérer cela !

--- En ce cas, il n'avait pas à se faire prendre. Il n'y a nulle excuse à la défaite quand on est souverain de ses terres. »

Joscelin se mordit la joue, la lèvre. Béatrice avait raison et, au final, il partageait plus ou moins son point de vue. Pourtant, il bouillait de constater que la bravoure de son père ne les avait menés qu'à la ruine. Il ne voyait pas où des erreurs avaient été commises, quels faux pas ils avaient faits, à quel moment ils avaient déchu de leur rang.

« Quoi qu'il en soit, je ne saurais admettre que Turbessel demeure en mains foraines. Je fais serment que nous y reviendrons, en maîtres.

--- Puissent Dieu et tous les saints t'exaucer, fils » souffla sa mère, soudain lasse.

Agnès s'approcha et attrapa la main de son frère, la lui serra en silence, un mince sourire sur son fin visage. Dans ses yeux, il vit qu'elle serait là, le jour où ils seraient de retour chez eux. Et qu'elle ne ménagerait pas sa peine pour faire advenir ce jour. Il la prit dans ses bras pour une longue étreinte.

### Jérusalem, palais des Courtenay, comtes d'Édesse, soir du jeudi 4 juillet 1157

La venaison en sauce caramélisée répandait ses effluves dans toute la pièce. Le jeune Josselin avait participé à une chasse les jours précédents et avait pu rapporter de l'antilope et quelques oiseaux. Cela le mettait toujours d'excellente humeur d'avoir parcouru la campagne, même s'il regrettait de ne pas galoper dans ses propres terres. Il s'était néanmoins fait à la vie de baron royal, membre de la Haute Cour pour les quelques domaines que Baudoin III lui avait données en fief autour d'Acre.

Il avait pourtant tout récemment perdu la Tour de Plomb, une forteresse du nord qu'il avait réussi à enlever aux musulmans d'Alep après un difficile siège. Il avait fait ensuite remonter les murs en scellant les maçonneries de plomb, espérant ainsi se garantir des assauts ultérieurs. Seulement, sa victoire fut de courte durée et la tête de pont depuis laquelle il escomptait lancer la reconquête du comté lui avait été rapidement reprise.

Leur mère ayant déserté le palais pour la côte, fuyant les grosses chaleurs du plus fort de l'été, il se retrouvait à table juste avec sa sœur Agnès. Bien qu'il ait toujours été proche d'elle, il la battait froid depuis quelques mois, depuis qu'il avait appris qu'elle fréquentait en secret le comte de Jaffa, Amaury, le frère du roi. Ce dernier était un homme porté sur le beau sexe, dont la réputation de coureur de jupons n'était plus à faire, et le fait qu'Agnès ait pu succomber à ses avances alors qu'elle était promise à un autre, c'en était trop pour le jeune Joscelin.

Ce soir, Agnès faisait pourtant de nombreux efforts pour nouer conversation, relançant la discussion aussi laborieusement que Joscelin la laissait s'éteindre. Elle finit par avouer pourquoi elle déployait tous ces trésors d'amabilité : elle avait une excellente nouvelle à annoncer à son frère.

« Tantôt, j'ai eu longue discussion avec le sire comte de Jaffa, il a été sensible à notre situation, et il va proposer à son frère de te nommer maréchal, Saint-Amand ayant été pris à Panéas le mois dernier[^80]. »

Joscelin fit la grimace, avala un morceau de pain, puis grogna d'une voix maussade.

« Tu sais que je n'aime guère apprendre que tu entretiens le sire comte de Jaffa. On parle déjà bien assez dans mon dos. Même les valets se gaussent de toi et de...

--- De quoi ? Que comprennent-ils à ma vie, à notre vie, Joscelin ? Il se trouve que le comte a quelque amitié pour moi, est-ce donc ignominieux que de le sensibiliser à nos soucis ?

--- Tu sais très bien de quoi je parle ! Je croise souventes fois Hugues de Ramla, ou ses frères. Que puis-je leur dire ? Ils nous ont fort bien traités et voilà que tu leur fais terrible affront !

--- Oui, eh bien, tu ne risques plus guère de voir Hugues...[^81]

--- N'ajoute pas l'irrespect à la trahison !

--- Et toi ne te crois pas sire en son fief, je suis ton aînée !

--- En l'absence de père, j'ai devoir de veiller sur toi et ta réputation. »

Agnès lui lança un regard qui aurait paralysé de peur n'importe quel inféodé, mais Joscelin était immunisé contre les œillades assassines de sa sœur. Voyant cela, elle se contenta de faire mine de se concentrer sur le délicieux plat. Elle en picora deux ou trois bouchées avant de revenir à la charge, mais d'une voix plus douce.

« Joscelin, je sais que tu désapprouves ma conduite, mais sois assuré que je ne m'abaisse en rien. Hugues a parfaitement compris et demeure un ami proche de mon cœur. C'est juste qu'il n'a plus la première place. Elle est pour Amaury, et il a pour moi tendres sentiments. C'est pour cela qu'il pense à moi, à nous. Il nous considère comme siens, et il veut par cette offre nous faire témoignage de son souci pour nos intérêts.

--- Alors pourquoi ne parle-t-il pas de mariage si ses intentions sont si nobles ?

--- Peut-être que la Haute Cour ne sait pas tout, mon ami. Il se dit entre lui et moi certaines choses qui n'ont pas à tomber dans toutes les oreilles. »

Interloqué, Joscelin scruta le visage de sa sœur, dans l'espoir d'y lire quelque indication sur ce qu'elle insinuait, sans pour autant l'affirmer. Peut-être dans la crainte de tenter le destin et de se faire cruellement punir.

« Soit. Passons. Mais je n'ai pas désir de m'installer ici, au rebours de toi. Père ne saurait entendre que j'ai abandonné tout espoir de reprendre Turbessel et nos terres.

--- Tu te lamentes souventes fois que tu n'as pas les moyens qu'il te faudrait. Apprends donc de l'expérience du vieil Onfroy[^82]. Sa science des arts de la guerre te sera utile. Tu pourras te faire bons camarades de bannerets et soudoyés. Emplis ton grenier des viandes avant de partir en chevauchée ! Jamais je n'attendrais de toi que tu te contentes de vivre de tes fiefs d'Acre. Mais il nous faut apprendre la patience, toute chose que père n'a pas su nous enseigner. »

Joscelin acquiesça, souriant pour la première fois de la soirée. Il avait eu tort de désespérer d'Agnès. Elle demeurait une femme forte, ambitieuse, et sa sœur aimante. Ensemble, ils ne redouteraient jamais aucun ennemi, ensemble ils viendraient à bout de l'adversité. Et ils parcourraient de nouveau les sentiers des rives du Daysan.

### Jérusalem, palais royal, chambre du roi Baudoin, matin du mercredi 8 février 1161

Avant les audiences officielles et sessions de la Haute Cour, Baudoin avait pris l'habitude de voir ses proches pour échanger avec eux. Il profitait généralement du matin pour recevoir dans l'intimité de sa chambre, en présence de sa jeune épouse. Comme elle n'avait qu'une douzaine d'années lors de leur mariage, ils avaient longtemps dormi séparément. Les années passant, ils avaient décidé de partager le même lit depuis quelques mois et tout le royaume espérait qu'un enfant naîtrait bientôt, garantissant la continuité de la dynastie.

Affable et facile d'abord, Baudoin était très familier avec ses invités, se comportant généralement de façon amicale et ouverte, se levant de temps à autre pour servir son épouse ou appeler un valet. Il avait demandé la veille à Joscelin de le rejoindre pour son en-cas du matin, fruits au vin et biscuits au sésame, car il voulait l'entretenir d'une idée qui lui était venue.

Le temps d'avaler la collation, Baudoin se contenta de parler de menus détails de leur vie de palais, de choses sans importance. Il s'était entiché d'un cheval et, sachant parfaitement que Joscelin était adepte de la chasse, lui avait proposé de partir un de ces jours tester la valeur de la bête. Tout en se caressant la barbe du geste machinal qui ne le quittait que peu, il échangeait sur la vision qu'ils se faisaient de la monture idéale pour courir le lion ou la gazelle. Puis, repoussant sa coupe, il fit un grand sourire à celui qui l'avait servi comme maréchal durant plusieurs années.

« Tu te doutes que je ne t'ai pas demandé de venir pour simplement parler destrier et palefroi, même si je t'y sais fort savant. »

Il assortit sa remarque d'un sourire complice.

« L'affaire qui me préoccupe est bien plus grave pour la couronne, et pour les provinces d'ici. Tu as sans nul doute appris le départ de Renaud de Saint-Valéry, qui tenait Harenc[^83] ? »

Baudoin savait très bien que nulle information concernant le septentrion n'était ignorée de Joscelin et n'attendit pas la réponse.

« Avec la capture du prince Renaud[^84], les terres du Nord se trouvent bien dégarnies. Là-bas, tous les barons d'importance ont été pris, tués ou mis hors d'état. Ce me serait d'une grande aide que tu considères de devenir sire d'Harenc. J'ai bien conscience que ce serait déchoir quelque peu peu pour toi, car cela demande de prêter hommage à Antioche.

--- Je n'ai pas le genou si roide que mon père, paix à sa mémoire.

--- Voilà ce qui me pousse à te prier d'accepter : je te sais féal et de bonne sapience. Il me faut des hommes tels que toi là-bas, pour garantir nos frontières avec le sire d'Alep, fort entreprenant. J'ai l'intention de proposer au patriarche Aimery de devenir bayle[^85] d'Antioche, le temps pour le jeune Bohémond de prendre les rênes. Mais je crains que la princesse Constance soit de la même étoffe que sa mère et n'y entende guère des intérêts autres que les siens.

--- Vous me faites grand honneur, mon sire. Je ne pourrais dire à quel point votre fiance me ravit le cœur.

--- Nous sommes désormais presque frères, par le mariage de ta sœur. Il me semble de raison que nous nous gardions les uns les autres en ces temps troublés. Te savoir à Harenc me procurera quelque répit en mes soucis.

--- Je ferai tout pour me montrer digne de vous, mon sire. »

Baudoin offrit un large sourire à Joscelin et leva sa coupe.

« Puisse cette première forteresse ouvrir la voie vers Roha, sire comte d'Édesse. » ❧

### Notes

L'histoire de la famille d'Édesse, les Courtenay, apparaît étroitement liée au destin de la couronne de Jérusalem à partir des années 1150. Pourtant, ce sont aussi les héritiers du premier territoire latin en Outremer, pétris de culture arménienne. La présentation qu'en fait Guillaume de Tyr en biaise la perception, et il me paraissait intéressant d'aller creuser un peu dans les vies de Joscelin et Agnès, dont les agissements seront si déterminants par la suite.

On brosse habituellement un portrait très noir de Josselin II, dont l'ombre a dû planer longtemps sur le destin du fils. Pourtant Josselin III ne donne pas l'impression d'avoir démérité au long de sa carrière, même s'il semble avoir été quelque peu en retrait. Il a néanmoins tenu des rôles importants, et le silence de Guillaume de Tyr sur ce point peut laisser penser qu'il n'y avait pas matière à critiquer, la plus petite occasion de le faire à l'encontre d'Agnès étant saisie avec empressement par l'évêque de Tyr. En outre, l'attitude de l'historien de la Terre sainte envers Renaud de Châtillon, dont il minimise les accomplissements, quand il ne les attribue pas carrément à d'autres, peut inciter à la prudence quant à l'effacement de Josselin lorsqu'il occupait des postes de premier plan. Quoiqu'il en soit, il me paraissait intéressant de brosser à grands traits quelques moments qui auraient pu être d'importance dans la jeunesse de cet éminent membre de la Haute Cour de Jérusalem.

### Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Amouroux-Mourad Monique, *Le comté d'Édesse 1098-1150*, Paris : Librairie orientaliste Geuthner, 1988.

Robert Lawrence Nicholson, *Joscelyn III and the Fall of the Crusader States 1134-1199*, Leiden : Brill, 1973.

Accordailles
------------

### Jérusalem, cour de l'église Sainte-Agnès, matin du dimanche 24 avril 1160

Quelques choucas tournaient, lançant des cris depuis les hauteurs afin d'éloigner la foule importune d'humains massés auprès de l'amandier. En plus des proches et des amis d'Ernaut et Libourc étaient venus quelques curieux et surtout la congrégation habituelle, dont la messe hebdomadaire allait s'animer d'une cérémonie nuptiale. Avec une telle presse, Ernaut ne voyait que difficilement le petit groupe autour de sa belle-famille, et Libourc lui était cachée à dessein. Il avait malgré tout réussi à apercevoir rapidement sa future épouse, méconnaissable avec sa coiffe de mariée.

Lui était habillé d'une magnifique cotte de laine orange, aménagement d'un don de l'hôtel royal pour l'occasion, agrémentée de nombreux triangles d'aisance taillés dans un 'aba[^86] de soie sauvage ocre clair. Ses proches témoins, qui porteraient le voile nuptial durant la bénédiction, se tenaient près de lui, aussi endimanchés qu'il était possible. Les vêtements sentaient la menthe et la sauge, les cheveux étaient lavés et peignés, les barbes rasées. Lambert, responsable de la bague et des treize deniers, contrôlait sans relâche que chacun savait ce qu'il avait à faire et semblait encore plus fébrile que son jeune frère. Sa récente épouse, Osanne, avait pris en charge le fleurissement de l'autel et distribuait quelques couronnes aux filles qui le souhaitaient, embaumant le lieu de fragrances légères.

La porte de l'église s'ouvrit en long grincement espéré et le calme se fit, comme une onde en reflux. Le père Galien apparut, aussi solennel qu'un roi, flanqué d'un enfant de chœur porteur d'un petit bol d'eau bénite où reposait un goupillon. Le prêtre leva la main en un silencieux salut. Puis, d'un regard, il invita Ernaut à se présenter face à lui. Il empoigna l'aspersoir et entama l'*Asperges me*[^87] d'une voix forte, soutenue par le soprano du jeune garçon. Bientôt la congrégation entonnait, plus ou moins correctement, l'antienne tout en recevant la bénédiction en même temps que les gouttelettes. Gonflé d'enthousiasme, Ernaut vociférait si passionnément que Lambert se sentit obligé de le rappeler à l'ordre d'un coup de coude, d'autant qu'il était toujours aussi approximatif dans sa récitation du latin.

L'aspersion et le chant terminés, le père Galien renvoya l'enfant et, d'une voix habituée au prêche et aux sermons, prit la foule à partie.

« Bonnes gens, nous avons proclamé en la sainte église trois bans par trois jours entre ces deux personnes. Présentement déclarons donc le quart ban : que s'il y a aucun ou aucune qui sachent empêchement par quoi l'un ne puisse avoir l'autre par loi de mariage, si le dit sous peine d'excommuniement. »

Le silence qui s'abattit ne laissa s'échapper que frottements de pieds, chuchotements d'enfants, toux discrètes et raclements de gorge. Pendant ce temps, d'un geste de la main, le prêtre indiqua à Sanson d'approcher, tenant sa fille par la main. Ernaut sentit son cœur fondre en la découvrant rayonnante comme un soleil, ses fossettes accentuant son sourire gracieux et la malice de ses yeux emplis de joie. Elle portait une robe de laine couleur miel, dont il lui semblait qu'elle avait capté la chaleur de l'astre du jour. Le visage encadré dans un voile très strict, certainement fixé par Mahaut, elle paraissait soudain plus femme encore qu'il ne l'avait jamais vu. Son regard rivé à lui, elle se laissait guider sans prendre garde où elle mettait les pieds, trébuchant à plusieurs reprises sur les racines affleurant.

Sanson vint jusqu'à Ernaut, lui accordant lui aussi de grands sourires, et se plaça entre les deux futurs époux. Pendant ce temps, le servant de messe était revenu avec une croix qu'il brandit tour à tour devant les mariés, afin qu'ils la baisent. Embrouillé par l'émotion, Ernaut ne se remémora ce qu'il devait dire que grâce à l'enfant de chœur. Le prêtre avait concédé l'usage de la langue vulgaire, ayant reconnu que le verbiage de Ernaut en latin ne rendait pas justice aux formules consacrées, mais cela ne rendait pas la tâche plus aisée au jeune homme.

« Nous t'adorons, Seigneur Jésus-Christ, et nous te bénissons, parce que par ta sainte Croix, tu as racheté le monde. »

Le père Galien adoptait toujours un pas de sénateur dans le déroulement des offices et bénédictions, laissant s'écouler lentement le temps pour que chacun puisse s'imprégner du sérieux de la situation. Il accentuait donc le plus insigne des gestes, la moindre phrase. Il inclinait la tête, les yeux baissés, en guise de ponctuation aux répons, marquant le rythme de ses mimiques silencieuses. Même si ce n'était pas un sacrement à proprement parler, le mariage était un moment essentiel dans la vie d'un chrétien, et rien ne devait en entacher la gravité. Solennel, il se tourna avec Sanson et d'une voix forte, déclama la formule consacrée.

« Veux-tu donner cette femme, Libourc, en épouse à Ernaut ?

--- Je le veux. »

Il s'adressa alors en un sourire à la jeune fille.

« Libourc, veux-tu recevoir cet homme comme mari ?

--- Je le veux. »

Ernaut sentit son cœur s'emballer en entendant ces quelques mots, mais respira un grand coup alors que l'officiant s'apprêtait à l'interroger.

« Ernaut, veux-tu vraiment celle-ci comme épouse ? La garder saine et malade tout le temps de sa vie, comme un honnête homme doit garder son épouse ? Lui faire fidèle société de ton corps et de tes biens ?

--- Je le veux.

--- La reçois-tu dans ta foi ?

--- Oui

--- Et toi Libourc, le garderas-tu dans ta foi ?

--- Oui. »

Prenant alors la parole, un peu vite pour le goût du prêtre, Sanson déposa la main de Libourc dans celle d'Ernaut.

« Je te donne et remets Libourc comme épouse, en sorte que tu lui gardes toujours ta foi et que tu ne la renvoies jamais pour une autre femme. »

En disant cela, son regard était bien clair, qu'au-delà du bonheur qu'il éprouvait, l'avertissement n'était pas que de pure forme. Il en oublia la formule de bénédiction, que le père Galien lança avec un peu de brusquerie.

« *Et ego conjungo vos in nomine Patris et Filii et Spiritus Sancti*[^88]. Amen. »

D'un mouvement de la tête il fit ensuite comprendre à Lambert d'approcher. Celui-ci tenait bien haut l'anneau dans une main et la bourse avec treize deniers dans l'autre. Il tendit les monnaies à son frère qui les remit cérémonieusement à Libourc. Celle-ci en préleva dix pièces qu'elle remit une à une au jeune servant, en gardant trois pour elle qu'elle glissa dans l'aumônière brodée qui pendait à sa ceinture. Puis elle replaça sa main droite dans celle d'Ernaut. Celui-ci prit alors la bague et la présenta tour à tour à chacun des doigts.

« *In nomine Patris*... *Filii*... *Et Spiritus Sancti*... »

Puis il recommença à la main gauche, où il enfila l'alliance à l'annulaire en disant « *Amen* ». Il lui sembla un court instant qu'il n'aurait pas assez d'air pour emplir ses poumons tellement sa poitrine se dilatait de bonheur. Sa voix lui parut mince filet tandis qu'il ânonnait la formule patiemment apprise.

« De cet anneau je t'épouse, et de cette dot je te doue. »

Inspirant longuement avant de se pencher vers celle qui allait désormais partager sa vie, il vint déposer sur ses lèvres un délicat baiser de paix, ému de l'embrasser devant une telle foule assemblée. Il n'entendit même pas le *pax tecum* du prêtre qui signifiait la fin de la cérémonie.

Les portes furent largement ouvertes et la congrégation put prendre place dans la nef tandis que les jeunes mariés avançaient devant l'autel, derrière l'officiant. La messe n'avait rien de particulier, si ce n'est juste avant la communion. Le père Galien fit alors s'approcher plus encore Ernaut et Libourc, pour qu'ils s'agenouillent sous une toile tenue par leurs témoins et, là, il les bénit de nouveau.

« Et ambo ad beatorum requiem atque ad coelestia regna perveniant, et videant filios filiorum suorum usque in tertiam et quartam generationem et ad optatam perveniant senectutem[^89]. »

Quand ils sortirent de la fraîche bâtisse pour retrouver la chaleur empoussiérée de la cour, Ernaut et Libourc se tenaient la main avec vigueur. Ni l'une ni l'autre ne retint le moindre message de félicitation qu'ils reçurent alors, emplis qu'ils étaient de cette communion charnelle.

### Jérusalem, hostel d'Ernaut et Libourc, veillée du dimanche 24 avril 1160

La journée était passée comme un rêve pour Libouc. Avec Ernaut, ils avaient à peine le temps de profiter l'un de l'autre qu'ils étaient sollicités pour des embrassades ou des bons vœux, aussi enthousiastes que répétitifs. Partout les enfants courraient, la bouche pleine d'amandes et de galettes. Quelques adultes dansaient, rythmés par des musiciens loués pour la soirée. Dans un coin, les plus âgés discourraient sur l'avenir du couple, estimant les probabilités de naissances dans les mois à venir à l'aune des attentions qu'ils devinaient entre les deux jeunes gens. Ils évaluaient en outre les chances de succès futurs et la richesse du foyer à l'importance des avoirs présentés dans la grande salle.

Libourc ne pouvait se retenir de sourire devant l'étalage un peu pompeux des céramiques colorées, des verres et des étoffes. Ernaut avait voulu lui faire quelques surprises et elle n'était pas convaincue du bien fondé de certains achats. Il avait en effet tenu à faire les choses en grand et avait dépensé pratiquement toutes ses économies pour acquérir de la vaisselle de prix, exposée sur la table pour y proposer les plats, des toiles de qualité tendues sur les murs et du mobilier en suffisance, dont un magnifique coffre peint qui servirait à conserver leur linge à l'abri.

Il était également particulièrement content d'un châssis orné de verre qu'il avait fait mettre à une des ouvertures. Avec fierté, il expliquait à qui voulait l'entendre que cela permettrait à Libourc de tirer l'aiguille en tout confort, profitant de la lumière toute l'année sans jamais craindre les courants d'air. La jeune femme savait néanmoins que l'enthousiasme de son désormais mari serait rapidement tempéré par la maîtrise des clefs qu'elle arborait triomphalement à la hanche, comme un bachelier sa nouvelle épée.

Libourc essayait de se conformer à l'image qu'elle s'était formée d'une parfaite maîtresse de foyer, peinant à abandonner le quotidien de la gestion des plats et du service à sa mère et ses amies. Mahaut ne cessait de lui répéter qu'elle aurait bien vite sa suffisance de toutes ces corvées et qu'il lui fallait profiter de cette rare journée où on avait soin de l'en décharger.

Elle se contentait donc de s'assurer que chacun avait boisson et mets à volonté, s'efforçant de faire bon accueil à tous. Elle savait que pour ses désormais voisins, cela pouvait conditionner leurs relations à venir. Elle espérait que ces citadines ne trouveraient pas ses manières trop rustiques, elle qui vivait depuis des années dans un modeste casal. Tiphaine et Ysabelon, épouses des collègues les plus proches d'Ernaut lui semblaient bienveillantes à son égard, même si la dernière semblait un peu revêche. L'épouse d'Eudes, au contraire, était une femme avenante et engageante. Elle avait déjà proposé à Libourc de lui faire découvrir les meilleurs endroits en ville où approvisionner son cellier, ainsi que quelques tours de main en couture qui ne seraient pas inutiles, vu le rude traitement que les hommes imposaient à leur tenue dans leur service.

Libourc ne prit conscience que tardivement que le soleil était couché depuis un moment, ayant abandonné la place aux lueurs ambrées des lampes. Au dodelinement de la tête de ses parents, elle comprit qu'il était temps pour Ernaut et elle de se retirer, afin que les plus âgés puissent assister aux ultimes bénédictions. Le père Galien était toujours là, sirotant un vin miellé tout en discutant avec Le François, chef du quartier et responsable des portes y menant.

Libourc rejoignit son époux. Elle avait flotté depuis le matin dans un état second et, si le bonheur transfigurait ses traits, la fatigue commençait à s'y manifester. Comprenant cela sans un mot, Ernaut acquiesça à l'idée d'achever la journée dès à présent. Du moins sa partie publique.

Il ne fallut guère de temps pour rassembler tout le monde à l'entrée de la chambre, en un amas tassé où chacun tordait le cou pour tenter d'y voir. Seuls les intimes avaient accès aux abords du lit où Ernaut et Libourc s'étaient assis, main dans la main. Devant eux, le père Galien avait revêtu une étole et affichait de nouveau son masque empreint de sérieux. Il leva la main, attendit que le silence se fît et entama un large signe de croix, répété dans toutes les directions.

« *Benedic, Domine, thalamum hoc, et omnes habitantes in eo ; uta in amore tuo vivent, et senescant, et multiplicentur in longitudine dierum. Per Dominum*[^90]. »

Il s'avança, écartant la foule amassée tel le soc fendant la terre, et se plaça au pied du lit, face aux deux jeunes mariés et marqua Libourc du signe de la croix.

« *Benedicat Deus corpora et animas vestras, et det super vos benedictionem suam sicut benedixit Abraham, Isaac et Jacob*. *Amen*.[^91] »

Il fit ensuite un pas de côté et traça la même bénédiction sur Ernaut.

« *Manus Domini sit super vos, mittatque angelum suum sanctum, qui custodiat vos omnibus diebus vitae vestrae*. *Amen*[^92]. »

Puis, d'un mouvement du bras et d'un regard impérieux, il signifia à tous de se retirer de la chambre, les repoussant tel le pasteur ses ouailles bêlantes. Sans même un dernier salut pour Ernaut et Libourc, indifférent aux petits gestes et signes divers qu'il occultait, il fit refluer la presse puis tira la porte derrière lui. De la foule ne demeura qu'une rumeur.

Assis au pied de leur lit, les deux jeunes mariés se dévisageaient, souriant, un peu enivrés de leur journée. D'une caresse de la main, ils s'accordèrent et unirent leurs souffles sans un mot. Bien vite ils oublièrent, au loin, le tapage assourdi des fêtards qui résonnait dans leur salle commune. ❧

### Notes

Je l'ai déjà écrit à de multiples reprises, le mariage est jusqu'à assez tard au Moyen âge une procédure civile et non religieuse. Le but de l'union est de mettre en place une structure économique viable et pérenne, qui assure la reproduction du groupe social ou son étendue. L'Église n'y intervenait que pour bénir sous de bons auspices l'arrangement contracté par les individus ou les familles. Mais peu à peu les ecclésiastiques ont entrepris de se garantir que les époux n'étaient pas bigames ou incestueux, de vérifier que les choses étaient conformes aux usages licites. Cela a renforcé l'institutionnalisation de la pratique, tout en y introduisant une notion, de consentement mutuel, qui n'était pas forcément présente auparavant.

Le déroulement proprement dit a fait l'objet de nombreux rituels, selon le temps, les zones géographiques et les traditions religieuses. N'ayant rien de précis pour le Jérusalem du XIIe siècle, j'ai recouru à un patchwork de gestes et de formules qui me semble vraisemblable. Sans l'ouvrage de Jean-Baptiste Molin et Protais Mutembe, je n'aurais pas été en mesure de me repérer dans la jungle des usages. Il est toujours peu évident d'accéder aux sources liturgiques véritables, et encore moins d'en comprendre le possible arrangement de façon claire et pratique, surtout quand on n'a pas fait le séminaire. C'est pourtant une des interrogations les plus fréquentes qui nous sont posées quand on discute avec le grand public, et des éléments essentiels pour arriver à reconstruire les paysages mentaux de nos prédécesseurs médiévaux.

De la même façon, l'alternance de formules en latin et en langue commune semble avoir été fluctuant. Le fait que des clercs aient rédigé les déroulés en latin ne prouve en rien qu'ils aient été vocalisés ainsi, surtout pour les répons. Rien de strict ne semble avoir été pratiqué, les passages dans une langue ou l'autre semblaient pouvoir varier selon les lieux et les habitudes. Peut-être que la maîtrise du latin par l'officiant et son adhésion à une liturgie stricte pouvait aussi grandement varier selon sa formation et son rapport à sa hiérarchie et à la congrégation dont il avait la charge d'âmes.

### Références

Beauchet Ludovic, « Étude historique sur les formes de la célébration du mariage dans l'ancien droit français », dans *Nouvelle revue historique de droit français et étranger*, vol. 6 (1882), p. 351-393.

Molin Jean-Baptiste, Mutembe Protais, *Le rituel du mariage en France du XIIe au XVIe siècle*, Éditions Beauchesne, Paris : 1974.

La vigie du Levant
------------------

### Château d'Harim, début de soirée du jeudi 15 novembre 1162

Les doigts collés de son torchis sommaire, Karpis tentait de boucher les courants d'air qui s'infiltraient par les volets de bois de la chambrée qu'il partageait avec quelques camarades. Grommelant face au très relatif succès de son entreprise, il finit par enduire d'une épaisse couche brunâtre pratiquement tous les rebords. Les hivers précédents il s'était contenté de bloquer l'ouverture de bouchons de paille, mais cette fois le vent froid qui descendait des plateaux orientaux s'immisçait partout depuis plusieurs jours.

« Nous n'aurons plus guère de lumière, mais au moins aurons-nous chaud pour dormir ! »

Il se rinça les mains dans un seau et s'essuya rapidement d'une touaille avant de se chauffer au brasero improvisé dans une vieille jarre. Il soupira en voyant le maigre tas de bois.

« Les bûches apparaissent pas là par magie, compères. Il faudrait s'activer à en porter suffisance... »

Nul n'osa lui répondre ni même le regarder directement. Ses compagnons avaient appris à respecter son statut de vétéran, mais aussi à connaître son humeur maussade. Jamais rien ne semblait le réjouir. Il s'installa sur son lit, sobre matelas de paille garni d'une couche de couvertures élimées. Le repas du soir approchait et il espérait que ce serait enfin une épaisse soupe d'orge, d'oignons et de lentilles, comme il en demandait régulièrement aux cuisines. Il n'était pas le seul arménien de la garnison, et chacun à son tour réclamait ce plat simple et roboratif qui leur rappelait à tous leur enfance dans les reliefs de l'Anti-Taurus.

« Karpis, on te mande en la basse cour ! »

La voix qui l'avait interpelé était celle d'un Latin, un jeune archer à la dégaine maladive, Renart. Récemment recruté, il servait un peu d'homme à tout faire aux anciens, acceptant sans broncher les tâches les plus humiliantes. Il savait que son intégration était à ce prix et qu'il lui serait loisible d'en faire autant lorsqu'un nouveau prendrait sa place. À l'encontre de ce qui se faisait habituellement, et pourtant sans se soucier outre mesure de la sensibilité de son prochain, Karpis n'aimait pas qu'on rabaisse par plaisir ou, pire encore, par conformisme. Il s'efforça donc de répondre sans colère malgré son agacement à devoir ainsi redescendre alors qu'il s'installait pour la soirée.

« Quelque urgence ? Ou cela peut attendre ? »

Le jeune haussa les épaules.

« Un vilain s'est présenté à la porte et déclare avoir important message. Mais il ne veut rien nous dire, à *nous*. Sevrin a pensé que tu saurais le faire parler sans qu'on ait besoin d'en appeler au sire Hugues. »

Karpis grogna. Les fellahs des environs , tout en acceptant de verser les impôts sans trop oser relever la tête, n'appréciaient guère les envahisseurs latins qui avaient pris possession des lieux. Les pauvres cultivateurs devaient renâcler tout autant face aux guerriers turcs et, lorsqu'ils avaient une requête, préféraient toujours s'adresser aux Syriaques, ou, à défaut, aux Arméniens, avec lesquels certains partageaient la religion orthodoxe. D'un signe de menton, il marqua son accord et entreprit d'ajouter une chaude veste fourrée à ses couches de vêtements. Puis il se coiffa d'un bonnet de laine épaisse. Enfin, il attacha son baudrier, symbole de son statut d'homme d'armes.

Lorsqu'il fut prêt, Renart avait décampé, et il prit donc son temps pour traverser les salles, courettes et galeries qui conduisaient en bas vers la porterie. La vaste entrée était maintenue fermée à toute heure et on ne pouvait s'approcher que par une mince poterne et un passage piéton. La cité d'Alep n'était pas si éloignée et on avait conscience d'être là comme une avancée en territoire ennemi, en deça du Pont du Fer[^93] qui menait à Antioche et ses puissantes murailles.

La cour était boueuse des pluies récentes et, sous le ciel plombé de fin de journée, la couleur avait déserté les lieux. La tête dans les épaules, quelques hommes s'étaient regroupés autour d'un fellah et de son gamin, pieds nus dans la fange. Ils accueillirent Karpis avec contentement. Sevrin, le gros rouquin qui commandait habituellement la troupe fit signe qu'on laisse l'Arménien s'approcher. Le paysan, en l'apercevant, fut visiblement soulagé et salua d'un air désolé. Il attendit l'accord du Franc et prit la parole dans un salmigondis essentiellement syriaque.

« Je suis heureux encontrer guerrier des Montagnes. Toi ne voleras pas Mu'adh !

--- De quoi parles-tu ?

--- Il est usage donner belle pièce d'argent à premier à voir soldats d'Alep. Fils à moi les a vus, proches ! »

Fronçant les sourcils, Karpis dévisagea le gamin. Dépenaillé, le petit était habillé de ce qui ressemblait à différentes couches de sacs, unis par la crasse et leur aspect mité. Jambes nues, il était crotté de boue jusqu'à mi-mollet et pourtant souriait de toutes ses dents d'un ivoire étincelant. Il ne paraissait avoir guère plus de 8 ou 9 ans.

« Qu'as-tu vu ? Et où ?

--- Dans les terres nord, j'ai cherché chèvre enfuie. Force Turkmènes, pas nomades, soldats...

--- Vers la route du Pont ?

--- Vers Levant. »

Karpis traduisit pour Sevrin, qui trahit sa pensée en tordant la bouche.

« On n'a guère encontré de troupes là-haut depuis long moment. C'est peut-être juste brigandage...

--- J'en serais fort heureux ! Avisons le sire Hugues sans tarder.

--- Ouais, qu'il puisse mander patrouille dans le froid et la pluie. On va encore se tremper la couenne... J'aimerais bien y échapper cette fois ! »

Karpis acquiesça. Le châtelain, Hugues d'Harenc, était un homme prévisible qui appliquait toujours les mêmes stratégies. Il enverrait un petit groupe de cavaliers vérifier l'information, en y intégrant le sergent auquel il se fiait le plus, ainsi que l'autochtone dont il se méfiait le moins : Sevrin et Karpis.

Avant que ce dernier ne puisse s'éloigner, le fellah l'interpella respectueusement.

« Pas oublier, mon fils a gagné belle pièce en argent ?

--- Quoi encore ? s'interrogea Sevrin.

--- Il était de tradition de mercier d'un besant celui qui signalait le premier des forces armées ennemies. Il veut s'assurer que nous la lui donnerons.

--- Peuh ! Argent de bon aloi pour lui et frimas et averses pour nous ? C'est le monde cul par-dessus tête ! »

Sans un mot de plus, Karpis sourit au fellah et emboîta le pas à Sevrin. Il veillerait à ce que le gamin ait sa pièce.

### Château d'Harim, soir du vendredi 16 novembre 1162

Le déluge furieux et incessant avait lessivé la terrasse où le bûcher du feu d'alerte était empilé et on n'en discernait les formes que grâce à la lueur rousse échappée par la porte donnant dans la tour. Indifférent à la pluie qui l'avait détrempé depuis son départ au matin, Karpis essayait de creuser un petit tunnel sous le tas de bois, de paille et de copeaux. Il espérait qu'en y versant de l'huile il pourrait ensuite arriver à lancer le feu malgré les intempéries.

Avec un tel temps, il n'était même pas certain que cela soit vu par les vigies à l'ouest et que le message soit répercuté jusqu'à Antioche. Par précaution, le châtelain avait décidé de lâcher des pigeons porteurs de missives en plus de l'alerte visuelle habituelle par flamme. Sentant les gouttes perler dans son dos, Karpis frissonnait malgré lui, s'écorchant les doigts tremblants et engourdis tandis qu'il préparait l'allumage.

Derrière lui, quelques sentinelles le soutenaient moralement ou lui lançaient d'inutiles conseils. Il leur avait ordonné de rester à l'abri, estimant ridicule d'être plusieurs à se tremper alors que lui l'était déjà, d'avoir chevauché depuis le matin. À grand-peine, il réussit à constituer une sorte de caverne, où il imbiba quelques copeaux d'huile avant d'y glisser une lampe, tirant la mèche pour en obtenir une longue flamme. Il espérait que la réserve de combustible serait suffisante pour initier le départ de feu.

Il se recula, jaugeant ses chances de succès aux arabesques de la flammèche qui oscillait dans sa cabane précaire. Puis il rejoignit le couvert, rappelant aux hommes de surveiller la petite lueur et de la remplacer si jamais elle venait à s'éteindre. Leur sécurité valait bien quelques lampes sacrifiées, estimait-il. Puis il descendit dans une quasi-obscurité, familier des recoins et des marches, ses pieds faisant un bruit de succion à chaque pas. Il sentait les gouttes se regrouper, s'échapper de ses vêtements, de ses membres, avant de frapper la poussière et la pierre d'un plic plic régulier.

Lorsqu'il souleva la portière d'épaisse toile qui donnait sur la grande salle, il faillit en suffoquer tellement l'air lui parut étouffant. Au milieu d'une myriade de lampes, le seigneur Hugues d'Harenc était occupé à parcourir des tablettes et des documents, assisté par un de ses clercs. Près du feu qui brulait largement, son épouse, ses enfants et quelques familiers jouaient aux tables, discutaient ou tiraient l'aiguille paisiblement. La vocifération du vent ne parvenait pas jusque-là.

Le châtelain leva la tête à son entrée et lui fit signe d'approcher.

« Prends donc un verre de ce vin chaud, tu m'as tout l'air d'un vieux chien abandonné sous l'orage ! Il ne serait pas bon que tu attrapes fluxion de poitrine en ces temps difficiles ! »

Puis il reposa sa tablette, prenant soin de la disposer à sa place, et rangeant d'un geste mécanique ce qui se trouvait devant lui.

« Le feu d'alerte est-il lancé ?

--- Je l'ose croire, mon sire. Je venais en rendre compte. Mais l'humidité rend la chose malaisée et je vais remonter vérifier qu'il ne puisse céder sous l'assaut de la pluie.

--- Certes. Certes. Tu agis de raison. J'ai de toute façon lâché deux autres oiseaux pour porter le message. Il ne faut guère économiser ces volatiles quand l'ennemi est à nos portes ! »

Karpis garda le silence, il avait vu de ses yeux les patrouilles du prince d'Alep, Nūr ad-Dīn, en trop grand nombre pour que cela ne soit pas une avant-garde. Pour autant, il trouvait que le châtelain s'agitait bien trop. Ils connaissaient les risques, à tenir une forteresse de frontière, et Karpis estimait dangereux de montrer de l'anxiété quand on devait diriger des hommes.

« J'ai vérifié les listes et inventaires demandés pour nos celliers. Nous avons bonnes réserves, et enserrés dans nos murs, nous n'avons guère de souci à nous faire. »

C'était là une déclaration que le châtelain d'Harenc s'adressait à lui-même, Karpis le comprit. Il se retint d'évoquer la prise de la citadelle une douzaine d'années plus tôt environ. Puis sa recapture par les forces franques même pas dix ans plus tard. Aucune forteresse n'était inviolable. Ce n'était qu'une question de temps et d'habileté. Mais il était habitué aux soliloques de son seigneur, plus destinés à lui-même qu'à son auditoire.

« Tu me confirmes de nouvel que l'accès au Pont du fer nous est scellé ? Quelque prompt coursier ne pourrait s'y faufiler ?

--- Les Turkmènes ont rapides montures, meilleures que toutes celles que nous avons en nos écuries. Et leur premier soin sera sans doute aucun de se garantir que nul ne court à Antioche. Et au vu de leur avancée, je dirais que demain les principaux corps de l'askar seront dans la plaine, leurs fourrageurs et leurs patrouilles vaquant comme en leurs provinces. »

Hugues d'Harenc hocha la tête. Fouillant dans sa mémoire, il vérifiait s'il n'avait pas négligé un point ou l'autre des instructions qu'il avait reçues. Le légitime seigneur du lieu, Josselin de Courtenay, était absent, comme souvent, et il savait que le jeune baron prenait ombrage du moindre manquement, qu'il avait tendance à interpréter comme une insulte personnelle. Homme de guerre, Hugues était plus à l'aise à mener un conroi, maniant la lance et dirigeant les soldats lors des assauts qu'à naviguer dans les eaux troubles des conseils nobiliaires. Dans la bataille, les choses étaient simples, et les conséquences rapides. Le doute n'avait pas le temps de fouailler son âme.

### Château d'Harim, nuit du mardi 27 novembre 1162

La cuisine, où sommeillait à toute heure un feu léger, bruissait des échanges entre les hommes installés autour de la table habituellement réservée à la préparation des repas. Un marmiton à moitié réveillé veillait à maintenir un épais gruau chaud à toute heure pour les vigies qui, sans relâche, parcouraient les murs d'enceinte afin de surveiller l'ennemi. Depuis plusieurs jours, l'armée de Nūr ad-Dīn s'était déployée aux abords du tell, occupant le village voisin et disposant ses lourds engins de siège. Le pilonnage du massif d'entrée avait commencé en fin de matinée, marquant de ses chocs sourds et répétés le tempo vers leur inévitable reddition.

La nuit avait une nouvelle fois été accueillie par une forte averse et un déluge s'abattait désormais sur le pays, bouchant la vue depuis les murailles. Le seul réconfort de la garnison était de profiter de salles chauffées et d'abris solides tandis que leurs assaillants devaient certainement se contenter de cabanes précaires, en toile ou de branchages ramassés alentour.

Néanmoins, en l'absence de nouvelles, le châtelain avait décidé d'envoyer un messager. Il devait également informer d'éventuels renforts qu'il ne leur faudrait pas trop tarder, la citadelle n'ayant guère les moyens de tenir plus de quelques semaines face à l'armée d'un prince aussi déterminé.

Sevrin avait désigné Renart pour la mission et lui avait indiqué de se préparer en ce sens. Karpis avait alors invité le sergent à boire un coup, espérant le dissuader d'un tel choix. Mais le vieil entêté n'en démordait pas.

« C'est la coutume, compère. On ne va pas mander sochon[^94] quand un béjaune[^95] est là à attendre de faire ses preuves !

--- Fariboles, Sevrin, tu sais comme moi qu'il n'a pas une chance. Il ne connaît pas le pays ! Il ne découvre qu'à peine le castel et tu l'envoies errer dans la nuit noire, en espérant qu'il pourra trouver le Pont. Norreddin en fera son serf avant le lever du jour, c'est acertainé.

--- Et quoi ? Tu le sais comme moi, ils sont là, au-dehors, tels criquets. Sortir, c'est aller à la mort ! J'ai bien triste rôle, de choisir qui tantôt goûtera du fer...

--- Il en est d'aucuns ici mieux armés à cela, cul-Dieu ! Qui auraient au moins une chance !

--- Hé quoi ? Aucun de mes compères ne serait enjoyé à l'idée de se jeter dans la tanière du lion...

--- Je n'irais pas le cœur en joie, mais je le ferais si tu me l'ordonnais... »

Le rouquin fronça un sourcil, avala une gorgée, se suçotant les dents tandis qu'il réfléchissait. Il semblait chercher l'entourloupe dans le marché qu'on lui proposait.

« Tu es prêt à risquer ta couenne pour ce freluquet ?

--- C'est surtout que moi j'ai une chance de passer. J'ai usage de ça, je connais cette terre et, si on me surprend, je peux toujours mentir. Lui aura les épaules libérées de son chef dès qu'on l'apercevra. »

Sevrin se gratta la tête, inspira longuement.

« Si tu tiens tant que ça, vas-y donc, bougre d'âne ! Moi je m'en lave les mains si tu y laisses ta peau. »

Karpis hocha le menton et se leva, les yeux rivés dans ceux du sergent.

« N'aie crainte, je serais bientôt de retour pour botter tes grosses fesses, le Celte ! »

Puis, sur un clin d'œil, il sortit de la pièce, se dépêchant de retrouver Renart. Il le trouva en train de ranger ses affaires dans son coffre, équipant un long chaperon qui le désignerait à la moindre sentinelle comme un franc.

« Sevrin a changé d'avis, c'est moi qui irais. »

Le jeune homme en resta coi, balbutiant sans arriver à s'exprimer. Karpis ne lui en laissa pas le temps.

« Tu es archer, c'est stupide de t'envoyer au-dehors alors que tu es utile ici. Moi je ne sais ni l'arc ni la fronde... »

Il sortit la clef qui ouvrait son propre coffre afin de s'équiper en prévision de son périple nocturne.

« Par contre, le temps que je descendrais la muraille par la corde, j'apprécierais fort que tu sois là, pour flécher quiconque risquerait de s'en prendre à moi ! »

### Château d'Harim, nuit du jeudi 29 novembre 1162

Transi de froid, Guymar allait et venait sur la coursive orientale, emmitouflé dans une couverture. Au crépuscule, la pluie s'était arrêtée et la lune jouait depuis lors à cache-cache avec les nuages effilochés, apportant par moment une lueur bienvenue pour observer les environs. Il était impatient d'aller se coucher, épuisé d'avoir participé au renfort de la muraille sud, affaiblie par les tirs répétés des engins. Toute la journée il avait porté de la terre, des cailloux, des gravats, afin de consolider le mur où de longues lézardes couraient. Et le repos avait été de courte durée avant qu'il vienne prendre son tour de guet.

Il y avait peu de chance que les ennemis décident d'écheler en pleine nuit du côté le plus haut, mais il fallait surveiller tous les abords. Au moins y avait-il quelque lumière lunaire intermittente désormais, qui permettait de discerner à plus de dix pas. Mais l'exercice restait pénible et résister au sommeil qui montait n'en était pas la plus petite des difficultés. Sans même parler de la peur de servir de cible à ces maudits archers turcs, capables selon les rumeurs de percer l'œil d'un oiseau en plein vol à plus de deux cents+ coudées.

Alors qu'il battait des pieds pour réchauffer tout en baillant, il lui sembla entendre un bruit au bout de la terrasse. Circonspect, il s'approcha prudemment.

« Souris ou quelque bestiole » se rassura-t-il.

Mais cela recommença, comme si un petit animal dérangeait le gravier de la plateforme. Pourtant Guymarl ne voyait rien. Écarquillant les yeux dans l'obscurité, il restait à bonne distance du parapet, soucieux de ne pas offrir de cible à un éventuel tireur. La nuit n'arrêtait pas ces démons, lui avait-on expliqué.

Il entendit de nouveau le bruit, plus près. Il fit un pas prudent... et reçut un choc sur le front, qui le fit reculer. Un instant, son cœur s'affola à l'idée d'avoir reçu une balle de fronde et d'en mourir, mais il se passa la main sur le visage. Il n'y sentit aucune blessure, ce n'était qu'un minuscule projectile.

À pas de loup, il vint plus près du rebord , risqua un œil dans le vide, pour n'y découvrir que ténèbres. C'est alors qu'il entendit un petit sifflement. Qu'il aperçut du mouvement parmi la noirceur. Il appela, doucement.

« Qui va là ?

--- Chut ! C'est moi, Karpis, envoyez une corde ! »

En peu de temps, le messager fut hissé par quelques hommes rapidement réveillés et il se retrouva à l'abri des murs, essoufflé, mais souriant.

« J'ai bonnes nouvelles, compères. Une forte armée arrive et ce diable de Norreddin lève le camp ! » ❧

### Notes

La période médiévale invoque immédiatement en nous des images de forteresses assiégées. Il est néanmoins difficile d'en évoquer la réalité matérielle, tant les pratiques et les conditions variaient énormément d'un lieu à l'autre, d'un moment à l'autre. Durant les croisades, les citadelles étaient généralement intégrées à un habitat (ou l'incorporaient). Au final ces forteresses se retrouvaient souvent garnies d'assez peu de défenseurs et comptaient sur une armée de campagne pour être délivrées. Le renseignement, les informateurs étaient alors essentiels, ainsi que des moyens de communication afin de prévenir ses alliés de la situation dans laquelle on se trouvait. L'usage de pigeons ou de feux est avéré dans les territoires musulmans, j'ai donc estimé qu'il était probable que les Latins en soient dotés également.

### Références

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Rogers Randall, *Latin siege warfare in the twelfth century*, Oxford : Oxford University Press, 1997.

Ultra
-----

### Jérusalem, hostel d'Ernaut, début de soirée du vendredi 5 février 1160

Dans un coin de pièce, un maigre feu couvait sous l'épaisse marmite de céramique où frémissait un pâteux brouet au lard. Au centre, autour d'une table improvisée de planches, quatre silhouettes avalaient le consistant mélange avec de longs soupirs de satisfaction. Droart, Eudes, Baset et Ernaut avaient passé la journée à s'activer dans la maison que ce dernier occuperait bientôt avec Libourc. Même si des artisans avaient fait le plus gros du travail, il demeurait de nombreuses tâches annexes, petits détails qui ne pouvaient s'abandonner aux soins d'inconnus.

Les lieux commençaient à avoir fière allure et le mobilier s'entassait dans la grande pièce, dans l'attente d'être installé de façon définitive. Plusieurs coffres, jarres et tonneaux serviraient à entreposer nourriture et effets. Du linge de maison, couvertures et draps pour faire les couchages, était méticuleusement roulé, protégé d'herbes aromatiques pour repousser les insectes. Ernaut savait que ses choix initiaux seraient certainement remis en cause par sa future épouse, mais il estimait que c'était dans l'ordre des choses. Leur logement serait le royaume de Libourc et cela lui convenait parfaitement. Il espérait simplement que l'endroit serait à la hauteur des attentes de sa fiancée. Sanson était passé plusieurs fois vérifier les installations et Mahaut avait tenu à inspecter linge et matériel de cuisine. Malgré ses habitudes pincées, elle avait lâché quelques remarques, qui pour n'être pas enthousiastes, ne marquaient aucun désaccord ni critique, ce qui mettait Ernaut en joie. Il s'enorgueillissait de clouer le bec de sa belle-mère, toujours prompte à le dévaloriser selon lui.

Baset, s'essuyant la main de son revers de manche après une longue rasade de vin, servit ses compagnons.

« Vous avez ouï rumeurs de la chevauchée pour tantôt ?

--- Si tu parles de celle qui se déroule en lit royal chaque nuit, je suis prêt à en entendre ! ricana Droart.

--- Oui-da, le sire vicomte en discutait l'autre jour avec Brun confirma Eudes, sans prendre note de la moquerie de son ami. Le sire Baudoin a reçu coursiers disant que Norredin[^96] est fort occupé au septentrion. Ses appétits pour le Cham[^97] vont de nouvel s'aiguiser...

--- Que voilà belle possibilité, il y aura de certes quelques mailles à se partir ! » approuva Ernaut.

Droart fit une moue, retrouvant son sérieux.

« Si les rois et princes n'ont pu cueillir le fruit alors qu'ils étaient nombreux, cela me semble hasardeux[^98].

--- Connaissant Baudoin, il ne désire pas à toute force prendre l'arbre quand il peut se contenter du fruit. Il a toujours grand appétit de monnaies, qui lui filent entre les doigts comme l'eau claire.

--- J'espère qu'ils n'en appelleront qu'à volontaires, grommela Baset. Je n'ai nul goût à coucher sur la pierre, à manger de la poussière et me dessoiffer de flaques croupies.

--- Pour ça, je te dis *Amen*, compain, renchérit Droart. Je suis bien aise de demeurer en nos murs plutôt que de marcher sous le soleil, avec les flèches turques pointant sur mon cul comme récompense.

--- Je pense que j'en serai » déclara Eudes, un peu solennel.

Un silence se fit autour de la table, le petit monde de la sergenterie avait eu de grands espoirs lorsque le vicomte Arnulf était parti pour la forteresse de Blanchegarde, quelques mois plus tôt, emmenant Ucs de Monthels avec lui. Mais c'était Fouques, jeune homme ambitieux, familier d'Arnulf, qui avait eu le poste de mathessep. Et c'était le tonitruant Eudes de Saint-Amand qui était désormais vicomte, en plus d'être châtelain de Jérusalem. Ses imprécations et sa brusquerie avaient bouleversé la vie dans la Cour des bourgeois et l'hôtel royal, habitués depuis des années aux manières feutrées et diplomates d'Arnulf.

« Il me semble douteux que le sire vicomte choisisse Gaston, malgré son expérience, car il n'apprécie rien tant que bras solides et pieds vaillants, indiqua Ernaut. Je compte bien aussi être de ceux qui porteront le fer pour cette marche.

--- Tu devrais plutôt penser à tes accordailles qui ne sauraient tarder, Ernaut. Pâques sera tôt passé. Libourc aura besoin de toi pour la cérémonie, as-tu oublié ? ironisa Droart.

--- De vrai, compère. Mais si j'encroie l'usage du sire Baudoin, ainsi qu'il a fait voilà quelques années à Panéas, cela ne demandera guère long temps. D'ici là, j'aurais bel écot à verser en mon hostel.

--- Baudoin a bien failli y laisser sa couenne, rétorqua Baset. Et grands barons ont lourd payé quelques destriers et menues monnaies pour la Secrète[^99].

--- Remarque que cette fois, ce n'est pas piétaille qui a subi le fer, que voilà bon augure ! » s'amusa Ernaut.

Eudes se leva et alla puiser quelques cuillérées de potage, prenant la parole à son tour.

« Fouques nous le montre bien, compères. Si on ne se fait pas connaître de barons, les meilleures places vont à d'autres, parfois plus jeunes, mais de certe plus habiles à vanter leurs talents. Quand le trésor royal est à sec et que la solde est reportée, Tiphaine a toujours grand mal à obtenir délais pour grain, vin ou viandes[^100]. Surtout que c'est souventes fois le moment où les accapareurs font leur gras. Alors si je peux aider à rentrer cliquailles en l'hostel Baudoin et y ajouter ma propre picorée, cela me semble bon choix. »

Ernaut piqua du nez dans son écuelle. Il avait en tête surtout la perspective de chevaleresques exploits à conter, de prouesses dignes du grand Charles[^101]. Il appréciait le confort et le butin accumulé jusque-là lui avait offert l'occasion d'acquérir un bel hôtel où installer sa famille. Pourtant, ce qui faisait battre son cœur était plutôt l'aventure qui frappait à sa porte. Inconsciemment il jouait sans cesse de son pouce contre l'anneau donné par le prince d'Antioche lui-même quelques mois plus tôt. Il aurait bien échangé tous les trésors de la terre pour une autre soirée comme celle qu'il avait alors vécu.

### Damas, sud de la Ghûta, veillée du jeudi 25 février 1160

Les gouttes d'une pluie fine pointillaient les tuiles de l'appentis en contrebas de la pièce où s'étaient installés Raguenel et Gosbert, deux sergents montés responsables d'unités de fantassins. Depuis ce salon, ils bénéficiaient d'une magnifique vue sur les environs, la verdoyante oasis qui baignait de jardins et de canaux les abords de Damas. Quelques cahutes branlantes, des cabanes hâtivement assemblées, des parcs à chevaux et des monceaux de sacs de toile cirée s'égayaient partout dans la palmeraie. Parmi les arbres, on entendait l'activité d'un camp affairé, où les hommes goûtaient le bonheur d'avoir amassé un large butin sans trop avoir à ferrailler, comptant et recomptant à l'envi les sommes accumulées jusque-là. Beaucoup avaient d'ailleurs commencé à en dilapider le fruit en plaisirs éphémères, impatients de siroter le suc de la vie après en avoir enduré l'ivraie.

Le lieu était certainement une maison de repos, agrémenté de quelques matelas et nattes, ses propriétaires y apportant avec eux le confort de tapis et coussins lors de leur venue. Mais pour les deux hommes habitués aux nuits dans les buissons et aux haltes le long de sentiers caillouteux et arides, cela avait déjà un goût de paradis. Ils partageaient même un repas chaud, ragoût insipide réalisé avec les découvertes du jour, épaissi de blé concassé. Allongé d'un vin rubis, le souper leur faisait l'impression d'un festin, mangé à l'abri, ce qui en renforçait les saveurs, somme toute bien ordinaires.

Déchirant un pain plat, Raguenel en tendit un morceau à son compagnon.

« Si on arrive à mener tout ça jusqu'à nos murs, je me verrais bien laisser l'épée au fourreau. Finir sur belle moisson, c'est pas tant l'usage, pour nous autres...

--- Tu comptes les œufs dans le cul de la poule... Il n'y aura pas de quoi vivre bien vieux.

--- Je demande juste de quoi m'installer bon commerce de lames, de fers d'hasts et fourniment de guerre. Entre ceux qui partent sans guère de place pour emporter plus que simple palme et ceux qui arrivent avec désir de trancher du col mahométan, je pense que j'aurais de quoi cuire mon pain. Et avoir assez pour me trouver garcelette pouvant pétrir le nécessaire, pour mes vieux jours. »

Gosbert acquiesça, engloutissant sa cuillerée en expirant bruyamment à cause du liquide encore brûlant. Raguenel souffla sur sa bouchée avant de l'avaler. Il savait que le sujet de se retirer des combats était toujours délicat avec son compagnon. Celui-ci se racla la gorge et entama son couplet habituel.

« Je te souhaite d'avoir tout ce qu'il te faut, compain. Rien ne vaut enfançon pour garantir la vieillesse. Puisse ta future avoir ne serait-ce que la moitié des qualités de ma Peironelle.

--- Et toi, tu n'as pas désir d'enfin lâcher le fer ? Profiter un peu de ton temps ?

--- Je ne sais. Il y a encore la benjamine à doter, qu'elle trouve bon mari. Pas un vaut-rien comme nous autres ! » ricana-t-il.

Il lécha rapidement sa cuiller puis reprit.

« Je t'ai dit que le Merle m'avait passé nouvelles, juste après la Noël ? Ça date, c'est un pérégrin parti avant Pâques dernier qui l'a encontré. Un gentil gars, le Merle, il t'aurait plu. Il s'occupe bien des miens. Je lui ferai porter ce que j'aurai grappillé ici. Je ferai peut-être encore une saison ou deux, puis je passerai outre la mer ultime fois. Revoir mes filles et mes fils, embrasser mon espousée. Que de bonnes années à venir ! Mais je tiens à achever ma promesse, que mes vœux soient parfaits. »

Raguenel hocha la tête, avalant une autre bouchée. Il avait appris voilà des mois que Merle était mort depuis bien longtemps et chacun savait que Gosbert dépensait sa solde et ses butins dans les bordels et les tavernes de Jérusalem, faisant rouler les dés à la moindre occasion. Il parlait pourtant à qui voulait l'entendre de sa famille, à qui il envoyait la moindre miette amassée, tandis qu'il accomplissait son devoir de pèlerin. Raguenel jeta un coup d'œil à la croix de tissu cousue au col du vêtement de Gosbert. Elle n'avait plus de couleur et avait été tant reprisée qu'elle ressemblait à un chiffon, lointain souvenir du ruban écarlate de son serment.

« Ça te dirait pas de t'associer avec moi ? On sait évaluer la qualité d'une lame au premier coup d'œil, et à tous deux, on a les noms de toute la sergenterie d'Antioche à Ascalon ! Fini la fatigue et les mauvaises nuits, les repas manqués et les flux de ventre...

--- Je ferais quoi d'un commerce en la Terre ? Les miens sont au loin, je n'ai rien à bâtir ici, juste devoir de pérégrin défenseur de la Foi. Non, c'est amiable à toi, compère, mais tant que je serai en ces provinces, ce sera avec le glaive en main pour protéger la Croix. Une fois mon ouvrage accompli, je m'en retournerai. »

Gosbert acheva sa réponse d'un clin d'œil et se rinça la bouche d'une longue goulée d'une outre sentant la piquette. Il s'étira alors le dos puis se frotta le ventre de contentement.

« Crois-m'en, tu m'envieras bien vite si tu te retires dans ta modeste échoppe de ferraille. Plus que deux-trois Pâques, c'est bien le moins pour nettoyer méchante âme comme la mienne, disent les bons pères. Et je profiterai tantôt de la vie, tel soudan[^102] en son palais ! »

Raguenel se dit qu'il devrait peut-être en parler à un ecclésiastique, justement. Peut-être les frères de Saint-Jean sauraient quoi faire. On racontait qu'ils guérissaient les corps et les cœurs, ou prenaient soin de ceux qui vivaient comme dans un rêve. Gosbert était un compagnon agréable, un soldat efficace et un bon meneur d'hommes, mais avec le temps, il semblait s'enliser dans un monde qui n'avait que peu à voir avec la réalité. Les moines auraient peut-être une réponse, dans leurs prières si ce n'est dans leurs potions.

### Damas, ouest de la Ghûta, veillée du vendredi 10 juin 1160

Dans les feuillages, quelques oiseaux nocturnes invectivaient les soldats entassés dans leur territoire. Une nouvelle fois l'armée de Jérusalem avait envahi le riche faubourg damascène et ses hommes y avaient pillé allègrement, infligeant aux munificents jardins plus de dégâts qu'une horde de criquets.

Parti se dégourdir les jambes après de trop longues heures assis à sa table dans sa tente à gérer des inventaires, transmettre des messages ou donner des ordres, Onfroy de Toron, connétable de Jérusalem, passait le long des cordes où on avait attaché les montures de l'hôtel royal. Quelques palefreniers dormaient là, dans le foin, partageant leurs nuits avec les bêtes qu'ils soignaient le jour. Onfroy aimait l'odeur piquante des chevaux, synonyme pour lui de liberté juvénile, perdue depuis qu'il avait pris en charge son office auprès de Baudoin. Il ne regrettait nullement les choses, mais les senteurs éveillaient en lui le souvenir de ses années d'apprentissage, où il vivait quasiment tout le temps en selle.

Il déambulait en dégustant quelques maamouls[^103] qu'un de ses valets avait dénichés au marché. Cette dernière campagne lui offrait également la chance de savourer des dattes parmi les meilleures, au plus frais de leur ramassage. Tout ce qui en était tiré comme pâtisserie ou boisson lui agréait fort et, s'il ne cédait que rarement à la gloutonnerie, il ne résistait que peu à profiter d'un plat apprécié, ni à s'initier à de nouveaux goûts.

Il vit passer Isaac Vacher et l'apostropha amicalement. C'était le neveu du regretté Bernard, qui servait désormais depuis un moment dans l'hôtel royal. Un solide chevalier, habile à la lance, mais parfois trop impétueux, comme tous les bacheliers[^104]. Son air préoccupé, en ces temps prodigues, avait incité Onfroy à le héler. Le jeune homme s'approcha, prit meilleure contenance, faisant bon accueil à un puissant baron qui l'avait toujours traité en ami, voire en parent.

« Je te vois bien aigre alors que tout nous sourit depuis des mois, mon garçon. Quelque souci dont je pourrais te soulager ?

--- J'ai demandé congé pour aller en la cité et il m'a été refusé.

--- C'est sage décision. Nous n'avons nulle trêve avec Damas, ils attendent juste que nous partions. Et, de toute façon, vu que nous avons couru sus à leurs murailles à peine le dernier traité obsolète, tu aurais fort mauvais accueil là-bas.

--- J'ai quelques noms qui sauraient me faire ouvrir des portes, rassurez-vous. Et je parle leur idiome parfaitement.

--- Ainsi que moi ! Mais pour autant, je n'ai guère désir de voir fils de bonne famille se perdre en leur pouvoir en ces temps. »

Onfroy se doutait de ce qui motivait le jeune homme, mais il lui fallait l'entendre afin d'en combattre le bien fondé, aussi posa-t-il la question en proposant nonchalamment un maamoul.

« Et qu'irais-tu faire là-bas ? Nous les avons rançonnés et pillés deux fois en l'espace de quelques mois, ils ne risquent guère d'avoir attentive oreille à tes demandes. »

Isaac hésita à choisir un biscuit, se décida finalement à en avaler un, prenant son temps pour le mâcher tandis qu'il réfléchissait. Il savait que c'était impoli de faire attendre une personne aussi importante que le connétable, mais il avait compris que ce dernier lui avait accordé un délai en même temps que la friandise. Chacun savait exactement ce dont il s'agissait, au fond.

« C'est rapport à mon oncle. Peut-être certains ici en connaissent plus qu'on a voulu nous en dire[^105]. Il mérite qu'on voit s'il n'est pas à espérer qu'on paie sa rançon...

--- Penses-tu que nous n'aurions pas obtenu la libération du porte-bannière du roi s'il avait été en geôle quand Ayyoub a versé les dinars, avant Pâques ?

--- Peut-être ne sait-il pas qu'il est détenu ? Des brigands auraient pu s'en emparer.

--- Que voilà bien redoutable captif pour simples malandrins de chemin. Outre le fait qu'il aurait fallu force soldats aguerris pour prendre Bernard, nul maraudeur n'oserait se brûler les doigts avec si dangereux marron. »

Il se rapprocha d'Isaac, baissant la voix et adoptant un ton paternaliste.

« Tu sais comme j'étais lié à ton oncle. Penses-tu que je n'ai pas cherché ? Que je n'ai pas lancé coursiers et ambassadeurs ? Si le roi l'a battu froid pour cette sombre histoire, il est resté féal en mon cœur. »

Il posa une main robuste sur l'épaule du jeune homme.

« Ton oncle, malgré toute la hardiesse qui l'habitait, a quitté ce monde de douleurs. Admets-le. Sinon tu courras après des ombres. Cela ne diminue en rien l'éclat de sa vie, sa droiture et sa vaillance !

--- Il mérite bien chrétienne sépulture, pour tout cela, non ? Sinon comment pourra-t-il connaître la Résurrection à la fin des temps ? »

Onfroy soupira longuement, retira sa main.

« Il y a bien assez de souffrances en ce monde pour ne pas porchacier les diables, mon garçon. » ❧

### Notes

Le début de l'année 1160 fut relativement faste pour le royaume de Jérusalem. Profitant des difficultés de Nūr ad-Dīn au nord, Baudoin pilla de nombreuses zones entre la terre de Suète, par delà le Jourdain, jusqu'aux abords de Damas. Après une première trêve de trois mois qui lui permit de rapporter son butin en toute sécurité, il revint une nouvelle fois ravager les territoires musulmans avant l'été et le retour du prince d'Alep.

Ultra : *latin*. Au-delà de, par delà, en avant, plus loin.

### Références

Crawley Charles, *Medieval lands. A prosopography of medieval European noble and royal families*, « Jerusalem nobility » http://fmg.ac/Projects/MedLands/JERUSALEM.htm ( v4.0 Updated 27 February 2019, consultée le 11/06/202)

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972.

Flamme, fumée et cendre
-----------------------

### Bilbais, berges du Nil, fin de journée du jeudi 21 septembre 1161

Assis en haut du talus qui plongeait de façon abrupte dans l'eau fangeuse, Droin laissait son regard divaguer sur les voiliers et esquifs à rames, sur les pirogues de roseau et les ambitieux transports au pont encombré de balles de marchandises. Dans l'île face à lui, des chèvres et des moutons broutaient paisiblement à l'ombre de palmiers au pied moussu, indifférents aux crocodiles se gavant de soleil à quelques encablures de là. Une nauséabonde fragrance venait lui titiller les narines, se mêlant aux subtiles odeurs du marché dont il entendait la clameur derrière lui. Enrubanné d'étoffes diverses pour se protéger des ardeurs de l'astre triomphant, il avait terriblement chaud. Il sentait la sueur qui se formait en gouttes sur ses flancs, imprégnant les tissus qui collaient à sa peau.

Il mordit dans le pain plat frotté d'huile que lui avait tendu Nijm puis dans un rude fromage, mâchant lentement, laissant les saveurs exploser en bouche en écho à l'exubérance qui s'offrait à sa vue, son nez, ses oreilles. Pour la première fois de sa vie, il contemplait le Nil.

Cela faisait des mois que ses désormais compagnons de voyage, la tribu des banu Jarm[^106], évoquaient le mystérieux fleuve qui avait donné naissance à Misr et qui lui redonnait un nouveau souffle chaque année. Les Bédouins y venaient de temps à autre afin d'échanger leurs paniers en feuilles de palme, leurs accessoires en cuir de chèvre et de chameau ou le sel de la mer Morte contre des étoffes, du riz ou des roseaux fins. Le sheikh, al-Saghir, avait décidé cette fois de se rendre jusqu'à la cité frontière de Bilbais pour y entendre les dernières nouvelles. De nombreux voyageurs racontaient que de terribles conflits agitaient le pouvoir califal fâtimide. Vivant à la marche des deux royaumes, latin et musulman, il était vital de savoir si des opérations militaires étaient à envisager, pour s'en protéger, y participer du côté le plus apte à remporter la bataille, ou dans l'espoir de se trouver dans les parages lorsque le pillage serait profitable sans risque.

Droin n'était pas à l'aise avec cet opportunisme, mais il se gardait bien de faire le moindre commentaire déplacé, surtout à Nijm qui le traitait tel un frère. Mieux même, vu que les seuls qu'il ait jamais eus, enfant, se comportaient comme son père violent, fainéant et désabusé. Il goûtait juste la compagnie des Bédouins, tolérant leurs façons de faire pour répondre à leurs nécessités en cette terre de sable, de roc et de pierre. Ils étaient la première vraie famille qu'il ait jamais eue.

En contrebas, une longue felouque s'encombrait peu à peu de balles de toile cirée, de paniers et d'outres, de vases et de sacs de grains et de pois. Les senteurs épicées montaient jusqu'à lui, au rythme des rafales qui faisaient claquer le pan de voile mal fixé au mât oblique. Les portefaix s'invectivaient pour emprunter les minces planches d'accès à bord, sous le regard indifférent des matelots, plus concernés par l'assiette du navire et le bon arrimage de la cargaison. En arrière-plan, une fumée grasse s'élevait depuis la palmeraie qui occupait une île proche, peut-être d'une plantation de cannes à sucre ou d'une fabrique de céramiques. Mais elle ne survivait guère aux assauts de la chaleur et du constant zéphyr.

Droin se tourna, flatta le petit chien qui s'était attaché à ses pas depuis quelques semaines, puis lui offrit un croûton. C'était une brave bête, destinée à garder les chèvres et alerter si des prédateurs rôdaient. Droin avait entrepris de le dresser à faire des cabrioles, lui apprenant à sauter pour le plaisir des enfants. C'était une façon pour lui de se faire accepter par ses nouveaux compagnons, encore hésitants sur la manière d'intégrer ce nouveau venu, même après les mois passés ensemble. La plupart se montraient assez amicaux et de bonne volonté, mais aucun n'était aussi ouvert que Nijm et il manquait cette familiarité instinctive née d'une constante proximité pour que Droin se sente parfaitement à l'aise.

Le marché auquel il tournait le dos était un souk de négociants. On y croisait des marchands de retour d'Adan[^107] ou sur le départ pour al-Sham[^108], pour le lointain al-Sūdaan[^109] ou encore de plus exotiques et distantes contrées dont le nom n'évoquait que villes mystères et territoires inconnus. Droin avait abandonné l'espoir de s'y repérer, profitant de la magie de ces noms pour nourrir ses appétits de merveilleux.

« Le vizir est mort. Les amis d'hier se déchirent pour savoir qui sera le maître à Misr. »

La voix étonnamment jeune du sheikh interrompit Droin dans ses rêveries. Le vieil homme venait de s'accroupir vers eux et s'empara d'autorité de l'outre avant d'en avaler une longue rasade. Puis il cracha un peu d'eau dans sa main afin de s'en rafraichir le visage. Le soleil luisait sur la peau cuivrée de ses pommettes, accentuant la noirceur des fosses plissées qui abritaient ses yeux sombres.

« On m'a recommandé à un marchand désireux de remonter vers le nord, au-delà d'Anṭākiyya[^110], chez les Rums[^111]. Il transporte surtout épices, noix de bétel et poivre. Il craint de prendre la mer en cette période. C'est un ami de al-Shada'id al-Namari ibn Musa. Nous pourrons lui faire escorte jusque Gaza, voire al-Khalil[^112] selon la route qu'il choisira. »

Nijm hochait la tête par pur conformisme, personne n'attendant que quiconque mette en question une décision du sheikh. C'était lui qui statuait sur tout dès l'instant où cela impliquait la tribu en son entier. Son pouvoir s'arrêtait à l'entrée de chaque toile, mais s'appliquait en dehors sans la moindre contestation. Du moins tant qu'il ne se trouvait pas un chef de famille plus apte à attirer des fidèles par ses avis et suggestions. Auquel cas on n'appellerait plus al-Saghir qu'abu Sa'id, nom de son aîné, et il rejoindrait le conseil des anciens, mémoire et conseil de la tribu, vénérables assistants du sheikh, soumis à son autorité.

Al-Saghir embrassa le paysage d'un rapide coup d'œil, puis fit un rictus à Droin, en ce qui tenait chez lui d'un sourire.

« Le fleuve a bien nourri la terre. Nous sommes à plusieurs semaines de son point haut et les îles n'ont pas toutes réapparu. Les mois à venir seront prospères alors que les princes se battent. Ce sera une bonne année pour nous, avec toutes ces richesses amassées par les hommes des villes et aucun bras puissant pour nous empêcher d'y prendre notre dû. »

### Faubourgs de Gaza, camp nomade, matin du vendredi 9 novembre 1162

Encore ensommeillé, Droin s'efforçait de réveiller ses sens par quelques vigoureuses ablutions près du bassin où leur modeste campement était établi. Il avait profité la veille des bains de la ville, luxe auquel il s'adonnait à la moindre occasion. Il sentait avec ravissement les effluves de savon qui s'attachaient à sa peau. Ils étaient quelques hommes de la tribu installés aux abords de la localité franque, espérant quelques menus travaux pour l'hiver afin de gagner de quoi s'offrir les raffinements citadins qu'ils ne pouvaient tirer du désert.

Quelques tentes noires formaient une cour centrale où ils tenaient les chevaux, autour d'une poignée de genêts épineux et d'un vieux pin d'Alep aux branches tortueuses. Droin avait envie de faire bonne impression pour sa prestation auprès du sheikh. Celui-ci le sollicitait chaque fois qu'il fallait s'entretenir avec des Latins. Même si c'était souvent laborieux pour Droin, qui ne comprenait que de loin en loin les idiomes trop différents du bourguignon, il y voyait une opportunité d'apporter sa contribution à la tribu. C'était pour lui une responsabilité dont il s'enorgueillissait.

Il finissait de nouer son turban, à la nomade, avec des pans sous le menton, quand il remarqua les Francs dont Nijm lui avait parlé. Il s'avança vers eux afin de les intercepter le premier. Il identifia un solide gaillard aussi hirsute qu'un ours comme étant le chef. Une tête mangée de barbe, deux énormes oreilles en chou-fleur servant de cadre à un imposant orifice nasal trônaient sur un large torse. Droin les salua en langue d'oïl et d'oc, principaux langages du royaume, tout en s'inclinant poliment. Le petit groupe se figea et le meneur le dévisagea avec circonspection avant de répondre en langue d'oïl avec un fort accent, mais de façon intelligible. Ses trois acolytes se contentèrent de hocher le menton.

La tente d'accueil avait été préparée avec soin, comme chaque fois qu'une négociation allait se dérouler ou que des étrangers entraient en contact. Au centre, le sheikh était assis à la place d'honneur, sur un coussin, avec les hommes les plus importants de leur modeste ambassade autour de lui. À ses genoux, quelques plats cuisinés par les femmes avant leur départ ou achetés à des commerces locaux allaient offrir l'occasion d'échanger des banalités avant d'en venir au fait. Puis des nattes soigneusement balayées avaient été déroulées pour les Francs peu au fait des coutumes et qui n'hésiteraient pas à garder leurs souliers pour s'installer. Droin les invita à prendre place puis se concentra sur son travail d'interprète.

C'était pour une fois une tâche aisée, il s'agissait qu'un petit groupe de soldats de fortune qui avaient appris l'appel aux armes de l'ancien gouverneur de Moyenne Égypte, Šāwar. Arbalétriers expérimentés, ils savaient que leur compétence serait appréciée en ces périodes plutôt calmes dans le royaume chrétien. L'émir d'Alep et principal instigateur des combats contre les Latins, Nūr ad-Dīn était en pèlerinage à la Mecque. De son côté, Baudoin se trouvait trop occupé à gérer les territoires sans dirigeant pour envisager des campagnes d'ampleur nécessitant un recrutement.

Droin remarqua que, comme à son habitude, le sheikh, tout en discutant âprement le montant de leur assistance pour traverser le désert méridional, ne laissait échapper aucun commentaire quant aux chances de succès de ces mercenaires. Ce n'était pas la première fois que des Latins allaient ainsi louer leurs bras et leurs lames à des princes d'autres religions, mais il était rare qu'ils en reviennent plus riches. Difficultés de s'adapter à de nouvelles façons de faire, hostilité plus ou moins manifeste des populations, duplicité des dirigeants, rien n'était épargné à ces hommes capables de verser le sang pour remplir leur ventre. Pour avoir un temps envisagé de vivre comme eux, Droin appréciait de ne pas partager leur sort.

Une fois parvenus à un accord, les soldats ne restèrent que peu, montrant là aussi leur méconnaissance des usages. Ils se comportaient selon leurs habitudes, sans chercher à s'intégrer à ceux qui allaient les guider jusqu'aux rives du Nil. Ils seraient donc traités ainsi que des bêtes qu'on doit mener, avec rigueur et attention, mais sans complaisance. Ils n'étaient qu'une tâche à accomplir, dont la tribu, toujours inquiète de sa réputation et de son honneur, se chargerait avec sérieux et application, mais sans volonté de se lier. Des étrangers partageant le même chemin.

Droin raccompagna les Latins et retourna à la tente. Il était d'usage que chacun fasse ses commentaires sur les gens que le groupe allait escorter. On ne revenait jamais sur la transaction, mais les modalités d'accueil étaient supputées, les sentiments, craintes et espoirs de chacun étaient entendus et soupesés. Au fil des mois et des voyages, Droin avait senti que la confiance, bâtie sur l'acceptation sans réserve de Nijm, se renforçait. Il avait apprécié à sa juste valeur qu'al-Saghir approuve une de ses remarques, quelques mois plus tôt, en lui attribuant de façon solennelle le nom que chacun lui donnait faute de mieux. Et ce n'était pas un moindre honneur que le vieux sage le désigne ainsi, de ce sobriquet initié par plaisanterie par Nijm : *al-Nazif*[^113]. Depuis, il s'en était fait une véritable identité, veillant à sa tenue, à sa propreté comme il ne l'avait jamais fait jusque-là. Il se sentait enfin lavé des sarcasmes infantiles.

Tout en discutant, les hommes finissaient les plats, dont un succulent sumaghiyyeh[^114] et du poisson frit, mariné et farci. Mais le principal attrait de la ville demeurait le zibdieh, réchauffé au maigre feu du camp, et les plus gourmands se brulaient les doigts en tirant les crevettes du ragoût fumant.

« Il nous faudrait encore un ou deux commanditaires et nous pourrons prendre la route, indiqua al-Saghir. Mais pas des soldats. Marchands ou voyageurs seulement. Il est toujours mauvais que des lames inconnues soient en trop grand nombre parmi les nôtres.

--- Si la guerre se prépare à Misr, les hommes voudront rentrer protéger les leurs. Nous avons plus d'un mois avant le départ prévu. Nous pourrions envoyer quelques frères à Asqalan[^115] battre le rappel.

--- J'en ai parlé à quelques-uns de nos familiers ici, attendons de voir. Je n'aime pas savoir nos fils au loin en ces terres de villes malodorantes et de forteresses de pierre. »

Comme tous les nomades, al-Saghir vouait une grande méfiance aux provinces urbanisées, aux territoires aménagés et avait plus confiance dans le lion, le crocodile ou la panthère que dans le citadin dont il n'escomptait guère de bien. Et il était d'une circonspection quasi maternelle envers les gens de la tribu.

« Je pourrais aussi aller traîner auprès des entrepôts génois ou pisans, proposa Droin. Ces hommes ont le commerce dans le sang, ils ne sauraient refuser les perspectives de profit des luttes à venir. »

Alors qu'il allait avaler une bouchée, le sheikh suspendit son geste, visiblement surpris qu'al-Nazif prenne la parole spontanément pour la première fois, et pas en réponse à une question. Puis il hocha le menton, avant d'engloutir la crevette. Face à Droin, Nijm était tout sourire.

### Désert du Negev, abords du canyon du Nahal Zin, nuit du youm al arbia 10 chawwal 558[^116]

La lueur de la lune ne pouvait concurrencer les flammes dansantes qui peignaient d'ambre et d'ocre les falaises environnantes, les peupliers et les arroches des abords du camp. Le clan Tha'laba s'était regroupé au fil des jours et les derniers attendus avaient établi leurs tentes la veille. Il était donc plus que largement temps de célébrer l'événement : les sheikhs avaient convenu de se retrouver là pour parler de l'avenir des familles. Des moutons étaient marchandés, des chameaux accouplés, des alliances négociées, des naissances préparées et les troubles en Égypte alimentaient de nombreuses discussions pleines d'espoir.

C'était la première fois qu'al-Nazif participait à de telles réjouissances. Il regardait tout cela comme dans un rêve, aidant les gens de sa tribu à installer le camp, entretenir les feux ou à choisir les bêtes abattues pour les repas. Il habitait toujours la tente de Nijm, désormais marié, et y vivait comme son frère. Les deux hommes partageaient leurs biens et leurs moutons, et le petit Hamid l'appelait « mon oncle » de façon très naturelle.

Al-Nazif et Nijm finissaient de s'habiller pour la fête, ajustant leur coiffure, lissant leurs barbes et nouant avec soin leur ceinture. Ce soir allait se danser le dabkeh, chaque tribu faisant démonstration de ses talents avant que tous se mêlent en une immense célébration. Al-Nazif avait l'habitude de se joindre aux autres, mais il sentait toute l'importance du moment. Bien que tous appartinssent au même clan, il fallait défendre l'honneur de ses proches. Et il savait que les langues allaient bon train sur son compte. Il demeurait un étranger pour beaucoup de ces fils du désert et le plus petit manquement rejaillirait sur les Banu Jarm, qui en lui feraient payer le prix d'une façon ou d'une autre. Il avait veillé à une stricte observation des prières depuis le début du rassemblement et ne relâchait son attention à ses moindres gestes qu'une fois de retour dans sa toile, à l'abri des murs de broussailles, parmi les siens.

« Ne sois pas si inquiet, frère, tu n'es pas, et de loin, le plus mauvais danseur parmi nous. Et le ras[^117] de ce soir n'est pas tant aventureux...

--- C'est facile pour toi, qui a usage de telles fêtes, mais c'est pour moi prime fois !

--- Et je peux t'assurer que tu y tiens ta place. Tu vas te gâcher le plaisir à tant t'inquiéter. Profite un peu, laisse-toi porter. Si tu dois chuter durant le dabkeh, *inch'hallah* !

--- Et mon frère pense que ça va me réconforter ? » ricana al-Nazif, trop angoissé pour arriver à rire de la plaisanterie.

Les deux hommes avaient à peine quitté leur tente qu'ils virent venir à eux al-Saghir. Toujours vaillant en dépit d'une boiterie grandissante, il tenait fièrement son rang et marchait à grandes enjambées malgré la souffrance que cela lui infligeait. Il accompagnait un autre vénérable à la barbe grise, sheikh ou ancien d'une des tribus. Ils s'arrêtèrent devant Nijm et al-Nazif, faisant croître l'angoisse dans le cœur de ce dernier. Il avait commis une faute majeure et on venait le lui signifier, il le devinait à l'air grave des deux hommes. Al-Saghir attendit qu'ils saluent dans les formes et prit la parole.

« Voici abu al-Fazari, mon cousin. Nous bavardions des liens entre nos tribus et il se trouve qu'une de ses nièces est bonne à marier. Je lui ai dit qu'al-Nazir vivait encore en célibataire chez son frère et qu'il serait temps pour lui d'avoir sa propre tente. Nous partagerons donc tous trois le repas sous la toile d'abu al-Fazari ce soir, que je puisse en discuter avec lui, avant le dabkeh. Il aimerait en savoir plus sur vos bêtes et vos biens. » ❧

### Notes

La réalité matérielle de la vie quotidienne dans les populations bédouines est difficile à appréhender pour la période médiévale et une bonne dose d'interpolation à base d'études ethnographiques récentes ou de récits de voyage modernes ne permet que des reconstructions hautement hypothétiques. Un des caractères tant vanté de cette vie, célébré à l'envi dans la poésie arabe est par exemple une certaine intangibilité au fil des siècles. Illusion bien pratique dont on affuble souvent les cultures traditionnelles recourant peu ou pas à l'écrit. Tout ce qui concerne donc les aspects les plus intimes et les plus domestiques des aventures de Droin ne demeure que des spéculations de ma part, inférences de lectures transversales de données éparses.

Le titre vient d'un dicton arabe : « la vie est comme un feu, flamme, fumée et cendre ».

### Références

Bianquis Thierry, *La famille arabe médiévale*, Bruxelles : Éditions Complexe, 2005.

Bonnenfant Paul, « L'évolution de la vie bédouine en Arabie centrale. Notes sociologiques », dans *Revue de l'Occident musulman et de la Méditerranée*, n°23, 1977, p. 111-178.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Cowan gregory, *Nomadology in architecture. Ephemerality, Movement and Collaboration*, Dissertation for master in Architecture, University of Adelaide, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I à 6, Berkeley et Los Angeles : University of California Press, 1999.

Goitein Shlomo Dov, Friedman Mordechai Akiva, *India traders of the Middle Ages. Documents from the Cairo Geniza ('India Book')*, Leiden - Boston : Brill, 2008.

Mayeux F. J., *Les bédouins ou Arabes du désert*, 3 tomes, Paris : Ferra jeune, 1816.

La main gauche de la nuit
-------------------------

### Jaffa, faubourgs, abords du quartier hospitalier, tôt le matin du mercredi 16 novembre 1138

Accroupi au-dessus du mince ru formé par les intermittentes pluies matinales, un enfant jouait avec des brindilles, imaginant de frêles esquifs brinquebalant sur des rapides. Indifférent à la boue froide qui maculait ses pieds, il déplaçait de sa baguette les échoués, réalignait les désorientés, poussait les retardataires.

« Ma femme voulait qu'on l'appelle Luce...

--- Il serait mieux qu'elle porte un nom des miens, nous en avons déjà parlé. »

Les voix graves des deux hommes s'entendaient à peine, chuchotées par-dessus le petit enfant emmailloté de hardes que l'un d'eux tenait dans ses bras. Arborant une ample barbe grisonnante, celui-ci, qui s'exprimait depuis l'abri d'un renfoncement avait échangé le bambin contre une bourse où cliquetait du métal. Le visage las, il semblait pressé de repartir chez lui, d'oublier la sordide transaction. Le second, jeune homme au physique noueux dans des vêtements fatigués, s'attardait à dévisager le minuscule être qui allait quitter sa vie : une fille qui lui avait pris son épouse, Marguerot, morte en couches et enterrée le mois précédent. Soulagé de n'avoir plus à s'occuper de cet enfant dont il ne savait que faire, il n'arrivait pas à briser ce fil ténu et se tenait là, tête nue sous les gouttes, cherchant à assouvir, sans espoir, son amour de père.

« Il faudrait partir, maintenant, souffla le barbu. Nous risquons d'attirer l'attention à nous.

--- Vous pourriez pas la baptiser, au moins ? Même si vous lui choisissez autre prénom que Luce ? »

Le porteur du bébé soupira. Il ne souhaitait pas répondre à cette question absurde. Il ferait bien sûr bénir la nomination de sa fille, qu'il pensait appeler Rébecca, comme sa propre mère. Mais il préférait ne pas indiquer à ce Celte que cela se déroulerait à la synagogue. Il se contenta de ramener l'enfant contre lui, dans sa chaleur.

« À palabrer ainsi, nous risquons de la réveiller et d'attirer les curieux. Il est temps... »

Comme il allait partir, Gautier tendit un pouce hésitant et traça le signe de croix sur le front de sa fille. Puis il se détourna, cherchant du regard Lotier, toujours occupé à pousser ses bâtonnets dans le ruisselet. Il lui prit la main sans ménagement, s'écartant à grands pas en direction des quartiers est de la ville.

« Avance, gamin. On va se trouver de quoi se nourrir sur la route d'Acre. Le chemin sera long.

--- On retourne pas chez les bons pères ?

--- Non, c'est fini pour nous, ça. On va s'en retourner au Nord.

--- Et Luce, elle vient pas ? »

Gautier lança un regard peu amène à son petit garçon, espérant que cela suffirait à le faire taire. Depuis quelques semaines, Lotier avait appris que les coups pouvaient rapidement pleuvoir. Il baissa donc le menton, trottinant à la suite de son père dans les flaques de boue en direction de la porte orientale. Ils passèrent sans encombre au milieu des gardes, qui ne s'occupaient que des entrants, et prirent le chemin au nord, parmi la palmeraie et les jardins qui bordaient la côte aux alentours de Saint-Nicolas. Une pluie fine, mais insistante les incita à profiter de l'abri d'une halte où ils purent échanger quelques derniers méreaux de l'Hôpital contre de la nourriture.

Assis contre le mur du bâtiment, à l'abri des regards et des gouttes, Gautier vérifia son honteux pécule, de minces rondelles d'argent étincelant. Il espérait s'installer à Acre le temps de trouver une façon de remonter vers les siens, à Antioche. Il avait quitté la grande cité afin de fuir les conditions de vie misérable de sa famille, manouvriers et journaliers sans le sou. On vantait alors les riches terres du sud, offertes à qui voulait les travailler. Il avait convaincu Marguerot de l'épouser pour vivre ce rêve méridional.

Il n'avait pas escompté que sans avoir, nul arpent n'était concédé. Une fois encore le bien allait aux plus aisés. Les deux jeunes mariés avaient donc erré, louant leurs bras saison après saison. La naissance de Lotier avait affaibli Marguerot, mais elle n'avait pas accepté de rester en retrait : elle pouvait abattre le travail d'un homme sans se plaindre. Ils avaient toujours trouvé de quoi s'employer, traversant de-ci de-là la plaine de Sharon selon les mois, quémandant auprès des frères de Saint-Jean lorsque la jointure se faisait rude.

La seconde grossesse de Marguerot, manifeste durant Carême, n'avait été que souci et inquiétude. Alors que son ventre s'arrondissait, ses traits se creusaient, sa peau virait au gris. Elle eut juste assez de souffle pour donner naissance à sa fille. Cependant, elle rendit son âme à Dieu sans jamais avoir entendu son enfant crier. Encombré d'un garçon en bas âge en plus d'un bébé, Gautier avait fait ce qu'il pouvait, abandonnant ses maigres économies auprès de nourrices pas toujours aimables ni serviables. Et la mauvaise saison voyait les prix du pain s'envoler tandis que le travail se faisait rare. Au Nord, il ne serait plus seul, au moins.

### Tyr, plage septentrionale, début d'après-midi du lundi 6 janvier 1141

Clopinant dans le sable derrière Gautier, Lotier sentait les embruns projetés par les vagues. Il aimait l'odeur saline, les relents de putréfaction et d'algues de la côte. Il rêvait de se faire marin, imaginait que son père et lui prendraient un bateau pour retourner chez eux, au nord, à Antioche. Il n'osait pas en parler, de crainte de se voir rabrouer. Gautier était un fermier, qui ne connaissait que la terre, préférant se crotter de boue tandis qu'il maniait la houe ou la pelle plutôt que de se risquer en mer. Et avec le temps, il ne s'adressait plus à son fils que par injonctions, imprécations ou vitupérations. Le gamin avait appris à se faire docile et muet.

Quelques pêcheurs attardés débarquaient leurs paniers sous des nuages menaçants, la tête dans les épaules. Si les cieux n'étaient guère cléments, les prises ne s'en ressentaient guère et la cité était florissante. Gautier espérait toujours un bon prix des poissons les plus petits, ceux qui s'étaient abîmés dans les mailles des filets ou qui présentaient une difformité. Il faisait cuire le tout dans un infâme potage où le père et le fils trempaient leur pain à tour de rôle. Rien que d'y penser, Lotier en avait la nausée.

De lourds nimbus venus du large poussèrent chacun à se hâter vers un abri, en les murs. Gautier et Lotier rejoignirent juste à temps le modeste appentis appuyé à l'enceinte qu'ils louaient. Le propriétaire, un hirsute musulman tisserand de voiles, ne parlait qu'avec difficulté la langue des conquérants et ne s'intéressait guère à ce qui se passait dans son local tant que le loyer était payé. Ce qui convenait parfaitement à Gautier.

Il s'était installé là pour les mois d'hiver, cherchait à demeurer inaperçu autant que possible. Il se louait parfois comme portefaix, mais la concurrence était rude. Dans l'arrière-pays, il était quasiment impossible de trouver de l'emploi. En dehors des casaux peuplés d'Européens, les autochtones étaient généralement réticents à l'idée d'embaucher un Latin, dont ils savaient qu'ils ne pourraient se défendre devant les tribunaux en cas de problème. Au fil des mois, le capital qu'il avait à son départ de Jaffa avait irrémédiablement fondu.

Il s'agaçait aussi de devoir se préoccuper sans cesse de Lotier, ou de devoir payer pour sa garde lorsqu'il trouvait quelques travaux nomades, escorte de voyageur pour l'entretien des bêtes ou des chantiers où les enfants encombraient. Peu après la Saint-Michel, il avait dû s'en remettre une nouvelle fois aux frères de Saint-Jean pour soigner les fièvres du garçonnet, contribuant à contrecœur aux dépenses de simples et de potions nécessaires pour son rétablissement complet. Il ne pouvait reprocher aux moines de s'assurer qu'il poursuive le traitement qu'ils avaient offert gracieusement plusieurs jours durant, mais c'était là un luxe dont il se serait bien passé. Ils l'avaient aussi critiqué de ne pas surveiller assez l'hygiène de Lotier. Ce qui l'avait amené à sermonner durement le gamin pour qu'il prenne l'habitude de se laver plus souvent à la fontaine.

Ils s'installèrent sur le grabat qui leur servait de lit, profitant de la chiche lumière de la petite fenêtre. Gautier attendait la fin de l'averse pour allumer le feu au-dehors. Son bailleur lui interdisait de cuisiner à l'intérieur ou d'y apporter la moindre flamme, par crainte des incendies. Il maugréait sans cesse contre ce zèle précautionneux, mais en admettait le bien fondé. Il ne tenait surtout pas à attirer l'attention sur lui, s'estimant de passage dans une ville qu'il n'avait guère envie d'apprendre à apprécier.

« Père, j'ai mal au ventre...

--- C'est la faim, le repas arrivera tantôt.

--- Je ne sais, ça me tord les boyels depuis hier... »

Gautier posa la main sur le front du gamin. Il était trop chaud, clairement fiévreux !

« Où es-tu allé traîner encore ?

--- Nulle part, père, je reste ici quand... »

La gifle tomba comme un couperet, réhaussée d'un regard qui en annonçait une autre en cas de récidive.

« Si tu restais ici à l'abri comme je dis, t'aurais pas respiré males vapeurs. Tu sais combien tes fantaisies vont de nouveau coûter ? Tu crois que je trime pas assez ? »

D'agacement, il secoua et poussa Lotier sur la couche.

« Mets-toi au moins sous la couverture. Qu'on raconte pas encore que je suis mauvais père. »

En disant cela, il jeta un coup d'œil dehors. Le temps semblait se dégager, il pourrait peut-être préparer le repas dès à présent, que le gamin avale un plat chaud. Il ne savait jamais si c'était la chose à faire. C'était là le travail d'une femme, pas le sien. Il maudissait le sort de lui avoir pris celle qui aurait dû le soutenir en cette période difficile. La veille, à la messe, il avait prié pour le repos de l'âme de Marguerot. Il avait aussi demandé à la Vierge de protéger sa petite Luce, confiée à des mains inconnues. Il soupira longuement. Si seulement ce gamin savait se tenir tranquille et arrêter ses bêtises, les choses seraient tellement plus simples !

### Jérusalem, parvis du Saint-Sépulcre, fin de matinée du vendredi 13 avril 1145

Embauché par l'Hôpital pour faire escorte à des pèlerins venus de Beyrouth, Gautier s'était résolu à profiter de son séjour à Jérusalem pendant la Semaine sainte. Il n'avait jamais assisté aux grandes célébrations de Pâques dans la capitale du royaume et s'émerveillait de la foule qu'il y croisait. Le ventre plein de son repas du matin, il s'aventurait pour la première fois hors de l'enclos des frères de Saint-Jean, découvrant l'édifice qui hébergeait le tombeau du Christ et la Croix qui l'avait vu mourir. Il était un peu hésitant à y entrer, oscillant sous le haut tympan sculpté, esquivant les pèlerins qui allaient et venaient.

Perdu dans ses pensées, il ne prit pas garde à l'approche d'un homme de grande taille, le cheveu brun et le menton fier, dans une tenue dont on sentait qu'elle l'endimanchait plus qu'elle ne le vêtait.

« Que les démons m'emportent ! C'est bien le Petit ! Gautier le Petit ! Si je m'attendais à ça ! »

Ainsi apostrophé, il dévisagea celui qui se présentait face à lui, mains sur les hanches, opposant sans y prêter garde un récif aux flots de dévots, irrités de se voir gênés dans leur progression spirituelle.

« Ça alors, le Grand ! Que fais-tu en la Cité ? Tes Pâques ?

--- Oui-da. Il est bien temps ! J'ai suivi tes traces au sud.

--- Comment ça ?

--- J'ai décidé mon vieux père à me doter suffisamment pour venir chercher bonnes terres par ici. J'ai trouvé beau manse auprès du Saint-Sépulcre, en un casal pas loin, au septentrion, dans les collines de Judée. »

Gautier hocha la tête. Aymeric appartenait à une famille de charpentiers débarqués avec les premiers conquérants normands. Leur maison était florissante et il ne faisait aucun doute que même s'il n'était pas l'aîné, il avait eu de quoi s'installer confortablement.

« Et toi alors ? Tu avais marié la Marguerot avant de partir. Vous avez prospéré ? Vous êtes dans les environs ?

--- Non, je suis seul, je... »

À la mine soudain déconfite de Gautier, Aymeric comprit que le sujet était sensible. Il entoura son ami d'enfance d'un bras protecteur.

« Foin de ces commérages, compère. Allons donc trouver taverne où réjouir nos gosiers de festives retrouvailles. Christ va ressusciter et le Grand et le Petit sont de nouveau ensemble. Que peut-on espérer de mieux ? »

Sans même escompter une réponse, il prit le chemin de Malquisinat, se contenant de sourires pour tout échange en attendant de pouvoir se poser. Il estima en un clin d'œil que la situation de Gautier n'était pas prospère, ses vêtements élimés en ces temps de fête indiquant certainement le secours de quelque institution charitable. Et sans épouse pour veiller sur sa tenue, il était notoire que nul ne savait entretenir son linge correctement. Aymeric avait longuement regretté le départ de son compère de toujours, compagnon de bêtises et de punition, confident et équipier de ses aventures infantiles. Même si sa famille n'approuvait guère qu'il s'acoquine avec un enfant des classes les plus modestes, Gautier était son ami et il le défendait contre vents et marées.

Ils trouvèrent un banc où s'assoir près d'un commerce de soupes, d'où s'échappait le fumet d'un excellent pain. Munis d'un pichet de vin, ils commencèrent à trinquer aux retrouvailles et il fallut peu de temps à Aymeric pour vanter le casal où il s'était installé voilà quelques mois, après de longues recherches.

« C'est à deux pas de la Cité, en direction de Naplouse et la Samarie, dans un des coins les plus sûrs de la région. Ici pas de Turkmènes nomades qui viennent piller ou de soudan belliqueux. Et les clercs du Saint-Sépulcre font honnêtes conditions.

--- Ils ne veulent de certes pas simple manouvrier comme juré en leurs terres. J'ai bien tenté ma chance un peu partout, jamais on m'a fait bon accueil.

--- Hé quoi, dira-t-on de moi que je n'aide sochon dans le besoin ? Si je me porte garant, je doute que frère Pisan, l'intendant, voie d'un mauvais œil ton arrivée.

--- C'est fort amiable à toi, compère, mais je n'ai ni fer ni monnaies. Je n'ai que mes bras à louer à qui veut.

--- Que je peux jurer à tous comme valeureux ! Je pourrais t'employer un temps, j'ai tant à faire ! Le moment venu, nous trouverons à qui emprunter de quoi acquérir ce qu'il te faudra pour avoir ton propre manse. Accueillir fidèle compaing à la Mahomerie, voilà qui m'enjoierait le cœur. »

Gautier sentait un poids qui disparaissait de ses épaules. Suivre les pas du Christ était peut-être tout ce qu'il avait eu à faire pour que ses problèmes s'évanouissent à jamais. Il sourit à son ami, qui prit cela comme une invitation à poursuivre.

« Quoi qu'il en soit, tu dois voir mon espousée avant la fin des Pâques ! Ou, mieux encore : nous pourrions écouter messes de concert, comme quand nous étions garcelets auprès du tombeau de saint Julien ! »

### La Mahomerie, église paroissiale Sainte-Marie, veillée du mercredi 17 mai 1150

Quelques veilleuses éparses dans les chapelles scintillaient dans la noirceur de la grande église. Installés dans la nef principale, sur des bancs autour de la maigre lumière d'une lampe, Gautier, son épouse Alison et le père Pandouffle, curé de la paroisse, discutaient tranquillement. Sur les genoux de la jeune femme, un bébé somnolait, repu d'une longue tétée. Le prêtre essayait de faire entendre raison à Gautier pour le baptême de la petite fille, prévu pour la Pentecôte prochaine.

« Je ne disconviens pas que ce soit joli, mais comme le dit Alison, il est d'usage que l'enfant soit nommé ainsi que sa marraine, en l'occasion.

--- D'autant que tu n'as pas voulu nommer notre garçon comme son parrain, Aymeric. Ne sera-t-il pas vexé de voir que tu fais de même avec son espouse ?

--- Il comprend bien mes raisons. C'est de coutume en la famille de mon père : le premier fils est appelé Lotier et la première fille Luce. Pensez-vous que je sois si mauvais enfançon que je respecte pas mes aïeux ?

--- C'est là fort louable désir. Mais l'usage commun, de certes sage et ancien, a pour bienfait de fort marquer les liens. Il nous fait souvenance de la proximité de cette famille de cœur, choisie.

Gautier maugréa entre ses dents. Il avait pris cette décision sans trop savoir pourquoi et refusait d'en démordre sans plus de raison. Mais il sentait au fond de lui-même que c'était la chose à faire. Il ne pouvait juste pas parler de ses deux premiers enfants. Personne, pas même Aymeric, n'était au courant à la Mahomerie et il craignait les réactions si on apprenait ce qu'il avait fait. Il se disait qu'il tenait l'occasion de se racheter de ses précédentes fautes. Il avait dorénavant un manse à lui, une jeune épouse pleine de santé, qui lui avait déjà donné deux petits dont la vigueur était renforcée par une saine alimentation. Cette nouvelle famille serait heureuse et il saurait s'y comporter en bon père. Il avait désormais quelques arpents à labourer pour y récolter de quoi tous les nourrir. Bien plus que les siens n'en avaient jamais eu ! Avec Luce et Lotier à qui offrir une belle vie. ❧

### Notes

L'idée initiale de ce Qit'a est venue de la lecture de l'interdiction par le Concile du Latran III de la vente d'enfants « aux sarrasins et aux juifs » (en 1179). On sait que bien souvent les textes réprimant certaines pratiques permettent d'en deviner l'existence. Il faut malgré tout se méfier d'en faire trop de généralités, car cela peut aussi être l'émanation de craintes diffuses et profondes de la société. Elles peuvent s'exprimer sous cette forme très officielle sans que la réalité en soit avérée clairement. Dès l'instant où des interdictions cléricales font référence aux autres religions, la circonspection me semble de mise. Je n'ai ainsi pas découvert de procès intenté pour de tels agissements à la période qui m'intéresse.

Je me suis dit que ce pouvait être l'occasion de se pencher sur les relations à l'intérieur d'une famille, sur ce que pouvaient être de mauvais traitements et la question des pratiques genrées à l'époque médiévale. Là encore, je n'ai rien trouvé de bien clair et documenté sur ce qui pouvait exister factuellement et j'ai décidé de construire ce récit en m'inspirant de ce que j'ai pu retenir de lecture plus générales, ou d'exemples plus lointains, comme celui de Montaillou.

L'idée du traitement du personnage de Gautier m'a été insufflée par l'excellent roman d'Ursula Le Guin, *La main gauche de la nuit*, où les questions de dualité et de genre au sein d'une société (extra-terrestre dans son cas) sont posées de façon à nous renvoyer à nos propres usages. De plus le titre fait référence à un texte poétique interne à l'ouvrage que je trouve particulièrement évocateur et inspirant (dans sa version originale : Kroeber Le Guin, Ursula, *The Left Hand of Darkness*, New York : Ace Books, 1969) :

> Light is the left hand of darkness,\
> and darkness the right hand of light.\
> Two are one, life and death, lying\
> together like lovers in kemmer,\
> like hands joined together,\
> like the end and the way.

Traduction française © Jean Bailhache pour les éditions Robert Laffont :

> Le jour est la main gauche de la nuit,\
> et la nuit la main droite du jour.\
> Deux font un, la vie et la mort\
> enlacés comme des amants en kemma,\
> comme deux mains jointes,\
> comme la fin et le moyen.

### Références

Alexandre-Bidon Danièle, Lett Didier, *Les enfants au Moyen Âge*, Paris : Hachette, 1997.

Bourin Monique, Durand Robert, *Vivre au village au Moyen Âge. les solidarités paysannes du XIe au XIIIe siècle*, Rennes : Presses Universitaires de Rennes, 2000.

Kroeber Le Guin Ursula, *La Main gauche de la nuit*, Paris : Robert Laffont, collection « Ailleurs et Demain », 1971.

Le Roy Ladurie Emmanuel, *Montaillou, village occitan de 1294 à 1324*, Paris : Folio Histoire, 2008.

Les fruits du labeur
--------------------

### Édesse, demeure de Roupen, abords de la citadelle de Thoros, soir du vendredi 15 septembre 1122

L'excellente table du riche négociant avait attiré les estomacs plus sûrement que ses arguments, Roupen le savait. Il escomptait toujours que les mezzés ou les brochettes parviendraient à lui rallier des partisans et ne rechignait jamais à proposer vin et than, halvas et sari bourmas en abondance. Le temps que ses invités ingèrent tout ce qui leur était offert, il avait loisir de développer ses idées sans risquer la contradiction. Ils en étaient à s'échanger l'excellent choreg qui faisait la réputation de sa demeure, préparé en toute occasion. S'affichant plus mécréant qu'il n'était vraiment, Roupen aimait à dire que cette brioche lui avait apporté à elle seule plus de bienfaits que toutes ses prières et ses dons à l'église.

Depuis qu'il avait appris la capture du jeune comte par ses adversaires turks, il n'avait pas ménagé ses efforts pour manifester son soutien à la famille dirigeante des Courtenay. Il faisait partie des Arméniens acquis à la cause du nouveau souverain de ces terres. Il avait l'intention de collecter de quoi aider à payer la rançon, ou, à défaut, de quoi solder quelques hommes pour tenter de libérer le captif. Il savait que l'entreprise était fort hardie, et beaucoup de ses coreligionnaires ne voyaient pas d'un mauvais œil ce souverain trublion et batailleur quitter le devant de la scène. Comme venait de le lui faire remarquer un de ses invités, il n'avait pas hésité à convoler avec une princesse latine, Marie de Salerne, fille du régent d'Antioche après le décès de sa première épouse, princesse arménienne.

« Je pense que tu fais erreur, il a surtout marié la citadelle de Azaz[^118]. Un bon choix, à l'abri, près de ses terres de toujours, qui lui permet de se préparer au mieux pour conquérir Alep.

--- Il aura besoin pour cela de retrouver sa liberté. L'Ortoqide[^119] n'aura guère désir de lâcher si belle prise ! Son oncle Il-Ghazi va le noyer sous les paniers d'or et de joyaux.

--- De cela tu ne devrais jurer. Balak est peu aimé à Alep, il est trop brutal, trop violent, trop impétueux. Je ne serais pas surpris qu'il préfère s'octroyer trésor pour prendre la place du vieux. Ce qui serait stupide, car si \*l'avare perd un œil, l'ambitieux perd les deux[^120].

--- J'entends bien ton désir de flatter les Celtes, Roupen, mais à quoi bon ? Les puissants vont et viennent, j'en ai connu tant depuis mes enfances... Ils ont de commun que jamais leur appétit n'est satisfait. Pourquoi dépenser son or pour l'un d'eux ?

--- Vois tout ce qu'a fait Josselin pour les siens lorsqu'il tenait Tell Bashir[^121] ! Il a le sang du lion, mais un cœur d'agneau avec ses fidèles. Mon fils, Mesrob, qui se trouve à mes côtés, a soldé souventes fois de ses sergents qui pouvaient en jurer. Dis-leur, mon garçon ! »

Le jeune homme, habitué aux jeux rhétoriques de son père, hocha la tête avec métier avant de prendre la parole d'une voix posée.

« J'ai embauché qui-ci qui-là à mon service un Celte nommé Clément, qui n'a que louanges à faire du sire comte. Il est juste avec le faible et rabroue la superbe du fort. Féroce le fer en main, mais équitable et libéral avec les siens. Avec lui, tout semble possible.

--- Ah ! Vous entendez ? Je ne suis pas seul à croire que sa bannière pourrait un jour flotter sur Alep. Le gris gagne dans la chevelure d'Il-Ghazi et regardez ses fils et ses neveux ! Ils se déchireront bientôt. La cité sera comme fruit mûr à prendre pour qui en aura l'audace.

--- *Les chiens qui se battent entre eux s'unissent contre le loup*. Tu vas un peu vite en besogne.

--- Le crois-tu vraiment ? J'ai vu, comme toi, comme nombre de nos amis, arriver ces Celtes d'outre les mers. J'ai pensé aussi que ce n'étaient que nouveaux mercenaires des Roums[^122]. Mais nous avions tous tort. Ils sont venus tels barons en quête de royaumes. Et s'ils ont la vigueur de bêtes féroces, ils sont nos frères de religion... Mieux vaut eux pour nous diriger que des infidèles ou, pire, des syriaques hypocrites.

--- Il n'y aucun bien à attendre d'eux, le père Matthieu de Kaysun[^123] l'a bien expliqué : ce sont des ingrats et des êtres cupides. N'ont-il pas pris la place de Thoros ? Tu as encontré parmi les premiers Baudoin[^124], et tu as fait en sorte que notre prince en fasse son fils. Et depuis, il a remplacé tous les nôtres par des hommes à lui, sans égard pour nos barons.

--- Thoros n'était même pas de notre Église, mais de celle des Romains[^125].

--- Baudoin et Joscelin n'en sont pas plus !

--- *Prêtre à l'extérieur, Diable à l'intérieur* : comme beaucoup de clercs, le père Matthieu déteste les autres religions, car il voit taxes, impôts et offrandes partir ailleurs.

--- Attention, Roupen, il est certaines bornes qui ne se franchissent pas. C'est là saint homme !

--- Alors qu'il pratique un peu la compassion que lui et ses semblables professent à chaque messe ! »

Agacé de ces réticences imprévues, Roupen déchira de ses dents un gros morceau de choreg, maculant sa belle tenue de miettes tandis qu'il mâchait furieusement. Ce fut son fils qui rompit le silence tendu, de sa voix diplomate.

« Je sais par Clément qu'il est possible d'avoir audience auprès de la comtesse ou du sire de Marash, baillis des terres. Nous pourrions déjà lui faire part de nos regrets et indiquer que nous sommes prêts à aider à affranchir Joscelin. Sans entrer dans le détail.

--- Excellente idée, mon fils. Il faudra lui dire que nous donnerons lampes et chandelles pour illuminer les prières pour sa libération. Qui nous porterait reproches de solliciter le Très-Haut ?

--- De certes ! Joscelin a fait montre de grand cœur et d'amitié pour les nôtres depuis qu'il dirige ces terres. Montrons-lui que nous ne sommes pas des ingrats. Prier est bien le moins. »

Roupen se détendit. Si le vieux Missak l'approuvait ainsi, d'autres étaient certainement convaincus. Et si les quelques riches notables de leur communauté qu'il avait réunis là affichaient leur attachement au comte captif, nul doute que d'autres suivraient. Il n'en avait guère fallu plus lorsque les Latins avaient remplacé Thoros, quelques décennies plus tôt.

### Turbessel, écuries de Mesrob, après-midi du dimanche 20 août 1150

La chaude poussière poussée par le vent depuis la cour venait ajouter aux relents âcres de la terre battue. Le sol exhalait des senteurs chevalines, mélange de sueur animale, de déjections, de cuirs échauffés et de graisse, de foin entassé. Une vingtaine de riches négociants, influents sages ou prospères propriétaires, s'était installée au mieux pour débattre de la situation. Ils regardaient, impuissants, les musulmans s'emparer des cités les unes après les autres et le pouvoir arménien refluer au fur et à mesure. Leur collaboration avec la couronne latine leur portait dès lors fortement préjudice et leurs rivaux syriaques voyaient là une bonne occasion de se venger de décennies d'humiliation ou de rebuffades.

Face au groupe, deux hommes se tenaient. L'un d'eux avait le physique un peu lourd, la barbe sombre négligée et il aurait pu passer pour un florissant laboureur s'il n'était habillé de la meilleure des soies, bien qu'elle soit maculée par endroit. Le second en aurait remontré à un piquet en termes de raideur et sa maigreur était renforcée par sa longue robe noire, ornée d'une croix à l'épaule. Il semblait s'impatienter à entendre les tergiversations des notables face à lui, remâchant son angoisse en lançant des regards assassins dans le vide.

Le petit homme à ses côtés finit par frapper dans ses mains, de l'air affable du maître débonnaire qui rassemble ses élèves.

« Mes amis, il ne sert de rien d'ainsi ergoter sur les intentions d'autrui. La chose demeure : Joscelin a vendu ses droits sur nos cités aux Romains.

--- Le sire comte n'aurait jamais permis cela, c'est sa femme et le roi qui ont trahi...

--- Narek, que tu croies cela ne change pas les faits. Nous n'avons pas loisir d'ordonner le monde, seulement d'y cheminer de notre mieux.

--- Facile pour toi de dire cela, Mesrob ! J'ai déjà perdu beaucoup et je crains que mes dernières richesses ne disparaissent aux mains des Turks ! Toi, tu possèdes ânes, mules et chameaux. Il t'est commode d'y charger tes avoirs.

--- Vais-je y mettre ma demeure ? La tombe de mon père pourra-t-elle y être fixée ? Mon frère, retiré en son monastère enfourchera-t-il une de mes bêtes ? Ne sois pas si cruel de me prêter indifférence à la situation. Frère Matthieu est venu nous prévenir sans tarder de la décision royale. Mettons ce temps à profit pour organiser cela au mieux pour tous. Au moins les humbles et les marchands que nous sommes pourront profiter de la vaillance des lances celtes ! Le chemin sera long jusqu'à Antioche, même sous leur protection. »

Il se tourna vers le jeune homme qui se tenait jusque-là en retrait, derrière lui.

« Mon fils Gushak revient d'Antioche, où il est établi depuis quelques années en mon nom. Il connaît les sentiers et les voies, il saura mener marcheurs et bêtes. »

Quelques têtes acquiescèrent sans enthousiasme, mais plusieurs visages demeuraient fermés, voire hostiles. Parmi les bourgeois rassemblés, certains voyaient leurs derniers avoirs en grand péril d'être pillés, dérobés ou simplement détruits. Et la perspective de quitter tout ce qu'ils avaient connu jusqu'alors ne les enchantait guère. Malgré tout, la crainte de rencontrer le même sort que les Édesséniens planait sur tous les esprits. Fuir et sauver ce qui pouvait s'emporter ou rester et risquer de perdre bien plus.

Mesrob échangea un regard un peu dépité avec le frère hospitalier.

« D'aucune façon, je ne saurais vous pousser à demeurer ou pas. Chacun décidera pour lui ! En ce qui me concerne, mon choix est fait, les miens finissent de préparer mes balles. Et j'ai disposé de quelques bêtes pour y mettre avoirs de ceux qui auront besoin. Nous organiserons cela dès l'aube à venir.

--- Moi je reste, déclara avec arrogance un vieil homme au visage flasque. Les Romains sont bons guerriers. Rien ne dit qu'ils feront pis que les Celtes ! »

Indifférent à la pique, l'hospitalier prit la parole, à son rythme réfractaire aux aléas des temps.

« Je ne saurais parler pour les Griffons[^126], mais je n'ai nul connoissance de troupes en chemin. Les quelques forteresses et cités devront, je le crains fort, s'en remettre à leurs propres troupes, au moins pour les semaines à venir. Le roi a bien conscience de cela et c'est là charité de sa part que d'offrir à nos frères et sœurs en grand malheur de marcher à l'ombre de sa bannière, jusqu'en des terres plus assurées.

--- Et pourquoi ne pas combattre et repousser nos ennemis ?

--- Partout les barons tombent. Baudoin ne saurait tenir seul face à tous ces assaillants. Il lui a fallu faire terrible choix, que nul n'a loisir de commenter, entre un péril et l'autre. Nous ne sommes qu'hommes de peu à l'aune de tels géants, bien grands sont leur devoirs. »

Mesrob se rapprocha des notables, la démarche lourde, ses mains et ses bras s'agitant tandis que la ferveur le prenait.

« Il est vrai que nous ne sommes pas forcément tous du même avis ni que nul ici n'est ami de chacun. Mais il nous faut faire montre de fraternité en ces temps troublés. Il y a deux partis entre lesquels choisir. Ma décision est prise et, pour la plupart de vous, c'est aussi le cas. Accordons-nous au moins là-dessus et annonçons autour de nous cette division, sans chercher à en tirer influence ou fidèles. Laissons à chaque chef de famille le soin de se rallier à l'un ou l'autre, en lui indiquant juste où aller et comment se mettre en branle.

--- Et puisse Dieu tous nous guider en ces périlleux chemins » précisa à voix basse l'ecclésiastique.

Mesrob n'était pas naïf au point de présumer que nul ne tenterait de tirer parti des circonstances. Il avait trop voyagé, négocié, pour conserver une opinion innocente sur le monde. Mais il avait aussi appris à ne pas toujours croire le pire inévitable. Il se trouvait quelques bonnes âmes au sein de ses supposés soutiens, et même parmi ses opposants, pour faire en sorte que la communauté s'en sorte au mieux. Lui-même avait pris ses précautions depuis des années, et son réseau de transport s'appuyait sur des partenaires de toutes les confessions. Il avait déjà des idées de la façon dont il pourrait gérer la situation sans trop y perdre si, comme il le pensait, les dernières cités arméniennes tombaient aux mains des dynasties turques. Cela lui donnait la tranquillité d'esprit requise pour prendre en charge les nécessités de sa communauté. Après tout, n'était-ce pas le rôle de l'élite, de s'occuper des plus modestes ?

### Jérusalem, rue de David, matinée du vendredi 3 juillet 1153

Les deux hospitaliers qui descendaient la rue de David étaient aussi dissemblables qu'il était possible. L'un d'eux était émacié et maladif, long et rigide dans tous ses gestes, les traits empreints de tristesse ou de sérieux. L'autre s'étendait quasiment autant en largeur qu'en hauteur, ondulant de toute sa rondeur à chaque pas. Son visage poupin était noyé de la graisse de ses excès alimentaires, son double menton prolongé par un goitre surplombant une bedaine achevant de supprimer tout angle de sa personne. Il avançait à petits pas, essoufflé du moindre effort, s'essuyant régulièrement la face ruisselante de sueur. Les chaleurs estivales l'incitaient généralement à demeurer au plus frais des caves, parmi les vivres qu'il inventoriait, classait et, parfois, goutait, pour le grand hôpital de Saint-Jean.

« N'était-il pas possible à ce négociant de venir en nos murs, mon frère ? Je n'ai pas usage d'aller ainsi quémander...

--- J'ai cru comprendre que nos celliers sont fort dépourvus de fruits, malgré la saison. Et cela alors que l'afflux de pèlerins ne saurait tarder avec les fêtes de la mi-mois. Il y a urgence, chacun en a convenu au chapitre, ce tantôt.

--- De certes, de certes, mais l'empressement est souventes fois mauvais conseiller. »

Frère Matthieu adressa un regard mi-surpris, mi-vexé au gros cellérier. S'il pouvait reconnaître le déplaisir de frère Raoul à subir les chaleurs, il n'appréciait guère d'être possiblement persiflé par un glouton victime de ses propres vices. Lui-même ne se plaignait que fort peu, et toujours raisonnablement, des humeurs fiévreuses qui l'accablaient régulièrement et l'incitaient au repos absolu.

Le message fut parfaitement perçu par le clerc habitué aux échanges muets et il fallut qu'ils bifurquent dans la rue des Arméniens pour que frère Raoul ose briser de nouveau leur silence gêné.

« Vous me disiez que cet homme n'est pas de notre église, c'est cela ?

--- Il est de la meilleure famille qui soit, ermin des terres perdues du nord. Je l'ai connu tandis je compaignais le roi Baudoin après la capture du comte Joscelin. Sans lui, nul doute que beaucoup auraient péri lors de la périlleuse pérégrination depuis Édesse.

--- Et vous dites qu'il aurait de quoi approvisionner nos réserves ?

--- Je sais qu'il fait négoce de produits frais et secs, et nous devons en trouver suffisance. *La mer se fait de gouttes*. »

Le cellérier acquiesça, faisant ondoyer ses replis de graisses. Il était en charge d'une partie des stocks et depuis qu'il était jeune clerc, c'était la première fois qu'il avait si peu d'avance à disposition. Depuis quelques années, l'ordre fournissait chaque année plus de lances aux expéditions militaires et, vu que c'étaient des frères profès et pas de simples soudards employés temporairement, il fallait aussi les nourrir. Cadets de grandes familles et descendants de barons puissants, ils n'étaient guère enclins à se contenter de gruau insipide ou de hareng dur comme semelle. Un peu contrit de reprocher à d'autres son péché le plus flagrant, le moine s'éclaircit la gorge tandis qu'ils approchaient de la boutique.

À l'angle de la rue des Écrivains, la bâtisse était de belle apparence. Deux palmiers dépassaient du mur d'enceinte, apportant dans la cour une ombre bienvenue. Un des côtés était occupé par une écurie, flanquée d'une imposante citerne. En face, un large hôtel surplombait des arcades où le marchand faisait son négoce et entreposait ses balles et paniers. Alors que les deux hospitaliers entraient, un petit homme au turban bien ajusté s'avança, chassant de la main un valet qui s'approchait. Son visage, bien que replet, était anguleux, avec un nez en forme de bec et des orbites sombres où se noyaient ses yeux bruns.

« Mestre Gushak, voici frère Raoul, le cellérier dont je vous ai entrenu hier.

--- Soyez les bienvenus en ma demeure, frères » répondit l'arménien avec un léger accent.

Sa voix douce contrastait avec son attitude énergique. Il était habillé de façon sobre, fonctionnelle, et, les doigts tachés d'encre, paraissait occupé à quelque tâche d'écriture. Il appela rapidement pour qu'on leur apporte de quoi boire et manger. Ce faisant, il les guida jusqu'à une petite salle contigüe à ses entrepôts, où des suffah soigneusement disposés incitaient à la discussion ou à la sieste. Ils parlèrent un peu de généralités, du siège d'Ascalon qui durait depuis des mois, des craintes de hausse du prix du grain avec les récoltes mitigées au sud et des rumeurs de pirates dans le nord avant d'aborder ce qui les intéressait vraiment.

« Je voyage beaucoup pour mes affaires, au Bilad al-Sham[^127] essentiellement, mais aussi Triple[^128], et al-Misr[^129] à l'occasion. J'achète sec ou frais, mais transporte assez peu ainsi, car les pertes sont trop grandes. Je ne le fais qu'à la demande de confrères, souventes fois par gourmandise de baron ou de puissant...

--- Nous avons toujours usage de raisins secs, de figues et d'abricots, et surtout de dattes. Principalement, mais sans se limiter à cela.

--- De tout cela, je connais bons producteurs, et j'ai pas mal d'amis qui font ces négoces. Je pourrais envoyer quelques lettres et acheter en fonction de vos souhaits.

--- Il me faudrait assavoir les prix usuels que vous pratiquez, avant de pouvoir me prononcer. »

Gushak ouvrit un petit coffre et en sortit un carnet de papier, épaisse liasse de feuillets où une écriture serrée avait noté des listes de chiffres.

« J'ai là quelque historique des tarifs que l'on me fait. Je suis prêt à acheter en votre nom à ces montants, sans y prendre la moindre obole en sus. »

Frère Raoul en eut le souffle coupé. Il n'était pas rare que des marchands négocient leur place au paradis par quelque rabais, mais jamais il n'avait entendu pareille proposition, qui lui paraissait farfelue, voire suspecte. Il fronça les sourcils.

« *L'homme doit gagner son pain à la sueur de son front*. Il me semble bien excessif de faire telle offre...

--- Je serai franc avec vous, mon frère : si on sait que j'achète pour Saint-Jean de Jérusalem, cela ne sera pas pour une ou deux balles. Les prix seront d'autant plus généreux que le volume sera important. Je me fais fort d'obtenir pour moi appréciable escompte pour attribuer votre commande. »

Frère Raoul gloussa. L'homme était habile, tout en s'affichant assez droit. Cela lui plaisait. Si Dieu avait fait le marchand rusé, cela devait servir quelque dessein. Et puis avoir de nouveau accès aux dattes damascènes mettait en joie son palais. ❧

### Notes

La question de la mobilité sociale est relativement récente dans les travaux historiques. Néanmoins il s'agit selon moi d'un thème central de l'univers Hexagora, en ce qu'il concerne les rapports sociaux et les paysages mentaux des communautés. Plus encore que l'horizon géographique, cela conditionne les espoirs et les attentes des personnes. Même s'il n'est pas toujours aisé d'en tracer les contours pour les périodes médiévales, surtout les plus hautes, plusieurs études semblent aller dans le sens d'un assez grand conservatisme en ce qui concerne les groupes.

En effet, bien qu'il existe quelques destins ou trajets individuels qui parviennent à franchir les barrières, il est rare que cela fasse souche et que les lignées s'installent de façon pérenne dans un autre milieu que celui d'origine. Les cercles dominants, que ce soit au niveau politique, économique ou intellectuel, paraissent avoir été particulièrement réticents à l'intégration de nouveaux éléments sur le long terme. On pourra noter que certaines études indiquent que cet immobilisme s'est maintenu au moins jusqu'au XXe siècle dans certaines zones européennes.

### Références

Amouroux-Mourad Monique, *Le comté d'Édesse 1098-1150*, Paris : Librairie orientaliste Geuthner, 1988.

Barone G, Mocetti S, « Intergenerational mobility in the very long run: Florence 1427-2011 », Temi di Discussione, Bank of Italy working papers, No. 1060, 2016.

Clark Gregory, Cummins Neil, « Surname and Social Mobility in England, 1170-2012 », dans *Human Nature*, 2014, Vol. 25, p. 517-537.

Corak Miles, « Income Inequality, Equality of Opportunity, and Intergenerational Mobility », dans *The Journal of Economic Perspectives*, 2013, Vol. 27, No. 3,p. 79-102.

Dostourian Ara Edmond, *Armenia and the Crusades, Tenth to Twelfth Centuries - The Chronicle of Matthew of Edessa*, Lanham - New York - Londres : University Press of America, 1993.

Padgett John F., « Social Mobility, Marriage, and Family in Florence, 1282-1494 », dans *Renaissance Quaterly*, 2010, Vol. 63, No. 2, p. 357-411.

Perho Irmeli, « Climbing the ladder : Social Mobility in the Mamluk Period », dans *Mamluk Studies Review*, 2011, Vol. 2011 / XV, No. 1, p. 19-35.

Commère
-------

### Clepsta, hostel de Girout et Clarin, soirée du dimanche 6 avril 1158

Malgré un relatif bon accueil le matin, Joris sentait la tension qui s'était installée au cours de la journée. Sa sœur n'était jamais d'un enthousiasme débordant, mais il avait compris à mille petits signes indéchiffrables pour d'autres que quelque chose la chiffonnait. Son époux Clarin lui-même n'avait fait aucun effort pour se montrer aimable, quoiqu'on n'aurait pu dire si c'était dû à son caractère taciturne, à un mécontentement engendré par quelque raison personnelle ou une simple conformation à l'humeur de sa compagne. Quant à Marie, jeune orpheline fiancée de Joris, elle se faisait une telle fête d'être accueillie dans ce qu'elle voyait comme sa future famille qu'elle ne semblait pas être sensible à l'atmosphère tendue.

Lorsque Girout proposa à Joris de l'accompagner dehors pour rentrer les poules et prendre un peu de quoi alimenter le feu, il comprit qu'elle souhaitait en fait s'entretenir avec lui en privé. Le moment des explications était venu. Sa sœur et son beau-frère habitaient un beau manse, mais dont le potager était distant afin de profiter d'une irrigation collective. C'était également là que les volailles étaient regroupées le soir, dans un cabanon branlant contre lequel s'appuyait un tas de bois et la niche d'un féroce mâtin qui surveillait les parcelles cultivées. Indifférent au gros animal qui vint le renifler, Joris posa la corbeille et entreprit d'y charger des bûches. Il laissait à Girout l'initiative et demeura silencieux tandis qu'elle faisait un tour du jardin afin de vérifier qu'aucune attardée n'y traînait.

Elle revint finalement vers lui, jeta un coup d'œil dans le poulailler avant de mettre en place la planche de fermeture. Puis elle s'essuya les mains sur son tablier tout en le regardant faire.

« C'est pas bien malin d'avoir rien dit, mon frère. Tu espérais quoi en me faisant pareille surprise ? »

Décontenancé d'une telle entrée en matière, Joris se figea et posa lentement la bûchette qu'il tenait avant de se relever, l'air ébahi.

« De quoi tu causes ?

--- Ta Marie, là, tu penses que ça se voit pas qu'elle est païenne ? »

Devant l'absurdité de la remarque, il pouffa ostensiblement.

« Élevée par les sœurs de Saint-Jean ?

--- Tu entends très bien de quoi je parle, te fais pas plus bête que t'es. Elle est pas si bonne chrétienne que son nom le prétend...

--- Elle a perdu père et mère et grandi entre les murs d'un couvent, en la Cité. À ce compte, nous sommes tous deux plus païens encore !

--- Que voilà Paradis aisément acquis ! Mais *la caque sent toujours le hareng*, nul doute que ses ancêtres ont craché sur la croix ! »

Joris souffla, devinant la colère qui montait en lui.

« Et alors ? On doit lui faire grief d'un temps passé qu'elle a pas connu ?

--- C'est pas de ça qu'il est question, tu le sais bien !

--- Eh bien non, je ne sais ! Raconte donc, à moi qui suis si fol ! »

Girout se mordit la lèvre, semblant soudain hésitante. Sa colère se muait en confusion, ses doigts se mêlant à l'étoffe de son tablier sans qu'elle y prête attention. Elle s'assit finalement sur un billot destiné à fendre le bois, se frotta plusieurs fois le visage. Sa voix se fit plus calme quand elle se décida à répondre.

« Je n'ai rien contre elle, frère, elle me semble gentille garce. Mais as-tu désir que tes enfants soient traités comme nous ? Que ta Marie soit méjugée ainsi que mère l'a été ?

Joris dévisagea sa sœur longuement sans répliquer, puis s'assit à son tour sur un caillou appuyé au muret.

« J'ai pas pensé à ça. Pour moi elle est bonne chrétienne. Tu aurais rencontré sa marraine, la Mabile, t'aurais aucune crainte là-dessus. Elle arriverait à faire reproche de mécréance au patriarche lui-même si elle en avait l'occasion.

--- Va l'annoncer à tous les gamins, les voisins et les langues acides qui médiront d'elle et de vos enfançons. Mioche, j'en ai entendu plus qu'à mon tour, des « *fille de païenne* », quand c'était pas pire. Même le Clarin, il y allait de sa ritournelle à l'époque. Et si le père m'a fait belle dot, c'était aussi pour ça. Personne ne voulait épouser la fille de *la sarrasine*.

--- Mère n'était nulle païenne, elle communiait à l'Église comme tout le monde.

--- Elle a reçu le baptême pour se marier. Quand t'es venu, ça s'était un peu tassé, mais pour le Pierre et moi, ça a été dur... »

Girout soupira longuement, n'en finissant pas de se nettoyer les mains de son chiffon.

« Si on apprend que t'as marié une fille comme elle, ça va encore nous retomber dessus. »

Elle marqua une pause, se relevant en se tenant les reins.

« Et t'avise pas d'aller faire visite au Pierre. Il verrait ça d'un mauvais œil...

--- Tu veux dire qu'il serait moins charitable que ma sœur qui me fait remontrance d'avoir encontré bonne fille à épouser chez les nonnes de Saint-Jean ?

--- Sois pas insolent ! Je l'ai servie à ma table ! Et je discuterai avec le Clarin, savoir comment ça va se passer. Mais le Pierre, lui, il t'ouvrira même pas sa porte. »

### Mirabel, manse des Terres d'Épines, matin du mercredi 3 septembre 1158

Savourant l'abri du petit appentis qu'il avait dressé contre un des murs de la maison, Joris mangeait un léger gruau, rehaussé de quelques raisins secs tout en profitant de l'ombre. Le jour n'était pas très avancé, mais la chaleur et le soleil étaient déjà bien présents. Il avait passé la matinée à finir de ramasser les épis de sorgho chez un voisin et il leur fallait désormais emprunter une mule pour porter le tout sur les toits où ils seraient mis à sécher. Joris avait négocié d'être rémunéré en grain, car il comptait bien emblaver sa parcelle dès le printemps suivant.

Il avait bien avancé le défrichement et s'y employait tous les jours où il ne trouvait pas d'ouvrage aux alentours. Il était loin d'avoir de quoi ensemencer tout ce qu'il avait nettoyé, mais, grâce à l'aide de l'intendant, il avait commencé à discuter avec quelques villageois dans le but d'y planter leurs graines et d'en partager la récolte. Comme il devrait aussi leur louer l'attelage et l'araire pour préparer le sol, il n'aurait qu'une faible part de la moisson, mais cela ne le dérangeait pas. Il voyait chaque jour son domaine s'agrandir, chaque soir sa maison l'accueillir.

Tandis qu'il finissait son écuelle, il suivait des yeux quelques nuages approchant de l'ouest, de la côte. On lui avait dit que c'était la saison des orages et il redoutait que de fortes averses ne viennent compromettre les récoltes. Un temps égal, des pluies suffisantes au bon moment, du soleil en quantité et pas de froid précoce, voilà ce qu'il demandait dans ses prières nocturnes. En plus de la bonne santé de son épouse, dont le ventre s'arrondissait chaque jour. Il avait trop vu de femmes mourir en couches pour ne pas s'en inquiéter. Même si c'était en périphérie de son esprit, la crainte ne le quittait jamais vraiment.

Marie revenait justement du lavoir, sa corbeille de linge sur la hanche. Ils avaient bénéficié d'un don de vieux draps de l'Hôpital, rapiécés et effilochés, mais dont elle pensait pouvoir tirer chemises et langes pour l'enfant à naître. Essoufflée, elle s'assit à ses côtés, étendant ses avant-bras humides au soleil pour les faire sécher.

« Le fils à Hellysent et Bernar a malines fièvres, encore une fois.

--- Tout le temps malade, ce gamin...

--- Ils ont demandé au père curé de le baptiser, au cas où. »

Joris hocha la tête. Les bébés mourraient, il n'y pouvait rien.

« Elles parlaient aussi de la foire à venir, pour la Saint-Michel. C'est usage ici que colporteurs s'assemblent en nombre. On y trouve toiles, jarres et outils de fer...

--- Je dirais pas non à une houe d'acier, pour sûr, mais on n'a rien pour payer. Et on est trop tard venus pour obtenir crédit auprès d'eux.

--- Odouart se porterait pas garant ? »

Joris haussa les épaules. Odouart avait le manse voisin du leur et leur avait fait un relatif bon accueil. Lui aussi était relativement désargenté et travaillait comme un forcené à améliorer son maigre domaine. Frère cadet d'un paysan bien installé dans un casal d'Acre, il avait décidé de tenter sa chance par lui-même. Son épouse était employée en tant que nourrice dans des maisons nobles des environs d'Acre, ce qui lui apportait quelques maigres revenus pour s'équiper. Bien implanté dans le village, on l'écoutait lors des assemblées et il paraissait en bonne voie pour finir au conseil des jurés et des anciens.

« Je lui en causerai tantôt. On doit épierrer la parcelle du contrebas dans les jours qui viennent. »

Marie approuva en silence, hésita à se lever, puis, avant cela, prit son courage à deux mains.

« On pourrait profiter de la foire pour faire porter message à ta famille.

--- Y'a pas tant à leur dire.

--- Quand même ! Clarin et Girout nous ont reçus. Ce serait bien qu'ils sachent pour le gamin qui vient. »

Joris continuait de racler sa gamelle, pourléchant sa cuiller de bois entre deux phrases.

« On aura bien temps à l'hiver.

--- Pourquoi attendre ? Moi, je serais heureuse d'avoir bonnes nouvelles des nôtres. Ça t'enjoierait pas, pareil message de ta sœur ou de ton frère ?

--- J'ai pas toujours su pour mes neveux. C'est pas tant important quand on vit loin.

--- On peut les inclure dans nos prières, ou leur prévoir menu présent quand on les voit. C'est comme ça qu'on fait, non ? »

Joris fit une moue peu éloquente, se leva avant de faire mine de rentrer dans la maison.

« Je vais aller couper le bois de nos terres, si jamais on me cherche. »

Comprenant qu'il cherchait à éviter la discussion, Marie acquiesça du menton.

« Moi je vais me rendre au jardin. Après le linge, j'ai du zaatar[^130] à ramasser et je voudrais nettoyer le petit coin au levant. Si on pouvait y mettre des fèves, ce serait bien.

--- Il faudrait aussi de l'eau, la jarre est presque vide, j'ai vu tantôt. »

Marie opina, mais elle n'était pas décidée à accepter si aisément sa défaite.

« Tu me diras ce soir, pour le message ? Certains des marchands viennent de Jérusalem, ce serait l'occasion ou jamais. »

### Clepsta, terres de Girout et Clarin, midi du jeudi 2 octobre 1158

Girout retrouva Clarin en contrebas d'un bosquet d'acacia, appuyé contre un rocher pour en savourer la fraîcheur. Elle vint s'assoir sans un mot et lui tendit une calebasse de vin coupé d'eau. Puis elle s'employa à disposer entre eux de quoi se restaurer. Pas plus bavard qu'elle, Clarin regardait la parcelle de cotonniers qu'avec quelques autres paysans, il dépouillait de ses capsules depuis deux jours. Au-delà, dans les reliefs environnants, des labours commençaient à zébrer de rouge et de brun le gris ocre des lopins brûlés par le soleil estival.

« Je crois pas que ça soit intéressant, le coton. On en fera pas sur nos biens.

--- Tu as eu le nez creux de ne pas suivre le Tassin. »

Clarin ne réagit pas au commentaire de son épouse, habitué qu'il était à voir confirmer la moindre de ses remarques, vite érigée en aphorisme par les siens.

« C'est pas bonne terre pour ça. Et ça gâche de l'eau... On fera comme je pensais, on va plutôt acheter nos propres bêtes, qu'on louera pour le portage, les labours, le foulage.

--- Le bélier se fait vieux aussi, il met moins d'ardeur à couvrir.

--- Si y crève dans l'année, on demandera au Gilet, le Normand. Il a de beaux mâles. Il nous vaudra bien ça, vu comme je l'ai aidé pour sa citerne. »

Tout en échangeant, ils puisaient dans un pot un solide fromage de chèvre, mélangé d'herbes aromatiques, de compotée d'ail et d'oignons, qu'ils étalaient sur un épais pain noir. Face à eux, en aval, un couple de rapaces planait sur les courants chauds, lançant de temps à autre un cri perçant. Tout en mâchant, Girout en vint au fait de ce qu'elle avait en tête depuis quelque temps.

« J'ai eu des nouvelles du Joris, aussi. Par un pied poudreux revenant du sud. »

Sans prêter attention au grommellement de son époux, elle continua.

« Ils auront un petit avant le printemps, à ce qu'il dit.

--- Tant mieux pour eux...

--- Et ils m'ont demandé d'être marraine du gamin, qu'ils appelleront de mon nom si c'est une fille.

--- Pour sûr, vont pas lui donner le nom de ta mère... »

Girout dévisagea son époux, les lèvres pincées.

« C'est pas rien, quand même, lui donner mon nom !

--- Ils doivent escompter que ça leur donnera droit à quelques monnaies...

--- Ils se sont mariés sans même qu'on y aille, je peux bien tenir leur enfançon pour en faire bon chrétien. »

La lueur courroucée qui brilla dans le regard de son compagnon convainquit Girout qu'elle s'aventurait en terrain glissant. Clarin avait catégoriquement refusé de se montrer à la cérémonie. Il ne voulait rien avoir à faire avec « la petite païenne » comme il l'appelait, et certainement pas à Jérusalem, où il connaissait du monde. Il s'était déjà emporté, en lui interdisant de reparler de cette affaire. Elle baissa les yeux, faisant mine de battre sa coulpe. Elle avala quelques bouchées, sans prêter attention à ce qu'elle mangeait. Puis elle revint à la charge, d'une voix adoucie.

« Quand même, Clarin, c'est mon frère ! Et il s'agit de veiller à ce que le gamin soit bon fidèle. Tu sais bien comment il est, le Joris, tête folle. Il est de mon devoir de prendre soin de lui.

--- Tu es désormais de ma famille, surtout. Ils t'apitoient avec leurs histoires d'enfançons. T'as le cœur trop tendre, voilà ce qu'il y a. Dès que ça cause de marmots, tu te fais tout miel... »

Le ton était plus dépité qu'irrité, cette fois. Girout conserva un mutisme stratégique, attendant que le processus fasse son chemin dans l'esprit sévère de son époux. Il n'était pas si mauvais homme qu'il se plaisait à en faire démonstration. Elle ne fut donc pas étonnée quand il lâcha abruptement :

« Il est espéré pour quand, ce marmot ?

--- Le début de Carême. Ça sera pas aisé pour elle, surtout que ça sera son premier. Je serais de bonne assistance, c'est là soucis de femmes.

--- D'aucune façon, ça sera déjà bien assez si tu y vas. On garde nos monnaies pour le bétail, on a rien à distraire pour eux. »

Girout hocha le menton. Il était hors de question qu'elle se présente chez son frère les mains vides pour une naissance. Elle avait bien en tête quelques vieux vêtements et linges mis de côté qui seraient utiles. Et d'ici là, elle trouverait moyen de se procurer quelques jouets pour le bébé. De toute façon, ce n'était pas là affaire d'homme. Clarin n'avait rien à en savoir.

### Mirabel, manse des Terres d'Épines, soir du mardi 27 janvier 1159

Profitant de la présence de sa sœur, Joris avait pu s'absenter pour participer à une veillée chez un voisin. On allait y discuter des labours de printemps, de l'organisation des travaux collectifs et de la rotation des cultures. Les décisions faisaient habituellement l'objet d'un consensus, si ce n'est d'un accord général, de façon à ce que les récoltes soient les plus avantageuses pour chacun. C'était pour lui la première veillée où il s'absentait sans inquiétude, sachant que son épouse était entre de bonnes mains.

Marie et Girout avaient donc soupé toutes deux, assises sur le bord de la couche. Gênées de se retrouver pour la première fois seules, elles se contentaient de remarques pratiques et de questions anodines. Girout éprouvait un léger pincement au cœur de découvrir l'indigence dans laquelle Joris vivait. Il n'y avait pas de table, juste deux tabourets et un coffre en guise de meubles, et la plupart de leurs biens étaient entassés dans des paniers plus moins bancals, certainement l'œuvre de Joris lui-même. Elle avait aussi pu voir leurs réserves, fort basses.

Elle prenait conscience que son petit frère était devenu chef de famille, et qu'il s'employait ardemment à avoir un foyer prospère pour les siens. Ce n'était plus le gamin au nez morveux qui enchaînait bêtise sur bêtise. Elle regretta un instant de n'avoir pas apporté plus, même si elle n'avait aucun doute que Clarin en aurait fait une scène.

Marie avait le visage émacié, les traits tirés de fatigue et de douleur, mais elle refusait de se poser. Il faudrait pourtant la convaincre de s'allonger un peu, avec l'arrivée du bébé proche. Il était mauvais que la mère soit épuisée, Girout le savait. Elle irait voir quelles voisines pourraient aider, et qui était la meilleure accoucheuse par ici. Elle avait découvert avec surprise qu'on ne regardait pas trop de travers sa belle-sœur dans le casal. Elle y avait constaté que la communauté était assez diversifiée dans les environs, et que les villages indigènes y étaient plus nombreux.

« Je n'aurais jamais cru qu'un bébé puisse autant bouger, lâcha tout-à-trac Marie, tandis qu'elle se tenait le ventre.

--- Moi, mes filles ont toujours été très agitées. Les garçons étaient plus fainéants, occupés à dormir. »

Marie sourit.

« Je n'ai guère de souvenirs de femmes grosses, quand j'étais enfant. Au couvent, on ne les voyait que pour la délivrance...

--- C'est fort heureux pour la réputation des nonnes, ironisa Girout dans un rictus, tout en se levant pour débarrasser leurs écuelles. Je vais aller les frotter.

--- Tu es mon invitée, ce n'est pas à toi de le faire ! s'exclama, indignée, Marie.

--- Tu es grosse jusqu'aux yeux. Il est temps pour toi de souffler un peu. C'est mieux pour le bébé.

--- Tu vas tout de même pas faire mes corvées en ma demeure ?

--- Eh quoi, penses-tu que Joris les fera ? »

L'idée d'imaginer un homme occupé à la cuisine ou à l'entretien du linge les fit pouffer toutes les deux.

« Je suis sérieuse, Marie. Je suis là pour ça. La famille... »

Elle ne finit pas sa phrase, comprenant que le mot en disait bien assez en l'occurrence. Puis elle entreprit de frotter les récipients de bois avec de la cendre, avant de les rincer rapidement dans le seau.

« Chaque fois que mes enfants sont nés, mes sœurs sont venues, et plusieurs voisines ont aidé, aussi. C'est le moment où se soutenir entre femmes. Pas d'hommes pour nous diriger, il faut en profiter. Ils ont tellement peur de ce qui se passe alors qu'on a enfin la paix, pour quelques trop courtes semaines.

--- Joris est bon époux, tu sais.

--- J'espère bien ! Sinon je pourrais encore lui frotter les oreilles, tout chef de famille qu'il est désormais. »

Redevenant sérieuse, Girout revint s'assoir vers Marie. Elle ne pouvait s'empêcher d'éprouver de l'affection pour cette femme courageuse. Mais tellement naïve ! Elle avait l'impression d'entendre ses petites sœurs.

« Tu sais, Marie, tu as de certes encontré bon garçon avec mon frère. Mais si se marier ça donne une famille, ça te pose surtout des chaînes aux pieds. » ❧

### Notes

Les activités quotidiennes des femmes des classes populaires demeurent extrêmement difficiles à décrire précisément. Les sources sont très éparses (je me sers abondamment des travaux autour de Montaillou, postérieur de plus de 100 ans), vu que cela concerne un univers fort éloigné de ce qui nous est parvenu, issu généralement de milieux aristocratiques, cléricaux, urbains et, surtout, masculins. La difficulté est de ne pas proposer une lecture a posteriori progressiste et donc d'imaginer obligatoirement le pire, ou de tomber dans un angélisme célébrant les temps passés. Le monde était rude pour tous, hommes et femmes, et il n'est pas évident qu'elles aient été aussi puissamment dominées que dans certaines périodes ultérieures.

Il demeure malgré tout un élément sur lequel nous avons des informations relativement abondantes, c'est la complexité de l'accouchement et de ce qui tourne autour. Il s'agissait assurément de la première cause de mortalité féminine, ce qui explique peut-être le succès des vocations religieuses. C'était un moment où la société relâchait son emprise sur le corps féminin, ainsi que sur les injonctions diverses, préférant isoler la future mère, au moins jusqu'à ce qu'on nommait les *relevailles*. Il s'ouvrait alors un espace de non-mixité certainement propice au passage d'une culture féminine si ce n'est féministe, comme on peut en voir dans les assemblées au sein des hammams dans les communautés méditerranéennes. C'est en tout cas l'hypothèse que j'ai décidé d'adopter.

Le titre vient d'un sens ancien du mot, qui désigne les personnes liées par le baptême d'un enfant.

### Références

Hodgson Natasha R., *Women, Crusading and the Holy Land in Historical Narrative*, 2017, coll. « Warfare in History », 304 p.

Le Roy Ladurie Emmanuel, *Montaillou, village occitan de 1294 à 1324*, Paris, Gallimard, 2008, coll.« Folio/Histoire », n˚ 9.

Lewis Katherine J. et al. (éd.), *Young Medieval Women*, New York, St. Martin's Press, 1999.

Shahar Shulamith, *The Fourth Estate: A History of Woman in the Middle Ages*, trad. par Chaya Galai, New York, Routledge, 1983.

Nonette suis
------------

### Jérusalem, palais royal, après-midi du vendredi 24 mai 1129

Le petit jardin embaumait les jacinthes, offrant l'abri d'une tonnelle où le jasmin prospérait. Quelques massifs de roses, amoureusement entretenus, s'épanouissaient sous le soleil déjà chaud. Assise sur un banc, une fillette attendait, l'air grave. Vêtue d'une simple robe de laine grise, les cheveux noirs nattés avec soin, elle regardait ses pieds se balancer entre l'ombre et la lumière, fredonnant paisiblement. À ses côtés, une poupée de chiffon habillée d'une superbe étoffe de soie contemplait de ses yeux de bois les abeilles occupées à butiner.

Une porte grinça, sortant l'enfant de sa torpeur. Elle tendit le cou afin de voir dans la pénombre du petit couloir bordé de colonnes : c'était sa sœur aînée, la princesse Mélisende, qui venait à elle, environnée comme toujours de quelques suivantes, servantes ou filles de baron. Elle se déplaçait toujours avec un certain aréopage qui soulignait son importance, concédant un intérêt poli aux pépiements qui l'entouraient sans cesse. Mais son regard était rivé sur Iveta, qui l'attendait patiemment.

La fillette sauta sur ses pieds et fit une révérence des plus formelles. Mélisende lui accorda en retour un sourire gracieux qui illumina son visage tandis qu'elle s'avançait pour serrer contre elle l'enfant qu'elle n'avait pas vu depuis des semaines.

« Ma chère Iveta. Chaque fois que je te regarde, j'ai l'impression que des années ont passé, à te découvrir si bel !

--- Merci, ma sœur.

--- Je t'en prie, foin de ces balivernes qu'on a a dû te mettre en tête. Je vais prendre époux et être couronnée reine, de certes, mais je demeure avant tout ton amie. Rien n'a changé entre nous. »

Iveta adressa un sourire plein de fossettes à son aînée et se laissa approcher plus gracieusement, non sans avoir attrapé sa poupée. Sans un mot, d'un simple geste de la main, Mélisende chassa ses compagnes et vint s'installer à l'ombre des feuillages.

« As-tu encontré celui qui sera à mes côtés sur le trône[^131] ?

--- Pas encore. »

La fillette hésita un instant.

« Il est si vieux qu'on le dit ?

--- Pas tant, se mit à rire Mélisende. Mais il a déjà fort bataillé et gouverné.

--- Il est vrai qu'il a renoncé à devenir roi d'Angleterre ?

--- Pas du tout ! Il a renoncé à ses terres là-bas, en Anjou, mais c'est son aîné qui a épousé celle qui sera reine. »

La jeune fille hocha la tête gravement. Puis se tint coi un long moment avant de reprendre d'une voix faible, hésitante.

« Il y aura un prince pour moi aussi ?

--- Pourquoi t'en inquiéter, mon enfant ? C'est souci de père, et le mien.

--- Matilda dit que je vais devoir épouser mon cousin Joscelin[^132] vu que c'est le seul qui reste de mon rang.

--- Matilda n'est qu'une niaise qui n'y connaît rien. Nous te trouverons bon mari en France, en Normandie ou chez les Germains. »

Iveta se renfrogna, lissant la tenue de sa poupée de façon machinale.

« Pourquoi je ne pourrais pas demeurer, comme mère, en l'abbaye Sainte-Marie ?

--- Ne dis pas de sottises. Elle était fatiguée et âgée, elle a souhaité y finir ses jours. Mais elle a fait son devoir avant cela.

--- Prier pour vous, mes sœurs, et demander aux saints et aux saintes de protéger le royaume, ne voilà pas une tâche digne de moi ?

--- Tu ne sais pas encore, mon enfant. Tu es trop jeune. »

Iveta acquiesça poliment, mais sans se défaire d'une moue éloquente. Elle dévisagea sa poupée, traçant du doigt les motifs sur le tissu chatoyant.

« J'ai souvenance d'un gentil prince, quand j'étais petite. Usamah. Il riait à tout instant, heureux de ses chasses ou du bon temps qu'il prenait. Pourquoi ne pourrais-je pas vivre selon mes désirs ainsi que lui ?

--- S'il était prince, il avait de certes moult devoirs, Iveta. »

Mélisende étudia le profil de sa jeune sœur, qu'elle considérait presque comme sa fille. Si leur différence d'âge n'était pas si importante, en tant qu'aînée, elle avait dû grandir vite. Et Iveta avait servi d'otages quelques années plus tôt, le temps que leur père paie sa rançon, ce qui les avait séparées de longs mois. Mélisende acceptait ce difficile destin de princesse, et l'embrassait volontiers, mais elle conservait une pointe d'affection pour sa benjamine, qu'elle espérait protéger des aspects les plus rudes de la vie.

« Entrer en religion te fermera les portes du monde, ma mie. Est-ce bien l'inclination de ton cœur ?

--- Je me sens bien plus à mon aise priant au parmi des sœurs que lorsque je vois père gouverner les barons de la Haute Cour. Je n'ai que faire de porter belles robes ou d'avoir un époux. Pour cela le Christ me semble le meilleur choix à faire. »

Mélisende acquiesça gravement. Puis elle se releva, épousseta machinalement sa tenue qui n'avait pas un pli et sourit à Iveta.

« Si ton désir persiste de prendre voile, j'en parlerai à père. Laisse-moi du temps pour trouver le bon moment. Et, quoi qu'il arrive, tu ne saurais demeurer simple nonette dans l'ombre d'une abbesse, fut-elle mère de Saint-Anne ! »

### Jérusalem, palais royal, matin du mardi 8 octobre 1146

La chambre de la reine était baignée du soleil matinal, faisant resplendir les riches couleurs des tapis, des étoffes, briller les bronzes et les bois cirés. Les fenêtres ouvertes laissaient pénétrer les bruits de la cité, du palais, des pèlerins tout proches. Assise sur un magnifique coffre peint au pied du lit, Ivetta détonnait par sa sobre tenue : une simple robe de laine sombre et un voile clair qui cachait sa chevelure. D'un air absent, elle feuilletait le somptueux et délicat psautier que Foulques avait fait réaliser pour son épouse quelques années plus tôt. Face à elle, la reine lisait quelques courriers, sans y prêter grande attention.

« Ce n'était pas si mauvais époux, tout compte fait... »

La voix de Mélisende semblait moins autoritaire dans ce cadre domestique, mais son ton gardait un aspect péremptoire. Iveta y était habituée et ne s'en formalisait pas. De toute façon, elle partageait fréquemment les vues de celle qu'elle considérait comme une mère, plus que comme une sœur.

« Il t'aurait sûrement richement dotée. Mais il n'avait jamais envisagé que Matilde puisse lui survivre et que tu ne deviennes pas abbesse de son vivant. Pauvre Foulques... c'est bien de lui, de penser toute femme en inférieure.

--- N'est-ce pas méprise qu'il a souventes fois commise ? s'amusa Iveta, un sourire complice sur les lèvres.

--- C'est l'erreur que tout homme fait » soupira Mélisende.

Elle reposa un des feuillets.

« En dehors du père abbé de Citeaux...[^133]

--- As-tu nouvelles de lui ? Du pérégrinage du roi Louis ?

--- Pas de lui directement, mais il s'active fort à prêcher pour nous. Le très Saint-Père a fulminé de nouvel la bulle apostolique qui appelle à l'aide. L'espoir est grand, si les barons de France et d'Aquitaine viennent nous porter secours. »

Iveta pinça les lèvres, admirant le ciel bleu extérieur un instant.

« Je ne saurais dire si cela m'enjoie ou m'horrifie, d'apprendre que le fer et le feu vont encore dévaster le pays.

--- Nous ne pouvons laisser nos ennemis détruire ce qui importe à nos yeux !

--- Je prie souventes fois pour que le Seigneur leur décille les leurs.

--- Mieux vaut le faire à l'abri de solides murailles, ma sœur. Garder le siècle en deçà ne se peut que si courtines et tours nous protègent. »

Iveta opina doucement. Elle regrettait de temps à autre de ne pouvoir vivre comme une simple moniale. Elle enviait alors Eudeline, sa prieure, qui ne sortait que fort peu de la clôture et s'occupait avant tout de la vie réglée des religieuses. Chaque jour les mêmes prières, les mêmes tâches, en une perpétuelle et réconfortante récapitulation. Mais elle était fille de souverain, abbesse et confidente de la reine de Jérusalem. Elle ne pouvait se retirer du monde aussi commodément qu'elle en avait le désir.

« Il te faudrait faire visite du monastère, découvrir comme toutes choses avancent bien, d'ailleurs. Tu n'es pas venue depuis des semaines.

--- Ce serait avec grand plaisir. Mais trop de requêtes à examiner, de soucis à traiter, qui me retiennent ici ou qui me font voyager en des lieux où je n'ai guère envie d'aller. La mort de Zengi[^134] nous offre beaux espoirs, mais Baudoin n'est que garcelet à peine dégrossi, tout juste bon à trousser tout ce qu'il goûte. Un vrai bélier en rut !

--- Manassès disait ce tantôt qu'il apprécie les choses de la guerre et y montre quelque aptitude.

--- Cela ne suffirait à faire de lui un roi. Il se sait destiné à cela depuis la naissance. J'ai eu moins de temps pour cela que lui ! »

Mélisende se replongea dans ses lectures, laissant Iveta seule dans son examen du livre pieux. Bien que supérieure d'un riche couvent, elle n'avait pas de tels ouvrages personnels, même plus modestes d'apparence, préférant doter l'abbaye en elle-même de tous les objets liturgiques et ostentatoires qui louaient la grandeur divine. Elle avait entendu parler de l'austérité prônée par le dirigeant de Citeaux, qui raillait le décorum usuel des bénédictins, mais il lui semblait logique de célébrer le Tout-Puissant avec les réalisations les plus magnifiques qui soient. Peut-être était-elle trop habituée au luxe oriental pour envisager que Dieu n'y soit pas sensible.

« N'a-t-on pas de nouvelles de l'ambassadeur damascène ? »

Mélisende retint un sourire. Malgré son retrait du monde, Iveta conservait quelques souvenirs infantiles de sa période de captivité à Shayzar. Et son affection, distante, un peu fantasmée, mais réelle, avec le prince munqidhite demeurait un de ces fils ténus.

« Je n'imagine pas qu'on le reverra de sitôt. Unur n'est guère tendre avec les félons. Il serait toujours au Caire, à ce qu'on dit. »

Mélisende leva la tête, soudain songeuse.

« Il nous faudrait d'ailleurs envoyer quelque émissaire à Damas, rappeler à Unur combien nous sommes attachés à son amitié... »

Elle attrapa une tablette de cire et entreprit d'y noter quelques idées, laissant Iveta de nouveau dans ses pensées, plongée dans les détails de la miniature présentant la résurrection de Lazare, Marie et Marthe à ses pieds. Si Iveta se sentait proche de la première, à n'en pas douter Mélisende s'activait autant que la seconde.

### Béthanie, couvent des Saint-Lazare-Marthe-et-Marie, après-midi du lundi 7 octobre 1157

Iveta relisait la charte d'échange de terres avec attention. Le scriptorium en avait fait copie, et elle y avait fait apposer son sceau le matin même. Elle était de temps à autre surprise de l'attrait que représentait sa congrégation et du volume des dons, offrandes et légations qui lui étaient faits. Il était vrai que, situé sur le trajet entre le Jourdain, Jéricho et Jérusalem, aucun pèlerin ne manquait l'occasion de s'arrêter sur le tombeau qui avait accueilli un miracle du Christ. Mais il lui semblait que tout cela était irréel quand elle se trouvait là, dans la simplicité de sa cellule qu'elle avait souhaité dépouillée dans son mobilier et peu ornée sur ses murs. Son seul luxe était la vue qu'elle avait sur le cloître, minuscule paradis terrestre où les massifs de fleurs et la verdure des arbres abritaient de nombreux oiseaux.

Elle en était à vérifier la liste des témoins quand on frappa à sa porte. Eudeline entra sans bruit, suivie par une visiteuse en tenue de novice. Iveta reconnut immédiatement Sybille, sa nièce, la femme du comte de Flandre. Celui-ci s'était de nouveau croisé et son épouse l'avait cette fois accompagné. Mais elle avait choisi de ne pas participer aux campagnes militaires, préférant le calme du monastère pour y attendre le retour de son conjoint. Sybille était la fille d'un premier lit du défunt roi Foulques, et donc la nièce d'Iveta. Mais elle était un peu plus âgée, de quelques années.

« Sybille sollicite audience, mère Iveta. Comme vous m'aviez indiqué que vous gardiez votre porte ouverte pour de possibles entretiens, j'ai pris la liberté de venir directement avec elle. »

D'un sourire, Iveta acquiesça et invita Sybille à s'assoir face à elle. À peine eut-elle eu le temps de le faire qu'Eudeline avait disparu sans un bruit, tirant l'épaisse huisserie derrière elle.

« Tout va bien, j'espère, Sybille ? Il n'est nulle contrariété qui trouble ton séjour parmi nous ?

--- Oh non, bien à rebours ! Et c'est de cela qu'il s'agit. J'aimerais savoir ce que vous penseriez si je devenais professe en vos murs. M'accepteriez-vous ? »

D'étonnement, Ivetta demeura coite un temps, ce que Sybille prit pour une réticence.

« Je suis lasse du siècle et n'aspire à rien de plus qu'au silence et à la prière. Et vous avez ici si bel endroit, épargné de la fureur du monde.

--- J'en serais très heureuse, ma mie, assurément ! Mais en avez-vous parlé à votre époux ? Sans son accord, rien ne se pourra faire.

--- Je voulais d'abord avoir votre assentiment, mais nul doute que mon cher Thierry approuvera. J'ai fait mon devoir, il a fils et filles en suffisance. J'ai même tenu le bayle[^135] de Flandre un temps. Il me voue trop belle amitié pour m'opposer refus. De toute façon, il a bien plus appétit de terres ici que de ma présence. »

Iveta avait entendu des rumeurs comme quoi le bouillant comte de Flandre avait espoir de s'arroger le territoire de Césarée sur l'Oronte[^136] une fois la ville prise. Là où elle avait passé plusieurs années, lorsqu'enfant, elle avait servi d'otage le temps pour son père de payer sa rançon. Il pourrait être ironique qu'elle y retourne un jour pour y fonder un établissement religieux que dirigerait sa nièce. Elle jeta un regard vers la charte, posée entre elles.

« Je confesse que vous avoir à mes côtés serait grand bonheur. Il y a tant à faire ici !

--- Si cela vous était possible, je préférerais n'être que modeste nonette. J'ai bien assez bataillé en ma vie, je n'ai plus goût à toutes ces choses de gouvernement. Prier me suffira pour les jours qui me restent.

--- Si vous m'autorisez à vous demander qui-ci qui-là conseil, cela me conviendra tout à fait. Je comprends combien simple sceau pèse dans la main qui doit le manier. Il m'est souventes fois arrivé d'espérer m'en décharger. Mais Dieu en a voulu autrement.

--- *Amen*. »

Bien qu'elle éprouvât une relative déception de ne pouvoir s'adjoindre Sybille pour les affaires du couvent, Ivetta était heureuse d'imaginer qu'elles partageraient leurs jours dans le monastère désormais achevé. Adolescente, elle avait pris plaisir à fréquenter cette jeune femme, venue avec son père loin de chez elle, dans l'attente d'un époux qui lui serait désigné. Elle se remémora alors combien Sybille avait apprécié la vie dans la Cité. Elle n'était d'ailleurs peut-être pas étrangère à l'attrait de la Terre sainte sur le comte de Flandre.

« Avez-vous eu quelque nouvelle du nord ? Le siège se déroule-t-il comme espéré ?

--- Aucun message depuis le départ de la Bocquée. Je prie chaque jour que Dieu leur prête victoire.

--- Nous demanderons au père de célébrer une messe pour appeler sur eux la miséricorde du Christ. »

### Béthanie, couvent des Saint-Lazare-Marthe-et-Marie, matin du jeudi 11 mai 1161

La basse-cour du monastère grouillait de valets et de serviteurs, de palefreniers et de gardes. Un baron en route pour la guerre n'aurait pas provoqué plus d'agitation. Au-dessus de cette effervescence, dans le couvent, Iveta donnait ses instructions à la prieure Eudeline qui la remplacerait une nouvelle fois. Pendant ce temps, Sybille supervisait les préparatifs personnels de l'abbesse. Son confesseur avait été prévenu et les bagages s'apprêtaient en concertation avec la drapière. Il fallait également organiser le secrétariat, qui devrait suivre et assurer la correspondance. Nul ne savait combien de temps Iveta s'absenterait.

Lorsqu'Eudeline sortit de la cellule, Iveta s'accorda un peu de relâchement dans sa posture et s'affaissa sur le lit. Elle avait veillé une partie de la nuit et n'avait pas encore pris de repas depuis que le messager était arrivé la veille au soir. Seule Sybille pouvait alors la voir ainsi dans son intimité, sans fard.

« Ne veux-tu pas un bouillon ou un léger gruau, ma mère ?

--- Rien qu'à l'idée d'avaler la moindre bouchée, mon estomac se révolte. J'ai trop grande hâte de parvenir à Naplouse.

--- Le voyage sera éprouvant, il serait plus raisonnable de l'affronter le ventre plein. »

Iveta accorda un sourire triste à sa nièce.

« Ma chère, ma très chère amie. Dieu t'a envoyée à moi pour m'éviter le désespoir... Fais ainsi que tu le désires, je ne suis qu'effroi et appréhension. Tant que je n'aurais pas la main de Mélisende en la mienne, je serai telle une de ces Vierges folles...

--- C'est là bien le moindre. Mon angoisse à moi est partagée entre toi et la reine. »

Iveta se redressa, caressant de ses doigts décharnés la joue de Sybille.

« Ne crains rien pour moi. Ce n'est que mauvais moment à passer. Cela fait si longtemps que je l'appréhende.

--- Est-elle si mal ?

--- Le coursier a dit qu'elle s'est effondrée quand elle a appris que Baudoin avait évincé Constance de la régence de la princée [^137]. Elle était déjà si faible, ce maudit garnement n'en aura donc jamais assez de lui faire tort !

--- Ne succombe pas à la même colère que ta sœur !

--- Oh, je suis bien moins sanguine qu'elle, mais je le trouve bien mal aimant avec celle qui l'a fait tel qu'il est. »

Sybille ne répondit pas, se contentant de caresser doucement la main de son amie.

« Quoiqu'il en soit, Antioche n'est pas la question pour le moment. Constance a l'âme si fort trempée que sa mère. Je dois veiller avant tout sur mon aînée.

--- Nous serons prêts à partir avant sixte m'a assuré la cellérière. Elle a mandé quelques sergents depuis la cité, afin de nous faire escorte.

--- Le coursier m'a dit que des chevaucheurs avait aussi été envoyés à Triple prévenir Hodierne[^138]. Je crois que Baudoin y réside, après son séjour à Antioche et dans le nord. Si seulement il acceptait de se rendre au chevet de Mélisende...

--- N'escomptez en rien cela, ma mie. Il est trop empli de colère envers elle, nul homme ne vient ainsi à résipiscence.

--- Alors nous prierons aussi pour lui, car son âme est en bien grand péril de ne pas honorer une mère comme Mélisende. » ❧

### Notes

L'histoire médiévale s'attarde souvent sur les individus dominants d'une époque, et il est toujours malaisé de reconstruire le parcours, et encore moins la personnalité des plus modestes. Et parfois même de figures de premier plan, mais qui ont été occultés par les hasards des études et récits ultérieurs. On ne retient donc généralement des filles de Baudoin II que Mélisende, dont l'éclat ternit la visibilité de ses sœurs, d'Iveta en premier lieu. Celle-ci avait pourtant fait un choix de vie radical, pour n'être pas si rare au XIIe siècle.

Le fait de devenir religieuse lui permettait d'acquérir une autonomie et un pouvoir qui lui aurait été bien disputé si elle avait du se marier, l'histoire de ses sœurs le montre à l'envi. Par ses origines, elle a pu se hisser à un niveau d'influence certain, dont on peut voir la réalité par quelques signes, tel l'usage d'un sceau (elle est la seule femme en dehors de la reine à en posséder un à cette époque). En outre, elle dirigeait un établissement réputé parmi les pèlerins, dont le souvenir aujourd'hui ne rend pas compte de l'importance d'alors.

Par ailleurs, le lien avec Usamah ibn Munqidh est une pure invention de ma part, mais je me suis imaginé qu'Iveta ayant passé plusieurs mois comme otage à Shayzar alors qu'elle était très jeune, elle avait pu y apercevoir le prince. Et le retrouver des années plus tard, quand il représentait Unur de Damas à la cour de Jérusalem.

Le titre est une référence au texte de Christine de Pisan « Seulette suis » :

> Seulette suis et seulette veux être,\
> Seulette m'a mon doux ami laissée,\
> Seulette suis sans compagnon ni maître,\
> Seulette suis, dolente et courroucée,\
> Seulette suis en langueur malaisée,\
> Seulette suis plus que nulle égarée,\
> Seulette suis, sans ami demeurée.

> Seulette suis à huis ou à fenêtre,\
> Seulette suis en un angle mussée,\
> Seulette suis pour de pleurs me repaître,\
> Seulette suis, dolente ou apaisée,\
> Seulette suis, rien n'est qui tant me siée,\
> Seulette suis en ma chambre enserrée,\
> Seulette suis, sans ami demeurée.

> Seulette suis partout en tout est.\
> Seulette suis où j'aille et où je siée\
> Seulette suis plus qu'autre rien terrestre,\
> Seulette suis de chacun délaissée,\
> Seulette suis durement abaissée,\
> Seulette suis souvent toute éplorée,\
> Seulette suis, sans ami demeurée.

> Princes, or est ma douleur commencée :\
> Seulette suis de tout deuil menacée,\
> Seulette suis plus sombre que morée\
> Seulette suis, sans ami demeurée.

Texte extrait du site du Centre National pour la Poésie : https://www.printempsdespoetes.com/Seulette-de-Christine-de-Pisan

### Références

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Jordan, Erin L., « Hostage, Sister, Abbess: The Life of Iveta of Jerusalem », Medieval Prosopography, vol. 32, 2017, pp. 66‑86.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », Dumbarton Oaks Papers, 1972, vol. 26, p. 93‑182.

La raison des femmes
--------------------

### Jérusalem, quartier de la porte d'Hérode, après-midi du mercredi 7 avril 1148

Peu désireuse de se mélanger aux servants qui avaient accompagné son maître lors de son déplacement, Ghadir demeurait à l'entrée de son refuge, la cuisine, faisant mine de balayer le sol d'un vieil amas de brindilles. Ce faisant, elle ne manquait rien de ce qui déroulait dans la cour sans avoir à s'y mêler, accumulant de quoi nourrir sa verve à propos de l'incompétence des employés loués à la tâche, de l'inconséquence des jeunes et, de façon générale, de l'inaptitude fondamentale de tous les hommes. Assis à ses côtés dans une posture tout aussi attentive, Bankala se pourléchait du bon repas qu'elle venait de lui offrir. Elle avait dressé le chien à lui obéir au doigt et à l'œil, portant sur son dos des paniers lors de sa tournée des souks ou veillant d'un grognement à ce que nul ne l'aborde quand elle n'en avait pas envie.

Les valets occupés à décharger les balles dans l'entrepôt connaissaient l'irascible gouvernante et certains mollets gardaient un souvenir cuisant de leur rencontre avec Bankala. Ils prennaient donc soin de demeurer discrets tout en accomplissant leur tâche au plus vite. Ceux qui avaient pris part au voyage étaient de toute façon impatients de rentrer chez eux, maintenant que la nouvelle de leur retour était certainement parvenue jusqu'à leur demeure.

Le maître de maison, Tha'lab al-Fayyad était un négociant au long cours. Bien que vieillissant, il tenait à faire au moins un déplacement chaque année, mais il s'aventurait de moins en moins loin, choisissant ses escales et ses destinations parmi son cercle d'intimes où il savait pouvoir loger confortablement. Il était donc resté depuis le début d'année sur la côte, goûtant la clémence des cieux riverains de la Méditerranée.

Comme il était veuf depuis de nombreuses années, Ghadir avait géré la demeure en son absence, n'ayant que la charge du jeune Abdul Yasu. Celui-ci profitait du départ de son père pour vivre de façon encore plus licencieuse qu'à l'accoutumée, malgré les récriminations de la vieille femme. Il savait néanmoins qu'elle ne rapportait rien de ses agissements et qu'il fallait lui arracher la moindre confidence sur ce qui se passait dans la maison ; pour être acariâtre, elle n'en était pas pour autant commère. Elle réservait la primeur de ses épanchements au molosse qui ne la quittait jamais, préférant visiblement la compagnie d'un canidé muet et obéissant à celle de ses contemporains.

Déterminée à s'installer à l'abri, mais face à la porte, soucieuse de ne pas interrompre sa surveillance lorsqu'elle entamerait la préparation du repas, elle se retirait dans sa cuisine quand elle entendit un appel du vieux Tha'lab résonner dans les couloirs. Soupirant, elle abandonna sa vigie et s'en fut d'un pas trainant, ajustant ses vêtements afin de paraître décente à son employeur. Pour le côtoyer depuis plusieurs dizaines d'années, elle ne s'accordait certainement pas la licence de se présenter sans avoir conformé son apparence à ses propres critères de respectabilité : un voile impeccablement fixé et une mine contrite. Lorsqu'elle pénétra dans le majlis où il se tenait, elle le découvrit les sourcils froncés, mains sur les hanches.

« Qu'est-ce donc ? Que s'est-il passé ? Le khamaseen[^139] a-t-il soufflé en mon absence ? »

Ghadir baissa les yeux révérencieusement une fraction de seconde puis dévisagea son maître. Elle avait déplacé tous les meubles, suffah et tapis dans la pièce, tâche qu'elle repoussait depuis des années bien qu'elle l'estimât essentielle. Elle y avait consacré le temps nécessaire pour faire les choses de façon idéale.

« Rien ici n'était organisé comme il seyait, abu... Sa voix se fit moins forte, presque hésitante. J'ai entendu bien méchantes remarques des domestiques voisins, qui répétaient ce qui se disait. Ce majlis ne te faisait pas honneur, j'en ai pris la mesure, car c'est là mon devoir de te servir correctement. »

Tha'lad al-Fayyad se rengorgea, faisant trembler son goitre tandis qu'il regardait les lieux d'un œil neuf. Il se frotta le visage un instant, se gratta le nez.

« Tu as agi avec sagesse, mais tu n'as pas à décider ainsi sans mon consentement. C'est ma demeure et nul ne doit savoir que je n'ai pas voulu l'arrangement de la pièce où je reçois.

--- Tu as déjà tant à faire, abu. J'ai désiré t'épargner cette tâche qui n'est pas de ton rang.

--- Ni du tien ! C'était là celui de ma pauvre épouse, que Dieu la bénisse. »

Ghadir adopta une posture contrite, faisant le dos rond avec le naturel né d'une pratique ancienne.

« Souhaites-tu que je remette tout comme avant ?

--- Non, non. Je n'aime pas qu'on bouge les choses autour de moi. Nous allons voir ce que ça donne et nous arrangerons ce qui me déplaît. Mais plus de telles initiatives, compris ? »

Puis il la chassa d'un geste de la main, la renvoyant à ses occupations domestiques tandis qu'il s'installait sur les coussins, certainement pour une courte sieste comme il les affectionnait en fin de journée, avant le souper.

À peine de retour dans son univers, Ghadir alla vérifier où en étaient les valets et maugréa de constater qu'ils avaient fini de tout décharger. Elle ne pourrait donc pas diriger sa vindicte vers eux. Elle se contenta d'une caresse au chien et alla chercher dans la réserve de quoi préparer le repas du soir. Elle avait prévu originellement de faire des boulettes aux légumes, mais Tha'lab appréciait l'agneau par-dessus tout. Elle pourrait peut-être en emprunter un morceau à une des voisines. Et elle avait le temps de faire quelques pâtisseries à la pistache et au miel si elle se dépêchait. Cela réconforterait Tha'lab du voyage.

Tout en s'activant pour concevoir un repas attrayant à son maître, elle bougonnait. Malgré ses récriminations, le vieux ne doutait pas qu'elle ait pris la bonne décision, comme toujours. Les hommes étaient incapables de prendre soin d'un foyer ou d'en faire un lieu accueillant, tout le monde savait ça. Ils s'attachaient à tout critiquer car ils avaient besoin de manifester leur autorité. Mais nul n'ignorait que la maison, dans cette famille, c'était le domaine réservé de Ghadir.

### Encharim, casal d'al-Najjar, matin du jeudi 26 décembre 1157

Confortablement confinée dans la grande salle, la famille du vieil al-Ajjar profitait des fêtes pour se rassembler. Cousins lointains, frères et sœurs distants, tout le monde avait fait le voyage pour retrouver une nouvelle fois le vieil homme, conteur émérite, dont la diction approximative et chuintante n'entamait en rien son pouvoir d'évocation. Pendant une semaine, le moindre recoin bruissait des jeux des plus jeunes et des conversations des adultes. Près du feu, les femmes s'activaient chaque jour à préparer des plats d'exception, puisant dans les réserves accumulées depuis des mois pour s'octroyer le plaisir de plantureux repas.

Ghadir aidait une de ses nièces à ajuster grossièrement les vêtements des enfants, se contentant de rapides ourlets pour raccourcir une robe ou une manche. Tout en tirant l'aiguille, elle faisait part de son insatisfaction vis-à-vis de ces jeunes qui ne savaient pas prendre soin de leurs affaires et qui auraient mérité d'aller nus par les rues, comme les animaux qu'ils étaient. Seuls les plus éloignés de la famille, ou les plus fiers des parents, étaient choqués de ses propos. Ce fut son frère, Jabir, qui l'apostropha un peu sèchement depuis le coin des hommes alors qu'elle morigénait un de ses petits-fils.

« Laisse donc Tahir. Tu as beau jeu de tancer ainsi garçonnet si peu en âge !

--- Gare, elle pourrait tout aussi bien m'en prendre à toi, ricana un de ses voisins.

--- Je réprimande comme il convient pour éduquer ces petits, n'ayant pas la chance d'avoir les miens à choyer. Je fais ma part pour aider ceux-ci.

--- En les grondant à propos de toute chose ? Tu as l'affection bien acide, ma sœur.

--- Je suis tel que la vie m'a faite. J'ai eu mon content de malheur. »

Elle poussa un soupir à faire vibrer les murs, évoquant par cette manifestation sonore son veuvage précoce et la perte de son enfant puis le désaveu de sa belle-famille qui avait suivis. Aucun de ses proches n'ignorait son infortune, pas plus que quiconque dans son voisinage. Mais Jabir subissait les diatribes de Ghadir depuis trop longtemps pour s'en laisser attendrir. Il estimait qu'elle s'était toujours comportée ainsi, revêche et acrimonieuse. Et il ne voulait pas rater une occasion de montrer à tous ici qu'il était son aîné en plus d'être un homme.

« N'est-ce pas toi qui a tout fait pour aller minauder en la cité ? À ne rien réclamer que t'installer à Jérusalem ? Père a beaucoup renâclé à te laisser épouser un melkite[^140]. Ces gens-là n'ont pas d'honneur. Maintenant tu moissonnes ce que tu as semé, ivraie incluse. »

Ghadir baissa le front. Elle ne voulait pas faire d'esclandre, préférant régler ses comptes à sa façon. Jabir était fanfaron imbu de lui-même, elle n'obtiendrait que sa colère, voire ses coups, si elle le provoquait ici. Mais il n'y aurait pas toujours du public et il aurait besoin d'elle un jour. Il serait alors bien temps de lui faire souvenir de son comportement. Lui qui avait hérité de tout, sans rien faire, et qui n'avait eu qu'à reprendre le florissant atelier de charpente familial.

Une de ses jeunes nièces, qui tirait le fil à ses côtés lui adressa un sourire encourageant et y ajouta à mi-voix :

« Tu sais, ma tante, il y aura toujours une place pour toi en notre demeure, quoi qu'en dise Jabir. Il ne laisserait jamais sa propre sœur dans la rue si tu venais frapper à sa porte. »

Ghadir lui retourna un regard épouvanté.

« Revenir parmi poules et mulets ? Mieux vaut encore ces paons de Celtes[^141] de la ville ! »

### Jérusalem, maison de bains, matin du mardi 21 avril 1159

Le mardi était réservé aux femmes dans le hammam que fréquentait Ghadir. Elle ne s'y rendait pas toutes les semaines, mais aimait s'accorder ce moment de plaisir chaque fois qu'elle s'autorisait un peu de relâchement dans son emploi du temps. Elle en profitait chaque fois pour s'offrir quelques pâtisseries sur le retour, l'égoïsme renforçant la volupté des confiseries. Elle avait en effet un peu de trajet à faire, préférant fréquenter l'établissement qu'elle avait connu lors de son mariage à celui, moins éloigné, qui était rattaché à son quartier.

Elle avait sa place depuis des années, installée sur un muret derrière une petite colonne, un peu en retrait. Elle n'aimait pas se trouver à la vue d'autrui et choisissait la discrétion des abords et des recoins. De là elle pouvait s'employer à regarder les autres, à les juger en silence, activité qui lui convenait fort bien. Elle profitait de temps à autre de la présence d'une oreille pas trop loin d'elle pour s'épancher à propos de ce qui nourrissait sa rancœur de l'instant. Mais les habituées étaient méfiantes et seules les néophytes s'approchaient généralement.

Elle avait cette fois-là découvert une compagne de malheur de son âge, qui compatissait d'autant mieux qu'elle n'écoutait guère, exploitant juste les moments où elle pouvait insérer ses propres griefs dans la logorrhée de Ghadir. Elles se rejoignaient néanmoins sur leur commune détestation des jeunes femmes, qui ajoutaient à l'indécence un irrespect total envers leurs personnes.

« Cela fait des années que je sers bien mon maître, sans jamais rien demander pour moi. Ni me plaindre de mon sort, insistait Ghadir. Il avait même dit qu'il ferait de moi sa concubine, si ce n'est son épouse. Mais les années filent et je ne vois rien changer. »

Elle se rapprocha de sa confidente, vérifiant qu'aucune oreille juvénile ennemie ne trainait aux alentours.

« Et voilà qu'il y a la fille de son petit dernier qui se prend pour la maîtresse de la maison. Elle ordonne, organise, demande les clefs et inspecte les réserves ! Comme si cette demeure était sienne, et pas celle de Tha'lab. Aucun respect pour ses aînés !

--- Et il ne sévit pas ? Il tolère de se laisser mener ainsi ? s'horrifia l'autre. Moi, mon Ahmed n'aurait jamais accepté ça ! D'ailleurs...

--- Il a trop bon cœur, voilà son problème. Le fils en profite et elle tient celui-ci par ses minauderies. Mais cela ne prend pas avec moi ! Oh ça non... J'aurai tôt fait de la mettre au pas ! »

Elle remâcha sa colère un instant, qui fut suffisant pour sa voisine embraye sur ses propres soucis.

« C'est toujours pareil. Il s'en faut d'un rien pour que les hommes fassent n'importe quoi ! Mon pauvre Ahmed, lui, était bien sage comparé à tous ces fous. Jamais il n'aurait traité sa mère comme mes fils le font ! J'aurais su combien ils se montreraient peu reconnaissants, j'en aurais bien moins fait pour eux.

--- Et encore, toi tu as la chance de pouvoir réformer tes garçons. Moi je n'ai plus rien, Dieu m'a tout pris, époux et enfant. Je n'ai que mon pauvre Bankala !

--- Ils nous usent jusqu'à la moelle, puis nous oublient. Des ingrats, voilà ce qu'ils sont ! Si au moins j'avais eu une fille ! Je l'aurais gardée pour mes vieux jours. Elle m'aiderait bien, en ces temps difficiles.

--- Les jeunes, elles n'ont rien dans la tête. Pas comme nous à leur âge. Moi j'ai toujours été bonne enfant : jamais ma mère n'a eu à se plaindre. Elle n'avait que prières à la bouche quand elle parlait de moi. »

Poursuivant leurs monologues en duo, les deux vieilles continuèrent un moment leur conciliabule discret, recroquevillées dans la noirceur de leur alcôve, noyées dans les chaudes brumes du hammam. Puis elles se saluèrent courtoisement et se séparèrent, un peu soulagées quoiqu'insatisfaite l'une et l'autre de voir que nulle ne prêtait attention à leurs paroles emplies de sagesse.

### Jérusalem, quartier de la porte d'Hérode, midi du jeudi 6 octobre 1160

Abdul Yasu rentrait d'une course matinale lorsqu'il entendit du bruit dans sa chambre, où dormait généralement sa fille. Il reconnut une berceuse fredonnée, qui lui rappelait des souvenirs. Le sourire sur les lèvres, il s'apprêta à surprendre sa femme, certainement occupée à calmer ou nourrir leur petit enfant. Il marqua donc un temps d'arrêt quand il découvrit le dos tordu de Ghadir qui balançait délicatement Maryam en chantonnant un air qui lui était familier. Étonné de voir la vieille gouvernante exprimer autant de douceur, il s'appuya, attendri, contre le chambranle de la porte pour admirer la scène. Ghadir mignotait le nourrisson avec une telle prévenance qu'il se demandait si un djinn n'avait pas lancé un sort sur elle. Ill aurait observé un lion lécher un agneau il n'aurait pas été moins surpris.

Tournant sur elle-même, Ghadir finit par se présenter de côté et découvrit sa présence. Il s'en fallut de peu qu'elle ne sursautât, arrêtant immédiatement sa chanson tandis que ses commissures de lèvres retombaient avec lourdeur. Elle le foudroya du regard, mais sans pour autant lâcher l'enfant.

« Ta femme néglige ses devoirs, j'ai entendu la petite qui pleurait. Je pense qu'elle a faim.

--- Est-ce pour cela que tu lui fredonnais une berceuse ? Pour la rassasier ?

--- Je faisais ce qu'il y avait à faire, vu qu'en dehors de moi, personne ne se soucie des autres dans cette maison !

--- Allons donc ! J'ai bien reconnu. N'était-ce pas ce que tu me chantais, enfant ?

--- Je ne sais pas, je n'ai jamais été douée pour les berceuses. C'est la première chose qui m'est venue en tête ! »

Abdul Yasu ricana. Il était un des rares à se montrer imperméable à sa mauvaise humeur, ce qui énervait d'autant plus Ghadir. Il ne pouvait pourtant retenir un sourire narquois qui en accroissait chaque fois la violence. Elle lui posa le bébé dans les bras sans ménagement.

« Tiens, plutôt que de moquer une vieille femme aux membres fatigués, va donc le donner à ton épouse.

--- Où est-elle ?

--- Aucune idée. Sûrement à fouiner quelque part... »

Un peu encombré par l'enfant, Abdul Yasu ne se poussa pas de la porte, fixant Ghadir avec désinvolture.

« Tout de même, cela faisait bien des années que je ne t'avais pas entendue chanter. Cette enfant te procure du bonheur aussi, avoue-le donc.

--- Surtout du travail en plus, maugréa-t-elle.

--- N'y a-t-il rien qui te redonne le sourire ?

--- C'est comme ça depuis que je suis née. Déjà lors, tout le monde était en joie autour de moi tandis que j'étais dans les pleurs ! » ❧

### Notes

Ghadir représente un des figures les plus complexes que j'avais à brosser, et cela explique pourquoi on ne l'avait aperçue qu'en ombre jusqu'à présent. Pour l'esquisser, j'ai fait un mélange de *Vieux Fusil*, le personnage campé par Biyouna dans *La source des femmes* assaisonné de *Tatie Danièle* interprétée par Tsilla Chelton dans le film éponyme. Cela me semblait correspondre à tous les topos que j'avais pu retrouver dans la littérature autour des femmes un peu âgées dans le moyen-orient d'alors.

Si on leur concède volontiers une aisance à l'oral, elles n'ont que peu d'espace pour la déployer et doivent se restreindre dans leur liberté de parole qui doit subir l'autorité masculine, du moins de façon formelle. L'autre aspect intéressant du personnage dépeint par les sources est le fait que les femmes âgées sont représentées comme une malédiction pour leur entourage. Surtout si elles n'ont pas accompli leur devoir de procréer des fils... Il y avait là je trouvais un cocktail passionnant pour tracer un caractère riche et complexe qui, pour n'être pas forcément sympathique d'abord, pouvait donner plus à voir que ces rapides traits, tant décriés sous la plume des auteurs classiques et des correspondances privées.

Le titre vient d'un dicton arabe, dont le propos sexiste m'avait suffisamment frappé pour que naisse en moi l'envie de le faire mentir : *la raison des femmes, c'est la folie ; leur religion, c'est l'amour*. C'est également un renvoi à l'ouvrage de même nom de la philosophe Geneviève Fraisse.

### Références

Benkheira Mohammed Hocine, « Hammam, nudité et ordre moral dans l'islam médiéval (I) », *Revue de l'histoire des religions*, 2007, vol. 224, num. 3, p. 319‑371.

Benkheira Mohammed Hocine, « Hammam, nudité et ordre moral dans l'islam médiéval (II) », *Revue de l'histoire des religions*, 2008, vol. 225, num. 1, p. 75‑128.

Cortese Delia, Calderini Simonetta, *Women and the Fatimids in the World of Islam*, Edinburgh, Edinburgh University Press, 2006, vol. 1.

Fraisse Geneviève, *La raison des femmes*, Paris : Plon, 1992.

Goitein Shelomo Dov, *A Mediterranean society; the Jewish communities of the Arab world as portrayed in the documents of the Cairo Geniza*, Berkeley, University of California Press, 1967-1993, vol. Volume 1 à 6.

Marin Manuela, Deguilhem Randi (dir.), *Writing the feminine. Women in arab sources*, Londres - New York, I.B. Tauris, 2002, vol. 1.

Myrne Pernilla, Narrative, *Gender and Authority in 'Abbāsid Literature on Women*, Västerås, Edita Vastra Aros AB, 2010, vol. 22.

Tillier Mathieu, « Women before the qāḍī under the Abbasids », *Islamic Law and Society*, 01/01/2009, vol. 16, num. 3‑4, p. 280‑301.

Spectorsky Susan A., *Women in Classical Islamic Law. A survey of the Sources*, Leiden - Boston, Brill, 2010, vol. 5.

Histoire d'un aller et retour
-----------------------------

### Saint-Vaast, parvis de l'église paroissiale, fin de matinée du dimanche 1er décembre 1146

Les averses matinales avaient cédé la place à un ciel bas, qui se reflétait en un gris acier dans les flaques parsemées sur le parvis bordé de frênes. Autour de la porte menant au jardin du presbytère, un petit tonneau avait été mis en perce par le sacristain, comme chaque semaine quand le temps le permettait. Le moment après la messe était toujours l'occasion pour le père Ligier d'offrir à boire de sa cave à ses paroissiens. Ce moment convivial, qu'il avait rapidement instauré après sa nomination, prolongeait le sermon en des sujets plus séculiers, plus immédiats et parfois plus matériels.

Les conversations allaient bon train, avec les soucis d'épizootie qui frappait certains troupeaux de moutons, et la rumeur d'une importante meute de loups aperçue dans les bois au nord du hameau. De nombreux villageois envisageaient d'envoyer une délégation au châtelain afin de demander à ce que des battues soient organisées. Les langues s'activaient aussi autour des commérages qui décrivaient le pèlerinage de grands barons pour aller délivrer la Terre sainte. Le père Ligier, qui avait pu échanger sur le sujet avec ses supérieurs, les chanoines de Saint-Lucien de Bury, était tout feu tout flammes à l'évocation de ce projet.

« Songez, mes amis, que le roi de France et le comte de Flandre eux-mêmes ont annoncé leur désir de prendre la croix ! Comme nos aïeux l'ont fait, ils vont délivrer le tombeau du Christ !

--- En est-on là ? Que font donc les barons outremer ? Ne sont-ils pas puissants et bien armés pour garder ce que nos pères ont conquis pour eux ?

--- Il se dit que les Turks sont véritables démons, aussi nombreux que criquets et sauvages comme des leus !

--- Ils brûlent et pillent chacun des lieux bénis du Christ, y répandant sel et vitriol pour en effacer toute trace, que le Matthieu m'a narré !

--- Il tient ça de qui, le Matthieu ? J'encrois pas qu'il ait jamais porté sa hotte plus loin que Beauvais ! moqua un des plus jeunes.

--- Justement, à la grande foire de la Saint-Martin, il a encontré pérégrin qui revenait tout droit de la sainte ville : un marchand de toile de ses amis, prud'homme et juré de son métier. »

Le père Ligier hocha de sa grosse tête ornée de sa longue barbe. Lui aussi avait entendu les pires récits sur ce qui se passait au loin. Lors d'une visite de l'évêque chez les chanoines, on avait évoqué le destin des prélats orientaux, qui envoyaient lettre après lettre en appelant à l'aide pour reprendre les territoires perdus les uns après les autres. La situation paraissait catastrophique et que de puissants souverains d'ici se décident à fourbir leurs armes était un signe de la miséricorde divine : il demeurait encore bons chrétiens pour défendre les lieux saints face aux envahisseurs païens.

« Gardons espoir, mes enfants. Saint Vaast lui-même a dû faire face aux hordes de Huns. Que sont ces Turks par rapport aux démons anciens ?

--- Vous avez dit qu'ils ont quand même pris le tombeau de l'apôtre Jean. C'est pas rien, ça !

--- C'est au juste pour pas qu'il reste en leurs mains que les osts s'assemblent. Que pourront leurs lames dérisoires face aux lances et à la bravoure des guerriers d'ici ? On parle d'une troupe forte de plusieurs milliers de combattants.

--- Nombreux sont les hommes de Dieu qui ont pris leur houlette pour porter la bonne parole et affermir les cœurs. La prière armera leurs bras aussi sûrement que leurs hasts. »

Un des anciens, à demi aveugle, frappa le sol de sa canne, postillonnant de ses mâchoires édentées tandis que la colère montait en lui.

« Mon fils m'a dit qu'un saint moine à l'est invitait à se purifier, que tout ça était dû au relâchement des mœurs au parmi des jeunes. On accepte trop la corruption chez nous. Commençons donc par nettoyer les mauvaises humeurs qui salissent nos diocèses ! »

Ligier avait entendu parler des événements de Mayence et des alentours. La population, soulevée par les propos d'un prêtre vagant, s'en était pris aux communautés juives, comme souvent lorsque la ferveur religieuse s'emparait d'eux. L'intervention des ecclésiastiques avait permis d'éviter un embrasement général, mais de nombreuses exactions, allant jusqu'au meurtre, étaient à déplorer. Pour n'être jamais confronté aux juifs et ne rien connaître d'eux, Ligier savait néanmoins qu'ils étaient sous la protection de l'Église. L'évêque l'avait rappelé en des termes clairs : il ne tolérerait aucune entorse à sa juridiction.

« Ne prêtez pas l'oreille à si méchantes rumeurs. Ce soi-disant clerc était mécréant et le saint père abbé de Citeaux l'a bien montré : nul ne sert de se purger de nos fautes au détriment des protégés de l'Église. Gardons nos forces pour soutenir les valeureux qui prendront la croix. C'est là bien plus vitale nécessité. »

Le vieux haussa les épaules, contrarié de se voir désavoué en public par l'autorité religieuse du hameau. Agacé de ce revers, il décida de s'octroyer un verre de plus en compensation et s'éloigna d'un pas trainant vers le fût.

« Pensez-vous qu'on va devoir payer la taille pour la Noël ? s'inquiéta l'un des jurés. C'est qu'avec les soucis en ce moment, il n'y a guère de monnaies en nos besaces.

--- Les pères chanoines ne m'ont rien mentionné à ce sujet. En avez-vous parlé au châtelain ou à son intendant ? Ils seront les premiers à le dire si cela doit être.

--- On n'a guère échangé depuis le paiement du cens, à la Toussaint. »

Depuis qu'il était jeune, Ligier avait assisté chaque année à la remise en cause et aux tractations sans fin autour des redevances. En dehors du cens, rarement discuté sauf lorsqu'on cherchait à l'augmenter, et la dîme, sur laquelle on ergotait plus dans son calcul que sur son bien-fondé, tous les prélèvements étaient l'objet d'âpres marchandages. Il était bien heureux de n'avoir pas à se confronter à de tels marchandages, comme son frère aîné dans la petite seigneurie familiale. En tant que vicaire de la paroisse, il n'était même pas le destinataire de la dîme et se contentait d'une prébende forfaitaire. Au moins ne pouvait-on pas le suspecter au sein du village de tenter de déposséder quiconque, ce qui lui facilitait les choses pour leur faire entendre raison.

« Le sire évêque Odon n'a rien déclaré pour ce qui ressort de ses châtellenies, c'est tout ce que je sais. J'ose encroire qu'annonce sera faite au préalable si cela devait être le cas à l'avenir. »

Autour de lui, les mentons acquiescèrent silencieusement. Chacun être au courant que, aussi sournoisement que les païens à l'assaut des lieux saints, les taxes s'annonçaient rarement longtemps à l'avance.

### Saint-Vaast, manse de Rémy de la Poterrie, début de soirée du lundi 16 juin 1152

La célébration du mariage du fils cadet du vieux Rémy avait rassemblé la moitié du village. Homme affable et débonnaire, le négociant en vases et céramiques avait prospéré au fil des ans et s'était toujours montré amical et accueillant. Chacun avait porté un plat ou une boisson, avait participé aux décorations, et on profitait donc d'une joyeuse occasion, un des derniers moments de fête avant le début des longs et harassants travaux de l'été. Malgré un soleil paresseux, la pluie n'était pas venue, et les danseurs et danseuses farandolaient depuis des heures, au milieu des cris des enfants et des discussions des aînés.

Le père Ligier que l'embonpoint, plus sûrement que la retenue ecclésiastique, empêchait de danser avait trouvé refuge entre une table où rissoles et pâtés étaient proposés à tous à côté des tonneaux de vin mis en perce. Il s'assurait ainsi la compagnie de nombreux autres villageois qui préféraient les délices du palais à celui des pieds et de la musique. Les langues s'activaient autant que les dents.

Se tenait parmi les présents Gobert, un carrier prospère, qui avait récemment perdu un bras dans un accident. C'était un homme taciturne et peu enclin aux plaisirs de la chère ou de la boisson dont Ligier sentait bien qu'il avait le désir de se confier. Il lui laissa le temps, sachant qu'il ne servait à rien de brusquer cet homme austère. Ce fut en lui versant un gobelet de vin que la glace fut finalement rompue.

« Dites-moi, père, comment ça se passe pour prendre l'habit ? »

La question sembla un peu incongrue à Ligier, qui prit le loisir d'avaler une gorgée avant de répondre.

« Tu parles de la cérémonie ou d'autre chose ?

--- Comment se fait-on moine, en pratique ? Il suffit d'aller frapper à la porte d'un couvent ?

--- Oh non, c'est bien importante décision, et il s'en faut d'un long temps entre le désir et la profession. Pourquoi cette question ? Aurais-tu souhait pour un de tes fils ? »

Gobert secoua la tête en dénégation.

« J'ai beaucoup réfléchi et prié alors que je gisais, après l'accident. Je me demandais si ce n'était pas là un signe de Dieu d'abandonner mes tâches et me vouer à plus saint office.

--- Ne doute pas que Dieu est caché derrière toute chose. Mais pourquoi penses-tu qu'il veut que tu te fasses tonsurer ?

--- Je ne sais, à dire le vrai. J'ai pourpensé ma vie et je suis peut-être trop revêche, excessivement dur avec mes œuvriers. J'en ai pas tant perdu, mais plusieurs sont tout de même morts depuis que j'ai la carrière en charge. Dieu m'est témoin que je ne faisais pas cela pour m'enrichir. J'ai toujours eu désir de bien faire, en chaque chose, exigeant souventes fois un peu plus que la suffisance. Était-ce là vanité, mon père ? »

Tout en parlant, il jetait de rapides coups d'œil en direction de Pons de Mello, un des anciens du village, cadet d'une longue fratrie qui avait été brisé par des années de labeur en tant que simple journalier. Conteur infatigable, il était aussi le confident silencieux et attentif de bien des âmes, qu'il savait guider adroitement vers le curé lorsque c'était nécessaire. Lui aurait fait un moine tout à fait honorable, estima Ligier.

Gobert était un des piliers de la communauté. Veuf depuis des années, il s'était consacré à son travail, d'extraction de pierres, avec une vigueur et une discipline qui n'avait rien à envier à la vie régulée la plus stricte. Il était aussi un organisateur de talent pour les corvées, habile à remblayer un chemin, drainer un marécage, fortifier un gué ou tracer une route. Ce serait une perte certaine pour le village que toutes ces compétences s'évanouissent derrière les murs d'un couvent.

« Se faire moine alors qu'on est ni enfançon ni vieillard, que voilà bien hasardeux choix, mon fils. Pourquoi ne pas te faire pérégrin plutôt ? Tu pourrais soutenir de tes deniers quelques indigents et vous faire compagnons pour un temps de foi et de prière. Et à ton retour, les tiens te retrouveraient, l'âme débarrassée de ces tourments, prêt à aider ainsi que tu l'as toujours fait par tes suggestions et conseils, forts avisés. »

Gobert plissa les yeux, fronçant les sourcils tandis qu'il analysait la proposition. Il demeura un certain temps ainsi figé, insensible à la musique et au brouhaha ambiant. Tout en sirotant son vin, Ligier pouvait presque entendre les rouages de sa réflexion qui s'activaient.

« N'avez-vous pas dit tantôt que plus méritant pérégrinage était aussi le plus périlleux ?

--- De certes. La mortification du corps aide à la résipiscence, mon fils. Tu n'as pas besoin de prier pour guérison ou secours. Rome serait bonne destination...

--- Je pensais plutôt à la Sainte Cité. À Jérusalem. »

Ligier s'accorda une moue stupéfaite. Il avait déjà assisté à des bénédictions dans la cathédrale de Beauvais, mais jamais un de ses paroissiens n'avait envisagé si ambitieux voyage. C'était aussi un signe de profonde religiosité auquel il ne pouvait demeurer insensible.

« Eh bien, eh bien, comme tu y vas, mon fils. C'est là bien formidable vœu, bien merveilleuse destination ! Prier sur les pas du Christ ! Sans doute aucun, tu en reviendrais transformé.

--- Je pourrais laisser l'affaire au gamin, il est grand bien assez. Et j'ai de quoi payer pour quelques âmes qui souhaiteraient me compaigner. »

En disant cela, il regardait Pons, dont le sourire ravi dévoilait les gencives dégarnies. Le vieil homme avait les yeux larmoyants lorsqu'il plongea les doigts dans son col pour en sortir un anneau qu'il portait en sautoir.

« Je n'ai pu me résoudre à confier à la terre la bague de ma Beneoite. Le tremper dans le Jourdain serait mon souhait le plus cher... »

Gobert posa une main amicale sur le bras de Pons, confirmant d'un sourire discret la silencieuse promesse. Devant tant de sollicitude, le père Ligier perdit sa faconde habituelle. Il ne réfléchit guère avant de s'exprimer, emporté par son naturel débonnaire.

« Il faut raison garder, mes fils, mais j'en parlerai à l'évêque Henri. Quoiqu'avant cela avec les chanoines ! Vous aurez besoin de pasteur pour cheminer d'un bon pas tout ce temps. »

### Bury, collégiale Saint-Lucien, veillée du mardi 16 novembre 1154

Le froid humide consécutif à de fréquentes journées brumeuses et pluvieuses avait incité les chanoines à se regrouper autour d'un imposant brasero de fer. Douillettement installés dans des couvertures, ils abordaient comme chaque soir différents points organisationnels ou liturgiques. Un des sujets les plus complexes qui les occupait depuis de nombreux mois était le départ prochain de leur vicaire de Saint-Vaast, le père Ligier. Il fallait lui trouver un remplaçant le temps de son absence, ce qui n'était pas une mince affaire. La prébende était correcte, mais sans excès, et ne pouvait attirer les meilleurs clercs. Les chanoines ne tenaient pas à rompre une tradition ancienne en relevant les montants, sans pour autant se résoudre à confier les âmes du hameau à n'importe quel prêtre mal dégrossi.

Malgré la surcharge de travail que cela entraînait, ils étaient plutôt satisfaits du pèlerinage en préparation. C'était un excellent signe envoyé aux autres paroisses et un indicateur de la rigueur de leur prédication, que ne manquerait pas de remarquer l'évêque de Beauvais. Celui-ci, le propre frère du roi de France, qui avait lui-même pris la croix quelques années plus tôt, était un ecclésiastique assez austère, dont les exigences morales étaient parfois pesantes. Il n'avait d'ailleurs accepté les honneurs de la charge qu'avec réticence, ayant longtemps préféré demeurer simple moine.

« J'ai envoyé quelque lettres à des amis que j'ai encontré alors que j'étudiais à Tours, expliqua un des chanoines, tout en essuyant son nez d'un revers de main. Il se trouve toujours des écoliers de valeur en ces lieux.

--- Nous devrions peut-être insister sur le fait qu'il s'agit là d'une belle paroisse, dont l'église vient d'être rebâtie de bonnes pierres de taille, pas d'une petite chapelle retirée ? hasarda l'un d'eux, sans conviction.

--- Je n'aurais guère confiance en un prêtre sensible à si séculier argument, mon frère, le corrigea un des autres, la voix pincée.

--- Le souci nait de ce que les meilleurs d'entre tous préfèrent justement la règle au siècle. Il demeure pourtant que l'apostolat est de première importance. Le proclamer en lieux saints dignes de la célébration du Très-Haut me semble receler bel attrait pour le souligner. »

Les têtes hochèrent en cadence, doucement, tandis que chacun s'abîmait dans ses réflexions afin de découvrir une solution à leur problème. Ce fut la voix du plus âgé qui les sortit de leur marasme.

« Nous verrons bien si nos différents courriers portent fruit. Nous pourrons en reparler au père Pierre. Il doit se dénicher quelque novice en ses murs qui pourraient nous porter assistance à moindre mal. »

Il se racla la gorge en un long grincement avant de continuer.

« À propos de bel édifice, je voulais vous entretenir des travaux en cours ici. Je ne sais pas pour vous, mais j'ai grand mal à trouver le repos avec toute cette agitation... Il me serait amiable de m'accorder droit de coucher en ma petite maison à la sortie du hameau. Avec les froids hivernaux, je sens que je n'ai plus guère de vaillance, et mon cœur se serre à l'idée de pauvrement célébrer nos messes par cause de forte fatigue.

--- Oui, je suis d'accord. Ces travaux, de nécessité, sont pénibles à la longue, et je souscris aussi à cette motion pour que nous puissions déroger à la règle, de façon provisoire, pour affermir nos corps.

--- Hum. Nous devrions en référer au père abbé, ce me semble. C'est là contraire à nos devoirs.

--- Ce ne serait que contingent, la période des frimas. Le service de Dieu me semble de plus grande importance que de dormir ici dans l'inconfort. Cela n'apporte rien, et risque de grever notre tâche divine.

--- Sans compter que le temps d'envoyer missive et de recevoir son accord, dont je ne doute pas un instant, les beaux jours seront de retour et la situation aura changé. »

Un peu mal à l'aise, le chanoine opina. Lui aussi trouvait que les cellules de leur collégiale étaient bien austères, et ce sans que cela ne serve leur vocation. Il serait possible d'en améliorer le confort lorsque le printemps reviendrait. De façon à conserver leurs forces pour leur mission première, de célébration du mystère divin.

### Saint-Vaast, manse de Gobert le carrier, veillée du samedi 26 mars 1160

Comme cela devenait traditionnel depuis le retour des marcheurs de Dieu, les Vigiles de Pâques étaient prolongées par une veillée chez Gobert. C'était l'occasion pour ceux qui étaient allé en Terre sainte de montrer les palmes ou leurs vêtements usagés, encore ornés de la croix, ou leur besace, rafistolée et distendue.

De son côté, le père Ligier avait ramené un petit crucifix, taillé dans une pierre du Mont de la Transfiguration. On lui avait expliqué qu'il transformerait n'importe quel vin en un remède contre le poison si on le trempait dedans. Il était à demi convaincu de l'affirmation, partagé entre l'espoir de voir un miracle opérer et la crainte qu'on lui ait menti. Il chérissait malgré tout l'objet, souvenir de moments inoubliables. Il avait senti que sa prédication était désormais plus vivante, plus incarnée. Il maîtrisait d'autant mieux les récits des évangélistes qu'il avait foulé les lieux où tout cela s'était déroulé. De plus, il éprouvait au fond de lui la certitude que ses paroissiens accueillaient intensément la vérité contenue dans ces écrits. Il avait dépassé le stade de la récitation d'un savoir froid, il était devenu lui-même un apôtre en mettant ses pas dans ceux du Christ. À travers chaque parabole, il leur parlait de sa vie, de son expérience personnelle. Et ces mots empreints de lui touchaient d'autant plus juste, plus fort.

De son côté, Gobert s'était un peu déridé. Il avait gagné en souplesse d'esprit ce que son corps avait perdu en vigueur. Désormais moins taiseux, il ne se faisait généralement pas prier pour faire part de ses récits, mais ne les imposait jamais. Il semblait avoir cultivé une vie intérieure plus harmonieuse et moins inquiète. Ayant retrouvé la carrière dirigée de main de maître par son fils, il s'était mis en retrait, se contentant de superviser les comptes ou les acheminements sans plus trop se rendre dans les zones d'extractions. Il avait de nouveau évoqué avec le père Ligier le projet de quitter le siècle. Mais sa demande était moins angoissée, et semblait naître du désir de prolonger le voyage intérieur qu'il avait entamé. Il faisait peu de doute qu'il rejoindrait une abbaye d'ici quelques mois, quelques années au plus.

Le vieux Pons, dont l'esprit vagabondait de plus en plus, émerveillait les enfants par ses récits fantastiques. À l'entendre, chaque lieu regorgeait de miracles et de créatures plus incroyables les unes que les autres. Et il achevait ces veillées en brandissant l'anneau dont il disait qu'il avait touché toutes les places les plus saintes de la terre, avant d'être purifié de tout mal par un baptême à l'endroit même où le Christ s'était baigné.

Malgré les inventions dont il parsemait ses récits, le père Ligier n'avait pas le cœur de reprendre le vieil homme. Il avait cru longtemps qu'il était trop fragile pour affronter un tel trajet, et pensait qu'il demeurerait là-bas, une fois son vœu accompli. Mais Pons avait choisi de revenir, car il voulait montrer la bague à son épouse. Il avait laissé un peu de son esprit en chemin, mais n'en était devenu que plus attachant et plus simple dans les manifestations de sa foi. Le visage tendu vers un au-delà qu'il était seul à percevoir dans la pénombre des lampes à huile, il brandissait devant les enfants son anneau tel un prêtre l'hostie durant l'Eucharistie. ❧

### Notes

Les motivations des croyants médiévaux qui décidaient de tout abandonner pour prendre le chemin de lointaines destinations sans certitude de retour ou d'accomplissement de leur vœu nous demeureront à tout jamais inaccessibles. Alors que leur paysage mental ne dépassait guère leur paroisse, ou leur évêché, pour la plupart semblable parcours devait ébranler profondément leur rapport au monde. En outre, la découverte de la réalité matérielle de ce qui n'était jusque là que des récits vaguement compris ou connus, leur permettait de créer un lien plus immédiat, personnel, avec la complexe narration chrétienne.

En même temps qu'ils incarnaient plus pleinement leur foi au sein de leur communauté, ils se faisaient les propagateurs d'histoires qui n'avaient que peu à voir avec la religion telle que professée par les prédicateurs orthodoxes. Il me semble qu'ils représentent une parfaite illustration de cette ambivalence de la pratique catholique médiévale, nourrie d'une liturgie et de commandements très encadrés par des préceptes prêchés avec une sévérité de plus en plus stricte et d'un folklore narratif environnant basé sur un fantastique riche et contradictoire dans ses développements.

Pour l'anecdote, le titre de ce Qit'a est une référence directe au récit que Bilbo Sacquet fait de son voyage dans le roman de J.R.R Tolkien « Bilbo le Hobbit ». Toute présence d'un anneau dans mon Qit'a n'est certainement pas fortuite.

### Références

Graboïs Aryeh, *Le pèlerin occidental en Terre sainte au Moyen Âge*, De Boeck Univers, Paris & Bruxelles : 1998.

Tolkien John Ronald Reuel, Lauzon Daniel (trad.), *Le hobbit*, Paris : Christian Bourgeois, 2012.

Ward Benedicta, *Miracles and the medieval mind : Theory, record, and event, 1000-1215*, Revised edition, Philadelphia, University of Pennsylvania Press, 1987.

Curiales
--------

### Stavelot, abbaye, soir du lundi 20 décembre 1137

Les pas du frère qui menait Herman claquaient sur les dalles de pierre en rythme avec le balancement de sa lampe. Herman découvrait pour la première fois cette zone du couvent, où circulaient habituellement princes et magnats de ce monde, parmi lesquels le puissant abbé des lieux, Wibald. Ce dernier était récemment rentré de ses voyages et tenait à rencontrer personnellement tous les novices qui étaient arrivés en son absence. Herman n'était donc pas particulièrement inquiet, même s'il redoutait de ne pas faire bonne impression. Le prélat était un homme à la réputation de rigueur et de labeur. On disait son esprit nourri d'une érudition sans faille encore plus acéré que sa langue.

La cellule qui tenait lieu de chambre et de bureau surprit Herman par sa simplicité. Bien que les murs en fussent ornés de fresques aux scènes édifiantes, le mobilier en demeurait sobre, fonctionnel et sans ostentation. Assis à une vaste table de travail encombrée de missives, lettres, parchemins et tablettes, Wibald était perdu dans ses pensées quand ils pénétrèrent dans ses quartiers. Le visage extrait des ténèbres ambiantes par quelques lampes disséminées sur le plateau offrait une apparence bonhomme. La faéce ronde, presque lunaire aurait pu facilement engendrer un aspect poupin sans la rigueur de l'ecclésiastique en ce qui concernait les plaisirs de la chère. Sa tonsure était nette, tracée aussi strictement que sa barbe fine et ne laissait qu'une mince bande de cheveux. À son annulaire brillait une imposante bague abbatiale, écho de la lourde croix d'émaux qui pendait à son cou.

Bien qu'il n'ait pu ignorer leur entrée, il demeura un instant les doigts sur les tempes, perdu en une introspection née de la lecture des documents devant lui. Puis il leva les yeux vers les moines qui attendaient, immobiles. Sans un mot de plus, son secrétaire se retira, laissant Herman seul face à l'examen, toujours silencieux, du maître des lieux. Il finit par joindre les mains, dissimulant ses lèvres fines.

« Voilà donc notre jeune rossignol ! J'ai eu grand plaisir à t'entendre, mon fils. Ce n'est pas tous les jours que Dieu met tant de grâce en l'un de nous. »

Herman sourit en remerciement, attendant une invitation formelle avant d'oser prendre la parole.

« J'ai également appris que tu es passé par Saint-Pancrace[^142]. Sais-tu que l'admirable Hugues de Saint-Victor de Paris y débuta ses études ? »

Herman hésita à répondre, n'étant pas habitué à converser avec l'abbé. Mais, à l'attitude de son supérieur, il comprit bien vite que le grand homme appréciait fort les monologues, et attendit patiemment une fois de plus.

« Tu l'as sans nul doute appris, mais la mort récente du roi Lothaire risque de provoquer remous et troubles. Il nous faut tout faire pour que les provinces demeurent en une main ferme, mais point cruelle. Il en va des intérêts de l'Église ici, à Rome et au-delà. As-tu connoissance du duc de Franconie ? »

La question était cette fois directe et Herman répondit d'une voix qu'il espérait assurée.

« C'est le neveu de l'empereur Henri[^143] auquel il a souhaité succédé, mais ses attentes ont été déçues. Il s'est opposé un temps à Lothaire, pour ne se soumettre, avec son frère, qu'après la Diète de Bamberg.

« C'est là savoir du commun, je pensais que tu avais plus fortes attaches avec lui. On dit que ta famille est de celles qui n'hésitent pas à parler en bien des Souabes. »

Herman sentit son cœur s'affoler à ce qui était peut-être l'échec à un test. Mais Wibald ne lui laissa pas l'occasion de développer, il se pencha en avant, posant le menton sur ses mains croisées.

« Les temps qui s'annoncent vont être difficiles, mon fils. Malgré la campagne victorieuse contre les Siciliens, bien des menaces demeurent. Il faut à nos princes des fanaux pour les guider dans ces océans de ténèbres, des hommes issus des meilleures écoles, familiers des Pères, habiles à résoudre les problèmes et solides en leur Foi. Ce n'est plus un temps où la vaillance des lames peut suffire, quoi qu'en disent nos intrépides reitres. »

Il fit mine de prendre une des missives, laissant le silence s'installer, puis se recula dans son siège. Les lueurs dansantes creusaient son visage d'inquiétants traits d'ombre tandis qu'il réfléchissait.

« Tu vas quitter le service du chantre et entrer au mien. Tu recevras l'ordination sacerdotale au plus vite, car il ne sied pas que simple frère soit mon calligraphe. »

Herman se retint d'exprimer sa surprise. Il avait de prime abord attendu une ascension rapide, mais patientait depuis de si longs mois qu'il avait peu à peu perdu espoir. Il lui sembla fugacement enfin apercevoir la première des marches qui le conduiraient peut-être à finir évêque ou mieux encore. Il hocha la tête sans un mot, s'efforçant à la digne réserve bénédictine.

« Tu iras voir le frère drapier, pour te fournir de quoi voyager tantôt au parmi des frimas à mes côtés. Dès après la Noël, nous prendrons la route de Coblence. Il va nous falloir honorable successeur de Lothaire... »

### Freising, palais épiscopal, matinée du mercredi 12 mars 1147

Le temps humide avait incité plusieurs clercs de l'administration épiscopale à se retrouver dans le chauffoir pour y travailler plus confortablement. Dehors, une pluie intermittente noyait de gris ardoise l'Isar. Dans ce havre douillet, aux craquements du bois sec répondait le grattement des plumes sur le papier ou le parchemin. Tous étaient issus d'une stricte éducation monastique où le bavardage ne cohabitait que peu avec le labeur. Et il y avait fort à faire en ce printemps maussade. De nombreux courriers étaient échangés en cette période de préparation d'un long voyage outremer pour l'évêque Othon, en compagnie de son demi-frère le roi de Germanie Conrad.

Herman appréciait cette ambiance studieuse familière, savourant également le fait de n'avoir que peu à faire. Il profitait de son séjour pour parcourir les ouvrages de l'imposante bibliothèque d'Othon de Freising. Il dévorait pour le moment une copie du *De unitate et trinitate divina, sive Theologia Summi Boni* [^144], texte d'Abélard le sulfureux écolâtre parisien, récemment décédé, qui s'était durablement opposé au non moins célèbre abbé de Citeaux Bernard.

Comme celui-ci était actuellement en train de sillonner les territoires alémaniques pour prêcher la prise de la croix par le plus grand nombre, Herman savourait le délicieux paradoxe. S'il souscrivait pleinement à la vision de la Foi défendue par Bernard, le génie rhétorique et la puissance conceptuelle du savant parisien résonnaient fort parmi les jeunes générations de lettrés dont il était, formés dans ses traces, au moins intellectuelles, si ce n'était spirituelles. Le seul désaccord qu'il avait avec les théories de Bernard concernait les Juifs. Lui-même n'appréciait guère leur présence et les aurait volontiers forcés à la conversion ou au départ. Adepte d'une moindre violence, il n'approuvait pas les récents massacres, mais comprenait la colère qui avait animé leurs instigateurs.

Hermann avait été envoyé par son abbé auprès de l'évêque de Freising, car il intégrerait son administration le temps du voyage vers la Terre sainte. Wibald ne pouvait prendre la route, mais il avait tenu à adjoindre au roi de Germanie un clerc de confiance, dont il ne doutait pas des fréquents rapports à lui destinés. Il était particulièrement impatient d'en apprendre plus sur le basileus byzantin, dont il estimait qu'il s'agissait, avec le pape et l'empereur germanique, d'un des piliers de la chrétienté. Dans la conjonction de ces trois puissances résidait l'avenir de l'humanité, Wibald de Stavelot en était persuadé. Et cela avait suffi pour qu'Hermann se voie contraint à un long, périlleux et bien inconfortable périple jusqu'à Jérusalem.

Il craignait que cela ne mine définitivement ses chances de promotion, à courir ainsi loin des centres de pouvoir. S'il se trouverait au plus près du prince régnant actuellement sur l'empire, sans toutefois en porter le titre, il ne pourrait que difficilement rencontrer d'occasions de se hisser à l'épiscopat ou l'abbatiat, qu'il visait depuis tant d'années. Il redoutait de finir semblable à Rahewin, confident et secrétaire du puissant évêque de Freising. Homme d'importance, mais dont l'existence était subordonnée à un maître. Hermann découvrait chaque jour avec horreur un nouveau point de ressemblance avec le discret fonctionnaire, qu'il commençait à considérer comme un ami.

Depuis le matin, Rahewin établissait une liste d'ouvrages et de documents que le prélat emporterait avec lui. Fin connaisseur des volumes de la bibliothèque et des archives, il notait en un long inventaire ce que les valets devraient soigneusement empaqueter. Il convenait de n'omettre aucun livre d'importance, de façon à avoir sous la main tous les arguments et les preuves tangibles de la supériorité de leur foi et de leur liturgie quand ils seraient dans les provinces byzantines.

Rahewin marqua finalement une pause, reposant sa plume avant de bâiller ostensiblement.

« Il me semble que la lumière baisse, je vais mander quelques lampes, déclara-t-il. Et aussi un pot ou deux de fruits au vin, le souper demeure fort loin et nous n'avons diné que légèrement. »

Sans pour autant succomber à une gourmandise effrénée dont on moquait bien des clercs, Rahewin aimait manger à toute heure, se nourrissant autant de la vue des autres grignotant que de ses propres bouchées. Doux et chaleureux, il avait à cœur de se faire apprécier de tous, quand bien même ce n'étaient que de ses subordonnés. Hermann y établissait la différence fondamentale entre eux : le secrétaire de l'évêque de Freising n'avait aucune ambition, satisfait du rang auquel il était parvenu.

La proposition relâcha la discipline implicite et les bavardages naquirent tandis que chacun prenait place pour l'encas. Hermann lui-même approcha son tabouret du grand feu de la cheminée, tendant ses mains vers les flammes. Rahewin vint s'installer près de lui et il se dit qu'il pourrait en profiter pour lui soutirer quelques informations. Sans pour autant être un espion, il veillait à en collecter le plus possible pour son abbé.

« Était-ce coursier du roi qui est arrivé ce matin ? J'ai vu son train en la cour. Importantes nouvelles ?

--- Rien de bien nouveau. Il confirme que le grand départ sera fait depuis Ratisbonne, où se tient ces jours-ci la Diète[^145]. Avant toute chose, il souhaite garantir la couronne à son fils. Nous ferons donc route certainement peu après les Pâques. »

Il se rapprocha insensiblement du clerc, avant de sortir d'une de ses manches une chute de parchemin sur laquelle son écriture précise emplissait les moindres recoins.

« Mais foin de ces soucis de labeur ! Je vous sais fort amateur de poétique et j'ai espoir de porter en satisfaisants hexamètres l'hagiographie de saint Théophile. J'y vois là sujet d'édification propre à répandre l'amour de la Vierge, bienheureuse mère de Dieu. Écoutez donc et dites-moi si ces paroles résonnent en vous... »

### Damas, oasis de la Ghuta, chapelle royale de Conrad, matin du mercredi 28 juillet 1148

Le vent faisait craquer les membrures de la toile et se balancer les lampes suspendues tandis qu'Hermann et Rahewin finissaient de ranger leurs tenues de célébration. Une fois encore, Conrad avait écourté sa présence à la messe, emportant avec lui son aréopage de soldats et conseillers. Les deux prêtres déploraient que le combat spirituel soit ainsi négligé, et voyaient là un accroc au prestige de leur fonction. Et ils s'accordaient parfaitement sur le résultat redouté de ces façons de faire cavalières envers leur Créateur.

« Le roi est furieux que de si grands feudataires commencent à faire désaffection, se lamenta Rahewin. J'espère qu'il saura les ramener à la raison.

--- Le souci, c'est que les barons d'ici aiment trop à écouter le son de leur propre voix. Et ils n'ont goût que pour batailles et lances rompues ! Nulle surprise qu'ils aient si peu entendu ce que narrait l'évêque Geoffroy[^146] !

--- Il se dit au parmi des hommes que des dinars infidèles auraient convaincu certains seigneurs poulains...

--- Cela expliquerait l'empressement de certains à vanter la puissance des armées ennemies en marche pour libérer la ville. J'ai bien vu combien cette nouvelle avait assombri l'humeur du roi ! »

Rahewin finit de plier son étole et la rangea dans une solide malle dont il ferma les serrures d'une clef à sa ceinture.

« Souvenez-vous des conseils de l'impératrice à son père[^147] ! Les fils des intrépides conquérants menés par l'évêque Adhémar[^148] ne sont que des nains par rapport à leurs illustres devanciers. Ils se querellent la moindre terre, la plus petite province.

--- De certes, leur tempérament batailleur les incite à chercher renommée et vaillance, alors qu'ils n'obtiennent que gloriole et témérité. Sans mesurés conseillers pour les assister, toute leur force ne devient que bravache. »

Rahewin opina du chef longuement, un peu dépité.

« L'évêque Othon m'a confié que le roi a espoir de rallier de nouvelles échelles, pour revenir en force. Et il escompte trouver en la sainte cité plus de courageux capitaines, bien fournis en viandes et en armes, plutôt que crasseux pérégrins. »

N'ayant aucun intérêt pour la campagne en cours, Hermann ne se sentait que peu dépité à l'idée de quitter le campement de toiles, même si c'était pour quelques semaines. Il avait espéré, comme tous les membres de l'hôtel royal de Germanie, que le siège se déroulerait aisément. Mais il avait appris, depuis leur effroyable traversée de l'Anatolie, que les périls étaient nombreux dans ces régions. Il attendait juste avec impatience le moment où il pourrait prendre le chemin du retour. Néanmoins, conscient que son compagnon s'était plus enflammé pour leur sainte mission, il gardait ses réflexions pour lui.

### Pavie, palais épiscopal, midi du vendredi 4 mars 1160

D'une humeur sombre, Hermann sélectionnait les tenues qu'il emporterait avec lui lpour ce nouveau long périple. Il ne pouvait prendre qu'un coffre et devrait subir les périls de la haute mer, dans une galère spécialement affrétée. Il était malade rien qu'à l'idée de devoir supporter d'interminables journées sans voir aucune côte. Lorsque le concile avait élu le pape Victor, il était évident que son opposant, qui se faisait appeler Alexandre, n'accepterait jamais d'abdiquer. Il avait déjà refusé de se présenter devant l'empereur Frédéric pour se soumettre à son jugement. La suite logique de ces affrontements était l'envoi de diplomates auprès de tous les monarques afin de se garantir leur soutien et la reconnaissance du souverain pontife légitime.

Et la curie impériale l'avait désigné, lui, comme première ambassade en Outremer, auprès du roi Baudoin, le gamin écervelé qui avait assiégé en vain une cité au milieu du désert une douzaine d'années plus tôt. Il ne doutait pas un seul instant que c'était là le résultat de quelques manœuvres de jeunes clercs de son entourage, Erlung ou bien Burchard. Il avait vu arriver ces petits ambitieux et les observait se hisser dans les meilleurs postes avec une célérité qui le consumait de jalousie. Une telle mise à l'écart à son âge avancé risquait de le contraindre à finir simple moine dans un couvent de Silésie ou de Poméranie.

Pour une fois, Hermann n'aurait pas désapprouvé l'envoi d'un homme de guerre, plus enclin à affronter les pirates siciliens. Néanmoins, il savait l'empereur souverain habile et ne pouvait que se rendre à sa froide logique. Il utilisait chacun tel un des pions aux échecs afin de garantir sa victoire. Et il avait besoin de toutes les lances disponibles pour abattre Milan. En outre, le roi de Jérusalem, encore jeune et sans enfant, n'était pas vraiment intégré dans les jeux diplomatiques entre France, Angleterre et empire. Le rallier serait un avantage, mais son appui n'était pas stratégique. Ce qui renvoyait Herman à l'étendue de son importance à la curie. Il savait qu'il n'y allait que dans l'urgence, pour préparer le terrain à plus éminent plénipotentiaire, désigné par la suite, mais il ressentait cela comme une certaine rebuffade qui n'améliorait guère son humeur.

Faisant le tri dans ses affaires, il se remémora avec aigreur d'un des poèmes qui circulait dans les écoles qu'il avait fréquentées, enfant :

> *Pontificum causas regumque negocia tractes*\
> *Qui libi divicias deliciasque parant*.[^149] ❧

### Notes

Le XIIe siècle a vu l'accession de nouveaux administrateurs à des postes d'influence, qui n'étaient pas issus de la noblesse héréditaire ou des hommes de guerre. Il s'agissait de clercs, des lettrés formés dans les meilleures écoles, qui prirent peu à peu leur place dans les structures de gouvernement des différents monarques, dans des proportions assez variables. Le dédain avec lequel ils étaient accueillis par les tenants d'un pouvoir viriliste ou traditionnel n'était égalé que par leur superbe, nourrie de savantes références et de maîtrise d'abstractions érudites. Ils apportèrent néanmoins de nouvelles manières de faire dans la gestion des provinces et renouvelèrent le périmètre de recrutement habituel des cours seigneuriales.

Le terme de *curiales* était à l'origine utilisé de façon péjorative, comme par Pierre Damien en 1072, pour désigner ceux qui se servaient de leurs cercles relationnels pour obtenir des avantages.

La citation finale est rapportée à l'origine par Étienne de Tournai, qui cite un poème qu'il a connu alors qu'il étudiait à Bologne avec Richard Barre.

### Références

Casagrande Maria, Vecchio Silvana, « Clercs et jongleurs dans la société médiévale (XIIe et XIIIe siècles) », Annales, 1979, vol. 34, num. 5, p. 913‑928.

Foulon Jean Hervé, « Le clerc et son image dans la prédication synodale de Geoffroy Babion, archevêque de Bordeaux (1136-1158) », Amiens, 1991.

Turner Ralph V, « Toward a Definition of the Curialis: Educated Court Cleric, Courtier, Administrator, or "New Man"? », Medieval Prosopography, 1994, vol. 15, num. 2, p. 3‑35.

Primus inter pares
------------------

### Acre, château royal, matin du mercredi 10 novembre 1143

La brume matinale s'était retirée rapidement, laissant un timide soleil chasser les ombres récalcitrantes. La cour, gorgée de l'eau des averses nocturnes, n'était que flaques et boue, mais cela ne semblait nullement gêner le jeune homme blond, richement habillé, agenouillé auprès de chiots. Il agaçait d'un bâton ceux qui voulaient jouer, éprouvant le mordant des plus vifs d'entre eux. À ses côtés, un valet aux jambes arquées, vêtu de toile solide, surveillait d'un œil distrait que les bêtes n'en prenaient pas trop à leur aise. Si le prince Baudoin était amical et peu enclin à reprocher à quiconque ses propres décisions, la reine était moins débonnaire vis-à-vis des serviteurs qui laissaient son fils revenir crotté comme un vilain. Il y avait des limites à la simplicité de mœurs estimait-elle.

« Je choisirais bien le petit marron pour diriger ma meute, il me semble plein d'allant et a le regard qui sied à bon chasseur !

--- Sire, il est trop jeune. Il lui faudra faire ses crocs avant de prétendre mener la courre.

--- Fi donc, il me paraît solidement taillé, jarret épais et large gueule. Il doit bien peser une demi-livre de plus que ses frères !

--- Cela ne saurait suffire. Il ne servira de rien sans soumission des autres. La meute se fait ainsi que l'a voulu Dieu, et pas au souhait de l'homme, fut-il prince de grand royaume. »

Gembert attrapa le chiot pour l'examiner d'un œil sévère, tout en montrant une bête adulte enfermée avec ses pairs dans l'enclos de clayonnage peu loin.

« Regardez Tiens-Bon : comment il se tient... »

Baudoin releva la tête et étudia la meute qui se tenait au repos. Ce n'étaient pas les chiens qu'ils préféraient, ayant l'habitude des bêtes de Jérusalem. Sur la côte, il n'aimait rien tant que lancer ses rapaces, qu'il pouvait admirer à loisir tandis qu'ils survolaient les vagues. Il ne connaissait donc pas personnellement chacun des animaux. Ne voyant pas où voulait en venir le maître veneur, il haussa les épaules.

« Il me semble surtout qu'il a grand désir de sieste, allongé comme il est.

--- Regardez plus avant : il garde les yeux ouverts et, surtout, il est en hauteur.

--- Comme chacun, il n'aime guère s'affaler en la boue !

--- Surtout, il peut voir tous les siens, guetter lesquels gagnent de l'assurance, lesquels se tiennent cois...

--- Es-tu en train de me dire que simple cabot surveille sa mesnie comme un juif ses deniers ? Me prends-tu pour jouvenceau de cité ? »

Gembert laissa échapper un rire rauque et rendit l'animal au jeune prince, s'approchant pour lui montrer ce qui se déroulait dans l'enclos.

« Je leur ai donné quelques restes ce tantôt. Il en conserve plus bel os entre ses pattes, mais a laissé ce qui ne l'intéressait pas. Il surveille qui y arrive le premier et qui ose passer dans ses traces. Il sait que le défi viendra de là. S'il a désir de garder la tête, il lui faut être au fait de qui a volonté de le pousser hors. Demeurer plus haut lui garantit de ne rien manquer du spectacle. Outre, il se fait voir ainsi que le donjon d'un seigneur à ses vilains : il leur manifeste sa primauté. »

Baudoin acquiesça, caressant le chiot machinalement. Il n'avait jamais pensé que la constitution d'une meute répondait à autant de règles. Habitué à ce que les choses se conforment à ses souhaits, il découvrait la nécessité d'assimiler la façon dont les animaux fonctionnaient. Cela contrariait quelque peu ses désirs, mais il en éprouvait le bien fondé. Il n'était pas de ces jeunes nobliaux fats et trop imbus d'eux-mêmes pour ne jamais rien apprendre des plus modestes. Il y avait du bon sens à retirer de la main qui sème, du dos qui porte et même du chien qui ronge son os, il en était persuadé. Il se mit à étudier le petit animal qui, excité par les manipulations, tentait de lui mordre le pouce.

Une effervescence dans la cour le fit se retourner vers le logis, d'où venait de sortir la silhouette empâtée de leur seigneur à tous, le roi Foulque son père. Celui-ci s'avançait à grandes enjambées, agité ainsi qu'à son habitude, un sourire aux lèvres, appréciant la chaleur des rayons sur son visage. Vêtu d'une sobre tenue de toile solide, il héla son fils de sa voix habituée au fracas des lances et des chevaux :

« Fils, nous allons courir sus quelques bestes, si Dieu le veult. Ta mère a grand désir de sortir de cette cité pestilentielle, et j'aurai moi-même bon plaisir à galoper en nos terres avant que l'hiver ne vienne nous confiner entre quatre murs tels tonsurés. Va donc quérir ton cadet : aujourd'hui, nous chassons ! »

Heureux de cette perspective, Baudoin rendit le chiot au veneur et se hâta d'aller préparer quelques affaires. Il repenserait plus tard à sa propre meute, quand il serait en âge. Pour le moment, il se contenterait de profiter de celles de son père. Il n'était pas temps pour lui de s'encombrer de telles préoccupations.

Il ne le savait pas encore, mais c'était là le dernier jour d'insouciance de sa vie.

### Naplouse, cour du manoir seigneurial, matin du vendredi 23 juin 1161

Installé sur un banc près de la grande porte, Aymar gardait un œil sur les entrées et sorties. Occupé à graisser sangles et sacoches, il réfléchissait aux scénarios qui s'offraient à lui pour les semaines à venir. La maladie de Mélisende était dans tous les esprits, car le destin de leur maître à tous, Philippe de Milly, était trop imbriqué à celui de Mélisende pour que la disparition de cette dernière ne soit pas sans conséquence. Le seigneur de Naplouse était l'ultime grand baron qui avait fait fronde avec la reine quand Baudoin avait désiré s'affranchir de sa tutelle. Et si depuis pratiquement une dizaine d'années, il régnait quasiment sans partage, il avait sévèrement puni tous ceux qui avaient alors fait le mauvais choix. Tous, sauf Philippe de Milly, seigneur de la ville où la reine mère s'était retirée.

Aymar savait qu'un soldat capable comme lui trouverait toujours de l'embauche, mais il avait pris ses aises à Naplouse, où il logeait dans une petite maison avec femme et enfants depuis plusieurs années. Il s'était habitué à son figuier, ses poules et la nuée de chats qui baguenaudaient dans le quartier. Il avait déjà vécu l'exil lorsque son précédent maître, Manassès de Hierges, avait connu la disgrâce et n'avait aucune envie d'y être de nouveau confronté.

Il reconnut la silhouette empressée d'un des clercs de la reine qui sortait du grand bâtiment. Formé dans un monastère, il avançait à petits pas ainsi qu'il seyait à un digne lettré, mais il le faisait si vite qu'il marquait le rythme de la tête d'une façon semblable à un gallinacé. Aymar posa son pot de graisse et s'essuya les mains tout en venant à la rencontre du secrétaire.

« Le bon jour, Jehan. As-tu porté quelque nouvelle réjouissante à mon sire ?

--- Il n'appartient pas au messager de choisir les oreilles qui doivent entendre ! » répondit le fin notaire, horrifié de se faire ainsi apostropher.

Puis, comprenant toute l'angoisse de la situation dans laquelle tous étaient plongés par la maladie de Mélisende, il se fendit d'un sourire bref avant d'ajouter d'une voix plus amène.

« Ton maître a grand amour de la reine, il est honnête qu'il sache matin et soir comment évolue sa santé...

--- Nous prions tous pour sa guérison rapide, soyez-en assuré.

--- De cela je ne doute. Seulement, je crains que celles-ci n'aident à sauver que son âme. Sa dépouille mortelle est au plus mal, malgré tous les bons soins de ses sœurs, de ses médecins... »

Aymar gratta son imposant nez, comme il le faisait chaque fois qu'il était ennuyé, se rapprocha du clerc et lui demanda à mi-voix :

« Le roi n'a mandé nul coursier ? »

Le visage du clerc d'austère devint pathétique.

« Rien. Pas même un message de circonstance. Nul n'ignore la détestation qu'il éprouve, mais c'eut été fort chrétien...

--- Il se murmure qu'il affûte sa hache pour le col de l'ultime féal de Mélisende. Tout le monde ici ajoute à son désespoir de savoir la reine si mal l'angoisse d'imaginer l'hôtel de Milly être mis à bas.

--- Nous serons de certes les derniers à découvrir les intentions de Baudoin. Nous en parlons souventes fois avec mestre Gui, qui a porte ouverte en la chambre de la reine. Il se murmure que son fils a gros appétit de ces terres et qu'il y verrait bien simple châtelain y percevoir les taxes en son nom.

--- Tout de même, il ne peut ainsi congédier baron comme le sire Philippe !

--- C'est le roi. Il a la main au Grand Conseil. »

Jehan se fendit d'un sourire matois, savourant ce qu'il savait comme une délicieuse sucrerie.

« Malgré tout, il doit compter avec ses tantes. La comtesse et l'abbesse ne laisseront pas les choses se faire sans peser de tout leur poids. Et qui les connaît ne peut ignorer combien elles ont l'oreille des prélats, pour le moins. »

Aymar n'était pas au fait des jeux d'alliance et de la subtilité des rapports de pouvoir dans la famille royale. Il avait entendu d'étranges rumeurs autour d'Hodierne de Tripoli, dont l'époux avec lequel elle était en conflit avait fini sous les coups d'un assassin. Et il connaissait de réputation l'abbesse de Béthanie, à la tête d'un des plus importants lieux de pèlerinage des territoires latins. Quel que soit le rôle qu'elles joueraient, si elles demeuraient loyales à leur sœur, ainsi qu'elles l'avaient toujours fait, les choses ne seraient peut-être pas si aisées pour Baudoin. Et, après tout, Philippe de Milly n'avait-il pas travaillé d'arrache-pied à protéger Jérusalem ces dernières années ? Son dévouement à Mélisende était avant tout fidélité à la couronne, et depuis que Baudoin avait pris seul les rênes du pouvoir, le seigneur de Naplouse n'avait jamais restreint son énergie pour soutenir les ambitions royales.

### Jérusalem, Temple de la Milice du Christ, veillée du mercredi 5 juillet 1161

En fin de journée, la terrasse préférée du maître de la Milice Bertrand de Blanquefort offrait une magnifique perspective sur le dôme du Temple. Il y venait pour assister à l'engourdissement progressif de la ville tandis que les lueurs plus vacillantes des flammes domestiques succédaient au flamboiement du soleil couchant. Il en profitait pour relire certains courriers, en espérant trouver l'inspiration pour des réponses durant son court sommeil. Il appréciait aussi de jouer aux tables, surtout aux échecs, qui lui permettaient de s'abstraire du monde pour focaliser sa pensée sur un problème ou l'autre. Il invitait donc à tour de rôle ses principaux officiers, ses intimes ou ses familiers pour passer agréablement ces soirées citadines.

Il avait une affection toute particulière pour Geoffroy Foucher, le neveu du maître précédent. Fils de bonne famille au tempérament batailleur, celui-ci s'était assagi en venant en Terre sainte. Bien qu'il fût tout à fait à l'aise à rompre des lances, il était rarement employé comme combattant, envoyé là où ses talents de mesure et de diplomatie pouvaient être plus efficaces. On le disait capable de parler plusieurs langues, de lire le latin aussi bien qu'un clerc et il avait en tête les relations et la généalogie de nombreuses lignées régnantes, y compris chez leurs adversaires musulmans. Mais son plus grand atout était de pousser le pion agréablement, sans chercher à gagner à tout prix, accordant la victoire non sans avoir opposé suffisamment de résistance pour qu'elle ne soit pas factice. Bertrand soupçonnait là quelque habileté de diplomate, mais appréciait de se les voir appliquer. Il n'était jamais insensible aux marques de respect.

Il allait donner la main à son adversaire d'un soir, certain de son succès prochain, quand il aborda une des questions qui lui tournait en tête depuis quelques semaines.

« Vous qui avez bonne affection pour la reine, vous devez être au fait des inquiétudes qui agitent la haute Cour. N'avez-vous rien à m'en dire qui puisse m'éclairer ?

--- Mélisende est pour moi une amie fidèle et un soutien sans faille, mais je n'ai que vague connoissance de ces sœurs. Quant au sire de Milly, auquel vous pensez, nous nous côtoyons, sans que je ne puisse me prétendre son intime.

--- C'est lui que j'ai en tête, de certes. Tout le monde sait que Mélisende vit ses derniers instants. Son confesseur l'entend chaque jour dit-on, la pauvre âme. Et sans le respect qu'elle inspirait encore à son fils, si ce n'était plus de la crainte, il y a de quoi s'inquiéter du destin de son plus indéfectible fidèle.

--- La couronne a plus que jamais grand besoin d'hommes de sapience et de vaillance. Baudoin n'est pas irréfléchi...

--- Certes pas, mais il a la rancune tenace et la mémoire longue, contrairement à son père. Milly compte-t-il quelques appuis qui pourraient s'exprimer en sa faveur ? Le connétable pourrait glisser quelques mots aimables propres à panser l'orgueil blessé de Baudoin...

--- Onfroy n'est guère pressé de faire remembrance au roi de ces moments où lui-même a hésité. Mais il connaît la valeur du sire de Milly, et ne manquera de soutenir quiconque parlerait en son nom. »

Geofroy déplaça un de ses comtes, sachant qu'il exposait là quelques-uns de ses piétons. Mais cela ne lui importait guère, il n'aimait pas tant les échecs pour chercher à y remporter toutes les guerres. Y accomplir quelques belles manœuvres seyait mieux à son esthétique belliqueuse. Tandis que le maître réfléchissait à son tour, il passa en revue les éventuels soutiens que pouvait compter le baron vieillissant de Naplouse.

« Il est avéré que le sire évêque d'Acre a bonne estime de sire Guy, mais il n'a pas tant l'oreille du roi... »

Geoffroy marqua une pause tandis son cerveau passait en revue les relations interpersonnelles dont il avait connaissance autour de lui, et qui contenaient Philippe de Milly et le roi Baudoin dans leurs toiles. Puis il eut une soudaine inspiration.

« Mais, attendez, nous pourrions tout simplement en appeler au sire comte de Jaffa ! Il a toujours gardé bonne amitié aux proches de sa mère, et saura toucher par quelque parole tendre son frère ainé. Ce serait naturel ambassadeur.

--- Vous l'en pensez capable ? On le décrit assez mol de caractère, irrésolu et peu volontaire.

--- Ce sont là billevesées de médisants, je vous assure. Prêtées à lui en raison d'un bégaiement dont il a tardé à se débarrasser. C'est, à n'en pas douter, une belle âme, qui n'hésitera pas à faire droit à ceux qu'il a tant côtoyés avec sa mère, enfant.

--- Il faudra que ce soit vous qui lui parliez, alors. Vous saurez trouver les mots qui feront mouche. Nous ne pouvons nous défaire d'un homme de la valeur de Philippe de Milly en ces temps troublés. Je n'ai guère envie de le regarder prendre les voiles pour la Flandre ou la Bourgogne. »

Geoffroy partageait l'analyse du vieux stratège. Il avait vu les principaux dirigeants des territoires latins se faire capturer ou mourir en l'espace de quelques années. Partout ce n'étaient que régence et mineurs. Et, pour leur plus grand malheur, il n'y avait plus de célèbre abbé par-delà les mers pour en appeler à sauver la cité du Christ.

### Jérusalem, demeure de Droart, soirée du vendredi 21 juillet 1161

La cité était encore agitée, ses rues emplies des pèlerins fébriles à l'idée de fêter la prise de la ville, quelques décennies plus tôt. Les habitants préféraient donc se calfeutrer chez eux pour y passer la veillée en bonne compagnie. Comme souvent, Droart recevait ses intimes, Ernaut et Libourc avaient une nouvelle fois pu goûter les excellents plats d'Ysabelon. Cette dernière se montrait plus enjouée depuis le mariage, ayant reconnu en Libourc les façons d'une de ses jeunes sœurs. Elle s'était depuis lors mis en tête de lui communiquer tous les secrets qui lui seraient nécessaires pour supporter les hommes, disait-elle.

Ayant abandonné les femmes et les enfants dans la cour, Ernaut et Droart étaient montés sur la terrasse pour y admirer l'apparition des étoiles. Malgré le brouhaha des quartiers environnants, le lieu respirait la sérénité. Avec les beaux jours, des matelas y étaient installés et il arrivait même que la famille y passe la nuit.

« Tu as déjà pensé à quitter l'hostel le roi, Ernaut ? »

La question, posée d'une voix calme par Droart, fit sursauter Ernaut, qui tourna un visage inquiet vers son ami.

« Que me chantes-tu là ? Je viens de me marier et je n'ai nulle famille ici, nul bien. Que pourrais-je faire d'autre ?

--- Je me demande parfois si le roi est... bon maître.

--- Tu as encontré quelques soucis ces derniers temps ? »

Ernaut n'ignorait pas que Droart était impliqué dans de nombreux trafics, mais cela restait dans les limites acceptables par tous. Sans qu'il se sente suffisamment confiant pour s'associer, il n'éprouvait nulle inquiétude pour son ami, sauf à vouloir faire un exemple, ce qui arrivait malgré tout de temps à autre afin de garder la mesure.

« Non, rien de personnel. Je pensais au sire de Milly. Mélisende n'est pas encore en son tombeau que Baudoin règle ses comptes. Il l'envoie au désert.

--- De quoi parles-tu ?

--- J'ai dîné tantôt avec Brun et Raoul. Une charte se prépare, qui fait échange de provinces entre la couronne et Philippe. Il va perdre tout ce qu'il possède à Naplouse et ne récupère que les pierres et la poussière d'Oultre Jourdain, la seigneurie de Montréal et de Kérak.

--- Tout de même, voilà terres royales, non ? Bel exil pour qui lui a déplu. À ce compte je veux bien en être.

--- Une charruée là-bas ne vaut pas courtil de Samarie, crois-m'en : au-delà de la Rivière du Diable, c'est le bout du monde. »

Ernaut ne s'expliquait toujours pas l'attachement que beaucoup éprouvaient envers Mélisende. Lui avait juré sa foi au roi Baudoin et il ne le trouvait pas mauvais maître. Chaque fois qu'il l'avait croisé ou aperçu, il lui avait fait l'impression d'être courtois et affable et nul ne le dépeignait en mal parmi les domestiques de l'hôtel qui le côtoyaient chaque jour.

Sa poitrine se comprima néanmoins à cause de la dernière remarque de Droart. Cela faisait plusieurs années qu'il était sans nouvelle de Droin, dont il avait appris les démêlés avec les moines près du Jourdain. Connaissant son compagnon d'enfance, il n'avait pu demeurer en retrait bien longtemps et, si on n'entendait plus parler de lui, c'était certainement parce qu'il était parti dans un endroit loin de tout.

« C'est toujours le royaume, tout de même !

--- Chaudemar et les grottes des ladres au sud font partie de Jérusalem aussi... »

Ernaut se laissait contaminer par l'humeur nostalgique de Droart. Il se demandait si, à l'avenir, on lui reprocherait ses amitiés, ses liens avec l'un ou l'autre, et qu'on lui signifierait son exil parce qu'il avait accordé sa confiance à la mauvaise personne ? Il avait désormais une épouse, une famille dont il espérait voir les enfants grandir. Partageraient-ils le poids de ses fautes ? Lui si habitué aux fanfaronnades quand on parlait de son physique, il se sentait soudain les épaules bien lourdes. Sans y penser, il jouait avec son anneau d'or. ❧

### Notes

Au tournant de la décennie 1160, de nombreux changements eurent lieu au sein de la noblesse de Jérusalem, en partie causés par les batailles et guerres incessantes, mais aussi par voie légale. J'ai déjà évoqué la charte qui installa Guy de Milly au sud du royaume, après avoir abandonné ses terres. Certains y voient une disgrâce, d'autres un choix stratégique. J'ai pris l'option de considérer les deux hypothèses mêlées. Baudoin n'était pas un autocrate comme pouvait l'être Louis XIV et il devait sans cesse gérer les influences à la haute Cour pour arriver à ses fins. Ce qui pouvait parfois occasionner des dénouements complexes à appréhender.

Par ailleurs, le destin d'Ernaut est lié à ces événements et sa vie va connaître de profonds bouleversements dans la décennie qui s'ouvre, en partie à cause de cette charte, ce qui explique aussi son importance comme point de pivot de plusieurs Qit'a.

### Références

Barber Malcolm, « The career of Philip of Nablus in the kingdom of Jerusalem », in Peter W. Edbury et Jonathan P. Philips (dir.), The experience of crusading, Cambridge University Press, 2003, vol.2, p. 60‑78.

Burgtorf Jochen, *The Central Convent of Hospitallers and Templars: History, organization, and personnel (1099/1120-1310)*, Leiden - Boston, Brill, 2008, vol. 50.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », Dumbarton Oaks Papers, 1972, vol. 26, p. 93‑182.

Apostat
-------

### Antioche, casernement près de la porte du Chien, matin du lundi 4 juillet 1149

Galien fut réveillé par les éclats de voix d'une dispute. Inquiet de l'entendre si près, dans son dortoir, il ouvrit un œil. Son capitaine essayait visiblement de ramener de l'ordre dans une discussion animée entre deux groupes. Il se releva, s'asseyant sur sa paillasse posée à même le sol. Il s'était couché tout habillé, ayant veillé fort tard aux environs de la porte du Chien qu'il contribuait à garder. La dispute semblait se calmer et l'attroupement commencer à se disperser. Galien se tourna vers Courrat, son compère, d'un air interrogateur. Celui-ci était en train de ranger ses affaires dans son coffre.

« Rien de grave. Certains des gars pensent qu'il faut absolument tenir et d'autres avancent qu'on devrait demander l'aman[^150]. Le capitaine leur a dit que de toute façon ça dépendait pas de nous. »

Il s'assit sur sa malle, laçant ses bottines.

« Nous devons de nouvel nous poster vers la porte du Chien ce matin, les engins semblent vouloir se tourner sur cette partie de l'enceinte.

--- D'aucune façon, nous ne saurions tenir solidement les murailles avec le peu d'hommes que nous sommes.

--- Je ne suis pas si sûr. Il est encore temps que le roi Jérusalem s'en vienne.

--- Nul coursier du sud ne s'est présenté, que je sache...

--- La reddition te semble donc une solution envisageable ? »

Galien se rapprocha de son ami et lui répondit à voix basse.

« Et alors ? Je suis sûr que de bons soldats trouvent toujours preneurs. Réfléchis un peu à ça, mon vieux. Moi, je suis pas prêt à me faire sabrer le col pour un besant la semaine. »

Il se leva pesamment puis rassembla ses maigres affaires dans son coffre, qu'il referma à clef après en avoir sorti son armement. Équipé de son casque et de son arbalète, il alla remplir son carquois de carreaux dans un des tonneaux à cet usage au bout de la pièce. Enfin, il se rendit dans une grande salle adjacente où il pouvait se restaurer. Plusieurs de ses camarades étaient là, se préparant nonchalamment à une longue veille. En quelques mois, Galien avait fait partie de diverses garnisons depuis qu'il était venu des environs de Paris avec le roi Louis. Toujours prêt à accepter la moindre tâche militaire, il avait été recruté quelques jours plus tôt pour garnir les murs de la cité, maintenant que le seigneur des lieux avait été tué dans la bataille de Fons Muratus.

L'avancée du maître d'Alep, Nur ad Din, avait été fulgurante depuis sa victoire sur l'armée du prince Raymond. Il se disait dans les marchés que la capitulation des villes sur son chemin était promptement négociée. En cas de refus, les citadelles étaient mises à bas avec une cadence qui laissait craindre le pire. Des commérages détaillaient la détresse de la princesse Constance, qui avait confié la régence de la Cité au patriarche Aymeri.

Depuis quelques jours, des rumeurs circulaient parmi la population, et certains osaient discuter de reddition. Les esprits étaient troublés, entre les tenants d'un jusqu'au-boutisme absolu, dont les plus récents arrivés étaient, et ceux qui pensaient qu'il était possible de se rendre de façon honorable, s'appuyant sur le fait que sans armée pour les défendre, la situation était désespérée.

Le capitaine siffla le rassemblement alors qu'ils finissaient de prendre leur repas de bouillie froide, agrémenté d'une pincée de fruits secs. De sa voix rauque, il leur donna les instructions pour la journée. Les troupes musulmanes semblaient attendre, leur camp demeurant relativement tranquille. Il fallait néanmoins rester vigilants et en profiter pour se faire voir sur les murailles. Chaque tronçon en fut donc attribuée à un modeste groupe d'hommes, appuyé par des serviteurs ayant pour tâche de nettoyer et remettre en état les chemins de ronde. Galien et Courrat s'arrangèrent pour être ensemble et furent affectés à une section de courtine entre la porte Saint-Georges et le mont Silpius, où la situation était assez calme.

Chemin faisant vers le sud, alors qu'ils traversaient un secteur commerçant, Galien s'arrêta longuement chez un contact marchand, Chirag. Il l'avait connu au moment de son arrivée dans la principauté et avait travaillé pour lui à l'occasion, comme escorte pour ses caravanes. L'arménien avait de nombreuses relations d'affaires un peu partout dans la région et pratiquait le négoce, donc la diplomatie, avec un talent réputé. Galien voulait lui parler du plan qu'il avait en tête.

### Antioche, porte du Fer, soir du jeudi 7 juillet 1149

Galien rejoignit ses trois compères, appuyés contre un merlon du rempart, occupés à manger des dattes dont ils crachaient les noyaux en contrebas. Au-dehors, des feux illuminaient l'accès des gorges qui menaient à la ville, gardées par leurs adversaires. Bien souvent, des foyers étaient allumés en masse par les assiégeants afin de convaincre les captifs que l'armée qui les environnait était plus imposante qu'en réalité.

Comme il était peu probable que le gros des troupes puisse arriver par là, c'était un lieu assez calme, et il avait fallu aux compagnons batailler contre les prétentions des autres sergents qui souhaitaient aussi se retrouver dans un endroit peu exposé. Heureusement pour eux, les gardes de nuit à encadrer quelques miliciens n'étaient pas les plus recherchées. Responsable de la petite équipe sur cette section de courtine, Galien avait distribué quelques corvées à ses hommes, de la même façon que ses compères aux abords, espérant que cela leur laisserait le temps de glisser le long de l'enceinte et de s'évanouir dans le canyon entre Staurin et Silpius, avant que l'alerte ne soit donnée. Aucun d'eux n'avait envie de servir de cible aux archers.

Galien s'était muni des affaires qu'il tenait absolument à garder avec lui, ainsi que d'une arbalète, comme chacun de ses compagnons et avait confié le reste de ses possessions à Chirag. Il avait également pris une corde pour descendre la muraille. Galien et ses amis avaient choisi de tenter leur chance sous d'autres cieux.

« Bon, j'ai la lettre de Chirag qui devrait nous permettre d'être bien accueillis chez les sarrasins. Vous êtes toujours partants ? »

Jannoni, Wikerus et Courrat firent chacun à leur tour un signe de tête. Ils étaient décidés à déserter et à rejoindre le camp vainqueur. Galien fixa alors la corde à un des merlons de l'enceinte.

« À Dieu va ! dit-il en lançant un des brins par-dessus le parapet. J'y vais le premier. »

Les quatre hommes se retrouvèrent rapidement réunis au pied du mur, puis se faufilèrent comme des ombres, profitant de la très faible lune. Ils tentaient de se fondre contre les rochers qui balisaient le sentier entre les versants abrupts. Ils n'eurent pas le temps de cheminer bien loin qu'une voix se fit entendre, en arabe :

« Halte ! Qui êtes-vous, et que faites-vous là ? »

Celui qui venait de parler s'avança hors du repli de terrain qui le cachait jusque là. C'était un soldat bien armé, revêtu de mailles, accompagné d'une demi-douzaine de gardes moins bien équipés.

« Je vous ai vu descendre de la muraille. Vous êtes des espions ? Des assassins, avec toutes ces lames ?

--- Nous arbalétriers, nous vouloir aider ost Alep » répondit Galien dans un arabe approximatif en tendant sa lettre.

L'officier fit un signe de tête à un de ses hommes pour qu'il lui apporte le document. Sans même le regarder, il le glissa dans sa ceinture d'étoffe et lança quelques ordres secs, afin qu'on désarme les quatre déserteurs. Puis, d'un geste du bras, il leur fit comprendre de les accompagner, solidement escortés.

Ils empruntèrent plusieurs sentiers, contournant les reliefs vers le nord, se rapprochant de la grande route menant au Pont du Fer. Ils ne tardèrent pas à apercevoir les amas de toiles et de corde, les cabanes de ce qui constituait le gros du camp de siège, en retrait des zones de combat. Ils suivirent, un peu effrayés, leurs gardiens jusqu'à une esplanade où des broussailles épineuses délimitaient un espace de détention. Aux alentours, des sentinelles patrouillaient entre des feux servant d'éclairage.

À quelque distance, une tente de belle taille avait été érigée. Le meneur y entra, les laissant attendre à l'extérieur avec les soldats. Nul ne semblait s'émouvoir de leur présence et les quatre captifs échangeaient des regards circonspects, constatant l'ampleur du campement qui abritait ceux qu'ils appelaient jusque là leurs ennemis. L'officier ressortit un long moment après, donna quelques instructions puis les abandonna aux gardes. Ils furent alors rapidement conduits à l'enclos où ils découvrirent une poignée de prisonniers sales et hirsutes, certains arborant des pansements sommaires. Galien essaya de dire dans son arabe approximatif à ses geôliers qu'ils étaient des soldats et venaient se battre, il fut poussé sans ménagement et jeté au milieu des autres.

Jannoni le regarda, interdit :

« Tu es sûr de ton ami ermin, dis-moi ?

--- Autant qu'on puisse l'être. Il m'a jamais trahi.

--- Moi, j'ai l'impression qu'on va finir esclaves » déclara Courrat en s'asseyant dans la poussière.

En les entendant, un des captifs s'approcha d'eux

« Demeurez coi si vous souhaitez éviter les ennuis.

--- Qui es-tu ?

--- J'étais de la garnison Bazmashan[^151]. Ils m'ont proposé de m'engager si j'acceptais de me convertir. C'était ça ou finir esclave... Si j'ai bien compris, on va garnir une citadelle dans les reliefs au nord, je sais pas trop.

--- Devenir un païen ? Dans les montagnes ? » s'exclama Galien.

Courrat le regarda d'un air désolé.

« Tu t'attendais à quoi ? »

Galien, contrarié, s'éloigna de ses compagnons et regarda par-dessus les reliefs, vers la muraille de la cité d'Antioche, où flottaient encore les bannières franques, ombres à peine visibles qui cachaient les étoiles par intermittence.

### Harran, baraquements des soldats, soirée du youm al joumouia 29 jumada al-thani 549[^152]

Comme chaque jour de prêche à la Grande Mosquée, la cité avait été en effervescence, chacun profitait de ce jour de repos pour aller visiter ses proches, faire la fête ou pratiquer quelques loisirs. Galien s'était rendu avec ses compagnons à la prière solennelle sous la supervision de son officier, puis avait passé le temps à jouer aux dés avec eux. Ils avaient également improvisé un concours de tir, rivalisant d'adresse avec quelques archers turcs.

Séparé des amis avec qui il avait déserté, cela faisait plusieurs années qu'il était cantonné dans ce baraquement, d'où on le sortait bien trop rarement à son goût pour de courtes campagnes, généralement vers l'orient. Il avait rapidement appris des rudiments d'arabe et de turc, mais demeurait complètement étranger aux jeux politiques qui expliquaient les affrontements auxquels il participait ou les garnisons qu'il renforçait ici et là. Il était affecté à Harran la plupart du temps, où il ne percevait presque rien comme solde, mais bénéficiait du gîte.

Il frappa à la porte de la salle où son émir, Rashid al-Nasir tenait ses quartiers. Celui-ci était un homme assez paternaliste, parfois autoritaire, mais sans sévérité. Il aimait plaisanter à propos de tout et il était souvent difficile de savoir ce qu'il avait en tête. Galien avait gagné son estime en ne rechignant jamais aux corvées et aux travaux pénibles.

« Al-Anṭākīyyah ! Que viens-tu quémander en cette heure tardive ?

--- J'espère que je ne dérange pas, j'aurais besoin de votre avis.

--- Si cela concerne les femmes, ne t'embarrasse donc pas de questions, fais assaut ! » ricana l'émir en caressant son imposante barbe.

Comprenant que Galien avait une demande plus complexe, il l'invita de la main à prendre place sur la paillasse face à lui. Il était visiblement en train d'écrire une lettre, mais il poussa son nécessaire et interrogea d'un regard son subordonné.

« Vous nous permettez qui-ci qui-là menus travaux pour les gens de la cité, et j'aurais besoin de connaître jusqu'où cette autorisation s'étend.

--- Eh bien, tant que tes pas te gardent en nos murs et que tu es de retour au baraquement pour la prière hebdomadaire, ça me va. »

Galien hocha la tête, apparemment peu enthousiasmé par la réponse.

« Vous savez, j'ai usage de me louer à 'Isa al-Muzani...

--- Le négociant de plats ? Oui, je connais bien sa famille.

--- Voilà ! Et il se trouve qu'il prévoit un large convoi pour Alep, dans les semaines à venir. Il m'a proposé d'en être. »

Al-Nasir fit une moue qui se tourna vite en rictus réjoui.

« Alep me semble fort au-delà de nos murs... Et fort proche d'Anṭākīyyah !

--- Il y aurait d'autres membres de la garnison qui se loueraient : Da'ud al-Jahdami, Pâkalin et Boutoum.

--- Plusieurs canailles assemblées ne font pas un honnête homme, mon ami, s'amusa l'émir. »

Il passait sa main sur sa longue barbe tout en étudiant les réactions de Galien. À la lueur des lampes, ses yeux sombres semblaient des prédateurs camouflés sous ses épais sourcils.

« La période des campagnes est terminée, aussi paisible qu'à son habitude pour nous. Je comprends ton désir d'amasser quelques dirhems pour l'hiver. Qui serais-je pour m'opposer à cela ? »

Il assortit sa réponse d'un grand sourire, tout en ouvrant les mains en signe d'impuissance.

« Et s'il se trouve quelques absents lors de la revue de printemps, ce pourrait être dû aux fièvres de la saison froide, à quelque blessure mal soignée ou tout autre malheur du sort.

--- Je serai de retour avant ramadan !

--- Ne promets rien que tu ne puisses tenir, fils, *chercher à se justifier quand on n'est pas coupable, c'est s'accuser* ! »

Galien se renfrogna. Il avait bien sûr envisagé de profiter de ce déplacement qui le rapprocherait des provinces sous domination latine pour s'enfuir, mais le projet lui paraissait encore trop fou, trop flou, trop loin, pour prendre corps dans son esprit. Ce n'était rien de plus qu'une envie diffuse qui considérait soudain une porte de sortie. Al-Nasir hocha la tête.

« Pour ce travail, tu peux t'absenter, je ne te signalerai pas. Et pour le reste... N'oublie pas, fils : *ce qui est passé a fui ; ce que tu espères est absent ; mais le présent est à toi* ! »

Et sur cette parole énigmatique, il chassa Galien d'un geste de la main. Lorsque ce dernier fut dehors, l'émir s'autorisa un sourire, le temps de réaliser qu'il lui faudrait peut-être trouver quatre nouvelles recrues dans les mois à venir. Il balaya rapidement ces sombres pensées de son dicton favori : *Aujourd'hui maintenant, demain inch'allah*.

### Terre de Suète, abords du Yarmouk près des caves de Suète, matin du vendredi 11 juillet 1158

Galien et ses compagnons finissaient de rouler leurs sacs quand ils entendirent corner le premier appel. L'armée royale allait se mettre en marche, et cette fois c'était après une victoire, une de plus qui marquait un printemps plutôt florissant. L'arbalétrier avait rejoint une troupe sous le commandement d'un chevalier croisé, Hugues. Celui-ci était venu en Terre sainte dans l'espoir d'y aider au triomphe des forces chrétiennes et s'activait avec ferveur. Indifférent à la soif, la fatigue, les blessures, il dépensait sans compter pour soutenir l'effort de guerre.

C'était lui qui les avait prévenus la veille au soir de la décision du roi Baudoin de retourner à Jérusalem. Bien qu'obéissant, Hugues trouvait tel repli inopportun, prématuré, et aurait préféré que l'armée pousse son avantage pour capturer leur adversaire. Dépité de ce manque de témérité, il avait rejoint certains autres esprits échauffés autour de Bertaud des Lices. Celui-ci avait proposé de partir vers le sud où des escadrons égyptiens avaient ravagé certains territoires. Persuadé que Dieu leur apporterait une nouvelle fois la victoire, Bertaud envisageait de simplement se ravitailler dans la cité sainte avant de patrouiller la zone méridionale du royaume, jusqu'à Gaza et le désert.

La paie étant correcte, Galien avait décidé de prendre part au voyage. Il n'avait guère eu son content de butin avec les expéditions printanières et espérait encore pouvoir amasser de quoi tenir l'hiver confortablement. Il formait aussi depuis plusieurs mois le projet d'obtenir un coursier. Il sentait les années qui s'accumulaient sur ses épaules et sa blessure à la jambe se rappelait à lui de façon plus fréquente, augmentant sa claudication. Il avait donc entrepris de négocier pour se faire attribuer une monture afin de s'épargner la marche sur les sentiers caillouteux. Il espérait par ce biais être reconnu arbalétrier monté, voire capitaine pour ceux officiant à pieds. Il avait l'expérience pour cela, et ne manquaient que les quelques livres qui lui auraient permis de posséder son propre animal, garant de son statut.

Il finissait d'assujettir ses affaires sur son dos, son arme soigneusement protégée dans sa housse de cuir, quand le cor résonna pour la seconde fois. Sans se presser, les hommes se rassemblaient pour former le long cortège, chacun retrouvant la place qu'il avait tenue lors de leur arrivée. Galien déposa un de ses sacs, contenant un supplément de carreaux dans le chariot qui lui avait été assigné et rejoignit son rang. La poussière commençait déjà à irriter la gorge et certains nouaient un foulard sur leur visage pour s'en préserver. Au-dessus d'eux, un soleil éclatant resplendissait sur les reliefs gris. La journée serait interminable, mais au moins nulle crainte n'était à avoir d'éventuels assauts turcs.

Un des capitaines les longea au petit trot, balançant des outres de chèvre où allait croupir l'eau fraichement prélevée dans le Yarmouk. Des valets courbés sous de gros sacs de toile distribuaient du pain noir dans les rangs.

« On va pas faire de pause » grogna le voisin de Galien tandis qu'il tranchait avec peine la solide croute.

Galien haussa les épaules, prit sa part et fit passer la miche. Il avait l'habitude des mauvais repas avalés tout en marchant. Mais il avait hâte que cela finisse. Il n'avait plus de haine pour ses adversaires, pour en avoir partagé le quotidien durant quelques années. Ils n'étaient pas mieux traités, et ne possédaient rien qui puisse en faire les démons que le seigneur Hugues évoquait. Rashid al-Nasir ne s'était jamais montré pire officier que ceux qu'il avait subis depuis, en territoire franc. Il se contentait donc de suivre les ordres, de protéger ses compagnons, dans l'espoir, bien mince, qu'il arriverait un jour à connaître une vie moins âpre. ❧

### Notes

Le projet de parler d'un soldat qui irait servant un camp puis l'autre m'est venu il y a très longtemps, après avoir lu dans un ouvrage, dont j'ai oublié la référence, le récit d'un templier qui avait déserté la chrétienté après la chute des états latins à la fin du XIIIe siècle. Il faisait du négoce sur les rives de la mer Noire, après s'être converti à l'Islam et avoir épousé une femme.

Je me suis toujours dit que les liens sociaux plus distendus et moins denses qu'en Europe pouvaient favoriser ce genre de ruptures dans les habitudes. Nos ancêtres médiévaux possédaient dans l'ensemble la même capacité d'adaptation que nous et les circonstances pouvaient ainsi mener à de bien étranges destins. Y compris pour ceux qui risquaient leur vie les armes à la main, soi-disant pour défendre un idéal. Comme souvent, c'était au final bien plus couramment pour assurer leur propre survie et celle de leurs camarades les plus proches que pour promouvoir des causes plus larges.

### Références

Asbridge Thomas, *The creation of the Principality of Antioch 1098-1130*, Rochester : The Boydell Press, 2000.

Asbridge Thomas, Edgington Susan B., *Walter the Chancellor's The Antiochene Wars*, Aldershot : Ashgate Publishing Limited, 1999.

Barrois Claude, *Psychanalyse du guerrier*, Paris : Presses Universitaires de France, 1993.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Modus vivendi
-------------

### Casal de Salomé, demeure d'Abu Qasim, début d'après-midi du youm al joumouia 14 muharram 551[^153]

Le ra'is du casal avait proposé au shaykh Abu Mahmud de partager son repas, car il espérait avoir ainsi le temps de se faire une idée de l'état d'esprit du vieux avant les palabres plus officiels. Ils avaient de nombreux points à voir pour la gestion de la communauté, que ce soit l'organisation des pâtures, la préparation des récoltes ou l'attribution des terres pour l'année qui suivrait. Contrairement aux Latins qui se concentraient chacun sur leur propre parcelle, les autochtones exploitaient la zone en commun, chacun ayant droit à une part proportionnelle à ses apports et son travail. La répartition était toujours délicate, orchestrée par les doyens du village, dont Abu Mahmud était le plus respecté représentant.

Abu Qasim était pour sa part le plus fortuné, et celui qui devait parler en leur nom à tous au seigneur du lieu, le bouillonnant Robert de Retest. Et il serait bientôt temps de lui verser la taxe qui les frappait en tant que non-chrétiens. Comme beaucoup d'autres redevances, dont celles sur la production agricole, elle était exigée pour la communauté et chacun devait y contribuer selon un complexe assemblage d'évaluations personnelles de sa richesse propre et de souci de tenir son rang. Ne pas payer ce qu'on estimait correspondre à sa place dans le casal était insultant, débourser plus était injuste.

C'était un des impôts que beaucoup regardaient, dans le fond, comme le plus dur à supporter, car il était réclamé en monnaies. Et celles-ci étaient fort rares dans le village. Il fallait s'arranger pour faire du commerce, forcément limité vu les nombreuses interdictions qui entravaient leurs initiatives. La plupart des pièces étaient acheminées puis distribuées dans le casal par le biais du négoce au long cours que le ra'is était quasiment le seul à exercer. Ce qui renforçait d'autant plus son pouvoir et nourrissait une part de la détestation qui pouvait naître envers lui. S'il ne daignait pas vous acheter un produit ou l'autre dans l'année, vous risquiez de ne pas pouvoir vous acquitter de votre contribution à la taxe.

Abu Mahmud s'efforçait d'éviter que les ressentiments n'explosent en colère et cherchait toujours à trouver une façon acceptable par l'honneur de chacun pour que les monnaies soient là où il le fallait au bon moment. Et celui-ci était venu.

Abu Qasim finissait de grignoter des pistaches, le visage arborant l'air satisfait de celui qui a fait un savoureux repas. Le ra'is ne se montrait jamais mesquin avec ses invités, quand bien même c'étaient les fellahs les plus indigents des lieux. C'était pour lui autant une question de politesse, de respect de son rang que de représentation. Avalant un verre de lebné, il se tourna vers Abu Mahmud, occupé à déguster une excellente pâtisserie à la semoule.

« Alors, Abu, penses-tu que nous pourrons collecter la jizyah tantôt ? Le terme en est le mois prochain, à la fin de safar.

--- Tout n'est pas prêt. Certains espèrent encore des marchés qui arrivent.

--- Il s'en trouve toujours qui ne savent s'organiser. Pourtant chacun a appris combien il aura à verser, depuis un an !

--- Il y a souvent des imprévus, et certains sont plus avisés que d'autres dans leurs choix. »

Abu Qasim ne pouvait qu'approuver à cette dernière remarque. Si la plupart des fellahs payaient leurs taxes sans trop poser de problème, la majorité des soucis venait immanquablement d'une minorité trublionne, mesquine ou imbécile.

« Je me demande parfois si nous ne devrions pas faire un exemple, que la leçon porte pour les autres.

--- La plupart ne font pas cela de mauvaise intention.

--- Mais le fait est là que je devrai, comme chaque année ou presque, m'humilier devant le sire Robert pour obtenir délai. J'en ai assez de devoir payer pour les réfractaires ! »

Abu Mahmud ne répondit rien, sachant pertinemment qu'Abu Qasim avait plus que les moyens de verser la taxe pour les quelques malheureux qu'il vilipendait. Cela lui offrait en outre l'occasion de se faire valoir auprès d'eux comme leur mécène. Il tenait ainsi, par ces dettes d'honneur, une bonne partie des familles les plus pauvres, qui n'osaient plus prendre position contre lui lors des assemblées.

« Peut-être que le sire de Retest accepterait de nous donner licence de commercer plus avant ? Nous avons de la laine, du fil et quelques denrées qui pourraient trouver acheteur, mais dans l'incapacité de quitter les environs, comment faire ?

--- Si tu penses que c'est là opportune idée, ne te prive pas de lui en parler, mais moi je ne m'y risquerai pas. »

Abu Qasim savait pertinemment qu'Abu Mahmud n'oserait jamais demander audience au seigneur du casal, il le craignait bien trop pour cela, peut-être encore plus que tous les autres fellahs. Et lui, en tant que ra'is, avait quelques privilèges qui renforçaient sa position, et dont il ne se séparerait pas volontiers. Il ne voyait personne dans le village qui puisse améliorer les choses mieux que lui ne s'y employait. Et il avait besoin de moyens pour cela. N'était-il pas le plus généreux donateur à la mosquée pour l'huile des lampes ?

### Casal de Salomé, demeure d'Abu Mahmud, soirée du youm al talata 17 jha'ban 552[^154]

Abu Mahmud était rentré de sa journée à récolter les feuilles de sumac en grognant. Sa mauvaise jambe lui faisait mal et la chaleur de l'été commençait à lui peser. Il n'avait qu'à peine eu le loisir de se rafraîchir dans la cour, puisant dans une bassine d'eau tiède pour s'en asperger le visage, que des quémandeurs se présentaient déjà sa porte. Il savait qu'avec la fin des travaux des champs, les hommes auraient de nouveau du temps pour discuter, mais il ne pensait pas que cela débuterait si tôt.

Il convia donc Ahmed et Mawdud, deux fellahs du sud du hameau à venir prendre des mezzé avec lui. Sans un mot de reproche ou un regard déplacé, sa femme se contenta de prendre acte de bouches supplémentaires, une fois de plus, à leur repas du soir. Heureusement pour elle, la communauté se montrait généreuse avec leur foyer et il n'était pas rare que des voisines lui déposent du pain, des vivres voire carrément un plat. Il demeurait qu'elle devait gérer les invités seule, ses enfants ayant tous depuis longtemps quitté leur modeste maison pour vivre leur vie.

C'était d'ailleurs ce soir la question qui amenait leurs hôtes. Une des filles de leur famille était en âge de se marier et ils avaient des cousins dans un casal peu éloigné qui pouvaient arranger l'union avec un beau parti : un aîné, dont le père possédait pas mal de têtes de bétail, chèvres et moutons, mais surtout un élevage de mules réputées. C'était là une occasion providentielle pour eux. Ils avaient amassé de quoi constituer une dot plus qu'honorable et espéraient remporter l'affaire. Pour peu que le seigneur des lieux approuve cette union en dehors de ses terres. Abu Mahmud essayait de faire comprendre la bonne façon de présenter cela à Robert de Retest pour obtenir la précieuse autorisation.

« Cela se déroulerait avec un fellah d'un autre village du sire de Retest, j'encrois qu'il ne ferait pas de souci. Il l'a déjà accepté par le passé, et on peut s'en tirer par un petit présent, en reconnaissance. Rien d'important, mais qui marque la gratitude. Mais là, il s'agit d'une seigneurie distincte...

--- Peut-être, mais cette union rejaillira sur nous, sur tout le casal !

--- Pour être Ifranj et soldat, Retest n'est pas pour autant idiot. Il sait bien que cela ne lui servira en rien, qu'une fille qui quitte son père appartient à son époux. Il préfère les voir arriver dans ses casals qu'en partir.

--- Et lui dire que nous pourrions ainsi avoir nouvelles facilités à disposer de bêtes pour travailler la terre ?

--- Il n'en croira rien. C'est trop éloigné pour que les mules puissent nous être d'usage. »

Abu Mahmud grignota quelques graines, qu'il décortiqua avec soin, prenant un moment de réflexion.

« Pour la petite de Hasan, il y a quelque temps, il a fini par concéder son accord, mais il a fallu le dédommager. Je ne pense pas qu'on pourra l'éviter.

--- Hasan avait des monnaies, qu'il avait obtenues de la ville en gardant les moutons d'un riche négociant ! Moi je n'ai rien de cela.

--- Tu as pas mal de chèvres, et quelques mesures de vigne. Ce dernier point pourrait nous aider. Les Ifranjs aiment le vin, plus encore que l'or j'ai parfois l'impression. Peut-être pourrais-tu lui proposer champart de ta récolte ?

--- C'est avec ça que je gagne de quoi payer la jizyah ! Je vais faire comment si j'y renonce ?

--- Un problème après l'autre ! D'abord, trouvons comment marier ta fille. Le reste, nous en reparlerons. »

### Casal de Salomé, mosquée, après-midi du youm el itnine 1 dhu al-qi'dah 553[^155]

Après des averses matinales, le soleil avait retrouvé sa prééminence et apportait par les fenêtres une douce lumière à l'assemblée des hommes regroupés dans la mosquée. Devant le mihrab, flanqué d'Abu Qasim qui traduisait, l'intendant de Robert de Retest avait pris place. Il était venu les voir, et en particulier les plus anciens d'entre eux, car il avait besoin de savoir quelles étaient les traditions en ce qui concernait les routes et chemins. Certains endroits demandaient de l'entretien et il convenait de faire les choses correctement.

Pour avoir conquis les terres et soumis les hommes, les Latins tenaient à ne pas s'aliéner les populations et, dans la mesure du possible, les coutumes, usages et redevances étaient perpétués, parfois adaptés. Le manque d'archives écrites et la méconnaissance de la langue faisaient qu'ils étaient souvent contraints de consulter la mémoire des locaux, confrontant les avis pour en établir une proposition qui faisait alors force de loi. Il était bien sûr nécessaire de vérifier que les souvenirs ne se montrassent pas trop troués sur les obligations et trop étendus en ce qui concernait les droits. La moindre mise au clair était donc l'objet d'intenses palabres. L'essentiel demeurait que les nouveaux maîtres puissent assoir leur pouvoir et les redevances affiliées sans que cela requière trop d'efforts ou de violence.

Abu Qasim venait de traduire la demande sur les usages et personne n'osait prendre la parole. Nul n'avait envie d'être celui par qui une nouvelle corvée, un nouvel impôt leur serait exigé. Quand bien même il ne s'agissait que de la résurrection d'un prélèvement commodément oublié. Ce fut Rawh al-Furat, celui qui entretenait la mosquée et appelait à la prière qui fut le plus téméraire.

« Il faut lui dire qu'on ne se soucie pas des chemins. Ce sont les Bédouins qui vont et viennent. Nous, on reste au village. »

De nombreuses têtes approuvèrent. C'était une bonne entrée en matière. Abu Qasim traduisit pour l'intendant.

« Il déclare que les voies ne sont pas le fait des fellahs, qui vivent attachés à la terre.

--- Je sais bien cela, mais il demeure que des routes existent, entre les cités, et qu'il faut bien en prendre soin. Qu'exigeait-on d'eux par le passé pour cela ? »

Abu Qasim traduisit, ajoutant à la fin un commentaire de son cru.

« Il ne sert à rien de nier l'évidence. Il y a le sentier de l'oued, qui est bien façonné sur son flanc, qu'il a déjà parcouru maintes et maintes fois. Et les murets qui conduisent moutons et chèvres en dehors des cultures...

--- Nos pères ont bâti cela pour eux. Là c'est différent, il espère que nous allons aider à établir routes caravanières ! Qu'y gagnerons-nous ? Nul chamelier ne viendra jusqu'ici, pour aller où ?

--- Moi je veux bien aller suer pour eux, mais contre dirhems brillants ! s'enthousiasma un autre. Et avec de bons outils de fer... »

Comprenant que les tractations commençaient, l'intendant interrogea d'un regard son interprète.

« Ils déclarent qu'aucune coutume ne les a jamais obligés à casser la pierre et renforcer les chemins. Ils n'ont aucun souvenir de cela. Nos sentiers sont suffisants pour nos ânes et nos biques.

--- Dis-leur que cela va changer. Il est de juste pratique en nos provinces que pareilles corvées soient prescrites.

--- Je le leur ai signifié, mestre Heimon. Mais ce sont des fellahs, tout juste bons à semer le grain et traire la chèvre. Peu de place en leur crâne pour y mettre quelque nouvelle idée. »

Abu Qasim marqua une pause, faisant mine de réfléchir tout en tendant une oreille à ce qui s'exprimait dans la salle. Il profitait de ce que le ton montait pour prendre un air inquiet.

« Ne leur laissez pas imaginer que vous les voyez comme esclafs, ce serait faire insulte à leur condition d'hommes libres.

--- Je n'ai jamais dit pareille chose, s'offusqua l'intendant. Chez nous, vilains doivent corvées, voilà tout.

--- Je vous connais assez pour ne pas croire cela de vous, mais c'est un point... chatouilleux pour eux. »

Pendant ce temps, les échanges avaient continué entre ceux qui estimaient qu'ils n'avaient rien à faire des sentiers et des routes qu'ils n'emprunteraient jamais et un autre groupe qui ne considérait pas d'un mauvais œil l'idée de gagner quelques monnaies, ou, au pire, de solides outils.

« Voyez, ils s'agitent, car certains pensent que vous avez désir de changer leurs usages, ce que d'aucuns, dont je fais partie, ne peuvent croire. M'est avis qu'il serait plus avisé de ne pas insister sur ces demandes.

--- Il faudra bien que les chemins soient tenus en bon état !

--- Je vous l'ai dit, ce sont de simples fellahs, attachés à leurs traditions. Mais ce sont aussi des hommes laborieux, que motiverait un dédommagement, j'en suis certain.

--- Que voulez-vous dire ?

--- Si vous pensez pouvoir distraire quelques piécettes et de quoi armer leurs mains ouvrières, je me fais fort d'en convaincre d'accepter cette corvée. Même si je sais la tâche malaisée, cela me semble de mon devoir au service du sire de Retest. »

### Château de Salomé, grande salle, après-midi du youm al had 19 ramadan 554[^156]

Agité comme à son habitude, Robert de Retest allait et venait sur la petite estrade où se tenait la chaise où il était sensé tenir audience. Il recevait en fait toujours debout, dépensant son trop-plein d'énergie par d'incessants déplacements qu'il ponctuait de coups de la badine qui ne le quittait plus guère depuis quelques années. Devant lui, aussi impassible d'un récif au plus fort de la tempête, Abu Qasim répondait à ses questions, avec un flegme étudié, né d'années de pratique du tonitruant chevalier.

« Je sais que c'est le moment de repos pour vous autres ! Et avec la fin des travaux des champs, je n'ai nul désir de voir mes fellahs s'évaporer comme neige au soleil ! Penses-tu que j'ignore que certains complotent, organisent la fuite... Mais je ne lâcherai rien ! Nul ne peut disposer de mon bien à sa guise ! »

Il regarda danser sa baguette un moment, y déversant son trop-plein de pression.

« S'il me faut fouetter tous les dos des casals, je le ferai, crois-m'en ! Il n'est d'étalon trop fier pour supporter la selle ! »

Fatigué par l'achèvement de cette longue diatribe, où il tentait de faire comprendre au ra'is de Salomé qu'il avait intérêt à mettre fin aux évasions avant qu'il ne soit forcé de sévir plus durement, il se laissa tomber dans son siège. Abu Qasim devina qu'il pouvait désormais s'exprimer, avec toute la componction servile qui flattait cet homme violent et autoritaire.

« J'ai retransmis vos ordres et mandements avec zèle et précision, sire Robert. Mais le fellah a de l'âne l'entêtement et le mauvais caractère, loin de la noblesse de l'étalon.

--- À quoi me sers-tu donc, alors ?

--- J'ai usage de ces gens, sire. Sauf à faire utile exemple qui-ci qui-là, les frapper trop durement ne serait guère efficace. Il faut les mener fermement, mais sans qu'ils se sentent contraints.

--- Ai-je l'air d'un pâtre qui gère un troupeau ?

--- Certes pas ! Vous êtes le souverain de ces terres, et c'est à moi que revient cette tâche. Je sais comment tirer l'huile de l'olive, en la pressant peu à peu. Il ne serait d'aucune utilité de la frapper d'un marteau, n'est-ce pas ? »

Robert de Retest grogna. Il n'aimait pas que les choses lui résistent, et encore moins devoir envisager de changer de méthode. Cela faisait des années qu'il punissait lourdement les captifs, n'hésitant pas à recourir à des mutilations, parfois sévères. On s'en était même ému à la Haute Cour, ce qui lui déplaisait au plus haut point. D'un geste impatient de sa badine, il fit comprendre à Abu Qasim de disposer. Il était temps pour lui de porter son message, qu'il lui délivra d'une voix sèche, née de sa colère froide.

« Que les choses soient claires : si d'aucuns s'aventurent à quitter mes terres encore cet hiver, ma punition sera féroce sur tous les résidents. Envers toi le premier ! Dis-le-leur à tous ! »

Lorsqu'il salua une dernière fois en se retirant de la salle, Abu Qasim soupira. Il était de plus en plus las de ces petits jeux entre le seigneur franc et les villageois. Il avait été jusqu'à présent habile à éviter les coups, d'un côté ou de l'autre, mais ne se faisait guère d'illusion sur l'amitié qui lui serait manifestée s'il perdait son statut. Pourtant, depuis des années, il ne ménageait pas sa peine pour faire en sorte que la vie dans le casal soit la meilleure possible, cherchant à épargner les fellahs et satisfaire un maître exigeant, comme ils le sont tous.

Il ne décolérait pas des fugitifs qui le mettaient ainsi en porte-à-faux auprès du seigneur latin. Cela d'autant plus qu'il estimait lâche de leur part d'abdiquer. Il était né à Salomé, comme ses pères avant lui. Jamais rien ne pourrait le décider à l'abandonner ! Ni un pouvoir inique venu de par delà les mers ou des plaines orientales, ni l'entêtement imbécile de quelques esprits chagrins qui se faisaient manipuler par une poignée d'imams avides d'influence. ❧

### Notes

Les autochtones des territoires du Moyen-Orient ont vu défiler nombre de conquérants au début du second millénaire. Malgré cela, les populations demeurèrent majoritairement en place, se contentant de verser les impôts à un nouveau dirigeant sans que leur quotidien en soit bouleversé. Les administrations qui se succédaient devaient pour leur part s'étendre sur des zones où un certain nombre d'usage avait été validé par le temps. Il était toujours délicat de faire table rase, d'autant plus que les moyens militaires étaient rarement suffisants pour permettre de déployer des forces de police internes en plus des armées levées contre les ennemis extérieurs. Cela a assurément amené à des situations qui, pour conflictuelles qu'elles fussent, devaient se dénouer sans complètement aliéner un parti ou l'autre. Et, bien sûr, au centre de ces articulations de pouvoir se trouvaient des hommes faisant interface.

Le terme de jizya, qui désigne normalement la taxe de capitation due par les non-musulmans en terre musulmane, est ici indûment utilisé par des musulmans autochtones pour nommer l'impôt de même type qu'ils devaient en territoire latin. Je n'ai néanmoins pas retenu l'aspect infamant qui se rattachait lors du versement, dont je n'ai découvert aucune trace. J'ai plutôt choisi de l'envisager comme un prélèvement habituel, à payer par la communauté complète et pas par les individus.

### Références

Ellenblum Ronnie, *Frankish rural settlement in the Latin kingdom of Jerusalem*, Cambridge, Cambridge University Press, 1998.

Ellenblum Ronnie, « Settlement and society formation in crusader palestine », in Thomas Evan Levy (dir.), The Archaeology of Society in the Holy Land, 1995, p. 502‑511.

Weulerse Jacques, *Paysans de Syrie et du Proche-Orient*, Paris, Gallimard, 1946.

La lettre cachée
----------------

### Sidon, château seigneurial, veillée du vendredi 12 février 1160

Avec la nuit était venue une tempête plongeant la cité dans les ténèbres. Le vent violent du large hurlait sur les reliefs et fracassait les vagues sur les rochers, ajoutant les embruns salés au déluge déversé par les nuages épais. Bien au chaud près du feu dans la grande salle, Gilbert goûtait le plaisir d'une soirée tranquille, à jouer aux échecs contre Renaud, le cadet du seigneur des lieux. C'était le membre de la famille avec lequel il avait le plus sympathisé. Guère plus âgé que lui, Renaud avait choisi de suivre la voix des armes, mais n'avait pas négligé les études pour autant. Il lisait parfaitement, écrivait superbement, et connaissait beaucoup d'auteurs qu'il pouvait citer de mémoire.

Sa curiosité l'avait poussé à s'intéresser à des sujets que n'aurait pas considéré un clerc, et il interloquait souvent Gilbert par ses remarques, qu'il lançait d'un air narquois. Le jeune prêtre avait été surpris de découvrir que sous des abords assez repoussants, une belle âme pouvait se cacher. Car si le Seigneur l'avait doté d'un esprit brillant, il ne l'avait guère avantagé au niveau physique. Tout en lui était disgracieux, et si ce n'était le soin qu'il portait à sa tenue et à ses attitudes, on l'aurait volontiers classé parmi les manants les plus ordinaires.

Renaud déplaça un de ses évêques et s'empara de son verre, laissant le loisir à Gilbert de réfléchir.

« J'ai lu ce que vous m'avez fait passer tantôt, mon père. »

Gilbert leva les yeux des pièces, avide de savoir ce que le jeune bachelier en aurait à dire. C'était certainement le plus cultivé des gens de son entourage depuis qu'il avait quitté le milieu clérical pour devenir chapelain.

« La forme est en bien tournée et il s'y trouve bien sages conseils, à n'en point douter.

--- Grand merci !

--- Il y a néanmoins quelques raffinements que j'y grefferai. Si cette missive vient si loin de l'est, il faudrait y ajouter détails précis, que l'esprit soit nourri des merveilles de ces terres, propres à enflammer l'imagination. Il y a là quelques jolies formules, aptes à édifier le plus mécréant des souverains, mais il y manque quelque saveur orientale pour que ce soit achevé. »

Gilbert était un peu mitigé. Il avait espéré des louanges ininterrompues, susceptibles de flatter son ego, mais se sentait aiguillonné par la perspective d'améliorer encore ce qu'il estimait être un travail majeur. Même s'il était nourri avant tout du labeur d'Herbelot Gontheux, c'était lui qui avait trouvé la forme épistolaire adéquate.

« À quoi pensez-vous ?

--- Eh bien, quand j'ai appris à lire et écrire, un de mes maîtres m'a beaucoup transmis sur les récits de ces terres : les histoires moins connues de jadis, les légendes qui sont contées lors des veillées chez les fellahs et au cœur des cités musulmanes, ce genre de chose.

--- Il est question d'édifier selon de doctes principes chrétiens, ne serait-ce pas y mêler trop de fantasmagories ?

--- Ce sera à vous de trancher, c'est votre lettre, mon père. Mais j'encrois qu'il y manque ce qui fait la force des épopées d'ici, dont on sent qu'ils sont nés voilà bien longtemps, ressassés au fil des siècles pour en affiner le poli et le brillant. Si jamais vous en avez le souhait, je vous présenterai à mon vieux précepteur. Il demeure toujours à Sayette[^157]. »

Sans être complètement certain de son désir d'orientaliser son récit, Gilbert était curieux de rencontrer la personne qui était à l'origine de ce savoir si peu orthodoxe. Formé de façon très conservatrice dans un monastère bénédictin tout à fait traditionaliste, il avait pris conscience depuis quelques mois qu'il était totalement inculte en dehors de quelques domaines bien circonscrits. Lui qui était persuadé d'être un érudit et un lettré, il était presque vexé d'être ainsi mis en défaut sur ce qui était censé le caractériser. Sachant qu'il s'attaquait à une tâche qui serait très critiquée, il en repoussait sans cesse la formalisation définitive et la diffusion publique, inquiet de se voir moquer en raison d'un insuffisant travail de conception et de réalisation.

Il déplaça sans trop y penser un de ses rocs, amenant un sourire gourmand sur le visage de Renaud. Gilbert ne s'attarda pas à cette manifestation de joie qui était parfois feinte chez son adversaire, redoutable manipulateur dès lors qu'il s'agissait de s'affronter à un jeu ou l'autre. En aucune circonstance Renaud ne reculait devant un stratagème pour remporter la victoire.

« Je vous remercie, ce serait volontiers, j'ai toujours plaisir à encontrer mes aînés, surtout quand ceux-ci sont lettrés. Est-il issu de quelque prestigieuse école ? Laon, Orléans, Paris peut-être ?

--- Oh, rien de tout cela, s'amusa Renaud. Il a fréquenté des maîtres, peut-être à l'ombre des cathédrales, protégé par un évêque ou l'autre, mais certainement pas élève de Thierry de Chartres ! Il est juif, un de leurs doctes, nommé rabbin par ceux de sa foi. »

En disant cela, il observa attentivement Gilbert, semblant espérer quelque surprise dont il appréciait toujours la manifestation chez autrui lorsqu'il s'exprimait. Il ne fut pas déçu et osa un sourire satisfait, quoique sans ironie.

« Ne craignez rien, il n'a jamais cherché à me vanter sa pratique. C'est un homme trop sage pour cela, quoique bien trop sentencieux pour moi. À n'en point douter un père insupportable et un guide spirituel certainement fort strict. Mais il n'était que mon professeur pour apprendre la langue des gens d'ici, ainsi qu'améliorer mon écriture. Il m'a ainsi fait découvrir nombre de récits merveilleux dont je n'avais pas idée. »

Gilbert hocha la tête. Il était régulièrement désarçonné par l'apparente insouciance du jeune chevalier, qui confessait en plaisantant avoir été soumis à l'enseignement d'un maître juif. Il y avait certainement là occasion à quelque pénitence, Gilbert n'en doutait pas. Ces gens étaient connus pour leurs pratiques magiques, souvent impies. Ce qui renforçait d'autant plus leur attrait pour les esprits curieux...

### Sidon, demeure de maître Benjamin, après-midi du mercredi 9 mars 1160

Gilbert avait été presque déçu quand il avait finalement décidé de se risquer à rencontrer maître Benjamin, le précepteur de Renaud. C'était un homme entre deux âges, la barbe grise assez soignée, au maintien assez raide, dont l'élocution était rendue difficile avec la perte de nombreuses dents. Sa vision n'était plus si bonne, qu'il ne quittait plus guère son cabinet de travail. Il avait donc reçu le chapelain avec respect dans cette petite pièce sentant la poussière, encombrée de feuillets et de parchemins, à peine éclairée par une fenêtre orientée au nord.

Un jeune apprenti aussi silencieux et discret qu'un novice leur avait servi de quoi boire et avait disparu sans une parole du vieux maître. Celui-ci s'était installé sur un suffah fatigué, après avoir invité son hôte à faire de même à côté de lui. Autour d'eux, sur des étagères, de nombreux volumes se présentaient à portée de main. Gilbert se demandait combien d'entre eux contenaient des savoirs interdits ou blasphématoires. Il était à la fois grisé et inquiet de se trouver là.

« Le jeune Renaud m'a appris que vous étudiez les terres orientales, c'est cela ? Quelque ouvrage érudit en préparation ?

--- C'est cela. Il pensait que vous auriez connoissance de ces sujets, propres à compléter mes recherches.

--- Si je peux vous être utile, ce sera avec grand plaisir. Avez-vous copie des récits d'Eldad ?

--- Ce nom ne me dit rien, non.

--- Il me faudrait un jour traduire son périple en latin, que cela soit mieux connu. Il narre le destin des tribus perdues d'Israël, loin au levant. Si vous étudiez ces provinces, cela pourrait vous être de bon usage. Si cela vous convient, je peux vous en faire récit, j'en ai une copie ici même si vous avez besoin de précisions ou de détails. Nous pourrons à tout moment nous y référer. »

Gilbert accepta avec enthousiasme. Si le texte n'en avait pas été établi en latin, il était fort probable que nul parmi ses pairs ne le connaissait. Il allait accéder à une nouvelle source d'information qu'il serait le premier à dévoiler aux savants et lettrés.

« Peut-être pouvons-nous esquisser déjà quelques grandes notions sur ces terres. Peut-être en prenant le Sambatyon comme frontière ?

--- C'est là nom inconnu à mes oreilles...

--- Ah, dans ce cas, nous allons devoir progresser pas à pas, car c'est à ce fleuve de pierre que se trace la limite avec les provinces orientales. Laissez-moi vous montrer... »

Les heures qui s'ensuivirent furent un véritable ravissement pour Gilbert. Il avait l'impression de fouler de ses pieds des chemins inexplorés, des terres vierges qu'il pourrait faire découvrir à tous. Il y avait là formidable matière pour situer la retraite d'un docte moine, au fait de toutes choses. Il se disait que le placer dans un royaume soumis à l'autorité des Juifs allait mettre en valeur son enseignement, lui qui avait à subir le joug de païens. Il saurait trouver les bons mots et les phrases qui toucheraient le cœur des souverains chrétiens. Et les précisions géographiques nouvelles renforceraient sans nul doute la puissance de ses révélations.

Alors qu'il rentrait au château, indifférent aux badauds qu'il croisait, Gilbert reprenait de tête différentes parts de son récit, réarrangeait certaines formules, embellissait de mille détails sa narration. Il était impatient de coucher tout cela sur papier, d'en faire un miroir des princes qui servirait à édifier les futurs monarques. Renaud avait eu raison, son travail allait grandement s'enrichir des apports de maître Benjamin.

### Sidon, château seigneurial, fin d'après-midi du lundi 27 juin 1160

Lorsque le jongleur Remiggio passait sur la côte, il ne manquait jamais de proposer une veillée aux habitants du château. Il y savait la table excellente et on ne lui avait jamais refusé la possibilité de tendre sa toile pour y présenter quelques spectacles, plus ou moins édifiants selon le public. S'il réservait les récits les plus épiques et religieux à la grande salle, ce qu'il racontait pour la foule des domestiques et des servants était souvent plus licencieux, voire graveleux.

Il avait pourtant veillé à se mettre en bons termes avec le chapelain, Gilbert, comme il le faisait dans chaque maison noble. Il savait combien les ecclésiastiques pouvaient rapidement lui causer des problèmes, voire lui faire interdire l'accès. Il n'hésitait donc jamais à évoquer les quelques années où lui aussi avait porté la tonsure cléricale. Avec Gilbert, cela avait été extrêmement aisé, le jeune homme n'étant guère vindicatif, amoureux de musique et peu sensibilisé aux questions théologiques les plus épineuses.

Il n'était pas rare qu'ils passent du temps tous les deux, à pratiquer sur tous les instruments dont ils disposaient, comparant des accords et des enchaînements. Ils parlaient parfois également poésie, quoique sur ce point, Remiggio fasse attention à ne pas froisser les oreilles, fort chastes, du prêtre. Il se contentait donc souvent de son registre le plus honorable. Il savait aussi que Gilbert appréciait fort tout ce qui concernait les terres lointaines, les animaux fabuleux et les peuplades les plus étranges et ne manquait jamais une occasion de lui rapporter ses moissons de récits merveilleux.

« Ce soleil frappe si fort qu'il ne plairait qu'au fénix !

--- Qu'est-ce donc ?

--- Nous n'en avons jamais parlé ? C'est incroyable oiseau, dont on dit qu'il n'en existe de plus beau. Après avoir vécu cent années, il vole au plus près de l'astre du jour pour s'enflammer.

--- Que voilà triste fin !

--- Oh, mais cela ne s'arrête pas ainsi. Il s'empresse alors de rejoindre son nid et tombe en cendres. De là, un ver en naît, qui devient à son tour fénix au bout de quelque temps. »

Les yeux de Gilbert s'écarquillèrent de plaisir.

« C'est assurément fort admirable oisel. Dont on pourrait tirer bien des sermons édifiants !

--- De certes !

--- En as-tu déjà vu ?

--- Un mien compère à moi, Basam, dit qu'ils demeurent loin au Levant. Ils seraient apparentés aux Rokhs selon lui.

--- Les Rokhs ? Peux-tu m'en dire plus sur eux aussi, tu les as évoqués ce tantôt, mais sans trop en narrer. »

Remiggio maîtrisait l'art de maintenir son public dans une perpétuelle attente, à dévider ainsi lentement son récit. Il n'ignorait pas qu'en cette occasion, cela lui permettait de profiter du séjour un peu plus longtemps, justifiant sa présence par ses échanges avec le clerc. Il prit donc un air concentré, faisant mine de chercher dans ses souvenirs, lui qui avait une mémoire sans défaut.

« Il y a tant de choses sur eux que je ne saurais par où commencer. Peut-être pourrais-je confier cette tâche au marin qui les a le plus fréquentés ? Si cela te sied, je pourrai narrer à la veillée les incroyables et merveilleux voyages d'as-Sindibādu, un navigateur qui n'a connu son pareil depuis des siècles. »

### Jérusalem, palais royal, fin de matinée du samedi 16 juillet 1160

Les célébrations pour la capture de la ville venaient à peine de s'achever et une foule immense était encore présente dans les rues, s'attardant sur les traces des conquérants autant que sur celles du Christ et des apôtres. Peu à l'aise dans une pareille multitude, Gilbert profitait du moindre prétexte pour se réfugier dans les enclaves tranquilles, jardinets et cours intérieurs, qui fleurissaient dans les plus grandes demeures.

Il avait eu le plaisir de rencontrer Hermann, un clerc formé dans les mêmes couvents que lui. Il était venu pour une mission diplomatique majeure, disait-il, sans s'étendre dessus. Il était néanmoins fréquemment en audience avec le roi et ses proches et logé au palais, signe de son importance. Malgré sa réserve toute bénédictine, il s'était montré assez chaleureux avec Gilbert, heureux de retrouver quelqu'un qui lui rappelait ses jeunes années. Il avait appris au prêtre la récente disparition de leur ancien abbé, Wibald de Stavelot.

En quelques semaines, ils étaient devenus inséparables, prenant plaisir à se remémorer les lieux dont leurs sandales avaient foulé les dalles. Seulement, Gilbert était là pour lui faire ses adieux. Son seigneur repartait dans ses terres et il devait le suivre. Il avait préparé quelques lettres, dont la plus conséquente s'adressait à la famille de sa bienfaitrice, qu'il continuait d'honorer par des messes régulières. Mais, en cette ultime rencontre, il avait surtout espoir d'obtenir le retour d'un véritable érudit sur ses travaux.

« Avez-vous eu le loisir de parcourir mes lignes ? J'aurais grand plaisir à savoir ce que vous en pensez.

--- Cela m'a pris quelque temps, mais oui, j'ai tout lu. Je dois dire que j'ai été assurément impressionné par le propos. »

La phrase se déversait dans les oreilles de Gilbert comme de l'ambroisie l'aurait fait dans sa gorge.

« J'ai d'ailleurs une proposition à vous faire, mon ami. Je suis ici mandé par le roi des Romains, mais je suis avant tout un proche du noble évêque de Freising, Othon, dont vous avez sans nul doute entendu parler. Il est fort intime de l'empereur, depuis des années, et c'est un homme aussi savant que sage. »

Hermann s'arrêta un instant, comme hésitant à confier un secret, puis poursuivit.

« De grandes choses peuvent advenir, mon ami. Mais il faut pour cela que les puissants s'accordent, afin de bouter hors les mécréants de ces terres. L'empereur du Saint-Empire, celui des Griffons[^158] et Sa Sainteté le pape. Et votre étude peut aider à cela, j'ai ai certeté.

--- Mon modeste travail ? Le croyez-vous si bon ?

--- Et comment ! Savoir qu'un grand monarque chrétien ne demande qu'à unir ses forces aux nôtres pour broyer entre marteau et enclumes les païens, cela serait considérable nouvelle, propre à soulever l'enthousiasme.

--- Mais cela n'est que supposition de ma part, ce sont surtout pieux conseils assemblés pour guider un prince.

--- Et quoi donc, ne m'avez-vous pas dit que vous avez consulté les meilleurs érudits d'ici ? Appuyé votre texte sur de hautes autorités ?

--- Si, mais... »

Gilbert hésitait. Il avait était enthousiasmé par les ajouts qu'il avait fait, persuadé que cela servait son propos, mais jamais il n'avait envisagé que son récit puisse être pris au pied de la lettre. Il était étonné qu'Hermann puisse le recevoir ainsi. Ce dernier perçut son embarras, sans forcément en comprendre l'origine et réfléchit un moment avant de briser le silence une nouvelle fois.

« Il demeure certains points que nous ne pourrions souffrir, comme ces royaumes juifs, qui ne peuvent être, car cela serait contraire à toute tradition établie. Il faudra donc le corriger, mais en dehors de ce détail d'importance, rien n'interdit d'imaginer que vous décrivez toutes choses existantes.

--- Mais jamais le prêtre Jehan n'a écrit cette lettre !

--- Qui serait assez fol pour le croire ? C'est là simple mise en forme qui permet de faire comprendre le plus subtil aux plus obtus, artifice de narration, rien de plus. La vérité emprunte parfois de bien tortueux chemins pour se manifester. Je sais qu'un évêque a, par le passé, déjà parlé de ce prêtre à mon sire. Vous avez recueilli tout ce qui se raconte à son propos, peu ou prou réalité, ou du moins ce qui en est parvenu jusqu'à nous. C'est là œuvre capitale, guidée par la volonté de Dieu sans doute aucun. »

Gilbert respira un grand coup. Jamais il n'avait envisagé que son travail puisse ainsi trouver sa place dans le monde, en si magnifique position. Il n'avait écrit tout cela qu'en suivant son inspiration pensait-il, en un hommage à une personne appréciée tout d'abord, puis comme divertissement intellectuel par la suite. Se pouvait-il que, ce faisant, il ait accompli un dessein supérieur ? Était-il vraiment le créateur de tout ceci ? Cette idée lui donnait le vertige : son œuvre allait le dépasser en tout. ❧

### Notes

Ce Qit'a constitue le dernier auquel je me consacre après avoir passé plus de dix ans à rédiger des textes se déroulant dans le XIIe siècle des Croisades, concluant un cycle en un récit qui en dévoile un des arcs narratifs les plus discrets, mais essentiels : la diffusion culturelle. La lettre du prêtre Jean est un des sujets qui m'a toujours fasciné et j'ai eu l'ambition d'en proposer une genèse qui en explique la complexité narrative et référentielle, la polysémie et les lectures variées possibles. Je ne crois pas qu'une pareille création ait pu voir le jour ex nihilo et sa richesse vient à mon avis aussi de l'investissement que certains ont voulu y mettre a posteriori, plaquant ainsi leurs desseins, peut-être parfois contradictoires avec l'intention préalable.

J'ai malheureusement dû brusquer un peu les choses pour ne pas laisser cet arc en suspens. J'aurais préféré pouvoir poursuivre sa lente diffusion au fil des Qit'a, année après année. Seulement la réalité matérielle du travail littéraire fait que je ne peux plus m'offrir le luxe de continuer cette série. Cela me demande trop de temps et d'énergie, tout en n'assurant que bien trop peu de revenus. Je laisse donc à d'autres le soin d'étendre cet univers s'il s'en sentent l'envie. Je souhaitais juste clarifier les choses sur mes aspirations sur le long terme avant de lâcher l'affaire, au moins pour un moment. Chacun en fera ce qu'il veut, ainsi qu'Hermann.

Enfin, car il y a toujours de nombreuses intentions entremêlées dans un écrit, le titre est bien évidemment une référence directe au texte d'Edgar Poe, « La lettre volée », qui était si bien cachée en étant exposée à tous, comme mon ambition de passeur de culture à travers Hexagora. Ceux qui verraient dans ce Qit'a une mise en abyme de ce qui me semble être le travail d'un poète pourraient bien être dans le vrai.

### Références

Bar-Ilan Meir, « Prester John : Fiction and History », History of European Ideas, 1995, XX, num. 1‑3, p. 291‑298.

Blake Denton, « The Quest for Prester John », Vexillum, 2012, vol. 2, p. 203‑219.

Rouxpetel Camille, « La figure du Prêtre Jean : les mutations d'une prophétie. Souverain chrétien idéal, figure providentielle ou paradigme de l'orientalisme médiéval ? », Questes, 2014, vol. 28, p. 99‑120.

Taylor Christopher Eric, « Waiting For Prester John: The Legend, the Fifth Crusade, and Medieval Christian Holy War » Master of Arts, University of Texas, Austin, 2011.

[^1]: Vêtement byzantin à ouverture frontale boutonné devant, mais pas ouvert jusqu'en bas.

[^2]: Balcon en encorbellement qui surplombe la porte.

[^3]: Vêtement byzantin à manches ouvertes au niveau des épaules.

[^4]: Association de marchands de la mer du Nord et de la mer Baltique.

[^5]: Frédéric Ier de Hohenstaufen, dit Frédéric Barberousse (1122 -- 10 juin 1190), empereur romain germanique, roi des Romains, roi d'Italie, duc de Souabe et duc d'Alsace, comte palatin de Bourgogne.

[^6]: Shayzar, sur l'Oronte, Syrie.

[^7]: Large siège rembourré, parfois à dos et baldaquin, qui a donné le mot sofa.

[^8]: Plat byzantin à base de boulettes de viande.

[^9]: Sucrerie à base de miel et de graines de sésame.

[^10]: Al-Hākim bi-Amr Allah (19 octobre 996-19 février 1021), calife fātimide du Caire qui persécuta un temps les chrétiens et fit, en particulier, détruire le Saint-Sépulcre de Jérusalem.

[^11]: Voir Qit'a *Traditions*.

[^12]: Arménien.

[^13]: Le roi Baudoin Ier de Jérusalem.

[^14]: Voir le troisième tome des enquêtes d'Ernaut, *La terre des morts*.

[^15]: Balcon en encorbellement qui surplombe la porte.

[^16]: Bénédicité populaire en latin, « Bénissez-nous, Seigneur, ainsi que la nourriture que nous allons prendre, par Jésus-Christ Notre-Seigneur. », les présents répondent alors « Amen », c'est-à-dire « Ainsi soit-il. ».

[^17]: *Le mariage est consommé*.

[^18]: Voir le second tome des enquêtes d'Ernaut, *Les Pâques de sang*.

[^19]: Le roi Baudoin Ier de Jérusalem.

[^20]: Voir le troisième tome des enquêtes d'Ernaut, *La terre des morts*.

[^21]: Question 190 du pénitentiel de Burchard de Worms : « As-tu mangé la nourriture des Juifs ou des autres païens, qu'ils ont préparée pour eux ? Si oui, tu feras pénitence dix jours au pain et à l'eau ».

[^22]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^23]: Chapeau de feutre à fronton triangulaire, un des symboles du statut de guerrier chez les Turcs.

[^24]: Terme usuel pour désigner les Grecs, c'est à dire les Byzantins.

[^25]: Voir le premier tome des enquêtes d'Ernaut, *La nef des loups*.

[^26]: Voir Qit'a *La main du destin*.

[^27]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^28]: Région dominée par Damas.

[^29]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^30]: vendredi 15 juillet 1099

[^31]: *al-Khwarizmi* signifie originaire du Khwarezm, région perse située au sud de la mer d'Aral, en Ouzbékistan actuel.

[^32]: Évangile selon Luc, chapitre 6, verset 20 : « Heureux vous qui êtes pauvres, car le royaume de Dieu est à vous ! »

[^33]: Balcon en encorbellement qui surplombe la porte.

[^34]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^35]: Aujourd'hui Aqaba, en Jordanie.

[^36]: Bertrand de Blanquefort a été capturé en juin 1157 par Nūr ad-Dīn lors de la bataille aux abords du lac de Houla, voir *Chétif destin*.

[^37]: Shayzar, sur l'Oronte, Syrie.

[^38]: Bohémond III d'Antioche (1142/49 - 1201), prince d'Antioche, considéré mineur jusqu'en 1163. Renaud de Châtillon, son beau père, époux de sa mère Constance, assura la régence jusqu'en 1160.

[^39]: Formation de combat équestre.

[^40]: Aujourd'hui Harim, Syrie.

[^41]: Lundi 12 janvier 1159.

[^42]: Autre façon de désigner l'émir Nūr ad-Dīn.

[^43]: Jeudi 22 janvier 1159.

[^44]: Sourate XXIII, dite sourate des Croyants, verset 50. Il y est fait allusion à une colline qu'on disait être celle de Damas.

[^45]: Mardi 3 février 1159.

[^46]: Jeudi 12 mars 1159.

[^47]: « La victorieuse », Le Caire.

[^48]: Large robe aux manches amples.

[^49]: « Maître », en l'occurrence, responsable d'un des secteurs du marché.

[^50]: Pièce de réception.

[^51]: Ascalon.

[^52]: Le Caire, *al-Qahira* (la victorieuse). C'est aussi le nom donné à l'Égypte en général.

[^53]: Nom arabe de Jérusalem.

[^54]: Nom latin de Naplouse, aujourd'hui Nablous (arabe) ou Chékhem (hébreu), à environ 50 kilomètres au nord de Jérusalem, en Palestine.

[^55]: Région dominée par Damas.

[^56]: Lundi 21 mars 1160.

[^57]: Importante ville d'iRan occidental, sur les pentes de l'Alvand, à plus de 1800 mètres d'altitude. C'était alors la capitale seldjoukide.

[^58]: Aujourd'hui Ispahan, en Iran.

[^59]: Dimanche 10 juillet 1160.

[^60]: Plat à base de navet.

[^61]: Samedi 3 septembre 1160.

[^62]: La savate est considérée comme un élément impur, frapper quelqu'un avec constitue une insulte grave, surtout au visage.

[^63]: Large siège rembourré, parfois à dos et baldaquin, qui a donné le mot sofa.

[^64]: En droit lmusulman, cela désigne une rente perpétuelle fait à une institution charitable, pieuse ou publique.

[^65]: Pièce de réception des riches demeures.

[^66]: Pièce 141 des Poèmes d'Omar Khayyam (v. 1048 - 1131), savant, mathématicien, astronome, poète et philosophe. Traduction de Franz Toussaint.

[^67]: Ou doogh, boisson à base de lait fermenté, d'eau et de menthe séchée.

[^68]: De la ville d'Édesse.

[^69]: Raymond de Poitiers, prince d'Antioche (v. 1098-1149). Ses relations avec le comte d'Édesse Josselin II furent souvent très tendues, voire carrément hostiles.

[^70]: Le roi de France Louis VII, 1120-1180.

[^71]: Allusions aux événements de la seconde Croisade, qui a décidé d'aller assiéger Damas au lieu de porter assistance aux territoires du Nord, comme c'était espéré lors de l'appel.

[^72]: Nom communément donné à la ville d'Édesse.

[^73]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^74]: Mu'īn ad-dīn Anur (ou Unur) ( ? - 1149), principal dirigeant de l'état bouride de Damas de 1135 à sa mort en 1149.

[^75]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^76]: Terme usuel pour désigner les Grecs, c'est à dire les Byzantins.

[^77]: Communément appelé tell bashir, c'est aujourd'hui Tilbeşar en Turquie.

[^78]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^79]: Titre donné à l'empereur byzantin.

[^80]: Voir les Qit'a *Chétif destin* et *Liaisons dangereuses*.

[^81]: Fait prisonnier en même temps qu'Eudes de Saint-Amand, vers Panéas.

[^82]: Onfroy II de Toron (v.1117-v.1179), connétable du royaume de Jérusalem.

[^83]: Aujourd'hui Harim, Syrie.

[^84]: Renaud de Châtillon (v. 1120-1187), prince d'Antioche de 1153 jusqu'à sa capture en 1160.

[^85]: Régent.

[^86]: Large robe aux manches amples.

[^87]: Antienne accompagnant dans la messe le rite de la bénédiction et de l'aspersion d'eau bénite.

[^88]: « Je vous donne l'un à l'autre au nom du père, du Fils et du Saint-Esprit. »

[^89]: « Que tous deux parviennent au repos des élus et au royaume des cieux, et qu'ils voient les enfants de leurs enfants jusqu'à la troisième et quatrième génération. »

[^90]: « Bénis, Seigneur, cette chambre. Fais que tous ceux qui l'habitent soient établis dans ta paix, et que dans ta volonté ils demeurent, vieillissent et se multiplient pour une longue suite de jours. »

[^91]: « Que Dieu bénisse vos corps et vos âmes, et répande sur vous sa bénédiction comme il a béni Abraham, Isaac et Jacob. Amen. »

[^92]: « Que la main du Seigneur soit sur vous, et qu'il envoie son saint ange vous protéger tous les jours de votre vie. Amen. »

[^93]: L'Oronte.

[^94]: Compagnon.

[^95]: Contraction de *bec-jaune*, très jeune personne.

[^96]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^97]: Région dominée par Damas.

[^98]: Allusion à la Seconde croisade, en 1148-49, où croisés français et allemands, Latins de Terre sainte, se rejetaient la faute de l'échec du siège de Damas.

[^99]: Administration royale des finances, sous l'autorité du sénéchal.

[^100]: On emploie le terme *viandes* pour désigner la nourriture de façon générale.

[^101]: Traduction littérale de *Carolus Magnus*, nom latin de Charlemagne

[^102]: Impropriété dérivé du titre de sultan, que les Latins utilisaient souvent de façon générale pour désigner les princes musulmans.

[^103]: Pâtisserie du Levant, à base de pâte farcie généralement de dattes, de pistaches ou de noix. Des versions salées, avec du poisson, existent.

[^104]: Jeune chevalier n'ayant pas encore de fief.

[^105]: Voir la trilogie de Qit'a *Pour une poignée de dinars*, *Pour quelques dinars de plus* et *Impitoyable*.

[^106]: Voir les Qit'a *Poing de trop* et *Désert*.

[^107]: Aden, au Yémen, port naturel ouvrant sur le golfe éponyme.

[^108]: Région dominée par Damas.

[^109]: En arabe, littéralement « *Pays des noirs* », qui désigne alors les terres au sud de l'Égypte.

[^110]: Antioche, en Turquie.

[^111]: Désignes les *Romains*, c'est à dire les Byzantins.

[^112]: Nom arabe de Hébron, en Cisjordanie.

[^113]: Littéralement *Le propre*.

[^114]: Plat traditionnel de la région de Gaza, en Palestine. Il est préparé à base de sumac, de tahin, de pois chiches et de viande, le tout assaisonné d'épice, d'ail et de piments. On peut le manger froid.

[^115]: Ascalon.

[^116]: Mercredi 11 septembre 1163.

[^117]: Meneur de la danse.

[^118]: La ville, au sud-ouest du comté d'Édesse, constituait la dot de la jeune femme.

[^119]: Balak ibn Bahram ibn Ortok (? - v.1125). Atabeg d'Alep en 1124-1125, c'était un officier turc de premier plan, de la famille qui a gouverné Jérusalem et la Palestine.

[^120]: Proverbe arménien.

[^121]: Appelé Turbessel par les Latins, c'est aujourd'hui Tilbeşar en Turquie.

[^122]: Désignes les *Romains*, c'est à dire les Byzantins.

[^123]: Matthieu d'Édesse (seconde moitié du XIe siècle - 1144) était abbé d'un monastère près de Kaysun. Il a écrit une *Chronique* qui constitue une source de repmière importance pour l'histoire de cette région. Il était très critique envers les Byzantins et les Latins.

[^124]: Le roi Baudoin Ier de Jérusalem (v.1060 - 2 avril 1118), comte d'Édesse puis roi de Jérusalem.

[^125]: Ce sont les Byzantins qu'on appelle alors Romains, comme ils se nomment eux-mêmes.

[^126]: Terme usuel pour désigner les Grecs, c'est à dire les Byzantins.

[^127]: Région dominée par Damas.

[^128]: Tripoli, actuellement au Liban.

[^129]: Le Caire, *al-Qahira* (la victorieuse). C'est aussi le nom donné à l'Égypte en général.

[^130]: Désormais utilisé pour désigner un mélange d'épices, le terme désignait selon les régions une herbe aromatique ou l'autre : l'hysope au Moyen-Orient ou l'origan au Maghreb.

[^131]: Foulque d'Anjou, dit le jeune, (v.1092-1144) devenu roi de Jérusalem par son mariage avec Mélisende en 1131.

[^132]: Joscelin II, comte d'Édesse (vers 1110-1159).

[^133]: Saint Bernard, qui fut un des grands promoteurs de la Seconde croisade.

[^134]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^135]: La régence.

[^136]: Shayzar, sur l'Oronte, Syrie.

[^137]: En 1161, Baudoin III choisit d'écarter sa cousine de la régence d'Antioche. Comme sa mère Alice, la sœur de Mélisende, avant elle, Constance était une femme entreprenante, cherchant à diriger la principauté en toute indépendance.

[^138]: Hodierne (v. 1110-v. 1164), fille du roi Baudoin II de Jérusalem et de Morfia de Malatya. Elle épouse en 1131 Raymond II de Toulouse comte de Tripoli.

[^139]: Très violent vent de poussière venu des terres désertiques.

[^140]: Chrétien syriaque soutenant les positions christologiques byzantines, souvent de milieu urbain. Les chrétiens syriaques orthodoxes, appelés jacobites, étaient alors plus nombreux en milieu rural.

[^141]: Terme utilisé pour désigner les Latins.

[^142]: Monastère Saint-Pancrace, vers Hamersleben près d'Halberstadt, en Allemagne.

[^143]: Henri V, empereur du Saint-Empire (11 août 1086-23 mai 1125).

[^144]: « De l'unité et de la trinité divine, ou Théologie du Bien suprême », vers 1120.

[^145]: *Diaeta imperii* en latin, *Reichstag* en allemand. Assemblée peu formelle des souverains composant l'empire germanique, et qui se réunissait selon les besoins.

[^146]: de Langres.

[^147]: L'impératrice Irène, épouse du basileus Manuel Ier Comnène était en fait la belle-sœur du roi Conrad. Ce dernier en avait fait sa fille adoptive pour rehausser son statut lors des négociations de mariage avec Byzance.

[^148]: Adhémar de Monteil, légat du pape pour la première croisade en en 1096.

[^149]: « Vous vous occuperez de la cause des évêques et des affaires des rois // Qui vous préparent à la richesse et au plaisir ».

[^150]: Un sauf-conduit en cas de reddition.

[^151]: Aujourd'hui Meshmeshan, dans la région d'Idlib, en Syrie.

[^152]: Vendredi 10 septembre 1154.

[^153]: Vendredi 9 mars 1156.

[^154]: Mardi 24 septembre 1157.

[^155]: Lundi 24 novembre 1158.

[^156]: Dimanche 4 octobre 1159.

[^157]: Aujourd'hui Sidon, au Liban.

[^158]: Terme usuel pour désigner les Grecs, c'est à dire les Byzantins.
