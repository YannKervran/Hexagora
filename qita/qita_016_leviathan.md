---
date: 2013-03-15
abstract: 1157. La Terre sainte était nommée l’Outremer à l’époque médiévale et ses côtes accueillaient les plus anciennes cités marchandes de Méditerrannée. Elle n’était pas néanmoins qu’une route et elle effrayait autant qu’elle était utile. De nombreux mythes en sont nés. Alors qu’Ernaut découvre les tâches de sergent, il se retrouve dans la cité de Jaffa pour une mission d’escorte. Les circonstances lui donnent l’occasion de prendre une revanche sur les flots impitoyables.
characters: Ernaut, Gaston, Jonas
---

# Léviathan

## Port de Jaffa, matin du mercredi 27 novembre 1157

Installées sur la plage auprès des débarcadères, plusieurs silhouettes suivaient du regard le haut navire qui venait de mettre à la manœuvre. Avec sa voile immaculée et sa coque peinte, cinglant parmi les frêles canots de pêcheurs et les lourds vaisseaux de commerce, la galère avait fière allure. Elle longeait pour l’heure la côte, attendant d’avoir dépassé les rochers d’Andromède avant pour pouvoir s’engager au large. À son mât flottait la bannière royale, aux croix d’or sur fond d’argent, ondulant sous les assauts du vent. L’ambassade était finalement en route pour la capitale byzantine, Constantinople. Parmi les curieux qui observaient le navire, le plus petit des deux se tourna vers son gigantesque compagnon.

« Allons donc en la citadelle, Ernaut, rendre compte au sire comte de la bonne partance du connétable et de l’archevêque.

— Ne devons-nous pas rentrer au plus tôt ?

— Amaury a peut-être quelque valet prêt à bondir sur le dos de rapide coursier. Nous ne saurions nous mesurer à pareil messagier. Et puis, il me semble que le sire frère du roi a le droit d’apprendre cela. »

Le jeune homme haussa les épaules. Il avait constaté que parmi les sergents au service du souverain, chacun avait ses lubies, ses habitudes, et que les affrontements au sein du royaume, quelques années plus tôt, avaient laissé des blessures, des préférences, des rancœurs. Gaston, un boiteux entre deux âges, à la voix rauque, aux bras épais et aux mains semblables à des pattes d’ours, avait un faible pour le frère cadet du roi Baudoin, qui était pour l’heure comte de Jaffa. Il ne s’en était jamais expliqué, mais on avait dit à Ernaut qu’il avait été un temps au service de la vieille Mélisende, la reine mère, désormais retirée à Naplouse. Peut-être avait-il approché le jeune noble alors qu’il n’était qu’un enfant et en avait gardé une certaine affection à son égard.

Pour l’heure, il suivait des yeux le navire qui glissait sur les vagues, ombre brune découpée sur le ciel d’azur et les nuages cotonneux.

« On va tâcher de se mettre quelque chose dans le col en chemin, j’ai grand-soif de ce départ aux matines. Un grand gamin comme toi doit toujours avoir large place pour y fourrer bonne pitance, non ? »

Disant cela, il flatta son compagnon d’une bourrade et commença à avancer parmi les caisses et les sacs, les barques tirées sur la plage, les filets en plein ravaudage. Comme tous les matins, une activité intense agitait l’endroit, qui durerait jusqu’au retour des pêcheurs et au départ de tous les navires au long cours.

Ce n’était plus la saison maritime, et Jaffa n’était pas un grand port : s’il était possible d’y débarquer dans la baie protégée par les récifs d’Andromède, la faible profondeur obligeait à tout transborder sur des barges, jusqu’aux pontons. Les vaisseaux demeuraient ensuite à l’ancre un peu en retrait. C’était certes un endroit stratégique, car il ouvrait la voie terrestre directe pour Jérusalem, mais il n’avait jamais pu concurrencer des haltes commerciales comme Césarée, un peu plus haut sur la côte ou Ascalon, en direction de l’Égypte ou, surtout, Saint Jean d’Acre, plus au nord.

C’était là que la plupart des pèlerins et la majeure partie des marchandises parvenaient au royaume de Jérusalem. Gaston ronchonna en recevant du sable soulevé par la course de gamins excités qui les dépassèrent en riant. Les deux sergents longeaient les murailles de la ville, moins forte de ce côté peu exposé, pour rejoindre la porte de la Mer, qui permettait d’accéder au faubourg par le quartier pisan.

Ernaut n’était au service du roi que depuis quelques mois et ne se voyait pas confier de tâches importantes, ce qui lui convenait parfaitement. Escorter des voyageurs, porter des messages : encadrer la levée des cens[^cens] constituaient la majeure partie de son travail. De temps à autre, certains des hommes étaient convoqués pour servir au sein de l’ost[^ost], lors des campagnes de Baudoin III[^baudoinIII], mais la plupart du temps, c’était pour des activités subalternes.

Cette fois, ils avaient accompagné une ambassade qui se rendait auprès du basileus Manuel Comnène pour négocier une épouse au jeune roi. Une telle politique matrimoniale permettait de resserrer les liens entre territoires chrétiens. Après les exactions à Chypre de la part du prince Renaud d’Antioche, il était plus que nécessaire de se rapprocher des Byzantins.

Dans les rues, tout le monde commentait les événements récents : les tremblements de terre du nord, signes de colère divine contre leurs adversaires, l’arrivée du grand baron, Thierry d’Alsace comte de Flandre, la maladie qui frappait leur ennemi à tous, Nūr ad-Dīn. Toutes ces nouvelles emplissaient d’allégresse le cœur des habitants du royaume de Jérusalem. Les deux sergents partageaient cet enthousiasme et déambulaient, le sourire aux lèvres. Une odeur forte leur assaillit les narines alors qu’ils passaient aux abords de tonneaux où des gamins mélangeaient des oeufs de morue, du sable et de l’eau de mer.

« Ça sent la sardine par ici, garçon, lança joyeusement Gaston. On va s’en offrir quelques-unes sur du pain à l’huile, ça te dit ? »

Joignant le geste à la parole, il se dirigeait vers un appentis où deux femmes faisaient griller du poisson, qu’elles glissaient dans du pain généreusement arrosé d’aromates. Â côté, un adolescent proposait du vin tiré du tonneau qu’il couvait comme une mère poule.

Plusieurs hommes, portefaix, vieux pêcheurs, ouvriers de marine, discutaient aux abords tout en prenant une pause. On y commentait plus le temps et les conditions de mer que la situation politique du royaume. Depuis plusieurs années, Jaffa était dans une région abritée, et ses récifs la protégeaient généralement des attaques navales. La vie s’y déroulait désormais paisiblement.

Quelques mouettes criaient, perchées sur l’enceinte ou planant au-dessus de leurs têtes, dans l’attente de quelque nourriture à chaparder. À côté de claies, des enfants brandissaient des feuilles de palmier pour effrayer les insolents volatiles qui venaient s’en prendre au poisson mis à sécher. Le vent du large était frais, mais peu vigoureux, et parvenait à peine à chasser les odeurs salines entêtantes, additionnées du goudron des navires et de la sueur des hommes. Des cloches commencèrent à sonner dans la ville, appelant les moines astreints à la prière des heures[^heures]. Avalant des bouchées ambitieuses, barbouillant leur menton de sauce, Ernaut et son compagnon passèrent la voûte de l’enceinte sous le regard indifférent du soldat de faction, occupé à musarder avec une jeune femme.

## Abords de la chapelle Saint-Nicolas, soir du mercredi 27 novembre 1157

Installés à l’abri d’une dune, en contrebas d’un taillis d’épais buissons épineux, les deux sergents profitaient des derniers rayons du soleil. La journée avait été encore chaude, et ils accueillaient la brise marine avec reconnaissance. D’où ils étaient, ils voyaient la cité de Jaffa agglutinée sur les pentes, enserrée dans ses murs d’où dépassaient les tours fortes, les clochers élancés. Au plus haut du relief, l’imposante demeure de pierre du comte, Amaury, semblait défier la mer. Les chevaux paissaient aux abords, attachés à une corde tendue entre deux palmiers. Au-delà, en retrait de la route qui menait à Arsuf, une petite bourgade de misérables maisons de pêcheurs s’étirait, tenant compagnie à une chapelle flanquée d’un modeste cimetière. Les embarcations tirées sur la plage, entre lesquelles jouaient quelques gamins dépenaillés, s’alignaient sur le sable.

Allongés sur leurs couvertures, les deux sergents profitaient de la vue magnifique du ciel se teintant d’orange et de vermeil alors que le soleil disparaissait dans les nuages lointains. Ils avaient soupé de houmous et de brochettes de poulet, et avaient encore un peu d’eau coupée de vin dans leurs gourdes de peau. Leur mission achevée, ils n’avaient plus qu’à retourner à Jérusalem, porteurs de plusieurs messages à destination des différentes administrations.

Gaston n’aimait guère demeurer enfermé entre quatre murs depuis, disait-il, une mauvaise expérience de siège. Il avait donc proposé à Ernaut de profiter du temps durablement beau en cette fin d’année pour dormir à la belle étoile. Le jeune homme, qui n’appréciait rien tant que se baigner, avait accepté avec grand plaisir. Il n’avait d’ailleurs guère tardé à aller délasser ses pieds et ses mollets dans les vagues avant de s’allonger, le dos réchauffé par le sable encore tiède du soleil d’après-midi. Il commençait à s’assoupir, bercé par le ressac lorsqu’une voix éraillée le tira de sa rêverie.

«Vous ne devriez pas demeurer hors les murs en cette nuit, je ne serai guère étonné qu’un fort vent se lève d’ici peu. »

Gaston se tourna et avisa leur interlocuteur : un vieil homme à la tenue fatiguée, d’épaisse toile de coton grise. Les manches maintes fois reprisées et rapiécées apportaient des touches de couleur surprenantes dans sa mise fort modeste. Il était accompagné d’un petit chien au poil ras et sombre. Lorsqu’il s’approcha, Ernaut put voir qu’il était tonsuré. Sûrement un des serviteurs de l’édifice religieux, peut-être le curé en charge de la paroisse.

« Voyez ces lourds rideaux sombres qui avancent sur le large. Ils vont venir sus nous, j’en suis quasi acertainé !

— Vous connaissez bien la mer ?

— Pas tant… Mais fort bien cette côte, et j’y ai vu souvente fois arriver tempêtes du large.

— Pourtant la soirée est fort claire et bien agréable.

— C’est là danger, surtout pour les marins demeurés sur les navires à l’ancre. »

Il se tourna vers le hameau d’où pointait le modeste clocher.

« La cité aura clos ses portes, vous pouvez donc vous abriter en la nef si le coeur vous en dit. J’ai usage d’héberger voyageurs. »

Ernaut le dévisagea, avec sa tenue abîmée et sa triste allure.

« Vous êtes le prêtre du lieu ?

— Certes non, répondit-il dans un rire. Je ne suis que pauvre clerc à son service, à peine capable de déclamer *credo*, *pater* et *ave*[^prieres]. Le père curé réside en la ville de Jaffa, je garde l’église pour ma part. »

Le vieil homme vint jusqu’à eux, les mains sur les hanches, admirant l’endroit comme s’il en était le maître. Il pointa l’horizon, plissant les yeux pour mieux en percevoir les détails.

« Voyez les raies sombres au loin, il y a là forte pluie au large, et le vent la rabat sur nous. »

Gaston s’était relevé et hochait la tête.

« Il y a là certes belle tempête qui obscurcit la vue. J’espère que les nôtres ne l’auront pas subie.

— De qui parlez-vous ?

— De la _Gloria_, une galée royale qui navigue au nord.

— Nulle crainte pour elle, là ce grain monte depuis les côtes sud. »

Le vieux clerc sourit.

« Vous devriez aussi attacher vos montures en la cour de l’église, elles y seront plus rassurées. J’y tiens déjà un âne, qui saura leur tenir compagnie et les calmer. »

Ernaut s’était accoudé et regardait, un peu inquiet la tempête qui lui paraissait si loin. Cela lui rappelait de mauvais souvenirs, les journées enfermé dans la cale à subir les assauts des vents et des vagues, tandis que le Falconus le menait en Terre sainte[^voirt1].

« Vous êtes certain qu’elle va venir à nous ? »

Le vieil homme s’esclaffa et montra du doigt les canots retournés qui jalonnaient la plage.

« Quand bien même j’aurais doutance, je peux m’en remettre à la sagesse des hommes de cette côte. Puis, reprenant un air grave, il ajouta : cette nuit ne sera guère reposante, amis, je vous le garantis ! »

## Plage de Jaffa, nuit du mercredi 27 novembre

Les vagues grondaient en se fracassant sur la plage, le vent de tempête projetait les embruns jusque sur les dunes. Les nuages compacts avaient masqué la lune, plongeant la nuit dans une noirceur d’encre. Les hommes criaient, vociféraient, mais leurs voix bien frêles face au déchainement des éléments paraissaient dérisoires. Parmi les flots, quelques silhouettes s’agitaient, abattues par les crêtes écumeuses, renversées par les rouleaux hurlants. Ernaut était de ceux-là.

Assuré autour de la taille d’une corde tenue par une ligne de villageois sur la plage, il tentait d’avancer vers les débris et les vestiges du navire. Un peu plus tôt, il avait été réveillé par le vieux gardien de l’église : un bâtiment dont les amarres avaient été rompues par la tempête était en difficulté. Le temps que les habitants se rendent sur la côte avec quelques lampes, il ne subsistait du vaisseau qu’une épave. Rapidement, les pêcheurs, habitués à lutter avec les flots, avaient organisé les secours.

Indifférent au danger, Ernaut s’était proposé pour aller parmi les vagues tenter de sauver les hommes tombés à la mer. Tandis qu’il avançait, tantôt battant des pieds et des mains, tantôt marchant, il écartait les planches arrachées, les sacs déchirés, les lambeaux de la cargaison et du navire. Il hurlait à pleins poumons, espérant se faire entendre des matelots ballotés dans les flots. Souvent, ce qu’il prenait pour une tête, un corps évanoui, ne se révélait que pitoyable débris, fragment de tonneau ou de coque.

Lorsqu’on le traînait sur la plage, à force de bras, il y découvrait les restes entassés par les vagues, les ballots qui avaient survécu et flotté jusque-là. Les femmes se chargeaient de les tirer vers les dunes, tout en accueillant les marins sauvés des eaux. Ce ne fut qu’à sa troisième tentative qu’Ernaut parvint à agripper un poignet. La panique du naufragé était telle qu’il faillit les noyer tous les deux à se débattre comme un forcené. Le bras puissant du colosse s’enroula autour de son torse et il le hâla vers la côte où ils se trainèrent tous les deux, épuisés par l’effort. On les porta à l’écart des flots, et des mains secourables les protégèrent du froid avec une épaisse couverture.

Un des pêcheurs hurla à Ernaut qu’il devrait prendre un peu de repos à son tour, mais le jeune homme refusa et repartit dans les vagues. Il était en compte avec les mers et espérait bien cette fois-ci en sauver autant qu’il le pourrait.

Au petit matin, le vent était retombé. Une clarté hésitante mangeait le ciel du côté de la plaine, saluée par le chant joyeux des oiseaux, indifférents à la détresse de la nuit passée. Parmi un petit groupe, Ernaut était assis près d’un feu, une couverture sur les épaules, appuyé contre le muret d’un jardin. Depuis la butte, il voyait la plage qui s’étendait en arc de cercle jusqu’à l’éminence où se tenait Jaffa. Des mâts de navires dansaient à l’horizon, dans la rade.

Il regarda autour de lui : sur le sable, entassés pêle-mêle, des marchandises, des fragments du bateau, des morceaux de cordage, des caisses éventrées, gisaient. Un des hommes expliquait que cela serait rendu aux affréteurs. Puis il ajouta, en souriant à demi :

« Sauf si le roi se met en tête de s’emparer de cela !

— Pourquoi ferait-il cela ? s’étonna Gaston, lui aussi épuisé par la nuit passée à s’activer.

— Je ne saurais dire, mais il l’a déjà fait, voilà quelques années de cela. Il a tout pris à un de ces émirs dont le navire avait été drossé sur la côte. Je peux te dire qu’il y avait là bien moults choses, et pas tant abîmées car le navire s’était d’abord échoué[^voirscriptamanent]. »

Ernaut haussa les épaules. Les agissements des puissants n’étaient pas de son ressort. En outre, il était au service du roi, il ne se sentait donc pas en verve pour en deviser, après une pareille nuit. Il fixa ses mains, couvertes d’ecchymoses, et entreprit d’en arracher une écharde.

Il leva les yeux quand le vieux gardien s’approcha, une miche de pain sous le bras. Il en trancha plusieurs généreux morceaux qu’il accompagna d’une portion de fromage sec sorti d’une besace. Distribuant la pitance, il commenta les événements et confirma qu’il y avait six rescapés, et que seuls deux matelots avaient donc été perdus. Les hochements de tête, emplis de lassitude, se satisfaisaient de cet honorable accomplissement. Lorsque le vieil homme reprit le chemin de la chapelle, où les naufragés étaient hébergés, Ernaut se leva et le rejoignit.

« Je dois repartir tantôt pour la sainte Cité, pensez-vous qu’ils souhaiteraient que je porte message à leur parentèle ou à un associé ?

— J’en doute fort, ils ne sont pas du royaume.

— D’où sont-ils alors ?

— Ils viennent d’Alexandrie. »

Le jeune homme ne cacha pas son étonnement. Les Égyptiens étaient ennemis de Jérusalem depuis des dizaines d’années, leurs navires écumant les côtes comme de vulgaires pirates.

« Que faisaient-ils ici ?

— Des marchands, des marins… »

Le vieil homme haussa les épaules et laissa mourir sa voix. Puis il toussa et fit face à Ernaut.

« Étrange que nous ayons choisi de risquer nos vies pour sauver ceux-là mêmes que nous combattrons peut-être demain, n’est-ce pas ? »

Ernaut demeura muet, troublé par la question.

« Le Léviathan est notre ennemi, mon garçon, pas l’homme. *Sa vue seule suffit à terrasser. Il devient féroce quand on l’éveille, nul ne peut lui résister en face*[^job1]..

— Le Léviathan ? » s’interrogea Ernaut qui ignorait le terme.

Le vieil homme s’approcha de lui et lui tapa sur la poitrine d’un index impérieux.

« Le monstre qui peut tout dévorer, né des mers féroces et qui, chassé de son refuge abyssal par les navires des hommes, a trouvé refuge en chacun de nous ! »

Puis il reprit son chemin, un sourire mystérieux sur les lèvres. Ernaut l’interpella alors qu’il tournait au coin de l’église.

« Tu ne m’as même pas dit ton nom, l’homme.

— Jonas, on me nomme Jonas ! » ❧

## Notes

La cité de Jaffa, malgré un port peu accessible pour les navires à fort tirant, et ses dangereux écueils que la légende antique rattachait à Andromède, était au départ de la route qui, par la plaine de Sharon, filait droit en direction de Jérusalem, distant de cinquante-huit kilomètres. Sa possession était donc stratégique, et les Latins s’en emparèrent rapidement lors de la première Croisade. Érigé en fief, le comté fournissait en tout cent chevaliers, en faisant le premier territoire au service de la couronne. De lui dépendait Ascalon, Rama, Ibelin et Mirabel.

La navigation maritime, essentielle pour le commerce et l’afflux de colons et de combattants européens pour le royaume de Jérusalem, se tarissait pendant les mois d’hiver. Du cabotage devait subsister, mais les navires s’éloignaient habituellement peu des ports. Et même là, il arrivait parfois que des tempêtes emportent de nombreuses vies.

Bien qu’aucun texte législatif ne soit connu avant le règne d’Amaury (roi de 1163 à 1174), il existait des coutumes, généralement partout observées, pour protéger les avoirs de ceux qui voyaient leurs biens perdus en mer. C’est donc avec surprise qu’on peut lire le récit d’Usamah ibn Munqidh, le célèbre guerrier poète et diplomate, qui raconte s’être fait déposséder lors d’un naufrage par le roi de Jérusalem. Il y laissa, écrit-il, une bibliothèque forte de plusieurs milliers d’ouvrages.

La légende d’Andromède est parfois reliée à des mythes plus anciens, phéniciens, et pourrait se situer dans la tradition des combats contre des monstres comme celui de saint Georges, lui aussi originaire du Moyen-Orient. Nul étonnement à ce qu’on prête à la mer, grande dévoreuse d’hommes, les attributs d’une telle créature.

Dans la Bible, on rencontre l’histoire de Jonas qui eut, lui, la bonne fortune de s’en sortir vivant. Malgré cela, la tradition marine attache à ce nom la malchance.

## Références

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008, p.43-44.

De Mas Latrie Louis, « Les comtes de Jaffa et d’Ascalon du XIIe au XIXe siècle », dans *Revue des Études Historiques*, juillet 1879.
