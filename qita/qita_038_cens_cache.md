---
date: 2015-01-15
abstract: 1157. Servir un roi puissant et détenir un certain pouvoir peut inciter à certains abus. Droart ne voit aucun mal à améliorer l’ordinaire en rendant de menus services à qui en a besoin. Tout le monde y est gagnant au final, selon lui.
characters: Bertaud, Droart, Grifon de Tripoli, Kaspar, Père Josce
---

# Cens caché

## Jérusalem, soirée du vendredi 1er mars 1157

Les cloches conventuelles sonnant vêpres finirent par se taire, laissant place à un silence fatigué au sein de la ville sainte. Le père Josce traînait les pieds, allant et venant avec ses cruches à la main, qu’il déposait sur de rudimentaires tables à tréteaux. Une poignée de consommateurs affalés sur les bancs regardaient vaguement le spectacle offert par deux jongleurs sans grâce qui gesticulaient plus qu’autre chose.

La salle se développait autour d’un massif pilier central, rongé de salpêtre, griffé par les ans et les visiteurs. Des lampes à graisse apportaient lumière chiche et fumée épaisse au lieu, maculant les plafonds de traînées noires. Droart était assis là, les jambes tendues, faisant glouglouter le vin dans sa bouche. Malgré l’aspect peu engageant, la taverne offrait des crus de qualité correcte à des prix accessibles. En outre, son abord repoussant faisait qu’elle n’était pas trop fréquentée par les voyageurs, ce qui était plutôt un avantage en cette période de Pâques.

La ville était assaillie de hordes de pèlerins, de pieds poudreux, de curieux, de dévots, qui se déversaient dans les rues, dans les hôpitaux, le moindre local pouvant les héberger, les accueillir entre deux prières. Chaque année voyait ce flux printanier s’abattre sur Jérusalem. Les sergents tels que Droart étaient alors mis lourdement à contribution, entre la perception des cens, le contrôle des portes et la sécurité dans les quartiers.

Le calme, même sordide, de l’endroit lui offrait l’occasion de s’extraire de l’effervescence des rues, de son travail. Il aimait également sa modeste maison, mais avec les enfants et sa femme, il n’avait pas la même impression de tranquillité. Ce fut donc en grimaçant qu’il ouvrit les yeux quand il sentit quelqu’un s’asseoir face à lui.

Petit, le visage massif et les joues tombantes, l’homme paraissait âgé, avec son dos voûté et la calvitie qui ne laissait apparaître que de maigres fils de crin gris. Un pichet à la main, il souriait, offrant la vision de pauvres chicots usés par de la farine de piètre qualité.

« Mestre Droart de la sergenterie le roi ?

— Mouais. »

L’interlocuteur accentua son rictus et avança le pichet pour emplir de verre le Droart, ce que ce dernier ne refusa pas malgré un air peu engageant. Cela ne rebuta nullement l’arrivant, qui s’installa sur un tabouret.

« Faites excuse de vous aborder ainsi, mais j’ai grand besoin d’entreparler avec vous. »

D’un mouvement du menton, le sergent l’invita à continuer, tandis qu’il avalait une large goulée d’un vin fruité. Le vieux s’était fendu d’un cru de qualité. Un bon point pour lui.

« Je sais que vous êtes de ceux qui prélèvent le cens en ma rue. »

Entrée en matière irritante qui contracta les sourcils de Droart sans lui faire desserrer les dents pour autant.

« J’ai grand souci en ce moment, rapport au fils qu’a disparu… »

Le vieil homme bougeait sans cesse sur son tabouret, faisant grincer le bois et vibrer la table branlante. Une mouche agacée n’arrêtait pas de quitter son corps agité pour s’y reposer presque aussitôt.

« On a petit commerce de portage, voyez-vous… Mais je suis plus tant vaillant qu’en mes vertes années. Alors sans le fils… Et pis, je dois nourrir sa marmaille. Sa femme a le tétin sec, on doit trouver du lait pour le p’tiot…

— Vous me dites ça pour quoi ?

— Bein, dans quelques jours, je dois payer mon besant, comme à la saint Michel, la saint Jean et Noël. Mais je l’ai pas… »

Droart soupira. Encore un quémandeur qui venait pleurer sa misère pour échapper aux taxes.

« T’as de la cliquaille pour le vin quand même on dirait.

— C’est pour vous, le père Josce m’a dit que vous le goûtiez fort, celui-là. »

Droart reposa son gobelet. L’homme le dévisageait, l’air implorant, l’œil humide. Sa tenue fatiguée trahissait sa pauvreté et son teint blafard semblait indiquer qu’il ne mangeait pas à sa faim chaque jour.

« Combien qu’t’en a des mioches ?

— J’ai les cinq du fils, le dernier marche même pas, encore en langes. Et pis la mère de sa femme, une vieille qui peut avaler que du gruau. Elle a perdu toutes ses dents en venant ici.

— Je comprends ton souci, l’homme, mais j’y peux rien. Le cens, c’est pour le roi. »

L’autre hocha le menton faiblement puis, penchant la tête, ajouta d’une voix indécise :

« J’me suis dit qu’vous pourriez marquer mon cens comme payé sur vos rôles, au moins pour cette fois, ça m’aiderait bien. P’t-être que le fils va revenir dans l’été. Il était parti au nord, en Galilée avec un marchand, peu après l’Épiphanie. »

Il tendit la main et déposa une petite bourse de toile sur le plateau, l’air de rien. Droart y risqua un œil et demanda, d’un air contrarié :

« C’est quoi ça ?

— Y’a un vingt de carats là-dedans. Tout ce que j’ai pu gratter.

— Ça fait même pas la moitié. »

Le vieux acquiesça, l’air honteux.

« J’aurai pas plus d’ici Pâques, je le sais bien. Mais des fois que vous pourriez trouver que ça peut suffire. Et pis si vous trouvez façon de me rayer de la liste pour cette fois sans avoir à payer plus, vous pourrez garder la bourse pour vous. Ce serait pas souci pour moi, bien au contraire.

— Ouais, tu t’en tirerais à bon compte. Payer même pas la moitié de ta taxe, le vieux. »

L’autre baissa la tête, blessé par la remarque. Il soupira, fit jouer ses lèvres sur ses dents et se leva, l’air accablé. Il tendit la main pour récupérer la bourse, mais Droart y plaqua ses doigts avant lui.

«Attends, j’ai pas dit non, je vais voir ce que je peux faire. »

## Jérusalem, après-midi du jeudi 21 mars 1157

« Un petit chaudron de cuivre, montra Droart.

— Un petit chaudron de cuivre » répéta en marmonnant le clerc.

Assis sur un escabeau à l’entrée de la pièce, il notait scrupuleusement sur sa tablette de cire ce que Droart lui énumérait. Le bourgeois qui habitait cette propriété du roi était décédé quelques semaines plus tôt, sans aucun héritier connu. Ses possessions allaient donc être reversées à la couronne et devaient être inventoriées avant de décider de leur sort, vendues ou conservées. L’absence du maître de maison avait permis aux araignées et aux souris d’en prendre à leur aise. Dans les pots de la cuisine, de petites crottes s’ajoutaient souvent aux restes alimentaires.

Quelques beaux plats vernissés, en céramique vert et brun étaient soigneusement rangés sur une étagère. Le sergent les admira, un peu jaloux de ne pas avoir la possibilité de s’offrir si magnifique vaisselle. Le défunt était un négociant, apparemment, qui serait mort sur le chemin entre Acre et Tibériade. Un de ses compagnons de route avait rapporté la nouvelle quelques jours plus tôt, en même temps que ses paquets. Tout en fouillant dans les affaires pour ne rien oublier d’important, Droart continuait de lister tout ce qui tombait sous son regard. Un des coffres abritait des jattes en bois de belle taille.

L’une d’elles, au couvercle tiré, semblait de bonne contenance, qu’il estima à deux mines de grains. Elle était étonnamment pleine, alors que le marchand devait savoir qu’il s’absenterait un moment. Abandonner ainsi du froment chez soi était le plus sûr moyen d’attirer tous les nuisibles du quartier. Esquissant un sourire facétieux, il dégaina son couteau et planta la lame qui ne s’enfonça guère avant de heurter quelque chose.

« Le petit malin, murmura-t-il.

— Quoi donc ? Je n’ai pas compris.

— Rien, attends un instant. »

Il plongea la main dans les grains et sentit un petit sac, de ceux qui servaient à ranger des monnaies. Il fit une moue, estimant du bout des doigts la contenance et le nombre de pièces. Le clerc se leva et s’approcha, curieux.

« T’as trouvé quoi, compère ? »

Droart retira sa main et la frotta sur son vêtement. Il ne connaissait pas trop mal Bertaud, un petit gars né dans un casal des environs, formé à l’école des chanoines du Saint-Sépulcre. Il avait assisté à son mariage quelques saisons plus tôt, et, avec plusieurs collègues, lui avait offert à cette occasion un beau morceau de toile de laine pour se faire tailler une cotte. Qu’il portait ce jour même, d’ailleurs.

« Quel âge il a ton marmot, rappelle-moi ?

— Il aura tantôt une année, on le baptise pour les Pâques, la semaine prochaine. »

Droart hocha la tête doucement, hésitant encore un instant avant de se jeter à l’eau.

« Quelques cliquailles pour lui payer doux langes de lin, ainsi qu’une étoffe fine à ton espousée, ce serait pas de trop alors.

— Pourquoi tu me dis ça ?

— Parce qu’y a moyen de verser quelque lardon en notre brouet… »

Ce disant, il sortit la bourse de la jatte, la secouant pour en faire deviner le contenu. Bertaux écarquilla les yeux, stupéfié par cette apparition. Il allait parler, mais seule une bulle de salive se forma à la commissure de ses lèves. Le sergent soupira.

«Oui-da, je sais bien, mais si on prend quelques monnaies, qui s’en plaindra ? Le gars est mort, et le roi, bein il nage dans l’or, c’est un roi.

— Voler ainsi, cela va à rebours du mien serment.

— Ouais, je sais bien, mais on en prendrait qu’un peu. Pas le tout. »

Il délaça le cordon et examina le contenu, triant du doigt les pièces.

« Y’a plusieurs dinars, des argentins d’Antioche et sarrasins… Je dirais qu’il y en a pour au moins deux ou trois marcs. Bon poids. »

Le scribe se passa la langue sur les lèvres, réticent à parler. Il se gratta l’oreille nerveusement.

« Je te propose qu’on esbigne quelques dinars, mi-partis entre toi et moi. On laisse le plus gros au sire. »

Il commençait à extraire des pièces, qu’il posait délicatement dans sa paume, jusqu’à en avoir deux piles de douze.

« Voilà, on a là de quoi poivrer nos tranchoirs, l’ami. »

Il plongea son regard dans ceux de son compagnon, tendant la main vers lui. L’autre hésitait, fixant alternativement les pièces et Droart.

« T’attends quoi ? C’est pas les dix deniers du Judas que j’te propose. Nul mal pour personne, et de quoi mieux établir ton hostel. »

Tendant la main avec précaution, comme s’il l’approchait d’un piège à loups, Bertaud finit par s’emparer des monnaies. Cela fait, Droart dissimula bien vite les siennes et échappa un rire bref.

« Un pilon à broyer poivres et épices » aboya-t-il joyeusement, désignant un meuble posé là.

## Jérusalem, soirée du samedi 20 avril 1157

Droart finissait de se rincer, puisant dans un seau d’eau fraîche à l’aide d’une profonde cuiller. Échauffé par la brume ardente, il frissonnait sous la violence de la différence de température. Terminant par le visage, il évacua le trop-plein de ses yeux en les frottant vigoureusement, puis attrapa le linge qu’il avait posé sur une tringle en arrivant. Il sortit alors de la salle, ses soques de bois clapotant dans les flaques.

La pièce suivante était moins chaude, maintenue tiède, et accueillait les baigneurs qui souhaitaient se délasser avant de s’en aller. Des paillasses garnissaient des banquettes, et de nombreuses lampes à huile en verre irradiaient une lumière dansante depuis le plafond. Étant donnée l’heure tardive, il ne restait plus grand monde dans l’endroit, seulement un groupe qui discutait à voix basse en sirotant un pichet et en dégustant des sucreries.

Le sergent s’avança vers eux et toussa pour attirer leur attention. Il avait convenu de ce rendez-vous avec Kaspar, un de ses compagnons de débauche arménien. C’était un fils de commerçant qui se compromettait dans de nombreux trafics. Mais il était surtout un joyeux compère, toujours le mot pour rire et les mains dispensatrices de besants. Il accueillit Droart d’un signe enthousiaste, le présentant à un homme entre deux âges. Un peu avachi, appuyé sur un coude, il se nommait Grifon de Tripoli.

Visiblement métissé, il avait le teint olivâtre, mais les iris clairs et le cheveu, quoique rare, demeurait d’un ton châtain. S’il n’avait cette moue dédaigneuse en permanence et ses yeux tristes, il aurait pu être séduisant. Plusieurs dents qui lui manquaient sur le devant rendaient sa diction sifflante et l’obligeaient à avaler souvent sa salive. Droart s’assit sur le bord de la banquette et attrapa une boulette au sésame, prêt à entendre ce qu’ils avaient à lui dire. Kaspar lui résuma rapidement la situation.

« Grifon est bon compère à ma famille. Nous achetons et vendons l’un à l’autre et avons déjà fait contrats de partenariat ensemble. Sa spécialité, c’est le sucre, et c’est là le souci. »

L’autre acquiesça puis enchaîna.

« Avec la guerre contre l’Égypte, il est malaisé d’y acheminer mon sucre, alors que c’était là-bas que je le vendais au meilleur prix. »

Il soupira, avala une friandise comme pour se consoler.

« Il se plante moult champs de canne à sucre, et les engins à broyer deviennent légion. Nous sommes désormais si nombreux à produire que notre réputation, au comté, commence à en souffrir.

— La famille de Grifon exporte surtout du sucre sale^[Voir les notes pour la qualité du sucre.] et excellent, les meilleures qualités.

— Pas de dégoutté ou de moyen chez nous, non. On le garde pour les ventes locales » confirma le marchand.

Droart tendit un gobelet pour goûter de leur piquette, les interrompant.

« En quoi ça me regarde, tout ça ? Marchandies vont et viennent, les prix montent et descendent, c’est le lot de ceux qui s’adonnent au négoce.

— Justement, avec tout ce sucre qui se déverse comme de la Mane, difficile de le vendre au même prix qu’avant.

— Et les chemins ici sont fort mauvais, donc on passe de préférence par chameaux. La taxe se fait par tête.

— Je sais tout ça, c’est… »

Il réfléchit un instant.

« Quatre besants la tête à l’entrée, compléta l’autre. Et c’est là le souci. Avant, une charge me rapportait, bon an mal an, dans les quinze livres.

— Dites plutôt dans les vingt à vingt-cinq, maître, je connais les prix.

— Vous parlez de la vente à l’échoppe, moi je vends aux apothicaires et épiciers, pas au quidam. »

Droart hocha la tête de mauvais gré.

« Bon, même à quinze, voilà faible taxe.

— Je ne m’en plains pas. Mais voilà que désormais je peine à vendre au-delà de dix livres, avec toute cette concurrence, et la taxe ne change pas, elle ne tient pas compte de la baisse.

— Et porter par terre jusqu’ici, en chariot ? »

L’autre leva les yeux au ciel.

« Trop compliqué, pour Jérusalem. Autant me couper un bras et l’offrir aux pauvres de cette cité, j’en aurais plus grand bénéfice.

— Alors vous proposez quoi ? »

Kaspar se rapprocha et fit un sourire de connivence à Droart.

« C’est une idée mienne. J’ai par-delà les murs quelques enclos bien fermés et petite grange fort commode, en contrebas des vignobles de Saint-Étienne.

— Ouais, je vois.

— J’ai proposé à mestre Grifon qu’on réduise la taille de la caravane une fois aux abords. Difficile de fixer lourds paquets si on chemine longtemps, mais si ce n’est que pour passer la porte, on peut fixer bien trois à quatre kintars par bête.

— Et diminuer d’autant la taxe à payer, je vois. Mais personne sera dupe et on enquêtera pour savoir où sont les autres bêtes… »

Le négociant toussa et confirma d’un mouvement de tête.

« Il faut que les formes soient respectées, et si un sergent de la sainte cité aide la caravane à passer, j’aurai grande joie à l’en récompenser, avec ses compères. »

Droart avala une grande gorgée de vin, le faisant passer d’une joue à l’autre, en réfléchissant.

« Ça peut se faire, ouais. Je ne suis pas tant souvent à la porte de David, mais j’y ai quelques bons amis. Suffit juste que Baset soit pas dans le coin.

— Qui ça ? s’interrogea Grifon.

— Un mien compère de la sergenterie, bouffi de tellement de défauts qu’il ahonterait le diable lui-même. Mais y refuserait tout net de tremper là-dedans. Plus chatouilleux de son honneur qu’une pucelle de sa rondelle. Il me faudra distribuer quelques cliquailles ici et là. Vous prévoyez ça pour quand ?

— Une caravane devrait arriver vers la saint Vital[^saintvital].

— Si fait. Il y a aussi un nouveau, une sorte de Goliath à museau de goupil. Il me faut estimer d’où vient le vent avec lui. Je peux escompter combien à distribuer parmi sergenterie ? »

Grifon hésita, jeta un coup d’œil à Kaspar puis lâcha, du bout des lèvres.

« Si toutes mes bêtes passent en deça sans encombre, il y aura deux livres à vous départir.

— Dix besants ? Combien y’aura de têtes ?

— Seize avoua l’autre, à regret.

— Alors seize besants m’agréeraient plus avant. »

Grifon fit mine de calculer puis opina du chef. Kaspar en gloussa de plaisir et leva son gobelet pour un toast.

« Puissent nos ventres prospérer et nos vits joyeusement se dresser ! » ❧

## Notes

Les fonctionnaires au service d’un puissant, fût-il le roi de Jérusalem étaient appelés sergents, et tous ne faisaient pas profession de porter les armes. Une tâche essentielle des dirigeants d’alors était d’administrer un vaste patrimoine foncier, des droits et taxes, loyers et redevances. Ceux qui prêtaient serment se retrouvaient plus souvent à devoir superviser des opérations comptables qu’à dégainer l’épée. Le terme sergent est à rapprocher de celui de serviteur, et n’avait pas l’acception militaire stricte qu’on lui connaît désormais.

Malgré tout, certains hommes accompagnaient également l’armée lors de ses déplacements, mais on leur confiait habituellement toutes les corvées annexes de garde et de surveillance, avec un encadrement de professionnels expérimentés, voire de chevaliers.

La réputation de corruption des personnels intermédiaires faisait les gorges chaudes de la population, qui attribuait rarement au prince les dysfonctionnements d’un territoire, en rendant responsables la domesticité et les administrateurs. Les dirigeants, comme saint Louis un siècle plus tard, tentaient périodiquement de contrôler les exactions et abus de ceux censés les servir. Les peines infligées étaient d’ailleurs souvent assez lourdes : marques d’infamies, membre tranché et la mort en cas de récidive.

Les qualités du sucre présentées viennent de l’ouvrage d’Elyahu Ashtor, *Histoire des prix et des salaires dans l’Orient médiéval*, Paris, 1969, p.134. C’est une présentation d’une note venant des lettres et comptes issus de la genizah du Caire. En valeur décroissante, on pouvait trouver du sucre excellent, sale, moyen et dégoutté. Le prix entre le moins cher et le plus cher était de plus du simple au double. Si vous souhaitez en savoir plus sur le sujet, il existe un livre très complet sur la question : Mohamed Ouerfelli, *Le sucre. Production, commercialisation et usages dans la Méditerranée médiévale*, Leiden-Boston, Brill : 2008.

## Références

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Boas Adrian J., *Domestic Settings. Sources on Domestic Architecture and Day-to-Day Activities in the Crusader States*, Leiden et Boston : Brill, 2010.

Favier Jean, *Finance et fiscalité au bas Moyen âge*, Paris : Société d’édition d’enseignement supérieur, 1971.

Foucher Victor, *Assises de Jérusalem. Première partie : Assise des Bourgeois*, Paris: 1890.

Prawer Joshua, *Crusader Institutions*, New York : Oxford University Press, 1980.

Reinhold Röhricht, *Regesta Regni Hierosolymitani* (MXCVII-MCCXCI), Oeniponti : Libraria Academica Wagneriana, 1893.
