---
date : 2019-09-15
abstract : 1142-1158. Jeune templier ayant à peine fait ses vœux, Geoffroy découvre le royaume de Jérusalem où il rêvait d’accomplir de grands exploits l’épée en main. Et en lui s’opère un changement avec le temps, au fur et à mesure qu’il apprend à connaître cette terre qui est désormais la sienne.
characters : Geoffroy Foucher, André de Montbard, Philippe de Milly, Gaufroi Morisse, Armand
---

# Mon ennemi, mon frère

## Jérusalem, mont du Temple, après-midi du mardi 14 avril 1142

La cité s’était brusquement dévoilée au sortir de la route qui courait dans les gorges des monts de Judée depuis la plaine côtière. Énorme, solidement bâtie, elle présentait aux nouveaux arrivants son entrée la plus imposante, celle où s’adossait la forteresse. En plus de cela, elle était précédée d’un faubourg grouillant de voyageurs et d’animaux, bruissant d’activité. Geoffroy, originaire d’un petit village de Bourgogne, avait traversé ces lieux comme dans un rêve. La porte rapidement passée grâce au blanc manteau qu’il portait avec ses compagnons, il avait découvert la longue rue, bordée d’échoppes animées dont les éventaires mangeaient l’espace, qui menait vers le Temple dont il apercevait le dôme entre deux auvents, parmi les avancées des terrasses et les rawshans[^rawshan].

Lorsqu’il était enfin parvenu sur la vaste esplanade où battait le cœur de toute la chrétienté, où ses frères de la milice, porteurs de la cape blanche depuis quelques années, s’étaient installés, il avait poussé un soupir de soulagement, émerveillé du spectacle qui s’offrait à ses yeux. Autour de lui, ce n’étaient que lieux saints, prêtres et soldats de Dieu, bâtiments de la foi…

« Ne veux-tu pas descendre de ton roncin, frère ? »

La voix le ramena à la réalité. Un chevalier, habillé en civil, mais l’épée au côté, lui tenait la bride et attendait qu’il démonte.

« j’ai nom Armand, et on m’a donné pour mission de t’accueillir en nos murs.

— Grand merci, frère. Je suis Geoffroy Foucher, de Bourgogne.

— Oh, je sais fort bien qui tu es. Tu es espéré depuis long temps. Le sénéchal ne fait que deviser de toi, sien neveu, dont il apprécie tant les courriers. »

Geoffroy sourit en entendant parler d’André de Montbard, l’oncle qui l’avait convaincu de s’enrôler dans la milice du Temple. Pas tant par des discours recruteurs, mais bien plus par ses récits de vaillance, de don de soi et de guerre pour la vraie foi. Pour le jeune Geoffroy, il était l’idéal chevaleresque incarné. Bernard, l’abbé de Clairvaux qui avait tant fait pour la chrétienté, ne tarissait pas d’éloges sur ce cousin qui avait embrassé un état supérieur. Geoffroy et lui correspondaient depuis plusieurs années, et c’était André qui l’avait convaincu de demeurer dans ses terres, aux soins des cisterciens, le temps de devenir suffisamment lettré pour ne plus être considéré comme un esprit pesant par les clercs épris de culture. Il avait donc rongé son frein trois années, où il avait alterné pratiques militaires et tournois afin d’éprouver son bras et études littéraires et liturgiques pour aguerrir son intelligence. Ce ne fut qu’une fois prêt, au moment du départ pour Jérusalem qu’il avait prononcé ses vœux.

« Est-il visible ? Je n’ai guère eu l’occasion de le voir depuis si long temps !

— Il n’est pas en nos murs ces jours, mais il saura d’ici peu que tu es parmi nous, pour sûr. »

Armand invita d’un geste Geoffroy à le suivre, laissant sa monture à des valets, dont une nuée était à l’ouvrage afin de décharger les bêtes de la caravane.

« Nous allons voir tout de suite le frère drapier, que tu aies de quoi te vêtir et dormir ce soir. Nous avons un peu de temps avant la prochaine heure. »

Il fit une grimace de dépit avant de poursuivre d’un ton chagrin.

« Tu arrives malheureusement peu après la pitance. Les Pâques sont encore loin, et nous avons pris la collation de jeûne.

— Nous avons eu du pain et des olives en chemin.

— Tant mieux ! Sans cela, l’attente aurait été longue jusqu’à demain. »

Tout en échangeant, Geoffroy découvrait une enfilade de salles, de couloirs, de patios et de cours. Partout des hommes s’affairaient, discutaient, s’entraînaient. La plupart étaient porteurs d’un manteau blanc comme le sien, mais de nombreux subalternes n’étaient vêtus que de bure, de toile grossière. Armand passait d’un endroit à l’autre, indifférent à l’agitation des maçons et charpentiers occupés à agrandir et renforcer le lieu, aux serviteurs omniprésents et aux chevaliers qui déambulaient en tous sens. Au contraire, le quartier général du Temple faisait l’effet d’une ruche à Geoffroy : la forteresse d’où ils allaient essaimer afin de répandre le message du Christ. Tout au long du parcours, un sourire béat ornait son jeune visage.

« Nous avons ici tout le nécessaire, bains et fours, cuisines, dortoirs et réfectoires, forges et moult loges d’artisans. Nul besoin de sortir dans la cité. Et, surtout, nous avons des écuries… Une merveille que nul sur terre n’a pu voir depuis l’époque des Romains, pour sûr. Je te montrerai ça. Mais avant cela, il nous faut aller à la quarravane.

— Qu’est-ce donc ?

— C’est un peu comme notre maître cellier, mais pour tout ce qui n’est pas viandes. C’est là-bas que les drapiers t’équiperont. Veille à ne pas fâcher frère Remondin. Sans quoi tu n’auras droit qu’à braies percées et lame rouillée… »

Devant l’air dépité de son compagnon, Armand partit d’un grand rire.

« Je te moque, pardonne-moi ! Nous n’avons que bon harnois ici. Mais il est de fait que le frère Remondin a caractère bien trempé, comme ses glaives. Veille bien à ne jamais le contrarier ou tes oreilles pourraient bien chauffer. »

Geoffroy sourit à cette innocente plaisanterie. Il avait le cœur si léger que rien ne pouvait le rembrunir ce jour. Il était enfin parvenu à la destination de ses rêves et chevaucherait d’ici peu, lance au poing et casque en chef, à l’assaut des forces ennemies. Il sentait son sang bouillonner dans son jeune corps empli de vigueur.

## Faubourg damascène, camp du Temple, matin du mardi 27 juillet 1148

La réunion d’état-major était à peine achevée que les ordres avaient fusé : tout devait être démonté et chargé sur les bêtes au plus vite. La décision de lever le siège de la ville était prise et il ne fallait pas laisser à l’ennemi le temps de s’organiser pour harceler la colonne au départ. Des échelles de cavalerie allaient se mettre en place, appuyées de quelques fantassins, afin de patrouiller aux abords et éviter que des actions de guérilla ne viennent transformer la retraite en déroute.

Geoffroy n’avait pas été désigné pour le combat en selle, ce qu’il avait perçu comme une rebuffade personnelle, en plus de l’humiliant repli. La mine basse et le regard enflammé de courroux, il accompagnait son oncle jusqu’à sa tente afin de l’assister dans la rédaction de plusieurs courriers qui devaient partir sans délai.

Grisonnant depuis des années, le sénéchal du Temple André de Montbard était solidement bâti, la mâchoire large et les pommettes saillantes, le cheveu court et la barbe maintenue rase. Ses yeux rieurs étaient enfoncés sous des arcades proéminentes où jouaient ses sourcils mobiles. La bouche fine, comme une fente discrète, ne s’ouvrait que peu pour autre chose que le service, de Dieu ou des hommes.

« Je te vois bien en rage, neveu. Livre-moi donc ce qui te ronge le cœur. »

Geoffroy hésita un instant. Il avait énormément d’affection pour cet homme qui l’avait pris sous son aile depuis des années. Inlassable, à l’occasion autoritaire, il était malgré tout généralement bienveillant et ne s’emportait guère, même si son regard en disait parfois long sur les sentiments qui l’habitaient. Toujours sa voix demeurait égale et ses mots choisis. Geoffroy s’efforça de se montrer à l’image de son modèle et retint la bile qui lui brulait les entrailles.

« Ne pouvions-nous refuser ce honteux repli ?

— Les rois ont décidé avec leurs conseils. Toute notre vaillance ne saurait suffire à abattre ces murailles seuls.

— Nous avions pourtant immense troupe, le plus bel ost assemblé en ces lieux depuis les illustres souverains de nos pères.

— Tu parles vrai. Mais force soudards implique aussi grands appétits de barons. La tourte avait été coupée avant de l’enfourner, ce qui ne fait pas bonne pitance. »

Geoffroy savait que son oncle désapprouvait la répartition qui avait été faite des terres et de Damas, alors que la campagne n’avait même pas commencé. Selon lui, cela brisait la vigueur des jaloux mis de côté et rendait cupides les chanceux, peu désireux de démolir des fortifications qu’ils auraient ensuite à rebâtir. S’il appréciait la vaillance et l’engagement de Thierry d’Alsace, le comte de Flandre, il n’en aimait pas les ambitions parfois mesquines qu’il estimait contraires aux intérêts du royaume.

Ils pénétrèrent dans le petit pavillon qui servait de bureau et de chambre au sénéchal. Un valet s’y employait à ranger dans un coffre les objets usuels, sans s’approcher du plateau où étaient disposés les matériels d’écriture, les sacs d’archives et de correspondance. André de Montbard s’installa dans sa chaise curule et fit un signe à Geoffroy pour qu’il prenne note.

« De prime, j’aimerais discuter avec le sire Vacher, porte-bannière Baudoin. Il aura peut-être utiles nouvelles à propos de l’arrivée du sire d’Alep[^nuraldin]. »

Voyant Geoffroy froncer les sourcils, le sénéchal expliqua son idée.

« C’est de certes le plus informé et sage des hommes du roi. Il a usage de s’entretenir avec les mahométans. Je n’accorde nulle foi aux calomnies qui circulent sur lui, je le connais assez pour n’avoir aucun doute sur sa droiture. »

Il marqua une pause et esquissa un éphémère sourire, d’où la gaieté était absente.

« D’ailleurs, il serait bon que tu t’efforces de mieux le connaître, toi aussi. Il saura te donner les clefs qui te manquent parfois pour comprendre ce qui se passe en nos provinces. »

Il se tut, joua un instant avec une plume oubliée sur son bureau.

« Ce jour, mon neveu, il est temps pour toi d’apprendre à voir plus loin, plus large, plus grand. Tu disais tout à l’heure que nous faisions honteux repli. Il n’en est rien… Mais certaines batailles ne se gagnent pas que le fer en main. »

Puis il expliqua succinctement à Geoffroy que des échanges de courriers avaient eu lieu avec le seigneur de Damas, Mu‘īn ad-dīn Anur. Celui-ci, qui jouait ses adversaires les uns contre les autres afin de maintenir son indépendance, avait démontré que si les Latins s’entêtaient à assiéger sa cité, il se verrait contraint de se livrer à Nūr ad-Dīn, le nouvel homme fort du nord de la Syrie. Et l’union d’Alep et de Damas serait une catastrophe pour l’équilibre des puissances en présence. Faute de ne pas s’être emparés de la ville rapidement, les croisés devaient se résoudre à quitter la place pour ne pas empirer la situation à leurs frontières.

« Ce mouvement de retrait que tu déplores vaut bien des centaines de charges lance au poing, mon neveu. À la vaillance du soldat et à la pureté du moine, il est parfois roué d’ajouter le bon sens du paysan qui sait quand semer et quand récolter. »

## Wadi Arabah, camp de voyage, soir du mardi 10 août 1154

Sur les conseils des bédouins qui les guidaient, la modeste troupe de chevaliers du Temple s’était installée au pied d’une falaise, dans un ressaut qui apportait un peu d’ombre. Ils n’avaient eu que peu de temps pour mettre en place le bivouac avant que la nuit ne tombe. Ils avaient rapidement dessellé et brossé les montures, avaient nourri les bêtes. Un feu chiche crépitait désormais au milieu de leur petit cercle, tandis qu’ils avalaient leur repas du soir, poisson séché, pain et bière.

Un peu à l’écart, près de leurs chameaux, les bédouins discutaient d’une voix forte, certains chantaient de temps à autre, tandis que les étoiles piquaient d’éclats diamantés le ciel de velours. Les porteurs du manteau blanc demeuraient silencieux, habitués à manger sans proférer une parole, utilisant parfois quelques signes de la main ainsi qu’il était d’usage dans les monastères. Ils savaient qu’un temps d’échange était prévu une fois le repas terminé, où il était même permis de jouer ou de faire de la musique sans s’attirer de regards sentencieux.

Geoffroy commandait cette petite unité, qui acheminait des messages à Ayla[^ayla], patrouillant loin dans le sud, vers le seul port tenu sur la mer Rouge par le royaume de Jérusalem. Il s’était porté volontaire afin de mieux connaître cette région. Depuis quelques semaines, il ressassait l’insistance de la reine Mélisende à parler des zones méridionales. Depuis qu’il était arrivé et ses premiers entretiens avec elle, grâce aux courriers de recommandation envoyés par l’abbé Bernard de Clairvaux, il avait la chance de la rencontrer de temps à autre. Il avait toujours été impressionné par l’intelligence de cette femme de pouvoir. Malgré une apparence fragile, qui s’accentuait au fil des ans, il n’avait qu’exceptionnellement découvert autant de force et de détermination dans une personne, y compris chez ses frères du Temple.

Récemment, il avait été frappé par son insistance à étudier ce qui se passait dans les zones méridionales, alors que leurs principaux ennemis étaient au nord et à l’est. Il avait donc eu envie de voir ces régions de ses yeux. Il n’avait eu jusque là que rarement l’occasion de chevaucher si loin dans le désert.

C’était la première fois que Gaufroi Morisse, son nouvel écuyer, prenait la route et il admirait tout avec des yeux ronds. Sa candeur et son élan juvénile amusaient Geoffroy, même s’il craignait aussi que cela ne dénote un esprit mal discipliné. Il estimait de toute façon que des sorties en pleine nature, loin du couvent, constituaient le moment idéal pour éprouver une âme et se faire une idée du caractère d’un homme. Surtout dans des circonstances pareilles, où ils n’étaient qu’une poignée, isolés au milieu de la rocaille, des acacias et des gazelles.

Sachant que la troupe attendait de lui qu’il donne le signal de début de soirée, il brisa le silence en s’adressant à son écuyer.

« Alors, Gaufroi, trouves-tu le voyage à ton goût ?

— C’est… incroyable, mon sire Geoffroy ! Ici, tout n’est que poussière et roches. N’y a-t-il aucune bête, aucune plante ? Est-ce là un endroit oublié de Dieu ?

— Que non pas. Il te faut apprendre à voir le lézard qui se tapit dans l’ombre, l’antilope qui se dissimule dans les failles. Et surtout, le lion, qui fait la sieste dans un bosquet.

— Mais où trouvent-ils de l’eau ? Nous n’avons croisé nulle source depuis deux jours.

— C’est l’été, les rivières s’assèchent, et les mares se cachent. Mais les bêtes, et les bédouins, savent où regarder. »

Le jeune écuyer hocha la tête sans ajouter un mot. Geoffroy l’envoya chercher un portfolio de cuir où il gardait du papier, ainsi qu’une écritoire. Il avait vite remplacé ses tablettes de cire par un support plus résistant à la chaleur quand il était arrivé dans le royaume. Depuis, il conservait tous les documents de peu d’importance pour pouvoir y griffonner des remarques dans les moindres marges. Bien qu’ayant une excellente mémoire, il trouvait utile de coucher par écrit des annotations pour pouvoir organiser ses pensées.

Et là, il s’était fixé pour tâche de relever tout ce qui pouvait être d’un quelconque usage militaire dans la zone. Si jamais la guerre devait se déplacer vers le sud, ces lieux seraient l’unique point de passage possible pour les armées de Nūr ad-Dīn en direction de l’Égypte. Ce démon d’homme leur avait soufflé Damas quelques années plus tôt, et Geoffroy était désormais convaincu que c’était en partie dû à leur mauvaise préparation. Il comptait bien fournir au Temple suffisamment d’éléments pour ne pas laisser s’épanouir une nouvelle union, qui placerait le royaume au sein de gigantesques tenailles qu’une main ferme n’aurait que peu de peine à serrer.

À la pensée que cela pourrait signifier pour l’avenir de Jérusalem, il se signa silencieusement. Puis il se mit en devoir de noter ses souvenirs du jour de son écriture minuscule.

## Jaffa, maison du Temple, après-midi du mardi 14 octobre 1158

Un vent du large apportait de la fraîcheur et de l’humidité dans la petite cour qui avait été aménagée en succédané de cloître au centre des bâtiments conventuels de l’ordre. Installé dans une des salles ouvrant sur la galerie, Geoffroy Foucher était de passage pour quelques jours. Il s’employait à un recensement des matériels et combattants capables de participer à des affrontements navals alors que la plupart des des hommes se préparaient à partir vers le nord. Ils devaient se joindre à l’armée latine qui accueillerait le basileus byzantin Manuel Comnène avec lequel il était prévu de lancer ensuite une vaste offensive dans les territoires aleppins.

Malgré cela, Geoffroy s’employait toujours à réfléchir à la façon dont des opérations militaires d’ampleur pourraient être déployées dans la direction exactement opposée. Certains de ses frères en étaient venus à se moquer de cette lubie, parfois sans aménité, sans que cela n’entame sa détermination. Du reste, il avait été soutenu en ces travaux par son oncle, quand il avait accédé à la magistrature suprême de l’ordre après le siège d’Ascalon et la mort désastreuse de l’éphémère Bernard de Tramelay.

Le décès d’André, auquel avait succédé Bertrand de Blanquefort, n’avait rien changé à ces habitudes. Le nouveau maître, homme de guerre, mais aussi ouvert à la réflexion d’ampleur, lui avait confirmé cette mission d’analyse et d’étude. Geoffroy regrettait de ne plus autant chevaucher que par le passé, mais il estimait n’être plus en âge de s’amuser à chasser le lion, et s’ennuyait à escorter les pèlerins sur des chemins désormais bien balisés et protégés. Il avait même commencé à tisser un réseau d’informateurs venus d’Alexandrie, voire du Caire. Voyageurs et négociants qui s’y rendaient ou en venaient se voyaient régulièrement convoqués pour lui en apprendre plus sur le territoire voisin. Geoffroy se sentait parfois handicapé de ne pas savoir s’exprimer en arabe, à l’image de Bernard Vacher, dont l’absence se faisait si cruelle.

Il était dans sa petite cellule, à compulser des listes de marins et de sergents, d’équipements nautiques et de nolissement de navires, quand on frappa à sa porte. Sans qu’il n’ait le temps de répondre, une haute silhouette s’avança, emmitouflée dans une chape de voyage coupée dans une magnifique laine anglaise. Ce n’était certes pas un frère qui aurait pu s’autoriser une telle débauche de luxe, mais, pour être ami et proche du temple, Philippe de Milly n’était pas un profès. Sa vaillance et sa réputation le désignaient néanmoins comme un modèle naturel pour de nombreux chevaliers. Geoffroy reconnut immédiatement le seigneur de Naplouse et l’accueillit avec chaleur. Il l’avait croisé de temps à autre, tous les deux orbitaient dans les mêmes cercles, mais jamais ils n’avaient eu l’occasion d’échanger plus de quelques mots convenus.

D’autorité, le baron s’empara d’un escabeau et se laissa tomber dessus, invitant d’un geste le frère du Temple à demeurer assis. Son relatif mépris des marques ostentatoires de respect lui valait aussi une excellente réputation parmi les hommes de guerre. Il considérait volontiers les gens vaillants comme des égaux, sans attacher d’importance à son titre et son rôle éminent au sein du Haut Conseil.

« Je suis bien aise de vous encontrer alors même que je vais devoir quitter nos provinces de longs mois, avec ce qui se prépare à Antioche !

— Vous compaignez Baudoin ?

— Pas complètement. Disons que j’ai moindre presse que lui à me livrer au roi des Griffons.

— Quelque mésentente avec le basileus Manuel ? »

Philippe de Naplouse retint un sourire. Le templier faisait usage de la bonne titulature, signe de sa connaissance du sujet. Un bon point pour lui, nota-t-il en silence. Il balaya la question de la main.

« Si j’encrois les discussions, vous-mêmes n’accordez que peu d’importance à toutes ces affaires au septentrion, d’aucune façon. On dit de vous à la Haute Cour que vous arpentez les chemins du sud comme un pèlerin en quête du Salut.

— On parle donc de moi parmi barons ? s’amusa Geoffroy.

— De cela et de bien d’autres choses. On n’arrive pas ici recommandé par le grand Bernard lui-même sans attirer à soi les yeux et les oreilles. Mais si c’est bien vrai, j’encrois que vous devriez faire connoissance de Guy, mon frère sénéchal le comte de Jaffa, Amaury.

— Je ne savais pas qu’il était de votre fratrie. Je l’ai vu, à l’occasion, au palais. Ainsi que chez la reine. »

Philippe acquiesça. Il appréciait le fait que Geoffroy parle de Mélisende comme si elle était encore la seule et unique femme à porter ce titre. Pourtant elle avait abdiqué de toutes ses fonctions de gouvernement des années plus tôt. Et le jeune Baudoin avait désormais une épouse, la princesse byzantine Théodora, la propre nièce du basileus.

« Disons que mon frère assaille la muraille d’Amaury aux côtés de Mélisende, pour le convaincre de l’importance du Caire. Ainsi que vous le faites auprès des vôtres.

— Certes. Et il serait de plus de profit encore d’en convaincre le roi lui-même.

— Pour le moment, nul de nous n’a son oreille, toute tournée vers le nord, la Cilicie, l’Oronte et l’Euphrate. Mais si Amaury nous rejoint, si la milice lui apporte son soutien, nous pouvons peut-être infléchir les choses.

— Avec notre sire maistre par la grâce de Dieu dans les geôles mahométanes[^captureblanquefort], nous ne pouvons guère prendre pareilles décisions.

— De cela Dieu tranchera : s’il doit recouvrer la liberté, et le roi s’emploie à cela, ou pas. À défaut, celui qui le remplacera pour tenir le bâton de commandement saura peut-être entendre nos arguments. Il nous faut juste nous montrer prêts. »

Geoffroy hocha la tête et embrassa d’un mouvement de bras tous les documents empilés sur sa table de travail.

« Nous ne répéterons pas l’erreur de Damas, voilà chose assurée ! » ❧

## Notes

Il est rare d’avoir des renseignements sur les personnages de second plan et le templier Geoffroy Foucher figure parmi ceux-ci. On sait qu’il devint avec le temps un diplomate reconnu, envoyé négocier lors de difficiles conciliabules. Par ailleurs, on connaît grossièrement ses origines, étant lié par sa famille aux fondateurs de l’ordre où il fera carrière, jusqu’à atteindre un rôle d’importance.

Je me suis alors demandé, tout en cherchant à fournir une plausible réponse à la problématique, comment un tel homme, élevé dans une ambiance dévote voire zélote, avait pu, au fil des ans, se montrer si mesuré qu’on ne craignait pas de lui confier des missions où la retenue et la subtilité constituaient des clefs essentielles de réussite, autant qu’une nécessaire relative connaissance de ses interlocuteurs. J’ai donc eu envie de tracer une ligne générale, d’esquisser un arc narratif assez large, sur lequel je pourrai bâtir certaines intrigues à venir dans l’univers d’Hexagora. Car, comme les férus d’histoire le savent déjà, la décennie 1160 sera entièrement tournée vers l’Égypte et sa conquête.

## Références

Burgtof Jochen, *The Central Convent of Hospitallers and Templars. History, Organization, and Personnel (1099/1120-1310)*, History of Warfare, Vol. 50, Leiden - Boston, Brill, 2008.

Forey A. J., « Novitiate and Instruction in the Military Orders during the Twelfth and Thirteenth Centuries », dans *Speculum*, volume 61, numéro 1, 1986, p. 1-17.

Bottazi Marialuisa, « La lettre de Saint Bernard à la reine Mélisende », dans *La lettre-miroir dans l’Occident latin et vernaculaire du V au XV s.*, sous la direction de D. Demartini, S. Shimahara et Ch. Veyrard-Cosme, Paris : Institut d’Études Augustiniennes, 2018, p. 67-80.
