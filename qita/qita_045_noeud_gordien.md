---
date: 2015-08-15
abstract: 1150-1156. Willemo Marzonus, vétéran des guerres d’Espagne, a bien l’intention de se tailler un fief à la pointe de l’épée dans le Moyen-Orient des Croisades. Mais le destin qui s’offre finalement à lui dans ces terres d’aventure n’est pas celui qu’il espérait.
characters: Ansaldo, Bachir, Benedetta, Berta, Domenico Bota, Jezabel, Massina de Tripoli, Willelmo Marzonus
---

# Nœud gordien

## Port de Gibelet, midi du lundi 19 juin 1150

La clameur du port valait celle de Gênes, même si la taille en était bien modeste comparativement. Des chameaux dans un enclos près des murs de la cité, des palmiers qui apportaient un peu d’ombre indiquaient clairement que l’on était au Levant. Les hommes enturbannés, les visages olivâtres étaient encore plus nombreux, et les navires arboraient des couleurs et des formes exotiques. Devant le bâtiment de la Chaîne, les douanes où Willelmo était allé inscrire les biens qu’il importait, sa femme Benedetta et ses deux enfants attendaient, un peu inquiets.

Lorsque le vétéran sortit, cette vision l’amusa et il souriait largement quand il les rejoignit.

« Alors, femme, montre un peu entrain à fouler ce sol sacré ! »

Benedetta lui lança un regard noir. Il n’avait toujours pas réussi à briser le caractère de cette génoise au cœur fier, malgré les années, et les raclées autrefois régulières. Il avait fini par abandonner l’idée de la dresser à son idée, et s’étonnait de ne pas en être autrement affecté. Lui qui se vantait d’avoir passé par le fil de l’épée plus d’hommes, et de femmes, qu’il n’avait pu le compter, échouait à se faire respecter dans sa propre maisonnée. Il haussa les épaules, se penchant ensuite vers la petite Berta, sa dernière et lui murmura :

« Tu sais qu’ici ils ont des singes ? Des marmousets comme compagnons. Il te dirait d’en posséder un ? »

Empêtrée dans les robes de sa mère, la petite acquiesça avec un sourire incomplet.

« Allons-nous chercher hostel pour loger ?, l’interrompit Benedetta.

— Si fait, femme. Nous allons voir… un certain Domenico Bota, qui possède logement à louer pour une famille comme la nôtre. Nous allons nous installer ici un temps.

— Ne voulais-tu pas aller au plus vite auprès du comte ?

— Nous ne pouvons partir ainsi, comme valets sans maître. Je vais t’installer comme il sied, puis je louerai monture pour préparer notre venue en la grande cité. »

Benedetta sembla réconfortée à l’idée et marmonna un vague assentiment. Elle n’avait jamais eu à se plaindre de son époux sur le plan matériel. Fille d’un humble pêcheur, elle avait fait bon mariage selon ses critères. Il ramenait pas mal d’argent de ses expéditions de soldat, la laissait gérer la maison comme elle l’entendait et, surtout, il était souvent absent. De plus, avec le temps, sa main devenait légère, se faisant volontiers plus caressante que violente. Elle qui avait été habituée au ceinturon de son père, elle en était à la fois soulagée et un peu déstabilisée. Pas au point de s’en inquiéter malgré tout. Elle avait deux enfants dont s’occuper, sans compter les trois qu’elle avait perdus et à Gênes, ils avaient un logement dont elle pouvait s’enorgueillir, avec du verre aux châssis des fenêtres et de la vaisselle décorée sur la table.

« Tu vas voir, femme, les monnaies d’or vont pleuvoir comme l’ondée d’automne sur nos têtes. Il y a toujours emploi pour soldat avisé. Avec mes lettres, je suis acertainé de trouver bon poste auprès du sire comte. Je pourrai diriger un petit groupe de sergents, monté comme un chevalier. Pour ça, on soudoie jusqu’à six sols la journée. »

Il s’esclaffa, arrêtant sa femme d’un geste emphatique, et bloquant les autres badauds.

« De quoi t’habiller en laine d’Angleterre et soie de Babylone ! »

La remarque finit par arracher un sourire à Benedetta.

« Avoir bel hostel me sied mieux, tu le sais bien.

— On aura et l’un et l’autre ! Et valets pour nous servir, comme barons en leur fief. Je ne te parle là que de ce que le sire comte me versera. Il y a toujours du butin à grappiller. Regarde ce que j’ai pu amasser en Espaigne, alors imagine : ici, les ânes chient de la soie et les porcs des épices. Nous serons bien vite aussi riches que les Doria. »

Tout en parlant, ils franchirent l’entrée dans la ville, passant la muraille d’ocre pour pénétrer dans le dédale des venelles ombragées. Près du port, l’odeur de la marée, du varech et des poissons était encore forte, mais elle disparut peu à peu au profit des senteurs plus riches, plus sèches des souks et de la poussière des rues à boutiques.

## Tripoli, matin du mardi 7 octobre 1152

Willelmo tenta de s’asseoir un peu mieux sur sa couche, mais la douleur à la jambe le fit grimacer et même exhaler un râle. Benedetta, assise à côté de lui, en train de réparer un des vêtements des enfants, se tourna, inquiète.

« Rien de grave, je m’installe mieux, voilà tout.

— Désires-tu assistance ?

— Non pas. J’arrive encore. Curieux comme je peux souffrir d’un membre qu’on m’a coupé. »

Willelmo lança un regard triste sur le drap aplati qui s’étendait là où aurait dû se trouver son mollet droit. Il avait été amputé quelques semaines plus tôt, après qu’une flèche lui eût brisé le tibia. Le chirurgien avait craint que toute la jambe ne pourrisse et avait donc coupé sous le genou. Depuis lors, le soldat se reposait chez lui. Il avait reçu au début la visite de ses nouveaux compagnons d’armes.

Désormais, ils avaient repris le cours de leurs activités et l’avaient un peu oublié. Un temps, il avait enragé de son sort et abusé du vin arrosé de pavot qui l’aidait à supporter la douleur. Il ne gardait qu’un souvenir confus de cette période. Il n’avait refait surface que depuis peu, et commençait à accepter. Benedetta et les enfants lui apportaient beaucoup de réconfort. Mais leur présence lui rappelait qu’il ne pouvait demeurer ainsi très longtemps.

Sa femme était enceinte de nouveau, et il ne pouvait rester inactif. Ils avaient de quoi vivre plusieurs mois grâce à la gestion parcimonieuse de Benedetta, mais il était de son devoir de trouver une solution à la situation. Tout en réfléchissant à cela, il jouait d’un brin de fil, volé à sa femme, comme il l’avait toujours fait depuis qu’il était enfant. Il récupérait alors des mèches dans l’atelier de son père, cordier réputé, et refaisait les complexes boucles qu’il voyait les marins utiliser. Il interrompit d’un geste le travail de son épouse.

« Il faudrait dire à Bachir de faire venir sa femme, Benedetta. Tu es fort grosse, et je ne veux pas risquer de perdre l’enfant.

— Es-tu fol ? Dilapider notre bien à prendre valets alors que tu n’as plus de travail !

— J’ai fort pourpensé cela. Nous avons de quoi payer servante au moins jusqu’à tes relevailles, que tu ne fatigues pas trop. Pour ma part, je pourrai prendre une part de nos avoirs et faire négoce. Tripoli est un port, et tous les navires ont besoin de bons et beaux cordages. Je m’y entends en cela, mon père me l’a fait rentrer dans la caboche à coups de torgnoles bien senties.

— Je ne sais…

— Moi si. Bachir m’aidera dans mon travail. Il parle les langues d’ici et sait suffisamment compter. De simple valet, il peut devenir une sorte de clerc pour moi.

— Sait-il seulement écrire pour cela ?

— Et moi donc ?, s’amusa Willelmo. Je sais juste signer de mon nom !

— Alors il te faudra apprendre, que chacun sache qui est le maître en cet hostel. »

Willelmo réfléchit un petit moment, soudain plus grave.

« Tu parles de raison. Si j’ai espoir de faire négoce, il me faut pouvoir rédiger contrats et lettres. J’irai trouver à l’évêché un clerc qui puisse m’enseigner cela. Je ne suis pas plus idiot que ces tonsurés, après tout ! »

Benedetta se pencha soudain, puis, suspendant son geste, hésita à embrasser son époux sur la joue. Celui-ci lui attrapa la nuque et lui avala les lèvres en un baiser langoureux. Lorsqu’ils se séparèrent, il susura :

« Je n’ai plus qu’une jambe, mais je suis toujours un homme debout, Benedetta. Il ne sera pas dit que je ne sais nourrir les miens. »

Elle se releva, les joues enfiévrées, et tira sur sa cotte au ventre rebondi pour retrouver une contenance.

« Je n’ai jamais pensé le contraire, mon ami.

— Tu n’auras plus à craindre que je prenne un mauvais coup, je me contenterai d’affronter d’obèses marchands et des artisans âpres au gain. Et sur les routes, qui oserait s’en prendre à un ancien soldat ? »

Il attrapa la cruche et le gobelet posé à côté de lui et se servit une large rasade d’un vin clair, au goût léger. Puis il entreprit de faire un nœud au motif alambiqué, soudain espiègle comme un enfant.

« Il était dit que je ne finirai pas chevalier. Mais rien ne m’empêchera d’en avoir la richesse ! »

## Tripoli, veillée du samedi 7 mars 1153

À la lueur des lampes à huile, Willelmo additionnait et retranchait, vérifiant sans cesse ses totaux, de crainte de faire une erreur qui lui coûterait cher. Son assurance martiale et sa connaissance du travail de la corde lui avaient permis de se tailler rapidement une belle place dans la confrérie marchande, mais il était toujours inquiet de faire un impair dans la gestion de ses biens. Bachir s’était d’ailleurs révélé un assistant compétent et zélé. Il ne savait ni lire ni écrire, mais comptait fort bien de tête et parlait plusieurs idiomes locaux. Jusqu’à présent il s’était montré d’une fidélité sans faille. Et son aide à la mort de Benedetta avait grandement soulagé Willelmo.

L’embauche de son épouse Massina lui avait permis de ne pas avoir à se soucier des enfants. Il les aimait, forcément, mais ne se sentait guère à l’aise. C’était une affaire de femmes que d’éduquer les tout petits.

Oberto, à bientôt dix ans, pouvait commencer à suivre un peu son travail, pour être formé, mais il était encore trop jeune et fragile pour être vraiment utile. Et le benjamin, Ansaldo, né en prenant la vie de sa mère, avait eu besoin des soins d’une nourrice, dénichée par la vaillante Massina. Elle épiçait un peu trop les plats et ne savait pas coudre les vêtements qu’il affectionnait, mais en dehors de cela, il en était satisfait. Irrité par ces chiffres qui ne voulaient pas trouver leur place, il envoya promener les papiers et la tablette d’un geste agacé, manquant de renverser la lampe. Il tourna sur son escabeau et laissa son regard dériver dans la pièce. Il avait aménagé son ancienne chambre pour en faire son cabinet de travail. Un châssis de verre fermait la fenêtre, et on pouvait l’obturer d’un volet de bois, ou plus fréquemment d’un simple rideau aux motifs géométriques bleus et blancs.

Sur le sol, une natte tressée cachait les carreaux de terre cuite abîmés tandis que les pans de plusieurs étoffes légères recouvraient de couleurs pastels les parois chaulées. Une table, deux bancs et un escabeau s’appuyaient contre le mur opposé au lit, et un coffre lourd, de chêne ferré prenait place sur le côté. C’était là qu’il mettait à l’abri ses documents précieux, l’argent qu’il amassait. Une autre huche, de pin, et aux ferrures moins épaisses, servait à ranger ses habits sous la fenêtre. Il se leva, claudiquant sur sa jambe de bois. En fin de journée, quand il avait trop marché comme cet après-midi, son moignon était douloureux d’avoir frotté dans son emplacement. Il tira le rideau et ouvrit le châssis, s’accoudant au chambranle pour humer le vent du large. L’odeur d’embruns lui rappela des souvenirs. La lune gibbeuse dessinait de craie blanche les toits environnants, les mâts des navires amarrés, la crête des vagues. Il ne faisait pas froid. Des cris éclatèrent non loin, dans une langue inconnue de Willelmo. Deux soulards qui s’invectivaient, peut-être le couteau à la main. Il sourit, soudain replongé quelques années en arrière, quand il était de ces soudards, aventuriers sans crainte du lendemain. Le bruit de la porte de sa chambre le fit se retourner. Bachir le salua poliment, mais sans obséquiosité. C’était un homme libre, qui se montrait serviable sans servilité, ce que Willelmo appréciait grandement. Il détestait la faiblesse et la veulerie, autant chez les autres que chez lui. Cela ne lui inspirait que mépris. C’était peut-être pour cela qu’il se prenait à tant regretter Benedetta et son caractère ombrageux et fier. Il n’était pas un de ces damoiseaux qui chantaient leur amour, pleuraient leur bienaimée. Pourtant il sentait comme un vide dans ses entrailles quand il rejoignait son lit, seul.

« Qu’y a-t-il, Bachir ?

— Les enfants en appellent à toi, mestre Willelmo. Ils espèrent un baiser avant de dormir.

— Que ne sont-ils déjà au lit ?

— Tu leur avais accordé soirée de contes, mestre. C’est pour ça que Massina a fait les awaïmets[^awaimet]. »

Willelmo grommela ce qui fut pris pour un acquiescement par Bachir. Il se retira discrètement.

## Côtes du royaume de Jérusalem, midi du jeudi 10 mars 1155

La voile du *Fortissimus* était tendue sous une belle brise du nord-ouest, les côtes défilaient à bonne vitesse, les plages succédant aux affleurements rocheux. Ils avaient dépassé Jaffa peu auparavant et longeaient désormais le sud du royaume. C’était là que Willelmo craignait le plus une attaque de brigands égyptiens. Il avait suivi les conseils de plusieurs contacts, qui lui avaient expliqué que la flotte égyptienne était très demandeuse de fournitures. Il était mal à l’aise avec l’idée de commercer trop ouvertement avec des ennemis, et tirait nerveusement sur un morceau de ficelle dans l’espoir d’en faire une pomme de touline. Le goût des embruns le ramenait quelques années en arrière, lorsqu’il parcourait les mers comme un chasseur et non comme une proie.

Il s’essuya le visage à l’aide d’un rafraf, une des longues traines de son turban. Il avait adopté ce couvre-chef quelque mois plus tôt, de façon à la fois de protéger sa calvitie grandissante du soleil, mais aussi pour y celer des choses. Bachir lui avait montré comment dissimuler documents et petites monnaies dedans. En outre, il lui avait expliqué que cela amortissait efficacement les coups, sans pour autant trahir un usage militaire. Par la suite, il avait découvert que les rafrafs, pouvaient servir de nombreuses façons, dont la moins correcte était de s’essuyer les mains, le visage, voire le nez pour les plus malpolis.

Bachir le rejoignit sur le pont, portant leur repas, du pain plat garni de purée de pois chiches aux herbes. Caboter permettait de s’arrêter autant qu’on le voulait pour refaire le plein de produits frais, et on mangeait donc aussi bien qu’à terre. Une outre de vin, certainement dilué, accompagnait l’en-cas. Le valet aida son maître à s’asseoir, opération toujours malaisée avec la jambe de bois, d’autant plus sur un navire sans cesse en mouvement.

« Le *patronus* m’a dit qu’il espère faire brève escale à Ascalon. Il n’y a guère de marchandies à y décharger. Nous devrions apponter à Alexandrie d’ici une semaine.

— Je me demande encore si je ne devrais pas vendre mes filins et cordes à Ascalon et, de là, repartir au nord.

— Nous avons quelques contacts en la cité égyptienne. Al-Baghdadi nous a donné le nom de trois de ses associés qui achèteront certainement à bon prix. Avec ses lettres, nous n’aurons nul souci à trouver preneur. Il a très bonne réputation.

— De cela je ne doute guère. Mais commercer avec les infidèles, j’y répugne tout de même.

— Ils sont peut-être musulmans, mais ils doivent pêcher, naviguer, commercer comme nous. Ils achètent beaucoup d’accastillage, de voiles et de bois, de poix et de corde. À très bon prix, car leur nation en est pauvre. En tant que marchand, il est juste que nous leur vendions, non ?

— Je trouve cela un peu trop simple. Ce sont des adversaires que j’ai combattus le fer en main durant des années. Et là, je vogue vers eux pour les assister.

— Tu sais, maître, moi je suis né ici, au Levant, et suis bon chrétien. Seulement, je ne peux me permettre de haïr le musulman, car c’est mon voisin. D’ailleurs le vieux comte[^raymondIIb] lui-même a fait alliance un temps avec Norredin, quand il y trouva son intérêt. »

Willelmo opina, sans conviction. Jusqu’alors, les musulmans étaient des ennemis lointains qui habitaient des cités à piller dont il revenait les mains pleines de butin. Il n’avait pas envisagé qu’en Terre sainte, il vivrait auprès d’eux et, comme le disait Bachir, s’en ferait des voisins. Pas des amis, certainement pas. Mais un familier, avec un visage, un métier, une famille. Pouvait-on sereinement faire comme si de rien n’était ?

## Tripoli, après-midi du lundi 6 février 1156

Willelmo somnolait, tandis que ses doigts s’agitaient, occupés à faire et défaire un nœud de ride jamais achevé. Il employait pour cela un échantillon de corde qu’il avait prélevé chez un de ses fournisseurs.

Il avait entrepris de rédiger seul une lettre pour un ami resté à Gênes, dont il se disait qu’il ferait un bon garde du corps lors de ses voyages. Enrico était plus jeune, mais avait connu les campagnes en Espagne, et ils avaient partagé bien assez de coups durs. Certainement que Maza se moquerait de la panse rebondie de son ancien compagnon et de sa façon de gérer ses affaires. Mais il était un homme fidèle à sa parole et un soldat compétent. Willelmo avait appris qu’il se faisait payer comme arbalétrier sur les navires, un poste bien rémunéré mais toujours soumis aux aléas.

Willelmo lui parlait donc de la douceur de vivre au Levant, de la fortune qu’il amassait dans le négoce, sans plus avoir à tirer la lame. Il gardait sous silence la mort de Benedetta, car il ne savait pas si Maza approuverait son choix de se remarier avec Jezabel. Sûr qu’il trouverait la jeune femme à son goût, avec ses longs cheveux bruns et son visage fin. Mais épouser une Levantine, même chrétienne, avait été une décision difficile à prendre. Willelmo ne regrettait nullement. La fille de Massina et Bachir était très dévouée à son foyer, adorait ses enfants et espérait bien lui en donner d’autres.

Elle mettait autant d’ardeur à tenir la maison qu’à le satisfaire sous les draps. Il l’avait embauchée au début pour assister sa mère dans les tâches domestiques, vu qu’il était de plus en plus souvent absent pour son négoce. Puis, à force de la fréquenter, il s’était surpris à suivre du regard les courbes qui se dessinaient sous les amples robes, à s’interroger sur ce que recelaient ces épais vêtements. Elle n’avait jamais minaudé et avait toujours fait en sorte de ne pas à avoir à repousser les avances d’un maître concupiscent. Puis il s’était aperçu qu’il hésitait à profiter d’elle, car il avait de l’estime pour le père, qu’il traitait plus comme un associé que comme un employé. Il lui avait même concédé, avec le temps, une part sur certains bénéfices et délégué le travail en rapport. Et puis ses enfants appréciaient fort Jezabel, qui les gâtait un peu trop à son goût lorsqu’il était absent.

Vu qu’il était nouveau dans la cité de Tripoli et pas encore suffisamment riche, il n’espérait guère pouvoir s’attirer les faveurs d’une maison prestigieuse pour conclure un mariage avantageux. D’autant qu’il était veuf avec déjà deux garçons, héritiers possibles. La famille de Bachir était chrétienne, d’un culte qui reconnaissait l’autorité du pape, donc rien ne s’opposait à leur union. De plus, Jezabel insista pour se faire baptiser selon le rite catholique avant le mariage. Elle n’attachait en fait que fort peu d’intérêt à la chose religieuse et se conformait aux usages latins avec aussi peu d’enthousiasme qu’elle avait manifesté envers la foi maronite.

Pour installer les siens confortablement et montrer son enrichissement à tous, il avait emménagé dans une belle demeure avec un jardin, et une terrasse d’où il avait une jolie vue sur la mer. De plus, il bénéficiait d’assez de pièces où accueillir ses associés et entreposer des marchandises. En ce moment, il hébergeait Abdel ben Mokhtar al-Tabari, un de ses contacts damascènes avec lequel il entretenait beaucoup de relations. Il investissait pas mal dans les productions de cuivre et de céramique que celui-ci achetait dans sa ville avant de les acheminer un peu partout, jusqu’à l’Ifriqiyah. Il avait même proposé à Willelmo de l’accompagner un jour dans un de ses voyages, qui pourrait le mener, qui sait, en Espagne. Willelmo avait toujours été discret sur son passé, indiquant qu’il avait été marin et soldat. Mais il renâclait un peu à l’idée d’aller en invité là où il avait débarqué dans le feu et le sang.

Il pencha le nez vers sa lettre, ponctuée d’hésitations et de taches d’encre. Il avait bien progressé mais frère Benedetto, le chanoine qui lui apprenait à écrire, à lui et à ses enfants, froncerait sûrement les sourcils devant un travail aussi bâclé. Il soupira d’aise et se frotta le crâne, désormais orné d’une simple couronne de cheveux coupés courts, puis avala une longue gorgée de vin de Chypre, un luxe auquel il avait pris goût. Puis il reprit la plume, en trempa l’extrémité dans le petit encrier. Il fallait à tout prix finir cette maudite lettre avant que le *Rubens* ne mette à la voile dans les jours qui venaient, s’il voulait espérer voir Maza avant la fin de l’année. ❧

## Notes

Il est malaisé d’entrer dans le quotidien de ceux qui, depuis l’Europe, faisaient le choix de s’installer au Moyen-Orient. Comme le colonialisme plus tardif l’a bien démontré et ainsi que Ronnie Ellenblum l’a écrit, les sociétés pouvaient se juxtaposer, vivre à côté l’une de l’autre sans se mélanger ou s’influencer.

Malgré tout, la promiscuité incitait immanquablement certains à tisser des liens. Sans aller jusqu’à l’interconfessionnalité, on connaît des unions entre Latins et Levantins. Les conversions étaient en outre possibles. D’autre part, l’installation dans ces nouvelles terres se faisait aussi en abandonnant les rapports anciens que l’on avait dans son territoire de naissance. Si, dans les chartes, on nomme parfois certains depuis leur origine européenne (le Bourguignon, le Provençal…), on voit également apparaître des toponymes plus proches, originaires de Terre sainte. Des réseaux se créaient entre les nouveaux arrivants et ceux qui étaient là depuis plus longtemps, enfants issus des premières vagues d’immigration.

En outre, on trouve dans les textes, comme ceux d’Usamah ibn Munqidh, la mention de Latins très orientalisés. L’émir cite l’exemple d’un Franc antiochéen qui refusait le porc à sa table, pour ne pas indisposer ses invités musulmans. On ne peut négliger l’hypothèse que ce fin lettré ait souhaité démontrer ainsi la supériorité de sa propre culture, qui finissait à s’imposer à tous mais il est possible que cela fasse écho à une réalité. On trouve bien dans l’inventaire après décès d’un riche marchand vénitien (Gratianus Gradenigo) des tenues désignées comme de type oriental. Le commissaire priseur n’avait aucun intérêt à falsifier la provenance du vêtement dans sa liste. Enfin, certains Latins savaient s’exprimer dans les idiomes locaux, pouvant se passer d’interprètes lors des négociations (comme Renaud de Sidon, bien connu pour cela).

## Références

Louise Buenger Robbert, « Twelfth-Century Italian Prices: Food and Clothing in Pisa and Venice », *Social Science History*, vol.7, n°4 (Autumn), 1983, p. 381-403.

Paul M. Cobb, *Usama ibn Munqidh. The book of Contemplation. Islam and the Crusades*, Londres : Penguin Books, 2008.

Ronnie Elleblum, *Frankish Rural Settlement in the Latin Kingdom of Jerusalem*, Cambridge University press, Cambridge, 1998.
