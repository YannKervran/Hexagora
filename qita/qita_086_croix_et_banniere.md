---
date : 2018-12-15
abstract : 1152-1156. Ernaut, jeune homme plein d’enthousiasme et géant amateur de bagarres, se trouve mêlé au soulèvement populaire des gens du bourg contre leur seigneur, l’abbé Pons de la Madeleine.
characters : Ermont de la Treille, Ernaut, Arnaud, Lambert, Hugues de Saint-Pierre, Robert le Chapelain, Seguin
---

# La croix et la bannière

## Vézelay, pâturages communs, après-midi du mardi 17 juin 1152

Quelques nuages cotonneux épars flottaient autour d’un chaud soleil annonçant l’été à venir. Au pied de la colline où le bourg et l’abbaye s’étendaient, assis le long d’une auge de bois où l’on faisait boire les bêtes, une dizaine de garçons se rafraîchissaient, rouges et haletants d’avoir trop couru. En contrebas, près de taillis et de ronciers abritant un ruisseau, tracées de pierres et de brindilles, on voyait encore les lignes qui délimitaient les camps de jeu, brouillées par les nombreuses parties de barres[^barres].

Au-dessus des têtes résonna la cloche des moines, qui appelait ces derniers à la prière. Aucun n’y prit garde, indifférents qu’ils étaient au temps religieux, occupés de leurs importantes affaires d’enfants. Parmi les adolescents, un géant dominait tous les autres, avec un corps massif semblable à celui d’un bûcheron ou d’un portefaix. Il n’avait pourtant qu’un visage juvénile où le poil n’était que duvet, et sa voix oscillait malgré lui parfois entre graves et aigus. Sur ses vêtements gris de poussière, une traînée brune coulait depuis son menton, après les gorgées qu’il venait de prendre à même l’abreuvoir. En dépit de son essoufflement et de son teint rouge, son entrain ne paraissait guère entamé et il éclaboussait quiconque s’approchait de lui, le regard invitant à une bagarre amicale.

Ernaut aimait les jeux où sa taille et sa force étaient rudement éprouvés. Rien ne le remplissait plus de joie que de se confronter à plus âgé, plus retors ou plus nombreux que lui. Il lui fallait se mesurer à tous les adversaires et les vaincre avant de s’estimer satisfait. Bien que son père Ermont de la Treille soit un riche vigneron, il était généralement vêtu de vieilles cottes fatiguées, rapiécées et reprisées, sa famille ayant compris qu’il ne servait à rien de l’habiller de belle toile. Jamais il n’avait pris soin de ses affaires, se roulant dans la poussière ou la boue pour peu qu’on lui offre l’occasion d’un difficile affrontement.

Il n’était néanmoins pas belliqueux, simplement désireux de mesurer sa force et d’en garantir la suprématie face à tous. Il n’hésitait d’ailleurs pas à prêter main-forte à quiconque pouvait en avoir besoin, goûtant autant les compliments sur sa puissance que la victoire lors d’un jeu disputé. Il avait prévu pour la fin de journée de retrouver un de ses camarades afin de l’aider à s’acquitter d’une corvée d’épierrage. Malgré l’aisance de son père, Ernaut ne dédaignait pas les amitiés plus modestes comme avec Droin, même s’il se brouillait avec celui-ci aussi souvent qu’il se réconciliait. Il s’humecta une dernière fois la nuque et se rinça les mains avant de s’adresser au groupe.

« Ceux qui se sentent peuvent me compaigner jusque chez le Groin. Je lui ai fait promesse d’assistance, pour son vieux et lui.

— Nous prends-tu donc pour serfs des bons frères ? se railla un grand brun au visage rond.

— Il s’agit juste d’aider compère à sa corvée…

— Il suffit d’attendre et les tonsurés ne pourront plus exiger pareille redevance, d’ici peu. »

Arnaud était parmi les plus vieux de leur petit groupe et le compagnon habituel de bêtises d’Ernaut. Lui tirait une grande fierté de la richesse de son père Hugues et s’arrangeait souvent pour diriger les activités. Conscient du naturel dominant d’Ernaut, il usait de paroles astucieuses et de propositions insidieuses, laissant à son cousin annoncer de sa voix autoritaire ce qu’Arnaud lui avait soufflé par d’habiles suggestions. Et s’il aimait aller faire les pires diableries auprès des moines avec Droin, fils d’un des servants porchers, il était moins enthousiaste à l’idée de se fatiguer à accomplir une de ses obligations.

« Le comte Nevers saura bientôt nous libérer du joug des clercs, c’est mon père qui le dit.

— Vous croyez qu’ils feront comme à l’époque de l’Ancien ? Petiot, il me disait qu’ils avaient pendu l’abbé en leur temps ! s’amusa un de leurs compagnons.

— Crois-moi qu’il faudra bien à ces tonsurés demeurer en leur place et reconnaître qu’ils tirent de nous injustes exactions ! » confirma Arnaud.

Tous lancèrent un regard vers le chantier des murailles de la ville, sur la partie ouest du relief. Depuis de nombreuses années, les habitants du bourg tentaient de s’affranchir de la tutelle exigeante de l’abbaye de la Madeleine. Ils avaient trouvé une oreille complaisante dans la famille du comte de Nevers, dont les appétits pour cette riche agglomération aiguisaient l’intérêt. Les villageois espéraient assouplir les prétentions fiscales de l’abbé et avoir un recours auprès de qui en appeler lorsqu’ils estimaient les jugements trop partiaux ou les impôts trop pesants, voire injustes. Parmi les nombreuses redevances contestées ou considérées trop lourdes depuis des décennies, le droit d’héberge, qui permettait aux moines de faire loger des pèlerins chez l’habitant était l’occasion de récriminations et de revendications qui dégénéraient parfois en affrontements ouverts.

« Que dirais-tu d’aller plutôt cueillir quelques framboises aux abords de la pâture aux moines ? Le pâtre n’y est guère, en ce moment, les bêtes sont plus bas. On y trouve baies grosses comme le pouce ! »

Avec une telle proposition, Arnaud était certain de convaincre Ernaut, dont la gourmandise le disputait au bon cœur. Assaisonnée d’un soupçon de licence, l’éventualité d’un pareil goûter était de nature à faire pencher la balance vers plus agréable activité. Il ne fallut guère plus de quelques instants pour que résonne l’appel du géant à sa troupe.

« Allez, compères, allons quérir monnaie de nos impôts ! Sus aux framboises des tonsurés ! »

## Vézelay, demeure d’Ermont de la Treille, soirée du lundi 28 juillet 1152

Comme chaque soir, le repas était pris en silence, chacun vidant son écuelle sans mot dire une fois le bénédicité récité. Ermont de la Treille mâchait difficilement, handicapé par ses dents usées ou manquantes. Il engloutit rapidement son potage, épaissi de pain, mais eut besoin de plus de temps pour manger les navets qui suivirent, agrémentés de lardons et d’une sauce au vin. Lorsqu’il vint finalement à bout de sa portion, il s’octroya un dernier verre avant de replier le canif qu’il gardait toujours sur lui. C’était le signal tacite du début des échanges que, privilège de l’âge, il entamait en premier. Il s’adressa à Seguin, son aîné, qui s’occupait de la plupart des affaires que le vieil Ermont dédaignait. En dehors de la vigne, il ne prenait guère plaisir à travailler la terre.

« On en est où, du grain ? 

— J’ai déjà empli les deux grands silos et la petite réserve. Nous avons encore plusieurs sacs emplis, alors que nous n’avons pas encore fini battage et vannage. Nous aurons large provision, plus que nos besoins.

— Il faudrait voir avec le vieux Gennon. Il a toujours de la place en ses greniers. Et il n’est point trop gourmand… As-tu prévenu le père Breton, à Autun ? Il nous achète toujours bon prix.

— Pas encore. Je ne sais s’il fera bon voir son grain par vaux et chemins avec les temps qui s’annoncent. »

Ermont opina lentement. La situation était en train de dégénérer entre les habitants du bourg et l’abbaye de la Madeleine, qui restait sourde à leurs demandes d’allégements fiscaux. Le comte de Nevers avait versé de l’huile sur le feu tout en se proposant comme arbitre, espérant par là établir une main-mise sur certains aspects de la vie de la cité. Le vieil homme se tourna vers son cadet, occupé à gober des boulettes de pain à l’autre bout de la table.

« Parlant de ça, Ernaut, j’ai ouï dire que tu avais été de ceux qui aboyaient avec les chiens de Nevers, ce tantôt. »

Le jeune homme acquiesça, circonspect. Son père n’était jamais bien autoritaire avec lui, mais il avait parfois le verbe cinglant. Et subir une brimade devant sa famille et les valets valait bien tous les châtiments corporels en terme de honte.

« Il serait sage de ne point trop te mêler de cela. Nul n’accorde l’eau à un moulin dont il n’espère tirer farine. Le comte est bien loin et la violence qu’il a déchaînée sur les terres de la Madeleine ne nous sera de guère d’utilité, à nous autres. Nous n’avons qu’un seul seigneur et maître et c’est le sire abbé. Nous devons apprendre à lui adresser nos doléances de la bonne façon. Laisse ces importantes questions aux doyens, nous saurons la régler.

— Avez-vous eu l’occasion d’en parler avec l’oncle Robert, père ? demanda Seguin.

— Oui et non. En tant que curé de la paroisse Saint-Étienne, il est homme du bourg et n’a guère l’oreille du sire abbé Pons. Pour autant, il pense comme moi qu’il faut demeurer fidèles sujets, sans se risquer à outrages et violences, qui n’ont jamais rien engendré de bon.

— Il n’a pas eu de nouvelles des discussions d’Auxerre, avec l’envoyé du Saint-Père ?

— Pas encore. Mais je n’en espère que peu, vu comme les envoyés de Nevers se comportent. Je crains un terrible été. Puissent nos vignes ne pas en souffrir ! »

Il garda le silence un moment et se tourna vers Seguin.

« Fais donc porter le grain en trop ici, nous lui trouverons bien une place où le garder. »

## Vézelay, village de Saint-Pierre, demeure de Hugues, début d’après-midi du samedi 25 avril 1153

Hugues de Saint-Pierre était installé sur son banc préféré, sous un auvent accolé à sa maison, là où il entassait le bois prêt à brûler et où il s’adonnait à ses tâches à l’abri des intempéries, tout en admirant ses propriétés qui s’étiraient jusqu’aux rives de la Cure et même au-delà. Le visage rond, presque poupin malgré sa barbe grise et son teint rouge, il parlait d’une fois forte, où les r grondaient comme des rochers lancés sur des pentes. Ses larges mains posées sur les cuisses, il se voyait assez comme un seigneur en son fief, en dépit de sa roture et de l’aspect comique que le moindre vêtement de valeur lui donnait. Quels que soient ses efforts de toilette, il ressemblait immanquablement à un chapon se prenant pour un paon.

Ermont de la Treille ne venait que rarement lui faire visite, surtout depuis le décès d’Edelote, sœur handicapée de Hugues qu’Ermont avait épousée en secondes noces. Elle n’avait survécu à son unique enfant, Ernaut, que de quelques semaines. La relation entre les deux hommes, déjà peu amicale, n’avait pas été améliorée par leur chagrin.

Hugues se doutait qu’il allait être question de l’abbaye et des dernières nouvelles. Une sentence d’excommunication avait été prononcée par le pape à l’encontre du comte de Nevers et des habitants du bourg. Si le premier, important vassal à l’entregent nombreux, n’y voyait qu’une gêne à combattre avec l’appui de ses alliés ecclésiastiques, l’évêque d’Autun en tête, beaucoup de fidèles avaient été ébranlés par l’annonce, inquiets d’être mis au ban de leurs coreligionnaires. Sans même parler des menaces que cela faisait peser sur eux lors du moindre voyage.

La violence s’était donc déchaînée des deux côtés, les bourgeois répondant aux sentences et vexations légales par des coups de main et des pillages, orchestrés avec l’aide des hommes du comte de Nevers. Et tout le monde savait que leur âme damnée sur place, c’était Hugues de Saint-Pierre, qui soufflait sur les braises de la discorde avec un art consommé.

« Alors, Ermont, tu n’es pas venu jusqu’ici juste pour fleurir la tombe de ma sœur, n’est-ce pas ?

— Non pas. J’ai important sujet à évoquer avec toi.

— Si tu viens porteur d’un message de l’abbé par ton frère, je suis prêt à l’entendre…

— Il n’est question ni de l’un ni de l’autre. Je veux te parler d’Ernaut. Arrête de l’entraîner en tes affaires. Je n’ai nulle prétention à te sermonner sur toi et les tiens, mais laisse mon fils en dehors de cela.

— Hé quoi, il est mien neveu, et seul fils de ma sœur préférée ! S’il veut me joindre, je l’accueille avec plaisir, il est homme assez pour faire ses choix ! »

Ermont serra les mâchoires et se contint, s’efforçant de tenir la colère au loin. Hugues cherchait la confrontation, comme chaque fois. Il était trop heureux d’avoir sa revanche sur son beau-frère, un de « ceux du bourg », qu’il méprisait si fort en parole tant il avait désir d’être semblable à eux. L’homme fort du moment se tourna vers Ermont, le visage étonnamment amical.

« Je ne veux pas te manquer de respect, Ermont, mais tu ne peux demeurer en retrait et espérer que les tiens fassent de même. Ernaut a compris que nous ne pouvons rester ainsi, à supporter vexation après vexation, sans jamais pouvoir en appeler à quiconque de la féroce autorité du père abbé. Joins-toi à nous.

— Je ne saurais approuver les pillages et les sacrilèges envers un saint homme. Toutes ces batailles ne font qu’entraver nos chances de lui poser calmement nos demandes.

— Les a-t-il jamais entendues ? Depuis nos pères, qui ont occis le vieil Artaud, qu’avons-nous obtenu, à courber la tête ? Les frères te cuisent-ils ton pain malgré l’interdit, avec toute ta déférence ? Ils te traitent comme un impie, malgré toute ta bonne volonté. »

Ermont poussa un soupir. Il était las de ces querelles. Il se leva, serra l’avant-bras de Hugues en signe de salut et lui lança un regard désolé.

« Je ne crois pas en ces violences, Hugues. Arrête d’attirer mon fils en ces voies, l’ivraie ne donne jamais bonne moisson. »

Puis il s’éloigna d’un pas décidé, sans un regard en arrière.

## Vézelay, domaine de l’abbaye de la Madeleine, début de soirée du dimanche 10 avril 1155

Ernaut était assis dans un recoin des jardins, contre une pierre servant de base à un cabanon. Le souffle court, il sentait la fatigue et la lassitude gagner tout son corps. Ses doigts et ses bras étaient endoloris d’avoir frappé en tout sens, ses vêtements étaient déchirés, salis de terre et de poussière. Dans sa bouche dominait le goût métallique du sang, qu’il crachait mêlé de salive entre ses pieds. Il fixait devant lui une pierre, dont les reliefs saillants brillaient d’un vif écarlate.

Plus tôt dans la journée, il avait fait le mur de chez son père pour assister à un combat judiciaire. Avant même que la cérémonie ne commence, la foule s’était soulevée en masse contre les représentants de l’abbaye et les avait pourchassés jusqu’à les faire fuir à l’abri des fortifications monastiques. La cohue avait été sans nom, depuis le cimetière où le duel avait été prévu. 

Avec son cousin Arnaud et quelques compères, Ernaut avait rejoint une bande qui avait obliqué vers les propriétés des moines, profitant du désordre pour se servir dans les réserves. Ernaut avait enfourné poulets et volailles dans un sac qu’il avait ensuite perdu dans l’excitation. Ils avaient brandi les pauvres animaux sous le nez des religieux, abrités derrière leurs murailles. Puis avaient décidé de s’attaquer à d’autres biens.

Il ne faisait pas bon être pris à partie par les meutes qui s’étaient alors mises à rôder et Ernaut espérait que les siens étaient saufs. On connaissait son père pour sa modération et il craignait qu’on ne lui en tienne rigueur. Au moins Ernaut pourrait faire valoir sa présence aux côtés des insurgés si la situation le demandait. Mais pour l’instant il n’en était plus là. Épuisé, il avait le regard rivé sur la pierre devant lui, incapable de faire un pas de plus, hébété de toute cette rage qui s’était emparée de lui.

Après avoir dévalisé une partie des réserves des moines, certains avaient pris la direction du sanctuaire et de ses objets précieux pour le mettre à sac. Ernaut et quelques autres avaient refusé de se joindre à eux et avaient préféré demeurer dans les celliers. Ils distribuaient tout qu’ils empoignaient, pillant par haine de leur maître plus que par amour des biens. Et lorsqu’ils voyaient quelques fidèles de l’abbé, ils le poursuivaient en vociférant, cherchant à l’acculer pour le bousculer, le passer à tabac, l’invectiver ou lui cracher dessus, selon les méfaits qu’on lui prêtait.

Puis une rumeur avait enflé : depuis leur abri les moines tiraient flèches et carreaux, vidaient leurs pots d’aisance sur les bourgeois à leur portée. Les plus modérés devinrent alors aussi enragés que les autres et les mains empoignèrent tout ce qui pouvait blesser, tuer. Les serpes et les houes se faisaient hasts pour quérir les droits qu’on ne voulait accorder. Des voix de plus en plus nombreuses s’élevaient, en appelant à couper la tête de l’abbé Pons. Et Ernaut avait suivi, les bras emplis de projectiles et le verbe haut, emporté par la conviction qu’ils obtiendraient leur liberté à la force de leurs poings.

Les derniers partisans des moines ayant rejoint l’enceinte fortifiée, il ne resta guère d’adversaires à empoigner et la colère retomba. Les corps fatigués de tant d’agitation, meurtris des affrontements, s’étaient faits lourds. Quelques hommes s’étaient proposés pour monter la garde alors que d’autres s’étaient esquivés pour aller rapiner ce qui pouvait encore être utile. Conscient que son absence avait dû être remarquée, Ernaut avait pris congé de ses compagnons de combat, partageant leurs fanfaronnades outrées. Puis il avait entamé le chemin du retour, traversant les jardins. Avec son groupe, ils étaient passés par là plus tôt dans l’après-midi, tandis qu’ils pourchassaient sans pitié leurs adversaires.

C’était là que l’attendait la pierre couverte de sang. Celle qui avait frappé le vieux Tous-Saint. Un des hommes de l’abbaye, un convers un peu simplet à la langue pendante, qui se signait chaque fois qu’on disait un prénom, en mémoire du saint qu’il associait à chaque personne. Le vieux Tous-Saint, qui faisait rire tous les enfants quand il trébuchait avec les bannières de l’église lors des processions. Le vieux Tous-Saint qu’il avait vu s’effondrer, le crâne défoncé par cette pierre, avant d’être piétiné par ses camarades.

## Vézelay, demeure d’Ermont de la Treille, matin du jeudi 24 novembre 1155

Ermont était en train de brosser un des chiens terriers qu’il employait dans ses vignes pour pourchasser les campagnols lorsque son frère Robert se présenta à la porte. La tonsure cachée par sa chape de pluie, il ne donnait guère l’impression d’être un prêtre. Grand et maigre, il avait l’élocution sifflante à cause de nombreuses dents manquantes et n’y voyait guère, ce qui l’obligeait à constamment plisser les yeux, en recherche des interlocuteurs auxquels il s’adressait.

« Entre donc, frère. Par cette pluie, tu vas attraper la mort. Veux-tu quelque gobelet de vin ? J’ai en perce un de mes coteaux ouest… »

N’attendant pas la réponse, Ermont héla un des valets pour qu’on leur porte à boire et s’installa à la grande table qui emplissait la majeure partie de la pièce. Robert s’assit sur le banc, pliant sa chape avec cérémonie. On aurait dit qu’il était constamment en office, accordant soin et mesure à chacun de ses gestes. Une fois servis, ils burent tous deux en échangeant quelques banalités sur le temps et des nouvelles sans importance, avant que Robert ne se décide à aborder ce qui l’amenait là.

« J’ai pu parler de notre affaire avec les moines. Ils veulent faire condamner tous les fauteurs de trouble, sans exception. Le roi leur a donné franches coudées… »

Le visage d’Ermont s’allongea en entendant la mauvaise nouvelle. Il ne savait pas au juste ce qu’Ernaut avait fait, mais il l’avait sévèrement puni et consigné dans l’enceinte de la maison depuis des mois, occupé aux plus ingrates des tâches. Lorsque le roi était finalement intervenu, l’abbé Pons avait obtenu gain de cause, rétabli dans toutes ses prétentions, et il avait obtenu la condamnation de tous ses adversaires.

« Malgré tout, le père abbé n’est pas tant opiniâtre qu’on le dit. Il sait que tu as prêché la mesure et il n’est pas demeuré insensible à cela, je l’ai bien vu. Seulement des bruits courent sur ce qu’aurait fait le gamin… »

Ermont craignait qu’on ne mutile son fils, voire qu’on lui inflige une flétrissure, signe d’infamie : nez, langue ou oreilles tranchées ou visage marqué au fer rouge. Il savait que cela ferait de Ernaut un paria. Il était prêt à tout pour éviter cela, quitte à s’acquitter d’une très lourde amende. Il en avait soufflé l’idée à son frère.

« J’apense que le père abbé souhaite avant tout que le calme revienne. Cela ne se fera pas en accrochant à des fourches patibulaires tous les habitants du bourg. Il s’en prendra aux meneurs, de certes, mais cela n’est point le cas d’Ernaut, juste imbécile gamin. C’est qu’il ne passe guère inaperçu… Il se murmure qu’il a fait bien terribles choses. »

Robert prit la main de son frère et la tapota délicatement.

« Je ferai de mon mieux pour que rien de définitif ne soit entrepris à son encontre, mais je crains que la meilleure des solutions ne soit pour lui un exil volontaire, qu’on l’oublie ici. »

Ermont hocha la tête, abattu. Il lança un regard au fidèle petit chien lové à ses pieds, le désigna du menton à son frère.

« Sais-tu que c’est le dernier fils qui me reste de P’tite Reine, la chienne à Edelote ? » 

## Vézelay, demeure d’Ermont de la Treille, veillée du dimanche 25 décembre 1155

Malgré les circonstances difficiles, la demeure était animée de vie pour la célébration de Noël. Les enfants jouaient bruyamment, installés devant l’âtre où brûlait une bûche de grande taille, tandis que les plus âgés parlaient du bon temps à venir avec plus d’enthousiasme que les années précédentes. Avec la paix retrouvée, les affaires pourraient reprendre normalement, et nul n’aurait plus à s’inquiéter pour son bien. De nombreuses dissensions demeuraient, prêtes à renaître à la plus petite étincelle, mais pour l’instant, chacun s’accordait sur le plaisir qu’il y avait de jouir du fruit de son travail sans trop craindre du lendemain.

Ernaut était parmi les rares à ne pas partager ces moments de joie. Il était assis dans l’ombre, près de la porte du cellier, jouant sans entrain avec ses cousins qu’il faisait sauter sur ses genoux. Il n’avait pas prêté la moindre attention au babil des filles, au sol non loin de lui. Régulièrement, il lançait des regards vers Ermont, figure patriarcale confortablement installée sur un escabeau garni de coussins, une couverture sur les jambes. Un verre à la main, il hochait parfois le menton en réponse aux questions, mais n’allait guère au-delà des monosyllabes. Lui aussi avait autre chose en tête. Tous les visages convergèrent vers lui lorsqu’il toussa à plusieurs reprises.

« Je voulais profiter que nous soyons tous ici réunis pour vous faire annonce de plusieurs choses. Nous avons eu de longs échanges avec le père abbé, grâce à Robert, et notre maisonnée n’aura pas à souffrir d’infamie. Nul nom d’ici ne sera cité, et si un de mes fils l’est, on n’y lira que le fait qu’il s’agit d’un neveu de Robert, sans préciser outre. »

De nombreux visages exprimèrent leur soulagement en entendant cela, mais la voix en suspens du vieil homme les incitait à la prudence.

« Malgré tout, afin d’être absous de ses péchés, Ernaut devra pèleriner, demandant pardon de ses fautes et méditant sur les moyens de n’y pas retomber. »

Ermont marqua un temps. C’était là le genre de pénitence habituelle, par laquelle les clercs aimaient à mortifier leurs adversaires et personne ne s’en étonna. Il y avait pléthore de saints à qui demander pardon dans les environs, et il n’était jamais inutile d’éloigner les fauteurs de trouble un moment, en attendant que les choses rentrent dans l’ordre.

« Il devra demander rémission pour ses offenses sur le tombeau du Christ, à Jérusalem. »

Sans s’arrêter sur les visages ébahis de l’assemblée, Ermont enchaîna.

« Outre, une fois son vœu accompli, il serait bon qu’il profite des généreuses offres faites là-bas pour qui veut s’y établir. Dans le but d’éviter que des querelles ne puissent naître à l’avenir, le père abbé a demandé à ce qu’Ernaut ait la chance de vivre en la contrée où Christ a vécu. »

En de telles circonstances, une demande de l’abbé Pons équivalait à un ordre. Un silence stupéfié succéda donc à cette dernière phrase. Ce furent les enfants qui, les premiers, le rompirent, s’esclaffant de joie à l’idée que leur parent puisse aller en ce lieu empli de mystères et de légendes, au centre du monde. Seguin ramena le calme et, d’un geste, invita son père à terminer son annonce.

« Nous avons eu nouvelles de Lambert, qui ne trouve pas à s’établir comme tonnelier à Pontoise. Il a désir de compaigner son frère Ernaut jusque là bas et de s’établir lui aussi. Ils pourront ainsi se conforter l’un l’autre pendant le voyage et une fois installés. »

Le vieil homme était visiblement abattu et n’ajouta pas un mot. Il adressa un regard à la fois courroucé et ému vers son plus jeune fils, imposante masse de muscle ramassée sur son banc.

## Route du Vif (Châteauneuf-Val-de-Bargis), après-midi du jeudi 28 juin 1156

Après un rapide repas pris à proximité d’un ruisseau où ils avaient puisé de quoi se désaltérer, Ernaut et Lambert étaient repartis d’un bon pas. Si les deux frères partageaient vigueur et entrain dans leur marche, tout les opposait dans le caractère. Ils avaient quitté Vézelay quelques jours plus tôt et n’avaient cessé de se chamailler depuis. Ernaut avait fini par envisager ce périple comme une invitation à l’aventure, s’efforçant de manifester autant d’enthousiasme qu’il le pouvait, peut-être afin de se rassurer. Il avait l’habitude de voyager avec son père, jusqu’en Normandie parfois, et n’avait pas encore pleinement ressenti l’aspect définitif de ce départ.

Ernaut avait souvenir d’un frère, de plus de dix ans son aîné, assez sentencieux et réservé, mais il avait oublié un élément essentiel : Lambert était sincère dans ses inclinations sérieuses et dévotes. Le pèlerinage ne serait pas simplement de découverte et d’émerveillement, mais se devait d’être empli de haltes pieuses et de pratiques religieuses régulières. Le chant en faisait partie, fortifiant l’âme autant que le souffle selon Lambert, au grand désarroi d’Ernaut qui s’était jusque-là toujours contenté d’ânonner sans chercher à comprendre quoi que ce soit.

« Allons, Ernaut, l’oncle Robert t’a suffisamment répété ce beau psaume des montées, tu dois t’en souvenir !

— Je n’ai guère l’oreille musicale, peut-être que si tu commençais, je pourrais suivre… »

Interrompu par le cri d’un geai saluant ces intrus pénétrant sur son territoire, Ernaut se contenta de sourire niaisement à son frère en guise de conclusion. Lambert, peu habitué à devoir morigéner et surveiller en permanence un jeune homme au caractère fantasque, soupira. Puis il prit une profonde inspiration, lançant le chant d’une voix claire et assurée, marquée en rythme par le bruit de son bâton au sol.

« *ad Dominum in tribulatione mea clamavi et exaudivit me*[^psaume119]

— Ah doux minou entre hiboux là scions, mais acclame vite et tout ce qu’on dit vite, mais...

— *Domine libera animam meam a labio mendacii a lingua dolosa*

— Deux minets libèrent âne…» ❧

## Notes

Il aura finalement fallu sept ans, depuis novembre 2011 pour que j’explique la raison qui a poussé Ernaut à traverser le monde. Je pense qu’il était assez clair qu’il n’était pas suffisamment dévot pour avoir pris la décision de pèleriner si loin de son propre chef. J’ai parsemé certains textes d’indices sur le fait qu’il avait été contraint à ce départ et avait un secret à l’origine de cela. Les plus curieux pourront s’amuser à faire la chasse ici et là, au détour de certaines descriptions ou remarques.

L’insurrection de Vézelay est la raison qui m’a fait choisir ce lieu comme origine d’Ernaut. Je voulais m’appuyer sur les tristes événements du tournant du siècle pour bâtir la jeunesse de mon héros. Si certains ont accès au *Bulletin de la Société des sciences historiques et naturelles de l’Yonne*, volume 2, de 1848, page 550, ils pourront y trouver la mention du neveu de Robert le chapelain parmi la liste des pilleurs des biens de l’abbaye, et en particulier des volailles. C’est ce genre de document juridique, d’apparence très austère, qui m’invite le plus souvent à échafauder des récits de vie. De leur lecture naît une société surprenante, foisonnante, des liens se tissent, des histoires se déploient.

Si je suis arrivé à vous rendre plus proche le destin de nos pas si lointains prédécesseurs, alimentant votre réflexion par ce rapport subjectif à l’histoire, à la mémoire et à nos constructions mentales, j’estimerai que mon but est atteint. Je dis parfois que j’écris de la science-fiction, car n’en déplaise à certains, l’histoire est une science. Je m’en nourris pour fabuler, mais avec une exigence de vérité dans le propos, dans les situations qui me font penser que mes lectures adolescentes de Isaac Asimov n’ont pas été sans effet sur ma façon d’aborder mon travail d’écrivain. Ayant récemment lu des récits d’Ursula le Guin, je me suis aperçu que je conservais en outre, comme elle, une attention particulière à ce qui constitue le véritable héros d’Hexagora : l’humain.

## Références

Cherest Aimé, *Vézelay, étude historique*, tome 1, Auxerre : Perriquet et Rouillé, 1848.

De Bastard D’estang Léon, « Recherches sur l’insurrection communale de Vézelay, au XIIe siècle », *Bibliothèque de l’École des chartes*, volume 12, Numéro 1, 1851, p. 339-365.

De Bastard D’estang Léon, « De la commune de Vézelay », *Bulletin de la Société des sciences historiques et naturelles de l’Yonne*, volume 2, 1848, page 527-570.

Jonquay Sylvestre, Müllers Fabian, *Les jeux au Moyen Âge*, Auvilliers en Gâtinais : Éditions La Muse, 2016.
