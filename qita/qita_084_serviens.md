---
date : 2018-10-15
abstract : 1151-1158. Gosbert, sergent d’armes ayant suivi le puissant comte de Flandre en terre Sainte lors de ses voyages devient avec le temps un capitaine d’aventure à la compétence militaire éprouvée. Mais quel prix lui ont coûté ses rêves d’aventure ?
characters : Gosbert, Ligier, Peironelle, Arnoul, Layla, Raguenel
---

# Serviens

## Bourg Saint-Martin, parvis de l’église, fin de matinée du vendredi 16 février 1151

Le petit marché aux poissons finissant répandait ses odeurs de marée 
jusqu’à la porte de l’église devant laquelle Gosbert avait attaché sa monture. Il était trempé d’avoir chevauché dans le crachin et la brume matinale, ses vêtements alourdis de pluie gouttant sur le sol grossièrement empierré. Il regardait la femme face à lui, qui se tenait prudemment de l’autre côté du portail, abritée elle aussi sous les voussures, les bras croisés. Elle n’avait guère changé estima-t-il. Le visage mince était devenu maigre, les joues creusées par l’absence de quelques dents. Peut-être avait-elle un peu de neige dans ses cheveux noirs, mais elle les dissimulait sous un épais voile de chanvre. Elle le dévisageait de ses yeux bruns cernés, les lèvres pincées, les sourcils froncés. Il retint un sourire à la voir arborer ces mimiques familières. Elle ne semblait pourtant guère enchantée de le croiser. Sa voix grinça comme l’acier sur la roche.

« On a porté la nouvelle de ta mort voilà plusieurs Carêmes. »

Gosbert tira la capuche de sa chape de voyage, révélant son visage rond, le poil désormais mâtiné de gris. Il s’était fait coiffer avant de venir, raser quelques jours auparavant. Il s’efforça de rendre sa voix aimable.

« C’était rude voyage. Mais me voilà de retour.

— Y’a plus rien ici pour toi, Gosbert. On a même dit des messes. C’est les miens qu’ont payé. »

Gosbert hocha la tête. Il avait appris la nouvelle avant même d’arriver au hameau où il était né et avait grandi : toute sa famille avait été emportée par une épidémie et les terres avaient été redistribuées à d’autres par le seigneur local.

« T’as survécu aux fièvres ? Et les petiots ? La Maheut, Liénart ?

— J’étais plus là-bas. Ton père m’avait chassée quand Liénart a été emporté, un peu avant sa quatrième année. Maheut a pas connu les Pâques après ton départ… »

Gosbert soupira. Son père avait toujours été un homme emporté et autoritaire, qui n’avait pas supporté de le voir partir comme soldat au lieu de travailler la terre comme eux tous. C’était bien de lui de se venger sur son épouse, une fois sans enfant.

Peironelle chercha un moment ses mots puis le fixa avec sévérité, un brin de colère animant sa voix.

« Tu veux quoi, Gosbert ? Tu crois pas que t’as assez fait le mal ?

— Dis pas ça, je pensais qu’on serait mieux…

— T’étais un rêveur, mon vieux m’avait prévenue. La mort te suit, Gosbert. T’as jamais rien apporté de bon dans ma vie. Ou dans celle de personne. »

Le sergent se crispa à entendre son épouse le morigéner ainsi. Il était las de son interminable voyage, de tous ces combats en Terre sainte pour revenir en un comté de Flandre dévasté par la guerre avec le Hainaut. Mois après mois, il avait repoussé l’échéance où il retournerait chez les siens. Il n’avait pas l’auréole de gloire de ses prédécesseurs en Palestine, ni même aucune richesse tangible, ayant tout perdu en beuveries et fêtes grossières, où il tentait d’oublier ses griefs contre la vie. Il avait espéré jouir du prestige de son périple, mais l’accueil était plus que glacial, inquiet. Il cherchait ses mots, gêné, et fut devancé par Peironelle.

« J’ai marié un autre homme, qui m’a fait un beau garçon. Il laboure sa terre pour les siens, au lieu de… »

Gosbert lui lança un regard courroucé, auquel elle répondit d’un air de défi, le menton levé. Elle était toujours aussi vive. Il baissa la tête, fit mine de laisser courir ses yeux au loin. Une nouvelle fois, ce fut elle qui brisa le silence.

« Tu comptes revenir ici ? »

Gosbert haussa les épaules. C’était en effet ce qu’il escomptait. Abandonner pour de bon son épée pour la serpe et la houe. Oublier les neiges d’Anatolie et les flèches turques des déserts, les marches et les assauts dans le vent, la poussière. Ne plus subir la chaleur ou le froid, la faim et la soif. Vivre sans la peur au ventre. Troquer tout cela contre des récits pour les veillées, où les plus jeunes s’endormiraient, souriant des exploits de leur aîné. Par bravade, il montra sa sacoche de cuir où étaient lacés son casque et son haubergeon, à l’abri de l’humidité, fit claquer son fourreau contre sa cuisse.

« Y’a guère de labeur pour l’homme d’armes, par ici. Le sire comte va marier sa cadette au jeune Baudoin du Hainaut. La paix va revenir. »

Peironelle hocha la tête. Pour autant, ce n’était pas la réponse qu’elle attendait. Il regarda la petite place où les derniers étals se vidaient de leurs poissons. Quelques chats disputaient les abats à de gros rats juchés sur un tas d’immondices. Un charroi de bois de charpente tiré par deux bœufs passait, les essieux grinçants. Il revint à Peironelle, s’appuya contre le portail de pierre, secoua la tête.

« Je te veux pas le mal, la mère. Je passais juste comme ça. Personne m’a reconnu, t’auras qu’à dire que j’étais un cousin à ton premier mari qu’était avec lui en outremer. »

Elle lui accorda un visage plus amène, clairement soulagée de ne pas avoir à répondre d’une accusation de bigamie qui l’aurait menée droit au couvent, voire pire. Son nouvel époux était certes un homme peu violent, quoique dur à la tâche, mais il avait l’honneur chatouilleux et n’aurait pas apprécié de se voir traité de cocu. Gosbert fouilla dans son sac et en sortit une palme fanée, brisée en plusieurs endroits.

« Tiens, j’étais certes pas croisé de plein droit, mais ça vient quand même de là-bas, béni par les moines du Sépulcre le Christ. »

Elle hésita un moment, peu désireuse de se rapprocher de lui, méfiante jusque dans ses regards. Il osa un sourire qu’il espérait engageant et posa la feuille au sol entre eux deux. Il se secoua, se composa un visage qu’il pensait allègre.

« Bon, c’est pas tout ça, mais si je veux passer les portes de Lille avant la nuit, faut pas que je traîne.

— Alors tu comptes pas rester dans le coin ? Voir les tombes des tiens ? Celles des petiots… »

Il secoua la tête, reprit son sac, le noua rapidement à l’arrière de son troussequin puis détacha ses rênes, faisant tourner sa monture. Peironelle ne put retenir un regard d’admiration devant le vaillant roncin à l’œil vif et à la croupe puissante. Il se hissa en selle, mit son cheval en place et le vit volter avec adresse. Qu’elle garde de lui un bon souvenir.

« Allez, que Dieu te garde la mère. On aurait pu… »

Sa voix mourut dans sa gorge, il remonta sa capuche et talonna sa bête, la lançant à petit trot sans se retourner. Derrière lui, Peironnelle se pencha lentement, ramassa la feuille de palme, serrant le poing autour de la tige à en faire blanchir ses phalanges tout en le regardant disparaître, une nouvelle fois.

## Bruges, cellier de l’hôpital de Saint-Jean, fin de matinée du vendredi 28 décembre 1156

Gosbert avait encore la bouche pâteuse et le crâne comme un tambour des excès de la veille, mais il s’efforçait de se composer un regard attentif. Devant lui se tenait Arnoul, le vétéran de la croisade qui lui avait obtenu un poste de sergent au service du seigneur de Bruges. Son visage massif, rond à force d’abus de chère ne laissait que peu de marge quant à l’interprétation de son humeur. Il frottait sa large panse en respirant bruyamment, sans dire un mot, la tête penchée.

« Je peux dire que j’ai eu pleintée de gredins qui ne valaient pas un pet d’âne, mais là… »

Il écarta ses deux larges mains, un air d’incompréhension totale naissant sur ses traits globulaires.

« Il me fait remembrance que tu avais en charge la porte de Gand jusqu’à la saint Thomas, c’est bien ça ? Veiller aux hommes du guet, garantir le paiement des droits et coutumes, éventuellement prévenir tout brigandage. C’est bien là mission à toi confiée ? »

Gosbert hocha la tête, au prix d’élancements qui se propagèrent de ses tempes jusqu’à ses reins. Arnoul interpella le petit Ligier, compère habituel des débordements, qui s’était pour l’instant prudemment tenu en retrait derrière Gosbert. Jeune sergent, il s’était attaché à son aîné et le servait comme un valet tout en participant à ses frasques les plus diverses.

« Dis-moi, garçon, as-tu connoissance d’autres choses demandées au sergent de guet ? »

Ligier, qu’on surnommait Merle en raison de sa formidable aptitude à siffler, n’osa que timidement déclore son bec, et certes pas pour joyeusement s’exprimer. Encore alourdie par l’alcool, sa voix leur fit plutôt l’effet d’un croassement.

« Prendre fripons et menteurs ?

— Ah, oui, c’est vrai. Cela se peut. Vois-tu autre chose ? »

Le jeune homme lançait des œillades désespérées à Gosbert, tordant ses doigts en se trémoussant. Il n’appréciait guère d’assister à l’humiliation de celui qu’il estimait comme un maître et ne voulait y participer que de mauvaise grâce. Arnoul lui accorda un sourire grimaçant, peut-être conciliant, puis se rapprocha de Gosbert.

« Rien d’autre que cela, de certes ! Rien qui n’indique qu’il faille pisser sur la robe du chapelain de Saint-Jean ! »

Voyant la mine déconfite que Gosbert arborait, il baissa d’un ton et une lueur d’amusement pétilla dans son regard.

« D’un autre côté, peut-être espérais-tu rincer les vomissures dont tu as orné l’entrée de leur chapelle ? Cela ne saurait être tenu pour mauvais vouloir, en ce cas. »

Un large sourire fendait désormais son visage et, après avoir vérifié qu’ils étaient seuls, il invita ses compagnons à s’asseoir.

« Au rebours de moi, qui n’aime guère ces tonsurés exempts de tout, je ne te cache pas que ça ne fait pas rire au castel. Le chapelain a parlé de signes d’infamie, rien de moins ! Poing percé, marque au fer rouge… Tu sais pourtant qu’ils sont fort appréciés ici. Avec leur bel hospice tout neuf, nul n’ira les contrarier ! Ne peux-tu modérer tes veillées, compère ? »

Il laissa le silence s’installer un moment, se grattant la tête tout en reniflant avec volupté les senteurs de vin et de moisissure.

« Las, il ne m’est plus possible de leur rappeler une nouvelle fois que tu as répondu à l’appel de Bernard[^croisade2b]. Ils sont bien trop échauffés. Mais j’ai peut-être ce qu’il te faut. »

Il rapprocha son tabouret, parlant d’une voix de conspirateur, au grand plaisir de Gosbert, dont le mal de crâne commençait à peine à refluer.

« Le sire comte Thierry remet les voiles pour Jérusalem. Il en a appelé à ses châtelains et bonnes cités pour lui fournir sergents et valets en quantité. Demande à te croiser en pénitence, proposant de t’amender en versant ton sang pour Christ.

— Hum. Je suis trop vieux pour ça, j’en ai fini avec l’outremer.

— Alors, prépare-toi à te faire marquer la couenne ou couper quelque oreille ou le nez. Je n’ai guère savoir des coutumes de clerc, mais ce me serait surprise s’ils ne te prenaient pas un bout ou l’autre. »

En finissant sa phrase, il eut un regard éloquent pour l’entrejambe de Gosbert. Celui-ci accusa le coup, essayant de penser malgré la mélasse dans laquelle il devait pousser la moindre réflexion.

« Un don à leur autel ne serait pas enclin à leur adoucir le cœur ?

— Ce ne serait que d’eux, je ne sais. Mais le sire châtelain en a plus qu’assez de tes errements, Gosbert. Quand ce n’est pas clerc que tu éclabousses de pissat, c’est un marchand que tu rudoies ou un quartier que tu éveilles avec tes paillardises. Il serait temps pour toi de faire oublier ton nom en cette cité. »

Gosbert opina lentement, accordant une maigre bourrade à son compagnon d’armes. Arnoul avait toujours fait de son mieux pour arrondir les angles à chaque incartade, faisant l’entremetteur pour payer les dégâts, proposer des excuses ou débrouiller une querelle. Mais il avait raison, la coupe était pleine et il n’y aurait pas longtemps avant qu’un notable demande à ce qu’on inflige une correction bien plus radicale qu’une simple marque d’infamie. Il se tourna vers Ligier, un air faussement goguenard peint sur la face.

« Dis donc gamin, tu as ouï plus qu’à ton tour mes aventures en terre du Christ. Te dirais-tu d’y aller écrire ta propre légende ? La geste du Merle, que voilà joli conte à chanter aux veillées, qu’en dis-tu ? »

L’éclat qui naquit alors dans les prunelles du jeune homme lui rappela douloureusement celui qui avait brillé dans les siennes moins de dix ans plus tôt. Au moins le gamin bénéficierait-il d’un vétéran qui l’affranchirait d’un certain nombre de périls. Il serra la main d’Arnoul avec chaleur.

« Puisse Dieu et tous les saints te bénir d’avoir ainsi croisé mon chemin, compère !

— Je ne les en mercierai que plus, avec cierges et messes, quand tu auras déguerpi et arrêté de me causer tous ces ennuis » rétorqua l’autre avec un clin d’œil.

## Plaine de la Bocquée, après-midi du dimanche 29 septembre 1157

Accompagné de Merle chargé de sacs et de paniers, Gosbert déambulait dans le marché aux armées, au sein de ce qu’on nommait le quartier de la Madeleine. On trouvait un véritable camp de foire attaché à tous les déplacements, avec de quoi se restaurer, des fournitures diverses et même des commerces que l’Église réprouvait. Officiellement les femmes qui suivaient les troupes se consacraient à l’entretien du linge, mais on y rencontrait surtout des prostituées, plus ou moins organisées ou protégées. Il n’était pas rare que cela se fasse parfois sous l’égide de quelques clercs qui voyaient là un moindre mal, apte à éviter de bien plus terribles agissements quand les soudards en venaient à se soulager par la violence sur d’honnêtes épouses ou jeunes filles. Les soldats avaient l’usage de désigner ce vaste bordel de toile et de cabanes du nom de la réprouvée qui avait lavé les pieds du Christ.

Gosbert avait pris l’habitude de fréquenter un petit groupe de prostituées menées par Coleite, une blonde aux formes généreuses. Il savait que le signal du départ était prévu pour le lendemain et voulait s’assurer qu’elles suivraient. Il avait l’intention de leur proposer de les protéger en échange de tarifs plus accommodants pour ses hommes et lui. Il appréciait fort la routine et estimait que des présences féminines apportaient du calme dans les rangs. Du moins tant qu’il s’en trouvait en suffisance. Il fréquentait les armées depuis assez longtemps pour savoir que les prostituées voyaient également d’un œil favorable la possibilité d’avoir des défenseurs attitrés, pour peu que ceux-ci ne se montrent pas trop gourmands. 

Sur ce dernier point, Gosbert était intransigeant, il réprimandait invariablement les hommes qui se laissaient aller à frapper trop souvent ou trop durement et n’avait jamais autorisé un soldat de son groupe à dépouiller une femme du quartier de la Madeleine. Ça lui avait valu quelques bagarres et une dent cassée, mais aussi une assez bonne réputation, qui équilibrait sa notoriété d’ivrogne braillard. Il se rencontrait dans les armées des âmes plus noires que la sienne.

Lorsqu’il arriva à la petite carriole du campement de Coleite, il ne s’y trouvait que Layla, brune maigrichonne au caractère emporté dont il appréciait avec mesure l’effronterie calculée. Elle était en train de nettoyer leurs écuelles du repas de midi en les frottant dans les cendres.

« Le bon jour Layla. Où sont donc tes compagnes parties ? »

Elle répondit d’un haussement d’épaules, tempéré d’un sourire. Elle savait reconnaître un bon client.

« Il faudra te contenter de moi…

— Je suis ici pour causer de la campagne à venir. Vous avez appris le départ ? Viendrez-vous ? »

Elle abandonna sa tâche, se leva en époussetant sa robe de la poussière grise qui s’y était accumulée. Malgré de beaux cheveux noirs, elle n’était guère attrayante, avec ses formes osseuses et son visage triste. Mais son regard était habité d’une flamme si vive qu’elle réchauffait le cœur des hommes qu’elle fixait.

« Pas toutes. Certaines n’aiment pas le nord et préfèrent rester dans le coin.

— Et toi, tu en dis quoi ?

— Moi, je préfère être au parmi de soudards armés qui peuvent me garantir des bédouins. J’accompagnerai le chariot de la Coleite. »

Gosbert hocha la tête, satisfait.

« Si vous en êtes d’accord, on pourrait trouver accommodement pour que chacun ait ses aises…

— Tu penses à quoi ?

— Eh bien, dès le camp à se monter, vous pourriez trouver ma bannière et installer vos affaires parmi nos cordes.

— Le sire comte n’y trouvera rien à redire, ou son maréchal ?

— Tant que ça tient les hommes calmes, il n’y aura pas de souci. »

Layla opina. Elle n’avait jamais suivi de campagne d’ampleur où se trouvaient des milliers d’hommes comme ici. Des troupes étaient venues de partout : Palestine, Judée, Galilée, Outrejourdain… et même du comté de Tripoli. On disait qu’il s’en ajouterait d’autres d’Antioche voire d’un royaume arménien encore plus au nord. Jamais elle n’avait vu un si grand camp ni autant de soldats, de cavaliers, d’archers. Il y avait  des enclos de chameaux bâtés de poutraisons pour du matériel de siège. Elle avait choisi de demeurer parmi eux, car, pour la première fois de sa vie, elle s’y sentait en sécurité. Dormir au sein même de la troupe offrirait encore davantage de garanties.

« Ça sera pas donné, ton offre, j’encrois ?

— Rien de trop gourmand. Vous pourriez rebattre vos prix quelque peu pour les gars. Et vous serez à moi sans débourser.

— Rien que ça ?

— Ça vaut bien ! Si ça se passe bien, il sera même possible de faire pot et feu commun. Ça fera moins de corvées à chacun et on en vivra que mieux. C’est normal d’accorder au baron du lieu les bénéfices de son titre, non ? »

Elle lui accorda un rictus amer. Il n’était pas le pire des hommes, mais son cœur n’était guère généreux. Elle lui promit d’en parler aux autres filles et de retourner le voir d’ici la nuit pour s’entendre sur ce qui serait fait.

En repartant, Merle s’approcha de Gosbert. Il ne s’exprimait que peu en public, mais se montrait toujours curieux et empli de questionnements dès qu’ils se retrouvaient seuls. S’il portait l’épée et le casque du soldat, il agissait avant tout en valet, volontaire pour toutes les corvées et désireux de plaire à celui qu’il servait comme son maître. Gosbert avait fini par le considérer comme un animal familier, parfois avide d’attention, mais qui repayait au centuple les faveurs dont il avait été gratifié.

« À voir cette catin, on aurait dit que c’est d’elle que sera la faveur. Elle avait le menton bien fier.

— Eh quoi, Merle, que lui reste-t-il sinon cela ? Ne faisons pas grief à la femme d’avoir désir d’affirmer sa valeur. Je ne suis pas de ces crétins qui les dédaignent tant. Au pays, quand je rentrerai, il me faudra marier mes six filles et rien ne me plairait tant que de les savoir exigeantes sur leurs époux. »

Le gamin acquiesça. Il n’avait jamais eu la chance de rencontrer la famille du capitaine, mais il gardait le secret espoir de lui être présenté un jour. Parfois, les soirs de solitude, il rêvait qu’il finirait valet au service de l’une ou l’autre. Il en avait tant entendu parler au fil des ans qu’il lui semblait les connaître toutes, même s’il s’embrouillait sur les âges et les prénoms.

## Shayzar sur l’Oronte, camp du comte de Flandre, matinée du mardi 15 octobre 1157

La chaleur montait rapidement et les assaillants avaient déjà pas mal sué à préparer leur matériel. Gosbert allait et venait le long des deux énormes échelles dont il avait la responsabilité. Il avait revêtu son haubergeon, son baudrier d’arme et son casque, en plus du bouclier qui l’attendait au sol. Il vérifiait l’ordonnancement des soldats qu’il avait répartis de façon à avoir les hommes les plus intrépides et les plus lourds en tête. 

Ils seraient tous suivis en retrait de quelques archers et arbalétriers ainsi que d’une poignée de frondeurs. Abrités derrière des mantelets légers, leur rôle était d’empêcher des tireurs ennemis de prendre place sur les défenses.

« Dès que ça cornera, on assaillira le pan de courtine que je vous ai désigné. Attention à ne pas glisser lors de la montée, on va être fort serrés. »

Il vérifia que ses têtes de colonne étaient correctement équipées. Il avait désigné deux auxiliaires pour chacun, portant des pavois afin de les garantir des flèches et projectiles lors de l’approche vers le mur. Ceux qui escaladeraient les barreaux les premiers étaient chargés de coincer le crochet de fer fixé au moyen d’une chaîne. Rien que le poids rendrait la tâche malaisée à qui voudrait faire tomber l’échelle, mais cela participait aussi à rassurer les assaillants qui devraient monter péniblement les quelques toises qui les mèneraient au chemin de ronde.

« Une fois arrivés, protégez-vous et tenez la place. N’avancez pas sans ordre ! Il n’y a guère de soldat là-dedans et une fois regroupés, rien ne nous résistera ! »

Un cor résonna depuis le centre du camp, bientôt repris par tous les musiciens qui propagèrent l’ordre bref.

« Formez les rangs ! »

Rapidement, chacun trouva sa place, vérifia une dernière fois qu’il avait toutes ses affaires, remua les épaules pour éprouver et répartir le poids de son armure ou s’assura d’un geste que son baudrier était bien positionné. Gosbert attira à lui Merle, qui devait demeurer en arrière, ayant eu le bras brisé par une flèche lors des journées précédentes. Malgré la fièvre, il avait tenu à être avec eux jusqu’au départ de l’assaut. Il avait apporté plusieurs outres et versait de l’eau fraîche dans le gosier de qui demandait.

« Tu demeures ici tant qu’on a pas emporté le morceau, Merle, tu m’as entendu ?

— Oui, mestre Gosbert. Je ne viendrai qu’au cri de *Ville prise !*. Je vous porterai de l’eau. »

Gosbert lui serra l’épaule dans un sourire puis se tourna vers le centre du campement, attendant avec impatience. Il vit la bannière annoncer l’ordre avant même de percevoir les cors et hurla de sa voix la plus forte.

« Mouvez ! Prenez l’échelle, les gars ! »

Au son des instruments de musique, les deux colonnes s’avancèrent en parallèle, encadrées d’autres unités qui marchaient du même pas lourd sur la pente. Loin à l’ouest, les bricoles[^bricole] s’activaient, projetant des boulets sur la porte et ses fortifications afin d’y attirer le maximum de défenseurs. Des troupes de Tripoli y étaient disposées, en plus des farouches montagnards de Thoros l’Arménien, au cas où une brèche pourrait être ouverte. Néanmoins les barons comptaient surtout sur la forêt d’échelles confectionnées ces derniers jours pour emporter la victoire. Des informateurs leur avaient indiqué que les habitants avaient été surpris par l’assaut et n’avaient ni ressources militaires ni réserves de vivres pour un long siège.

Hurlant des encouragements sans regarder où il mettait les pieds, Gosbert trébucha à plusieurs reprises, entravé dans sa marche par son encombrant équipement. Il surveillait avec angoisse la section de muraille à laquelle il devait s’attaquer, craignant d’y voir apparaître des archers, voire de simples miliciens avec des paniers de pierres. Tous ses hommes arrivèrent néanmoins sains et saufs au pied des fortifications, au milieu de buissons d’épineux qu’ils piétinèrent avec entrain.

Autour d’eux, les autres groupes avaient aussi bien avancé et seuls ceux les plus à l’est semblaient en retard. Peut-être étaient-ils confrontés à une opposition depuis les hauteurs ? Il ordonna le silence, conseillant à chacun de garder son souffle pour ce qui allait venir. Les yeux plissés, il attendait de voir le dernier signal. Il passa la langue sur ses lèvres sèches, regretta de ne pas avoir laissé Merle les suivre jusque là. Il apercevait le gamin en contrebas, à l’ombre du verger d’où ils avaient jailli.

L’appel des cors le fit sursauter et il prit une grande inspiration avant de hurler.

« Dieu le veult ! »

L’ordre fut repris par les soldats tandis qu’ils levaient leurs échelles, en un grondement déferlant à l’assaut de l’enceinte. Le bois heurta la pierre et les sergents commencèrent l’ascension. Sans opposition, elle fut relativement rapide même si elle était laborieuse. Chacun devait porter plusieurs dizaines de kilos de matériel, entravé par une hast[^hast] ou un écu lacé dans le dos. Il suffisait qu’un pied glissât sur un barreau et la colonne entière pouvait s’effondrer en un amas de corps douloureux. Gosbert avait donc bien insisté pour que chacun se garantisse à chaque instant, ne lâchant une main que lorsque les deux pieds étaient bien en place. Le nez collé au talon du précédent, chaque soldat se hissait peu à eu en une grappe humaine, jusqu’à pouvoir enjamber le parapet.

Gosbert était second sur une de ses échelles et lorsqu’il arriva sur la plateforme, ce fut pour voir qu’ils étaient dans une zone éloignée de tout accès, encadrés par d’autres soldats latins. À leurs pieds, la ville déroulait ses quartiers en un méandre de rues, de venelles et de places. Au nord, l’imposante enceinte du château écrasait la cité étendue à ses pieds, à peine égalée en hauteur par quelques tours de mosquées. À l’ouest, un chevalier avait fait monter sa bannière. Certainement dans l’intention d’aller libérer la porte, il se dirigeait vers des escaliers où se rassemblaient quelques miliciens derrière des boucliers. Voyant que nul secours ne descendait depuis la citadelle fermée, Gosbert sentit son cœur s’emballer. La ville serait tôt prise si les musulmans ne mettaient pas plus de vigueur à la défendre. De la voix et du geste, il encouragea ses hommes à avancer au plus vite. Il ne s’agissait pas de rester en retrait, si quelque pillage pouvait s’offrir à eux.

## Harim, camp du comte de Flandre, fin d’après-midi du mercredi 25 décembre 1157

L’ambiance était morose dans le camp qui se mettait en place péniblement sous les averses éparses. Les hommes n’avaient été guère enchantés de devoir abandonner le siège de la forteresse après avoir pris la ville de Shayzar. De plus, c’était pour aller s’attaquer à une autre citadelle, encore plus près des territoires de l’émir, qui risquait donc d’offrir une résistance bien plus farouche. Gosbert avait été volontaire pour aider à mettre en place la tente chapelle au centre de leur village de toile, afin de pouvoir demander une faveur au prêtre.

Mal soigné après sa blessure au bras, le petit Merle avait vu sa plaie s’infecter et, malgré son courage à se laisser couper par deux fois le membre, il avait finalement succombé après une longue agonie, apaisée du sirop d’opium que le capitaine avait pu dénicher. Il était mort sur le trajet et ils avaient enveloppé son corps dans un drap afin de lui trouver un endroit où l’inhumer correctement. Mais pour cela ils avaient besoin d’un clerc.

Gosbert n’était pas porté sur la religion, mais il estimait qu’il ne faisait pas bon voir sa dépouille disparaître sans qu’un prêtre y ait appliqué les rituels adaptés. C’était en cela qu’on était un humain et pas un animal. Sachant qu’il y avait là de hauts seigneurs ecclésiastiques, il espérait dénicher un chapelain ou un curé de seconde zone qui accepterait de se déplacer pour un simple sergent. Ils étaient bien assez nombreux pour venir leur fustiger les oreilles au moindre écart de conduite !

Lorsqu’il revint dans le périmètre attribué à son groupe, il constata avec soulagement que leurs toiles avaient été tendues entre les arbres et qu’un feu brûlait vivement, le temps de faire les braises pour le souper. Ils avaient encore pas mal de pois secs et du pain, au moins ils auraient le ventre plein d’un repas chaud pour la soirée. Les prostituées avaient leur propre installation, petit auvent agrémenté de branchages, près d’un muret de pierres grises. Il siffla pour attirer l’attention sur lui.

« Le père curé s’en vient pour qu’on mette en terre le Merle. Me faites pas honte et rincez vos faces et vos mains pour venir bénir la tombe du gamin. »

Layla, qui avait pris un peu d’ascendant sur ses compagnes maintenant que Coleite était partie, se rapprocha de lui. Elle avait passé pas mal de temps à s’occuper du blessé et lui avait témoigné une rare douceur, qu’on aurait pu croire étrangère à son caractère. Gosbert en avait peu à peu fait sa favorite, mais même si elle appréciait le changement de statut que cela lui conférait au sein des soldats, elle n’en était pas plus aimable avec lui. Elle n’hésitait guère à lui faire des remarques acerbes qu’aucun homme n’aurait osé concevoir et encore moins exprimer. Étrangement, il ne lui en tenait aucune rigueur.

« On a tressé une petite couronne de feuillages, les fleurs sont fort rares en ces temps.

— Tu m’étonnes ! On est le jour de la Noël ! »

Il remarqua à peine que cela n’éveilla en Layla aucun intérêt. Il avait déjà noté à plusieurs reprises son manque de foi, sans qu’il s’en préoccupe. Peut-être était-elle d’une de ces sectes orientales, dont on dit qu’ils sont chrétiens, mais qui ont leurs coutumes à eux. L’essentiel était qu’elle n’était pas musulmane, arborant une croix discrète autour de son cou.

Un clerc habillé sobrement, la tonsure fatiguée et le visage couperosé se présenta à lui, ses vêtements liturgiques autour du bras. Il avait l’haleine avinée et une tenue à peine plus propre que les soldats. Mais il affichait son plus éclatant sourire, amputé de quelques dents quand il s’exprima, la voix pâteuse.

« C’est ici qu’il faut mettre en terre la Corneille ? »

## Jérusalem, quartier de l’Ânerie, midi du vendredi 9 mai 1158

Gosbert et Raguenel dégustaient un ragoût de porc aux lentilles, confortablement installés à l’ombre d’un vénérable pistachier. Ils goûtaient avec un réel enthousiasme le plaisir d’une cuisine plus élaborée que celle des camps et n’échangèrent pas deux mots de tout le temps que leurs écuelles étaient pleines. Ils partageaient la table avec des marchands et des voyageurs, servis par une vieille femme et sa fille, aussi rébarbatives l’une que l’autre. Mais elles offraient de solides repas à un bon prix.

Cela faisait plusieurs semaines que la campagne était terminée et chacun était désormais rentré chez lui. Il ne demeurait que les soldats du comte de Flandre à patienter, installés pour le moment dans les faubourgs de la ville. Parmi eux, Gosbert et ses hommes attendaient de savoir ce qu’il allait advenir de leur petit groupe. Raguenel, qui servait un chevalier de Jérusalem, avait été envoyé justement pour transmettre les nouvelles instructions. Une fois le ventre plein, il entreprit de se curer les dents avec des échardes arrachées à un bâton le temps que Gosbert ait fini également.

La journée était ensoleillée et Jérusalem voyait retomber l’agitation des dernières semaines. Les pèlerins venus pour Pâques étaient repartis, ou proches de l’être, et on pouvait de nouveau déambuler sans trop de peine. Les marchés étaient plus calmes et on y mangeait mieux pour moins cher. Ceci durerait jusqu’aux célébrations de l’été, pour marquer l’anniversaire de la prise de la cité. C’était néanmoins un événement qui attirait moins les foules, même s’il était l’occasion de festivités aussi enthousiastes.

Voyant que Gosbert dégustait son vin en guise de digestif, Raguenel se décida à transmettre les ordres.

« Ta bande et toi, vous allez vous rendre à la Tour-Baudoin, au nord de la cité. il s’y trouve petite forteresse où vous pourrez prendre vos quartiers.

— Ça veut dire qu’on est là pour un autre été, c’est ça ?

— Je ne saurais dire. D’ici la clôture[^cloturemer] il peut s’en passer des choses… La maladie du Soudan[^nuraldin] pourrait offrir belles occasions dont les barons ne veulent pas se priver. »

Gosbert regarda le fond de son gobelet vide et le reposa, l’air déçu.

« Les gars vont pas aimer s’éloigner de la cité.

— T’as qu’à leur dire que vous paierez la provende bien moins cher là-bas qu’ici. Et c’est une région à raisin, vous crèverez pas de soif ! »

Gosbert acquiesça lourdement, prit le pichet pour demander à ce qu’on le lui remplisse. Il se sentait d’humeur à faire durer le repas, voire à lancer les dés. Le voyant faire, Raguenel chercha autour d’eux, l’air surpris.

« Dis donc, il est où le Merle ? À Césarée il ne s’éloignait guère plus que ton ombre.

— Il est parti…

— Il a dû être bien marri de devoir te quitter !

— Pas tant, je l’ai envoyé avec mon butin chez les miens, porter message à mon épouse et mes filles. Elles l’accueilleront comme un prince, chargé comme il l’est de ma picorée !

— Tu dois avoir grande fiance en lui, adoncques.

— La meilleure ! Jamais il ne m’a déçu. Il fera un bon valet pour mon aînée, j’en ai certeté. Six filles, ça crée du souci. » ❧

## Notes

Il est très difficile de se faire une idée du quotidien des hommes de guerre de l’époque des croisades. Il n’existe pas une façon de faire standard et les coutumes et usages étaient aussi variés que les unités. Beaucoup de choses évoquées dans ce texte sont des reconstructions bâties sur des informations postérieures ou fragmentaires. J’avais réalisé voilà de nombreuses années un petit fascicule qui tentait de regrouper des notes sur le sujet. J’ai eu l’occasion de l’exhumer et de le parcourir de nouveau et cela m’a donné l’envie d’y ajouter un peu de chair, de le rendre plus vivant. J’en ai profité pour en reprendre la présentation afin de le mettre à disposition de ceux qui seraient intéressés, en sachant qu’il n’y a là que quelques notes de lectures compilées.

Le titre de ce Qit’a vient du terme latin par lequel on appelait de façon générale les serviteurs au sein d’une maison, quelle que soit leur fonction : militaire, domestique ou agricole. Il exprime une notion de dépendance à un maître, à rattacher à l’ancien terme *serf* (dont la racine est la même). Cela a donné les termes sergents, servant, service…

## Références

France John, *Western warfare in the Age of the Crusades 1000-1300*, Ithaca, New York : Cornell University Press, 1999

Smail Raymond Charles, *Crusading Warfare, 1097-1193, Second edition with a new Bibliographical introduction by Christopher Marshall*, Cambridge : Cambridge University Press, 1995.

Verbruggen Jan Frans, *The art of warfare in Western Europe during the Middle Ages*, Woodbridge : Boydell press, 1997.
