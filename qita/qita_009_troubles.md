---
date: 2012-07-15
abstract: 1154. L’état de guerre quasi-constant que devait affronter le royaume de Jérusalem, comme tous les territoires latins, eut pour conséquence d’en rendre la gestion interne bien malaisée. Tandis que certains s’employaient à garantir la survie des États, jaillissaient d’étranges querelles entre des adversaires inattendus. L’occasion de retrouver un des héros de *La nef des loups*, mais aussi d’en apprendre un peu plus sur un événement évoqué dans *Les Pâques de sang*
characters: Régnier d’Eaucourt, Onfroy de Toron, Robert
---

# Troubles

## Palais royal, Jérusalem, matinée du vendredi 16 avril 1154

La petite pièce était encombrée d’un immense plateau de chêne, et les nombreuses niches sur les parois étaient chacune munie d’une porte à verrou. Le décor, qui avait connu de meilleurs jours, représentait une tenture plissée en bas des murs et un faux appareillage ocre sur la partie supérieure. Des fenêtres à panneaux de verre éclairaient la scène, mais plusieurs lampes à huile avaient été ajoutées, le ciel demeurant gris et peu lumineux en ce triste matin d’avril.

Assis à la table, entouré de nombreux documents, tablettes de cire, parchemins et autres papiers griffonnés, le connétable Onfroy de Toron réfléchissait, le menton dans le poing, l’air ennuyé. Son impressionnante tignasse n’était que grossièrement domptée en une coupe à l’écuelle. Son cou puissant, tanné par les journées en selle, supportait un visage massif et impénétrable. Le regard inquisiteur était noyé sous des sourcils broussailleux et sa barbe naissante semée de blanc. Tout en lui inspirait le respect et la discipline, comme il seyait à un chef d’armée. Il soupira et repoussa plusieurs feuillets de devant lui, les tendant à un clerc aux yeux tristes et lui indiquant d’un geste de les ranger. Face à lui, assis sur un banc, se trouvaient deux autres personnes à l’allure martiale.

L’un d’eux était, comme lui, habillé d’un magnifique vêtement en soie qui scintillait à chaque fois qu’il bougeait. Plus jeune, il était aussi plus volubile et son visage franc inspirait immédiatement la confiance : un des membres de la maison de Ramla, de Hugues d’Ibelin. Il était tourné vers le dernier homme à la table, un chevalier poussiéreux encore en haubert. Le haut du crâne dégarni, le cheveu très court, celui-ci arborait un air affable, mais pour l’heure il semblait plutôt contrarié. Il avait posé son casque devant lui et se frottait régulièrement le nez, qu’il avait aquilin, d’un geste nerveux.

Le connétable brisa le silence, de sa voix habituée à ordonner et à lancer ses instructions en plein fracas des batailles.

« Si Norredin prend Damas, nous risquons grands soucis. Il sera en mesure de lever un ost pareil aux plaies d’Égypte. Je ne vois pas comment je pourrais lui opposer assez d’hommes.

— La situation n’est pas encore perdue, messire connétable. Le prince de Damas propose même la cité de Baalbek et des territoires à l’entour, pour peu que nous lui prêtions assistance.

— Le souci, Régnier, c’est que je ne vois pas comment lui venir en aide à temps. Vous m’avez dit que les troupes de l’isfahsalâr Shîrkûh étaient arrivées jusqu’où ?

— Au septentrion oriental de la ville, le long d’une des voies.

— S’il offre Baalbek, le roi Baudoin la recevra volontiers, mais il nous faudrait agir même sans cela… »

Soupirant, il reprit la liste des fiefs, suivant du regard l’énumération des hommes qu’il pourrait rassembler.

« En partant dans les jours qui viennent, je devrai me passer des forces d’Hébron et de celles d’Outre-Jourdain. Ceux de Césarée pourraient nous joindre, au moins en partie, plus la Galilée, Sidon et Beyrouth tandis que nous monterons… »

Se tournant vers son autre interlocuteur, il lui demanda :

« Le frère du roi aura temps de convoquer son ban à Jaffa selon toi, Robert ?

— Je ne suis pas sûr qu’il soit possible d’appeler les troupes du sud, d’Ascalon. Mais une partie d’Ibelin, de Rama et de Mirabel pourra arriver à temps. Mon seigneur Amaury suit la situation de près. »

Le connétable hocha la tête en assentiment. Le frère du roi Baudoin était un allié fidèle et sérieux, et son comté de Jaffa-Ascalon répondait généralement sans faille aux appels de la couronne.

« Il me semble bien vain d’espérer arriver aux murailles de Damas avant… »

Il se tourna vers le clerc impassible à ses côtés, qui attendait, un stylet à la main, qu’on lui donne des instructions.

« Quel jour serons-nous dans une semaine ?

— Ce sera la saint Georges, messire Onfroy.

— Ah! S’esclaffa le guerrier. Que voilà bon signe ! Si seulement nous pouvions arriver là-bas pour cette date…

— En ne prenant que quelques contingents d’hommes à pied, parmi ceux du nord, et surtout un fort parti de cavalerie, cela devrait être possible, avança Robert.

— Dans le meilleur des cas, nous aurions dans les trois cents à quatre cent lances à leur opposer.

— Cela pourrait suffire à l’affaire. Cela n’est peut-être qu’une nouvelle tentative pour intimider le prince Mugir al-Dîn » risqua le chevalier.

Régnier d’Eaucourt secoua la tête en dénégation.

« J’en ai grand doute. Cette fois, le fruit semble mûr. Il ne reste aucun opposant de valeur au seigneur d’Alep, et il a su convaincre le peuple.

— En l’affamant, traître qu’il est ! lâcha le connétable.

— Des hommes à sa solde répandent le bruit que le prince préfère nous livrer le grain du Hauran que nourrir la cité. Mais c’est en fait Norredin lui-même qui a fait intercepter les convois du nord.

— N’y a-t-il personne pour dévoiler son mensonge ?

— L’aḥdāṯ[^ahdath] semble avoir pris son parti, et beaucoup préfèrent les Turcs à nous, car ils sont musulmans.

— La belle affaire, ils vont les dépouiller aussi sûrement que des criquets !

— L’assaut à l’encontre de leur cité ne plaide guère en notre faveur, messire. »

Le connétable se mordit la lèvre. Il savait que l’assaut manqué, en 1148, malgré le renfort de nombreux contingents venus d’Europe, demeurerait longtemps un déchirement pour le royaume. Leurs mains avaient été si proches de s’emparer de la cité que la simple mention de l’expédition était douloureuse. Il n’était pas encore connétable à cette période, mais il avait sué sous l’armure comme beaucoup de combattants, pour s’en revenir bredouille, la rage au cœur. Et maintenant leur pire ennemi, le Turc d’Alep, Nūr ad-Dīn, risquait de cueillir le fruit qu’ils convoitaient depuis si longtemps. Il avala quelques gorgées du verre qu’il venait de se remplir puis reprit la parole.

« De toute façon, nous ne pouvons demeurer sans rien faire. Je vais demander au roi de lever le ban pour que nous puissions répondre à l’appel damascène. Il nous faudra… »

Il se tut lorsque la porte s’ouvrit brusquement, dévoilant un sergent d’arme essoufflé, le regard affolé. Onfroy de Toron le dévisagea avec fureur, aussi énervé d’avoir été interrompu qu’inquiet de ce qui avait pu inciter à pareil outrage. Le soldat attendit néanmoins qu’on lui fasse signe de parler, soucieux de marquer son respect au chef des armées du roi.

« Messire connétable, on se bat en les rues de la Cité !

— Pardon ? éructa Onfroy de sa voix tonitruante. Qui se bat ? Où ?

— En le parvis du Saint-Sépulcre, à une portée d’arc à peine… Des hommes de Saint-Jean et ceux des chanoines. On parle de blessés, et peut-être de morts. »

Le connétable laissa échapper un soupir énervé.

« Quelle bande de pisseux vérolés ! N’ont-ils donc rien d’autre à faire ? Et pourquoi me déranger ? Le mathessep, ou plutôt le vicomte, sont là pour ça, non ?

— Il n’y a personne au palais en dehors de vous, messire. Et comme cela est dans la rue à côté… »

Le connétable fit taire l’homme d’un geste, soufflant comme un bœuf, d’un air dépité et agacé. Régnier d’Eaucourt se leva, attrapant son casque.

« Laissez-moi m’en occuper, messire. Je suis en tenue et pourrai me protéger si l’envie leur en prenait.

— Qu’ils essaient de s’en prendre à un chevalier de l’hôtel du roi, et il pourrait se voir quelques écorchés avant la fin du jour ! gronda Onfroy avant de se reprendre d’une voix plus calme : Mais tu as raison, prends avec toi les quelques sergents que tu croiseras et tente de calmer ces maudits clercs. »

Le chevalier salua d’un signe et sortit précipitamment, suivi du soldat. Le connétable secouait la tête, dépité.

« L’ennemi est partout autour de nous et ces maudits tonsurés n’ont que ça à faire, de s’entrebattre !

— On m’avait dit qu’il y avait soucis, mais je ne les pensais pas si fort courroucés.

— Ah ! Ils seraient prêts à s’égorger pour l’étron d’un vilain, s’ils pensaient qu’il doit chier en leur latrine. Maudits moinillons pleins de morgue ! »

## Parvis du Saint-Sépulcre, Jérusalem, midi du vendredi 16 avril 1154

Lorsqu’il arriva au débouché de la rue qui donnait sur le Saint-Sépulcre, Régnier constata que les pèlerins avaient reflué jusque-là, bloquant en partie l’accès. Quelques curieux tendaient le cou, intrigués par l’échauffourée. Avec l’aide de sa poignée d’hommes, le chevalier parvint néanmoins à forcer le passage et s’avança au milieu de l’endroit désert, les poings sur les hanches. C’était la première fois qu’il découvrait le lieu sans une foule allant et venant, ni camelots affairés. Ces derniers avaient tous remballé rapidement leurs pacotilles et s’étaient éclipsés, craignant les affrontements qui n’allaient certainement pas tarder à dégénérer.

Régnier examina l’entrée du sanctuaire. Les portes de bronze avaient été fermées, et seul l’accès en haut des escaliers, à droite, était maintenu ouvert. Quelques visages inquiets y étaient discernables, protégés derrière l’angle du mur et un banc dressé. Au sud, le vaste bâtiment des frères de l’hôpital de Saint-Jean était presque achevé. L’imposante tour qui couronnait l’ensemble était encore en cours d’édification, et des passerelles de bois s’y accrochaient, suspendues dans le vide. Là aussi, plusieurs têtes qui tentaient de se faire discrètes apparaissaient par endroits. La grande porte d’accès aux salles était également bloquée.

Régnier leva le regard, plissant les yeux pour se protéger du soleil qui pointait derrière les nuages. Il salua une fois, la main haute et s’annonça comme chevalier du roi. Il n’obtint pour seule réponse qu’un envol de quelques pigeons, qui se réfugièrent sur une corniche plus élevée, d’où ils auraient une meilleure vue sur l’amusant spectacle des hommes. Cherchant les vestiges d’un éventuel affrontement, Régnier remarqua plusieurs flèches brisées sur le sol, contre le mur de l’église. Mais nulle part il ne découvrit de traces de sang. Il souffla, rassuré, et lança d’une voix forte :

« Il n’est nul besoin de répandre le sang en la Cité. Que deux hommes s’avancent vers moi depuis chaque côté.

— Qu’ils jettent à bas leurs arcs d’abord, rétorqua une voix venue du Saint-Sépulcre.

— Ils sont armés et ont tenté de nous assaillir, répliqua un homme de l’hôpital »

Régnier fit signe à ses hommes d’approcher et les disposa en ligne, séparant la cour en deux zones.

« Voilà, des sergents du roi empêcheront quiconque d’avancer, nul ne pourra les passer outre sans devenir l’ennemi de la couronne. Cela vous suffit-il comme garantie ?

— Et les archers, lança la voix depuis l’église.

— Ils ne tireront pas sur mes hommes et moi, et vous ne saurez les assaillir, ils n’ont donc nulle raison de décocher des traits. »

Après un long moment, une ambassade s’avança depuis chacun des côtés. Pour l’hôpital, un frère de Saint-Jean à la tenue de travail fatiguée, au regard effarouché mais à l’aspect rusé, accompagné d’un massif ouvrier, les mains comme des massues, taillé dans un tronc de chêne et le visage aussi alerte que celui d’un baudet. Face à lui, un chanoine à la robe de qualité plissant sur ses formes rebondies, au crâne d’oeuf et à l’élocution zézayante. Il s’était également adjoint un aide de camp à l’intelligence toute musculaire. Les quatre hommes se dévisageaient avec colère, chacun des deux grands costauds très désireux de prouver à l’autre qu’il pouvait adopter un air plus bovin que lui. Régnier prit quelques instants pour réfléchir à la façon de mener cet entretien avant de se lancer.

« Comment se fait-il que j’encontre clercs en bataille aux portes mêmes du Sépulcre du Christ ?

— Cela ne concerne nullement le roi, précisa le petit chanoine replet, c’est là affaire privée entre les frères de Saint-Jean et nous.

— Cela concerne forcément le roi que deux des plus grands ordres s’affrontent en bataille à une portée d’arc de son palais ! »

L’hospitalier leva un doigt pour s’immiscer dans la conversation.

« Je tiens à démentir. Nous ne portons le fer nulle part, nous ne faisons que défendre nos biens… »

Il n’eut pas le temps de finir sa phrase que l’autre clerc tempêtait et l’insultait, avec des expressions qui auraient certainement choqué plus d’un charretier. Il fallut un petit moment avant que le calme ne revienne.

« Faites-moi récit de ce qui s’est passé, sans en chercher cause, demanda Régnier.

— Des hommes du Sépulcre ont tenté de mettre bas nos échafauds…

— Certes pas, ils voulaient juste calmer un peu l’ardeur des sonneurs de cloches !

— Nous avons droit de cloche, ne vous en déplaise…

— Alors sonnez lorsque c’est temps, pas en plein sermon de mon seigneur le patriarche.

— Qu’a-t-il besoin de faire sermon en le parvis aussi ?

— C’est là son privilège, depuis les temps du bon roi Godefroy, qui était ami du Saint-Sépulcre…

— Calme, amis ! Calmes ! On m’a parlé de blessés, voire de meurtrerie ! Je ne vois pas de sang mais quelques traits au sol. Pouvez-vous me dire… »

Il n’eut pas le temps de finir que le chanoine éructait, excité comme un coq de combat.

« Ils ont tiré moult flèches sur nos gens, voilà. Et en ont blessé de leurs traits assassins !

— Ils venaient à nous brandissant des hasts[^hast], comme larrons en chemin. Heureusement que nous avions un archer de talent, sinon ils auraient bien mis à bas notre tour ! »

Une fois encore, Régnier intervint, séparant les deux adversaires, attendant patiemment qu’ils se lassent de s’invectiver.

« Combien d’hommes sont navrés parmi les vôtres ? » demanda-t-il au chanoine.

Après quelques tergiversations, le clerc finit par avouer d’une voix moins assurée :

« Il y en a un qui a reçu profonde entaille en la main, et un autre blessé à la cheville…

— Par un trait ?

— Non, hésita l’homme. Il a chu en trouvant refuge. Mais si on ne lui avait pas tiré dessus, ce ne serait…

— Pas de mort ? »

Le chanoine secoua la tête en dénégation, presque déçu de n’avoir pas de plus beaux martyrs à brandir pour sa cause. Le chevalier se tourna vers l’hospitalier et lui posa la même question.

« De notre côté, ce sont surtout des atteintes à nos biens, à l’édifice que nous édifions pour les marcheurs de Dieu… Cela risque d’entraver notre sainte mission. »

Régnier hocha la tête et reprit la parole d’une voix ferme.

« Je vais laisser ici quelques sergents, qui veilleront à ce que nul trouble ne vienne se répandre. Le vicomte sera informé et je pense qu’il verra des gens de vos ordres. D’ici là, veillez à ce que vos gens ne s’emportent pas. Pensez à tous ces pèlerins qui espèrent aux portes du sanctuaire ou dans les lits de l’hospice. Il sera temps de vider votre querelle devant la Cour de Sa Sainteté le Pape. »

Les deux clercs hochèrent la tête, contrariés, obtempérant visiblement à contrecœur. Lorsque le petit groupe se dispersa, le chanoine ramassa avec soin toutes les flèches qu’il aperçut, et les brandit en un signe de défi avant de rentrer dans l’église du Saint-Sépulcre. Régnier reprit le chemin du palais, accolé au sud-ouest du sanctuaire. Il se frottait le visage, inquiet. Le royaume venait à peine de se remettre des affrontements entre la reine mère Mélisende et le roi Baudoin. Il ne manquait plus qu’aux clercs de dégainer les armes pour de futiles prétextes tandis que pendant ce temps, Nūr ad-Dīn approchait ses mains de la cité de Damas… ❧

## Notes

Dans les premières années de la décennie 1150, le jeune Baudoin III parvint enfin à s’assurer la mainmise sur le royaume. L’affrontement avec sa mère Mélisende avait créé de nombreuses difficultés internes au royaume alors que la Seconde Croisade échouait devant les murs de Damas.

Le succès à Ascalon supprimait la dernière tête de pont Fâtimide mais ne suffisait pas à éclipser l’échec dans les combats orientaux. Damas demeura hors de portée du pouvoir chrétien et, surtout, fut annexée par Nūr ad-Dīn à ses possessions nordiques. L’émir établissait ainsi dans les territoires musulmans une unité telle qu’il n’y en avait pas eu depuis l’arrivée des Latins en Terre sainte, plus de cinquante ans auparavant. Cela explique pourquoi les efforts des deux adversaires se portèrent ensuite sur l’Égypte, où la dynastie chiite en place vacillait, offrant le pays à qui aurait la force de s’en emparer.

À côté de cela, la fortune des ordres religieux militaires en avait fait des pouvoirs quasi autonomes au sein des territoires chrétiens. Indépendants du royaume laïc, ils avaient été également affranchis par le Pape de la tutelle du clergé local, ce qui provoqua de nombreux remous. En outre, depuis quelques années, parmi ces exempts, les frères de Saint-Jean (communément appelé Hospitaliers) se militarisaient à grande vitesse. Il se voyaient régulièrement remettre des forteresses, comme on le faisait déjà pour l’ordre du Temple. Fort de cette puissance armée, Gilbert d’Assailly, futur grand maître de l’Hôpital, fut d’ailleurs un des chauds partisans de la politique agressive envers l’Égypte.

## Références

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Elisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades*, Damas : Institut Français de Damas, 1967.

Riley-Smith Jonathan, *The Knights of St. John in Jerusalem and Cyprus c.1050-1310*, McMillan, 1967.
