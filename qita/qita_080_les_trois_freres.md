---
date : 2018-06-15
abstract : 1156-58. Depuis l’arrivée des premiers croisés à la fin du XIe siècle, les territoires latins se sont développés essentiellement en Palestine et en Syrie. Pour certains des barons, la décennie 1150 marque le moment où songer à s’étendre vers d’autres lieux. Les trois frères de Milly, chacun à sa façon, commencent à voir l’Égypte comme une province qu’il serait bon d’annexer avant que le pouvoir musulman ne le fasse.
characters : Henri le Buffle, Philippe de Milly, Guy de Milly, Bassam, Ibrahim al-Salar, Balô, Baudoin d’Ibelin, Balian d’Ibelin, Amaury de Jérusalem
---

# Les trois frères

## Misr-Fustat, demeure de Ashraf ben Panoub, fin d’après-midi du mercredi 24 octobre 1156

Confortablement installé sur un matelas épais, Henri le Buffle sirotait un jus d’agrumes en laissant son regard errer par les ouvertures du claustra surplombant la rue. Malgré la chaleur, une intense activité s’y discernait et un brouhaha agité en montait à travers les auvents de toiles usées et de feuilles de palmier racornies. Son hôte, Ashraf ben Panoub, demeurait non loin d’un caravansérail où les marchands arrivés du sud venaient présenter les merveilles acheminées depuis les contrées de l’ouest. Le vacarme des chameaux en train de blatérer, les cris des escortes et les exclamations des boutiquiers inquiets du passage des lourds convois animaient le quartier tout au long de la journée.

Ashraf assistait les négociants méridionaux, parfois originaires de fort loin, soit de Nubie, soit d’au-delà de la Mer Rouge. Il les mettait en contact avec des marchands méditerranéens, des caravanes partant vers le Maghreb ou remontant vers Byzance. Il lui arrivait aussi de prendre des participations lorsqu’il se sentait en fonds. Néanmoins, il s’agissait là de commerce à longue distance dont il n’avait guère les moyens. Il fallait avoir une assise bien plus large et établie que la sienne pour se permettre de voir ses investissements errer durant des mois par des chemins et des voies maritimes risquées avant d’en espérer le moindre bénéfice. Sans même parler des malheurs, catastrophes naturelles et brigandages possibles.

Il hébergeait fréquemment dans sa vieille demeure ses différents partenaires, aimant à accueillir à sa table une grande variété de voyageurs. Il était avide d’anecdotes pittoresques et n’hésitait pas à se faire réciter plusieurs fois les récits les plus incongrus, jusqu’à les coucher ensuite lui-même sur le papier. Il conservait d’ailleurs tout cela en pêle-mêle dans un renfoncement de son majlis[^majlis], annonçant à chaque invité qu’il avait l’intention de compiler tout cela en un bel et définitif ouvrage sur les contes lointains. Au fil des ans, Henri avait vu le bazar de feuillets croître sans que jamais un semblant d’ordre n’y fût initié.

Henri bénéficiait d’une pièce au deuxième étage, où il avait ses repères. Un des escaliers permettait de rejoindre, par une petite cour, une venelle discrète d’où il savait se perdre dans le dédale des rues anciennes. Habitué à se vêtir comme les Levantins, il parlait parfaitement l’arabe de ces régions et avait appris à aimer déambuler dans les quartiers animés de la capitale économique du pays. Il y avait ses usages, achetant brochettes de viande et pâtisseries au miel aux mêmes boutiquiers depuis des années. Il se rendait aussi à l’église proche chaque fois qu’il demeurait là, adoptant les cérémonies coptes locales et laissant entendre qu’il était de rite maronite sans jamais le confirmer formellement, la terre d’Égypte étant traditionnellement tolérante des croyances personnelles. Il était plus important d’être connu et reconnu de bonne réputation, faisant de pieux dons à sa congrégation, que de débattre de ses convictions et pratiques religieuses.

Quelques coups rapides à la porte le firent sursauter alors qu’il glissait dans une léthargie annonçant le sommeil. Il alla pousser le verrou et découvrit avec plaisir que son jeune valet nubien, Balô, avait réussi à trouver Ibrahim al-Salar. Celui-ci s’avança, un sourire large barrant son épaisse barbe brune, démenti par l’éclat froid de ses yeux bouffis. Ibrahim faisait l’essentiel de ses affaires entre Mer Rouge et Méditerranée, allant et venant à travers tout le pays. Indifférent au pouvoir central, il n’y voyait qu’une gêne fiscale à minorer par tous les moyens. Il pourvoyait donc Henri de nombreux et détaillés rapports contre des avantages financiers âprement négociés. Sa faconde et ses manières amicales le rendaient malgré tout sympathique.

Il salua avec chaleur et respect, conscient de naviguer en eau trouble et habile à ménager chèvre et chou à chaque moment. Après de rapides échanges formels de politesse, ils allèrent s’installer sur les matelas formant majlis dans la niche qui surplombait la rue. Henri fit apporter quelques rafraîchissements agrémentés de fruits secs et d’olives épicées, puis envoya Balô effectuer quelques courses pour la soirée. Il préférait avoir le moins de témoins possible de ses discussions, sachant parfaitement, pour les acheter, qu’il n’était nul domestique à l’intégrité parfaite. Observant son petit manège, al-Salar adoptait un air narquois, semblant s’amuser de cette situation ambiguë. Henri le suspectait de vendre ses services à bien d’autres, se garantissant des représailles éventuelles par le marchandage indiscriminé de ses informations.

« J’ai quelques joyaux pour vous, mon ami. De quoi tailler bijou à turban. »

Il avala quelques pistaches, ménageant ses effets. Henri prit une gorgée de sa boisson, sans trop marquer son intérêt.

« L’étoile de Nasir al-Dawla Yaqut tente de s’approcher du firmament, au risque de sombrer dans les ténèbres. Il semble trouver Qûs[^qus] trop petit pour ses ambitions, désormais. Sa victoire de Dumyât semble lui avoir donné des envies d’un plus grand pouvoir.

— Il semble avoir donné tous les gages de fidélité au vizir Ruzzîk, pourtant. Comment s’est-il dévoilé à toi ?

— Oh, il demeure encore tapi, mais fourbit ses armes et apprécie l’étendue de son bras. De nombreuses allées et venues ont été signalées entre le palais, sa citadelle et les appartements de Sitt al-Qusur. Je ne suis pas convaincu que celle-ci va chanter les louanges du vizir, dont chacun sait qu’elle trouve l’indépendance un peu trop marquée par rapport à son calife de neveu. »

Henri prit un peu de temps pour estimer les répercussions de ces nouvelles. Cela recoupait des rumeurs et informations entendues au Caire. Il savait que des dissensions avaient grandi entre le vizir et la famille régnante, après leur enthousiasme initial. Beaucoup, dont la tante du calife que venait d’évoquer al-Salar, s’inquiétaient des convictions religieuses de ce dernier. Il était arménien d’origine, converti au chiisme, mais avec des préférences marquées pour la branche duodécimaine, dont ils n’étaient pas. Les subtilités confessionnelles échappaient complètement à Henri qui n’en percevait, comme pour sa propre croyance, que les implications politiques.

Le vizir Tala’i ben Ruzzîk avait restauré l’autorité vacillante du calife dans tout le pays en quelques années. Au début, Henri avait pensé que c’était une bonne nouvelle pour le royaume de Jérusalem. Un pouvoir égyptien chiite indépendant de la Syrie sunnite permettait d’espérer des désaccords sur lesquels bâtir pour empêcher toute alliance durable. Mais avec le temps, il commençait à s’inquiéter de l’efficacité du vizir. Il avait un bel entregent au niveau du palais et une certaine estime populaire, mais devait composer avec une hiérarchie militaire assez turbulente. Afin de les canaliser, il ne fallait pas qu’il fasse de l’Égypte un adversaire aussi redoutable que la Syrie de Nūr ad-Dīn.

« Quelles sont les chances du gouverneur de Qûs, selon toi ?

— Il possède une belle assise là-bas, contrôlant les richesses venues d’Orient et y faisant sa picorée, mais al-Qahira[^caire] maîtrise l’armée. Et de nos jours, al-Qahira, c’est le vizir. Al-Qusur n’est qu’une femme, elle a besoin d’un champion et pense user de Yaqut comme elle l’a fait de Ruzzîk. Mais ce dernier a fait sa pelote, il ne sera pas aussi aisé à vaincre qu’Abbas et les siens. Il est aimé assez et a toujours bien pris garde à se revendiquer de l’appui de la famille d’al-Fa’iz. Les plans de la tante du calife me semblent trop tortueux pour aboutir. À croire qu’elle aimerait devenir vizir elle-même ! » conclut-il, amusé.

Henri accorda un sourire connivent à son invité, sans trop se dévoiler toutefois. Une telle situation était dangereuse pour le califat et il ne doutait pas un instant que les espions de Nūr ad-Dīn seraient informés aussi vite que lui. Bien que, ces derniers temps, la plupart des affrontements aient eu lieu au nord, surtout au niveau de la principauté d’Antioche, les campagnes de l’été prochain allaient se décider autour de l’équilibre des forces en Égypte. De nombreux émirs poussaient le vizir à la guerre, estimant qu’il était humiliant de verser tribut aux Latins. Beaucoup gardaient un souvenir amer de la perte d’Ascalon aux mains de Baudoin et souhaitaient une revanche. Quelques mois plus tôt, ils n’avaient d’ailleurs pas hésité à lancer une opération maritime sans son consentement.

À l’origine, Henri espérait demeurer dans Fustat pendant la clôture des mers et ne remonter qu’aux beaux jours vers le nord, garni de toutes ses informations. Il lui semblait désormais nécessaire de trouver rapidement un contact susceptible de transporter une lettre chiffrée à destination de l’hôtel du roi. Même s’il n’y avait aucun élément définitif à y inclure, il était important que chacun là-bas sache que la situation en Égypte était mouvante, et que l’accord de paix était donc bien fragile. Il faudrait aussi en informer son propre frère Guy, à charge pour lui de se garantir que son seigneur le comte de Jaffa soit bien au fait des aléas de la politique de son voisin.

## Naplouse, palais seigneurial, veillée du mardi 12 août 1158

Après une journée brûlante de soleil, la petite cour du seigneur de Naplouse, Philippe de Milly, s’assoupissait tranquillement en assistant à une représentation de théâtre d’ombres dans la grande salle où ils avaient soupé. Si les artistes en étaient de qualité, leur répertoire n’était pas du tout aux goûts de l’assemblée. Ils narraient les aventures d’un voyageur marchand, larron à l’occasion, lors de ses périples en mer. Le maître des lieux, n’y tenant plus, finit par se lever et demander qu’on leur offre un spectacle seyant à chevaliers et pas négociants de souks. Le temps que les bateleurs s’organisent, quelques musiciens vinrent égayer l’atmosphère devenue pesante. Dans l’attente, Philippe se fit servir un peu de vin aux fruits, allongé d’eau, puis entreprit de profiter de l’intermède pour discuter un peu avec Baudoin et Balian d’Ibelin, ses invités du jour. Ils revenaient d’Acre, en mission pour le comte de Jaffa. Le plus jeune des deux, Balian, était à peine majeur, mais il avait la morgue et le caractère bien trempé du vieux Barisan, comme toute sa fratrie. Il approuvait d’un air hargneux toutes les affirmations de son frère Baudoin, aussi impétueux que vigoureux.

« Vous ne m’en avez guère dit sur vos terres. Qu’en est-il de la libération de votre aîné ?

— Nous collectons pour la rançon, et le sire roi nous a garanti qu’il ne faillirait pas à notre débours. Il ne cesse de louer la vaillance de notre maison, qui l’a sorti du piège de Meleha.

— J’enrage de n’avoir point été aux côtés du roi en cette occasion. Ce Norredin a une audace et une chance insolentes !

— Les marches regorgent d’espies et de nomades à son service ! Père nous a toujours appris à leur tenir la bride serrée. Sans quoi ils s’empressent de nous trahir pour le soudan[^soudan2]. »

La mention du vieux patriarche, décédé quelques années plus tôt, ramena Philippe plusieurs décennies en arrière, quand il n’était lui-même qu’un jeune bachelier comme ces deux cadets face à lui. Néanmoins, à la différence d’eux, il avait l’espoir de finir à la tête d’une riche seigneurie au centre du royaume. Barisan d’Ibelin était un fin stratège, autoritaire et péremptoire, se souvenait Philippe. Pourtant, nombreux étaient ceux qui auraient vendu père et mère pour se ranger dans son échelle durant les batailles. Il était féroce dans l’affrontement et audacieux dans ses tactiques. Il y avait gagné ses terres, à la frontière du territoire d’Ascalon, alors aux mains des Fātimides.

« Il est triste que votre père n’ait manqué que de peu la prise d’Ascalon. Il avait bien mérité d’y voir flotter la bannière royale.

— De certes, il n’aurait souffert de voir la milice du Temple s’y engouffrer les premiers ! confirma Baudoin.

— J’encrois qu’il aurait flairé le danger de s’y laisser prendre dans la nasse et n’aurait pas poingné à la franche marguerite comme eux. Ton père était aussi avisé qu’adroit à briser les lances. »

De mauvais gré, Baudoin acquiesça en silence à cette douche froide de son exaltation guerrière. Soucieux de ne pas voir cette rebuffade montée en épingle, il lança un regard sévère à Balian. Ce dernier plongea le nez dans son gobelet de verre et en but le nectar sans mot dire. Indifférent aux émois des jeunes chevaliers à ses côtés, Philippe continuait son voyage mémoriel. Lui-même gardait un souvenir assez vif de son père, homme au caractère difficile, quoique tendre avec son épouse. Il avait enseigné à ses fils l’importance du devoir et de la parole donnée, essentiellement par l’exemple qu’il leur présentait chaque jour. Philippe conservait généralement autour du cou une petite croix reliquaire qu’il avait trouvé dans ses affaires après sa mort.

La fin d’une pièce musicale le tira de sa torpeur et il adressa un sourire poli aux deux jeunes bacheliers demeurés muets pendant son introspection. Ils étaient familiers du comportement parfois étrange du seigneur de Naplouse, pour l’avoir côtoyé depuis leur enfance.

« De votre côté, nulle agitation parmi les nomades et les mahométans ? Depuis Carême, le vizir du Caire ne cesse de lancer chevauchées en nos terres du sud. Avez-vous idée de ce qui se trame ?

— Comme vous le dites, ce sont chevauchées de pillards et non ost de conquête. Ils évitent soigneusement les places fortes, frappant là où ils espèrent la résistance nulle.

— Ils ont tout de même tenté de forcer Vaux-Moïse[^vauxmoyse], et envoyé espies jusqu’à Montréal. Sans même parler de leur percée jusqu’à Bethgibelin.

— Quelques marcheurs de Dieu se sont mis en tête de leur rendre l’honneur et pensent poindre depuis Gaza.

— Vous les allez joindre ?

— En l’absence de Hugues, je ne peux décider en dehors du Conseil des barons du sire Amaury. Ce ne serait que de moi, j’y porterais ma bannière avec plaisir. Ils ont trop l’habitude qu’on les laisse tranquilles lors des campagnes estivales. »

Surpris par la pertinence de la remarque, Philippe de Milly hocha le menton. Les affrontements avec les principautés d’Alep, Hama, Hims ou Damas étaient usuels, les protagonistes évoluant sur les territoires des uns et des autres en continuelles fluctuations. Il était par contre très rare d’aller au-delà des sables de Gaza au sud. Les Égyptiens profitaient de leur suprématie maritime et des contreforts du désert pour préserver leurs propres provinces, tout en portant le fer et le feu chez leur adversaire latin.

« Sans l’appui du roi ou du comte de Flandre, cela ne peut aller bien loin. Il faudrait longuement préparer pareil assaut si on veut n’y point faillir. Il a fallu des décennies pour prendre Ascalon et le désert ne la protégeait pas. ».

Découvrant alors que la musique s’était tue et que l’on attendait un geste de sa part pour reprendre le spectacle de théâtre d’ombres, il sourit aux présents puis, d’un mouvement de tête, invita les artistes à commencer. Il lui fallait repenser tranquillement à cette idée d’aller porter la guerre en terre d’Égypte. La première chose à faire était de se garantir que jamais il ne pourrait en provenir d’assauts concertés avec les forces syriennes. En aucun cas les troupes royales n’auraient les moyens de combattre sur deux fronts. Il étendit ses longues jambes, sirota un peu de son vin et suivit sans vraiment les voir les personnages de cuir dont les formes ondulaient sur la toile de lin. Au moins était-il question de hauts faits d’armes cette fois.

## Ascalon, château comtal, matin du mardi 30 septembre 1158

Comme chaque matin depuis son récent mariage, Amaury badinait en prenant son premier repas de la journée. Vêtu d’une simple chemise, il n’était pas encore toiletté, rasé, ni coiffé. Tout en profitant des premières lueurs, il aimait à se faire lire des poètes, sollicitant son chapelain dont il avait exigé qu’il ait une voix douce et chantante. En outre, son humeur, déjà habituellement avenante dès le réveil, était outrancièrement légère depuis qu’il avait convolé en noces. Il n’avait que chansons guillerettes, citations rimées et bons mots aux lèvres, le sourire ne les quittant que peu.

Les jours les plus torrides de l’année étant passés, il s’installait devant les vastes fenêtres qui donnaient à l’est, vers la plaine, cherchant à discerner dans le lointain les fortifications qui s’étalaient jusqu’aux reliefs judéens enserrant la Rivière du Diable[^rivierediable]. Comme chaque matin, à peine vêtu, il avait assisté à la messe et se sentait l’âme en paix. Le gros de la saison des batailles était derrière eux, sans trop de heurts pour le royaume, et la cour se préparait à un important voyage au Nord afin de rencontrer le basileus byzantin. Amaury se demandait si ce souverain serait conforme à toutes les histoires qu’on lui narrait.

Selon lui, les Romains, comme ils aimaient à se nommer, formaient le plus étrange des peuples, avec des coutumes dont il n’arrivait à comprendre la logique. La pompe dont on entourait le dirigeant lui paraissait tout particulièrement absurde. Même s’il avait un grand respect envers son frère Baudoin, il ne le considérait que comme le premier d’entre les barons. Le roi des Romains, Manuel, semblait plus sacré que le Pape  pour les siens et le protocole autour de sa personne encore confus et insensé.

Entendant des pas s’approcher, il se retourna pour voir arriver Guy le François, son sénéchal. C’était un fidèle d’entre les fidèles, que sa mère lui avait conseillé. Homme discret et d’un caractère avenant, Amaury le considérait, ainsi que ses frères Philippe et Henri qu’il côtoyait depuis son enfance, plus comme un oncle que comme un vassal. Pourtant, même si Amaury se montrait parfois assez familier, jamais Guy ne s’était permis la moindre indélicatesse. En outre, il était d’une grande rigueur dans la tenue du domaine. Il supervisait toute l’administration territoriale, finances et forteresses, sans que les plaintes d’abus n’en viennent trop fort aux oreilles du comte, ce qui était à son honneur. Il avait malgré tout la désagréable habitude de ne jamais laisser un point litigieux en suspens, poursuivant son seigneur de ses questions jusqu’à obtenir une réponse.

Amaury fit une moue en constatant qu’il y allait avoir une nouvelle fois assaut : Guy apportait avec lui les cartons et esquisses qu’il avait fait exécuter par quelques maîtres d’œuvre. Depuis le début de l’année, il semblait pris de frénésie de construction. Quand ce n’étaient pas des murailles à exhausser, c’étaient des tours à hourdir ou des fossés à curer.

Le sénéchal inclina son fin buste avec respect, un sourire amical sur le visage. Sa silhouette agitée, habillée comme toujours à la dernière mode, trahissait aussi sa bonne humeur. Il savait que le comte n’était guère enclin à parler boutique et avait donc choisi de le traquer en sa tanière avant qu’il ne s’échappe, en quête d’un lion ou d’une gazelle à chasser.

« le soudan est-il à nos portes que tu m’assailles dès potron-jacquet[^potronjacquet] ?

— Je m’emploie à ce que jamais il ne puisse ainsi nous surprendre, sire comte » répliqua, amusé, Guy.

D’un geste, Amaury l’invita à sa table et lui fit porter oublies, compote de fruits et fromage frais. Lui-même ne mangeait que peu, malgré sa silhouette grasseyante, mais il tenait à faire bon accueil en son hôtel. Il ne supportait pas les maisons où l’on pleurait le pain aux voyageurs ou à la domesticité.

« J’avais espoir que vous pourriez examiner ces propositions. J’ai trouvé un fort habile arpenteur ermin[^ermin] qui se propose de creuser citerne grande assez pour augmenter les réserves d’eau de la cité.

— Encore un bassin ? N’en avons-nous assez ?

— C’est aux fins de compléter les terres gastes[^gaste] au long du chemin outre la porte Jérusalem.

— Je ne suis mie aise à l’idée de fournir ainsi de quoi abreuver ceux qui attaqueront mes murailles.

— Il n’est question que cela ne puisse aller outre quelques semaines, et certes pas un mois plein, pour forte troupe. »

Amaury déchira un petit coin de crêpe qu’il se mit à grignoter, tournant la tête vers la plaine, les yeux dans le vague. Pour autant, Guy ne désarma pas.

« Pour le moment, nous ne pouvons accueillir forte troupe plus d’un jour ou deux…

— Au rebours de toi, je ne suis pas convaincu que nous pourrons faire d’Ascalon la Séphorie méridionale. Il me semble que Gaza serait bien plus indiqué, au plus près des sables.

— Il ne serait guère aisé de faire admettre royale décision aux hommes de la milice du Temple. D’autant que la capture de leur maître Bertrand de Blanquefort les pousse à ne regarder que vers Damas et Alep. »

Amaury grommela à l’évocation des Templiers. Comme tous les barons de Jérusalem, il en admirait la vaillance autant qu’il en déplorait l’esprit d’indépendance. Bertrand de Blanquefort avait récemment obtenu du pape d’être appelé *Maître par la grâce de Dieu* et il exhibait un bâton, symbole de son statut, qui n’était pas sans ressembler à un sceptre. Suffisamment en tout cas pour que le roi et ses proches s’en émeuvent auprès du Saint-Père, sans résultat.

Le jeune comte de Jaffa tendit la main vers les documents du sénéchal.

« Voyons ce qu’il en est de ces cartons…

— J’ai aussi une estime du coût. Avec les esclaves que nous pouvons faire travailler à l’épierrage et les moellons que nous possédons déjà, cela ne serait que de peu de prix pour le trésor comtal.

— Je te laisse apprécier ces choses. Il ne m’est pas tant important de compter pécunes, tant qu’il m’en reste assez pour mon hostel et mes domaines. »

Amaury examina le tracé des citernes qui devaient prendre place sur des terrains abandonnés au pâturage lâche. Non loin de là, accolée à une maison forte, une grange était envisagée pour y entasser viandes et fournitures. La plupart des soldats se débrouillaient par eux-mêmes pour leur pitance, mais il était habituel d’approvisionner au moins l’hôtel royal. Sans compter que le fourrage devait être en quantité pour nourrir toutes les montures en plus de l’avoine. Le peu d’herbe qui n’avait pas grillé en été ne suffisait guère et il fallait donc prévoir des réserves de foin quand les pluies de printemps avaient verdi les campagnes.

« Tout ça pour aller se perdre au parmi des sables d’Égypte… » soupira Amaury. ❧

## Notes

La structure de ce Qit’a est née de mon désir décrire un texte qui illustre au mieux le projet que servent ces textes. Cette mosaïque de destins propose au regard une vision multipolaire de l’Histoire et tente d’en évoquer la part insaisissable, tout en possédant un arc narratif lâche qui indique le mouvement de l’Histoire. La décennie qui va commencer au sein d’Hexagora verra les campagnes en Égypte et je souhaitais montrer comment des événements de grande importance pouvaient avoir leur origine dans un enchaînement bien plus riche et complexe qu’une décision autocratique d’un dirigeant dont le devenir serait aussi exceptionnel que le caractère. Je postule que l’histoire est plus souvent qu’on ne le dit l’œuvre de multiples et désordonnées influences dont on retrace une cohérence après coup, en un arrangement vers un possible comme dénoncé par Bergson.

Par jeu, car il m’est toujours nécessaire de ne pas prendre tout cela pour une entreprise trop sérieuse, il m’a semblé évident pour narrer les agissements d’illustres inconnus, de nommer cela *Les trois frères*, en hommage à d’autres Inconnus et leur film éponyme. Les héros de ce Qit’a, qui occupèrent une place de premier plan dans la politique du royaume de Jérusalem dans ces décennies, présentaient toutes les caractéristiques pour que je puisse leur prêter ce rôle de messagers de la Fortune. À part pour Philippe de Milly, qui devint par la suite grand maître du Temple, nous n’en savons guère sur eux, ce qui laisse une belle marge de manœuvre pour l’imagination et la reconstruction a posteriori.

## Références

Barber Malcolm, « The career of Philip of Nablus in the kingdom of Jerusalem » dans Edbury Peter, Phillips Jonathan, *The Experience of Crusading*, Volume 2, Cambridge University Press : 2003, p. 60—78.

Cahen Claude, Ragib Yusuf, Tahir Mustafa Anwar, « L’achat et le waqf d’un grand domaine égyptien par le vizir fatimide Tala’i b. Ruzzik », dans *Annales Islamologiques*, volume 14, Institut Franaçais d’Archéologie Orientale, Le Caire : 1978, p.59-126.
