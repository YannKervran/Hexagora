---
date : 2020-04-15
abstract : 1160. Après de longues fiançailles qui permirent à Ernaut d’avoir de quoi fonder honorablement une famille, il est enfin temps pour Libourc et lui de s’unir officiellement devant leur communauté.
characters : Libourc, Ernaut, Lambert, Sanson, Mahaut, Godefroy le Borgne, Perrotte, Osanne, Droart, Eudes, Père Galien, Ysabelon, Houdart, Sylvette, Jaque, Tiphaine, Perrin, Raoul le Scribe, Le François
---

# Accordailles

## Jérusalem, cour de l’église Sainte-Agnès, matin du dimanche 24 avril 1160

Quelques choucas tournaient, lançant des cris depuis les hauteurs afin d’éloigner la foule importune d’humains massés auprès de l’amandier. En plus des proches et des amis d’Ernaut et Libourc étaient venus quelques curieux et surtout la congrégation habituelle, dont la messe hebdomadaire allait s’animer d’une cérémonie nuptiale. Avec une telle presse, Ernaut ne voyait que difficilement le petit groupe autour de sa belle-famille, et Libourc lui était cachée à dessein. Il avait malgré tout réussi à apercevoir rapidement sa future épouse, méconnaissable avec sa coiffe de mariée.

Lui était habillé d’une magnifique cotte de laine orange, aménagement d’un don de l’hôtel royal pour l’occasion, agrémentée de nombreux triangles d’aisance taillés dans un ’aba[^aba] de soie sauvage ocre clair. Ses proches témoins, qui porteraient le voile nuptial durant la bénédiction, se tenaient près de lui, aussi endimanchés qu’il était possible. Les vêtements sentaient la menthe et la sauge, les cheveux étaient lavés et peignés, les barbes rasées. Lambert, responsable de la bague et des treize deniers, contrôlait sans relâche que chacun savait ce qu’il avait à faire et semblait encore plus fébrile que son jeune frère. Sa récente épouse, Osanne, avait pris en charge le fleurissement de l’autel et distribuait quelques couronnes aux filles qui le souhaitaient, embaumant le lieu de fragrances légères.

La porte de l’église s’ouvrit en long grincement espéré et le calme se fit, comme une onde en reflux. Le père Galien apparut, aussi solennel qu’un roi, flanqué d’un enfant de chœur porteur d’un petit bol d’eau bénite où reposait un goupillon. Le prêtre leva la main en un silencieux salut. Puis, d’un regard, il invita Ernaut à se présenter face à lui. Il empoigna l’aspersoir et entama l’*Asperges me*[^aspergesme] d’une voix forte, soutenue par le soprano du jeune garçon. Bientôt la congrégation entonnait, plus ou moins correctement, l’antienne tout en recevant la bénédiction en même temps que les gouttelettes. Gonflé d’enthousiasme, Ernaut vociférait si passionnément que Lambert se sentit obligé de le rappeler à l’ordre d’un coup de coude, d’autant qu’il était toujours aussi approximatif dans sa récitation du latin.

L’aspersion et le chant terminés, le père Galien renvoya l’enfant et, d’une voix habituée au prêche et aux sermons, prit la foule à partie.

« Bonnes gens, nous avons proclamé en la sainte église trois bans par trois jours entre ces deux personnes. Présentement déclarons donc le quart ban : que s’il y a aucun ou aucune qui sachent empêchement par quoi l’un ne puisse avoir l’autre par loi de mariage, si le dit sous peine d’excommuniement. »

Le silence qui s’abattit ne laissa s’échapper que frottements de pieds, chuchotements d’enfants, toux discrètes et raclements de gorge. Pendant ce temps, d’un geste de la main, le prêtre indiqua à Sanson d’approcher, tenant sa fille par la main. Ernaut sentit son cœur fondre en la découvrant rayonnante comme un soleil, ses fossettes accentuant son sourire gracieux et la malice de ses yeux emplis de joie. Elle portait une robe de laine couleur miel, dont il lui semblait qu’elle avait capté la chaleur de l’astre du jour. Le visage encadré dans un voile très strict, certainement fixé par Mahaut, elle paraissait soudain plus femme encore qu’il ne l’avait jamais vu. Son regard rivé à lui, elle se laissait guider sans prendre garde où elle mettait les pieds, trébuchant à plusieurs reprises sur les racines affleurant.

Sanson vint jusqu’à Ernaut, lui accordant lui aussi de grands sourires, et se plaça entre les deux futurs époux. Pendant ce temps, le servant de messe était revenu avec une croix qu’il brandit tour à tour devant les mariés, afin qu’ils la baisent. Embrouillé par l’émotion, Ernaut ne se remémora ce qu’il devait dire que grâce à l’enfant de chœur. Le prêtre avait concédé l’usage de la langue vulgaire, ayant reconnu que le verbiage de Ernaut en latin ne rendait pas justice aux formules consacrées, mais cela ne rendait pas la tâche plus aisée au jeune homme.

« Nous t’adorons, Seigneur Jésus-Christ, et nous te bénissons, parce que par ta sainte Croix, tu as racheté le monde. »

Le père Galien adoptait toujours un pas de sénateur dans le déroulement des offices et bénédictions, laissant s’écouler lentement le temps pour que chacun puisse s’imprégner du sérieux de la situation. Il accentuait donc le plus insigne des gestes, la moindre phrase. Il inclinait la tête, les yeux baissés, en guise de ponctuation aux répons, marquant le rythme de ses mimiques silencieuses. Même si ce n’était pas un sacrement à proprement parler, le mariage était un moment essentiel dans la vie d’un chrétien, et rien ne devait en entacher la gravité. Solennel, il se tourna avec Sanson et d’une voix forte, déclama la formule consacrée.

« Veux-tu donner cette femme, Libourc, en épouse à Ernaut ?

— Je le veux. »

Il s’adressa alors en un sourire à la jeune fille.

« Libourc, veux-tu recevoir cet homme comme mari ?

— Je le veux. »

Ernaut sentit son cœur s’emballer en entendant ces quelques mots, mais respira un grand coup alors que l’officiant s’apprêtait à l’interroger.

« Ernaut, veux-tu vraiment celle-ci comme épouse ? La garder saine et malade tout le temps de sa vie, comme un honnête homme doit garder son épouse ? Lui faire fidèle société de ton corps et de tes biens ?

— Je le veux.

— La reçois-tu dans ta foi ?

— Oui

— Et toi Libourc, le garderas-tu dans ta foi ?

— Oui. »

Prenant alors la parole, un peu vite pour le goût du prêtre, Sanson déposa la main de Libourc dans celle d’Ernaut.

« Je te donne et remets Libourc comme épouse, en sorte que tu lui gardes toujours ta foi et que tu ne la renvoies jamais pour une autre femme. »

En disant cela, son regard était bien clair, qu’au-delà du bonheur qu’il éprouvait, l’avertissement n’était pas que de pure forme. Il en oublia la formule de bénédiction, que le père Galien lança avec un peu de brusquerie.

« *Et ego conjungo vos in nomine Patris et Filii et Spiritus Sancti*[^etegoconjungo]. Amen. »

D’un mouvement de la tête il fit ensuite comprendre à Lambert d’approcher. Celui-ci tenait bien haut l’anneau dans une main et la bourse avec treize deniers dans l’autre. Il tendit les monnaies à son frère qui les remit cérémonieusement à Libourc. Celle-ci en préleva dix pièces qu’elle remit une à une au jeune servant, en gardant trois pour elle qu’elle glissa dans l’aumônière brodée qui pendait à sa ceinture. Puis elle replaça sa main droite dans celle d’Ernaut. Celui-ci prit alors la bague et la présenta tour à tour à chacun des doigts.

« *In nomine Patris*… *Filii*… *Et Spiritus Sancti*… »

Puis il recommença à la main gauche, où il enfila l’alliance à l’annulaire en disant « *Amen* ». Il lui sembla un court instant qu’il n’aurait pas assez d’air pour emplir ses poumons tellement sa poitrine se dilatait de bonheur. Sa voix lui parut mince filet tandis qu’il  ânonnait la formule patiemment apprise.

« De cet anneau je t’épouse, et de cette dot je te doue. »

Inspirant longuement avant de se pencher vers celle qui allait désormais partager sa vie, il vint déposer sur ses lèvres un délicat baiser de paix, ému de l’embrasser devant une telle foule assemblée. Il n’entendit même pas le *pax tecum* du prêtre qui signifiait la fin de la cérémonie.

Les portes furent largement ouvertes et la congrégation put prendre place dans la nef tandis que les jeunes mariés avançaient devant l’autel, derrière l’officiant. La messe n’avait rien de particulier, si ce n’est juste avant la communion. Le père Galien fit alors s’approcher plus encore Ernaut et Libourc, pour qu’ils s’agenouillent sous une toile tenue par leurs témoins et, là, il les bénit de nouveau.

« Et ambo ad beatorum requiem atque ad coelestia regna perveniant, et videant filios filiorum suorum usque in tertiam et quartam generationem et ad optatam perveniant senectutem[^etambo]. »

Quand ils sortirent de la fraîche bâtisse pour retrouver la chaleur empoussiérée de la cour, Ernaut et Libourc se tenaient la main avec vigueur. Ni l’une ni l’autre ne retint le moindre message de félicitation qu’ils reçurent alors, emplis qu’ils étaient de cette communion charnelle.

## Jérusalem, hostel d’Ernaut et Libourc, veillée du dimanche 24 avril 1160

La journée était passée comme un rêve pour Libouc. Avec Ernaut, ils avaient à peine le temps de profiter l’un de l’autre qu’ils étaient sollicités pour des embrassades ou des bons vœux, aussi enthousiastes que répétitifs. Partout les enfants courraient, la bouche pleine d’amandes et de galettes. Quelques adultes dansaient, rythmés par des musiciens loués pour la soirée. Dans un coin, les plus âgés discourraient sur l’avenir du couple, estimant les probabilités de naissances dans les mois à venir à l’aune des attentions qu’ils devinaient entre les deux jeunes gens. Ils évaluaient en outre les chances de succès futurs et la richesse du foyer à l’importance des avoirs présentés dans la grande salle.

Libourc ne pouvait se retenir de sourire devant l’étalage un peu pompeux des céramiques colorées, des verres et des étoffes. Ernaut avait voulu lui faire quelques surprises et elle n’était pas convaincue du bien fondé de certains achats. Il avait en effet tenu à faire les choses en grand et avait dépensé pratiquement toutes ses économies pour acquérir de la vaisselle de prix, exposée sur la table pour y proposer les plats, des toiles de qualité tendues sur les murs et du mobilier en suffisance, dont un magnifique coffre peint qui servirait à conserver leur linge à l’abri.

Il était également particulièrement content d’un châssis orné de verre qu’il avait fait mettre à une des ouvertures. Avec fierté, il expliquait à qui voulait l’entendre que cela permettrait à Libourc de tirer l’aiguille en tout confort, profitant de la lumière toute l’année sans jamais craindre les courants d’air. La jeune femme savait néanmoins que l’enthousiasme de son désormais mari serait rapidement tempéré par la maîtrise des clefs qu’elle arborait triomphalement à la hanche, comme un bachelier sa nouvelle épée.

Libourc essayait de se conformer à l’image qu’elle s’était formée d’une parfaite maîtresse de foyer, peinant à abandonner le quotidien de la gestion des plats et du service à sa mère et ses amies. Mahaut ne cessait de lui répéter qu’elle aurait bien vite sa suffisance de toutes ces corvées et qu’il lui fallait profiter de cette rare journée où on avait soin de l’en décharger.

Elle se contentait donc de s’assurer que chacun avait boisson et mets à volonté, s’efforçant de faire bon accueil à tous. Elle savait que pour ses désormais voisins, cela pouvait conditionner leurs relations à venir. Elle espérait que ces citadines ne trouveraient pas ses manières trop rustiques, elle qui vivait depuis des années dans un modeste casal. Tiphaine et Ysabelon, épouses des collègues les plus proches d’Ernaut lui semblaient bienveillantes à son égard, même si la dernière semblait un peu revêche. L’épouse d’Eudes, au contraire, était une femme avenante et engageante. Elle avait déjà proposé à Libourc de lui faire découvrir les meilleurs endroits en ville où approvisionner son cellier, ainsi que quelques tours de main en couture qui ne seraient pas inutiles, vu le rude traitement que les hommes imposaient à leur tenue dans leur service.

Libourc ne prit conscience que tardivement que le soleil était couché depuis un moment, ayant abandonné la place aux lueurs ambrées des lampes. Au dodelinement de la tête de ses parents, elle comprit qu’il était temps pour Ernaut et elle de se retirer, afin que les plus âgés puissent assister aux ultimes bénédictions. Le père Galien était toujours là, sirotant un vin miellé tout en discutant avec Le François, chef du quartier et responsable des portes y menant.

Libourc rejoignit son époux. Elle avait flotté depuis le matin dans un état second et, si le bonheur transfigurait ses traits, la fatigue commençait à s’y manifester. Comprenant cela sans un mot, Ernaut acquiesça à l’idée d’achever la journée dès à présent. Du moins sa partie publique.

Il ne fallut guère de temps pour rassembler tout le monde à l’entrée de la chambre, en un amas tassé où chacun tordait le cou pour tenter d’y voir. Seuls les intimes avaient accès aux abords du lit où Ernaut et Libourc s’étaient assis, main dans la main. Devant eux, le père Galien avait revêtu une étole et affichait de nouveau son masque empreint de sérieux. Il leva la main, attendit que le silence se fît et entama un large signe de croix, répété dans toutes les directions.

« *Benedic, Domine, thalamum hoc, et omnes habitantes in eo ; uta in amore tuo vivent, et senescant, et multiplicentur in longitudine dierum. Per Dominum*[^benedicthalamum]. »

Il s’avança, écartant la foule amassée tel le soc fendant la terre, et se plaça au pied du lit, face aux deux jeunes mariés et marqua Libourc du signe de la croix.

« *Benedicat Deus corpora et animas vestras, et det super vos benedictionem suam sicut benedixit Abraham, Isaac et Jacob*. *Amen*.[^benedicatdeus] »

Il fit ensuite un pas de côté et traça la même bénédiction sur Ernaut.

« *Manus Domini sit super vos, mittatque angelum suum sanctum, qui custodiat vos omnibus diebus vitae vestrae*. *Amen*[^manusdomini]. »

Puis, d’un mouvement du bras et d’un regard impérieux, il signifia à tous de se retirer de la chambre, les repoussant tel le pasteur ses ouailles bêlantes. Sans même un dernier salut pour Ernaut et Libourc, indifférent aux petits gestes et signes divers qu’il occultait, il fit refluer la presse puis tira la porte derrière lui. De la foule ne demeura qu’une rumeur.

Assis au pied de leur lit, les deux jeunes mariés se dévisageaient, souriant, un peu enivrés de leur journée. D’une caresse de la main, ils s’accordèrent et unirent leurs souffles sans un mot. Bien vite ils oublièrent, au loin, le tapage assourdi des fêtards qui résonnait dans leur salle commune. ❧

## Notes

Je l’ai déjà écrit à de multiples reprises, le mariage est jusqu’à assez tard au Moyen âge une procédure civile et non religieuse. Le but de l’union est de mettre en place une structure économique viable et pérenne, qui assure la reproduction du groupe social ou son étendue. L’Église n’y intervenait que pour bénir sous de bons auspices l’arrangement contracté par les individus ou les familles. Mais peu à peu les ecclésiastiques ont entrepris de se garantir que les époux n’étaient pas bigames ou incestueux, de vérifier que les choses étaient conformes aux usages licites. Cela a renforcé l’institutionnalisation de la pratique, tout en y introduisant une notion, de consentement mutuel, qui n’était pas forcément présente auparavant.

Le déroulement proprement dit a fait l’objet de nombreux rituels, selon le temps, les zones géographiques et les traditions religieuses. N’ayant rien de précis pour le Jérusalem du XIIe siècle, j’ai recouru à un patchwork de gestes et de formules qui me semble vraisemblable. Sans l’ouvrage de Jean-Baptiste Molin et Protais Mutembe, je n’aurais pas été en mesure de me repérer dans la jungle des usages. Il est toujours peu évident d’accéder aux sources liturgiques véritables, et encore moins d’en comprendre le possible arrangement de façon claire et pratique, surtout quand on n’a pas fait le séminaire. C’est pourtant une des interrogations les plus fréquentes qui nous sont posées quand on discute avec le grand public, et des éléments essentiels pour arriver à reconstruire les paysages mentaux de nos prédécesseurs médiévaux.

De la même façon, l’alternance de formules en latin et en langue commune semble avoir été fluctuant. Le fait que des clercs aient rédigé les déroulés en latin ne prouve en rien qu’ils aient été vocalisés ainsi, surtout pour les répons. Rien de strict ne semble avoir été pratiqué, les passages dans une langue ou l’autre semblaient pouvoir varier selon les lieux et les habitudes. Peut-être que la maîtrise du latin par l’officiant et son adhésion à une liturgie stricte pouvait aussi grandement varier selon sa formation et son rapport à sa hiérarchie et à la congrégation dont il avait la charge d’âmes.

## Références

Beauchet Ludovic, « Étude historique sur les formes de la célébration du mariage dans l’ancien droit français », dans *Nouvelle revue historique de droit français et étranger*, vol. 6 (1882), p. 351-393.

Molin Jean-Baptiste, Mutembe Protais, *Le rituel du mariage en France du XIIe au XVIe siècle*, Éditions Beauchesne, Paris : 1974.
