![Framabook, le pari du livre libre](logo_framabook_epub.jpg)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif : promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d'informations sur Framasoft, consultez <http://www.framasoft.org>

Se démarquant de l'édition classique, les Framabooks sont dits « livres libres » parce qu'ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu'un utilisateur de logiciels libres. Les Framabooks s'inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l'appropriation collective de la connaissance.

Pour plus d'informations sur le projet Framabook, consultez <http://framabook.org>

Copyright 2020 : Yann Kervran, Framasoft (coll. Framabook)

Ce recueil de textes Qit'a est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

ISBN : 979-10-92674-28-6\
Dépôt légal : Mai 2020\
Couverture : L'Archange Isrâfil, scène des *Merveilles de la création* d'al-Qazwînî, vers 1280. Domaine Public - [commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:Irakischer_Maler_um_1280_001.jpg)

Remerciements
-------------

La publication de ce premier lot de quatre recueils de textes courts Qit'a a demandé beaucoup de temps et d'énergie au membres du groupe Framabook, je tiens à les en remercier, Mireille en tête, avec qui les échanges autour de corrections proposées ont toujours constitué d'agréables moments.

*À la mémoire des exclus de la Mémoire*

Besace et bourdon
-----------------

### Route du Vif (Châteauneuf-Val-de-Bargis), après-midi du jeudi 28 juin 1156

Intrigué par une odeur suspecte, le renard franchit les broussailles de l'orée du bois, la truffe au ras du sol. Il cligna des yeux, tournant la tête vivement de droite et de gauche, anxieux de se présenter en pleine lumière. Une brise légère berçait les chênes, comme s'ils cherchaient à effacer de leur cime les rares nuages cotonneux maculant le ciel d'un bleu azur.

Le cri d'un geai ne l'inquiéta guère et il avança en foulées souples jusqu'à la large route. Une flaque asséchée semblait être à l'origine de son émoi. Il sortit un bout de langue rosée, mais n'eut guère le temps d'en faire plus. Un tintamarre effrayant s'approchait : des voyageurs chantant des hymnes religieux. Du moins pour l'un d'entre eux. Le second ahanait d'une voix vigoureuse avec un total mépris pour les règles musicales élémentaires, dont la première était de ne pas se fier au volume pour pallier une qualité déficiente.

« *ad Dominum in tribulatione mea clamavi et exaudivit me*[^1]

--- Ah doux minou entre hiboux là scions, mais acclame vite et tout ce qu'on dit vite, mais...

--- *Domine libera animam meam a labio mendacii a lingua dolosa*

--- Deux minets libèrent âne...

--- Non, Ernaut ! Non ! Par tous les saints, entends un peu mes paroles. Tu répètes n'importe quoi ! »

Le goupil n'attendit pas de savoir à qui appartenaient ces voix. Avant même que les têtes ne se montrent au-delà de la butte, il avait disparu sans un bruit, frustré de n'avoir pu achever son enquête. En premier apparut un visage à la mâchoire trapue, au nez long et cassé, avec des yeux mobiles surmontés d'une tignasse peu disciplinée couleur des blés, posée telle une écuelle sur le crâne. Le duvet qui ornait ses tempes et son menton indiquait qu'il était encore jeune et pourtant le tout était enchâssé sur un corps qui aurait fait envie à Samson lui-même.

Immense, massif comme un chêne centenaire, il avait un cou de taureau, le torse puissant d'un bûcheron et les bras d'un forgeron. Le plus étonnant était sa taille : il devait rendre facilement une tête à l'homme brun à ses côtés, plus âgé, d'une trentaine d'années, la mine visiblement contrariée. Ils étaient vêtus de toile solide, sans ostentation, mais de qualité, avec des souliers bien semelés. Ils portaient chacun, en plus de sacs variés, une besace et un bourdon qui indiquaient qu'ils étaient pèlerins en marche. La croix à leur épaule faisait savoir qu'ils cheminaient à destination de Jérusalem, le centre du monde. Mais pour l'heure, ils n'étaient qu'à guère plus de trois jours à l'ouest de Vézelay et avaient passé leur temps à se chamailler.

« Tu es censé savoir ce psaume, pourtant !

--- Je ne m'y entends guère en latineries.

--- Comment s'en étonner ? Il t'aurait fallu écouter le frère pour cela. Tu aurais pu au moins faire effort de retenir les maîtres chants. »

Les bâtons de marche ferrés battaient en cadence, les deux hommes avançaient d'un bon pas, soulevant une fine poussière dans les endroits où le sol n'était que graviers et cailloux.

« Droitement ce que j'ai fait. Seulement je déclame pas le latin comme toi, Lambert, voilà tout. Peut-être est-ce toi qui ne sais pas dire juste... »

Le plus âgé des deux allait répondre, agacé mais il se ravisa et se contenta de soupirer, les yeux levés au Ciel. Il savait que sa patience serait mise à l'épreuve durant leur périple, son frère ne s'avouait jamais vaincu. Lorsqu'il lui avait confié Ernaut, leur père l'avait prévenu. Il estimait trop dangereux de laisser Ernaut se rendre seul dans la Cité sainte. Il lui fallait un gardien, quelqu'un de raisonnable pour veiller sur lui jusqu'à ce qu'il ait accompli son vœu.

Dans un second temps, il était nécessaire qu'un homme d'expérience soit là pour faire les bons choix. Ils comptaient s'installer dans le royaume de Jérusalem, devenir colons comme tant d'autres avant eux. Et si Ernaut ne rechignait que rarement à la tâche, Lambert assistait son père depuis des années dans les affaires et avait bénéficié d'un excellent apprentissage. Il saurait trouver l'endroit idéal, des terres fertiles à travailler.

Ils cheminèrent ainsi pendant quelque temps, accompagnés seulement du bruit de leurs pas, des chants d'oiseaux et de quelques cris de bûcherons dans la forêt. Ils voyaient devant eux depuis un moment une petite carriole tirée par un âne, menée par un jeune homme balançant une badine dans son dos tandis qu'il marchait. Toujours sans rien dire, Ernaut attrapa sa gourde de peau et en avala de grandes lampées avant de la tendre à son frère. Arborant un sourire forcé, ce dernier accepta et but une gorgée.

Ils avaient entretemps rejoint la voiture, arrêtée sur le côté de la voie. Le charretier s'efforçait d'en dégager la roue d'une ornière. Le chargement, quelques paniers du fer extrait des mines environnantes, semblait bien trop lourd pour qu'il puisse espérer s'en sortir seul. Les deux frères stoppèrent et s'approchèrent, affichant un visage amical. L'homme se gratta la tête, arborant quelques dents éparses, surmontées d'un nez camus, en un sourire incertain.

« C'te bestiasse est partie de senestre[^2] alors que je veillais pas. Elle a coincé sa rouelle parmi la pierraille et c'est tout bloqué. »

Il se poussa de côté, laissant voir l'ampleur du désastre. Ernaut fit une moue appréciatrice et lui passa son bourdon. Il posa ses sacs au sol et, confiant, il empoigna le plateau, bras tendus et jambes pliées. En un effort, il hissa la roue hors de l'ornière, sous les yeux admiratifs du charretier.

« Par la barbe Dieu, jamais vu si grande force ! Je vous remercie bien, le pérégrin.

--- J'ai nom Ernaut et voilà mon frère Lambert. Ce n'était pas grande chose.

--- P't être pour vous, mais j'aurais été bien en peine d'hissir pareillement le charroi. »

Tout en parlant, il mena son âne vers le centre de la route, tendant à Ernaut son bâton de marche.

« J'ai nom Obert. Posez donc votre bagage, il pèsera guère et on dit que c'est chrétien de porter aide au pérégrin. J'peux vous mener jusqu'en l'abbaye où la charité des bons frères fera le reste. Vous cheminez vers la Cité, à ce que je vois. »

Lambert hocha le menton tandis qu'il posait son sac. Ils emboîtèrent le pas à l'équipage.

« Nous allons passer par Bourges et Limoges pour faire visite à Charroux d'abord. Puis nous prendrons un bateau pour l'outremer ».

Le charretier ne sembla nullement impressionné.

« J'ai usage de voir grand nombre de marcheurs, allant voir la Madeleine[^3] ou en revenant. Des gens qui vont visiter saint Étienne[^4] itou. J'ai même partagé la route avec des Allemands qui se rendaient à Saint-Jacques de Galice[^5]. Ils étaient justement passés par Vézelay, bien sûr. On a beau dire, c'est quand même la seule femme qu'a eu l'oreille du Christ, hein ? C'est quand même pas rien. »

Convaincu de la pertinence de son argument, il l'agrémenta d'un crachat expert qui conclut sa démonstration.

Les trois hommes continuèrent leur chemin en silence, accompagnés cette fois du grincement des roues. Alors qu'ils arrivaient en haut d'une légère pente, Ernaut avisa la longue piste qui partait droit devant eux vers le couchant, parmi les arbres de la forêt. Il hésita un instant avant de poser sa question.

« La voie est-elle donc toujours ainsi bien roide ? On n'en voit plus la fin. »

Odert ricana avant de hocher la tête, amusé. Lambert soupira bruyamment.

« Voilà deux jours de ça, tu te lamentais des sentiers tortueux et ravinés. Maintenant tu désespères de si beau chemin. Vas-tu te décider si tu es fille ou garçonnet ?

--- J'en dis que je me verrais plutôt cavalier, cul en selle, monté sur gentil hongre. »

Lambert secoua la tête en dénégation.

« Je te l'ai déjà dit, nous en prendrons à Charroux. D'ici là, nous devons cheminer comme modestes pérégrins, sur nos deux jambes. »

Obert se tourna vers eux.

« L'automne dernier, j'ai eu vu passer un évêque et son train pérégrinant pour Bourges. Ils cheminaient en selle, même le saint homme. »

Le regard noir que lui lança Lambert l'incita à ne pas développer. Le crachat qu'il dirigea sur le sol mit fin à son intervention. Derrière eux, un peu en retrait, Ernaut se mit à fredonner une chanson, un sourire impertinent sur les lèvres.

### Hôpital de la Charité, soirée du jeudi 28 juin 1156

Lorsqu'ils arrivèrent aux abords du bâtiment des moines, Ernaut et Lambert furent accueillis par un homme entre deux âges, à la barbe poivre et sel, à qui il manquait un bras. Il les salua et les invita à s'asseoir sur un des bancs disposés devant un escalier de pierre accédant à l'étage. Deux autres voyageurs étaient installés là, prenant le soleil, après avoir eu leurs pieds lavés par un des servants. Les deux frères prirent place en silence. Ils étaient un peu las, n'ayant pas fait de longs trajets depuis quelques semaines.

Tandis qu'on achevait de les sécher, un convers, à la bure fatiguée, s'approcha d'eux. Il était assez jeune, plutôt laid, et flottait dans son habit comme un crapaud dans un sac. On aurait dit que la verrue qui ornait son nez l'avait incité à loucher. Le sourire déformant ses traits le rendait pourtant inexplicablement sympathique. Son visage rayonna lorsqu'il aperçut les besaces et, surtout, les croix aux épaules.

« La bienvenue en l'hospice des frères de Notre-Dame de la Charité, voyageurs. J'ai nom Thibald. Grande est ma joie à accueillir des marcheurs pour l'outremer. »

Lambert remercia d'un signe de tête, sans interrompre le moine.

« Si vous avez besoin de rapetasser souliers ou vêtements, nécessité d'en changer, nous avons ample fourniment pour vous. Sur votre demande, nous pouvons de même mander le barbier pour tailler barbe et cheveux.

--- Grand merci de votre charité, frère, nous ne sommes partis que de peu, et n'avons guère besoin. Un bon repas et un endroit où dormir seront bien assez pour nous. »

Le moine hocha la tête avec énergie.

« Vous avez encore temps de vous rendre en la grande prieurale avant souper. Saint Jovinien nous honore fort souvent de beaux miracles.

--- Nous sommes déjà venus prier devant la châsse, nous ne venons que de Vézelay. Je pensais plutôt m'y rendre demain avant le départ.

--- Faites selon vos souhaits. Parmi le marché traversé pour venir ici se trouvent des marchands de cierges, si vous souhaitez laisser quelque lumière pour éclairer votre voyage. Je vous laisse, il me faut vaquer à mes occupations. »

Tandis qu'ils se délassaient, Ernaut discuta avec les voyageurs autour d'eux. Deux se rendaient justement à Vézelay, pour demander la guérison de leur sœur, frappée de cécité. Parmi trois autres en chemin pour Fleury s'en trouvait un le visage déformé par une tumeur. Elle lui obstruait pratiquement un œil et pendait jusque sur son épaule, sans qu'il en semble gêné.

Tous étaient impressionnés par le voyage qu'entreprenaient les deux frères. Ils furent peu après rejoints par un vieil homme squelettique, qui n'ouvrit pas la bouche si ce n'était pour tousser et qui suivait leurs discussions en hochant benoîtement du chef. En le voyant, un des servants le houspilla et le fit rentrer, en expliquant qu'il n'avait plus toute sa tête et devait rester au chaud, allongé. Il s'échappait tout le temps et se baladait dans la ville à demi nu. Après cela, le manchot revint chercher Ernaut et Lambert pour les mener à leurs lits, où ils pourraient laisser leurs affaires. Il leur donna des draps propres et des couvertures épaisses, ainsi que deux oreillers chacun. Le tout sentait bon l'herbe fraîchement coupée et les fleurs des champs.

Ils étaient installés dans une grande chambre, avec une quinzaine d'autres couches, où n'étaient logés que des hommes. Les moines de l'abbaye trouvaient plus sage de séparer les deux sexes, même s'il se rencontrait parfois des époux parmi les voyageurs. Ils accueillaient sans demander, mais tenaient à diriger selon leurs règles. Au fond de la pièce, un petit oratoire était orné d'un crucifix, chichement éclairé d'une lampe à huile. Assis devant, un jeune garçon priait avec ferveur. Le serviteur leur expliqua à mi-voix, le regard attristé, que c'était le dernier fils d'un charbonnier. Le pauvre avait perdu l'usage de ses jambes suite à un accident dans les bois. Il était là depuis plusieurs semaines.

Lorsqu'ils redescendirent, une bonne odeur de nourriture chaude sortait de la grande salle. Il s'y trouvait une vingtaine de personnes, des pèlerins comme eux, mais aussi quelques malades et de simples voyageurs, trois hommes qui se tenaient à l'écart, vêtus comme des marchands. Des écuelles avaient été distribuées, de larges miches de pain tranchées et des pichets disposés.

Un moine à la tonsure soignée, habillé de laine très sombre d'excellente qualité, surveillait les préparatifs du repas. Son visage fin avait le teint pâle des clercs peu habitués aux grands espaces et ses yeux étaient semblables à deux charbons noirs, profondément enchâssés. D'un geste, il invita les deux frères à s'installer sur un des bancs. Une fois tout le monde assis, il toussa pour s'éclaircir la gorge puis lança d'une voix douce et chantante :

« *Benedic, Domine, nos et haec tua dona quae de tua largitate sumus sumpturi*.[^6] »

Puis il marqua un temps avant de terminer : « *Per Christum Dominum nostrum.* ».

Chacun répondit alors « *Amen* » en se signant. Les serviteurs entrèrent, distribuant un épais potage très parfumé, dans lequel nageaient des morceaux de viande. Ernaut versa à son frère un vin rouge que Lambert apprécia en connaisseur. En quelques instants la rumeur enfla, chacun commentant sa journée, son voyage, ses affaires, avec ses voisins. Passant parmi les tables après avoir rapidement avalé le contenu de son écuelle, le jeune moine vérifiait que chacun avait tout ce qu'il lui fallait. Il ne manqua pas de bénir avec enthousiasme les deux marcheurs à destination de Jérusalem.

### Hôpital de la Charité, nuit du jeudi 28 juin 1156

Réveillé en sursaut par un ronflement plus fort venu de sa droite, Ernaut tournait depuis un moment dans ses draps sans parvenir à replonger dans le sommeil. Lassé de tourner sur une épaule puis l'autre, il se résolut à quitter son lit. Attrapant sa chape, il sortit sans bruit, couvert par le tintamarre des différents grognements, soupirs et gargouillis émis par ses compagnons de chambrée. Il descendit l'escalier extérieur et retrouva la cour, afin de boire quelques gorgées à la fontaine.

Une lueur dans la salle commune du rez-de-chaussée attira son regard. Intrigué, il s'avança et poussa la porte en grand. Thibald, le frère convers qui les avait accueillis était assis à une table, grignotant du fromage. Il avait à côté de lui des tablettes de cire et des feuillets, ainsi qu'une corne d'encre et quelques plumes. Le grincement de l'huis le fit se dresser, les sourcils froncés. Reconnaissant Ernaut et le voyant à peine vêtu, il lui sourit.

« Un peu tôt pour achever sa nuit. Laudes est encore loin... »

Ernaut manifesta son accord par un grognement qu'il s'efforça de rendre poli, ce qui ne rebuta pas le moine qui continua.

« As-tu la gorge sèche ? Je peux te porter quelque boisson.

--- Merci non. Je vais juste m'asseoir ici un moment, en espérant que la chambrée sera moins bruyante plus tard. On dirait cantique de gorets. »

La remarque fit s'esclaffer Thibald.

« C'est par malheur bien fréquent. Une fois, la clameur était telle qu'un pèlerin préféra dormir dehors parmi la cour. Mais les choucas s'activèrent si tôt que sa nuit n'en fut guère meilleure. Ses compagnons le gratifièrent du surnom de Choucar depuis lors, m'a-t-on dit. »

Il reprit son sérieux.

« Certains demandent aussi à demeurer en l'église toute la nuitée. Mais les frères ne goûtent point cela. Cela dérange les offices et permet de bien fâcheuses rencontres... »

Il souligna sa dernière remarque d'un regard plein de sous-entendus. Puis se tint coi quelques minutes, fixant son stylet comme s'il le voyait pour la première fois. La flamme de la lampe donnait une couleur satanique à son visage ingrat et le faisait paraître plus vieux qu'il n'était. Lorsqu'il bougea de nouveau, il secoua doucement la tête, puis se tourna vers Ernaut.

« Vous empruntez bien long chemin, je vous envie.

--- Nous allons prendre nef à Gênes, mais devons passer par Charroux auparavant.

--- Excellente idée ! J'ai vu moult pérégrins faire semblable choix. »

Il marqua un temps avant de poursuivre, la mine désolée.

« J'aurais pris grande joie à faire pareil voyage, mais j'ai trop de choses à faire ici...

--- Vous ne pouvez demander congé à l'évêque ?

--- Pour moi, ce serait auprès du prieur et de l'abbé Pierre[^7], à Cluny. Mais ce serait bien lourd tracas pour tous de me remplacer, il me faut demeurer à ma place. Parlons de toi plutôt. Tu es bien jeune pour pareil vœu, le fais-tu pour un parent, un bienfaiteur ? »

Ernaut réfléchit quelques instants, un peu embarassé. Puis répondit :

« Non pas. Nous allons faire nos dévotions au Sépulcre, mais comptons nous établir là-bas. On nous a dit qu'ils espèrent la venue de nombreux colons. »

Thibald ouvrit de grands yeux, visiblement impressionné.

« C'est merveilleux d'avoir ainsi entendu l'appel ! Tes parents doivent être bien fiers, garçon. »

Peu désireux de poursuivre cette conversation, Ernaut se leva, arborant l'air le plus fatigué qu'il pouvait.

« Certes. Père a tout fait pour que notre installation là-bas se passe au mieux.

--- Mes pensées vous accompagneront, ton frère et toi, en tout cas. À Dieu te mande[^8].

--- Grand merci, frère. Je vais d'ailleurs de ce pas me reposer en la couche que vous nous avez préparée. »

Après un rapide salut, Ernaut sortit de la salle, sous le regard attendri du moine. Thibald trouvait néanmoins étrange ce manque d'enthousiasme. Quelques années plus tôt, un tel voyage et la perspective de s'installer en Terre sainte auraient constitué pour lui un rêve fabuleux. Pourtant Ernaut semblait le vivre péniblement. Le convers se demanda un instant si le pèlerinage était bien une aspiration du jeune homme. Puis, balayant ces idées de sa tête, il se replongea dans l'inventaire de ses besoins pour le mois à venir.

Dehors Ernaut déambula un peu dans la cour, se rendit jusque sur les bords de la Loire. La lune gibbeuse, habillée de lambeaux filandreux, éclairait le pont de bois qui filait vers l'ouest. Le vacarme des flots était accompagné du grincement des moulins attachés aux piles. Deux silhouettes flânaient, une lanterne à la main, au débouché sur la rive. Quelques masures sur l'île en face avaient leurs cheminées encore fumantes et, parfois, un rai de lumière éclaboussait de jaune les sombres façades. Le jeune homme demeura un long moment le regard dans le vide, humant à pleins poumons l'air frais. La nuit débutait à peine, il avait le temps. Il se frotta le crâne de la paume, nerveux. Une angoisse commençait à lui peser sur la poitrine : il se rendait compte qu'il ne reverrait jamais plus ces endroits. ❧

### Notes

Les pèlerins médiévaux suivaient les routes des autres voyageurs et, même si certaines voies étaient plus fréquentées que d'autres, beaucoup choisissaient leur parcours en fonction d'impératifs personnels. La plupart du temps, ils allaient à un sanctuaire régional, voire local, ou cheminaient de l'un à l'autre. Il est difficile d'indiquer un tracé idéal.

Sur le trajet , l'hébergement des plus humbles était aléatoire, et bien souvent, c'était une institution monastique charitable qui s'occupait de nourrir et d'accueillir tous ceux qui se présentaient ainsi que les malades. Des abus existèrent, mais cela ne remit pas en cause la vocation hospitalière de nombreux établissements, du moins pas au XIIe siècle.

Le texte exact du cantique, le psaume 119 (120), massacré par Ernaut est le suivant :

> 1\. Dans ma détresse, c'est à l'Éternel Que je crie, et il m'exauce\
> 1. *Ad Dominum in tribulatione mea clamavi et exaudivit me*

> 2\. Éternel, délivre mon âme de la lèvre mensongère, De la langue trompeuse !\
> 2. *Domine libera animam meam a labio mendacii a lingua dolosa*

> 3\. Que te donne, que te rapporte Une langue trompeuse ?\
> 3. *quid detur tibi aut quid adponatur tibi ad linguam dolosam*

> 4\. Les traits aigus du guerrier, Avec les charbons ardents du genêt.\
> 4. *sagittae potentis acutae cum carbonibus iuniperorum*

> 5\. Malheureux que je suis de vivre en exil, d'habiter parmi les tentes de Kédar !\
> 5. *heu mihi quia peregrinatio mea prolongata est habitavi cum tabernaculis Cedar*

> 6\. Assez longtemps mon âme a demeuré Auprès de ceux qui haïssent la paix.\
> 6. *multum peregrinata est anima mea cum odientibus pacem*

> 7\. Je suis pour la paix ; mais dès que je parle, Ils sont pour la guerre.\
> 7. *ego pacifica loquebar et illi bellantia*

### Références

Collectif, *Voyages et voyageurs au Moyen Âge*, Paris : Publications de la Sorbonne, 1996. \[En ligne\], consulté le 23 septembre 2011, Adresse URL: <http://www.persee.fr/issue/shmes_1261-9078_1996_act_26_1>

Heath Sydney, *Pilgrim Life in the Middle Ages,* Londres, Leipsic: T. Fisher Unwin, 1911.

Verdon Jean, *Voyager au Moyen Âge*, Paris : Perrin, 2003.

Webb Diana, *Pilgrims and Pilgrimage in the Medieval West*, Londres - New York: I.B. Tauris, 1999.

Faux espoirs
------------

### Cap de Gata, sud d'Almería, vendredi 25 juillet 1147

Les quinze galées génoises du consul Balduino Doria se balançaient en cadence, à une large encablure du rivage andalou. Le soleil, impitoyable, transformait les côtes déchiquetées en flammes sombres, ondoyant sous le regard des hommes aux yeux collés par le sel. Ils attendaient.

Depuis des semaines, ils patientaient, inutiles rameurs allongés sous de dérisoires abris de toile. Parfois quelques ordres étaient criés d'un bord à l'autre, un canot était mouillé, allant aux nouvelles ou à l'approvisionnement. Rien ne venait bousculer la monotonie des jours qui s'écoulaient, pas même un vaisseau ennemi aventureux. Le capitaneus[^9] Oberto Farrugia était affalé, à l'ombre du château arrière, mollement assoupi, chassant de temps à autre les mouches bourdonnant à ses oreilles d'un geste distrait. Il s'était rendu avec espoir à la réunion du matin sur le navire amiral, auprès des consuls. Les troupes de l'empereur Alphonse[^10] n'étaient toujours pas annoncées, en retard de trois mois désormais. Seuls quelques matelots trouvaient la force de discuter, coincés entre les bancs de nage, assis parmi leurs affaires personnelles.

Willelmo Marzonus, vétéran de l'assaut sur les Baléares décrivait par le menu ses exploits passés et imaginaires aux recrues les plus novices. Les yeux rieurs et la malice qu'on y voyait animaient le visage hâlé, tanné par la vie en mer. Malgré sa jeunesse, le sel commençait à gagner son épaisse chevelure noire et sa barbe mal taillée. Tandis qu'il narrait ses aventures, quelques postillons écumeux trouvaient leur chemin à travers la dentition fragmentaire et venaient rejoindre les autres taches à l'origine incertaine maculant sa tenue.

« L'an passé, on a pas eu besoin des princes hispaniques pour rafler la mise. Et on était juste vingt-deux nefs de guerre ! On a connu Oberto Torre plus aventureux !

--- Ils doivent penser que belle cité comme Almería ne se force pas avec quelques équipages. Il nous faut bonne cavalerie castillane, rétorqua un jeune basané au torse puissant, Enrico Maza.

--- Fadaises ! Nous tenons en cale tout le nécessaire : chat[^11], pierrières[^12] et arbalètes en tour... Que faut-il en sus ? On se cuit la panse à espérer ici, à serper le fer[^13] ici pour le mouiller là... Sans aucun butin en vue. Nous avons foison d'hommes, de naves, assaillons-donc ! L'empereur sera bien aise de venir cavaler par les rues en notre pouvoir, quand il daignera nous joindre. »

Un des hommes les plus âgés, qui avait place auprès des officiers de poupe s'immisça dans la conversation :

« Au matin, ils devisaient de missionner coursier auprès la cour hispanique, peut-être allons-nous enfin connaître bonnes nouvelles d'ici peu.

--- Prions que ce soit de vrai, compagnons, notre chère cité de Gênes espère pour tous les trésors que nous amasserons... »

### Almería, mardi 19 août 1147

« Hardi compaings, demeurez en bons rangs ! Laissons leur temps de nous approcher au mieux... »

Le jeune Enrico s'était équipé légèrement, d'une armure d'épaisse toile rembourrée et arborait à son flanc l'épée neuve qu'il s'était offerte au départ de Gênes. Il avait noué son nasal avec fébrilité et regrettait désormais de ne l'avoir pas suffisamment serré. Couvert de son large bouclier, il lançait de temps à autre des regards en coin à ses compagnons alentour, s'efforçait de contenir la nervosité qui le gagnait. Ils avaient pris pied sur la plage, comme prévu, mais devaient attendre l'approche des forces ennemies avant de retourner au plus vite dans les vaisseaux, à l'abri.

En premier lieu, ils avaient discerné deux éclaireurs sur la crête. Puis la poussière annonciatrice d'une considérable troupe les avait rejoints, accompagnée de la clameur et du brouhaha des guerriers musulmans. Les cris de ralliement incompréhensibles, le tumulte des tambours et des trompettes avaient tout d'abord ébranlé l'assurance des compagnies génoises. Puis lorsque l'horizon s'était rempli d'une marée de boucliers, de lances, d'épées et d'arcs, certains jurons avaient fusé. Les officiers gardaient les yeux rivés sur le navire d'Ansaldo Doria, qui devait lancer le signal du retrait puis de l'attaque générale, en concertation avec les Barcelonais embusqués non loin de là. Le plan avait paru limpide à Willemo et Enrico : en attirant les forces musulmanes loin de la cité fortifiée, ils allaient les prendre dans une vraie souricière. Ils n'avaient pas réalisé que pour un tel projet, il fallait un appât, au destin parfois héroïque, toujours tragique.

Des chocs sourds vinrent frapper les larges pavois : les archers commençaient leur œuvre. Les arbalétriers génois restés en retrait et les servants des balistes sur les navires s'activaient de leur mieux, mais ne pouvaient s'opposer au nombre. Enrico reçut un coup violent dans les côtes :

« Maza ! Tiens la ligne et baisse la tête, bougre de pisseux ! » lui beugla le capitaine responsable de son unité.

Une flèche glissa sur son épaule puis vint tinter contre le casque d'un compagnon. Quelques râles se firent entendre, bientôt suivis d'appels à l'aide. La presse se fit plus forte, les hommes se tenaient serrés les uns contre les autres, offrant comme seule cible à l'adversaire un bloc solide.

Les projectiles volaient de toutes parts et se frayaient un chemin jusqu'aux chairs, jusqu'aux os, jusqu'aux tripes. Un cri se répercuta soudain : « Aux naves ! Aux naves ! ». Enrico fut tenté de tout lâcher pour courir, mais il ne le pouvait pas, à demi porté par ses compagnons. Ils reculaient lentement. De sec le sable fut vite mouillé sous ses pieds, et il se retrouva en quelques pas dans les vagues. Il tâtonnait du talon, se déplaçant sans oser ouvrir les yeux, collé contre ses frères d'armes, cherchant à se fondre dans la masse. Les flèches continuaient à siffler, il sentait leurs pointes se ficher dans le bois.

Peu à peu, le bruit de l'eau se fit plus fort, les cris des marins qui aidaient à rembarquer plus présents.

« Aux filins ! Hissez et à vos rames ! »

Le tumulte ambiant submergea le jeune homme. Partout, des soldats s'agrippaient aux cordages le long de la coque, aidés par leurs compagnons déjà à bord. On se passait les armes, les écus, on jetait ses affaires avant de monter. Oubliant le péril, Enrico se retourna et présenta son pavois, vite chargé, puis chercha des yeux quelques prises pour grimper se mettre à l'abri. Parmi les cris et les appels, il reconnut Willelmo, penché au-dessus de lui, le bras tendu, la paume offerte. Il sauta, saisit la forte poigne, battant des pieds pour se propulser.

Il était bousculé par des épaules, des coudes, des mains qui furetaient dans l'espoir de se hisser. Certains retombaient, dans une gerbe d'eau accompagnée d'exclamations inquiètes. Bientôt il ne resta plus à Enrico qu'à enjamber la pavesade[^14] pour être en sécurité. Au moment où il se laissait choir sur son compagnon, qui ne l'avait toujours pas lâché, il sentit une intense douleur percer son flanc. Un filet de sang s'écoula de sa bouche lorsqu'il s'écroula sur le banc, inconscient. Willelmo le retourna, inquiet, et avisa finalement un matelot à ses côtés, l'air goguenard.

« Ce nigaud a réussi à s'emmancher son fourreau dans la panse ! La douleur l'a assommé ! »

### Almería, matin du vendredi 17 octobre 1147

Chacun s'était aménagé un abri aux abords du rivage, auprès des galées échouées. Assis sous sa cabane de branchages, Enrico ajustait les énarmes de son bouclier. Après un rapide conseil, les consuls avaient ordonné de se préparer à un grand assaut pour le matin. Ses compagnons les plus proches, avec qui il partageait le feu et ce coin de plage, commentaient la décision de leurs dirigeants. On craignait une désertion des forces espagnoles.

« Le capitaneus disait que le comte d'Urgel était des envoyés auprès les païens de la cité. Ces maudits adorateurs d'Apollon ont promis cent mille maravedis[^15] aux forces de l'empereur pour qu'il se retire... »

Un arbalétrier au visage maigre affecta une moue désapprobatrice tandis qu'il finissait de ranger ses cordes de rechange.

« M'étonnerait fort qu'Alfonse se déshonore de telle façon ! Nous ne sommes qu'à quelques pas de la victoire. N'avons-nous pas déjà deux tours prises et plus de cinquante pieds de muraille abattus ? Bien fol le goupil qui hésite à croquer la poule une fois le chien occis... »

Les hommes approuvèrent silencieusement, inquiets, nerveux à l'idée de l'attaque, mais aussi de la possible désaffection de leurs alliés. Ils attendaient depuis plusieurs mois, avaient enduré un long voyage, supporté les privations pendant le siège qu'ils tenaient depuis août. Plus encore que la mort, ils redoutaient d'avoir accompli tout cela en vain, de repartir plus pauvres et pitoyables qu'ils n'étaient arrivés.

Un cavalier passa aux abords, hurlant à chacun de se rendre auprès du camp principal, à la Porta Lena, de façon à former les compagnies d'assaut. Tout en s'avançant, Willemo vint frapper fraternellement sur l'épaule d'Enrico.

« Ce jour d'hui nous serons plus riches à la nuitée qu'au lever, ami. Une poignée de sarrasins maudits outre quelques coudées de pierre nous séparent de notre récompense. À nous de la prendre ! Et vaillamment ! »

Enrico lui sourit, une récente lueur féroce animant ses yeux.

« Cul-Dieu ! Puisses-tu parler de vrai. J'ai grand faim de monnaies d'or, d'étoffes soyeuses et de langoureuses chairs...

--- Tu en auras tout ton soûl, crois-m'en ! Plus que tes mains pourront en palper ! »

Willelmo éclata d'un rire gras.

« Vigiles de la sainte Luce[^16], voilà heureux signe que le Créateur nous envoie. Puisse ma lame ouvrir quelques lumières dans les crânes ennemis. »

Enrico opina, un large sourire carnassier illuminant son visage. Il n'était plus le jeune garçon plein d'idéal embarqué à Gênes. Il avait vu des amis succomber le crâne écrasé sous des pierres projetées depuis les murs de la ville, d'autres les entrailles ouvertes par des lames impitoyables.

La douleur, les cris, la sueur et les larmes emplissaient ses jours tandis que la nuit, les lamentations, les gémissements des blessés et des malades, la faim le maintenaient éveillé. Plus d'une fois, il avait adressé une prière silencieuse à la Vierge, allongé dans sa cabane branlante, le visage fouetté par le vent de sable qui balayait la côte. Il la suppliait de le protéger un jour de plus, jusqu'au crépuscule suivant, sans oser prétendre à plus de temps ici-bas. Puis au matin, il retrouvait toute sa morgue tandis qu'il revêtait son armure de toile, laçait son casque, apprêtait ses lames. Enrico était soldat, la mort marchait à ses côtés.

### Almería, midi du vendredi 17 octobre 1147

Depuis que les trompettes avaient sonné l'assaut, les Génois avaient senti le temps se ralentir. Marchant inexorablement sur la ville, ils avaient repoussé les tentatives désespérées des habitants de contenir l'envahisseur. Alors que le mitan du jour était passé, les forces chrétiennes s'étaient répandues dans tous les quartiers, et le massacre avait commencé. Les demeures, les biens, les personnes, tout n'était que proie ou butin. Chacun s'empressait d'amasser dans ses sacs ce qui se pouvait prendre, se réservait les captifs, sélectionnait les femmes à son goût pour de coupables plaisirs à venir. Quelques réduits résistaient, sans espoir, pour l'honneur, sans guère de conviction.

Enrico et Willelmo déambulaient au hasard des rues avec quelques acolytes, forçant les demeures qui leur paraissaient receler des richesses. Dans leurs besaces tintaient les objets de métal précieux, bijoux et pièces, triste butin happé dans les maisons saccagées. Ils riaient comme des chasseurs ivres, les poings rougis du sang de leurs victimes. La victoire ne révélait certes pas le meilleur en eux. Un claquement fit sursauter Enrico, qui fronça les sourcils en direction du bruit. Une porte lui semblait avoir été fermée à leur approche. Confiant, il s'avança et, d'un coup de pied, frappa violemment dans l'huis. Un râle surpris accompagna la brusque ouverture. Au sol, le jeune soldat aperçut une jambe dans l'obscurité de la maison, une cheville fine ornée d'une élégante chaussure. Il s'élança en lançant à ses frères d'armes :

« Foutre ! Voilà diablesse qu'il conviendrait de dresser ! »

Il s'avança dans l'ouverture, les yeux plissés, tentant de vaincre l'obscurité. Il devinait une forme mouvante à ses pieds, vêtue de couleur claire, de petite taille. Elle brandissait ses mains, implorant dans une langue inconnue de lui. Enrico se retourna, cherchant l'approbation de ses amis. L'un d'eux hurla, la bave aux lèvres, l'air triomphant :

« Voilà bien ce qu'il te fallait ! La mignonne supplie qu'on l'honore ! Comment refuser si appétissante proposition ! »

Enrico fit face, regardant la jeune femme qui tentait de se relever devant lui, paniquée. Il la gifla sauvagement du revers de la main, claquant de la mâchoire comme un prédateur affamé. Elle retomba sur le sol, non sans avoir violemment heurté et renversé quelques meubles bas. Derrière lui, ses compagnons scandaient son nom, l'encourageaient de paroles lubriques, de commentaires paillards. Il ne pouvait plus reculer. Il appuya les genoux sur le ventre de sa victime et chercha à lui agripper les cheveux. Il voulait plonger dans son regard avant de commettre son forfait, se repaître de sa terreur. Toutefois, lorsque leurs yeux se croisèrent, il ne trouva aucune peur, seulement de la haine. Et il ne vit la lame dans sa main que trop tard.

Lorsque ses chairs furent frappées, la douleur lui déchira le crâne ; il se jeta en arrière, les tempes bourdonnant de souffrance. Grognant un blasphème, Willelmo dégaina son épée et trancha le visage de la femme d'un seul geste. À ses pieds, allongé dans son propre sang, Enrico hurlait de douleur, les mains sur la tête. Finalement la Vierge ne lui avait pas accordé cette dernière journée. ❧

### Notes

Alors que les participants à la Seconde croisade se préparaient à leurs combats en Terre sainte, et échouèrent devant Damas, une coalition entre Espagnols et Génois (et Pisans) décida d'aller assiéger Almería, loin au sud, en Andalousie. L'attaque devait en être faite en mai 1147, mais le retard des forces d'Alphonse VII ne permit pas de commencer les opérations avant l'été.

Après plusieurs mois d'intenses affrontements, la ville fut conquise par un assaut initié par les consuls, qui craignaient (à tort ou à raison) que les Espagnols abandonnent le siège. Par la suite, les navires participèrent à la prise de Tortose, au sud de Barcelone, fin décembre 1148. Bien que victorieuse, cette longue et difficile expédition fut à l'origine d'une crise économique et politique majeure au sein de la cité italienne.

Le récit de ces opérations a été fait par Caffaro di Rustico (1080 -- vers 1164), ancien consul de Gênes, chef de guerre, diplomate, ambassadeur. Écrivain prolifique, il a rédigé entre autres les Annales génoises, pour la période de 1099 à 1163. Ayant participé tout jeune à la Première croisade, il a également raconté les exploits de ses compatriotes en Terre sainte.

### Références

Bach, Erik, *La cité de Gênes au XIIe siècle*, Gyldendal, Copenhague : 1955.

Epstein, Steven, *Genoa and the Genoese 958-1528*, The University of North Carolina Press, Chapel Hill & Londres : 1996.

Loud, G.A., trad., (page consultée le 7 juin 2011), *The Capture of Almeria and Tortosa, by Caffaro*, Leeds Medieval History Texts in Translation Website, University of Leeds, 2004, \[En ligne\]. Adresse URL : <http://www.leeds.ac.uk/arts/downloads/file/689/medieval_text_centre>

In pulverem reverteris
----------------------

### Gênes, soirée du jeudi 4 septembre 1155

Les nombreuses lampes à huile disséminées dans l'opulente pièce éclairaient d'une lumière fauve le dos du jeune homme occupé à contempler au-dehors par la fenêtre vitrée. Sa grande taille était accentuée par une longue cotte de laine souple, de couleur sombre et profonde. Son visage, tourné vers la noirceur de la nuit, resplendissait de force. Bien que des traits réguliers en altérassent la sévérité, tout en lui inspirait l'autorité naturelle. Son regard noir, franc et inquisiteur, était pour l'heure perdu dans le lointain.

Il avait entrebâillé une des huisseries et humait l'air frais porteur de saveurs salées et d'embruns distants. Le vent soulevait les fines mèches de ses cheveux bouclés et faisait osciller les flammes. Il frissonna un instant, se frotta le nez et se tourna vers l'intérieur. Autour de la table se tenaient quatre hommes également habillés de beaux atours, dont l'un d'entre eux, le plus vieux, était assis sur un banc, occupé à lire à voix haute le texte d'un énorme ouvrage. Sa voix croassante donnait vie aux mots avec une étonnante vigueur, montrant l'habitude qu'il avait à déclamer les formules précises, les indications légales et les subtilités administratives. Son crâne chauve ressemblait au parchemin sur lequel son doigt courait, et ses cheveux ne formaient plus qu'une couronne argentée éparse. Régulièrement, il avalait sa salive en un puissant bruit de succion, faisant claquer ses lèvres d'un répugnant gargouillis. Parfois, il levait un index impérieux vers le ciel, le sol ou le mur, haussant le ton comme jongleur en foire. Assez rapidement, il s'arrêta et se redressa, grattant son oreille comme s'il s'y trouvait quelques puces. Il se tourna vers le jeune homme :

« Messire Jacomo, ce contrat vous sied-il en ces termes ?

--- Si fait. Je n'y vois certes rien de fâcheux. Oncques n'ai été déçu de vos écritures, maître Embroni. »

La remarque arracha un couinement d'obséquieuse félicité au vieil homme.

Tout sourire, il se tourna vers le grand gaillard à ses côtés, large d'épaules, occupé à se suçoter les dents d'un air distrait, comme ennuyé d'être là.

« Et vous, maître Mallonus ? Êtes-vous en accord avec les déclarations ?

--- Certes, cela me convient fort. J'aurais mauvaise grâce à m'en plaindre. »

Tout en répondant, il avait adressé de la main un petit merci au jeune homme. Ce dernier referma la fenêtre et vint prendre place sur un des bancs, incitant d'un geste les autres à faire de même.

« Si nos témoins en sont d'accord, il ne nous reste plus qu'à baptiser nos espoirs de vin épicé.

--- Ce sera avec grande joie que je boirai aux vôtres succès. » répondit un petit homme au physique épais, surmonté d'un crâne orné de cheveux crépus, le visage flasque et fatigué.

« J'aurais fort goûté de participer à une telle aventure, même simple *socius tractans*[^17], vu mes maigres biens. »

La remarque soutira un juron à son voisin, un jeune homme blond aux manières brusques. Il s'esclaffa et lança un regard consterné à la cantonade, arrachant quelques rires polis.

« À qui pensez-vous faire accroire semblable chose, ami ? Vous avez si bien amassé la cliquaille depuis nos aventures en Espaigne qu'il ne doit demeurer aucun espace libre en vos celliers et entrepôts !

--- Je ne saurai nier que le Seigneur m'a comblé de ses bienfaits, mais là, il s'agit de tout autre chose, par Dieu ! Plus de cent cinquante livres en poix, fer et charpenterie ! Jamais je n'ai fait pareil accord, même en mes plus brillants moments. »

Jacomo hocha la tête avec assurance, regardant son gobelet de verre avec attention.

« De sûr, certains me trouveront bien fol d'investir pareil montant. Depuis le temps que j'espérais faire commerce alexandrin, je suis fort aise d'avoir encontré Imberto ! Les louanges pleuvent sur lui comme sur la Vierge au jour de Noël. Pas un de mes compaignons d'affaires qui ne clame son habileté pour négocier avec l'Égypte.

--- J'ai surtout usage d'éviter les ennuis en route. Les papistes ont beau jeu de nous promettre éternelle souffrance ou damnation. Quand bien même, qu'ils nous laissent œuvrer en paix. Est-ce que je vais leur dire comment bénir pain et vin, moi ? »

Sa saillie déclencha quelques rictus ennuyés et grimaces polies, et seul Jacomo s'esclaffa bruyamment.

« Puissent nos pères t'entendre, Imberto. C'est tout de même grande honterie que de se cacher pour établir contrat entre nous, comme larrons en maraude. Ces clercs me rendent malade, ils sont les premiers à se jeter sur les étoffes sarrasines, mais n'acceptent pas d'en payer le prix. »

Il marqua un temps, avalant une gorgée de vin, les sourcils froncés, prêt à mordre quiconque s'opposerait à lui. Il reposa son verre brutalement, faisant tinter le délicat gobelet de verre émaillé.

« Qu'ils nous laissent commercer en paix et embellissent leurs liturgies. Il n'entendent rien aux affaires de ce monde, qui les répugne visiblement. Alors qu'ils s'enclosent en leurs couvents, nous saurons bien mener le navire, et certainement pas en plus mauvais courant qu'eux.

--- Amen ! » lança Imberto avant de s'envoyer une nouvelle gorgée de vin. Puis il se leva avec vigueur, empoignant un sac posé à ses côtés. Les deux témoins l'imitèrent, se dépêchant de finir leurs verres.

« Il est temps pour nous de vous laisser, messire Jacomo. Il me reste encore beaucoup de choses à faire avant de mettre les voiles. Et tôt parti, tôt rentré comme dit l'adage. »

Il tendit la main au jeune marchand, son associé.

Ce dernier l'accepta de bon cœur, un large sourire illuminant son visage.

« Je suis très heureux de vous voir prendre mes produits, maître Mallonus. Il y a tant à faire avec l'Égypte, et rares sont les associés assez aventureux pour y mener les denrées les plus demandées.

--- Je ne doute mie que nous ferons d'autres beaux trajets à venir, ami. »

Il salua les présents d'un signe de tête et s'avança vers la porte menant à l'escalier, bientôt suivi par les deux témoins. Le vieux notaire caressait machinalement son registre en sirotant son vin, l'air béat. Réalisant qu'il venait d'en absorber la dernière goutte, ses traits mobiles virèrent à l'affliction, et il lança une œillade envieuse vers le pichet. L'air de rien, il se tourna vers Jacomo, tenant son verre vide devant lui.

« Messire, il me semble tout de même bien naturel de vous présenter force félicitations pour la bonne fortune dont vous jouissez. Et du courage dont vous faites montre. Il me semble vous voir encore tout marmouset, suivant votre père en ses affaires, il y a si peu.

--- Je n'ai certes guère attendu avant de m'intéresser au négoce. Et n'ai pas perdu de temps à voyager pour enrichir d'autres. L'Espaigne fut pour moi la chance que d'aucuns espèrent toute une vie.

--- Votre père fut bien avisé en cette affaire, je l'avoue. Quelle tragédie qu'on s'en soit pris à lui, un si grand homme ! »

Jacomo baissa la tête, le visage soudain fermé et les yeux brillants d'un éclair hargneux.

« Les émeutiers n'étaient que bêtes enragées ! Il aurait fallu en pendre quelques dizaines et le calme serait revenu bien vite en la cité. Les Consuls ne se sont guère montrés à la hauteur en cette question. Père en est mort de chagrin, j'en suis acertainé.

--- Il faut les comprendre, beaucoup ont perdu en cette affaire d'Espaigne, les navires sont restés trop longtemps au loin...

--- On dirait que vous êtes en accord avec ces miséreux, incapables d'œuvrer à la richesse des leurs. Étais-je si riche avant que ne mettions à la voile pour Almería ? Certes non. J'ai placé mes biens avec sagacité, comme nombre de mes amis. Et je moissonne ce jour fruit de mon labeur, de mes biens droitement gérés. »

Ému, le vieil homme sourit et prit quelques instants avant de déclarer :

« Je n'ai qu'un regret, messire, que votre père ne soit pas là en cet instant à nos côtés. Comme il serait fier de vous voir pareil au lion, ainsi qu'il l'était en ses jouvences. À travers son nom, vous portez sa mémoire bien haut. »

Embarrassé, Jacomo demeura coi un long moment, savourant l'instant et imaginant déjà les profits qu'il allait amasser d'ici peu.

Puis il tapota amicalement l'épaule du notaire avant de lui resservir une bonne quantité de vin.

« Pourriez-vous me faire copie de ce contrat, maître Embroni ? Je vous la paierai au tarif habituel.

--- Je n'en ai que pour quelques minutes, messire Jacomo » affirma le vieil homme, le regard étincelant à la vision de son verre plein.

« Prenez votre temps, maître. Peut-être souhaiteriez-vous quelques douceurs, pour accompagner ce vin ? J'en partagerai volontiers quelques plats en votre compagnie. »

Certain que le vieux gourmand ne déclinerait pas l'invitation, Jacomo se dirigea vers la porte pour donner des ordres. La compagnie du vieil Embroni lui était plaisante. Il avait couché par écrit toutes les transactions de la famille Platealonga aussi loin que remontait sa mémoire. Son père avait toujours conseillé de s'en remettre à lui pour les affaires qu'il aurait à traiter. C'était un notaire fiable, scrupuleux, et surtout discret. Avec le temps, Jacomo avait fini par le considérer comme un vieil oncle, un confident qui l'avait chaque fois soutenu dans ses projets professionnels.

### Gênes, port, matin du mardi 28 février 1156

La brume qui recouvrait le port s'avançait jusqu'aux abords de la galerie longeant la façade de l'entrepôt. Un seul des deux grands battants avait été ouvert, de façon à apporter un peu de lumière, et une table disposée à l'entrée avec quelques tabourets était utilisée pour y déposer les documents de travail. De nombreuses balles, des sacs, des paniers, des tonneaux attendaient de partir pour de lointaines destinations. Aucune poussière ne les recouvrait, et il était évident que la halle ne conservait pas longtemps son stock. Chaque chose y était proprement entreposée, inventoriée, classée.

Mais pour l'heure, le petit attroupement qui discutait dans l'air glacial et humide ne s'intéressait guère aux marchandises. Au milieu du groupe se tenait Jacomo Platealonga, chaudement emmitouflé dans une chape de laine écarlate. Il brandissait une épée et un fourreau qui faisaient l'admiration de ses compagnons. La lame, ornée d'une inscription et de croix, étincelait comme un miroir. La garde de bronze était réhaussée de têtes d'animaux, et le pommeau gravé de décors marins mettait fin à une fusée enveloppée de cuir noir. L'étui, de bois recouvert de maroquin émeraude, n'était pas en reste, avec de nombreuses appliques métalliques et une bouterolle dorée ciselée de délicats feuillages. C'était une arme magnifique, digne d'un comte, que le marchand venait de se faire livrer. Ses compagnons l'étudiaient avec attention, envieux d'un si bel objet.

« Où as-tu trouvé si splendide lame, Jacomo ? »

Demanda un jeune homme à l'air efféminé, vêtu de bleu des pieds à la tête.

--- Cédée par quelque Lombard, un négociant ami de Rustico. Fort étrange en ses affaires ! Il semble n'avoir jamais rien à négocier et se trouve pourtant à toutes les foires. Je devais l'encontrer à Milan pour la saint Michel et il n'y était pas. Il s'est présenté ici pour l'Avent alors que je ne l'attendais plus.

--- Tu as tout de même conclu marché avec lui ? Périlleuse idée selon moi.

--- Pour qui me prends-tu ? J'ai obtenu bon prix et ses produits étaient de première qualité, comme tu peux le voir. Tout n'était pas si bel et bon, mais il se trouve de quoi intéresser quelques contacts de mes amis. »

À sa gauche, un marchand tout en jambes, sec comme une brindille, l'air précieux, tendit les mains pour admirer le travail.

« Il est vrai, on ne peut qu'admirer semblable qualité. Même montée simplement, pareille lame peut se vendre sans problème 5 ou 6 sols. À quel prix les as-tu eues ?

--- Si je te le dis, tu vas encore pester comme diable en eau bénite !

--- Combien de pièces ?

--- J'avais trois tonnelets, dont chacun tenait trois douzaines de lames. Et j'ai obtenu le tout pour 16 livres ! »

Écarquillant des yeux ronds, le marchand ne put retenir un sourire dépité.

« Comment fais-tu pour toujours arriver à trouver pareilles aubaines !

--- Il était pressé de faire affaire me semble-t-il. Un projet important pour lequel il lui fallait toutes ses finances. J'étais là au bon moment. »

Un de leurs compagnons, qui avait un accent chantant, pouffa de rire.

« Il faudrait tout de même trouver un nom pour pareil négociant qui ne négocie guère ! »

Jacomo sourit à la remarque et rengaina son arme. Il ne lui restait plus qu'à faire accrocher l'ensemble à un baudrier et il pourrait l'arborer avec fierté.

« En quelle occasion pourras-tu la ceindre, ami ? Tu ne voyages guère et il est défendu d'en tenir une à sa hanche en la cité.

--- Je sais bien, mais il me faut parfois m'assurer contre marauds. Je ne suis point toujours assuré lorsque je rentre à la nuit ou en certaines venelles.

--- En ce cas, on va vouloir te dépouiller, rien que pour cette arme. Elle pourrait nourrir une famille plusieurs semaines, et pas de pain bis, mais de bonnes viandes ! »

Jacomo hocha la tête, reprenant son sérieux. Il renifla doucement et se tourna vers son ami à l'air goguenard.

« En ce cas, il leur faudra la prendre de mes doigts morts. Elle sera là pour tuer assaillants, pas seulement pour le paraître. Les consuls ont bien conscience qu'il nous faut être prêts à facer la vilenie si tôt hors de nos demeures. Il leur faudra bien accepter un jour que les membres de la Compagna ne restent sans défense. Il y a trop de jaloux, de fainéants avides de rapines... »

Rapidement se répandirent de sincères hochements de tête dans le groupe d'opulents marchands chaudement vêtus. Ils étaient tous conscients de la fragilité de leur situation, malgré tous leurs biens accumulés, voire à cause d'eux. Maints déshérités leur reprochaient leur insolente richesse, la comparant à leur propre détresse quotidienne. Les troubles qui avaient enfiévré la ville quelques années plus tôt, suite à l'expédition espagnole, leur avaient coûté, momentanément, le pouvoir. De nombreuses échauffourées avaient éclaté un peu partout et ils demeuraient inquiets à l'idée qu'on pouvait de nouveau s'en prendre à eux et aux leurs.

Jacomo n'était pas le seul à penser à s'armer.

### Gênes, cathédrale San Laurenzo, matin du dimanche 3 juin 1156

La foule était importante, comme à chaque messe dans la cathédrale. Les plus dévots étaient près du chœur, talonnés par les fort ambitieux. Car s'il était de mise pour un chrétien d'entendre l'office célébré par l'archevêque et les chanoines, il était indispensable pour un notable de la cité de s'y faire voir, avec ostentation. Les bousculades n'étaient pas rares, et il fallait souvent jouer des coudes pour se maintenir à un bon emplacement.

La richesse des tenues des fidèles faisait écho à la superbe des ecclésiastiques, et on retrouvait chez chacun soieries, broderies fines, passementerie et bijoux précieux. Certaines élégantes passaient une partie de la nuit à se préparer pour se montrer à leur avantage. Jacomo ricana à la vue de certaines femmes qui n'avaient plus l'âge, ni le physique, pour faire semblable étalage de leurs charmes, mais qui s'entêtaient pourtant. Il n'était d'ailleurs lui-même pas en reste. Il était vêtu d'un long bliaud de fine laine anglaise verte, dont les motifs en arêtes chatoyaient sous la lumière. Des bandes brodées de soie et d'or en ornaient plusieurs endroits. Ses chausses, d'un bleu profond, étaient parfaitement ajustées et ses pieds étaient protégés de souliers de souple cuir noir sur lesquels des fils de couleurs vives traçaient des entrelacs. Une cape de la même étoffe que son bliaud était posée sur ses épaules, doublée de fourrure courte. Il portait en outre de nombreux bijoux, bagues et broches. Sa ceinture, de fils de soie tissés en d'élégants motifs animaliers, arborait une aumônière brodée de belle taille, dans laquelle il avait placé quelques piécettes à distribuer aux pauvres et aux indigents.

Il était pour l'heure appuyé contre une des sombres colonnes de l'église, laissant son regard errer parmi les groupes de riches marchands devant lui. Il s'attardait sans vergogne sur les jeunes filles des puissantes familles patriciennes, espérant y trouver une future épouse. Un de ses compagnons le remarqua et le tança amicalement, le poussant du coude.

« Te voilà de nouveau en chasse ? As-tu trouvé jeune poulaille à ton goût ?

--- Pas vraiment. J'ai encore temps. On m'a dit que Benedetto Spinola avait jolie jeunette d'à peine douze printemps. Cela pourrait faire mon affaire.

--- Je ne te savais pas si fort porté sur la jouvence !

--- Ce n'est pas tant la fille qui me semble joliette que sa famille de belle importance. J'ai besoin d'appuis si je souhaite mes affaires prospérer. Pour le reste, je sais bien où m'adresser. Quelques deniers donnés en la bonne maison me donnent occasion de m'épancher à plaisir. »

Badin, son voisin hocha la tête. Lui-même était encore jeune, trop pour espérer se marier avantageusement. Il lui fallait accroître sa richesse et améliorer sa position sociale. Mais pour l'heure, il songeait surtout à s'amuser. Il donna du coude dans les côtes de son ami.

« Avise la Pedegola qui arrive, voilà belle matrone qu'il me plairait fort de couvrir ! »

Jacomo fit une moue condescendante.

« Je ne sais si tu ferais là belle affaire. Oberto a fort à faire avec elle. Il lui faut souventes fois user de force pour la contraindre à son devoir. Il me l'a dit tantôt que nous étions chez Alda. »

L'autre soupira.

« Quelle pitié ! Elle a si beau minois et belles formes. Je m'ensouviens quand elle était jouvencelle, toute en cheveux, avec ses nattes brunes.

--- Il en est de certaines femmes qui échaudent le sang, mais n'aiment à l'apaiser. Je n'en voudrais mie. Tant qu'elle me donne beaux enfants, sans trop rechigner, je n'en demande pas plus.

--- Tu ne la voudrais tout de même pas comme la fille Boleti ? » se moqua son compère. La remarque arracha un rire surpris à Jacomo.

« Restons dans la mesure. Point trop belle, je m'en accommoderai, mais pareille à porcelette, certes pas. Je ne tiens pas à avoir hontage chaque dimanche pour la mener ici. »

Pendant qu'ils discutaient, comme tout le monde autour d'eux, la cérémonie se finissait lentement et ce n'est qu'au moment de l'« ite missa est »[^18] qu'un sursaut religieux les parcourut, le temps de dire « Amen » à l'unisson. Puis la foule commença à sortir, doucement, chacun s'interpelant désormais sans plus aucune retenue.

Certains avaient fait mener leurs montures au bas des marches, ce qui aggravait la cohue. De nombreux marchands ambulants tentaient d'écouler leur bimbeloterie tandis que des défavorisés tendaient la main, dans l'espoir d'une aumône. Lorsqu'il déboucha sur le parvis, Jacomo discutait avec quelques confrères de la saison qui s'amorçait.

Malgré les événements dans le sud de la péninsule, entre le pape Adrien, Guillaume de Sicile et Manuel Comnène le basileus byzantin, les perspectives étaient bonnes. Une vaste flotte de navires était prévue pour l'Outremer[^19], et les notaires n'avaient guère chômé pour rédiger tous les contrats. Après quelques années maigres, la prospérité allait revenir. Beaucoup misaient sur les nouveaux royaumes latins au Levant, espérant un rapide développement avec la prise récente d'une cité comme Ascalon. En outre, la situation de guerre permanente qui y régnait permettait d'y écouler facilement certaines marchandises, difficiles à se procurer là-bas.

À cause des interdictions officielles et, surtout, cléricales, Jacomo avait longtemps hésité à commercer avec les musulmans. Cela tenait autant à une pieuse superstition qu'à son désir de fuir les ennuis. Il s'était laissé convaincre, emporté par l'enthousiasme de nombre de ses contacts, dont les profits amassés le rendaient songeur. À l'avenir, il serait toujours temps pour lui de verser quelques subsides à l'Église pour que des messes soient dites afin de racheter son âme. Une nuée de pauvres hères les assaillirent subitement, bras faméliques habillés de haillons crasseux, visages hâves et édentés, yeux caves et suppliants. Beaucoup n'avaient guère plus d'une dizaine d'années, trop jeunes pour se vendre et trop vieux pour avoir un parent qui s'occupe d'eux.

Jacomo porta la main à sa bourse et en sortit quelques pièces de cuivres, méreaux qui donneraient droit à un peu de pain et de soupe. Il distribuait volontiers et se trouva rapidement encerclé. Il haussa la voix pour ramener le calme et se faire un peu remarquer. Il ne donnait pas uniquement pour nourrir des bouches. Il savait que la parentèle Visconti appréciait qu'on soit prodigue envers les pauvres. Le vieil archevêque Siro de Porcello lui-même encourageait la charité, l'estimant comme la juste contribution des plus riches au bien commun. S'il souhaitait attirer un jour l'attention d'un de ces notables de premier plan, il devait leur montrer qu'il pouvait faire jeu égal avec eux, et qu'il souscrivait aux mêmes valeurs. Par ailleurs, il finissait par apprécier ces distributions dominicales, où il obtenait de la gratitude à bon compte. Il n'était pas encore suffisamment âgé, cynique et endurci pour ne pas affectionner des remerciements enthousiastes, bien que bruyants.

### Gênes, maison d'Alda la Joliette, nuit du mardi 31 juillet 1156

L'ambiance était plutôt braillarde lorsque la matrone, Alda, revint avec quelques jeunes filles. La vaste salle était emplie de chants inspirés par le vin, dérivés du poème qu'avait entamé un jongleur loué pour la soirée. Imperturbable, ce dernier tentait avec son psaltérion et bien peu de réussite à couvrir les beuglements. L'arrivée des femmes ramena le calme, chacun reprenant ses esprits tout en examinant d'un œil approbateur ce qui était proposé. Comme il s'agissait d'une maison réputée, où les hommes les plus riches aimaient à se délasser, elles étaient toutes assez jeunes, et pouvaient contenter tous les goûts.

Quelques-unes venaient visiblement de l'autre côté de la Méditerranée, ténébreuses beautés au regard éteint. Habillées simplement de leurs chemises et de leurs chausses, elles avaient toutes des coiffures soignées et le teint fardé. Selon leur visage, on les sentait bravaches, soumises ou perdues, indifférentes ou résignées. Alda, la seule entièrement vêtue, d'une cotte taillée dans une magnifique étoffe orange, les faisait marcher, danser quelques pas pour les mettre en valeur. Elle ne les brusquait pas, les invitant d'un geste gracieux. Pas encore assez vieille pour ne plus séduire, elle conservait de sa jeunesse une opulente chevelure blonde qui avait fait sa fortune. Et si quelques rides plissaient désormais les contours de ses yeux et de sa bouche, elle savait toujours enflammer les cœurs d'une œillade adroite, les corps d'une science experte.

Pourtant, lorsqu'elle dirigeait sa maison, nulle humanité n'habitait son regard. Son inflexible réputation suffisait généralement à ce qu'elle soit obéie sans retard. On lui prêtait de terribles vengeances sur les filles qui avaient eu le malheur de lui déplaire. Parmi les arrivantes, Jacomo crut reconnaître une jolie brune, le visage rond et les pommettes écarlates. Ses yeux étaient baissés, sans vie, mais évoquaient une pétulance qui l'intriguait. Il fit signe à la maquerelle de s'approcher tout en désignant la jeune femme.

« Dis-moi, Alda, voilà jolie novelté en ton hostel. Il me semble en avoir connoissance. »

La femme minauda un instant, cherchant toujours à plaire aux hommes.

« Fort possible. Tu ne l'as certes pas vue en chemise jusque-là, mais pour quelques pièces, elle sera à toi toute la nuit, bel étalon. »

Elle sourit, joueuse et s'approcha avec lascivité, effleurant de ses lèvres l'oreille du jeune homme.

« C'est la fille d'Anselmo Usura, la petite Benedetta. »

À ce nom, le visage de Jacomo s'éclaira, un rictus mauvais s'y déployant. Il susurra :

« On m'avait dit qu'il avait été mené au fétu[^20], mais je ne le savais si bas. »

Alda se recula, s'appuyant langoureusement sur l'épaule. Elle plissa légèrement les yeux, se pourléchant comme un félin repu.

« Elle n'est pas fort experte, mais sa belle nature vaut le prix. Elle a fiers tétins et doux pertuis, rasé de frais.

--- Sais-tu que je l'avais convoitée en mes jouvences ? Elle m'avait moqué devant tous mes amis, me trouvant en bien trop pauvre tenue pour l'aborder.

--- Voilà belle occasion pour toi de savoir si le fruit a le goût de la fleur. »

Jacomo acquiesça silencieusement, souriant pour lui-même. Le vin l'abêtissait et il ricana à l'idée qui se formait dans sa tête.

« Il me plairait fort de la partager avec quelques miens amis, parmi mes compagnons. »

Alda le regarda, l'air faussement courroucé, faisant mine de le frapper d'un geste délicat.

« La pauvresse, que voilà bien méchante proposition !

--- J'y mettrai bon prix, maîtresse, cela va sans dire. »

La femme se releva, laissant glisser sa main sur le bras de Jacomo. L'air mutin, elle penchait la tête, comme hésitante. Affectant une moue provocante, elle lâcha :

« Pas plus de trois alors, et tu paieras de plus une quarte part. »

Jacomo acquiesça d'un hochement de tête lourd, embrumé par l'alcool.

« Prépare-la moi, maîtresse, j'arrive ! »

Il se leva péniblement et tituba quelques pas, faisant signe à deux de ses compagnons d'approcher. Il leur expliqua ce qu'il avait concocté, s'attirant des remarques amusées et éveillant des regards libidineux.

Une fois l'affaire entendue, Jacomo s'excusa : il lui fallait d'abord s'absenter quelques instants, temps pour lui de se soulager de tout le vin ingurgité. Il ne se souvenait pas de l'endroit où pouvaient se trouver quelques lieux d'aisance, et décida donc d'aller au plus court, dans la ruelle voisine. Tandis qu'il avançait dans le couloir, il croisa un gros homme, qui le considéra d'un air enjoué. Reconnaissant immédiatement son ami Pedegola, il le salua joyeusement de la main.

« Alors, Oberto, on vient se mêler à canaille ?

--- Tant qu'il n'y a que compaings pareils à toi, cela me sied, Jacomo. »

Ils se serrèrent la main chaleureusement. Le jeune homme fronça les sourcils, cherchant à arborer un profil sévère.

« Mais est-ce bien là lieu pour tôt marié ? Votre jeune épousée n'est-elle déjà plus à votre goût ? »

Franchement surpris, Oberto Pedegola fit une moue dégoûtée.

« Tout de même, ami, elle est là pour me donner enfançons ! Ces jouvencelles ont autre usage... »

Jacomo hocha la tête sans retenue.

« Comme l'a dit saint Augustin, compaing ! Le fier évêque d'Hippone lui-même ![^21]»

Il tapa joyeusement sur l'épaule de son confrère et ami et continua dans le couloir. Une lampe à huile éclairait le passage ouvrant sur une ruelle sombre et discrète. Au débouché dans la noirceur, Jacomo plissa les paupières, cherchant à discerner la venelle. La lune gibbeuse était cachée parmi d'épais nuages, lourds de pluie, et il était difficile de voir où on mettait les pieds. Titubant légèrement, il tenta d'avancer de quelques pas, histoire de ne pas se soulager juste à l'entrée. Les yeux baissés, il manqua de heurter une silhouette surgie devant lui. C'était le valet de Pedegola, chargé de surveiller la monture de son maître. L'homme s'excusa en reculant rapidement.

« Mordiable ! Veux-tu finir couvert de pissat ? » s'exclama Jacomo, énervé, dans une éruption de postillons. Finalement, la remarque l'amusa tant qu'il se mit à rire et commença à uriner en direction du domestique, qui détala sans demander son reste, peu désireux d'accomplir son devoir sous si sordide crachin.

« Voilà ! Il me plaît d'avoir mes aises lorsque je me soulage. »

Pour ne pas se montrer mesquin dans son ignonomie, il ajouta quelques rots sonores et un pet, puis cracha aux alentours le temps de soulager sa vessie. Après cela, il lui fallut quelques minutes pour remettre ses braies en place, gêné par la longueur de sa cotte et son degré d'alcoolémie. Il finissait son inspection, satisfait, et s'apprêtait à rentrer dans la maison lorsque des pas feutrés s'approchèrent de lui, furtivement. Trop éméché, il ne comprit rien à ce qui lui arrivait.

### Gênes, port, matin du lundi 6 août 1156

Berta avançait en trottinant, moitié dansant, moitié sautant. Elle aimait se rendre dans le port, où elle pouvait admirer les navires tout en pêchant des amorces pour son père. Bien sûr, comme toujours, ses deux frères l'accompagnaient, plus grands qu'elle et parfois un peu bêtes. Dans l'ensemble, ils étaient néanmoins assez gentils et la protégeaient quand quelqu'un faisait mine de l'ennuyer. Chemin faisant, elle essayait de se remémorer la chanson que le bateleur avait déclamée la veille lors d'un spectacle sur la placette à côté de chez elle. Il y était question d'oiseaux, de voyages et de bateaux, mais elle n'arrivait pas à se rappeler les paroles, se contentant donc de fredonner l'air.

Naviguant au milieu de la cohue habituelle avec aisance, les trois gamins retrouvèrent un coin où les courants menaient beaucoup de détritus. De nombreux poissons y étaient attirés, garantissant de belles prises à chaque fois. Il fallait de temps à autre jouer du poing avec quelques terreurs installées là, mais en général, la cohabitation se faisait assez paisiblement.

À peine arrivée, Berta s'assit sur la berge, les pieds ballants, et lança sa ligne d'un geste sûr. Ses petites mains maniaient avec expertise la canne, menant son appât dans les endroits les plus poissonneux. Appliquée, elle ne quittait que rarement des yeux son fil, simplement pour s'assurer que ses frères étaient toujours dans les environs. Finalement insatisfaite, elle se déplaça auprès des nefs d'où l'on déchargeait tout ce qui pouvait s'acheter et se vendre autour de la Méditerranée. D'un coup de poignet, elle ramena sa ligne, cherchant parmi les courants troubles les amas d'alevins où elle pourrait faire sa moisson.

Soudain elle poussa un cri strident. Elle venait de voir le dos d'un homme flottant entre deux eaux, juste contre la coque du navire. Une fois la foule entassée aux abords, quelques-uns prirent les choses en main. Il fallut l'intervention de plusieurs gaillards pour hisser la dépouille sur le quai car le corps était très abîmé, ayant passé un long moment dans les flots. Plusieurs animaux opportunistes y avaient d'ailleurs trouvé leur pitance. Après de nombreux palabres, et le concours de plusieurs hommes d'importance, on finit par l'identifier, à son riche bliaud, qui n'était, comme son porteur, plus que l'ombre de lui-même.

C'était un marchand prospère, un jeune qui promettait beaucoup, le fils du vieux Platealonga, lui-même décédé depuis peu. Il se nommait Jacomo. Quelques confrères attristés se signèrent et repartirent d'un pas vif vers leurs affaires, adoptant un éphémère masque d'affliction pour la circonstance. Finalement, deux manouvriers se mirent d'accord pour emmener la dépouille à la famille, espérant quelque récompense pour leur peine. Seule Berta restait, les yeux humides, reniflant à la vue du cadavre s'éloignant. Son frère s'approcha et la serra contre lui.

« T'inquiète, la môme. Il a de sûr eu son content avant d'aller. »

De sa manche, la petite fille s'essuya le nez et lança à son frère, l'air maussade :

« Et alors ?

--- Il n'y a pas de quoi pleurer, sois acertainée, il a mangé bon pain chaque jour ce gars. Te mets pas en pleurs pour lui.

--- C'est pas pour lui, c'est parce que moi, en le voyant, j'ai lâché la jolie canne que père m'avait donnée » ❧

### Notes

Les marchands génois employaient essentiellement deux types de contrats pour leurs investissements commerciaux. Un investisseur, le socius stans apportait les capitaux que le socius tractans se chargeait de faire fructifier au loin. Lorsque l'apport était uniquement le fait du socius stans, on parlait de commende, et le bénéfice était partagé, 25 % revenant au socius tractans. Dans le cas où ce dernier versait un tiers du capital, on parlait de societa et cette fois, il récupérait la moitié du bénéfice. La participation des forces génoises à la prise d'Almería et de Tortose, au moment où la Seconde croisade échouait devant Damas ne se fit pas sans douleur. L'éloignement des navires pendant de longs mois mit en grand danger beaucoup de marchands, incapables d'acheminer leurs denrées. Cela entraîna une crise politique dont les soubresauts durèrent un bon moment. La conversion des monnaies se faisait comme suit : 12 deniers pour un sou, 20 sous faisant une livre. À noter que les hautes valeurs n'existaient que sous forme d'unités de compte.

### Références

Bach, Erik, *La cité de Gênes au XIIe siècle*, Gyldendal, Copenhague : 1955.

Balard, Michel.« Les transports maritimes génois vers la Terre sainte », dans *I comuni italiani nel Regno crociato di Gerusalemme*, Airaldi G., Kedar B.Z. (éds.), Gênes, 1986, p.141-174.

Epstein, Steven A., *Genoa and the Genoese 958-1528*, Chapel Hill & Londres, The University of California Press, 1996.

Epstein, Steven A., « Secrecy and Genoese commercial practices », Journal of Medieval History, Vol. 20, Décembre 1994, p. 313-325.

Tortueuse âme
-------------

### Gibelet, soirée du mercredi 26 mai 1154

« Sont-ils tous pareillement fols ? Nul prince n'a donc soupçon de jugeote que ce soit chez les infidèles ou chez nous ? »

Le vieil homme accoudé à la magnifique fenêtre sculptée de sa demeure pestait d'une voix habituée à commander, frappant du poing l'appui tandis qu'il vociférait. De rage, il avait froissé le document à l'origine de son courroux et l'avait jeté à terre. Il ne doutait pas de sa véracité, son auteur était un de ses fidèles et le coursier, Guilhem Torte, un sergent de valeur qu'il employait depuis des années. Ce dernier avait chevauché durement, la boîte de message attachée à sa ceinture, sans savoir exactement ce qu'elle recelait, mais prêt à mourir pour en délivrer le contenu à son juste destinataire, Mauro Spinola. En outre, pour avoir combattu et voyagé depuis plus de dix ans sur les pistes et dans les villes de la région, il était autant au fait de la politique locale que son maître, peut-être même plus, n'ayant nul besoin de s'absenter régulièrement pour s'en retourner dans la cité pour laquelle le vieux diplomate s'agitait tant, Gênes. Et ce qu'il avait glané comme informations tandis qu'il revenait des zones méridionales causait beaucoup d'inquiétude au génois.

Remâchant sa colère, Spinola se tourna vers le soldat assis sur un des bancs dans la salle. Sans sa tenue de guerrier latin, on aurait facilement pu le prendre pour un syrien ou un égyptien. Portant encore son haubert plein de poussière, il n'avait qu'ôté la coiffe de mailles et demeurait en partie couvert de la crasse du voyage. Ses yeux bruns brillaient dans un visage enlaidi par la fatigue, les rides, et l'inquiétude. Sa barbe naissante et ses courts et épais cheveux gras n'arrangeaient guère l'image qu'il donnait. C'était un homme de guerre, au faciès rude, à la mâchoire agressive, s'accommodant volontiers de cela sans y prêter attention. Il ne releva pas le mouvement d'humeur de son seigneur, comme si cela faisait partie des échanges normaux, et s'employait à choisir un fruit dans la coupe vernissée posée sur la table à son intention. Sa décision prise, il frotta la figue en un geste dérisoire, de sa main sale, et intervint avant de mordre à belles dents.

« L'Égypte n'est que bataille et sang ! La parentèle du calife assassiné a mandé Al-Salih Tala'i' ben al-Ruzzayk. Il est rentré du désert méridional, menant fer et feu au Caire. La victoire paraît sienne, les comploteurs s'apprêtent à fuir vers les plateaux du Sinaï. »

Frottant son crâne qui se dégarnissait largement, Mauro commença à faire les cent pas devant les magnifiques ouvertures de sa demeure, donnant sur le port de la ville. Au-dehors, le ciel s'empourprait, ombré de nombreux nuages filandreux qui formaient autant de taches sombres au-dessus de la mer.

« Noreddin[^22], ce damné turc va de certes tenter de prendre place, ou imposer un des siens. Maintenant qu'il s'est assuré de Damas ! Après que son père ait forcé Édesse, que les démons l'emportent !

--- Votre homme semblait accroire que cela faisait de l'Égypte un fruit mûr pour le roi Baudoin[^23], au contraire. Et que de toute façon, jamais hommes du Caire ne se livreraient à un turc du calife de Bagdad. »

Le diplomate dévisagea son valet comme s'il avait proféré une grossièreté en plein milieu de la messe. Puis il reprit ses allées et venues, les bras désormais dans le dos, les yeux se plissant tandis qu'il s'efforçait de comprendre ce qui se passait, et les perspectives qui en découlaient. Son sergent, impassible, attendant la suite, en était à son sixième fruit lorsque Mauro ouvrit de nouveau la bouche, la voix vibrant de colère contenue.

« Et ce maudit syrien, ibn Munqidh, où est-il ? Si jamais il tente d'attirer Noreddin en Égypte, nous sommes perdus ! »

Le sergent fit une moue.

« Il était du complot, c'est de bonne fame. Il doit se terrer avec ses compaings parmi roches du désert. Je ne serais guère surpris qu'il espère joindre Damas, il y a forte acquointance.

--- Mais puissants ennemis aussi, pour notre bonheur. Faisons assavoir sa male fortune, peut-être à l'émir 'Ayn ad-Dawla[^24].

--- Il y faudra grande habileté. Voilà fier baron, et bien féal à son sire Noreddin. Il ne faudrait pas qu'il voie là quelque manœuvre. »

La remarque fit sourire le vieil homme. L'air enfin déridé, il prit le temps de réfléchir avant de répondre, lentement, tout en se mordillant les lèvres.

« Il doit déjà en savoir autant que nous. Autant ne pas se disperser pareillement. Songeons surtout à ne pas permettre le rêve d'union entre princes syriens et calife d'Égypte. Cet espoir est mort avec l'ancien vizir Sallâr, et doit le demeurer. »

Hochant la tête, le soldat avala une bouchée, puis renifla avant d'arborer une mine assurée.

« Outre, il se murmure au palais du roi Baudoin qu'on ne va tarder à lever le ban dans le sud, pour éviter que des fuyards ne ravagent la Judée.

--- Excellent ! Releva Spinola, une lueur malicieuse dans l'œil. Espérons que le roi grappe si belle occasion de tenir les ennemis du futur vizir. Cela pourrait peser bon poids en la balance à l'avenir. Au final, il y a peut-être belles gerbes à moissonner de ces stupides querelles de palais. »

Avisant le triste aspect de son messager, il se racla la gorge.

« Tu devrais prendre quelque repos, Guilhem, je vais avoir besoin que tu retournes en le royaume, mais pour m'escorter cette fois. Je ne peux demeurer dans le comté alors que tout se joue au loin. Je vais donner ordre pour que nous partions au levant demain. »

Une ombre fugace, reflet d'une déception ou d'un agacement, survola subrepticement le visage du coursier. Puis il se leva, un peu raide, heurtant son siège du fourreau de la longue épée qu'il arborait toujours sur la cuisse. Il salua d'un bref mouvement de tête et se retira d'un pas rapide, laissant retomber la lourde tapisserie derrière lui en sortant. Désormais seul, Mauro Spinola se pencha et ramassa le document, le relisant machinalement. Glissant le fin morceau de papier dans une de ses manches, il quitta à son tour la pièce, en direction des cuisines où il était sûr d'y trouver des flammes pour le détruire.

### Gaza, nuit du lundi 7 juin 1154

Dans la nuit noire, seuls les feux allumés par les hommes de faction lançaient une lumière tremblante à travers les bâtiments encore nombreux à être ruinés. La ville n'avait été réinvestie que depuis quelques années, le château tenu par les chevaliers au blanc manteau du Temple garantissait désormais une relative protection. Depuis la prise d'Ascalon, que la forteresse était auparavant censée surveiller, des colons avaient saisi l'occasion de s'installer et s'étaient attelés à la tâche de reconstruire les édifices écroulés. Le travail à accomplir était immense et des quartiers entiers n'étaient encore qu'amas de décombres, tronçons de toitures, murs béants.

À la périphérie de la ville, une de ces demeures partiellement debout scintillait des feux d'un occupant temporaire. Mauro Spinola s'y était établi avec ses serviteurs, dans le sillage des guerriers du roi de Jérusalem en quête des comploteurs fuyant l'Égypte. Ces derniers étaient venus prêter main-forte à la garnison du Temple installée là, mais le vieux génois avait tout autre projet en tête. Il pourchassait les informations, rarement les hommes.

Pour l'heure, il soupait d'un épais gruau, en silence, assis sur une poutre dans le renfoncement d'une bâtisse à demi écroulée. Il attendait d'en apprendre plus sur la victoire de la journée. Les combats avaient été rudes, mais les Latins fêtaient désormais leur réussite. Restait à savoir quelle était l'étendue de ce succès. Le treillage qui servait de porte fut soudain repoussé, laissant apparaître l'homme de faction dehors, puis Guilhem. La mine réjouie, il portait une cruche, qu'il tendit à ses compagnons en entrant.

« Grande veillée que voilà. Les sergents du Temple du Christ arrosent dignement leur butin. Je ne saurais nombrer les montures prises ce jour. Ils les ont parquées aux abords en si vaste enclos ! »

Mauro finit d'avaler sa cuiller et désigna du menton un vieux tabouret de guingois à son homme.

« J'espère qu'ils n'ont pas saisi que destriers, même turkmènes !

--- Certes pas. Ils tiennent enferré Nasr ben 'Abbâs en la forte tour. »

Le vieil homme esquissa un rictus de contentement, mais ne cessa d'avaler son repas en silence, désireux d'en apprendre plus. Guilhem s'empara à son tour d'une écuelle qu'on lui tendait et engloutit quelques bouchées avant de continuer, l'air moins affable.

« Par contre, son père a connu malemort en la bataille. »

Spinola contracta les mâchoires, fronçant le nez à la nouvelle qu'il ne goûtait guère. Il darda deux yeux inquisiteurs sur le sergent, impatient de poser la question qui le taraudait.

« Et le *Syrien* ? Munqidh ?

--- Nulle trace de lui. Il a réussi à trouver passage, mais on ne sait pour où !

--- Le maudit ! Nul doute qu'il ne quittera les étriers qu'une fois sauf en les murailles damascènes !

--- Peut-être va-t-il tenter de retrouver la protection des siens, à Shayzar... »

Le génois racla son écuelle d'un mouvement rageur, avalant rapidement une dernière bouchée.

« Je n'y crois guère. Il va mendier auprès des Turcs, plaider sa cause. Comment une garnison a-t-elle pu manquer un vieillard comme lui ! Il est encore plus vieux que moi ! »

Personne ne répondit à la question, toute rhétorique. Guilhem se contentait de dévisager son maître, avalant tranquillement sa bouillie comme si c'était son dernier repas. Il savait que des ordres n'allaient pas tarder, l'esprit minutieux de Spinola s'était mis en branle, et accoucherait d'un plan, comme toujours. Cela lui laissa le temps de finir son plat et de se servir un gobelet de vin coupé. Il en dégustait la première gorgée lorsque la voix grave reprit.

« Je vais demander audience au plus vite au châtelain des frères du Temple et lui conseiller de céder Nasr ben 'Abbâs aux Égyptiens. Tu as possibilité de te rendre prestement au Caire ?

--- Je pourrais trouver nef de pêcheur pour me mener sur les côtes, vers al Arish. De là, louer rapide monture... Comptons jusqu'à la saint Gervais[^25] pour faire bon poids. »

Présentant une petite bourse qu'il avait sortie de son vêtement, Mauro la lui lança.

« En ce cas, ne tarde pas. Rends-toi au port de Taydâ et apprête-toi au départ. Attends là-bas mon message. Tu le mèneras sans tarder au nouveau vizir, avec force discrétion.

--- De qui puis-je me recommander ?

--- Si on te questionne fort avant, tu pourras lâcher mon nom. Mais seulement si tu devines grand risque pour toi ou ta mission. »

Congédiant son homme de la main, Spinola s'appuya le menton dans la paume, plongeant son regard vers le sol, fuyant la lumière du petit foyer. Au moment de franchir la porte, le sergent se figea, puis se retourna vers son maître, le visage interrogateur. Apercevant du coin de l'œil le geste, Mauro leva un front agacé.

« Eh bien ! Ne demeure là figé comme statue ! Tu as tes ordres ! »

Acquiesçant d'une moue qu'il s'efforça de rendre convaincue, Guilhem disparut dans la noirceur de la nuit. Il salua le garde d'un geste amical sur l'épaule et se dirigea vers son cheval, attaché auprès des vestiges d'une citerne. Lorsqu'il monta en selle, il lança un regard vers la forteresse du temple. On n'en percevait qu'à grand-peine les contours, avec la lune presque invisible, mais les fines raies de lumière qui y brillaient évoquaient autant de joyaux de plaisir au sergent. Il devait y avoir belles réjouissances parmi la garnison victorieuse. Chassant ces pensées de son esprit, il caressa de la main l'encolure de son cheval. Reniflant avec vigueur, il éperonna doucement et se mit en marche, avalé par les ténèbres.

### Gênes, matin du mercredi 2 novembre 1155

Lorsqu'il avait aperçu la puissante stature d'Imberto Mallonus déambuler parmi la foule amassée aux abords de la cathédrale Saint-Laurent, Mauro Spinola avait eu un sourire de contentement. Le navire était de retour, porteur de marchandises égyptiennes, d'argent et de nouvelles. Mais l'air grave du patronus[^26] avait rapidement mis un terme à cette bonne humeur. Comprenant qu'il s'agissait d'une affaire importante, il attira le marin un peu à l'écart, sur le côté des marches, indiquant au passage d'un geste à son valet de patienter en retrait. Le navigateur n'avait visiblement même pas pris le temps de se changer ni de s'apprêter pour venir le voir. Sa tenue était sale et sa mine affreuse arborait les stigmates d'une difficile traversée. Après un rapide salut, il entra directement dans le vif du sujet.

« Les Égyptiens ont viré de bord. Plusieurs de leurs nefs ont écumé les côtes du royaume, et nous n'avons qu'à grand-peine pu aborder à Alexandrie.

--- Que me contes-tu là ? Tu y étais avec l'aval du vizir Ruzzayk lui-même !

--- Certes, mais il s'avère qu'il y a grande colère là-bas, car on le trouve bien trop amiable avec les chrétiens.

--- Alors qu'il fait assaillir nos ports ?

--- L'escadre n'est peut-être pas de son fait... »

Mauro fit claquer sa mâchoire de surprise. Il n'avait pas prévu que le nouveau vizir égyptien n'arriverait pas à tenir ses officiers. Il avait misé sur lui et jouait gros, à la fois en tant que diplomate, mais aussi comme marchand. Comprenant l'embarras du vieil homme, Mallonus enchaîna.

« Par heureuse fortune, les balles... importantes étaient faciles d'accès. Je les ai donc déchargées prestement. Et j'ai préféré aller vendre le reste au royaume. Cela a reporté mon départ, mais avec le bon vent, cela n'a guère eu de conséquence. J'ai pu tirer bon prix tout de même des peaux et des étoffes. »

Hochant la tête en affichant un sourire de circonstance, Spinola eut un peu de baume au cœur en entendant cela, mais sur le fond, il était fortement affecté. Il s'en voulait de n'avoir pas su prévoir l'attaque. Il tenait son pouvoir de ses informations, mais aussi de sa capacité à pressentir l'avenir. Ses opposants au sein de la Compagna ne manqueraient pas de se servir de cela pour tenter de l'abattre. La cité n'avait retrouvé le calme que tout récemment, après les scandales liés à l'expédition espagnole. Il regrettait un peu d'y avoir pris part, cela l'avait entraîné trop loin de l'Outremer, et trop longtemps. Il fallait qu'il reprenne la main rapidement. Et pour ce faire, il avait peu d'alternatives. La seule option qu'il avait était égyptienne, les Turcs de Syrie se montrant décidément bien trop hostiles négociateurs à son encontre.

« As-tu idée de ton prochain voyage Outremer ?

--- Je viens à peine de mouiller les fers, messire ! Et la clôture[^27] est proche. »

Le diplomate acquiesça, légèrement agacé. Il allait devoir trouver un autre messager pour envoyer de nouvelles instructions à son espion au loin. Une fois averti, Guilhem saurait assurément dénicher ce qui lui manquait, prendre les bonnes décisions et lui faire parvenir un rapport précis. Lui-même ne pouvait embarquer, de nombreuses affaires le retenaient dans la cité, et il était important qu'il soit là en cette période difficile. Sans compter qu'il devrait vraisemblablement faire quelques voyages vers le nord, dans des régions qu'il appréciait moins que les côtes syriennes. Sa principale crainte venait du fait que le Temple avait exigé une rançon considérable pour Nasr, soixante mille dinars.

Ruzzayk avait certainement trouvé le montant un peu exagéré, mais il avait compris la nécessité de ne pas laisser un adversaire hors d'atteinte. Il s'était d'ailleurs empressé de le livrer à la famille de ses prédécesseurs massacrés, qui se chargea de lui faire payer cruellement ses agissements. Mais il avait peut-être gardé quelque ressentiment pour la somme extorquée,versée en outre à un ordre religieux qu'il savait être des plus féroces opposants à l'Islam.

Comprenant que le patronus attendait un signe de sa part, il le remercia chaleureusement et lui proposa de passer chez lui dans la semaine pour faire les comptes et se réjouir autour d'un bon repas des affaires réalisées. Impatient de profiter d'un peu de repos après un voyage éprouvant, le marin ne se fit pas prier et il repartit avec vigueur, non sans avoir salué avec respect.

Mauro Spinola chercha des yeux son valet et le découvrit assis sur les marches, occupé à discuter fort aimablement avec une servante au joli minois. Sans qu'il ne se l'explique, il en fut agacé et appela d'une voix furieuse son domestique. Comprenant immédiatement la situation, celui-ci ne prit qu'un bref instant pour retrouver sa place à la suite de son maître. Néanmoins, avant de s'éloigner sur ses pas, il lança un regard joyeux vers la jeune fille, qui lui répondit d'un salut enthousiaste.

Heureusement pour lui, Mauro était de nouveau plongé dans ses pensées et n'accordait guère d'attention à ce qui l'entourait, avançant instinctivement en direction de sa demeure. Le vieux diplomate commençait à douter de lui, ce qu'il n'appréciait guère. D'un côté, il était possible que Ruzzayk ait finalement décidé de prendre les armes contre le royaume de Jérusalem, ce qui était stupide de sa part, sauf dans le cas où il envisageait de se rapprocher des Syriens car il n'avait pas la puissance militaire de s'attaquer seul aux troupes franques.

Mais d'un autre côté, Ruzzayk pouvait suffisamment perturber les Latins pour les pousser à fragiliser leur frontière orientale et laisser ainsi le champ libre aux Turcs d'Alep et de Damas. Cela semblait néanmoins bien incroyable. L'Égypte sortait d'une difficile guerre civile, et il était plus probable que le vizir doive faire face à des opposants qui contrecarraient sa politique de trêve avec les chrétiens, voire à des opportunistes profitant de la relative anarchie pour faire rapidement fortune.

Dans tous les cas, cela contrariait fort les plans du vieil intrigant. Et ça, il n'était certes pas prêt à l'accepter sans se battre.

### Tripoli, matinée du lundi 24 décembre 1156

Lorsqu'il leva la tête de ses papiers pour accueillir l'hospitalier, Mauro eut un instant de surprise. Il ne s'attendait pas à un tel homme. Pour lui, les frères de saint Jean n'étaient guère plus que des valets en bure, occupés à nourrir et soigner les miséreux.

Pourtant, se tenait devant lui un grand gaillard porteur du manteau noir à la croix blanche, par-dessus une vêture assez riche, long bliaud sombre et fine ceinture passementée. Malgré une impressionnante musculature, ses mouvements étaient gracieux et son visage étonnamment fin. Les joues creusées, son regard paraissait presque fiévreux, larges orbites inexpressives sous d'épais sourcils. Sur son crâne, on apercevait une discrète tonsure, luisante, refaite certainement à l'occasion des fêtes de Noël à venir. Il sourit avec chaleur et inclina légèrement le buste, se présentant comme frère Raymond.

Le vieil homme se leva et l'invita à s'asseoir devant lui. Il était confortablement installé dans une petite pièce richement aménagée de tapis et de meubles orientaux. Seul élément occidental, une belle table à tréteaux était recouverte de nombreux documents, courriers, extraits de comptes, et recevait la vive lumière qui filtrait à travers les panneaux de verre.

Un scribe, les doigts tachés d'encre accueillit d'un sourire le nouvel arrivant puis se replongea bien vite dans ses travaux d'écriture. Après avoir appelé un valet pour qu'on leur serve à boire, Mauro se présenta rapidement avant d'aborder ce qui lui tenait à cœur, mais l'hospitalier ne lui laissa guère le temps de développer.

« Le frère cellérier m'a dit qui vous étiez, messire Spinola. Outre, il serait fort malheureux pour officier de mon ordre, si présent en le comté, de ne pas vous connaître, du moins de nom. »

Le vieux diplomate accepta le compliment avec grâce, mais circonspect quant à la suite, subodorant là une entrée en matière ambiguë.

« Vos produits sont toujours de premier choix, m'a-t-il affirmé, et vous n'exigez pas fortes sommes pour ceux-ci, ce dont mon ordre ne peut que vous louer. Mais si vous avez demandé à me voir, il y fort peu de chance que ce soit pour discuter négoce.

--- Certes non. Votre nom m'a été recommandé, car je recherche quelqu'un qui se trouve peut-être en geôles sarrasines. »

Frère Raymond marqua un temps puis écarquilla les yeux, attendant d'en savoir plus.

« Un des miens serviteurs a disparu depuis de nombreux mois alors qu'il était à Damas, il a pour nom Guilhem Torte. »

Le moine plissa les yeux, marquant son attention puis toussa avant de répondre d'une voix lente et posée.

« Je vois. Point crucial à prendre en compte : vous seriez prêt à verser rançon pour lui ?

--- Certes ! Il vaut bien chevalier. »

L'hospitalier remua sur sa chaise et se gratta l'oreille, pensif.

« Il me faudra une description, et la date présumée et le lieu où il aurait pu être pris également, et j'en parlerai autour de moi.

--- Mille grâces !

--- Il me faut néanmoins vous indiquer que je n'œuvre que peu avec les Damascènes. Depuis l'arrivée des aleppins, les contacts sont moins aisés. Je m'emploie surtout à libérer des captifs de pirates égyptiens. »

Mauro hoqueta de surprise.

« Je pensais que la trêve avec eux était efficace.

--- Elle l'est. Il s'agit de quelques pérégrins oubliés en leurs geôles depuis bien trop long temps. Ils ont été pris par traîtrise, peu avant la trêve. Avec un navire et des matelots maquillés. Comme ils étaient aux mains d'officiers en désaccord avec le vizir, il a été difficile de les récupérer. Et le prix demandé est exorbitant ! »

Prenant le temps de la réflexion, Mauro se tourna finalement vers son notaire et l'interpella :

« N'avons-nous pas ici quelques monnaies qui pourraient aider à la libération de captifs, Giacomo ? »

Décontenancé par la demande, le serviteur tourna la tête de droite et de gauche, comme s'il avait la capacité de faire apparaître les pièces près de son écritoire. Un soudain éclair d'intelligence lui permit de comprendre ce qu'on attendait de lui et il tendit la main vers un paquet de tablettes de cire reliées, qu'il commença à parcourir en marmonnant. N'ayant rien perdu de l'échange, frère Raymond inclina du chef avec élégance.

« Ne vous sentez pas mon obligé en ce qui concerne votre demande. Nous aidons à recouvrer les captifs, car c'est notre mission de chrétien. Nul don n'est requis.

--- Je l'ai bien entendu ainsi, frère, mais il me sied aussi de prêter aide et assistance à mes frères et sœurs engeôlés. »

Pendant ce temps, le domestique s'était approché et tendait une tablette à Mauro, indiquant de son stylet une liste récapitulant des bourses scellées contenant des dinars. Le diplomate écarta la tête, cherchant à lire les minuscules inscriptions, les paupières plissées, puis annonça à l'hospitalier :

« Je peux offrir à l'instant deux bourses de pièces d'or égyptiennes, la première de trente dinars et la seconde de quinze, toutes deux scellées par le Trésor califal.

--- Voilà fort charitable geste, messire Spinola. Je ne sais que dire !

--- Il n'y a rien à dire, frère. Dieu me donne assez pour que je partage. »

Il indiqua d'un signe de tête qu'on lui apporte le coffret ferré dans lequel il tenait ses monnaies. Puis il dévisagea l'hospitalier, encore ébahi de l'offre. Savourant sa surprise, il afficha un large sourire, assez énigmatique.

« Noël n'est-il pas occasion de s'enjoyer ? »

Lorsque l'affaire fut réglée et le frère hospitalier reparti, encore charmé de l'incroyable don qu'on lui avait fait, Spinola demeura un long moment le regard dans le vide, savourant un verre de vin de Judée qu'il appréciait tout particulièrement. Percevant que son écrivain semblait ennuyé et faisait beaucoup de tapage à bouger ses documents, il se tourna vers lui, un peu irrité.

« Qu'est-ce donc, Giacomo ? Pourquoi t'agiter pareillement ?

--- Je vous demande le pardon, messire. Je reprenais quelques comptes, car ces dinars vont peut-être nous manquer. Nous n'avons guère de monnaies, tout est en marchandises, et nous avons moult règlements prochainement.

--- Tu estimes ma générosité mal à propos, c'est cela ? »

Le vieil homme parlait doucement, sans qu'il soit possible de déterminer son sentiment. Son domestique dansait d'un pied sur l'autre, désireux de se sortir du mauvais pas dans lequel il venait de s'engouffrer. Il connaissait suffisamment son maître pour le savoir prompt à la colère. D'un autre côté, ne pas le prévenir pouvait tout aussi bien attirer sur sa tête tous les malheurs de l'enfer.

« Ce n'est certes pas à moi d'en juger, mais nous départir de dinars du trésor en pareil moment, ce n'est effectivement pas facile pour vos affaires. »

Le vieux génois examinait son gobelet de verre émaillé comme s'il y voyait quelque vérité cachée, et répondit lentement, appuyant les syllabes avec emphase.

« Tu as raison, Giacomo... »

Le notaire exhala un profond soupir de soulagement, qui s'interrompit bien vite.

« ...Ce n'est pas à toi d'en juger. » ❧

### Notes

L'espionnage et le renseignement jouaient bien évidemment leur rôle dans les conflits qui opposaient les puissances moyen-orientales au XIIe siècle. La bonne connaissance de la composition des armées adverses, des possibles défections qui pouvaient y être suscitées, constituaient tout un pan de la diplomatie intense qu'on pouvait observer alors. Des émissaires allaient et venaient entre les différents princes, et les marchands savaient se faire à l'occasion collecteurs d'information, ainsi que des voyageurs plus discrets, comme les pèlerins. Néanmoins, il n'est pas certain que la plupart aient eu une véritable vision d'ensemble qui leur aurait permis d'avoir une stratégie globale à l'échelle de la région.

Par ailleurs, la situation du califat fatimide après la perte d'Ascalon en 1153 se dégrada fortement. Les dirigeants se déchiraient dans des conflits internes, laissant la porte ouverte aux influences extérieures qui leur seraient fatales. Les Turcs et Kurdes de Syrie, sous le contrôle de Nūr ad-Dīn, bataillèrent ferme pour s'assurer la région de Damas en plus du nord de la Syrie, et se tournèrent ensuite naturellement vers le Caire. Mais le royaume latin de Jérusalem partageait les mêmes ambitions, ce qui entraîna de tragiques et épuisants affrontements entre les différents groupes, durant de longues années..

### Références

Bach Erik, *La cité de Gênes au XIIe siècle*, Gyldendal, Copenhague : 1955.

Elisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Gravelle Yves, *Le problème des prisonniers de guerre pendant les croisades orientales (1095-1192)*, Mémoire de maîtrise ès Arts en histoire, Université de Sherbrooke, Sherbrooke, 1999.

Prawer Joshua, *Histoire du Royaume latin de Jérusalem*, CNRS Éditions, Paris : 2007.

Les croisés du Poron
--------------------

### Lieu-dit du Poron, environs de Vézelay, après-midi du jeudi 6 juin 1146

Une brise parfumée parcourait les feuillages des saules et des bouleaux parsemant la rive de la Cure. Quelques moineaux jouaient à se poursuivre en piaillant, indifférents aux hommes occupés à tirer l'eau salée des sources proches pour en extraire le sel. Deux manouvriers exténués faisaient la sieste à l'ombre des arbres, ronflant de concert, les pieds à l'air après un bain revigorant. Non loin de là, d'un taillis plus serré fusaient quelques voix enfantines. L'une d'elles, plus forte et plus assurée se fit entendre une nouvelle fois par-dessus le clapotis :

« J'ai dents sans tête et queue sans cul. Qui suis-je ? »

La petite assemblée d'enfants groupée autour du tronc couché par-dessus la rivière regardait d'un air interrogateur Ernaut, l'auteur de la devinette.

Celui-ci arborait une coupe à l'écuelle récente, le cheveu châtain raide dessinant comme un casque sur son crâne. Son visage, assez massif, pétillait d'intelligence et ses yeux couleur azur scintillaient tandis qu'il évaluait son public. Bien qu'ayant passé l'âge de raison seulement depuis peu, il dépassait tous ses camarades d'une bonne tête, même ceux qui avaient deux à trois ans de plus que lui. Ses joues rebondies et sa cotte ventrue lui donnaient un aspect bonhomme, démenti par l'éclat sauvage qui habitait son regard. Le sourcil froncé, il toisait son petit monde l'air narquois, lançant de temps à autre un caillou dans le cours d'eau. Il attendit que quelques propositions fausses soient lancées avant de s'écrier, l'air triomphant :

« C'est un râteau, bande de nigauds ! »

Une fois encore, il triomphait. Satisfait de cette victoire, il descendit de son perchoir et proposa à ses compagnons d'aller s'aventurer vers les sources salées, où se trouveraient bien quelques valets dont ils pourraient se moquer. Un petit brun frisé à l'air renfrogné, peu emballé par la proposition, avança une autre idée :

« Et si on jouait plutôt à la dîme ? »

Agacé par cette remise en cause de son autorité, mais voyant là une occasion de la conforter, le jeune géant acquiesça de la tête.

« Si fait ! Je serai l'abbé et toi mon prieur. Et vous mes serfs, qui devrez aller me quérir de quoi payer. »

Traversant une clairière bordant les étendues où s'activaient quelques travailleurs sous le chaud soleil de printemps, il rejoignit leur repaire. Ce n'était qu'un roncier recelant un espace découvert, mais il devenait selon les jours monastère, château fort, voire navire en route pour le lointain.

Avec toujours à la tête de ce petit monde Ernaut, le fier-à-bras le plus imposant du groupe. Il choisit au passage un bâton et se tint de façon solennelle au centre, attendant ses serviteurs. Puis il distribua avec gravité des tâches diverses, depuis le ramassage de quelques feuilles jusqu'à la collecte de cailloux de couleurs variées. Tandis qu'il organisait les travaux, Raoul, son prieur désigné l'interpella :

« Ce serait quoi ton nom ? Pons comme le père abbé ? »

À l'évocation de ce maître peu apprécié de la population locale, Ernaut grimaça.

« Certes non ! Bernard, voilà joli nom de clerc. Comme le prud'homme qui vint à Pâques parler au roi et aux barons de la croisade outre les mers.

--- Je l'ai ouï dire. Le père dit que c'était bien étrange d'encontrer clerc parlant d'autres choses que de cens et de taxes... »

Voyant une nouvelle occasion de briller, Ernaut se rengorgea, l'air précieux :

« Et à moi il a parlé ! »

Raoul ouvrit des yeux émerveillés.

« Ça alors, quelle bonne fortune ! Que t'a-t-il dit ?

--- Eh bien...

--- Menteries que voilà, il est resté en l'abbaye à tout moment » l'interrompit un grand gaillard maigrelet à qui il manquait deux dents.

--- Et qu'en sais-tu, Droin-le-Groin ? rétorqua Ernaut, l'air rogue.

--- J'en sais que tu racontes que niaiseries, et puis cet idiot de Raoul gobe tout comme goujon crétin. »

Ernaut s'avança vers son détracteur, le visage haineux, le bâton en main. Il ne s'arrêta qu'une fois collé à lui et le provoqua d'un mouvement de menton.

« Tu as souhait de perdre d'autres dents, crapaud sans cul ? »

Puis il le poussa en arrière d'un geste brusque. Mais Droin s'était préparé à la rebuffade et résista, attrapant son adversaire à l'épaule. Bientôt ils roulèrent au sol, parmi les caillasses et la poussière, s'invectivant de tous les noms d'oiseaux qu'ils connaissaient, ahanant comme bucherons à l'abattage, encouragés chacun par quelques partisans.

Lorsqu'Ernaut rentra chez lui, il se dirigea droit dans la cuisine. Il avait tenté de rendre sa tenue plus présentable, mais ne se faisait guère d'espoir. Sa cotte était déchirée à l'épaule et une de ses joues était mal en point, avec une large griffure qui courait depuis son nez d'où l'on voyait que du sang avait coulé. La servante qui lui avait ouvert la porte avait levé silencieusement les bras au ciel et était partie bien vite, certainement chercher la maîtresse de maison.

Heureusement pour le garçon, ce fut son père qui le trouva le premier. La cinquantaine passée, Ermont de la Treille marchait avec difficulté, gêné par son embonpoint. Son visage ingrat, veiné de couperose, s'illumina à la vue de son enfant et il s'avança, un franc sourire aux lèvres. Il lui ébouriffa les cheveux affectueusement et s'esclaffa tandis qu'il s'asseyait à son tour sur le banc.

« Encore quelques comptes à régler avec de méchantes gens, petit lion ? »

Ernaut n'osa répondre, se contentant de renifler en silence. Il savait que la tempête n'allait pas tarder, même s'il se sentait plus confiant pour l'affronter désormais. Son père le fit asseoir à côté de lui et attira à eux un pichet et deux gobelets.

Ce fut à cet instant qu'entra dans la pièce Laurette, l'épouse du fils aîné, qui régnait sur la maison depuis son mariage. Les manches relevées, elle devait être occupée à quelque lessive dans l'arrière-cour de la demeure, avec des domestiques. Encadré de son voile de travail un peu passé, son visage était barré d'épais sourcils noirs, froncés comme bien souvent. Voyant son beau-père, elle marqua un temps, mais s'avança tout de même vers l'enfant, l'air franchement contrariée. Détaillant son aspect misérable, elle manifestait sa désapprobation par des mouvements de tête exaspérés.

« Ernaut ! Où as-tu été te rouler encore ? »

L'enfant demeura coi, baissant les yeux, espérant de prompts secours.

« Et bien ? J'attends ! Tu as vu ta cotte ? Crois-tu que les vêtements poussent sur les arbres, qu'il nous suffit de les cueillir ? »

Ernaut se rapprocha ostensiblement de son père.

« As-tu seulement idée du coût de tes bêtises ? Il va bientôt nous falloir te faire neuve cotte à chaque saison !

--- Paix ma bru ! Ce n'est qu'un enfant ! » répondit doucement le vieil homme, posant un bras protecteur autour de son fils.

La jeune femme pinça les lèvres.

« J'entends bien, père, mais il est désormais en âge de comprendre qu'il fait mal. Je suis fatiguée de devoir repriser sans cesse ses tenues, il use le linge si vite !

--- Ce n'est pas quelques aiguillées de fil qui nous mèneront à la ruine, voyons ! Avec tout ce que je vous ai légué, vous devriez bien trouver de quoi vêtir mon dernier né. »

Les épaules de la jeune femme s'affaissèrent. Elle inspira lentement, vexée par cette rebuffade puis quitta la pièce, le visage sombre. Le vieil homme fit un clin d'oeil à son enfant puis se leva du banc, en direction du cellier.

« Quelques morceaux de fromage nous feront oublier cette mégère, qu'en penses-tu, garçon ? »

### Abords du village de Saint-Pierre, matin du mardi 11 juin 1146

La bande s'était donné rendez-vous aux abords de la vigne de l'Alemant, où le père d'un des gamins était employé comme garde. En attendant les retardataires, Ernaut rongeait son frein, impatient d'en découdre. Non content de lui chercher querelle, Droin s'était mis en tête de constituer son propre groupe. Comble des problèmes, il avait revendiqué, et occupé, le carré de roncier qu'Ernaut considérait comme son fief. La guerre était inévitable, il ne fallait pas laisser une telle rébellion s'installer.

Raoul et les amis fidèles avaient donc retrouvé Ernaut au départ du sentier qui rejoignait la clairière des sources salées afin d'organiser un assaut. Pour l'heure, ils patientaient aux abords du chemin caillouteux menant à Saint-Pierre et de là à Vézelay, perché sur sa butte, à une demie lieue à peine. Un colporteur au visage ridé comme une pomme accompagné d'un petit âne lourdement chargé les dépassa, amusé de voir leurs préparatifs guerriers. Une poignée parmi les plus courageux s'était munie de bâtons, transformés en épées avec leurs gardes lacées d'un cordon de cuir. Les autres se contenteraient de lancer des cailloux de loin, voire de seulement invectiver leurs adversaires.

Équipé d'un morceau de charbon, Raoul s'appliquait à tracer une croix sur chaque épaule, afin de bien marquer le caractère sacré de leur entreprise. Ernaut avait insisté pour qu'ils arborent ce symbole, car il avait entendu le sermon qu'avait fait le fameux Bernard, le moine présent quelques semaines auparavant pour Pâques. Il en avait retenu que ceux qui se battaient avec un tel signe sur eux ne pouvaient connaître la défaite, que Dieu était de leur côté. Bien que confiant dans ses capacités, le jeune garçon jugeait plus prudent de s'assurer d'un allié de poids.

Tandis qu'il lorgnait au loin dans l'espoir que ses dernières recrues allaient enfin arriver, Ernaut vit une troupe de cavaliers armés qui avançaient au pas, descendant visiblement de Vézelay. Il les avait croisés plusieurs fois alors que leur chef s'entretenait avec son père. C'étaient des soldats du comte de Nevers, venus en mission. Ernaut n'avait guère compris de quoi il relevait, mais il avait entendu dire que cela amènerait certainement encore des problèmes avec l'abbé Pons, qui s'était toujours opposé au vieux Guillaume de Nevers[^28]. Ernaut s'en moquait, il admirait la morgue de ces hommes en haubert de mailles, l'épée au côté et le casque sur la tête. Lorsqu'ils laçaient la ventaille de leur coiffe, on ne voyait plus que leurs yeux intrépides, encadrant le nasal d'acier.

Les autres gamins vinrent regarder passer la petite troupe, bouche bée devant ces anges de la mort à l'attitude désinvolte. Le chef de l'escadron, monté sur un superbe étalon bai à l'allure anxieuse, reconnut Ernaut et lui fit un rapide geste amical. Il s'esclaffa, en désignant les enfants à ses hommes :

« Voyez donc compaings, que voilà féroce troupe prête à massacrer les Turcs ! »

Ernaut ne pouvait laisser croire qu'il ne s'agissait là que de déguisements. Il fit un pas et lança à la cantonade :

« Nous allons à l'assaut de quelques méchants garçons qui nous cherchent noise. Le Seigneur est avec nous, car nous sommes dans notre droit. »

Le chevalier arrêta sa monture et examina le garçon devant lui.

« Par la Barbedieu, que voilà marmot plus fier qu'un pet ! Qui a osé s'en prendre à toi et les tiens ?

--- Il a pour nom Droin, mais ne vaut guère plus qu'étron de loup. Une tête vérolée posée sur un tas pourri, voilà ce qu'il est. »

Le cavalier hocha la tête, un sourire aux lèvres.

« Tu parles de vrai, marmouset. Il en faudrait plus comme toi dans l'ost ! Rappelle-moi donc quel nom est tien, que je prenne garde à ne jamais croiser ta route qu'en ami. »

Indifférent à l'ironie, qu'il avait à peine perçue, Ernaut bomba le torse et leva le menton.

«On me nomme Ernaut, sire, Ernaut de Vézelay, fils d'Ermont de la Treille.

--- Et bien, Ernaut, fils d'Ermon, je suis aise de te connaître mieux. Si d'aventure dans les années qui viennent, tu as désir d'un peu d'action, demande donc après moi. Je suis Crestien de Breban, chevalier en l'ost le comte Guillaume de Nevers. Je suis assez souvent en votre cité de Vézelay... »

Puis le chevalier fit pivoter sa monture et salua de la main levée. Ernaut inclina la tête et regarda la troupe de soldats reprendre sa route d'un pas lent. La poussière du chemin était d'un blanc éclatant sous le soleil montant. Il se retourna vers ses hommes, aussi déterminé qu'un taureau d'arène. Les autres le dévisageaient, mi-effrayés, mi-admiratif.

« Il est temps de nous rendre au repaire, compaings. Qu'ils broutent la boue une fois pour toutes ! »

Puis il s'enfonça sur le sentier ombragé qui partait en direction des sources salées. Sentant qu'il fallait donner de l'entrain à ses hommes, il avait prévu de chanter une comptine qu'ils entendaient de temps à autre braillée par quelques soudards avinés ou impertinents. Ernaut entonna donc le début de la chanson, qui les avait fait tant rire la première fois qu'ils l'avaient déclamée aux moines de l'abbaye. Intrigués, quelques journaliers occupés à palisser les vignes levèrent la tête. Ils reprirent bien vite l'ouvrage, amusés par la petite troupe armée qui déclamaient d'une voix vaillante bien qu'aigrelette les couplets salaces :

> « J'ai outil bel et adroit,\
> Tantôt courbe tantôt droit.\
> Dieu qu'il est beau quand il s'étend,\
> Et ne vaut rien si ne se tend.\
> Je pousse aval, je tire amont,\
> Je touche au loin, et bien profond.\
> Fier-Archer voilà mon nom ! »

Sous le chaud soleil de juin, parmi l'herbe jaunie du chemin, les graviers crissaient sous les souliers. D'un pas martial, ils frappaient le sol en cadence, effrayant papillons, étourneaux et musaraignes. Braillant un hymne qui n'avait que peu à voir avec les psaumes, ils avançaient fièrement vers la bataille. Les combattants de la Foi étaient sur le sentier de guerre, bien déterminés à reprendre leur territoire sacré, le roncier du Poron. ❧

### Notes

Depuis quelques années, la conception traditionnelle de la condition des enfants au Moyen Âge a bien évolué. À travers les sources scripturaires et les découvertes archéologiques, les chercheurs nous brossent un portrait beaucoup moins noir qu'envisagé depuis les travaux de Philippe Ariès. Il existait un temps pour s'amuser, y compris dans les couches les plus populaires, et si certaines situations décrites (surtout dans les sources judiciaires) attestent de maltraitance ou d'exploitation, voire de viols, on ne peut négliger qu'il s'agisse là d'un biais de perception.

Les enfants étaient très nombreux, représentant une partie importante de la population à une époque où la mortalité infantile était dévastatrice. Malgré ces décès fréquents, l'attachement des parents était bien réel, et même les clercs, parmi les plus prompts à la punition, faisaient parfois montre de clémence envers ceux dont l'innocence était sans cesse vantée. La figure de saint Augustin, petit chenapan voleur de poires devenu Père de l'Église demeurait une référence incontournable.

Cette nouvelle est parue dans le numéro 41 (décembre 2011 -- janvier 2012) du magazine Histoire et images Médiévales. Des illustrations ont été réalisées à cette occasion par Callixte, qu'on peut retrouver sur [son site](http://www.callixte.com/index.php?2011/12/01/80-les-croises-du-poron).

Retrouvez la chanson du *Fier archer* en intégralité sur le site Hexagora.

### Références

Alexandre-Bidon, Danièle, Lett Didier, *Les enfants au Moyen Âge*, Paris : Hachette, 1997.

Alexandre-Bidon Danièle, Riché Pierre, *L'enfance au Moyen-âge*, Paris :Seuil, 1994.

De Bastard DʼEstang, Léon, « Recherches sur lʼinsurrection communale de Vézelay, au XIIe siècle », Bibliothèque de l'École des Chartes 12, no. 1 (1851): 339-365.

Fils de moine
-------------

### Bords de Loire, matinée du mardi 9 mai 1133

Sous un ciel invisible, revêtu d'un voile gris, les lambeaux de nuage semblaient piégés par les branches de saule. Au loin on entendait quelques sternes lançant des cris rauques, alarmées par un prédateur. Un héron passa silencieusement, frôlant l'eau de ses longues ailes. Menée d'un geste sûr par le passeur, la barge résonna doucement contre le ponton rudimentaire, quelques troncs assemblés en bord de plage. Prenant son temps pour se préparer, un des voyageurs descendit de l'embarcation avec précaution, guidant attentivement sa monture. Il était soulagé de n'avoir pas eu à traverser à gué, vu le haut niveau des eaux ce printemps. Le grand pont de bois était en pleine construction et il n'était pas possible de traverser autrement qu'en bac pour le moment.

Frissonnant sous sa chape de voyage, il frotta son nez rougi par le froid et l'humidité. Il n'avait décidé de quitter le palais de l'archevêque[^29] Vulgrin qu'à contrecœur, satisfait du confort douillet de l'endroit. Il soupira, le regard perdu dans le vague parmi l'épaisse brume du val. Une bourrasque de vent souleva sa capuche, révélant la fine tonsure marquant son état de clerc. En dehors de ce détail, il était difficile de le croire homme de Dieu. Élégamment vêtu d'étoffes de laine colorée, chaussé de délicats souliers guère adaptés à la marche, il tenait la bride d'un cheval de prix, dont le harnachement aurait fait pâlir d'envie de nombreux chevaliers. La voix du passeur le ramena à la réalité :

« Le bon jour, biau sire, puisse la charité des moines de Notre-Dame vous faire bon accueil. »

Sans lui répondre, le passager le salua d'un sourire. En quelques grandes enjambées, il avait rejoint le petit groupe qu'il avait côtoyé pour franchir le premier bras de Loire : deux hommes dirigeant un convoi de mules, au regard moins vif que celui de leurs animaux, un pâtre rigolard menant trois moutons, aidé d'un chien galeux et d'un garçonnet à l'air espiègle. Ils dépassèrent en silence les quelques maisons qui s'étaient installées sur l'île, modestes cabanes de pêcheurs ou de mariniers, avançant en direction du vaste bourg appuyé sur l'autre rive de la Loire, accolé à l'imposante prieurale Notre-Dame, fille aînée de Cluny.

Comme sur le premier bras du fleuve, un imposant pont de bois était en train d'être refait, mais le passage n'en était possible que pour de simples piétons, pas trop lourdement encombrés. Le long des piles, des pontons accueillaient des moulins, en partie achevés également. Le cavalier n'était guère rassuré par les flots écumants et ronflants qu'il découvrait. Le fin crachin virevoltant ajoutait à son humeur maussade. Les mules stoppèrent brutalement, apparemment peu convaincues par la perspective d'une nouvelle traversée. Les moutons se rallièrent à cet avis et n'avancèrent sur le bac que contraints par les morsures du chien et la crosse du berger. Le voyageur flatta sa monture sur l'encolure afin de l'apaiser et la fit progresser en lui parlant calmement. Une fois à bord, il s'employa à la tenir. Tout à son effort, il ne jeta que de rapides coups d'œil à l'édifice qui perçait la brume, vaste vaisseau de pierre déchirant le brouillard, à l'assaut du ciel. Le passeur chantonnait pour lui-même, indifférent aux tourbillons. Voyant le regard angoissé des voyageurs, il cracha dans l'eau, l'air goguenard, avant de leur crier :

« D'meurez sans peur, c'est quand l'eau manque qu'il faut craindre. Là, avec si bon courant, on ira pas s'écueiller en une souche, et pis on traverse en un rien de temps... »

De fait, l'embarcation arriva rapidement sur la berge orientale, sans dommage, au grand soulagement du voyageur. Descendant le premier, il entreprit de se diriger au plus vite vers l'entrée de l'hôtellerie du monastère. Traversant d'un bon pas les rives affairées, où les nombreux chalands déchargeaient ou installaient leurs cargaisons, il n'eut pas un regard pour les pêcheurs hissant depuis leurs fûtreaux[^30] leurs paniers de loches, d'aloses et d'anguilles.

Une fois en ville, il entendit distinctement les ouvriers s'activer sur la vaste église et n'avait qu'une hâte : admirer de ses yeux le nouveau chœur, les chapelles récentes, les dernières travées. On lui avait longtemps vanté l'édifice, parmi les plus grandioses que la chrétienté ait jamais réalisés. La prieurale Notre-Dame avait reçu toute l'attention du riche ordre clunisien, et les dons avaient afflué depuis un demi-siècle.

La façade occidentale, en plein travaux, était parsemée d'échafaudages improbables, de cordages pendants et de poulies grinçantes. Quelques portefaix grimpaient sur des échelles, courbés sous le poids du mortier ou des mœllons. La place était bordée des loges des sculpteurs et des tailleurs de pierre ainsi que d'une forge où tintait le marteau sur l'enclume.

Sous la direction du vieux prieur Eudes[^31], récemment décédé, l'endroit avait connu la prospérité, à l'image du puissant ordre auquel il appartenait. L'entrée du monastère se trouvait sur la gauche, les battants grand ouverts. Un convers[^32], à la bure fatiguée et dépenaillée, avisa le bel aspect du nouvel arrivant et partit en courant prévenir le frère hôtelier de l'arrivée d'un visiteur de marque.

Les départs terminés, la cour était calme. Seuls quelques moines s'affairaient autour du cellier, entreposant à l'abri les denrées collectées à Pâques. L'hôtelier, un petit homme au visage fripé, s'approcha rapidement, ordonnant d'un geste de s'occuper du cheval. Il était accompagné d'un jeune moine à la face rubiconde, porteur d'une aiguière et d'une serviette de lin. Ils s'inclinèrent devant le nouvel arrivant, qui les salua à son tour en ôtant sa capuche, révélant ainsi son état et se présentant rapidement. L'hôtelier esquissa un rapide sourire, dévoilant de trop rares dents tandis que le voyageur se lavait rapidement les mains avant de les sécher.

« Grand honneur de vous avoir en nos murs, frère. Escomptez-vous rencontrer le père prieur Ymarus ?

--- Selon son désir. Pour ma part, je n'ai nulle tâche auprès de lui, je suis porteur de courriers à destination de Paris. »

Le vieil homme hocha la tête, prenant un air grave.

« Avec tous ces travaux, il est fort occupé. Je lui ferai savoir que vous êtes présent, et il avisera. »

Le messager hocha la tête en assentiment.

« Pourriez-vous me faire savoir le chemin pour se rendre en la grande prieurale ? J'aurai grande joie à me recueillir sur vos autels... »

Souriant de toutes ses gencives, le moine indiqua son jeune compagnon, qui ne les avait pas lâchés d'une semelle tandis qu'ils s'approchaient du bâtiment réservé aux hôtes de marque.

« Humbaud est là pour vous servir, il vous indiquera le chemin, aisé à trouver par ailleurs. Vous resterez pour un long temps ?

--- Certes pas plus d'une poignée de jours. Il me faut juste trouver une nave pour descendre la Loire jusqu'à Orléans. J'ai quelques missives pour Fleury en chemin.

--- Et pour quel sire vous m'avez dit porter messages ?

--- Je ne l'ai pas dit, rétorqua le mystérieux clerc avec un grand sourire.

### Prieurale Notre-Dame de la Charité-sur-Loire, midi du jeudi 11 mai 1133

Le soleil cru frappait durement la pierre blanche de l'édifice, obligeant à plisser les yeux pour en admirer l'ordonnancement des volumes. Sur les toits, les couvreurs travaillaient à la protection du nouveau chœur. En tant que clerc, le voyageur avait été autorisé à se rendre dans le cloître, tant qu'il ne perturbait pas les frères.

Néanmoins, avec le chahut des jeunes novices à qui l'on faisait classe, il aurait été difficile de se faire remarquer à moins d'y hurler comme un dément. Il passait par là pour se rendre dans l'église, par le bras septentrional du transept. En y entrant, il ne pouvait s'empêcher de s'émerveiller devant le magnifique ordonnancement de l'endroit. Bien que le chœur en fût de nouveau en travaux, augmenté en raison de l'affluence des pèlerins, il s'en dégageait une telle paix qu'il aurait apprécié de pouvoir s'y retirer. Seulement, son devoir l'appelait ailleurs, et il n'était pas encore prêt à renoncer au siècle. Peut-être après quelques années encore, lorsque les horreurs de ses semblables auraient fini de briser l'espoir en lui.

Les maçons et les sculpteurs avaient laissé la place aux peintres et aux charpentiers, et quelques frères surveillaient d'un œil expert la réalisation des décors figurés, selon l'enseignement qu'ils avaient reçu. Se faufilant parmi les groupes de pèlerins aux tenues fatiguées, le messager fit le tour du déambulatoire en flânant, prenant le temps de se recueillir à chaque chapelle. Puis il se rendit par une porte latérale dans le passage qui menait vers l'est, au-delà du chevet et de la vieille église Saint-Laurent dont il ne restait plus guère que le chœur et un tronçon de nef. Il avait envie de rejoindre les jardins du prieuré, de se délasser un peu à l'écart, au calme.

Comme dans tous les monastères bénédictins, les jardins étaient très soignés, les vergers faisaient de nombreux envieux, et les fleurs cultivées servaient à embellir l'édifice. Le visiteur se pencha, intrigué par la taille d'un parterre de lys. Il sourit pour lui-même : logique, dans une prieurale dédiée à la Vierge. Tandis qu'il se relevait, il lui sembla entendre un miaulement dans un buisson adjacent. Intrigué, il s'approcha, curieux de voir le matou qui se cachait par là. Il ne vit qu'un panier, oublié par quelque jardinier distrait. Le miaulement reprit, différemment cette fois. Inquiet de ce qu'il croyait reconnaître, il s'approcha et souleva le couvercle d'osier, révélant un bébé, âgé de quelques mois tout au plus, bien emmitouflé dans un vieux drap de laine. Il gazouillait, à demi endormi. Le clerc se releva brusquement, cherchant la mère aux alentours. Sûrement une chapardeuse, vu que les femmes n'avaient rien à faire dans l'enceinte du monastère.

Un peu éberlué, il chercha parmi les bosquets, derrière les claies, voir si personne ne se dissimulait, attendant son départ. Il appela plusieurs fois, en vain. Puis il se décida : il attrapa délicatement le panier et prit la direction de l'hôtellerie.

### Cellier du prieuré Notre-Dame de la Charité-sur-Loire, après-midi du jeudi 11 mai 1133

Le couffin improvisé trônait sur la table du cellier, autour de laquelle s'interrogeaient plusieurs officiers du monastère : l'hôtelier, le cellérier, le maître des novices, en plus du messager. Tous se demandaient d'où venait cet enfant et, surtout, ce qu'il convenait d'en faire. Après un long silence, le maître des novices osa la première remarque, de sa voix chantante de méridional :

« Il me paraît fort jeune, il ne saurait se passer du lait maternel... »

Le cellérier, un vieux barbu au nez couperosé, hocha la tête en assentiment. Attendri, il sourit au bébé avant de reprendre contenance.

« Il faudra dire au prêtre de la paroisse de faire annonce au prochain dimanche, si ses parents le cherchent. »

L'hôtelier oscilla de la tête, désapprobateur.

« Je ne vois guère comment on pourrait l'égarer en notre courtil[^33]. Il leur a fallu grimper outre le mur de clôture. Non, je suis assuré que cet enfançon ne peut compter que sur nous, les siens n'ont pu le garder pour un motif qui sera jugé par le Seigneur. »

Le cellérier souleva le tissu qui enveloppait l'enfant, déclenchant une gesticulation qui le fit bien vite reculer, non sans avoir vérifié ce qu'il voulait voir.

« Au moins c'est un garçonnet, sa présence n'est donc pas déplacée. »

L'hôtelier haussa un sourcil narquois.

« Et alors ? Le voilà bien trop jeunot pour demeurer en la clôture...

--- Nous pouvons toujours le confier à une vilaine de nos terres, le temps pour lui de se passer de lait » rétorqua le maître des novices en souriant à l'enfant.

Le voyageur renifla pour attirer l'attention sur lui et adopta un air surpris :

« Vous ne comptez donc pas chercher sa famille ? »

L'air désolé, le cellérier hocha la tête.

« Nous ferons annonce, comme il se doit, sans trop en espérer ni perdre notre temps en vaines chasses . M'est avis qu'elle espérait que nous le prendrions en garde.

--- Si fait, ajouta le maître des novices. Ce n'est là ni premier ni dernier. Nous avons l'usage des oblats[^34], plusieurs parmi nous le sommes d'ailleurs. »

L'enfant portait son regard alternativement vers les uns et les autres, babillant d'un air gracieux comme s'il trouvait normal de se trouver entouré de vieux messieurs habillés de noir, dans une cave mal éclairée. Le messager lui sourit à son tour, s'attirant quelques gazouillis aimables. Le maître des novices se frotta le visage avant de déclarer :

« Je vais voir comment démêler cet écheveau prestement. Je vais quérir permission du père prieur pour sortir confier l'enfançon à une famille ou l'autre.

--- Demandez aux visiteurs usuels de l'hôtellerie, ils sauront bien vous donner utile conseil en ce sens. »

L'affaire était donc entendue, et chacun s'apprêtait à repartir à ses devoirs lorsque le messager s'exclama :

« Attendez, nous ne savons même pas si cet enfant a reçu baptême chrétien, ni quel nom est sien...

--- De vrai, acquiesça l'hôtelier. Il faudrait remédier à cela au plus vite, non ? »

Le maître des novices sourit avec candeur.

« Cela sera facile, nous pourrons le porter au bassin ce dimanche, pour la Pentecôte. Plusieurs petits n'ont pu être baptisés pour les Pâques. Un de plus ou de moins...

--- Il lui faudra des parents dans la Foi pour le tenir au bassin, riposta le cellérier.

--- Nous n'aurons qu'à demander à la nourrice. Elle sera de sûr bien aise de rendre ce service de rien au prieuré. »

L'étranger était resté silencieux tandis que les trois moines traçaient la destinée de l'enfant. D'un doigt, il chatouillait la main qui cherchait à l'agripper. Sans bien réfléchir, il répondit :

« Je serais heureux de le tenir pour son entrée parmi les fidèles.

--- Voilà magnifique idée ! s'exclama le maître des novices, dans un jet de postillons. Nous le nommerons donc... Mais quel est votre nom, mon frère ?

--- On me nomme Herbelot.

--- Eh bien, il en sera de même pour lui, en mémoire de son parrain. La chose est dite ! Grande chance pour lui que vous l'ayez découvert. Un homme de votre rang pour le mener en la maison du Seigneur... »

Le jeune clerc sourit à l'enfant babillant, indifférent au destin qui se scellait pour lui, dans cette cave humide aux relents de moisi. Peut-être que la chance n'y était pour rien, que c'était la main de Dieu qui l'avait guidé là. Dans un nouvel élan d'humanité, il ajouta :

« J'aimerais qu'il soit confié au prieuré comme novice dès qu'il sera en âge, je ferai donation en ce sens, qu'il ne soit une charge pour la communauté.

--- *Deo gratias*[^35] s'exclama le frère cellérier, rasséréné par l'idée d'un don à son établissement. ❧

### Notes

Les abandons d'enfants n'étaient pas si fréquents que cela, et se faisaient généralement aux abords d'édifices religieux. Les parents désespérés faisaient confiance à la charité de ces endroits pour garantir à leurs enfants une vie qu'ils ne pouvaient leur assurer. Par ailleurs, l'intégration de jeunes garçons au sein des communautés monastiques était suffisamment importante pour qu'on s'en inquiète. Des recommandations furent donc données par les autorités cléricales pour que cela ne soit plus irréversible. Il demeurait important de s'assurer que la vie monastique était un choix véritable, fait par un adulte, qui devait donc répéter l'engagement pris pour lui quand il était enfant..

Par ailleurs, le baptême, qui marquait l'entrée dans la communauté des Chrétiens était donnée de plus en plus tôt, de façon à s'assurer du salut des enfants morts en bas âge. La mortalité infantile était telle que les parents étaient généralement pressés de voir leur progéniture sous la protection divine. Cela se faisait habituellement avant le troisième anniversaire, lors d'une cérémonie d'importance (généralement Pâques). Cela pouvait se doubler d'une confirmation, vers les sept ans, qui indiquait la reconnaissance et l'acceptation par l'enfant de tous ses devoirs de chrétien. Il commençait en outre à communier vers cet âge là.

### Références

Alexandre-Bidon, Danièle, Lett Didier, *Les enfants au Moyen Âge*, Paris : Hachette, 1997.

Alexandre-Bidon Danièle, Riché Pierre, *L'enfance au Moyen-âge*, Paris :Seuil, 1994.

Anonyme, Collectif (éd.), *Histoire Cronologique du Prieuré de La Charité sur Loire*, La Charité-sur-Loire : Les Amis de la Charité-sur-Loire, 1991.

Traîtres sentiers
-----------------

### Abords de Laodicée, plateaux anatoliens, nuit du samedi 3 janvier 1148

Appuyé contre un tronc nu, l'homme regardait la vapeur s'échappant de sa bouche monter vers le firmament. Les étoiles scintillaient, indifférentes. La lune n'était qu'à quelques jours de son plein et éclairait d'une lumière froide le camp et les murailles proches de Laodicée. Vers le sud, un peu à l'est, une ombre gigantesque dévorait le ciel : le mont Cadmus. Un souffle d'air glacé fit frissonner Ganelon qui baissa la tête vers le maigre feu qu'il partageait avec ses trois compagnons : Alerot le second valet, son ami Aimon l'Englois, un archer qui les assistait et les protégeait et surtout Régnier d'Eaucourt, son seigneur et maître, chevalier de l'ost du roi Louis.

Pour l'heure, il était le seul éveillé, responsable du maintien des flammes. Au moins ils arrivaient à se réchauffer, même si le bois vert et mouillé rabattait sur leurs visages une fumée suffocante chaque fois qu'ils s'en approchaient. Tous les foyers alentour produisaient une semblable colonne, éclairée d'une lumière orangée à sa base. Le camp était calme, les discussions se faisaient à voix basse, et même les bêtes, les rares montures et animaux de bât qui avaient survécu jusque-là, ne bougeaient guère. Les quelques prédateurs qu'ils avaient découverts ici, chacals dorés, hyènes rayées, ne s'aventuraient pas aussi près d'une cité comme Laodicée.

Les patrouilles craignaient bien plus les féroces Turcs qui les assaillaient à tous moments, décochant leurs flèches avant de galoper à l'abri. Ganelon tira sur ses épaules la couverture dont il s'était enveloppé, grelottant malgré ses pieds près du feu. Il aurait bien avalé un peu d'eau, histoire d'oublier sa faim et de se remplir l'estomac, mais il n'avait guère envie de bouger dans le froid. Loin de la côte, le ravitaillement faisait défaut. En fait, il semblait au valet qu'il n'avait rien mangé de correct depuis qu'ils avaient quitté sa Picardie natale. Lui qu'on appelait volontiers le Gros n'était plus que l'ombre de lui-même. Il s'était résigné à percer de nombreux trous à sa ceinture au fur et à mesure de leur avancée.

Depuis qu'ils étaient dans les territoires byzantins, la situation était devenue effroyablement difficile. Les paysans qu'ils croisaient, ceux qui n'avaient pas eu le temps de fuir, ne lui paraissaient guère différer des guerriers qui s'opposaient à leur marche. Quant aux soldats qui tenaient les villes, leur comportement était souvent agressif, hostile. Alerot disait que c'était la faute aux troupes allemandes, qui les avaient devancés sur la route. Ganelon pensait que c'était plutôt parce que ces maudits Grecs avaient partie liée avec les sarrasins. Il avait eu un moment d'espoir lorsque la rumeur avait couru que le roi allait rejoindre l'empereur de Sicile et qu'à eux deux ils prendraient la trop fière cité de Constantinople. L'évêque de Langres soutenait l'idée qu'il suffisait d'attendre à Adrianople les navires siciliens.

Ganelon avait vite déchanté quand les voiles aperçues furent celles des Byzantins chargés de transporter rapidement les unités françaises sur la côte anatolienne. Ces maudits orientaux ! Leur fourberie était désormais légendaire, et les hommes de la troupe se racontaient toutes les occasions où ils y étaient confrontés. Ils étaient supposés être des frères chrétiens, mais se comportaient comme des païens infidèles. Ils cachaient les vivres, refusaient l'entrée de leurs cités, se réfugiaient dans les montagnes à leur approche. On disait que si l'armée allemande avait été perdue, c'était parce que leurs guides l'avaient menée dans des chemins impossibles, puis qu'ils s'étaient enfuis afin de prévenir l'ennemi.

Le craquement d'une brindille dans le silence givré le fit se retourner. Leur mule, Bone-Amie, semblait intriguée par ce qu'elle voyait à ses pieds. Elle se mit à frapper du sabot et à gratter tout en tirant sur sa longe, perturbant le grand destrier du chevalier. Ganelon se leva péniblement et avança vers elle, sortant la main dans la froidure pour lui flatter l'encolure.

« Là, ma belle, demeure calme, il ne s'agit pas d'éveiller les nôtres compères. Nous avons fort parcours à suivre demain. »

Bone-Amie vint se frotter affectueusement, avec toute la délicatesse d'une bête de bât de plusieurs centaines de kilos, manquant de faire tomber le valet. Ganelon eut un sourire fugace. Il aimait les animaux, car il n'était jamais déçu par eux. Son maître avait acheté la mule pour un prix indécent, lorsqu'ils avaient débarqué du navire, mais Ganelon et Alerot s'étaient félicités de leur bonne étoile. Une telle assistance était inestimable, vu le long périple qu'ils devaient accomplir. Ils avaient vite baptisé à leur goût la courageuse bête, n'ayant pu comprendre le baragouinage du vendeur. Et tous les deux passaient une grande partie de leur temps à chercher de quoi nourrir le groupe.

Car si les hommes manquaient de vivre, les animaux en étaient réduits à dévorer des arbrisseaux gelés, des lichens et des mousses, le moindre brin d'herbe qui pointait sur le bord du chemin. L'hiver, la traversée préalable des forces allemandes et la traîtrise des Grecs avaient fait disparaître tout le fourrage. Le roi avait réuni le conseil en soirée, et ils devaient partir le lendemain en quête des habitants de la ville, réfugiés dans les montagnes, de façon à pouvoir obtenir d'eux les vivres qui leur permettraient de rejoindre Adalia.

Ganelon soupira. Il commençait à trouver le temps long. Plus de six mois avaient passé depuis qu'il avait assisté, au sein d'une immense foule, à la prédication du grand abbé Bernard de Clairvaux devant le magnifique sanctuaire de Saint-Denis, aux portes de Paris. D'où il était, il n'avait suivi le discours que grâce aux autres spectateurs, qui se répétaient de loin en loin les paroles du saint homme. Le moine était déjà légendaire, même pour un simple valet picard comme Ganelon, mais il ne lui était apparu que comme une vague silhouette blanche, perdue parmi les riches habits des dignitaires de l'Église. À leurs côtés, devant les trois énormes portails colorés, se tenaient d'imposantes figures aux vêtements bigarrés, grands barons, comtes et le roi Louis en personne.

Ganelon avait été gagné par la ferveur religieuse et s'était senti investi d'une mission divine. Le départ avait empli son coeur d'allégresse, même s'il ne faisait que suivre son maître. Il lui semblait avoir sa part à jouer dans les événements à venir. Des centaines et des centaines de lieues plus tard, il en était là, après avoir escaladé des montagnes, franchi des mers et survécu à des batailles. Dans le froid, sous la lune au milieu d'une foule endormie. Avec une mule pour seule compagnie.

### Mont Cadmus, jeudi 8 janvier 1148

Le froid n'était pas suffisant pour geler le sol et l'étroit sentier n'était que boue et pierres amoncelées. Les marcheurs hésitaient, claudiquant d'un pas à l'autre. Sabots et chaussures dérapaient pareillement. Les plus chanceux trébuchaient à peine, les malheureux glissaient jusqu'à un précipice, qui avalait gens et bêtes sans distinction. Les rares zones plus faciles recelaient des bandes de pillards turcs, embusqués derrière les rochers, qui tiraient leurs traits assassins sur tout ce qui s'aventurait à leur portée. Perdu dans les nuages accrochés au relief accidenté de ce haut passage, le convoi avançait tellement lentement que chacun finissait par croire qu'il attendait plus qu'il ne marchait. Ganelon et Alerot s'aidaient de bâtons et progressaient avec quelques autres, protégés par une douzaine d'archers et d'hommes équipés de boucliers. Enviés au départ, ces derniers haletaient plus fort que leurs compagnons de devoir soulever régulièrement leurs larges pavois. Mais les nombreux tronçons de fûts et fers plantés dans le bois témoignaient de l'importance de leur bagage.

Une clameur monta de l'arrière. Rapidement, les écus se placèrent en ligne avec les tireurs derrière. Parmi eux, Aimon soufflait sur ses doigts gelés, rendus douloureux par les gerçures. Les flèches étaient encochées, les regards inquiets attendaient de voir apparaître un éventuel ennemi émerger des brumes cotonneuses. Le vacarme se fit plus fort et un frémissement fut perceptible dans la colonne en contrebas : des cris de bataille, des hennissements, le son des corps s'écroulant, de la foule prise de panique, des rochers traîtres qui roulaient. Puis jaillirent plusieurs cavaliers sur leurs petits chevaux nerveux, leurs tresses volant dans le vent. Ils portaient le chapeau à bord de fourrure que Ganelon n'avait que trop vu depuis quelque temps. Taillant de leurs sabres ou décochant des flèches rapides de leurs arcs courbes, il forçaient le passage plus qu'ils ne cherchaient à tuer. Il sembla à Ganelon qu'ils fuyaient ou du moins tentaient de s'éloigner. Il ne put retenir un cri :

« Dieu le Veut ! Hardi compaings, frappez-les sans crainte ! Percez-les de vos fers ! »

Quelques archers décochèrent, mais de nombreux croisés se trouvaient autour des Turcs. Ceux-ci approchaient tant bien que mal, talonnant leurs montures sans ménagement. Ils n'étaient plus qu'à une trentaine de pas lorsque Ganelon comprit pourquoi ils gravissaient le chemin : des cavaliers francs les poursuivaient de près, frappant de la lance les retardataires, piétinant les corps de ceux qui avaient glissé de leur selle. Régnier d'Eaucourt était avec l'avant-garde, sous le commandement de l'oncle du roi, mais il aurait aimé sans nul doute voir un tel spectacle. Cela redonna du courage à la petite troupe. Ils se sentaient prêts à affronter l'ennemi.

Le premier turc qui s'approcha reçut un trait dans le bras, ce qui ne l'empêcha pas de continuer sa route et de rejoindre une sente abrupte qui montait droit vers le sommet. Plusieurs cavaliers le suivirent de près, dont certains avaient encore des flèches en main. Profitant de ce qu'on faisait moins attention à eux, ils tirèrent quelques volées rapides avant de s'évanouir dans la végétation à flanc de montagne, projetant des gerbes de pierraille tandis qu'ils avançaient. Instinctivement, Ganelon avait plongé derrière Bone-Amie, et se sentit honteux lorsqu'il se releva et aperçut un empennage dépasser des bâts.

Il enfonça la main pour voir si la blessure était importante, mais découvrit que le fer était planté dans un sac. Il se mit à rire et se retourna vers Alerot, hilare. Son compagnon ne partagea jamais son moment d'euphorie, il était penché au-dessus d'Aimon, frappé d'une flèche à la poitrine, crachant du sang. Il avait perdu connaissance, peut-être en s'écroulant au sol. Ganelon voulut leur porter secours, mais les cavaliers en grand haubert de mailles, porteurs de la croix sur l'épaule, s'approchèrent, hurlant :

« Avant ! Avant ! Ne faites pas halte ! Il faut passer outre le col au plus vite ! »

Ganelon secoua la tête et montra son compagnon blessé :

« Il nous faut panser notre compaing, il vient de recevoir navrure parmi le corps, il crache du sang... »

Un des chevaliers avança sa monture et hurla d'un air contrarié :

« Mordiable ! Vas-tu obéir, foutre vérolé ! D'autres attendent en aval, et tu occupes la sente !

--- Et comment je fais ? Je ne peux l'abandonner ici...

--- Porte-le sur ta mule ou tes épaules, je m'en gabe, avance ou je m'arrange pour que tu lui tiennes compagnie, en pareil état ! »

Seul le regard inflexible de l'homme était visible, recouvert qu'il était d'acier des pieds à la tête, sa ventaille rabattue sur le bas du visage et un casque à nasal sur la tête. Il avait en main une lame rouge de sang à peine séché, brandie d'un air menaçant. Sa monture elle-même était nerveuse, remuant en tout sens, les yeux affolés. Résigné, le petit groupe se mit en branle, Ganelon et un autre soldat se penchèrent pour prendre Aimon avec eux, mais celui-ci éructa des bulles écarlates, manquant de s'étouffer. Alerot, qui tenait la longe de Bone-Amie intervint :

« Il ne faut certes pas le mouvoir ainsi, il y a grand risque de l'occire pour de bon...

--- Il faudrait au moins arracher fer et fût, non ?

--- Comment donc ? Je ne m'y entends guère en chirurgie !

--- Et moi donc ? Nous ne pouvons tout de même pas l'abandonner ici... »

Tandis qu'ils discutaient, la colonne avait repris son avance et ils gênaient le passage, s'attirant des regards désapprobateurs des marcheurs épuisés et inquiets. Alerot commençait à progresser, entrainé par Bone-Amie qui suivait la file.

« Il me faut continuer, je ne peux barrer le passage, essayez de le porter à deux. »

Ganelon lança un regard désolé au soldat resté pour l'aider et ils tentèrent de soulever une nouvelle fois Aimon. Le flot de sang fut tel qu'ils le lâchèrent presque brutalement sur le sol. Ganelon se frottait le visage, comme s'il essayait de fuir un mauvais rêve. Son compagnon le dévisagea, d'un air contrit.

« Il n'y a rien qu'on puisse faire, compère. Il n'y a guère d'espoir pour lui. Sans un habile chirurgien, il passera d'ici peu. Rejoignons les autres avant qu'ils ne soient trop loin. »

Ganelon lança à l'homme un regard empli de haine et de colère.

« Tu veux qu'on l'abandonne, comme un chien crevé dans une fosse ?

--- Peut-être une bonne âme saura le soigner parmi nos suivants. Pas nous. Et si nous restons là, nous risquons nos vies aussi. »

Ganelon hocha la tête, consterné par l'horrible évidence.

« Aide-moi au moins à l'installer au mieux sur le côté. Enroulons-le dans une couverture. »

Lorsqu'ils reprirent leur avancée, d'un pas aussi rapide qu'ils le pouvaient, pour rejoindre Alerot, Ganelon sentait les larmes lui embrouiller les yeux et s'écouler sur ses joues. Il ruminait en son for intérieur, déchiré de n'avoir pu rien faire. Tandis qu'il posait un pied puis l'autre, comme un somnambule, il passa auprès du corps mutilé d'un guerrier turc tombé. Quelques croisés se dépêchaient de le dépouiller de ses affaires de leurs mains fiévreuses. Rageur, Ganelon asséna en passant un coup de pied au cadavre à demi nu :

« Honnis soient ces Orientaux et leur pays maudit » ❧

### Notes

Le passage des reliefs anatoliens constitua une des épreuves les plus terribles pour les contingents franco-allemands de la Seconde croisade en 1147-1148. Arrivées les premières, les forces de Conrad furent d'ailleurs décimées et il dut faire demi-tour, physiquement très affecté. Le roi Louis de France lui-même n'en réchappa que par ses capacités de guerrier et grâce au fait que ses assaillants ne l'avaient certainement pas reconnu.

Le passage aux alentours du Mont Cadmus constitua l'un des moments parmi les plus difficiles pour les hommes, le train de bagages s'étant retrouvé assez mal protégé en raison de soucis de commandement à l'avant. Malgré cela, la vaillance de l'arrière-garde et du roi permirent de retourner la situation et l'armée française réussit à passer le col et à se rassembler sur le versant opposé.

### Références

Eudes de Deuil, *La croisade de Louis VII, roi de France*, publiée par Henri Waqet, Paris:Geuthner, 1949. Une version électronique de la version de Guizot (1824) existe à l'adresse : <http://remacle.org/bloodwolf/historiens/odondedeuil/table.htm> (consultée le 8 juillet 2011).

Nicolle David, *The Second Crusade 1148: Disaster Outside Damascus*, Londres : Osprey Publishing, 2009.

Nicolle David, Kervran Yann (trad., adapt.), *La Seconde croisade et le siège de Damas en 1148 - Un pari manqué*, à paraître.

Le meilleur des fils
--------------------

### Césarée, mercredi 3 juillet 1140, après-midi

Le soleil jouait au travers des branches du palmier, s'employait à gâcher la sieste de Sawirus. Allongé entre le tronc et un muret de pierre sèche, le marchand tentait d'oublier un peu la chaleur de la journée. Sa petite taille lui avait permis de se ménager un endroit agréable où se délasser, installé sur une couverture utilisée habituellement pour protéger le dos de sa mule Rayya du frottement des paniers. Mais pour l'heure, même elle, son infatigable servante, réfugiée à l'ombre, avait la tête basse le regard absent. Attrapant un de ses rafrafs[^36], Sawirus tenta de s'en faire un masque sur les yeux. Ce fut alors au tour des mouches de venir bourdonner à ses oreilles, de se poser sur ses mains et ses narines.

Agacé par ce harcèlement, il s'assit, se frottant le visage comme s'il sortait d'un long sommeil. Une colonie de puces semblait s'être établie dans sa barbe, qu'il gratta avec vigueur. Une brise marine apportait quelques senteurs iodées, rafraichissant un peu l'atmosphère emplie de poussière. Il renifla et soupira silencieusement, les yeux à demi-clos, avisant d'un regard son potager. Son valet, Mahran, était monté sur le muret qui séparait l'enclos des terres voisines, protégeant les cultures des animaux et des maraudeurs. S'abritant du soleil d'une main, il était tourné vers le sud, scrutant par-delà les jardins.

« Vois-tu souci au loin ? »

Tout joyeux, l'homme se retourna vivement, les yeux rieurs :

« Force animaux bâtés, cavaliers en selle et pieds poudreux. Une belle caravane est aux abords de la porte depuis Nazareth. »

Sawirus se leva, époussetant son thwab[^37] de fin coton rayé. Jouant des pieds et des mains, il rejoignit son domestique et admira à son tour l'importante procession qui soulevait un nuage de poussière tel que les murailles et la porte semblaient sortir d'une brume dorée. Sawirus ne put s'empêcher de sourire.

« Nous avons là surtout des marchands à ce que j'en vois, fors quelques hommes du Christ à la croix écarlate, mais ils ne sont guère. Une simple escorte je dirais.

--- Si fait. Ils protègent la fin de convoi. Certainement des marcheurs de la Foi de retour de la ville sainte.

--- Mais les chameaux que je vois, ainsi que les mules, semblent bien chargés de vivres. Il se trouve là des négociants. Aide-moi à bâter Rayya puis empresse-toi de quérir nouvelles. Tu me retrouveras à la maison. »

Excité à l'idée des transactions qui allaient s'offrir à lui, Sawirus se hâta vers les paniers de bât, emplis de lentilles fraîchement récoltées, de bottes d'oignons et des derniers pois chiches de la saison, d'anémones et d'oeillets coupés avec soin pour décorer sa demeure. Son valet sangla rapidement et s'en fut, trottinant d'un pas lourd sur le chemin caillouteux. D'autres jardiniers curieux l'accompagnaient en direction de la grande porte orientale. Sawirus préféra éviter la cohue qui devait régner à cette entrée et choisit prudemment de faire le tour par la partie nord de la cité, guère éloignée.

Menant sa mule d'une main nonchalante, il rejoignit la muraille de pierres couleur sable, puis la longea à main gauche avant de bifurquer en direction du littoral. Tandis qu'il avançait sur le sentier au nord des fortifications, il laissait son regard errer au loin, sur la mer. Quelques esquifs, modestes embarcations de pêcheurs, ondulaient sur les flots, par-delà les dangereux rochers aux abords de la côte. Une voile de belle taille coupait l'horizon tel un canif pointé vers les cieux. Un navire de commerce, ou de guerre, qui faisait route vers le nord, peut-être vers Acre, Beyrouth ou plus loin encore.

Sawirus était heureux de ne plus avoir à voyager. Il avait désormais un garçon en âge de se charger de cette corvée, et lui se contentait de gérer ses affaires depuis sa maison. Il sourit à la pensée de sa femme et de sa fille qui l'attendaient, du repas qu'ils prendraient ensemble à la tombée de la nuit. Un peu en retrait du passage, sous les palmiers, un large groupe d'enfants jouait avec quelques chiens à l'allure famélique. Il leur sourit aussi, sans même y réfléchir, heureux de son sort et de la bonne fortune qui avait favorisé sa demeure jusqu'alors.

À l'entrée de la ville se trouvait seulement un homme de faction, qui accomplissait sa tâche avec un zèle rare, affalé dans un escalier accolé à la tour, le regard perdu dans le vague. Reconnaissant Sawirus, il le salua à peine d'un mouvement de menton, puis se replongea dans l'examen attentif de l'intérieur d'une savate usée. Pour ne pas faire de détour supplémentaire, le vieil homme décida de couper par le marché, une zone encombrée de sacs et de paniers, d'étals qui empiétaient sur la rue déjà fort étroite. Mais cela permettait de cheminer à l'ombre, car tout un fatras de canisses et de treillages avait été lancé d'une façade à l'autre, dans un désordre sans nom. Bien trop souvent, un simple oiseau en faisait s'écrouler de larges portions sur les badauds et les marchands situés en dessous, provoquant un nuage de poussière, et plus encore de cris indignés et énervés. Mais les roseaux et les fines lattes retrouvaient vite leur place jusqu'à leur affaissement suivant. Les artisans s'affairaient souvent devant leurs clients et exhibaient leur production à l'entrée de leur échoppe.

Le passage préféré de Sawirus était celui où de petits éléments de mobilier en bois tourné étaient réalisés. Une senteur délicate, mélange de toutes les essences creusées, encore vertes, embaumait l'air. Alors qu'il s'engageait dans la venelle menant à sa maison, il salua de la main quelques amis. Installés devant une petite table sur un perron auquel on accédait par une volée de marches, ils commentaient l'activité avec indolence. Il stoppa Rayya et souleva plusieurs fois le heurtoir de la grande porte arrière, par laquelle il faisait toujours entrer la mule. Ce fut une servante qui lui ouvrit, non sans avoir au préalable vérifié par le guichet l'identité du visiteur. Sans attendre, il lui donna la longe et lui demanda de décharger l'animal, puis de le confier aux soins de Mahran dès son retour.

« Justement, maître, il est déjà là, il vous attend dans la *qa'a*. »

À son air contrit, Sawirus sentit sa poitrine se serrer. Sans plus un mot, il se hâta vers le cœur de la demeure. Là, au débouché du couloir d'entrée se tenait Mahran, flanqué d'un gros homme habillé à l'occidentale mais avec un turban sur la tête. Les traits tirés, il était manifestement fatigué. La sueur avait dessiné des rides sur son visage, comme des traces de charbon dans la cendre et la poussière du voyage ternissait ses vêtements. Visiblement désolé, il souriait d'un air gauche. Il n'avait pas plus de vingt-cinq ans. Sawirus reconnut le fils d'un associé habituel. Le jeune homme salua rapidement, ne sachant que faire de ses mains. Sawirus inclina la tête à son tour.

« Maître Ayoul ! Quelle surprise, je ne vous savais pas en route pour ma cité, mais nous saurons vous loger comme il se doit, en ami.

--- Je vous rends grâce de votre accueil, maître Sawirus, mais vous me voyez cheminer le cœur lourd jusqu'à votre hôtel...

--- Que se passe-t-il ? Ton père aurait-il rendu son âme à Dieu ? Il est pourtant de quelques années mon cadet !

--- Il ne s'agit pas de lui... Si je suis ici, c'est pour mener vos biens, à la demande de mon père justement, et pour vous informer que nous avons porté en terre votre fils, le pauvre Yazid, il y a deux huitaines de cela. Des brigands, à l'entour de Tyr et Toron, sûrement des fuyards de Panéas[^38] ou de la garnison turque massacrée par le prince d'Antioche... »

Sawirus crut que son crâne allait exploser, il se retint à la porte pour ne pas défaillir. Les silhouettes se troublèrent devant ses yeux embués et sa gorge le serra tant qu'il lui paraissait être étranglé par quelque démon. Il se laissa glisser doucement au sol. De la salle voisine, un cri de femme déchira le silence. Une mère avait perdu son fils.

### Ascalon, jeudi 10 septembre 1153, matin

Le modeste groupe de cavaliers avançait doucement au travers des jardins abandonnés. Les murets en avaient été abattus, les arbres maltraités, les cultures dévastées. Parfois, quelques cabanes rapidement édifiées s'écroulaient dans un nuage de poussière. Ici et là, des croix surmontaient les fosses où des corps avaient été pieusement allongés. En tête, un peu affaissé sur l'encolure de sa monture se tenait Sawirus. Il regardait d'un œil triste le spectacle qui s'offrait à eux.

Le fossé était par endroits empli de broussailles, de terre et de gravats, sauvagement arrachés dans les potagers voisins. Des protections de bois, des vestiges de navires dépecés gisaient ici et là, arborant quelques tronçons de flèches comme les stigmates de la bataille qui avait eu lieu. Ironiquement, des souïmangas au plumage irisé lançaient leurs trilles, posés sur ces perchoirs improvisés. Les restes d'une grande tour d'assaut en partie incendiée étaient en cours de démontage, et des captifs habillés de hardes s'activaient au dégagement des éboulis de pierre au droit des murailles éventrées, sous la surveillance goguenarde de soldats repus.

Ascalon avait été prise après de longs mois de siège, désormais la bannière du roi de Jérusalem claquait dans le vent sur les remparts. Et les négociants comme Sawirus et ses compagnons s'en venaient pour voir comment rendre profitable cette désolation. La guerre engraissait le commerce, et les marchands. L'entrée se faisait par la grande porte, celle du levant, où se trouvaient désormais les hommes en charge de la ville d'Ascalon, en attendant l'installation de l'administration royale. Le garde qui les arrêta, la barbe poussiéreuse et le cheveu rare, avait le côté du visage encore tuméfié et parlait avec un chuintement sifflant. Il leur indiqua le bâtiment converti en halle, une ancienne mosquée appelée la Verte.

Une fois l'enceinte franchie, le lieu s'annonça plus paisible. La rue principale, comme la majeure partie de la cité, n'avait vu aucun affrontement et la reddition négociée avait permis à l'endroit d'échapper à la dévastation. De temps à autre, on apercevait une ombre rasant les murs, quelques chrétiens orientaux qui avaient répugné à abandonner leurs biens. Depuis un peu plus de deux semaines qu'Ascalon était tombée, la vie reprenait peu à peu ses droits, les hommes devenaient plus hardis, et les voyageurs autorisés à entrer apportaient un peu d'animation moins effrayante que les soldats en armes. Obliquant en direction de la porte de la mer, ils pouvaient désormais admirer le bâtiment qu'on leur avait indiqué, situé sur un promontoire au sud-ouest de la ville. La taille imposante de ce dernier et sa proximité avec la plage en faisaient un centre de commerce idéal.

Abandonnant les chevaux à un de leurs valets, les trois marchands pénétrèrent dans la grande cour, au travers de la cohue générale. On s'invectivait dans toutes les langues du Levant, on s'interpellait d'une arcade à l'autre, on se saluait de gestes démonstratifs. Des paquets étaient vivement hissés sur les bâts, des ballots se faisaient éventrer pour en présenter le précieux contenu. Étoffes délicates, rutilants objets de cuivre, céramiques colorées, verre, bijoux..., les richesses de la ville étaient offertes au regard, posées à même le sol sur de pauvres tapis ou brandies bruyamment par les camelots. Quelques soudards, la mine réjouie, négociaient le fruit de leurs rapines avec des négociants chicaneurs. On mordait les pièces[^39], on soupesait leur poids, on palpait les bourses de monnaies scellées[^40].

Sawirus fut bousculé par un homme à l'expression dédaigneuse qui menait une mule chargée d'un incommensurable fatras d'armures, casques, fers de lance, épées. Les trois compères se regardèrent, un peu perdus, ne sachant par où commencer. Le plus jeune, Shabib, proposa de se séparer afin de prospecter plus rapidement. Sans même attendre de réponse, il disparut dans la foule bigarrée. Sawirus n'aimait guère se trouver ainsi malmené de droite et de gauche, devant surveiller sans cesse sa ceinture, craignant qu'un larron n'y mette la main sur son argent.

Il croisa quelques visages patibulaires de guerriers en patrouille, le regard mauvais. Ils auraient certainement préféré s'activer à dépecer la ville de ses richesses plutôt qu'à garantir la sécurité de ceux qui profitaient de l'aubaine. Ils se contentaient donc de cheminer par petits groupes, heurtant de leurs armures sales les négociants affairés. Indifférente à leur avance, la foule n'était guère plus vexée de leur brutalité qu'un torrent malmené par des rochers.

Sawirus pénétra dans la grande salle, où il découvrit que se déroulait la vente des esclaves. Sur la chaire de prêche convertie en estrade, on exhibait les captifs, les mains et les pieds entravés par des cordes ou par des fers pour les plus intrépides. Pour l'heure, des soldats turcs attendaient en file, crachant et maudissant de leur langue rude leurs geôliers tout en se débattant. Un grand guerrier aux longues tresses, au port altier, venait d'être vendu pour trente-cinq dinars égyptiens, une misère pour un tel homme.

Se demandant à quel taux il pouvait acquérir des monnaies, Sawirus s'avança vers les bancs de change qui occupaient tout un pan de mur. Là, les trébuchets cliquetaient des pesées rapides, chacun comparait ses notes avec le voisin. Pour l'heure, la pièce d'or égyptienne pouvait s'obtenir pour un besant de Jérusalem, ce qui constituait une bonne affaire.

Il était perdu dans ses réflexions, se lissant la barbe, désormais blanche, tout en calculant grossièrement les profits possibles, lorsqu'il entendit claquer un fouet. Il se retourna et vit un sergent franc frapper à coups répétés sur un jeune captif musulman recroquevillé à ses pieds, s'attirant un regard indigné des marchands les moins endurcis. Sawirus s'avança calmement, interpellant l'homme d'une voix douce.

« Cesse donc, soudard, tu vas tuer ce pauvre enfançon ! »

Le bras se figea et un regard surpris croisa celui du marchand. L'homme n'était visiblement pas bien méchant.

« C'est qu'il a le teston plus dur qu'une pierre de meule ! Il refuse d'obéir.

--- À tant le bestancier, tu n'arriveras guère à le faire avancer.

--- Tant qu'il arrive à l'estrade plus vif que mort, ça me va. »

L'enfant desserra un peu les bras, inquiet de comprendre pourquoi sa correction s'était interrompue. Les croûtes qui zébraient ses membres, la crasse qui maculait son visage, ses cheveux et ses guenilles indiquaient qu'il subissait ce traitement depuis un petit moment déjà. Lorsque Sawirus aperçut son regard d'animal apeuré, la question franchit ses lèvres malgré lui :

« Et quel prix en demandes-tu ? »

L'homme se racla la gorge bruyamment.

« Avec tous ces captifs, si j'en tire une douzaine de besants, j'en serai aise. »

Le marchand se pencha vers l'enfant prostré, qui se recula instinctivement.

« Quel est ton nom, garçon ? »

Il n'obtint en réponse qu'un souffle muet puis un murmure à peine audible :

« Yazid. »

Sans même y réfléchir, Sawirus se redressa puis compta vingt pièces d'argent au soldat ébahi. ❧

### Notes

La prise d'Ascalon en 1153 fut une grande victoire stratégique pour les forces latines moyen-orientales. La ville tenue jusque-là par les Fâtimides d'Égypte constituait une tête de pont qui menaçait sans cesse les territoires du sud des royaumes, jusqu'aux alentours de Jérusalem. La dynastie finissante des shi'ites égyptiens constituait une proie rêvée pour les pouvoirs régionaux. Ce fut d'ailleurs Saladin qui s'en empara finalement, avant de réaliser l'unification avec les territoires syriens sous son pouvoir. Bien que la prise de la ville se fût faite suite à une reddition, les habitants musulmans en furent délogés et escortés jusqu'aux territoires fâtimides, où ils furent razziés par des pillards turcs. On sait que certains habitants restèrent, dont des Juifs et vraisemblablement des Chrétiens orientaux qui n'avaient, en principe, rien à redouter des attaquants catholiques.

Le commerce continuait bien évidemment pendant les opérations militaires, qui n'avaient pas une ampleur telle que les communications fussent impossibles. Bien au contraire, un marché suivait généralement les armées, leur fournissant des vivres et recyclant, dans les échanges commerciaux, les prises de guerre dont les esclaves. Par ailleurs, le change demeurait possible à tout moment et certains négociants spéculaient justement sur les taux. Les marchands étaient très mobiles, n'hésitant pas à quitter leur famille pour des mois, voire des années. Et il arrivait qu'ils succombent des aléas des voyages, naufrage, maladie ou brigands et pirates.

### Références

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I à 6, Berkeley et Los Angeles : University of California Press, 1999.

Moy Daniel R., *Military strategy in the Latin Kingdom of Jerusalem : The Crusader Fortification at Caesarea*, Mast. Of Arts Thesis, Norman, Okalhoma, 1998.

Prawer Joshua, *Histoire du Royaume latin de Jérusalem*, Paris : CNRS Éditions, 2007.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem. A corpus. Volume I. A-K*, Cambridge : Cambridge University Press, 2008.

Troubles
--------

### Palais royal, Jérusalem, matinée du vendredi 16 avril 1154

La petite pièce était encombrée d'un immense plateau de chêne, et les nombreuses niches sur les parois étaient chacune munie d'une porte à verrou. Le décor, qui avait connu de meilleurs jours, représentait une tenture plissée en bas des murs et un faux appareillage ocre sur la partie supérieure. Des fenêtres à panneaux de verre éclairaient la scène, mais plusieurs lampes à huile avaient été ajoutées, le ciel demeurant gris et peu lumineux en ce triste matin d'avril.

Assis à la table, entouré de nombreux documents, tablettes de cire, parchemins et autres papiers griffonnés, le connétable Onfroy de Toron réfléchissait, le menton dans le poing, l'air ennuyé. Son impressionnante tignasse n'était que grossièrement domptée en une coupe à l'écuelle. Son cou puissant, tanné par les journées en selle, supportait un visage massif et impénétrable. Le regard inquisiteur était noyé sous des sourcils broussailleux et sa barbe naissante semée de blanc. Tout en lui inspirait le respect et la discipline, comme il seyait à un chef d'armée. Il soupira et repoussa plusieurs feuillets de devant lui, les tendant à un clerc aux yeux tristes et lui indiquant d'un geste de les ranger. Face à lui, assis sur un banc, se trouvaient deux autres personnes à l'allure martiale.

L'un d'eux était, comme lui, habillé d'un magnifique vêtement en soie qui scintillait à chaque fois qu'il bougeait. Plus jeune, il était aussi plus volubile et son visage franc inspirait immédiatement la confiance : un des membres de la maison de Ramla, de Hugues d'Ibelin. Il était tourné vers le dernier homme à la table, un chevalier poussiéreux encore en haubert. Le haut du crâne dégarni, le cheveu très court, celui-ci arborait un air affable, mais pour l'heure il semblait plutôt contrarié. Il avait posé son casque devant lui et se frottait régulièrement le nez, qu'il avait aquilin, d'un geste nerveux.

Le connétable brisa le silence, de sa voix habituée à ordonner et à lancer ses instructions en plein fracas des batailles.

« Si Norredin prend Damas, nous risquons grands soucis. Il sera en mesure de lever un ost pareil aux plaies d'Égypte. Je ne vois pas comment je pourrais lui opposer assez d'hommes.

--- La situation n'est pas encore perdue, messire connétable. Le prince de Damas propose même la cité de Baalbek et des territoires à l'entour, pour peu que nous lui prêtions assistance.

--- Le souci, Régnier, c'est que je ne vois pas comment lui venir en aide à temps. Vous m'avez dit que les troupes de l'isfahsalâr Shîrkûh étaient arrivées jusqu'où ?

--- Au septentrion oriental de la ville, le long d'une des voies.

--- S'il offre Baalbek, le roi Baudoin la recevra volontiers, mais il nous faudrait agir même sans cela... »

Soupirant, il reprit la liste des fiefs, suivant du regard l'énumération des hommes qu'il pourrait rassembler.

« En partant dans les jours qui viennent, je devrai me passer des forces d'Hébron et de celles d'Outre-Jourdain. Ceux de Césarée pourraient nous joindre, au moins en partie, plus la Galilée, Sidon et Beyrouth tandis que nous monterons... »

Se tournant vers son autre interlocuteur, il lui demanda :

« Le frère du roi aura temps de convoquer son ban à Jaffa selon toi, Robert ?

--- Je ne suis pas sûr qu'il soit possible d'appeler les troupes du sud, d'Ascalon. Mais une partie d'Ibelin, de Rama et de Mirabel pourra arriver à temps. Mon seigneur Amaury suit la situation de près. »

Le connétable hocha la tête en assentiment. Le frère du roi Baudoin était un allié fidèle et sérieux, et son comté de Jaffa-Ascalon répondait généralement sans faille aux appels de la couronne.

« Il me semble bien vain d'espérer arriver aux murailles de Damas avant... »

Il se tourna vers le clerc impassible à ses côtés, qui attendait, un stylet à la main, qu'on lui donne des instructions.

« Quel jour serons-nous dans une semaine ?

--- Ce sera la saint Georges, messire Onfroy.

--- Ah! S'esclaffa le guerrier. Que voilà bon signe ! Si seulement nous pouvions arriver là-bas pour cette date...

--- En ne prenant que quelques contingents d'hommes à pied, parmi ceux du nord, et surtout un fort parti de cavalerie, cela devrait être possible, avança Robert.

--- Dans le meilleur des cas, nous aurions dans les trois cents à quatre cent lances à leur opposer.

--- Cela pourrait suffire à l'affaire. Cela n'est peut-être qu'une nouvelle tentative pour intimider le prince Mugir al-Dîn » risqua le chevalier.

Régnier d'Eaucourt secoua la tête en dénégation.

« J'en ai grand doute. Cette fois, le fruit semble mûr. Il ne reste aucun opposant de valeur au seigneur d'Alep, et il a su convaincre le peuple.

--- En l'affamant, traître qu'il est ! lâcha le connétable.

--- Des hommes à sa solde répandent le bruit que le prince préfère nous livrer le grain du Hauran que nourrir la cité. Mais c'est en fait Norredin lui-même qui a fait intercepter les convois du nord.

--- N'y a-t-il personne pour dévoiler son mensonge ?

--- L'aḥdāṯ[^41] semble avoir pris son parti, et beaucoup préfèrent les Turcs à nous, car ils sont musulmans.

--- La belle affaire, ils vont les dépouiller aussi sûrement que des criquets !

--- L'assaut à l'encontre de leur cité ne plaide guère en notre faveur, messire. »

Le connétable se mordit la lèvre. Il savait que l'assaut manqué, en 1148, malgré le renfort de nombreux contingents venus d'Europe, demeurerait longtemps un déchirement pour le royaume. Leurs mains avaient été si proches de s'emparer de la cité que la simple mention de l'expédition était douloureuse. Il n'était pas encore connétable à cette période, mais il avait sué sous l'armure comme beaucoup de combattants, pour s'en revenir bredouille, la rage au cœur. Et maintenant leur pire ennemi, le Turc d'Alep, Nūr ad-Dīn, risquait de cueillir le fruit qu'ils convoitaient depuis si longtemps. Il avala quelques gorgées du verre qu'il venait de se remplir puis reprit la parole.

« De toute façon, nous ne pouvons demeurer sans rien faire. Je vais demander au roi de lever le ban pour que nous puissions répondre à l'appel damascène. Il nous faudra... »

Il se tut lorsque la porte s'ouvrit brusquement, dévoilant un sergent d'arme essoufflé, le regard affolé. Onfroy de Toron le dévisagea avec fureur, aussi énervé d'avoir été interrompu qu'inquiet de ce qui avait pu inciter à pareil outrage. Le soldat attendit néanmoins qu'on lui fasse signe de parler, soucieux de marquer son respect au chef des armées du roi.

« Messire connétable, on se bat en les rues de la Cité !

--- Pardon ? éructa Onfroy de sa voix tonitruante. Qui se bat ? Où ?

--- En le parvis du Saint-Sépulcre, à une portée d'arc à peine... Des hommes de Saint-Jean et ceux des chanoines. On parle de blessés, et peut-être de morts. »

Le connétable laissa échapper un soupir énervé.

« Quelle bande de pisseux vérolés ! N'ont-ils donc rien d'autre à faire ? Et pourquoi me déranger ? Le mathessep, ou plutôt le vicomte, sont là pour ça, non ?

--- Il n'y a personne au palais en dehors de vous, messire. Et comme cela est dans la rue à côté... »

Le connétable fit taire l'homme d'un geste, soufflant comme un bœuf, d'un air dépité et agacé. Régnier d'Eaucourt se leva, attrapant son casque.

« Laissez-moi m'en occuper, messire. Je suis en tenue et pourrai me protéger si l'envie leur en prenait.

--- Qu'ils essaient de s'en prendre à un chevalier de l'hôtel du roi, et il pourrait se voir quelques écorchés avant la fin du jour ! gronda Onfroy avant de se reprendre d'une voix plus calme : Mais tu as raison, prends avec toi les quelques sergents que tu croiseras et tente de calmer ces maudits clercs. »

Le chevalier salua d'un signe et sortit précipitamment, suivi du soldat. Le connétable secouait la tête, dépité.

« L'ennemi est partout autour de nous et ces maudits tonsurés n'ont que ça à faire, de s'entrebattre !

--- On m'avait dit qu'il y avait soucis, mais je ne les pensais pas si fort courroucés.

--- Ah ! Ils seraient prêts à s'égorger pour l'étron d'un vilain, s'ils pensaient qu'il doit chier en leur latrine. Maudits moinillons pleins de morgue ! »

### Parvis du Saint-Sépulcre, Jérusalem, midi du vendredi 16 avril 1154

Lorsqu'il arriva au débouché de la rue qui donnait sur le Saint-Sépulcre, Régnier constata que les pèlerins avaient reflué jusque-là, bloquant en partie l'accès. Quelques curieux tendaient le cou, intrigués par l'échauffourée. Avec l'aide de sa poignée d'hommes, le chevalier parvint néanmoins à forcer le passage et s'avança au milieu de l'endroit désert, les poings sur les hanches. C'était la première fois qu'il découvrait le lieu sans une foule allant et venant, ni camelots affairés. Ces derniers avaient tous remballé rapidement leurs pacotilles et s'étaient éclipsés, craignant les affrontements qui n'allaient certainement pas tarder à dégénérer.

Régnier examina l'entrée du sanctuaire. Les portes de bronze avaient été fermées, et seul l'accès en haut des escaliers, à droite, était maintenu ouvert. Quelques visages inquiets y étaient discernables, protégés derrière l'angle du mur et un banc dressé. Au sud, le vaste bâtiment des frères de l'hôpital de Saint-Jean était presque achevé. L'imposante tour qui couronnait l'ensemble était encore en cours d'édification, et des passerelles de bois s'y accrochaient, suspendues dans le vide. Là aussi, plusieurs têtes qui tentaient de se faire discrètes apparaissaient par endroits. La grande porte d'accès aux salles était également bloquée.

Régnier leva le regard, plissant les yeux pour se protéger du soleil qui pointait derrière les nuages. Il salua une fois, la main haute et s'annonça comme chevalier du roi. Il n'obtint pour seule réponse qu'un envol de quelques pigeons, qui se réfugièrent sur une corniche plus élevée, d'où ils auraient une meilleure vue sur l'amusant spectacle des hommes. Cherchant les vestiges d'un éventuel affrontement, Régnier remarqua plusieurs flèches brisées sur le sol, contre le mur de l'église. Mais nulle part il ne découvrit de traces de sang. Il souffla, rassuré, et lança d'une voix forte :

« Il n'est nul besoin de répandre le sang en la Cité. Que deux hommes s'avancent vers moi depuis chaque côté.

--- Qu'ils jettent à bas leurs arcs d'abord, rétorqua une voix venue du Saint-Sépulcre.

--- Ils sont armés et ont tenté de nous assaillir, répliqua un homme de l'hôpital »

Régnier fit signe à ses hommes d'approcher et les disposa en ligne, séparant la cour en deux zones.

« Voilà, des sergents du roi empêcheront quiconque d'avancer, nul ne pourra les passer outre sans devenir l'ennemi de la couronne. Cela vous suffit-il comme garantie ?

--- Et les archers, lança la voix depuis l'église.

--- Ils ne tireront pas sur mes hommes et moi, et vous ne saurez les assaillir, ils n'ont donc nulle raison de décocher des traits. »

Après un long moment, une ambassade s'avança depuis chacun des côtés. Pour l'hôpital, un frère de Saint-Jean à la tenue de travail fatiguée, au regard effarouché mais à l'aspect rusé, accompagné d'un massif ouvrier, les mains comme des massues, taillé dans un tronc de chêne et le visage aussi alerte que celui d'un baudet. Face à lui, un chanoine à la robe de qualité plissant sur ses formes rebondies, au crâne d'oeuf et à l'élocution zézayante. Il s'était également adjoint un aide de camp à l'intelligence toute musculaire. Les quatre hommes se dévisageaient avec colère, chacun des deux grands costauds très désireux de prouver à l'autre qu'il pouvait adopter un air plus bovin que lui. Régnier prit quelques instants pour réfléchir à la façon de mener cet entretien avant de se lancer.

« Comment se fait-il que j'encontre clercs en bataille aux portes mêmes du Sépulcre du Christ ?

--- Cela ne concerne nullement le roi, précisa le petit chanoine replet, c'est là affaire privée entre les frères de Saint-Jean et nous.

--- Cela concerne forcément le roi que deux des plus grands ordres s'affrontent en bataille à une portée d'arc de son palais ! »

L'hospitalier leva un doigt pour s'immiscer dans la conversation.

« Je tiens à démentir. Nous ne portons le fer nulle part, nous ne faisons que défendre nos biens... »

Il n'eut pas le temps de finir sa phrase que l'autre clerc tempêtait et l'insultait, avec des expressions qui auraient certainement choqué plus d'un charretier. Il fallut un petit moment avant que le calme ne revienne.

« Faites-moi récit de ce qui s'est passé, sans en chercher cause, demanda Régnier.

--- Des hommes du Sépulcre ont tenté de mettre bas nos échafauds...

--- Certes pas, ils voulaient juste calmer un peu l'ardeur des sonneurs de cloches !

--- Nous avons droit de cloche, ne vous en déplaise...

--- Alors sonnez lorsque c'est temps, pas en plein sermon de mon seigneur le patriarche.

--- Qu'a-t-il besoin de faire sermon en le parvis aussi ?

--- C'est là son privilège, depuis les temps du bon roi Godefroy, qui était ami du Saint-Sépulcre...

--- Calme, amis ! Calmes ! On m'a parlé de blessés, voire de meurtrerie ! Je ne vois pas de sang mais quelques traits au sol. Pouvez-vous me dire... »

Il n'eut pas le temps de finir que le chanoine éructait, excité comme un coq de combat.

« Ils ont tiré moult flèches sur nos gens, voilà. Et en ont blessé de leurs traits assassins !

--- Ils venaient à nous brandissant des hasts[^42], comme larrons en chemin. Heureusement que nous avions un archer de talent, sinon ils auraient bien mis à bas notre tour ! »

Une fois encore, Régnier intervint, séparant les deux adversaires, attendant patiemment qu'ils se lassent de s'invectiver.

« Combien d'hommes sont navrés parmi les vôtres ? » demanda-t-il au chanoine.

Après quelques tergiversations, le clerc finit par avouer d'une voix moins assurée :

« Il y en a un qui a reçu profonde entaille en la main, et un autre blessé à la cheville...

--- Par un trait ?

--- Non, hésita l'homme. Il a chu en trouvant refuge. Mais si on ne lui avait pas tiré dessus, ce ne serait...

--- Pas de mort ? »

Le chanoine secoua la tête en dénégation, presque déçu de n'avoir pas de plus beaux martyrs à brandir pour sa cause. Le chevalier se tourna vers l'hospitalier et lui posa la même question.

« De notre côté, ce sont surtout des atteintes à nos biens, à l'édifice que nous édifions pour les marcheurs de Dieu... Cela risque d'entraver notre sainte mission. »

Régnier hocha la tête et reprit la parole d'une voix ferme.

« Je vais laisser ici quelques sergents, qui veilleront à ce que nul trouble ne vienne se répandre. Le vicomte sera informé et je pense qu'il verra des gens de vos ordres. D'ici là, veillez à ce que vos gens ne s'emportent pas. Pensez à tous ces pèlerins qui espèrent aux portes du sanctuaire ou dans les lits de l'hospice. Il sera temps de vider votre querelle devant la Cour de Sa Sainteté le Pape. »

Les deux clercs hochèrent la tête, contrariés, obtempérant visiblement à contrecœur. Lorsque le petit groupe se dispersa, le chanoine ramassa avec soin toutes les flèches qu'il aperçut, et les brandit en un signe de défi avant de rentrer dans l'église du Saint-Sépulcre. Régnier reprit le chemin du palais, accolé au sud-ouest du sanctuaire. Il se frottait le visage, inquiet. Le royaume venait à peine de se remettre des affrontements entre la reine mère Mélisende et le roi Baudoin. Il ne manquait plus qu'aux clercs de dégainer les armes pour de futiles prétextes tandis que pendant ce temps, Nūr ad-Dīn approchait ses mains de la cité de Damas... ❧

### Notes

Dans les premières années de la décennie 1150, le jeune Baudoin III parvint enfin à s'assurer la mainmise sur le royaume. L'affrontement avec sa mère Mélisende avait créé de nombreuses difficultés internes au royaume alors que la Seconde Croisade échouait devant les murs de Damas.

Le succès à Ascalon supprimait la dernière tête de pont Fâtimide mais ne suffisait pas à éclipser l'échec dans les combats orientaux. Damas demeura hors de portée du pouvoir chrétien et, surtout, fut annexée par Nūr ad-Dīn à ses possessions nordiques. L'émir établissait ainsi dans les territoires musulmans une unité telle qu'il n'y en avait pas eu depuis l'arrivée des Latins en Terre sainte, plus de cinquante ans auparavant. Cela explique pourquoi les efforts des deux adversaires se portèrent ensuite sur l'Égypte, où la dynastie chiite en place vacillait, offrant le pays à qui aurait la force de s'en emparer.

À côté de cela, la fortune des ordres religieux militaires en avait fait des pouvoirs quasi autonomes au sein des territoires chrétiens. Indépendants du royaume laïc, ils avaient été également affranchis par le Pape de la tutelle du clergé local, ce qui provoqua de nombreux remous. En outre, depuis quelques années, parmi ces exempts, les frères de Saint-Jean (communément appelé Hospitaliers) se militarisaient à grande vitesse. Il se voyaient régulièrement remettre des forteresses, comme on le faisait déjà pour l'ordre du Temple. Fort de cette puissance armée, Gilbert d'Assailly, futur grand maître de l'Hôpital, fut d'ailleurs un des chauds partisans de la politique agressive envers l'Égypte.

### Références

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.

Elisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades*, Damas : Institut Français de Damas, 1967.

Riley-Smith Jonathan, *The Knights of St. John in Jerusalem and Cyprus c.1050-1310*, McMillan, 1967.

Amis pour la vie
----------------

### Abords du Bourg-l'Évêque, Bayeux, mercredi 3 novembre 1148

La presse était si dense sur le champ de foire de la Toussaint qu'il fallait sans cesse se contorsionner ne serait-ce que pour demeurer en place. On se bousculait, on se gênait, on piétinait dans l'herbe devenue boue en raison de la pluie et de la cohue agglutinée parmi les étals et les cabanes. Des éclats de voix amicaux, intrigués, interrogateurs, moqueurs fusaient depuis les groupes de marchands industrieux, jaillissant des échoppes le long des allées. Des soldats parcouraient la foule, d'un pas alerte, le visage goguenard au milieu des hommes d'affaires. Ils fronçaient les sourcils, dévisageaient quiconque aurait pu se prêter à un larcin. De temps à autre, un notable, fier chevalier à l'allure martiale, ou ecclésiastique à la tenue chatoyante s'avançait, impérieux, généralement environné de parasites bourdonnants et obséquieux, saluant d'une main condescendante les boutiquiers dont ils venaient admirer les produits.

Devant un stand où plusieurs tonneaux avaient été mis en perce, un trio de négociants devisait de leurs marchandises respectives. Le premier, grisonnant à l'imposante bedaine drapée dans une riche cotte de laine orange, s'exprimait avec une voix forte.

« Entendez bien, maître Robert, je peux acheminer plusieurs douzaines de tonnels de ces vins. Chacun est de six muids parisiens, de bonne mesure.

--- Certes, maître Ermont, seulement douze sols la futaille me semble bien dispendieux, même si le vin est fort gouteux.

--- Mon souci, ce sont toutes les nouvelles taxes depuis Vézelay. Chaque seigneur fait son pont et y installe péage en ses piles !

--- Ne pourrions-nous trouver accord à quelques deniers rabattus ? Se rapprocher de onze sols ? »

Ne portant aucun intérêt à la discussion, un jeune garçon à l'impressionnante carrure, qui n'avait pas dix ans, le cheveu châtain coupé court et la mâchoire forte encadrant un regard clair, écarquillait de grands yeux sans s'éloigner de son père Ermont, le Bourguignon. Il n'avait jamais vu autant de denrées en un seul lieu. Des étoffes, surtout, de toutes les épaisseurs, dans un ébahissant foisonnement de coloris et de motifs.

En bons camelots, certains vendeurs interpelaient les passants devant eux en déployant en gestes amples les toiles, faisant miroiter les finitions, vantant la finesse des tissages. La plupart proposaient de la laine, bien sûr, mais on pouvait aussi trouver d'autres fibres, du chanvre, du lin voire du coton, ou des assemblages mélangeant l'une et l'autre. La plus désirable néanmoins, la plus attirante, c'était la soie, que bien peu présentaient à l'envi. Il fallait s'afficher comme acheteur potentiel pour pouvoir admirer les riches brocards, les pièces brillantes gardées bien à l'abri, dans un écrin, dans un coffre discret. Un coupon nécessaire à la confection d'un vêtement aurait permis à deux ou trois familles de vivre une année entière, alors forcément, on ne les dévoilait pas à n'importe qui, on se méfiait des curieux qui auraient pu salir de leurs doigts fureteurs, endommager de leurs regards envieux ces articles de prix.

Par ailleurs, beaucoup de marchands n'avaient avec eux que des échantillons, car ils étaient là pour chercher des partenaires, comme son père. Il produisait du vin, achetait et revendait celui de nombreux autres vignerons, qu'il acheminait jusqu'aux lieux de foire. Pour l'heure, il avait quitté son emplacement, laissant Lambert, un de ses fils, tenir seul l'endroit, avec les quelques tonneaux qu'ils avaient apportés avec eux depuis Vézelay.

Ermont de la Treille était un négociant opulent, en biens comme en volume. Son ventre impressionnant était d'ailleurs en accord avec son teint couperosé. Il aimait son vin, celui des autres, et tout ce qui pouvait les accompagner lors de bons repas. Si ce n'était son dos épais et ses larges épaules, on aurait pu le croire abbé ou évêque tant il semblait empâté. La rouerie qui allumait parfois son regard et sa voix chaleureuse constituaient autant de signes évidents de son habitude au marchandage. Pour l'heure, il discutait âprement avec deux clients potentiels, dont le premier, aussi ventru qu'Ermont, remontait sans relâche une longue mèche malmenée par le vent sur son crâne. Le second paraissait presque maigre, enserré entre ces deux panses rebondies. Mais il appartenait sans doute aucun au même commerce qu'Ermont, son nez pouvait en témoigner. À ses côtés, un autre garçon s'ennuyait ferme, piaffant d'impatience, le regard au loin, s'attardant parfois sur la silhouette imposante d'Ernaut, cherchant à en sonder les intentions. Le visage brun et le poil frisé, un sourire espiègle apparaissait par intermittence, dévoilant deux dents cassées. Se rapprochant du jeune Bourguignon sans pour autant perdre le contact avec son père, il lui lança :

« Vous avez cheminé long temps ?

--- Pour sûr ! Par barge et chariot, il nous a fallu plusieurs semaines. Mais père a grand usage de cela... »

Le garçonnet hocha la tête, non sans avoir jaugé d'un œil expert le gabarit d'Ermont. Puis il reporta son attention sur Ernaut et lui sourit de nouveau.

« On me nomme Gillot, fils de Robert le Tonnelier. Et toi ?

--- Ernaut, de Vézelay.

--- Ton père fait aussi négoce de vin ?

--- Oui-da. Nos coteaux ont bonne fame par chez nous, et père s'emploie à les faire connaître au loin. »

Le jeune Normand secoua une nouvelle fois la tête en accord, bien décidé à ne rien céder en ce qui concernait son expertise du sujet. Son seul souci était qu'il n'y connaissait pas grand-chose. Prudent, il choisit donc de se rabattre sur un autre, mieux maîtrisé.

« As-tu parcouru la foire ? C'est peut-être la prime fois que tu t'y trouves ?

--- Je n'ai guère eu occasion à cela, je suis mon père en ses affaires, voilà tout. Mais j'ai pu apercevoir quelques attrayantes boutiques.

--- Il te faut absolument me compaigner... »

Il se pencha et souffla, le regard malicieux :

« Il y a au nord du pré plusieurs bains installés pour les voyageurs. Et ils ne dédaignent pas d'accueillir en leur baquet ravissantes pucelles. Je connais des lieux par où faire l'espie ! »

Ernaut ne put réprimer un gloussement de ravissement à l'idée d'aller voir. Pas tant pour ce qu'il pourrait y admirer, mais simplement enthousiasmé par la perspective de s'abandonner à un coupable amusement. Gillot tira sur la manche de son père, assurant d'un regard à son désormais complice qu'il saurait obtenir l'autorisation de baguenauder parmi les étals.

### Bayeux, vendredi 5 novembre 1148

Les deux enfants couraient, armés de bâtons pour tenter d'effrayer ceux qui se mettaient en travers de leur route. Pour l'heure ils n'avaient eu à affronter que quelques volailles, une poignée de chats et de rares chiens, dont certains n'hésitaient pas à montrer les dents s'ils étaient affairés après sordide repas trouvé dans le caniveau. Le seul qui s'était opposé avec indifférence à leur avancée était un énorme goret au poil hirsute, dont la plus repoussante qualité était d'être surveillé par un porcher dont la méchanceté du regard n'était battue en brèche que par l'odeur pestilentielle s'exhalant de ses vêtements déguenillés. Les farouches aventuriers avaient opté pour une retraite stratégique.

Depuis deux jours qu'ils écumaient les meilleurs coins connus par Gillot, les deux garnements étaient parvenus à se faire une réputation satisfaisante auprès de badauds indignés ou d'habitants agacés, voire de commerçants chagrinés. Pour l'heure, ils caracolaient en direction du château, à l'angle sud-ouest de la cité enclose, fouettant de leurs badines leurs montures imaginaires. Les nombreuses flaques en chemin étaient autant d'occasions de salir leurs propres chausses que les personnes environnantes, dans un vaste éclat de rire bien entendu. Essoufflés, ils parvinrent aux abords de la rue qui partait de la cathédrale, depuis la petite place accueillant le parvis.

Un groupe de badauds discutait devant la colossale façade de pierre, entourant une poignée de voyageurs arborant la tenue des pèlerins, bâton en main et croix sur l'épaule. Gillot, intrigué, se rapprocha l'air de rien, tendant l'oreille. Il était question du passage Outremer du roi, que ces marcheurs de la Foi espéraient rejoindre. Ils venaient du Cotentin et prévoyaient d'arriver en Terre sainte d'ici l'été. Des curieux et des promeneurs leur demandaient de porter des nouvelles du pays à plusieurs d'entre eux partis depuis de nombreux mois, dont certains avec l'ost[^43] de France. Les informations étaient rares et la progression semblait moins aisée qu'il ne l'avait été supposé. De fréquentes bénédictions et pieuses invocations accompagnaient ceux qui allaient à leur tour entreprendre ce périlleux voyage.

Reprenant leur avancée vers le château, le jeune normand se tourna vers Ernaut, impatient de lui dévoiler le prestige de sa nation.

« Tu sais que le père de notre duc, il est devenu roi à Jérusalem ?

--- C'est pas Godefroy[^44] le preux, le suzerain du royaume Outremer plutôt ?

--- Ah non, le père du nouveau duc, Geoffroy[^45], est parti pour faire épousailles de la princesse, voilà déjà long temps.

--- Et il est toujours roi ?

--- Aucune idée. Il a certainement dû batailler ferme contre les païens ! »

Joignant le geste à la parole, Gillot faisait de grands moulinets avec son bâton, aussi dangereux pour les alentours que pour son propre visage.

« S'il est aussi adroit et rusé que le grand Guillaume[^46], il est de certes encore en selle, lance en main, et boute ses ennemis droitement...

--- Le grand Guillaume ? Je n'en ai point connoissance. C'était fort chevalier normand ? »

Gillot écarquilla les yeux, amusé et sauta à pieds joints dans une flaque.

« Le plus grand ! Il devint roi par ses prouesses !

--- Je ne te crois point, il n'est nul roi de France avec pareil nom, c'est là fable d'enfançon... »

S'arrêtant brutalement, Gillot posa les deux mains sur les hanches et commença à entonner à tue-tête une chanson égrillarde :

> « Fils à diable plus fort qu'ourson,\
> Il était fort bel enfançon.\
> Poil soufré bien dru sur son chief,\
> De sa mère tira bon lait\
> Qui le fit haut et bien épais\
> Mais de son père n'eut point fief. »

Secouant la tête comme un dément, il s'attirait quelques sourires convenus parmi les passants, du moins ceux qui comprenaient sans doute fort bien l'air qu'il déclamait.

> « D'un pet repoussait les saxons\
> Rotait flammèches sur bataillons,\
> Les seigneurs n'en voulurent baron\
> Mais devint roi à sa façon »

Tout en continuant à lancer ses vers avec entrain, le jeune garçon reprit son chemin en sautillant, accompagnant parfois son chant de gestes éloquents. Ce fut donc haletant qu'il parvint au débouché de la rue, face à l'impressionnante muraille. Mais il avait gardé suffisamment de souffle pour lancer avec emphase :

«Voici chastiaux que le roi Henri[^47] prit voilà bien longtemps, quand il fit brûler la ville. »

Devant le regard admiratif d'Ernaut, le jeune guide sourit.

« Ça te dirait de voir la belle demeure de pierre du chanoine Conan ? Elle aussi a brûlé alors, mais elle demeura assez belle pour faire envie au roi de France dit-on... »

Sans même attendre la réponse, Gillot se mit à galoper avec entrain en direction d'une ruelle qui s'ouvrait entre deux boutiques à la belle devanture. Le sourire aux lèvres, Ernaut le rejoignit, faisant claquer son bâton contre les murs des maisons.

### Bayeux, tôt le matin du jeudi 1er mai 1152

La cohue effroyable avait attiré un grand nombre de curieux et fait fuir les chats et chiens errants du quartier. Toute la jeunesse environnante s'était assemblée sous les fenêtres du vieil orfèvre récemment remarié. L'épouse venue de Bretagne, disait-on, était aussi sage et timorée qu'agréable à regarder, et les adolescents estimaient que cela était prétexte à aller les réveiller bien tôt en ce matin du premier mai. Nombreux parmi ceux présents auraient volontiers tressé une couronne pour orner son joli front.

Au milieu des agités, frappant comme beau diable ses deux louches en bois sur tout ce qui passait à sa portée se trouvait Ernaut. Lui n'avait aucune idée de l'aspect de la dame, mais cognait avec autant d'enthousiasme que les autres, hurlant et vociférant comme un animal en rut. Lorsque le vieil époux et sa nouvelle femme finirent par se montrer à la fenêtre, ils eurent droit à plusieurs chants aussi passionnés que grossiers. De bonne grâce, ils saluèrent la foule, et l'orfèvre, en bon stratège, leur jeta même quelques monnaies pour aller s'offrir des pichets de vin à leur santé. Les chansons grivoises se transformèrent quasi instantanément en bénédictions et souhaits de long bonheur. Puis la colonne reprit sa route, continuant un peu de tapage, histoire de ne pas faire oublier que ce jour, ils avaient bien l'intention de célébrer la jeunesse. Les matrones qui les croisaient arboraient un franc sourire ou un froncement de sourcils hostile, selon qu'elles aient une fille dans leur maison ou pas. En peu de temps, le charivari parvint à la porte du faubourg Saint-Jean.

L'enthousiasme n'avait guère baissé, mais les voix s'étaient calmées le temps pour eux de se concerter. Le guet n'allait guère tarder à déverrouiller les accès, il fallait se dépêcher de se confectionner des couronnes pour offrir aux jeunes filles, ainsi qu'amasser de nombreux rameaux à accrocher aux portes des belles. Ernaut n'en connaissait guère, mais il faisait confiance à Gillot, lui-même comptant parmi les adolescents les plus exubérants.

« Je te ferai montrance des plus belles, n'aie mie crainte. Aucune ne saurait résister à beau Bourguignon comme toi ! »

Disant cela, le jeune Normand lança une bourrade dans les côtes de son ami. Il était vrai que la stature d'Ernaut attirait souvent les regards féminins. Déjà plus grand que la plupart des adultes, presque d'une tête, beaucoup enviaient son impressionnante carrure. Il manipulait les tonneaux de son père comme d'autres de simples tonnelets, et rien ne semblait jamais pouvoir le fatiguer. Son inépuisable énergie n'était limitée que par sa malheureuse propension à la gaspiller en vains efforts ou geste maladroits. Sans que cela n'entame en rien son enthousiasme, d'ailleurs.

À la fin de l'après-midi, une partie de la population s'était assemblée aux abords du champ de foire pour y achever la journée en musique et en danse. Chacun souriait, heureux de voir que les traditions étaient respectées, et que la jeunesse était pleine de fougue et d'entrain. On avait ri de la mésaventure de quelques parents, ayant découvert un de leurs enfants parmi les couples à l'ardeur un peu trop prononcée. On avait chanté, dansé et joué sans se soucier des remontrances d'une poignée de chanoines révoltés par le paganisme latent de ces pratiques. Une longue farandole serpentait, ondulant au rythme des musiques et des cris, emportée par des adolescents bondissants.

Transpirants, Ernaut et Gillot lâchèrent les mains au détour d'un virage et rejoignirent un tavernier qui avait fort opportunément mené quelques tonneaux. C'était de la petite bière, pleine d'amertume, mais fort rafraichissante.

Tandis qu'ils dégustaient leur breuvage, s'essuyant le front de leur manche, un troupeau de jeunes filles gloussantes et ricanantes s'approcha des deux garçons. La moins farouche, jolie brune à l'air espiègle, esquissa une courbette moqueuse et sourit malicieusement à Gillot.

« Dis-moi, le Tonnelier, comptes-tu tenir à l'écart ton compaing ainsi toutes les danses ?

--- La paix, Osane, nous sommes en eau à force de sauts et de cabrioles !

--- Déjà épuisés ? Puis elle ajouta, le regard lourd de sous-entendus à destination d'Ernaut : quel dommage, pourtant on le croirait bâti comme ours sauvage... »

La remarque déclencha une salve de ricanements chez ses compagnes. Ernaut ne savait plus où donner de la tête, un sourire barrant son visage. Au cas où, il bomba légèrement le torse tandis qu'il avalait la dernière goutte de son gobelet, dévisageant la jeune fille aux longs cheveux nattés.

« Je ne voudrais certes pas affliger si joliets minois ! »

Osane offrit un large sourire en réponse, faisant plisser son nez de séduisante façon. Bousculée par ses camarades lors de cet échange, elle en perdit sa couronne de fleurs. Avant qu'elle n'ait eu le temps de réagir, Ernaut l'avait ramassée et la lui tendait, avec une joyeuse révérence. Les jeunes filles se mirent à pousser de petits cris et Gillot lui-même tapa dans ses mains tandis qu'elles commencèrent à scander :

« Le baiser de Mai ! Le baiser de Mai ! »

Un peu interdit, le colosse interrogea Gillot du regard. Amusé, celui-ci lui expliqua qu'à Bayeux, il était coutume d'embrasser celle à qui l'on offrait la couronne de mai. Les joues empourprées, Osane avait un peu perdu de sa superbe et espérait visiblement qu'il s'acquitte de son devoir. Bien peu embarrassé, Ernaut approcha son visage et déposa un rapide baiser sur les lèvres de la jeune fille, ce qui déclencha une explosion de rires et d'exclamations joyeuses. Puis, rejoignant la farandole, les mains s'accrochèrent et les jambes s'élancèrent en rythme. Ernaut eut à peine le temps de rendre son gobelet qu'Osane l'entraînait dans une danse échevelée, le regard énamouré, illuminé par un sourire radieux.

### Vézelay, après-midi du mardi 29 mai 1156

Ermont de la Treille était en train de superviser un chargement de tonneaux à destination de Cravant, où ils seraient placés sur des barges. Il avait ajouté à ses propres fûts ceux d'autres petits récoltants des environs, et il expédiait tout cela, par voie d'eau, jusqu'à Paris, la Normandie et même au-delà. Revêtu de son habituelle cotte de laine fatiguée à la couleur indéfinissable, il mettait souvent la main à la pâte, travaillant aussi dur que ses employés.

Pour l'heure, la chaleur était telle qu'il prenait le frais tout en discutant avec le charretier qui mènerait son bien par les chemins. Ayant son cellier ouvert directement sur la grande rue qui montait vers la basilique de La Madeleine, il aperçut bien vite le petit groupe de voyageurs, dont il reconnut immédiatement plusieurs membres. C'étaient comme lui des habitués des foires normandes et parisiennes, des négociants avec lesquels il faisait parfois affaire. Mais il était surprenant de les voir venir jusqu'à Vézelay. Levant la main en guise de salut, il leur sourit, déclenchant en retour d'éloquentes démonstrations amicales.

« Le bon jour à toi, Ermont !

--- La bien venue en notre fière cité de Vézelay, compaings. Heureuse surprise que voilà, je n'ai pas usage de vous y encontrer. »

Le visage de son interlocuteur se ferma instantanément tandis qu'il serrait avec enthousiasme la main offerte.

« C'est que je suis porteur de bien tristes nouvelles. J'ai donc décidé de pousser jusque ici, les donner de moi-même.

--- Quelles sont-elles ? Quelque guerre en France de nouveau ? Ou en Normandie ?

--- Rien de ce genre, même si cela n'est jamais exclu...

--- Vous m'intriguez fort. »

La face couperosée et débonnaire d'Ermont devint inquiète tandis que son front se plissait. D'un geste, il fit signe à la cantonade d'entrer chez lui. Une fois dans la cave, parmi les fraîches odeurs de vin, il redemanda ce qu'il en était.

« Je suis venu vous faire savoir que le Tonnelier est passé, voilà plusieurs semaines de cela.

--- Robert ? Mais comment cela ? Il était bien de dix ans mon cadet, et aussi solide que beffroi d'église !

--- De certes. Mais il a péri, avec ses biens et son fils Gillot. Emportés en estuaire de Seine avec leur embarcation. »

Ermont remua la tête, secoué.

« Je n'ai jamais aimé ces lieux. Trop mouvants, trop capricieux... Cette maudite brume !

--- Il portait à Rouen marchandises. Son pilote habituel, le père Berthelot n'a pas passé l'hiver, à cause de fièvres quartes. Alors il a pris un jeune, mais nul n'en est revenu.

--- Un pilote jeunot pour frayer parmi l'estuaire, voilà bien folle idée... Le Ratier[^48] n'est guère enclin à pardon. »

Les deux hommes gardèrent le silence un long moment. Puis Ermont remplit un pichet avant de servir l'assemblée, les lèvres toujours closes. Seules des paroles venues de la rue ou de l'arrière-cour, les aboiements de chien excités ou amusés résonnaient par-delà la porte. Un chant se fit soudain entendre, poignée de strophes sans queue ni tête lancées sur un air entraînant par une voix grave. Le regard d'Ermont se fit sévère.

« Voilà mon cadet, Ernaut. Il est fort compaing du jeune Gillot. Son passage Outremer est prévu pour dans quelques jours, à destination de la Sainte Cité. Ne lui en soufflez mot, il en aurait grande peine et y verrait peut-être ténébreux présage. »

Lorsque le jeune géant entra dans la salle voûtée, il marqua un temps, avant de reconnaître certains des visages. Puis il salua l'assemblée d'un geste nonchalant, son imposante silhouette se découpant sur la lumière extérieure. N'obtenant qu'une réponse peu enthousiaste, il s'esclaffa.

« Hé bien, compaings, n'y a-t-il pas toujours joie à arriver sauf à destination ? » ❧

### Notes

Le commerce médiéval n'était pas que local et de nombreux produits voyageaient au loin. On pense généralement aux épices venues d'Orient, par-delà la Méditerranée, mais d'autres denrées bien moins exotiques traversaient l'Europe. Le vin de Bourgogne était ainsi acheminé par voie d'eau tout au long de la Seine, ce qui lui permettait de toucher les régions nordiques, les ports normands offrant bien évidemment des débouchés vers l'Angleterre et au-delà.

Les foires et marchés étaient des lieux de rencontre et de sociabilisation où les réseaux se formaient. Cela pouvait être à la fois des zones de contact pour gros négociants, qui faisaient voyager les produits de loin en loin, mais aussi pour les revendeurs et colporteurs locaux. Ceux-ci approvisionnaient ensuite les villes et villages environnants.

Ces échanges expliquent la forte mobilité de nombreux hommes d'affaires, rompus au commerce à longue distance, aux taux de change et aux poids et mesures, ainsi qu'aux frais de douane divers. La batellerie était importante, beaucoup plus fréquemment utilisée que le transport par voie de terre. Ceci malgré les conditions de navigation parfois difficiles, souvent dangereuses.

### Références

Alexandre-Bidon, Danièle, Lett Didier, *Les enfants au Moyen Âge*, Paris : Hachette, 1997.

Neveux François, *La Normandie des ducs auxs rois Xe-XIIe*, Rennes : Ouest-France Université, 1998.

Sadourny Alain, « Les transports sur la Seine aux XIIIe et XIVe siècles », dans *Actes des congrès de la Société des historiens médiévistes de l'enseignement supérieur public. 7e Congrès*, Rennes : 1976, p.231-244.

Délivrance
----------

### Large des côtes de Terre sainte, matin du dimanche 16 octobre 1155

Le jeune homme regardait la voile du *Peregrina* battre avec rage, bousculée par un souffle fougueux. Pourtant les matelots chargés d'en retendre les attaches ne semblaient guère s'en émouvoir. Ils étaient tournés vers l'ouest, les yeux écarquillés sur une minuscule tache signalée par la vigie. Autour de Giovanni, une foule s'était amassée à tribord, dans l'espoir de pouvoir contredire l'annonce. Une petite galère rapide, qui n'arborait clairement aucun signe chrétien s'approchait à grande vitesse. Un vaisseau hostile les avait pris en chasse.

Le jeune marin se mordait la lèvre, plissant les paupières sans parvenir à effacer l'inquiétante vision. Ce fut la voix de Nirart, un des pèlerins français, qui le fit émerger de sa torpeur.

« Avons-nous chance de trouver havre sauf avant qu'il n'arrive sur nous ? »

Frappé de mutisme, le matelot fronçait le nez de désagrément. Ses paupières fatiguées se baissèrent lorsqu'il finit par répondre.

« Je crains bien que non. Avec ce vent arrière en rafale, nous ne sommes guère aidés. La galée arrive par le travers, à force rames. Elle est donc moins gênée par les vagues. »

Après un long moment, le voyageur secoua la tête, la voix tremblante :

« Et ne pouvons-nous pas voguer au plus près des côtes ? Nul soutien ne pourrait en venir ? Il s'agit là tout de même du puissant royaume chrétien de Jérusalem... »

La moue éloquente en guise de réponse lui compressa la poitrine, lui arracha le souffle. Sa femme l'accompagnait, ainsi que tous ses amis du village. Partis de France, ils avaient cousu sur leurs vêtements la croix des pèlerins avec la joie dans leur cœur, impatients de s'agenouiller auprès du tombeau du Christ.

À quelques encablures de la Terre promise, à une poignée de jours de voyage de leur destination, après tous ces sacrifices sur la route, ils voyaient leurs espoirs anéantis, emportés dans un tourbillon de violence et de sang. Ils avaient toujours su que le risque existait, mais avaient préféré le négliger. Leurs âmes simples ne pouvaient envisager si atroce réalité.

Oubliant Giovanni, Nirart fixa son épouse Phelipote, pelotonnée contre lui, cherchant un bien maigre réconfort dans ses bras. Tournant la tête pour baiser tendrement son front, il s'aperçut alors que l'agitation gagnait l'équipage. Sur le château arrière, le patronus lançait ses ordres, visiblement décidé à ne pas abdiquer sans se battre. Il semblait croire son vaisseau assez rapide pour semer le poursuivant. En un instant, les haubans étaient emplis de mains affairées, les voiles assurées par des gestes précis.

Bien que l'angoisse soit présente, quasi palpable, il ne subsistait aucun doute dans les regards, dans les voix des marins aguerris. Leur seul espoir résidait dans la fuite. Ce n'était pas les quelques armes désuètes ni les trois soldats du bord qui pourraient les protéger de la furie de leurs assaillants. Le salut se trouvait dans leur adresse à contrôler le Peregrina avec talent. Un labeur qu'ils connaissaient.

Inutiles sur le pont, on repoussa les pèlerins dans la cale nauséabonde d'où ils avaient jailli aux cris de la vigie. Mais avant cela, sans ménagement, on sépara les hommes des femmes et des enfants. Lorsque Nirart demanda pour quelle raison, on le mena sans plus d'explication auprès d'un tonneau d'où l'on extrait à son intention une épée, tout en lui passant une targe[^49] fatiguée, amenée depuis le château avant.

« Je croyais qu'il y avait espoir de les semer... N'est-ce pas ce que vous croyez ? » glissa-t-il.

Le matelot au rude visage tanné qui venait de lui donner le bouclier inspira lentement, entre angoisse et exaspération.

« Espoir, de certes. Mais guère plus... »

Puis il tapa sur l'épaule du pèlerin en un geste qui se voulait réconfortant, tout en faisant signe au suivant de s'avancer. Tandis qu'on le menait sur un côté avec les autres hommes équipés, hagard parmi une foule de stupéfaits, Nirart lança un regard vers la trappe qui descendait vers les entrailles du navire. Voyant qu'on en clouait l'accès, il en eut le cœur brisé : il n'avait même pas eu le temps d'embrasser son épouse comme il l'aurait souhaité.

### À bord de la galée égyptienne, fin de matinée

Ja'mal Abd al'Aziz triait avec soin les flèches qu'il allait décocher dès que leur navire serait assez proche du bateau franc. Il était particulièrement adroit pour lâcher des traits visant à trancher les cordages, déchirer les voilures. Il n'était donc pas de ceux qui s'épuisaient présentement à souquer pour intercepter le vaisseau ennemi.

À bord ne se trouvaient que des soldats, qui se faisaient rameurs à l'occasion, le temps de s'emparer de leur proie. Et parmi eux, Ja'mal n'était pas un novice, ayant appartenu à la flotte d'Ascalon, avant que la ville ne tombe aux mains des chrétiens. Là, il avait pris part à de fréquentes batailles. Bon archer, il était toujours resté en retrait des affrontements, autant qu'il le pouvait.

Une méchante blessure suite à sa rencontre avec l'arme d'un lourd cavalier de Jérusalem lui avait valu d'osciller entre la vie et la mort durant de longues semaines. Aujourd'hui encore, des années après, il claudiquait et ressentait la douleur dans ses hanches de temps à autre. Ses compagnons le brocardaient parfois, mais lui conservaient néanmoins du respect.

C'était d'ailleurs bien la seule chose qu'il avait pu conserver. Malgré le butin amassé, les gages corrects qu'il percevait en se mettant parfois au service de chefs de guerre turcs, il n'avait jamais réussi à épargner le moindre dinar. L'argent lui glissait entre les doigts comme du sable. Il savait pourtant que le temps n'était guère clément pour les soldats. Lorsque certains émirs avaient décidé d'armer des vaisseaux pour attaquer les côtes franques, il avait tout de suite voulu se joindre à l'expédition. En dépit de l'opposition du vizir, ibn Ruzzîk, qui préférait traiter avec l'ennemi, une escadre avait pris la mer.

Ja'mal était à bord d'une des galées, fin vaisseau conçu pour la course et les coups de main rapides. D'autres, plus ventrus, avaient été emplis d'hommes parlant la langue des Francs, habillés comme eux, pour aller chercher des informations et s'emparer par la ruse de ce qu'ils pourraient saisir.

Ja'mal était un soldat, un soldat usé. Il fallait absolument que cette dernière campagne lui rapporte. Avec assez de biens, il pourrait s'offrir un mariage satisfaisant. Peut-être pas avec la plus belle ou la plus talentueuse des épouses, mais au moins avec une qui le soignerait sur ses vieux jours. Et, qui sait ? qui pourrait lui donner quelques enfants.

Il était assis à côté d'Ahmed, pour qui c'était la première saison. Un blanc-bec jouant les fiers à bras, mais qui semblait présentement ne pas en mener large. Ja'mal en avait vu beaucoup comme ce garçon, pleins d'entrain tant que l'épreuve n'était pas encore là. Bien peu la réussissaient correctement, sans y laisser tout ou partie de leur âme, de leur corps ou de leur vie. Pour l'heure, le jeune homme ramait avec application, tentant de jeter un coup d'œil à la dérobée, afin d'estimer la distance demeurant à parcourir. Interceptant son regard, Ahmed le dévisagea un instant. Le vieux soldat lui sourit avec tout ce qui lui restait de dents et leva les sourcils, l'air faussement amusé.

« Tu as grande chance pour ton premier assaut, c'est nef de voyageurs. Ils sont moins prêts à en découdre que les marchands qui tiennent à leurs ballots. Nous allons moissonner comme gais laboureurs... »

Amusé par l'idée, le jeune homme dévoila à son tour des dents, régulières celles-là, mais une sourde appréhension demeurait dans ses prunelles. Craignait-il la blessure, la mort ou la rencontre avec lui-même ? Il ne le savait guère.

### Cale du Peregrina, vers midi

Asceline et Phelipote, l'épouse de Nirart, se serraient l'une contre l'autre, appuyées contre une des cloisons de la cale. Les ténèbres avaient envahi la zone, combattues par une poignée de lampes qui n'apportaient guère de lumière, et aucune chaleur. Le tumulte qui grondait au-dessus d'elles empêchait d'entendre les pleurs, les gémissements de celles et ceux qui, comme elles, étaient condamnés à attendre.

Les chocs résonnant contre le plafond de chêne faisaient sursauter les petits groupes, frissonner les plus vaillants et arrachait un sanglot de désespoir aux plus inquiets. Le cliquetis des armes était encore plus sinistre, tout comme les cris, en langue familière ou inconnue, et les râles porteurs de sourde angoisse. Asceline se tourna vers son amie et le garçonnet qui se trouvaient à côté d'elle, les incitant à répéter la prière à la Vierge, généralement prompte à secourir les nécessiteux. Mais le grondement déchirant qui précéda un fracas retentissant au-dessus de leurs têtes, résonnant dans tous les bois du navire, les fit ânonner toujours moins fort, à demi implorants, ajoutant leurs larmes à l'offrande gémissante de leurs voix.

Une des femmes se leva soudain, hurlant comme une démente à l'intention du plafond :

« Que Dieu et tous les Saints fassent mettre à glaive tous ces maudits ! »

Puis, prenant à témoin la masse humaine agglutinée dans un recoin empli d'ombre, elle vociféra :

« Christ en soit témoin, pas un de ces souillards ne posera sa main sur moi, plutôt me navrer au parmi du cœur ! »

Ajoutant le geste à la phrase, elle brandissait un petit couteau, certainement destiné à la cuisine.

« Mais il leur faudra m'approcher d'abord et je jure sur les Saintes Évangiles que je jouterai bataille avant cela ! J'en escouillerai quelques poignées... »

Puis, comme hallucinée, elle déambula dans la cale, brinquebalée par les cahots du navire tandis qu'elle frappait d'imaginaires assaillants.

Au-dessus d'elle, toujours résonnait le vacarme des soldats, des matelots et des voyageurs bataillant sur le pont. Après avoir laissé échapper un gémissement, Phelipote trouva entre deux hoquets de pleurs la force de murmurer à Asceline :

« Allons-nous donc finir ainsi ?

--- Allons, bonne amie, garde ta foi en Christ. Il nous a menés ici, puisse-t-il nous sauvegarder encore... »

Sa compagne garda le silence un long moment, reniflant de temps à autre, les yeux noyés de larmes.

« Je crains pour mon époux. Le pauvre n'a jamais été bien vaillant. Le plus loyal des compagnons, de certes, mais guère habitué à...

--- Il n'est pas seul. Mon frère Amalric, mon époux ou Point-l'Asne sont à ses flancs. Aucun ne laissera l'autre en arrière. »

Phelipote hocha la tête lentement, incapable de parler, la gorge nouée. Au-dessus d'elles, le vacarme prospérait, bruits de corps s'écrasant, de pas affolés, précipités, de ferraille cliquetante, de bois se brisant. Par-dessus le tout se répandaient des cris... de terreur, de rage, de haine, de désespoir, de douleur, aucun ne manquait à l'appel. La crainte de reconnaître l'une ou l'autre de ces voix était la plus épouvantable pour les infortunés enfermés dans les tréfonds du navire.

### Pont du Peregrina, midi

Abrité par un amas de tonneaux désormais brisés et de cordages rompus, Giovanni se traînait sur le côté. De la main gauche, il se tenait le flanc, sans y avoir même jeté un regard, dans un ultime espoir de nier la réalité. Chaque mouvement lui extorquait un cri ou un gémissement de douleur.

Non loin, près du château arrière, un groupe de matelots et de pèlerins résistait, empêchant les musulmans d'avancer et de déloger les archers embusqués à l'étage. Une fois appuyé, il risqua un œil vers l'avant du navire. Plusieurs cadavres et moribonds jonchaient le sol, dont plusieurs assaillants, ce qui arracha un sourire sans joie au marin. Des combats isolés continuaient, avec fureur, empêtrés dans la grand-voile qui s'était abattue avec fracas lorsque les haubans en furent coupés. Le mât avait brisé un des plats-bords et gisait en partie coincé dans ce qui restait de la chaloupe, oscillant au gré des vagues dans lesquelles son extrémité reposait, emmêlé dans la toile.

Giovanni roula sur lui-même et inspira avant de se résoudre à examiner son ventre, levant la main pour la découvrir maculée de sang, de son sang. À la vue de ses entrailles béant au-dehors, il eut un haut-le-cœur et sa bouche s'emplit d'une saveur saline et métallique. Il manqua défaillir et, des yeux, chercha du réconfort vers ses compagnons, à quelques mètres de là. Ils tentaient de se maintenir à l'abri des tireurs ennemis, derrière leurs targes. Son regard croisa celui de Nirart, encore debout, son pavois devant lui, le visage décomposé. L'effroi que Giovanni surprit quand le pèlerin l'aperçut lui fit lâcher un soupir accablé qui se mua en grognement de rage.

À l'avant du navire, plusieurs groupes s'affrontaient autour de la terrasse. Ahmed était parmi eux, son épée à la main, tenant ferme son bouclier. Indécis, il frappait de temps à autre par-dessus la ligne, sans grand espoir d'atteindre les hommes qui s'étaient retranchés contre un des escaliers d'accès à l'étage. Les forces étant à peu près égales de part et d'autre, une sorte de statu quo s'était instauré depuis un moment, aucun d'eux n'osant prendre l'initiative d'un assaut. Les coups d'estoc menés à la lance étaient plus symboliques qu'efficaces, et personne n'avait encore tenté de forcer la situation.

Conscient qu'il n'était guère utile à cet endroit, Ahmed se tourna, cherchant du regard un affrontement où il pourrait employer sa lame. Il n'avait guère envie qu'on le traite ensuite de poltron. Nul ne saurait jamais la boule d'angoisse qui lui taraudait la poitrine, lui coupait le souffle, lui asséchait la bouche. Faisant chemin vers l'arrière parmi le pont empli de décombres, il enjambait les plis de la voile avec précaution, veillant à ne pas glisser dans les flaques de sang, les déjections immondes, les chairs arrachées. Il s'efforçait de ne pas prêter attention aux pénibles affrontements alentour, avançait courbé pour ne pas s'offrir à d'éventuels tirs ennemis. Agenouillé, il guettait le moment où s'élancer vers le mât principal, où il serait de nouveau à l'abri, quand il entendit un bruit sur sa droite, accompagné d'un mouvement furtif.

Brandissant son épée, il se tourna, l'air aussi déterminé et résolu que possible. Mais il ne découvrit qu'un moribond rampant, dans une mare de sang mêlé d'eau de mer, une jambe à demi tranchée et des tronçons de flèches dépassant de ses flancs. Dans un sursaut bien inutile, il tentait d'avancer. Vers quelle destination se demanda le jeune musulman. Le regard était aveugle et la bouche emplie de caillots ne pouvait plus parler. Ahmed réprima avec peine un frisson de terreur. Lorsqu'il se tourna, l'homme gisait affalé, la vie s'échappant de son corps désormais à gros bouillons.

Ahmed finit par rejoindre ses compagnons, qui progressaient peu à peu vers l'arrière, protégés de leurs épaisses targes. Ja'mal était parmi eux, indiquant d'une voix ferme mais discrète ce qu'il convenait de faire. Il fallait à tout prix demeurer unis, avancer très doucement, n'offrir aucune cible au dernier archer ennemi. Une fois qu'il serait tombé, ils pourraient s'élancer sur le groupe de Francs. Ja'mal dévisagea Ahmed et lui fit signe de se baisser et de les rejoindre. Là il donna ses ordres, le visage fermé, les traits inquiets.

« Dès que nous n'aurons plus à craindre les traits ennemis, je crierai 'Allah est grand'. Alors, vous avancerez comme les démons que vous êtes et bousculerez les idolâtres de vos boucliers. Ceux armés d'une lance doivent aider mais ceux qui n'ont ni l'un ni l'autre doivent demeurer en arrière. Est-ce bien compris ? »

N'obtenant qu'un murmure d'approbation, il s'empourpra de colère et gifla Ahmed, qui était désormais le plus près.

« Je n'ai rien entendu ! »

Choqués par le geste, les hommes rugirent avec plus de détermination, ce qui sembla satisfaire le vieux soldat, le visage barré d'une grimace hideuse. Lorsqu'après un long moment, le cri « Allah est grand » retentit, la pression accumulée jusque-là explosa en une fureur haineuse. La ligne de boucliers adverses fut enfoncée avec vigueur, les combattants tombèrent, enchevêtrés, mêlés. On bousculait plus qu'on ne se battait, les armes souvent entravées, l'acier fouaillant les corps sans discernement.

Ahmed s'était élancé à son tour, levant son épée dans un geste de défi, hurlant comme un animal tout l'effroi et la colère qui l'avaient envahi. Mais il ne trouva aucune chair à trancher, aucun ennemi à affronter. Nul endroit où sa lame pourrait s'abattre : assortiment de coups ahanant, d'empoignades féroces, de pugilats tourmentés. Affolé, il cherchait en tout sens l'éventuel adversaire sournoisement tapi à portée de bras. Sentant sur sa nuque le regard attentif du vieux Ja'mal, il savait qu'en ce jour, on le jugerait à la hauteur de ses actes.

Bondissant sur un amas de débris, il débusqua un Franc oublié par ses compagnons. L'homme haletait, hagard, larmoyant, les mains emplies de ses propres entrailles, incapable de réaliser ce qui lui arrivait. De sa bouche s'écoulait plus de sang que de salive, maculant son torse et se répandant sur son ventre béant. Ses yeux hallucinés, exorbités exprimaient toute la terreur qui ne pouvait trouver son chemin au travers de la gorge gargouillante. Lentement, il leva la tête vers le soldat ennemi, la lèvre pendante tel un dément.

Dans un souffle la lame s'abattit rageusement, couvrant d'un ultime voile le regard du jeune homme. ❧

### Notes

Malgré les fréquentes trêves et les relations diplomatiques, la situation de guère quasi-permanente en Terre sainte et dans les régions environnantes offrait de nombreuses occasions à des guerriers déterminés d'acquérir biens et pouvoirs. Aussi, lorsque le pouvoir égyptien conclut un accord avec le royaume de Jérusalem quelques années après qu'il eût perdu la ville d'Ascalon, de nombreux émirs ne l'entendirent pas ainsi et lancèrent des raids navals sur les côtes levantines.

Les pèlerins n'étaient pas les seules cibles, et bien souvent les opérations militaires n'étaient envisagées qu'en fonction du gain qu'elles pouvaient générer. Malgré tout, la valeur des esclaves était suffisante pour que la capture d'un navire de croyants adverses permette de s'enrichir tout en accomplissant des hauts faits pour sa propre religion.

### Références

Elisséef Nikita, *Nūr ad-Dīn Un grand prince musulman de Syrie au temps des Croisades*, Damas : Institut Français de Damas, 1967.

Gravelle Yves, *Le Problème Des Prisonniers De Guerre Pendant Les Croisades Orientales (1095-1192)*, Mémoire de Maîtrise, Université de Sherbrooke, 1999.

Prawer Joshua, *Histoire du Royaume latin de Jérusalem*, 2nde édition, Paris : CNRS éditions, 2001,2007.

Là-bas
------

### Le Monteil, mercredi 4 novembre 1142

Le bruit des cognées résonnant sur les troncs emplissait le bosquet. Peu d'animaux avaient osé demeurer dans les environs tandis que les hommes dévoraient la forêt, élargissaient la clairière. L'air était sec, les visages rouges. Le sol craquait sous les pas lorsqu'un des bûcherons s'aventurait à l'ombre. Une poignée de cabanes rapidement édifiées abritait des artisans œuvrant le bois encore vert, transformant les billots en écuelles, les perches en manches, les grumes en poutres. Sous un ciel moutonneux, les bras s'affairaient, réchauffaient les corps dont l'haleine s'échappait parfois en fumée.

Près d'un char robuste, attelé de deux bœufs au poil roux et aux longues cornes en lyre, un petit groupe s'employait à entasser un chargement de rondins.

« Hardi compaings, voilà bonne chauffe que celle de ce bois. La première à l'abattre et le trancher, et une autre à le porter ! » lança la voix joviale d'un jeune homme à l'allure soignée.

Bien qu'il ne soit revêtu que d'une tenue de laine épaisse, grossièrement teintée et plusieurs fois reprisée, il se dégageait de sa personne une élégance innée. Sa chevelure bien coupée à l'écuelle et son visage avenant tranchaient avec les rudes faciès des valets qu'il dirigeait. L'un d'eux possédait d'ailleurs un nez qui tenait plus du champignon violacé, pas tant en raison de la froidure que des larges rasades de vin qu'il s'accordait avec générosité. Ses joues flasques et sa barbe miteuse n'annonçaient que superficiellement la crasse qui le caractérisait, compagne d'effluves dont on disait dans tout le Monteil qu'elles auraient fait reculer plus d'un blaireau. Néanmoins Peyre était un homme apprécié, vacher reconnu et roulier expérimenté. Sa réputation moqueuse était aussi connue que son odeur corporelle et son rire enthousiaste résonnait souvent parmi les cols tandis qu'il menait son attelage.

« Pour ma part, je préfère bonne vinasse qui échauffe le sang par en dedans ! La meilleure des flambées est celle qui me compaigne en ma gourde.

--- Soit assuré que le père aura tiré quelques pichets de la réserve quand nous arriverons, Peyre. La mère aura aussi sorti provendes pour rassasier nos bouches.

--- Je n'ai guère frayeur à ce sujet, Ucs. La tienne maisonnée est bien pourvue et n'hésite guère à ouvrir sa tablée. J'ai souvenance de plusieurs veillées à chauffer ma panse en votre ostel après bonne chère !

--- Nous n'avons pas à nous plaindre, l'année a été bonne en notre domaine. Père a pleines réserves de toisons encore à faire filer. Nous avons de quoi faire œuvrer toutes les femmes de la vallée, je crois bien ! »

Le vieux bouvier sourit, flattant ses bêtes tandis que les hommes finissaient de charger quelques épais rondins et plusieurs souches noueuses. Une fois le tout correctement arrimé, Ucs s'avança vers les valets.

« Je vais vous laisser mener l'équipage jusqu'à l'ostel, j'aimerais surveiller ce que font les autres œuvriers. »

Ses compagnons sourirent d'un air entendu, ramassèrent leurs sacs et paniers et prirent le sentier du retour, s'épongeant le front de leurs manches, bavardant entre eux à propos de la foire à venir en la vallée voisine, auprès du château des comtours[^50] d'Apchon.

Ucs les suivit du regard un petit moment, jusqu'à ce qu'ils aient bifurqué dans une laie[^51] dissimulée parmi les broussailles. Puis il emprunta à son tour le chemin du bois, se faufilant avec célérité entre les branches en direction de la berge de la Santoire. Il progressa ensuite vers l'aval, s'efforçant de ne pas se faire remarquer lorsqu'il s'approcha du gué, aux abords du hameau.

Plusieurs femmes étaient là à discuter, leur panier de linge à la main, les bras rougis par l'eau vive du torrent qui descendait du col de Cabre. Un pêcheur était occupé à relever ses nasses. Bondissant plus que marchant, Ucs traversa la Santoire avec légèreté, familier des pierres que chaque année les villageois remettaient en place après les crues du printemps. Quelques frênes s'accrochaient sur les pentes menant au petit hameau ramassé aux abords de la vieille tour qui veillait cette zone de la vallée. De taille dérisoire, ancienne et peu glorieuse construction, elle proclamait néanmoins la puissance de son propriétaire, seigneur du lieu, même si sa richesse n'était que toute relative, à proportion du modeste amas de cabanes pelotonnées contre les palissades de bois.

Tandis qu'il progressait, le souffle laborieux, il tendait l'oreille. Il se dirigeait vers une clairière légèrement pentue orientée vers le soleil, abritée des vents et un peu à l'écart des habitations. Là ne résonnaient aucune cognée ni aucun cri de travailleurs, mais les voix légères et enjouées de jeunes filles.

Aussi discret qu'un traqueur à l'affût, Ucs s'approcha et se fit plus attentif encore, espérant entendre une personne en particulier. Il n'eut pas longtemps à patienter et s'annonça alors plus franchement, un sourire charmant sur le visage. Les jouvencelles, occupées à coudre et broder, confortablement installées sur d'épaisses couvertures, s'esclaffèrent de le voir s'avancer vers elles. D'un geste de la main, d'un signe de tête ou juste d'un regard, toutes le saluèrent et l'une d'elles se leva, l'air faussement embarrassé et les joues en feu.

Vêtue d'une robe simple, mais d'une étoffe de qualité teinte d'un ton fauve mettant en valeur ses longs cheveux clairs nattés, elle souriait largement, creusant des fossettes sur son visage rond. Elle lui prit la main lorsqu'elle le rejoignit et chuchota, tout en l'entraînant à l'écart :

« Ucs ! Que voilà belle surprise ! Je n'espérais plus guère, depuis tous ces jours.

--- Nous avons grand ouvrage à abattre à l'approche de l'hivernage, Ysane. Je ne peux m'éloigner pour long, car les valets prendraient vite leurs aises. »

Souriant à la jeune fille, Ucs lui vola un rapide baiser, qu'elle rendit, les yeux emplis de désir.

« Le temps m'a paru si long depuis ce dimanche. J'avais désir de t'enlacer, et de pouvoir baiser ces lèvres bienaimées. »

Le jeune homme hocha la tête et la serra contre lui, effleurant son dos à travers les nombreuses et épaisses couches de vêtements. Leurs souffles emmêlés se firent plus haletants, leurs gestes plus empressés et durant un moment plus rien n'exista pour chacun d'eux que ce corps avide de caresses pressé contre le leur, cette bouche insatiable. Lorsqu'ils s'écartèrent un peu, les yeux noyés dans le regard de l'autre, il leur fallut un peu de temps pour réaliser que des voix masculines s'étaient mêlées à celles des jeunes filles de la clairière. Ysane en reconnut une en particulier, emplie de colère et d'agacement. Elle repoussa Ucs doucement et lui sourit avant de chuchoter.

« Je ferai mieux de retrouver mes amies, Ucs. Mon frère est là.

--- Il m'enfriçonne guère. Je lui ai déjà fait ravaler sa morgue.

--- Il ne t'en déteste que plus fort. Il se pensait redoutable lutteur et tu l'as mis au sol si aisément !

--- Il confond force et adresse, ton frère ! Ce n'est qu'un benêt empli de vent. »

Ysane soupira, l'air soudain affligé.

« De certes, mais il a l'oreille de père et je sais qu'ils me prévoient des épousailles avec des cousins du sire de Mercoeur... Il a peur de ce que nous pourrions faire avant cela. »

Elle eut un petit rire espiègle aux pensées qui la traversèrent. Caressant la joue de Ucs, elle s'éloigna lentement, un sourire tendre illuminant ses traits. Revenu à lui, le jeune homme ressentit soudain le froid, entendit le cri d'un milan au-dessus de la vallée. Il se frotta le nez, passa sa main sur la joue qu'elle avait effleurée. Puis il reprit le sentier qui menait vers la Santoire.

### Église de Dienne, dimanche 2 juillet 1144

Un vent léger balayait la vallée, faisant trembler les frênes du bosquet aux abords de l'église. Ucs était appuyé contre la porte de l'édifice et regardait d'un air absent le ciel où quelques massifs nuages immaculés glissaient lentement. De temps à autre, il reportait son attention sur le petit groupe de moineaux qui sautillaient aux alentours. Ce n'était que bien rarement qu'il se penchait vers l'intérieur du lieu sacré pour y suivre ce qui s'y passait. La plupart des villageois assemblés là n'étaient d'ailleurs guère plus assidus. Certains allaient et venaient en discutant, d'autres étaient assis, occupés à marchander avec un colporteur le contenu de sa balle.

Seule une poignée était attentive, massée auprès du clerc qui officiait à l'entrée de l'abside, leur tournant le dos. Parmi ceux-ci, Léon de Diane[^52], le puissant seigneur du lieu, se remarquait aisément à sa riche tenue de soie colorée, son port altier. Sa bedaine, imposante malgré sa jeunesse, trahissait son goût pour la bonne chère. Pour l'instant, il était en grande discussion avec son épouse, petite femme au visage guère attrayant, mais dont la chaleur et l'amabilité étaient renommées dans toute la vallée. Ucs était là pour voir si des voyageurs seraient intéressés par un guide pour franchir les plateaux, les cols.

Depuis plusieurs années il louait ses bras, ses jambes et sa connaissance des montagnes en accompagnant des marchands ou des étrangers fortunés par les chemins. Sa réputation était flatteuse et on louait à mots couverts sa vivacité à prendre le parti de ceux qui le payaient.

Récemment encore, il s'était violemment opposé à un sergent à l'entrée de Saint-Flour parce qu'il demandait un péage exorbitant à ses clients. La discussion avait dégénéré et les lames auraient vite été sorties si des paysans des alentours ne s'étaient interposés. Beaucoup de villageois du Monteil étaient d'ailleurs soulagés de savoir qu'Ucs avait trouvé de nouveaux endroits où laisser s'exprimer ses poings. Sa réputation de bagarreur n'était plus à faire. Et si on reconnaissait qu'il n'était que rarement à l'origine des altercations, et presque jamais le premier à frapper, beaucoup estimaient qu'il était un peu trop souvent impliqué dans de tels affrontements. Il semblait même y prendre goût, habitué à finir encore debout, et parfois éclaboussé d'un sang qui n'était pas le sien. En outre, il arborait fièrement à sa hanche une longue épée d'acier. Personne ne savait s'il était vraiment efficace avec, mais peu se seraient aventurés à le lui demander, craignant d'en subir la démonstration.

Des éclats de voix jaillis de la nef l'arrachèrent à sa rêverie. L'assemblée commençait à bruisser d'une bousculade. Il faut dire que la messe était l'occasion de parler affaires et plusieurs des paroissiens étaient venus avec des animaux, une poule pour certains, mais carrément un mouton pour d'autres. C'était autour d'une de ces bêtes que l'empoignade semblait s'envenimer.

« Fils à moine, tu voix bien qu'il a patte fol, cet ouaille !

--- Miladiu[^53]! Il était bien vif quand je te l'ai cédé.

--- Tu l'avais celé au parmi du troupeau, que je ne le voie pas, sale bougre de rat vérolé... »

Les invectives ne semblant plus suffire aux deux compères, ils en étaient venus à se secouer l'un l'autre, attrapant la cotte élimée de leur adversaire d'une poigne mal assurée. L'officiant avait dû cesser la messe, cherchant à ramener le calme, sans aucun résultat.

Le seigneur de Diane, lui, assistait à la scène non sans une lueur joyeuse dans le regard. Les querelles n'étaient pas chose rare, et il savait qu'en cas de blessure, on aurait recours à son jugement. Et on le disait toujours prêt à lever une amende, à se faire payer son droit de justice. Pour l'heure, il semblait surtout amusé par les insultes salaces que les deux hommes se lançaient au visage, incapables de faire plus que se tirer et se pousser, le malheureux mouton affolé coincé entre eux deux.

Lorsqu'un couteau fut libéré de sa gaine, des cris de frayeur accompagnèrent le mouvement de reflux. Visiblement, l'un des deux était prêt à faire violence pour voir ses arguments triompher. Le prêtre avait beau agiter les bras comme un épouvantail, personne ne lui prêtait attention. Les deux paysans avaient de vieux contentieux à régler, héritiers de litiges anciens, aussi graves et sérieux que des abus de droits de pacage ou du bois traitreusement ramassé. Le sang allait parler et chacun suivait la scène, fasciné.

Mais le dénouement ne fut pas celui attendu, sinon espéré, par la foule. En quelques pas, Ucs était sur l'homme au couteau et le lui avait fait lâcher d'un coup sec sur le bras. Ne laissant pas à son adversaire le temps de réagir, il chassa la lame du pied et s'interposa, le regard sévère.

« Qu'as-tu en tête, le Peyre ? Saigner Geraud en pleine messe pour un vieux mouton mité ! »

Décontenancés, les deux bagarreurs échangeaient des regards circonspects, hésitant à passer outre le vigoureux arbitre qui s'interposait. On les frustrait d'une bonne empoignade, et cela les contrariait visiblement. Mais ils n'étaient pas prêts pour autant à se frotter à celui dont on disait qu'il ne craignait pas de tenir tête à un chevalier en armes.

« C'est pas ton affaire, Ucs. Il m'a robé, il doit rembourser.

--- Face d'étron ! T'as qu'à mieux soigner tes bêtes. Elle était saine quand je te l'ai changée pour ta terre...

--- La Paix ! tonna Ucs. Je me demande si cette pauvre bête n'a pas plus de cervelle que vous deux réunis. Il y a d'autres lieux et d'autres moments pour s'entrebattre ! Si vous n'arrivez à vous entendre, allez donc visiter un prud'homme. Il saura démêler le bon grain de vos criements. »

Le calme revint soudainement dans l'église. Brièvement on entendit au dehors des aboiements de chiens, le bêlement des troupeaux proches, le cri d'un rapace qui surveillait son territoire. Les deux paysans se jaugèrent un instant et s'écartèrent, l'air mauvais. Ils n'étaient pas forcément convaincus, mais s'accordèrent d'un regard pour liquider leur différend un autre jour. Maussade, Peyre ramassa son canif et s'éloigna avec son mouton, suivi de sa femme et de ses enfants.

Le prêtre toussa pour attirer l'attention sur lui et se prépara à terminer la cérémonie, sans que personne n'y prête le moindre intérêt. Tout le monde était tourné vers le jeune homme, qu'avait rejoint Léon de Diane en quelques enjambées. Souriant à Ucs, il le jaugea de la tête aux pieds en un rapide coup d'œil, s'attardant quelques instants sur le fourreau qui pendait crânement à sa taille.

« Tu es bien téméraire, l'ami. Quel est ton nom ?

--- Je suis Ucs, du Monthel, fils d'Aymon.

--- Le jeune Ucs ? Dont on parle tant durant les veillées ? Je m'enjoie de te voir ennemi de la bagarre. »

Ucs sourit d'un air bravache, tentant de se montrer plus assuré qu'il n'était face à un si puissant seigneur et répondit :

« J'ai appris à préférer le calme à la tourmente. Nul ne gagne à chercher noise à son voisin, et je regrette d'y avoir eu ma part en mes jouvences.

--- Parole frappée au sceau de la vérité... »

Léon de Diane se gratta le menton un petit moment, un sourire mystérieux planant sur ses lèvres.

« J'ai toujours emploi d'hommes tels que toi, Ucs du Monthel. Si tu sais te montrer fidèle à ton seigneur, tu récolteras bonne moisson à mon service. Viens donc au castel un de ces jours, nous parlerons de cela. »

Le visage rayonnant, Ucs inclina la tête en acceptation, et salua par la même occasion. Il avait toujours rêvé de s'affranchir de sa condition de fils de riche paysan. Malgré tous ses biens, il n'avait pu prétendre à la jeune femme de ses pensées quelques mois plus tôt.

Sa vocation pour les armes était en partie née de son désir de devenir quelqu'un de valeur, qui n'aurait plus à subir la morgue des puissants, des seigneurs titrés. Mais il n'aurait jamais espéré qu'un homme aussi important que le comtour de Diane lui fasse l'honneur de l'inviter à se mettre à son service devant toute la paroisse assemblée. Alors que le noble rejoignait sa femme pour assister à la fin de la cérémonie, Ucs toisait les villageoises et villageois réunis là, le regard envieux, jaloux, parfois admiratif, voire craintif. Il sentait qu'ils le dévisageaient comme s'il n'était plus des leurs. Et cela le réjouissait.

### Le Monteil, jeudi 19 décembre 1146

L'écir[^54] qui balayait la vallée, soulevait la neige légère et l'amassait en épaisses congères contre les haies, les maisons, les troncs des arbres. Impitoyable, le vent cinglait le visage des voyageurs attardés. Levant vers le nord un nez rougi par le froid, Ucs toussa, retenant in extremis sa capuche qui allait être emportée par une rafale. Il venait d'installer sa monture dans l'étable familiale et n'avait que quelques pas à faire pour rejoindre le chaud foyer où les siens se tenaient. Comme à chaque hiver, une fois les volets colmatés de paille, ils attendraient patiemment que le temps s'améliore au coin du feu, échangeant les nouvelles avec des voisins, cousant, filant, fabricant des paniers, réparant des outils ou profitant juste d'un peu de bon temps.

Lorsqu'il poussa la porte après avoir frappé, toutes les têtes se tournèrent dans sa direction, inquiètes de voir ce voyageur bravant les intempéries qui osait entrer sans y avoir été invité. La chaleur le happa, en même temps que l'odeur forte, mélange de fumée, de sueur, de poussière et de potages mijotés. Il lui fallut quelques instants pour discerner les formes qui se tenaient là dans la pénombre, chichement éclairées par les flammes léchant les bûches.

Son père était déjà debout, le visage empli de joie. Ils étaient tous installés autour de l'âtre central, se chauffant les pieds, cherchant la clarté dans la chaumière barricadée contre les éléments. Ils plissaient les yeux, aveuglés par la vive lumière venant du dehors, accompagnée de vent froid et de quelques flocons aventureux.

« Le bonjour à tous, les parents ! » lança gaiement Ucs tandis qu'il battait un peu des pieds avant d'entrer et de refermer la porte derrière lui.

Les visages s'illuminèrent et, en un instant, le voyageur était entouré de ses jeunes frères et sœurs, invité à s'asseoir par sa mère, serré contre son cœur par son père.

« Que voilà bien folle idée d'aller par les chemins en ce temps, Ucs ! Le sire Léon est bien dur avec toi !

--- Nous avions tous grand désir d'être de retour pour la Noël. Et le chemin n'est pas si mauvais plus au nord. Nous n'avons quasi pas vu de neige avant le Limon. »

Son père dodelina de la tête, peu convaincu.

« Tu ne m'ôteras pas de la tête que le Bon Dieu n'a pas voulu que le voyageur s'aventure au-dehors en pleine froidure.

--- La paix, Aymon, le fils s'en revient à peine et tu le sermonnes déjà ! »

L'épouse embrassa son fils et l'aida à se dévêtir de son épaisse chape de voyage.

« As-tu appétit pour un fond de brouet ? Histoire de te chauffer par en dedans ? »

Sans même se soucier d'une réponse, elle s'élança pour touiller dans le pot qui mijotait au coin de l'âtre, indiquant d'un geste impérieux à un de ses enfants plus jeunes d'aller chercher une écuelle.

Ucs souriait, heureux de se retrouver parmi les siens. Il s'assit sur un tabouret et ôta ses chaussures mouillées, se frictionna les pieds. Son père avait relancé le feu, y ajoutant une belle bûche pour que les flammes donnent plus de clarté. Chacun avait repris sa place et attendait désormais qu'il leur conte ses dernières aventures, ou au moins rapporte les nouvelles les plus importantes du monde.

À chaque retour, le rituel était immuable : il savourait d'abord un rapide en-cas, ménageant l'intérêt de ses proches pour le récit qu'il allait faire. Ses yeux couraient de l'un à l'autre : ses deux petits frères, adolescents, installés sur un banc contre le mur d'entrée, façonnaient le bois pour en faire des cuillers, des manches. Ses trois sœurs, plus jeunes, assises à même le sol devant le feu parmi leurs poupées et leurs jouets lui lançaient des regards envieux. Sa mère, à droite du foyer, près du lit, reprisait des chausses d'un geste tranquille après lui avoir servi de la soupe fumante, avec une épaisse tranche de pain noir.

Derrière l'âtre, entouré de débris d'osiers, de branches entassées et de paniers en cours de fabrication, son père le dévisageait, les mains allant et venant d'elles-mêmes dans les entrelacs de tiges souples. À l'approche de Noël, les valets s'en étaient retournés dans leur parentèle et il ne restait donc que la famille dans l'ostal. Ce serait la première fois que Bertrande, l'ancêtre, ne leur conterait pas ses histoires à la veillée.

Grand-mère de son père, elle avait toujours égayé les soirées par ses récits. Ucs n'avait appris son décès qu'à son retour l'été dernier. Une fièvre de printemps avait emporté la vieille dame et il avait dû se contenter d'aller saluer la tombe fraîchement refermée aux abords de l'église. Il était néanmoins heureux de pouvoir passer une nouvelle fois les fêtes avec les siens, d'autant qu'il ne savait jamais ce que l'avenir lui réservait.

Le comtour de Diane, Léon, était un seigneur important et se comportait comme tel. Il visitait souvent ses proches, en particulier la puissante famille de Mercœur. Ucs avait aussi pris l'habitude de résider en une propriété ou l'autre de l'évêque de Clermont. Et, présentement, il revenait d'une assemblée du comte d'Auvergne, Robert[^55].

Ragaillardi par le potage, Ucs fouilla dans ses affaires. Il vérifia que sa courte cotte de mailles n'avait pas trop souffert de la neige, solidement roulée dans un sac de peau huilée pour l'empêcher de rouiller. Il n'était pas peu fier de ce vêtement, qui complétait son casque de fer, lui aussi emmailloté dans une poche graissée. Sa valeur à elle seule lui aurait permis de s'acheter de nombreuses bêtes, mais elle n'était pas vraiment sienne, confiée à lui par le seigneur qu'il servait. Il la portait avec orgueil, assuré de faire son effet auprès des jeunes filles et attirant les regards envieux des hommes condamnés à pousser la charrue, à mener le bœuf.

Il sortit une petite bourse et fit un signe de tête à son père, lui montrant qu'il avait l'intention, comme à son habitude, de lui donner une part de ses gages. Il avait encore touché une belle somme récemment, et partageait chaque fois avec sa famille. Leur prospérité à tous était déjà reconnue au village, mais depuis qu'il s'était fait sergent d'armes, elle n'en était que plus éclatante. Contrairement à la plupart des paysans, une partie de leur richesse était maintenant en monnaies sonnantes et trébuchantes, ce qui leur permettait d'acheter et de vendre comme des négociants de la ville.

Son père faisait désormais commerce de bêtes entre les vallées et prêtait parfois à des voisins impécunieux de quoi ensemencer leurs terrains, contre redevance rémunératrice. Ucs allait s'approcher pour leur parler à tous lorsque des coups légers résonnèrent sur la porte. Un des fils ouvrit et un garçon se faufila prestement à l'intérieur. C'était un des gamins des environs, le visage radieux, les joues rougies par sa course dans le froid.

« Le comtour Léon va à Jérusalem ! » cria-t-il d'une voix enthousiaste sans même saluer.

La stupeur fut générale à l'évocation de ce nom, à la fois si familier et si exotique : la ville au centre du Monde, là où les miracles du Seigneur étaient si fréquents. Ce fut le père d'Ucs qui réagit le premier.

« J'avais ouï parler que le roi de la France[^56] avait fait serment de s'y rendre. Il doit chercher à avoir grand ost. »

Tout en parlant, il se tourna vers son grand fils, le visage inquiet. Ucs hocha gravement la tête.

« Le seigneur comte de Clermont, Robert, ira avec le roi et le comtour Léon a également pris la croix.

--- Tu veux dire que... souffla sa mère, soudain blanche de frayeur.

--- Oui, la mère. Je vais prendre les chemins pour aller libérer le tombeau du Christ en la Sainte Cité. Je pars pour Jérusalem. » ❧

### Notes

La Haute-Auvergne du XIIe siècle était assez densément peuplée, au regard de la situation ultérieure. Les conditions climatiques favorables avaient permis l'implantation de nombreux villages sur les plateaux, en des lieux qui seraient désertés par la suite (avec la baisse des températures au XIVe siècle). De nombreuses familles nobles détenaient là des territoires assez importants, qui ne dépendaient pas du lien féodo-vassalique plus développé dans le monde septentrional. Beaucoup de nobles avaient hérité de leurs biens en alleux - en totale indépendance - (de même que certains paysans), et tenaient farouchement à leur statut, refusant de s'inféoder à d'autres.

Les relations qui s'instauraient entre les puissants et les petits propriétaires étaient donc bien différentes, plus complexes que celles qui unissaient le vassal et son seigneur au nord de la France.

### Références

Phalip Bruno, *Seigneurs et bâtisseurs. Le château et l'habitat seigneurial en Haute-Auvergne et Brivadois entre le XIe et le XVe siècle*, Clermont-Ferrand : Publication du l'Institut d'Études du Massif-Central, Collection prestige, n°III, 1993.

Ribier du Châtelet Jean-Baptiste de , *Dictionnaire statistique, ou Histoire, description et statistique du département du Cantal*, 5 volumes, Aurillac : 1852-1857

Sassier Yves, *Louis VII*, Paris : Fayard, 1991.

Cœur vaillant
-------------

### Bords de la Marne, environs de Meaux, mardi 17 avril 1151

La carriole tirée par un âne bringuebalait en grinçant, menée par l'adroite badine courbe d'un jeune valet à la cotte crasseuse. Le regard moins vif que celui de sa bête, il mâchonnait une graminée tout en observant les embarcations sur le cours d'eau qu'ils longeaient en direction du levant.

« Prends donc garde, ne vois-tu pas que tu mènes ma fille parmi mottes et terriers ? »

La voix s'efforçant de réveiller, sans grand effet, l'adolescent était celle d'un homme assez vieux pour être son père, voire son grand-père. Sa face était ravagée par le chagrin et sa barbe mal entetenue, les cernes qui creusaient son visage le faisaient paraître encore plus âgé. Sa tenue de laine indiquait l'aisance, mais pas la richesse. Une femme plus jeune, mais tout aussi épuisée que lui marchait, une main posée sur l'enfant allongé dans le petit chariot. Caché parmi de nombreux replis de couverture, emmitouflé jusqu'au bout du nez, on en discernait à peine les traits. Les yeux mi-clos, les joues rosées par la fraîcheur ambiante et la fièvre, il laissait s'échapper un râle lorsque le cahot était trop violent. La mère tourna un visage sévère vers le vieil homme, les lèvres pincées.

« C'est folie d'avoir voulu cheminer outre les murs de la cité, Sanson. Elle est trop faible pour cela, ses douleurs sont si fortes ! »

Son époux lui lança un regard horrifié.

« Crois-tu que je m'enjoie à voir ma cadette endurer semblables souffrances ? Si je la porte auprès de saint Fiacre, c'est parce que j'y ai espoir de guérison pour elle. »

La femme se renfrogna, mais hocha la tête en assentiment. Elle lâcha, comme à regret :

« Pardonne-moi, mon ami. Il m'est fort difficile de voir Libourc ainsi endolorie, elle si aimable enfançonne. »

Sanson se redressa, les yeux emplis de colère.

« Ne parle pas d'elle telle une moribonde. Nos prières la sauveront ! On dit saint Fiacre fort efficace. J'ai de quoi faire dire bon nombre de messes, et prévu chandelles de cire pour plusieurs jours. Jean Tonnel a sauvé ainsi son second fils d'une atteinte à la gorge, la fièvre qu'il avait contractée lors de la Noël.

--- Puissent tous les saints nous entendre ! »

Sanson toussa, transpirant sous son épaisse chape de laine malgré la fraicheur des températures. Il était complètement désemparé, prêt à tout pour sauver sa fille. Le front bas, la démarche hésitante, il avançait tel un animal mené au boucher, indifférent à tout ce qui n'était pas le souffle de son enfant souffreteux.

Les derniers lambeaux de brume finirent par se déchirer sous les assauts du soleil et la Marne se mit à scintiller de mille diamants. De nombreux oiseaux patrouillaient à ses abords, nageaient à sa surface. Des barques agiles s'y déplaçaient, ainsi que de pesantes barges couvertes de ballots, de tonneaux, de lourds matériaux. La vie continuait son cours, indifférente au drame des deux parents accompagnant leur enfant malade.

Lorsque la chaleur se fit plus forte, ils s'arrêtèrent près d'un manse[^57] fatigué. L'homme occupé à passer la houe dans son jardin offrit de leur donner un peu de brouet et interpella son épouse d'une voix autoritaire, la commandant d'une façon bien peu en accord avec sa charitable proposition. Il confirma qu'ils devaient bientôt quitter les bords du fleuve, longer un bois de belle taille à main gauche, pour arriver à l'église de saint Fiacre.

Plusieurs gamins couraient dans les environs et l'un d'eux, le nez sale et les genoux crottés, aidait à épierrer les abords du potager. Le paysan, bien que bourru, se voulut encourageant, attestant des nombreuses guérisons dont il avait pu être témoin le long de cette route. Avalant avec lenteur les cuillères de potage que lui présentait sa mère, la petite fille, d'une dizaine d'années, écoutait avec attention. Elle ne pouvait néanmoins se retenir de tourner les yeux vers les enfants batifolant aux alentours, brandissant des bâtons pour mener les poules de-ci de-là. Son regard brillant peinait à demeurer concentré et se perdait souvent dans le lointain.

« Ta couche n'est pas trop dure, Libourc ? », demanda Mahaut tout en surveillant le tassement de la paillasse.

La malade secoua la tête, esquissant un sourire timide.

« Le chemin est-il long jusqu'aux reliques ? »

Sa voix rauque, issue d'une gorge nouée ne semblait pas naturelle dans ce petit corps malingre.

« Je ne pense pas... répondit la mère d'une voix faussement assurée. Veux-tu que je te frictionne un peu ? Laisse-moi voir tes cataplasmes, s'ils n'ont pas glissé.

--- Je les sens encore, mère...

--- Je préférerais ne pas la découvrir ici, Mahaut. Il ne fait guère chaud, et la froidure est mauvaise pour ses membres a dit le frère infirmier. » l'interrompit Sanson.

La femme se renfrogna, vexée de la remarque et s'efforça de sourire à sa fille malgré sa mauvaise humeur.

« Si tes douleurs reviennent trop fortement, préviens-m'en, veux-tu ?

--- Oui mère, mais je me sens beaucoup mieux avec ces nouveaux emplâtres. »

Joignant le geste à la parole, la fillette remua un peu les membres supérieurs, mais fut vite trahie par quelques grimaces.

« Ne t'échauffe pas trop pour autant, mon enfant, cela n'aide pas à ta guérison. »

Pendant ce temps, son mari avait repris sa discussion avec le jardinier appuyé sur sa houe. Ce dernier était curieux de savoir d'où ils venaient, et ravi d'apprendre qu'ils étaient de Meaux. Il était persuadé que le fait d'être de la région incitait les saints locaux à être plus efficaces.

« Sauf vot' respect, je crois que c'est normal qu'y s'occupent de ceux qui les prient l'an durant. Venir quémander depuis la Provence à un saint qui nous connaît pas, je vois pas bien comment ça marcherait !

--- Tu ne crois pas à l'efficacité des marcheurs qui se rendent au loin ?

--- Je n'ai pas le latin d'un clerc, je l'avoue, mais moi je serais guérisseur, je m'occuperais de mes voisins avant de soigner des Picards, des Normands ou des Flamands.

--- Et le Christ ? »

La question ennuya l'homme, qui se frotta sa barbe naissante d'une main rugueuse.

« Ça, j'peux pas dire, le curé explique qu'il est partout, mais j'ai pas bien compris... Pour moi, y doit mieux voir ceux auprès de chez lui, comme tout le monde.

--- Jérusalem... souffla le vieil homme.

--- Oui, c'est ça, mais c'est pas pour nous, ça. Il marqua une pause, cherchant dans sa mémoire ce qu'il avait appris sur le lieu. C'est par-delà les mers, au centre du monde que mon vieux disait. Même que les rois et les barons en reviennent pas toujours. Y'a que le Grand Charles[^58] qui s'y rendait rapidement, pour combattre les païens. »

Sanson hochait la tête en accord, n'écoutant plus que d'une oreille. Son esprit vagabondait au loin, formulait des hypothèses, se nourrissait de nouveaux espoirs. Saluant l'homme d'un geste amical, il indiqua à sa petite troupe qu'ils reprenaient le chemin. Son valet, qui s'était allongé dans un fossé pour se reposer, indifférent à la rosée, vérifia les brancards du chariot puis d'un coup de trique sur la croupe du baudet, remit l'attelage en branle. Il n'avait toujours pas proféré une parole.

Ils croisèrent une poignée de colporteurs, le dos courbé sous leurs produits, leurs paniers. Des bergers faisaient paître les animaux sur les berges, nettoyant parfois l'accès à des plages où quelques barques gisaient. Une cavalcade de soldats les dépassa, imperméables à leur malheur, parlant d'une voix forte et braillant des chansons salaces. Leur voix mourait au loin lorsque Sanson, se rapprochant de son épouse, lui déclara avec autorité :

« De retour en notre hostel, nous demanderons licence à l'évêque de prendre la croix. »

Mahaut se raidit de stupeur et chercha ses mots un moment avant de répondre.

« Prendre la croix ? Nous n'avons pas...

--- Nos fils sont tous grands et bien mariés, et je peux vendre mon métier. Tu as les courtils que ton père t'a donnés, et moi j'ai toujours les droits de pâture et de fagotage que je peux vendre sur le Bois aux Prêles.

--- Mais nous ne pouvons...

--- Ma décision est prise, Mahaut. Si Libourc trouve guérison grâce à saint Fiacre, je veux qu'elle puisse en remercier le Seigneur en son tombeau. »

Tirant sur son voile pour le remettre en place, Mahaut fixa son enfant allongée parmi les étoffes et des larmes lui montèrent aux yeux. Elle regarda son époux avec tendresse, étonnante douceur surgie d'un visage généralement glaçant. Reniflant pour contenir son émotion, elle laissa s'échapper d'un filet de voix brisée :

« Amen. »

### Meaux, jeudi 16 juillet 1153

Les dernières gouttes de pluie attardées tombaient du toit et s'écrasaient dans les flaques creusées à la périphérie de la cour. Les nuages, plus clairs, semblaient vouloir enfin se dissoudre. Quelques mésanges et un merle lançaient leurs trilles depuis les maigres buissons du petit jardin enclos. Face à la porte grande ouverte, Sanson de Brie était occupé à tailler une mortaise, sous l'œil attentif d'un jeune apprenti. Un autre, légèrement plus âgé, vérifiait avec le valet la planéité d'une planche après rabotage. L'atelier était empli d'odeurs de sève, de tannin, et des copeaux voletaient devant la truffe indifférente d'un chien allongé à l'entrée. Donnant sur la rue, l'éventaire laissait se propager la clameur de la place de la cathédrale guère éloignée.

Quelques badauds discutaient non loin de là, riant et plaisantant. Un goret, sur une piste depuis l'égout central, hésita à pénétrer et, inquiété par le regard vindicatif du mâtin, opta pour une prudente retraite. Le porcher, occupé à parler météorologie avec des femmes revenant de la Marne avec leurs tas de linge sentant le savon, ne semblait guère soucieux de ce que son troupeau vaquait un peu partout.

Mahaut esquiva le groupe bloquant le passage et entra dans l'atelier, un panier empli de pains au bras, suivie d'une jeune fille le dos courbé sous une hotte débordant de légumes. D'un geste impérieux, elle envoya la servante dans la cuisine, de l'autre côté de l'arrière-cour et se dirigea elle-même vers son époux. Souriant, il se releva en débarrassant sa cotte élimée des copeaux et de la sciure. Elle posa une main douce sur son épaule, établissant un contact avant de parler.

« Les Martin te passent le salut.

--- La fièvre de leur petit est passée ?

--- Si fait. Il court de nouveau partout comme un cabri en pré. »

Le menuisier hocha la tête, satisfait.

« De mon côté, j'ai discuté avec le vieil Aymar, de Saint-Rémy. Il ne serait pas contre l'idée de nous acheter la vieille pâture.

--- Il aura l'argent dans l'année ?

--- De certes. Il espère belle moisson de ses terres et devrait aussi toucher quelques monnaies de marchands venus pour la foire, à l'échéance de la Toussaint. »

Mahaut opina, récapitulant dans sa tête tout ce qu'il leur restait à faire avant de pouvoir accomplir le voyage qu'ils escomptaient. Son époux l'interrompit dans ses pensées.

« J'ai aussi vu le chanoine, le petit jeune, de la famille du comte.

--- Le rouquin ? Guiraut ?

--- Voilà ! Il m'a indiqué que nous devrions aller le voir au palais de l'évêque. Il veut nous expliquer en détail les privilèges du pèlerin. Il n'est nul besoin d'être prêts à partir pour cela, m'a-t-il expliqué. Mais tous ceux qui feront vœu devront être là. »

Le vieil homme lança un regard à ses aides, dans l'atelier, qui continuaient leurs tâches sans prêter attention à la discussion. Le plus jeune taillait avec application des chevilles sous un auvent à l'arrière.

« Il me semble que nous devrions partir seulement tous les trois. Trouver valet pour si long voyage sera malaisé, sans compter les dépenses. Nous pouvons patienter que d'autres prennent la croix pour ne pas cheminer seuls. »

Il s'interrompit, surpris par un mouvement de tête de sa femme. Elle regardait au-dessus d'eux, en direction du raide escalier de bois qui menait à la pièce de vie à l'étage. Il comprit lorsqu'une voix légère demanda :

« Mère, j'ai fini de recoudre les chausses, je peux descendre ?

--- Soit, mais ne va pas dans la rue, demeure par ici ! »

N'en espérant plus, la jeune Libourc descendit l'escalier avec entrain, un sourire illuminant ses traits. Le visage rayonnant, elle traversa l'atelier, caressant le chien au passage et se dirigea droit vers l'appentis au fond de la cour, remise à bois dont l'ancien fenil servait de débarras.

L'échelle grinçait sous son faible poids et les planches gémirent lorsqu'elle rejoignit son royaume. En ouvrant le petit volet qui révéla les poussières voletant dans l'air, elle illumina le sombre local encombré de vieilles céramiques fendues, d'escabeaux fatigués et branlants, ainsi que d'une couverture utilisée par les valets quand ils dormaient là. En se penchant par l'étroit fenestron, elle réussit à apercevoir un coin de ciel bleu, ce qui la mit en joie. En fait elle espérait surtout entendre ce que disaient ses parents.

Ils préparaient leur départ, discutaient de toutes les formalités qu'ils avaient à faire, de la cérémonie qu'ils feraient avec l'évêque, qui leur octroierait officiellement le statut de pèlerin, de la façon dont ils allaient constituer leur pécule, comment les contrats des apprentis pouvaient être rompus. Mille petits détails rendant leur voyage plus lointain, moins immédiat pour la jeune fille.

Elle s'en réjouissait, car elle n'était pas encore prête à quitter la cité. Comme tous les enfants, elle se faisait une fête à l'idée de découvrir le monde, mais son frère aîné lui avait laissé entendre que le trajet serait long et difficile, qu'ils ne seraient pas arrivés avant des semaines, des mois. Elle avait fait la fanfaronne, mais n'était jamais allée plus loin que Saint-Denis, aux abords de la Seine et de Paris.

Désormais, elle se languissait, curieuse des aventures à venir, mais inquiète de tout quitter, en particulier le beau Clément. C'était le fils d'un des charrons en haut de la rue. Il avait deux ou trois ans de plus qu'elle et roulait des mécaniques lorsqu'elle lui souriait chaque dimanche à la messe. Ils s'étaient embrassés une fois, cachés derrière un des piliers de l'église, et elle en gardait un chaleureux souvenir. Arriverait-elle à se passer de lui pendant si longtemps ? Des éclats de voix joyeuses montèrent depuis l'entrée, dans lesquels elle reconnut ses amies Marion et Cateline. Elles l'aperçurent dans sa vigie et lui firent de grands signes en traversant la petite cour. Vérifiant que les parents de Libourc étaient occupés à autre chose, elles lui lancèrent avec entrain :

« Nous avons message pour toi, de la part du charron ! »

Rassérénée, elle oublia en un instant ses doutes et fit de la place autour d'elle pour recevoir ses amies. Pour l'heure, elle était toujours à Meaux, et il s'en faudrait d'un long moment avant qu'elle ne doive quitter sa maison.

### Route de Jaffa à Jérusalem, mardi 21 août 1156

Le petit feu crépitait, zébrant de jaune doré les visages des marcheurs avachis. Le piaffement des chevaux de leur escorte s'échappait du taillis où ils étaient attachés. Les broussailles frémissaient tandis que les animaux en arrachaient les feuilles. Une lune gibbeuse traçait à la craie les silhouettes qui déambulaient parmi le camp. Des voix à l'accent étrange animaient la nuit, ponctuant les rêves des plus fatigués des pèlerins, déjà en train de dormir.

Les visages étaient épuisés, après de longs mois sur les chemins, certains ayant fait le trajet depuis l'Europe sans prendre le bateau, si ce n'était pour traverser le détroit à Byzance. Ils régalaient leurs compagnons d'un temps de magnifiques descriptions d'églises merveilleuses, de territoires immenses. Mais souvent, leur récit se brisait sur les mille écueils sur lesquels les vies des leurs s'étaient échouées.

Il n'y avait pas un pèlerin qui n'ait connu son lot de souffrance, de maladie, de blessures ou de mort. On évoquait à mots couverts les terribles Turcs qui écumaient les montagnes au nord et dont les lames courbes moissonnaient les souffles, tandis que leurs traits sifflaient.

Mais pour l'heure, ils étaient tous fébriles, impatients d'apercevoir la cité de David, à seulement quelques jours de marche. Les regards étaient parfois enfiévrés, enchâssés dans des orbites creusées par les privations, la fatigue et la peur. Parmi eux, Sanson, Mahaut et Libourc ne dépareillaient guère. Leurs vêtements avachis, enduits de poussière, déchirés n'avaient plus leur bel éclat du jour de leur départ. Ils étaient pelotonnés les uns contre les autres, non pas tant en raison du froid, car la température était plutôt chaude en cette nuit d'été, mais par habitude.

Sanson, les yeux fermés, serrait contre lui son épouse, qui dormait paisiblement. Elle qui avait toujours tenu à se montrer selon son rang à Meaux, était étendue à même le sol, la coiffe tournée, la bouche entr'ouverte, ronflant doucement. Assise à côté d'eux, la jeune fille avait perdu son visage rond d'enfant, grandissant et s'épanouissant tandis qu'elle s'approchait de la Cité sainte. Ses longs cheveux bruns étaient gris de poussière et ses joues maculées. Elle avait tenté de se joindre aux chants de quelques dévots, mais ses lèvres crevassées demeuraient désormais closes. Épuisée, elle fixait le feu sans bouger, trop anéantie pour même fermer les paupières.

Son regard fut attiré par la blancheur d'une cape. Le fourreau battant sur ses chausses d'acier, un des frères du Temple qui les escortait s'avançait en devisant doucement avec un de ses sergents. Ce dernier était un Syrien, et ils échangeaient dans sa langue. Il portait un petit bouclier rond dans le dos, une épée droite suspendue à la ceinture, mais surtout un arc, à la façon des Orientaux, avec plusieurs courbures. Il arborait une belle barbe noire que parsemaient deux rivières argentées, de part et d'autre de sa bouche. Ici et là, des hommes pareillement habillés montaient la garde, tandis que d'autres s'amusaient autour de leur feu, apportant un peu de vie à leur morne campement.

Sentant qu'elle ne pouvait différer plus l'assouvissement d'un besoin naturel, Libourc se leva avec précaution pour ne réveiller personne et s'écarta pesamment des cercles de lumière en direction d'un taillis. Elle était consciente du danger qu'il y avait à s'éloigner, mille fois avertie par sa mère et ne tarda pas à trouver un endroit où s'accroupir pour se soulager.

Autour d'elle seul le bruissement des branches, des feuilles animait la nuit. Aucun hurlement de loup. Même les chevaux proches étaient à peine audibles. Frissonnant de ce calme, elle se hâta de s'en retourner vers la clairière. Elle venait à peine de quitter son bosquet qu'elle poussa un cri vite étouffé. Une des vigies du camp se tenait devant elle, le doigt sur la bouche. De son bras gauche, protégé d'un petit bouclier rond, il la fit passer derrière lui en silence. Il scrutait l'obscurité, cherchant à en percer les mystères, le visage crispé, le corps tendu.

Libourc n'osait plus respirer. Puis brutalement il lança avec rage un cri dans sa langue gutturale, s'avançant pour mettre Libourc dans son dos. Au même instant, la forêt prit vie, des branchages s'agitèrent, le sol trembla de la course de nombreux pieds. Des coups sourds furent échangés, l'acier tinta contre l'acier et les boucliers résonnèrent. Ils étaient attaqués !

Du camp, une trompe sonna à plusieurs reprises et tous les hommes de l'escorte se levèrent, faisant rempart de leurs corps devant les pèlerins paniqués. Au milieu d'eux, quelques longs manteaux de laine blanche virevoltaient, une croix sombre ornant leur poitrine.

Libourc s'était figée lorsque le garde l'avait mise de côté et elle tressaillait à chaque fois qu'elle sentait les assaillants frapper. Avec l'obscurité, parmi les branches, le combat était très désordonné, mais plusieurs fois elle aperçut l'éclat d'une lame, d'un fer qui cherchait à mordre dans les chairs de son protecteur. Elle n'osait plus bouger, pelotonnée contre le tronc d'un arbre, et finit par garder les yeux fermés.

L'assaut ne dura pas plus de quelques minutes, qui parurentt une éternité à Sanson et Mahaut car ils avaient découvert l'absence de Libourc. Lorsque le calme revint et que l'escorte commença à ranger les armes, ils interpellaient tout le monde d'une voix inquiète. Rapidement, ils aperçurent leur enfant à l'orée du bois, qui discutait avec un des chevaliers du Temple et un des soldats. Ils s'élancèrent, bouleversés. Mahaut allait tancer sa fille de la peur qu'elle leur avait causée, mais n'en eut pas le temps. Libourc se tourna vers elle et lui fit un sourire inquiet :

« Père, mère, je tiens à vous présenter quelqu'un. »

Elle désignait visiblement le soldat levantin devant elle. Entre eux deux, le templier, un homme au visage rude, ridé et fatigué malgré son jeune âge, souriait. Libourc continua à l'intention de ses parents.

« Ce brave mérite récompense pour sa bravoure, il m'a défendue comme un lion.

--- Que faisais-tu en dehors du camp ? » rétorqua sèchement sa mère, enflammée par l'inquiétude.

Sa fille haussa les épaules et ne répondit pas. Elle dévisagea Sanson, attendant qu'il réagisse. Le vieux menuisier fixa le sergent, puis le chevalier, et enfin Libourc.

« Comment s'appelle-t-il ? » demanda-t-il d'une voix brisée.

Ce fut le chevalier qui répondit.

« Jehan le Batié.

--- Tu as sauvé mon bien le plus précieux, Jehan, je ne sais comment te mercier. »

Le soldat fit un sourire mité qui révélait une dentition incomplète, ce qui lui donnait un air plus terrible encore.

« J'ai fille aussi. Belle comme tienne. »

Tout en répondant, il porta la main droite à son cœur. Du sang s'écoulait lentement du rapide bandage qu'il s'était fait, tel une moufle. Libourc regarda les gouttes sombres se former puis tomber, glissant sur la tenue, jusqu'à rejoindre une flaque à leurs pieds. C'était leur première nuit en Terre sainte. ❧

### Notes

Le voyage pour raison religieuse menait souvent dans les lieux saints les plus proches. Les sanctuaires locaux étaient très fréquentés, et le culte rendu au loin n'était pas forcément le plus fréquent, même si l'on connaissait les tombeaux les plus prestigieux.

Parmi ceux-ci, celui du Christ avait une place à part, évidemment. Et bien que la dévotion en fût populaire, aller par-delà les mers pour se recueillir là-bas n'était pas chose facile. Les autorités ecclésiastiques, du ressort desquelles dépendait l'obtention du statut officiel, protecteur, encourageaient de telles expéditions, mais il fallait encore que la motivation personnelle soit présente. Tout cela avait un coût, en terme financier évidemment, mais aussi en temps, sans compter bien sûr tous les risques physiques.

En outre, on voyageait généralement avec ses proches, avec des membres de sa communauté d'origine si on le pouvait. L'instinct grégaire regroupait les gens, perdus de façon identique en des terres où tout leur était étranger, selon leur ville, leur province, leur langue.

La condition féminine est une des grandes absentes des études scientifiques, en raison de l'inexistence relative des femmes dans les sources médiévales. Cela est d'autant plus vrai des personnes du commun, qui ne jouaient aucun rôle politique de premier plan ni n'assumaient une carrière religieuse ou savante. Lorsque l'on cherche en plus à en apprendre plus sur leurs jeunes années, les traces évanescentes deviennent carrément fantomatiques. Néanmoins, depuis quelques années, des historiens s'efforcent de rassembler les indices épars pour donner des pistes.

### Références

Graboïs Aryeh, *Le pèlerin occidental en Terre sainte au Moyen Âge*, De Boeck Univers, Paris & Bruxelles : 1998.

Schaus Margaret (éd.), *Women and Gender in Medieval Europe, an Encyclopedia*, Routledge, New York & Londres : 2006.

Charité bien ordonnée
---------------------

### Saint-Jean d'Acre, taverne d'Ayoul, mercredi 28 mai 1152

La petite salle était bondée, la faisant paraître encore plus étroite qu'elle n'était vraiment. Deux tables y étaient installées, mais on s'asseyait partout où on le pouvait : sur les marches descendant dans le lieu, sur des tonneaux remisés, sur les trop rares bancs. La lumière se déversait à flots par les portes grandes ouvertes, amenant avec elle l'odeur de la marée, du poisson et des embruns.

Les bouches s'emplissaient de vin, se cherchaient avec frénésie, lâchaient des rots ou lançaient des trilles égrillards. On se remuait, on se chahutait, on se bousculait avec entrain dans les ricanements, les beuglements, les cris énervés. L'humanité la plus bestiale, la plus jouisseuse, s'agitait, en soubresauts joyeux, en éclats de voix, de rires, de colère, nourrie de gnôle aigre et de plaisanteries salées.

Sur une des tables, débarassée pour l'occasion, un nain dansait de façon outrée, un pichet à la main. Ses courts membres se démenaient tandis qu'il bondissait, sautait, cabriolait comme un dément, ses mèches blondes collées sur le front, répandant la boisson autour de lui. De sa voix embrumée par l'alcool, mais au timbre juste, il hurlait ses vers comme semeur au printemps, récoltant hilarité salace et encouragements paillards.

> « Bienheureux enfançon, engendré tout chantant\
> Joyeux jusqu'à sa fin, à sa mort va claironnant.\
> Né fringant d'une fesse, on le baptisa vesse\
> On lui prédit grand ouvrage et belles prouesses... »

Tout en déclamant, il mimait avec vigueur, forçant le trait, grimaçant comme un démon. Son entrain attirait à lui tous les regards, éteints ou avinés, joyeux ou étonnés. Et lui caracolait d'autant plus, souriant aux femmes, clignant de l'œil aux hommes. Il connaissait son public et savait comment le flatter. Il était du métier. Il en vint au dernier couplet, qui devait briller d'un éclat particulier.

> « Il fait froncer sourcils, ahonter son parent\
> S'échappe alors en un timide chuintement\
> Sans se donner aucune chance de fort sonner\
> Car las, en nous quittant, nous laisse triste fumet. »

Se pinçant le nez avec dégout, il fit mine de chasser ce qui se serait échappé de ses fesses en direction du visage le plus proche. Cela souleva des rires frénétiques, d'autant que le destinataire soutenait qu'il avait été frappé par une bien horrible odeur. On l'encensa, on le congratula, et quelques verres lui furent adressés. Puis chacun revint à son pichet, à quelque chair à pétrir, à son histoire à conter. Le danseur s'effondra sur le banc, tout sourire, parmi ses compagnons.

« Je crois que tu ne saurais mieux conter aucune histoire, Mile...

--- Pour ça, j'ai du vécu ! » confirma le nain, jovial.

Le petit groupe autour de lui semblait moins saoul, moins fiévreux que les autres clients. Ils avaient avec eux leurs sacs, tous leurs biens serrés en de fort maigres bagages. L'un d'eux, un grand brun à la mine sévère qui n'aurait pas déparé chez des ermites rigoristes, avala un peu de vin et lança de sa voix sourde :

« On ferait mieux de quitter Saint-Jean, trop de dévots par ici. On n'amasse nulle cliquaille. Nos jongleries n'attirent guère.

--- Tu parles de vrai. On ne tiendra guère comme ça, à croquer du hareng et du vieux pain gris. »

Une petite brune aux yeux clairs, qui aurait pu être belle si la vie ne s'était pas acharnée contre elle, déclara d'une voix éteinte :

« Césarée. C'est bien Césarée ! Un port, mais moins empli de pèlerins. Des marchands, des marins, un peu de soldats...

--- Tu goûtes fort la sergenterie, hein ? » lui lança un de ses compagnons, narquois.

Elle ne répondit pas et plongea le visage dans son gobelet. Mile s'accouda, le menton dans ses mains potelées.

« Je ne suis jamais allé là-bas, on dit qu'il y a de périlleux marais aux abords.

--- Et alors ? On ne quittera pas la cité d'aucune façon ! On passera à Haifa, on y aura bonne provende, et de là on sera presque arrivés.

--- On voit bien que tu as grandes jambes, toi ! rétorqua Mile en secouant les siennes, le visage fendu d'un sourire cynique.

--- Tu demanderas à ta bonne amie Anceline de te prendre en ses bras en ce cas !

--- Ce n'est pas ma posture favorite avec elle » répliqua le petit homme, malicieux.

La brune sourit, de dépit autant que de plaisir. Elle était habituée à n'être évoquée que pour son corps et jamais pour son âme, qui avait pourtant grand-soif de compagnie.

« Il nous faudrait trouver sire à flatter, nous ne faisons pas bonne provende parmi les vilains, asséna le brun à l'allure sévère.

--- Promesse de seigneur n'est pas fief ! affirma Mile. Mais tu as raison, il nous faut bouger si nous voulons mieux manger.

--- Au pire, on peut se faire passer pèlerin.

--- Tous les frères hospitaliers d'ici à Babylone connaissent nos trognes, à force. Certains n'aiment guère nous voir aux abords.

--- Quelle idée aussi d'aller fouiller parmi besaces des dormeurs ! »

La réflexion conclut les échanges, et chacun but son vin par petites gorgées, indifférent au chaos environnant. Mile se passa la main sur le front, faisant claquer son gobelet sur le plateau après en avoir avalé la dernière goutte.

« Soit, partons pour Césarée. Mourir ici ou ailleurs, n'importe quelle terre fera bien l'affaire. »

Et il sourit avec chaleur à Anceline, qui ne le regardait pas.

### Jérusalem, Grand Hôpital de Saint-Jean, vendredi 30 octobre 1153

Le Paradis ? Une odeur de propre flattait ses narines, et sa joue frottait sur une toile douce et fine. Mile eut tout d'abord l'impression d'être sur un nuage. Il remua la langue dans sa bouche, un peu pâteuse. Il n'avait plus cet horrible goût acide, râpeux, mêlé de bile, de sang et de terre. Il hésitait encore à écarter les paupières.

Il n'entendait pas le chœur des anges, mais des voix calmes, des pas étouffés, des toux, quelques éternuements et même, un peu plus loin, des gémissements. Assurément pas le Paradis.

Il risqua un œil. La pièce était lumineuse. Il était couché sur le flanc, regardant de côté une vaste salle où s'alignaient des lits. Entre les rangées allaient et venaient des servants, quelques silhouettes de noir vêtues. Une odeur de potage l'environna. Il bougea les mains, sentit le drap, le poids d'une couverture sur lui. Il soupira. Face à lui, un vieillard dormait, une bulle de salive éclatant à une de ses commissures à chaque souffle.

La vision lui arracha un sourire. Il renifla, se frotta le nez. Aucun visage connu n'était aux abords. Il inspira et expira lentement plusieurs fois, profitant de la chaleur douillette de sa couche. Il allait fermer de nouveau les yeux lorsqu'une ombre vint se placer devant lui.

« Vous voilà éveillé ! Grâces à Dieu. »

Il leva le regard. Devant lui, un jeune homme au teint frais, habillé d'une longue robe de laine brute, souriait. Avenant, il n'en était pas moins repoussant de laideur, avec des yeux trop gros qui cherchaient à s'extraire de ses chairs molles, un nez sinueux qui ne savait où aller et explosait en une forme de tubercule. Le tout posé sur une mâchoire épaisse, sans menton, d'où jaillissaient des dents disposées en tout sens bien au-delà de lèvres invisibles. Mile lui sourit, gardant le silence.

« J'ai eu fort peur de vous perdre. À votre arrivée, nous n'avions guère espoir.

--- Je suis où ?

--- À l'hospital de Saint-Jean. Cela fait maintenant trois jours depuis votre découverte.

--- Trouvé ?

--- Oui. Vous étiez fort malade, dans le faubourg de la porte David. On vous a porté ici. »

Mile se redressa un peu, tirant les couvertures jusqu'à son cou.

« Mes compagnons ? »

Le moine secoua la tête.

« Nos valets vous ont mené.

--- Et mes compaings ?

--- Ils étaient bien mal en point, eux aussi. Vous êtes le seul en avoir réchappé. » répondit le frère d'une voix douce, emplie de compassion et de chaleur.

Mile sentit une boule se former dans sa poitrine, broyer son cœur, écraser ses entrailles.

« Tous morts ? »

Il repensa aux dernières semaines. Ils erraient depuis des jours, sans arriver à amasser de quoi suffisamment manger ou boire. Ils avaient quitté Jaffa où ils survivaient à grand-peine depuis des jours, se faisant passer pour des pèlerins, mais on les avait chassé de plusieurs endroits. Les jongleurs n'étaient pas souvent appréciés des clercs et des moines. Ils volaient des fruits dans les champs et se désaltéraient à l'eau des abreuvoirs, avec toujours la faim qui les aiguillonnait, les tenaillait à chaque instant.

Jusqu'à ce jour où ils avaient commencé à avoir les entrailles en feu, les uns après les autres. Brisés de douleur, ils s'étaient réfugiés dans une grange, vomissant, victimes de diarrhées sanglantes. Ils avaient fini par être trop faibles pour sortir à chaque flux de ventre. Mile se rappela le visage d'Anceline, décharné, haletant, les cheveux mêlés de paille, de terre, le regard vitreux. Elle lui avait souri avant qu'il ne ferme les yeux.

« Il y avait aussi une femme...

--- Je ne sais. Ici nous ne recevons que les hommes.

--- Vous ne soignez pas les femmes ? s'insurgea Mile, haussant le ton

--- Pas ici. En un autre lieu, ce sont nos sœurs qui s'en occupent. »

Mile ouvrit la bouche, surpris de son emportement. Il hocha la tête.

« Il est possible d'avoir nouvelles de l'une d'elles ?

--- Je vais m'enquérir. Il faudra me dire son nom et me la dépeindre. »

Les jours suivants, Mile reprit des forces, sous les soins constants des gens de salle, nourri de plats sinon savoureux du moins roboratifs. Il n'avait jamais avalé autant de pain blanc de sa vie. Il se sentait presque gêné de toutes ces attentions, mais il avait constaté que, s'il avait un menu particulier, tous les malades étaient traités comme lui. En contrepartie, il s'efforçait de se montrer moins iconoclaste qu'habituellement et faisait même montre d'une piété timide. Sans trop se forcer, étonnamment. Il avait tellement moqué les moines et les clercs par le passé qu'il était perturbé par tous les égards qu'on lui témoignait, sans rien lui demander en échange. De toute façon, il avait trop sué par des chemins difficiles pour avaler avec amertume le vin offert. Il dormait, mangeait et buvait donc avec empressement, faisant de son mieux pour être un patient exemplaire. Sa seule angoisse venait de son absence de nouvelles d'Anceline.

L'hospitalier ne l'avait pas trouvée. Il gardait espoir néanmoins, elle pouvait encore être inconsciente disait-on, ou avoir donné un autre nom, par méfiance ou par pudeur, ainsi que le faisaient certains. Arriva le jour où Mile fut enfin complètement rétabli. Il attendait la visite du frère responsable, avec le médecin, pour avoir l'autorisation de quitter les lieux.

Seulement, il n'en avait que peu envie. Il avait goûté les draps propres, les couvertures douillettes, les repas chauds chaque jour. L'idée de se retrouver dehors, seul, à l'approche de l'hiver, ne l'enchantait guère. Le groupe arriva devant lui, avec les valets empressés qui tourbillonnaient autour. Le médecin semblait satisfait et confirma son accord pour le départ de Mile. En entendant cela, ce dernier interpela l'hospitalier.

« Je ne sais comment vous dire, frère, mais... je n'ai nul lieu où aller. Aucune nouvelle de mon amie, toujours, et nulle famille ici. Ni feu ni lieu.

--- Oh ! échappa le frère, avec une douceur et un effroi instinctifs. À l'approche de la mauvaise saison...

--- Il y a peut-être tâche où je pourrais m'employer ici, il y a toujours grand besoin de valets. Je ne suis pas fort grand, mais bien vaillant. »

L'hospitalier hocha la tête, affichant un sourire capable d'effrayer n'importe quel enfant.

« Je vais voir si je peux vous trouver place dans un de nos lieux.

--- Grâce vous en soit rendue, frère ! Je suis prêt à assumer n'importe quelle tâche. »

L'hospitalier fronça les sourcils et réfléchit un instant.

« Il y a bien quelque endroit où nous avons besoin d'un valet, pour le tenir en la nuit et s'assurer que nul n'y entre...

--- Je suis plus féroce que mâtin et serai féal valet !

--- Avez-vous déjà entendu parler de Chaudemar ? risqua timidement le moine.

--- La porte des Enfers ? » échappa un des valets aux alentours.

Non, en effet, Mile n'était pas au Paradis. ❧

### Notes

Les amuseurs publics, qu'on nommait alors les jongleurs quels que soient leurs compétences : acrobaties, jeux d'adresse, contes, chansons, n'ont guère laissé de trace dans les sources textuelles. On sait pourtant qu'ils existaient, car ils sont souvent représentés dans l'iconographie, et étaient parfois employés comme figure pour des textes moralisateurs. Mais au final, de la vraie condition de leur existence au XIIe siècle, on n'en sait que peu.

À l'opposé, le grand hôpital de Saint-Jean de Jérusalem est bien connu, et de nombreux documents en traitent. Pourtant, là encore, certains aspects pourtant essentiels demeurent obscurs. Son emplacement précis fait par exemple encore l'objet de discussion malgré certaines hypothèses anciennes qu'on pensait validées. Par contre, son fonctionnement a fait l'objet d'un texte assez détaillé de la part d'un voyageur, qui semble y avoir longtemps séjourné, peut-être même comme membre de son personnel vu tous les détails qui sont fournis. En ce qui concerne Chaudemar (lieu que je n'ai pas inventé), plus d'informations se trouvent à son sujet dans la seconde enquête d'Ernaut, *Les Pâques de sang*.

Remarque : la chanson déclamée par Mile est une invention basée sur des blagues médiévales. Le thème qui l'a inspiré constituait un motif comique fréquent, d'ailleurs pas complètement oblitéré de nos jours.

Plus de précision dans le texte sur « Le fils à fesse ».

### Références

Beltjens Alain, « Le récit d'une journée au grand hôpital de Saint-Jean de Jérusalem sous le règne des derniers rois latins ayant résidé à Jérusalem ou le témoignage d'un clerc anonyme conservé dans le manuscrit Clm 4620 de Munich » dans *Bulletin de la Société de l'Histoire et du Patrimoine de l'Ordre de Malte*, N°14 (Spécial), 2004.

Boas Adrian, « Akeldama Charnel House/Carnarium/Chaudemar » dans *Jerusalem in the Time of the Crusades. Society, landscape and art in the Holy City under Frankish rule*, Londres & New York : Routledge, 2001, p.185-187.

Casagrande Caria, Vecchio Silvana, « Clercs et jongleurs dans la société médiévale (XIIe et XIIIe siècles) » dans *Annales*, volume 34, N°5, 1979, p.913-928.

John Riley-Smith, *The Knights of St. John in Jerusalem and Cyprus c.1050-1310*, McMillan St. Martin Press, Londres, 1967.

Épineuse couronne
-----------------

### Abords du Saint-Sépulcre, Jérusalem, matin du lundi 31 mars 1152

Les acclamations grondaient, montaient et retombaient ; les hourras volaient parmi les cieux, la joie emplissait d'aise les cœurs. Les applaudissements résonnaient, faisant concurrence aux exclamations, aux chants de liesse. Comme toujours à Pâques, le parvis du Saint-Sépulcre n'était que bousculade et cohue, rires et hymnes. Pourtant, cette fois-ci, les pèlerins n'étaient pas seuls à jubiler.

Objet de cette ferveur, un jeune homme se tenait sur les marches flanquant le portail d'entrée, encadré de quelques nobles en grande tenue, bliauts de soie et mantels brodés. D'à peine vingt ans, il était habillé plus magnifiquement encore que son entourage, recouvert d'orfrois et de passementerie. Mais ce qui le distinguait entre tous, c'était la couronne qui ceignait son front. La couronne des rois de Jérusalem qu'il arborait face à la foule venue l'acclamer. Il souriait, saluant les présents, son peuple, comme ses ancêtres avant lui.

Parmi les anonymes, un rouquin s'enthousiasmait, criait, emporté par la liesse communicative. Les dernières années avaient été rudes, avec la perte de territoires au nord, l'échec de la grande campagne menée avec les souverains allemands et français face à Damas. En outre, depuis la mort de Foulque[^59], la reine mère[^60] et le jeune Baudoin[^61] ne parvenaient pas à s'entendre pour diriger le royaume. Habituellement le roi se faisait de nouveau couronner à l'occasion des fêtes de Pâques, profitant de la présence des voyageurs lointains pour proclamer son pouvoir. Or, depuis plusieurs années le peuple de Jérusalem n'avait pu assister à pareille cérémonie. Son apparition sur les marches, ceint du symbole de son autorité, avait embrasé la population inquiète et fait retentir des hymnes de joie, des Actions de grâce.

Eudes tendit l'oreille à ce qui se disait alentour : le vieux Patriarche n'avait pu accepter de poser lui-même la couronne sur le front du jeune Baudoin, car Mélisende était absente, et cela ne pouvait se faire sans elle. Pourtant le roi était là, face à la foule, acclamé comme le souverain qu'il était. Recevant un léger coup de coude, Eudes se tourna vers son compagnon, un brun enrobé répondant au nom de Droart. Guère plus âgé, celui-ci venait d'entrer comme sergent dans l'administration royale. Il passait d'ailleurs la plupart de sa solde gagnée à surveiller les portes de la ville en liquides divers et variés qu'il ingérait avec ses amis. Empli de retenue, son visage était inquiet malgré l'enthousiasme ambiant.

« Ça va pas plaire à la reine... dit Droart.

--- Pourquoi donc ? Le roi a coutume de se faire ainsi reconnaître.

--- Elle a été couronnée de nouvel en même temps que lui, et saura rappeler qu'elle dirige le royaume également.

--- Il est temps pour elle de laisser place, non ?

--- Elle est fille et femme de roi, sa place est aux côtés de Baudoin ! »

Eudes comprit que son ami ne goûtait guère de voir le jeune roi manifester tant d'indépendance. Comme beaucoup d'habitants de Jérusalem, Droart appréciait beaucoup Mélisende. Si elle n'était pas aussi habile que son fils à la guerre ou dans les affaires diplomatiques, son talent pour gérer le domaine et satisfaire les populations était reconnu.

Fille de Baudoin du Bourcq, elle était née pratiquement en même temps que le royaume et l'incarnait pleinement depuis un demi-siècle. En outre, depuis une vingtaine d'années qu'elle dirigeait, on s'était habitué à sa présence, sa silhouette hiératique lors des cérémonies. Ainsi qu'à son caractère ombrageux, bien qu'il trouvât rarement à s'exercer à l'encontre des plus humbles.

Baudoin monta les quelques marches qui menaient à la chapelle du Calvaire, y pénétrant après un dernier salut à la foule. Voyant la presse sous les portes du sanctuaire, Droart et Eudes renoncèrent à y entrer et préférèrent s'éloigner. Il leur fallut jouer des coudes et se faufiler péniblement avant de s'extraire de la rue, près du change syrien. De là, ils rejoignirent rapidement Malquisinat, où ils espéraient trouver de quoi se restaurer.

C'était un jour férié, mais les commerces de bouche étaient ouverts, par permission exceptionnelle. L'afflux de pèlerins et de voyageurs était tel qu'il était difficile de nourrir tout le monde correctement au sein des établissements religieux. Ceux qui en avaient les moyens, ou le désir, pouvaient donc acheter des mets tout prêts aux charcutiers, pâtissiers, cuisiniers.

Pour l'heure, Droart n'aurait pas craché sur une friandise, après les longues semaines de Carême, même s'il avait surtout envie d'un peu de vin pour se désaltérer. Il trouva finalement un pichet de ce qu'il cherchait, à un prix qui le fit râler car augmenté certainement en raison de la forte demande. Il lâcha les pièces en grognant et proposa à son compère d'en prendre quelques lampées. Il burent debout, appuyés contre l'arcade en pierre d'un vendeur d'oublies dont l'odeur sucrée leur caressait les narines, tout en regardant aller et venir la foule qui s'agitait en ce jour de fête. Droart fit une grimace, repensant à ce qu'ils avaient dit un peu plus tôt, sur le parvis.

« J'espérais ces histoires achevées et là, venir se faire acclamer en la cité où la reine est si fort appréciée... »

Il ne finit pas sa phrase, préférant avaler un peu de vin. Eudes en profita pour avancer ce qu'il pensait.

« Avec ce qui se passe en la terre des Turcs, il est bon que le roi se fasse voir. Moult campagnes se préparent, de certes.

--- Je ne dis le contraire. Seulement, pourquoi faire pareil affront à la reine ? Ils ont parti le royaume, ce me semble. Il n'est guère prude de revenir là-dessus. »

Eudes ne pouvait qu'approuver. Le jeune Baudoin manifestait plus d'indépendance depuis quelques mois, plus d'assurance en raison de ses succès à gérer les territoires dans le nord. Si tout le monde s'accordait sur le fait qu'il était nécessaire que le royaume soit dirigé par quelqu'un de compétent, la crainte que leurs ennemis proches profitent du moindre flottement était la plus forte. Personne n'aimait l'incertitude née des affrontements de la volonté des puissants.

### Maison de Pierre Salomon, Jérusalem, soir du mardi 8 avril 1152

La grande salle était éclairée de nombreuses bougies et lampes, signe de la fortune du propriétaire. Les murs peints de motifs géométriques faisaient écho aux entrelacs savants des tapis. Les châssis de verre avaient été tirés des fenêtres, laissant la brise fraîche pénétrer dans la pièce, apportant les exhalaisons du jardin d'agrément contigu.

Assis sur des coussins et des banquettes, une quinzaine de bourgeois à l'évidente opulence discutaient âprement des événements, sans prêter attention aux friandises et aux boissons amenées là par le maître de maison. Celui-ci, ventripotent, d'âge mûr, se grattait le front, l'air abattu. Il échangeait à voix basse avec le dernier arrivant, encore chaussé de ses éperons dorés, une veste de voyage sur les épaules. Son visage las était collé de la poussière des chemins, mais son regard demeurait déterminé. Dégarni, le nez aquilin, il inspectait la salle avec application, identifiant les présents les uns après les autres. Lorsqu'on lui proposa un verre, il le prit avec un sourire poli. Puis il attendit que Pierre Salomon l'annonce. Ce dernier tapa dans ses mains plusieurs fois pour attirer l'attention et faire taire les échanges. Comme habituellement, il transpirait abondamment, et semblait au bord de l'apoplexie.

« Compères, le sire Régnier d'Eaucourt arrive avec moult nouvelles. Je crois que vous devriez faire silence et l'écouter... »

Puis il secoua la tête, se désolant à l'avance de ce qu'il allait entendre, avant de se laisser tomber sur un matelas. Le chevalier salua d'un rapide signe de menton, très martial, et prit la parole d'une voix forte, si ce n'est assurée.

« J'arrive de Mirabel, où le roi est entré. »

Plusieurs cris de surprise, peut-être d'indignation, se firent entendre, mais il ne s'interrompit pas pour autant.

« Le connétable Manassès a été prié d'abandonner sa charge, car le roi n'a mie besoin de ses services. »

Nouvelles exclamations.

« Des espies du roi l'ont informé que la reine Mélisende avait cheminé hors la cité de Naplouse et faisait route pour la sainte cité.

--- Êtes-vous ici pour nous demander de lui garder portes scellées ? s'indigna un vieil homme à la voix aiguë.

--- J'étais homme du sire connétable, et me voilà mandé par le sire Onfroy de Toron qui m'a pris à son service. Il n'est pas dans ses coutumes de demander félonie, répondit sèchement le chevalier.

--- Peuh ! Il a bien accepté les miettes de la reine quand il voyait le vent venir » protesta un jeune homme à la barbe drue.

Régnier lui adressa un regard à effrayer la Mort elle-même, mais s'abstint de tout commentaire. Il enchaîna.

« Le roi est déterminé à avoir les moyens de ses tâches. Il ne pourra les accomplir si la reine ne se dépossède pas quelque peu.

--- Il en veut encore plus, quoi ? s'étrangla un des présents.

--- C'est le roi, après tout, maître Turcame, protesta avec froideur un vieil homme à la toison d'argent.

--- Justement, il devrait avoir meilleur jugement sur sa mère la reine, qui est femme de savoir et de vertu. »

Les commentaires fusèrent alors de toute part, approuvant ou critiquant les dernières paroles. La plupart, néanmoins, reconnaissaient la bienveillance de la reine et le manque d'égard que Baudoin manifestait. Ils avaient été choyés par l'administration efficace de Mélisende et redoutaient, en bons gestionnaires, d'abandonner cela pour se retrouver sous la coupe d'un jeune homme dont on ne savait pas grand-chose finalement, en dehors de son goût pour les femmes. Pierre Salomon tapa de nouveau à plusieurs reprises dans ses mains.

« Mes amis, écoutez donc le sire d'Eaucourt. Il n'est pas simple messager, il assistait à nos conseils, et vient là en ami.

--- Merci à vous, maître Salomon. Je dois remonter en selle au plus vite, aux fins de voir la reine. J'aime à croire qu'une entente est encore possible.

--- Grâces vous soient rendues, sire, d'avoir pris le temps de nous informer de cela. Peut-être auriez-vous un conseil à nous donner, ou une autre information à nous délivrer.

--- Je viens à vous, car je crois que les Bourgeois du Roi sont amis de la paix, comme je le suis. Je n'ai nul besoin de porter conseil à vos oreilles. Vous saurez faire les bons choix le moment venu. N'oubliez pas que nos ennemis ne sont pas si loin. Damas n'est qu'à une chevauchée de notre cité. »

Il inclina sèchement le buste pour prendre congé, non sans avoir salué Pierre Salomon d'un geste amical sur l'épaule et d'un sourire. Puis il disparut derrière la tenture qu'il avait soulevée pour sortir. Un silence pesant s'infiltra alors dans la pièce, paralysant les coeurs, immobilisant les langues. Seul Pierre Salomon osa faire entendre sa voix.

« Alors, compères, que ferons-nous si le roi et la reine veulent entrer en la cité ? »

### Abords de la tour de David, Jérusalem, fin de matinée du lundi 14 avril 1152

Les mains s'accrochèrent aux cordages et les hommes tirèrent de toutes leurs forces pour abaisser le balancier faisant pivot, propulsant la pierre de la fronde avec vigueur, dans un ronflement inquiétant. Le projectile siffla et percuta le mur avec un bruit sec, y arrachant un éclat de moellon dans sa dislocation. L'ingénieur responsable de la bricole échappa un grognement de rage et sermonna les soldats. Ils avaient dévié vers la droite, car ils n'appliquaient pas tous le même effort. La porte de la forteresse les narguait, intacte.

Tandis qu'il préparait un nouveau boulet, des voix se firent entendre : l'accès s'ouvrait et une main se montra. Plusieurs archers et arbalétriers se mirent en position derrière les mantelets grossièrement disposés dans la rue. On appela les capitaines, les officiers. Un chevalier en grand haubert de mailles, mais la coiffe détachée, pendant sur ses épaules, s'approcha. Ses cheveux coupés courts étaient hirsutes et son visage renfrogné était sali du frottement de l'armure. Il franchit les protections et se posta à quelques pas en avant, les mains sur les hanches, l'air bravache, défiant un quelconque tireur de le viser depuis la forteresse royale. Il attendait sans rien dire.

La porte s'ouvrit plus complètement et un homme s'avança à son tour. Lui aussi était en tenue de guerre, le casque lacé et le bouclier au bras. Il n'avait néanmoins pas son épée dégainée. Voyant qu'il n'était pas en danger, il descendit les marches avec soulagement et franchit, quasiment en sautant, le fossé comblé de bric et de broc. Arrivé devant le chevalier, il défit son heaume et laissa tomber sa ventaille[^62], révélant un visage également fatigué et sale, mais au cheveu plus rare. Il souriait, d'un air un peu désolé, et posa sa main sur l'épaule face à lui.

« Je suis aise de te voir, Raoul, j'ai message pour le sire de Toron. »

Lorsqu'ils franchirent les protections de bois, la porte de la forteresse était déjà refermée. Les soldats retournèrent à leur ouvrage, empoignant les cordes de l'engin de siège. Une fois un peu en retrait, Raoul stoppa son compagnon.

« Je ne peux te laisser aller ainsi, Régnier. Tu as disparu auprès de la reine depuis presque une semaine.

--- Le sire de Toron m'avait missionné, Raoul.

--- Je sais bien, mais tu servais Manassès de Hierges avant cela. D'aucuns parlent félonie.

--- Qui ose dire cela ?

--- Personne ne le fera face à toi, mais les langues sont acides. »

Il soupira et indiqua le baudrier.

« Il va falloir que tu me confies ton épée si tu veux voir le connétable. »

À son froncement de sourcil, Raoul comprit que Régnier d'Eaucourt attendait d'en savoir plus. Ils avaient chevauché bien trop souvent botte à botte, brisé assez de lances côte à côte pour qu'il le traite comme un étranger ou un félon.

« Ce n'est pas encore officiel, mais le sire de Toron sera le nouveau connétable. »

Il eut un sourire contrit.

« Je sais bien ce que tu penses, que chacun se place maintenant que le jeune roi a le vent avec lui.

--- Je n'aurais pas cru possible que Baudoin assaille sa propre mère en la cité de Jérusalem, surtout. Sais-tu que des hommes ont été blessés ?

--- Bien sûr, ne vois-tu pas que j'ai porté haubert tous ces derniers jours ? Les archers de la reine n'ont guère été plus amables ! »

Régnier inspira, frappant du poing sur le mur.

« Tout cela est ridicule ! S'entrebattre alors que Norredin[^63] rôde comme un loup affamé !

--- Il faut faire cesser cette folie au plus tôt, je m'en accorde. Un jeune roi, à la main leste et de grand courage, voilà ce qu'il nous faut.

--- As-tu oublié le serment prêté à la reine Mélisende ? A-t-elle démérité ? »

Raoul baissa les yeux, ennuyé. Il passa sa langue sur des lèvres craquelées avant de répondre.

« Les choses passent, Régnier. Il est temps pour le royaume d'avoir un roi à sa tête. »

Régnier d'Eaucourt grogna, le visage crispé. Il secoua la tête en désaccord, puis finalement ses épaules s'affaissèrent. Ses mains se portèrent à son baudrier, qu'il détacha sans un mot. Il le tendit à Raoul.

« Allons voir le sire connétable, mettre sauf ce qui peut l'être de la terre du Christ. » ❧

### Notes

La situation politique du royaume de Jérusalem au XIIe siècle n'a jamais été très stable. La difficulté pour la dynastie régnante à se maintenir en place, la relative puissance des grands barons, l'imbrication dans les jeux des puissances régionales, ont entretenu le pouvoir royal dans un équilibre précaire, jusqu'à son effondrement face à Saladin dans le dernier quart du siècle.

Les solutions trouvées pour tenter de consolider la continuité de la lignée furent parfois à l'origine des problèmes. Ainsi, Foulque tenait lui-même sa couronne de son mariage avec Mélisende, et Baudoin avant lui avait fortement ancré le pouvoir dans les mains de la reine, de façon à éviter qu'il ne soit accaparé par un époux étranger. Cela a malheureusement débouché sur une crise lorsque le jeune Baudoin fut en âge de régner et désireux de le faire.

La dissension alla jusqu'à un accord éphémère de partition du royaume et se termina par un siège en règle de la Tour de David où Mélisende s'était réfugiée, par les forces de Baudoin. Abandonnée par la plupart de ses soutiens, la reine mère accepta finalement de se retirer à Naplouse et ne prit plus part au gouvernement jusqu'à sa mort une dizaine d'années plus tard.

S'appuyant sur une analyse très fine des chartes délivrées par les chancelleries des deux dirigeants, Hans Eberhard Mayer propose une lecture plus complexe des événements, qu'il place en 1152, qui ont vu les deux souverains s'opposer. Je n'ai donc pas retenu le récit issu des écrits de Réné Grousset (*Histoire des croisades et du royaume franc de Jérusalem - II. 1131-1187 L'équilibre*, Paris : Perrin, 1935, 2006) auquel je préfère généralement le livre de Joshua Prawer, plus récent, pour resituer l'histoire du royaume dans une perspective de longue durée.

### Références

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972, p.93-182.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Léviathan
---------

### Port de Jaffa, matin du mercredi 27 novembre 1157

Installées sur la plage auprès des débarcadères, plusieurs silhouettes suivaient du regard le haut navire qui venait de mettre à la manœuvre. Avec sa voile immaculée et sa coque peinte, cinglant parmi les frêles canots de pêcheurs et les lourds vaisseaux de commerce, la galère avait fière allure. Elle longeait pour l'heure la côte, attendant d'avoir dépassé les rochers d'Andromède avant pour pouvoir s'engager au large. À son mât flottait la bannière royale, aux croix d'or sur fond d'argent, ondulant sous les assauts du vent. L'ambassade était finalement en route pour la capitale byzantine, Constantinople. Parmi les curieux qui observaient le navire, le plus petit des deux se tourna vers son gigantesque compagnon.

« Allons donc en la citadelle, Ernaut, rendre compte au sire comte de la bonne partance du connétable et de l'archevêque.

--- Ne devons-nous pas rentrer au plus tôt ?

--- Amaury a peut-être quelque valet prêt à bondir sur le dos de rapide coursier. Nous ne saurions nous mesurer à pareil messagier. Et puis, il me semble que le sire frère du roi a le droit d'apprendre cela. »

Le jeune homme haussa les épaules. Il avait constaté que parmi les sergents au service du souverain, chacun avait ses lubies, ses habitudes, et que les affrontements au sein du royaume, quelques années plus tôt, avaient laissé des blessures, des préférences, des rancœurs. Gaston, un boiteux entre deux âges, à la voix rauque, aux bras épais et aux mains semblables à des pattes d'ours, avait un faible pour le frère cadet du roi Baudoin, qui était pour l'heure comte de Jaffa. Il ne s'en était jamais expliqué, mais on avait dit à Ernaut qu'il avait été un temps au service de la vieille Mélisende, la reine mère, désormais retirée à Naplouse. Peut-être avait-il approché le jeune noble alors qu'il n'était qu'un enfant et en avait gardé une certaine affection à son égard.

Pour l'heure, il suivait des yeux le navire qui glissait sur les vagues, ombre brune découpée sur le ciel d'azur et les nuages cotonneux.

« On va tâcher de se mettre quelque chose dans le col en chemin, j'ai grand-soif de ce départ aux matines. Un grand gamin comme toi doit toujours avoir large place pour y fourrer bonne pitance, non ? »

Disant cela, il flatta son compagnon d'une bourrade et commença à avancer parmi les caisses et les sacs, les barques tirées sur la plage, les filets en plein ravaudage. Comme tous les matins, une activité intense agitait l'endroit, qui durerait jusqu'au retour des pêcheurs et au départ de tous les navires au long cours.

Ce n'était plus la saison maritime, et Jaffa n'était pas un grand port : s'il était possible d'y débarquer dans la baie protégée par les récifs d'Andromède, la faible profondeur obligeait à tout transborder sur des barges, jusqu'aux pontons. Les vaisseaux demeuraient ensuite à l'ancre un peu en retrait. C'était certes un endroit stratégique, car il ouvrait la voie terrestre directe pour Jérusalem, mais il n'avait jamais pu concurrencer des haltes commerciales comme Césarée, un peu plus haut sur la côte ou Ascalon, en direction de l'Égypte ou, surtout, Saint Jean d'Acre, plus au nord.

C'était là que la plupart des pèlerins et la majeure partie des marchandises parvenaient au royaume de Jérusalem. Gaston ronchonna en recevant du sable soulevé par la course de gamins excités qui les dépassèrent en riant. Les deux sergents longeaient les murailles de la ville, moins forte de ce côté peu exposé, pour rejoindre la porte de la Mer, qui permettait d'accéder au faubourg par le quartier pisan.

Ernaut n'était au service du roi que depuis quelques mois et ne se voyait pas confier de tâches importantes, ce qui lui convenait parfaitement. Escorter des voyageurs, porter des messages : encadrer la levée des cens[^64] constituaient la majeure partie de son travail. De temps à autre, certains des hommes étaient convoqués pour servir au sein de l'ost[^65], lors des campagnes de Baudoin III[^66], mais la plupart du temps, c'était pour des activités subalternes.

Cette fois, ils avaient accompagné une ambassade qui se rendait auprès du basileus Manuel Comnène pour négocier une épouse au jeune roi. Une telle politique matrimoniale permettait de resserrer les liens entre territoires chrétiens. Après les exactions à Chypre de la part du prince Renaud d'Antioche, il était plus que nécessaire de se rapprocher des Byzantins.

Dans les rues, tout le monde commentait les événements récents : les tremblements de terre du nord, signes de colère divine contre leurs adversaires, l'arrivée du grand baron, Thierry d'Alsace comte de Flandre, la maladie qui frappait leur ennemi à tous, Nūr ad-Dīn. Toutes ces nouvelles emplissaient d'allégresse le cœur des habitants du royaume de Jérusalem. Les deux sergents partageaient cet enthousiasme et déambulaient, le sourire aux lèvres. Une odeur forte leur assaillit les narines alors qu'ils passaient aux abords de tonneaux où des gamins mélangeaient des oeufs de morue, du sable et de l'eau de mer.

« Ça sent la sardine par ici, garçon, lança joyeusement Gaston. On va s'en offrir quelques-unes sur du pain à l'huile, ça te dit ? »

Joignant le geste à la parole, il se dirigeait vers un appentis où deux femmes faisaient griller du poisson, qu'elles glissaient dans du pain généreusement arrosé d'aromates. Â côté, un adolescent proposait du vin tiré du tonneau qu'il couvait comme une mère poule.

Plusieurs hommes, portefaix, vieux pêcheurs, ouvriers de marine, discutaient aux abords tout en prenant une pause. On y commentait plus le temps et les conditions de mer que la situation politique du royaume. Depuis plusieurs années, Jaffa était dans une région abritée, et ses récifs la protégeaient généralement des attaques navales. La vie s'y déroulait désormais paisiblement.

Quelques mouettes criaient, perchées sur l'enceinte ou planant au-dessus de leurs têtes, dans l'attente de quelque nourriture à chaparder. À côté de claies, des enfants brandissaient des feuilles de palmier pour effrayer les insolents volatiles qui venaient s'en prendre au poisson mis à sécher. Le vent du large était frais, mais peu vigoureux, et parvenait à peine à chasser les odeurs salines entêtantes, additionnées du goudron des navires et de la sueur des hommes. Des cloches commencèrent à sonner dans la ville, appelant les moines astreints à la prière des heures[^67]. Avalant des bouchées ambitieuses, barbouillant leur menton de sauce, Ernaut et son compagnon passèrent la voûte de l'enceinte sous le regard indifférent du soldat de faction, occupé à musarder avec une jeune femme.

### Abords de la chapelle Saint-Nicolas, soir du mercredi 27 novembre 1157

Installés à l'abri d'une dune, en contrebas d'un taillis d'épais buissons épineux, les deux sergents profitaient des derniers rayons du soleil. La journée avait été encore chaude, et ils accueillaient la brise marine avec reconnaissance. D'où ils étaient, ils voyaient la cité de Jaffa agglutinée sur les pentes, enserrée dans ses murs d'où dépassaient les tours fortes, les clochers élancés. Au plus haut du relief, l'imposante demeure de pierre du comte, Amaury, semblait défier la mer. Les chevaux paissaient aux abords, attachés à une corde tendue entre deux palmiers. Au-delà, en retrait de la route qui menait à Arsuf, une petite bourgade de misérables maisons de pêcheurs s'étirait, tenant compagnie à une chapelle flanquée d'un modeste cimetière. Les embarcations tirées sur la plage, entre lesquelles jouaient quelques gamins dépenaillés, s'alignaient sur le sable.

Allongés sur leurs couvertures, les deux sergents profitaient de la vue magnifique du ciel se teintant d'orange et de vermeil alors que le soleil disparaissait dans les nuages lointains. Ils avaient soupé de houmous et de brochettes de poulet, et avaient encore un peu d'eau coupée de vin dans leurs gourdes de peau. Leur mission achevée, ils n'avaient plus qu'à retourner à Jérusalem, porteurs de plusieurs messages à destination des différentes administrations.

Gaston n'aimait guère demeurer enfermé entre quatre murs depuis, disait-il, une mauvaise expérience de siège. Il avait donc proposé à Ernaut de profiter du temps durablement beau en cette fin d'année pour dormir à la belle étoile. Le jeune homme, qui n'appréciait rien tant que se baigner, avait accepté avec grand plaisir. Il n'avait d'ailleurs guère tardé à aller délasser ses pieds et ses mollets dans les vagues avant de s'allonger, le dos réchauffé par le sable encore tiède du soleil d'après-midi. Il commençait à s'assoupir, bercé par le ressac lorsqu'une voix éraillée le tira de sa rêverie.

«Vous ne devriez pas demeurer hors les murs en cette nuit, je ne serai guère étonné qu'un fort vent se lève d'ici peu. »

Gaston se tourna et avisa leur interlocuteur : un vieil homme à la tenue fatiguée, d'épaisse toile de coton grise. Les manches maintes fois reprisées et rapiécées apportaient des touches de couleur surprenantes dans sa mise fort modeste. Il était accompagné d'un petit chien au poil ras et sombre. Lorsqu'il s'approcha, Ernaut put voir qu'il était tonsuré. Sûrement un des serviteurs de l'édifice religieux, peut-être le curé en charge de la paroisse.

« Voyez ces lourds rideaux sombres qui avancent sur le large. Ils vont venir sus nous, j'en suis quasi acertainé !

--- Vous connaissez bien la mer ?

--- Pas tant... Mais fort bien cette côte, et j'y ai vu souvente fois arriver tempêtes du large.

--- Pourtant la soirée est fort claire et bien agréable.

--- C'est là danger, surtout pour les marins demeurés sur les navires à l'ancre. »

Il se tourna vers le hameau d'où pointait le modeste clocher.

« La cité aura clos ses portes, vous pouvez donc vous abriter en la nef si le coeur vous en dit. J'ai usage d'héberger voyageurs. »

Ernaut le dévisagea, avec sa tenue abîmée et sa triste allure.

« Vous êtes le prêtre du lieu ?

--- Certes non, répondit-il dans un rire. Je ne suis que pauvre clerc à son service, à peine capable de déclamer *credo*, *pater* et *ave*[^68]. Le père curé réside en la ville de Jaffa, je garde l'église pour ma part. »

Le vieil homme vint jusqu'à eux, les mains sur les hanches, admirant l'endroit comme s'il en était le maître. Il pointa l'horizon, plissant les yeux pour mieux en percevoir les détails.

« Voyez les raies sombres au loin, il y a là forte pluie au large, et le vent la rabat sur nous. »

Gaston s'était relevé et hochait la tête.

« Il y a là certes belle tempête qui obscurcit la vue. J'espère que les nôtres ne l'auront pas subie.

--- De qui parlez-vous ?

--- De la *Gloria*, une galée royale qui navigue au nord.

--- Nulle crainte pour elle, là ce grain monte depuis les côtes sud. »

Le vieux clerc sourit.

« Vous devriez aussi attacher vos montures en la cour de l'église, elles y seront plus rassurées. J'y tiens déjà un âne, qui saura leur tenir compagnie et les calmer. »

Ernaut s'était accoudé et regardait, un peu inquiet la tempête qui lui paraissait si loin. Cela lui rappelait de mauvais souvenirs, les journées enfermé dans la cale à subir les assauts des vents et des vagues, tandis que le Falconus le menait en Terre sainte[^69].

« Vous êtes certain qu'elle va venir à nous ? »

Le vieil homme s'esclaffa et montra du doigt les canots retournés qui jalonnaient la plage.

« Quand bien même j'aurais doutance, je peux m'en remettre à la sagesse des hommes de cette côte. Puis, reprenant un air grave, il ajouta : cette nuit ne sera guère reposante, amis, je vous le garantis ! »

### Plage de Jaffa, nuit du mercredi 27 novembre

Les vagues grondaient en se fracassant sur la plage, le vent de tempête projetait les embruns jusque sur les dunes. Les nuages compacts avaient masqué la lune, plongeant la nuit dans une noirceur d'encre. Les hommes criaient, vociféraient, mais leurs voix bien frêles face au déchainement des éléments paraissaient dérisoires. Parmi les flots, quelques silhouettes s'agitaient, abattues par les crêtes écumeuses, renversées par les rouleaux hurlants. Ernaut était de ceux-là.

Assuré autour de la taille d'une corde tenue par une ligne de villageois sur la plage, il tentait d'avancer vers les débris et les vestiges du navire. Un peu plus tôt, il avait été réveillé par le vieux gardien de l'église : un bâtiment dont les amarres avaient été rompues par la tempête était en difficulté. Le temps que les habitants se rendent sur la côte avec quelques lampes, il ne subsistait du vaisseau qu'une épave. Rapidement, les pêcheurs, habitués à lutter avec les flots, avaient organisé les secours.

Indifférent au danger, Ernaut s'était proposé pour aller parmi les vagues tenter de sauver les hommes tombés à la mer. Tandis qu'il avançait, tantôt battant des pieds et des mains, tantôt marchant, il écartait les planches arrachées, les sacs déchirés, les lambeaux de la cargaison et du navire. Il hurlait à pleins poumons, espérant se faire entendre des matelots ballotés dans les flots. Souvent, ce qu'il prenait pour une tête, un corps évanoui, ne se révélait que pitoyable débris, fragment de tonneau ou de coque.

Lorsqu'on le traînait sur la plage, à force de bras, il y découvrait les restes entassés par les vagues, les ballots qui avaient survécu et flotté jusque-là. Les femmes se chargeaient de les tirer vers les dunes, tout en accueillant les marins sauvés des eaux. Ce ne fut qu'à sa troisième tentative qu'Ernaut parvint à agripper un poignet. La panique du naufragé était telle qu'il faillit les noyer tous les deux à se débattre comme un forcené. Le bras puissant du colosse s'enroula autour de son torse et il le hâla vers la côte où ils se trainèrent tous les deux, épuisés par l'effort. On les porta à l'écart des flots, et des mains secourables les protégèrent du froid avec une épaisse couverture.

Un des pêcheurs hurla à Ernaut qu'il devrait prendre un peu de repos à son tour, mais le jeune homme refusa et repartit dans les vagues. Il était en compte avec les mers et espérait bien cette fois-ci en sauver autant qu'il le pourrait.

Au petit matin, le vent était retombé. Une clarté hésitante mangeait le ciel du côté de la plaine, saluée par le chant joyeux des oiseaux, indifférents à la détresse de la nuit passée. Parmi un petit groupe, Ernaut était assis près d'un feu, une couverture sur les épaules, appuyé contre le muret d'un jardin. Depuis la butte, il voyait la plage qui s'étendait en arc de cercle jusqu'à l'éminence où se tenait Jaffa. Des mâts de navires dansaient à l'horizon, dans la rade.

Il regarda autour de lui : sur le sable, entassés pêle-mêle, des marchandises, des fragments du bateau, des morceaux de cordage, des caisses éventrées, gisaient. Un des hommes expliquait que cela serait rendu aux affréteurs. Puis il ajouta, en souriant à demi :

« Sauf si le roi se met en tête de s'emparer de cela !

--- Pourquoi ferait-il cela ? s'étonna Gaston, lui aussi épuisé par la nuit passée à s'activer.

--- Je ne saurais dire, mais il l'a déjà fait, voilà quelques années de cela. Il a tout pris à un de ces émirs dont le navire avait été drossé sur la côte. Je peux te dire qu'il y avait là bien moults choses, et pas tant abîmées car le navire s'était d'abord échoué[^70]. »

Ernaut haussa les épaules. Les agissements des puissants n'étaient pas de son ressort. En outre, il était au service du roi, il ne se sentait donc pas en verve pour en deviser, après une pareille nuit. Il fixa ses mains, couvertes d'ecchymoses, et entreprit d'en arracher une écharde.

Il leva les yeux quand le vieux gardien s'approcha, une miche de pain sous le bras. Il en trancha plusieurs généreux morceaux qu'il accompagna d'une portion de fromage sec sorti d'une besace. Distribuant la pitance, il commenta les événements et confirma qu'il y avait six rescapés, et que seuls deux matelots avaient donc été perdus. Les hochements de tête, emplis de lassitude, se satisfaisaient de cet honorable accomplissement. Lorsque le vieil homme reprit le chemin de la chapelle, où les naufragés étaient hébergés, Ernaut se leva et le rejoignit.

« Je dois repartir tantôt pour la sainte Cité, pensez-vous qu'ils souhaiteraient que je porte message à leur parentèle ou à un associé ?

--- J'en doute fort, ils ne sont pas du royaume.

--- D'où sont-ils alors ?

--- Ils viennent d'Alexandrie. »

Le jeune homme ne cacha pas son étonnement. Les Égyptiens étaient ennemis de Jérusalem depuis des dizaines d'années, leurs navires écumant les côtes comme de vulgaires pirates.

« Que faisaient-ils ici ?

--- Des marchands, des marins... »

Le vieil homme haussa les épaules et laissa mourir sa voix. Puis il toussa et fit face à Ernaut.

« Étrange que nous ayons choisi de risquer nos vies pour sauver ceux-là mêmes que nous combattrons peut-être demain, n'est-ce pas ? »

Ernaut demeura muet, troublé par la question.

« Le Léviathan est notre ennemi, mon garçon, pas l'homme. *Sa vue seule suffit à terrasser. Il devient féroce quand on l'éveille, nul ne peut lui résister en face*[^71]..

--- Le Léviathan ? » s'interrogea Ernaut qui ignorait le terme.

Le vieil homme s'approcha de lui et lui tapa sur la poitrine d'un index impérieux.

« Le monstre qui peut tout dévorer, né des mers féroces et qui, chassé de son refuge abyssal par les navires des hommes, a trouvé refuge en chacun de nous ! »

Puis il reprit son chemin, un sourire mystérieux sur les lèvres. Ernaut l'interpella alors qu'il tournait au coin de l'église.

« Tu ne m'as même pas dit ton nom, l'homme.

--- Jonas, on me nomme Jonas ! » ❧

### Notes

La cité de Jaffa, malgré un port peu accessible pour les navires à fort tirant, et ses dangereux écueils que la légende antique rattachait à Andromède, était au départ de la route qui, par la plaine de Sharon, filait droit en direction de Jérusalem, distant de cinquante-huit kilomètres. Sa possession était donc stratégique, et les Latins s'en emparèrent rapidement lors de la première Croisade. Érigé en fief, le comté fournissait en tout cent chevaliers, en faisant le premier territoire au service de la couronne. De lui dépendait Ascalon, Rama, Ibelin et Mirabel.

La navigation maritime, essentielle pour le commerce et l'afflux de colons et de combattants européens pour le royaume de Jérusalem, se tarissait pendant les mois d'hiver. Du cabotage devait subsister, mais les navires s'éloignaient habituellement peu des ports. Et même là, il arrivait parfois que des tempêtes emportent de nombreuses vies.

Bien qu'aucun texte législatif ne soit connu avant le règne d'Amaury (roi de 1163 à 1174), il existait des coutumes, généralement partout observées, pour protéger les avoirs de ceux qui voyaient leurs biens perdus en mer. C'est donc avec surprise qu'on peut lire le récit d'Usamah ibn Munqidh, le célèbre guerrier poète et diplomate, qui raconte s'être fait déposséder lors d'un naufrage par le roi de Jérusalem. Il y laissa, écrit-il, une bibliothèque forte de plusieurs milliers d'ouvrages.

La légende d'Andromède est parfois reliée à des mythes plus anciens, phéniciens, et pourrait se situer dans la tradition des combats contre des monstres comme celui de saint Georges, lui aussi originaire du Moyen-Orient. Nul étonnement à ce qu'on prête à la mer, grande dévoreuse d'hommes, les attributs d'une telle créature.

Dans la Bible, on rencontre l'histoire de Jonas qui eut, lui, la bonne fortune de s'en sortir vivant. Malgré cela, la tradition marine attache à ce nom la malchance.

### Références

Cobb Paul M., *Usamah ibn Munqidh, The Book of Contemplation*, Londres : Penguin Books, 2008, p.43-44.

De Mas Latrie Louis, « Les comtes de Jaffa et d'Ascalon du XIIe au XIXe siècle », dans *Revue des Études Historiques*, juillet 1879.

L'ennemi intime
---------------

### Abords du village de Rabwé, environ de Damas, matin du lundi 26 juillet 1148

Le hennissement d'un destrier fit sursauter le chevalier, qui arracha en un geste vif sa couverture, prêt à bondir sur ses pieds. Les yeux hagards, il scruta les alentours. Tout semblait calme. Son valet était occupé à alimenter en bois un petit feu appuyé à un muret. Sous des arbres fruitiers, une corde avait été tendue pour y attacher les montures. Le soleil, déjà vaillant, perçait de ses flèches dorées la voûte d'émeraude. Le cliquetis des armes, le frottement des cuirs de sellerie, l'odeur des chevaux, s'efforçaient de vaincre les saveurs sucrées et délicates des jardins où les Latins avaient pris position, dans le faubourg occidental de Damas, l'oasis artificielle de la Gûta.

Rassuré, Régnier d'Eaucourt bailla, s'étira. La nuit avait été tranquille après une journée harassante. Il faisait partie d'une échelle[^72] menée par le connétable du royaume latin de Jérusalem, Manassès de Hierges. Tout le jour durant, la veille, ils s'étaient opposés à des forces jaillies de la ville, au nord du canal de Bânâs.

L'arrivée de contingents ennemis depuis les régions septentrionales les avait obligés à refluer vers leurs positions, non sans avoir durement malmené les musulmans. Il s'assit, frottant d'une main lasse son visage gris de poussière, orné d'une barbe vieille de plusieurs jours. Il sentait la sueur déjà couler dans son dos quand bien même il n'avait pas encore enfilé son haubert de mailles par-dessus sa cotte gamboisée[^73].

Ganelon, son valet, vint s'enquérir de lui et repartit lui préparer une collation. Une agitation laborieuse parcourait les environs. Des hommes en chemise et chausses s'activaient à abattre les petits édifices des jardins, tailler les arbres, rebâtir les murets, de façon à fortifier leur position. D'innombrables canaux d'irrigation leur causaient de grands soucis pour déployer leur cavalerie et ils espéraient ainsi avoir un endroit d'où ils pourraient s'élancer contre les unités ennemies. Il entendit des soldats évoquer l'installation des troupes du roi Conrad sur une large zone herbeuse découverte, plus au sud.

Après avoir rompu le jeûne, le chevalier se dirigea vers une maisonnette érigée par un notable damascène dans son jardin d'agrément. Modeste par la taille, elle était néanmoins bâtie avec goût et des structures légères en bois décoré ornaient l'étage où le propriétaire des lieux devait venir prendre le frais au plus fort de l'été. Pour l'heure, elle abritait les quartiers du connétable du royaume, le maître de Régnier. Un auvent avait été tiré contre, de façon à accueillir quelques chevaux. Aucun soldat ne s'opposa à l'entrée de Régnier, chevalier connu et respecté.

Il monta directement l'escalier, laissant la petite cuisine où s'activaient deux valets. De si bon matin, l'odeur de plats épicés le saisit à la gorge. Ce fut donc en toussant qu'il acheva de grimper les marches menant à l'étage.

« Notre féal picard ! Qui corne bien curieusement son entrée ! »

Le connétable était assis sur un siège curule, face à une table où s'étalait son harnois de guerre, mailles et lames, casque et baudrier. Un verre de vin à la main, il était jusqu'alors en train de discuter avec deux hommes debout devant lui, un gros chevalier à la barbe fleurie, les yeux torves animant un faciès cassé, le crâne orné d'une coupe à l'écuelle, flanqué d'un maigre civil, habillé de toile fatiguée, poussiéreuse, occupé à gratter d'un pouce énervé le pavillon de son nez fort imposant. Tous deux tournèrent leurs regards vers le nouvel arrivant, qui se fendit d'un salut, les sourcils froncés de ne pas les reconnaître. Le connétable n'en dit pas plus et ne prit que quelques instants pour congédier le premier homme, puis il invita Régnier à se joindre à lui, en montrant le civil de la main.

« Aymar, que voilà, me porte nouvelles de la cité. Elle semble en proie à quelque panique, et c'est fort bon pour nous. Chaque jour nous renforce et les habitants en sont moult inquiets.

--- Les contingents arrivés hier étaient pourtant de belle importance, sire. »

Le connétable fit un geste vague de la main.

« Renforcements tardifs ! Ils s'échinent à relever leur muraille fatiguée, ils craignent notre assaut imminent. »

Il fit une grimace.

« Ce qui me pose fort souci, ce ne sont pas combats en champ mais embuscades fort coûteuses en hommes et en moral. Encore cette nuit, plusieurs patrouilles ont subi violent assaut et je ne m'enjoie guère de voir les têtes de mes hommes orner les murs de la ville. »

Il se tourna vers Régnier, soudain rembruni.

« Ils vont et viennent à leur aise en ces jardins, familiers des sentiers, des détours de canaux et du tracé des murets. Nos coursiers se font souventes fois harceler, voire tuer. Il nous faut avoir nouvelles des différents points de l'assaut, sans quoi nous ne pouvons frapper adroitement ! »

Comme faisant écho à ses paroles, au-dehors une trompe sonna plusieurs coups répétés, attirant le regard des trois hommes vers la fenêtre, dont les volets délicatement ouvragés avaient été ouverts. Rapidement, l'effervescence gagna la cour. Un sergent cria :

« Aux armes ! Aux armes ! »

Régnier n'attendit pas les instructions, il savait ce qu'il avait à faire. Après un salut silencieux, il s'engouffra dans les escaliers, rejoignant à pas rapides son valet, qui devait avoir préparé son fourniment de guerre.

« Biens vaillants, pour des assiégés démoralisés ! » pensa-t-il en son for intérieur.

### Village de Rabwé, soir du lundi 26 juillet

Les flammes des lampes dessinaient des combats d'ombre sur les murs de toile. Avachi sur son siège, face à plusieurs autres qui attendaient, anxieux, ce qu'il allait leur dire, le connétable Manassès réfléchissait. Tous arboraient encore le harnois de guerre, haubert de mailles, baudrier et épée sur la cuisse. Les visages fatigués, collés de sueur et de poussière, n'avaient été que grossièrement lavés, et les rides qui creusaient leurs traits n'étaient pas que de vieillesse. Manassès tenait à la main un document de papier plié que le dernier arrivant, un jeune coursier aux yeux caves, la mine défaite, venait de lui porter. Il s'empara d'un gobelet de verre émaillé et, d'une gorgée, s'éclaircit la voix rendue rauque par l'inquiétude.

« Anur n'a pas menti, le soudan d'Alep[^74] nous court sus. Il a descendu l'Oronte à marche forcée et se trouve sûrement dès avant Hims. »

La colère faisait trembler ses mots. Il se tourna vers le clerc lui servant de secrétaire et le congédia d'un geste impatient. Il répéta le mouvement à l'adresse du messager, puis invita les trois hommes demeurés là à se rapprocher de lui. Il reprit d'une voix moins forte.

« Ce démon païen joue avec nous, je ne le crois pas assez fol pour risquer de voir les Turcs devenir maîtres chez lui. Mais leur arrivée mettrait à mal nos positions, de certes.

--- Nous sommes fort exposés, et devons nous opposer à la citadelle depuis ces lieux, sire. Déplaçons-nous plus au sud, et assaillons la ville depuis là. Nous n'y serons pas proie facile, la ville sera entre nous et les Turcs.

--- Le roi pourrait y consentir, mais Conrad et Louis[^75] ne verront certes pas cela d'un bon oeil. Ils frémissent déjà de colère de ne pas avoir encore pris la cité !

--- Pour se la partir entre eux, comme si nous n'étions pas là. J'ai ouï dire qu'elle serait destinée au comte Thierry[^76]. N'a-t-il pas déjà assez de terres en Flandre ?

--- Silence, foin de ces jérémiades ! » tonna le connétable, agacé.

Il se leva, commençant à faire les cent pas tout en grondant et vociférant à mi-voix. La campagne ne s'avérait pas aussi facile qu'escomptée, malgré l'arrivée des forces armées venues d'Europe. Il se tourna vers Elinand, prince de Galilée, qui patientait tranquillement, le regard dans le vague. C'était un homme d'action, capable des coups de main les plus audacieux lorsqu'il était en selle, mais n'aimant guère à se perdre en longs palabres. Il était un partisan fervent de la reine Mélisende, dont Manassès était le plus sûr soutien.

Les deux hommes pouvaient parler à coeur ouvert de leurs projets pour éviter que le jeune Baudoin[^77] ne prenne trop d'assurance dans cette campagne. En outre, en tant que seigneur des terres les plus proches de Damas, il était certainement un des plus capables militairement parmi les conseillers auxquels le connétable pouvait se fier.

« Que t'inspirent ces nouvelles, Elinand ?

--- Je ne saurais dire que cela m'enjoie le cœur, sire, mais nous devons faire avec. Il est de meilleur choix de garder Anur à nos portes que de livrer Damas à Norredin. »

Le connétable plissa les yeux de ruse, reprenant place dans son siège.

« Que ferais-tu à ma place, donc ?

--- Le sire Anur a offert de reconnaître notre suzeraineté sur Panéas, acceptons cela, déjà. Puis voyons ce que nous pouvons exiger outre. Il sera bien temps de revenir autre fois.

--- Jamais le roi Louis n'acceptera de se retirer ! »

Le prince de Galilée soupira, excédé.

« Si l'ost de l'émir n'est qu'à quatre ou cinq jours comme le dit votre message, cela ne nous laisse guère de temps pour prendre la cité et la fortifier de nouvel. Nous n'arrivons à rien face à la citadelle et n'empêchons guère renforts d'entrer par le nord, ou l'est.

--- Le sud alors, voila ton conseil ? »

Le chevalier haussa les épaules.

« De prime, il faudrait voir comment se présentent les choses. Ici, en les jardins, nous sommes protégés des ardeurs du soleil, et avons plein fourrage pour hommes et bêtes. Si nous faisons route, ce sera pour assaillir à grand force et prendre la cité vite, et bien, sans quoi nous serons en fâcheuse posture. »

Le dernier homme, Bernard Vacher, avança d'un pas, hésitant à formuler ce qu'il avait en tête. Il l'énonça avec difficulté alors même que cela prenait corps dans son esprit.

« Nous avons tenu conseil au moment des Pâques, sire. Aussi ne saurais-je croire que le seigneur Anur n'a pas eu vent de nos préparatifs. Il aurait pu appeler à l'aide bien plus tôt. Je crois qu'il nous préfère comme voisins que les Turcs. Rappelez-vous qu'il n'a pas cherché à détruire l'ost lors de la rébellion d'Altuntash[^78].

--- C'est oublier bien vite que tu as failli, et que nous n'y avons rien gagné.

--- Peut-être qu'il a pu éprouver déjà le poids du joug turc sur ses épaules à cette occasion. Et ne l'a point goûté », répliqua l'autre, indifférent à la rebuffade.

Le connétable acquiesça sans conviction. Bernard Vacher était un grand connaisseur de l'âme des musulmans. Il avait mené de nombreuses ambassades auprès de Damas. Parfois, il se disait qu'il était peut-être trop familier avec eux, et des rumeurs circulaient comme quoi il ne dédaignait pas les dinars d'or que le seigneur de Damas distribuait avec prodigalité. Il gardait pourtant la faveur de Mélisende et du connétable, et ne s'était jamais montré déloyal.

Manassès avala encore un peu de vin et mit fin à leur entretien. Il demeura un long moment, seul, à méditer sur la décision à prendre. Il s'agissait de bien choisir parmi les différents chemins qui se traçaient devant lui. Il était là pour conseiller le roi et l'influencer au mieux de ses intérêts, mais il était surtout un fidèle de la reine. Si elle pouvait apprécier une victoire sur les Damascènes, elle ne priserait guère que son fils puisse en tirer gloire, et s'affirme alors indépendant de son pouvoir.

Organiser une défaite révoltait tout son être, mais contribuer à la réussite ne devait se faire qu'avec doigté, afin que le mérite en revienne à d'autres que Baudoin. Familier de la cour française, il se dit qu'il devrait souffler l'idée de faire mouvement au sud à quelque baron de l'entourage du roi Louis. Ou plutôt, moins suspect de complaisance envers les musulmans, à un seigneur ecclésiastique. Quelqu'un que jamais personne ne soupçonnerait de vouloir déplacer l'attaque pour une autre raison que de meilleures chances de succès. Il appela un valet : il allait demander audience auprès de l'évêque Geoffroy de Langres, un des intimes du fameux Bernard de Clairvaux[^79].

### Oasis sud-ouest de Damas, matin du 29 juillet

Le campement abandonné ressemblait au squelette de toile et de bois d'un grand animal décomposé. Déjà, telles des fourmis patientes, les unités musulmanes reprenaient sans faillir les jardins, investissaient zone après zone, s'abritant derrière les remparts durement assemblés pour se protéger d'eux. À l'ombre d'un bosquet de palmiers, Ganelon finissait de ramasser des sacs et entreprit de caler un blessé sur sa mule lorsqu'une voix forte tonna dans son dos :

« Fais mouvement, bougre de ribaud, ou tu finiras le col percé d'un trait sarrasin ! »

Le chevalier qui l'avait invectivé poussait sa monture transpirante, soulevant graviers et poussière dans son agitation. Son écu orné de fers plantés, de fûts brisés, attestait de la proximité des archers musulmans. Ils étaient les derniers à partir, l'amertume empoisonnant leur âme. Ganelon frappa de la trique son animal et avança au petit trot rejoindre le gros du bagage.

Un peu hébété, il avait le regard éteint des hommes qui croyaient avoir tout perdu et qui venaient d'être spoliés une nouvelle fois. Il avait marché des mois durant, vu ses amis mourir, renoncé à tout pour se trouver là, aux côtés de son seigneur Régnier d'Eaucourt. Tout cela en pure perte. La fière armée croisée avait échoué. Elle n'avait pas tenu plus de quelques jours, à tenter de prendre une misérable cité abritée dans ses jardins, au milieu du désert. Son âme se grossissait de fiel tandis qu'il s'éloignait, baissant la tête sous les cris, bousculant ses compagnons, trébuchant sur le sol caillouteux mille fois malmené devant lui. Qu'elle avait goût de poussière, dans sa bouche, la Terre promise. Il cracha une salive épaisse, fronçant les sourcils et tira un foulard devant son visage. Tandis qu'il trottinait, inquiet, hagard, affolé, il ne cessait de se répéter :

« Honnis soient ces Orientaux et leur pays maudit ». ❧

### Notes

L'assaut sur Damas lors de la Seconde croisade constitue toujours une gageure à expliquer. Les relations, bien que tendues, entre les deux puissances (Damas et Jérusalem), se maintenaient plus ou moins stables, et aucune des deux n'avait envie de voir les forces aleppines (Nur al -Dîn) venir si loin dans le sud. Néanmoins, la pression étrangère finit par pousser à l'attaque, peut-être relayée par le désir du jeune Baudoin III de briller au cours d'une campagne militaire dont sa mère serait forcément exclue. Toujours est-il que, même si on arrive à comprendre les raisons qui ont mené les armées chrétiennes dans l'oasis, les hypothèses demeurent nombreuses quant aux motivations ultérieures.

Après des combats très difficiles, où la victoire n'était jamais franche, le siège fut rapidement abandonné au bout de quelques jours, peut-être sous la menace des troupes nord-syriennes et en raison d'un manque de ravitaillement. La retraite se déroula en bon ordre et Damas versa par la suite régulièrement un tribut pour s'épargner un nouvel assaut.

Néanmoins, le désastre moral entraîna le délitement des forces croisées et aucun succès militaire d'envergure ne couronna la venue des deux puissants souverains européens (Louis VII de France et Conrad d'Allemagne), bénie par saint Bernard lui-même. Cet échec pesa lourdement sur les consciences, à comparer aux victoires de leurs prédécesseurs, et l'esprit de croisade ne fut désormais plus jamais le même.

### Références

Élisséef, Nikita, *La description de Damas d'Ibn 'Asâkir*, Institut Français de Damas, Damas : 1959.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972, p.93-182.

Nicolle David, Kervran Yann (trad., adapt.), *La Seconde croisade et le siège de Damas en 1148 - Un pari manqué*, à paraître.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Malquisinat
-----------

### Quartier du palais royal, Jérusalem, fin de matinée du lundi 3 mars 1158

Dans la rue donnant accès à une des portes du palais royal, une foule bigarrée s'avançait mollement. Le brouhaha des discussions se répercutait entre les hauts murs dominés par le clocher du Saint-Sépulcre, sous le regard curieux des pigeons et de quelques chats en maraude sur les toits. Le soleil n'avait pas encore vaincu la fraîcheur de la pierre, et dans l'ombre bienvenue, chacun y allait de son commentaire à propos de la séance de la Cour des Bourgeois. Une poignée de citadins, joyeux de s'être vus accordé ce qu'ils demandaient, parlaient fort et riaient avec ostentation de leur bonne fortune. Certains, épaules rentrées, visage morne, ruminaient leur détresse, leur colère.

D'autres, enfin, glosaient sur les décisions rendues, les informations révélées, avec compétence, amusement ou surprise indignée. Parmi ces derniers, un petit groupe d'hommes à l'allure opulente bruissait de mécontentement scandalisé. Un rouquin, le cheveu en bataille et les traits fins, le nez pareil à un bec, avait la voix acerbe :

« Ce n'est pas tant le montant que le principe ! »

Un de ses compagnons, le visage mangé de barbe drue, bien taillée, les dévisagea de ses yeux globuleux et ajouta, en hochant la tête d'un air sentencieux :

« Je vous l'avais bien dit, ce béjaune a gueule d'ogre et ne sait que gloutoyer !

--- Tout de même, maître Turcame, c'est le roi ! objecta un gros homme accablé, à moitié asphyxié de parler en marchant.

--- Et quoi ? Le roi n'est pas Dieu que je sache. Sa mère n'aurait jamais osé faire pareille chose. »

Le petit groupe acquiesça lentement, tout en demeurant craintif de le faire aussi près du palais de Baudoin III[^80]. La vieille Mélisende[^81] s'était retirée à Naplouse à l'écart de la chose publique et ne comptait plus guère de soutiens au sein de la Haute Cour. Parmi les habitants de Jérusalem, nombreux étaient ceux qui la regrettaient, elle qui avait su si bien se concilier leurs bonnes grâces, ne ménageant pas sa peine pour ses chers bourgeois.

Un petit méditerranéen au cheveu noir et rare, la barbe raide, haussa les épaules.

« Je ne peux néanmoins donner tort au roi, certaines rues sont fort sales et les puanteurs en la saison d'été sont terribles.

--- De cela je veux bien convenir, maître Tosetus, mais c'était affaire à entreparler en Cour avant d'en prendre décision, répliqua Odo de Turcame. Bien sûr qu'il faut curer venelles et courettes, et d'autant mieux pour les Pâques, avec l'affluence de marcheurs de Dieu. De cela tout le monde s'en accordera. Mais je n'aime guère les façons de faire de Baudoin, qui ne respecte pas nos coutumes.

--- Nous devrions en discuter avec nos compères jurés et peut-être aborder l'affaire avec le sire vicomte à une prochaine assemblée plénière ? » proposa le gros homme.

L'idée ne souleva pas l'enthousiasme, mais aucun n'y objecta. Ils obliquèrent à gauche et continuèrent d'avancer, passant sans même y jeter un oeil devant l'entrée du Saint-Sépulcre, le parvis le plus vénéré de la chrétienté. Ils serpentaient au travers de la rue emplie de pèlerins, de croyants au regard exalté, mais sans guère y prêter attention. Ils parcouraient ces lieux depuis tant d'années qu'ils en étaient arrivés à en oublier le caractère merveilleux, pour l'ensemble du monde chrétien.

Alors qu'ils se faufilaient devant un marchand de cierges qui accrochait des faisceaux de chandelles sur ses panneaux, l'homme aux cheveux roux grogna :

« D'où lui est venu cette soudaine idée, d'ailleurs ? La cité ne me paraît guère plus sale qu'habituellement...

--- Peut-être a-t-il eu envie d'aller faire balade vers la porte des Tanneurs, s'amusa Turcame. Les effluves en sont certes ignobles !

--- Il est rentré voilà quelques jours à peine de la chevauchée au nord avec le sire Thierry[^82], il s'est habitué à la vie sous la tente et ne supporte plus ce qui lui paraissait bien normal jusqu'alors. »

À l'évocation du nom du seigneur croisé, Odo de Turcame fit la grimace et une lueur de compréhension brilla dans ses yeux.

« Je suis acertainé que c'est là mâle influence du sire comte de Flandre !

--- Et pourquoi donc ? s'interrogea André de Tosetus.

--- Tous ces barons d'outremer aiment à tenir le pouvoir bien serré et ne lâchent que de mauvaise grâce la décime part de ce que nos souverains ont toujours considéré normal de nous accorder. »

Il s'arrêta, considérant d'un geste large la rue, les boutiques, le quartier et au-delà, la cité tout entière.

« Tous nos compères l'auront fort mauvaise de voir neuve taxe leur être extorquée, c'est là mauvaise coutume qui s'installe !

--- La Secrète[^83] a toujours grand faim de monnaies et n'aime rien tant que nouveaux moyens de les faire venir à elle.

--- Voilà bien ridicule moyen en tout cas. Ce n'est pas en condamnant à payer ceux qui escurent mal devant leur hostel qu'on fera venir les gens. Sans borgesies bien tenues, pas de loyer, sans commerce pas de péages... C'est compte de benêt de penser qu'il y a là bonne source. Celui qui a soufflé pareille idée au roi n'a que du vent entre les oreilles ! »

Sa fureur avait fait tourner les têtes dans sa direction et certains s'inquiétaient de ce qui pouvait ainsi passionner les jurés qui les représentaient auprès du roi. Sous peu, la ville allait bruire de nouvelles, aussi farfelues qu'imaginatives, sur ce qui avait bien pu se dire lors de la séance. Le gros homme passa une manche sur son front transpirant, invitant de la main ses compagnons à avancer de nouveau.

« Venez donc boire quelque vin de Judée à mon logis. J'en ai reçu tonnels voilà peu, et il saura apaiser la flamme de votre courroux. Tant que la nouvelle n'est pas criée, nous avons encore bon temps pour voir si nous ne pouvons convaincre le roi de revenir sur cette décision.

--- Il nous faudrait l'oreille du sénéchal peut-être. Si on lui démontre qu'extorquer par mal usage sept sous et demi à aucuns bourgeois en fera perdre bien plus au trésor, il comprendra vite qu'il n'y a rien à gagner à appliquer pareille décision. » expliqua le petit homme, les yeux rieurs.

Le rouquin acquiesça avec vigueur, soudain calmé.

« Voilà fort bonne idée, maître Tosetus. Le roi n'a pas tant pillé à Panéas ou récemment à Damas pour risquer de perdre fortune juste aux fins d'impressionner aucun baron étranger de passage. Il nous faut lui en faire montrance ! »

Tandis que leurs élégantes tenues se perdaient dans le flot ininterrompu de curieux, d'exaltés, de marchands, de valets, de pèlerins, qui emplissait la rue, les riches bourgeois souriaient de satisfaction. Tels des généraux à l'approche d'une cité enclose, ils venaient de voir des failles en la muraille ennemie et savaient désormais comment lancer l'assaut. Il ne serait pas dit qu'ils laisseraient un jeune roi leur retirer les privilèges durement acquis par leurs aînés.

### Malquisinat, Jérusalem, matin du mardi 4 mars 1158

Les mains sur ses larges hanches, Margue réfléchissait, les yeux traînant sur l'étagère où elle entreposait la farine. Elle se tourna vers son valet, un jeune homme à l'air perpétuellement étonné, les oreilles en chou-fleur. Grignotant un morceau de fouace sèche avec l'application d'une vache, il attendait les instructions.

« Porte à moudre encore deux boisseaux de froment, du plus fin. Ce jour nous devrions avoir bel ouvrage, c'est le dernier jour charnel, il faut en profiter. Oublies et brioches devraient bien se vendre. »

Une fois son serviteur parti, elle entreprit de balayer le sol de sa petite boutique. Elle tenait une échoppe près du carrefour principal de Jérusalem, proposant à la vente des plats à emporter, des pâtisseries, des beignets, tartes et confiseries. À l'approche de Carême, les gens profitaient des derniers jours pour faire bombance et elle réalisait alors ses meilleures affaires de plusieurs semaines. Il existait bien des recettes savoureuses acceptables pour les jours de jeûne, mais le commerce s'en ressentait malgré tout, en dépit de l'afflux régulier et grandissant de pèlerins jusqu'à Pâques.

Pour l'heure, en milieu de matinée, elle s'estimait satisfaite, ayant vendu une large partie des produits restant de la veille. En outre, une fournée avait été portée au four du Saint-Sépulcre non loin. Elle profitait de l'accalmie pour remettre de l'ordre dans son échoppe où son imposante silhouette se faufilait avec habitude. Une fois la vaisselle terminée, elle vida son seau d'eau dans le caniveau, d'un geste ample qui aspergea les environs. Quelques badauds surpris froncèrent les sourcils, inquiets d'avoir été éclaboussés.

Au moment où elle reprenait place derrière son éventaire, elle fut apostrophée par un boutiquier voisin, vendeur de pâtés et tourtes de viande.

« As-tu entendu parler de la nouvelle taxe de sept sous et demi, l'Allemande ? »

Elle s'essuya le front de l'avant-bras, penchant son buste de matrone pour voir son compère.

« De quoi me parles-tu ?

--- Le roi veut nous faire payer nouvelle fois, les gens de la Secrète ont trouvé bonne idée pour nous prendre encore.

--- Et pourquoi donc cela ? Et tous les combien ? »

Le voisin s'avança dans la rue. De petite taille, il avait les pommettes et le nez violet des amateurs de vin, les yeux bridés à force de les plisser. Il passait une main nerveuse dans des cheveux grisonnants coupés courts.

« Ce serait amende si la rue est trop sale au-devant de notre échoppe !

--- Nous veut-il devenir cureteur de ruisseau ? Ce n'est pas à nous d'escurer pareil lieu. »

Le boutiquier n'attendait que cette occasion pour laisser sa colère éclater et sa voix monta dans les aigus tandis qu'elle prenait de la vigueur. Bientôt son visage devint intégralement cramoisi.

« De certes ! Nous avons déjà fort à faire en nos ouvroirs, si en plus nous devons surveiller rues et passages ! Nous payons cens, il me semble que c'est déjà assez. Le Saint-Sépulcre ou les frères de Saint-Jean n'ont qu'à faire passer les porcs plus souvent. Ils ont pleins dortoirs de valets gras comme chapons, qu'ils les envoient gratter venelles ! »

Margue opina du menton. Elle suivait néanmoins du regard les passants, espérant toujours que certains s'arrêtent. Désireuse de ne pas les effrayer par des discussions agressives, elle se rapprocha du petit homme appuyé contre la colonne qui séparait leurs deux étals.

« Les Bourgeois du roi ont laissé faire ? Ne sont-ils pas là pour nous protéger ?

--- Pfff, ils ne sont bons qu'à s'aider eux-mêmes. Pas un n'a jamais trimé de sa sueur. Ils achètent aux Mahométans pour revendre aux Génois, peu leur chaut nos soucis de boutiquiers. Ils pétrissent livres et marcs[^84] comme nous farine et oeufs.

--- Détrompez-vous, maître, ils sont fort courroucés de cela et comptent bien en reparler en conseil », objecta une voix féminine, grave et et douce.

Les deux commerçants se tournèrent de concert vers la femme qui, un panier au bras, examinait les marchandises offertes à la vente par Margue. Elle était vêtue d'une robe de laine rouge de bonne qualité, ancienne, mais bien entretenue, un voile lâche posé sur ses cheveux d'encre. Le visage aussi digne que celui d'une princesse byzantine, des plis marquaient ses yeux et sa bouche fine, prompte à donner des ordres. Aloys était connue pour son établissement de bains, où les jeunes filles n'étaient guère farouches. Elle leur sourit poliment, se rapprochant pour ne pas avoir à élever la voix.

« Certains des bourgeois du roi pensent que pareille ordonnance n'a nulle valeur, étant donné qu'elle n'a pas été prise avec leur aval.

--- Quelle mouche a bien pu piquer le sire roi pour s'inquiéter ainsi des caniveaux ? N'a-t-il pas plus graves soucis avec les Turcs qui se baladent aux alentours de Damas ? »

La petite femme rouge et la grande au physique empâté haussèrent les épaules en même temps. Margue renifla et répondit la première :

« Il suffit qu'il ait senti mauvaise odeur monter de la rue au près du palais, et il s'en sera ému... Difficile de savoir ce qui motive puissants barons. Ils ne sont pas comme nous.

--- En attendant, je ne vois pas comment nous pourrons nourrir nos familles si on nous prend par taxes et cens la moindre monnaie par nous gagnée. Un jour, c'en sera trop et... »

Il abattit sa main dans son poing, fendant comme un couperet, le visage empli de colère. Puis, hélé par un chaland, il repartit à sa boutique sans s'arrêter de grommeler. Les deux femmes échangèrent un regard circonspect et reprirent le cours de leurs activités après un salut discret.

### Rue de David, près du change, début de soirée du jeudi 3 avril 1158

Suivant une effluve intéressante, le chien avançait sans prendre garde aux jambes, aux pieds qui le bousculaient, le poussaient, le heurtaient. Il était habitué à se faufiler parmi les hommes et les femmes de la cité. Il connaissait les odeurs des porteurs de triques, s'arrangeait pour les éviter avant même de les voir. Le poil gris, légèrement frisé, il avait le ventre creux des bêtes vagabondes et un œil perdu lors d'une bagarre pour de juteux déchets.

Cela faisait maintenant des jours et des jours qu'il errait dans le quartier où il savait la nourriture abondante. Il fallait souvent la disputer aux gros animaux grogneurs, mais dans l'ensemble il était plutôt satisfait de lui. Parfois il obtenait des restes frais, jetés par quelque main compatissante, et même une caresse fugace de temps à autre. Il aimait bien les gamins, toujours prêts à jouer avec lui, à courir, sauter en riant. Pour les grands, les adultes, il avait développé une technique bien à lui, mélange de regards implorants et d'attitude digne, les pattes avant croisées, la tête penchée, la langue léchant les babines régulièrement. Pour l'heure, il avait repéré deux proies dont il espérait bien tirer profit. Il s'installa donc entre elles deux, à l'abri d'un renfoncement du mur et commença son entreprise de séduction silencieuse.

« Le fort souci de toutes ces chevauchées, c'est le nombre d'espies parmi nos gens. Noreddin[^85] est informé de tout avant même que les hommes soient en selle.

--- Le roi a fort compris la leçon de Meleha[^86], Fouques, et pourpense à tout cela. »

Les deux hommes étaient habillés de belles tenues. Le premier, d'une cotte de laine souple, d'un vert tendre, rehaussé de bandes brodées par endroit. Ses chausses, ses souliers, le baudrier et la poignée de son épée étaient à l'avenant, d'excellente qualité sans ostentation. Le vicomte Arnulf ne succombait guère au luxe, mais savait tenir son rang. Il échangeait avec un jeune homme, la barbe courte taillée en pointe, les cheveux mi-longs, un nez en trompette jaillissant d'entre ses joues flasques. Il n'avait pas aux chevilles les éperons d'or des chevaliers, mais portait beau lui aussi : cotte de laine orange, chausses souples, ceinture et fourreau clairs agrémentés de décors métalliques. Ils discutaient, assis sur un banc de pierre, tout en avalant un peu de vin, non loin de la troupe de sergents qui se restauraient parmi les boutiques de Malquisinat. Arnulf s'étira, grognant.

« Le prince Guillaume[^87] pense qu'il serait sage de fortifier plus à l'ouest de Saphet, indiqua le sergent, pointant du doigt sur une carte imaginaire tracée entre eux deux.

--- Ce serait fort bon lieu, approuva le vicomte, avec Panéas[^88] qui garde le pied du mont Hermon. Mais l'ouvrage me parait moult ardu : il y a sept ou huit lieues de Saphet au Jourdain, et le passage est prisé par la cavalerie turque. Avec Belfort au nord qui les contient, ils se faufilent par là jusqu'en Galilée où rien ne les retient plus. »

Il avala une gorgée de vin, dubitatif.

« Je crains fort que nous devions encore et toujours batailler en ces lieux, tant que la cité de Damas ne sera pas nôtre... »

Voyant son verre vide, il le tendit au soldat, qui se leva pour aller le rendre au tavernier. Pendant ce temps, Arnulf fit quelques pas, flattant d'une main légère le chien efflanqué. Comprenant finalement qu'il n'y avait aucune nourriture à gagner, ce dernier s'éloigna rapidement, la queue entre les jambes, déçu. Un groupe d'acheteurs libéra alors la devanture d'un marchand d'oublies, de pâtisseries et la vision des tartes et beignets fit saliver le vicomte. Il s'approcha, parcourut des yeux l'éventaire, choisissant quelques friandises à avaler avant de se remettre en selle pour la ronde du guet. Derrière le plateau, le vendeur souriait d'un air professionnel, un peu intimidé de servir si important personnage. Une fois l'affaire conclue, il empocha les piécettes et se pencha, comme pour parler, mais sans s'y risquer. Le vicomte l'invita d'un geste à s'exprimer, tout en mordant dans la première pâtisserie aux fruits.

« Je voulais vous demander, sire vicomte, si la rumeur est vraie qu'il va nous falloir payer pour escurer les rues. »

Arnulf fronça les sourcils, contrarié que la nouvelle se propage ainsi, aussi vite et aussi mal. Il secoua la tête en dénégation.

« Non. Le roi a décidé de mettre à l'amende ceux qui ne nettoieraient pas au-devant de leur hostel.

--- Ah... échappa le marchand, le visage morne.

--- C'est pour le bien de tous. En certaines venelles, les immondices sont telles qu'on ne peut y marcher sans risquer de tomber au parmi de la merdaille ! Il n'est tout de même pas si compliqué de faire venir charette ou mulets pour issir tout cela hors les murs.

--- De vrai. Mais nous sommes modestes œuvriers et savoir que l'amende s'adresse à nous appuie le fardeau en nos épaules. Peut-être serait-ce aux propriétaires des lieux de s'acquitter de la taxe. N'avez-vous point envisagé cela au conseil ? »

Arnulf soupira.

« Rien n'est encore décidé de toute façon, mais ne serait-ce que pour le commerce, il est bien plus sage de tenir sa rue propre et nette. Bien rares ceux qui aiment à manger les pieds dans le fumier, non ? »

Ne laissant pas le temps à son interlocuteur de répondre, il s'éloigna et rejoignit son cheval et les sergents, qui finissaient d'avaler leur repas en discutant joyeusement. Son air renfrogné inquiéta l'un d'eux, un petit brun au visage contracté. Il vint tenir l'étrier au chevalier pour qu'il se remette en selle.

« Quelque souci, messire vicomte ?

--- Certes oui, Baset, il s'en trouve toujours pour demander plus, mais aucun n'est jamais prêt à porter la main en sa bourse. Tout pour rien, voilà ce qu'ils veulent tous ! »

Ne comprenant pas où voulait en venir le vicomte, le sergent fit un signe de tête à ses compagnons, accompagné d'un sifflet rapide. Il était temps de commencer la ronde de nuit.

### Église Saint-Sauveur, Jardins de Gethsémani, après-midi du lundi 7 avril 1158

Le glissement des pas légers sur les dalles avait cédé la place aux raclements sur le sol de gravier et de poussière. Les visages tristes, les épaules basses, franchissaient la porte en silence, dans les volutes d'encens répandues par de jeunes clercs. Le vieux Tosetus, dont les fils André et Rohard avaient repris fièrement le commerce, avait été en son temps un des notables les plus puissants de la cité de Jérusalem. Homme d'action, prompt à la colère, mais dévoué aux siens, il s'était lancé en politique très tôt, et avait fini par obtenir une grande influence à la Cour des Bourgeois. Il avait été un fidèle du roi Foulque[^89] puis de la reine Mélisende dont il n'avait jamais accepté la mise à l'écart, qui avait entrainé la sienne propre. Depuis plusieurs années, il se tenait loin des célébrations officielles, des intrigues, et savourait la vie qui s'écoulait depuis son petit jardin aux abords de la porte Saint-Étienne. C'était là qu'il s'était effondré brutalement, trois jours plus tôt, en partie paralysé jusqu'à sa mort quelques heures plus tard.

Généreux donateur, il avait obtenu le privilège d'être enterré auprès de l'église qu'il avait richement dotée de son vivant, sur les contreforts du Mont des Oliviers. Des messes seraient régulièrement célébrées pour le repos de son âme. Après une ultime bénédiction et l'inhumation du corps, quelques proches, venus soutenir une dernière fois la famille endeuillée, s'étaient retrouvés autour de l'officiant. Le visage grave de circonstance, il semblait que ses traits avaient été figés dans cette mimique mi-austère, mi-attristée, qui convient en toutes circonstances à un ministre du culte empli du sérieux de sa fonction. Pour le moment, il hochait la tête gravement tandis que les présents énuméraient les hauts faits du récent défunt, forcément innombrables. André, le plus jeune des fils, était le plus prolixe sur le sujet.

« Je suis aise de savoir qu'il a pu rendre son âme à Dieu en toute paix, il a bien mérité du Paradis.

--- Sa générosité était de bonne fame, mon fils, et cela comptera aussi au jour du Jugement, répliqua le clerc d'une voix monocorde.

--- S'il y a certeté en cette terre, c'est bien qu'il a sa place au sein des Anges. »

Tout le monde hocha la tête avec solennité à l'affirmation péremptoire, jusqu'à ce que le prêtre ose timidement ajouter :

« Il n'est jamais inutile de dire prières et messes pour l'âme des vivants et des morts, quelles qu'aient été leurs bienfaits, mon fils.

--- Malgré toutes ses largesses, il n'y a donc pas garantie de Paradis pour lui ?

--- Nul n'est sûr de rien en ce bas Monde. Il nous faut espérer en la bienveillance divine, et se tenir prêt à la seule vérité connue de nous tous, l'amour de Dieu.

--- Nulle certitude outre l'Amour de Dieu? », s'inquiéta André, le visage tendu.

« L'amour de Dieu... et les taxes ! » plaisanta son confrère Odo en lui tapant sur l'épaule, un sourire évanescent sur les lèvres. ❧

### Notes

*Malquisinat* désigne le quartier de Jérusalem où l'on trouvait beaucoup de commerces de bouche. L'étymologie, difficile, a parfois évoqué les relents de cuisine qui devaient s'y répandre, curieusement affublés d'une connotation négative. J'ai choisi de m'amuser de cette interprétation et de parler d'une autre cuisine, bien souvent indigeste, concernant la gestion de la chose publique.

Le gouvernement des territoires en Terre sainte ne se faisait pas de façon aussi centralisée que le schéma théorique pourrait le laisser croire. De nombreuses tensions existaient entre les différents groupes sociaux, jaloux de chacun conserver ses privilèges, défendus par leurs membres les plus éminents. Même les bourgeois, dont on a tendance, au choix, à sous-estimer le rôle en Europe ou à peut-être en surestimer l'importance outremer, bénéficiaient d'un statut reconnu. La reine Mélisende s'était d'ailleurs attiré un fort soutien de leur part, demeurés fidèles dans le conflit qui l'opposa à son fils Baudoin, tandis que la noblesse s'avéra plus volage (ou plus malcommode à châtier).

La création de l'amende de nettoyage des rues a dû engendrer un certain nombre de problèmes, car le manuscrit qui en consigna l'existence, des dizaines d'années plus tard, rappelait que cela s'était fait sans le consentement de la Cour des Bourgeois. Le vicomte était donc incité à la clémence dans son application. Même si le besoin de salubrité apparaissait à tous, personne n'était prêt à devoir en supporter la charge. Je vous propose de lire la retranscription du comte Beugnot au XIXe siècle, qui a inspiré « Malquisinat ». Il est curieux de voir comme un texte législatif peut acquérir de la saveur avec les ans et venir chatouiller l'imagination :

> **Chapitre CCCIII** *Ici dit la raison de l'escouver de la ville et des rues, coument hom doit faire par raison.*\
> Bien sachés que la raison ne prent mie à droit nus de VII. sos et demy d'escouver les rues, por ce que li rois Bauduins y mist ces establissemens sans le conseill de ses homes et de ses borgeis de la cité. Et por ce coumande la raison et l'assise que puis l'on a fait crier le banc par la vile c'on esnete les rues, et aucun home ou aucune feme faut à celuy banc et qui riens ne fasse net devant son hostel, la raison juge que le vesconte det aver de ce mesfait mout grant pitié, si que prendre ne deit que au mains il pora, et deit pardouner ces VII. sos et demy par pitié.

### Références

Le comte Beugnot, *Assises de Jérusalem. Tome II. Assises de la cour des Bourgeois*, Paris: 1843, p.225.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972, p.93-182.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Prawer Joshua, *Crusader Institutions*, New York : Oxford University Press, 1980.

Mêh
---

### Plateau du Golan, fin d'après-midi du mardi 12 février 1157

La poussière soulevée par les troupeaux agités embrumait les buissons gris. Hommes et bêtes respiraient de la craie, la face poudreuse collée de sueur. Cris, hennissements, bêlements, insultes, hurlements. Le chaos s'était abattu sur les paisibles pâturages au nord-est de Baniyas. Là-dessus, un ciel bleu acier hébergeait un soleil triste, sans chaleur. Un étalon paniqué roulait des yeux affolés, dressé sur ses postérieurs, prêt à frapper de ses sabots quiconque s'approcherait de lui. Des moutons à queue grasse, imbéciles et inquiets, tentaient de se faufiler entre ses jambes et celles de ses compagnons, indifférents aux coups. Plusieurs soldats, les bras écartés, s'efforçaient de les regrouper en sifflant, hurlant, ajoutant à l'effervescence.

Il fallut un long moment avant que le plateau retrouve un peu de calme. Une centaine de têtes de bétail avaient été rassemblées près d'une zone buissonneuse et la présence de nourriture avait fait miracle. Une quinzaine de chevaux de belle allure, dont l'étalon, étaient surveillés par une troupe d'une vingtaine d'hommes, le front las. De temps à autre, un cavalier en arme venait donner des ordres, emmenant avec lui une poignée de fantassins pour une mission. Près des moutons, assis autour d'un rocher, plusieurs soldats soufflaient, partageant l'eau croupie mêlée de vin de leurs outres, massant les corps fatigués, nettoyant leurs ecchymoses ainsi que font les loups après la chasse. L'un d'eux arborait une ancienne cicatrice au visage et parlait avec un fort accent génois. Il n'était pas dans la troupe depuis longtemps et s'était pourtant déjà taillé une belle réputation. Tout en se reposant, il graissait la corde de son arbalète, frottait et rangeait les carreaux qu'il avait récupérés après quelques tirs. Un de ses compagnons lui tendit la gourde, regardant les nombreuses brebis en train de paître à leurs côtés.

« J'espère qu'on pourra s'en gloutoyer une tantôt, après tous nos efforts. J'ai si grand faim que je pourrais avaler l'étalon !

--- Le capitaine n'a rien dit en ce sens. Le butin est au roi et à ses féaux. T'es pas baron que je sache. »

Un sourire hargneux fendit la face craquelée du soldat.

« Si une ouaille a grande navrure, elle ne saura marcher jusqu'à nos terres. On pourrait la manger pour ne pas perdre sa viande.

--- Certes, s'il s'agit de ne pas gaspiller, c'est bien différente chose ! »

Le petit groupe s'esclaffa. Ils savaient que le butin était considérable : au moins des centaines de moutons, de chèvres et de chevaux avaient été pris, sans compter les nombreux captifs. Quelques Turcomans avaient tenté de résister les armes à la main, vite abattus. La bataille s'était résumée à une moisson grasse sans que le bras n'ait guère à se fatiguer à faucher.

Au milieu du groupe, appuyé contre le rocher, un garçon au poil brun se rinçait le flanc avec le contenu de son outre, tout en grimaçant, le visage en sueur. Le rouge de son sang se mêlait à la boisson et il tentait d'en endiguer le flot en y appliquant un chiffon sale, vite imbibé. À côté de lui, des vêtements déchirés, souillés, voisinaient avec d'autres, prêts à être utilisés. Il serrait les dents, les yeux fébriles tandis qu'il compressait la plaie.

L'arbalétrier se tourna vers lui, curieux, examinant la blessure.

« Sans emplâtre, ça ne s'arrêtera pas de saigner, compère...

--- Je sais bien, Maza, seulement il n'y a rien ici pour ça » croassa l'autre.

Se penchant plus près, Enrico Maza souleva doucement le linge. Les chairs étaient tranchées profondément et des bulles éclataient tandis que le liquide vital s'écoulait à gros bouillons. Il échappa une grimace mécontente.

« Je vais voir si je ne trouve pas quelque chirurgien pour t'arranger ça ! »

Il se leva, empoignant ses armes et son carquois, sans jeter un regard à son compagnon. Il savait que la seule chose qui serait utile à celui-ci était un prêtre. La lame avait touché les entrailles du malheureux et ce n'était plus qu'un mort en sursis, un cadavre inconscient de son état. Maza n'avait pas envie d'assister à sa lente agonie. Il en avait vu assez. Il était là depuis trop peu de temps pour s'être fait des amis, et avait appris à se défier des liens qui se tissaient entre les hommes, alors même que leur destin à tous se jouait les armes à la main. Il déambula jusqu'à un autre groupe où il fut accueilli par des regards méfiants. Voyant que les soldats faisaient cercle, il comprit qu'on se partageait un butin en catimini, certainement prélevé au nez et à la barbe des chevaliers et des barons.

Aucun guerrier ne trouvait la solde suffisante et chacun agrippait ce qu'il pouvait dès qu'il en avait l'occasion. Cela devait juste demeurer discret, pour ne pas insulter les puissants et éviter de se faire dépouiller par plus fort. Il arriva dans une clairière bordée de peupliers, où des captifs attendaient, les bras noués dans le dos à une longue corde les reliant les uns aux autres, sous le regard attentif de sentinelles. Dans un bosquet, un chevalier se délassait en discutant avec un guerrier portant la cape noire à croix rouge des frères de Saint-Jean. Ils riaient et parlaient fort, heureux de leur bonne fortune.

Enrico avança, sans considération pour les silhouettes misérables qui piétinaient ou patientaient, écroulées dans la poussière et les graviers, le visage fermé. La plupart des prisonniers n'étaient pas des soldats, juste de simples bergers, des nomades qui faisaient paître les bêtes qu'on leur confiait. Rassurés par la trêve avec le royaume de Jérusalem, ils étaient venus dans les riches pâturages du Golan, aux abords de Baniyas. Seulement le roi Baudoin avait besoin d'argent. Le parjure plutôt que la ruine.

Maza ne retrouva ses compagnons que bien plus tard, sans avoir pu trouver quelqu'un qui aurait pu soulager un peu le blessé, ni prêtre ni chirurgien. Entretemps, ils s'étaient attaqués à un mouton qu'ils s'employaient à préparer pour le repas du soir. Comme lui, ils savaient tous qu'il n'y avait rien à faire pour le malheureux. Enrico Maza se pencha et examina le jeune homme, dont la tête avait roulé sur la poitrine. Il approcha son oreille des lèvres, écoutant avec attention. Il voyait la tache brune qui s'était répandue sous le blessé, avait imbibé toutes les étoffes. Il soupira, tourna les yeux vers les autres, qui taillaient dans la viande avec des rires gourmands. Alors qu'il tendait la main vers la sacoche encore accrochée à l'épaule, une voix l'interpela :

« Hé ! Maza ! Tu crois quoi ?

--- Il aura pas usage de ça là où il va, Aymar !

--- Je dis pas le contraire. T'avise pas de tout garder pour toi ! »

### Chateau Emmaüs, matinée du jeudi 16 mai 1157

Lâchant ses forces[^90], Blayves se redressa en grimaçant, le dos rompu par l'effort. D'un signe de tête, il indiqua à son aide de libérer le mouton, qui s'éloigna en quelques bonds, surpris de se voir si léger et fort rafraichi. Beaucoup de bêtes, inquiètes, poussaient des bêlements anxieux, contrariées d'être séparées de leurs congénères.

Certaines paissaient sous la surveillance de valets et de chiens jusqu'aux abords du caravansérail, où un peu de fraicheur naissait au pied du mur, près de l'église. Il en demeurait encore un grand nombre à tondre. Le berger attrapa une calebasse et avala une gorgée, s'essuyant le front de la manche. Le soleil était déjà chaud et, malgré les frondaisons, la suée venait vite.

Contrôlant rapidement d'un œil que tout se passait bien chez les autres ouvriers, il se pencha et commença à plier soigneusement la toison. Il en profitait toujours pour couper les parties sales, les plus souillées et en arracher les brindilles. Le Boiteux avait bonne réputation, et c'était pour cela qu'il ne peinait jamais à trouver de l'embauche. Un père dur à la tâche et prompt à la colère et aux coups lui avait inculqué le goût du travail bien fait. Il n'hésitait pas lui-même à réprimander vertement ses manouvriers, maniant parfois la trique sur les échines des plus réfractaires.

Il porta la toison sur le chariot que les frères de l'Hôpital avaient placé à son intention puis descendit la pente vers le gros du troupeau. La sueur mouillait son cou, ses aisselles, son dos, brûlant de son sel la peau tannée par le soleil. Il s'essuya la nuque d'une main crispée, toute de tendons et de muscles. À peine fut-il près des bêtes que l'un des chiens vint le sentir et s'assit un court instant devant lui. Il sourit, caressa le crâne de l'animal sans s'arrêter pour autant. Il suivit le haut édifice à main droite, contournant l'église pour rejoindre la rue qui longeait la façade de l'hébergement destiné aux pèlerins. Deux chameaux lourdement bâtés de pièces de bois patientaient, le regard absent, mâchant une herbe rare qu'ils attrapaient de leurs lèvres mobiles.

Le village était quasi-désert, tout le monde étant affairé dans sa parcelle ou à l'abri de la chaleur. On entendait seulement le bruit d'un chantier de construction non loin : de nouvelles maisons se bâtissaient à l'entrée du hameau. Près de la fontaine, au centre de la place principale, une vieille femme filait, surveillant distraitement une marmaille agitée qui semblait la laisser indifférente.

Blayves pénétra dans le caravansérail, retrouvant la fraicheur du passage couvert menant à la cour autour de laquelle les entrepôts et hébergements se déployaient. Les pèlerins avaient pratiquement tous repris la route, et un grand calme régnait. Un chaton traversa en miaulant, un instant tenté d'effrayer des pigeons occupés à gratter à l'endroit où on attachait les montures. Il disparut en un éclair à l'approche de Blayves. Celui-ci se dirigea droit vers le côté ouest, au fond des arcades, où il savait probable de trouver le frère cellérier.

Celui-ci, un jeune homme d'origine syrienne, le cheveu aussi noir que sa tenue, recopiait depuis sa tablette de cire plusieurs chiffres sur un abaque à calcul tracé à même la table. Il accueillit le berger d'un sourire, reposant les tesselles servant de marqueurs. Blayves le salua d'un imperceptible signe de tête.

« Il faudrait envoyer valet vider le char, frère.

--- Il est déjà empli des tontes du matin ?

--- Certes, et bientôt nous devrons les poser au sol si rien n'est fait prestement. »

Le jeune clerc se mordit les lèvres.

« C'est que je n'ai personne de disponible pour le moment. »

Il hésita un moment, puis proposa d'une voix douce :

« Aucun de vos valets ne saurait mener la mule ? »

Blayves soupira, contrarié. Chaque fois, on tentait de l'abuser.

« Je ne saurais dire, et puis ils sont là pour les toisons, ils n'ont guère le temps de s'occuper de l'attelage.

--- Je comprends, cela ne serait pas fort long.

--- Si vous aviez voulu que je prévoie valet d'attelage, il aurait fallu me prévenir, frère Chrestien. Nous avons contrat pour la tonte, rien de plus, je ne peux payer mes gens à œuvrer pour votre part de l'accord. »

Le cellérier acquiesça en silence, comprenant que le berger ne céderait rien.

« Je vais voir ce que je peux faire pour vous envoyer valet d'ici peu.

--- Fort bien. D'ici là si nous n'avons plus de place, nous entasserons les ballots devant le char.

--- Je comprends, faites ainsi. »

Le Boiteux repartit d'un pas lent, longeant la cour à l'abri des arcatures, le visage toujours fermé. Le chien, qui l'avait suivi en catimini, l'attendait assis devant la porte, la queue balayant la poussière du chemin. Il sourit et lui caressa de nouveau la tête, l'invitant d'un geste à avancer. Finalement, il préférait la compagnie des bêtes.

### Jérusalem, entrepôts des hospitaliers de Saint-Jean, après-midi du jeudi 20 juin 1157

Debout face à une montagne de toisons empilées, Gringoire la Breite souriait. Les mains sur les hanches, il toisait l'or blanc, beige et brun entassé devant lui, à la lueur des lampes à huile. Il n'avait jamais vu semblable amas de laine de sa vie. Des doigts fébriles fouillèrent sa chevelure argentée, et il ne put retenir un gloussement de joie.

« Foutrecul ! Que voilà superbe vision, mon frère ! »

Le petit moine qui l'accompagnait lui adressa un regard courroucé en entendant le juron. Il n'eut pas le temps de lui en faire reproche que l'autre continuait sur sa lancée, postillonnant et gesticulant.

« Je suis fort aise de me rendre utile, d'autant que vos gens ne me semblent guère s'y entendre en cet ouvrage ! »

Il s'approcha d'un premier paquet et froissa quelques fibres de la main.

« Déjà, même s'ils ne savent partir poil de brebis de celui du bélier, ils auraient pu éviter de mêler bruns et ocres, blancs et sables ! Du travail de foutriquet, ça ! »

Il fit quelques pas et renifla un autre amas.

« Vous ne lavez pas les laines avant la tonte ce me semble ? J'avais appris ainsi avec mon père, moi. D'autant que chez vous, ça sécherait vite !

--- Hé bien...

--- Remarquez, ça dépend aussi de la bête, bien sûr, je ne voudrais pas vous faire affront. Je ne m'y entends guère à vos types d'ouailles. Ces animaux à grosse queue, ça me parait fort étrange tout de même ! Le poil en est-il correct ?

--- Nous avons... »

Attrapant une touffe, le grand gaillard la porta à son oreille et fit un signe impératif de silence au clerc. Puis il fit crisser les fibres avec attention, échappant un sourire, le regard perdu au loin dans les ténèbres des magasins non éclairés. Puis il lâcha un gloussement, qui s'entacha d'un rot.

« M'ouais, pas si mal. Seulement vous avez là de tout, j'en jouerai mes braies ! N'indiquez-vous pas à vos valets de séparer au moins les laines de bêtes mortes des autres ?

--- Je ne...

--- C'est très important, ça, vous savez. Sinon on perd du temps. Les meilleurs drapiers savent faire la différence. Bien faire les choses dès le départ, ça évite suées tardives comme disait mon vieux. »

L'hospitalier soupira, s'efforçant de faire bonne figure. Il s'occupait de la draperie de Saint-Jean depuis bientôt quinze ans et s'y entendait au moins autant que son interlocuteur, pourvu qu'on lui laissât le temps de s'en expliquer. L'homme, un pèlerin dont la femme était en couche dans la section des soeurs, avait insisté pour se rendre utile, en remerciement. Le pauvre moine commençait à croire que les autres avaient surtout cherché un moyen de s'en débarrasser.

Néanmoins, sa bonne volonté attirait la sympathie, malgré un comportement agaçant. Le frère drapier rongeait donc son frein, attendant patiemment que Gringoire ait fini sa diatribe pour pouvoir se mettre au travail. Gringoire s'avisa d'un paquet et l'examina plus précisément, faisant soudain silence. Le fait étonna suffisamment le clerc pour qu'il s'approche, intrigué. Il évita de justesse d'être bousculé quand l'autre se releva brusquement.

« Et voilà, j'en étais sûr ! On vous a mis poil à carder et bonne fibre à peigner en vrac ! »

L'hospitalier fronça les sourcils, contrarié.

« Êtes-vous sûr ?

--- Ça, je ne m'y entends peut-être guère pour dire le Pater et l'Ave sans faute, par contre on ne me fait pas prendre toison de bélier pour fourrure d'agnel. Vous avez là de la méchante laine de second choix qui se cache parmi les belles fibres longues. On vous aura trompé sur la marchandise !

--- Il ne peut être question de cela, mon frère.

--- Par Dieu, dites-vous que je mens ? »

Le clerc sourit, amusé malgré le nouveau blasphème.

« Certes pas, c'est juste qu'il ne peut y avoir tromperie, car tout cela n'a pas été acheté, ce sont toisons de nos domaines. Simple erreur de tri, voilà tout. »

Gringoire écarquilla les yeux, le souffle coupé.

« Tout ça vient de vos terres ? Toutes... »

Pour la peine, il en perdait la voix, ce qui amusa d'autant plus le clerc.

« Enfin, pas exactement. La grande majorité, oui. D'aucunes toisons nous ont été données en obole aussi, perçues comme butin des batailles que nous menons. Ce sont paiements de redevances, et aussi et surtout lainages de nos bêtes. »

Gringoire acquiesça, impressionné.

« Il y a là de quoi tisser des vêtements pour le royaume entier !

--- Nous donnons tout à peigner ou carder puis filer, et revendons ensuite beaucoup. Nous ne gardons que modeste part pour nous, pour nos besoins de draps dans les différentes héberges.

--- C'est là véritable trésor royal, par la jambe-Dieu !

--- Il nous faut bien payer le pain et les lits des pèlerins, mon frère. Nous commerçons nos biens pour pouvoir offrir bon accueil au pérégrin qui frappe à notre porte. »

Le grand costaud continuait de hocher la tête, admirant les ballots qu'il avait devant les yeux, imaginant des pyramides de laine qui s'étendaient à l'infini. Pour le coup, son silence inquiéta le clerc, qui s'avança à son tour, touchant le bras de Gringoire.

« Vous vouliez donc m'aider à faire le tri des meilleures toisons ? »

### Plaine d'Esdrelon, midi du mardi 15 octobre 1157

Le groupe de voyageurs avait fait halte auprès d'une citerne abritée dans un bosquet. Alentour, les broussailles et les arbustes finissaient de ronger les ruines de bâtisses éventrées. Deux cavaliers en cotte de mailles, dont l'un portait le manteau noir frappé de la croix rouge, discutaient avec un frère de Saint-Jean vêtu de bure. Ils étaient tranquillement accoudés à un muret tandis que les marcheurs, une trentaine de personnes arborant la croix sur l'épaule, la besace en bandoulière et le bourdon à la main, se désaltéraient à l'eau fraîche.

Certains s'étaient assis pour avaler un peu de pain. Le ciel était d'un blanc gris laiteux, sans soleil et un vent venu de la côte caressait les frondaisons. L'été arrivait à sa fin et, dans un champ voisin, quelques paysans attardés finissaient de récolter le coton. L'un d'eux leva son chapeau de paille en guise de salut.

Un peu à l'écart du groupe, abrité des bourrasques par un buisson, Droin mâchait son pain sans plaisir, la bouche pâteuse. Il jouait à soulever la poussière de la pointe de sa chaussure. Il savait qu'ils ne s'arrêteraient pas longtemps, souffla de dépit et s'apprêtait à s'allonger tout de même lorsqu'il aperçut une silhouette qui entrait dans le hameau ruiné d'un pas lourd, fatigué. Il plissa les yeux, détaillant l'arrivant. La tenue ne lui était guère familière, le teint olivâtre et l'aspect général de l'individu lui firent froncer les sourcils.

Il siffla et, de la main, fit signe à l'homme de s'éloigner, sans résultat. Il se releva et entreprit de le chasser avec de grands gestes, toujours sans que cela ralentît le miséreux vêtu de haillons. Droin s'approcha et lui jeta une poignée de gravier et de poussière en vociférant. Les visages s'étaient tournés vers lui maintenant et chacun s'interrogeait. Le cavalier au manteau noir s'avança à son tour, interpellant Droin. Le vagabond se figea en voyant le soldat sans pour autant reculer.

« Que se passe-t-il ?

--- C'est ce païen ! Il n'a rien à faire là ! » s'emporta Droin.

L'hospitalier prit une mine surprise.

« Comment savez-vous qu'il n'est pas bon chrétien ?, s'amusa le chevalier.

La question parut si saugrenue à Droin qu'il faillit s'en étouffer. Il parvint à articuler quelques mots sans cohérence, crachant sa haine des infidèles qui souillaient le royaume de Dieu. L'homme, clairement un natif de Syrie, gardait les yeux fixés au sol, passif, indifférent à ce qui se jouait autour de lui. L'hospitalier le tira un peu à l'écart et lui demanda son nom, et où il se rendait, sans obtenir de réponse. Il fit alors signe à son compagnon sans arme, qui le rejoignit rapidement. Ils lui répétèrent les mêmes questions dans la langue des Syriens, échangeant péniblement quelques mots avec le vagabond. Prenant un quignon de pain dans son sac, le frère le tendit au malheureux, et l'invita à se désaltérer à la citerne, sous le regard étonné des pèlerins. Le miséreux hésita un long moment, s'exécuta sans hâte, de façon mécanique, en prenant garde à ne dévisager personne.

Alors qu'il s'éloignait de nouveau, le moine l'apostropha dans sa langue une dernière fois, n'obtenant qu'un souffle en guise de réponse. Hagard, l'homme reprit ensuite le chemin vers le sud.

Droin, qui s'était un peu calmé, s'approcha du frère hospitalier :

« C'était pas un infidèle ?

--- Je ne sais.

--- Il aurait pas fallu lui donner du pain, si c'en était un. »

Le clerc se tourna vers le pèlerin, désapprobateur.

« La charité est devoir chrétien, je n'ai pas souvenance qu'il faille choisir à qui la proposer.

--- S'il veut du pain, il n'a qu'à semer et suer comme nous tous.

--- Il ne voulait pas notre aide, j'ai dû insister. Il a refusé de nous suivre jusqu'à l'hôpital, où on aurait pu l'accueillir.

--- C'est ce qu'il a dit à la fin ?

--- Il a dit qu'il était berger, pas mendiant, et qu'un jour on lui rendrait ses brebis volées.

--- Des brigands lui ont dérobé ses bêtes? » s'inquiéta Droin, instinctivement plus proche de celui qui était, comme lui, un travailleur de la terre.

L'hospitalier haussa les épaules, dépité. Il allait pour parler, se reprit et lança finalement à la cantonade d'une voix lasse :

« Il est temps de se remettre en route, mes frères! »❧

### Notes

Le pillage par le roi Baudoin des plateaux du Golan en début d'année 1157 fournit un bon prétexte pour aborder de façon transversale un aspect essentiel de l'économie médiévale : le tissu. Il ne s'agit ici que d'effleurer une première fois le parcours de la matière première, à travers le destin de quelques-uns amenés à croiser ce matériau, source de richesse et parfois objet de luxe, d'un prix élevé quelle que soit sa qualité. Les sociétés modernes ont tellement optimisé la production industrielle des tissus que leur valeur s'est effondrée. Pourtant pendant des siècles, encore plus qu'aujourd'hui, le vêtement et les étoffes ont constitué un élément marqueur de la classe sociale et représentaient un budget très important pour les familles. La chaîne de production des lainages était éminemment complexe et organisée, réglementée, en Europe comme au Moyen-Orient.

### Références

Cardon Dominique, *La draperie au Moyen Âge. Essor d'une grande industrie européenne*, Paris : CNRS Éditions, 1999.

Lombard Maurice, *Les textiles dans le monde musulman du VIIe au XIIe siècle*, Paris: Éditions de l'EHESS, 2002, réédition.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Chétif destin
-------------

### Meleha, abords du lac de Houla, soirée du mardi 18 juin 1157

Le moustique vrombissait aux oreilles de Jehan le Batié. Agacé, pestant fort rageusement, le soldat agitait les doigts en tout sens autour de sa tête.

« Le démon saisisse ces infernales bestioles ! Que dévorent-elles donc quand nul sergent de la Milice du Christ n'est là ? »

Un de ses compagnons, occupé à souffler sur leur maigre feu de camp, lui sourit, protégé par l'épaisse fumée s'échappant du bois vert.

« S'enduire de boue est fort efficace, ainsi que le font les buffles d'eau ! »

Jehan s'esclaffa sans vraiment rire puis vint se placer à son tour sous le vent. Il ajouta dans la marmite quelques poignées de sorgho au riz acheté aux paysans du lieu. Autour d'eux, l'escorte royale s'était installée sous les frondaisons qui habillaient les pentes où s'alanguissait le vieux village aux maisons grises. L'éminence jaillissait des terres marécageuses alentour.

La route menant vers Tibériade, depuis Panéas[^91], avançait au pied des reliefs occidentaux, près des champs de canne à sucre. Le paysage au levant n'était que roselières, cultures et étendues d'eau jusqu'aux contreforts du Golan. Des colonies d'oiseau tachetaient la vaste plaine de couleurs vives et chamarrées, et leurs cris et chants résonnaient sur les paluds. Les hautes tiges souples des plantes aquatiques murmuraient dans le vent faible, sous un ciel d'un cyan immaculé. Le feu du soir n'avait pas commencé à ronger l'horizon.

Jehan étendit ses jambes, fourbu après une journée de monte. S'il savait bien tenir en selle, il n'était pas habitué à y rester longtemps. Il servait, en tant que fantassin, la milice du Christ qu'on appelait désormais du lieu où se trouvait le quartier général : le Temple. Escortant Baudoin de Jérusalem[^92], ils revenaient de Panéas, attaquée récemment par les soldats turcs de Nūr ad-Dīn[^93]. Voyant que son adversaire avait fui à son approche, le roi s'était contenté de libérer la forteresse puis s'était employé à faire relever maisons et fortifications détruites. La plupart des hommes étaient demeurés sur place de façon à achever le travail, tandis que la cavalerie reprenait le chemin de la cité sainte.

Quelques sergents, parmi lesquels Jehan, avaient été désignés pour encadrer les animaux de bât. Comme son nom, Le Batié, l'indiquait, il n'était pas chrétien depuis fort longtemps. C'était un captif converti pour échapper à l'esclavage. Tout jeune, il avait pris les armes par désoeuvrement, trop pauvre pour pouvoir acheter des bêtes, trop fier pour se faire fellah. Devenu brigand, il pillait et rançonnait les voyageurs imprudents. Jusqu'à tomber aux mains des féroces cavaliers au blanc manteau. Peu versé dans la religion, il avait rapidement compris qu'une meilleure vie pouvait s'offrir à lui. Il abjura la foi de ses ancêtres, qu'il ne pratiquait que des lèvres, pour celle qu'il ne connaissait guère plus de ses conquérants.

On l'avait aspergé de quelques gouttes, il avait prononcé des mots mystérieux et avait retrouvé sa liberté. Toute relative, car il suait désormais sous le harnais, installant le campement, prenant soin des montures et tenant la lance et le pavois à l'occasion. Il n'y était pas maladroit, mais préférait son petit bouclier rond et son épée, une bonne lame d'Europe dont il avait refait la poignée lui-même. Il ne goûtait finalement guère le repos, mais dormir le ventre plein était un luxe pour qui avait connu des années où la poussière des chemins servait de souper et les broussailles rêches de matelas. Le regard embrassant le paysage, il jouait machinalement avec la croix qu'il portait autour du cou, symbole de son émancipation et garantie pour lui de son statut. Sur un ressaut du terrain, quelques hommes nourrissaient d'avoine les montures des chevaliers, tandis que du foin était prodigué aux autres.

Le village, simple amas de cabanes de pisé entourant une solide bâtisse où l'on extrayait le sucre des cannes, n'avait aucune fortification. Des paysans, de retour des champs de canne à sucre, venaient aux nouvelles, s'informant de ce qui se passait au-delà des frontières de leur petit monde routinier. Ils se contentaient de travailler la terre, de payer les impôts à celui qui se proclamait maître des lieux. La guerre pouvait en changer le visage, mais n'en rendait pas la poigne plus douce et ne perturbait pas le lent déroulement des saisons, du labeur pour arracher aux sillons sa subsistance.

Jehan n'avait que dépit pour ces hommes serviles, selon lui tout juste bons à être pressurés. Il était d'une ascendance nomade, un enfant de pasteurs, pillards à l'occasion. Il aspirait à retourner sous la tente de poil, d'y boire le leben en chantant les poèmes des exploits de ses ancêtres, dans l'attente de ses fils qui porteraient haut la gloire de son nom. Il était affligé de l'avoir abandonné pour cette appellation latine et chrétienne. Mais il n'était plus un homme du désert de toute façon, et ses rêves de grandeur n'étaient désormais que fantasmes. Il soupira et se tourna vers ses compagnons de route. Le repas était prêt.

### Abords du Jourdain, soir du mercredi 19 juin 1157

Germant sous le pas des montures et des soldats affairés, la poussière tourbillonnait en volutes de cendre. Les visages transpirants se maculaient, les plaies devenaient grises, la boue buvait le sang. Parmi les hennissements des chevaux énervés, les cris des hommes agacés, fébriles, on discernait des râles, des gémissements, des prières.

Brisée, la superbe escorte du roi de Jérusalem était répandue dans une clairière, les corps des agonisants servants de matelas aux blessés. Les yeux inquiets, brûlés par la sueur et le soleil, cherchaient un sens à la folie environnante. De temps à autre, une escouade turque venait prélever quelques captifs et les emportait, sans qu'on ne puisse tirer d'eux autre chose que des coups et des ordres lancés d'une voix dure.

Les chevaliers gisaient tels des mendiants, affalés dans les broussailles épineuses. On avait forcé les plus fiers à s'agenouiller, garroté serré les arrogants, humiliant leur morgue de rires moqueurs.

Sans blessure sérieuse, Jehan avait été désigné pour aider à porter les plus mal en point. Rapidement interrogé par un émir au sharbush[^94] crasseux, il avait prétendu être un habitant de la côte, chrétien depuis toujours. Il savait quelle punition attendait généralement ceux qui avaient abjuré la foi musulmane. Le tranchant d'une lame raccourcissait de façon définitive la tête qui avait osé se dresser contre les traditions. Il avait expliqué que sa famille était dans le commerce, espérant se donner ainsi de la valeur.

Fouillant sous les mottes de phalagnon, cherchant un galet à sucer, il passa une langue pâteuse sur ses lèvres sèches. Le vent les rafraichissait enfin, mais la journée avait été torride, les affrontements impitoyables. Partis de Meleha au matin, ils avaient suivi tranquillement la route vers le sud, jusqu'à ce que des unités de Nūr ad-Dīn fondent sur eux au détour d'un relief, en contrebas de Saphet[^95].

La cavalerie, rompue aux exercices, n'avait pas cédé et s'était rapidement organisée en plusieurs corps. Mais la valeur n'avait pas suffi à compenser le nombre. L'embuscade avait été bien préparée et l'émir était venu avec tous les hommes qui l'avaient accompagné à l'assaut de Panéas. Quelques escadrons de chevaliers, sans infanterie pour les soutenir, ne pouvaient s'opposer à pareille force. La vaillance des coeurs et la robustesse des lances ne fit que rendre plus pénible l'inéluctable. Jehan avait compris que c'était la fin lorsque la bannière du roi s'était affaissée dans la cohue. Peu avant, il avait vu le grand maître, Bertrand de Blanquefort, s'écrouler, son cheval tué sous lui, tandis qu'il menait une charge intrépide.

Les hommes du Temple capturés avaient été mis à l'écart, étroitement surveillés avant d'être emmenés sous les lazzis des combattants musulmans. Certains leur crachaient dessus, d'autres leur jetaient pierres et gravillons, volaient leur manteau pour lui faire subir tous les outrages. Adversaires irréductibles de l'Islam, ils constituaient des captifs de valeur. Leur colonne avait disparu derrière les bosquets de pins et de pistachiers descendant vers le gué sur le Jourdain. On murmurait qu'ils allaient être décapités devant les officiers musulmans, offrant un spectacle de choix aux vainqueurs méritants. Jehan avait été soulagé de ne pas être intégré à leur groupe. Captif, esclave, il pouvait encore espérer en réchapper. La survie était inscrite dans son âme.

Déroulant le foulard qui lui servait de ceinture, il s'essuya le front, le cou. Croisant le regard d'un chevalier assis face à lui, il le lui proposa d'un geste, en silence. L'homme acquiesça et prit l'étoffe, la passant sur son visage souillé. On lui avait retiré son haubert de mailles, mais son gambison de soie attestait encore de son statut. La trentaine, il avait le faciès rude du guerrier, les mains puissantes du cavalier amateur de joutes. Il riva ses yeux couleur d'eau sur le sergent.

« Tu n'es pas sergent le roi ? Ton visage ne m'est guère familier... »

Jehan toussota et répondit d'une voix faible.

« Certes pas, mais je préfère ne pas faire assavoir à ceux-là qui je servais. »

Le chevalier hocha la tête, esquissant un rictus amer.

« As-tu vu si d'aucuns ont pu s'en réchapper ?

--- Je ne saurais dire, j'étais un peu en retrait. J'ai vu les bannières tomber les unes après les autres.

--- As-tu vu le roi se faire prendre ? »

Jehan secoua la tête.

« J'ai vu son étendard flancher, rien de plus. Par contre, le sire de Blanquefort a été saisi. »

Le chevalier serra la mâchoire et hocha la tête en silence avant de lâcher, d'une voix brisée :

« Nous voilà entre les mains de Dieu. »

Jehan baissa la tête, jouant de son caillou avec la langue.

« Plutôt dans celles des diables turcs » murmura-t-il.

### Damas, matin du lundi 24 juin 1157

Jeté en pleine lumière, le troupeau humain émit un gémissement. L'ardeur du soleil piquait les yeux habitués aux ténèbres. Plissant les paupières, ils se dévisageaient, hagards, après les jours passés dans une quasi-obscurité, entassés parmi la paille moisie, les excréments, les rats et les cadavres de leurs compagnons. Les vêtements pendaient sur les silhouettes amaigries, fatiguées et un linceul de cendre recouvrait leur corps. Rendus dociles par la faim, l'épuisement et la lassitude, ils marchaient en traînant des pieds, les bras ballants, la mine égarée.

On les fit s'asseoir et quelques serviteurs leur distribuèrent du pain et des pots emplis d'une eau croupie. Chacun s'en fit un festin et il fallut donner de la voix et du fouet pour que certains n'accaparent la modeste pitance. Jehan mâchait en silence, se grattant frénétiquement le ventre et le cou dévorés de vermine. Le dos lui cuisait encore des coups de bâtons reçus pendant leur venue jusqu'à la ville, sans qu'il n'en ait su le motif. Il s'efforçait d'être aussi coopératif que possible, obéissant avec tout le zèle qu'il pouvait insuffler dans son corps affaibli. Le captif à sa gauche, issu d'une autre geôle, lui proposa la cruche après en avoir avalé une longue gorgée qu'il faisait jouer entre ses dents, la savourant comme nectar de Chypre. Il but rapidement et passa le récipient à son tour, engloutissant goulûment les lambeaux de pain qu'il avait pu saisir. Il réalisa que son voisin le dévisageait et l'interrogea d'un regard.

L'homme pencha la tête et répondit à voix basse, bougeant à peine les lèvres.

« Tu as été pris où ?

--- En Galilée, voilà quelques jours.

--- Grosse bataille ?

--- Non, une embuscade. Nous étions une poignée. Le roi n'en a échappé que de peu. »

L'homme hoqueta de surprise et déchira d'un coup de dent une portion de pain.

« Le roi ? En Galilée ? Avons-nous tant perdu de terres ?

--- Non, c'était l'armée du soudan qui rôdait après l'assaut sur Panéas. Nous avons joué de male chance. »

Jehan garda le silence un moment et demanda finalement.

« Sais-tu ce que nous faisons ici ?

--- Ils nous préparent pour nous montrer au peuple. Le soudan aime à fêter ses victoires. Cela fait plusieurs fois qu'on me tire de mes labeurs pour ça.

--- Tu es là depuis longtemps ? »

L'homme soupira, mais ne répondit pas.

« J'avais nom Guilhem, mais je ne suis plus qu'un esclave parmi les ombres.

--- Au moins ils nous gardent vifs. Mieux vaut serf que moribond. »

Guilhem leva le menton, indiquant quelque chose derrière Jehan. Celui-ci hésita à se tourner, voyant l'air abattu de son compagnon d'infortune. Inquiété, il mit un moment à se décider avant de porter son regard vers l'arrière. Puis ses yeux s'emplirent d'horreur. Sur plusieurs chameaux, des chevaliers étaient installés deux par deux, et on fournissait à chacun un étendard pris aux chrétiens. Sans oublier d'y suspendre par les cheveux la tête de quelques captifs. ❧

### Notes

Le terme de « chétif » joue sur le sens commun et son usage ancien, qui signifie « captif ». Son emploi le plus célèbre est celui qui désigne le texte que Graindor de Douai a intercalé au XIIe siècle entre la *Chanson d'Antioche* et la *Conquête de Jérusalem*. Il y développe le récit de chevaliers capturés, mettant en avant leurs aventures d'une façon romanesque qui tranche avec la tradition plus descriptive et historique de la chanson précédente.

Les sources médiévales font régulièrement allusion aux turcoples/turcopoles, et toutes les hypothèses ont été échafaudées sur leur réalité. Il est désormais illusoire de penser pouvoir la définir de façon monolithique, tant le mot est polysémique. Toujours est-il qu'on retrouve de ces combattants dans l'armée franque, parfois affublés de noms, comme Jehan le Batié, Jean le Baptisé, qui semblent indiquer des convertis récents.

Mais pour autant, est-ce que cela veut dire que tous les turcoples, du moins tous ceux désignés sous ce terme étaient ainsi ? Apparemment ils étaient de culture et de tenue moyen-orientale, mais on n'est même pas certain de devoir les rattacher aux traditions locales, syriennes et arabes, ou turques nomades.

### Références

Gibb H. A. R., *The Damascus Chronicle of the Crusades. Extracted and translated from the chronicle of Ibn al-Qalanisi*, Londres: Luzac & Co., 1932, réédition : Mineola : Dover Publication, 2002.

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Richard Jean, « Les turcoples au service des royaumes de Jérusalem et de Chypre: musulmans convertis ou chrétiens orientaux? », dans Richard Jean, *Croisades et Etats latins d'Orient. Points de vue et Documents*, Aldershot : Ashgate, 1992.

Branleterre
-----------

### Damas, sud de la Ghûta, fin de matinée du lundi 12 août 1157

Douleur. Dans les jambes. Visage écrasé sur des pierres. La bouche et le nez emplis de poussière. Un nuage de cendre voilait les environs.

Des bourdonnements à ses oreilles occultaient les cris, distants, étouffés. Guilhem tenta de soulever la tête, désorienté par la secousse, abruti par le choc.

Il grimaça lorsqu'il se mit sur les fesses, se frottant les joues comme pour en arracher le linceul poudreux. Il repoussa des mains les gravats qui avaient cherché à l'ensevelir. Il pouvait bouger les pieds, cette découverte le réconforta. Secouant son crâne doucement, il s'efforçait de comprendre ce qui se passait autour de lui.

Tout semblait calme, des voix criaient par moment, comme au ralenti. Quelques animaux braillaient, des chiens aboyaient au loin, et l'un d'eux hurlait à la mort. Le bâtiment sur lequel ils avaient travaillé ces dernières semaines n'était plus qu'un monceau de ruines éparpillées parmi les arbres fruitiers. Des silhouettes s'y discernaient avec peine, mêlées à la gangue de pierre, de mortier et de bois.

Il toussa, cracha à de nombreuses reprises. Il était couvert d'ecchymoses et se sentait trop désorienté encore pour se lever, mais il était sauf. À quelques pas de lui, il voyait un pied émerger des décombres, la jambe de son propriétaire disparaissant sous un des pans de mur écroulé. Un de ses compagnons d'infortune s'approcha à pas lents, une outre à la main. Un jeune captif à la peau noire, originaire du Soudan. Sans un mot, il lui tendit la gourde et s'accroupit à ses côtés, scrutant avec effarement devant lui.

L'eau fit le plus grand bien à Guilhem. Son cerveau se remettait en marche, méthodique, affairé. Il tourna la tête en tous sens, à la recherche des soldats qui les surveillaient. Ils n'étaient qu'une poignée d'esclaves employés sur ce bâtiment, aux abords des tombes de Suhayb al-Rumi et Umm'Atika, tout près du maydan al-Hasa. Les gardes s'étaient réfugiés à l'intérieur, profitant de l'ombre pour se reposer au plus fort des chaleurs.

Seul le petit groupe affecté à la préparation des repas et à l'approvisionnement en eau et lui, à la gâche du mortier, demeuraient dehors, les travaux se poursuivant à l'étage. Il sourit, heureux de son sort. Le soudanais le dévisageait désormais, effrayé, interdit. Il le rassura d'une voix rauque, expliquant qu'ils tenaient là une occasion unique. En se relevant, il tituba, surpris par la douleur qui rayonnait de ses jambes. Le gamin attendait, toujours aussi inquiet.

Retrouvant peu à peu son équilibre, Guilhem se traîna jusqu'aux décombres. C'est alors qu'il ressentit de nouveau un tremblement, une oscillation. Un nouveau séisme. Perdu au milieu des jardins, allongé sur le tas de gravats, il ne risquait plus rien. Mais la peur était la plus forte et il se figea, fermant les yeux tout en priant à voix basse. La secousse fut brève, peu importante et il reprit son examen sans tarder. Il cherchait tout ce qui pouvait lui servir, amassant les objets, les vêtements, sans discernement. Il ramenait tout à une pile qu'il faisait près de l'endroit où ils attachaient l'âne qui les accompagnait.

L'animal s'était enfui, mais il demeurait le panier empli des repas : pain, olives, quelques fruits séchés. Le gamin le voyait faire et se mit à l'imiter. Retrouvant son assurance coutumière, Guilhem dépouillait sans vergogne les corps pas trop exposés et récupéra même les couteaux qui les équipaient, ainsi qu'une masse de belle allure. Celle de Kalil. Il jetait parfois un regard inquiet autour de lui, dans la crainte que l'on vienne s'enquérir de ce qui s'était passé là. Il ne savait pas que, dans la ville, chacun courait après sa famille, s'extrayait des bâtiments écroulés avec l'aide des voisins, des amis. Nul ne se soucierait de son sort avant longtemps.

Il chercha un outil qui lui permettrait de se débarrasser de l'anneau de fer qui le marquait comme un esclave. Un burin, utilisé pour retailler les moellons fut sa seule trouvaille et il ne parvint pas à entamer le solide bracelet. Inquiet à l'idée de perdre du temps, il préféra l'attacher d'un chiffon le plus haut qu'il pouvait, et le dissimula sous la manche d'un large manteau. Il s'équipa de tous les vêtements qu'il pouvait dénicher, échangea ses souliers francs, usés et déchirés, contre des savates de belle facture. Puis termina par s'enrouler un turban sur la tête, sans grand talent, mais avec obstination.

Il espérait que cela suffirait pour qu'il rejoigne les territoires chrétiens. Il devait partir à l'ouest, sans être bien certain du temps qu'il lui faudrait pour cela. Son plan était simple : il était un marchand ruiné par le tremblement de terre à Damas qui s'empressait de retourner chez les siens, sur la côte. Il savait suffisamment bien parler arabe pour se faire comprendre. Tant qu'on ne voyait pas son bracelet, il pouvait faire illusion. Quand les autorités se rendraient compte qu'il avait filé et n'était pas enseveli parmi les vestiges du bâtiment, il serait loin.

Le garçon l'avait suivi, d'abord du regard, puis l'avait aidé à entasser tout ce qui pourrait lui être utile lors de son périple. Depuis un moment, il était invisible. Guilhem espérait qu'il n'était pas parti chercher du secours. Plus probablement, il avait tenté sa chance aussi. Mais que pouvait bien un gamin d'à peine une douzaine d'années livré à lui-même ? Il finirait brigand, ou serait repris. Il n'avait même rien récupéré dans les maigres bagages accumulés.

Tandis que Guilhem entassait avec fièvre dans le panier de nourriture tout ce qu'il pourrait échanger ou revendre, il entendit clopiner sur le chemin de graviers. Le garçon revenait avec le baudet en longe. Inquiet, apeuré, l'animal avait les oreilles baissées, et n'avançait qu'en raison des coups de trique obstinés. Guilhem esquissa un sourire. Un marchand avec un domestique et un âne serait certainement moins suspect qu'un homme seul.

### Damas, soirée du youm al joumouia, 12 ramadan 552[^96] {#damas-soirée-du-youm-al-joumouia-12-ramadan-55296}

Une odeur d'épices et de coriandre flottait encore dans la pièce douillette. Abu Malik étendit un peu les jambes lorsqu'il s'assit de nouveau sur le kursi[^97] où il aimait s'installer pour rédiger son courrier. Il n'allait pas jusqu'à utiliser un large plateau de bois comme bureau, comme les Ifranjs, qu'il côtoyait tant, mais s'appuyait simplement sur une écritoire. Un brasero apportait un peu de chaleur en cette fraîche soirée, une lampe à huile à la flamme tremblotante complétait la chiche lumière des claustra. Il entendait dans une salle voisine son épouse jouer avec leurs filles. La rumeur de la rue était tenue à l'écart de la cour intérieure de sa demeure.

Il sortit un feuillet d'un étui de cuir ouvragé et le relit attentivement. C'était une missive à l'intention de frère Raymond, un membre de l'hôpital de Saint-Jean de Jérusalem. Ils étaient en contact régulier, car l'un comme l'autre œuvraient à la libération des captifs, en aidant au versement des rançons, à leur rachat par des âmes pieuses. Bien que fidèle pratiquant de l'Islam, Abu Malik n'était pas un homme belliqueux et préférait entretenir des rapports cordiaux avec les différentes congrégations. Issu d'une vieille famille de marchands damascènes, il savait que les dirigeants allaient et venaient et qu'il était plus sage de ne pas s'aliéner un puissant. Tant qu'il pouvait vivre sa foi en toute quiétude, il était satisfait.

D'ailleurs, il avait toujours plaisir à rencontrer des négociants de la côte, chrétiens nestoriens ou juifs d'Espagne. Il se sentait plus proche d'eux que des intolérants croisés arrivés voilà quelques dizaines d'années par armées immenses, ou même que des Turcs nomades, dont il déplorait la rudesse, jusque dans la langue. Tous ces nouveaux arrivants étaient imbus d'eux-mêmes, avides d'étendre leur pouvoir, et bataillaient sans cesse, plus pour leur propre gloire que dans le souci de propager leur foi, selon Abu Malik.

Il pointa les montants exigés pour la libération de certains prisonniers que leurs propriétaires étaient prêts à relâcher. Il indiquait également ceux qu'il savait avoir rendu l'âme lors de leur détention. Il en arrivait à un nom qui lui rappelait de mauvais souvenirs. Guilhem Torte ! Ce captif avait été tué lors des tremblements de terre de l'été, qui avaient frappé durement les territoires, autour de Damas, et encore plus au nord. Des quartiers entiers n'étaient plus que ruines, des familles broyées sous la pierre et les décombres, des vies anéanties en un séisme dévastateur. Colère divine hurlaient certains, fatalité pensaient d'autres. Abu Malik avait tressailli quand on lui avait raconté l'histoire de cette école effondrée sur ses élèves, ne laissant pour tout survivant que le professeur, sorti dans la cour. Il ne lui restait plus qu'à chercher le salut de son âme dans la prière et le dégoût de lui-même, à celui-là.

Le négociant soupira. Il s'estimait heureux de n'avoir que des filles en cette affaire, pour une fois. Elles n'allaient pas à l'école, et sa demeure était solidement bâtie. Ils n'avaient d'ailleurs eu que peu de dégâts : de la vaisselle cassée et un appentis écroulé sur le toit. Ses entrepôts ne contenaient que fort peu de marchandises, et ce n'était généralement que des étoffes, des tissus qu'il conservait le temps de leur faire prendre de la valeur, de les céder au bon moment. Sa fortune n'était que de papier, des parts dans des cargaisons, des investissements dans des négoces lointains...

Il voyageait de moins en moins, avec l'âge, et préférait demeurer de ce côté de la mer, désormais. À chaque départ, il craignait de ce qu'il trouverait à son retour. D'ailleurs, le matin, lorsqu'il était à la mosquée, il avait discuté avec ses amis de la difficile situation politique de la cité. Ils regrettaient tous le temps de leur indépendance, mais n'osaient guère le proclamer à haute voix. Nombreux avaient été ceux qui, mécontents de leurs précédents dirigeants, avaient placé leurs espoirs dans le chef turc.

L'appel à la prière incluait désormais le nom de ce farouche maître de guerre, Nūr ad-Dīn Mahmud ben Zengi ben aq Sunqur et ses émirs patrouillaient dans les rues. Damas s'éteignait, Abu Malik le sentait bien. Ce n'était qu'une des provinces pour l'homme qui tenait dans sa main aussi Alep et Mossoul. Il ne lui manquait que le Caire, disait-on, pour cueillir les territoires des Ifranjs comme des fruits murs. L'appétit des puissants ne connaissait nulle mesure, surtout quand leur bras maniait le sabre et la lance.

Yusuf ben Ahmed al-Ifriqiya, un de ses vieux amis, leur avait appris la nouvelle au matin : leur prince était tombé gravement malade alors qu'il était en campagne. Il se murmurait qu'il sentait sa fin proche et avait désigné son successeur, son frère Nusrat al-Din. Son fidèle entre tous, Asad al-Din Shirkuh, était arrivé récemment avec son askar pour prendre le contrôle de la cité. Abu Malik n'évoqua pas l'information dans son courrier. Après tout, frère Raymond appartenait à un des ordres les plus réfractaires à la puissance musulmane. Certains d'eux se battaient sous le même manteau que lui, portant le haubert et l'épée. De toute façon, il serait certainement au courant de la nouvelle rapidement. Peut-être qu'ils chercheraient à tirer avantage de la situation. Encore plus de batailles, plus de morts, de blessés et de captifs. N'étaient-ils pas enfin repus de tout ce sang versé, de toutes ces richesses accumulées ?

### Shayzar, soir du vendredi 22 novembre 1157

Le feu crépitait et lançait autant d'étincelles qu'il propageait de fumée. Ils brûlaient tout ce qui leur tombait sous la main et qui pouvait aider à se réchauffer. Jamais Gosbert n'aurait pensé qu'il regretterait aussi vite les chaudes journées de l'été. Depuis trois jours, il se croyait revenu dans sa Flandre natale, avec un ciel gris, bas, et un crachin fin et régulier. La morosité s'était abattue sur l'armée latine en même temps que les gouttes et, rapidement, des cabanes avaient été érigées dans les décombres de la cité.

Les trois osts, celui de Jérusalem, d'Antioche, et les troupes croisées de Thierry d'Alsace, comte de Flandre, avaient fait campagne vers Césarée de Syrie[^98], espérant prendre le contrôle de son imposant pont de pierre enjambant le fleuve de Fer[^99]. Les récents séismes qui avaient frappé les territoires musulmans constituaient une vraie aubaine. L'opportunisme militaire faisait fi de toutes les autres considérations.

La quinzaine d'hommes dont Gosbert s'occupait commentait avec enthousiasme les opérations tout en plumant les poulets qu'ils avaient dénichés pour le repas du soir. Ils avaient aussi des pains plats, confisqués à des habitants, ainsi que divers légumes qui mijotaient dans une grosse oule.

« Je vous le dis, en vérite, compères, Dieu a décidé de favoriser le sire comte !

--- Le père curé dit que c'est comme à Gerfaut, que le Seigneur fait corner ses troupes et la muraille s'en écroule d'elle-même !

--- C'est Jéricho, crâne de piaf !

--- Ouais, c'est tout comme. N'empêche que quand le sire comte sera seigneur du lieu, j'espère qu'il nous donnera quelques terres. J'ai repéré bel hostel où il ne me déplairait pas de m'installer.

--- Écoutez-le, le père-la-regratte ! Il se croit déjà sire en son fief, le cul au parmi des coussins de duvet ! »

Une vague de rire parcourut l'assemblée, heureuse de son sort. Un des hommes, qui venait de porter du bois se tourna vers Gosbert :

« Je ne comprends pas que les gens d'ici n'aient pas encore bouté tous ces païens hors les murs. Nous n'avons eu guère de mal à les déloger.

--- Ceux qui tenaient la ville n'étaient pas là depuis longtemps. Ils s'accrochent à la forteresse en plus, tu sembles l'oublier. »

Le soldat haussa les épaules, narquois.

« Ça sera tantôt réglé. »

Une voix l'interrompit, un autre sergent faisant irruption dans leur courette, secouant sa chape humide. Il avait l'air accablé, le regard bas.

« On doit se préparer à issir hors la cité très prochainement. »

Gosbert se leva, fronçant les sourcils. Tous les hommes s'étaient figés et attendaient d'avoir des explications.

« Mauvaises nouvelles ? Les Turcs font route sus nous ?

--- Rien de ce genre. La faute en revient au prince Renaud. Il a exigé hommage du sire comte pour la cité.

--- Comment ose-t-il ? Il n'est qu'à un roi que le sire comte doit préséance !

--- C'est ce qu'il a répondu, mais le prince n'en démord pas. Il exige hommage, alors le sire comte a dit qu'il ne s'échinerait pas à prendre le chastel par force pour un autre. »

Un des soldats occupé à vider sa volaille la jeta au sol de dépit.

« La peste soit de ce prince fils de chienne ! N'a-t-il pas assez de terres et de cités ?

--- La paix ! le réprimanda Gosbert. Mercions plutôt Dieu de nous avoir donne cité à piller. Nous avons jusqu'à demain pour lui faire rendre butin. La nuit sera longue, compères. »

Les sourires qui illuminèrent les faces n'avaient rien de réjouissant. On eut dit des loups affamés, les yeux brillants à l'idée de la chasse. Pourtant nulle fringale ne tenaillait leurs entrailles, à ces fauves-là. Le désir et la violence y prenaient toute la place. ❧

### Notes

Le Moyen-Orient connut de nombreux séismes durant la période des croisades. Ils furent doctement consignés par les chroniqueurs, tel que Ibn al-Qalânisî, parfois avec de grands détails. Selon les zones affectées, cela incita les dirigeants ennemis à tenter des coups de force pour prendre une ville dont les murailles s'étaient écroulées, une forteresse abattue.

La cité de Shayzar, qui abritait la famille du célèbre érudit et diplomate Usamah ibn Munqidh, fut très sévèrement touchée en 1157 et tous, ou quasiment, périrent lors de l'événement.

Des Ismaéliens (la secte appelée par la suite Assassins) et les Francs y virent une occasion unique de capturer l'imposante place forte située sur l'Oronte. Sans succès. Elle demeura finalement dans le giron de Nūr ad-Dīn. Cette période de forte activité sismique coïncida avec des épisodes de maladie pour le dirigeant musulman qui sentit sa fin proche. Il organisa sa succession et se préparait visiblement à décéder. Ses adversaires ne se doutaient pas qu'il vivrait encore longtemps, assez pour mourir la même année que l'héritier du roi Baudoin III de Jérusalem, 17 ans plus tard.

### Références

Élisséef, Nikita, *La description de Damas d'Ibn 'Asâkir*, Institut Français de Damas, Damas : 1959.

Élisséef, Nikita, *Nūr ad-Dīn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Nicolle, David, *The Second Crusade 1148: Disaster Outside Damascus*, Londres : Osprey Publishing, 2009.

Nicolle, David, Kervran, Yann (trad., adapt.), *La Seconde croisade et le siège de Damas en 1148 - Un pari manqué*, à paraître.

In vino veritas
---------------

### Terre sainte, abords de Naplouse, après-midi du mercredi 22 février 1156

Bertaud le Sueur était satisfait. Assis sur le muret de pierre sèche ceinturant sa parcelle, il savourait des salaisons. Les chairs flasques de son visage tressaillaient à chaque bouchée, tandis que se tendait son long cou ridé, mal rasé. Goûtant une pause en cette fin de journée, il admirait son bien : une poignée d'acres d'oliviers disséminés sur une restanque au-dessus de Naplouse.

Cela n'avait pas été sans peine, de nombreux cultivars avaient dû être taillés, parfois sévèrement, pour les fortifier, mais l'entretien en était désormais plus facile. Non pas qu'il ait jamais transpiré à un tel labeur, Dieu l'en préserve. Il s'en tenait à donner des ordres, à surveiller le travail des autres. Personne n'aurait osé l'appeler de son surnom, « Le Sueur » en sa présence, car le sobriquet lui avait été choisi par ceux-là mêmes qui trouvaient qu'il n'en dépensait guère, de sueur. Assuré de sa supériorité naturelle, il laissait cela à des manouvriers, des journaliers, sur lesquels il pouvait en outre déverser sa mauvaise humeur et ses remarques assassines.

Mastiquant avec énergie, il était d'ailleurs en train de préparer une éruption libératrice. Guère de raison à cela, pourtant : depuis le matin, ses ouvriers coupaient les branches pour la fructification sans qu'il n'y trouve motif à réprimande. Rien n'était retiré qu'il n'aurait ôté lui-même, sans pour autant entraîner d'oubli qu'il aurait pu pointer avec brio. C'était peut-être cela même qui commençait à l'agacer : ils ne faisaient rien de travers, n'offraient aucune occasion à son ire de s'enflammer.

Un craquement sonore résonna, assorti d'un juron expressif. Le faux-pas tant espéré avait fini par arriver. Bertaud se leva avec vigueur et s'avança à grands pas vers l'origine de l'incident. Il n'était pas encore sur place que les invectives s'élançaient de sa bouche, accompagnant des miettes de son repas assaisonnées d'une généreuse portion de postillons.

« Foutre ! N'as-tu jamais appris à respecter bien d'autrui, gâte-blé ? »

L'ouvrier, le manche brisé de la serpe toujours en main, n'eut guère le temps de répondre, les exclamations fusaient, impatientes, heureuses de surgir.

« Bien deshonnête est l'œuvrier qui s'emploie à la ruine de son maître ! Ne peux-tu prêter attention à mon oustillement, maudit croque-lardon ? »

L'employé se crut invité à répondre à cette question et tenta timidement d'objecter que l'emmanchement était bien fatigué, la soie de fixation rongée par la rouille. Cela décupla la fureur de Bertaud, le visage désormais cramoisi et les yeux exorbités.

« Cherches-tu à me faire insulte, face d'étron ? Voilà bien façons de fils à putain ! »

Mordant sa lèvre à défaut d'autre chose, il se tut un instant sans pour autant retrouver son calme. Sa voix était blanche de colère lorsqu'il ajouta :

« Puisque, non content de briser mon bien, tu m'en fais reproche, je vais te retenir deux des cinq deniers que je t'avais promis. Et si jamais je daignais un jour te reprendre, sache que tes gages seraient écornés d'un demi-denier par rapport aux autres, pour ta punition et ton édification ! »

Devant tant de fureur et de mauvaise foi, le malheureux se contenta de baisser la tête, subissant patiemment plusieurs salves triomphales du propriétaire vindicatif. De mémoire d'ouvrier, jamais personne n'avait réussi à avoir le dernier mot sur Bertaud. Et certes pas quand il avait l'opportunité de s'épargner de la dépense.

### Naplouse, soirée du mercredi 22 février 1156

Déambulant dans les ruelles étroites de la ville, un large broc vide à la main, Bertaud avait tout oublié de l'incident. Il était familier de ces coups d'éclat, dont la vague refluait aussi vite qu'elle s'abattait. Par ailleurs, les travaux de taille étaient presque achevés, et il sortait de chez le forgeron qui pouvait lui réparer la serpe pour un coût modique, inférieur à la retenue infligée.

Sifflotant, il marchait gaiement en direction de la taverne où il achetait chaque soir son vin pour le lendemain, un pichet d'un carton[^100]. Lorsqu'il entra dans la salle voûtée, il fut accueilli par le vieux chien de la maison, qui était bien le seul à lui témoigner spontanément de l'amitié. Il caressa la bête d'une main distraite tout en cherchant la silhouette du tenancier à la lueur discrète des lampes à huile. La voix le surprit tandis qu'il écarquillait les yeux sans résultat.

« Le bon soir, maître Bertaud. Je suis à vous dans un instant, je cale une futaille reçue ce jour des monts de Judée. Du rouge capiteux, qui n'a rien à envier aux productions de Chypre. Il pourrait vous plaire, j'en ai certeté.

--- Je ne sais, j'ai usage de mon clairet chaque jour. À quel prix faites-vous le pichet ?

--- Le carton en est à quatre deniers.

--- Mordiable ! Que voilà bien dispendieux breuvage.

--- Aucune truandaille en cela, il est vraiment digne de table de roi. Je ne saurai en rebattre un denier. Si vous en avez appétit, je peux vous en faire taster quelques goulées, pur, sans même le laver. »

Joignant le geste à la parole, le tavernier tira le coin du tonneau qu'il venait de mettre en perce et remplit un petit godet qu'il tendit à son client. Avalant avec méfiance les premières gouttes, ce dernier ne tarda pas à vider son récipient avec un plaisir évident.

« Je dois l'avouer fort goûteux, un vrai miel. Mais bien coûteux pour ma modeste bourse, je vais donc me contenter de mon vin habituel.

--- Un carton de jaunet de Belvoir ? Je vous emplis ça. »

Prenant le pichet des mains de Bertaud, l'homme le remplit d'un vin clair, à la belle robe dorée à la lueur des flammes et l'échangea contre quelques monnaies argentées.

### Naplouse, après-midi du samedi 25 février 1156

Le temps frais n'incitait guère à l'indolence et Bertaud s'était résolu à couper quelques broussailles pour allumer un feu. Heureux de se réchauffer, il frappait avec entrain dans les branchages lorsqu'il entendit un bruit de course. Sourcils froncés, il se tourna, prêt à réprimander le manouvrier qui venait le déranger.

Il eut la surprise de voir passer une demi-douzaine de silhouettes en guenilles, cavalant de façon désordonnée. Parmi elles trottinait une fillette d'une dizaine d'années, l'air affolé, le visage sale. Il eut à peine le temps de croiser son regard fiévreux, implorant. Pourtant, sans raison claire, un début de sourire s'était dessiné sur ses traits avides. Reprenant ses esprits, il s'éclaircit longuement et bruyamment la gorge et s'absorba de nouveau dans sa tâche.

De retour à sa parcelle, les bras chargés, il y découvrit une troupe de soldats en armes, à cheval, menés par un homme en grand haubert. Un chevalier de haut rang, cela se voyait à son allure, et pas un chef facile, cela se lisait sur son visage rude.

« As-tu vu esclaves sarrasinois en fuite, l'homme ? »

Un peu interloqué, Bertaud hocha la tête en silence puis, lâchant sa brassée, fit un signe en direction du bosquet d'où il venait.

« Oui-da, j'en ai encontré une poignée voilà peu, en cette direction. Ils ne peuvent être bien loin. Je peux vous conduire si vous en avez désir. »

Acceptant la proposition d'un signe de tête, le chevalier mit sa monture au pas derrière Bertaud. Ce dernier trottinait, espérant s'attirer les bonnes grâces d'un important seigneur, tout en aidant à la capture de quelques musulmans. Il les avait toujours eus en horreur, se refusait à en employer sur ses terres. S'il en avait eu le pouvoir, ils auraient été tous contraints de se convertir, ou chassés au loin du royaume de Jérusalem.

Très rapidement le groupe de fugitifs fut en vue, entouré de cavaliers, rassemblé comme un troupeau pour l'abattoir. Le chevalier fouilla dans une de ses fontes et lança quelques pièces à Bertaud, qui les accueillit avec un grognement de satisfaction.

« La merci à vous, mon sire, je n'espérais pas récompense, mais suis seulement bon chrétien.

--- Hé bien, tu sauras alors quoi faire de cette cliquaille pour soigner ton âme. Je pourchassais ces vauriens depuis le matin, échappés d'un de mes casaux. »

Il approcha sa monture d'un des fuyards, l'obligeant à reculer d'un coup de poitrail de son étalon. Puis d'un claquement de doigt, il mit en branle ses hommes.

« Celui-là me semble bien fier. Apprenez-lui donc à tenir son rang. Un pied brisé devrait l'inciter à demeurer en mon fief. »

L'homme se débattit de son mieux, mais ne pouvait rien faire face au nombre. Devant ses compagnons d'infortune, tenus en respect à la pointe de l'épée, on lui brisa une cheville, d'un coup sec, avec une méthode trahissant une effroyable expertise.

Désireux d'assister au spectacle, Bertaud fut néanmoins indisposé par les cris de douleur et il tourna la tête, tenté un instant de se boucher les oreilles. Il en profita pour laisser son regard errer sur les autres esclaves en fuite.

« La fillette ! Il se trouvait enfançonne parmi eux, j'en jurerai ! Et je ne la vois mie en leur assemblée...

--- Peu me chaut, répondit le chevalier, l'air amusé. Elle nourrira les chacals bien tôt. Elle n'a pas grande valeur. Pas même le prix d'un goret, nul besoin d'échauffer les sangs de mes chevaux pour elle. »

### Naplouse, soirée du samedi 25 février 1156

La démarche sautillante, la tête haute, Bertaud interpellait gaiement les connaissances qu'il croisait, arrachant quelques grimaces étonnées aux commerçants salués avec tant d'enthousiasme. Il avait en poche de quoi s'offrir quelques douceurs pour son repas, à commencer par un carton du meilleur cru du tavernier. Celui-ci l'accueillit sur la pas de la cave :

« Alors, maître, je vous encontre bien léger cette soirée ! Agréable journée ?

--- Tel que tu me vois, j'ai bien intention qu'elle continue ainsi. J'ai en ma bourse plusieurs deniers qui ne demandent qu'à joindre les tiens, en échange d'un carton de ton nectar de Judée.

--- Vous faites abandon de votre carton de jaune pour le rouge ? Je dois m'attendre à pluie de rainettes, s'esclaffa le vendeur, tout en s'effaçant pour laisser entrer son client.

--- N'est-on pas aux vigiles de dimanche ? Bonne fin vient à bonne semaine. Il n'est nulle malice à se faire du bien. »

Sur le chemin de sa demeure, Bertaud se surprit à siffloter, le pot bien calé contre son sein. Il en humait ainsi les excellentes vapeurs. Débouchant dans une ruelle, il manqua renverser une fillette. Agacé, il s'apprêtait à la réprimander lorsqu'il la reconnut.

C'était la gamine qu'il avait aperçue dans le taillis, courant avec ses proches.

Elle l'avait suivi. Il se rappela qu'il lui avait timidement témoigné de la sympathie, bien qu'à son corps défendant. Elle se tenait là, au milieu du caniveau, impérieuse et implorante, les cheveux emmêlés, les habits dépenaillés et déchirés. Bertaud se pencha vers elle, tentant d'afficher son air le plus affable. Après tout, bien que sarrasine, elle n'était pas si vilaine. Il se passa une langue fine sur les lèvres.

Certes maigrichonne, mais une fois lavée, elle pourrait devenir acceptable. Sa bouche se fendit en un onctueux sourire. Il n'avait pas connu de femmes sans avoir à payer depuis des années, et toujours les mêmes, de moins en moins appétissantes au fil des ans. Alors il finissait par trouver du charme à la petite donzelle face à lui. Il tendit une main vers son visage, se pencha un peu.

En un éclair la lame jaillit de sous les hardes. Et se ficha dans la gorge du vieil homme. La bouche figée en un rictus stupéfait, il s'écroula comme un tas de linge sale, brisant au passage son pichet. Dans le caniveau, le sang et le vin se mêlèrent, coulant en un mince filet sous les pieds de la fillette.

Elle le toisa un instant puis cracha sur le cadavre, les yeux emplis de larmes, avant de prendre ses jambes à son cou. ❧

### Notes

L'idée de ce texte court est venu d'un concours de nouvelles dont le sujet était « Carton rouge ». Bien que le sens médiéval d'un quarton (ou carton) en soit éloigné de celui évoqué à première vue par l'expression imposée, cela m'a donné envie de relier les deux (le carton ou quarton est une mesure de capacité de vin, d'environ 2 litres). Il s'agit donc d'un exercice de style qui prend pour prétexte à la fois la répression sévère que certains seigneurs francs exerçaient à l'encontre de leurs serfs musulmans et les tentatives avérées de fuite de certaines communautés musulmanes des environs des Naplouse à cette période.

### Références

Godefroy, Frédéric. *Dictionnaire de l'ancienne langue française et de tous ses dialectes - Du IXe au XVe siècle Tome 6 - Parsommer-Remembrance*. Paris: Librairie Émile Bouillon, 1889 ou <http://www.cnrtl.fr/definition/quarton>)

Kedar Benjamin Z., « Some new sources on Palestinian Muslims before and during the Crusades » dans *Die Kreuzfahrerstaaten als Multikulturelle Gesellschaft: Einwanderer und Minderheiten im 12. und 13. Jahrhundert*, éd. Hans Mayer, R. Oldenbourg, 1997, repr. dans *Franks, Muslims, and Oriental Christians in the Latin Levant*, Aldershot: Ashgate, Variorum Collected Studies Series, 2006, pp 129-40.

Talmon-Heller Danielle, « The Shaykh and the Community: Popular Hanbalite Islam in 12th-13th Century Jabal Nablus and Jabal Qasyûn », dans *Studia Islamica*, N°79, 1994, p. 103-120.

Graines d'espoir
----------------

### Jérusalem, vendredi 26 avril 1157

Au sortir du Saint-Sépulchre, Ernaut et Lambert s'étaient dirigés droit vers Malquisinat où ils savaient trouver de quoi se restaurer. Tout en fendant la foule, ils parlaient d'une voix forte, enjouée, ce qui n'était pas fréquent chez le plus âgé des deux frères. Aussi espiègle qu'un enfant, il faisait de grands gestes et riait d'un rien, attirant l'attention sur lui.

L'entretien avec l'intendant descendu de la Mahomerie s'était fort bien passé. Des terres et un logement l'attendaient désormais dans le casal au nord de la cité, pour peu qu'il prêtât serment et accepte à l'avenir de verser champart et payer cens. Rien que d'habituel en ce cas. La perspective le mettait en joie, même s'il savait qu'il s'y installerait seul, et pas avec Ernaut, comme cela avait été envisagé à l'origine. Pourtant, son entrain demeurait intact.

« Il va me falloir trouver semences pour le prochain automne. Le frère Pisan m'a dit que les semailles se font dès après les premières pluies, on récolte fort tôt ici.

--- C'est qui ce Pisan ?

--- L'intendant du casal, avec qui je parlais en sortant du cloître. Il m'a invité à faire visite aussi tôt que possible, que je voie les terres. »

Ils contournèrent un groupe dense d'ouvriers transportant des planches et des clayonnages, les vêtements collés de mortier. Lambert les suivit du regard tandis qu'ils les dépassaient.

« Il me faudra aussi trouver oustillement en suffisance. Mieux vaut l'acheter ici, j'aurais plus grand choix.

--- Tu parlais de semences, mais as-tu terre à vigne aussi ?

--- Certes, quelques maigres arpents, et je pourrai planter mes propres ceps en nouvelles pentes, des champs seront tôt ouverts. Il me faudra tonnels pour garder ma vendange et paniers à ramasser les grappes.

--- Tout doux, frère, tu n'as pas encore le moindre cépage ! »

Lambert se mit à rire.

« Tu parles de raison. Il faut prévoir et dépenser droitement le pécule de père. Il ne s'agirait pas d'être à court lorsqu'il faudra œuvrer. Seulement alors j'aurai besoin d'avoir paniers, houe et serpe...

--- Je pourrai t'aider en cela. Je n'ai guère usage de ma solde.

--- Il te faut penser à ton établissement aussi, tu ne demeureras pas sergent du roi. »

Ernaut haussa les épaules, souriant, mais ne répondit pas. Il n'avait prêté serment que depuis peu et découvrait à peine le travail. Il consistait avant tout à surveiller les portes de la ville la journée, non pas pour en repousser d'éventuels attaquants mais afin de taxer les denrées y entrant. Des patrouilles la nuit, et des rondes sur les murailles, voilà ce qu'on lui demandait. Aucune besogne bien difficile, rien d'exaltant non plus. En fait, il était susceptible de remplir n'importe quelle tâche pour le roi, et il avait juré de le servir fidèlement. Certains de ses collègues lui avaient raconté qu'ils portaient des messages, escortaient des ambassades, accompagnaient l'ost royal lors des combats. À ce titre, il avait le droit de porter une épée au côté dans la cité sainte. Il faisait d'ailleurs partie des rares à s'entraîner, sinon régulièrement, du moins honnêtement.

Ils débouchèrent dans la rue aux Herbes, après avoir dépassé le change syrien. La foule était plus dense et le tohu-bohu des langues et sabirs se mélangeait aux bruits de cuisine, aux aboiements des chiens quémandeurs. Chaque pas faisait découvrir une nouvelle senteur, sucrée, saline ou acide, flattant le nez ou piquant les yeux. Lambert indiqua du doigt un vendeur de pain occupé à trancher une large miche, projetant des miettes et des mies sur le sol aux alentours.

« Bientôt je ferai moudre mon propre grain. Quoi de mieux que se nourrir de son bien, mon frère ?

--- Se nourrir dès à présent je dirais, rétorqua Ernaut, moqueur. Il me faudrait fief de moûtier pour contenter mes appétits ce midi.

--- Achetons donc gras pâté de viande, fêtons ces bonnes nouvelles ! » approuva Lambert, enthousiaste.

### Jérusalem, matin du mardi 7 mai 1157

Ernaut déposa le sac sur le sol, laissant à son frère le soin de l'assujettir sur la mule.

« Voilà tes dernières affaires ! »

Ils se tenaient au milieu de la rue, au bas de l'immeuble dans lequel ils s'étaient installé à leur arrivée, plusieurs mois avant cela. Saïd, leur voisin, était présent. D'un naturel serviable, il se proposait toujours pour aider, détaillant ses actions en un baragouinage que lui seul comprenait. Il flattait l'encolure de la bête, lui sussurant des mots aimables.

L'aube était encore proche et le ciel pas tout à fait bleu, la rue ombragée exhalait l'humidité et la fraicheur. Lambert se gratta le front, soulevant son chapeau de paille, tout en laçant les dernière courroies.

« De toute façon, tu pourras toujours m'apporter ce que j'aurai oublié. Tu ne seras pas si long à me faire visitance.

--- Certes pas. Le casal n'est qu'à une poignée de lieues, avec un des chevaux d'Abdul Yasu le voyage sera tôt fait. »

Lambert s'accouda sur les paquets, un sourire forcé sur le visage.

« Eh bien voilà, cette fois, je crois que nous y sommes !

--- Paysan en la terre du Seigneur, qui l'eût-cru ? glissa Ernaut dans un sourire.

--- Tant de mois à patienter, et c'est désormais là. Il frappa de la main sur ses affaires, comme s'il carressait un animal. Père serait si fort enjoyé de savoir cela !

--- Si d'aucuns Bourguignons passent, j'essaierai de lui faire porter nouvelles.

--- Je me demande ce qu'il fait en ce moment.

--- M'est avis que si le temps a été, il a fini le liage et s'est attaqué aux gourmands, serpe en main. »

L'évocation des travaux familiers rendit Lambert nostalgique et il approuva, ému.

« J'espère pouvoir faire de même bientôt et te donner à goûter le jus de ma treille.

--- Si tu n'en fais pas vin aigre, je veux bien m'y risquer ! »

Lambert tapa amicalement sur le bras de son frère et concéda un rire forcé, mal à l'aise. Plus âgé de pas mal d'années, il s'était toujours senti responsable d'Ernaut, d'autant qu'ils avaient traversé le monde pour venir dans le royaume de Jérusalem. Bien qu'il soit très heureux d'enfin s'établir, il réalisait désormais qu'il le ferait seul, maintenant que son frère était entré dans la sergenterie du royaume. Le jeune homme sentit le silence s'installer et serra à son tour le bras de son aîné.

« Tu es sûr que tu ne veux pas que je te compaigne ?

--- Nul besoin. Je dois juste mener ces dernières affaires et commencer à m'installer. Tu viendras me voir quand je serai prêt à te faire bonne héberge.

--- J'apporterai quelques rissoles et oublies de Margue si tu veux bien, ce sera moins risqué !

--- Je ne dis pas non, je n'ai guère espoir de trouver pareils délices en le casal. Mais j'ai déjà encontré d'aucuns colons de chez nous, de Bourgogne. Ils m'ont fait bon accueil. »

Ernaut sourit.

« C'est toujours bon d'avoir compaings avec qui rompre le pain. Je pense me rapprocher un peu du palais pour ma part, et dénicher meilleur logis que celui-là. Droart m'a parlé d'une chambre au près de la rue du Temple. Je pourrai mieux y accueillir mes compères.

--- Je te ferai visite en ce nouveau logis à ma prochaine venue en la cité alors.

--- Certes ! »

Lambert acquiesça plusieurs fois, lentement, tourna les yeux vers ses affaires, se remémorant rapidement le contenu des balles et des paniers, se passant le pouce autour des lèvres tout en énumérant les objets. Puis il expira longuement, presque un soupir.

« Il est temps... Le soleil va chauffer ce jour et j'aime autant marcher avant qu'il ne m'arde trop avant. »

Il s'avança et enlaça le géant qui lui servait de frère, dérisoire silhouette face à la montagne de muscles qui l'enserra tel un ours.

« Prends soin de toi, Ernaut, et évite les bêtises et les ennuis.

--- Je suis de ceux qui les empêchent, désormais ».

Après une longue accolade, ils se séparèrent doucement et Lambert récupéra la longe de la mule que lui tendait Saïd. Il le salua d'un signe de la main et s'avança, invitant la bête à le suivre d'un claquement de langue. Le regardant s'éloigner puis disparaître au coin de la rue, Ernaut se figea un moment, les yeux dans le vague et, d'un geste, interpela Saïd :

« Ça te dirait d'aller vider quelques godets ? C'est moi qui rince. »

### Casal de la Mahomerie, midi du mercredi 29 mai 1157

La chaleur était écrasante et les hommes s'étaient réfugiés sous l'ombre des caroubiers et des alliboufiers[^101], en grappes resserrées. Le mitan du jour était abandonné au soleil, aux cigales et aux reptiles.

Pour les moissonneurs, le repas puis la sieste permettaient de reprendre des forces avant de retourner s'échiner, faucille en main, sur les tiges et les épis dorés. Ernaut mâchonnait de la sève de pistachier, alangui, les mains sous la tête. Il ne portait que sa chemise et des braies retroussées au plus haut sur les cuisses. Lambert avait remonté ses manches et lacé ses jambières sous le genou. Il était assis le dos contre un tronc, dévorant à belles dents un morceau de pain et du fromage dur. Il contemplait la vaste parcelle qu'ils avaient attaquée au matin, où les gerbes s'empilaient désormais en faisceaux couleur miel.

Des meules bâties la veille sur les terres alentours ponctuaient les champs aux tiges désormais rases. Ernaut soupira d'aise, s'appuya sur un coude, s'empara d'une outre, y but à la régalade une longue gorgée. Une brise évanescente avait séché leur sueur et ils étaient désormais prêts à goûter un peu de repos. Lambert s'allongea à son tour, souriant.

« Nous aurons bien tôt fait la récolte, à ce rythme !

--- M'oui. On va bon train.

--- Dire qu'au pays, certains voient à peine les pousses sortir. Avec si beau temps, les chaumes sont bien secs...

--- Ce n'est pas tant le soleil qui pose souci ici, mais les pluies automnales, à ce qui se dit. »

Lambert tourna la tête vers son frère et sourit.

« Certes. Chacun a les yeux rivés au ciel dès la saint Martin passée. Ce n'est pas comme chez nous...

--- Je croyais que c'était ici, chez toi, maintenant » répliqua Ernaut, taquin.

Lambert lui lança une tige de blé.

« En tout cas, j'aurai de quoi semer mes terres à la fin de l'an, comme ça.

--- Le gars t'a donné un tarif pour les grains ?

--- Non, ça dépendra de la récolte, mais vu les jours passés à l'aider, je suis acertainé d'avoir bon prix.

--- Tu n'as pas si grand de terres à emblaver. Tu veux y mettre quoi ?

--- Blé et orge. De ce dernier, il a déjà nombeux silos bien pleins il m'a dit. Je ferai aussi du sésame aux fins de ne pas affaiblir la terre. Outre, j'ai un bosquet pas loin, tu as vu. Les champs gastes[^102] commencent au-delà. Il se pourrait que j'y mette plusieurs paniers pour les mouches à miel. »

Ernaut grogna en acquiescement, bougeant légèrement afin d'éviter l'ankylose.

« J'y suis allé tantôt me soulager, d'aucuns y prélèvent la résine.

--- J'ai vu ça. Il faudra que je me renseigne sur les droits. On peut en faire bon commerce, en plus des cultures. Mais je vais avoir déjà fort ouvrage, avec les vignes.

--- Le vin qu'on nous a donné n'est pas mauvais d'ailleurs. »

Lambert hocha la tête en assentiment.

« Pas de quoi le vendre bien loin mais autant de soleil, ça ne fait pas de mal, certes. Seulement, celui qui taillait avant moi avait la main un peu lourde et l'entretien n'est pas à mon goût.

--- Les meilleures parcelles sont prises depuis longtemps je crains. »

Lambert s'éclaircit la gorge, se massant les cuisses tout en parlant.

« Ce n'est pas tant la terre qui manque que les hommes. Mes fils m'aideront quand ils seront en âge.

--- Tes fils ?, se moqua Ernaut. Tu n'as même pas épouse, sais-tu que cela est utile pour en avoir ?

--- Maintenant que j'ai du bien, ce ne sera guère difficile à trouver, frère. » ❧

### Notes

La société latine moyen-orientale au XIIe siècle n'était pas exclusivement urbaine ainsi qu'on l'a longtemps cru. De vastes zones géographiques accueillaient des colons, incités à s'établir par les taxations plus légères offertes par les propriétaires terriens, souvent des établissements religieux.

Les implantations de villages de colons étaient planifiées, organisées, parfois sur les lieux mêmes d'anciennes localités abandonnées les siècles précédents. Ces agglomérations, appelées « casal » complétaient le maillage rural composé de villages autochtones, peuplés de paysans indigènes, de toutes confessions.

Il arrivait parfois que des liens se tissent entre chrétiens latins et orientaux, mais en dehors de ces deux communautés, les populations ne se mélangeaient pas, pour des raisons à la fois culturelles et légales. Les musulmans étaient par exemple considérés comme des serfs, des non-libres, ayant guère plus de droits qu'un meuble.

### Références

Ellenblum Ronnie, *Frankish Rural Settlement in the Latin Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1998.

Pringle Denys, *Fortification and Settlement in Crusader Palestine*, Aldershot : Ashgate Variorum, 2000.

Rey Emmanuel Guillaume, *Les colonies franques de Syrie aux XIIe et XIIIe siècles*, Paris : A. Picard, 1883.

La reine du nord
----------------

### Jérusalem, matin du mardi 3 septembre 1157

Une langoureuse lumière orangée emplissait les rues abandonnées aux chiens, aux rats et aux lève-tôt. Ernaut était de ceux-là et clopinait, un sac de cuir à l'épaule, en direction de la forteresse de la tour de David. Il baillait à s'en décrocher la machoire, les yeux encore embués de sommeil. Inquiet de se présenter trop tard pour le départ, il avait peiné à s'endormir et s'était levé alors qu'un velours ambré commençait à peine d'éclaircir l'orient. Ses affaires prêtes depuis la veille, il s'était habillé et avait pris le chemin du départ en un instant.

Il avait été désigné comme escorte pour quelques émissaires se rendant à Naplouse. Là, dans la demeure de la vieillissante reine Mélisende[^103] allaient se retrouver des ambassadeurs et représentants de nombreux dirigeants. Parmi eux, on comptait Thoros d'Arménie, avec lequel les seigneurs latins espéraient organiser une campagne dans le nord, profitant de la désorganisation de leur ennemi commun Nūr ad-Dīn suite aux tremblements de terre de l'été. La mère de Baudoin[^104] était fille des montagnes arméniennes et conservait un grand prestige, dont elle se servait désormais au service de son fils, et du royaume comme toujours.

Ernaut était curieux de découvrir cette femme qui avait bataillé ferme pour défendre Jérusalem contre tous ses adversaires, dont les sœurs avait épousé les princes des territoires voisins, et qui n'avait pas hésité à prendre les armes pour s'opposer à un fils qu'elle n'estimait pas encore assez mûr pour lui succéder. Aux abords de la citadelle, des hennissements, des appels, le tohu-bohu des valets empressés se déversaient dans les rues encore endormies. La vaste porte d'accès était largement ouverte et à l'intérieur, la fébrilité régnait.

Les magnifiques palefrois de la suite royale étaient apprêtés, les hongres des sergents bouchonnés et des paquets prenaient place aux flancs des roncins de bât. Jamais le jeune homme n'avait pris part à un tel équipage. Jusqu'alors, il se contentait de les regarder passer, au hasard de la route d'un évêque, d'un comte ou d'un abbé. Cette fois, il serait de ceux qui toiseraient fièrement les pieds-poudreux, les industrieux de la glèbe, qui n'oseraient lever sur eux les yeux qu'emplis de respect et de crainte.

Il rejoignit les compagnons avec qui il chevaucherait, le visage plein d'espoir et d'émerveillement. Droart l'accueillit d'une bourrade amicale mais rapide.

« Tu arrives à point, l'ami. Il faut aider les valets à lier les besaces et sacoches que tu vois là. »

Il s'arrêta et regarda Ernaut des pieds à la tête, le regard malicieux.

« Que te voilà en grand appareil, maistre Ernaut. Pourquoi donc t'endimancher pareillement ?

--- Eh bien, n'allons-nous pas chevaucher au suivi de la bannière royale ? Il nous faut mener grand train, à errer ainsi au nom du souverain.

--- Voilà bien vérité, par ma foi. Mais nous allons surtout chevaucher parmi caillasse et poussière, et ta cote flamboyante aura couleur de souris à notre arrivée. Roule-la donc en ta sacoche et enfile vêtement moins clinquant. Garde ta belle tenue pour Naplouse. »

Ernaut opina et se dépêcha d'accomplir ce qui lui avait été recommandé. Il passa donc, non sans regret, sa vieille cotte, d'un vert délavé, dont le chemin ne pourrait entamer la teinte, déjà fort passée. Peu après résonna un premier long coup de trompe, lancé depuis la maîtresse tour. Droart lui sourit :

« Au premier cornage, chacun se doit de s'aprêter. »

Ernaut acquiesça et noua ses affaires au troussequin de sa selle. Tandis qu'il s'affairait à régler la sous-ventrière, il aperçut le petit groupe de dignitaires qui sortait d'un des bâtiments. Autour du connétable Onfroy de Toron, dont la barbe majestueuse n'était pas sans rappeler celle du grand Charles[^105], gravitaient plusieurs hommes de moindre envergure. Parmi ceux-ci, il reconnut le visage basané et le regard perçant de Régnier d'Eaucourt.

Le chevalier était vêtu d'une tenue de voyage très sobre et avançait à grandes foulées, remuant les mains tandis qu'il parlait. Peu de temps après, Ernaut eut l'occasion d'échanger quelques mots avec Ganelon, son valet. Il lui confirma qu'ils faisaient partie du convoi, son maître ayant de nombreux contacts avec les familiers de la reine, qu'il avait servie fidèlement pendant des années avant de se résoudre à rejoindre Baudoin. Lorsque la bannière portant les couleurs de Jérusalem sortit de la chapelle, fièrement brandie par un jeune homme en grand haubert, l'activité redoubla. Seul Ernaut perdit quelques instants à admirer le carré de soie ondoyant dans le vent.

Un second coup de trompe retentit, l'arrachant à sa rêverie.

« En selle ! lui cria Droart. Nous allons former conroi de route. »

Un chevalier au menton volontaire, le visage barré d'une cicatrice qui lui avait emporté une partie du nez allait et venait le long de la colonne, indiquant d'une voix sèche ou d'un index autoritaire les emplacements de chacun. Puis il rejoignit l'escorte du connétable.

Tout le monde attendait, les voix s'étaient tues. On n'entendait que le piétinement des sabots, le ronflement des étalons tenus serrés, le cliquetis des harnais. Onfroy de Toron leva le bras et la trompe sonna une dernière fois, indiquant le départ de la troupe. Ernaut admira les chevaliers menés par la bannière royale franchir le portail et talonna son cheval pour tenir sa place, parmi les valets et les bidets.

### Naplouse, fin de journée du mardi 3 septembre 1157

Lorsque la colonne était finalement parvenue à Naplouse, Ernaut se sentit soulagé. Il n'était pas habitué à chevaucher toute la journée et ses cuisses, ses fesses étaient endolories par la selle dont le matelassage lui semblait tout symbolique. Les dernières lieues lui avaient paru une éternité. La chaleur avait été assommante et, bien qu'ils eussent fait une longue halte à la mi-journée pour se protéger des moments les plus chauds, il avait la gorge sèche et la bouche comme du carton, ayant fini sa gourde depuis bien longtemps.

Lorsqu'il avait réalisé qu'ils prenaient la route de la Mahomerie, il avait secrètement cultivé l'espoir que son frère pourrait l'admirer tandis qu'il passerait, majestueux, dans la colonne royale. Mais, las, ils étaient passés au large du casal, sur la grande route, et nul paysan n'avait pointé son nez pour venir les soutenir de cris de joie ou, à défaut, les admirer en silence. Il avait remarqué que, si parfois quelques-uns levaient le nez de leurs travaux, la plupart se contentaient d'un rapide coup d'œil, habitués qu'ils étaient à voir passer des contingents d'hommes en armes à la suite d'une bannière. Le royaume était en guerre de façon quasi permanente depuis sa création et, lorsqu'il ne l'était pas, il s'employait à préparer la prochaine. L'enthousiasme d'Ernaut était donc retombé, au même rythme que ses épaules, tandis que la chevauchée pesait plus lourdement sur son corps.

La cité de Naplouse lui parut bien modeste, étant donnée sa réputation. Aucune enceinte n'en protégeait les habitations, et nulle forteresse de grande ampleur ne surplombait la verdoyante vallée où elle se lovait. Sur les pentes environnantes, des terrasses soigneusement entretenues accueillaient une grande variété de cultures, avec une prédominance de vignes et d'oliviers. Une vaste palmeraie enserrait les bâtiments, apportant ombre et fraicheur.

Ils stoppèrent à peine arrivés, la plupart des sergents ayant ordre d'attendre à l'entrée des quartiers commerçants, près d'une grande église. Le connétable et les chevaliers continuèrent, accompagnés de quelques valets. Plusieurs commerces étaient encore ouverts, des coups de maillets se faisaient entendre d'une échoppe non loin.

Un vieil homme à la barbe mitée, un bonnet tricoté de travers sur le crâne, s'arrêta devant eux, tenant ferme la longe de son âne qui disparaissait sous un impressionnant chargement de bois. Il resta à étudier la troupe un petit moment avant de s'effacer dans une des ruelles adjacentes. Ne demeuraient à les admirer que quelques gamins à l'allure espiègle qui dévisageaient, les yeux écarquillés, les valeureux hommes du roi.

Un frère convers de l'hôpital, reconnaissable à une tonsure peu récente et une tenue fatiguée, vint à eux et se présenta. Il avait pour charge de les mener là où ils seraient hébergés le temps qu'ils resteraient à Naplouse. Ernaut grimaça, il avait espéré demeurer auprès des puissants, et se trouvait relégué dans l'hospice des voyageurs, avec les pèlerins et les malades.

Le logis était non loin, massive construction aux bloc soigneusement appareillés. En peu de temps, la cour reprit vie, épouvantant les volailles qui péroraient jusque là. Un chien curieux profita de la pagaille pour venir les renifler puis, son inspection faite, retourna dans la dépendance dont il était sorti. Le moine courait partout, le cheveu fou et le regard inquiet. Il passait régulièrement un doigt agacé dans son encolure, comme si l'étoffe le serrait et il déglutissait bruyamment avant de dire la moindre chose.

Les hommes habitués ne mirent pas longtemps à décharger les affaires et à les porter dans la grande salle où les atttendaient des banquettes munies de matelas et de couvertures. Très vite, certains firent glisser leurs savates, s'allongeant sur la couche qui serait la leur pour les jours à venir. Quelques-uns déballèrent des affaires, d'autres encore allèrent s'enquérir des repas, des commodités ou des attractions que la ville offrait.

Ernaut et Droart furent de ceux qui se préoccupaient de leur estomac avant tout. Le souper était prévu d'ici peu, certains pèlerins assistant à l'office. Droart s'étonna de leur présence, en cette période si avancée de l'année. La plupart arrivaient au printemps et repartaient à la fin de l'été, avant la clôture des mers. Le frère hospitalier haussa les épaules et confirma qu'il était rare, même en plein hiver qu'il n'y ait pas quelques marcheurs qui se présentât.

Lorsqu'ils sortirent de la salle commune où ils venaient de prendre leur repas, Droart se frotta le ventre, soupirant.

« Il doit bien avoir taverne où se payer bon vin. J'ai souvenance de futailles à mon goût en cette bonne cité.

--- Il faudra demander aux goujats plutôt qu'aux frères. Le valet a plus grande habitude de gouleyer le vin à la veillée.

--- M'est avis que d'aucuns pourraient même apprécier de rouler les dés ! » confirma Droart, un sourire fendant son visage rond d'une oreille à l'autre.

Il fit quelques mouvement qui se voulaient d'assouplissement.

« J'ai le fondement comme beurre en baratte ! Espérons que nous aurons beau temps à demeurer à l'héberge.

--- N'avons-nous pas labeur à faire ?

--- Et quoi donc, cul-Dieu ? On nous mandera bien assez tôt. Demain, nous irons au castel de la reine et pourrons roupiller jusqu'à vêpres, j'en ai espoir.

--- Pourquoi donc nous faire venir pour rien ?

--- Parce qu'il plaît au sire roi et à son connétable de faire étalage de leur puissance en voyageant avec moult valets et sergents, quand bien même ils ne servent à rien. »

Il s'esclaffa et ajouta, narquois.

« Et puis Baudoin a peut-être bien appris la leçon de Safet[^106] ! Les démons ont failli lui ardre[^107] le cul. Donc nul voyage sans escorte ! »

Marquant son assentiment, Ernaut sourit. Il demeurait néanmoins fort circonspect quant à la valeur militaire d'une escorte d'hommes comme Droart.

### Naplouse, matin du vendredi 6 septembre 1157

« Cinq ! annonça Ernaut.

« Quatre » répliqua Droart, tout en ouvrant sa main. La mine déçue, le jeune homme s'esclaffa de dépit.

« Cul-Dieu, je ne m'y entendrai jamais à la mourre !

--- C'est affaire de talent et j'ai le don depuis que je suis enfançon. T'y peux rien. »

Les deux compères étaient affalés contre des palmiers sur un talus face à l'entrée principale, dans la cour du château. Comme chaque matin, ils s'étaient présentés à l'aube, prêts à accepter toute mission, mais espérant bien y couper un jour de plus. Ils avaient désormais leurs habitudes et, quand ils ne jouaient pas ou en avaient assez de discuter, se contentaient d'attendre, tranquillement allongés, que le repas soit corné ou que la nuit arrive.

Une silhouette fine s'avança dans leur direction. Visage plat, yeux fatigués et blonds cheveux raides, c'était Jehan, le scribe de la reine. Persuadé de son importance, il avançait à petits pas pressés, ondulant du chef comme une poule anxieuse. De la main, il fit signe à Ernaut d'approcher.

« L'hostel du roi a message à faire porter en la cité.

--- J'ai monture prête, il me faut juste la seller, maître Jehan.

--- En ce cas, suivez-moi ! »

Le clerc repartit au même rythme affairé, comme s'il était le seul à besogner dans la demeure. Ils pénétrèrent dans plusieurs pièces, empruntèrent un escalier étroit pour finalement déboucher dans une grande salle, haute de plafond, aux murs peints de fresques colorées.

Ernaut n'était encore jamais entré là et il se figea, admirant les personnages empreints de majesté qui déambulaient autour de lui dans des vêtements richement ornés taillés dans des étoffes de prix. La pièce était emplie de petits groupes parlant entre eux, foulant des tapis dont le prix d'un seul aurait nourri une famille des années durant. Le vacarme était impressionnant et pourtant nul ne semblait s'en soucier.

Sous un dais brodé de couleurs vives, une femme se tenait debout à côté d'une chaise curule aux couleurs éclatantes. Sa tenue croulait sous les ors et les soieries. Il n'en apercevait guère le profil mais comprit bien vite que la frèle silhouette, à la main nerveuse, était la reine Mélisende. Elle parlait à un homme au physique épais, rond de ventre et de visage, dont la cotte chamarée se tendait chaque fois qu'il bougeait. Il se contentait d'hocher la tête, écoutant avec attention, un index sur les lèvres. Jehan l'amena jusqu'à un petit groupe qui faisait écran devant le connétable, assis sur un coffre, plusieurs documents étalés à côté de lui.

Ernaut fut accueilli dans un sourire par Régnier d'Eaucourt :

« Sergent, voici plusieurs messages à porter à la cité, à l'intention du roi ou du sénéchal, le premier que tu trouveras. Tu y attendras réponse, que tu nous porteras au plus vite. »

Tout en parlant, Régnier indiquait plusieurs documents cachetés, qu'il glissa dans une boîte métallique ornée des couleurs du roi. Le géant ne savait pas quoi dire, le cœur battant la chamade. C'était sa première mission d'importance. Il se contenta d'acquiescer en silence. Le chevalier lui sourit avec chaleur, et posa une main sur son bras, ajoutant à mi-voix :

« Cela signifie qu'il te faut te mettre en selle au plus vite, garçon. »

Ernaut lança un regard désolé, sourit avec reconnaissance et s'éloigna, la boîte à la main. Il en avait vu un certain nombre déjà, tandis qu'il officiait à la porte de la ville. Cette fois, c'est lui qui pourrait pénétrer bride abattue dans les rues de Jérusalem.

Avant de sortir, il jeta tout de même un dernier coup d'œil en direction du siège d'honneur. La reine était désormais assise, écoutant un homme habillé de soie verte, le crâne protégé d'un turban éclatant de blancheur. Son visage fin s'était asséché avec les ans et ses traits accusaient son caractère autoritaire, mais on devinait parfois un sourire fugace dans les séduisants yeux noirs.

Il ne quitta la grande salle qu'à contrecœur, d'un pas traînant, rasséréné néanmoins à l'idée de chevaucher librement jusqu'à la cité, représentant officiel du roi en mission, cavalier d'importance à la monture pressée. Lorsqu'il sortit, Droart s'approcha et remarqua la boîte de messager.

« Te voilà bon pour te tanner le cul jusqu'à la cité !

--- Et retour, car je dois porter réponse.

--- Viens-t'en donc, on va sangler ton roncin. »

Les deux hommes ne mirent pas longtemps à fixer les sangles et le filet, et Droart ajouta deux gourdes et un sac avec du pain et du fromage. Il les fixait à l'arçon de la selle tandis qu'Ernaut réglait ses rênes.

« J'ai souvenir de fringales lors de ces voyages, il vaut mieux prévoir. Et prends garde à la nuit, la lune n'est qu'un trait, tu y verras bien mal.

--- Il me faut chevaucher à la nuitée ?, s'inquiéta Ernaut tout en prenant place en selle.

--- Que crois-tu donc ? Que le connétable te mande à Jérusalem pour t'y prélasser la nuit durant en bordel pendant qu'il espère réponse du roi ? »

Puis il claqua la croupe du hongre, saluant son ami d'un signe de main. Ernaut talonna, donna une impulsion de reins et partit au petit galop, dans un crissement de gravier.

« Te voilà messagier royal, mon vieux ! » ❧

### Notes

Si la reine Mélisende s'est effectivement retirée du gouvernement après l'affrontement qui l'avait opposée à son fils au début des années 1150 (voir Qit'a « Épineuse couronne »), son statut lui permit de conserver une influence certaine. Il semble néanmoins qu'elle se soit contentée de s'en servir pour appuyer la politique de Baudoin, sans plus prendre de décision autonome. La plupart de son administration avait été de toute façon soit remplacée, soit exilée avec elle. La ville de Naplouse connut donc un regain d'activité alors qu'elle était installée là, jusqu'à sa mort au début de la décennie suivante.

Au service des puissants, les messagers pouvaient être de plusieurs types, le plus modeste d'entre eux étant le simple porteur de document, qui n'avait pour mission que de délivrer une lettre, dont il n'est pas certain qu'il ait connu la teneur. Bien qu'on ne connaisse pas de boîte de messager du XIIe siècle, l'iconographie en propose une représentation établie.

### Références

La Monte John, « The viscounts of Naplouse in the twelfth century », dans *Syria*, 1938, vol.19, no.3, p. 272-278.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem » dans *Dumbarton Oaks*, vol.26, 1972, p.93-182.

Weber R.E.J., *La boîte de messager en tant que signe distinctif du messager à pied*, Haarlem : Joh. Enschedé en Znen Grafiche Inrichting B.V., 1972.

Merceron Jacques, *Le messager et sa fiction. La communication par messager dans la littérature française des XIIe et XIIIe siècles*, Berkeley - Los Angeles - Londres : University of California Press, 1998.

Addenda
=======

### Un univers pour protagoniste : Qit'a

Lorsque j'ai entrepris de proposer aux lecteurs de plonger avec moi à la découverte du monde des Croisades, j'ai dès l'origine conçu ce projet de façon globale. Bien que les romans d'Ernaut en constituent la colonne vertébrale pour le moment, j'espère arriver à donner principalement vie à l'univers que j'ai imaginé pour les accueillir : Hexagora. Je l'ai nommé en combinant « Hexagora », du grec hexa, six (allusions à la théorie des six degrés de séparation) et agora, assemblée. Le terme évoque donc à la fois les entrelacements de destins au sein d'un lieu défini et une communauté culturelle dont le cœur est, pour l'essentiel, autour de la Méditerranée, d'où le choix d'un terme issu du grec. Pour parvenir à mon but, je m'appuie entre autres sur les textes courts Qit'a, publiés sur le site d'hexagora chaque mois.

Qu'il s'agisse de l'enfance de protagonistes de premier plan, d'aventures de personnalités secondaires ou de scènes glanées autour d'un fil conducteur spécifique, je tente à chaque fois de rendre plus palpable la réalité dans laquelle se meuvent mes héros, qu'elle soit moins monolithique et plus complexe. Je pense que c'est aussi l'occasion de densifier les personnages en enrichissant leur caractérisation première de détails annexes (voire de révélations sur leur nature profonde).

Je m'efforce également de lister les sources scientifiques qui ont alimenté mon texte ou les processus sociaux et historiques à l'œuvre en arrière-plan. Attention, il s'agit bien pour moi de faire un travail romanesque de fictionnalisation, mais cela ne me paraît nullement en opposition avec une ambition de vérité historique et sociale. Du moins telle que je peux l'esquisser en un paysage vraisemblable selon mes connaissances et l'agencement que j'en fais.

### Des textes courts et fréquents

Le format court n'est guère prisé en édition francophone malgré son succès auprès des lecteurs. Il s'agit d'un exercice qui propose une formidable souplesse de traitement pour les écrivains. Sa moindre complexité par rapport aux longs romans offre une plus grande facilité à tenter des choses différentes de ses habitudes. Ray Bradbury en a d'ailleurs fait la promotion lors d'une conférence, ce qui a incité Neil Jomunsi à se lancer dans un marathon d'écriture voilà quelques années.

En ce qui me concerne, je me contente d'un texte par mois, dans un format plus court que la nouvelle. Lorsque j'ai commencé, c'était en collaboration avec le magazine papier Histoire et images Médiévales (désormais uniquement en ligne) et 12 000 caractères constituaient une limite haute, en partie pour des raisons de maquettage chez eux. Depuis, comme je publie uniquement sur support électronique, j'ai plus de liberté et il arrive que les récits atteignent 25 000 caractères. Ce n'est néanmoins pas un cadre formel strict et je me contente de suivre ce que chaque sujet me semble nécessiter. Comme en sport, la régularité de l'exercice m'importe plus que son intensité relative. Il s'agit avant tout de pratiquer.

Même sans me fixer des objectifs aussi impressionnants que Neil Jomunsi, il est indéniable que cela m'a fait évoluer en tant qu'écrivain. J'ai moins de difficultés à faire les premiers jets, j'améliore mes préparations et chaque Qit'a m'offre l'opportunité de questionner ce que je suis en train de faire, d'avoir du recul sur mon métier, avec l'ambition, bien sûr, de pouvoir partager le fruit de ces réflexions. Par l'exemple avec les textes même, mais aussi en échangeant avec le public lors de rencontres, voire en différé comme sur le blog éponyme.

### De la multiplicité des approches scripturales

De la même façon que je ne me bride pas en ce qui concerne la longueur, je n'ai pas de ligne bien définie dans la manière dont j'aborde chaque nouveau texte. Certains sont basés sur des exercices stylistiques, comme Meh. Il s'agissait de découvrir en un rapide survol une partie de la chaîne de fabrication du tissu de laine, industrie essentielle du monde médiéval. Je m'étais fixé comme objectif de ne jamais recourir à la conjonction « mais », car mon tout premier manuscrit en était truffé et mon éditeur d'alors, Jean-Louis Marteil, m'en avait fait la remarque. D'une blague que nous seuls pouvions comprendre j'ai fait une contrainte stimulante qui m'aidait à ne pas penser uniquement au contenu historique et à sortir de l'ornière du récit trop didactique.

Pour chaque Qit'a, je tente de m'offrir une perspective narrative nouvelle, un traitement stylistique à envisager, en complément du savoir universitaire sur lequel je m'appuie. Cela peut amener parfois à des constructions surprenantes, qui excluent par exemple la notion de protagoniste en tant que tel pour s'intéresser à un lieu et son évolution au fil du temps (je pense ici à *Omphalos* en particulier, dans le Recueil Qit'a 3). Adolescent, j'avais été admiratif du talent déployé par Andreas dans la bande dessinée *Raffington Event, détective*. Chaque histoire courte proposait un nouveau style graphique, un récit structuré différemment. Cela rejoint tout à fait les jeux chers à Raymond Queneau et aux adeptes de l'Oulipo. Il s'agit de s'affranchir de ses biais narratifs, techniques ou conceptuels en s'imposant des obligations clairement définies.

Cela peut bien sûr déboucher sur de véritables exercices, comme les gammes pour un pianiste ou les vocalises pour un interprète, mais l'envisager comme un travail public, qui a vocation à être délivré à des lecteurs, fait que cela doit aussi constituer une œuvre artistique en soi. Il y a fréquemment une réticence, une pudeur, de la part des créatifs et tout particulièrement parmi ceux qui écrivent, à livrer le fruit de leur labeur à un tiers. J'ai toujours pensé que c'était pourtant consubstantiel à l'objet littéraire, qui ne peut s'actualiser qu'une fois qu'un autre s'en est emparé. Avant, il ne s'agit que d'un potentiel, d'une œuvre en suspens. Un peu comme le chat de Schrödinger, sa réalité ne saurait être définie sans observateur.

### De la rigueur historique pour alimenter la cohérence

Constituant fondateur de Qit'a, le Moyen-Orient des croisades n'est pas que le cadre qui accueille les récits. Il s'agit d'en faire un personnage à part entière, de la même façon que la Terre du Milieu est le héros de Tolkien, Tschaï celui de Jack Vance ou l'Empire, à travers les Rougon Macquart, celui d'Émile Zola. Au final, la construction de Hexagora est totalement dans la lignée de ce que Balzac avait opéré en son temps avec la Comédie humaine. Je n'ai commencé par de longs récits policiers que par la rigueur que cela demande, et donc les garde-fous qu'une pratique aussi exigeante offre au jeune écrivain.

Néanmoins, à la différence de ces glorieux et talentueux prédécesseurs, je dépeins une société disparue qui a préexisté à la nôtre. Je ne saurai réfuter l'évidente influence de cette dernière sur mes constructions et la manière dont je recompose l'histoire, mais j'ai le projet de proposer aux lecteurs une plongée dans un univers distinct de son quotidien. Ce souhait est issu de mon dépit à voir présentée cette période des croisades d'une façon extrêmement caricaturale, où le mythe et les requalifications postérieures alimentent de nombreux fantasmes de tous genres. De ce noir et blanc tranché j'ai eu l'envie de tirer un camaïeu, et j'espère même arriver à intégrer ici et là des couleurs variées.

Malgré tout, dans ces créations, je ne suis nullement un historien, dont la pratique est totalement distincte de ce que je fais. Un tel chercheur fait œuvre scientifique tandis que je m'adonne à la création artistique. Il ne faut pas se voiler la face, peu de personnes lisent des ouvrages universitaires et une grande part de ce qui est considéré comme de la culture historique a été délivrée au public par le biais de fictions. Pour prétendre à cet adjectif « historique » en épithète de mes romans, il m'apparaît essentiel d'aller aux sources du sujet que je traite.

Depuis mes études, je suis plongé, par goût, dans la période médiévale. Je fais par ailleurs de la reconstitution historique, ce qui m'a permis de découvrir ce qu'est, de façon pratique, de charger en armure avec une lance, de forger ou aborder un col enneigé en tenue de pèlerin médiéval, entre autres. Il me semble important de concilier connaissances théoriques et expérience esthétique, physique, pour nourrir mon imaginaire et mon écriture.

Ce sont pour ces raisons que je me présente souvent comme un expert de cette période. Expertise basée sur des travaux universitaires de nombreux spécialistes, archéologues, linguistes, juristes, etc., et qui s'exprime à travers mon œuvre de médiateur. Une des difficultés est justement de s'extraire de cette science, de la digérer pour la rendre assimilable par d'autres, de ne plus être dans la posture du savant, du professeur, mais du poète qui raconte.

Rien n'est plus fatigant, dans certains romans historiques, que de devoir parcourir des pages entières d'énumérations minutieuses et exhaustives de généalogie ou de descriptions topographiques qu'on croirait sorties de guides touristiques. Nul besoin de recourir à la fiction si on se contente de lister des faits. Il faut apporter autre chose en tant que romancier. Ces éléments doivent prendre vie, se présenter de façon naturelle pour offrir au lecteur l'opportunité qu'il les fasse siens. Il doit être littéralement projeté dans le récit, non pas se retrouver dans un antique musée poussiéreux exhibant des artefacts plus ou moins bien étiquetés.

Je m'efforce donc de parcourir la bibliographie la plus à jour à laquelle je peux accéder ou que je peux acquérir, car les publications universitaires coûtent parfois très cher - au passage merci aux éditions Brill de dépasser régulièrement les 100 € pour un livre ! Mais après cela, je laisse ces informations en repos, infuser lentement, se recouper avec d'autres lectures, pour enfanter une trame qui prenne véritablement place dans le contexte, pour qu'elles ne soient pas des idées transplantées dans un cadre exotique certes, mais sans aucun rapport avec celui-ci.

L'influence de la documentation s'exerce à plusieurs niveaux des arcs narratifs : destin individuel ou évolutions au long cours qui traversent plusieurs récits. À cet égard, j'ai été particulièrement enchanté du talent avec lequel Alexander Kent (alias Douglas Reeman, tout récemment décédé hélas) nous fait voyager avec la marine anglaise au tournant des XVIIIe et XIXe siècles dans sa saga des Bolitho. Je cite également souvent Jean-François Parot (récemment défunt aussi, malheureusement), avec les enquêtes de Nicolas le Floch, comme un exemple inspirant. J'ai même rendu hommage à son goût pour les recettes narrées par un des protagonistes dans *Acédie* dans le Recueil volume 3.

Les Qit'a m'offrent l'occasion d'attirer l'attention des lecteurs sur des anecdotes dont le traitement en roman ne me semble pas envisageable. Parfois, je m'en sers pour enrichir la perspective autour d'un événement déjà abordé dans un autre texte. Leur récurrence et leur brièveté permettent de ne pas devoir broder sur des points inconnus en s'attardant sur ce qui est à peu près certain. Les liens qui se tissent ensuite sont l'apanage de chacun. Je peux de temps à autre également m'amuser à semer des éléments très diffus, plaçant par exemple en arrière-plan des personnages cités, mais non nommés, mais que les plus attentifs des lecteurs pourront reconnaître. Quoi qu'il en soit, être capable de relier tous ces fils n'est nullement nécessaire pour comprendre la trame générale et ne constitue qu'une récompense supplémentaire pour les plus joueurs.

### Une licence libre pour reverser au pot commun

Lorsque j'ai commencé à rédiger des textes courts, il me semblait important de les partager sans retenue. Comme les enquêtes d'Ernaut étaient publiées par ailleurs chez un éditeur classique, une licence de libre diffusion (creative commons CC BY NC ND) convenait parfaitement à mes partenaires. J'ai eu l'occasion en conférence d'évoquer la facile acceptation de cette licence dans le milieu éditorial traditionnel. Cela permet de circonscrire une certaine liberté accordée aux lecteurs, et de ne pas risquer de voir des éditions concurrentes apparaître. Je me suis néanmoins vite trouvé à l'étroit dans cette solution, car j'avais la sensation d'accaparer une production que je savais issue du patrimoine commun. J'avais en quelque sorte l'impression de privatiser l'histoire.

J'avais par ailleurs espoir que des personnes souhaiteraient participer à cette création, sous d'autres formats, afin de nourrir cet univers fictionnel à base historique. Mon envie d'écrire certaines chansons populaires vient probablement de ce désir. C'est ce qui m'a motivé à évoluer vers une licence plus libre, qui ne restreigne pas les autres artistes et ceci sans qu'ils n'aient d'autorisation préalable ni de validation à me demander. C'est souvent un des regrets quand je vois une riche création captive de l'imaginaire de propriétaires bien définis. La fanfiction constitue à mon avis un pis-aller, une condescendance inavouée qui hiérarchise les travaux et qui demeure à chaque instant soumis à une possible oblitération totale de la part des ayants droit légaux. La rencontre avec Framabook m'a permis de concilier mon espoir de voir Ernaut continuer en tant que projet éditorial relativement traditionnel, en partenariat avec un éditeur et mon souhait de libérer mes textes sous une licence entièrement libre (CC BY SA).

Les conditions qui sont apportées à ce partage est la mention de l'origine, à savoir mon site et moi, de l'œuvre modifiée, mais surtout de garantir que toute nouvelle création issue de mon travail bénéficiera de cette même licence ouverte. C'est la notion de copyleft, qui constitue une des questions les plus épineuses du monde du libre. J'ai fait ce choix, car j'ai non seulement envie que mon œuvre soit versée aux communs, mais que ce qui en serait dérivé y figure aussi, de façon à nourrir cet espace d'interaction. Rien ne serait plus frustrant pour moi que de voir un bel ajout demeurer à part, interdit à l'appropriation.

Je reconnais qu'il s'agit là d'une bascule intellectuelle peu évidente, de ne pas chérir ce qu'on nomme, fort maladroitement à mon sens, le droit d'auteur. Il m'a fallu un certain temps pour en arriver à ce point où j'ai envie de lâcher prise sur le fruit de mon travail. La maîtrise que j'en avais jusqu'ici était de toute façon très illusoire. Par contre, ce nouveau paradigme accentue la difficulté à en vivre, car, déjà que l'industrie traditionnelle offre de moins en moins de débouchés aux écrivains, cette opportunité n'a aucun modèle économique à lui opposer, si ce n'est le soutien/mécénat par une communauté.

Voilà désormais un peu plus de huit ans que je sème à tout vent les graines qui cherchent à germer en vos esprits pour y donner naissance à une certaine vision de l'univers des croisades. Cela a donné lieu à une centaine de textes courts, représentant en gros la longueur de plus de quatre tomes d'Ernaut. Il était temps de détailler un peu ce qui motivait ce projet pour ceux qui n'avaient pas l'opportunité de m'entendre le leur expliquer directement.

Si vous appréciez mon travail, avez envie de me permettre à continuer de nourrir cet univers partagé, n'hésitez pas à vous rendre sur la page de soutien du site Hexagora :

https://hexagora.fr/fr:donate

Texte repris et actualisé à partir de https://hexagora.fr/fr:blog:intro\_qita du mercredi 26 avril 2017

[^1]: Début du Psaume 119(120), premier Cantique des degrés, souvent chanté par les pèlerins.

[^2]: Gauche

[^3]: À Vézelay, où l'on disait trouver des reliques de Marie-Madeleine.

[^4]: À Bourges.

[^5]: Saint-Jacques de Compostelle.

[^6]: Bénédicité populaire en latin, « Bénissez-nous, Seigneur, ainsi que la nourriture que nous allons prendre, par Jésus-Christ Notre-Seigneur. », les présents répondent alors « Amen », c'est-à-dire « Ainsi soit-il. ».

[^7]: L'abbé de Cluny, Pierre le Vénérable (v.1094-1156).

[^8]: « Je te recommande à Dieu ».

[^9]: Capitaine d'un vaisseau de guerre.

[^10]: Le roi Alfonse VII de Castille (1126-1157) se faisait appeler l'empereur d'Espagne.

[^11]: Tunnel d'attaque pour un siège.

[^12]: Type de catapulte.

[^13]: Relever l'ancre.

[^14]: Rang de boucliers qui protège les rameurs.

[^15]: Nom espagnol du dinar, pièce d'or à la base du système monétaire musulman.

[^16]: Les Génois firent cet assaut la veille de la sainte Luce.

[^17]: On vérifiait sommairement la qualité des pièces en les mordant et en les faisant tinter, avant de les peser.

[^18]: « Allez, la messe est dite ». Formule chantée qui indique la fin de la messe.

[^19]: Les territoires latins du Moyen-Orient.

[^20]: Mis sur la paille.

[^21]: On a longtemps propagé des citations pseudo-augustiniennes qui toléraient la prostitution, ce qui en légitimait peu ou prou l'encadrement et l'organisation.

[^22]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^23]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^24]: 'Ayn ad-Dawla Tûmân al-Yârûqī, important émir de Nūr ad-Dīn, qui avait contribué à chasser Usamah ibn Munqidh de Damas quelques années auparavant.

[^25]: 19 juin.

[^26]: Commandant d'un navire commercial.

[^27]: La navigation s'arrêtait de façon traditionnelle l'hiver en Méditerrannée.

[^28]: Guillaume II de Nevers, 1083-1148.

[^29]: De Bourges (1120-1137).

[^30]: Embarcation de taille moindre que les grands chalands de portages, souvent en convois.

[^31]: Eudes Arpin ( ? -- vers 1130), ancien vicomte de Bourges, devenu moine après son voyage en Terre sainte vers 1100.

[^32]: Moine se consacrant à des tâches subalternes, souvent manuelles.

[^33]: Jardin.

[^34]: Enfant donné à un monastère pour en faire un religieux.

[^35]: « Rendons grâce à Dieu ».

[^36]: Longue extrémité du turban.

[^37]: Sorte de robe à manches longues, vêtement de base des populations moyen-orientales d'alors.

[^38]: La ville avait été remise courant juin par les Damascènes au royaume de Jérusalem, après un siège commun de la ville tenue par un officier sous l'autorité de Zengî, alors en guerre avec Damas.

[^39]: On vérifiait sommairement la qualité des pièces en les mordant et en les faisant tinter, avant de les peser.

[^40]: Les pouvoirs publics scellaient des bourses de monnaies dont le contenu était garanti, permettant d'attester d'une valeur sans peser individuellement chaque pièce.

[^41]: Milice urbaine.

[^42]: Bâton, hampe. Désigne plus largement tout ce qui comporte un long manche, arme comme outil.

[^43]: Armée.

[^44]: Godefroy de Bouillon (v.1058-1100), conquérant de Jérusalem, dont le personnage est vite devenu légendaire.

[^45]: Geoffroy Plantagenêt (1113-1151), duc de Normandie depuis 1144. Son père était Foulque V d'Anjou (1092-1144), qui épousa Mélisende de Jérusalem en 1129.

[^46]: Guillaume le Conquérant (v.1027-1087).

[^47]: Henri Ier Beauclerc (1068-1135), en 1105, prit la ville de Bayeux et le feu s'y répandit, détruisant entre autres une partie de la cathédrale.

[^48]: Rocher réputé dangereux.

[^49]: Petit bouclier léger.

[^50]: Ou comptour, désigne les barons les plus éminents d'Auvergne.

[^51]: Sentier tracé en forêt pour y établir des coupes.

[^52]: Nom médiéval de la ville de Dienne (Cantal).

[^53]: Expression auvergnate signifiant « Nom de Dieu ».

[^54]: Vent tempétueux d'hiver qui forme les congères.

[^55]: Robert III d'Auvergne (v.1095-v.1147).

[^56]: Le roi de France Louis VII, 1120-1180.

[^57]: Unité familiale d'exploitation du sol cultivable.

[^58]: Charlemagne était la figure légendaire favorite de beaucoup de traditions, et on lui prêtait des voyages un peu partout.

[^59]: Foulque d'Anjou, dit le jeune, (v.1092-1144) devenu roi de Jérusalem par son mariage avec Mélisende en 1131.

[^60]: Mélisende de Jérusalem (1101-1161) fille de Baudoin II du Bourcq, second roi de Jérusalem.

[^61]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^62]: Partie amovible de la coiffe qui protège le bas du visage.

[^63]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^64]: Genre de loyer.

[^65]: Armée.

[^66]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^67]: Liturgie impliquant des prières à certains moments précis de la journée, et qui concernait alors essentiellement les ordres réguliers.

[^68]: Les trois prières de base que tout bon catholique devait savoir : Credo, Pater noster et Ave Maria.

[^69]: Voir le premier tome des enquêtes d'Ernaut, *La nef des loups*.

[^70]: Voir Qit'a *Scripta manent*.

[^71]: (Job, 42, 1-2).

[^72]: Formation de combat équestre.

[^73]: Se dit d'un vêtement rembourré de façon à s'en servir d'armure.

[^74]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^75]: Conrad, roi d'Allemagne et Louis VII, roi de France, venus en Terre sainte pour ce qu'on nomme la Seconde croisade.

[^76]: Thierry d'Alsace, comte de Flandre.

[^77]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Mélisende et roi de Jérusalem, alors en conflit plus ou moins larvé avec sa mère pour le contrôle du royaume.

[^78]: Seigneur musulman de Salkhad et Busra, qui chercha à gagner son indépendance de Damas en 1147 et se rallia à Jérusalem. Ce fut un échec, malgré les tentatives de négociation, menées par Bernard Vacher pour Jérusalem.

[^79]: Saint Bernard, qui fut un des grands promoteurs de la Seconde croisade.

[^80]: Le roi Baudoin III de Jérusalem (1131-1162), qui prit véritablement le contrôle du royaume en 1152.

[^81]: Mélisende de Jérusalem (1101-1161) fille de Baudoin II du Bourcq, second roi de Jérusalem.

[^82]: Thierry d'Alsace, comte de Flandre.

[^83]: Administration royale en charge des finances, dirigée par le sénéchal.

[^84]: Unités monétaires de compte.

[^85]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^86]: Défaite des latins face aux forces de Nūr ad-Dīn, le 19 juin 1157, qui vit la mort ou la capture de nombreux dignitaires. Voir Qit'a *Chétif destin*.

[^87]: Guillaume II de Bures, prince de Galilée, 1148-1158.

[^88]: Aujourd'hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^89]: Foulque d'Anjou, dit le jeune, (v.1092-1144) devenu roi de Jérusalem par son mariage avec Mélisende en 1131.

[^90]: Outil de coupe des toisons, des poils en général, selon leur taille.

[^91]: Aujourd'hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^92]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^93]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^94]: Chapeau de feutre à fronton triangulaire, un des symboles du statut de guerrier chez les Turcs.

[^95]: Aujourd'hui Safed, ville de Haute Galilée où se trouvait une forteresse franque.

[^96]: Vendredi 18 octobre 1157.

[^97]: Petit siège.

[^98]: Shayzar, dans le nord de la Syrie.

[^99]: L'Oronte.

[^100]: Voir les explications en fin de texte.

[^101]: *Styrax officinalis*, dont la résine séchée donne le benjoin.

[^102]: Désigne les zones impropres à l'exploitation agricole.

[^103]: Mélisende de Jérusalem (1101-1161) fille de Baudoin II du Bourcq, second roi de Jérusalem.

[^104]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^105]: Charlemagne était la figure légendaire favorite de beaucoup de traditions, dont celle des hommes barbus.

[^106]: Défaite des latins face aux forces de Nūr ad-Dīn, le 19 juin 1157, qui vit la mort ou la capture de nombreux dignitaires. Voir Qit'a *Chétif destin*.

[^107]: Brûler.
