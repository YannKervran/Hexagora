====== {0} ======

===== Personnages =====
{4}
===== Lieux =====

===== Chronologie =====
{5}
==== Descriptif court ====
<markdown>
Bonjour,
une nouvelle [publication Qit’a](http://www.ernautdejerusalem.com/fr:qita) vous attend, comme chaque mois, sur le site d’[Ernaut de Jérusalem](http://www.ernautdejerusalem.com/) gratuitement, sans DRM et sous licence {1}.

### {0}

{2}

Texte intégral lisible et téléchargeable gratuitement :
[http://www.ernautdejerusalem.com/fr:qita:{3}](http://www.ernautdejerusalem.com/fr:qita:{3})

En vous souhaitant bonne lecture

    #qita #hexagora #ernautdejerusalem #ecriture #écrivain #écrivains #edition #édition #éditionnumérique #lecturenumerique #moyen-âge #policier #nouvelle #Lire #lecture #littérature #histoire #moyenorient #moyen-âge #policier #nouvelle #Lire #lecture #littérature #histoire #moyen-orient #nouvelles #CreativeCommons #CCbyncnd #CC #byncnd #histoiremédiévale

</markdown>

===== Backlinks =====
{{{{backlinks>.}}}}

{{{{tag> qit'a}}}}
