\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Besace et bourdon}{1}{chapter.0.1}%
\contentsline {section}{\numberline {}Route du Vif (Châteauneuf-Val-de-Bargis), après-midi du jeudi~28~juin~1156}{1}{section.0.1.1}%
\contentsline {section}{\numberline {}Hôpital de la Charité, soirée du jeudi~28~juin~1156}{6}{section.0.1.2}%
\contentsline {section}{\numberline {}Hôpital de la Charité, nuit du jeudi~28~juin~1156}{9}{section.0.1.3}%
\contentsline {section}{\numberline {}Notes}{12}{section.0.1.4}%
\contentsline {section}{\numberline {}Références}{13}{section.0.1.5}%
\contentsline {chapter}{\numberline {2}Faux espoirs}{15}{chapter.0.2}%
\contentsline {section}{\numberline {}Cap de Gata, sud d'Almería, vendredi~25~juillet~1147}{15}{section.0.2.1}%
\contentsline {section}{\numberline {}Almería, mardi 19 août 1147}{17}{section.0.2.2}%
\contentsline {section}{\numberline {}Almería, matin du vendredi~17~octobre~1147}{19}{section.0.2.3}%
\contentsline {section}{\numberline {}Almería, midi du vendredi~17~octobre~1147}{21}{section.0.2.4}%
\contentsline {section}{\numberline {}Notes}{23}{section.0.2.5}%
\contentsline {section}{\numberline {}Références}{23}{section.0.2.6}%
\contentsline {chapter}{\numberline {3}In pulverem reverteris}{25}{chapter.0.3}%
\contentsline {section}{\numberline {}Gênes, soirée du jeudi~4~septembre~1155}{25}{section.0.3.1}%
\contentsline {section}{\numberline {}Gênes, port, matin du mardi~28~février~1156}{30}{section.0.3.2}%
\contentsline {section}{\numberline {}Gênes, cathédrale San Laurenzo, matin du dimanche~3~juin~1156}{33}{section.0.3.3}%
\contentsline {section}{\numberline {}Gênes, maison d'Alda la Joliette, nuit du mardi~31~juillet~1156}{37}{section.0.3.4}%
\contentsline {section}{\numberline {}Gênes, port, matin du lundi~6~août~1156}{41}{section.0.3.5}%
\contentsline {section}{\numberline {}Notes}{43}{section.0.3.6}%
\contentsline {section}{\numberline {}Références}{43}{section.0.3.7}%
\contentsline {chapter}{\numberline {4}Tortueuse âme}{45}{chapter.0.4}%
\contentsline {section}{\numberline {}Gibelet, soirée du mercredi~26~mai~1154}{45}{section.0.4.1}%
\contentsline {section}{\numberline {}Gaza, nuit du lundi~7~juin~1154}{48}{section.0.4.2}%
\contentsline {section}{\numberline {}Gênes, matin du mercredi~2~novembre~1155}{52}{section.0.4.3}%
\contentsline {section}{\numberline {}Tripoli, matinée du lundi~24~décembre~1156}{55}{section.0.4.4}%
\contentsline {section}{\numberline {}Notes}{59}{section.0.4.5}%
\contentsline {section}{\numberline {}Références}{60}{section.0.4.6}%
\contentsline {chapter}{\numberline {5}Les croisés du Poron}{61}{chapter.0.5}%
\contentsline {section}{\numberline {}Lieu-dit du Poron, environs de Vézelay, après-midi du jeudi~6~juin~1146}{61}{section.0.5.1}%
\contentsline {section}{\numberline {}Abords du village de Saint-Pierre, matin du mardi~11~juin~1146}{65}{section.0.5.2}%
\contentsline {section}{\numberline {}Notes}{68}{section.0.5.3}%
\contentsline {section}{\numberline {}Références}{69}{section.0.5.4}%
\contentsline {chapter}{\numberline {6}Fils de moine}{71}{chapter.0.6}%
\contentsline {section}{\numberline {}Bords de Loire, matinée du mardi~9~mai~1133}{71}{section.0.6.1}%
\contentsline {section}{\numberline {}Prieurale Notre-Dame de la Charité-sur-Loire, midi du jeudi~11~mai~1133}{75}{section.0.6.2}%
\contentsline {section}{\numberline {}Cellier du prieuré Notre-Dame de la Charité-sur-Loire, après-midi du jeudi~11~mai~1133}{76}{section.0.6.3}%
\contentsline {section}{\numberline {}Notes}{79}{section.0.6.4}%
\contentsline {section}{\numberline {}Références}{80}{section.0.6.5}%
\contentsline {chapter}{\numberline {7}Traîtres sentiers}{81}{chapter.0.7}%
\contentsline {section}{\numberline {}Abords de Laodicée, plateaux anatoliens, nuit du samedi~3~janvier~1148}{81}{section.0.7.1}%
\contentsline {section}{\numberline {}Mont Cadmus, jeudi~8~janvier~1148}{84}{section.0.7.2}%
\contentsline {section}{\numberline {}Notes}{88}{section.0.7.3}%
\contentsline {section}{\numberline {}Références}{89}{section.0.7.4}%
\contentsline {chapter}{\numberline {8}Le meilleur des fils}{91}{chapter.0.8}%
\contentsline {section}{\numberline {}Césarée, mercredi~3~juillet~1140, après-midi}{91}{section.0.8.1}%
\contentsline {section}{\numberline {}Ascalon, jeudi~10~septembre~1153, matin}{95}{section.0.8.2}%
\contentsline {section}{\numberline {}Notes}{99}{section.0.8.3}%
\contentsline {section}{\numberline {}Références}{100}{section.0.8.4}%
\contentsline {chapter}{\numberline {9}Troubles}{101}{chapter.0.9}%
\contentsline {section}{\numberline {}Palais royal, Jérusalem, matinée du vendredi~16~avril~1154}{101}{section.0.9.1}%
\contentsline {section}{\numberline {}Parvis du Saint-Sépulcre, Jérusalem, midi du vendredi~16~avril~1154}{106}{section.0.9.2}%
\contentsline {section}{\numberline {}Notes}{110}{section.0.9.3}%
\contentsline {section}{\numberline {}Références}{111}{section.0.9.4}%
\contentsline {chapter}{\numberline {10}Amis pour la vie}{113}{chapter.0.10}%
\contentsline {section}{\numberline {}Abords du Bourg-l'Évêque, Bayeux, mercredi~3~novembre~1148}{113}{section.0.10.1}%
\contentsline {section}{\numberline {}Bayeux, vendredi 5~novembre~1148}{116}{section.0.10.2}%
\contentsline {section}{\numberline {}Bayeux, tôt le matin du jeudi~1\textsuperscript {er}~mai~1152}{120}{section.0.10.3}%
\contentsline {section}{\numberline {}Vézelay, après-midi du mardi~29~mai~1156}{123}{section.0.10.4}%
\contentsline {section}{\numberline {}Notes}{125}{section.0.10.5}%
\contentsline {section}{\numberline {}Références}{125}{section.0.10.6}%
\contentsline {chapter}{\numberline {11}Délivrance}{127}{chapter.0.11}%
\contentsline {section}{\numberline {}Large des côtes de Terre sainte, matin du dimanche~16~octobre~1155}{127}{section.0.11.1}%
\contentsline {section}{\numberline {}À bord de la galée égyptienne, fin de matinée}{129}{section.0.11.2}%
\contentsline {section}{\numberline {}Cale du Peregrina, vers midi}{131}{section.0.11.3}%
\contentsline {section}{\numberline {}Pont du Peregrina, midi}{133}{section.0.11.4}%
\contentsline {section}{\numberline {}Notes}{136}{section.0.11.5}%
\contentsline {section}{\numberline {}Références}{137}{section.0.11.6}%
\contentsline {chapter}{\numberline {12}Là-bas}{139}{chapter.0.12}%
\contentsline {section}{\numberline {}Le Monteil, mercredi~4~novembre~1142}{139}{section.0.12.1}%
\contentsline {section}{\numberline {}Église de Dienne, dimanche~2~juillet~1144}{143}{section.0.12.2}%
\contentsline {section}{\numberline {}Le Monteil, jeudi~19~décembre~1146}{148}{section.0.12.3}%
\contentsline {section}{\numberline {}Notes}{152}{section.0.12.4}%
\contentsline {section}{\numberline {}Références}{152}{section.0.12.5}%
\contentsline {chapter}{\numberline {13}Cœur vaillant}{153}{chapter.0.13}%
\contentsline {section}{\numberline {}Bords de la Marne, environs de Meaux, mardi~17~avril~1151}{153}{section.0.13.1}%
\contentsline {section}{\numberline {}Meaux, jeudi~16~juillet~1153}{158}{section.0.13.2}%
\contentsline {section}{\numberline {}Route de Jaffa à Jérusalem, mardi~21~août~1156}{161}{section.0.13.3}%
\contentsline {section}{\numberline {}Notes}{165}{section.0.13.4}%
\contentsline {section}{\numberline {}Références}{166}{section.0.13.5}%
\contentsline {chapter}{\numberline {14}Charité bien ordonnée}{167}{chapter.0.14}%
\contentsline {section}{\numberline {}Saint-Jean d'Acre, taverne d'Ayoul, mercredi~28~mai~1152}{167}{section.0.14.1}%
\contentsline {section}{\numberline {}Jérusalem, Grand Hôpital de Saint-Jean, vendredi~30~octobre~1153}{170}{section.0.14.2}%
\contentsline {section}{\numberline {}Notes}{174}{section.0.14.3}%
\contentsline {section}{\numberline {}Références}{175}{section.0.14.4}%
\contentsline {chapter}{\numberline {15}Épineuse couronne}{177}{chapter.0.15}%
\contentsline {section}{\numberline {}Abords du Saint-Sépulcre, Jérusalem, matin du lundi~31~mars~1152}{177}{section.0.15.1}%
\contentsline {section}{\numberline {}Maison de Pierre Salomon, Jérusalem, soir du mardi~8~avril~1152}{180}{section.0.15.2}%
\contentsline {section}{\numberline {}Abords de la tour de David, Jérusalem, fin de matinée du lundi~14~avril~1152}{183}{section.0.15.3}%
\contentsline {section}{\numberline {}Notes}{185}{section.0.15.4}%
\contentsline {section}{\numberline {}Références}{186}{section.0.15.5}%
\contentsline {chapter}{\numberline {16}Léviathan}{189}{chapter.0.16}%
\contentsline {section}{\numberline {}Port de Jaffa, matin du mercredi~27~novembre~1157}{189}{section.0.16.1}%
\contentsline {section}{\numberline {}Abords de la chapelle Saint-Nicolas, soir du mercredi~27~novembre~1157}{193}{section.0.16.2}%
\contentsline {section}{\numberline {}Plage de Jaffa, nuit du mercredi 27 novembre}{195}{section.0.16.3}%
\contentsline {section}{\numberline {}Notes}{199}{section.0.16.4}%
\contentsline {section}{\numberline {}Références}{200}{section.0.16.5}%
\contentsline {chapter}{\numberline {17}L'ennemi intime}{201}{chapter.0.17}%
\contentsline {section}{\numberline {}Abords du village de Rabwé, environ de Damas, matin du lundi~26~juillet~1148}{201}{section.0.17.1}%
\contentsline {section}{\numberline {}Village de Rabwé, soir du lundi~26~juillet}{204}{section.0.17.2}%
\contentsline {section}{\numberline {}Oasis sud-ouest de Damas, matin du 29~juillet}{208}{section.0.17.3}%
\contentsline {section}{\numberline {}Notes}{209}{section.0.17.4}%
\contentsline {section}{\numberline {}Références}{210}{section.0.17.5}%
\contentsline {chapter}{\numberline {18}Malquisinat}{211}{chapter.0.18}%
\contentsline {section}{\numberline {}Quartier du palais royal, Jérusalem, fin de matinée du lundi~3~mars~1158}{211}{section.0.18.1}%
\contentsline {section}{\numberline {}Malquisinat, Jérusalem, matin du mardi~4~mars~1158}{215}{section.0.18.2}%
\contentsline {section}{\numberline {}Rue de David, près du change, début de soirée du jeudi~3~avril~1158}{218}{section.0.18.3}%
\contentsline {section}{\numberline {}Église Saint-Sauveur, Jardins de Gethsémani, après-midi du lundi~7~avril~1158}{221}{section.0.18.4}%
\contentsline {section}{\numberline {}Notes}{223}{section.0.18.5}%
\contentsline {section}{\numberline {}Références}{225}{section.0.18.6}%
\contentsline {chapter}{\numberline {19}Mêh}{227}{chapter.0.19}%
\contentsline {section}{\numberline {}Plateau du Golan, fin d'après-midi du mardi~12~février~1157}{227}{section.0.19.1}%
\contentsline {section}{\numberline {}Chateau Emmaüs, matinée du jeudi~16~mai~1157}{231}{section.0.19.2}%
\contentsline {section}{\numberline {}Jérusalem, entrepôts des hospitaliers de Saint-Jean, après-midi du jeudi~20~juin~1157}{233}{section.0.19.3}%
\contentsline {section}{\numberline {}Plaine d'Esdrelon, midi du mardi~15~octobre~1157}{236}{section.0.19.4}%
\contentsline {section}{\numberline {}Notes}{239}{section.0.19.5}%
\contentsline {section}{\numberline {}Références}{239}{section.0.19.6}%
\contentsline {chapter}{\numberline {20}Chétif destin}{241}{chapter.0.20}%
\contentsline {section}{\numberline {}Meleha, abords du lac de Houla, soirée du mardi~18~juin~1157}{241}{section.0.20.1}%
\contentsline {section}{\numberline {}Abords du Jourdain, soir du mercredi~19~juin~1157}{244}{section.0.20.2}%
\contentsline {section}{\numberline {}Damas, matin du lundi~24~juin~1157}{246}{section.0.20.3}%
\contentsline {section}{\numberline {}Notes}{248}{section.0.20.4}%
\contentsline {section}{\numberline {}Références}{249}{section.0.20.5}%
\contentsline {chapter}{\numberline {21}Branleterre}{251}{chapter.0.21}%
\contentsline {section}{\numberline {}Damas, sud de la Ghûta, fin de matinée du lundi~12~août~1157}{251}{section.0.21.1}%
\contentsline {section}{\numberline {}Damas, soirée du youm al~joumouia, 12~ramadan~552}{254}{section.0.21.2}%
\contentsline {section}{\numberline {}Shayzar, soir du vendredi~22~novembre~1157}{257}{section.0.21.3}%
\contentsline {section}{\numberline {}Notes}{259}{section.0.21.4}%
\contentsline {section}{\numberline {}Références}{260}{section.0.21.5}%
\contentsline {chapter}{\numberline {22}In vino veritas}{261}{chapter.0.22}%
\contentsline {section}{\numberline {}Terre sainte, abords de Naplouse, après-midi du mercredi~22~février~1156}{261}{section.0.22.1}%
\contentsline {section}{\numberline {}Naplouse, soirée du mercredi~22~février~1156}{263}{section.0.22.2}%
\contentsline {section}{\numberline {}Naplouse, après-midi du samedi~25~février~1156}{264}{section.0.22.3}%
\contentsline {section}{\numberline {}Naplouse, soirée du samedi~25~février~1156}{266}{section.0.22.4}%
\contentsline {section}{\numberline {}Notes}{268}{section.0.22.5}%
\contentsline {section}{\numberline {}Références}{268}{section.0.22.6}%
\contentsline {chapter}{\numberline {23}Graines d'espoir}{269}{chapter.0.23}%
\contentsline {section}{\numberline {}Jérusalem, vendredi~26~avril~1157}{269}{section.0.23.1}%
\contentsline {section}{\numberline {}Jérusalem, matin du mardi~7~mai~1157}{271}{section.0.23.2}%
\contentsline {section}{\numberline {}Casal de la Mahomerie, midi du mercredi~29~mai~1157}{274}{section.0.23.3}%
\contentsline {section}{\numberline {}Notes}{276}{section.0.23.4}%
\contentsline {section}{\numberline {}Références}{276}{section.0.23.5}%
\contentsline {chapter}{\numberline {24}La reine du nord}{277}{chapter.0.24}%
\contentsline {section}{\numberline {}Jérusalem, matin du mardi~3~septembre~1157}{277}{section.0.24.1}%
\contentsline {section}{\numberline {}Naplouse, fin de journée du mardi~3~septembre~1157}{280}{section.0.24.2}%
\contentsline {section}{\numberline {}Naplouse, matin du vendredi~6~septembre~1157}{283}{section.0.24.3}%
\contentsline {section}{\numberline {}Notes}{286}{section.0.24.4}%
\contentsline {section}{\numberline {}Références}{287}{section.0.24.5}%
\contentsline {chapter}{\numberline {25}Addenda}{289}{chapter.0.25}%
\contentsline {chapter}{\numberline {26}Un univers pour protagoniste~: Qit'a}{291}{chapter.0.26}%
\contentsline {section}{\numberline {}Des textes courts et fréquents}{292}{section.0.26.1}%
\contentsline {section}{\numberline {}De la multiplicité des approches scripturales}{293}{section.0.26.2}%
\contentsline {section}{\numberline {}De la rigueur historique pour alimenter la cohérence}{294}{section.0.26.3}%
\contentsline {section}{\numberline {}Une licence libre pour reverser au pot commun}{297}{section.0.26.4}%
\contentsfinish 
