# Instructions de compilation pour epub et pdf A5 pour impression

## Création des fichiers epub

Dans le répertoire epub/destination/stockepub se trouve l'arborescence du contenu de l'epub. On peut y modifier les fichiers finaux puis recréer un nouveau ernaut_04.epub manuellement : se placer dans le répertoire epub/destination/stockepub, y effacer l'actuel fichier ernaut_04.epub puis taper la commande :

    $ zip -X0 ernaut_04.epub mimetype

afin de recréer un fichier avec juste le mimetype en premier, non compressé. Ensuite, on y ajoute tous les autres fichiers, avec leur hiérarchie :

    $ zip -X9Dr ernaut04.epub META-INF OEBPS

## Mise en forme automatisée de l'epub avec les scripts dude

Se placer dans le répertoire et taper la commande :

    pandoc ernaut_04_part00_garde_epub.md ernaut_04_part01_introduction_epub.md ernaut_04_part02_le_souffle_du_dragon.md ernaut_04_part03_epilogue_epub.md ernaut_04_part04_annexes.md ../../notes/notes.md --wrap=none -o epub/ernaut_04_epub.md

On obtient ainsi un document markdown unifié qui comprend les appels de notes et les bons éléments adaptés au fichier epub.

Ensuite, aller dans le répertoire epub

    cd epub/

Entrer la commande dudeit adaptée (vérifier qu'on a bien installé le script python dans un répertoire d'où il peut être appelé) :

    dudeit -e ernaut_04_epub.md

On obtient alors les différents formats epub dans le répertoire epub/destination/stockepub. Les autres répertoires ne sont utilisés que pour la génération de fichiers dokuwiki pour inclusion dans le site.

Se placer dans le répertoire et taper la commande :

    pandoc ernaut_04_part00_garde_pdf.md ernaut_04_part01_introduction_pdf.md ernaut_04_part02_le_souffle_du_dragon.md ernaut_04_part03_epilogue_pdf.md ernaut_04_part04_annexes.md ../../notes/notes.md --wrap=none -o TeX/ernaut04_impress.tex
