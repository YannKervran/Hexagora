---
date : 2017-10-15
abstract : 1158-1159. Désireux de sortir de la disgrâce dans laquelle son erreur l’a plongé, Bernard Vacher remue ciel et terre pour dévoiler le complot dont il fut victime. Parcourant le royaume, et allant jusqu’à Damas pour cela, il n’hésite pas à prendre tous les risques, soucieux de laver son nom.
characters : Bernard Vacher, Germain, Pietro Gradenigo, Abu Malik al-Muhallab, Yalim al-Buzzjani, Nağm ad-Dīn Ayyūb
---

# Impitoyable

## Jérusalem, cuisines du palais royal, matin du lundi 27 octobre 1158

Comme dans toutes les maisons nobles, les cuisines bruissaient d’agitation dès les précoces lueurs de l’aube. Premiers levés, les mitrons avaient allumé les feux, les queux avaient pétri la farine et l’eau du pain quotidien, réveillant la domesticité avec une bonne odeur de cuisson. Une fois les portes de la ville ouvertes, au son des cloches monastiques, on voyait arriver les produits frais, légumes et bêtes, pour compléter les vastes réserves des celliers. Pléthore de gamins et de jeunes grouillots s’employaient à toutes les tâches annexes : mouture du grain, épluchage, plumage, apport de bois, nettoyage de la vaisselle, transport de baquets et cruches… Autour d’eux s’agitait une ribambelle de cuisiniers qui avaient pour dur labeur de nourrir l’hôtel du roi, ses proches, ses serviteurs et toutes les personnes qui se présenteraient à table ce jour.

Bernard Vacher aimait à se caler dans un recoin, goûtant la mie souple encore tiède du four, trempée d’huile savoureuse. Il y ajoutait volontiers de petits oignons au vinaigre, douceur que lui concédait le maître queux qu’il connaissait depuis de longues années. On ne prêtait plus attention à l’imposante silhouette, souvent en armes, qui se coinçait contre le piédroit d’une des vastes cheminées, sur un escabeau comme le plus simple des commis. Pour être vaillant et redouté, parfois autoritaire et exigeant, Bernard Vacher n’avait pas la superbe des orgueilleux.

Maussade, il ressassait toujours sa disgrâce. L’absence du roi lui permettait de se sentir moins à l’écart, mais il n’avait pas encore réussi à revenir dans ses faveurs avant le départ de Baudoin pour le Nord et la rencontre avec la puissance byzantine. D’une certaine façon, il préférait ne pas avoir à côtoyer le basileus et ses diplomates, trop tortueux et ambigus selon lui. Il avait donc quelques mois devant lui pour trouver une solution.

Parmi la valetaille qui s’agitait, un petit homme replet se figea un instant devant lui, saluant de façon appuyée. Bernard sourit en reconnaissant le visage. Germain, un des domestiques de l’archevêque de Tyr, aimait à traîner ici, où les mets étaient plus savoureux que dans le palais du patriarche voisin. Le nouvel occupant, Amaury de Nesle, n’était pas adepte du faste et se contentait d’un service assez sobre. Les deux édifices s’imbriquant l’un dans l’autre, il était facile au jeune valet de venir baguenauder en de plus exubérantes cuisines.

Une idée se fit peu à peu jour dans l’esprit de Bernard Vacher. Il fit signe à Germain de s’approcher.

« Dis-moi, mon garçon, tu connais bien la ville de l’archevêque ?

— De certes, sire Vacher. J’y ai grandi, aucune ruelle ne m’en est inconnue ! se vanta Germain.

— Que peux-tu me dire du quartier des Vénitiens ?

— Ils sont encore plus secrets que des Juifs ! Ils verrouillent la zone comme s’ils y serraient monceaux d’or ! Et ils parlent leur langue, entre eux. »

Bernard Vacher se frotta le menton, cherchant une façon de ne pas trop en dévoiler.

« Quand fais-tu chemin de retour à Tyr ? J’aurais peut-être besoin d’aller y quérir besants sarracinois…

— Le père Herbelot semble avoir grande hâte de rentrer… Certainement demain. »

Germain se frotta les fesses, en rappel des douloureux souvenirs que lui inspiraient les voyages.

« Je ferai peut-être voyage avec vous, en ce cas. Cela semble toujours plus court, en agréable compagnie.

— Le père Gonteux sera enchanté de vous savoir à siens côtés ! Avez-vous mission près l’archevêque ?

— Nullement. Le roi m’a donné congé le temps qu’il était au nord. Il est de peu de risque que l’émir nous assaille, avec toutes nos armées près de sa bonne ville d’Alep. Il me faut juste aller voir les Vénitiens. »

Le jeune homme tira un long nez, se rapprocha comme pour délivrer une importante confidence.

« Je ne saurais trop vous conseiller de ne pas accorder fiance à ces marchands. Ils ont un abaque en guise de cœur.

— Aurais-tu quelques griefs à leur encontre ?

— Pas spécialement, reconnut de mauvaise grâce Germain, mais il est de bonne fame qu’il est sage de recompter ses doigts après leur avoir serré la main. On dit qu’un Vénitien vaut deux Arméniens…

— Qui eux-mêmes valent chacun deux juifs ! sourit Bernard Vacher. Je te mercie de ton conseil. Je ferai attention à cela. Ne connais-tu pas personne parmi eux ?

— Le vieux Maursin[^maursin], leur curé et notaire, est plutôt amiable. Il nous donnait parfois friandises lors des fêtes. »

Le vieux chevalier hocha la tête. Au moins avait-il une entrée pour aller mener l’enquête sur place. Il n’était pas tant familier de cette cité royale portuaire.

## Tyr, cathédrale Sainte-Croix, après-midi du mardi 4 novembre 1158

Une violente averse frappait le parvis de la cathédrale lorsque Bernard Vacher et son compagnon y parvinrent enfin. À l’abri des hautes voussures, ils s’ébrouèrent puis pénétrèrent dans le lieu. Une forte odeur d’encens les prit à la gorge et il leur fallut un peu de temps pour s’accoutumer à l’obscurité. Les lumières s’accrochaient surtout sur les décors du chœur, à l’autre extrémité de la longue basilique. De plus, la majeure partie des personnes qui cherchaient avant tout à fuir la pluie s’étaient installées vers l’entrée, ajoutant des ombres au brouhaha ambiant.

Bernard Vacher invita Pietro Gradenigo à s’asseoir au pied d’un des vastes massifs de piliers soutenant les voûtes, au nord, un peu à l’écart de la porte ouvrant sur le cloître afin de ne pas être dérangé. Il lui avait fallu un peu de temps pour dénicher le jeune homme et encore un peu plus pour le convaincre de parler. Pietro n’était pas originaire de Jérusalem et se réfugiait volontiers derrière son statut de Vénitien. Il n’avait pas souhaité recevoir le chevalier chez lui, dans le quartier où il serait sous la juridiction vénitienne, mais avait choisi un lieu de culte où, il le savait, le pouvoir royal ne pouvait pas s’exercer non plus. Bernard Vacher espérait donc d’importantes révélations.

Il avait fouillé autant qu’il l’avait pu dans la ville, sans trop se dévoiler. Il lui avait fallu peu de temps pour comprendre que le privilège de frappe de monnaies d’or concédé par les rois de Jérusalem aux Vénitiens constituait l’enjeu d’une âpre bataille entre les familles ici et à Venise. Et pour l’heure, il semblait bien que les Gradenigo, dont l’influence et le pouvoir étaient fameux, ne se satisfaisaient pas de leur position subalterne.

Après quelques préalables polis, ils en vinrent assez rapidement au fait, le visage de Pietro alors barré d’une moue condescendante.

« C’est la famille Maureceno qui a la haute main sur l’atelier de frappe. Ils ne laissent guère de place aux autres.

— J’aurais surtout besoin de savoir qui conserve les coins[^coin], qui y a accès… »

Le jeune Vénitien lui adressa un regard aigu, esquissa un sourire, mais s’abstint de tout commentaire.

« C’est désormais Giovanni qui en a la charge, habituellement. Comme son oncle avant lui.

— Le connaissez-vous bien ? Est-il homme de bonne fame ?

— Il n’est ici que depuis peu, il a clamé son héritage voilà quelques mois, à peine débarqué du bateau.

— Et son oncle ? Comment était-il ? »

Pietro s’esclaffa.

« On le surnommait Malvicino[^malvicino], pensez-en ce que vous voulez ! Il doit enseigner aux démons comment se bien comporter aux Enfers, désormais.

— Quand est-il mort ?

— C’était peu après les Pâques de l’an passé^[Soit vers avril 1157.]. Il est tombé sous les coups de brigands sur la route depuis Panéas[^paneas] et a rendu l’âme sans que les mires[^mire] n’y puissent rien faire. »

La date fit grimacer Bernard. Une telle coïncidence de calendrier, sur une voie qui menait à la région damascène l’incitait à penser que ce n’étaient pas là de simples détrousseurs. Le Vénitien avait été réduit au silence. S’était-il montré trop gourmand ou trop curieux ? Bernard estima nécessaire d’en apprendre plus sur ce Giovanni et découvrir quels pouvaient être les contacts de son oncle, sans pour autant attirer l’attention. Le plus efficace serait peut-être de confier ces recherches à la Chaîne[^chaine] de la cité. Ils pourraient prétexter des contrôles fiscaux sur la succession et les frais douaniers afférents pour se faire transmettre le maximum d’archives. Il savait que, faisant cela il usurpait quelque peu l’autorité royale dont il était investi, mais après tout ne faisait-il pas tout cela également pour Baudoin ?

## Jérusalem, quartier de la demeure d’Abu Malik, début de soirée du mardi 18 novembre 1158

Le tintamarre que faisait le chevalier en grande tenue sur son destrier fit fuir les enfants qui étaient encore attroupés près de la fontaine. D’un coup de rein, Bernard Vacher stoppa sa monture et en descendit avec aisance, fruit de dizaines d’années de pratique. Il était pourtant vermoulu et prit quelques instants pour s’étirer le dos, engoncé dans la cotte de mailles. Il n’avait pas lacé sa coiffe ni son heaume, mais tenait à voyager en armure, privilège de son rang. Il réinstalla correctement son fourreau sur la cuisse, cherchant du regard un des moins farouches parmi les gamins. Il s’en trouvait toujours un plus vaillant, heureux de rendre service en menant boire un cheval de prix.

Sa monture en de bonnes mains, Bernard Vacher se dirigea vers une des demeures et frappa à l’huis. Il se fit connaître à travers le guichet par le jeune valet d’Abu Malik et pénétra rapidement dans la salle principale où ne trônait qu’une petite lampe hâtivement placée par le domestique. Abu Malik était apparemment en train de manger, des odeurs de cuisine s’attachant encore à lui lorsqu’il entra à son tour dans la pièce. Il écarquilla les yeux en voyant un homme équipé de pied en cap dans son salon. Le chevalier lui adressa un sourire qu’il espérait engageant.

« Je suis désolé de venir ainsi sans m’annoncer, mais j’ai des nouvelles urgentes et voulais avoir votre avis sur la situation. »

Abu Malik ne fit pas de commentaires et invita Bernard à s’asseoir. Il donna quelques instructions pour qu’on leur porte de quoi se désaltérer et quelques friandises à grignoter. Puis il alluma d’autres lampes et, enfin, prit place sur un des coussins. Tout en s’affalant, Bernard Vacher lui expliqua qu’il avait enquêté à Tyr et en avait tiré quelques renseignements.

« Ce Malvicino a pu faire réaliser assez aisément de fausses monnaies, vu son libre accès à l’atelier.

— C’est là bien grand risque !

— Ce qui me conforte en cette idée, c’est qu’il avait de nombreux échanges avec un négociant damascène. Ils étaient en contact depuis des années et ont investi de belles sommes en commun. Je n’ai pu voir aucune lettre personnelle, mais il existait un lien très fort. J’aurais juste besoin de savoir si vous auriez connoissance de ce marchand : Sa’ad ad-Dīn ’Utmān. »

Le visage d’Abu Malik s’allongea instantanément. C’était une figure bien connue dans sa ville. Un homme très riche, qui s’était toujours tenu à l’écart de la politique. Il avait de nombreux amis parmi les fonctionnaires, sans pour autant privilégier jamais aucun parti. On le disait mécène de mosquées chiites et sunnites, et même d’églises chrétiennes ou de synagogues. Un opportuniste assez flamboyant.

« Qui ne le connaît pas dans le monde des affaires ? Il est partout. C’est peut-être d’ailleurs pour cela qu’il est mentionné dans les comptes de ce Tyrien.

— C’est un Vénitien, pas un sujet du roi de Jérusalem.

— Cela me semble maigre comme lien. Je pourrais vous citer des quantités d’hommes faisant affaire avec Sa’ad ad-Dīn ’Utmān. Et je pourrais me porter garant de la majorité d’entre eux. »

Bernard Vacher grimaça. Il était conscient de ne pas avoir grand-chose, mais le faisceau d’indices lui paraissait suffisant pour aller creuser dans la cité syrienne. Il avait besoin de voir s’il existait un lien entre Sa’ad ad-Dīn ’Utmān et un quelconque responsable du versement du tribut.

« Le souci, c’est que vous trouverez forcément des échanges, des repas et des cérémonies où il aura côtoyé les hommes désormais en place. Je ne serais même pas surpris qu’il ait personnellement participé à la collecte du tribut. Lui ou un des hommes à son service, du moins.

— Serait-il homme prêt à tout pour ravir pareille fortune ?

— On le dit immensément riche. Donc si votre question est : en a-t-il besoin ? La réponse est alors non, sans nul doute. Mais si vous demandez s’il n’aurait pas désir de s’emparer d’un tel butin, alors je vous dirais que les hommes comme lui pensent en terme de négoce, d’apport à risquer et de bénéfice escompté. Rien de plus. Même leurs dons à la communauté, pour les pauvres, les captifs, sont pesés en ces termes. Ils paient pour ne pas être importunés dans la rue, pour être célébrés comme de grands hommes par les humbles qu’ils méprisent. »

Le chevalier sentait la rancœur qui suintait des propos. Abu Malik n’aimait guère ce genre de personnes, c’était flagrant. Il était pourtant lui aussi homme de commerce, vivait parmi eux. Lui faisaient-ils effet de miroir répulsif, salutaire en ce qui le concernait, vu qu’il s’efforçait au bien selon ses propres critères ?

« Je vais tout de même aller jusqu’à Damas. Nous ne pouvons laisser ces serpents s’en tirer si aisément !

— Soyez bien prudent. Depuis ma fuite, je n’y suis repassé qu’une fois, voilà quelques mois, et la situation est encore bien malaisée. Beaucoup de gens observent, attendent. Entre la maladie de l’émir, les tremblements de terre dans les territoires au nord, la ville semble prête à basculer à tout moment. Jamais je n’avais éprouvé pareil sentiment…

— Les choses changent très vite en ce moment, de certes. Nos pères s’enhonteraient de voir comment nous soignons bien mal notre héritage. »

Abu Malik hocha le menton en silence. Il avait vu au fil des ans la situation se détériorer pour la cité damascène. De la capitale, demeure du calife qu’elle était dans l’ancien temps, elle n’était plus qu’une ville satellite du pouvoir turc, appétissant fruit que cherchaient à gober tous les puissants environnants.

« Si vous tenez à retourner là-bas, faites donc part de vos idées à mon ami al Zarqa’ ou, à défaut à Yalim al-Buzzjani. Il vous faudra une protection si vous avez intention de mettre en cause un homme tel que Sa’ad ad-Dīn ’Utmān. »

Alors que Bernard Vacher se levait pour prendre congé, Abu Malik le retint d’un geste. Le Damascène le fixait avec intensité, à la recherche d’un message amical, d’une parole de réconfort. Les seuls mots hésitants qui franchirent ses lèvres n’étaient pas de lui.

« *Qui allume le feu doit savoir qu’il brûle*. »

## Damas, citadelle, début d’après-midi du youm al arbia 15 dhu al-hijjah 553^[Mercredi 7 janvier 1159.]

Bernard Vacher retrouvait les salles d’apparat dont il avait été familier du temps des bourides[^bourides]. Nostalgique, il se remémorait les âpres négociations qu’il avait dû mener là, face au talentueux Anur[^anur]. Avec le temps, il en venait presque à regretter l’astucieux dirigeant damascène, malgré ses machiavéliques procédés. Voire, il en privilégiait le souvenir malicieux, les façons courtoises et les apparences amicales. Il en oubliait les habitudes de faux-semblants et les manipulations éhontées.

Ils ne s’arrêtèrent pas dans la grande salle d’audience, glaciale en cette période hivernale, pour se rendre dans un des cabinets attenants. Là, dans une douillette atmosphère alourdie d’encens boisé, était confortablement installé Nağm ad-Dīn Ayyūb. Il était en train de lire un imposant ouvrage posé sur un lutrin devant lui lorsque Yalim al-Buzzjani fut introduit, avec le chevalier franc à sa suite. Aucune parole ne fut échangée le temps pour chacun de prendre sa place. Les deux arrivants se présentaient debout, à quelque distance. Entre eux et le seigneur de la ville se tenaient deux hommes en armes, sabre et lance, casque en tête. Leurs cagoules de mailles ne laissaient voir que leurs yeux attentifs. Bernard Vacher savait qu’il serait découpé en tranche avant de pouvoir approcher de quelques pas.

Il redoutait cette entrevue à laquelle il ne s’était résolu que la mort dans l’âme. Avec l’absence d’al-Zarqa’, Yalim al-Buzzjani était le seul contact qu’il avait pu joindre et celui-ci ne s’était guère montré coopératif. L’officier tenait à dévoiler l’affaire aux dirigeants de la cité, qu’il estimait en droit de régler le problème par eux-mêmes. La veille au soir, lors d’une violence dispute, Bernard Vacher avait fini par se rendre à ses arguments. Fût-il un grand chevalier et de bonne réputation, ce n’était pas à lui de se faire justice dans un territoire d’un prince étranger. Quand bien même il souhaitait que cela se fasse en toute discrétion, il ne pouvait faire l’économie de s’adresser à celui dont la main pouvait s’abattre de façon légitime.

La barbe grise, Nağm ad-Dīn Ayyūb était vêtu dans une tenue civile. Bernard Vacher le savait pourtant guerrier redouté et dirigeant efficace. Il avait réussi à s’infiltrer dans la région de Damas avec talent, préparant le terrain pour son maître Zengī[^zengi] puis, avec l’aide de son frère Shīrkūh, pour Nūr ad-Dīn[^nuraldin]. Ses traits émaciés caractérisaient un homme d’action plus que d’ascèse, bien qu’on le prétendît pieux. Mais les laudateurs avaient beau jeu de toujours louer la piété des puissants, dans l’espoir de les voir lâcher quelques subsides grâce à la charité qu’ils célébraient. Sa tenue, une magnifique robe d’honneur, était d’une splendide couleur orangée, la soie se parant de mille reflets. Sur sa tête, un imposant turban était orné d’une broche d’argent sertie de pierres.

Ce fut al-Buzzjani qui résuma l’affaire, expliquant ce qui était arrivé au tribut et les recherches de Bernard Vacher, d’une façon qui suggérait que tout cela était entièrement nouveau pour le gouverneur. Le chevalier n’en était pas dupe un seul instant, l’entrevue ayant certainement été préparée avec grand soin. Il était familier de ces circonvolutions diplomatiques et les subit avec patience, admirant les décors des tapis, des pièces d’orfèvrerie et des céramiques qui parsemaient la salle.

Il nota néanmoins que l’officier ne fit aucune mention des intermédiaires qui les avaient mis en contact. En cachant ses sources, le soldat protégeait ceux qui lui avaient fait confiance. Ou se ménageait un levier d’action dans le cas où il aurait à se défendre contre des agissements dangereux pour lui. Bernard Vacher espérait que l’émir était plutôt enclin à fonctionner selon la première hypothèse.

La voix grave et légèrement nasale du gouverneur résonna après un long moment de silence.

« Ton nom et ta réputation t’ont précédé ici, al-Baqara. J’entends donc ce que tu me révèles avec la plus grande attention. »

Il se lança ensuite dans une diatribe contre les félons qui avaient mis pareillement en péril la situation entre leurs territoires. Il ne pouvait préjuger de ce que l’émir déciderait, mais des actions seraient prises rapidement pour tirer tout cela au clair. Il ne posa pas de questions, mais tint à assurer le chevalier que les coupables seraient dénichés et punis. Lorsque son tour vint de parler, Bernard Vacher avait une demande qui lui brûlait les lèvres.

« Établir de nouvel bonnes relations entre nos princes me semble chose importante. N’hésitez pas à mander un de vos émirs porteur de la bonne nouvelle d’une plaisante conclusion auprès de mon roi.

— Tu pourras déjà l’assurer de ma plus parfaite collaboration en cette affaire. Pour le reste, ce sera à l’émir de dire ses volontés. »

Après quelques échanges formels, le chevalier fut invité à se retirer. Il devait s’en remettre à la puissance du gouverneur pour la suite de l’affaire. Alors que Yalim s’apprêtait à sortir à son tour, il fut retenu par le prince d’un simple mouvement de menton. Ce fut donc un des gardes qui mena Bernard Vacher au-dehors.

L’émir attendait, un peu fébrile. Il n’aimait guère la tension qu’il lisait sur le visage de Nağm ad-Dīn. Le gouverneur croqua une douceur à la pistache, élaborant une stratégie maintenant qu’il avait tous les éléments en main et qu’il s’était fait une idée sur les différents protagonistes de l’affaire. Il prit la parole comme à dépit.

« Cet homme n’est pas envoyé par son roi. Il cherche justice pour lui, rien de plus. »

Il se rinça la bouche d’une boisson liquoreuse aux fruits puis d’un geste, indiqua à son émir de s’approcher.

« J’ai entendu le désir que tout cela demeure secret. C’est bien volontiers que je me rangerai à cette idée. Veille à cela, al-Buzzjani. »

Appuyant son ordre d’un regard lourd de sens, il congédia le soldat d’un geste des doigts avant de reprendre une pâtisserie.

Lorsqu’il franchit le seuil de la salle, Yalim contracta les poings, serra la mâchoire. Que de dédain dans cette condamnation à mort lancée entre deux bouchées ! ❧

## Notes

La conclusion de cette mini-trilogie permettra à chacun, je l’espère, de mieux appréhender la mosaïque des pouvoirs alors qu’Ernaut débarque en Terre sainte et y prend ses marques. J’ai essayé de donner une image riche et la plus fidèle possible des puissants en place et des rapports de force entre eux, à travers une anecdote. Ce sont eux qui vont canaliser les grands mouvements des décennies suivantes.

J’ai aussi également tenté de regrouper certains faisceaux narratifs éparpillés au gré des publications, de montrer des liens entre certains personnages instillés au fil des Qit’a. La dispersion des récits n’est qu’apparente et chacun d’eux peut, à un moment donner, faire basculer le destin d’un autre de façon tragique. Je sème assez souvent des indices très légers, je tisse des relations distendues, parfois sans les nommer explicitement, de façon à laisser le lecteur libre de renouer les fils selon sa fantaisie. J’espère avoir l’occasion à l’avenir de développer encore plus ces trames secondaires. Ou peut-être qu’un auteur aura l’envie de le faire, la possibilité en est ouverte avec la licence que j’utilise pour publier mes travaux.

L’idée de cette mini-série de Qit’a est née d’un simple jeu de mots. J’avais le désir de raconter ce qui aurait pu arriver à Bernard Vacher. Son nom me souffla que c’était là une histoire de vacher, c’est-à-dire de *cowboy*. Je tenais une piste pour les titres et les ambiances, grand amateur de Sergio Leone et des westerns crépusculaires de Clint Eastwood que je suis.

## Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tomes I, II et III, Damas : Institut Français de Damas, 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I, *Economic Foundations*, Berkeley et Los Angeles : University of California Press, 1999.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. IV. The Cities of Acre and Tyre*, Cambridge : Cambridge University Press, 2009.

Schlumberger Gustave-Léon, *Numismatique de l’Orient Latin*, paris : Ernest Leroux, 1878-1882.
