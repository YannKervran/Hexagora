---
date : 2021-05-15
abstract : 1156-1159. Entre deux mondes, latin et autochtone, Abu Qasim tente de vivre sur la terre de ses ancêtres, en tenant son rang. Au prix d’une certaine adaptation.
characters : Abu Qasim, Abu Mahmud, Robert de Retest, Heimon, Ahmed, Mawdud, Rawh al-Furat
---

# Modus vivendi

## Casal de Salomé, demeure d’Abu Qasim, début d’après-midi du youm al joumouia 14 muharram 551^[Vendredi 9 mars 1156.]

Le ra’is du casal avait proposé au shaykh Abu Mahmud de partager son repas, car il espérait avoir ainsi le temps de se faire une idée de l’état d’esprit du vieux avant les palabres plus officiels. Ils avaient de nombreux points à voir pour la gestion de la communauté, que ce soit l’organisation des pâtures, la préparation des récoltes ou l’attribution des terres pour l’année qui suivrait. Contrairement aux Latins qui se concentraient chacun sur leur propre parcelle, les autochtones exploitaient la zone en commun, chacun ayant droit à une part proportionnelle à ses apports et son travail. La répartition était toujours délicate, orchestrée par les doyens du village, dont Abu Mahmud était le plus respecté représentant.

Abu Qasim était pour sa part le plus fortuné, et celui qui devait parler en leur nom à tous au seigneur du lieu, le bouillonnant Robert de Retest. Et il serait bientôt temps de lui verser la taxe qui les frappait en tant que non-chrétiens. Comme beaucoup d’autres redevances, dont celles sur la production agricole, elle était exigée pour la communauté et chacun devait y contribuer selon un complexe assemblage d’évaluations personnelles de sa richesse propre et de souci de tenir son rang. Ne pas payer ce qu’on estimait correspondre à sa place dans le casal était insultant, débourser plus était injuste.

C’était un des impôts que beaucoup regardaient, dans le fond, comme le plus dur à supporter, car il était réclamé en monnaies. Et celles-ci étaient fort rares dans le village. Il fallait s’arranger pour faire du commerce, forcément limité vu les nombreuses interdictions qui entravaient leurs initiatives. La plupart des pièces étaient acheminées puis distribuées dans le casal par le biais du négoce au long cours que le ra’is était quasiment le seul à exercer. Ce qui renforçait d’autant plus son pouvoir et nourrissait une part de la détestation qui pouvait naître envers lui. S’il ne daignait pas vous acheter un produit ou l’autre dans l’année, vous risquiez de ne pas pouvoir vous acquitter de votre contribution à la taxe.

Abu Mahmud s’efforçait d’éviter que les ressentiments n’explosent en colère et cherchait toujours à trouver une façon acceptable par l’honneur de chacun pour que les monnaies soient là où il le fallait au bon moment. Et celui-ci était venu.

Abu Qasim finissait de grignoter des pistaches, le visage arborant l’air satisfait de celui qui a fait un savoureux repas. Le ra’is ne se montrait jamais mesquin avec ses invités, quand bien même c’étaient les fellahs les plus indigents des lieux. C’était pour lui autant une question de politesse, de respect de son rang que de représentation. Avalant un verre de lebné, il se tourna vers Abu Mahmud, occupé à déguster une excellente pâtisserie à la semoule.

« Alors, Abu, penses-tu que nous pourrons collecter la jizyah tantôt ? Le terme en est le mois prochain, à la fin de safar.

— Tout n’est pas prêt. Certains espèrent encore des marchés qui arrivent.

— Il s’en trouve toujours qui ne savent s’organiser. Pourtant chacun a appris combien il aura à verser, depuis un an !

— Il y a souvent des imprévus, et certains sont plus avisés que d’autres dans leurs choix. »

Abu Qasim ne pouvait qu’approuver à cette dernière remarque. Si la plupart des fellahs payaient leurs taxes sans trop poser de problème, la majorité des soucis venait immanquablement d’une minorité trublionne, mesquine ou imbécile.

« Je me demande parfois si nous ne devrions pas faire un exemple, que la leçon porte pour les autres.

— La plupart ne font pas cela de mauvaise intention.

— Mais le fait est là que je devrai, comme chaque année ou presque, m’humilier devant le sire Robert pour obtenir délai. J’en ai assez de devoir payer pour les réfractaires ! »

Abu Mahmud ne répondit rien, sachant pertinemment qu’Abu Qasim avait plus que les moyens de verser la taxe pour les quelques malheureux qu’il vilipendait. Cela lui offrait en outre l’occasion de se faire valoir auprès d’eux comme leur mécène. Il tenait ainsi, par ces dettes d’honneur, une bonne partie des familles les plus pauvres, qui n’osaient plus prendre position contre lui lors des assemblées.

« Peut-être que le sire de Retest accepterait de nous donner licence de commercer plus avant ? Nous avons de la laine, du fil et quelques denrées qui pourraient trouver acheteur, mais dans l’incapacité de quitter les environs, comment faire ?

— Si tu penses que c’est là opportune idée, ne te prive pas de lui en parler, mais moi je ne m’y risquerai pas. »

Abu Qasim savait pertinemment qu’Abu Mahmud n’oserait jamais demander audience au seigneur du casal, il le craignait bien trop pour cela, peut-être encore plus que tous les autres fellahs. Et lui, en tant que ra’is, avait quelques privilèges qui renforçaient sa position, et dont il ne se séparerait pas volontiers. Il ne voyait personne dans le village qui puisse améliorer les choses mieux que lui ne s’y employait. Et il avait besoin de moyens pour cela. N’était-il pas le plus généreux donateur à la mosquée pour l’huile des lampes ?

## Casal de Salomé, demeure d’Abu Mahmud, soirée du youm al talata 17 jha’ban 552^[Mardi 24 septembre 1157.]

Abu Mahmud était rentré de sa journée à récolter les feuilles de sumac en grognant. Sa mauvaise jambe lui faisait mal et la chaleur de l’été commençait à lui peser. Il n’avait qu’à peine eu le loisir de se rafraîchir dans la cour, puisant dans une bassine d’eau tiède pour s’en asperger le visage, que des quémandeurs se présentaient déjà sa porte. Il savait qu’avec la fin des travaux des champs, les hommes auraient de nouveau du temps pour discuter, mais il ne pensait pas que cela débuterait si tôt.

Il convia donc Ahmed et Mawdud, deux fellahs du sud du hameau à venir prendre des mezzé avec lui. Sans un mot de reproche ou un regard déplacé, sa femme se contenta de prendre acte de bouches supplémentaires, une fois de plus, à leur repas du soir. Heureusement pour elle, la communauté se montrait généreuse avec leur foyer et il n’était pas rare que des voisines lui déposent du pain, des vivres voire carrément un plat. Il demeurait qu’elle devait gérer les invités seule, ses enfants ayant tous depuis longtemps quitté leur modeste maison pour vivre leur vie.

C’était d’ailleurs ce soir la question qui amenait leurs hôtes. Une des filles de leur famille était en âge de se marier et ils avaient des cousins dans un casal peu éloigné qui pouvaient arranger l’union avec un beau parti : un aîné, dont le père possédait pas mal de têtes de bétail, chèvres et moutons, mais surtout un élevage de mules réputées. C’était là une occasion providentielle pour eux. Ils avaient amassé de quoi constituer une dot plus qu’honorable et espéraient remporter l’affaire. Pour peu que le seigneur des lieux approuve cette union en dehors de ses terres. Abu Mahmud essayait de faire comprendre la bonne façon de présenter cela à Robert de Retest pour obtenir la précieuse autorisation.

« Cela se déroulerait avec un fellah d’un autre village du sire de Retest, j’encrois qu’il ne ferait pas de souci. Il l’a déjà accepté par le passé, et on peut s’en tirer par un petit présent, en reconnaissance. Rien d’important, mais qui marque la gratitude. Mais là, il s’agit d’une seigneurie distincte…

— Peut-être, mais cette union rejaillira sur nous, sur tout le casal !

— Pour être Ifranj et soldat, Retest n’est pas pour autant idiot. Il sait bien que cela ne lui servira en rien, qu’une fille qui quitte son père appartient à son époux. Il préfère les voir arriver dans ses casals qu’en partir.

— Et lui dire que nous pourrions ainsi avoir nouvelles facilités à disposer de bêtes pour travailler la terre ?

— Il n’en croira rien. C’est trop éloigné pour que les mules puissent nous être d’usage. »

Abu Mahmud grignota quelques graines, qu’il décortiqua avec soin, prenant un moment de réflexion.

« Pour la petite de Hasan, il y a quelque temps, il a fini par concéder son accord, mais il a fallu le dédommager. Je ne pense pas qu’on pourra l’éviter.

— Hasan avait des monnaies, qu’il avait obtenues de la ville en gardant les moutons d’un riche négociant ! Moi je n’ai rien de cela.

— Tu as pas mal de chèvres, et quelques mesures de vigne. Ce dernier point pourrait nous aider. Les Ifranjs aiment le vin, plus encore que l’or j’ai parfois l’impression. Peut-être pourrais-tu lui proposer champart de ta récolte ?

— C’est avec ça que je gagne de quoi payer la jizyah ! Je vais faire comment si j’y renonce ?

— Un problème après l’autre ! D’abord, trouvons comment marier ta fille. Le reste, nous en reparlerons. »

## Casal de Salomé, mosquée, après-midi du youm el itnine 1 dhu al-qi’dah 553^[Lundi 24 novembre 1158.]

Après des averses matinales, le soleil avait retrouvé sa prééminence et apportait par les fenêtres une douce lumière à l’assemblée des hommes regroupés dans la mosquée. Devant le mihrab, flanqué d’Abu Qasim qui traduisait, l’intendant de Robert de Retest avait pris place. Il était venu les voir, et en particulier les plus anciens d’entre eux, car il avait besoin de savoir quelles étaient les traditions en ce qui concernait les routes et chemins. Certains endroits demandaient de l’entretien et il convenait de faire les choses correctement.

Pour avoir conquis les terres et soumis les hommes, les Latins tenaient à ne pas s’aliéner les populations et, dans la mesure du possible, les coutumes, usages et redevances étaient perpétués, parfois adaptés. Le manque d’archives écrites et la méconnaissance de la langue faisaient qu’ils étaient souvent contraints de consulter la mémoire des locaux, confrontant les avis pour en établir une proposition qui faisait alors force de loi. Il était bien sûr nécessaire de vérifier que les souvenirs ne se montrassent pas trop troués sur les obligations et trop étendus en ce qui concernait les droits. La moindre mise au clair était donc l’objet d’intenses palabres. L’essentiel demeurait que les nouveaux maîtres puissent assoir leur pouvoir et les redevances affiliées sans que cela requière trop d’efforts ou de violence.

Abu Qasim venait de traduire la demande sur les usages et personne n’osait prendre la parole. Nul n’avait envie d’être celui par qui une nouvelle corvée, un nouvel impôt leur serait exigé. Quand bien même il ne s’agissait que de la résurrection d’un prélèvement commodément oublié. Ce fut Rawh al-Furat, celui qui entretenait la mosquée et appelait à la prière qui fut le plus téméraire.

« Il faut lui dire qu’on ne se soucie pas des chemins. Ce sont les Bédouins qui vont et viennent. Nous, on reste au village. »

De nombreuses têtes approuvèrent. C’était une bonne entrée en matière. Abu Qasim traduisit pour l’intendant.

« Il déclare que les voies ne sont pas le fait des fellahs, qui vivent attachés à la terre.

— Je sais bien cela, mais il demeure que des routes existent, entre les cités, et qu’il faut bien en prendre soin. Qu’exigeait-on d’eux par le passé pour cela ? »

Abu Qasim traduisit, ajoutant à la fin un commentaire de son cru.

« Il ne sert à rien de nier l’évidence. Il y a le sentier de l’oued, qui est bien façonné sur son flanc, qu’il a déjà parcouru maintes et maintes fois. Et les murets qui conduisent moutons et chèvres en dehors des cultures…

— Nos pères ont bâti cela pour eux. Là c’est différent, il espère que nous allons aider à établir routes caravanières ! Qu’y gagnerons-nous ? Nul chamelier ne viendra jusqu’ici, pour aller où ?

— Moi je veux bien aller suer pour eux, mais contre dirhems brillants ! s’enthousiasma un autre. Et avec de bons outils de fer… »

Comprenant que les tractations commençaient, l’intendant interrogea d’un regard son interprète.

« Ils déclarent qu’aucune coutume ne les a jamais obligés à casser la pierre et renforcer les chemins. Ils n’ont aucun souvenir de cela. Nos sentiers sont suffisants pour nos ânes et nos biques.

— Dis-leur que cela va changer. Il est de juste pratique en nos provinces que pareilles corvées soient prescrites.

— Je le leur ai signifié, mestre Heimon. Mais ce sont des fellahs, tout juste bons à semer le grain et traire la chèvre. Peu de place en leur crâne pour y mettre quelque nouvelle idée. »

Abu Qasim marqua une pause, faisant mine de réfléchir tout en tendant une oreille à ce qui s’exprimait dans la salle. Il profitait de ce que le ton montait pour prendre un air inquiet.

« Ne leur laissez pas imaginer que vous les voyez comme esclafs, ce serait faire insulte à leur condition d’hommes libres.

— Je n’ai jamais dit pareille chose, s’offusqua l’intendant. Chez nous, vilains doivent corvées, voilà tout.

— Je vous connais assez pour ne pas croire cela de vous, mais c’est un point… chatouilleux pour eux. »

Pendant ce temps, les échanges avaient continué entre ceux qui estimaient qu’ils n’avaient rien à faire des sentiers et des routes qu’ils n’emprunteraient jamais et un autre groupe qui ne considérait pas d’un mauvais œil l’idée de gagner quelques monnaies, ou, au pire, de solides outils.

« Voyez, ils s’agitent, car certains pensent que vous avez désir de changer leurs usages, ce que d’aucuns, dont je fais partie, ne peuvent croire. M’est avis qu’il serait plus avisé de ne pas insister sur ces demandes.

— Il faudra bien que les chemins soient tenus en bon état !

— Je vous l’ai dit, ce sont de simples fellahs, attachés à leurs traditions. Mais ce sont aussi des hommes laborieux, que motiverait un dédommagement, j’en suis certain.

— Que voulez-vous dire ?

— Si vous pensez pouvoir distraire quelques piécettes et de quoi armer leurs mains ouvrières, je me fais fort d’en convaincre d’accepter cette corvée. Même si je sais la tâche malaisée, cela me semble de mon devoir au service du sire de Retest. »

## Château de Salomé, grande salle, après-midi du youm al had 19 ramadan 554^[Dimanche 4 octobre 1159.]

Agité comme à son habitude, Robert de Retest allait et venait sur la petite estrade où se tenait la chaise où il était sensé tenir audience. Il recevait en fait toujours debout, dépensant son trop-plein d’énergie par d’incessants déplacements qu’il ponctuait de coups de la badine qui ne le quittait plus guère depuis quelques années. Devant lui, aussi impassible d’un récif au plus fort de la tempête, Abu Qasim répondait à ses questions, avec un flegme étudié, né d’années de pratique du tonitruant chevalier.

« Je sais que c’est le moment de repos pour vous autres ! Et avec la fin des travaux des champs, je n’ai nul désir de voir mes fellahs s’évaporer comme neige au soleil ! Penses-tu que j’ignore que certains complotent, organisent la fuite… Mais je ne lâcherai rien ! Nul ne peut disposer de mon bien à sa guise ! »

Il regarda danser sa baguette un moment, y déversant son trop-plein de pression.

« S’il me faut fouetter tous les dos des casals, je le ferai, crois-m’en ! Il n’est d’étalon trop fier pour supporter la selle ! »

Fatigué par l’achèvement de cette longue diatribe, où il tentait de faire comprendre au ra’is de Salomé qu’il avait intérêt à mettre fin aux évasions avant qu’il ne soit forcé de sévir plus durement, il se laissa tomber dans son siège. Abu Qasim devina qu’il pouvait désormais s’exprimer, avec toute la componction servile qui flattait cet homme violent et autoritaire.

« J’ai retransmis vos ordres et mandements avec zèle et précision, sire Robert. Mais le fellah a de l’âne l’entêtement et le mauvais caractère, loin de la noblesse de l’étalon.

— À quoi me sers-tu donc, alors ?

— J’ai usage de ces gens, sire. Sauf à faire utile exemple qui-ci qui-là, les frapper trop durement ne serait guère efficace. Il faut les mener fermement, mais sans qu’ils se sentent contraints.

— Ai-je l’air d’un pâtre qui gère un troupeau ?

— Certes pas ! Vous êtes le souverain de ces terres, et c’est à moi que revient cette tâche. Je sais comment tirer l’huile de l’olive, en la pressant peu à peu. Il ne serait d’aucune utilité de la frapper d’un marteau, n’est-ce pas ? »

Robert de Retest grogna. Il n’aimait pas que les choses lui résistent, et encore moins devoir envisager de changer de méthode. Cela faisait des années qu’il punissait lourdement les captifs, n’hésitant pas à recourir à des mutilations, parfois sévères. On s’en était même ému à la Haute Cour, ce qui lui déplaisait au plus haut point. D’un geste impatient de sa badine, il fit comprendre à Abu Qasim de disposer. Il était temps pour lui de porter son message, qu’il lui délivra d’une voix sèche, née de sa colère froide.

« Que les choses soient claires : si d’aucuns s’aventurent à quitter mes terres encore cet hiver, ma punition sera féroce sur tous les résidents. Envers toi le premier ! Dis-le-leur à tous ! »

Lorsqu’il salua une dernière fois en se retirant de la salle, Abu Qasim soupira. Il était de plus en plus las de ces petits jeux entre le seigneur franc et les villageois. Il avait été jusqu’à présent habile à éviter les coups, d’un côté ou de l’autre, mais ne se faisait guère d’illusion sur l’amitié qui lui serait manifestée s’il perdait son statut. Pourtant, depuis des années, il ne ménageait pas sa peine pour faire en sorte que la vie dans le casal soit la meilleure possible, cherchant à épargner les fellahs et satisfaire un maître exigeant, comme ils le sont tous.

Il ne décolérait pas des fugitifs qui le mettaient ainsi en porte-à-faux auprès du seigneur latin. Cela d’autant plus qu’il estimait lâche de leur part d’abdiquer. Il était né à Salomé, comme ses pères avant lui. Jamais rien ne pourrait le décider à l’abandonner ! Ni un pouvoir inique venu de par delà les mers ou des plaines orientales, ni l’entêtement imbécile de quelques esprits chagrins qui se faisaient manipuler par une poignée d’imams avides d’influence. ❧

## Notes

Les autochtones des territoires du Moyen-Orient ont vu défiler nombre de conquérants au début du second millénaire. Malgré cela, les populations demeurèrent majoritairement en place, se contentant de verser les impôts à un nouveau dirigeant sans que leur quotidien en soit bouleversé. Les administrations qui se succédaient devaient pour leur part s’étendre sur des zones où un certain nombre d’usage avait été validé par le temps. Il était toujours délicat de faire table rase, d’autant plus que les moyens militaires étaient rarement suffisants pour permettre de déployer des forces de police internes en plus des armées levées contre les ennemis extérieurs. Cela a assurément amené à des situations qui, pour conflictuelles qu’elles fussent, devaient se dénouer sans complètement aliéner un parti ou l’autre. Et, bien sûr, au centre de ces articulations de pouvoir se trouvaient des hommes faisant interface.

Le terme de jizya, qui désigne normalement la taxe de capitation due par les non-musulmans en terre musulmane, est ici indûment utilisé par des musulmans autochtones pour nommer l’impôt de même type qu’ils devaient en territoire latin. Je n’ai néanmoins pas retenu l’aspect infamant qui se rattachait lors du versement, dont je n’ai découvert aucune trace. J’ai plutôt choisi de l’envisager comme un prélèvement habituel, à payer par la communauté complète et pas par les individus.


## Références

Ellenblum Ronnie, *Frankish rural settlement in the Latin kingdom of Jerusalem*, Cambridge, Cambridge University Press, 1998.

Ellenblum Ronnie, « Settlement and society formation in crusader palestine », in Thomas Evan Levy (dir.), The Archaeology of Society in the Holy Land, 1995, p. 502‑511.

Weulerse Jacques, *Paysans de Syrie et du Proche-Orient*, Paris, Gallimard, 1946.












