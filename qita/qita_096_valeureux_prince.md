---
date: 2019-10-15
abstract: 1157-1158. Jeune sergent pétri de rêves de chevalerie, Ernaut découvre en Terre sainte un héros à la hauteur de ses aspirations, le vaillant et téméraire Renaud de Châtillon, prince d’Antioche à la sulfureuse réputation.
characters: Père Josce, Droart, Ernaut, Karpis, Tiphaine, Perrin, Rufus Larchier, Eudes Larchier
---

# Valeureux prince

## Taverne du père Josce, veillée du jeudi 19 décembre 1157

Emplie de monde, la pièce au pilier résonnait des conversations murmurées, des dés lancés avec espoir, des interjections graveleuses, des rires avinés ou des appels insultants. On y croisait plus de sergents que partout ailleurs en ville, la soldatesque aimant à s’y retrouver. Grâce à la foule qui s’y entassait, l’endroit était chaleureux malgré les frimas. On n’y voyait guère de pèlerins et de voyageurs, l’aspect décati des lieux n’incitant guère à y entrer. La qualité y était correcte, les prix modestes et, parfois, les filles accueillantes.

En cette période de fêtes hivernales, la taverne offrait l’occasion de se réchauffer le cœur et le corps à bon compte. Sueur, boissons et plats apportés depuis Malquisinat proche se mêlaient à la fragrance de paille humide suintant du sol et de salpêtre des murs.

Le père Josce lui-même ne faisait commerce que de vin, mais il laissait entrer librement les servantes des bains peu éloignés. De temps à autre, il acceptait aussi d’accueillir des jongleurs ou des musiciens, pour animer l’endroit. On le disait très vieux, eu égard à son absence quasi totale de cheveux, ses rides profondes et son dos voûté qui le faisait ressembler à une tortue. De plus, un mauvais œil faisait qu’il regardait toujours de côté comme un animal intrigué. Mais son ouïe était excellente, ainsi que sa mémoire, et il laissait les naïfs s’amuser de son apparence inoffensive sans s’émouvoir le moins du monde. On parlait souvent de l’Allemand châtré pour s’être mal conduit, sans savoir si c’était une vraie histoire. À chaque évocation, ceux qui la racontaient assuraient la tenir d’un témoin direct.

Sous une fine fenêtre, bouchée d’un volet coincé de paille, Ernaut discutait avec ses collègues de leur journée. Ils avaient appris depuis peu l’abandon du siège de Césarée[^cesareeoronte] par les armées royales. Les raisons de cet échec faisaient l’objet de commentaires enflammés et partiaux, parfois aussi mal informés que peu avisés.

« Je ne vois pas en quoi le sire comte a démérité, ’scuse moi, rétorqua à Ernaut un sergent au visage creusé, le nez comme un soc.

— Il est plus fier qu’étron de pape, Robert de Flandre ! Refuser hommage à prince, pour qui se prend-il ?

— Bein pour un grand sire comte, c’est bien le moins ! »

Ernaut manqua s’étouffer devant la provocation.

« Il n’y a pas ahonterie à jurer sa foi à prince d’Antioche quand on est simple comte !

— Bein quand ledit prince est cadet de famille chez lui, ça doit rendre les genoux moins souples…

— Il a refusé de faire hommage au petit prince[^bohemond3], aussi. C’est bien là vanité ! »

Droart, affalé contre le mur, ne prenait pas part à la conversation. Il savait qu’Ernaut s’était mis à entretenir une sorte de vénération pour le prince d’Antioche Renaud de Châtillon. Il se demandait si c’était lié au fait que tous les deux étaient Bourguignons ou à l’espoir qu’Ernaut caressait de connaître le même parcours. Renaud de Châtillon était un petit chevalier au service du roi Baudoin quand l’annonce de son mariage avec la veuve de Raymond d’Antioche avait surpris tout le monde. On le disait vaillant et bien de sa personne et il incarnait le rêve de beaucoup : seigneur d’un vaste territoire par ses mérites et son amour, habile à manier la lance et jamais en reste pour aller poindre des deux. Par contre, on le dépeignait également comme irascible, coléreux et violent. Un jeune bachelier convaincu que le monde n’était qu’un verger sans fin où cueillir les fruits qu’il dévorait à belles dents.

« Vu l’importance de la ville, le prince Renaud aurait pu accepter que le sire Thierry jure sa foi au roi, non ? Ainsi ils auraient pris la cité.

— Césaire dépendait d’Antioche dans les temps anciens, pourquoi rompre coutume pour caprice de baron ? Ce n’est pas l’usage ! N’en plaise au comte !

— Souventes fois choses varient, Ernaut.

— Doit-on ployer le genou parce que c’est désir de puissant ? Renaud est preux chevalier tel qu’il en faudrait quelques échelles[^echelle] pour le royaume. Les mahométans auraient tôt fait de se convertir ou de disparaître. »

Il conclut sa diatribe d’une longue rasade avant de claquer son verre sur la table, puis se leva.

« Moi je dis que le sire prince Renaud est homme de bien, on le rabaisse, car il n’est pas fils de comte. Sa vaillance est son droit ! »

Il jeta quelques piécettes de cuivre pour son écot de vin et s’en fut, enfilant sa chape tout en sortant. Droart sourit et se pencha vers son collègue, mi-figue, mi-raisin :

« Faut pas le chercher sur le prince d’Antioche. Renaud, c’est son héros ! »

## Palais royal, soir du vendredi 17 janvier 1158

La senteur de bois brûlé des braseros se mêlait à l’odeur de soupe et de sauce. Les cuillers claquaient dans les écuelles et on entendait les cruches heurter les gobelets dans le brouhaha des conversations. Des marmitons allaient et venaient, les bras chargés de pains et de plats, bourdonnant autour des serviteurs occupés à se restaurer. La haute table, sur l’estrade était vide, aucun hôte de marque n’étant présent. Le roi Baudoin guerroyait au nord, la plupart des gens de son hôtel l’avaient suivi. Et le vicomte Arnulf ne s’était pas montré ce soir. Malgré tout, il demeurait une foule de domestiques à sustenter : sergents de ville et hommes d’armes, voyageurs, invités et messagers de moindre rang. Même lorsque ce n’était pas fête, Baudoin nourrissait amplement sa maisonnée, comme il seyait à tout baron digne de ce nom.

En l’absence des officiers, on s’assemblait un peu selon ses affinités et Ernaut avait pris place près d’un cavalier arrivé dans la journée. Son turban étrangement noué, sa tenue et son visage indiquaient clairement qu’il n’était pas latin. Mais il n’était pas Syrien ou Turc pour autant et il aurait pour sûr accueilli comme une insulte qu’on l’appela Grec. Karpis était un Arménien, un montagnard farouche au geste sûr et au verbe haut. Il avait fait voyage depuis Harenc[^harenc] comme escorte d’un messager. Il décrivait le siège qui se poursuivait, malgré les difficiles conditions hivernales.

Tout en parlant, l’Arménien engloutissait tout ce qui lui était présenté. Il avait confié ne pas avoir si bien mangé depuis des semaines.

« Voilà long temps que tu t’es juré au service du prince ?

— Je l’ai fait au retour de Chypre. Jusque là, je ne servais aucun maître. Mais j’ai su en le voyant que c’était grand homme. »

Ernaut se redressa, arborant un large sourire. Il s’était rapproché de Karpis dans l’espoir d’en apprendre plus sur Renaud et il découvrait avec plaisir qu’il n’était pas le seul à l’admirer.

« Est-il si preux qu’on le dit ?

— Plus que ça ! Bien des hommes n’ont pas quart de sa fougue. Il fallait le voir, sur l’île, chasser les Griffons comme vulgaires poulets. Son épée tranchait les têtes comme on moissonne les blés.

— J’aurais grand plaisir à l’encontrer !

— Il emploie volontiers qui a désir de le bien servir. Et toi, avec ta stature d’ours des montagnes, tu serais accueilli à bras ouverts dans l’hôtel de n’importe quel baron. »

Celui qui disait ça était pourtant solidement charpenté, les membres épais et les mains puissantes, carrées, aux doigts noueux. La tête semblait émerger directement des larges épaules et sa tenue se tendait à chaque geste malgré l’aisance de la toile. Il avait un physique de lutteur de foire, sauf qu’il ne le mettait pas en avant. Ce qu’on voyait, c’était ses deux yeux bruns moqueurs, qui brillaient sous des sourcils broussailleux, aussi noirs que sa barbe raide.

« Je suis juré au roi pour le moment, mais j’ai espoir de ne pas demeurer ici, en la cité, et de le servir au-dehors, l’arme en main. Avec un peu de chance… »

L’Arménien avala un morceau de pain et lui sourit.

« Crois-m’en, il y a toujours bonne moisson à faire lorsque les princes font bataille. »

## Environs de la rue des Tanneurs, veillée du jeudi 30 janvier 1158

Depuis la maison voisine, l’ombre se faufila sans un bruit en quelques bonds agiles et discrets. Longeant la terrasse, elle se laissa couler souplement jusqu’à un encadrement de fenêtre avant d’atterrir mollement au sol. Elle se figea un instant, scrutant l’obscurité dans l’attente d’un éventuel danger. La petite cour était déserte, mais des voix filtraient depuis un volet entrebâillé. De la lumière en sortait également, ainsi qu’un effluve de nourriture. De viande, même. Intriguée, la silhouette bondit sur le rebord et risqua un œil vers l’intérieur. Elle y découvrit plusieurs personnes, les familiers de l’endroit, mais aussi le géant aux gestes amples qu’elle croisait de temps à autre. Agréable odeur que la sienne, sans compter les caresses qu’il prodiguait. Elle lui sauta donc dessus, dans l’espoir de s’en attirer quelques-unes.

« Prince ! » hurla un des gamins d’Eudes, tandis que le chat venait se frotter à Ernaut. Tiphaine, la femme d’Eudes, allait le chasser d’un coup de torchon, mais elle se retint, voyant que son invité semblait apprécier l’animal. Il ne perdait néanmoins rien pour attendre, ce petit chapardeur.

« C’est ta bête ? demanda Ernaut à Perrin, le cadet qui avait crié.

— Nan, il est juste souventes fois dans le coin.

— C’est moi qu’ai trouvé son nom, indiqua Rufus, l’aîné âgé de six ans. Il a comme couronne sur le chef, ajouta-t-il en riant et en se tortillant sur le banc

— Je vois ça ! » confirma Ernaut.

Il flatta un peu l’animal qui se mit à ronronner puis le posa sur ses genoux tandis qu’Eudes revenait avec une cruche de bière légère. À table, il préférait ça au vin et en servit une généreuse rasade à son invité.

« Grand merci, Eudes. J’ai fort bien mangé. J’avais pas avalé compotée de choux depuis bien longtemps !

— Tu auras bientôt un foyer où mijoteront les plats » s’amusa Tiphaine.

La jeune femme avait semblé un peu distante à Ernaut au début, mais il avait fini par l’apprécier. Elle demeurait habituellement légèrement en retrait, mais savait échapper quelques remarques amicales de temps à autre. Ses grands yeux bruns vous regardaient franchement et on voyait qu’elle était sincèrement attachée à Eudes, qui le lui rendait bien. Il aimait manger chez eux, car l’ambiance y était bien meilleure que chez Droart, même si la chère n’y était pas si bonne. Ils lui donnaient l’image du foyer qu’il espérait prochainement fonder avec Libourc.

« Toujours pas de date prévue ? demanda Eudes.

— Pour le mariage ? Certes non. Lambert a convenu d’en parler avec le vieux Sanson avant Carême. Mais je n’ai nul bien encore assez, et Sanson n’est pas homme à accepter un douaire de journalier. J’amasse mon pécule, trop lentement à mon goût. Il me faudra peut-être joindre l’ost, espérer bon butin.

— Tu as trop palabré avec l’Arménien, toi. Il t’a mis de drôles d’idées en tête mon ami. »

Ernaut haussa les épaules, avalant sa dernière bouchée avec une large portion de pain.

« J’aime à discuter avec les voyageurs, cela me donne l’impression de parcourir le monde.

— Sois prudent, il en est de certains qui avancent derrière un masque. Karpis est de ceux-là.

— Que veux-tu dire ?

— J’ai encontré un vétéran du vieux prince Raymond, désormais retiré en un casal vers Hébron. Il racontait certaines choses pas bien belles sur Chypre et le nom de Karpis était dans sa bouche en ces moments.

— Y’a pas qu’un âne qui s’appelle Martin ! » le coupa Ernaut, péremptoire.

Eudes commençait à bien connaître son ami et avait compris qu’il était illusoire de lui faire entendre raison quand il avait une idée en tête. Il liait à un physique de géant la détermination d’un sanglier. Mieux valait l’aborder par le flanc et laisser les choses se faire à leur rythme.

« Je sais bien que tu brûles d’aller batailler, mais c’est un endroit d’où on revient différent. Karpis est de ceux qui se sont perdus là-bas, je peux le voir, et tu t’en rendras compte un jour. Je pense que le prince Renaud est pareil. Trop de vaillance n’est pas forcément un don.

— Si j’ai envie d’écouter un prêche, je mange habituellement chez mon frère, le railla Ernaut.

— Cul-Dieu, si je me mets à deviser comme Lambert, je vais bientôt finir moine ! éclata de rire Eudes.

— Sers-moi donc de ta bière, frère Eudes, avant que d’aller te faire tonsurer le crâne. Moi je n’ai pas l’intention de quitter le siècle. »

Il allait ajouter une remarque grivoise, mais il se retint au dernier moment. Il n’était ni à la taverne ni avec d’autres gardes, et Tiphaine, malgré sa gentillesse, n’apprécierait guère qu’on parle légèrement des femmes sous son toit. Il se contenta de choquer son verre contre celui de son ami. Il repensa soudain à son héros, qu’il brûlait tant de rencontrer.

« Puisse Dieu me permettre de saluer un jour le beau sire prince d’Antioche, et me donner sa vaillance.

— Puisse Dieu te faire présent de plus de prudence » ajouta Eudes. ❧

## Notes

Ce Qit’a était le premier que j’avais commencé pour introduire Renaud de Châtillon dans Hexagora et je l’ai reporté maintes fois, espérant le conserver pour une passerelle vers la cinquième enquête d’Ernaut, dont le titre envisagé, « Les noces de porphyre » indiquent que l’intrigue va se déplacer vers le Nord, plus près des territoires byzantins. Le prince d’Antioche y est bien sûr un personnage incontournable. Ne sachant pas à quel moment j’aurai le temps de m’attaquer à ce nouvel opus, il m’a semblé préférable de publier sans attendre plus.

L’idée qui sous-tend ce court texte est celle de la propagation des nouvelles, qui se fait uniquement par voie orale. Comment les réputations se faisaient autour des échanges, en un rythme lent, qui ciselait au fil des mois et des années une image auprès d’un public qui recevait par la même voie les récits des figures légendaires et mythiques. Il m’a semblé intéressant de voir la façon dont l’identification pouvait alors être hybride, s’accrochant à une représentation qui n’avait pas besoin d’être complètement intégrée au réel. Cela vient donc en complément et miroir de ce qui s’est déroulé dans « Le lion et l’éléphant ».

## Références

Hamilton Bernard, « The Elephant of Christ: Reynald of Chatillon » dans *Studies in Church History*, vol. 15, 1978, p. 97-108.

Schlumberger Gustave, *Renaud de Châtillon, prince d’Antioche, seigneur de la terre d’Outre-Jourdain*, Paris, Plon : 1898.
