

---
shorttitle: Qit'a
parution: Mai 2020
website: http://www.hexagora.fr
title: Qit'a - Volume 4
subtitle: Textes courts dans l'univers Hexagora
subject: Hexagora, Qit'a
rights: Creative Commons BY SA
lang: fr-FR
frequency: Recueil - volume 4
description: Recueil de textes courts Qit'a - Volume 5. Qit'a signifie littéralement «&nbsp;morceau&nbsp;» en persan. De Bruges à Mossoul, Ascalon, Baniyas ou Vézelay, suivez pour quelques heures, quelques jours, le destin de femmes et d'hommes de tous rangs, de toutes conditions, modeste moine ou artilleur inventif, chanteuse, ambassadeur ou orpheline... Ils ont tous en commun de vivre et d'aimer, de travailler et de batailler dans l'univers qui accueille les enquêtes d'Ernaut de Jérusalem. Ils ont parfois croisé le jeune investigateur, ou ont pris des décisions qui ont pu en affecter le destin ou celui de ses proches. Tous sont liés, directement ou indirectement, les uns aux autres en une toile infiniment tissée, le monde d'Hexagora, reflet de notre passé au temps des Croisades.
date: 2020-05-15
cover-image: cover.jpg
characters: Ernaut, Lambert
author: Yann Kervran
abstract: Recueil de textes courts Qit'a - Volume 5. Qit'a signifie littéralement «&nbsp;morceau&nbsp;» en persan. De Bruges à Mossoul, Ascalon, Baniyas ou Vézelay, suivez pour quelques heures, quelques jours, le destin de femmes et d'hommes de tous rangs, de toutes conditions, modeste moine ou artilleur inventif, chanteuse, ambassadeur ou orpheline... Ils ont tous en commun de vivre et d'aimer, de travailler et de batailler dans l'univers qui accueille les enquêtes d'Ernaut de Jérusalem. Ils ont parfois croisé le jeune investigateur, ou ont pris des décisions qui ont pu en affecter le destin ou celui de ses proches. Tous sont liés, directement ou indirectement, les uns aux autres en une toile infiniment tissée, le monde d'Hexagora, reflet de notre passé au temps des Croisades.
---


![Framabook, le pari du livre libre](logo_framabook_epub.jpg)

Framasoft est un réseau d'éducation populaire, issu du monde éducatif, consacré principalement au logiciel libre. Il s'organise en trois axes sur un mode collaboratif&nbsp;: promotion, diffusion et développement de logiciels libres, enrichissement de la culture libre et offre de services libres en ligne.

Pour plus d'informations sur Framasoft, consultez <http://www.framasoft.org>

Se démarquant de l'édition classique, les Framabooks sont dits «&nbsp;livres libres&nbsp;» parce qu'ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu'un utilisateur de logiciels libres. Les Framabooks s'inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l'appropriation collective de la connaissance.

Pour plus d'informations sur le projet Framabook, consultez <http://framabook.org>

Copyright 2020&nbsp;: Yann Kervran, Framasoft (coll. Framabook)

Ce recueil de textes Qit'a est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).

ISBN&nbsp;: 979-10-92674-31-6\
Dépôt légal&nbsp;: Mai 2020\
Couverture&nbsp;: Saint Jean évangéliste, Évangiles provenant de Géorgie, Centre de Recherches de Dujcev, Gr. 358, fol. 197 v.1125. Domaine Public - [commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:JohnDRCGr358Fol107v.jpg)

Remerciements
-------------

La publication de ce premier lot de quatre recueils de textes courts Qit'a a demandé beaucoup de temps et d'énergie au membres du groupe Framabook, je tiens à les en remercier, Mireille en tête, avec qui les échanges autour de corrections proposées ont toujours constitué d'agréables moments.

*À la mémoire des exclus de la Mémoire*

La main du destin
-----------------

### Mossoul, citadelle des bords du Tigre, matin du youm el itnine 27 ramadan 530[^1] {#mossoul-citadelle-des-bords-du-tigre-matin-du-youm-el-itnine-27-ramadan-5301}

Du bout des doigts, le jeune garçon poussait une araignée dans la lumière poussiéreuse filtrant sous la porte. Bouleversé par les dernières semaines, il n'osait plus guère bouger, se laissant guider d'un endroit à l'autre sans un mot, sans un geste. Il n'avait même pas réagi à son installation récente dans une minuscule salle avec une paillasse et des couvertures. Il s'était contenté de se pelotonner au chaud, profitant de ce répit sans penser au lendemain. Les repas, plus consistants, lui avaient fait le plus grand bien. Demeurait malgré tout l'angoisse de ne rien comprendre à ce qui lui arrivait.

Il avait été séparé des siens après l'attaque sur la colonne de pèlerins, tandis qu'ils longeaient la côte anatolienne. Ses parents n'avaient pas les moyens de payer le voyage en bateau et avaient rejoint un vaste groupe de toutes les nationalités peu après leur traversée du Bosphore. Cela n'avait pas effrayé les Turcmènes qui s'étaient abattus sur eux, capturant tous ceux qui n'opposaient pas de résistance, exécutant ceux qui cherchaient à se défendre. Depuis lors, Garin avait été mené de place en place, encordé à d'autres miséreux comme lui. Il avait tenté au début de discuter un peu entre prisonniers, mais les cris, les coups et les insultes l'en avaient rapidement dissuadé. Depuis, il ne faisait qu'attendre, sans intention, sans espoir, sans vie.

De temps en temps, il entendait les voix rudes des soldats à travers la porte, lançant des ordres, plaisantant ou s'invectivant. Des pas lourds ou traînants, parsemés de cliquetis et de tintements métalliques résonnaient parfois de l'autre côté de la solide huisserie. Régulièrement, un serviteur venait lui porter un broc, du pain plat et des légumes, tout en échangeant son seau d'aisance pour un propre. Mais personne ne le regardait jamais dans les yeux ni n'esquissait l'envie de parler avec lui.

Ce jour-là, des voix autoritaires se firent entendre&nbsp;: on s'approchait de sa porte. Les verrous jouèrent dans un claquement sec et le vaste panneau céda la place à une forte lumière noyant la galerie. Instinctivement, Garin recula, levant un bras en dérisoire protection. Dans l'encadrement se tenaient deux hommes discutant de façon animée dans une langue qu'il ne comprenait pas.

«&nbsp;Je peux le faire laver, si vous souhaitez avoir meilleure idée.&nbsp;»

Le premier était de petite taille, le faciès rond bien nourri barré d'une imposante moustache camouflant ses lèvres. Les yeux disparaissaient dans les replis graisseux de son visage avenant, dont la faconde naturelle était amplifiée par l'attitude toute servile qu'il adoptait, légèrement incliné. Sa tenue, vastes vêtements voilant sa physionomie pour n'en laisser percevoir que l'empâtement, attestait de son aisance tandis que l'impressionnant turban trahissait son goût pour les démonstrations d'opulence exubérante.

«&nbsp;Il sera bien temps de le récurer s'il convient. Est-ce là genre qui plaît à l'atabeg&nbsp;?&nbsp;»

Le second personnage était assez grand, dissimulé lui aussi sous de multiples couches de vêtements aux larges drapés dont les manches touchaient quasiment le sol. Son chef était orné d'un turban moins extravagant, agrémenté d'une belle broche, tandis que son visage apprêté attestait de l'homme coquet. Sa barbe noire, mangée de gris par endroit, était tenue courte et proprement délimitée par le rasoir. Ses yeux bruns plissés donnaient une allure riante à son regard, bien qu'il s'exprimât avec autorité. Sur un de ses gestes, plusieurs valets s'engouffrèrent et empoignèrent Garin, présentant l'enfant blond ainsi qu'une bête à la foire. Il l'examina en prenant son temps.

«&nbsp;Il a beau visage et yeux sombres, ainsi qu'il sied. Ses cheveux couleur de blé le rendent exotique. Ses mains délicates sauront verser le vin avec adresse j'espère.&nbsp;»

D'un geste souple, il fit signe aux hommes de reposer le gamin à terre puis tourna la tête vers le négociant.

«&nbsp;Faites donc établir le contrat, il sera propriété du calife ar-Rachid billah Abu Djafar Mansur, et offert à l'atabeg Zankī, s'il l'accepte.&nbsp;»

L'homme à l'imposante moustache hocha la tête, satisfait.

«&nbsp;Ce qui plaît beaucoup à l'atabeg, c'est de conserver la fraîcheur de ces poulains en les faisant eunuques. Votre geste sera d'autant plus apprécié.

--- Très bien, fais-le donc opérer par un des chirurgiens de la ville. Tu me feras porter la note.

--- C'est que les meilleurs praticiens n'aiment guère à faire cela...

--- Avec tous les Juifs en cette cité, il ne se trouve aucun médecin pouvant se charger de cela&nbsp;?

--- Ils sont toujours à chicaner sur leurs honoraires&nbsp;!

--- Le prix ne m'intéresse pas. Que cela soit fait, promptement, mais proprement. Je n'achète pas un ghulam de vingt dinars pour le gâcher aux mains d'un incompétent. Ne regarde pas à la dépense.&nbsp;»

Le gros négociant acquiesça avec enthousiasme, escomptant par avance la part qui lui reviendrait dans cette opération. Ce n'était pas tous les jours que le vizir du calife offrait un si bel esclave à l'homme fort des bords du Tigre, Zankī. Il se tourna vers le garçon qui les dévisageait, l'air hagard, et demanda, dans une langue approximative et gutturale.

«&nbsp;Quoi ton nom&nbsp;?&nbsp;»

Surpris autant par la question et la prononciation que par le fait qu'on s'adressa à lui, le gamin demeura muet, ce qui agaça un peu son interlocuteur qui insista, lâchant des regards gênés en direction du vizir. Ce ne fut qu'après un petit moment de stupeur qu'un son parvint à sortir de la gorge de l'enfant.

«&nbsp;Garin de Cachan...&nbsp;»

Le négociant fronça les sourcils, toujours mal à l'aise avec ces mots celtes[^2]. Puis il se tourna vers un de ses domestiques, porteur de documents.

«&nbsp;Note son nom&nbsp;: Yarankash&nbsp;»

### Édesse, grande salle de la citadelle, soir du youm al khemis 1 jadjab 539[^3] {#édesse-grande-salle-de-la-citadelle-soir-du-youm-al-khemis-1-jadjab-5393}

Des lampes et des braséros disposés en abondance combattaient les ténèbres et le froid. Les visiteurs venus de l'extérieur ne tardaient pas à transpirer sous leurs épais qab'a[^4] de laine doublée et de feutre. Un peu partout s'amoncelaient tissus et tenues d'apparat, pièces d'orfèvrerie, armes et céramiques décorées. Jouissant de ce chaos, un homme était assis sur un diwan agrémenté de coussin et de fourrures, sous un dais préservant la chaleur.

La barbe largement blanche malgré quelques poils noirs, il jouait machinalement avec une de ses longues tresses, les yeux perdus dans le vague. Son visage aux joues creusées était veiné de rides profondes sillonnant la peau parcheminée. Sous son sharbush[^5], dont le fronton scintillait à la lueur des lampes, ses paupières fatiguées abritaient un regard éteint, presque absent. Jetés pêle-mêle sur ses épaules, des étoffes de prix le transformaient en statue, image de dévotion devant laquelle les émirs qui entraient se prosternaient à tour de rôle. Il se nommait Imād ad-Dīn Zankī, atabeg de Mossoul et d'Alep et il venait de conquérir Édesse.

Il paraissait indifférent aux hommes qui se succédaient devant lui, ne gratifiant que rarement les plus méritants d'un grognement ou d'un hochement de tête à peine perceptible. Les prisonniers s'entassaient, les cadeaux s'amoncelaient sans qu'il ne semble porter le moindre intérêt à ce qui se passait. Lorsque le défilé commença à se tarir, il leva la main péniblement, chassa d'un geste les derniers prétendants. Il ne restait face à lui que ses plus fidèles émirs, installés en un approximatif demi-cercle.

Yarankash était debout, en retrait du podium, occupé à recharger d'encens et de braises les fauves de bronze entourant le prince. Il conservait aussi à portée coupes, bouteilles et pichets. L'atabeg n'aimait guère attendre lorsqu'il avait soif. Il arrosait de libations ses nombreuses victoires et recherchait dans le vin l'oubli de ses échecs. Ses adversaires décriaient à l'envi son appétence pour une boisson interdite, mais il s'en moquait. Il refusait simplement qu'on évoque la question devant lui. Aucun de ses émirs n'aurait eu le courage de parler des récipients autour de lui ou du verre émaillé qu'il tenait. Le vin était un sujet tabou dans la cour de Zankī, chacun s'efforçant de faire comme s'il n'existait pas.

Il tendit son gobelet et le jeune eunuque s'empressa d'y verser le rubicond breuvage, patientant ensuite, la cruche à la main, si le prince voulait être resservi. Zankī dégusta sa boisson, sirotant les poils de ses moustaches. Sans bouger la tête, il observait ses officiers. Il en appréciait les compétences et s'en méfiait tout autant, sachant pertinemment que c'était sûrement d'eux que viendrait la trahison au moment où il s'y attendrait le moins. Sa voix chuchotante mit fin à un long moment de silence gêné.

«&nbsp;Les Ifranjs[^6] sont-ils tous morts&nbsp;?

--- Ceux qui ont été découverts les armes à la main ont la tête au bout d'un piquet. Les autres attendent leur sort.&nbsp;»

L'atabeg approuva et prit encore une gorgée. Certains de ses émirs partageaient son goût pour le vin et ne se privaient pas de l'imiter en silence.

«&nbsp;Le religieux syriaque, Bar Šumna[^7], accordons-lui donc ce qu'il m'a demandé. Que vos hommes fassent en sorte que les biens réclamés par son culte lui soient rendus, en totalité.

--- Le vieil imam arménien[^8] sera jaloux d'un tel privilège...

--- Il n'a qu'à se montrer plus reconnaissant. Je leur ai déjà rendu leurs églises&nbsp;! *Un chien reconnaissant vaut mieux qu'un homme ingrat.*&nbsp;»

Le silence se fit tandis que des serviteurs inquiets apportaient un plat de mouton et de riz, accompagné de pain. Les odeurs épicées se mêlaient aux relents entêtants des fumées. Les émirs se rapprochèrent et s'installèrent confortablement. À partir de là, l'ambiance fut plus décontractée. Même l'atabeg souriait lorsque le vieil Zayn ad-Dīn racontait une de ses anecdotes. Soldat émérite et général au sens tactique affiné, il conservait malgré les années un caractère espiègle qui en faisait un compagnon apprécié. Peu enclin à l'excès, il s'animait quand il narrait les facéties des guerriers qu'il avait eus sous son commandement, ou qu'il avait affrontés. Lorsqu'il acheva une de ses fameuses histoires, Zankī le remercia et fit un geste à l'intention de Yarankash. Voyant que celui-ci avait les yeux au sol, il en fut agacé et lui lança de la nourriture, un os à demi rongé.

Le jeune garçon sursauta, le cœur battant soudainement à tout rompre. Il savait que la correction viendrait tôt ou tard. L'atabeg détestait que ses esclaves ne se montrent pas empressés à lui obéir, il estimait que cela n'inspirait pas le respect à ses hommes. Chaque manquement était donc impitoyablement suivi de coups, de punitions et de vexations dont certaines entraînaient parfois la mort. Yarankash avait vu ainsi plusieurs de ses camarades finir mutilés ou exécutés pour avoir déplu à leur maître. Zankī lui lança quelques insultes et l'enjoignit à aller quérir ce qu'il lui avait fait mettre de côté un peu plus tôt. Contrit, Yarankash courut chercher la riche robe brodée de tiraz[^9] prestigieux. Encore tremblant, sous le regard d'aigle de l'atabeg, il s'inclina en la déposant sur les genoux du vieil homme tandis que Zankī prenait la parole de façon plus officielle.

«&nbsp;Que tous soient témoins ici que je te nomme, Abū al-Ḥassan Alī ben Begtegin[^10], isfahsalār de la cité d'Urfa[^11]. Tu y auras pour tâche de tenir la forteresse et de m'y représenter. Pour le reste, laisse donc les Syriaques et les Arméniens se débrouiller entre eux, tant qu'ils paient ce qu'ils doivent et respectent mon autorité. N'hésite pas à crucifier les Celtes qui auraient la mauvaise idée de traîner à l'entour.&nbsp;»

Il sourit en voyant le qab'a élimée du vieux soldat, dont la piété était connue, qui tranchait avec le magnifique vêtement qui venait de lui être remis.

«&nbsp;Tu n'auras pas à porter cette tenue quand tu mèneras les hommes. Comment te reconnaîtraient-ils si tu portais beau comme un bureaucrate baghdadi&nbsp;?&nbsp;»

Le général sourit et se leva avant de s'incliner cérémonieusement devant son prince. En le voyant faire, Yarankash se dit qu'il aurait aimé être offert à l'émir en même temps que la robe. Il était épuisé de vivre sans cesse dans la crainte des caprices, des exigences et des sautes d'humeur de son maître.

### Qal'at Ja'bar, camp de siège, nuit du youm al sebt 5 rabi' al-thani 541[^12] {#qalat-jabar-camp-de-siège-nuit-du-youm-al-sebt-5-rabi-al-thani-54112}

Les cordages de tension de la tente répondaient en grinçant au murmure des pins parasols. Les lampes suspendues à l'armature se balançaient mollement, projetant des ombres mouvantes sur les murs de toile. Se déplaçant sans bruit sur l'épaisse litière de tapis, de nattes et de fourrures, Yarankash s'employait à ranger les reliefs du repas et de la soirée. Zankī avait célébré avec ses émirs l'avancée du siège, dont il ne doutait pas qu'il serait bientôt terminé. Il s'enorgueillissait d'avoir floué le seigneur du lieu en le délestant d'une coquette somme par la ruse. Il avait proposé de partir moyennant le versement d'un fort montant. L'argent remis, il avait décidé de passer son agacement sur les messagers et avait empoché le tribut sans desserrer un tant soit peu son étreinte sur la forteresse.

Cela faisait plusieurs semaines qu'ils étaient installés sans vraiment combattre. Après avoir pillé l'opulente bourgade et saisi les biens de ceux qui vivaient là, Zankī s'était contenté d'instaurer un blocus total. Il profitait de la situation privilégiée du lieu dans la vallée de l'Euphrate pour percevoir les péages marchands qui enrichissaient jusque là les seigneurs uqaylides. Comme à son habitude lorsqu'il s'ennuyait, il avait tendance à boire copieusement chaque soir, sombrant dans une ivresse colérique et capricieuse qui éloignait de lui tous les hommes prudents.

Yarankash rassemblait les bouteilles et pichets aux abords de la couche de l'atabeg. Celui-ci était assoupi, affalé sur le magnifique matelas, noyé sous les coussins et de luxueuses couvertures. Le jeune garçon huma le vin dans une des céramiques et en apprécia les senteurs. Zankī n'aimait rien tant que les boissons les plus capiteuses et son esclave en avait pris le goût. Il en avala les restes, découvrant un breuvage charnu, rond et plein de finesse qui lui réjouit le palais. Après un rapide regard vers son maître, perdu dans les vapeurs alcoolisées, il se mit en quête du pichet pour s'en verser une coupe.

Il en était à son second verre, agrémenté de böreks[^13] au fromage lorsqu'une voix le fit sursauter.

«&nbsp;Mon vin est assez bon pour toi, sale chien&nbsp;?&nbsp;»

Zankī avait la tête relevée, les yeux entrouverts, et le fixait avec colère. Un fin filet de salive glissait des lèvres tandis que l'atabeg s'emportait, la voix rauque.

«&nbsp;Je vais te faire fouetter pour ça, maudit traître.&nbsp;»

Il se leva et, titubant, empoigna un couteau de service. Il contourna les tables basses du mezzé[^14] en s'approchant de Yarankash. Pétrifié, le jeune eunuque attendait, la coupe à la main. Zankī brandit la petite lame en souriant de façon sadique.

«&nbsp;Je vais t'arracher la langue, sale voleur&nbsp;!&nbsp;»

Il lança la main pour empoigner Yarankash, mais celui-ci recula instinctivement. L'atabeg, emporté par son élan et déséquilibré par son ivresse se prit les jambes dans les plateaux et les coupelles du repas et s'étala de tout son long sur les coussins des invités en grognant de stupeur. Plusieurs esclaves s'approchèrent alors discrètement, inquiets et curieux à la fois. Ils ne tenaient pas à voir l'ire de Zankī s'exercer à leur encontre.

Zankī ne bougeait plus, marmonnant, la tête plongée dans les étoffes. Yarankash se pencha, indécis, espérant arriver à installer son maître dans une position plus confortable sans recevoir un mauvais coup. Lorsqu'il le retourna, il eut un mouvement de recul&nbsp;: le durrâ'a[^15] de soie fine s'ornait d'une large tache sombre au niveau du ventre. L'atabeg gémissait doucement.

«&nbsp;Tu m'as tué, sale petit porc. Je vais t'écorcher, te crucifier...&nbsp;»

Il s'était empalé sur son couteau en tombant. Désemparé, Yarankash installa son maître comme il le put sur les coussins. Voyant que quelque chose n'était pas normal, les autres serviteurs s'étaient approchés et contemplaient la scène, abasourdis. Le jeune homme, affolé, ne savait que faire. Il était persuadé que la punition serait terrible, le lendemain. Jamais le vieux prince ne le laisserait vivant.

«&nbsp;Il faut appeler le chirurgien sans tarder, lança un des plus âgés.

--- Il va être d'une humeur massacrante, l'arrêta un autre.

--- On ne va pas le laisser mourir, quand même&nbsp;!&nbsp;»

Écoutant à peine ce qui se disait, sans un mot, Yarankash ramassa le petit couteau qui avait poignardé l'homme fort de la région. Un simple outil de service, à peine tranchant. Il contempla l'auréole grandissante, entendit les imprécations de plus en plus faibles. Il regarda ses compagnons, percevant vaguement leur discussion angoissée sans rien y comprendre vraiment. Puis il plongea la lame dans le ventre de l'atabeg, obtenant un râle plus soutenu.

Les autres autour de lui en demeurèrent sidérés. Aucun n'osa plus parler, médusés qu'ils étaient tous de ce geste sacrilège. Il posa ensuite le manche dans la main de son voisin et l'invita, d'un mouvement de la tête, à faire comme lui. En quelques instants, chacun avait porté au moins un coup au puissant seigneur de Mossoul et d'Alep qui se tut en un ultime anathème.

### Damas, geôles de la citadelle, matin du youm al sebt 29 jadjab 541[^16] {#damas-geôles-de-la-citadelle-matin-du-youm-al-sebt-29-jadjab-54116}

La silhouette élancée qui avançait à la suite du soldat porteur des clefs semblait incongrue dans cette coursive poussiéreuse et sombre. La qualité des étoffes y disputait avec l'ampleur faste de la coupe et la richesse exubérante des bandes à tiraz. L'homme, d'une cinquantaine d'année, paraissait pourtant à l'aise dans cet environnement austère. Une barbe soignée ornait le bas d'un visage fin sillonné de rides nées du soleil autant que de l'âge. Les cheveux très courts ne dépassaient que peu de l'imposant turban, porté haut et agrémenté d'un onéreux bijou. Nul ne pouvait douter en voyant Usamah ibn Munqidh qu'il avait affaire à un personnage de premier plan.

Ils quittèrent la zone des cellules pour rejoindre des bâtiments moins lugubres. Abandonnant son escorte, Usamah parvint dans un petit cabinet dominant les cours d'exercice. Là, emmitouflé dans des couvertures, le seigneur de la ville, Mu'īn ad-dīn Anur patientait. Il avait officiellement appelé à des manœuvres malgré le froid et le ciel peu clément, et les surveillait depuis ce poste de vigie. En fait il comptait surtout échanger discrètement avec l'habile diplomate et conseiller qui venait de le rejoindre. Il l'invita à s'asseoir face à lui sur un modeste tabouret. Usamah mit un peu de temps avant de s'installer confortablement, organisant les plis nombreux de sa tenue complexe.

«&nbsp;Je n'ai aucun doute que celui qui se vantait d'avoir occis l'atabeg soit un menteur. En se faisant passer pour Yarankash, il espère sûrement récompense.

--- La peste soit de ce fou&nbsp;! tonna Anur. Je ne solde pas les assassins, quand bien même ils me débarrassent d'un dangereux adversaire.&nbsp;»

Il demeura un moment à ruminer, les yeux errant par-delà les décors ajourés sur les corps des hommes à l'exercice. Il s'accorda un sourire de satisfaction en voyant quelques beaux échanges de coups. Puis il revint vers son conseiller, resté coi, perdu dans ses propres pensées, certainement occupé à tramer un stratagème ou à composer une poésie.

«&nbsp;La situation est déjà fort complexe&nbsp;! Maintenant qu'il a hurlé partout son nom, je ne peux l'ignorer...

 «&nbsp;Qui a le pouvoir doit plus qu'un autre pardonner".

--- Ton père, homme sage s'il en fut, avait renoncé à régner pour acquérir semblable vertu.

--- *Quand un chien vous aide à passer le fleuve, ne demandez pas s'il a la gale* ajoutait souvent Jum'a[^17]&nbsp;» précisa ironiquement Usamah.

La mort du puissant prince du nord bouleversait complètement les forces en présence et, comme à chaque disparition d'un personnage important, le chaos qui allait s'ensuivre offrait toutes les perspectives, bonnes ou mauvaises. Les compétences des dirigeants étaient avant tout sollicitées pour profiter à leur avantage de ces opportunités. La présence d'un homme qui se faisait passer pour l'assassin dans la cité de Damas n'était pas un sujet à prendre à la légère. Anur soupira.

«&nbsp;Je ne sais si les fils de l'atabeg s'accorderont, j'en ai grand doute. Mais qui que soit le gagnant, il lui faudra du temps. Il va falloir choisir avec grand soin notre champion.

--- Le jeune Mahmud[^18] est un soldat de valeur, je peux en attester. Il est bien placé à Alep, envoyons-y ce Yarankash en gage.

--- Tu as dit toi-même que c'était un imposteur&nbsp;!

--- Si nous faisons savoir à tous que c'est l'assassin réfugié en nos murs que nous renvoyons à ses maîtres pour recevoir son châtiment, qui osera nous traiter de menteurs&nbsp;? Que ce soit vraiment celui qui a occis l'atabeg importe peu, tant que la foule le croit.

--- Et si le vrai Yarankash se manifestait&nbsp;?

--- Après le sort réservé à celui qui vient de se déclarer chez nous, je n'encrois nullement qu'il y aura pleinté de candidats. Il est sûrement déjà en sauveté chez les Ifranjs. J'essaierai d'en apprendre plus à ce sujet lors de mon prochain voyage à al-Quds[^19].&nbsp;»

Une moue déforma les traits fatigués du seigneur de Damas, comme un acquiescement à contrecœur. La mort de Zankī lui offrait l'opportunité d'affirmer son pouvoir sans s'opposer ouvertement au royaume latin de Jérusalem. Pour autant, il ne souhaitait pas l'effondrement d'Alep, sachant pertinemment que Damas serait la cité suivante dans l'ordre des conquêtes latines.

«&nbsp;Tu dis que le jeune fils de Zankī est un vaillant soldat&nbsp;? Peut-être devrais-je le soutenir par une alliance formelle&nbsp;? Une de mes filles est justement bonne à marier. Les messagers acheminant le prisonnier pourraient délivrer un message d'avenir, de rapprochement entre nos cités.&nbsp;»

Usamah ibn Munqidh marqua son enthousiasme par un sourire appuyé. Il avait longtemps combattu aux côtés de l'atabeg Zankī et toute sa jeunesse s'était déroulée sur les rives de l'Oronte, à affronter les potentats locaux, francs et musulmans, pour le compte de sa famille à Shayzar. Ce qu'il craignait plus encore que la chute d'Alep en elle-même, c'était l'idée que le seigneur du défunt comté d'Édesse, Joscelin, puisse s'y installer après son échec récent à reprendre sa capitale. Une dizaine d'années plus tôt, une vaste campagne initiée par le basileus byzantin avait envisagé la conquête de la cité alépine, projet heureusement sabordé par les dissensions entre les francs d'Antioche et d'Édesse.

### Naplouse, cour des bourgeois, fin de matinée du jeudi 25 août 1149

Le vicomte sortit le premier, ventre en avant, suivi de peu par son appendice nasal. Les mains dans le dos, il avait comme à son habitude un œil fermé et l'autre plissé, résultat d'une mauvaise vision qui l'incitait à fixer ses interlocuteurs pour tenter de les reconnaître. On moquait parfois sa tendance à la bonhomie, sans toutefois manquer de respect à un dirigeant modéré. Il accomplissait son devoir avec sérieux et régularité. On vantait même sa relative honnêteté qui poussait à ne jamais lui offrir de pot-de-vin d'un montant mesquin. Derrière lui venait la cohorte des jurés de la Cour des Bourgeois, florilège de tenues dispendieuses ou graves, assortie de visages aussi austères que cérémonieux. Les suivaient quelques plaignants et un prévenu, encadré de deux sergents. Le jeune homme regardait ses pieds, ne laissant voir que sa chevelure blonde. Il était vêtu misérablement, chaussé de savates élimés, la cotte serrée d'un cordon de ficelle.

Il fut mené jusqu'à l'atelier du forgeron le plus proche, où attendait l'assemblée. Le vicomte Ulric monta sur un petit escabeau installé là à son attention et déclama d'une voix forte à l'intention de tous.

«&nbsp;Ainsi qu'il vient de l'être indiqué en l'audience plénière de la Cour, le ci-devant... Garin de Cachan, sergent reconnu félon à son maître Bertaud le Normand, aura le poing percé d'un fer porté au rouge en marque d'infamie.&nbsp;»

Voyant que le forgeron allait prendre le poinçon qu'il avait mis à chauffer, Ulric fit signe aux deux soldats de tenir la main du jeune homme sur le billot. Sur le devant du cercle qui assistait à la peine se trouvait le plaignant, gros homme aux chairs molles, un exploitant agricole chicaneur qui fatiguait la Cour avec ses requêtes incessantes. Il était pour une fois parfaitement dans son droit, ayant surpris un de ses domestiques à s'enfuir nuitamment. À voir la vêture du pauvre garçon, Ulric comprenait fort bien son envie de liberté. Mauvais maîtres font mauvais valets disait-on. C'était on ne plus vrai de Bertaud, que personne n'appelait le Normand, pour peu qu'il fût absent, mais «&nbsp;Le Sueur&nbsp;», cette denrée qu'il aimait à tirer de ses gens, mais dont il évitait la moindre production à son imposante carcasse.

Le jeune homme ne se révolta pas, chancelant et transpirant abondamment tandis que l'exécuteur s'avançait. Ulric tenait à ce que ça soit vite et bien fait. Quand la chair grésilla, que le burin déchira la paumes, Garin hurla et des larmes lui vinrent aux yeux. Désireux de ne pas handicaper un travailleur qui n'avait que ses bras pour se nourrir, le forgeron avait bien pris garde à frapper entre les os pour n'en briser aucun. À la requête du vicomte, un chirurgien s'empressa de bander la blessure. Les badauds commencèrent alors à s'éloigner, sans que cela ne perturbe Ulric, qui reprit la parole.

«&nbsp;Tu continueras à servir Bertaud le Normand jusqu'à la Toussaint ainsi que tu l'avais juré, sans percevoir plus de gages en châtiment, comme l'a décidé la Cour. Ce poing percé que tu arboreras désormais sera pour toi quotidien rappel de ton manquement, de cette main qui a trahi le maître qu'elle avait juré de servir. Puisse Dieu avoir pitié de toi.&nbsp;»

La Cour se dispersa alors, chacun retournant à ses activités quotidiennes en cette chaude journée. Le chirurgien laça le bandage et invita Garin à en changer les linges souvent. Puis il s'en fut à son tour, laissant le jeune homme assis dans la poussière, au milieu de la rue. Face à lui, Bertaud, les mains sur les hanches, affichait un sourire repu. ❧

### Notes

On ne sait que peu de choses du destin du jeune eunuque qui aurait poignardé Zankī. Certaines sources indiquaient que ce dernier avait tendance à s'emporter contre ses serviteurs, voire à se montrer cruel et il n'est pas impossible que ce ne soit rien de plus qu'une querelle domestique, thèse que j'ai retenue ici. Nul besoin alors d'envisager un complot ou une commande d'un adversaire de l'atabeg. Son méfait réalisé, l'eunuque fut signalé à Damas, où il n'eut que peu le temps de se vanter de son geste qu'il était remis aux dirigeants alépins avant d'être envoyé à Mossoul où il aurait été exécuté. Peut-être sans le savoir, il avait contribué à redessiner l'équilibre des forces en présence, offrant un sursis aux autorités damascènes face à l'appétit des atabegs turcs d'Alep. Ce qui en fit une cible pour la Croisade qui se déroula l'année suivante.

Ce texte est aussi l'occasion de dépeindre quelques-uns des grands personnages du côté musulman. L'historiographie est malheureusement moins riche que pour les Latins ou les Byzantins. De nombreux officiers d'importance, quoique de second rang, tels que Zayn al-Din, mériteraient des études plus documentées, étant donné leur rôle dans l'échiquier politique d'alors. Bien peu ont bénéficié d'une attention comparable à celle accordée à Usamah ibn Munqidh, dont les savoureux écrits constituent une source essentielle sur la période. Signalons enfin que Zankī lui-même n'a encore jamais été l'objet d'un travail aussi scrupuleux que celui réalisé sur le règne de son fils Nūr ad-Dīn par Nikita Élisséef.

### Références

Cobb Paul M., *Usama ibn Munqidh. The book of Contemplation. Islam and the Crusades*, Londres&nbsp;: Penguin Books, 2008.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas&nbsp;: Institut Français de Damas, 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I, *Economic Foundations*, Berkeley et Los Angeles&nbsp;: University of California Press, 1999.

Pour une poignée de dinars
--------------------------

### Damas, citadelle, début d'après-midi du jeudi 10 janvier 1157

La pièce dans laquelle Bernard Vacher venait d'entrer était de petite taille, austère, sans aucun décor et sentait l'humidité. On était bien loin du faste des salles de réception auxquelles les dirigeants bourides de la cité l'avaient jusqu'alors habitué. Leur temps avait passé, leur étoile s'était éteinte et c'était désormais celle de Nūr ad-Dīn qui brillait au firmament. Ce grand guerrier n'accordait pas la même importance au maintien de relations apaisées avec le trône de Jérusalem, qu'il voyait comme une verrue à éliminer. Encore plus farouchement opposé aux Latins que son père Zengī[^20], il le faisait comprendre de mille façons, entre autres en multipliant les vexations chaque fois que l'occasion lui en était donnée.

Le vieux chevalier s'efforçait de faire bonne figure. Il avait été chargé, une fois de plus, de récupérer le tribut versé en conséquence de la trêve. Il avait été accueilli par Muhammad ben Ğafar, un des nouveaux chambellans représentant le pouvoir damascène. Cela aussi constituait une première. Il avait l'habitude de traiter avec les princes ou leurs vizirs. Il se faisait l'impression d'être un lépreux et pressentait que les années à venir seraient rudes dans le Bilad al-Sham. Au moins cette année serait calme grâce à l'accord arraché à la fin de l'automne dernier.

Le petit fonctionnaire face à lui agitait les manches de son ample vêtement en se penchant vers les deux coffres cerclés de fer ouverts devant eux. La barbe poivre et sel taillée en pointe, le turban semblait à peine effleurer le haut de son crâne et paraissait prêt à verser à chaque fois qu'il inclinait la tête. Sa silhouette mince, ses mains délicates et ses épaules fines attestaient de son statut d'homme de robe et non d'un rôle militaire. Encore un affront envers le vieux soldat qu'était Bernard Vacher. On ne l'estimait pas suffisamment dangereux pour devoir lui opposer un émir de premier plan, comme Izz al-Din, nouveau gouverneur de la citadelle. Ce dernier avait néanmoins organisé une escorte pour accompagner les émissaires jusqu'aux confins de leur territoire, garantissant le bon acheminement de l'imposante somme.

«&nbsp;Tout y est, les huit mille dinars de Tyr sont là.&nbsp;»

Le fonctionnaire s'empara d'une bourse scellée d'un ruban cacheté à la cire.

«&nbsp;Chacune d'entre elles contient vingt dinars. Il y en a quatre cents, cachetées par la maison des monnaies de votre cité. Voulez-vous les compter&nbsp;? Peser les pièces&nbsp;?&nbsp;»

Une balance et des poids attendaient sur la table, au cas où le chevalier n'aurait pas confiance dans le versement. Bernard Vacher secoua la tête, se contentant de prendre la bourse et d'en rompre le cordon pour faire glisser les pièces d'or dans sa large paume.

«&nbsp;Il y a environ sept ratls et demi dans chaque huche[^21]. Nous avons préféré scinder en deux, pour plus d'aisance à mettre cela sur un animal.&nbsp;»

Muhammad ben Ğafar accompagnait chaque phrase d'un sourire qui se voulait engageant, mais qui finissait par sonner faux à force d'amabilité forcée.

«&nbsp;Vous avez bien fait&nbsp;» approuva le chevalier.

Il fit tinter un des dinars sur la plaque de marbre posée à côté de la balance, l'air de rien. Il faisait mine de jouer avec les pièces, mais tendait l'oreille pour s'assurer que le son lui convenait. Il n'avait jusqu'à présent jamais eu de mauvaises surprises de ce type. Mais avec une nouvelle administration, il fallait marquer son territoire, de façon discrète, mais compréhensible. Tant que les armes resteraient au fourreau, ce serait semblable jeu du chat et de la souris. C'était là, en amont, qu'une partie de la guerre se déroulait et, parfois, se gagnait.

«&nbsp;Une troupe vous escortera jusqu'à l'Arbre de la mesure[^22], aux fins de s'assurer que nul brigand ne soit tenté par cette fortune, précisa le chambellan.

--- Une partie de mes hommes nous y attendent. Comme c'est l'usage à chaque versement&nbsp;» ajouta Bernard, perfidement, un sourire fallacieux sur les lèvres.

Le chambellan lui répondit par une crispation des joues qui ne ressemblait guère à une manifestation de joie, bien qu' il s'efforçât péniblement de faire bonne figure. Après tout, il n'était qu'un sous-fifre et n'avait peut-être pas très envie de se trouver dans une salle close, avec un tas d'or et un vieil ambassadeur réputé pour son talent à la guerre. Bernard appréciait que cela soit connu chez ses adversaires. La valeur militaire était célébrée parmi les dirigeants turcs et une forme de respect pouvait en naître. Cela lui serait certainement utile dans les négociations à venir, qui s'annonçaient assez houleuses.

Il remit les pièces dans le sachet qu'il déposa avec les autres. Muhammad referma les rabats et verrouilla soigneusement les serrures, puis il tendit les deux clefs au chevalier, inclinant le buste. Enfin, il alla ouvrir la porte et frappa dans ses mains pour signifier le départ du tribut. Deux soldats latins, l'air mal à l'aise, accompagnés de deux miliciens damascènes pénétrèrent dans la salle et soulevèrent les coffres deux à deux.

Bernard soupira, la paix avait été encore plus difficile à obtenir qu'habituellement. Les tremblements de terre récents dans le nord aiguisaient les appétits des barons croisés, poussant à l'exploitation des opportunités que représentaient des murailles en partie abattues. Au moins cette région orientale serait tranquille pour quelques mois. Il prit la suite du petit convoi qui se dirigeait vers la cour où les attendaient leurs montures. Ils n'étaient pas restés une demi-journée dans la cité damascène.

### Jérusalem, palais royal, matinée du lundi 14 janvier 1157

Eudes tira le banc qu'il partageait avec Baset plus près de la table gravée de cases. Ils étaient installés dans une annexe de la vaste salle des monnaies, petite pièce carrée éclairée d'une haute fenêtre close de verre. Des barreaux épais en condamnaient l'accès. La porte elle-même était plaquée d'un décor métallique en fer forgé dont la fonction ornementale ne dissimulait que peu l'aspect massif.

Face à eux se trouvait Gaston, un sergent vétéran à qui on attribuait souvent les missions de confiance. Il était là pour vérifier qu'ils n'escamotaient rien tandis qu'ils triaient les dinars. Derrière lui, un clerc, Andri, notait ce qui était empilé dans les carrés de la table et les déplaçait une fois cela fait. Enfin, Brun, un des hommes de la Secrète[^23], inscrivait le tout sur ses tablettes, nons sans contrôler le travail de chacun dans la pièce. Ce n'était pas des petites monnaies, billons de cuivre ou mailles pour payer un commerçant qu'ils allaient compter. C'était le versement damascène, près de huit mille disques d'or qui devaient passer entre leurs mains. C'était à la fois une marque de confiance et un motif de nervosité.

Si jamais il manquait ne serait-ce qu'un dinar, la tension serait à son comble. Le sénéchal n'était pas le pire des maîtres, mais il ne pouvait accepter que des larrons en prennent à leur aise avec le trésor royal. Surtout pour un aussi stratégique versement. Que des œufs, une poule ou quelques mesures de grain ne finissent pas dans le bon cellier n'était pas de grande importance et considéré dans la plupart des maisons comme faisant partie des pratiques normales. Mais le tribut de Damas devait absolument correspondre à ce qui était attendu. Quand politique et finances convergeaient, il ne faisait pas bon être le grain de sable inopportun.

Le clerc principal, Brun, fit jouer les deux serrures et ouvrit les coffres. Des dizaines de sachets crème les remplissaient, chacun fermé d'un cordon où se voyait le sceau de l'atelier tyrien, un château surplombant un fin chemin de terre entouré d'eau. Eudes et Baset prélevèrent une bourse, en coupèrent le lien avec des forces puis le présentèrent à Andri, qui en vérifiait la conformité. Ils comptaient ensuite les vingt pièces, en rangées de trois, ce qui faisait une valeur de quatre besants par ligne. Le premier, Baset prit un des dinars et le posa dans le petit plateau d'une balance dédiée au contrôle du poids. Ils avaient aussi une plaque de marbre pour y faire sonner les monnaies, ce qui donnait une indication sur leur aloi.

Eudes comprit très vite que Baset n'était pas à l'aise avec le trébuchet. Gaston lui-même fixait le sergent, qui paraissait hésiter sur l'équilibrage du fléau. Le malaise grandit encore lorsque Baset approcha son oreille du plateau pour y faire tinter la pièce. Il semblait indécis, s'attirant finalement une remarque de Gaston.

«&nbsp;Quelque souci, Baset&nbsp;?

--- Je ne sais... Cette balance est-elle juste&nbsp;?

--- Elle vient de l'atelier des monnaies, un peu qu'elle est juste&nbsp;!&nbsp;»

Baset se mordit l'intérieur de la joue. Il n'aimait pas quand son quotidien était perturbé. Ses mains se mirent à trembler. Il prit une autre pièce, recommença les mêmes opérations avec plus de fébrilité encore. Eudes s'était interrompu et le regardait faire. Le malaise grandissait dans le petit groupe. Brun s'en émut et quitta son pupitre pour s'approcher, les yeux plissés et le nez froncé.

«&nbsp;Qu'est-ce donc, sergent&nbsp;?&nbsp;»

Baset lança un regard inquiet à Eudes, qu'il connaissait le mieux, puis à Gaston. Il n'avait guère envie de répondre ce que tous avaient compris. Il secoua la tête, fit les mêmes tests avec un autre dinar, en prenant son temps. Chacun retenait son souffle.

«&nbsp;Hé bien, alors&nbsp;? s'impatienta Brun.

--- Ni sonnante, ni trébuchante, maître Brun.

--- De quoi, de quoi&nbsp;? s'emporta le clerc habituellement impassible.

--- Ce ne sont pas monnaies de bon aloi, il y a certeté.&nbsp;»

Brun fronça les sourcils, se passa les mains sur le front.

«&nbsp;Vérifiez tout, au plus tôt&nbsp;! S'il en est ainsi de chacune de ces monnaies...&nbsp;»

Il ne termina pas sa phrase, catastrophé. Tout le monde ici imaginait bien que les conséquences seraient terribles. Ce n'était pas un petit fraudeur qui trichait sur son cens[^24] ou mentait au péage. Il s'agissait d'un versement en garantie de la paix. Eudes et Baset plongèrent du nez sur leur tâche, espérant, contre toute attente, que cela ne concernerait que de rares pièces.

### Jérusalem, palais royal, après-midi du mercredi 16 janvier 1157

Le roi Baudoin semblait avoir vieilli de plusieurs années en quelques jours. Les traits tirés, emmitouflé dans un épais manteau de laine écarlate fourrée, il était accoudé à une fenêtre, négligeant l'air froid venu du dehors. Il regardait un chat à l'affût des pigeons qui se dandinaient au faîtage du toit d'une galerie. Le ciel couleur de perle n'offrait guère de chaleur et il n'était pas exclu que de la neige blanchisse le paysage dans la nuit.

Il se retourna face aux trois hommes assis sur des chaises curules au pied de son lit. Ce n'était pas un réel conseil restreint, mais il avait besoin de formuler ses idées avant de les présenter à la Haute Cour. Depuis la prise d'Ascalon, quelques années plus tôt, le royaume ne s'était guère étendu et le trésor peinait à assumer toutes les dépenses. En outre, Baudoin était jeune, appréciait l'action et se languissait de retrouver la selle et la lance, de mener ses conrois à la bataille.

Devant lui se trouvaient ceux qu'il estimait le plus dans son hôtel, jusqu'à présent. Parmi ceux-ci, Bernard Vacher était en passe de perdre ce statut privilégié. Le porte-bannière royal avait gravement fauté, avec cette histoire de tribut en fausse monnaie. Son aspect piteux, ramassé et ses épaules affaissées marquaient son accablement. À ses côtés, en tenue d'une grande sobriété, le visage las, mais le regard affûté, le connétable Onfroy de Toron finissait de lire une tablette. Un rapport urgent dont il dévoilerait le contenu le cas échéant. Enfin, le sénéchal, Jean d'Acre, un homme fatigué, mais avisé, dont la tête dodelinait un peu en cette heure où il faisait habituellement la sieste.

«&nbsp;Barons, si je vous ai mandés, c'est pour échanger avec vous sur le sujet de ce tribut mensonger qui nous a été remis. J'ai besoin d'avoir vos avis avant d'arrêter ma décision et de la soumettre au Haut Conseil.&nbsp;»

Il s'approcha d'eux, s'installa sur un large siège recouvert de coussins, rajusta son manteau sur ses épaules. Il caressa sa longue barbe blonde, taillée à la mode, tout en parlant.

«&nbsp;Nous n'avons pas guerroyé encontre Damas depuis long temps. Le nouveau maître de la place a fort à faire dans le nord, les derniers messages en provenance de la princée d'Antioche en attestent.&nbsp;»

Il s'accorda un rictus en guise de sourire désabusé.

«&nbsp;Nous pouvons au moins accorder au bailli de la princée[^25] qu'il s'active fort contre nos ennemis. Cela ne laisse guère le temps au roi d'Alep de venir en nos terres. Pourtant, il nous fait injure de bien perfide façon, avec ses jeux de monnaies déloyales. Ce n'est pas là digne façon de s'opposer.

--- C'est ce qui me questionne, en cette affaire, mon roi, intervint Toron. Quel serait l'intérêt de Nūr ad-Dīn de nous provoquer de la sorte&nbsp;? Les Damascènes ont toujours préféré acheter la paix. Il les trahit autant qu'il nous ment, en cette affaire.

--- Peut-être espérait-il que nous n'y verrions que du feu&nbsp;! rétorqua le sénéchal. Je reconnais bien là les manières du fils de Sanguin[^26]. Il n'y a nul moyen de s'entendre avec eux, ce sont des sauvages qui vivent sous la tente, qui tètent les chèvres, quand ce ne sont pas les futailles&nbsp;! Tant que la cité avait un prince comme Anur à sa tête, nous pouvions parlementer. Mais là, il nous faut conquérir le lieu, au plus vite&nbsp;!&nbsp;»

Le connétable grimaça. Il avait beaucoup d'estime pour Jean d'Acre, mais il voyait le vieil administrateur se radicaliser avec les ans. Alors qu'il avait toujours été homme de mesure et de compromis, il se découvrait irascible et fougueux avec le temps. Il conservait malgré tout le talent de souvent pointer le bon argument.

«&nbsp;Je suis assez d'accord avec vous, sénéchal, approuva le jeune roi. Le fait demeure que nous ne saurions emporter la place sans quelque baron croisé à nos côtés.

--- Ce fut leçon bien chèrement apprise que, parfois même, cela ne suffit pas, enchérit Bernard Vacher.

--- Il n'y aurait pas eu insidieuses dissensions et un vrai chef à cet assaut, nous aurions emporté la place, Bernard. Cela au moins fut leçon, pour moi.&nbsp;»

La remarque avait été lancée avec morgue, incitant Vacher à rentrer la tête dans les épaules. Mal à l'aise avec le traitement infligé à un chevalier qu'il estimait, Onfroy intervint.

«&nbsp;Auriez-vous quelque courrier de votre frère[^27], mon roi&nbsp;?

--- Exactement&nbsp;! s'enthousiasma soudain Baudoin. Ma sœur elle-même envisage de venir, m'a-t-elle écrit. Ils seraient là dès avant l'été.&nbsp;»

Le sénéchal s'agitait sur sa chaise. Il était chargé des finances du royaume, entre autres tâches, et il savait que la situation n'était pas au beau fixe. Le comte de Flandre allait venir avec des hommes, certes. Mais il faudrait les nourrir et pouvoir lui adjoindre une armée. Faute de quoi la campagne risquait de tourner court. Il n'eut pas le temps d'exprimer ses réserves quant à une expédition estivale que Baudoin leva la main, prévenant à son objection.

«&nbsp;Seulement, il nous faut reconstituer le trésor. Les récoltes n'ont pas été tant bonnes et les péages assez moyens, au final, de ce que j'en ai vu. J'ai donc une idée pour régler ce souci, tout en infligeant un camouflet à ce déshonnête turc.&nbsp;»

Il joignit les doigts en un geste un peu cérémonial, savourant à l'avance la subtilité de son idée.

«&nbsp;Chaque fin d'hiver, il est de coutume que de nombreux propriétaires de troupeaux les confient à valets et gardiens pour qu'ils profitent de la manne des premières pâtures, sur le Golan.&nbsp;»

Bernard Vacher comprit là où le roi voulait en venir et ouvrit des yeux ronds, effaré. Échaudé par la précédente rebuffade, il n'osa pas prendre la parole, bien que tout en lui se révoltât contre l'idée avancée.

«&nbsp;Il n'y a là qu'une poignée de sergents, des nomades et des servants. Un large butin s'offre à nous, pour laver l'outrage&nbsp;!&nbsp;»

Le roi se tut, les yeux brillants d'excitation, attendant les commentaires, qu'il espérait élogieux, à son idée. Devant sa hardiesse, seul Onfroy osa s'exprimer.

«&nbsp;Mon roi, ce serait fâcheux précédent. Nous avons un peu partout sur les marches[^28] usage de côtoyer les Damascènes. C'est là bien peu héroïque comme assaut.

--- Et après&nbsp;? Est-ce noble de payer son dû en méreaux de plomb&nbsp;? gronda Baudoin. Je règle tous mes soucis en une fois. Réservons la gloire pour nos batailles de l'été. Avec des troupes que nous pourrons solder&nbsp;!

--- Mon roi, si vous faites cela, plus aucun habitant de Damas n'acceptera de traiter avec nous, souligna Bernard. La confiance sera rompue à jamais. Nūr ad-Dīn n'aura qu'à souffler sur les braises du mécontentement pour s'en faire indéfectibles alliés.

--- Il est bien question de cela, Vacher&nbsp;! De cette confiance par trop dispendieuse que vous avez accordée à notre ennemi. Sans elle, nous n'en serions pas là&nbsp;!&nbsp;» s'indigna Baudoin.

### Jérusalem, palais royal, soirée du mardi 18 novembre 1158

Isaac suivait un garde qui le menait à Bernard Vacher, son oncle, qui l'avait fait mander. Le vieux chevalier avait l'habitude d'être servi promptement et Isaac ne s'en formalisait guère. Depuis Naalein, au sud, il avait chevauché à bride abattue dès qu'il avait reçu le message. Il avait donc eu la surprise d'arriver le premier au palais et avait patienté un moment auprès des cuisines.

Isaac se perdait souvent dans le dédale de la résidence royale. Ce n'étaient que vastes bâtiments enchevêtrés, courettes et escaliers sans fin, alternant avec de splendides salles au décor oriental. Il avait eu l'occasion à de nombreuses reprises de venir, au service de son oncle ou lors de cérémonies et festivités officielles. Pourtant il n'arrivait toujours pas à se repérer correctement en dehors des endroits les plus éminents.

Il déboucha dans une des zones où de modestes écuries accueillaient les montures des nobles de première importance, ainsi que celles des hôtes de marque. Lui avait dû laisser son cheval à l'entrée de la cité et finir le trajet à pied. Il n'était pas étonnant que le prestigieux porte-bannière royal n'ait pas eu à mettre pied à terre avant d'être dans le palais.

La cour était calme, la forge de maréchalerie était éteinte depuis un moment. Quelques veilleuses, disposées bien à l'écart de la paille ou du foin permettaient de se repérer dans l'obscurité du crépuscule. L'odeur saline des bêtes, du crottin, rendait l'endroit familier aux narines d'Isaac, comme à tout jeune destiné à devenir chevalier. Il passait tellement de temps en selle qu'il ne savait plus s'il aimait ou haïssait ces animaux.

Une ombre plus épaisse que les autres se tenait à l'entrée des écuries. Bernard Vacher était assis sur un petit tabouret rustique, avalant le contenu d'une écuelle. Ses yeux brillèrent à la lueur des lampes quand il vit arriver Isaac. Il chassa le sergent d'un geste et se leva pour embrasser son neveu.

«&nbsp;Tu as bien fière allure, mon garçon. Je m'enjoie de te voir si promptement. J'ai à peine cru la vigie quand elle m'a indiqué que tu étais arrivé avant moi&nbsp;!

--- Si j'avais idée de vous négliger, père aurait bon gré de me tancer à juste mesure, mon oncle. Vous me vouliez ici séance tenante, me voilà&nbsp;!&nbsp;»

Bernard se laissa retomber lourdement sur son siège, invitant d'un geste Isaac à faire de même sur le banc où il avait posé sa gamelle et un pichet.

«&nbsp;Il sera bien temps d'ici peu de te faire mesurer tes éperons de chevalier.

--- J'ai grande hâte à cela, mon oncle. Je pense que je suis prêt.

--- Ah, je peux te jurer sur les Évangiles que tu ne l'es certes pas, nota sans joie le chevalier. Mais cela n'est d'aucune importance, on ne comprend que trop tard ce genre de chose&nbsp;!&nbsp;»

Il avala encore quelques cuillers avec empressement. Isaac lui trouvait bien mauvaise mine, les traits tirés et les gestes lourds. On aurait dit que les années l'avaient rattrapé, ces derniers mois. Il soufflait régulièrement comme si le moindre de ses mouvements lui coûtait un effort incommensurable. Lui qui était considéré comme un des plus vaillants chevaliers de l'hôtel du roi paraissait désormais gauche et empoté. Finalement le vieil homme reposa son assiette, s'essuya de la manche et se rinça la bouche d'une longue gorgée de vin, avant de proposer le pichet à son neveu.

«&nbsp;C'est bon augure que tu sois là si tôt, je vais pouvoir prendre la route dès potron-minet.

--- Où allons-nous&nbsp;? s'enthousiasma Isaac, heureux à l'idée d'accompagner son oncle malgré sa fatigue.

--- Toi, nulle part. Tu vas demeurer ici, dans l'attente de nouvelles de ma part. Tu les auras soit en personne, soit par un mien coursier. Il faut que tu te tiennes prêt à courir au Nord, pour rejoindre le roi.&nbsp;»

Il tapota une sacoche de cuir à sa hanche.

«&nbsp;J'ai ici de quoi subvenir à tes frais pour ce long voyage. N'épargne pas les besants, assure-toi d'arriver au plus vite en la princée d'Antioche.

--- Quelque message secret à porter à Baudoin&nbsp;?&nbsp;»

Le ton soudain enjoué à l'idée de toucher du doigt la réalité du service royal, Isaac se voyait déjà plénipotentiaire galopant à travers le pays.

«&nbsp;Si seulement&nbsp;! Ce ne sera qu'à mon service que tu chevaucheras, mon garçon. Mais cette course peut nous valoir un retour en grâce...&nbsp;»

Bernard Vacher hésita un petit moment, sa voix s'étant cassée sur sa dernière remarque, brisant net l'enthousiasme de son jeune neveu.

«&nbsp;Tu sais que... le roi a quelques griefs à mon encontre, depuis quelques saisons.

--- Père et vous ne me dîtes rien, comment le saurais-je&nbsp;?

--- J'ai assez d'estime envers toi pour ne point te savoir sot. Le roi a bonnes raisons de m'en vouloir. J'ai failli à son service et, par ma faute, de graves choses sont arrivées. Il me faut tenter à toutes forces de remédier à ce mal que j'ai causé.

--- Ne puis-je vous assister&nbsp;?

--- C'est exactement cela que tu feras, je t'assure. Il me faut aller à Damas chercher la clef de ces mésaventures. Je crains qu'il n'y ait quelque terrible machination à l'œuvre.

--- De qui&nbsp;? Pour quoi&nbsp;?&nbsp;»

Bernard sourit devant l'agitation de son neveu. Il haussa les épaules, ouvrit les mains et les joignit.

«&nbsp;Si je devais encroire ce que je sais, peut-être Nūr ad-Dīn a-t-il été trompé, comme nous. Il n'est de nul avantage pour lui de maintenir la guerre entre Damas et nous. Au contraire, il y trouve fourrage, bétail et impôts pour nourrir ses ambitions au nord.&nbsp;»

Isaac fronça les sourcils, hésitant à comprendre ce que cette révélation lui laissait entendre.

«&nbsp;Mon oncle, ces rumeurs seraient donc...

--- Pour ça, tu peux te fier aux ragots de tour de garde. Le tribut était bien falsifié. Le roi a fait interdire qu'on en parle, mais les faits demeurent.

--- Mais vous n'y êtes pour rien, ce sont eux qui...

--- Calme-toi mon garçon. C'est bien le souci&nbsp;: nous ne savons pas qui ils sont, *eux*.

--- Eh bien, nos ennemis, les Damascènes en l'occurrence&nbsp;!&nbsp;»

Bernard secoua la tête d'un air désolé et posa une main imposante sur l'épaule de son neveu.

«&nbsp;Isaac, c'est là réponse sans usage. Il me faut savoir le nom de la personne qui a décidé de mettre ces fausses monnaies en la cassette. De là, je pourrais tenter de comprendre ses motifs.

--- Il y a bien huit mille raisons qui ont pu la pousser&nbsp;!

--- L'or&nbsp;? Peut-être... Mais en ce cas, je n'aurais plus à m'en inquiéter. Le roi me maintiendra en déshonneur, à juste titre. J'aurais du temps à te consacrer pour m'assurer que tu romps la lance comme il sied&nbsp;!&nbsp;»

Il marqua à nouveau une pause, serra le poing, qu'il leva devant son visage.

«&nbsp;Mais si de bien plus puissantes machinations sont à l'œuvre, je dois en informer mon roi, pour qu'il puisse les briser&nbsp;! Et tu seras ma voix pour lui porter la nouvelle.&nbsp;»

Il se leva et saisit son neveu dans une embrassade bourrue. Il était épuisé de ces derniers mois, maintenu à l'écart après des dizaines d'années à servir la couronne. Il espérait comprendre quel feu lui avait ainsi brûlé les ailes. Plus que son honneur perdu, bien au-delà de l'arrêt des remarques acerbes qu'il entendait sur son passage, il avait besoin de voir la lueur de confiance renaître dans les yeux du roi Baudoin. ❧

### Notes

L'attaque de Baudoin III sur les plateaux du Golan en février 1157 n'a jamais reçu d'explication solidement étayée. Comme cela déboucha sur la prise de Baniyas, ville frontière essentielle, par le pouvoir musulman, il m'a semblé intéressant de voir si des phénomènes souterrains inconnus de nous n'avaient pas joué. Une trêve, assortie d'un lourd tribut, avait été signée peu de temps auparavant avec le nouveau maître de Damas, je me suis donc amusé à proposer une hypothèse assez rocambolesque.

En outre, cela m'offre l'occasion de suivre un peu le personnage de Bernard Vacher, qui a progressivement disparu des chartes après plusieurs décennies au premier plan de l'hôtel royal. Cet effacement n'est d'ailleurs pas forcément dû à une relégation comme je l'indique ici, c'est là pure conjecture. Il demeure possible, plus simplement, que les sources qui ont pu comporter des mentions de cet important serviteur, réputé pour son travail diplomatique avec Damas, aient été en trop petit nombre pour nous parvenir, plus de huit siècles après les faits.

### Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tomes I, II et III, Damas&nbsp;: Institut Français de Damas, 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I, *Economic Foundations*, Berkeley et Los Angeles&nbsp;: University of California Press, 1999.

Pour quelques dinars de plus
----------------------------

### Jérusalem, grand hôpital de Saint-Jean, soirée du samedi 19 janvier 1157

Il était rare pour Abu Malik de se rendre dans le principal établissement des frères de Saint-Jean. Avec leur militarisation croissante, il s'y trouvait de plus en plus mal à l'aise. Pourtant, malgré toutes ses craintes, il n'avait jamais eu à subir la moindre remarque, et encore moins de brimades, de la part des soldats en armes qu'il avait pu y croiser. Comme beaucoup de chevaliers de la Milice du Temple, il s'y recrutait de nombreux hommes nés en terres levantines, habitués à fréquenter des personnes d'autres religions que la leur. Tant qu'on ne remettait pas en cause leur prééminence, qu'on leur versait les impôts, ils n'étaient pas belliqueux. En quelques décennies, ils étaient devenus de vrais Orientaux.

Chaudement emmitouflé dans son épaisse jubba de laine colorée, il sentait le froid des pierres à travers ses souliers malgré ses chaussettes. L'hiver était une fois de plus très rigoureux à Jérusalem et les bâtiments, frais en été, se transformaient en de véritables glacières. Il voyait la buée qui s'échappait des lèvres des personnes qu'il croisait tandis qu'il était mené auprès de frère Raymond, qui avait demandé après lui. Il sentit avec délice les odeurs du repas dans la zone des cuisines, où il ne s'attarda cependant pas. Ils allaient au-delà des lieux ouverts aux voyageurs et pèlerins, dans la partie réservée aux moines.

Il finit par pénétrer dans une vaste salle parsemée de lutrins, où des copistes devaient s'user les yeux en journée à écrire chartes et documents, peut-être des ouvrages littéraires ou religieux pour les plus chanceux. L'endroit sentait la colle de peau, la gomme arabique et l'huile des lampes. Une seule lueur était visible, au fond de la pièce, éclairant une table avec de nombreux pots et gobelets emplis de plumes et de pinceaux. Un hospitalier de haute taille, au physique impressionnant, y griffonnait sur des tablettes, le crâne abrité sous une profonde capuche. On avait tiré à ses côtés un chariot à braises qui apportait un peu de chaleur. Abu Malik sourit en le reconnaissant.

«&nbsp;Frère Raymond&nbsp;! Grande joie de vous encontrer de nouvel&nbsp;!&nbsp;»

Le clerc se leva et fit bon accueil à celui qu'il considérait comme un ami, malgré leurs différences. Il paraissait épuisé, son fin visage creusé de rides de fatigue. Il demeurait néanmoins aussi alerte qu'à son habitude et, d'un geste, invita son hôte à s'asseoir sur un escabeau.

«&nbsp;Je vous prie de faire excuse pour cette fort tardive invitation. Mais j'ai cru apprendre que vous repartiez d'ici peu à Damas&nbsp;?

--- En effet, je n'aime guère traîner en ces froides montagnes l'hiver venu. Ce n'est que par grande nécessité que je suis ici. Je ne pensais pas vous y voir, on m'avait dit que vous étiez parti au nord.

--- J'arrive à peine de Tripoli, en effet.&nbsp;»

Le moine replia quelques documents, referma certaines de ses tablettes et rapprocha instinctivement le buste d'Abu Malik, bien qu'ils fussent seuls dans le lieu.

«&nbsp;J'aurais une question délicate pour vous. Je vous sais homme de paix, ainsi que moi, et elle se trouve en fort périlleuse situation.&nbsp;»

Le Damascène fronça les sourcils, inquiet de ce préambule. Les ombres autour d'eux lui semblèrent soudain menaçantes, curieuses et hostiles. Frère Raymond se passa plusieurs fois la langue sur les lèvres, cherchant visiblement à exprimer une idée difficile, ou hésitant à aborder un sujet délicat.

«&nbsp;Ce que je vais vous dire est frappé du plus grand secret. On m'a autorisé à vous en parler, en raison de la confiance entre nous. Mais je vous exhorte à la plus grande des prudences. Gardez cela sous le manteau autant qu'il est possible.&nbsp;»

L'hospitalier se gratta le menton, renversant ses ultimes préventions.

«&nbsp;Le tribut versé l'a été en fausse monnaie. Le roi est furieux de la chose et entend bien réparer l'outrage. Il vocifère comme diable en bénitier. Connétable et sénéchal m'ont incité à me renseigner sur l'ambiance qui règne dans le Bilad al-Sham[^29].&nbsp;»

Abu Malik écarquillait des yeux ronds, bien en accord avec sa bouche béante. Cela faisait de nombreuses années que Damas s'assurait d'une bienveillante neutralité des Latins en leur versant une rente garantissant la trêve. Le nouveau dirigeant de la cité, Nūr ad-Dīn[^30], fort occupé dans ses territoires au nord, avait donné l'impression de vouloir tempérer avec le royaume de Jérusalem et avait perpétué cette habitude. Certainement dans l'attente de se trouver en position de force.

«&nbsp;Certains barons de la Haute Cour sont au courant et tiennent à savoir ce qui se trame dans l'ombre. Cela n'est pas de bonne intelligence pour Nūr ad-Dīn que de chercher querelle en ces temps, ce me semble.

--- Certes pas. Il n'est d'ailleurs pas en les murs, plus préoccupé de ses domaines alépins. Il sait l'adhan[^31] proclamé en son nom et ses hommes à la tête des diwans[^32], cela lui convient.&nbsp;»

Frère Raymond soupira. Il se doutait de cette réponse qui l'inquiétait plus qu'elle ne le rassurait. Cela signifiait que des intrigants cherchaient à provoquer la guerre ou qu'ils étaient prêts à prendre ce risque pour faire main basse sur le montant du tribut.

«&nbsp;Sauriez-vous vous enquérir de cela&nbsp;? Si nous trouvons les malfaisants, cela peut éviter de sanglants conflits qui ne sauraient tarder.

--- Il se trouve des amis miens désireux de vivre en paix. Certains sont de l'administration, peut-être auront-ils eu vent de ce complot ? Rien n'est jamais aussi secret qu'on l'espère.&nbsp;»

Dépité, Frère Raymond hocha la tête. Il était bien conscient que malgré la volonté royale ne pas ébruiter l'affaire, les rumeurs iraient bon train. Les plus belliqueux des croisés y trouveraient de quoi faire prospérer les graines de la discorde. Abu Malik, comprenant la question urgente, fit mine de se lever. L'hospitalier l'arrêta d'un geste de la main et lui glissa un papier plié.

«&nbsp;Outre cela, pour nos affaires habituelles, j'ai là quelques noms de personnes qui se trouvent peut-être en geôles damascènes. Il en est une en particulier, pour qui rançon peut être versée.&nbsp;»

Il rouvrit le petit document pour y retrouver le nom qu'il avait sur le bout de la langue.

«&nbsp;Guilhem Torte. Si jamais les combats devaient reprendre, il serait bon de s'enquérir de ces malheureux au plus tôt, sans quoi ils moisiront en cachot encore des mois...&nbsp;»

Abu Malik prit le document et acquiesça. Il avait lui-même pu faire libérer quelques coreligionnaires à la faveur de la trêve. Il était heureux de pouvoir les ramener chez eux avant le début de la nouvelle année[^33], mais les réjouissances seraient de courte durée. Il sentait que la situation devenait de plus en plus délicate pour les hommes comme lui, ambassadeurs et diplomates malgré eux. Il s'annonçait un temps d'acier et de sang, il le percevait au fond de lui. Il accorda un sourire amer à l'hospitalier.

«&nbsp;Je verrai ce que je peux faire. Je vous adresserai dévoué messager dès que j'en saurais plus pour une affaire ou l'autre.&nbsp;»

Frère Raymond inclina le chef en guise de salut. Lui aussi pressentait de sombres futurs. Son visage se réfugia dans l'ombre de sa capuche tandis qu'il s'affairait à ranger ses derniers documents.

### Damas, demeure de Al-Zarqa', soirée du youm al sebt 19 dhu al-hijjah 551[^34] {#damas-demeure-de-al-zarqa-soirée-du-youm-al-sebt-19-dhu-al-hijjah-55134}

Les gouttes de pluie dansaient dans le bassin au centre de la cour sur laquelle donnait largement le majlis[^35]. Malgré l'averse, il ne faisait pas trop froid, mais les hommes installés sur les matelas étaient chaudement habillés, la pièce n'ayant aucun système de chauffage. Abu Malik avait finalement décidé de s'ouvrir de son problème à son ami Al-Zarqa'[^36]. Celui-ci était un fonctionnaire réputé, doublé d'un commerçant compétent, dont Abu Malik estimait grandement la probité. Derrière le sourire rusé et les yeux inquisiteurs se cachait un esprit acéré, avide de justice. Il savait néanmoins naviguer à vue dans les cercles du pouvoir et avait réussi à se maintenir en poste malgré la récente administration.

Il avait lui-même posé quelques questions et avait décidé d'inviter un soldat en qui il avait suffisamment confiance pour lui présenter Abu Malik. Yalim al-Buzzjani était un émir de l'ancien atabeg de Damas Mujīr ad-Dīn Abd al-Dawla qui avait demandé à rejoindre la troupe de Nūr ad-Dīn quand son maître avait choisi de se retirer de la région. Il était d'origine turque, mais avait longtemps servi les bourides, au point de se sentir aussi damascène qu'eux. Mais, surtout, avait avancé al-Zarqa, il était d'une grande loyauté une fois sa parole engagée et ne verrait pas d'un bon œil que des traîtres tentent de tromper l'émir.

Abu Malik avait expliqué l'histoire en quelques phrases rapides et laissait l'officier digérer les informations. Au fur et à mesure du récit, son visage s'était contracté et ses yeux s'étaient réduits à des fentes. Tout en réfléchissant, il jouait machinalement avec une de ses longues tresses brunes. Comme le sharbush[^37] et le qaba[^38], c'étaient, avec le sabre et l'arc, des attributs militaires constitutifs de son identité. Lorsqu'il prit la parole, sa voix était rauque, retenue, et il parlait lentement, visiblement choqué par le méfait.

«&nbsp;Vous me dévoilez grave nouvelle. Bien lourde à celer.&nbsp;»

Il dévisagea al-Zarqa' avec intensité, bravant le regard clair de son interlocuteur sans ciller.

«&nbsp;L'émir m'a fait l'honneur de me faire sien, et je suis désormais tenu de le servir loyalement. Nous sommes plusieurs à avoir juré ainsi. Mais certains n'ont que faire des paroles accordées.

--- L'or fait tourner bien des têtes&nbsp;! opina Abu Malik.

--- Il les fait souvent tomber, aussi&nbsp;» enchérit le soldat, le visage sévère.

Il se réfugia dans ses pensées encore un moment avant de briser de nouveau le silence.

«&nbsp;Le gouverneur Nağm ad-Dīn Ayyūb est fidèle, de ce que j'en sais. Il ne semble guère désireux de porter le fer et le feu chez l'ennemi. Il parle plus de ses matchs de polo que d'autre chose ces temps-ci. Sans compter que le tribut est dérisoire en regard du montant de l'iqta[^39] qu'il perdrait si pareille forfaiture venait aux oreilles de l'émir. Il nous faut donc chercher ailleurs.

--- Les coffres étaient à la garde du wali de la citadelle Izz al-Dīn. Je ne le connais guère...

--- C'est un ghulam de l'émir. J'ai eu peu affaire à lui. On le dit solide dans la mêlée, très bon archer et tacticien avisé. Il est aussi très autoritaire.

--- Il ne serait peut-être pas très heureux d'apprendre que les monnaies confiées à sa garde ont été falsifiées. Ne pouvons-nous l'interroger discrètement&nbsp;?

--- C'est à voir. Il ne me semble pas homme à pouvoir se faire pareillement flouer.

--- Cela voudrait dire qu'il serait au courant. Soit d'un plan de l'émir, dont nous ne savons rien, soit de la conspiration qui porte atteinte à son honneur.&nbsp;»

Embarrassé, Yalim hocha le menton. Il évoluait depuis l'enfance dans ces milieux de pouvoir, où le danger n'était pas moindre dans les soirées aux discours feutrés qu'au plus fort des batailles. Il avait vu autant de têtes tomber après des manigances qu'au cours des mêlées. Il détestait cet univers de faux-semblants et en arrivait parfois à porter plus grande estime à des adversaires acceptant un affrontement loyal les armes en main qu'à ses coreligionnaires amateurs d'intrigues sournoises.

«&nbsp;Pour moi, indiqua al-Zarqa', le personnage central est le mutawali ad-diwan[^40]. C'est lui qui était chargé de rassembler la somme, il avait toute latitude pour en falsifier le versement.

--- Qui est-ce désormais&nbsp;? s'enquit Abu Malik.

--- Amīn ad-Dīn Zayn al-Hağğ. Un obscur fonctionnaire venu dans l'orbite de l'émir.&nbsp;»

À sa voix, Yalim ne portait guère d'estime au personnage.

«&nbsp;S'il y a eu complot, je ne serai pas étonné qu'il en soit. Il avait toutes les clefs en main pour cela. Et cela ne m'étonnerait guère de lui.

--- Quelques taches obscurciraient donc son honneur&nbsp;?

--- Je n'en sais que peu sur lui, mais il a les yeux d'un homme prêt à tous les commerces.&nbsp;»

Al Zarqa' lança un regard à Abu Malik. Ce dernier n'en menait pas large. Il faisait confiance à son ami et, par extension, à Yalim. Mais il commençait à regretter de s'aventurer en si périlleux terrain. Il lui semblait qu'il s'apprêtait à frapper de ses mains nues un essaim de guêpes. En ces périodes tumultueuses, garder sa tête sur ses épaules dépendait parfois étroitement de sa propension à ne pas mettre son nez en des endroits inconnus. Mais c'était trop tard pour les remords. Il repensa à son cousin Idris. Il ne lui aurait jamais parlé d'une telle affaire, conscient que cet esprit volage n'aurait pas su tenir sa langue. Mais il était certain que ses yeux auraient pétillé et qu'il aurait lancé, goguenard&nbsp;: «&nbsp;Donne un cheval à celui qui dit la vérité ; il en aura besoin pour s'enfuir.&nbsp;»

### Jérusalem, hôtel de Bernard Vacher, fin d'après-midi du mardi 26 février 1157

Abu Malik était confortablement installé sur un siège rembourré de coussins moelleux. Le bâtiment n'était pas en très bon état, mais ça avait été une riche demeure par le passé. On sentait qu'elle était délaissée. L'homme qui l'avait accueilli était un soldat renommé, vétéran de nombreuses batailles. Il semblait par contre avoir négligé sa vie personnelle et possédait une maison sans âme, au silence à peine troublé par une poignée de domestiques. Si rien ne manquait, chaque chose paraissait avoir été retenue pour son unique caractère utilitaire et fonctionnel. Même dans cette pièce de réception, les ornements brillaient par leur absence.

Bernard Vacher lui faisait l'effet d'un homme las. Des cernes sous les yeux accentuaient son attitude avachie. Il s'attendait à un fier-à-bras à l'allure martiale, il découvrait une personne épuisée qui parlait d'une voix éteinte. Abu Malik l'avait appris&nbsp;: c'était lui qui avait accepté le tribut contrefait. Il devait certainement être sous le coup d'une disgrâce, malgré ses années de service fidèle. Abu Malik n'avait abordé la question qui l'inquiétait qu'après avoir évoqué le problème des prisonniers récents, qu'il espérait pouvoir faire libérer avant que les choses ne s'enveniment. L'absence de frère Raymond, en déplacement, l'avait contraint à dévoiler ce qu'il avait découvert à ce chevalier dont il ne savait que peu.

C'était de nouveau un risque qu'il prenait, se fiant à la parole d'Altûntâsh[^41]. Chaque nouvelle décision lui pesait encore plus que la précédente. Néanmoins, il était persuadé que ce qu'il faisait était aussi utile que de faire libérer des captifs. Sa conscience, ou Allah, ne l'aurait pas laissé en paix si des hommes perdaient la vie par suite de sa passivité.

Bernard Vacher, la mine renfrognée, avala un peu de vin.

«&nbsp;Je ne vous mercierai jamais assez de m'apprendre tout cela. J'imagine combien cela vous coûte et les risques que cela vous fait prendre.

--- Un mien cousin aurait rétorqué qu'*il n'est pas d'un brave homme de tarder à rendre service*.

--- Ce me semble que le dicton précise *ni de se presser à la vengeance*&nbsp;» sourit sans joie Bernard Vacher.

Ls deux hommes échangèrent un regard de connivence, soulagés de trouver un compagnon d'aventure en cette périlleuse voie.

«&nbsp;Le plus important est que l'émir n'ait nul désir de provoquer batailles. Cela au moins est de quelque réconfort.

--- Sauf que votre roi vient d'allumer l'étincelle qui embrasera peut-être l'été. C'est là possible espérance des comploteurs.&nbsp;»

Bernard Vacher maugréa. Il ne pouvait préciser son idée&nbsp;: Nūr ad-Dīn n'ayant pas prémédité tout cela, il n'avait pas de plan offensif, de stratégie mûrement réfléchie pour les semaines à venir. Il serait aussi pris au dépourvu de la récente attaque de Baudoin sur le Golan que le roi de Jérusalem l'avait été par le versement du tribut en fausse monnaie. Au moins aurait-il une bonne nouvelle à annoncer au connétable.

«&nbsp;Je vais voir ce que je peux faire pour tenter d'éviter que la situation s'envenime. Nulle forteresse n'a été prise. Il ne s'agit que de rembourser ce qui a été razzié.

--- Il demeure les blessés, les captifs et les morts. Leur mémoire pèsera lourd dans toute discussion.&nbsp;»

Bernard Vacher baissa la tête, les sourcils froncés. Il avait suffisamment arpenté les rues de Damas pour savoir qu'on ne pardonnerait pas aisément au roi de Jérusalem d'avoir lancé ses troupes sur des pacifiques nomades alors même qu'une trêve était en cours. Si les puissants n'hésitaient pas à trahir quand ils y voyaient un avantage, les modestes gens en concevaient longue rancune. Surtout, comme le disait Abu Malik, lorsque des humbles avaient été victimes, une fois de plus.

«&nbsp;Pourquoi votre roi n'a pas annoncé publiquement l'affront&nbsp;?

--- Il est déjà assez marri qu'on l'ait cru assez stupide pour ne pas reconnaître de fausses monnaies. Il n'a guère envie de clamer à tous qu'on le prend si peu en considération. Tous nos adversaires auraient beau jeu de le moquer et de ne lui accorder plus aucun crédit.

--- L'ombre de la reine l'inquiète donc encore&nbsp;?&nbsp;»

Le chevalier lança un regard surpris au négociant damascène. Il ne s'attendait pas à ce que la politique interne du royaume soit si bien connue chez leurs adversaires. L'idée lui était venue, en effet, que Baudoin craignait surtout que sa mère, Mélisende, n'apprenne sa déconvenue. Il n'avait réussi à l'éliminer du pouvoir que récemment et se sentait encore en délicate posture par rapport à elle, qui avait dirigé les territoires d'une main de fer pendant des années, aux côtés de son époux Foulque puis en tant que régente. Pour toute réponse, Bernard Vacher se contenta de hausser les épaules.

«&nbsp;Il va me falloir tenter de convaincre le connétable et le sénéchal, ainsi que le roi, que l'émir n'a rien manigancé. Histoire d'apaiser les esprits. Ensuite, espérons que les comploteurs seront démasqués. Avez-vous idée en ce sens&nbsp;?

--- Je n'ai guère vers qui me tourner. Chaque visage amiable peut cacher une âme de conspirateur. Je connaissais les anciens membres de l'administration, mais bien peu sont demeurés en place. Des amis à moi pistent la vérité...

--- Ne prenez pas de risque. Si les voleurs n'ont pas craint de provoquer batailles, ils n'auront certainement pas d'égard pour vous.

--- Je louvoie au milieu des serpents, j'en ai bien conscience.&nbsp;»

Bernard Vacher hocha la tête lentement.

«&nbsp;En pourpensant toute l'affaire, je me suis dit que Muhammad ben afar, le chambellan qui m'a remis les coffres, était certainement dans la confidence. Il avait accès aux monnaies et aurait pu éventer toute l'opération. Peut-être pourrez-vous en apprendre plus sur lui. Il m'a fait l'effet d'être veule plus qu'intrépide.

--- Pareils hommes se dévoilent parfois dangereux, frappant sans discernement. Mais je vous remercie, je me renseignerai.&nbsp;»

Le négociant s'apprêtant à partir, Bernard Vacher lui laissa quelques coordonnées de contacts utiles, en cas de besoin. Sa main ne saurait le protéger par-delà l'Arbre de la mesure[^42], mais il lui fit serment de lui faire escorte pour peu qu'il soit sur les terres latines. Son frère tenait un château royal, à Naalein, et serait averti d'accueillir au mieux le Damascène. Le chevalier se sentait humilié par l'histoire et se raidissait dans sa posture d'homme loyal et fidèle à sa parole. La proposition arracha un sourire triste à Abu Malik lorsqu'il prit congé.

### Damas, demeure de Abu Malik, soirée du youm al sebt 25 muharram 552[^43] {#damas-demeure-de-abu-malik-soirée-du-youm-al-sebt-25-muharram-55243}

La maison était en effervescence, malgré les efforts d'Abu Malik pour ne pas inquiéter ses domestiques et sa famille. Il ne s'était confié, quoique superficiellement, qu'à Fazila, son épouse, afin de ne pas alimenter les inévitables rumeurs qui ne manqueraient pas de naître à peine auraient-ils franchi Bab al-abiya[^44]. Officiellement, il prenait la route pour affaires durant plusieurs mois et avait décidé d'emmener sa femme avec lui, car ils devaient aller saluer des cousins. La maisonnée s'était étonnée de ce départ bousculé.

En réalité, cela faisait suite à une discussion qu'Abu Malik avait eue la veille, lors de la prière du vendredi à la Grande Mosquée. Al Zarqa' lui avait confié que des remous commençaient à se faire sentir dans les sphères du pouvoir. Des échos de leurs questions posées ici et là retentissaient un peu plus fort qu'ils ne l'auraient espéré. Le fonctionnaire avait conseillé à Abu Malik de s'absenter un moment, laissant le temps aux choses de se tasser. S'il disparaissait, on ne pouvait le soupçonner d'enquêter sur des sujets interdits. Quant à al-Zarqa', il ne s'inquiétait guère de la curiosité dont il serait l'objet, ses demandes pouvant tout à fait être légitimées par ses attributions officielles et sa réputation tatillonne.

Éclairé d'une maigre lampe, Abu Malik triait certains de ses documents de commerce, désireux de ne pas négliger les échanges en cours lorsque Fazila vint l'interrompre. Elle repoussa à demi la porte derrière elle, surveillant du coin de l'œil que personne ne s'approchait dans la salle adjacente.

«&nbsp;J'ai fini de préparer nos habits. Ne peux-tu m'en dire plus sur notre destination&nbsp;?

--- Je ne sais pas encore. La famille d'al-Baqara[^45] tient le casal de Naalein, nous allons tout d'abord nous rendre là-bas. Puis nous aviserons.

--- J'espère que nous n'aurons pas à demeurer chez les polythéistes. Nous avons nos vies ici&nbsp;: nos amis, nos familles...

--- Ce n'est que pour un temps&nbsp;!&nbsp;»

Les yeux de Fazila se firent inquisiteurs, la bouche pincée.

«&nbsp;Je ne comprends pas pourquoi nous devons pareillement fuir notre cité&nbsp;! Si tu te sens menacé, tu n'as qu'à te plaindre au qadi&nbsp;!

--- C'est une affaire compliquée, Fazila. En parler publiquement ne causerait que grands torts.

--- Plus grands qu'ainsi tout quitter comme des voleurs&nbsp;? Alors même que nous n'avons rien fait&nbsp;?

--- Cela nous dépasse, Fazila. Les conséquences vont au-delà de ce que je peux imaginer.&nbsp;»

La jeune femme leva le menton, plissant la bouche en signe de mécontentement. Elle avait cru épouser un simple commerçant, de bonne réputation par ses actions charitables, et elle découvrait que c'était un intrigant qui refusait de lui expliquer par le menu ce qu'il avait fait pour les exposer ainsi, elle et ses filles. Comprenant qu'il ne lui dirait rien de plus si elle cherchait l'affrontement, elle décida d'adopter une attitude plus conciliante. Elle s'accroupit devant lui, affichant un visage plus amène.

«&nbsp;Tu es un bon mari, Rashid, j'en rends grâce à Allah chaque jour. J'ai seulement grand désir d'être à tes côtés en chaque épreuve de la vie. Nous avons surmonté la mort de Malik sans nous déchirer, en confiance. Ne m'écarte pas ainsi.&nbsp;»

Abu Malik reposa ses feuillets et lui prit la main, touché de cette déclaration.

«&nbsp;T'en dévoiler plus risquerait de te mettre en danger. Prends ce voyage pour ce que nous prétendons qu'il est&nbsp;: l'occasion de cheminer ensemble et d'aller voir des proches. Nous irons de certes à Misr[^46] voir tes cousins, depuis le temps que tu m'en fais la demande. Avant cela, nous ferons juste halte au royaume de Badawil[^47].&nbsp;»

Elle lui accorda un sourire engageant avant de se relever. Lorsqu'elle se tourna pour repartir, elle ne retint plus son désappointement et son visage s'allongea de dépit. Elle ne répondit que par un hochement à la dernière instruction de son époux.

«&nbsp;J'ai fait mander Ibrahim pour demain matin. Il chargera les bêtes et nous pourrons prendre la route rapidement.&nbsp;»

À l'idée de ce voyage, Abu Malik se sentait fébrile comme il ne l'avait plus été depuis des années. Il parcourait les routes entre Damas et Jérusalem si souvent qu'il était familier de chaque lacet, chaque arbre, tout autant que des hébergements en chemin. Mais c'était la première fois qu'il partirait la peur au ventre, sans certitude quant à son retour. ❧

### Notes

La suite des événements initiés dans *Pour une poignée de dinars* demeure tout aussi conjecturale que son début. Je profite de l'occasion pour présenter un peu plus en détail le fonctionnement humain des deux principaux pouvoirs qui s'affrontaient pour le contrôle des territoires levantins. Loin d'être des structures monolithiques, guidées par des dirigeants tout-puissants au destin d'exception, c'étaient des amalgames de volontés disparates, qui ne tiraient pas toujours dans le même sens.

Bien que cela ne soit qu'un exercice spéculatif, j'avais envie de vous proposer de suivre la façon dont un tel complot aurait pu exister et comment il aurait affecté la vie quotidienne de simples individus. Même s'il s'agit de fiction (et tout en assumant clairement ce postulat), je pense salutaire d'imaginer les conséquences pour des acteurs mineurs, mais représentatifs, je l'espère, des deux camps. Nous sommes ici loin d'une vision de l'histoire déterministe, propre à bâtir des romans idéologiques. J'adopte un point de vue plus fractal, tentant d'analyser les rapports de force depuis les plus vastes groupes jusqu'au cœur de chaque individu entraîné dans ce grand tourbillon. Bien évidemment, pour avoir un quelconque intérêt dans l'alimentation de la perception et de la réflexion historiques, cela ne peut se concevoir qu'en puisant dans une documentation la plus large et sourcée que possible.

### Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tomes I, II et III, Damas&nbsp;: Institut Français de Damas, 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I, *Economic Foundations*, Berkeley et Los Angeles&nbsp;: University of California Press, 1999.

Impitoyable
-----------

### Jérusalem, cuisines du palais royal, matin du lundi 27 octobre 1158

Comme dans toutes les maisons nobles, les cuisines bruissaient d'agitation dès les précoces lueurs de l'aube. Premiers levés, les mitrons avaient allumé les feux, les queux avaient pétri la farine et l'eau du pain quotidien, réveillant la domesticité avec une bonne odeur de cuisson. Une fois les portes de la ville ouvertes, au son des cloches monastiques, on voyait arriver les produits frais, légumes et bêtes, pour compléter les vastes réserves des celliers. Pléthore de gamins et de jeunes grouillots s'employaient à toutes les tâches annexes&nbsp;: mouture du grain, épluchage, plumage, apport de bois, nettoyage de la vaisselle, transport de baquets et cruches... Autour d'eux s'agitait une ribambelle de cuisiniers qui avaient pour dur labeur de nourrir l'hôtel du roi, ses proches, ses serviteurs et toutes les personnes qui se présenteraient à table ce jour.

Bernard Vacher aimait à se caler dans un recoin, goûtant la mie souple encore tiède du four, trempée d'huile savoureuse. Il y ajoutait volontiers de petits oignons au vinaigre, douceur que lui concédait le maître queux qu'il connaissait depuis de longues années. On ne prêtait plus attention à l'imposante silhouette, souvent en armes, qui se coinçait contre le piédroit d'une des vastes cheminées, sur un escabeau comme le plus simple des commis. Pour être vaillant et redouté, parfois autoritaire et exigeant, Bernard Vacher n'avait pas la superbe des orgueilleux.

Maussade, il ressassait toujours sa disgrâce. L'absence du roi lui permettait de se sentir moins à l'écart, mais il n'avait pas encore réussi à revenir dans ses faveurs avant le départ de Baudoin pour le Nord et la rencontre avec la puissance byzantine. D'une certaine façon, il préférait ne pas avoir à côtoyer le basileus et ses diplomates, trop tortueux et ambigus selon lui. Il avait donc quelques mois devant lui pour trouver une solution.

Parmi la valetaille qui s'agitait, un petit homme replet se figea un instant devant lui, saluant de façon appuyée. Bernard sourit en reconnaissant le visage. Germain, un des domestiques de l'archevêque de Tyr, aimait à traîner ici, où les mets étaient plus savoureux que dans le palais du patriarche voisin. Le nouvel occupant, Amaury de Nesle, n'était pas adepte du faste et se contentait d'un service assez sobre. Les deux édifices s'imbriquant l'un dans l'autre, il était facile au jeune valet de venir baguenauder en de plus exubérantes cuisines.

Une idée se fit peu à peu jour dans l'esprit de Bernard Vacher. Il fit signe à Germain de s'approcher.

«&nbsp;Dis-moi, mon garçon, tu connais bien la ville de l'archevêque&nbsp;?

--- De certes, sire Vacher. J'y ai grandi, aucune ruelle ne m'en est inconnue&nbsp;! se vanta Germain.

--- Que peux-tu me dire du quartier des Vénitiens&nbsp;?

--- Ils sont encore plus secrets que des Juifs&nbsp;! Ils verrouillent la zone comme s'ils y serraient monceaux d'or&nbsp;! Et ils parlent leur langue, entre eux.&nbsp;»

Bernard Vacher se frotta le menton, cherchant une façon de ne pas trop en dévoiler.

«&nbsp;Quand fais-tu chemin de retour à Tyr&nbsp;? J'aurais peut-être besoin d'aller y quérir besants sarracinois...

--- Le père Herbelot semble avoir grande hâte de rentrer... Certainement demain.&nbsp;»

Germain se frotta les fesses, en rappel des douloureux souvenirs que lui inspiraient les voyages.

«&nbsp;Je ferai peut-être voyage avec vous, en ce cas. Cela semble toujours plus court, en agréable compagnie.

--- Le père Gonteux sera enchanté de vous savoir à siens côtés&nbsp;! Avez-vous mission près l'archevêque&nbsp;?

--- Nullement. Le roi m'a donné congé le temps qu'il était au nord. Il est de peu de risque que l'émir nous assaille, avec toutes nos armées près de sa bonne ville d'Alep. Il me faut juste aller voir les Vénitiens.&nbsp;»

Le jeune homme tira un long nez, se rapprocha comme pour délivrer une importante confidence.

«&nbsp;Je ne saurais trop vous conseiller de ne pas accorder fiance à ces marchands. Ils ont un abaque en guise de cœur.

--- Aurais-tu quelques griefs à leur encontre&nbsp;?

--- Pas spécialement, reconnut de mauvaise grâce Germain, mais il est de bonne fame qu'il est sage de recompter ses doigts après leur avoir serré la main. On dit qu'un Vénitien vaut deux Arméniens...

--- Qui eux-mêmes valent chacun deux juifs&nbsp;! sourit Bernard Vacher. Je te mercie de ton conseil. Je ferai attention à cela. Ne connais-tu pas personne parmi eux&nbsp;?

--- Le vieux Maursin[^48], leur curé et notaire, est plutôt amiable. Il nous donnait parfois friandises lors des fêtes.&nbsp;»

Le vieux chevalier hocha la tête. Au moins avait-il une entrée pour aller mener l'enquête sur place. Il n'était pas tant familier de cette cité royale portuaire.

### Tyr, cathédrale Sainte-Croix, après-midi du mardi 4 novembre 1158

Une violente averse frappait le parvis de la cathédrale lorsque Bernard Vacher et son compagnon y parvinrent enfin. À l'abri des hautes voussures, ils s'ébrouèrent puis pénétrèrent dans le lieu. Une forte odeur d'encens les prit à la gorge et il leur fallut un peu de temps pour s'accoutumer à l'obscurité. Les lumières s'accrochaient surtout sur les décors du chœur, à l'autre extrémité de la longue basilique. De plus, la majeure partie des personnes qui cherchaient avant tout à fuir la pluie s'étaient installées vers l'entrée, ajoutant des ombres au brouhaha ambiant.

Bernard Vacher invita Pietro Gradenigo à s'asseoir au pied d'un des vastes massifs de piliers soutenant les voûtes, au nord, un peu à l'écart de la porte ouvrant sur le cloître afin de ne pas être dérangé. Il lui avait fallu un peu de temps pour dénicher le jeune homme et encore un peu plus pour le convaincre de parler. Pietro n'était pas originaire de Jérusalem et se réfugiait volontiers derrière son statut de Vénitien. Il n'avait pas souhaité recevoir le chevalier chez lui, dans le quartier où il serait sous la juridiction vénitienne, mais avait choisi un lieu de culte où, il le savait, le pouvoir royal ne pouvait pas s'exercer non plus. Bernard Vacher espérait donc d'importantes révélations.

Il avait fouillé autant qu'il l'avait pu dans la ville, sans trop se dévoiler. Il lui avait fallu peu de temps pour comprendre que le privilège de frappe de monnaies d'or concédé par les rois de Jérusalem aux Vénitiens constituait l'enjeu d'une âpre bataille entre les familles ici et à Venise. Et pour l'heure, il semblait bien que les Gradenigo, dont l'influence et le pouvoir étaient fameux, ne se satisfaisaient pas de leur position subalterne.

Après quelques préalables polis, ils en vinrent assez rapidement au fait, le visage de Pietro alors barré d'une moue condescendante.

«&nbsp;C'est la famille Maureceno qui a la haute main sur l'atelier de frappe. Ils ne laissent guère de place aux autres.

--- J'aurais surtout besoin de savoir qui conserve les coins[^49], qui y a accès...&nbsp;»

Le jeune Vénitien lui adressa un regard aigu, esquissa un sourire, mais s'abstint de tout commentaire.

«&nbsp;C'est désormais Giovanni qui en a la charge, habituellement. Comme son oncle avant lui.

--- Le connaissez-vous bien&nbsp;? Est-il homme de bonne fame&nbsp;?

--- Il n'est ici que depuis peu, il a clamé son héritage voilà quelques mois, à peine débarqué du bateau.

--- Et son oncle&nbsp;? Comment était-il&nbsp;?&nbsp;»

Pietro s'esclaffa.

«&nbsp;On le surnommait Malvicino[^50], pensez-en ce que vous voulez&nbsp;! Il doit enseigner aux démons comment se bien comporter aux Enfers, désormais.

--- Quand est-il mort&nbsp;?

--- C'était peu après les Pâques de l'an passé[^51]. Il est tombé sous les coups de brigands sur la route depuis Panéas[^52] et a rendu l'âme sans que les mires[^53] n'y puissent rien faire.&nbsp;»

La date fit grimacer Bernard. Une telle coïncidence de calendrier, sur une voie qui menait à la région damascène l'incitait à penser que ce n'étaient pas là de simples détrousseurs. Le Vénitien avait été réduit au silence. S'était-il montré trop gourmand ou trop curieux&nbsp;? Bernard estima nécessaire d'en apprendre plus sur ce Giovanni et découvrir quels pouvaient être les contacts de son oncle, sans pour autant attirer l'attention. Le plus efficace serait peut-être de confier ces recherches à la Chaîne[^54] de la cité. Ils pourraient prétexter des contrôles fiscaux sur la succession et les frais douaniers afférents pour se faire transmettre le maximum d'archives. Il savait que, faisant cela il usurpait quelque peu l'autorité royale dont il était investi, mais après tout ne faisait-il pas tout cela également pour Baudoin&nbsp;?

### Jérusalem, quartier de la demeure d'Abu Malik, début de soirée du mardi 18 novembre 1158

Le tintamarre que faisait le chevalier en grande tenue sur son destrier fit fuir les enfants qui étaient encore attroupés près de la fontaine. D'un coup de rein, Bernard Vacher stoppa sa monture et en descendit avec aisance, fruit de dizaines d'années de pratique. Il était pourtant vermoulu et prit quelques instants pour s'étirer le dos, engoncé dans la cotte de mailles. Il n'avait pas lacé sa coiffe ni son heaume, mais tenait à voyager en armure, privilège de son rang. Il réinstalla correctement son fourreau sur la cuisse, cherchant du regard un des moins farouches parmi les gamins. Il s'en trouvait toujours un plus vaillant, heureux de rendre service en menant boire un cheval de prix.

Sa monture en de bonnes mains, Bernard Vacher se dirigea vers une des demeures et frappa à l'huis. Il se fit connaître à travers le guichet par le jeune valet d'Abu Malik et pénétra rapidement dans la salle principale où ne trônait qu'une petite lampe hâtivement placée par le domestique. Abu Malik était apparemment en train de manger, des odeurs de cuisine s'attachant encore à lui lorsqu'il entra à son tour dans la pièce. Il écarquilla les yeux en voyant un homme équipé de pied en cap dans son salon. Le chevalier lui adressa un sourire qu'il espérait engageant.

«&nbsp;Je suis désolé de venir ainsi sans m'annoncer, mais j'ai des nouvelles urgentes et voulais avoir votre avis sur la situation.&nbsp;»

Abu Malik ne fit pas de commentaires et invita Bernard à s'asseoir. Il donna quelques instructions pour qu'on leur porte de quoi se désaltérer et quelques friandises à grignoter. Puis il alluma d'autres lampes et, enfin, prit place sur un des coussins. Tout en s'affalant, Bernard Vacher lui expliqua qu'il avait enquêté à Tyr et en avait tiré quelques renseignements.

«&nbsp;Ce Malvicino a pu faire réaliser assez aisément de fausses monnaies, vu son libre accès à l'atelier.

--- C'est là bien grand risque !

--- Ce qui me conforte en cette idée, c'est qu'il avait de nombreux échanges avec un négociant damascène. Ils étaient en contact depuis des années et ont investi de belles sommes en commun. Je n'ai pu voir aucune lettre personnelle, mais il existait un lien très fort. J'aurais juste besoin de savoir si vous auriez connoissance de ce marchand&nbsp;: Sa'ad ad-Dīn 'Utmān.&nbsp;»

Le visage d'Abu Malik s'allongea instantanément. C'était une figure bien connue dans sa ville. Un homme très riche, qui s'était toujours tenu à l'écart de la politique. Il avait de nombreux amis parmi les fonctionnaires, sans pour autant privilégier jamais aucun parti. On le disait mécène de mosquées chiites et sunnites, et même d'églises chrétiennes ou de synagogues. Un opportuniste assez flamboyant.

«&nbsp;Qui ne le connaît pas dans le monde des affaires&nbsp;? Il est partout. C'est peut-être d'ailleurs pour cela qu'il est mentionné dans les comptes de ce Tyrien.

--- C'est un Vénitien, pas un sujet du roi de Jérusalem.

--- Cela me semble maigre comme lien. Je pourrais vous citer des quantités d'hommes faisant affaire avec Sa'ad ad-Dīn 'Utmān. Et je pourrais me porter garant de la majorité d'entre eux.&nbsp;»

Bernard Vacher grimaça. Il était conscient de ne pas avoir grand-chose, mais le faisceau d'indices lui paraissait suffisant pour aller creuser dans la cité syrienne. Il avait besoin de voir s'il existait un lien entre Sa'ad ad-Dīn 'Utmān et un quelconque responsable du versement du tribut.

«&nbsp;Le souci, c'est que vous trouverez forcément des échanges, des repas et des cérémonies où il aura côtoyé les hommes désormais en place. Je ne serais même pas surpris qu'il ait personnellement participé à la collecte du tribut. Lui ou un des hommes à son service, du moins.

--- Serait-il homme prêt à tout pour ravir pareille fortune&nbsp;?

--- On le dit immensément riche. Donc si votre question est&nbsp;: en a-t-il besoin&nbsp;? La réponse est alors non, sans nul doute. Mais si vous demandez s'il n'aurait pas désir de s'emparer d'un tel butin, alors je vous dirais que les hommes comme lui pensent en terme de négoce, d'apport à risquer et de bénéfice escompté. Rien de plus. Même leurs dons à la communauté, pour les pauvres, les captifs, sont pesés en ces termes. Ils paient pour ne pas être importunés dans la rue, pour être célébrés comme de grands hommes par les humbles qu'ils méprisent.&nbsp;»

Le chevalier sentait la rancœur qui suintait des propos. Abu Malik n'aimait guère ce genre de personnes, c'était flagrant. Il était pourtant lui aussi homme de commerce, vivait parmi eux. Lui faisaient-ils effet de miroir répulsif, salutaire en ce qui le concernait, vu qu'il s'efforçait au bien selon ses propres critères&nbsp;?

«&nbsp;Je vais tout de même aller jusqu'à Damas. Nous ne pouvons laisser ces serpents s'en tirer si aisément&nbsp;!

--- Soyez bien prudent. Depuis ma fuite, je n'y suis repassé qu'une fois, voilà quelques mois, et la situation est encore bien malaisée. Beaucoup de gens observent, attendent. Entre la maladie de l'émir, les tremblements de terre dans les territoires au nord, la ville semble prête à basculer à tout moment. Jamais je n'avais éprouvé pareil sentiment...

--- Les choses changent très vite en ce moment, de certes. Nos pères s'enhonteraient de voir comment nous soignons bien mal notre héritage.&nbsp;»

Abu Malik hocha le menton en silence. Il avait vu au fil des ans la situation se détériorer pour la cité damascène. De la capitale, demeure du calife qu'elle était dans l'ancien temps, elle n'était plus qu'une ville satellite du pouvoir turc, appétissant fruit que cherchaient à gober tous les puissants environnants.

«&nbsp;Si vous tenez à retourner là-bas, faites donc part de vos idées à mon ami al Zarqa' ou, à défaut à Yalim al-Buzzjani. Il vous faudra une protection si vous avez intention de mettre en cause un homme tel que Sa'ad ad-Dīn 'Utmān.&nbsp;»

Alors que Bernard Vacher se levait pour prendre congé, Abu Malik le retint d'un geste. Le Damascène le fixait avec intensité, à la recherche d'un message amical, d'une parole de réconfort. Les seuls mots hésitants qui franchirent ses lèvres n'étaient pas de lui.

«&nbsp;*Qui allume le feu doit savoir qu'il brûle*.&nbsp;»

### Damas, citadelle, début d'après-midi du youm al arbia 15 dhu al-hijjah 553[^55] {#damas-citadelle-début-daprès-midi-du-youm-al-arbia-15-dhu-al-hijjah-55355}

Bernard Vacher retrouvait les salles d'apparat dont il avait été familier du temps des bourides[^56]. Nostalgique, il se remémorait les âpres négociations qu'il avait dû mener là, face au talentueux Anur[^57]. Avec le temps, il en venait presque à regretter l'astucieux dirigeant damascène, malgré ses machiavéliques procédés. Voire, il en privilégiait le souvenir malicieux, les façons courtoises et les apparences amicales. Il en oubliait les habitudes de faux-semblants et les manipulations éhontées.

Ils ne s'arrêtèrent pas dans la grande salle d'audience, glaciale en cette période hivernale, pour se rendre dans un des cabinets attenants. Là, dans une douillette atmosphère alourdie d'encens boisé, était confortablement installé Nağm ad-Dīn Ayyūb. Il était en train de lire un imposant ouvrage posé sur un lutrin devant lui lorsque Yalim al-Buzzjani fut introduit, avec le chevalier franc à sa suite. Aucune parole ne fut échangée le temps pour chacun de prendre sa place. Les deux arrivants se présentaient debout, à quelque distance. Entre eux et le seigneur de la ville se tenaient deux hommes en armes, sabre et lance, casque en tête. Leurs cagoules de mailles ne laissaient voir que leurs yeux attentifs. Bernard Vacher savait qu'il serait découpé en tranche avant de pouvoir approcher de quelques pas.

Il redoutait cette entrevue à laquelle il ne s'était résolu que la mort dans l'âme. Avec l'absence d'al-Zarqa', Yalim al-Buzzjani était le seul contact qu'il avait pu joindre et celui-ci ne s'était guère montré coopératif. L'officier tenait à dévoiler l'affaire aux dirigeants de la cité, qu'il estimait en droit de régler le problème par eux-mêmes. La veille au soir, lors d'une violence dispute, Bernard Vacher avait fini par se rendre à ses arguments. Fût-il un grand chevalier et de bonne réputation, ce n'était pas à lui de se faire justice dans un territoire d'un prince étranger. Quand bien même il souhaitait que cela se fasse en toute discrétion, il ne pouvait faire l'économie de s'adresser à celui dont la main pouvait s'abattre de façon légitime.

La barbe grise, Nağm ad-Dīn Ayyūb était vêtu dans une tenue civile. Bernard Vacher le savait pourtant guerrier redouté et dirigeant efficace. Il avait réussi à s'infiltrer dans la région de Damas avec talent, préparant le terrain pour son maître Zengī[^58] puis, avec l'aide de son frère Shīrkūh, pour Nūr ad-Dīn[^59]. Ses traits émaciés caractérisaient un homme d'action plus que d'ascèse, bien qu'on le prétendît pieux. Mais les laudateurs avaient beau jeu de toujours louer la piété des puissants, dans l'espoir de les voir lâcher quelques subsides grâce à la charité qu'ils célébraient. Sa tenue, une magnifique robe d'honneur, était d'une splendide couleur orangée, la soie se parant de mille reflets. Sur sa tête, un imposant turban était orné d'une broche d'argent sertie de pierres.

Ce fut al-Buzzjani qui résuma l'affaire, expliquant ce qui était arrivé au tribut et les recherches de Bernard Vacher, d'une façon qui suggérait que tout cela était entièrement nouveau pour le gouverneur. Le chevalier n'en était pas dupe un seul instant, l'entrevue ayant certainement été préparée avec grand soin. Il était familier de ces circonvolutions diplomatiques et les subit avec patience, admirant les décors des tapis, des pièces d'orfèvrerie et des céramiques qui parsemaient la salle.

Il nota néanmoins que l'officier ne fit aucune mention des intermédiaires qui les avaient mis en contact. En cachant ses sources, le soldat protégeait ceux qui lui avaient fait confiance. Ou se ménageait un levier d'action dans le cas où il aurait à se défendre contre des agissements dangereux pour lui. Bernard Vacher espérait que l'émir était plutôt enclin à fonctionner selon la première hypothèse.

La voix grave et légèrement nasale du gouverneur résonna après un long moment de silence.

«&nbsp;Ton nom et ta réputation t'ont précédé ici, al-Baqara. J'entends donc ce que tu me révèles avec la plus grande attention.&nbsp;»

Il se lança ensuite dans une diatribe contre les félons qui avaient mis pareillement en péril la situation entre leurs territoires. Il ne pouvait préjuger de ce que l'émir déciderait, mais des actions seraient prises rapidement pour tirer tout cela au clair. Il ne posa pas de questions, mais tint à assurer le chevalier que les coupables seraient dénichés et punis. Lorsque son tour vint de parler, Bernard Vacher avait une demande qui lui brûlait les lèvres.

«&nbsp;Établir de nouvel bonnes relations entre nos princes me semble chose importante. N'hésitez pas à mander un de vos émirs porteur de la bonne nouvelle d'une plaisante conclusion auprès de mon roi.

--- Tu pourras déjà l'assurer de ma plus parfaite collaboration en cette affaire. Pour le reste, ce sera à l'émir de dire ses volontés.&nbsp;»

Après quelques échanges formels, le chevalier fut invité à se retirer. Il devait s'en remettre à la puissance du gouverneur pour la suite de l'affaire. Alors que Yalim s'apprêtait à sortir à son tour, il fut retenu par le prince d'un simple mouvement de menton. Ce fut donc un des gardes qui mena Bernard Vacher au-dehors.

L'émir attendait, un peu fébrile. Il n'aimait guère la tension qu'il lisait sur le visage de Nağm ad-Dīn. Le gouverneur croqua une douceur à la pistache, élaborant une stratégie maintenant qu'il avait tous les éléments en main et qu'il s'était fait une idée sur les différents protagonistes de l'affaire. Il prit la parole comme à dépit.

«&nbsp;Cet homme n'est pas envoyé par son roi. Il cherche justice pour lui, rien de plus.&nbsp;»

Il se rinça la bouche d'une boisson liquoreuse aux fruits puis d'un geste, indiqua à son émir de s'approcher.

«&nbsp;J'ai entendu le désir que tout cela demeure secret. C'est bien volontiers que je me rangerai à cette idée. Veille à cela, al-Buzzjani.&nbsp;»

Appuyant son ordre d'un regard lourd de sens, il congédia le soldat d'un geste des doigts avant de reprendre une pâtisserie.

Lorsqu'il franchit le seuil de la salle, Yalim contracta les poings, serra la mâchoire. Que de dédain dans cette condamnation à mort lancée entre deux bouchées&nbsp;! ❧

### Notes

La conclusion de cette mini-trilogie permettra à chacun, je l'espère, de mieux appréhender la mosaïque des pouvoirs alors qu'Ernaut débarque en Terre sainte et y prend ses marques. J'ai essayé de donner une image riche et la plus fidèle possible des puissants en place et des rapports de force entre eux, à travers une anecdote. Ce sont eux qui vont canaliser les grands mouvements des décennies suivantes.

J'ai aussi également tenté de regrouper certains faisceaux narratifs éparpillés au gré des publications, de montrer des liens entre certains personnages instillés au fil des Qit'a. La dispersion des récits n'est qu'apparente et chacun d'eux peut, à un moment donner, faire basculer le destin d'un autre de façon tragique. Je sème assez souvent des indices très légers, je tisse des relations distendues, parfois sans les nommer explicitement, de façon à laisser le lecteur libre de renouer les fils selon sa fantaisie. J'espère avoir l'occasion à l'avenir de développer encore plus ces trames secondaires. Ou peut-être qu'un auteur aura l'envie de le faire, la possibilité en est ouverte avec la licence que j'utilise pour publier mes travaux.

L'idée de cette mini-série de Qit'a est née d'un simple jeu de mots. J'avais le désir de raconter ce qui aurait pu arriver à Bernard Vacher. Son nom me souffla que c'était là une histoire de vacher, c'est-à-dire de *cowboy*. Je tenais une piste pour les titres et les ambiances, grand amateur de Sergio Leone et des westerns crépusculaires de Clint Eastwood que je suis.

### Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tomes I, II et III, Damas&nbsp;: Institut Français de Damas, 1967.

Gibb Hamilton Alexander Rosskeen, *The Damascus Chronicle of the Crusades, extracted and translated from the Chronicle of Ibn al-Qalânisî*, Dover Publications, Mineola, New York, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I, *Economic Foundations*, Berkeley et Los Angeles&nbsp;: University of California Press, 1999.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. IV. The Cities of Acre and Tyre*, Cambridge&nbsp;: Cambridge University Press, 2009.

Schlumberger Gustave-Léon, *Numismatique de l'Orient Latin*, paris&nbsp;: Ernest Leroux, 1878-1882.

Désert
------

### Sinaï, camp de bédouins, matin du lundi 22 septembre 1158

Les fesses dans la poussière, ligoté à un poteau, Droin attendait. Plusieurs fois il avait vu la nuit succéder au jour. Il avait pesté, s'était enflammé, agité, avait cherché à se libérer. Puis il s'était résolu. Il patientait, planté au milieu de toiles gris brun, au sein d'une oasis formée par quelques maigres arbustes et une poignée de broussailles racornies par le soleil. Chaque jour on lui apportait une ration d'eau croupie dans une outre en peau de chèvre, avec une gamelle de riz assaisonné de quelques légumes. Mais jamais on ne lui parlait. Chacun des nomades autour de lui semblait le considérer comme quantité négligeable. Seuls les enfants tournaient parfois vers lui des regards curieux ou moqueurs. Pour autant, aucun ne s'approchait. Au vu de tous, il était surveillé par chacun.

Il connaissait désormais les rituels du lieu. Le matin, il voyait les femmes émerger les premières des tentes, se regrouper autour de la source, échangeant à voix basse tout en puisant l'eau. Puis quelques-unes activaient un petit feu où confectionner les repas. Pendant ce temps, tandis que quelques gardiens nocturnes venaient s'étendre sur les nattes, la plupart des hommes allaient s'occuper des bêtes&nbsp;: chameaux, chèvres, moutons et quelques chevaux.

Ensuite apparaissaient les enfants qui couraient ici et là, affairés à rendre de menus services ou à s'amuser entre eux. Ils se regroupaient de temps à autre sous une des grandes toiles au plus près d'un arbre vénérable aux branches noueuses et écoutaient attentivement un vieillard à la barbe chenue. Même le désert recèle des clercs pour gêner les jeux, ironisa Droin.

À la mi-journée, hommes et femmes, chacun de leur côté, partageaient le repas entre amis avant une sieste. Puis les travaux reprenaient tranquillement au long de l'après-midi, selon le courage et les compétences de chacun. Parfois un cri résonnait et les mains empoignaient arc, lance, poignard ou épée. Sans que rien ne pénètre dans le périmètre. Puis le soir venait, où le souper, toujours léger et rapide, se prolongeait en veillée entre voisins, familiers ou amis. Enfin, de nouveau les étoiles scintillaient sur le camp endormi, en compagnie de quelques hommes qui déambulaient, fantômes parmi les ombres.

On lui avait donné une vieille robe déchirée, vêtement et couverture pour les nuits fraîches. Il devait se satisfaire du gravier et du sable en guise de matelas et d'oreiller. Il s'en moquait, sa vie avait sans cesse été ainsi, d'inconfort. Toute son enfance, il l'avait partagée avec les cochons que son père gardait. Ses frères et lui occupaient la cabane où les animaux étaient enfermés. Malgré les années, il en détestait toujours autant l'odeur. Il en haïssait également les cris et les grognements de contentement quand on les nourrissait.

Un petit groupe se matérialisa soudain autour de lui. Sans une parole, on le libéra de ses entraves et il fut incité vivement, mais sans violence, à les suivre. Il comprit très vite qu'on le menait à la plus grande des tentes, où se tenaient plusieurs bédouins au port altier. Au centre de l'assemblée, un homme au visage strié de rides, les yeux comme des fentes, tirait sur les poils de sa barbe. Il ne semblait pas mieux vêtu que les autres, ni plus riche, mais il était évident que tous ici le considéraient comme un chef. Le baron du coin pensa Droin.

Il s'assit, environné de son escorte, face au vieillard. Un temps passa, les regards échangés établissant les rapports de force entre les présents. Plus on se trouvait auprès du centre de gravité du pouvoir, plus les postures étaient hiératiques, les bouches pincées et l'attitude empreinte d'importance. Le vieux se contentait de tirer sur les poils de son menton, les plis au coin de ses paupières s'accentuant à peine par instant. Un cri de rapace emplit le ciel, des bêlements roulèrent jusqu'à eux, des rires d'enfants jaillirent non loin, puis une voix éraillée, presque un souffle, se fit entendre.

«&nbsp;Quel est ton nom, étranger&nbsp;?&nbsp;»

Le ton était ferme, sonore et autoritaire, mais sans agressivité. Droin se revit quelques années en amont, alors qu'ils étaient appelés à tour de rôle par le cellérier du monastère pour verser la dîme. Son propre timbre ne lui parut pas aussi assuré qu'il l'avait espéré. Les journées de silence avaient érodé sa diction.

«&nbsp;Droin de Vézelay.&nbsp;»

Il avait hésité à mentir, puis n'en vit pas l'intérêt. Nul ici ne pouvait savoir ce qu'il avait fait ni le dénoncer aux hospitaliers. Il était en dehors du monde connu.

Le scheik hocha le menton, déglutit lentement, tourna la tête autour de lui avant de reprendre.

«&nbsp;Peux-tu me conter ce qui t'a mené là où nous t'avons trouvé&nbsp;?&nbsp;»

Droin ne comprenait pas où l'échange pouvait le conduire, mais ne vit aucun souci à conter par le menu ses aventures. Il ne cacha pas qu'il avait quitté les religieux en désaccord profond avec eux, mais en négligeant de reconnaître clairement qu'il était légalement en fuite. Il ne voulait pas tenter les appétits de ces nomades dont on disait le plus grand mal dans les cités. Tandis qu'il parlait, revivant par le récit l'étrange parcours qui l'avait mené en ce lieu désolé, il prit conscience combien il s'était affranchi de ses origines. Cette pensée le réconforta et il termina son histoire avec bien plus de vigueur qu'il n'avait commencé. Après un court silence, les hommes face à lui échangèrent tranquillement à voix basse. Il se demanda combien ici connaissaient sa langue et avaient compris son exposé.

«&nbsp;Tu dis que tu as pris le chameau pour vous sauver, toi et l'homme que tu as trouvé&nbsp;?

--- Oc. Enfin, c'est plutôt lui qui m'a soufflé de le prendre, parce que moi, les chameaux...&nbsp;»

Il ponctua sa fin de phrase d'une grimace éloquente, qui lui attira quelques sourires dans l'assistance.

«&nbsp;Et que comptais-tu faire de l'homme&nbsp;?

--- Je sais pas. J'allais pas le laisser mourir comme une bête, quand même. J'ai guère eu le temps d'y penser. je voulais surtout trouver un abri. Et ensuite, rejoindre une cité ou l'autre... Il va comment, d'ailleurs&nbsp;? C'est un des vôtres&nbsp;?

--- Sais-tu seulement où tu es&nbsp;?

--- En plein dans votre fief, on dirait bien&nbsp;!&nbsp;»

La répartie sembla amuser le scheik. Droin aimait faire le fier-à-bras, gardant le menton haut et le regard plein de morgue.

«&nbsp;L'homme que tu as aidé s'appelle Nijm. Il se remet doucement, mais demeure bien faible. Il n'a ouvert les yeux que ce matin.&nbsp;»

Le scheik abandonna sa barbe pour joindre les deux mains de façon un peu solennelle.

«&nbsp;Il ne se souvient guère de ce qui s'est passé, mais rien dans son récit ou le tien ne semble indiquer mauvaises intentions de ta part à son égard. Tu peux donc te considérer comme notre hôte.&nbsp;»

Il désigna un moustachu au nez busqué, aux yeux ronds agités.

«&nbsp;Voici Abu Bā'z. Il s'occupera de toi le temps que tu passeras parmi nous.&nbsp;»

Abu Bā'z offrit un sourire aux dents jaunes.

Avant que Droin n'ait eu le temps de composer une réponse, tous les présents étaient levés et commençaient à s'éloigner. Seul le scheik demeurait assis, prenant ses aises pour une sieste, désormais complètement indifférent à ce qui se passait autour de lui. Abu Bā'z posa une main ferme, mais amicale sur l'épaule de Droin, puis, d'un geste, l'invita à le suivre. Un peu décontenancé, le jeune homme se vit arpenter les lieux qui lui étaient jusqu'alors interdits. Ils se rendirent à une petite tente près d'un muret en partie écroulé, ultimes reliefs d'un très ancien habitat. Abu Bā'z semblait déterminé à rattraper toutes les journées de silence contraint de Droin&nbsp;: il était volubile, parlait de tout et de rien, s'amusait de la plus formelle des réponses.

Il était éleveur, comme tous ici, et caravanier à l'occasion. C'était ce qu'il préférait faire, ayant alors la possibilité d'échanger des histoires avec ceux qu'il escortait. Il mélangeait parfois les langues et Droin devait lui rappeler de temps en temps qu'il était un Franc.

Sous la tente se trouvait Nijm, en train de dormir. Sans plus d'égard, Abu Bā'z s'installa sur une des nattes et sortit du matériel de bourrellerie d'un sac.

«&nbsp;Un de mes chameaux a brisé sa sangle&nbsp;» précisa-il avant de se mettre au travail. De l'autre côté d'une toile divisant l'espace en deux s'affairaient des silhouettes féminines, allant et venant au fil de leurs activités. Une fillette déposa devant Droin un paquet. Avisant le regard interrogateur du jeune homme, Abu expliqua&nbsp;:

«&nbsp;Tu as là un vêtement chaud pour la nuit, un turban et menues affaires qui te seront utiles.

--- Je n'ai pas de quoi payer, s'inquiéta Droin.

--- Vous êtes drôles, vous autres habitants à ciel couvert. Vous n'avez aucun respect de ce qui compte vraiment, mais voulez parfois payer ce qui vous est dû. *Je donne une datte au pauvre, pour en goûter la vraie saveur*.&nbsp;»

Puis il ricana doucement, tout en s'affairant à réparer sa courroie. Droin fouilla dans le sac et y trouva quelques objets de toilette et un petit couteau. Il s'empressa d'en nouer le fourreau à son braïel[^60]. Voyant son geste, Abu lui sourit et alla lui chercher un autre paquet.

«&nbsp;On a aussi nettoyé ton épée. Une belle lame, même si elle a été difficile à nettoyer. Elle méritera plus belle gaine que ces chiffons&nbsp;!&nbsp;»

### Sinaï, camp de bédouins, après-midi du mercredi 8 octobre 1158

Les effluves des chèvres parquées dans un enclos de buissons et de pierres sèches venaient se mêler à l'odeur irritante de la poussière soulevée par le vent. Droin n'y prêtait plus guère attention, profitant du début de l'après-midi pour se reposer à l'ombre de la toile. Il avait accompagné Nijm pour une courte balade autour du camp. Le bédouin se remettait à une vitesse étonnante, capable désormais de marcher sur les terrains accidentés alors même que ses blessures étaient à peine refermées. Chaque jour une des femmes venait nettoyer les plaies et changer ses pansements, enduits d'un emplâtre agressant le nez.

«&nbsp;Je pense que nous pourrons rejoindre ma tribu d'ici une lune, confia Nijm, indolemment allongé sur le paquet de ses affaires.

--- C'est loin d'ici&nbsp;?

--- Il nous font aller au Nord, vers le cœur des territoires de Badawil[^61]. Les miens sont le plus souvent autour de Bahr-Lt[^62] en cette saison, à récolter le sel avant de bientôt partir sur les zones de pâturages.&nbsp;»

Droin fit mine de comprendre et se renferma. Il n'était pas enthousiasmé à l'idée de retrouver trop vite les territoires qu'il avait fuis. Il n'avait aucune idée de la véhémence des hommes de Saint-Jean[^63], mais s'il devait se fier à la pugnacité des religieux de Vézelay, il lui faudrait se faire oublier un moment. Il demeura silencieux, d'une brindille traçant des motifs dans le sable.

«&nbsp;Tu connais la princée d'Outre-Jourdain&nbsp;?

--- Nous y allons parfois, pour compaigner des caravanes. De nombreux frères de mon clan habitent ces lieux. Nous savons bien négocier avec eux le passage.

--- Je me disais que j'irais bien là-bas. Le baron du coin édifie vaste citadelle et fait bon accueil aux colons.

--- J'ai vu les murs du karak al-Shawbak[^64] partir à l'assaut des cieux. Les tiens ne sont tant heureux qu'ils empilent deux pierres l'une sur l'autre.&nbsp;»

Sa remarque, nuancée d'un sourire, se voulait sans malice. Droin commençait à apprécier ce nomade aux yeux tristes et aux silences éloquents. Peut-être n'était-ce que le fait de sa convalescence, mais il savourait cette distance maintenue entre eux, ce respect de son espace personnel. Il n'y avait que bien rarement goûté.

Nijm s'empara d'un pichet, accompagnant le mouvement d'épaule d'une grimace. Jamais il ne se plaignait, mais laissait échapper des rictus qui en disaient long sur les souffrances qu'il endurait. Après avoir avalé une rapide gorgée, il s'affaissa de nouveau. Dehors, des gamins riaient en poussant du pied une boule de bois. Droin lança un furtif regard vers un groupe de femmes occupées à cuire des pains plats, non loin d'eux.

Les bédouins avaient proposé aux deux hommes une petite toile à eux, à côté de celle d'Abu Bā'z, où chaque jour un enfant ou Abu lui-même parfois, venait leur apporter de quoi manger. Jamais on ne leur avait demandé quoi que ce soit. Droin s'étonnait de cette libéralité alors que la tribu lui semblait à peine plus riche que sa propre famille. La plupart allaient dans un vêtement élimé, sans chaussure, les plus jeunes à demi nus. Il leur aurait à peine accordé un regard s'il les avait croisés aux abords d'une des villes.

Il détailla son sac, sa couverture, les nattes sous lui et la tente au-dessus de sa tête. Il ne manquait même pas une petite lampe à graisse pour y voir au plus noir de la nuit. Et tout cela leur avait été fourni sans un mot, sans aucune demande en retour.

«&nbsp;Ici, ce sont gens de ton peuple&nbsp;? demanda-t-il à Nijm.

--- Ce ne sont pas des al Fadl ni même des Banu Tayy[^65], mais nous avons bonne entente avec eux. Nous échangeons souvent le serment du pain et du sel entre nous.&nbsp;»

Une fois encore, il ne comprenait pas ce dont son compagnon parlait, mais acquérait une image plus large de l'Outremer. Le terme lui paraissait bien étrange, perdu au milieu des roches et du sable. Tout autour de lui ce n'étaient que dunes, graviers et poussière. À perte de vue un monde qui semblait minéral, mais qui regorgeait de vie pour celui qui prenait le temps de l'écouter, de l'observer. Et aux abords des sources, c'était une explosion végétale, teintant d'émeraude un univers en dégradé ocre.

«&nbsp;Je peux te guider jusqu'à la forteresse celte, proposa Nijm. D'ici, en partant au levant, nous trouverons Wadi Musa[^66]. De là, une piste régulière mène au nord, vers les miens.&nbsp;»

Leur échange s'interrompit quand une petite fille vint déposer un panier contenant du pain tout chaud de la fournée. Il était accompagné d'un pot de lait caillé et de fromage frais agrémenté de quelques rares olives. Ils se distribuèrent le repas puis mangèrent en silence. Tout le camp s'était arrêté un moment, laissant les bruits du désert s'imposer le temps que les estomacs se remplissent. Pour Droin, la portion paraissait congrue alors même qu'il avait remarqué qu'il était plutôt bien traité. Seulement, son appétit semblait plus important que celui de ces austères bédouins. Il commençait par ailleurs à s'habituer aux saveurs prononcées du fromage, des boissons. Quand ce n'était pas issu de la traite des chamelles, c'était tiré du pis des chèvres. Droin sentait en permanence les odeurs douceâtres des produits laitiers qu'on consommait tout au long de la journée imprégner jusqu'à ses propres vêtements.

Il avala sa dernière bouchée, s'essuya la main de sa manche puis se désaltéra longuement. Il n'avait pas non plus la capacité des nomades du désert de se rafraîchir d'une courte rasade. Il s'installa alors pour une sieste, mais avant de fermer les yeux, il se tourna vers Nijm, qui s'apprêtait à faire de même.

«&nbsp;J'irai avec toi vers le levant jusqu'à la cité du prince d'Outre-Jourdain, ainsi que tu me l'as proposé. Je verrai si je peux y trouver quelque manse qui manquerait de bras pour fructifier.&nbsp;»

### Cité antique de Pétra, Wadi Sabra, soir du jeudi 23 octobre 1158

Droin somnolait sur sa monture, fatigué et vermoulu de la journée demeuré en selle. Ils avaient fait une belle pause à la mi-journée, plus par habitude de son compagnon que par nécessité, mais cela n'avait pas suffi à lui faire retrouver des sensations normales dans son arrière-train. Il n'était plus aussi mal à l'aise avec les chameaux que par le passé, mais il avait toujours préféré voyager sur ses pieds. Pour lui, les animaux manifestaient un caractère au mieux un peu trop facétieux pour apprécier de se trouver sur leur dos.

Ils longeaient un passage caillouteux, une pente encombrée qui se hissait entre deux versants aux crêtes orangées. Ils remontaient le côté opposé de la vallée qu'ils venaient de franchir, séparant deux énormes massifs rocheux. Nijm tendit le bras vers le contrefort à leur gauche.

«&nbsp;En haut du wadi, nous prendrons étroit sentier taillé dans la roche. Nous serons alors arrivés. Les tribus sont souvent installées là. Mes cousins nous feront bon accueil.&nbsp;»

Ils parvinrent en effet devant une étroite faille qui paraissait déchirer la pierre en un mince fil de ténèbres. Les dernières lueurs du jour n'y apportaient aucune lumière. Alors qu'il pénétrait à la suite de Nijm dans le goulet, Droin contracta les épaules, s'attirant un regard désapprobateur de sa monture, gênée par le mouvement sur ses rênes tandis qu'ils disparaissaient entre les hautes murailles. Il lui semblait qu'il pourrait toucher les deux côtés du passage et hésitait à le faire, par peur de découvrir quelque chose qu'il n'apprécierait pas.

Les pas lourds de leurs chameaux faisaient résonner les graviers roulant à chaque enjambée. Droin avait l'impression qu'une caravane de bédouins s'était engouffrée à leur poursuite. La plus petite de ses respirations lui faisait l'effet d'un vacarme assourdissant et sa monture grondait comme une armée de dragons en furie. Ils débouchèrent heureusement assez vite sur une large esplanade qui s'étendait jusqu'aux pieds d'immenses falaises de roche stratifiée, déchiquetée par les vents et le sable du désert. Plusieurs passages tranchaient les murailles et des grottes perçaient d'ombres les parois. En de nombreux endroits, des feux scintillaient. Nijm se rapprocha de lui, un sourire faisant briller ses dents dans l'obscurité qui s'installait.

«&nbsp;Ce soir, avec un peu de chance, nous partagerons le mensaf[^67] avec les miens&nbsp;!&nbsp;»

Il talonna son chameau et s'élança joyeusement dans la poussière, suivi par un Droin, moins enthousiaste, quoique satisfait de décoller ses fesses de là où elles se ratatinaient depuis des heures.

### Cité de Kérak, zone des jardins occidentaux, début d'après-midi du vendredi 31 octobre 1158

Arrêtés à l'abord du croisement qui montait vers la porte du Burj az-Zahar[^68], Nijm et Droin partageaient quelques dattes dont ils crachaient les noyaux dans la poussière. Sur la restanque au-dessus d'eux, des familles s'employaient à ramasser les olives. Les plus jeunes et les plus intrépides grimpaient aux branches, y dénichaient les plus beaux fruits tout en laissant les plus murs tomber par eux-mêmes au sol. Là, les plus âgés les saisissaient par poignées, les lançant en l'air pour en souffler les feuilles mélangées avant de les jeter dans des paniers. L'ambiance était à la fête, les cueilleurs s'invectivant d'une parcelle à l'autre, quand ils n'entonnaient pas en chœur une chanson rythmée.

Au-dessus de ces terrains soigneusement entretenus se dressaient les murailles ambrées de la cité de Kérak. La ville où les deux hommes avaient convenu de se séparer. Droin repartait mieux doté que lorsqu'il avait fui dans le désert et sa tenue n'avait plus rien de latine. Il avait même abandonné les chaussures et n'enfilait ses savates que sur les plus agressifs des sols. Il avait regroupé toutes ses affaires dans une hotte et trois sacs, mais avait refusé le chameau que Hijm voulait lui offrir. Il n'avait aucune sympathie pour les animaux et n'était pas à l'aise à l'idée de revendre pareil cadeau. Il était trop peu familier des dons pour les accepter sans réticence.

Il tourna son regard vers le sud, où la forteresse continuait de s'édifier. On voyait des structures de bois suspendues au-dessus du vide, où des hommes s'activaient tels des insectes. Il se demandait quel effet cela pouvait bien faire d'embrasser le territoire alentour depuis les terrasses sommitales. Le paysage devait être à couper le souffle. Il pourrait s'y faire embaucher comme portefaix, histoire de satisfaire sa curiosité, pensa-t-il.

Nijm puisa un peu d'eau dans un bassin et se frotta rapidement le visage. Il tourna le regard vers l'ouest, en direction de l'oued qui mordait le haut plateau avant de s'écouler dans la vallée de la Mer Morte. Il semblait impatient de repartir, de retrouver les siens. Droin l'enviait d'être à la fois si libre et si bien intégré dans une communauté. Il s'était fait pèlerin et colon par dépit, afin de fuir une famille qu'il n'aimait guère.

«&nbsp;Tu es certain de ne pas vouloir garder cette bête&nbsp;? Elle est à toi, je te l'ai donnée.

--- Je crois qu'elle n'est guère en accord avec cela, ricana Droin. Non, je te mercie, compère, mais je ne suis pas à l'aise avec ces bêtes. Elles ont aussi mauvais caractère que les porcs, et je n'ai guère d'amitié pour ces derniers.

--- À tant les décrier, tu resteras en ma mémoire comme al-Nazf[^69], mon ami&nbsp;» se moqua Nijm.

Puis il fouilla dans un de ses sacs de nourriture, y déchira deux petits morceaux de pain qu'il frotta du sel sorti d'une boîte et en tendit un à Droin. Celui-ci, un peu surpris, accepta.

«&nbsp;Si tu l'acceptes, j'aimerais que nous prêtions serment. Tu m'as sauvé alors que je n'étais rien pour toi, soyons donc comme des frères dès cet instant.&nbsp;»

Un peu pris au dépourvu, Droin accepta sans trop y réfléchir et répéta à la suite du bédouin la formule que ce dernier lui traduisit dans sa langue.

«&nbsp;Par le pain et le sel, je ne te trahirai pas.&nbsp;»

Une fois cela fait, Njim lui sourit avec chaleur et lui donna une accolade bourrue. Puis il alla rattacher soigneusement ses affaires sur sa monture.

Un vieil homme assis sur un âne bardé de paniers d'olives ralentit pour les saluer d'un signe de son couvre-chef de guingois. Droin en profita pour l'apostropher. Comme le fellah parlait sa langue, il en espérait quelques renseignements sur la cité.

«&nbsp;Sais-tu si on trouve embauche en la citadelle&nbsp;?

--- Oh, il y a toujours de l'ouvrage pour la main habile, mais les hommes noirs ne paient guère, de ce qu'on dit. Ce sont eux qui agrandissent le rempart.

--- Des hommes noirs&nbsp;?&nbsp;»

Nijm intervint et échangea quelques paroles en arabe avec le vieux paysan, avant de traduire pour Droin.

«&nbsp;Ce sont les prêtres de vos cités, qui s'habillent de noir et nourrissent les pèlerins. Ils s'arment de plus en plus au fil des ans. On les rencontre souvent avec les hommes de la Milice du temple.&nbsp;»

Droin en fit grincer ses dents de mécontentement. Il fallait qu'ils viennent jusqu'ici, en plein désert. Nijm surprit son désarroi et, d'un regard, l'interrogea.

«&nbsp;Tu accepterais de me garder avec toi encore un moment&nbsp;?

--- Nous avons prêté le serment du pain et du sel. Partout où je serai, ma tente sera tienne.&nbsp;»

Droin haussa les épaules, admirant une nouvelle fois les hautes murailles. Quand on n'avait rien à perdre... Lorsqu'il se retourna, Nijm arborait un visage hilare, lui adressant un clin d'œil tandis qu'il lui tendait les rênes de son chameau. ❧

### Notes

Les populations bédouines font partie des grandes inconnues des sources médiévales. On ne les cite que de façon périphérique et les chroniqueurs y apportent rarement beaucoup d'attention. Ils représentaient pourtant une fraction importante des territoires où les pouvoirs latins et syriens s'affrontaient. Et contrairement à l'opinion professée par Dom Raphaël dans la traduction de F.-J. Mayeux dans ses textes du début du XIXe siècle, leur culture a beaucoup évolué avec le temps, sans parler des tribus et des clans qui ont migré sur de vastes zones. J'ai choisi néanmoins de me servir d'anciennes sources de voyageurs de ce type pour aller à leur rencontre, car elles regorgent d'anecdotes à propos de populations avant qu'elles ne soient imprégnées par le style de vie occidental.

Le titre de ce *Qit'a* est en rapport avec le fabuleux *Désert* de Jean-Marie Gustave Le Clézio, qui fut pour moi un vrai choc lors de sa découverte voilà des années. C'est lui qui m'a fait comprendre ce qu'était une expérience esthétique de lecture. Par ailleurs, ce grand écrivain a une sensible appétence pour l'ailleurs et y fait allusion aussi bien dans ses interviews que dans ses textes. Son talent pétri de curiosité littéraire et culturelle m'a montré une voie que j'essaie d'arpenter par mes propres moyens.

### Références

Bonnenfant Paul, «&nbsp;L'évolution de la vie bédouine en Arabie centrale. Notes sociologiques&nbsp;», dans *Revue de l'Occident musulman et de la Méditerranée*, n°23, 1977, p. 111-178.

Cowan gregory, *Nomadology in architecture. Ephemerality, Movement and Collaboration*, Dissertation for master in Architecture, University of Adelaide, 2002.

Le Clézio Jean-Marie Gustave, *Désert*, Paris: Gallimard, 1980.

Mayeux F. J., *Les bédouins ou Arabes du désert*, 3 tomes, Paris&nbsp;: Ferra jeune, 1816.

Semper ad altum
---------------

### Ascalon, camp de siège, après-midi du samedi 15 août 1153

Craquements et grincements du bois donnaient l'impression à Arnulf qu'il était à bord d'un navire. Appuyé contre un des bastaings de structure du beffroi d'assaut, il faisait évacuer les hommes, dont certains sautaient carrément depuis la plateforme la moins élevée, négligeant les échelles. La chaleur n'était pas encore perceptible à l'intérieur, protégés qu'ils étaient par les parois de vannerie et de boue, mais certaines flammes s'insinuaient parfois. Autour de lui, au second étage, étaient entassés des tonneaux de flèches que deux sergents balançaient par brassées le plus loin possible. Il fallait tenter de sauver ce qui pouvait l'être.

Arnulf sentait l'approche des fumées épaisses plus qu'il ne les voyait et se frotta le nez. Devant lui, au sol, se répandait l'arsenal qu'ils avaient patiemment amassé avant de faire avancer la tour vers les murailles. La haute plateforme leur donnait un avantage pour contrôler le chemin de ronde de la cité d'Ascalon sur tout un pan de l'enceinte. Mais les défenseurs avaient réussi à embraser le bois, grâce à un quelconque mélange secret de leurs ingénieurs. Arnulf enrageait de n'avoir pu mener l'assaut.

Lorsque les dernières armes furent jetées au loin, il fit signe aux deux hommes de descendre au plus vite. Le plafond au-dessus d'eux se voyait léché de flammèches de plus en plus aventureuses. La chaleur serait bientôt intenable. De là où il était, il surplombait les abris et les tentes, les mantelets mobiles et les bricoles du camp royal. Il espérait qu'il restait assez de bois sur le bateau dont la carcasse gisait sur la plage. Qu'ils pourraient y puiser de nouveaux matériaux.

Équipé de son haubert, casque en tête et bouclier au dos, il dévala lourdement les échelles, retrouva le sol, où la température n'était plus aussi intense. Avançant rapidement parmi les barricades légères, il rejoignit un bosquet de palmiers non loin de là, d'où il avait supervisé l'assaut. Il soupira, s'empara d'une outre qu'on lui tendait et délaça son heaume, avala une longue rasade. Il se rinça le visage, les mains noires.

Un violent incendie grondait désormais au niveau le plus haut de la tour, accroissant encore la sensation de chaleur. Sur sa gauche, une unité de cavalerie et des fantassins lourdement équipés patientaient depuis la zone affectée à la Milice du temple. Le roi et le connétable craignaient que les assiégés ne profitent de l'occasion pour tenter une sortie. Il lança un coup d'œil sur sa droite, vers l'horizon maritime. Les voiles de la flotte latine étaient toujours en place, faseyant mollement dans la légère brise de terre.

Les hommes se hâtaient de récupérer l'armement et de le regrouper à l'abri. Les plus audacieux se risquaient un peu trop près des murailles à son goût. Il grogna quelques ordres à un des soldats à son service, qu'il fasse reculer ces inconscients. Le sergent qui l'avait suivi jusque là sans un mot haussa les épaules.

«&nbsp;Le vent pousse au sud, mon sire. Les mahométans auront le cuir grillé s'ils s'approchent pour flécher nos gens.&nbsp;»

Arnulf dévisagea le jeune homme. C'était un des derniers à avoir quitté le beffroi avec lui. Il l'avait remarqué déjà, pour son application et sa vaillance, qui confinait parfois à la témérité. Plissant les yeux, il tourna le regard de nouveau vers les murailles ennemies. Elles étaient en effet vierges de défenseurs au plus près de la tour, dont le brasier s'intensifiait. Un sourire naquit sur ses lèvres. Il tapa sur l'épaule du jeune sergent&nbsp;:

«&nbsp;Garçon, tu parles de raison. Dieu nous offre l'occasion de leur rendre la politesse. Fais assavoir aux hommes du sire connétable que nous allons tenter de faire de cet échec un bienfait.&nbsp;»

Il fit corner le rassemblement et expliqua en quelques phrases lapidaires son plan&nbsp;: pousser la tour au plus près des murs avant qu'elle ne soit complètement en flammes. Avec le vent du nord qui se levait, l'incendie deviendrait leur allié, éclatant les pierres et écartant les défenseurs. Au moment d'entraîner les hommes avec lui, il fit signe au jeune sergent de s'approcher.

«&nbsp;Quel est nom, garçon&nbsp;?

--- Fouques, mon sire.

--- D'où viens-tu&nbsp;?

--- Je suis né à Césaire[^70], sur la côte. Mon père était mesureur de blé[^71].

--- Un enfant du royaume... Tu sers l'hostel du roi depuis longtemps&nbsp;?

--- Cela fait bientôt quatre ans, mon sire. J'ai suivi le sire comte de Jaffa[^72] pour ensuite entrer en la sergenterie le roi. J'y ai tout de suite été volontaire pour porter les armes.

--- Le royaume a fort besoin de jeunes désireux de servir. Et moi j'aime que mes gens emploient leur tête pour autre chose que tenir leur chapel en place. Tu tu présenteras à ma tente ce soir, après souper. Nous verrons si nous pouvons faire bon mâtin du jeune chiot que tu es.&nbsp;»

Fouques offrit en réponse son sourire le plus éclatant. C'était la première fois que dame Fortune le bénissait. Mais le chevalier se contenta d'un rictus en retour, fronçant les sourcils. La journée était encore loin d'être finie.

### Bethléem, parvis de la basilique de la Nativité, veillée du vendredi 24 décembre 1154

Une vaste foule occupait le parvis, humains et montures. Des lampes dansaient au gré des rafales de vent, ponctuant d'or les ombres bleues tracées par la lune quasiment à son plein. Une courte averse avait mouillé le sol tandis que les fidèles entendaient la messe, mais les nuages avaient rapidement déserté le ciel, poussés vers l'est.

Fouques avait sa place dans l'entourage d'Arnulf, tenant la bride de son destrier. Une large partie de la Haute Cour avait décidé de faire une procession depuis Bethléem jusqu'à Jérusalem. La distance n'était pas si grande, mais avec la cohorte de vieillards et d'enfants, les fréquentes stations pour prier, il était prévu d'y passer la nuit. C'était la première fois que Fouques allait y assister. Il n'était pas particulièrement porté sur la foi et se contentait de suivre le mouvement lorsqu'il y avait des célébrations. Il savait que le jeune roi, Baudoin, aimait tout particulièrement les festivités de Noël, car c'était là qu'il avait affirmé son pouvoir quelques années plus tôt[^73].

De nombreuses filles gravitaient autour du prince, dans l'espoir qu'elles susciteraient des projets maritaux. Las pour leurs familles, si Baudoin avait grand appétit de chairs tendres et dociles, il ne semblait guère pressé de prendre femme. Les épousailles du roi de Jérusalem étaient un programme politique en soi, dont chacun à la Haute Cour estimait qu'il lui revenait le droit de se mêler. Fouques, dont le goût pour les ébats égalait au moins celui de Baudoin, appréciait d'autant plus le spectacle. Il avait beaucoup gagné en assurance depuis qu'il suivait Arnulf en toutes choses, bénéficiait de gages plus conséquents et de dons de vêtements qui lui faisaient l'allure d'un jeune bachelier[^74]. En outre, il arborait habituellement crânement son épée à la hanche.

Depuis le siège d'Ascalon, il avait obtenu le droit de servir exclusivement Arnulf, chevalier du roi, en renfort d'un jeune écuyer, Gace. Celui-ci, un gamin d'à peine douze ans, était absent pour les fêtes, dans sa famille à Antioche. Il logeait désormais dans la demeure d'Arnulf, où il partageait la soupente des valets, mais possédait son propre coffre, avec une clef. Il était fréquent qu'il dorme sur une paillasse devant la porte ou aux pieds du lit où reposait son maître, signe de confiance dont il s'enorgueillissait.

Il n'avait eu l'opportunité de retourner voir ses parents qu'en deux occasions depuis qu'il avait quitté Césaire. Lors de la première, si les retrouvailles avec sa mère avaient été chaleureuses, son père avait à peine décroché quelques mots. Il avait grogné un borborygme de satisfaction en apprenant que son fils servait le roi, mais n'avait pas concédé une parole amène. Fouques n'était resté qu'un court moment et leur avait remis une bourse contenant bien plus que la somme qu'ils lui avaient confiée à son départ. Sa mère avait remercié avec chaleur, mais son père semblait vexé d'un tel geste, sans rendre les pièces pour autant. La seconde fois qu'il avait pu passer les voir, il avait croisé une de ses belles-sœurs, occupée à prodiguer les soins à leur père, bien diminué et alité. Sa mère lui avait fait l'impression d'une folle, le regard perdu et la bouche édentée laissant échapper un filet de salive quand elle parlait, sans rien dire de sensé. Il avait rapidement pris les jambes à son cou, dans l'espoir d'oublier d'où il était issu. Cela faisait désormais deux ans et il y pensait de moins en moins.

Un parfum fleuri s'amusa à chatouiller ses narines et il se tourna, intrigué. Une femme dans une magnifique robe de laine sombre, chaudement emmitouflée dans un manteau de voyage fourré, venait de passer près de lui. Il n'eut le temps que d'en capter le regard intense, la moue moqueuse. Il s'empourpra immédiatement, heureux à l'idée que la nuit cacherait son trouble et piqua du nez tel un domestique respectueux. Il vit qu'elle s'était arrêtée et échangeait quelques mots à voix basse avec un des chanoines qui allaient les accompagner. Tout en parlant, elle semblait l'étudier avec attention, un léger sourire sur les lèvres. Il ne savait plus où se mettre.

La voix d'Arnulf le fit trembler, ce qui amusa fort son maître.

«&nbsp;Eh bien, Fouques, on s'oublie devant les dames&nbsp;?&nbsp;»

La voix était plus moqueuse que sévère et Fouques avait appris à déchiffrer les subtiles humeurs de l'homme. Il se composa un sourire confus.

«&nbsp;Je ne pensais pas à mal, mon sire... Pardonnez mes manières, je n'ai pas usage de fréquenter damoiselles de la Haute Cour.&nbsp;»

Arnulf s'autorisa un éclat de rire, bref et soudain.

«&nbsp;Te voilà à flatter comme ménestrel&nbsp;! Elle serait fort aise de s'entendre ainsi appelée à son âge.

--- N'est-elle pas jeune fille&nbsp;?

--- J'ai entendu bien des noms pour désigner Ameline d'Abraham[^75], mais rarement celui-ci, j'en fais confesse.&nbsp;»

Fouques fronça les sourcils, intrigué. Il étudia discrètement la silhouette qui s'éloignait, dont les ondulations lui semblaient tout à coup fort aguicheuses.

«&nbsp;Elle pourrait être ta mère, de peu. Et le scandale s'attache à elle depuis sa naissance. Elle est fille naturelle du sire châtelain d'Abraham et a manœuvré pour éviter le couvent. Son époux a fini par prendre l'habit, lui aussi, espérant, en pure perte, qu'elle ferait de même.&nbsp;»

Arnulf accorda une bourrade à son domestique.

«&nbsp;On la prétend fort intempérante. Mais elle a quelques appuis à la Cour, dont je ne sais exactement par quels intérêts ils sont mus. Si tu as moyen d'en savoir plus, j'en serais fort aise.&nbsp;»

Fouques hocha la tête. Il ne pouvait s'empêcher de lancer des regards en direction de la femme qui avait désormais pris place sur une splendide monture non loin d'eux. Il n'était qu'un modeste fils cadet d'une famille populaire. Et même si elle était bâtarde et de bien sulfureuse renommée, elle appartenait à un autre monde. Pourtant, il aurait juré qu'elle tournait parfois la tête dans sa direction. La sienne, ou celle d'Arnulf&nbsp;?

### Jérusalem, demeure d'Ameline d'Abraham, soirée du vendredi 15 juillet 1155

«&nbsp;Arnulf vicomte&nbsp;! Le vieux Rohard va s'en étrangler de rage&nbsp;!&nbsp;»

Le rire de gorge d'Ameline se déversa dans la vaste chambre. Elle avait toujours abhorré la clique des anciens qui voulaient décider de son destin. Elle n'avait aucun grief personnel contre l'un ou l'autre, mais les détestait tous en bloc, sans discrimination. Voir l'un quelconque d'entre eux connaître une rebuffade la mettait chaque fois en joie. Elle se tourna vers son jeune amant, blotti contre elle dans les draps.

«&nbsp;Quand donc le roi l'annoncera-t-il&nbsp;? J'ai grand désir d'être là quand le vieux boira la soupe.

--- De certes ces jours-ci. Il y a grande affluence pour l'anniversaire de la prise de la cité. Le roi Baudoin saura en profiter pour le faire assavoir.&nbsp;»

Ameline se releva, offrant son buste à la vue du jeune homme. Sa chemise au large col dévoilait largement sa gorge et une partie de sa poitrine. Elle prit une pose mutine, caressant sa longue tresse. Bien qu'elle jouât à l'enfant, elle démontrait dans ses postures un savoir consommé, apte à maintenir l'intérêt de son compagnon.

«&nbsp;Vas-tu demeurer à son service&nbsp;? Il aura besoin d'une plus large domesticité&nbsp;!

--- Il semble que oui. Il m'a indiqué que je pourrai apprendre bien plus de choses, désormais.

--- C'est merveilleux. Avec Arnulf pour te montrer la voie et mes conseils pour ne faire aucun impair, la voie qui s'offre à toi te mènera loin&nbsp;!&nbsp;»

Fouques se rapprocha d'elle, la peau moite et encore frissonnante de leurs ébats.

«&nbsp;Qu'as-tu donc en tête&nbsp;?

--- Sois bien assuré que des embûches vont s'abattre sur le pauvre Arnulf. Et s'il peut compter sur toi pour l'en dégager, il pourrait faire de toi plus qu'un simple valet.

--- Je suis déjà bien aise de ma place&nbsp;!

--- Pourquoi te contenter des miettes quand tu pourrais mordre la miche&nbsp;? N'est-ce pas le vicomte qui désigne le *mathessep*[^76]&nbsp;?&nbsp;»

Fouques écarquilla les yeux. Il n'avait aucune idée des habiles manœuvres qu'Ameline semblait tracer quotidiennement.

«&nbsp;Il va te falloir y mettre plus d'entrain, jeune homme, le morigéna son amante.

--- Mais... que veux-tu que je fasse&nbsp;?

--- N'as-tu pas désir d'étreindre plus qu'il ne t'est assigné&nbsp;? J'ai toujours enragé de me voir cantonnée à la pénombre alors que d'autres, moins bien pourvus, avançaient en pleine lumière&nbsp;!

--- *Dieu dispose*. Je ne suis que fils de blatier, d'autres sont là pour diriger la terre, ainsi qu'il sied.&nbsp;»

De mutine, la voix d'Ameline se fit plus virulente.

«&nbsp;Crois-tu que je saurais m'en contenter&nbsp;? Tu as bien frais minois et bonnes dispositions. Mais si je t'ai ouvert mes draps, ce n'est pas pour y finir femme de... blatier&nbsp;!&nbsp;»

Comprenant sa bourde, Fouques se redressa à son tour, caressant les épaules de sa compagne.

«&nbsp;Accorde-moi pardon. Je n'ai guère usage de rêver si haut. Ce n'est qu'à ton contact que j'apprends.

--- Il te reste encore beaucoup à apprendre&nbsp;» rétorqua-t-elle, apaisée et faussement agacée.

Pour toute réponse, il ferma ses lèvres d'un long baiser, ses mains commençant à flatter ses courbes. Elle se refusa aux caresses et ne le suivit pas sur le matelas, se redressant d'un bond.

«&nbsp;Il me faut m'apprêter pour le banquet de ce soir, sait-on jamais&nbsp;! Et toi tu dois te vêtir pour aller prendre ton service.&nbsp;»

Taquine, elle lui lança ses habits ainsi qu'une enfant. Un peu vexé de voir ses avances repoussées, il se plia néanmoins au jeu de bonne grâce. Au moment de lui envoyer sa cotte, elle en caressa l'étoffe.

«&nbsp;Nous regarderons s'il ne se trouve pas meilleure vêture pour toi dans les coffres de mon époux. Il ne me plaît guère de te voir mal habillé le jour où nous triomphons.

--- C'est là cadeau de la Noël de sire Arnulf. Il sera surpris de me voir porter autre tenue pour si importante soirée&nbsp;!

--- Crois-tu vraiment que ton maître ne sait pas tout de toi, et de nous&nbsp;?&nbsp;»

Fouques se figea, un frisson peu agréable lui caressant l'échine. Devant sa mine de chien battu, Ameline ne put retenir de s'esclaffer une nouvelle fois.

«&nbsp;Allons, Fouques, un peu d'entrain&nbsp;! Si ton maître voyait à redire sur ce que tu fais ici, il t'aurait renvoyé dans ton galetas depuis longtemps&nbsp;!&nbsp;»

Elle se rapprocha de lui et déposa un baiser rapide sur ses lèvres avant de se diriger vers la porte pour appeler une de ses servantes.

«&nbsp;Il a vu comme moi que tu es du bois dont on fait bon chevalier&nbsp;!&nbsp;»

### Jérusalem, palais royal, début de matinée du mardi 26 mars 1157

Le vicomte Arnulf dégustait un verre de vin épicé avec quelques oublies, confortablement installé dans une galerie couverte des jardins royaux. Il avait chevauché toute la nuit, s'assurant du calme dans la cité envahie par les pèlerins venus pour Pâques. Il n'avait pas envisagé que sa charge serait aussi exigeante, mais comme c'était un homme scrupuleux, il s'astreignait à patrouiller en personne régulièrement, au moins lors des périodes d'affluence.

Tandis qu'il avalait son repas en silence, il regardait les oiseaux sautiller et pépier autour des valets en train de semer et planter des fleurs et des aromatiques. Il songea un moment qu'il aurait aimé avoir un domaine où ses gens travailleraient la terre pour les nourrir. Il n'était guère heureux de n'être que chevalier de soudée[^77], tout vicomte qu'il était. Jamais il n'avait obtenu la concession d'un fief, fût-il modeste.

Fouques se présenta à lui alors qu'il avalait sa troisième crêpe. Le jeune homme arborait une tenue impeccable et, si ce n'était les éperons dorés qui manquaient, on l'aurait pris volontiers pour un bachelier. Quoi qu'en ait pensé Arnulf au début, sa fréquentation de la bâtarde d'Abraham réussissait plutôt au sergent. Il le salua d'un mouvement de tête et lui indiqua un escabeau à ses côtés.

«&nbsp;Tu vas devoir me représenter ce matin, il nous faut échanger avec les frères de Saint-Jean et les chanoines du Saint-Sépulcre. Une autre pauvresse a été retrouvée alors même que j'achevais ma ronde de nuit.&nbsp;»

Les yeux de Fouques s'élargirent insensiblement. Il n'était pas tant affecté par la découverte d'un corps que par le fait de s'entendre une mission pareille confiée à lui seul. Arnulf poursuivit sans attendre de réponse.

«&nbsp;J'ai importante entrevue avec le sénéchal à laquelle je ne peux surseoir. Mais il nous faut absolument éviter que cela ne sème nouvelle discorde. Ces maudits clercs guettent la moindre occasion de se bestancier. Veille donc à ce que cela n'arrive pas. Outre, il ne faudrait pas que les pérégrins prennent peur ou, pis encore ne s'échauffent les sangs avec cette histoire. *Vilain courroucé est à demi enragé*...

--- Nulle famille n'est venue demander justice pour l'heure. Nous ne pouvons rien proposer en la Cour des Bourgeois&nbsp;!

--- C'est bien le souci. C'est à la main royale de s'abattre, mais elle ne le fera que si une demande est faite. Sans compter que nous manquons de bras en cette période de l'année. Le murdrier a fort mal choisi son moment. Il aurait voulu cracher à la face du roi qu'il ne s'y serait pas pris autrement&nbsp;!&nbsp;»

Il avala une gorgée du vin, s'accordant un rictus tellement il était fort en épice et en sucre.

«&nbsp;Le corps a été mené à l'hôpital Saint-Jean, ainsi qu'il sied, et j'ai fait porter message aux chanoines. Tu les retrouveras tous là-bas. Ne tarde pas, tu risquerais de les trouver en train de s'étrangler les uns les autres&nbsp;! Nous ne pouvons tolérer de pareils désordres dans la Cité, surtout avec l'affluence des Pâques. La France, la Provence, les Flandres, toutes les provinces amies bruisseraient bien vite de malfaisantes rumeurs. Le royaume n'a guère besoin de nouvelles dissensions.&nbsp;»

Il n'eut pas besoin de congédier Fouques que celui-ci se levait, s'inclinait cérémonieusement.

«&nbsp;J'y vais de ce pas, mon sire. Je vous rendrai compte dès l'entrevue achevée. Vous pouvez compter sur moi.&nbsp;»

Arnulf lui accorda un sourire amical, ce qui se résumait à une fente peu expressive chez lui, puis replongea le nez dans sa boisson. Fouques partit alors à grandes enjambées en direction de l'Hôpital de Saint-Jean. Sa poitrine lui semblait prête à éclater tellement il exultait. Il se sentait de taille à affronter ces nouvelles responsabilités, qu'il espérait depuis des mois. Il était bien loin le jeune garçon mal dégrossi qui quittait Césaire le cœur gros. Il jubilait à l'idée qu'il pourrait un jour se présenter à la demeure de ses parents semblable à un seigneur visitant ses paysans. Il hésitait quant au traitement qu'il réserverait à son père&nbsp;: dédain, colère ou pardon&nbsp;? Il était impatient d'être au soir pour parler de tout cela avec Ameline. Elle serait ravie pour lui, il en était convaincu. Si elle le tançait parfois de ses inconséquences ou de son manque d'ambition, elle s'enthousiasmait encore plus que lui à chacune de ses avancées dans la bonne société. Tandis qu'il traversait la rue du Sépulcre en direction de l'Hôpital de Saint-Jean, il lui semblait entendre tinter des éperons à ses pieds. ❧

### Notes

La mobilité sociale durant les croisades est un phénomène assez difficile à aborder, la documentation lacunaire permettant assez peu de suivre le destin des personnes, fussent-elles de premier plan. On relève malgré tout qu'il existait un certain nombre d'individus dont l'ascension fut assez importante. Il n'est que de constater le parcours de Renaud de Châtillon, dont on connaît mal les ascendants, de petite noblesse apparemment, voire de Guillaume de Tyr dont l'origine bourgeoise semble assez assurée (et que je tiens pour acquise dans mes récits). Pour autant, les élites non cléricales demeuraient étanches.

On peut malgré tout suivre les itinéraires de personnages secondaires à travers les chartes et documents officiels, et en délimiter les stades d'évolution. Au prix parfois de nombreuses interprétations, car les désignations et titulatures peuvent grandement changer d'un rédacteur à l'autre. L'image qui en ressort n'est pas du tout celle d'une société complètement figée. Des familles apparaissent et disparaissent, des noms semblent passer de la catégorie *bourgeois* à l'environnement des officiers royaux dans les chartes. C'est sur cela que je m'appuie pour proposer une vision d'une société féodale extrêmement conservatrice et traditionaliste, mais capable d'assimiler, voire de réformer, les éléments les plus réfractaires aux règles en place. Il ne faut pas oublier que le pouvoir y était partagé par de nombreux groupes, même au sein des unités territoriales cohérentes comme le royaume de Jérusalem et que le consensus, informel, s'établissait en fonction des négociations et tensions toujours actives entre eux.

*Semper ad altum*&nbsp;: «&nbsp;Toujours plus haut.&nbsp;»

### Références

Crawley Charles, *Medieval lands. A prosopography of medieval European noble and royal families*, «&nbsp;Jerusalem nobility&nbsp;» http://fmg.ac/Projects/MedLands/JERUSALEM.htm\#\_Toc423186882 ( v3.2 du 02 June 2017, consultée le 10/12/2017)

, «&nbsp;Angevins versus Normans: The New Men of King Fulk of Jerusalem&nbsp;», dans *Proceedings of the American Philosophical Society*, vol.133, n°1, Mars 1989, p. 1-25.

Mayer Hans Eberhard, «&nbsp;Studies in the History of Queen Melisende of Jerusalem&nbsp;», dans *Dumbarton Oaks Papers*, vol. 26, 1972.

Prawer Joshua, *Crusader Institutions*, Oxford University Press, Oxford, New York&nbsp;: 1980.

Deux vies
---------

### Jérusalem, quartier Saint-Étienne, fin de matinée du jeudi 11 septembre 1147

Un balai de palme à la main, Élainne frottait la calade de la petite cour ombragée d'un amandier. Son fils Fulbert y poussait de menus jouets de bois et de céramique, proposant certains d'entre eux à sa sœur installée près de lui. Tyèce se contentait de sucer et goûter tout ce qui passait à sa portée, gazouillant de satisfaction lorsqu'elle estimait l'objet suffisamment luisant de salive.

Malgré l'agitation du marché voisin, l'endroit était assez calme. Il n'y manquait qu'un jardin potager pour que cela soit parfait. Ils avaient à peine la place d'installer des claies pour y faire sécher amandes, fruits et légumes. Élainne y cultivait quelques maigres plates-bandes, aromatiques et fleurs. Comme il leur fallait acheminer l'eau depuis la fontaine, la récolte demeurait généralement faible. Heureusement que Gaillard, tailleur de pierre sur le chantier du Saint-Sépulcre gagnait suffisamment pour épargner à son épouse la corvée quotidienne. Ils se faisaient livrer à domicile, comme des patriciens plaisantaient-ils.

La jeune femme vérifia que le ragoût diffusant des senteurs de cumin mijotait paisiblement sur le petit feu de cuisine extérieur. Elle poussa quelques braises, ajouta des bûchettes. Elle avait apporté son pain au four tôt le matin et n'aurait plus qu'à aller chercher un pichet de vin à la taverne pour le souper. Son époux était généralement affamé quand il rentrait, le gosier sec de la poussière avalée sur les chantiers et il n'appréciait rien tant qu'un bon verre là-dessus. Elle regrettait de n'avoir plus l'occasion de brasser de la bière, comme sa mère le lui avait appris, mais leur logement, deux petites pièces en plus d'un minuscule cellier, ne lui en laissait pas le loisir. Elle espérait qu'un jour prochain ils s'installeraient dans une maison plus grande, peut-être dans un des villages qui offraient tant d'avantages aux colons. Le savoir-faire d'un maçon tailleur de pierres serait partout apprécié.

Elle allait s'asseoir pour repriser une chemise lorsque des coups résonnèrent sur la porte de la cour. Pensant accueillir les porteurs d'eau, habituels en cette heure, elle fronça les sourcils, inquiète de découvrir Roncin, un ami de son époux. Le visage gris, il trépignait d'un pied sur l'autre, ses grosses mains serrant un linge lui servant à se rafraîchir.

«&nbsp;Le bon jour, mestre Roncin. Gaillard est au chantier...

--- le bon jour, Élainne. J'en viens, justement.&nbsp;»

Il força un peu le passage pour entrer, sans se départir de son air embarrassé. Il lança un regard aux deux enfants puis s'approcha de la jeune femme.

«&nbsp;Une potence a lâché tandis qu'on hissait moellons. Le filet s'est déchiré et il a plu des pierres&nbsp;! Ton époux en a reçu plusieurs.&nbsp;»

Élainne en eut le souffle coupé, elle s'appuya sur le mur. Le solide maçon tenta un sourire réconfortant.

«&nbsp;Ils sont trois à avoir reçu navrures. On les a portés à l'hôpital des frères de Saint-Jean.

--- Est-il sévèrement touché&nbsp;? Comment est-il&nbsp;?

--- Il a eu le flanc durement frappé, mais je ne saurais dire. Les frères ont mandé habile cirurgien...&nbsp;»

Inspirant un grand coup, Élainne laissa Roncin entrer complètement et poussa la porte derrière lui. Elle fut rapidement prête, se contentant d'enlever le vieux surcot qui lui servait de protection pour les travaux ménagers. Elle déposa sa fille dans une hotte avec quelques affaires et prit la main de Fulbert. Pendant ce temps, Roncin s'occupa d'étouffer le petit feu qui serait sans surveillance. Tout en serrant sa poupée de chiffons, le garçon glissa d'une voix inquiète&nbsp;:

«&nbsp;Père va mourir&nbsp;?&nbsp;»

Les yeux agrandis d'effroi, Élainne se contenta de se mordre la lèvre. Le garçonnet n'eut pour toute réponse qu'un haussement d'épaules désolé du solide artisan.

### Jérusalem, église du Saint-Sépulcre, après-midi du dimanche 21 décembre 1147

La pluie avait repoussé les badauds dans l'édifice, augmentant la cohue, fréquente en ces périodes de fête. De nombreux pèlerins passaient l'hiver en Terre sainte afin de célébrer la naissance du Christ, puis Pâques, avant de rentrer avant les grosses chaleurs estivales. On entendait parler toutes les langues, y compris les plus exotiques et les clercs les plus rigoristes pinçaient le nez en découvrant certaines activités prendre place dans leur église. Il n'était pas rare que des dés roulent, que des amuseurs fassent montre de leur talent, voire que des prostituées monnayent leurs charmes. Il s'était même vu quelques négociants aventureux qui avaient amené leur marchandise pour y commercer à l'abri. C'était là le destin de tous les lieux de culte ouverts aux fidèles, qui n'étaient concurrencés par les cimetières que les jours de beaux temps.

Élainne profitait de son passage pour prier pour le repos de l'âme de son époux. Il avait mis plusieurs semaines avant de mourir, dans les plus grandes souffrances. Elle était désormais isolée, avec un bien maigre pécule cotisé par les compagnons de Gaillard. Il ne faudrait pas plus de quelques mois pour le voir épuisé. Elle avait fait écrire et porter des messages à ses frères et sœurs, dont aucun ne résidait plus à Jérusalem, mais un seul avait répondu, Hemeri, moine à Calanson. Elle avait reçu le pli quelques jours plus tôt et était venue faire lire sa missive par un des clercs du Saint-Sépulcre&nbsp;: il lui envoyait ses meilleures pensées et prierait pour leur Salut à tous. Qu'elle se trouvât dans le besoin ne semblait pas l'avoir effleuré. Benjamine de la fratrie, elle n'avait jamais été proche d'aucun d'eux et sentait désormais tout le poids de sa solitude. Gaillard ne fréquentait que ses confrères et elle n'avait jamais réellement sympathisé avec leurs épouses.

Lorsqu'elle se releva de ses oraisons, elle chercha Fulbert du regard. Le petit avait trouvé des compagnons de jeu et participait à une course-poursuite alternant avec des périodes d'escalade des bases de colonne. Elle assujettit le panier avec Tyèce, endormie, et se mit en quête de son fils. Le jour ne durait guère au plus fort de l'hiver et elle souhaitait être rentrée au plus tôt. Le clerc qui lui avait lu la missive, jeune chanoine aux yeux doux et aux gestes maladroits, s'approcha d'elle avec un homme de taille modeste, claudicant à ses côtés. Il était habillé de belle toile, sans ostentation, mais on sentait qu'il était aisé. Son visage ingrat, un peu bouffi, n'était guère rehaussé par l'énorme nævus poilu qui surplombait sa barbe maigrelette. Son regard démentait pourtant cette allure commune. Il était direct, franc et brillait de détermination. Le religieux adressa à Élainne un sourire et lui indiqua que, voyant son désarroi, il n'avait pu s'empêcher de chercher une façon de l'aider à améliorer son sort.

«&nbsp;Mestre Senoreth est de bonne fame en nostre congrégation. Peut-être aurait-il moyen de vous porter assistance&nbsp;?&nbsp;»

Le clerc passa les mains dans ses manches, hochant de la tête comme un gallinacé heureux de sa découverte. Il semblait néanmoins décidé à demeurer lors de l'échange, apportant ainsi sa garantie aux propos tenus.

«&nbsp;Lorsque le frère m'a parlé de votre malheur, je lui ai fait part de mon souhait de trouver nouvel apprenti en mon échoppe.

--- C'est grande amabilité de votre part, mestre, mais je ne sais guère que m'occuper de mon foyer.

--- Il s'agit là d'une tâche qui sied à toute femme&nbsp;: mercière. Je suis mestre passementier et j'ai toujours préféré l'habileté et le soin des dames pour les délicats travaux qu'on me confie. Vous savez de certes tirer l'aiguille&nbsp;?

--- Ma mère y a pourvu, comme de bien entendu. Mais de là à en faire mestier juré, je n'ai pas le talent ni la sapience de telles choses.

--- Ne vous souciez point de cela. L'apprentissage y pourvoira. J'ai usage de prendre plus jeunes personnes, mais le frère m'a ému de vos soucis et ce serait de justice que de faire bonnes œuvres en cette période de l'Avent.&nbsp;»

Élainne sourit aimablement, un peu réticente à l'idée de se lier ainsi à un inconnu. Elle savait que bon nombre de maîtres exploitaient les mains qu'ils recrutaient, quand ils n'abusaient pas de leur position éminente pour exiger de plus sournoises tâches.

«&nbsp;Si vous en êtes d'accord, vous n'aurez qu'à vous présenter à ma boutique, sur la voie menant porte Sainte-Étienne. Demandez mestre Senoreth. Mon épouse sera prévenue au cas où je sois absent. Prenez votre temps, je me suis laissé jusqu'aux Pâques pour décider, je n'ai pas d'ouvrage important en souffrance pour le moment.&nbsp;»

Le chanoine aurait convenu à la place, nul doute qu'il y aurait postulé tellement il acquiesçait d'enthousiasme.

«&nbsp;Je peux me porter témoin en votre engagement si nul ne peut le faire. Mandez après moi ici&nbsp;: Jehan de Berry&nbsp;» précisa-t-il, tout sourire.

### Jérusalem, demeure de Pierre Salomon, soirée du samedi 5 avril 1152

Dégoulinant de pluie, Ganelon se frottait les mains auprès du four encore chaud dans la cuisine du riche négociant. Il avait chevauché à bride abattue pour porter un message de la part de son maître, le chevalier Régnier d'Eaucourt, à Pierre Salomon, membre éminent et influent de la Cour des Bourgeois de Jérusalem. À peine avait-il pris la route depuis Mirabel que les nuages lourdement chargés de la côte s'étaient vidé sur lui. Bien que la température fût clémente, il se sentait transi jusqu'aux os. Il était heureux de ne pas avoir à emporter de réponse avant le lendemain.

Le royaume était sur le point d'exploser&nbsp;: Mélisende et son fils Baudoin se disputaient le pouvoir et les affrontements armés n'étaient pas loin. Le maître de Ganelon, fidèle serviteur du connétable de la reine jusque là venait de se rallier au jeune prince après que celui-ci ait capturé le vieux compagnon de route de sa mère. Ganelon avait été désigné pour porter un message afin de préparer une entrevue avec la Cour des Bourgeois. Leur allégeance allait traditionnellement à Mélisende, mais avec l'ascension irrésistible de Baudoin, peut-être auraient-ils le désir de changer de point de vue.

Pour Ganelon, c'était égal. S'il appréciait la compagnie de Régnier d'Eaucourt malgré ses exigences parfois élevées, il ne se mêlait jamais de politique. D'abord il n'y comprenait rien et, ensuite, il estimait que c'était le plus sûr moyen pour se voir le cou suspendu à une branche. Alors que personne n'avait jamais pendu un valet qui faisait correctement son travail. Il regrettait d'avoir décidé de venir en Terre sainte, quelques années plus tôt, attiré par l'aventure, et n'aspirait plus qu'à la tranquillité.

La cuisine était rangée et le feu moribond, mais un des jeunes valets, Jacques, était allé demander du pain et un peu d'accompagnement pour le souper de Ganelon. En l'absence du personnel, les portes du cellier étaient fermées et c'était l'épouse de maître Pierre qui en détenait les clefs. Une petite veilleuse posée sur la table dispensait une faible lumière dans la vaste salle, prévue pour y préparer les banquets et y donner les repas des domestiques.

Lorsque la maîtresse de maison entra, elle ne lança pas un regard à Ganelon. Bourgeoise d'importance, elle n'accordait aucun égard aux gens les plus modestes. Ganelon suivit son avancée d'un œil appréciateur, reconnaissant qu'elle conservait une séduisante allure malgré les années. Il fut surpris de découvrir dans son sillage une femme au beau visage dont l'ovale était souligné par un voile strictement noué. Elle baissait le regard modestement et prit place à table sans mot dire.

De retour du cellier, Jacques apportait une grosse miche, un pichet et déposa un pot dans le four encore tiède tandis que sa maîtresse ressortait en silence. Il disposa écuelles et gobelets, invitant Ganelon à s'asseoir sur le banc.

«&nbsp;Ganelon, je crois que t'as jamais encontré Élainne. Elle œuvre comme passementière chez mestre Senoreth. Vu l'heure tardive, elle va compaigner ton repas.&nbsp;»

Il fit rapidement les présentations puis se servit une solide rasade de vin coupé. Ganelon appréciait fort le jeune homme, dont les traits lui évoquaient ceux d'Alerot, un compagnon mort en chemin pour la Terre sainte. Il était néanmoins bien plus beau et savait tirer avantage de son frais minois. Il ne semblait pourtant guère intéressé par la couturière, qu'il estimait peut-être trop âgée pour lui. Tout au long du repas, il fit la conversation, sur des sujets légers et des ragots locaux. Ganelon en profita pour lancer quelques regards à la dérobée à Élainne, dont il s'aperçut bien vite qu'elle faisait de même. Il bomba imperceptiblement le torse, heureux d'avoir choisi d'enfiler sa plus belle cotte de travail pour venir délivrer ce message.

### Jérusalem, abords de l'église Saint-Étienne, matin du dimanche 27 septembre 1153

Goûtant le beau temps et une température clémente, les fidèles s'étaient amassés après l'office sur le parvis de l'église. C'était l'occasion d'échanger tranquillement des nouvelles de la cité et d'ailleurs. Ganelon était rentré depuis peu de la campagne royale victorieuse qui avait mené à la capture d'Ascalon. Il profitait de son jour de repos pour passer un moment avec Élainne, qu'il avait pris l'habitude de retrouver pour la messe dominicale.

À son grand dam, maître Senoreth estimait qu'il était de ses attributions de veiller aux bonnes mœurs de son apprentie et il ne leur accordait jamais plus de quelques pas de latitude. C'était néanmoins de bien agréables instants partagés. Autour d'eux, les enfants les plus aventureux couraient parmi les jardins et les enclos. D'un tempérament plutôt lymphatique, Fulbert, le fils d'Ellaine, tentait parfois de suivre le rythme endiablé des plus énergiques. Sa mère confia à Ganelon qu'elle ne savait pas ce qu'elle allait en faire. Elle ne pouvait en effet plus s'en occuper et elle craignait de le voir subir de mauvaises influences.

«&nbsp;Pourquoi ne pas le placer comme valet&nbsp;? proposa Ganelon. Il suffit de lui trouver bon maître et il suivra droit chemin.

--- J'y ai pensé, mais je ne sais à qui me fier. Mestre Senoreth n'a nul ami qui le prendrait à son service, je lui en ai déjà parlé.

--- Pourquoi pas au service des moines&nbsp;? Tu es fort proche des chanoines du sépulcre le Christ. Ils ont des terres tout entour le ventre. Ce serait bien le diable s'ils ne trouvaient lieu où faire œuvrer Fulbert. De plus, on se loue d'une année l'autre chez eux, cela permet de changer.&nbsp;»

Élainne accorda un sourire triste à Ganelon. Elle appréciait beaucoup sa présence et ses conseils, mais elle avait espéré que son fils pourrait connaître meilleure vie. Elle ne souhaitait pas froisser son ami, d'autant qu'elle avait pour lui une tendresse qui se renforçait au fil des mois, à la faveur de leurs échanges, tout de délicatesse et de mesure. Mais elle aurait voulu que son enfant soit plus que valet. Elle hocha la tête doucement et garda le silence un moment. Elle aperçut Tyèce, occupée à gratter la terre avec quelques compagnes de bêtises. Elle s'avança pour la tancer, n'appréciant guère de voir sa plus belle tenue ainsi malmenée, mais fut devancée par un parent plus proche.

Au bout d'un moment, Ganelon revint vers elle, un large sourire barrant son visage amical. Elle lui trouva fort correcte allure, dans sa cotte de laine, la barbe taillée et les chausses bien ajustées. Peut-être pourrait-elle lui confectionner un petit parement à fixer sur ses manches, pour une occasion prochaine. Il était aimable, travailleur et courtois. Plus qu'elle n'en demandait à un époux. Elle lui sourit à son tour.

### Jérusalem, jardins de la porte Saint-Étienne, fin d'après-midi du jeudi 15 juillet 1154

Les festivités de commémoration de la prise de la ville battaient leur plein. De nombreuses célébrations religieuses étaient fêtées par des volées de cloches et les farandoles, défilés et joyeux attroupements profanes se répandaient dans tout Jérusalem. Au nord de la cité, des jardins plantés d'arbres offraient des zones d'ombre où s'installer au plus chaud de l'été. Des musiciens marquaient le rythme, incitant les présents à de folles danses tandis que jongleurs et acrobates rivalisaient de prouesses.

Ganelon avait invité Élainne à profiter de ce jour férié. Il avait amplement fait provision en passant par Malquisinat et avait garni sa bourse de suffisamment de mailles et d'oboles pour régaler comme un prince autour de lui. Fulbert et Tyèce couraient de-ci de-là, mordant avec gourmandise brioches et fruits tout en applaudissant les artistes.

Devant eux, un montreur d'animaux racontait l'histoire de la prise de la ville avec des tableaux illustrés par les bêtes. Un âne servait de destrier aux héros représentés par un perroquet qui connaissait de nombreux cris de guerre tandis que les musulmans étaient symbolisés par un chien au vaste panel de compétences. Le récit était avant tout burlesque et le comédien en faisait des tonnes, pour le plus grand bonheur du public. L'ambiance était joyeuse, les nouvelles de l'été étaient bonnes.

Assise sur un muret à distance respectueuse de Ganelon, Élainne grignotait des pistaches tout en profitant du moment. Elle en proposa à son compagnon et s'enquit de ce qui pouvait le chagriner par une si belle journée. Elle avait remarqué son visage fermé et l'entrain forcé dont il faisait montre malgré toute sa bonne volonté.

«&nbsp;J'ai mauvaise nouvelle à te faire assavoir... Mon sire Régnier s'est vu confier importante mission et je dois le suivre.

--- Il est en ainsi depuis que nous nous connaissons, mon ami. En quoi est-ce différent&nbsp;?

--- Nous allons outremer, en Provence, Bourgogne, France, Normandie, Angleterre, Flandre... Il y en a pour des mois, si ce n'est des années&nbsp;!&nbsp;»

Élainne hocha la tête. Elle fréquentait Ganelon depuis bientôt deux ans. Elle avait ainsi un prétendant officiel qui lui permettait de repousser les avances qu'on aurait pu lui faire. Mais elle savait bien que celles-ci seraient de plus en plus rares. Elle n'était plus si loin de ses trente ans et définitivement sans avoir. Quel homme aurait voulu d'elle&nbsp;? D'un autre côté, jamais elle ne renoncerait à son apprentissage, dont elle n'avait fait qu'un peu plus de la moitié. La mort de Gaillard l'avait laissée démunie et elle avait besoin de jurer un métier, de devenir suffisamment indépendante pour ne pas risquer de se retrouver une nouvelle fois en si misérable posture, réduite à mendier auprès de sa famille, en pure perte. Elle adressa à Ganelon son plus charmant sourire.

«&nbsp;Vous reviendrez bien assez tôt que je ne me sois lassée d'attendre.

--- Que veux-tu dire&nbsp;?

--- Je ne ferai rien tant que je n'aurais pas obtenu maîtrise en mon métier. Et cela ne se fera pas avant long temps. Assez pour que tu reviennes et que tu me jures ta foi.&nbsp;»

Troublé par l'adresse directe, Ganelon faillit en bafouiller et se reprit bien vite.

«&nbsp;Tu accepterais de devenir mon espousée&nbsp;?

--- Il me faudra en demander avis à quelque doctes conseils. Peut-être demanderai-je à frère Jehan de Berry s'il te croit homme de bonne foi&nbsp;» le moqua-t-elle.

Lorsque Ganelon mordit dans sa fouace, il lui sembla qu'il n'en avait jamais goûté de meilleure.

### Jérusalem, rue de David, midi du vendredi 23 mai 1158

Se faufilant parmi la foule à travers la rue de David, toujours encombrée, Élainne succombait à une agitation inhabituelle chez elle. Elle avait reçu un message du chanoine Jehan, avec qui elle entretenait des relations presque fraternelles, depuis toutes ces années. Il lui avait fait lecture d'une excellente nouvelle&nbsp;: un des riches notables de la cité l'avait couchée dans son testament. Éminent membre de la Cour des Bourgeois, Tosetus était un bienfaiteur dont on avait vanté la loyauté et la chaleur humaine. Récemment décédé, il avait souhaité faire des donations à des œuvres et Élainne était du nombre. Elle n'avait pas souvenir de l'avoir rencontré, mais le fait était bien là.

Elle frappa avec enthousiasme à la porte de la demeure de Régnier d'Eaucourt. Elle espérait que Ganelon serait présent, impatiente qu'elle était de partager la nouvelle. Ce fut lui qui ouvrit. Il était en chemise, transpirant. Son visage s'éclaira en la voyant.

«&nbsp;Que voilà belle surprise&nbsp;! Je n'espérais guère après toi avant ce dimanche.&nbsp;»

Une fois entrée dans la cour, Élainne y découvrit tout un fourniment de combat&nbsp;: boucliers, lances, selles, casque et haubert, étalé un peu partout. C'était visiblement le grand déballage. Ganelon intercepta son regard.

«&nbsp;Une campagne se prépare, il me faut fourbir promptement les affaires de mon sire.&nbsp;»

Élainne hocha la tête, peu intéressée par les détails guerriers de cette nouvelle chevauchée. Elle avait toujours la crainte d'apprendre que Ganelon n'en était pas revenu. Ils en étaient encore à préparer leur installation, pour laquelle leurs maigres économies ne suffisaient guère. Elle commençait à peine à avoir suffisamment de travail pour subvenir à ses besoins propres et Ganelon n'avait nulle ressource en dehors de ses compétences comme valet, en plus de quelques besants âprement épargnés. De plus, elle espérait initier sa fille au métier, maintenant qu'elle en avait le droit, pour qu'elle soit indépendante.

Elle invita Ganelon à s'asseoir sur un des bancs.

«&nbsp;Je reviens du Saint-Sépulcre, Jehan m'a fait lecture d'un important document. Il se trouve qu'un riche bourgeois du roi a décidé d'aider quelques malheureux. Il me fait don d'une maison aux abords de la Tour de David.&nbsp;»

Ganelon en resta sans voix, battant des paupières comme si une poussière s'y attachait.

«&nbsp;Je demanderai à mestre Senoreth ce qu'il sait de lui, mais Jehan m'a dit que c'était fort pieu et charitable prud'homme.&nbsp;»

Elle s'enhardit à lui prendre les mains.

«&nbsp;Comprends-tu ce que cela signifie&nbsp;? Nous allons pouvoir nous jurer fidélité&nbsp;!&nbsp;» ❧

### Notes

J'en ai déjà parlé à de nombreuses reprises, l'étude du parcours de vie des plus humbles a longtemps été négligée par les historiens en raison du manque de sources et des difficultés d'analyse de celles-ci. Un pointilleux et laborieux travail de recension et collation était nécessaire en préalable à toute construction scientifique critique. Depuis quelques années, la numérisation permet de rendre tout cela bien plus aisé. Tout d'abord en mettant à disposition les textes, parfois anciens, qui font autorité sur le sujet. Que ce soit dans des bases comme Gallica ou Persée, il est désormais bien plus commode de consulter certaines publications que vingt ans plus tôt.

Mais le bénéfice des outils réseau ne s'arrête pas là. Des initiatives de synthèse critique sont élaborées par des instituts de recherche et offertes à la communauté. J'ai déjà cité le projet prosopographique de Charles Crowley (*Medieval lands. A prosopography of medieval European noble and royal families*&nbsp;: http://fmg.ac/Projects/MedLands/ ) et je voulais ici parler d'un autre programme, qui va grandement me faciliter le travail documentaire&nbsp;: *The Revised Regesta Regni Hierosolymitani*, qui propose de traduire en une base de données toutes les chartes patiemment étudiées par Reinhold Röhricht à la fin du XIXe siècle, base incontournable pour qui s'intéresse à l'histoire des croisades. Cela offre l'opportunité de reconstruire plus aisément le parcours de vie de signataires méconnus. Autant d'occasions pour moi d'étayer le destin des protagonistes de *Hexagora*, à charge pour les plus érudits ou curieux de faire les liens avec les sources scientifiques.

### Références

Besson Florian, «&nbsp;Fidélité et fluidité dans l'Orient latin. L'exemple de Rohard de Jérusalem (v.1105- v. 1185)&nbsp;» dans *Bulletin du Centre de recherche français à Jérusalem*, volume 26, 2015.

Mayer Hans Eberhard, «&nbsp;Studies in the History of Queen Melisende of Jerusalem&nbsp;», dans *Dumbarton Oaks Papers*, vol. 26, 1972.

Prawer Joshua, *Crusader Institutions*, Oxford University Press, Oxford, New York&nbsp;: 1980.

Riley-Smith Jonathan (ed.), Ellenblum Ronnie, Kedar Benjamin, Shagrir Iris, Gutgarts Anna, Edbury Peter, Phillips Jonathan et Bom Myra, *Revised Regesta Regni Hierosolymitani Database* http://crusades-regesta.com/ (consulté le 24/01/2018)

Traditions
----------

### Jérusalem, quartier de la porte d'Hérode, matin du mardi 6 mars 1145

Un grand fracas d'origine inconnue réveilla Abdul Yasu en sursaut. Il habitait non loin d'un souk où l'on façonnait le cuivre. Ce n'étaient pas tant les petits marteaux traçant les décors qui faisaient du bruit que le transport des marchandises allant et venant depuis la porte de David où l'on taxait les produits. Il résidait chez son père, Tha'lab al-Fayyad, dans une chambre du rez-de-chaussée de la très confortable demeure. Il y était fort bien traité, profitant de la richesse de la maison sans trop s'embarrasser de considérations matérielles. Il avait opté pour cette modeste pièce, car, si elle avait l'inconvénient d'être proche de la rue, elle offrait l'avantage d'être aisément accessible depuis la cour arrière, où s'ouvrait un guichet donnant sur l'extérieur. Il pouvait ainsi aller et venir en toute discrétion, sans que l'on puisse surveiller son emploi du temps.

Il se leva et fit une toilette soigneuse, se curant les ongles et les oreilles avec attention. Puis il enfila tuban[^78], sirwal[^79], fine chemise et durrâ'a[^80] de splendide laine zébrée de couleurs vives, posa sa qayah[^81] sur son crâne et assujettit méticuleusement son turban de soie paille, à longs rafrafs[^82] brodés de bleu sombre. Il était toujours très élégant et tenait à paraître impeccable à chaque occasion. De plus, il prévoyait en fin de journée de se rendre aux bains, de façon à être parfait pour une soirée festive avec quelques amis.

En sortant de sa chambre, sa sacoche au côté, il croisa Ghadir, la vieille gouvernante, qu'il salua poliment. Celle-ci lui accorda un sourire puis soupira en détaillant du regard la tenue d'apparat du jeune homme. Elle traitait Abdul comme s'il était encore un enfant, mais, paradoxalement, semblait exaspérée par son comportement juvénile. Indifférent à ces usuelles manifestations d'humeur, il lui donna des instructions pour qu'on lui porte des affaires aux bains voisins en fin d'après-midi. Avant qu'il n'ait le temps de s'esquiver en direction de Malquisinat, où il savait dénicher ses friandises préférées, znout assit[^83] arrosés de mouhalabie[^84], elle lui indiqua que son père souhaitait le voir.

Tha'lab al-Fayyad était un vieil homme bossu dont les membres grêles s'échappaient d'un corps empâté par les ans et la trop bonne chère. Sa voix elle-même était gênée par les replis tremblotants qui pendaient dans son cou, faisant onduler sa maigre barbe quand il prenait la parole. Ses yeux larmoyants et fatigués n'y voyaient plus si bien qu'il devait souvent demander à ce qu'on lui lise ses ouvrages favoris ou les lettres de ses amis. Abdul pensait que c'était la raison qui le menait en ces lieux.

«&nbsp;Entre, fils. Prends place, j'ai des choses à te dire.&nbsp;»

Abdul se méfia de l'entrée en matière. Son père était assez laxiste et peu exigeant, et bon nombre de voisins moquaient sa désinvolture envers ses domestiques et sa famille, mais il semblait peu s'en soucier. Il avait néanmoins parfois des lubies, des velléités autoritaires qui, sans durer, étaient pénibles à affronter.

«&nbsp;J'ai reçu tantôt belle lettre de ton frère Ma'ali. Il t'envoie ses meilleures pensées et espère que tu étudies ainsi qu'il sied...&nbsp;»

Le début était peu encourageant. Ma'ali était son aîné, parti voilà des années vers l'Égypte et au-delà. Il était désormais fort loin, naviguant dans la mer vers les Indes. Bien implanté dans sa ville, il avait contracté un riche mariage et échangeait avec les plus hautes autorités musulmanes, les plus opulents négociants de la place. Il faisait la fierté de son père et semblait, à en lire ses courriers, un des plus heureux et influents hommes de sa cité. De quoi agacer n'importe quel cadet. De malaise, Abdul en remua sur son coussin.

«&nbsp;Il a accompagné son pli de plusieurs balles de denrées, à vendre et pour notre maison. En particulier une belle pièce de soie qu'il espère te voir porter pour ton mariage.&nbsp;»

C'était donc cela. Depuis quelques mois, Tha'lab al-Fayyad n'avait de cesse de rappeler à son fils qu'il était temps pour lui de s'établir. Ce qui signifiait abandonner cette vie de loisirs et d'amusement pour s'adonner à un travail rémunérateur en vue de fonder sa propre famille. Il avait désormais plus de vingt ans et c'était déjà bien tard pour se mettre au labeur. Il aurait dû partir comme commis des années plus tôt, mais demeurait dans le confort, retenu par l'affection d'un père âgé.

«&nbsp;Je me disais que nous pourrions prélever de ses largesses de quoi t'installer. As-tu réfléchi à cela, depuis notre dernière conversation&nbsp;?

--- Je continue d'étudier vos notes, je ne sais encore quelles marchandies seraient les plus intéressantes.

--- Le commerce ne se fait pas sur le papier, Abdul&nbsp;! s'emporta son père. *Jeunesse sans discipline, maison sans toit* ! As-tu été discuter dans quelque souk&nbsp;? Réfléchi aux besoins des uns et des autres&nbsp;? Travaillé ton expertise en savoir de négoce utile&nbsp;?&nbsp;»

Abdul plongea du nez, conscient de déroger constamment à ses devoirs. Il avait reçu la meilleure éducation qui soit, profitant de la richesse familiale sans subir les contraintes s'exerçant sur les premiers-nés. Sa mère étant morte peu après sa venue au monde, ses proches s'en étaient fort émus et il avait été élevé préservé de toutes les difficultés, choyé par la gouvernante autant que par sa fratrie et son père. Il en tirait une certaine mollesse de caractère qui l'incitait à vivre dans l'inconséquence. Au grand dam de ceux qui l'entouraient, conscients qu'il ne serait peut-être pas capable de faire quoi que ce soit de sa vie.

Son père lui conservait pourtant une affection que les semonces n'altéraient pas plus que les manquements. Rien ne semblait résoudre Tha'lab al-Fayyad à sévir pour de bon. Il secoua la tête de dépit, faisant trembler ses multiples mentons, puis se gratta longuement le nez.

«&nbsp;Tu as bien quelque passion dans la vie en dehors de la musique et des jeux&nbsp;? Tu m'as fait acheter ce superbe alezan voilà deux saisons, est-ce pour en tirer commerce&nbsp;? Ou s'agit-il là encore d'un caprice passager&nbsp;?&nbsp;»

Vexé de se voir ainsi jeté à la face un de ses égarements, Abdul grogna et rétorqua vivement.

«&nbsp;Point du tout, je me disais qu'il ferait bon effet en des écuries de louage et pourrait saillir juments et ânesses.

--- Astucieuse idée, mon fils, j'en conviens. Que ne me l'as-tu dit plus tôt&nbsp;? As-tu trouvé un lieu où organiser ton affaire&nbsp;? Des pouliches en vue&nbsp;?&nbsp;»

Abdul hésita à prolonger le mensonge. Il n'avait jamais eu d'autre intention que de parader sur son superbe étalon. Il avait toujours apprécié les chevaux et goûtait fort les jeux équestres, mais de là à s'installer les pieds dans le crottin... Tandis qu'il cherchait un moyen de détromper son père, celui-ci s'emballait, réfléchissant déjà aux partenaires qui pourraient aider Abdul dans son entreprise.

### Jérusalem, porte de David, fin d'après-midi du vendredi 25 mars 1149

Jérusalem était toujours très agitée à l'approche de Pâques, mais cette année-là, l'effervescence était à son comble. Après l'échec de l'assaut sur Damas, le roi des Français Louis avait décidé de demeurer là pour l'importante fête religieuse. Son armée encore imposante occupait donc l'endroit en plus des habituels pèlerins. Cet afflux de visiteurs de renom attirait également de nombreux artistes, commerçants et écornifleurs de toutes sortes, impatients de bénéficier de la largesse d'une cour aussi foisonnante. La présence de la fameuse Aliénor d'Aquitaine et son goût pour les célébrations courtoises enflammaient les esprits.

Abdul Yasu tirait profit de cette activité. Ses écuries, installées aux abords de la porte de David, accueillaient des montures de voyageurs. De plus, il louait à bon prix ses plus beaux animaux pour permettre aux importants personnages de s'afficher en ville ainsi qu'il seyait à leur rang. Il avait fini par s'habituer à son commerce, abandonnant les corvées les plus salissantes à un employé et conservant les contacts avec les clients, en particulier les plus riches.

Non soumis aux droits et taxes, les pèlerins pouvaient entrer dans une file distincte de celles des marchands. S'y adjoignaient les habitants qui allaient et venaient depuis leurs jardins ou les fournisseurs bénéficiant d'une exemption. Naturellement Abdul prit sa place dans ce groupe et menait son petit âne d'un air badin. Cherchant à ne pas croiser le regard d'un des hommes de faction, il leva le nez, comme pour en évaluer la météo prochaine. Quelques filaments blancs s'étiraient paresseusement dans une mer d'azur. Plusieurs oiseaux tournaient, en quête de moucherons, s'élançant depuis les aspérités des puissantes murailles.

Il sursauta quand une main ferme vint lui frapper l'épaule. Tout à son rôle, il sourit benoîtement au garde qui le toisait d'un air soupçonneux. L'homme ne paraissait pas agressif, le visage rond et rougeaud, un petit nez en trompette surplombant une belle moustache.

«&nbsp;Dis-moi, compère, ton âne me semble porter bien lourds bagues[^85]&nbsp;!&nbsp;»

En disant cela, le sergent tâtait de la main les sacs amoncelés et rapidement ficelés sur le dos du petit animal. Abdul chercha à se composer un visage emprunt de modestie et de sincérité.

«&nbsp;Ce ne sont que mors, freins et selles de mes bêtes. Je les mène en réparation.&nbsp;»

Il tenta un sourire puis enchaîna.

«&nbsp;Vous me connaissez, je vais et viens moult fois chaque jour, pour le commerce de mes bêtes, en les écuries non loin.&nbsp;»

Tout en parlant, il indiqua grossièrement la direction de ses bâtiments. Le soldat se tenait impassible, un léger sourire à peine tracé sur les lèvres, les pouces coincés dans son ceinturon d'armes.

«&nbsp;Oui-da, j'ai bonne connoissance de ta face, mais aucune de ces marchandies. Il te faut aller dans l'autre groupe et payer la taxe ainsi qu'il sied.

--- Mais ce n'est pas là commerce&nbsp;! Juste des affaires miennes qui ont grand besoin de reprises.&nbsp;»

Le garde fit une moue et entreprit de jeter un coup d'œil au contenu d'un des sacs. Pas le plus gros, posé au-dessus, mais un des plus petits, adroitement ficelé sous les autres. Autour d'eux, les voyageurs avançaient, indifférents à ce contrôle qui ne les concernait pas.

Une belle courroie teintée de rouge apparut, ornée de bossettes argentées. Le sergent siffla discrètement.

«&nbsp;Si c'est là mauvaises sangles, tu dois louer destriers de baron fieffé&nbsp;!&nbsp;»

Abdul grimaça. Ce n'était pas son premier essai, de mêler ainsi de beaux harnais neufs à ses vieilles courroies rompues. Il les faisait venir d'Égypte et les revendait un bon prix aux riches seigneurs croisés désireux de ramener de prestigieux souvenirs de leur voyage au centre du monde. Les quelques précédentes fois, il avait pu passer sans encombre, mais ce nouveau sergent semblait plus suspicieux que les autres. Pourtant, Abdul nota néanmoins qu'il n'avait pas appelé auprès d'eux les officiers de la douane.

«&nbsp;Mon valet aura peut-être par mégarde rangé ce harnois neuf avec les anciens...

--- C'est tout un, erreur ou pas. La taxe est due pour chaque marchandie, entrée à dessein ou pas...

--- C'est que j'ai grand ouvrage à faire ce jour&nbsp;! Ne peut-on au moins faire en sorte que les choses se fassent promptement&nbsp;?

--- Que me suggères-tu, l'ami&nbsp;?&nbsp;»

Abdul hésita un instant. Le sergent pouvait être de ceux qui prêchaient le faux pour glaner le vrai, espérant faire moisson de coquin à bon compte en laissant la proie s'enferrer toute seule. Il avait pourtant le visage franc et ouvert autant qu'un Latin pouvait l'arborer. Son teint couperosé témoignant de son évident penchant pour la boisson, cela donna une idée à Abdul Yasu.

«&nbsp;Disons que je pourrais vous faire passer la taxe un peu plus tard, histoire de ne pas perdre de temps. J'ai usage de passer dans quelques tavernes au sud du Change Latin. Nous pourrions y convenir d'une rencontre.

--- Je m'en voudrais fort de retarder ton commerce, l'ami. Mais il me faut évaluer le montant de la taxe en détaillant ce que tu as dans ces sacs&nbsp;! Dénouons donc là tout ce fourniment&nbsp;!

--- Oh, il n'y a guère plus que ce que tu as vu. Pour une valeur d'une livre, peut-être deux, et certes pas plus.

--- L'octroi étant de trois besants le cent pour ce frein, cela ferait... hum, dans les quinze deniers. Si on y ajoute selle et courroies pour compléter harnois, on arrive à deux sous.&nbsp;»

Abdul répondit d'un rictus. Pour avoir la face avinée, l'homme n'en était pas moins assez fin et fort en chiffres. Avec une épée au côté, en plus. Il ne serait d'aucune utilité de se récriminer, au risque d'attirer l'attention et de se faire prendre. D'autant qu'il demeurait un beau bénéfice vu qu'il se trouvait dans ses bagages deux équipements complets dont un rapide brossage suffirait à en éliminer la poussière et en rehausser le brillant.

«&nbsp;D'accord, sergent. Grand merci. Nous nous retrouvons en la veillée là-bas&nbsp;?

--- C'est dit.&nbsp;»

Le sergent lui fit signe de la tête d'avancer promptement puis, alors qu'Abdul frappait la croupe de sa bête, il conclut&nbsp;:

«&nbsp;J'ai nom Droart.&nbsp;»

### Jérusalem, Malquisinat, midi du mardi 10 juillet 1156

La touffeur de l'air et les impitoyables rayons du soleil incitaient chacun à chercher la moindre parcelle d'ombre au plus fort du jour. Le temps d'une collation, Droart et Abdul Yasu avaient pris l'habitude de partager un petit recoin où un muret servait de banc, à l'abri d'auvents de fortune en feuilles de palme, non loin des échoppes proposant pâtés et tartes, crêpes, galettes et brochettes. À force de se croiser, sans même avoir une transaction en cours, ils aimaient à échanger tranquillement en dégustant leur repas parmi la foule des voyageurs et manouvriers familiers des lieux.

Les odeurs des caramels de cuisson y disputaient aux senteurs épicées des mousses et tartinades où l'on plongeait des légumes frais taillés en bâtonnets ou des portions généreuses de pain. Le vin, pas toujours de grande qualité, y était néanmoins assez fort pour inciter sinon à la sieste du moins à l'assoupissement. Et il y avait même de la place assez pour y tracer de quoi jouer aux mérelles, aux jeux de table de façon générale, voire aux dés pour ceux qui ne craignaient pas de tenter leur chance si près des lieux saints.

Abdul appréciait fort le jeune sergent, dont le bagou était à la hauteur de ce qu'il avait estimé lors de leur première rencontre. Ils avaient encore fait quelques affaires, mais se voyaient également pour le simple plaisir d'échanger des nouvelles sur le royaume, la cité et leurs voisins. Droart était assez friand de commérage, aussi impatient de les répéter que d'en apprendre de nouveau. Appartenant chacun à une communauté différente, ils pouvaient initier l'autre à tout un monde de risibles comportements. Abdul Yasu savait parler la langue des Latins et ils conversaient habituellement ainsi, mais Droart en profitait pour se familiariser avec l'arabe, qu'il employait sans grand talent et un accent qui faisait s'esclaffer Abdul.

Alors qu'ils finissaient leur repas avec des galettes au miel et à la pistache, Droart proposa à Abdul de se joindre à lui le soir pour la veillée qu'il passait généralement avec quelques amis.

«&nbsp;Nous passons le temps ainsi que nous le faisons tous les deux certains mitans du jour. Je suis acertainé que tu apprécieras mes compères. En particulier Eudes, le sergent au poil cuivré que tu as vu tantôt à la porte.

--- C'est bien amiable à toi, mais il n'est pas d'usage de passer veillées ensemble...

--- Et pourquoi donc&nbsp;? Si le père Foucher estime que les autels de tes prêtres ont leur place en son moustier, pourquoi donc ne pourrais-tu pas te joindre à nous&nbsp;?&nbsp;»

Bien qu'il appréciât l'invitation, Abdul était mal à l'aise à cette idée. Il n'était pas dans les usages de se mélanger avec d'autres communautés. Il était certes chrétien, quoique peu fervent et d'un culte distinct de celui des catholiques romains, mais il se sentait différent de ces derniers. Langue, culture, origine, tout en lui l'incitait à maintenir cette séparation. Il n'avait d'ailleurs jamais eu beaucoup d'échanges avec d'autres que Droart.

Celui-ci se rinça la bouche d'une longue rasade au pichet, essuyant les miettes accrochées à son menton d'un grand mouvement de bras.

«&nbsp;Tu m'as dit que ton frère a solide commerce avec les Mahométans dans sa cité. Et pourquoi tu n'aurais pas bonne intelligence avec nous&nbsp;? Toi aussi tu trouves qu'on sent prou l'ail et l'oignon&nbsp;? plaisanta-t-il.

--- J'aurais grand plaisir à découvrir tes sochons, mais peut-être qu'eux seront moins heureux de m'encontrer. Il est de bon ton de fréquenter les siens.

--- Foin de cela. Mes pères ne sont pas nés ici, mais moi si. Cela nous fait déjà un point commun. S'il se trouve quelque drôle à chicaner sur ce point, je lui ferai rentrer dans la gorge ses arguties.&nbsp;»

Il tendit le broc à Abdul, assorti d'un clin d'œil.

«&nbsp;Là d'où vient ma famille, ils ont bonne connoissance des vilains sur des lieues à la ronde, sachant combien de grains ils ont commercé avec chacun, la taille des œufs qu'ils versent en dîme ou les femmes qu'ils leur ont mariées. Un homme sans lieu n'est rien de plus que poussière portée par le vent. Et moi je suis d'ici, comme toi, alors il serait déraisonnable de faire comme si je te connaissais pas. Tout bon Chrétien est mon frère, disent les pères curés. Donc tant que tu te fais pas hérétique, j'aurai bon plaisir à te compter parmi les miens&nbsp;!&nbsp;»

### Jérusalem, quartier de la porte d'Hérode, fin d'après-midi du lundi 16 décembre 1157

Abdul résidait toujours chez son père, continuant à profiter des avantages à n'avoir pas à gérer sa propre demeure. Il bénéficiait également des plats de Ghadir, dont la main s'alourdissait sur les épices avec l'âge, mais sans qu'il le déplore. Il épargnait un peu de l'argent que son affaire de location lui rapportait, tout en s'accordant le droit d'en dépenser une large part en soirées, musique et boissons.

Il rentrait d'une journée un peu morose, le mauvais temps n'incitant guère aux voyages. De plus, il avait eu des frais médicaux, une de ses juments ayant développé un glome. C'était une bête fragile qu'il n'avait pas payée très cher, mais dont l'état de foulure chronique, malgré des soins et une attention constante, la rendait sensible à ce genre d'affection. Il en avait été quitte pour des applications d'emplâtre goudronné et un peu de repos à prévoir pour l'animal.

Passant par le centre de la Cité, il en avait profité pour acheter quelques kenafas dont l'odeur de miel fleuri l'avait tenté tout au long de sa fin de parcours. Il savait son père friand de ces desserts et Ghadir peu douée pour les réaliser. Tha'lab al-Fayyad l'accueillit aimablement quand il se présenta à lui, l'invitant à aller rapidement se laver les mains pour prendre le repas ensemble. Il semblait particulièrement enjoué.

Tandis qu'ils se désaltéraient d'un leben[^86] frais du jour, Abdul eut l'explication de cet enthousiasme. Une lettre venue d'Égypte était arrivée sans que son père n'ait eu le temps d'en prendre connaissance. Il espérait qu'elle soit de Ma'ali et déchanta un peu lorsqu'il apprit que l'expéditeur n'était que Durayd al-Khayyat al-Tusi, un de ses nombreux contacts d'affaire à Fustat. Il eut malgré tout un sourire malicieux quand il invita son fils à la lui lire.

Les informations qu'elles contenaient étaient surtout d'ordre commercial, après les salutations et échanges de bons vœux d'usage en introduction. Il indiquait les transactions qu'il avait opérées pour le compte de Tha'lab al-Fayyad, pour une belle somme au moment de la clôture hivernale des mers. Puis le sujet en changea brusquement, ce qui fit balbutier Abdul Yasu avant qu'il ne s'interrompe tout à fait.

«&nbsp;Pourquoi donc t'envoie-t-il des nouvelles de sa fille, père&nbsp;? Tu as l'intention de reprendre épouse&nbsp;?&nbsp;»

Abdul se voyait mal accueillir dans leur foyer une fille qu'il aurait à traiter comme sa mère. D'autant que les jeunes femmes qui épousaient des hommes très âgés avaient tendance à asseoir leur autorité de façon un peu trop empressée à son goût. Il eut un fugace sourire à l'idée de la réception d'une pareille nouvelle par Ghadir, qui faisait dans la maison ainsi qu'elle l'entendait depuis des années. D'un signe de la main de son père, il reprit la lecture. Rapidement le rictus se crispa pour s'accompagner d'un ébahissement grandissant. De surprise, sa voix devint moins forte et, de stupeur, il s'arrêta une seconde fois.

«&nbsp;Avez-vous projet de me faire épouser sa fille, cette Shaima, père&nbsp;?

--- C'est bien ce qui est convenu, en effet. Al-Khayyat est de bonne fame et un ami ancien. Sa fille est en âge de se marier et il a de quoi la doter en suffisance pour que l'offre soit honnête.

--- Mais père, jamais nous n'avons évoqué cela&nbsp;!

--- Voilà chose faite. Je lui ferai porter réponse dès le printemps revenu et il serait bon que tu joignes à ma lettre un petit mot pour montrer ton enthousiasme à l'idée d'unir nos deux familles. Je compte sur toi pour mettre en avant la bonne santé de ton commerce.&nbsp;»

Abdul Yasu en avait le souffle coupé. Il avait à peine passé la trentaine et pensait bien profiter de son célibat encore quelques années. Il n'avait aucun désir de s'établir et de devoir consacrer son pécule à l'entretien d'une famille ou d'une maison. Prodigue de nature, ce n'était pas tant la dépense qui l'effrayait, mais plutôt l'idée de devenir responsable d'autres que lui. Il déglutit lentement puis demanda&nbsp;:

«&nbsp;Est-elle jolie au moins&nbsp;? Je ne voudrais pas d'une épouse qui ne soit pas attrayante au regard.

--- J'ai dans mon vieux khaff[^87] plusieurs lettres d'al-Khayyat. Il s'y trouve joli portrait de cette juvénile beauté, que son père composa de façon droitement rimée. Je t'en donnerai connoissance.&nbsp;»

Voyant que son fils était contrarié, il s'éclaircit la gorge avant de reprendre d'une voix qui se voulait conciliante.

«&nbsp;Tu ne semblais guère enclin à aller te proposer dans les familles de nos amis, il fallait bien que je m'occupe de cela. Avec les années qui passent, tu passes trop de temps avec ces Ifranjs&nbsp;! J'avais grande crainte que tu n'y fasses malavisée rencontre. Ta réputation n'est déjà pas tant fameuse qu'une mésunion l'aurait plus encore entachée.&nbsp;» ❧

### Notes

Au XIIe siècle, parmi la bourgeoisie urbaine arabe, quelle que soit sa religion, il est habituel de commercer au sein d'un vaste réseau de correspondants qui dépasse les frontières. Un homme se doit de prouver sa capacité à gagner sa vie suffisamment pour s'établir avant de prétendre à avoir une famille. Ne pas être en mesure de subvenir à ses besoins est un motif valable de divorce pour une épouse, même si dans la réalité bien peu sont prononcés à leur initiative.

Pour les jeunes femmes, on leur demande surtout d'être nubiles et vierges. Ces deux exigences sociales expliquent la grande différence d'âge souvent constatée. Par ailleurs, le consentement des futurs mariés est tout à fait accessoire, l'union étant le résultat d'une politique familiale d'extension de ses amitiés et influences. Comme les noms l'attestent, l'individu n'existe que par son intégration à une filiation et, de façon plus générale, à une communauté. Cela explique peut-être aussi les fortes réticences envers les pratiques exogames, qui sont encore plus exigeantes que le simple respect de sa propre religion.

### Références

Bianquis Thierry, *La famille arabe médiévale*, Bruxelles&nbsp;: Éditions Complexe, 2005.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I à 6, Berkeley et Los Angeles&nbsp;: University of California Press, 1999.

Goitein Shlomo Dov, Friedman Mordechai Akiva, *India traders of the Middle Ages. Documents from the Cairo Geniza ('India Book')*, Leiden - Boston&nbsp;: Brill, 2008.

En route
--------

### Tyr, parvis de la cathédrale Sainte-Croix, fin de matinée du dimanche 18 février 1151

Poussée par la brise de mer, l'averse matinale avait refroidi l'atmosphère. Le sol, marqué de quelques flaques, devenait de la boue sous le piétinement des fidèles au sortir de la messe. Un ciel cendré promettait de nouvelles ondées, sans que cela inquiète la foule occupée à discuter. Des bandes d'enfants, s'affranchissant de la tutelle de leurs aînés en plein commérage, en profitaient pour se faufiler de-ci de-là en jouant et piaillant.

Plusieurs saltimbanques avaient pris place sur le parvis, espérant une prodigalité renforcée par l'office religieux. Plusieurs montreurs d'animaux faisaient cabrioler leur bête, s'attirant des cris d'émerveillement ou de surprise. Jongleurs et acrobates n'étaient pas en reste, apostrophant le chaland à grand renfort de tambours et d'exclamations outrées.

Au plus près de l'édifice sacré se tenaient des musiciens, profitant des marches pour se poster ainsi qu'en chaire face à leur auditoire. Parmi eux, tout au nord de l'endroit, une jeune femme chantait des psaumes de sa voix fatiguée. Elle n'avait guère de vêtements sur elle pour se prémunir du froid et se frottait continuellement les bras afin de se réchauffer. Son répertoire attirait à elle les plus dévots, pas pour autant toujours les plus charitables. Elle savait que c'était là sa plus belle quête de la semaine et mettait donc tout son cœur dans un latin approximatif, n'ayant guère idée de ce qu'elle déclamait. Elle se contentait de copier à l'oreille les chants des pèlerins et des clercs.

Anceline avait pris l'habitude de suivre la côte, quittant une cité où la générosité s'érodait pour une autre, le temps de faire oublier les largesses reçues. Elle s'était constitué un petit répertoire sacré qui ne recueillait pas autant de pièces que les habiles tours des artistes plus inventifs, mais qui lui permettait d'avoir bon accueil auprès des établissements religieux. Elle était une fidèle des hospices, y goûtant la soupe légère et le pain noir qu'elle engloutissait comme mets de choix.

Une fois encore, la récolte était maigre et ne lui donnerait guère l'occasion de festoyer. En outre, en ce mois de février, le plus frais de l'année, elle était frigorifiée. Elle n'avait pour tout vêtement que deux chemises, sa cotte bien fatiguée et une chape lui servant souvent de couverture lorsqu'elle était sur les chemins. Elle avait des chausses sans pieds, ayant depuis longtemps oublié l'usage des savates qu'il fallait sans cesse faire ressemeler.

Elle avait à peine vingt ans, mais en paraissait dix de plus, fanée par la trop fréquente exposition aux éléments et les durs labeurs. Elle se louait comme lavandière là où elle pouvait trouver de l'ouvrage, bien rarement et payé à la journée. Elle y avait gagné de nombreuses engelures et des mains douloureuses lorsque le temps tournait à la pluie. Il lui était aussi arrivé de négocier les attraits de son corps quand la faim se faisait trop pressante, mais elle évitait d'en faire une habitude.

Souvent, après avoir cédé au découragement, elle quittait la ville, désireuse d'oublier et craignant d'être ainsi connue. Malgré tout, à force de fréquenter les mêmes lieux, on la voyait de moins en moins comme une lavandière. Elle refusait néanmoins de s'abandonner à ce commerce et de suivre les troupes de soldats où il était plus aisé de s'y adonner. Sans plus vraiment y croire, elle ne désespérait pas de retrouver celui qui lui avait fait promesse de l'épouser. Elle ne voulait pas se présenter à lui irrémédiablement souillée, chérissant ce rêve sans jamais oser faire en sorte qu'il puisse éclore.

Elle comptait les quelques piécettes, oboles et méreaux, qu'elle avait reçus après son tour de chant quand elle fut apostrophée par une voix à l'intonation rieuse.

«&nbsp;Alors, belle amie, la moisson est-elle à la hauteur de ces divines strophes&nbsp;?&nbsp;»

Celui qui avait parlé était un petit homme, plusieurs degrés au-dessus d'elle et pourtant les yeux à peine à sa hauteur. Son corps et sa tête étaient normaux, mais ses membres semblaient ceux d'un enfant. Tout en s'exprimant, il secouait ses mains, amplifiant chaque mot d'un geste démonstratif. Clairement, il était comédien, de nature si ce n'était de profession. Son visage aux traits réguliers était mangé de barbe et ses vêtements, pour être de bonne qualité, avaient connu de meilleurs jours. Il bondit à côté d'elle et fit une révérence entre deux sautillements.

«&nbsp;J'ai nom Mile, comédien, chanteur et acrobate&nbsp;! Mes compères et moi t'avons entendue. Ce qui s'échappe de ta gorge est à l'aune de celle-ci et je tenais à te faire compliment pour le tout&nbsp;!

--- Je n'ai nulle fierté à tirer de cela, *Dieu dispose*.

--- Le sire patriarche en sa chaire n'aurait pas mieux dit, mais d'une voix moins doucette à mes oreilles&nbsp;!&nbsp;»

Le visage barré d'un franc sourire inspira confiance à Anceline, qui empocha rapidement sa collecte et ramassa son petit sac posé au sol. Le nain avait visiblement une idée derrière la tête et elle s'approcha un peu de lui.

«&nbsp;Et que puis-je pour toi, compère Mile, que ne pourrait t'accorder beau clerc en son chœur&nbsp;?&nbsp;»

La saillie éclaira le visage du nain plus encore et son regard en devint outrageusement égrillard.

«&nbsp;Rien que je ne saurais demander à mienne sœur, je t'en fais jurement. Avec mes compagnons, nous pensons aller au sud, vers Acre et sa chaude fournée de pérégrins qui ne saurait tarder à se déverser en nos terres. Une belle voix de plus en notre groupe ferait pousser cliquaille comme fleurs en mai. Outre, tu pourrais nous apprendre tes chants, plus propices à flatter l'oreille du dévot que mes paillardises.&nbsp;»

Tout en parlant, il désigna un petit groupe d'à peine une demi-douzaine, occupé à des acrobaties. Une femme mendiait auprès des chalands tout en leur lançant des plaisanteries dont Anceline devinait qu'elles n'étaient guère pieuses, à voir les mines mi indignées mi-enthousiastes autour d'elle. La présence d'une commère la rassurait un peu, bien qu'elle sache que cela ne garantissait pas toujours d'être en sécurité. Néanmoins, l'offre du petit homme était intéressante. Les pèlerins fraîchement débarqués pouvaient se montrer généreux. Peut-être s'y trouverait même un riche baron désireux de s'entourer de saltimbanques pour égayer ses soirées. Elle accorda à Mile son plus beau sourire.

«&nbsp;Eh bien, nomme-moi donc tes sochons. J'aurais usance de leur nom si nous devons compaigner un temps&nbsp;!&nbsp;»

### Jérusalem, échoppe de Margue l'Allemande, matin du mardi 3 novembre 1153

De chaudes odeurs briochées vinrent titiller les narines d'Anceline avant même qu'elle ne réalise le bruit autour d'elle. Elle était épuisée, vermoulue, nauséeuse, incapable d'ouvrir les yeux. Elle était installée sur une couche et bien couverte. Dans sa bouche raide comme du carton, elle sentait des remugles désagréables, héritiers de la bile qu'elle soupçonnait encore désireuse de jaillir de ses entrailles.

Des personnes discutaient aimablement, une voix féminine dominant les autres, donnant parfois quelques ordres secs ou flattant ce qu'Anceline devinait être des chalands. Elle remua un peu, émit un grognement et força pour entrouvrir les paupières.

Elle était sur une modeste plateforme, installée sur une paillasse. À ses côtés, un coffre cadenassé. Le lieu n'était pas très large, mais assez long, couvert d'une voûte chaulée. Tandis qu'elle clignait des yeux, une petite silhouette s'approcha d'elle. Un garçon, emmitouflé dans une épaisse chemise de laine. Il semblait aussi circonspect qu'elle. Avant qu'elle ne puisse émettre un son, il avait bondi vers le bord de la galerie et crié&nbsp;:

«&nbsp;Mère, mère&nbsp;! Elle est éveillée&nbsp;!&nbsp;»

Anceline déglutit lentement, sa gorge lui paraissant de feu. Elle sentit qu'on montait à l'échelle et une tête apparut bientôt. Un visage rond, laiteux, serré dans un foulard d'où s'échappaient quelques mèches brunes. La femme lui sourit.

«&nbsp;Alors, ma mie, comment te sens-tu&nbsp;? Nous avons eu grand effroi et craignions de devoir te livrer à la terre sans même connaître ton nom.

--- Anceline me nomme.&nbsp;»

Sa voix lui fit l'effet d'un terrible croassement, elle qui aimait tant chanter.

«&nbsp;Bien contente de t'ouïr, ma belle. J'ai nom Margue et voici mon enfançon, Robert.

--- Où suis-je&nbsp;? Qu'est-ce que...&nbsp;»

Sa question mourut dans une toux violente qui l'abattit. Margue finit de grimper l'échelle, faufilant son imposante masse auprès de la couche pour la border.

«&nbsp;Dors, il sera bien temps plus tard. J'ai aussi moult questions pour toi.&nbsp;»

Les jours qui s'égrenèrent ensuite furent très flous pour Anceline. Elle se réveillait de temps à autre, nourrie de bouillons et de soupes au vin par Margue. Elle percevait parfois sa chaleur pendant la nuit, lorsqu'elles partageaient le grabat. Elle entendait les berceuses chantées pour Robert, fredonnées à mi-voix. La journée, le commerce qui se déroulait sous elle lui apportait des effluves sucrés et capiteux, incitant son estomac à la rébellion. Elle se sentait néanmoins chaque jour plus forte que le précédent.

Un soir qu'elle lui servait son écuelle de potage, Margue lui expliqua que c'était son valet Belek qui l'avait trouvée, inconsciente, dans le faubourg de la porte de David.

«&nbsp;J'avais plusieurs compères et commères. Que sont-ils devenus&nbsp;?

--- Je ne sais. Tu étais seulette en la ruelle. Peut-être t'ont-ils abandonnée pour morte&nbsp;?&nbsp;»

Elle haussa ses fortes épaules tout en s'efforçant au sourire. Anceline se souvenait qu'ils avaient tous été atteints d'effroyables flux de ventre après des jours à se nourrir d'expédients. Les derniers mois avaient été particulièrement difficiles pour les bateleurs. Après les violents affrontements entre la reine Mélisende et son fils Baudoin, puis la prise de pouvoir effective et indisputée de celui-ci, il y avait eu de grands bouleversements dans le royaume.

Les barons n'étaient guère enclins à faire la fête, pas plus que les bourgeois à distribuer leurs aumônes. Et les moines préféraient les pèlerins aux saltimbanques, toujours suspects de mille fraudes et abus. Peut-être ses compagnons l'avaient-ils cru morte. Même Mile, qu'elle appréciait assez pour avoir partagé sa couche quelques fois, n'était plus si enjoué qu'à leurs débuts. Le désespoir les avait tous gagnés.

«&nbsp;S'ils t'ont laissée, c'est qu'ils ne méritent guère que tu te biles pour leur sort. Pense donc déjà à te bien porter&nbsp;!

--- Pourquoi m'avoir ramassée&nbsp;? Nous ne sommes pas...

--- Suis-je si male chrétienne que je ne peux aider plus nécessiteuse que moi&nbsp;? Nul ne sait quand il aura besoin d'une main tendue, mais il peut décider de la tendre à qui lui chante.

--- Je ne pourrai jamais payer de retour.

--- Tu pourras bien passer le balai ou tamiser la farine quand tu seras mieux. Belek est parfois si gauche que j'ai doutance qu'il fasse exprès dans l'espoir que je l'affranchisse de certaines corvées&nbsp;!&nbsp;»

Elle récupéra l'écuelle et se releva, non sans remettre en place la couverture au plus près d'Anceline.

«&nbsp;Laisse donc simple femme te porter charité à sa mesure. On gagne ainsi Paradis à bon compte.&nbsp;»

### Jérusalem, quartier du palais royal, matin du lundi 16 novembre 1153

Désormais plus assurée sur ses jambes, Anceline allait de temps à autre en ville faire quelques commissions et livraisons afin d'aider Margue à faire tourner son commerce. Ce jour-là, ses pas l'avaient menée aux abords du Saint-Sépulcre et du palais royal. Elle avait longuement hésité, baguenaudant parmi les boutiques aux alentours, avant de se décider à s'enquérir de Fouques[^88] auprès de la domesticité de Baudoin.

Des hommes de faction à l'entrée lui en interdirent l'accès. Elle n'avait selon eux aucun motif valable d'y pénétrer. Les profiteurs et larrons étaient légion et, si l'hôtel royal était généreux, il n'avait pas vocation à accueillir tous les miséreux de la terre. Devant le grand portail de la cour, un des sergents, moins méchant que son allure austère ne le laissait supposer la prit un peu en pitié et lui demanda après qui elle en avait.

«&nbsp;C'est jeune valet qui a trouvé service auprès du frère le roi, le comte de Jaffa. Il a nom Fouques et vient de Césaire, sur la côte.

--- Pourquoi donc le recherches-tu&nbsp;? T'aurait-il fait subir mal sort&nbsp;? Abandonnée avec enfançon&nbsp;?&nbsp;»

À sa question, Anceline comprit qu'il n'envisageait pas l'idée avec mansuétude.

«&nbsp;Rien de cela. Nous nous étions promis l'un à l'autre en nos jouvences et la vie nous a séparés. J'aurais néanmoins grande joie à l'encontrer de nouvel.&nbsp;»

L'homme hocha la tête avec gravité, visiblement soulagé de n'avoir pas à rechercher un pernicieux inconséquent. Il héla une petite femme à l'allure nerveuse qui s'approcha de lui, les bras encombrés d'une corbeille de draps. Il lui expliqua rapidement l'histoire et en appela à son jugement. Lingère du palais, elle voyait aller et venir la plupart de la domesticité pour leur fournir le couchage et, parfois, les vêtements.

«&nbsp;Je crois voir ce gamin là, indiqua-t-elle de sa voix haut perchée. Damoisel de belle allure, mais il n'est pas en nos murs. Le sire comte de Jaffa a tant attendu pour tenir Ascalon, il s'y est rendu tantôt.&nbsp;»

Elle adressa à Anceline un rictus désolé.

«&nbsp;Le sire Amaury[^89] va et vient souventes fois au palais, mais il risque de demeurer en ses terres le temps de s'assurer que nul ne vient lui contester sa prise. Ses ribauds seront avec lui, il y a sûrement fort ouvrage à accomplir là-bas.&nbsp;»

Anceline hocha la tête en silence et remercia d'un sourire. Puis elle prit congé des deux domestiques et reprit son chemin vers la proche boutique de Margue. Elle ne savait pas ce qu'elle avait imaginé. Même si elle avait retrouvé Fouques, il devait l'avoir oubliée. Cela faisait plusieurs années qu'ils s'étaient séparés. Elle ne maintenait son souvenir vivace que parce qu'il avait représenté un espoir de quitter sa vie de misère. Ce n'était qu'une chimère qu'elle s'entêtait à poursuivre, sans que cela ne lui apporte aucun réconfort.

Pourtant, elle ne pouvait s'empêcher de réfléchir au temps que lui prendrait un voyage vers le sud du royaume. Ascalon n'était pas si éloignée et de fréquents convois de marchands allaient sûrement s'y rendre depuis Jaffa. Elle pourrait y faire un tour, dans l'improbable espoir que Fouques y soit. Et qu'il accueille avec soulagement son amour de jeunesse. Elle pourrait tout à fait trouver de l'embauche comme lingère ou simple lavandière. Avec leurs deux soldes, ils pourraient vivre une belle vie et, même, avoir des enfants. Elle n'était pas si vieille pour ne pas en rêver. Elle aussi aurait plaisir à voir grandir un petit Robert auprès d'elle.

### Ascalon, abords de la porte de Jaffa, veillée du dimanche 13 décembre 1153

Anceline n'avait jamais vu une cité aux murailles pareillement dévastée. Une large partie de la courtine était abattue, objet de tous les soins d'une foule de maçons et de charpentiers. Elle venait chaque jour y trouver de l'ouvrage, en recherche de manouvriers désireux d'avoir leur vêtement nettoyé ou réparé. Elle avait renoncé à l'espoir d'obtenir de quoi manger après la messe, la population étant principalement constituée de voyageurs et de négociants. Lors de la prise de la ville, les musulmans avaient eu l'opportunité de fuir, abandonnant leurs maisons et leurs biens aux vainqueurs. Quelques familles de chrétiens orientaux, de juifs, étaient demeurées, mais elle n'était pas de leurs communautés pour oser leur demander l'aumône.

On parlait partout de l'ouvrage qui ne manquerait pas&nbsp;: cathédrale, château, fortifications. Amaury, seigneur d'Ascalon avait tout à faire en sa cité et les ouvriers se frottaient les mains des années de labeur qui s'annonçaient. Selon leur spécialité, ils allaient d'un chantier à l'autre, vendant leur savoir-faire aux plus offrants. Anceline leur enviait cette indépendance née d'une maîtrise, d'un talent dont ils défendaient jalousement la valeur.

Elle avait tenté d'entrer en contact avec les gens au service du comte, espérant enfin retrouver Fouques, pour apprendre qu'il n'était plus simple ribaud, mais valet d'armes et avait trouvé sa place dans l'ost royal au service d'un chevalier. Il pouvait désormais être n'importe où, tenant garnison pour l'hiver, portant des messages ou escortant un émissaire. La nouvelle l'avait abattue, ses dernières illusions lui étant arrachées, elle s'était alors abandonnée au commerce qu'elle déprisait fort jusque là. Mais au moins n'avait-elle guère à y penser, son fin visage et son regard doux suffisaient à lui attirer les hommes. Elle avait sombré un temps dans le désespoir le plus total.

Un matin, elle avait été prise à partie par d'autres prostituées qui lui reprochaient de brader ses charmes, exigeant à peine de quoi survivre, indifférente à ce qui était requis d'elle. La plus hargneuse l'avait si violemment secouée qu'elle était tombée à terre, la lèvre fendue par le choc contre une pierre. Puis les coups avaient plu sur elle, la plongeant dans une léthargie dont elle n'était sortie qu'avec difficulté, le corps endolori et l'esprit absent. Elle avait alors constaté que ses maigres affaires avaient disparu pendant son inconscience.

Alors qu'elle gisait là, hébétée et sans plus aucune attente, un manouvrier l'avait reconnue&nbsp;: Houdoin, un gâcheur de mortier aux mains cornées et brûlées par la chaux, dont elle avait déjà réparé quelques effets et rassasié les chairs. Il l'avait relevée et l'avait ramené avec lui, lui laissant l'usage de ses draps la journée contre l'abandon de son corps la nuit. Il n'était pas tant attentionné, mais pas brutal pour autant et lui apportait de quoi se restaurer chichement. En plus du gîte et du couchage, elle n'en attendait pas plus.

Houdoin et quelques compagnons s'étaient arrogé une échoppe avec une petite cour à l'arrière où ils pouvaient cuisiner sur un feu leurs soupes et brouets. Tandis qu'elle reprenait des forces, elle se proposa de leur entretenir le linge, tout en profitant de leur protection, de leur nourriture et de leur abri. Bien vite, elle partagea à l'occasion le grabat de l'un ou l'autre. Leurs épouses au loin, ils avaient des besoins à satisfaire, sans égards, mais sans violence. Elle s'en contenta et finit par s'habituer à son état.

Noël approchant, presque tous s'absentèrent pour retrouver leur famille, et Anceline demeura seule avec Houdoin. Un soir qu'ils savouraient un épais potage aux fèves agrémenté de lard, ce dernier se laissa aller à évoquer sa vie et ses projets. Sous ses dehors frustes, il n'était pas mauvais homme, quoique parfois empressé et autoritaire, héritage d'une existence de rude labeur.

«&nbsp;Et toi, commère, qu'as-tu en tête&nbsp;? Tu ne saurais demeurer ici ta vie durant. Nous allons de certes prendre le chemin d'ici l'été. Tu pourrais nous compaigner.

--- Cela fait long temps que tu erres en quête d'ouvrage, Houdoin&nbsp;?

--- Je ne sais de juste, je suis né l'année où Foulques reçut la couronne[^90]. Je cours les chantiers depuis bien longtemps, mis en apprentissage que le lait me coulait encore du nez. Mais j'ai espoir de pouvoir fonder ma famille d'ici quelques paires d'ans, j'ai amassé cliquaille assez pour décider un père à me donner sa fille. Pour peu qu'elle soit honnête et bien dotée...&nbsp;»

Le regard qu'ils échangèrent alors fit naître un rictus sur le visage tavelé de l'ouvrier.

«&nbsp;Qui voudrait espouser fille des rues telle que toi&nbsp;? Tu as mignonne face assez, mais c'est là bien faible dot pour fonder hostel.

--- Je sais cela. À défaut d'en marier un, au moins serais-je la femme de plusieurs pour manger assez.&nbsp;»

À sa grande surprise, Houdoin lui accorda une caresse sur l'épaule. Puis il avala une longue rasade de vin, le regard perdu dans le vague.

«&nbsp;La nuit s'en vient, commère, et la lune n'est qu'un filet. Il est temps de se glisser sous la couverte. Dormons ensemble, il ne fait pas tant chaud.&nbsp;»

### Lydda, quartier de Samarie, matin du lundi 15 décembre 1158

Anceline frappait le sol de ses pieds afin de se réchauffer. Malgré le soleil, la température était fraîche et elle était impatiente de prendre la route. Elle avait posé à côté d'elle la hotte où elle rangeait toute sa fortune. Elle était désormais habituée des chantiers et suivait des équipes d'artisans, ne restant jamais plus de quelques mois en un lieu, s'efforçant de ne pas faire naître des espoirs qu'elle savait sans cesse déçus. Il lui était arrivé plus d'une fois de se consacrer à un seul homme, lui réservant ses caresses, mais jamais aucun n'avait eu le désir d'officialiser cela, préférant s'en tenir à des échanges commerciaux là où elle recherchait de la stabilité si ce n'était de l'affection.

Le dernier de ses réguliers, Gerbaut, l'avait accompagnée pour ce départ du chantier de la basilique Saint-Georges. Silencieux, il profitait de la proximité d'une fontaine pour puiser l'eau qu'il distribuerait ensuite aux ouvriers. C'était un portefaix aux épaules robustes, durement blessé quelques jours auparavant, un bras cassé en écharpe.

Son humeur, déjà fort maussade au naturel, s'en était encore dégradée et cela avait convaincu Anceline de faire son baluchon une fois de plus. Elle avait appris qu'une belle fête se préparait à Naplouse, à la cour de la reine Mélisende. Peut-être aurait-elle l'occasion d'y trouver de l'emploi durant l'hiver. Malgré ses défauts, elle hésitait à quitter Gerbaut. Il s'était révélé un compagnon décent quoique fort taciturne. Il était peu causant et, les quelques mois qu'ils avaient vécu l'un à côté de l'autre, il ne s'était jamais confié. Elle l'avait deviné bien plus adroit et cultivé que son embauche comme portefaix ne le suggérait et elle avait découvert dans ses affaires des outils de tailleur de pierre. Mais jamais il n'avait abordé le sujet.

Il savait malgré tout montrer de la prévenance, à l'occasion, pensant à elle pour la distribution de vêtements des chanoines au début de l'Avent ou lui offrant de temps à autre une pâtisserie. Mais à chaque fois il n'avait accompagné ces attentions d'aucune explication ni d'aucun geste qui aurait pu laisser croire qu'il avait envie d'aller plus loin. Il n'avait guère manifesté de réaction lorsqu'elle avait évoqué son désir de partir pour Naplouse. Il avait simplement indiqué qu'il s'enquerrait des caravanes afin qu'elle voyage en sécurité.

Elle n'avait pas le cœur à lever les mystères de cet homme et se contentait de survivre un jour après l'autre. Elle avait abandonné l'espoir de trouver quelqu'un à marier et savait qu'aucun établissement religieux ne voudrait d'une novice qui viendrait prononcer ses vœux sans apporter un minimum de biens à la congrégation ni pouvoir attester d'une bonne moralité. Elle s'efforçait juste de ne pas penser au lendemain.

Enfin le maître caravanier donna le signal du départ, faisant naître une agitation bruyante parmi les voyageurs. Les chameaux et les mules de bat, les chevaux des plus aisés se mirent en branle dans un grand vacarme et un dense nuage de poussière. Anceline se tourna vers Gerbaut et l'accola tendrement. Malgré toutes ses préventions, elle ne pouvait s'empêcher de s'attacher.

«&nbsp;Que la paix de Dieu soit avec toi, Gerbaut.

--- À Dieu te mande, Commère. Garde-toi bien des périls de la route. J'espère te revoir aux beaux jours.&nbsp;»

Elle se figea, surprise, mais elle n'eut pas le temps d'en demander plus. Il s'écarta d'elle et, après lui avoir pressé le bras en signe d'adieu, il s'en fut sans plus un regard. Lorsqu'elle franchit la porte de la cité, elle essuya avec vigueur les larmes qui perlaient de ses paupières, oscillant entre agacement et espérance. ❧

### Notes

Route&nbsp;: XIIe siècle, du latin *\[via\] rupta* (pour un chemin percé à travers une forêt), du participe passé féminin *rupta* du verbe *rumpere*, «&nbsp;épuiser en imposant un effort, rompre, casser, percer&nbsp;».

Le titre de ce Qit'a fait bien évidemment référence à l'excellent roman *La route* de Cormac McCarthy dont fut tiré le film éponyme avec Viggo Mortensen dans le rôle principal. Je me suis dit qu'il était intéressant de suivre les pas et cheminements intérieurs d'une femme en proie à un monde violent. Leurs traces sont souvent faibles, aussi bien dans le texte de McCarthy que dans les sources médiévales.

La suite de l'histoire d'Anceline est très largement influencée par des lectures que j'ai pu faire autour des enquêtes de Jacques Fournier lors des ultimes soubresauts de l'hérésie cathare, environ 150 ans plus tard dans le sud de la France. Aucune existence scrutée par l'inquisiteur n'est telle que ce que j'écris. Cela m'a néanmoins donné une image assez dure, mais précise, des conditions de survie des femmes qui se trouvaient reléguées à la marge de la société. Je n'ai cependant aucune certitude sur la véracité d'une pareille vision pour la Terre sainte du XIIe siècle, quoiqu'en disent les sources pour d'autres lieux et périodes proches.

Pour avoir une illustration précise et documentée d'une vie de jeune fille marginale au Moyen Âge, je vous conseille l'excellent livre de Anne Brénon, *L'impénitente*, qui suit le destin d'une hérétique à la naissance du XIVe siècle.

### Références

Brénon Anne, *L'impéninente. le roman vrai de Guillelme maury, de Montaillou*, Cahors&nbsp;: La Louve éditions, 2002.

Karras Ruth Mazo, *Common Women. Prostitution and Sexuality in Medieval England*, New York, Oxford&nbsp;: Oxford University press, 1996.

Le Roy Ladurie Emmanuel, *Montaillou, village occitan de 1294 à 1324*, Paris&nbsp;: Folio Histoire, 2008.

McCarthy Cormac, *The road*, New York&nbsp;: Alfred A. Knopf, 2006 traduit *La route*, Paris&nbsp;: Éditions de l'Olivier, 2008.

Semper fidelis
--------------

### Jérusalem, vallée du Hinnom, fin d'après-midi du mercredi 5 février 1130

En contrebas d'arides terrasses où prospéraient de tortueux et anciens oliviers, une zone avait été abandonnée au gravier et aux pierres, aménagée en une plateforme permettant les exercices militaires. Les soldats y venaient de temps à autre accomplir de martiales manœuvres, quoique la place fût un peu resserrée pour les larges déploiements de cavalerie. En cette fin de journée, trois montures y trottaient, soulevant la poussière en un nuage gris. Brinquebalant sur les bêtes, de jeunes garçons s'efforçaient de tenir en selle, grimaçant à cause des douleurs et des crampes. Aucun son ne sortait néanmoins de leur bouche, subjugués qu'ils étaient par le regard impérieux de leur père Thomas, petit homme au physique tassé, pérorant depuis le centre de la carrière, les sourcils froncés, le front plissé et la moue sévère, occupé à cingler l'air de sa badine pour maintenir le rythme.

«&nbsp;Talons en bas&nbsp;! Yeux au loin&nbsp;!&nbsp;»

Ses ordres claquaient comme sa baguette et il accompagnait parfois sa réprimande d'un caillou adroitement lancé pour bien marquer le destinataire de l'instruction. Sourcillant plus encore, il huma le temps, inquiet qu'une averse ne vienne obscurcir le ciel, amenant d'autant plus vite la nuit. Il payait les montures à la journée et n'aimait guère voir ses deniers partir sans en tirer la quintessence.

«&nbsp;Rufin&nbsp;! On te dirait pareil à sac mal arrimé&nbsp;! Ne peux-tu faire corps plus étroit avec ta bête&nbsp;?&nbsp;»

Il s'accorda une longue lampée d'une calebasse où macérait un vin largement coupé d'eau, dont il tenait que c'était un bon fortifiant pour demeurer l'esprit alerte et le corps échauffé. Il se gargarisa de sa boisson, appréciant sans le montrer ce qu'il voyait de ses fils. Il leur avait payé des cours d'équitation, dispensés par un sergent d'armes de la maison du roi et, à en observer le résultat, il estimait que c'était là fortune bien dépensée. Selon lui, le fait de tenir en selle différenciait le commun du notable. Il avait donc consenti à ce sacrifice avec la même abnégation qui le faisait économiser pour doter correctement ses filles. Il avait les mains brûlées par son commerce de regrattier de sel[^91] et voulait à toutes forces voir ses enfants arriver plus haut qu'il ne s'était élevé.

Il avait donc concocté un programme pédagogique dont il prévoyait qu'il ferait de Rufin, Gauguein et Baset de solides prétendants à devenir hommes d'importance, sinon de renom, dans le royaume. Il estimait avoir fait sa part en ce chemin, une seule génération le séparant de la pauvreté la plus indigente, ses parents ayant récolté le sel pour d'autres, y abandonnant leur santé. À peine avait-il compris qu'un destin identique l'attendait qu'il avait fui son Languedoc natal, rejoignant la cohorte qui avait pris la route de la Terre sainte après l'appel lancé par le Pape. Il narrait parfois à la veillée les moments forts qu'il avait connus lors de ce long périple, mais sans jamais en évoquer le dénouement, sinon en une ellipse qui le dévoilait agenouillé sur le tombeau du Christ. Il avait été de ces malheureux débandés dans les plateaux anatoliens, pourchassés et exterminés par des Turcs impitoyables. Le seul souvenir qu'il ne pouvait en cacher était une claudication et celui qu'il tentait de dissimuler, une franche terreur à la vision d'archers montés.

Estimant que la pratique avait assez duré, il siffla la fin de la leçon et invita ses fils à s'approcher. Il leur donna à chacun un quignon et leur accorda une belle lampée, qu'ils avalèrent sans se faire prier.

«&nbsp;Il va être temps assez de s'en retourner à l'hostel. Frictionnez donc un peu vos bêtes.&nbsp;»

Tandis que les garçons s'exécutaient en silence, il en profita pour vérifier qu'ils y mettaient tout le soin qu'il attendait d'eux. Lorsqu'il vit Rufin inspecter négligemment les pieds de sa monture, il lui décocha une gifle sur l'arrière du crâne.

«&nbsp;Veux-tu risquer de la faire boiteuse&nbsp;? Penses-tu que le loueur aura plaisir à retrouver une bête estropiée&nbsp;?&nbsp;»

Puis il lui intima de recommencer les quatre sabots, cette fois sous son contrôle. Le cheval, sentant la nervosité, se montra rétif et agité, ce qui compliqua d'autant plus la tâche à son fils. Ce dernier s'exécuta pourtant sans un mot, craignant de recevoir, en plus d'une nouvelle réprimande, un autre soufflet. Rufin étant le plus âgé des trois, Thomas avait la main fort leste avec lui, mais s'efforçait de ne frapper que pour corriger et pas dans l'emportement du mécontentement ou de la colère. Il estimait sa violence tout éducative.

«&nbsp;Baset, pour s'en retourner, je prendrai ta bête. Tu iras en croupe&nbsp;!&nbsp;»

Le benjamin acquiesça sans mot dire et commença à régler les étrivières plus longues pour permettre à son père de grimper en selle. Surveillant ses moindres gestes, Thomas cherchait un motif de le corriger, conscient qu'il ne lui accordait pas tant de considération qu'aux autres, mais le gamin n'offrait que peu l'occasion de prêter le flanc à la critique. Il obtempérait docilement, avait la réussite modeste et l'échec plus discret encore, soucieux de ne jamais attirer l'attention sur lui. Son père tentait de ne pas le négliger malgré cette incroyable faculté à se faire oublier. Il en concevait quelque angoisse à l'idée que cela dénotait chez lui un manque d'ambition et veillait donc à en nourrir les appétits aussi fortement qu'il le pouvait.

«&nbsp;Tu veilleras à secouer ta cotte, elle est tellement grise de poussière qu'on te croirait mendiant crotteux&nbsp;! Je n'ai guère désir de te voir frotter à mes habits, sale comme tu es.&nbsp;»

Baset n'osa qu'un timide «&nbsp;Oui, père&nbsp;» en réponse et s'activa immédiatement à brosser ses vêtements avec vigueur. Il en était désespérant de docilité, à craindre qu'il ne fût jamais bon qu'à obéir. Thomas lança un œil vers Rufin et s'avança vers lui en secouant sa badine.

«&nbsp;Rufin&nbsp;! Serre donc ta sous-ventrière, bougre d'âne&nbsp;! Tu vas finir le cul à terre si on repart ainsi&nbsp;!&nbsp;»

### Casal Techua, fin de matinée du vendredi 15 septembre 1139

Le village n'était pas un hameau de colons, mais une ancienne agglomération dans laquelle les seigneurs latins avaient érigé une maison forte. Quelques bâtisses neuves s'y étaient accolées, pour les plus aventureux des pèlerins qui avaient eu le désir de faire souche ici en profitant des avantages fiscaux accordés.

Pour le moment, la seule habitante en était une poule, occupée à gratter le sol aux abords d'un abreuvoir de pierre. Elle observait d'un œil circonspect la troupe armée qui s'était égaillée en tout sens. Ses membres avaient constaté l'abandon du village, le forçage de quelques portes et inspectaient les lieux sans rencontrer aucune résistance.

Arrivé parmi les derniers, avec les sergents du roi, Baset avait trouvé une cruche partiellement brisée dont il se servait comme d'un gobelet. La soif lui brûlait la gorge, avec toute la poussière avalée à chevaucher à l'arrière du convoi venu de Jérusalem au matin. La veille, il avait été réquisitionné par le porte-bannière du roi, Bernard Vacher, pour se joindre à un groupe de la milice du Temple. Une bande de turcs ou de turcmènes avait été signalée dans les abords de ce hameau, profitant de l'absence de l'armée royale, en campagne loin à l'est dans le Hauran, pour mettre au pillage le secteur.

Baset avait espéré y couper, comptant sur son inexpérience au combat, mais à son grand désarroi on l'avait désigné comme bon cavalier. Il avait prêté serment quelques semaines plus tôt, pleinement satisfait que ses tâches fussent essentiellement administratives, en plus du guet. Même là, leur nombre faisait qu'ils avaient rarement à tirer l'épée ou pointer la lance, ce qui lui convenait parfaitement. S'il estimait important de servir loyalement, il n'avait guère envie que cela se fasse au détriment de sa survie.

Il avait donc été soulagé de découvrir que les brigands avaient décampé, ne voulant pas se frotter à une troupe armée alors qu'ils avaient fait provision de butin. Toutes les portes n'avaient pas été défoncées, mais les pillards avaient tenté de s'introduire un peu partout, profitant de la moindre fenêtre pour aller fouiller puis répandant les reliefs de leur tri ici et là au hasard des rues.

Drouet, qui dirigeait le petit groupe de sergents du roi, attendait les instructions de Bernard Vacher, en grande concertation avec le maître de la milice. Ce dernier semblait décidé à donner la chasse aux détrousseurs, ce qui ne soulevait guère d'enthousiasme chez Drouet, normand au caractère grognon.

«&nbsp;Je serais bien mal engroin qu'on aille risquer nos fesses pour la picorée de ces brigands. Nul n'est mort ici et ils n'ont guère eu le temps de faire bombance...

--- Il n'y avait donc personne dans la tour grosse pour s'opposer à eux&nbsp;? demanda Baset.

--- S'il y avait là soldaterie, elle a dû mener les habitants en quelque abri. Les vallées à l'entour comportent grottes et caves à foison. De quoi se rembarrer solidement le temps que la tempête passe.

--- Ils ne craignent guère pour leur grain&nbsp;!

--- Ces brigands ne sont pas barons tels Sanguin[^92] ou Anour[^93]&nbsp;! Ils ne vont pas s'encombrer avec si lourd butin. Les nomades grappent étoffes et tout ce qui a de la valeur sans trop peser. Les vilains s'encachent avec leur avoir, abandonnant ce qui n'est que de peu de prix à leurs yeux.&nbsp;»

Un des chevaliers de la milice, l'allure sévère et le visage fermé, s'approcha d'eux en menant son destrier au pas. Il désigna Drouet d'un coup de menton.

«&nbsp;On m'a dit que tu savais bien la route pour se rendre à Saint-Abraham[^94]&nbsp;?

--- Oc, sire frère de la Milice. Un mien cousin a ses mouches à miel dans la région.

--- Tu ouvriras la voie en ce cas&nbsp;!&nbsp;»

Puis il se tourna vers Baset.

«&nbsp;Tu nous compaignes aussi, si jamais il faut adresser messages depuis là-bas.&nbsp;»

Drouet et Baset hochèrent la tête et se mirent en branle sans oser questionner le chevalier sur la situation. Sa mine austère et son regard lointain n'incitaient que peu au dialogue. En outre, Baset se méfiait de ces soldats impétueux, dont il reconnaissait le courage, mais redoutait la témérité. Du moins quand cela pouvait déboucher sur un danger pour lui.

### Casal Techua, chemin méridional menant vers Hébron, midi du vendredi 15 septembre 1139

Le trio de cavaliers menait bon train, faisant voler les graviers et la poussière. Ils longeaient les reliefs en une piste ondulant vers le sud jusqu'à une voie vers l'ouest et Hébron. De nombreux pruniers y côtoyaient les inévitables oliviers. Sur les versants les mieux exposés se déployaient des vignes dont les vendanges ne sauraient tarder. Aucune âme n'était visible aux environs, chacun ayant pris soin de se dissimuler dès la bande de pillards repérée. Les pâtres avaient rassemblé leurs bêtes dans des ruines et des cavernes connues d'eux seuls et les villages ne pouvant opposer une résistance efficace s'étaient vidés, en direction de caches et de retranchements.

Les paysans de la région avaient fini par s'habituer aux allées et venues des groupes de voleurs&nbsp;: cela faisait plus d'un siècle qu'ils en étaient les victimes. N'ayant nulle terre lointaine où s'enfuir, ils subissaient les assauts et les affronts en priant à la mosquée, l'église ou la synagogue, pour que leur Dieu les débarrasse au plus vite de ces détrousseurs.

Baset gardait la tête baissée, s'efforçant de suivre le rythme haletant imposé par le templier. Il en voyait le blanc manteau qui dansait devant sa propre monture, plissant les paupières lorsque le soleil accrochait un éclat métallique sur le casque peint du chevalier. Baset avalait la poussière des deux cavaliers malgré le foulard dont il s'était entouré le bas du visage et sentait les larmes s'agglomérer en un ciment crasseux le long de ses tempes. Il y avait longtemps que sa bouche n'était que de carton et son nez plâtré et il espérait à chaque lacet découvrir les fortifications d'Hébron. Chaque courbe se dévoilait comme une nouvelle épreuve qu'il s'efforçait de passer l'une après l'autre.

Soudain, alors qu'ils abordaient une courte descente en surplomb de quelques ruines de terrasses en friche, il aperçut plus qu'il ne vit le cheval de Drouet faire un brusque écart, à se jeter contre des broussailles avant de s'effondrer subitement. Le templier n'eut pas le temps de réagir que sa propre monture sautait avec lui dans le vide à droite, laissant le champ libre pour que le roussin de Baset décide de son propre chef de bondir par-dessus l'obstacle.

Baset en perdit les étriers, retomba durement sur la selle, en échappa une des rênes. Se cramponnant de la jambe au troussequin, il empoigna une touffe de crin. Sans plus rien contrôler, il s'efforçait de ne pas être projeté à terre tandis que la bête se mettait à ruer, gênée par ce poids mort en déséquilibre sur son dos.

À demi aveuglé et peu enclin à observer son environnement, il n'eut pas le temps de voir le Turcmène vers lequel sa monture le menait qu'il vola en sa direction, fesses en avant, propulsé par un coup de cul circulaire. Le choc violent lui fit grincer les dents, vida ses poumons et résonna de ses reins à ses épaules.

Il reprit ses esprits pour se découvrir une jambe en l'air coincée dans la fourche d'un buisson épineux, le nez dans la terre et le bras tuméfié, allongé en partie sur le Turc. Lorsqu'il se tourna, il constata qu'il en avait écrasé le visage sur une pierre en le heurtant. Hagard, il tenta de se remettre dans une position plus confortable et vit qu'il avait brisé aussi l'arc et des flèches. Frottant ses reins, il y palpa des meurtrissures à travers les déchirures de sa cotte. Il se sentait nauséeux et hoqueta plusieurs fois, à la limite du vomissement.

Lorsque ses haut-le-cœur se calmèrent, il prit conscience d'une voix qui appelait. Il fut surpris d'en comprendre les mots. Il se mit debout, encore vacillant et flageolant sur ses jambes. La poussière retombait doucement, recouvrant mollement le sol. Il aperçut la monture de Drouet, deux flèches profondément enfoncées dans la gorge, un antérieur clairement fracturé et un flanc écorché par sa selle brisée. Elle claudiquait sur le chemin, dans l'espoir de rejoindre le cheval de Baset, dont un nuage poudreux indiquait la direction de fuite.

Drouet gisait, tel un amas de chiffon, le cou rompu et les membres en désordre, curieusement entassé contre un rocher. Il n'avait plus rien à y faire et Baset se contenta de lui dédier un rapide signe de croix. La voix persistait, c'était donc le templier qui appelait. Il avait chuté avec sa monture dans un petit ravin. La pauvre bête avait répandu le contenu de son crâne sur une pierre et, par sa mort, avait immobilisé son cavalier sous elle. Le chevalier échappa un soupir de soulagement en voyant Baset.

«&nbsp;Jambe Dieu&nbsp;! Tu as occis ce maudit Turc&nbsp;! Grâce à Dieu, tu as eu le bras plus vaillant que ses flèches traîtresses.&nbsp;»

Baset hocha la tête sans mot dire. Il ne voulait pas détromper le chevalier en lui expliquant que Dieu avait choisi une partie bien moins noble de son anatomie pour mener à bien son dessein. Il descendit péniblement dans la petite combe dans l'espoir de porter secours au blessé. Lorsqu'il tenta de le tirer, l'autre hurla soudainement de le lâcher. Puis il haleta, le visage cramoisi de douleur, les traits enfiévrés.

«&nbsp;Par la malepeste, j'ai la jambe brisée, je crois bien&nbsp;! Je ne sens que plaies et horions là-dessous&nbsp;!&nbsp;»

Il respira longuement plusieurs fois, dévisageant Baset.

«&nbsp;Ton cheval est où&nbsp;?

--- Enfui. Il m'a jeté sur le Turc et n'a pas demandé son reste.

--- Il n'y a personne aux alentours, au moins&nbsp;? Il est rare que ces pillards soient esseulés.&nbsp;»

Baset sentit soudain sa vessie prête à se vider et son visage perdit toute couleur. Il risqua un œil inquiet autour d'eux. Le templier comprit bien vite qu'il n'était pas de l'étoffe dont on fait les grands soldats.

«&nbsp;Regarde à l'entour. Il y a peu de chances qu'il se soit trouvé ici tout seul, à pied. Ces démons ne vivent sans monture.&nbsp;»

Encore peu assuré sur ses appuis et sentant des douleurs sourdre de chaque partie de son corps, Baset crapahuta de son mieux aux abords. Rien ne bougeait autour d'eux, ce qui le rasséréna un peu. Il explora le coin avec d'autant plus de détermination, pour y dénicher finalement un bel étalon alezan attaché dans un bosquet. Vu son équipement, c'était là la monture du Turc qui les avait attaqués. Un sac posé au sol contenait des affaires de palefrenier, sans qu'il en connaisse l'usage exact. Étudiant la bête, il comprit bien vite que l'homme s'était arrêté pour soigner une boiterie. Néanmoins, les oreilles aux aguets, elle se laissa mener sans problème.

Il traversa de nouveau le chemin, mais le cheval refusa de s'approcher du cadavre, renâclant et menaçant de lui échapper. Il l'attacha à un arbre et retourna voir le templier.

«&nbsp;Il semble avoir eu souci avec sa monture. Elle refuse d'approcher et je n'ai pas trouvé longue corde pour vous dégager avec son aide.&nbsp;»

Le teint blanc, le templier acquiesça. Il sentait ses forces l'abandonner. Peut-être était-il plus blessé qu'il ne l'avait cru de prime abord. Tant qu'il serait bloqué sous le cheval, il ne pourrait le savoir exactement.

«&nbsp;En ce cas, tu vas prendre cette bête afin de retourner au casal demander de l'aide à mes frères.

--- Je ne peux vous abandonner. Si jamais d'autres pillards s'en venaient&nbsp;?

--- De cela je ne doute guère, d'autant qu'ils vont voir arriver à eux ton cheval avec un harnachement qu'ils sauront reconnaître comme ennemi. Peu me chaut, j'ai juré en les mains du maître et mourrai glaive au poing si c'est la volonté de Dieu. Mais de cela rien n'est joué, contente-toi de galoper au loin. Ta célérité est mon espoir.&nbsp;»

Baset hésita à lui dire qu'il ne saurait aller bien vite avec un cheval boiteux, mais il se contenta de lui laisser à portée de main une outre qu'il avait dénouée de la selle du Turc.

«&nbsp;Je reviens au plus vite, sire chevalier. À Dieu vous mande&nbsp;!

--- Vole, maraud, pour mon salut. Et pour ton priement à Dieu, remembre-lui mon nom&nbsp;: Rolant...&nbsp;»

### Jérusalem, Saint-Sépulcre, matin du lundi 15 septembre 1152

Des nuages d'encens se répandaient depuis la rotonde du tombeau du Christ jusqu'au petit autel annexe où Baset avait habitude de venir se recueillir. Comme chaque année, il avait porté un cierge de belle taille et lâché une pièce dans le tronc des messes, puis s'était installé pour y réciter les seules prières qu'il connaissait : *Ave*, *Pater* et *Credo*. Il le faisait toujours dans cet ordre, car il estimait qu'il était plus habile de commencer par en appeler à la Vierge, plus encline à se montrer bienveillante, puis au Père qui saurait ne pas être jaloux de son attention première. Enfin, il glissait ses demandes avant de confirmer, par sa litanie finale, qu'il était bon chrétien, afin de rassurer Dieu de son éligibilité à voir ses vœux exaucés.

Finissant ses oraisons, il se signa rapidement et prenait le chemin du palais pour y débuter son service lorsqu'il fut apostrophé par un des chanoines. Habillé de belle laine noire, il avait le visage replet et le regard franc, un sourire aimable figé sur la face. Son allure bonhomme incita Baset à froncer les sourcils, toujours méfiant des sollicitations qui se préparaient derrière si avenantes façades. Il avait trop souvent eu affaire à des menteurs et des escrocs, que ce soit au péage des portes ou lors des rondes du guet, pour conserver une opinion favorable envers ses prochains.

«&nbsp;Le bon jour à vous, mon fils. On ne vous voit pas si souventes fois en nos murs, mais il me semble que vous y brûlez chaque an beau cierge en cette date. Seriez-vous nommé Nicomède ou lié peu ou prou à lui&nbsp;?&nbsp;»

Décontenancé par la question dont il ne voyait pas l'intérêt, Baset suspecta immédiatement le préambule à une quête, à laquelle il comptait bien se dérober. Il se fit le visage le plus rétif qu'il avait en réserve et répondit d'un ton morne.

«&nbsp;Jamais ouï parler de lui, père chanoine. Désolé...

--- Faites excuse, j'avais cru, à vous voir ici fêter ainsi son martyr. Il est de certes fort déconnu des fidèles et, même les plus instruits de nous n'en savent guère en dehors de ce que nous en a dit le Saint-Père Adrien.&nbsp;»

Le prêtre inclina la tête, espérant que Baset se dévoilerait un peu plus. Avant que le silence ne tourne au malaise, ce dernier se résolut à répondre a minima au clerc, estimant qu'il semblait inoffensif et, surtout, simplement curieux. Il pourrait ainsi s'en débarrasser à bon compte.

«&nbsp;Il se trouve que je viens prier pour Rolant.

--- Un vôtre parent&nbsp;?

--- Aucunement.

--- Est-ce quelqu'un qui a rendu son âme à Dieu en ce jour&nbsp;?

--- Je ne sais.

--- Pourquoi donc le choix de ce jour&nbsp;?

--- Il serait bien temps qu'on fête les Rolant, et hui me paraît aussi bon qu'un autre.&nbsp;»

Il haussa les épaules, adressa un dernier message muet à la flamme du beau cierge et s'en fut sans un mot de plus. Nullement rebuté, le chanoine dédia un sourire désabusé au sergent qui lui tournait le dos, puis, machinalement, ajusta le placement de la nappe d'autel brodée.

«&nbsp;Saint Rolant&nbsp;! Que voilà étrange lubie&nbsp;! Le pauvret aura trop entendu les aventures du grand Charles[^95]&nbsp;!&nbsp;» ❧

### Notes

Le baptême du feu de Baset, pour fort peu héroïque qu'il soit, se déroule lors d'un des nombreux affrontements dont il nous est peu parlé dans les sources. Guillaume de Tyr indique en quelques lignes que les Templiers y perdirent beaucoup d'hommes face à des pillards. Il ne faut pas imaginer une belle bataille rangée, mais une série d'escarmouches et d'embuscades qui prenaient place sur un assez vaste territoire. Comme souvent, les effectifs de chaque côté étaient très faibles, bien plus proches de la centaine que du millier.

La scène finale joue avec le fait que le 15 septembre soit désormais bien la saint Roland, en mémoire d'un Italien du XIVe siècle et pas du tout de ce malheureux chevalier que j'évoque. Si j'ai choisi cette date, c'est pour illustrer le fait que les choses ne sont pas toujours aussi assurées qu'on le croit dans les héritages culturels. Il y a souvent jonction, fusion, contagion entre des traditions diverses pour établir des idées considérés comme avérées et univoques par la suite. La recherche historique se bâtit à rebours en allant à la recherche des sources, parfois bien moins définitives que les certitudes communément enseignées.

Enfin, le titre de ce Qit'a, *Semper fidelis*, «&nbsp;Toujours fidèle&nbsp;» évoque à la fois la question de la fidélité des sources et aux sources, de l'image que l'on se construit du passé, mais également de la fidélité de Baset à son caractère, aux ambitions de son père, à son maître le roi et à son propre souvenir traumatisant. Mais c'est aussi une devise très guerrière qui peut faire référence au courage du templier, cette antienne ayant été adoptée par de nombreux combattants dans l'histoire, dont le corps états-unien des Marines est certainement le plus connu.

### Références

Guillaume de Tyr, *Historia rerum in partibus transmarinis gestarum*, Recueil des Historiens des Croisades, Occidentaux tome I

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem. A corpus. Volume I. A-K*, Cambridge&nbsp;: Cambridge University Press, 2008.

Edgeller Johnathan James (B.A.), *Taking the Templar Habit: Rule, Initiation Ritual, and the Accusations against the Order*, MA Thesis, Texas Tech University, 2010.

Forain
------

### Mezze, ouest de Damas, fin d'après-midi du lundi 12 août 1157

Ouvrant la voie à Toufik et son âne, Guilhem veillait à s'écarter du chemin dès qu'il apercevait ou entendait des montures. Au plus chaud de la journée, ils s'étaient avancés dans un bosquet de palmiers de façon à ne pas attirer l'attention sur eux. Inquiet de ne pas être pris pour des pillards par les fellahs des environs, Guilhem avait jugé plus sûr de s'installer pour une sieste apparente, tout en gardant les yeux en éveil. Pendant ce temps, Toufik, toujours muet comme une carpe, s'était abandonné à un vrai repos. Ils avaient ensuite amassé un peu de bois mort qu'ils avaient entassé sur le dos de leur bête.

Ils avaient croisé avec crainte quelques cavaliers arrivés trop soudainement pour être évités et même une troupe de Turcs en armes, mais aucun n'avait fait attention à eux. Le tremblement de terre ne semblait pas avoir fait grand dégât[^96], mais avait déclenché une vive agitation. Les hameaux qu'ils avaient traversés bruissaient des travaux de remise en état et de l'angoisse qu'avait fait naître la catastrophe naturelle. De-ci de-là, des bêtes s'étaient échappées et s'aventuraient dans des cultures ou dans les zones désertes. Guilhem avait veillé à ne pas s'en approcher plus que nécessaire. Sa crainte était d'être pris à partie et de devoir justifier de son identité. Il pouvait passer pour un paysan aux yeux des voyageurs, mais ne saurait être reconnu comme l'un d'eux par les hommes de la région. Dans chaque lieu, ils se connaissaient tous et avaient l'habitude des visages et apparences des familiers du secteur.

Il avait décidé de s'éloigner le plus possible des faubourgs de Damas en direction de Baniyas et la frontière avec le royaume latin, profitant du temps moins étouffant de fin de journée. Il n'était pas sans croiser d'autres personnes qui s'activaient aussi, maintenant que les ardeurs du soleil étaient un peu retombées. Au détour d'un lacet dans les reliefs méridionaux du Ğabal Qasyun, il s'arrêta pour faire boire l'âne dans un mince filet qui sourdait d'une crevasse surplombant un petit bassin grossièrement aménagé. Il en profita pour jeter un regard vers la cité de Damas en contrebas. Noyée dans son oasis de verdure, elle semblait exhaler des fumées en longues traînées. De si loin, on n'y discernait nulle animation suspecte, pas plus que de patrouilles étirées sur un chemin ou l'autre.

Il allait reprendre la longe de son âne pour l'inciter à repartir quand il entendit soudain le vacarme d'une troupe. Avant qu'il n'ait le temps de trouver une échappatoire, il vit s'avancer à lui une belle caravane, guidée par un homme sur un vieux baudet au poils miteux. Sans se soucier de lui, celui-ci indiqua une halte d'un geste impérieux aux siens, certainement dans l'intention de profiter du point d'eau. Guilhem n'était pas trop inquiet, ils venaient du sud et allaient vers Damas. Il ne pouvait se rencontrer parmi eux des personnes à sa recherche. Il prit également conscience que ce n'étaient que des marchands, escortés de quelques gardes.

Il s'écarta du passage, inclinant respectueusement la tête et s'employa à se remettre en marche. Il n'avait pas fait trois pas que le chef caravanier avait placé sa monture en travers de son chemin. Il n'était pas armé, si ce n'était d'un immense fouet de cuir autour de la taille, et ne semblait pas animé de mauvaises intentions. Il le salua du bout des lèvres et lui demanda d'où il venait.

«&nbsp;Je suis de Berzé, venu à Dimashq pour y vendre quelques affaires, mais la terre y a tremblé si fort que j'ai préféré m'en éloigner.

--- Nombreux ici ont leur famille en la cité. Compte-t-on beaucoup de morts&nbsp;? Des dégâts&nbsp;?

--- Je ne saurais dire, j'étais à peine en la Ghûta et je n'ai pas été en les murs...&nbsp;»

Le caravanier l'examina plus étroitement. Guilhem savait que son accent n'était pas crédible pour la région, ayant appris à parler dans les quartiers d'Alexandrie et en Égypte. Il espérait malgré tout qu'il l'était suffisamment pour passer pour un arabe. Un latin en ces lieux, seul, ne pouvait être qu'un espion.

«&nbsp;Quel est est commerce&nbsp;? Tu me sembles bien loin de chez toi&nbsp;!

--- Je n'ai plus de chez-moi, les Ifranjs ont pris ma cité, Asqalan[^97] et les soldats qui avaient fait jurement de nous faire escorte nous ont détroussés. Je n'ai gardé que ce gamin, seul à en avoir échappé avec moi.&nbsp;»

Son explication lui parut satisfaire la curiosité du guide, qui descendit de son âne et fit signe à tous d'abreuver les bêtes. Bientôt, plusieurs hommes vinrent poser le même genre de questions à Guilhem. Heureusement pour lui, aucun ne sembla connaître Ascalon ni y avoir des relations. C'étaient pour la plupart des marchands de victuailles qui suivaient l'armée de Damas dans les campagnes proches afin de ravitailler les soldats. Ils revenaient du fructueux assaut de l'émir sur la cité de Baniyas[^98] et se félicitaient du butin qui avait pour une bonne part fini dans leurs escarcelles. L'un d'eux, rond de visage et fort volubile, se présenta comme le mâalem[^99] d'une des zones du souk al-askar. Il était prêt à lui faire un prix sur un emplacement, s'il était intéressé, un de ses marchands ayant malheureusement péri de fièvres. Il adopta un court moment la face empreinte de dépit qu'il convenait, mais ne s'attarda pas sur une trop longue commisération, conscient qu'il avait là une occasion de récupérer le montant d'une patente qu'il avait perdue par la faute de la maladie.

Comprenant que l'homme était plus enclin à parler de lui et de commerce qu'à s'inquiéter de son identité, Guilhem l'incita à bavarder, notant au passage les noms et les anecdotes dont il émaillait son discours. Quand il vit que la caravane allait reprendre sa route, certainement dans l'espoir d'arriver aux murailles à la nuit, il s'enquit respectueusement de l'endroit où il pourrait visiter le mâalem en ville, dans l'espoir qu'il était, prétendait-il, d'avoir des produits qui seraient utiles à l'armée de l'émir.

Plusieurs négociants l'incitèrent à faire demi-tour, aucune trêve n'ayant été signée avec les Latins, ce qui mettait en grand péril le bon croyant esseulé qu'ils pourraient capturer à leurs frontières. Il fit mine d'accéder à leurs conseils et affirma être résolu à bifurquer vers Busrâ[^100] où il se trouvait d'autres rescapés d'Ascalon. Les marchands l'approuvèrent, ayant entendu que les troupes semblaient décidées à s'affronter plus au nord, là où le soleil ne les cuirait pas dans leurs armures, plaisantèrent-ils. Ils se quittèrent dans d'amicales dispositions, des bénédictions réciproques aux lèvres.

Lorsqu'ils se furent éloignés du petit groupe, de nouveau seuls avec leur âne sur le chemin caillouteux, le visage de Toufik s'illumina d'un timide sourire. Sans rien ajouter, Guilhem hocha la tête et lui en adressa un en retour. Devant eux, le soleil lançait ses derniers feux tout en s'effaçant derrière les reliefs. Il était temps de trouver un endroit pour la nuit, un peu en retrait du passage. La lune serait trop jeune pour leur permettre d'avancer en sécurité.

### Bait Jann, aube du vendredi 16 août 1157

Le chemin semblait n'en plus finir pour les deux voyageurs. Guilhem persévérait dans sa stratégie visant à s'écarter le plus possible de la voie principale, espérant être pris pour un simple fellah occupé dans la campagne. À chaque fois qu'ils le pouvaient, ils louvoyaient d'un côté à l'autre, n'avançant que fort peu dans la direction qu'ils suivaient réellement. Cela leur permit de voir passer plusieurs convois, dont la plupart semblaient n'être que des caravanes de marchandises ou de voyageurs. Guilhem savait qu'en rejoindre une, pour n'être pas forcément évident, aurait été idéal, quoiqu'il n'ait guère de quoi payer pour cela, mais il craignait la promiscuité. Il n'était pas impossible qu'on le vende à une patrouille au service d'un potentat local si on le découvrait esclave en fuite. Comme Toufik, le bracelet de fer qu'il avait dissimulé au mieux sous ses manches pouvait le trahir à chaque instant, sans parler même du baratin sur son identité, qui ne pourrait soutenir un examen serré.

Ils avaient dormi dans un petit abri sous roche prêt d'une oliveraie un peu en retrait du village de Bait Jann, le dernier sous le contrôle de Damas avant la frontière. Ils en étaient repartis avant la fin de la nuit, préférant comme la plupart des voyageurs avancer avant que la chaleur du jour ne fût trop forte. Jusqu'à longer cette combe où courait un léger torrent, ils avaient tout particulièrement souffert de la soif, n'ayant qu'une outre pour se désaltérer, qui plus est percée. Ils avaient encore un peu d'olives et du pain, mais leur estomac leur semblait bien creux pour parcourir toutes ces lieues.

L'avance se fit difficile lorsqu'ils abordèrent la montée d'un col. Cela faisait un bon moment qu'ils se trouvaient obligés de demeurer sur le chemin, d'abord au fond d'un vallon encaissé et peu large, qui serpentait en direction de l'ouest puis dans une boucle ascendante, vers le nord pour enfin redescendre ensuite vers le sud. Ils contournaient le Mont Hermon, où étaient certainement posté des éclaireurs surveillant la frontière. Travaillant son rôle en cas de mauvaise rencontre, Guilhem avait disposé sur le dessus de leurs paquets quelques outils qu'ils avaient ramassés avant de fuir les décombres du chantier détruit par le tremblement de terre. Il espérait juste qu'on ne lui ferait pas demande de montrer son savoir-faire en maçonnerie, ni même qu'on regarderait ses mains de trop près. La seule corne qui s'y trouvait était celle qu'il s'était faite sur le pouce à force de manier l'épée.

Un énorme chêne marquait normalement la frontière avec le royaume de Jérusalem, mais Guilhem n'était pas certain que ce fût toujours le cas. Il avait appris, pour en avoir parlé rapidement avec des voyageurs croisés, que Baniyas n'était pas tombée face à l'émir, mais il avait peut-être maintenu une forte présence militaire pour s'assurer de la zone. Sans trêve officielle, c'était là une marche bien dangereuse à fréquenter lorsqu'on ne faisait pas partie d'un imposant convoi.

À un détour du chemin, il aperçut un petit groupe de cavaliers qui venait à leur rencontre. Aucun animal de bât ne les accompagnait, c'était vraisemblablement des soldats, du moins des hommes en armes. Même en plissant les yeux, il ne sut dire si c'étaient des nomades, des Turcs, des Arabes ou des Latins. Lorsqu'il comprit que c'étaient apparemment une patrouille du pouvoir damascène, il ralentit le pas et se mit en devoir de leur céder le passage comme si de rien n'était. Il baissa la tête en signe de respect, dissimulant en partie son visage. Ce fut peine perdue, ils stoppèrent à son abord. La voix qui l'apostropha était rude et l'accent turc très marqué. Au moins seraient-ils plus aisément floués par son arabe.

«&nbsp;Que fais-tu ici sans guide ni caravane&nbsp;? Ne sais-tu pas que des brigands ifranjs infestent ces contrées&nbsp;?

--- Je ne suis que modeste maçon, j'ai espoir de m'en retourner vers les miens, à Misr[^101].

--- En passant par ici, fou que tu es&nbsp;? Tu es bien loin des tiens...

--- J'avais fui avec ma famille lorsque Asqalan fut prise par les Celtes. J'ai renvoyé ma femme et mes enfants auprès des nôtres dès que j'ai pu leur payer le passage, voilà quelques mois. Il me fallait finir un chantier pour lequel je m'étais engagé. Je n'ai gardé que ce ghulam[^102] pour m'assister.&nbsp;»

Les membres du petit peloton ne faisaient guère attention à lui, laissant leur officier décider du sort à faire à ceux qu'ils rencontraient. Selon qu'ils étaient suffisamment bien payés et traités, ils obéissaient plus ou moins aux consignes données par les princes des territoires, mais ils ne dédaignaient parfois pas d'enfreindre les instructions, surtout si la perspective de butin était forte. Guilhem savait qu'il ne fallait pas leur suggérer l'idée qu'il possédait quoi que ce soit de valeur. Il poursuivit son récit en soupirant, l'air pitoyable.

«&nbsp;Las, j'ai été floué par ce mauvais maître, qui m'a chassé sans gages une fois mon ouvrage achevé. Me sachant sans famille, il a tenté de me dépouiller. Sans la fidélité de mon Toufik, qui m'a rejoint nuitamment avec quelques affaires qui m'appartenaient, je serais aussi nu qu'à ma naissance. J'ai donc décidé de prendre la route pour retrouver les miens, n'ayant que trop vécu au loin.&nbsp;»

Giulhem se demandait si on n'attendait pas de lui quelques monnaies. N'en ayant aucune à proposer, il ne pouvait compter que sur sa bonne mine et son histoire cousue de fil blanc. Au pire, il pourrait leur abandonner Toufik, mais le gamin n'aurait guère de valeur avec tous les captifs qui avaient été amassés dans la province depuis le début de l'année. Il inventoriait tout ce qu'ils avaient dans leurs sacoches dont il pourrait se défaire quand un des soldats siffla en indiquant la vallée derrière lui. Un épais nuage de fumée en montait, porté par un vent ascendant qui y dessinait des volutes.

En quelques instants, les chevaux furent remis en position. L'officier échangea quelques mots avec ses hommes et, sans plus jeter un regard à Guilhem, il lança sa petite troupe au trot. Guilhem lâcha un soupir de soulagement et reprit la route avec empressement. Un plus gros gibier avait fait diversion. En pressant un peu le pas, Guilhem espérait être à Baniyas avant la tombée de la nuit.

### Baniyas, soirée du vendredi 16 août 1157

Lorsqu'ils obliquèrent enfin sur le chemin qui menait, par-delà les sources du Jourdain et le pont permettant de les traverser, vers la cité de Baniyas, Guilhem et Toufik poussèrent un soupir de soulagement. Leurs pieds étaient brisés de souffrance à déambuler dans les oueds asséchés. Guilhem avait choisi le trajet le plus long, mais aussi celui qu'il espérait le plus sûr. Ils y avaient récolté plaies et bosses, griffures et lacérations sur tout le corps. Ils s'enveloppèrent une nouvelle fois grossièrement les pieds dans des chiffons et repartirent, pressés de se retrouver en sécurité derrière les murailles.

Au-dessus de leurs têtes, l'outremer gagnait peu à peu sur le cyan et la lune dans son premier quartier commençait à dispenser sa lumière argentée. Un ressaut du terrain leur offrit la vision sur la cité lovée dans les contreforts du Mont Hermon. De nombreuses lueurs la ponctuaient d'ambre, y compris aux abords de l'enceinte. Guilhem ne connaissait pas bien les lieux, mais savait seulement que la place avait été l'objet d'âpres combats ces derniers temps. Il ne fut pas surpris de voir qu'un avant-poste veillait sur le pont et s'y avança avec circonspection. La ville demeurait aux mains des Latins, mais les esprits devaient être échauffés des récents affrontements. Sans compter que les hommes de faction n'étaient pas toujours aussi honnêtes et dévoués à leurs maîtres qu'il aimaient à le faire croire. Dans les zones de frontière comme ici, nombreux étaient ceux qui avaient un ami, une relation, un cousin, à qui revendre des informations ou un esclave échappé.

Une barrière de sacs et de branches avait été placée au milieu du passage, et quelques mantelets garantissaient d'éventuels archers ceux qui montaient la garde. Guilhem compta presque une dizaine d'hommes, ce qui n'était pas rien et lui sembla mauvais signe. Par contre, seuls trois d'entre eux paraissaient s'intéresser à son équipage, ce qui le rasséréna. Ils étaient menés par un géant au dos voûté, qui possédait à la ceinture un large carquois en plus d'une épée et d'une masse. Il avait le visage long et les cheveux en bataille, la tenue fatiguée, mais soigneusement entretenue. Il portait un haubergeon de mailles sans manches et avait remonté les manches de sa tunique. Il arrêta Guilhem d'un ordre sec avant qu'il n'arrive à leur hauteur et s'enquit en arabe de qui il était et de ce qu'il voulait. La zone était encore en guerre contre l'émir de Damas et les errants devaient justifier de leur présence sous peine de finir engeôlés. Guilhem lui répondit en provençal, langue latine qu'il connaissait le mieux, puis dut répéter dans celle des Francs de Jérusalem pour se faire comprendre. À ses côtés, Toufik demeurait ainsi qu'une statue, ses yeux seuls allant d'un homme à l'autre tandis qu'il tentait de suivre ce qui se passait.

«&nbsp;Je suis Guilhem de Gibelet, féal d'un des plus éminents membres de la Compania de Gênes, mon sire Mauro Spinola. J'ai été tenu en geôle sarrazinoise des mois durant. J'ai pu m'échapper voilà quelques jours. je demande asile, dans l'espoir d'aller retrouver les miens.&nbsp;»

Le géant fit quelques pas en avant, et examina plus attentivement les voyageurs.

«&nbsp;Il se trouve moult espies avides de voir si la cité pourrait tomber. Qui me dit que tu ne mens point&nbsp;? As-tu compère en nos murs, qui puisse jurer de toi&nbsp;? Tu m'as tout l'air d'un gagne denier de misère.

--- Je n'ai pas usance de venir en ces terres et j'encroie peu qu'on puisse jurer de moi ici. Mais je peux être mené à un bon père, qui saura vérifier que je connais l'*Ave*, le *Pater* et le *Credo*.

--- Pff, je saurais bien jurer de ma Mahomerie si ma vie en dépendait&nbsp;! Je ne laisserai clerc juger de cela.&nbsp;»

Il fit signe à quelques hommes de venir à lui puis alla chercher un grand arc qu'il avait laissé jusque-là à l'abri.

«&nbsp;Vous allez nous suivre. De plus avisés que moi sauront bien trouver s'il faut vous brancher ou vous laisser aller.&nbsp;»

Lorsqu'il vit qu'on le faisait passer la barrière pour le conduire en ville, Guilhem se laissa mener sans rechigner. Il n'était pas encore certain qu'on ne le dépouillerait pas, mais c'était sans importance pour lui. Tout ce qu'il espérait, c'était une façon de renouer le contact avec ceux pour qui il travaillait.

Il découvrit avec stupéfaction les dégâts des sièges qui s'étaient déroulés les semaines précédentes. Il n'osa pas en demander le détail, craignant qu'on ne trouve suspect son trop grand intérêt. Une des tours était complètement ruinée et des hommes, certainement des captifs, continuaient d'en dégager les alentours malgré l'heure tardive. De nombreux impacts attestaient de l'ampleur des opérations d'artillerie et de la détermination des assaillants. Bien que la ville semblât avoir été forcée, les portes avaient l'air d'avoir résisté, probablement ouvertes après une infiltration réussie, voire grâce à des complicités intérieures.

La cité était calme, comme c'était l'habitude après des combats. Généralement, seuls les commerçants les plus audacieux osaient faire leurs affaires, parfois à la dérobée, en ne laissant pénétrer les clients dans les souks qu'au compte-goutte. La plupart des habitants demeuraient claquemurés dans leur maison, espérant que leurs huisseries suffiraient à repousser un éventuel assaut.

La patrouille qui le menait s'arrêta devant une forte porte puissamment ferrée, qui ne s'ouvrit qu'après quelques échanges à travers un guichet lui aussi grillagé de bon acier. Guilhem et Toufik se retrouvèrent dans une large cour où donnaient de hautes voûtes, surmontées d'une galerie couverte. On leur noua les bras à une perche à côté d'un abreuvoir avant de les laisser à la surveillance d'un des soldats. Celui-ci ne tarda pas à somnoler, échappant à la monotonie de sa tâche en tentant de cracher sur tous les cailloux environnants.

Lorsque le grand archer revint, il suivait un noble à l'allure nonchalante. Un pouce dans le baudrier d'armes, un morceau de viande dans l'autre main, il hochait le menton en écoutant le rapport, sans guère sembler y prêter garde. Son apparence martiale, quoique peu soignée, plut aussitôt à Guilhem. Il mit en genou en terre, certain qu'il s'agissait là d'un homme d'importance. La tête baissée, il n'eut pas longtemps à attendre pour qu'on l'incitât à se relever.

Le chevalier savourait un petit éclat d'os à grand renfort de bruits de succion. Il avait un visage lourd, mal rasé et la silhouette témoignait de son goût de la bonne chère. Mais rien en lui ne trahissait de mollesse ou de laisser-aller. Ses cheveux, pour être peu ordonnés, étaient proprement coupés à l'écuelle et ses mains avaient été correctement frottées avant de passer à table.

«&nbsp;Alors, mon brave, Kaspar me dit que tu prétends être Génois&nbsp;?

--- Je suis tripolitain, à leur service. Mon maître est sire Mauro Spinola.&nbsp;»

Le chevalier hocha la tête, lança quelques ordres pour qu'on fouille soigneusement les deux captifs.

«&nbsp;Je veux aussi avoir assurance que ce n'est pas sergent en fuite, porteur de quelque signe d'infamie.&nbsp;»

Il pointa de son os le bracelet qui apparut au poignet de Guilhem.

«&nbsp;Tu portes là cercle de fer qui peut indiquer que tu as été mis en servage. Comment savoir si tu n'es pas un simple fuyard&nbsp;?

--- Je le suis, mais pas d'un maître chrétien. Vous n'avez qu'à me questionner et vous verrez que je suis fidèle disciple de la sainte Église.&nbsp;»

Le chevalier le fixa un petit moment, apprécia longuement les affaires étalées devant lui, qui ne lui arrachèrent guère plus qu'un sourcillement désabusé.

«&nbsp;Je n'ai ni l'envie ni les moyens de nourrir des bouches inutiles ici, tu iras plaider ta cause en la Cité. Mon sire le connétable saura bien apprendre de toi ce dont il pourrait avoir nécessité.

--- Je ne suis mie espie.

--- De cela je n'ai cure. J'ai bien assez à faire ici déjà. Si tu es tel que tu le dis, tu seras plus proche de tes maîtres et pourra donner quelqu'utile renseignement. Et dans le cas inverse, que Dieu ait ton âme&nbsp;!&nbsp;»

Puis, après avoir donné des instructions pour qu'on ligote solidement Guilhem et Toufik, il repartit d'un pas vif. Lorsque l'archer le fit se relever, Guilhem lui demanda&nbsp;:

«&nbsp;Ne pourrait-on au moins avoir quelque croûton&nbsp;? La nuit est bien longue à celui qui s'encouche le ventre creux.

--- J'irais voir si les pères n'ont pas de quoi te faire la charité. Ils ne sont plus tant riches depuis le pillage de la cité, mais s'ils voient en toi un de leur frère de religion, qui sait&nbsp;?&nbsp;»

### Jérusalem, quartier Saint-Jean, midi du mercredi 4 septembre 1157

Le trajet depuis Baniyas s'était fait assez tranquillement, les hommes de l'escorte ayant jugé qu'il était plus agréable de flâner parmi les chemins du royaume que de tenir une place forte à la frontière. Il faisaient de suffisantes pauses régulièrement et, peu à peu, Guilhem sympathisa avec eux. S'ils continuaient à le surveiller et le maintenaient ligoté, ils n'étaient pas mauvais compagnons et acceptèrent même de lancer les dés avec lui, misant des cailloux pour passer le temps.

Avant leur départ, ils lui avaient pris toutes ses affaires, y compris ses hardes, pour les examiner en détail. On savait les espions habiles à dissimuler des messages en des endroits surprenants. Lors de cette fouille intégrale, ils avaient dû quitter tous leurs vêtements et, Guilhem avait découvert avec stupeur que Toufik n'était pas un garçon comme il le pensait, mais une jeune fille à la féminité naissante. Elle avait supporté la gêne et les regards égrillards ou moqueurs sans desserrer les dents, mais semblait encore plus repliée sur elle-même depuis ce moment. Elle avait dès lors refusé tout échange, se contentant de suivre Guilhem comme un petit chiot. Tous deux arboraient désormais des tenues latines, mais toujours pas de chaussures.

Une fois parvenus au palais à Jérusalem, ils avaient longuement attendu dans une étroite resserre emplie de meubles pourris. Sans repère, Guilhem compta trois repas, sans savoir si cela faisait un jour ou deux. Un des gardes qui vint les chercher lui expliqua qu'ils allaient chez les hospitaliers de Saint-Jean. Nul ne voulait prendre en charge cet espion possible dans l'hôtel du roi, pratiquement désert, avec Baudoin et son armée loin au Nord. Mais comme les frères de Saint-Jean avaient eu des droits sur la ville, peut-être pourraient-ils s'occuper de vérifier son histoire.

On les mena cette fois dans une petite salle qui avait longtemps servi de poulailler. De la paille moisie était entassée dans un coin et les déjections des volailles empuantissaient l'endroit. Mais du jour passait par-dessous la porte, ainsi que par le fenestron qui donnait sur la cour, ce qui rendait le lieu moins sordide. Guilhem retrouva le son familier des cloches et les bruissements d'une grande maison. Ils restèrent à patienter encore deux jours, correctement nourris cette fois. Les hommes qui venaient leur porter le brouet, le pain et l'eau n'étaient plus des soldats, mais de simples domestiques. Guilhem commençait à se dire qu'il serait possible de s'évader si les choses perduraient. Il demeurait la question du bracelet de fer qui le désignait à chaque rencontre comme un fugitif.

Il cherchait une façon d'en détacher le rivet lorsque des pas s'approchèrent de la porte. La barre en fut tirée et un grand gaillard, mince et les épaules larges vint se planter devant les captifs. Son visage maigre paraissait n'être que d'os et dans ses yeux brillait un regard fiévreux sous d'épais sourcils. À sa poitrine se voyait la croix blanche du manteau des frères profès de Saint-Jean. Il ne laissa pas à Guilhem le temps de saluer.

«&nbsp;Si vous êtes bien celui que vous prétendez, j'ai quelques questions pour vous&nbsp;!

--- N'avez-vous pas fouillé mes affaires&nbsp;? Je suis bon chrétien, Guilhem, de Gibelet. Si vous pouvez vous enquérir auprès de mon sire Mauro Spinola, il vous confirmera cela.&nbsp;»

L'hospitalier lui adressa un amical sourire.

«&nbsp;Moi je suis frère Raymond. Votre sire m'a justement chargé de verser rançon pour vous sortir des geôles sarrazines.&nbsp;»❧

### Notes

«&nbsp;Forain&nbsp;»&nbsp;: du latin *foranus*, étranger. La signification est demeurée en français contemporain, avec le terme de forain&nbsp;: ce qui n'est pas familier, ce qui est étrange, à rapprocher du mot *foreign* qui, en anglais désigne l'étranger, dans un semblable espace lexical.

La notion d'identité était bien différente de celle que nous pouvons éprouver, avec nos documents officiels et les listes administratives de recensement. Le Moyen Âge se basait avant tout sur une toile de confiance qui permettait de faire certifier par des témoins que l'on était bien celui que l'on prétendait. Il était extrêmement aventureux de se rendre en des lieux où personne ne pouvait se porter garant de vous, car cela signifiait aussi que vous ne pouviez garantir la sincérité de vos rencontres. Appartenir à un cercle relationnel même lâche ou distant offrait une sécurité à chacun des interlocuteurs. C'est pour cela que les négociants, y compris parmi les grands voyageurs, opéraient le plus souvent dans un espace social bien circonscrit, parfois étendu géographiquement, mais sans trop aller au-delà.

### Références

Broadhurst Roland, *The Travels of Ibn Jubayr*, London&nbsp;: Cape, 1952.

Greif Avner, «&nbsp;Reputation and Coalitions in Medieval trade: Evidence on the Maghribi Traders&nbsp;», dans *The Journal of Economic History*, vol. 49, No. 4 (Déc. 1989), p. 857-882.

Ellenblum Ronnie, «&nbsp;Who Built Qalʿat al-Ṣubayba?&nbsp;», dans *Dumbarton Oaks Papers*, vol. 43 (1989), p. 103-112.

Les trois frères
----------------

### Misr-Fustat, demeure de Ashraf ben Panoub, fin d'après-midi du mercredi 24 octobre 1156

Confortablement installé sur un matelas épais, Henri le Buffle sirotait un jus d'agrumes en laissant son regard errer par les ouvertures du claustra surplombant la rue. Malgré la chaleur, une intense activité s'y discernait et un brouhaha agité en montait à travers les auvents de toiles usées et de feuilles de palmier racornies. Son hôte, Ashraf ben Panoub, demeurait non loin d'un caravansérail où les marchands arrivés du sud venaient présenter les merveilles acheminées depuis les contrées de l'ouest. Le vacarme des chameaux en train de blatérer, les cris des escortes et les exclamations des boutiquiers inquiets du passage des lourds convois animaient le quartier tout au long de la journée.

Ashraf assistait les négociants méridionaux, parfois originaires de fort loin, soit de Nubie, soit d'au-delà de la Mer Rouge. Il les mettait en contact avec des marchands méditerranéens, des caravanes partant vers le Maghreb ou remontant vers Byzance. Il lui arrivait aussi de prendre des participations lorsqu'il se sentait en fonds. Néanmoins, il s'agissait là de commerce à longue distance dont il n'avait guère les moyens. Il fallait avoir une assise bien plus large et établie que la sienne pour se permettre de voir ses investissements errer durant des mois par des chemins et des voies maritimes risquées avant d'en espérer le moindre bénéfice. Sans même parler des malheurs, catastrophes naturelles et brigandages possibles.

Il hébergeait fréquemment dans sa vieille demeure ses différents partenaires, aimant à accueillir à sa table une grande variété de voyageurs. Il était avide d'anecdotes pittoresques et n'hésitait pas à se faire réciter plusieurs fois les récits les plus incongrus, jusqu'à les coucher ensuite lui-même sur le papier. Il conservait d'ailleurs tout cela en pêle-mêle dans un renfoncement de son majlis[^103], annonçant à chaque invité qu'il avait l'intention de compiler tout cela en un bel et définitif ouvrage sur les contes lointains. Au fil des ans, Henri avait vu le bazar de feuillets croître sans que jamais un semblant d'ordre n'y fût initié.

Henri bénéficiait d'une pièce au deuxième étage, où il avait ses repères. Un des escaliers permettait de rejoindre, par une petite cour, une venelle discrète d'où il savait se perdre dans le dédale des rues anciennes. Habitué à se vêtir comme les Levantins, il parlait parfaitement l'arabe de ces régions et avait appris à aimer déambuler dans les quartiers animés de la capitale économique du pays. Il y avait ses usages, achetant brochettes de viande et pâtisseries au miel aux mêmes boutiquiers depuis des années. Il se rendait aussi à l'église proche chaque fois qu'il demeurait là, adoptant les cérémonies coptes locales et laissant entendre qu'il était de rite maronite sans jamais le confirmer formellement, la terre d'Égypte étant traditionnellement tolérante des croyances personnelles. Il était plus important d'être connu et reconnu de bonne réputation, faisant de pieux dons à sa congrégation, que de débattre de ses convictions et pratiques religieuses.

Quelques coups rapides à la porte le firent sursauter alors qu'il glissait dans une léthargie annonçant le sommeil. Il alla pousser le verrou et découvrit avec plaisir que son jeune valet nubien, Balô, avait réussi à trouver Ibrahim al-Salar. Celui-ci s'avança, un sourire large barrant son épaisse barbe brune, démenti par l'éclat froid de ses yeux bouffis. Ibrahim faisait l'essentiel de ses affaires entre Mer Rouge et Méditerranée, allant et venant à travers tout le pays. Indifférent au pouvoir central, il n'y voyait qu'une gêne fiscale à minorer par tous les moyens. Il pourvoyait donc Henri de nombreux et détaillés rapports contre des avantages financiers âprement négociés. Sa faconde et ses manières amicales le rendaient malgré tout sympathique.

Il salua avec chaleur et respect, conscient de naviguer en eau trouble et habile à ménager chèvre et chou à chaque moment. Après de rapides échanges formels de politesse, ils allèrent s'installer sur les matelas formant majlis dans la niche qui surplombait la rue. Henri fit apporter quelques rafraîchissements agrémentés de fruits secs et d'olives épicées, puis envoya Balô effectuer quelques courses pour la soirée. Il préférait avoir le moins de témoins possible de ses discussions, sachant parfaitement, pour les acheter, qu'il n'était nul domestique à l'intégrité parfaite. Observant son petit manège, al-Salar adoptait un air narquois, semblant s'amuser de cette situation ambiguë. Henri le suspectait de vendre ses services à bien d'autres, se garantissant des représailles éventuelles par le marchandage indiscriminé de ses informations.

«&nbsp;J'ai quelques joyaux pour vous, mon ami. De quoi tailler bijou à turban.&nbsp;»

Il avala quelques pistaches, ménageant ses effets. Henri prit une gorgée de sa boisson, sans trop marquer son intérêt.

«&nbsp;L'étoile de Nasir al-Dawla Yaqut tente de s'approcher du firmament, au risque de sombrer dans les ténèbres. Il semble trouver Qûs[^104] trop petit pour ses ambitions, désormais. Sa victoire de Dumyât semble lui avoir donné des envies d'un plus grand pouvoir.

--- Il semble avoir donné tous les gages de fidélité au vizir Ruzzîk, pourtant. Comment s'est-il dévoilé à toi&nbsp;?

--- Oh, il demeure encore tapi, mais fourbit ses armes et apprécie l'étendue de son bras. De nombreuses allées et venues ont été signalées entre le palais, sa citadelle et les appartements de Sitt al-Qusur. Je ne suis pas convaincu que celle-ci va chanter les louanges du vizir, dont chacun sait qu'elle trouve l'indépendance un peu trop marquée par rapport à son calife de neveu.&nbsp;»

Henri prit un peu de temps pour estimer les répercussions de ces nouvelles. Cela recoupait des rumeurs et informations entendues au Caire. Il savait que des dissensions avaient grandi entre le vizir et la famille régnante, après leur enthousiasme initial. Beaucoup, dont la tante du calife que venait d'évoquer al-Salar, s'inquiétaient des convictions religieuses de ce dernier. Il était arménien d'origine, converti au chiisme, mais avec des préférences marquées pour la branche duodécimaine, dont ils n'étaient pas. Les subtilités confessionnelles échappaient complètement à Henri qui n'en percevait, comme pour sa propre croyance, que les implications politiques.

Le vizir Tala'i ben Ruzzîk avait restauré l'autorité vacillante du calife dans tout le pays en quelques années. Au début, Henri avait pensé que c'était une bonne nouvelle pour le royaume de Jérusalem. Un pouvoir égyptien chiite indépendant de la Syrie sunnite permettait d'espérer des désaccords sur lesquels bâtir pour empêcher toute alliance durable. Mais avec le temps, il commençait à s'inquiéter de l'efficacité du vizir. Il avait un bel entregent au niveau du palais et une certaine estime populaire, mais devait composer avec une hiérarchie militaire assez turbulente. Afin de les canaliser, il ne fallait pas qu'il fasse de l'Égypte un adversaire aussi redoutable que la Syrie de Nūr ad-Dīn.

«&nbsp;Quelles sont les chances du gouverneur de Qûs, selon toi&nbsp;?

--- Il possède une belle assise là-bas, contrôlant les richesses venues d'Orient et y faisant sa picorée, mais al-Qahira[^105] maîtrise l'armée. Et de nos jours, al-Qahira, c'est le vizir. Al-Qusur n'est qu'une femme, elle a besoin d'un champion et pense user de Yaqut comme elle l'a fait de Ruzzîk. Mais ce dernier a fait sa pelote, il ne sera pas aussi aisé à vaincre qu'Abbas et les siens. Il est aimé assez et a toujours bien pris garde à se revendiquer de l'appui de la famille d'al-Fa'iz. Les plans de la tante du calife me semblent trop tortueux pour aboutir. À croire qu'elle aimerait devenir vizir elle-même&nbsp;!&nbsp;» conclut-il, amusé.

Henri accorda un sourire connivent à son invité, sans trop se dévoiler toutefois. Une telle situation était dangereuse pour le califat et il ne doutait pas un instant que les espions de Nūr ad-Dīn seraient informés aussi vite que lui. Bien que, ces derniers temps, la plupart des affrontements aient eu lieu au nord, surtout au niveau de la principauté d'Antioche, les campagnes de l'été prochain allaient se décider autour de l'équilibre des forces en Égypte. De nombreux émirs poussaient le vizir à la guerre, estimant qu'il était humiliant de verser tribut aux Latins. Beaucoup gardaient un souvenir amer de la perte d'Ascalon aux mains de Baudoin et souhaitaient une revanche. Quelques mois plus tôt, ils n'avaient d'ailleurs pas hésité à lancer une opération maritime sans son consentement.

À l'origine, Henri espérait demeurer dans Fustat pendant la clôture des mers et ne remonter qu'aux beaux jours vers le nord, garni de toutes ses informations. Il lui semblait désormais nécessaire de trouver rapidement un contact susceptible de transporter une lettre chiffrée à destination de l'hôtel du roi. Même s'il n'y avait aucun élément définitif à y inclure, il était important que chacun là-bas sache que la situation en Égypte était mouvante, et que l'accord de paix était donc bien fragile. Il faudrait aussi en informer son propre frère Guy, à charge pour lui de se garantir que son seigneur le comte de Jaffa soit bien au fait des aléas de la politique de son voisin.

### Naplouse, palais seigneurial, veillée du mardi 12 août 1158

Après une journée brûlante de soleil, la petite cour du seigneur de Naplouse, Philippe de Milly, s'assoupissait tranquillement en assistant à une représentation de théâtre d'ombres dans la grande salle où ils avaient soupé. Si les artistes en étaient de qualité, leur répertoire n'était pas du tout aux goûts de l'assemblée. Ils narraient les aventures d'un voyageur marchand, larron à l'occasion, lors de ses périples en mer. Le maître des lieux, n'y tenant plus, finit par se lever et demander qu'on leur offre un spectacle seyant à chevaliers et pas négociants de souks. Le temps que les bateleurs s'organisent, quelques musiciens vinrent égayer l'atmosphère devenue pesante. Dans l'attente, Philippe se fit servir un peu de vin aux fruits, allongé d'eau, puis entreprit de profiter de l'intermède pour discuter un peu avec Baudoin et Balian d'Ibelin, ses invités du jour. Ils revenaient d'Acre, en mission pour le comte de Jaffa. Le plus jeune des deux, Balian, était à peine majeur, mais il avait la morgue et le caractère bien trempé du vieux Barisan, comme toute sa fratrie. Il approuvait d'un air hargneux toutes les affirmations de son frère Baudoin, aussi impétueux que vigoureux.

«&nbsp;Vous ne m'en avez guère dit sur vos terres. Qu'en est-il de la libération de votre aîné&nbsp;?

--- Nous collectons pour la rançon, et le sire roi nous a garanti qu'il ne faillirait pas à notre débours. Il ne cesse de louer la vaillance de notre maison, qui l'a sorti du piège de Meleha.

--- J'enrage de n'avoir point été aux côtés du roi en cette occasion. Ce Norredin a une audace et une chance insolentes&nbsp;!

--- Les marches regorgent d'espies et de nomades à son service&nbsp;! Père nous a toujours appris à leur tenir la bride serrée. Sans quoi ils s'empressent de nous trahir pour le soudan[^106].&nbsp;»

La mention du vieux patriarche, décédé quelques années plus tôt, ramena Philippe plusieurs décennies en arrière, quand il n'était lui-même qu'un jeune bachelier comme ces deux cadets face à lui. Néanmoins, à la différence d'eux, il avait l'espoir de finir à la tête d'une riche seigneurie au centre du royaume. Barisan d'Ibelin était un fin stratège, autoritaire et péremptoire, se souvenait Philippe. Pourtant, nombreux étaient ceux qui auraient vendu père et mère pour se ranger dans son échelle durant les batailles. Il était féroce dans l'affrontement et audacieux dans ses tactiques. Il y avait gagné ses terres, à la frontière du territoire d'Ascalon, alors aux mains des Fātimides.

«&nbsp;Il est triste que votre père n'ait manqué que de peu la prise d'Ascalon. Il avait bien mérité d'y voir flotter la bannière royale.

--- De certes, il n'aurait souffert de voir la milice du Temple s'y engouffrer les premiers&nbsp;! confirma Baudoin.

--- J'encrois qu'il aurait flairé le danger de s'y laisser prendre dans la nasse et n'aurait pas poingné à la franche marguerite comme eux. Ton père était aussi avisé qu'adroit à briser les lances.&nbsp;»

De mauvais gré, Baudoin acquiesça en silence à cette douche froide de son exaltation guerrière. Soucieux de ne pas voir cette rebuffade montée en épingle, il lança un regard sévère à Balian. Ce dernier plongea le nez dans son gobelet de verre et en but le nectar sans mot dire. Indifférent aux émois des jeunes chevaliers à ses côtés, Philippe continuait son voyage mémoriel. Lui-même gardait un souvenir assez vif de son père, homme au caractère difficile, quoique tendre avec son épouse. Il avait enseigné à ses fils l'importance du devoir et de la parole donnée, essentiellement par l'exemple qu'il leur présentait chaque jour. Philippe conservait généralement autour du cou une petite croix reliquaire qu'il avait trouvé dans ses affaires après sa mort.

La fin d'une pièce musicale le tira de sa torpeur et il adressa un sourire poli aux deux jeunes bacheliers demeurés muets pendant son introspection. Ils étaient familiers du comportement parfois étrange du seigneur de Naplouse, pour l'avoir côtoyé depuis leur enfance.

«&nbsp;De votre côté, nulle agitation parmi les nomades et les mahométans&nbsp;? Depuis Carême, le vizir du Caire ne cesse de lancer chevauchées en nos terres du sud. Avez-vous idée de ce qui se trame&nbsp;?

--- Comme vous le dites, ce sont chevauchées de pillards et non ost de conquête. Ils évitent soigneusement les places fortes, frappant là où ils espèrent la résistance nulle.

--- Ils ont tout de même tenté de forcer Vaux-Moïse[^107], et envoyé espies jusqu'à Montréal. Sans même parler de leur percée jusqu'à Bethgibelin.

--- Quelques marcheurs de Dieu se sont mis en tête de leur rendre l'honneur et pensent poindre depuis Gaza.

--- Vous les allez joindre&nbsp;?

--- En l'absence de Hugues, je ne peux décider en dehors du Conseil des barons du sire Amaury. Ce ne serait que de moi, j'y porterais ma bannière avec plaisir. Ils ont trop l'habitude qu'on les laisse tranquilles lors des campagnes estivales.&nbsp;»

Surpris par la pertinence de la remarque, Philippe de Milly hocha le menton. Les affrontements avec les principautés d'Alep, Hama, Hims ou Damas étaient usuels, les protagonistes évoluant sur les territoires des uns et des autres en continuelles fluctuations. Il était par contre très rare d'aller au-delà des sables de Gaza au sud. Les Égyptiens profitaient de leur suprématie maritime et des contreforts du désert pour préserver leurs propres provinces, tout en portant le fer et le feu chez leur adversaire latin.

«&nbsp;Sans l'appui du roi ou du comte de Flandre, cela ne peut aller bien loin. Il faudrait longuement préparer pareil assaut si on veut n'y point faillir. Il a fallu des décennies pour prendre Ascalon et le désert ne la protégeait pas.&nbsp;».

Découvrant alors que la musique s'était tue et que l'on attendait un geste de sa part pour reprendre le spectacle de théâtre d'ombres, il sourit aux présents puis, d'un mouvement de tête, invita les artistes à commencer. Il lui fallait repenser tranquillement à cette idée d'aller porter la guerre en terre d'Égypte. La première chose à faire était de se garantir que jamais il ne pourrait en provenir d'assauts concertés avec les forces syriennes. En aucun cas les troupes royales n'auraient les moyens de combattre sur deux fronts. Il étendit ses longues jambes, sirota un peu de son vin et suivit sans vraiment les voir les personnages de cuir dont les formes ondulaient sur la toile de lin. Au moins était-il question de hauts faits d'armes cette fois.

### Ascalon, château comtal, matin du mardi 30 septembre 1158

Comme chaque matin depuis son récent mariage, Amaury badinait en prenant son premier repas de la journée. Vêtu d'une simple chemise, il n'était pas encore toiletté, rasé, ni coiffé. Tout en profitant des premières lueurs, il aimait à se faire lire des poètes, sollicitant son chapelain dont il avait exigé qu'il ait une voix douce et chantante. En outre, son humeur, déjà habituellement avenante dès le réveil, était outrancièrement légère depuis qu'il avait convolé en noces. Il n'avait que chansons guillerettes, citations rimées et bons mots aux lèvres, le sourire ne les quittant que peu.

Les jours les plus torrides de l'année étant passés, il s'installait devant les vastes fenêtres qui donnaient à l'est, vers la plaine, cherchant à discerner dans le lointain les fortifications qui s'étalaient jusqu'aux reliefs judéens enserrant la Rivière du Diable[^108]. Comme chaque matin, à peine vêtu, il avait assisté à la messe et se sentait l'âme en paix. Le gros de la saison des batailles était derrière eux, sans trop de heurts pour le royaume, et la cour se préparait à un important voyage au Nord afin de rencontrer le basileus byzantin. Amaury se demandait si ce souverain serait conforme à toutes les histoires qu'on lui narrait.

Selon lui, les Romains, comme ils aimaient à se nommer, formaient le plus étrange des peuples, avec des coutumes dont il n'arrivait à comprendre la logique. La pompe dont on entourait le dirigeant lui paraissait tout particulièrement absurde. Même s'il avait un grand respect envers son frère Baudoin, il ne le considérait que comme le premier d'entre les barons. Le roi des Romains, Manuel, semblait plus sacré que le Pape pour les siens et le protocole autour de sa personne encore confus et insensé.

Entendant des pas s'approcher, il se retourna pour voir arriver Guy le François, son sénéchal. C'était un fidèle d'entre les fidèles, que sa mère lui avait conseillé. Homme discret et d'un caractère avenant, Amaury le considérait, ainsi que ses frères Philippe et Henri qu'il côtoyait depuis son enfance, plus comme un oncle que comme un vassal. Pourtant, même si Amaury se montrait parfois assez familier, jamais Guy ne s'était permis la moindre indélicatesse. En outre, il était d'une grande rigueur dans la tenue du domaine. Il supervisait toute l'administration territoriale, finances et forteresses, sans que les plaintes d'abus n'en viennent trop fort aux oreilles du comte, ce qui était à son honneur. Il avait malgré tout la désagréable habitude de ne jamais laisser un point litigieux en suspens, poursuivant son seigneur de ses questions jusqu'à obtenir une réponse.

Amaury fit une moue en constatant qu'il y allait avoir une nouvelle fois assaut&nbsp;: Guy apportait avec lui les cartons et esquisses qu'il avait fait exécuter par quelques maîtres d'œuvre. Depuis le début de l'année, il semblait pris de frénésie de construction. Quand ce n'étaient pas des murailles à exhausser, c'étaient des tours à hourdir ou des fossés à curer.

Le sénéchal inclina son fin buste avec respect, un sourire amical sur le visage. Sa silhouette agitée, habillée comme toujours à la dernière mode, trahissait aussi sa bonne humeur. Il savait que le comte n'était guère enclin à parler boutique et avait donc choisi de le traquer en sa tanière avant qu'il ne s'échappe, en quête d'un lion ou d'une gazelle à chasser.

«&nbsp;le soudan est-il à nos portes que tu m'assailles dès potron-jacquet[^109]&nbsp;?

--- Je m'emploie à ce que jamais il ne puisse ainsi nous surprendre, sire comte&nbsp;» répliqua, amusé, Guy.

D'un geste, Amaury l'invita à sa table et lui fit porter oublies, compote de fruits et fromage frais. Lui-même ne mangeait que peu, malgré sa silhouette grasseyante, mais il tenait à faire bon accueil en son hôtel. Il ne supportait pas les maisons où l'on pleurait le pain aux voyageurs ou à la domesticité.

«&nbsp;J'avais espoir que vous pourriez examiner ces propositions. J'ai trouvé un fort habile arpenteur ermin[^110] qui se propose de creuser citerne grande assez pour augmenter les réserves d'eau de la cité.

--- Encore un bassin&nbsp;? N'en avons-nous assez&nbsp;?

--- C'est aux fins de compléter les terres gastes[^111] au long du chemin outre la porte Jérusalem.

--- Je ne suis mie aise à l'idée de fournir ainsi de quoi abreuver ceux qui attaqueront mes murailles.

--- Il n'est question que cela ne puisse aller outre quelques semaines, et certes pas un mois plein, pour forte troupe.&nbsp;»

Amaury déchira un petit coin de crêpe qu'il se mit à grignoter, tournant la tête vers la plaine, les yeux dans le vague. Pour autant, Guy ne désarma pas.

«&nbsp;Pour le moment, nous ne pouvons accueillir forte troupe plus d'un jour ou deux...

--- Au rebours de toi, je ne suis pas convaincu que nous pourrons faire d'Ascalon la Séphorie méridionale. Il me semble que Gaza serait bien plus indiqué, au plus près des sables.

--- Il ne serait guère aisé de faire admettre royale décision aux hommes de la milice du Temple. D'autant que la capture de leur maître Bertrand de Blanquefort les pousse à ne regarder que vers Damas et Alep.&nbsp;»

Amaury grommela à l'évocation des Templiers. Comme tous les barons de Jérusalem, il en admirait la vaillance autant qu'il en déplorait l'esprit d'indépendance. Bertrand de Blanquefort avait récemment obtenu du pape d'être appelé *Maître par la grâce de Dieu* et il exhibait un bâton, symbole de son statut, qui n'était pas sans ressembler à un sceptre. Suffisamment en tout cas pour que le roi et ses proches s'en émeuvent auprès du Saint-Père, sans résultat.

Le jeune comte de Jaffa tendit la main vers les documents du sénéchal.

«&nbsp;Voyons ce qu'il en est de ces cartons...

--- J'ai aussi une estime du coût. Avec les esclaves que nous pouvons faire travailler à l'épierrage et les moellons que nous possédons déjà, cela ne serait que de peu de prix pour le trésor comtal.

--- Je te laisse apprécier ces choses. Il ne m'est pas tant important de compter pécunes, tant qu'il m'en reste assez pour mon hostel et mes domaines.&nbsp;»

Amaury examina le tracé des citernes qui devaient prendre place sur des terrains abandonnés au pâturage lâche. Non loin de là, accolée à une maison forte, une grange était envisagée pour y entasser viandes et fournitures. La plupart des soldats se débrouillaient par eux-mêmes pour leur pitance, mais il était habituel d'approvisionner au moins l'hôtel royal. Sans compter que le fourrage devait être en quantité pour nourrir toutes les montures en plus de l'avoine. Le peu d'herbe qui n'avait pas grillé en été ne suffisait guère et il fallait donc prévoir des réserves de foin quand les pluies de printemps avaient verdi les campagnes.

«&nbsp;Tout ça pour aller se perdre au parmi des sables d'Égypte...&nbsp;» soupira Amaury. ❧

### Notes

La structure de ce Qit'a est née de mon désir décrire un texte qui illustre au mieux le projet que servent ces textes. Cette mosaïque de destins propose au regard une vision multipolaire de l'Histoire et tente d'en évoquer la part insaisissable, tout en possédant un arc narratif lâche qui indique le mouvement de l'Histoire. La décennie qui va commencer au sein d'Hexagora verra les campagnes en Égypte et je souhaitais montrer comment des événements de grande importance pouvaient avoir leur origine dans un enchaînement bien plus riche et complexe qu'une décision autocratique d'un dirigeant dont le devenir serait aussi exceptionnel que le caractère. Je postule que l'histoire est plus souvent qu'on ne le dit l'œuvre de multiples et désordonnées influences dont on retrace une cohérence après coup, en un arrangement vers un possible comme dénoncé par Bergson.

Par jeu, car il m'est toujours nécessaire de ne pas prendre tout cela pour une entreprise trop sérieuse, il m'a semblé évident pour narrer les agissements d'illustres inconnus, de nommer cela *Les trois frères*, en hommage à d'autres Inconnus et leur film éponyme. Les héros de ce Qit'a, qui occupèrent une place de premier plan dans la politique du royaume de Jérusalem dans ces décennies, présentaient toutes les caractéristiques pour que je puisse leur prêter ce rôle de messagers de la Fortune. À part pour Philippe de Milly, qui devint par la suite grand maître du Temple, nous n'en savons guère sur eux, ce qui laisse une belle marge de manœuvre pour l'imagination et la reconstruction a posteriori.

### Références

Barber Malcolm, «&nbsp;The career of Philip of Nablus in the kingdom of Jerusalem&nbsp;» dans Edbury Peter, Phillips Jonathan, *The Experience of Crusading*, Volume 2, Cambridge University Press&nbsp;: 2003, p. 60---78.

Cahen Claude, Ragib Yusuf, Tahir Mustafa Anwar, «&nbsp;L'achat et le waqf d'un grand domaine égyptien par le vizir fatimide Tala'i b. Ruzzik&nbsp;», dans *Annales Islamologiques*, volume 14, Institut Franaçais d'Archéologie Orientale, Le Caire&nbsp;: 1978, p.59-126.

Caritas
-------

### Calanson, halte caravanière, après-midi du mardi 13 mai 1158

L'impressionnante file de chameaux, mules, ânes et chevaux avait soulevé un nuage ocre gris qui faisait de toutes choses des statues. Les aboiements des chiens, les cris des hommes se perdaient dans les volutes de poussière qui partaient à l'assaut des maisons. L'agitation se concentrait autour des bassins où les bêtes pouvaient boire à tour de rôle. Se frayant un passage à force de coude, des muletiers puisaient de quoi abreuver leurs animaux, tenus à l'écart par le maître de la caravane principale.

Hashim, un garçon d'une dizaine d'années allait et venait, une vaste calebasse dans les mains, depuis la petite file distraitement surveillée par son père 'Unaynah al-'Abbas, des ânes, pour l'essentiel, qui portaient des sacs de pois et fèves. Ils avaient rejoint le convoi à Lydda et l'accompagnaient jusqu'à Césarée, où leur chargement était attendu. Ils bénéficiaient de l'escorte d'un fort groupe de marchands, de quelques pèlerins et voyageurs et même d'une demi-douzaine d'hommes en armes, habillés du noir manteau de Saint-Jean de Jérusalem.

Le garçon trottinait sans relâche tandis que 'Unaynah échangeait gaiement avec ses compagnons. Il avait sympathisé avec quelques muletiers, partageant avec certains d'entre eux le goût du vin et des plaisanteries grasses. Une nouvelle fois Hashim se retrouvait donc obligé de surveiller les bêtes et de donner des instructions à ses deux petits frères. Il avait l'habitude de cela, qu'il préférait à devoir demeurer aux abords de son père, qui avait souvent la main leste quand il lui semblait devoir faire montre d'autorité. Il n'était en général qu'indifférence envers ses enfants, sauf lorsqu'il craignait qu'on ne reconnaisse pas sa valeur. Frapper les membres de sa famille lui paraissait le moyen le plus pertinent pour valider auprès des autres la bonne opinion qu'il se forgeait pour lui-même à travers l'oisiveté et la boisson.

Lors des haltes, il se trouvait toujours quelques mains curieuses qui tentaient de délacer des liens, de trancher des courroies ou de percer les toiles dans l'espoir de récupérer des denrées. Les grosses balles, les amphores de grande taille et les tonneaux ne craignaient rien, mais les cargaisons de moindre importance, en sacs, constituaient la cible de prédilection des chapardeurs. Il fallait donc porter attention aux personnes qui approchaient des animaux. Tout en allant et venant depuis l'auge de pierre, Hashim s'assurait que nul ne venait trop près de son petit groupe.

Un instant il lui sembla qu'une ombre s'était infiltrée au milieu d'eux et il se retourna précipitamment, donnant sans le vouloir un coup de coude à un des ânes. Celui-ci répliqua d'un mouvement de tête agacé, le projetant contre son voisin, qui répondit à cela à l'aide d'un sabot postérieur, propulsant Hashim dans les graviers, cul par-dessus tête, dans une gerbe d'eau et un envol de calebasse.

L'animal ayant accompagné sa frappe d'un long braiment d'avertissement, tout le monde vit le garçon le nez dans la poussière. Il voulut se relever immédiatement, mais, sonné, il n'y parvint pas. En outre, une violente douleur à la jambe le fit lourdement retomber au sol à demi inconscient. Deux caravaniers vinrent à lui et l'aidèrent à reprendre ses esprits, lui proposant de l'eau. Un des membres de l'hôpital, sorti de la commanderie voisine pour échanger avec ses frères, s'approcha à son tour. Il était habillé comme un Latin, cotte et chausses, mais portait le manteau distinctif et une tonsure burinée de clerc. Son fin visage en lame de couteau, prolongé d'une barbe drue aussi noire qu'il était possible, reflétait une austérité démentie par sa silhouette empâtée. Il s'agenouilla près de l'enfant, parlant sans difficulté dans la langue des locaux.

«&nbsp;Demeure ainsi un temps, garçon. As-tu douleur en aucune partie de ton corps&nbsp;?

--- Mon genou me cause grand dol&nbsp;!&nbsp;»

D'un geste expert, le moine releva le vieux thawb qui faisait office de vêtement au gamin, en prenant bien garde de ne pas le remonter trop haut. Il connaissait la pudeur des Orientaux. La jambe était griffée et un gros hématome ornait le bas de la cuisse. Avec le gonflement, on ne voyait déjà plus la rotule. Le clerc grimaça.

«&nbsp;Il te faudrait patienter un temps au repos, alité de préférence. Si rien n'est cassé, tu seras guéri dans quelques jours.&nbsp;»

Hashim fit une moue, luttant contre la douleur lorsqu'une ombre vint leur cacher le soleil.

«&nbsp;Allez, debout, vaurien. Les bêtes ont encore soif&nbsp;!&nbsp;»

Le père de Hashim ne semblait nullement ému de la blessure de son fils et ne lui accorda pas plus d'un regard, tournant rapidement les talons pour aller se poster à l'ombre. L'hospitalier le rejoignit en quelques foulées.

«&nbsp;Pardonnez-moi, mestre. Peut-être n'avez vous pas vu que l'enfant est blessé, il vient de recevoir mauvais coup d'une des bêtes.

--- Et alors&nbsp;? J'y peux quoi&nbsp;? Il faut que les ânes boivent avant qu'on reparte&nbsp;!

--- Il lui faudrait quelques jours alité, le temps que sa jambe guérisse.

--- Peuh&nbsp;! On se remet mieux en forçant, c'est connu. Sans quoi on devient mou et plus bon à rien.

--- Laissez-moi au moins le faire voir à un de nos frères savant en simples et onguents.&nbsp;»

Leur altercation commençait à attirer les regards. 'Unaynah ne se sentait jamais en confiance dans ces cas-là, ce qui faisait monter sa voix dans les aigus et agitait son corps de tremblements.

«&nbsp;Je vous connais, les frères de Yahya[^112]&nbsp;! Vous ensorcelez les esprits et voulez me voler mon fils&nbsp;! Pas de ça avec moi&nbsp;! Laissez-le tranquille&nbsp;!&nbsp;»

L'hospitalier en fut muet de stupéfaction, ne sachant que répondre. Il laissa l'homme repartir tandis que le gamin s'esquivait en boitillant. Un autre frère, son doyen de plusieurs décennies s'approcha, les sourcils froncés.

«&nbsp;Certains n'aiment guère la charité qu'on leur propose...

--- Quel abruti que ce vaunéant&nbsp;! Il mériterait de se voir botter ainsi que son gamin&nbsp;!

--- Voyons, frère Hémeri, est-ce là toute votre charité&nbsp;? Le père la mérite tout autant que le fils et peut-être plus.

--- Le gamin risque de finir boiteux. Il ne lui aurait rien coûté de nous le laisser quelques jours&nbsp;!

--- Sa colère est peut-être à l'aune de son attachement, qu'en savons-nous&nbsp;? Nos portes sont ouvertes, mais il ne serait guère amiable d'obliger à y entrer...

--- Mouais, mon vieux aurait dit qu'on ne fait pas boire un âne qui n'a pas soif. Pour autant, un qui boit trop ne vaut guère.&nbsp;»

### Beith-Gibelin, église paroissiale, fin d'après-midi du mercredi 12 novembre 1158

Une pluie fine avait rabattu la foule amassée devant le château dans l'église paroissiale. Les voûtes y résonnaient des discussions animées, l'arrivée d'une belle caravane depuis Jérusalem étant toujours un événement. Dame Sédille avait accueilli avec sa faconde habituelle son beau-frère, récemment veuf de sa sœur, avec un bébé en très bas âge. Comme elle venait aussi d'accoucher et se savait suffisamment allaitante, elle avait proposé à Guillemot de lui apporter les enfants. Pour elle, quatre bouches en plus ne posaient aucun problème. Malgré sa petite taille, elle accomplissait toutes ses tâches ménagères avec l'énergie et l'obstination obtuse d'un sanglier, sans en avoir le caractère acariâtre.

Ils avaient suivi la foule dans le bâtiment, échangeant les nouvelles des derniers mois. La jeune femme s'ébaudissait des grands enfants qu'elle accueillait et se disait impatiente de les voir batifoler avec les siens dans les jardins. Comme ils ne savaient pas quand la caravane arriverait, elle avait laissé ses deux garçons accompagner leur père. Ils aimaient à se rendre au terrain qu'ils cultivaient, surtout quand c'était, comme aujourd'hui, avec la perspective de monter quelques murs.

«&nbsp;Peut-être finiront-ils maçons ou carrier, plaisanta Guillemot.

--- J'espère en tout cas que ce sera solides travaux, car ils n'y entendent guère aux choses délicates. Je suis bien aise de voir arriver Esdeline et cette pauvre petite Perronele. Je me sens parfois bien seule au milieu de tous ces hommes.&nbsp;»

Tout en parlant, elle berçait l'enfant qu'elle venait d'allaiter. N'ayant aucune nourrice qui puisse l'accompagner, Guillemot avait pris du lait de chèvre pour le voyage. Découvrant le biberon donné par des mains bien peu expertes, le bébé n'avait pas avalé grand-chose. Après deux journées difficiles, il était désormais enfin repu et dormait à poings fermés malgré le vacarme ambiant. L'hôpital de Saint-Jean avait fait venir un large groupe d'orphelins et déshérités divers, dont la garde serait confiée à des colons du casal. On attendait donc Frère Aimon, le commandeur local, qui indiquerait quels enfants étaient destinés à quelle maison. Dans l'intervalle, les familles d'accueil et les gamins échangeaient des regards circonspects, espérant anticiper qui ils rejoindraient parmi ces inconnus.

Frère Pons arriva accompagné du cellérier, Gilebin. Ils allaient souvent par paire, l'athlétique silhouette de frère Pons dominant la petite stature de son compagnon. Ce dernier compensait la différence de gabarit par une agitation perpétuelle des membres, des yeux et, surtout, de la langue. Il était réputé pour aimer discuter et le vide se faisait généralement devant lui lorsqu'il s'adonnait à une balade dans les environs. Pour être agréable d'abord, il n'en était pas pour autant capable de s'intéresser à autre chose que ses propres soucis et s'efforçait d'en partager le détail avec quiconque avait au moins une oreille en assez bon état.

Le commandeur appela au calme de sa voix habituée aux combats. Il n'était pas des frères issus du couvent, mais un fils de noble qui avait choisi d'embrasser la carrière ecclésiastique. Il se murmurait que c'était suite à un chagrin d'amour, mais personne n'en avait l'assurance.

«&nbsp;Frères et sœurs du casal, nous allons procéder à la répartition des enfançons. Avant cela, je tenais à vous rappeler quelques détails.&nbsp;»

Il invita Gilebin à le rejoindre devant l'autel, celui-ci enchaîna de sa voix grave.

«&nbsp;De prime, n'oubliez pas de passer au cellier après, nous vous donnerons du linge de lit pour chacun, un drap de laine, un coussin et un sac de coton. Chaque année, pour les enfants ayant à peu près dix ans révolus, nous fournirons à la Toussaint du linge de corps, robe et chemise.&nbsp;»

Il fit traîner un regard lourd de reproches.

«&nbsp;Il ne s'agit pas d'en dessaisir les enfants pour vendre leur toile au fripier comme certains ont tentation à le faire. Le drapier connaît ses toiles et saura si un gamin n'a pas sur le dos ce qui lui appartient.&nbsp;»

Le commandeur enchaîna de façon naturelle, d'une voix tout aussi autoritaire.

«&nbsp;Je vous fais également remembrance que ce sont enfantelets de Dieu et pas esclaves païens&nbsp;! Les abus seront punis. Qu'un garçon dont la taille approche celle d'un homme soit envoyé aux champs pour y apprendre le labeur des terres, soit, mais j'espère bien voir courir et s'amuser en notre casal les plus jeunes que nous voyons ici. Ce que vous faites au plus modeste de ces garcelets, vous le faites à Dieu&nbsp;!&nbsp;»

Sedille chuchota à Guillemot que certaines familles avaient cru qu'accueillir quelques enfants leur ferait de la main-d'œuvre à pas cher, mais l'Hôpital veillait généralement au grain. Un couple s'était vu infliger une forte amende dans un casal au nord, pour avoir profité d'une fratrie qui leur avait été confiée. Malgré cela, les abus étaient fréquents et les plus résolus des gamins placés s'enfuyaient, nourrissant la misère des gens de peu qui erraient par les routes.

Guillemot écoutait d'une oreille distraite les commentaires de sa belle-sœur. Effrayé par le destin réservé aux orphelins sans famille, il comptait bien trouver rapidement une nouvelle épouse, de façon à reprendre ses enfants et leur garantir un avenir. Le mari de Sédille était forgeron et s'était installé de belle façon. Basyle, Andri, Esdeline et Perronele s'épanouiraient certainement chez eux, surtout avec leurs cousins et cousines. Mais Guillemot craignait toujours un malheur. Il estimait l'endroit trop près de la frontière, ne s'était pas encore fait à l'idée que la prise d'Ascalon avait reporté celle-ci par-delà le désert. Les assauts égyptiens récents lui donnaient d'ailleurs raison, mais il n'avait guère le choix. En outre, il avait pu constater de ses yeux que l'Hôpital de Saint-Jean dépensait sans compter pour fortifier la zone et s'employait à renforcer leur propre établissement. Cela ne le rassurait qu'à moitié.

Frère Aimon achevait son sermon et le cellérier appelait désormais les enfants pour leur indiquer la famille où ils iraient. Certaines des femmes s'indignèrent lorsqu'elles apprirent que des fratries avaient été divisées, les plus benjamins étant demeurés à Jérusalem chez des nourrices familières de Saint-Jean. Le commandeur leur garantit qu'on les réunirait dès que les plus jeunes seraient sevrés. L'ordre estimait peu pratique de faire voyager des bébés dans leurs langes.

En voyant les regards inquiets, les gestes maladroits, les visages apeurés, Guillemot soupira. Il n'avait toujours pas osé demander audience au vicomte pour s'enquérir de tâches subalternes non militaires auxquelles il pourrait être affecté en priorité. Il n'avait pas souvent été envoyé au combat, mais cela lui était arrivé de temps à autre. Désormais, il craignait que ses enfants ne connaissent le sort des orphelins autour de lui.

### Château Emmaüs, salle capitulaire des frères de Saint-Jean de Jérusalem, après-midi du dimanche 7 juin 1159

Bien que l'été ne soit pas encore là, le soleil s'abattait avec force dans le modeste cloître de Château Emmaüs. Reculant devant la chaleur, Frère Chrétien avait invité ses visiteurs âgés, Lambequin le Breton et son épouse Alison, à venir s'asseoir dans la petite salle capitulaire. La lumière pénétrait par la porte ouverte, mais la pierre conservait de l'hiver fraîcheur et humidité. L'hospitalier leur avait indiqué les bancs et s'était installé sur un escabeau face à eux. Il avait l'habitude de les voir passer lors de leurs fréquents périples sur les traces du Christ et de ses disciples. Année après année, ils venaient, désormais en palanquin après avoir longtemps erré en mules.

Il connaissait le couple depuis son enfance. Riches artisans, ils avaient employé son père dès son adolescence dans leur établissement de travail des peaux. Lambequin avait également été le parrain de chacun des membres de la famille lors de la conversion de son commis au catholicisme. Et, pour finir, ils avaient permis à Chrétien de poursuivre de belles études quand ils avaient appris qu'il souhaitait devenir clerc. Ils le considéraient comme un des leurs, d'autant qu'aucun de leurs enfants ou petits-enfants ne partageait leur passion religieuse ni n'avait choisi la voie sacerdotale. Ils pèlerinaient presque chaque année, parfois pendant plusieurs semaines et ne regrettaient qu'une chose&nbsp;: de n'avoir pas pu aller à Rome plus tôt. Ils s'estimaient désormais trop âgés pour partir si loin sur les routes. Ils venaient consulter Daniel à propos du dernier voyage qu'ils souhaitaient faire, chacun de leur côté, quoiqu'unis en esprit.

Ils avaient l'intention de remettre à l'Hôpital tous leurs biens immobiliers restants, un bel hôtel à Jérusalem et une propriété rurale dans un casal des environs, avec tous les droits qui y étaient attachés. Ils ne savaient pas s'il était plus adéquat de vendre le tout et de faire don de la somme ou s'il valait mieux faire offrande des biens. En échange, ils demandaient à être tous deux reçus dans l'ordre afin de terminer leurs vies en prière.

Frère Chrétien les avait écoutés attentivement, sans mot dire, hochant la tête.

«&nbsp;C'est faire là grande charité à Saint-Jean que de vouloir faire si généreux don... Ne craignez-vous pas que Pierre ou Jacques n'y trouve occasion de querelle&nbsp;?

--- Chacun de mes enfants a eu sa suffisance. Il me semble que bien peu peuvent se targuer de partir avec autant de biens dans la vie. Le surplus ne ferait que les gâter. Je préfère en faire la part à Dieu, pour les nécessiteux et les malades.&nbsp;»

Lambequin postillonnait quand il parlait et sa surdité l'incitait à parler plus fort que nécessaire. Alison, tout aussi malentendante que lui, hochait la tête en remâchant les mots de son époux. Elle l'avait suffisamment pratiqué pour savoir ce qu'il disait quand bien même elle n'aurait plus eu de son du tout.

Chrétien avait beaucoup d'affection pour les deux vieillards et se trouvait gêné de les inviter à plus de modération dans leur générosité. Se dessaisir de tous les biens qu'il leur restait serait certainement fort apprécié, mais leur demande d'être reçus dans l'ordre en retour risquait de poser problème.

«&nbsp;La seule difficulté que je vois en tout cela, c'est votre désir de rejoindre notre ordre. Non pas que vous n'en soyez pas dignes, loin de là. Mais parce qu'il est fort rare que cela se fasse en ses vieux jours. Nous sommes tournés vers le siècle assez, ce qui demande régulier et important labeur.

--- N'y a-t-il pas frères et sœurs d'oraison en vostre maison&nbsp;?

--- Nous avons les chapelains, prêtres ordonnés, mais ils s'adonnent aux offices des frères et parfois de nos paroisses. Il s'agit de servir bien différemment des frères noirs ou blancs...&nbsp;»

Le vieux Lambequin se tut un moment, plus ennuyé que contrarié. Il avait attendu de cette entrevue qu'elle lui permettrait d'aborder les derniers détails pratiques qui lui ouvriraient les portes d'un couvent où finir ses jours. Son désappointement se mesurait à l'aune de son silence. Frère Chrétien lisait leur détresse à l'idée de se voir refuser l'avenir céleste qu'ils espéraient, par une vie achevée en perpétuelles oraisons.

«&nbsp;Si j'étais vous, je conserverais une part de mes biens pour me doter en vue de joindre une communauté de Cluny, voire de Citeaux. Ainsi garnis, vous aurez bon accueil de leur part et pourrez vous cloîtrer ainsi que vous en avez désir.

--- Ont-ils quelque établissement en ces lieux&nbsp;? Nous ne sommes pas tant vaillants pour traverser les mers.

--- De certes, il s'en trouve de très bonne fame, à Jérusalem même et aux abords.

--- Mais ils ont moins grande pratique envers les pauvres et les nécessiteux. J'aurais préféré que cela soulage les maux des plus modestes, plutôt que d'engraisser un abbé.&nbsp;»

La remarque fit sourire Chrétien. Il était de bon ton de critiquer les chefs d'établissement religieux dont les appétits terrestres étaient quelquefois amplement nourris, au détriment, craignait-on, de leurs aspirations célestes. Selon les endroits, la discipline était un peu relâchée, mais il demeurait difficile de faire la part entre légende et réalité, le mur de la clôture protégeant les frères des regards du monde. Quoi qu'il en soit, l'hospitalier conservait une certaine confiance dans la rigueur des maisons bénédictines.

«&nbsp;Ne vous y trompez pas, beaucoup ont un hospice digne des nôtres. Il s'en trouve moins en ces lieux, voilà tout. Certains sont peu désireux de se voir obligés de frayer avec des hommes de guerre, ainsi que nous le faisons. Ils se contentent donc de s'ouvrir un peu au monde en des terres moins dangereuses.&nbsp;»

Le vieux Lambequin hocha la tête, à demi convaincu. Il n'était guère habitué à devoir remettre en question ses décisions, surtout pour un point aussi essentiel. Il sourit poliment à son filleul, lui tapotant la main en un geste familier.

«&nbsp;Aurais-tu quelque frère à me conseiller dans un de ces ordres&nbsp;? Il va me falloir pourpenser tout cela.&nbsp;»

### Calanson, cellier des frères de Saint-Jean de Jérusalem, matin du jeudi 25 juin 1159

Frère Guillaume, encore cellérier en titre pour quelques jours, claudiquait, sa canne à la main. Les violentes fièvres de l'hiver l'avaient bien diminué et il toussait régulièrement, s'essoufflant au moindre effort. Il avait été convenu de le transférer à Aqua Bella, en charge du cellier une nouvelle fois, mais en sachant qu'il n'y avait là-bas que fort peu de travail par rapport à Calanson. C'était un lieu de retraite, de repos pour les blessés et malades d'importance de l'ordre. Il avait accueilli cette promotion avec hésitation. Il reconnaissait et appréciait l'honneur qui lui était fait, lui qui n'était que modeste fils de paysan, mais il n'avait jamais envisagé de quitter Calanson et pensait y finir ses jours tranquillement.

Il avait fait de sa cellule la maison dont il avait toujours rêvé, y entreposant au fil des ans de menus objets, sans valeur pour la plupart, mais auxquels il était fort attaché. Il lui avait donc fallu opérer un tri cruel, persuadé qu'il ne pourrait guère en emporter avec lui, l'ordre prônant la pauvreté de ses membres. Il ne savait pas comment serait son supérieur, mais il doutait qu'il soit bien vu d'arriver avec malles et coffres ainsi qu'un baron en campagne.

Il avait passé les derniers jours à énumérer ses tâches à frère Hémeri, son successeur. Cette nomination était connue depuis longtemps et frère Guillaume avait été consulté à ce propos, mais cela demeurait néanmoins un déchirement pour lui d'abandonner ces bâtiments si familiers, qu'il parcourait une ultime fois en boitillant, espérant se remémorer tous les détails négligés en voyant le lieu de son labeur. Il désigna une vaste amphore, enterrée dans une petite réserve où ils conservaient de la céramique commune, des jarres de manutention et des vases de moindre valeur.

«&nbsp;J'ai usage de verser là les restes d'huile de l'année, pour en faire usage d'éclairage. S'il s'en vient quelque vilain des manses environnants qui en aurait usage, on peut y puiser quelques pintes. Il ne faut pas en user pour les lampes d'autel ou les logis d'importance, elle est trop rancie pour cela.&nbsp;»

À ses côtés, Hémeri suivait sans mot dire. Frère Guillaume se félicitait de ce choix, persuadé que le jeune homme était organisé et pointilleux. Il était malgré tout un peu étonné de le voir manifester si peu d'enthousiasme à recevoir un tel office. Beaucoup se seraient enorgueillis d'une pareille distinction et Guillaume préféra attribuer cette placidité à une modestie de bon aloi. Il lui aurait été insupportable de penser qu'il serait relayé par un clerc de peu de foi, de talent ou d'abnégation.

Ils passèrent devant la bergerie où quelques brebis et leurs agneaux étaient gardés pour garantir du lait.

«&nbsp;Je ne sais plus si je te l'ai déjà dit, mais j'ai usage d'accepter de la laine de la part des Bédouins qui ne peuvent donner ouailles en suffisance pour les droits de pacage. Nous n'avons pas tant d'herbe, j'aime autant des toisons pour nos linges.&nbsp;»

Il marqua une pause, levant un doigt tandis qu'il réfléchissait.

«&nbsp;À ce propos, il me faut t'établir la liste des manses à qui nous commandons le fil à tisser. Je t'y indiquerai le poids de laine donné à chacun.&nbsp;»

Il stoppa un instant, reprenant son souffle alors qu'ils arrivaient devant le cellier principal.

«&nbsp;Parlant de laines, ne laisse pas partir toutes tes couvertures à l'approche de l'hiver. Avec la clôture des mers, il s'en vient moins de voyageurs et le frère cellérier à Jérusalem te fera certement pressantes demandes. Mais à trop te dégarnir, tu ne sauras accueillir comme il se doit tous ceux qui débarqueront au printemps. J'ai usage de retrancher au moins le décime de ce que demande Jérusalem. Idem pour le foin. Il se trouve toujours plus de besoins que de demande, comme la paille, donc je vais parfois jusqu'à deux décimes, selon les années.&nbsp;»

Ils arrivèrent près des greniers, silos en céramique semi-enterrés, où ils stockaient les grains, semences de l'année à venir et fournitures pour le pain et les bouillies quotidiennes.

«&nbsp;Je ne sais plus si tu en as remembrance, mais l'an passé, j'ai dû rappeler à quelques ra'is[^113] de nos casals que les grains à glaner après foulage et battage doivent être abandonnés aux plus pauvres. Garde-les à l'œil, certains ont la cupidité rivée au corps. Si jamais tu as des soucis de bornage avec eux, n'hésite pas à aller consulter les shaykhs[^114], ils sont souvent moins avides et plus soucieux de leur communauté.&nbsp;»

Ils revinrent à la porte principale, plissant les yeux devant la forte lumière, annonciatrice d'une chaude journée. Les mains sur les hanches, Hémeri envisageait le territoire dont il aurait désormais la supervision économique. Il soupira, soucieux du travail que cela demanderait et inquiet à l'idée de ne pas être à la hauteur. Il ne s'était fait clerc que par dépit et choisi l'Hôpital pour ne pas se voir cloîtré. Il fréquentait d'ailleurs de temps à autre des établissements où il dissimulait sa tonsure sous un bonnet, un peu honteux de s'adonner à ses vices alors qu'il était censé montrer l'exemple. Il ne se sentait juste pas à sa place et aurait préféré devenir simple paysan, avec femmes et enfants. Mais sa famille était trop pauvre pour qu'il puisse avoir sa terre et il avait connu trop de mauvais maîtres pour se faire valet. Au moins bénéficiait-il d'une relative liberté tant qu'il donnait les bons signes de soumission.

Frère Guillaume montra un des bâtiments du casal accolé à leur maison forte.

«&nbsp;Il faudra penser à faire retailler la meule du moulin Saint-Jean. J'en ai fait déjà la demande par trois fois, sans que cela n'aboutisse. Quelques vilains grognent qu'elle donne plus de poussière que de farine. Et ils n'ont pas tout à fait tort.&nbsp;»

Il cherchait quelque chose à dire encore, souhaitant prolonger le moment le plus possible. Puis il baissa la tête, dénoua de sa ceinture le lourd anneau de fer où les clefs étaient amoncelées et le tendit à Hémeri.

«&nbsp;Tu as déjà été nommé par le chapitre et je n'ai gardé cela que par déférence pour mon labeur passé. À toi de porter l'annel du cellérier désormais. Puisse-t-il ne pas trop peser&nbsp;!»

### Aqua-Bella, cellule de père Daniel, veillée du mercredi 18 novembre 1159

Le froid descendu des monts de Judée avait gagné le cœur du vaisseau de pierre attaché à ses pentes. Des braseros avaient été disposés dans la chambre des malades et les frères avaient revêtu une seconde robe par-dessus la première. Malgré les frimas, le père Daniel conservait l'habitude de porter des sandales, même avec des chausses. Il ne s'aventurait de toute façon que fort peu au-dehors et ne risquait pas de se mouiller ou de se salir les pieds.

Il avait malgré tout installé une couverture sur ses genoux. Il ne faisait apporter aucun chauffage dans sa cellule, persuadé que la rigueur qu'il s'infligeait, bien que son corps y rechignât, serait de bonne influence sur ses frères. Pour le coup, cela incitait la plupart à décliner les séances de travail à ses côtés, préférant la douceur de l'infirmerie.

Frère Guillaume, le nouveau cellérier parcourait à mi-voix une tablette de fournitures demandées par le château de Belmont, dont ils dépendaient. Daniel l'avait convoqué, car il avait besoin d'un avis plus posé que le sien. Il trouvait chaque fois les réquisitions de frère Pons plus gourmandes que précédemment, sans que cela soit au service d'une cause qu'il appréciait. Son supérieur était en effet un soldat, un chevalier cadet de famille qui s'était engagé dans l'ordre pour y mener bataille, lance en main. Le père Daniel était un religieux plus soucieux de charité et, plus encore, d'oraisons. Il se méfiait de ses agacements épidermiques, dont il craignait que cela ne devienne préjudiciable à des relations, sinon cordiales, du moins apaisées.

Frère Guillaume reposa la tablette, écartant la lampe à huile qu'il avait approchée pour gagner en clarté.

«&nbsp;Il y a certes forte quantité de paille pour notre maigre domaine. Il ne s'agirait pas que cela aille croissant chaque année. Il nous faut garder de quoi faire les grabats de nos valets, garnir nos écuries et refaire paillasses des lits de temps à autre. Si nous devons par la suite débourser pour nos besoins, cela serait de nul intérêt.

--- J'en suis bien d'accord. C'est là demande pour forte cavalerie, plus qu'il n'en a pour le moment. Adoncques, je ne serais guère surpris qu'il escompte l'augmenter.

--- Il demeure qu'il assure nombreuses patrouilles depuis la route maritime jusqu'à la Cité, sans même parler des chevauchées dans les montagnes.&nbsp;»

Le prieur lança un regard soupçonneux à son cellérier. Se pouvait-il que ce soit un homme de frère Pons&nbsp;? Ce dernier évoquait si souvent les patrouilles comme justification à toutes ses demandes que Daniel en venait à ne plus supporter d'en entendre parler. Inconscient des pensées qui agitaient son supérieur, frère Guillaume poursuivait ses réflexions à voix haute.

«&nbsp;Cela étant, je ne vois néanmoins pas pourquoi nous devrions fournir la toile d'un pavillon et de deux grebeleures[^115]. Les tentes sont de nul usage en escorte.

--- C'est ce qui me fait dire que c'est là demandes nouvelles pour fournir une échelle[^116].&nbsp;»

Frère Guillaume hocha la tête. Il n'était pas aussi détaché du siècle que le prieur et avait pu apprécier l'importance des hommes d'armes dans leur ordre. Il les considérait comme un mal nécessaire qui permettait de ne pas avoir recours à des expédients soudoyés, dont on n'était jamais certain ni de la vaillance ni de la tempérance. Malgré tout, il appréhendait les choses en gestionnaire et s'inquiétait de voir dépossédé un domaine dont il avait en partie la charge. Pour être moins prestigieuse, la pratique de la charité, y compris envers leurs frères indigents, ne devait pas céder le pas devant les menées guerrières. À ses yeux, c'était la vocation à accueillir qui avait entraîné la fondation de leur ordre, et tout le reste n'en était qu'un des aspects plus ou moins utiles, mais toujours subordonnés.

Prenant le silence de Guillaume pour un acquiescement à sa remarque, Daniel continua.

«&nbsp;Bientôt il nous fera demande de hauberts ou de glaives, j'en ai la crainte. Allons-nous acheter harnois de guerre avec les pieuses offrandes faites à nous&nbsp;? Christ ressuscité en le chemin d'Emmaüs a-t-il demandé à ses disciples un destrier pour aller semer la mort au parmi de ses assassins&nbsp;? Non, il a demandé l'héberge&nbsp;! J'ai fort grandes craintes pour l'avenir que certains dans notre ordre aient oublié ce fait essentiel...

--- Je ne suis pas tant versé que vous en ces points de foi ou en la connaissance des textes saints. Je sais juste que j'ai vu plus de bien naître des repas que j'ai pu distribuer que des violences nées des batailles.

--- Voilà sages paroles. Fort heureusement, il s'en trouve de fort savants qui opinent en ce sens à Paris ou Chartres. Puissent leurs voix porter plus fort que les tambours et cors de guerre&nbsp;! Pierre Damien professait que l'épée de l'esprit, la parole de Dieu, était supérieure au glaive du siècle et que l'Église ne devait pas se soucier de batailleuses querelles. Je crains que le grand abbé Bernard[^117] n'ait eu tort sur ce point. Il n'est de si grand esprit qu'il ne puisse se fourvoyer jamais.

--- Amen&nbsp;» opina Guillaume. ❧

### Notes

La notion de charité était au cœur de nombreuses institutions ecclésiastiques au XIIe siècle et bien sûr en particulier des frères de l'Hôpital. Il ne faut néanmoins pas y mettre le sens commun d'aujourd'hui, car cela correspondait à un aspect essentiel et plus vaste de la doctrine catholique. Il ne s'agissait pas du tout de *faire la charité* comme on peut le comprendre maintenant, avec toute la condescendance et le mépris qui peuvent s'en dégager, mais plutôt de se montrer *charitable*, bienveillant envers son prochain, considéré comme à l'image de Dieu. Les notions d'accueil, de soin aux plus pauvres et aux démunis étaient donc dépendants de ce précepte tant vanté par Paul de Tarse (1 Corinthiens 13:1-7). C'était lié également aux valeurs de solidarité et de fraternité au sein de la communauté. Les frères de Saint-Jean pratiquèrent cette vertu avec ampleur, avant même de devenir un ordre militaire.

Des voix s'élevaient d'ailleurs dans l'Église, à la fois pour s'inquiéter de ce mélange des genres peu aisé, mais aussi plus largement contre la violence, quelle qu'elle soit, y compris envers des croyants d'autres religions, voire contre des puissances guerrières adverses. Cet évangélisme prônait parfois l'exemplarité du martyr comme une voie vers un monde meilleur, préférable à l'adhésion aux règles brutales d'un environnement violent. Cette résistance à l'hypothèse que la guerre pouvait être juste, mais certainement pas sainte, participa à la désaffection progressive de l'idée de croisade.

### Références

Aurell Martin, *Des Chrétiens contre les croisades. XIIe-XIIIe siècles*, Paris&nbsp;: Fayard, 2013.

Aurell Martin, (page consultée le 15 juillet 2018), *Des pacifistes au temps des croisades*, conférence donnée le 2 mai 2017 à l'auditorium de la Cité des sciences et de l'industrie, \[En ligne\]. Adresse URL&nbsp;: https://www.dailymotion.com/video/x5oesuc

Riley-Smith Jonathan, *The knights of St John in Jerusalem and Cyprus c. 1050-1310*, Londres&nbsp;: McMillan, 1967.

Écueils
-------

### Jaffa, bassin de la lune, fin de matinée du lundi 24 mars 1158

Un soleil plus audacieux que les dernières semaines avait fini de déchirer les brumes matinales, inondant les reliefs côtiers d'une chaude lumière. Quelques barques de pêcheurs attardés fainéantaient aux abords des plages tandis que les plus chanceux étaient déjà le long des murailles de la cité, occupés à nettoyer leurs prises et à les vendre aux commères, valets et marchands. Gobin avait aidé à acheminer quelques corbeilles, essentiellement du bar, jusqu'aux commerces qui se trouvaient en ville. Il y avait gagné une odeur entêtante et la promesse de quelques piécettes à la fin de la semaine.

Il s'était arrêté pour avaler une brochette de poisson frais, glissée dans du pain, admirant les vaisseaux à l'ancre dans le port. La ville de Jaffa n'avait pas de quai, se contentant de débarquer les navires à l'aide de petits porteurs depuis une anse abritée par une ligne de rochers, où la légende disait qu'une princesse avait été livrée à un monstre marin pour mettre fin à ses attaques.

Il avait un modeste canot qu'il maniait avec habileté, se proposant au déchargement dès qu'un bateau se présentait. Avec la fin de l'hiver, les affaires reprenaient peu à peu, la saison de mauvaise mer finissant enfin. Il n'aimait guère devoir accepter des tâches qu'il estimait dégradantes et mener sa barque à son bon plaisir lui semblait plus prestigieux. Il tirait grande fierté de son indépendance et de son expérience au service du roi de Jérusalem. Il n'avait fait partie de son hôtel qu'à peine plus de deux ans, mais à l'entendre, il y avait passé sa vie.

Habitué des tavernes de bric et de broc qu'on rencontrait dans les environs du port et sur les plages même, il y contait sans lassitude la façon dont il avait dû se retirer du service après avoir été gravement blessé au pied. Il avait dû se battre pour qu'on ne l'ampute pas et il traînait désormais sa claudication, lançant son pied à chaque pas. Il prenait la chose avec philosophie et soignait ses douleurs avec de grandes quantités d'alcool, de toute provenance. On l'appelait plus fréquemment la Patte que Gobin.

Figure familière du quartier, il était arrivé qu'il soit recruté un temps pour prêter main-forte aux hommes affectés à la surveillance sur les murailles. Sa formation militaire, bien que modeste, lui offrait l'avantage de connaître les missions habituelles. Il conservait d'ailleurs précieusement son casque de fer et son épée, qu'il astiquait régulièrement pour en maintenir le brillant. Même au plus fort des coups du sort, il n'avait jamais voulu se séparer de ces symboles de sa compétence guerrière.

Il s'apprêtait à retourner en ville, où il espérait retrouver quelques compagnons désœuvrés comme lui, histoire de passer la journée à discuter et faire rouler les dés. Il n'était pas tant joueur, mais appréciait de commenter l'actualité avec une poignée de commensaux. Ils avaient leurs habitudes dans une ruelle qui serpentait depuis les hauteurs de la citadelle, ondulant sur les reliefs en amphithéâtre en direction du rivage.

Il franchissait les portes de la Mer lorsqu'une rumeur se mit à enfler. Une rapide agitation grondait depuis la berge au sud, pour parvenir jusqu'à lui. Des navires arrivaient, en assez grand nombre. Leur formation, leur allure, tout indiquait que ce n'était pas de simples vaisseaux de commerce, mais des galées taillées pour la course et la guerre. Et si elles montaient depuis les zones méridionales, elles étaient probablement égyptiennes, donc ennemies.

En peu de temps, le grouillement devint maelström. Chacun s'agitait pour porter à l'abri ses maigres biens dans l'enceinte de la ville. Les hommes dans les barques ramaient aussi vite qu'ils le pouvaient et échouaient leur embarcation au plus haut. Des cors résonnaient depuis les murailles tandis que les églises s'étaient mises à carillonner, propageant l'alerte.

Inquiet de n'avoir pas tiré son canot suffisamment loin, Gobin s'élança au rebours de la foule, en direction du sud. Il eut tout le temps de voir que l'escadre était forte de nombreux navires, qui ne laisseraient aucune chance aux quelques vaisseaux de commerce ancrés en deçà des rochers d'Andromède. Ce n'étaient peut-être que des pillards, les Égyptiens n'ayant jamais lancé de véritables attaques sur les cités fortifiées. Dans le doute, il ne tenait pas à ce que son seul moyen de subsistance finisse en bois d'œuvre pour un mantelet ou une bricole à l'assaut de la ville.

Il tira l'esquif aussi loin qu'il le put jusqu'au couvert des premiers jardins. Quelques autres avaient suivi son exemple, avant de disparaître dans les frondaisons. Voyant que la flotte se divisait désormais, certaines galères venant à la manœuvre pour passer dans le port, il opta pour un repli stratégique vers l'est, espérant franchir les murailles par Bab el-Belad, du côté opposé à la mer. Il avançait aussi vite qu'il le pouvait, mais sans s'abandonner à la terreur. Il savait qu'il faudrait du temps aux musulmans pour s'emparer des navires stationnés et débarquer éventuellement leurs hommes, dans le cas où ils envisageraient de s'attaquer à la ville. Ils ne pouvaient de toute façon certainement pas accoster sous les tirs des archers sur les chemins de ronde. Tout ce que la cité comptait d'arbalétriers et de frondeurs avait dû se poster sur les courtines, et peut-être même les bricoles avaient-elles été pointées sur le large. Les jeux nautiques pendant lesquels les quartiers, confréries et paroisses s'affrontaient à viser des tonneaux allaient finalement dévoiler leur utilité.

Il croisa quelques laboureurs et jardiniers, inquiets du tintamarre, leur expliqua en quelques mots que des navires ennemis étaient en vue. Même si personne ne croyait qu'une armée serait assez folle pour établir un camp de siège en plein cœur du royaume, tous craignaient le pillage. Ils emportaient donc tout ce qui pouvait avoir de la valeur à leurs yeux, qui ne pouvait se cacher et qui n'était pas trop lourd et encombrant.

L'oasis qui entourait la cité était florissante, les arbres fruitiers bénéficiant d'une irrigation incessante, apportée par des norias où tournaient poneys et mules. Les propriétaires des bêtes se hâtaient de les libérer de leur harnachement afin de les mener à l'abri dans les murs. Gobin prêta la main à un vague compagnon de soirée qui s'était mis en devoir de rapatrier deux ânes et quelques moutons qui appartenaient à son maître. Il avait abandonné l'espoir de rassembler la volaille.

Comprenant qu'il y aurait peut-être quelqu'avantage à ne pas se précipiter, Gobin le laissa filer devant, indiquant qu'il souhaitait s'assurer d'un autre compère. Il fit demi-tour, dans l'espoir de pouvoir profiter du désordre pour s'emparer de quelques victuailles. Que ces maudits musulmans lui permettent au moins de manger correctement dans les jours qui viendraient&nbsp;! Lorsqu'il quitta la zone des jardins, il portait à l'épaule un chiffon noué en sac où il gardait deux poules rapidement étranglées, une poignée d'œufs et des légumes pour quelques bons repas.

### Jaffa, porte orientale sur la route de Rama, début d'après-midi du lundi 24 mars 1158

Lorsqu'il parvint enfin à la porte orientale, une foule s'était massée devant les vantaux clos. Ceux qui n'avaient pu entrer interpellaient avec colère les hommes de faction sur la muraille au-dessus. Gobin reconnut quelques visages parmi les personnes du guet et siffla pour se signaler à eux.

«&nbsp;Pourquoi donc fermer les portes ici, compère&nbsp;? Ils sont en galées sur les plages, ce ne sont pas cavaliers&nbsp;!

--- Ordre du château&nbsp;! Toutes les portes ont été barrées. Seul un des chevaliers du comte peut nous ordonner la réouverture&nbsp;!

--- Ne peut-on pas au moins passer par le guichet&nbsp;?

--- On l'aurait ouvert si on le pouvait&nbsp;!

--- Vous allez nous laisser là dehors&nbsp;?

--- Restez dans le coin, on vous fera entrer dès qu'on pourra.&nbsp;»

Le petit groupe de refoulés se mit à pester de plus en plus fort, certains ayant avec eux bêtes et bagages. Ils estimaient injuste de se trouver séparés des leurs, risquant de se faire prendre par les attaquants. Curieux d'en savoir plus et inquiet de se voir questionné sur son chargement, Gobin continua de longer la muraille à main gauche, de façon à rejoindre la partie septentrionale des jardins. Il pourrait éventuellement y apercevoir ce qui se passait en avançant prudemment. De toute façon, en restant près des fortifications, il avait peu de chance de croiser des assaillants. Même s'ils étaient en quête d'esclaves, ils n'allaient pas débarquer ici. Il était bien plus facile de déposer les hommes à terre discrètement au large d'une petite agglomération et de faire un raid éclair sur des territoires sans protection. Ils en voulaient plus certainement aux navires à l'ancre.

L'oasis était étrangement calme, les cultivateurs ayant abandonné la zone comme au sud. Il croisa quelques chiens, ainsi qu'un âne qui avait dû échapper à son propriétaire. Il tenta de l'amadouer pour s'en emparer, en pure perte. Il continuait d'avancer sous les frondaisons en ne s'écartant guère des remparts, attentif au moindre bruit. Une brise de mer apportait les senteurs salines et lui permettrait d'entendre assez clairement ce qui se déroulait sur la plage. Quand il estima n'en être plus guère éloigné, il bifurqua vers le nord, en direction de l'église Saint-Nicolas. Elle était située en plein milieu de la zone de cimetières, sur un surplomb au-dessus du rivage et offrirait une belle vue sur ce qui s'y passait.

Il pourrait aussi s'assurer de Jonas, le vieux gardien, qu'on disait à demi fou. Gobin le connaissait depuis des années et savait qu'il avait néanmoins suffisamment de bon sens pour se mettre à l'abri. Il était original, mais pas téméraire. Sa nonchalance n'était pas si forte qu'il ne reconnaissait pas le danger. L'église, ancienne, était entourée de quelques bâtiments annexes. La nef y était suffisamment vaste pour y accueillir pas mal de personnes et servait surtout pour les cérémonies funéraires et quelques bénédictions en rapport avec la mer. Le prêtre titulaire n'y était que peu et c'était Jonas qui faisait la plupart des corvées, assisté parfois de quelques manouvriers. Il n'était pas rare de le voir sur le toit réparer les dégâts d'un fort coup de vent.

Lorsqu'il parvint aux abords de la clôture, Gobin y perçut les bruits familiers&nbsp;: quelques poules caquetaient et des bêtes pâturaient derrière les barricades. On y trouvait aussi un petit troupeau de chèvres et un âne. Ce dernier était généralement attaché à la roue du moulin qui puisait l'eau. La porte était fermée et Gobin ne chercha pas à frapper, sachant que Jonas ne répondrait certainement pas aux sollicitations. Il vint en rampant jusqu'en bordure du ressaut qui surplombait la plage.

Il ne discernait que peu la rade, masquée en partie par l'enceinte, mais put compter les nombreuses galères qui se tenaient au large, prêtes à intervenir si le besoin s'en faisait sentir. Il en voyait trois qui avaient pénétré dans le port par le nord, s'exposant au tir depuis les murailles. Ayant contourné les rochers d'Andromède, deux d'entre elles avaient abordé un navire à l'ancre et semblaient chercher à s'en emparer. Une fois l'équipage fait prisonnier ou tué, il s'agissait de mettre à la voile au plus vite avant de subir trop de dommages. Il était possible qu'ils tentent la même opération plus au sud, quoique les dangers y fussent plus grands, l'endroit se trouvant directement en contrebas de la citadelle, zone la mieux pourvue en défenseurs.

Gobin nota avec soulagement que nul attaquant n'avait posé les pieds à terre. C'était un raid de pillage comme les Égyptiens les affectionnaient, une razzia sur les mers qui ne prenait pas le risque de s'enliser dans de longs affrontements. Le temps qu'une réaction puisse s'organiser, ils étaient généralement déjà partis, comme les Bédouins du désert. Revenant sur ses pas, il osa alors appeler Jonas depuis l'entrée.

Le vieil homme mit un moment à lui répondre, d'une voix moins forte que la sienne. Il lui permit de se faufiler rapidement, reposant rapidement la barre du portail. Dans la cour se trouvaient quelques jardiniers, des enfants et une dizaine de marins et de pêcheurs.

«&nbsp;Il faut juste attendre, ils ne s'en prennent qu'aux navires de commerce.

--- Nous avons veillé leur attaque depuis le beffroi, indiqua Jonas. Nous regardons l'orage passer.&nbsp;»

La voix d'un gamin qui faisait la vigie les interrompit.

«&nbsp;Ils tentent de hisser les voiles&nbsp;! Leurs galées semblent s'éloigner du *Luna*. Ils doivent l'avoir pris&nbsp;!&nbsp;»

Gobin en grogna de mécontentement.

«&nbsp;Je vais aller voir ce qu'il en est de plus près.

--- Demeure à l'abri, tu ne sauras rien faire d'utile au-dehors.

--- Ils ne s'occuperont pas de moi s'ils sont là pour les marchandies.

--- Ils ne cracheront certes pas sur un esclave de plus&nbsp;!&nbsp;»

Négligeant l'avertissement, Gobin fit un sourire encourageant à Jonas puis ressortit du petit enclos. Il n'aimait pas se sentir enfermé, quand bien même c'était pour être à l'abri.

### Jaffa, plages septentrionales, après-midi du lundi 24 mars 1158

Demeurant sous le couvert des arbres, Gobin avait lentement longé la côte jusqu'à retrouver les murailles. Il pouvait ainsi avoir une meilleure vue sur ce qui se passait. Quelques galées se maintenaient au loin, à l'abri des armes de tir de la cité, tandis qu'une demi-douzaine s'affairait autour de trois gros navires amarrés là. Comme l'avait indiqué le gamin, le *Luna*, une nef ventrue en provenance de Pise manœuvrait pour prendre le vent en direction du large. Sur les autres bateaux, les choses semblaient moins claires, soit les affrontements y continuaient, soit les marins avaient saboté suffisamment les dispositifs de gouverne pour éviter qu'on s'en empare.

Les tirs depuis les murailles visaient sporadiquement les galères à distance des embarcations latines, sans grands effets. Gobin huma la brise&nbsp;: elle avait tourné et favorisait désormais les musulmans s'ils souhaitaient s'éloigner. Ils avaient finalement hissé les voiles sur un navire supplémentaire outre le *Luna* et les galées pivotaient pour laisser le champ libre. Ils allaient rejoindre le reste de l'escadre avant de rapatrier ce butin dans leurs ports. Soulagé de les voir partir, Gobin héla un des soldats sur la muraille et lui demanda quand ils ouvriraient les portes. Le guetteur ne savait pas, il fallait attendre les ordres de la citadelle.

Gobin envisageait de rebrousser chemin, estimant que ce serait certainement à l'orient qu'il serait possible d'entrer en premier, quand il aperçut des gerbes d'eau autour du *Luna*. Des matelots sautaient à la mer alors que le vaisseau glissait en prenant de la vitesse. Des cris montaient depuis la nef, en pure perte. Les hommes battaient des bras et des jambes dans les vagues avec l'énergie du désespoir, inquiets de recevoir une flèche tandis qu'ils s'échappaient. Les razzieurs n'étaient généralement pas féroces et craignaient les représailles, ne versaient le sang que lorsqu'ils y étaient contraints. Si les matelots arrivaient à rejoindre la rive, ils seraient sauvés.

Incapables de contrôler leur nage désordonnée, plusieurs d'entre eux se contentèrent d'aller jusqu'aux récifs, déjà heureux de sentir sous leurs pieds une assise solide. Quelques-uns parvinrent, épuisés et suffocants, à la berge. Pendant ce temps, les galères et leurs prises formaient une escadre qui gagnait de la vitesse en direction du nord-ouest. Comprenant que le danger était quasiment écarté, Gobin avança vers les rescapés et les aida à se mettre au sec. Puis il détacha un des canots tirés à l'abri et le hala jusque dans les flots pour aller chercher ceux qui s'étaient accrochés aux rochers. Avant même d'y parvenir, il reçut de leur part suffisamment de bénédictions et de remerciements pour se garantir un Salut dans l'Au-delà.

Les hommes sautèrent vers lui et se hissèrent en soufflant dans la petite embarcation, manquant de la faire chavirer dans leur agitation. Sans sa connaissance des lieux et sa maîtrise des rames, ils auraient tous fini à l'eau. Une fois installés dans la chaloupe, ils laissèrent éclater leur soulagement en vociférant blagues et invectives grossières, à la mesure de leur frayeur passée.

Le temps qu'ils reviennent au rivage, la porte de la Mer avait été ouverte et des secours s'étaient avancés pour accueillir les naufragés. Des pêcheurs sortaient aussi, soucieux de leurs embarcations. Gobin salua aimablement l'un d'eux, à qui il avait emprunté la barque et l'aida à la remettre en place. L'homme n'était qu'à demi content qu'on ait ainsi disposé de son bien sans lui en faire la demande, mais il se laissa convaincre de mauvaise grâce quand les remerciements et les félicitations fusèrent autour de Gobin. La solidarité en mer était une vertu louée et le risque, même mineur, qu'avait pris Gobin, offrait l'occasion de se réjouir en un jour plutôt malheureux. Les Égyptiens avaient saisi trois navires, dont un qui avait une belle cargaison de bois.

Gobin savourait ce petit moment de grâce, tout en espérant que cela lui redonnerait du crédit auprès de quelques commerçants. Il avait souvent la pépie et l'appétit plus conséquents que sa bourse, sans même parler de ses égarements au jeu. Il remerciait chaleureusement les bénédictions, souriant d'une oreille à l'autre tandis qu'il avançait avec les rescapés qu'il avait secourus. Ceux-ci tenaient à le convier à une veillée dans le quartier pisan, où on fêterait le salut des quelques matelots. Convaincu par ces agréables perspectives, Gobin se laissa mener, comprenant de moins en moins les échanges alors qu'ils se faisaient de plus en plus en italien.

### Jaffa, quartier pisan, veillée du lundi 24 mars 1158

Malgré tous les désagréments de la journée, l'ambiance était joyeuse avec les marins réchappés, et il y avait même un héros à célébrer&nbsp;: Gobin. C'étaient plutôt les familles humbles qui se félicitaient, n'ayant rien perdu dans l'affaire. Les maisons des patriciens, des négociants qui avaient vu partir leurs biens et leurs navires ne brillaient guère des feux de la fête. Pour une fois que les motifs de réjouissance touchaient davantage les plus modestes, ceux-ci ne boudaient pas leur plaisir. En plein Carême, chacun y allait de son cellier pour participer&nbsp;: pâtés, tartes et boissons jaillissaient comme si une corne d'abondance avait soudain surgi dans la cité. On avait sorti quelques instruments et, rapidement, branles et sardanes avaient agité bras et jambes.

Gobin profitait de l'instant, avalant tout ce qui se présentait à sa portée. Il avait posé dans un coin mal éclairé son butin de l'après-midi, en espérant qu'il serait toujours là lorsqu'il rentrerait chez lui. Avec son pied paralysé, il ne dansait pas, mais frappait des mains en sifflant pour accompagner la musique. Cela faisait des années qu'il n'avait pas passé une si bonne veillée. Il regretta que sa vieille mère n'ait manqué cette célébration que de quelques mois. Son frère, qui était resté enfermé dans leur maigre masure, avait fini par le rejoindre, amusé et curieux de voir Gobin pareillement admiré. Il craignait, disait-il, que l'avenir leur réserve une pluie de grenouilles ou de la neige en été, au rythme où allaient les choses. Il participa néanmoins de bon cœur à la fête, profitant du sillage de renommée pour se faire valoir à son tour.

Un moment que la musique retombait un peu et que les plus essoufflés avalaient de quoi récupérer l'énergie pour une nouvelle farandole, Gobin retrouva deux des hommes qu'il avait recueillis. Ils n'avaient guère eu le temps de discuter depuis l'après-midi et ils se présentèrent un peu. Bonado et Federico étaient Génois, embauchés depuis quelques mois sur le *Leo* après avoir vu le navire où ils étaient subir tellement d'avaries après une tempête qu'il risquait le désarmement. Loin de chez eux, à Saint-Syméon dans la principauté d'Antioche, ils avaient accepté le premier engagement venu. Ils se trouvaient donc une nouvelle fois désœuvrés, avec pour tout bien ce qu'ils portaient sur eux.

«&nbsp;Il se trouve ici quelques Génois, rassurez-vous, et au pire vous pourrez cheminer au nord, où ils tiennent plusieurs quartiers. Les mers viennent de s'ouvrir, de bons marins trouvent toujours de l'emploi.

--- J'avais acquis un peu de poivre et d'encens, tout ça va finir chez le Soudan[^118], grogna Bonado.

--- *Fortune tourne sa roue*, compère. Tu as sauvé ta vie, c'est déjà ça. Il ne fait pas bon finir en les geôles babyloniennes.

--- Mes derniers voyages m'ont rendu plus pauvre qu'à mon départ...&nbsp;»

Gobin haussa les épaules et avala un peu de vin d'une outre qu'il fit passer aux deux marins. Il n'avait guère envie de se lamenter et préférait profiter de l'instant. Il avait trop d'occasions de se plaindre et n'allait pas refuser pareil rayon de soleil. Demain il serait bien assez tôt pour retrouver sa vie ordinaire.

«&nbsp;Réjouis-toi de ta bonne fortune pour ce soir, compère. Demain nous verrons ce qu'on peut faire pour vous. Il ne sera pas dit que notre bonne cité de Jaffa ne sait faire charité au bon pèlerin&nbsp;!&nbsp;»

Il accorda une bourrade amicale aux deux matelots et leur reprit la gourde. S'il ne s'enivrait pas ce soir, il aurait le sentiment de faillir à sa réputation. Il était encore temps pour cela.

«&nbsp;Puisse le Seigneur faire que récifs où nous frappons être garni de quelque belle garcelette à la voix mélodieuse pour nous réconforter&nbsp;!&nbsp;» ❧

### Notes

Les opérations guerrières, latines ou musulmanes, lors des croisades n'étaient parfois que des raids de brigandage, que ce soit à terre ou en mer. Durant le mois de mars 1158, des émirs mécontents de la politique de soumission apparente du vizir égyptien au pouvoir latin décidèrent de braver ses ordres et entreprirent de mettre au pillage la côte franque. Ils en ramenèrent un confortable butin, après avoir remporté quelques succès dans les affrontements et créé une certaine pagaille sur les rivages.

Même si c'était là une campagne conçue par les autorités militaires, ce n'était pas des actions avec une visée stratégique d'ampleur. Le but était de déstabiliser le voisin tout en s'enrichissant. En l'occurrence, c'était peut-être aussi lié à un souhait de venger l'affront de la prise d'Ascalon quelques années plus tôt. Tout n'était pas motivé uniquement par le désir de conquête et l'esprit de lucre avait sa part dans les décisions.

### Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas&nbsp;: Institut Français de Damas, 1967.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem. A corpus. Volume I. A-K*, Cambridge&nbsp;: Cambridge University Press, 2008.

Riley-Smith Jonathan (ed.), Ellenblum Ronnie, Kedar Benjamin, Shagrir Iris, Gutgarts Anna, Edbury Peter, Phillips Jonathan et Bom Myra, *Revised Regesta Regni Hierosolymitani Database* http://crusades-regesta.com/ (consulté le 24/01/2018)

Vie commune
-----------

### Jérusalem, hôpital de Saint-Jean de Jérusalem, après-midi du mardi 18 décembre 1156

Capuche de son manteau sur la tête, bras croisés, appuyé au chambranle de la porte basse ouvrant sur la cuisine des femmes, Joris discutait avec les servantes qui allaient et venaient, portant de l'eau et des denrées pour le repas du soir. Il aimait à vaguer, usant de tous les prétextes pour prendre son temps lorsqu'une mission lui était attribuée. Bien qu'il n'ait que rarement été réprimandé pour un travail mal fait ou achevé de façon incorrecte, il s'était ainsi vu peu à peu confier des tâches de moins en moins intéressantes et agréables. Il répondait à cette déchéance avec un entrain déclinant et une inventivité redoublée pour flâner et discuter.

Entré au service de Saint-Jean quand il était tout juste sorti de l'enfance, il n'avait rien pu y apprendre d'utile pour accéder à un nouveau métier et avait fini à la préparation des morts pour leur ultime voyage. Depuis plusieurs mois, il était plus particulièrement affecté au transport des corps, au sein de l'établissement et pour leur acheminement jusqu'à Chaudemar où ils étaient grossièrement ensevelis, dans un immense charnier pour les plus humbles. Il supportait la chose avec philosophie, même s'il espérait pouvoir quitter un jour son état de valet. Enfant, il avait travaillé aux champs avec sa famille, et s'il n'y avait alors pas pris grand plaisir, il repensait à ces années comme à un temps d'insouciance et de liberté. Il estimait qu'il devait préserver ses forces pour ce moment béni où il peinerait sur sa terre et non pas au service d'un maître, fût-il parfois aussi bienveillant et peu exigeant qu'un frère de Saint-Jean.

Il avait un groupe d'amis qui renâclaient autant que lui à se fatiguer à la tâche. Ils avaient développé un talent certain pour se retrouver de façon imprévue en tous lieux de la clôture, prenant le temps d'échanger potins, rumeurs et nouvelles, voire blagues et devinettes. Étant un des rares hommes autorisés à pénétrer dans certaines zones réservées aux femmes, Joris en prenait avantage pour plaisanter avec celles qu'il rencontrait et il avait son petit cercle de commères avec qui papoter à l'abri des regards des responsables. Parmi celles-ci, Gilleite était la plus bavarde. D'une cinquantaine d'années, le physique pesant et rompu hérité d'une vie de labeur difficile, elle s'était donnée à l'ordre après la mort de son époux. N'ayant nul bien, elle travaillait pour le gîte et le couvert et n'avait prononcé que quelques vœux sans devenir sœur à part entière. Elle avait pris Joris en affection, d'autant qu'il supportait sans fatigue son babillage incessant et l'accueillait chaque fois d'un sourire.

Attendant qu'on lui confie une charrette à bras où quelques dépouilles féminines avaient été préparées dans des linceuls pour leur dernier voyage, il estimait qu'il avait toute licence pour bavarder, distrayant de leur tâche celles qui s'en sentaient d'humeur. Gilleite saisissait toujours ce genre d'occasion pour lui faire part de ses réflexions, mêlées de remarques sur ses douleurs corporelles et les aléas climatiques, avec une platitude et une inexactitude qui n'entamaient en rien l'enthousiasme qu'elle y mettait. Ces temps-ci, elle s'émouvait particulièrement du sort de la vieille reine mère Mélisende[^119], retirée à Naplouse, dont on disait la santé vacillante. Gilleite n'avait que des éloges à faire de son règne et déplorait que le fils n'en ait pas hérité tous les talents.

«&nbsp;Il devrait faire plus fréquentes visites à ses tantes. Il apprendrait d'elles meilleure religion, plutôt que de s'adonner à la lance et l'épieu&nbsp;!&nbsp;»

Joris acquiesçait de façon machinale à ces considérations générales. Il n'avait que peu d'opinions sur la famille royale quoiqu'il appréciât tout particulièrement le comte Amaury de Jaffa, dont le talent à la chasse et auprès des femmes était vanté parmi les hommes. Il n'en soufflait évidemment rien à sa confidente du jour, qui déplorait ces attitudes qu'elle jugeait peu dignes d'un baron. Si elle trouvait tout à fait normal que les plus humbles s'adonnent à leurs vices et leur pardonnait volontiers leurs écarts, elle estimait qu'il était du devoir des puissants de se montrer vertueux et sages, de belle allure et, surtout, très pieux.

Tout en pérorant, elle remarqua d'ailleurs que le regard du jeune homme volait vers la silhouette d'une servante affectée au tamisage de la farine et le tança gentiment en lui donnant un coup de coude.

«&nbsp;La Marie a bien grandi ces derniers temps, hein&nbsp;?&nbsp;»

Il confirma d'un coup de menton, sans lâcher la fille des yeux. Il la connaissait depuis des années, l'avait toujours considérée comme une gamine un peu effrontée, mais d'agréable compagnie, qu'il houspillait parfois pour la forme, dans un sourire. Aujourd'hui, pour protéger ses cheveux, elle les avait noués sous un linge en un simulacre de foulard qui la faisait paraître telle une femme mariée. Elle avait en quelques mois développé tous les signes de sa féminité, perdant pour le coup son allure de garçon dégingandé. Il semblait aussi à Joris qu'elle mettait plus de grâce dans ses gestes, plus d'affectation dans ses postures. Peut-être avait-elle senti l'attention d'un homme sur elle&nbsp;?

Gilleite ricana et lui donna une légère bourrade.

«&nbsp;Tu sais, elle s'est pas donnée à l'ordre. Elle sert ici jusqu'à ce qu'elle trouve un mari.&nbsp;»

Elle eut un mouvement d'épaules, ses traits se durcissant soudain.

«&nbsp;Enfin, si elle a occasion que quelqu'un s'occupe d'en dénicher un qui s'accorderait de la dot de misère d'une orpheline&nbsp;!

--- Elle n'a plus personne&nbsp;?

--- Je crois pas, peut-être quelques frères, mais elle est sans le sou, pour sûr. Tu pourrais en faire honnête femme, je vois bien qu'elle est à ton goût&nbsp;! Les sœurs seraient ravies de la voir marier bon chrétien, elle qui n'est que baptisée.&nbsp;»

Joris étira un rictus en réponse. Il savait qu'il ne pourrait obtenir un manse en tant que célibataire et une orpheline d'origine musulmane serait plus aisée à épouser, moins exigeante sur ce qu'il devrait fournir. Toutefois, en la regardant, il se dit qu'il pourrait aussi s'éprendre de cette jeune fille au caractère bien trempé. D'autant qu'il avait toujours trouvé les Syriennes plus attirantes que les Européennes. Peut-être y retrouvait-il l'image de sa mère, qu'il avait tant aimée, enfant&nbsp;?

### Jérusalem, hôpital de Saint-Jean de Jérusalem, fin de matinée du vendredi 4 janvier 1157

Marie finissait à peine de border le lit dont elle venait de changer les draps qu'on lui amenait une femme dont la toux avait épuisé la voix. Elle lui sourit sans chaleur, habituée à recevoir tant de malades qui ne survivaient que quelques jours qu'elle évitait de trop sympathiser. La pauvre était brûlante de fièvre, avait été recueillie par un valet d'une boutique qui l'avait trouvée sans connaissance dans la rue[^120]. Elle avait pu murmurer son nom, Luce, et qu'elle était pèlerine. Épuisée par ses quintes, les paupières gonflées par le manque de sommeil, elle était aussi très amaigrie par les privations du long voyage. Elle se laissait faire comme une enfant, à peine capable de lever les bras ou de tenir les yeux ouverts. Marie l'aida à enfiler une chemise de laine propre et rassembla ses rares affaires dans un sac qu'elle confia à la sœur responsable de la travée. Ses habits seraient nettoyés et éventuellement réparés et conservés jusqu'à son départ ou son décès. À voir la croix encore cousue à son épaule, elle n'avait pas accompli son vœu et était vraisemblablement arrivée depuis peu.

Au moment où Marie achevait de l'allonger, ramenant sur elle l'épaisse couverture, Luce murmura quelques mots. Elle avait un de ses compagnons, Marcel l'Espéron, logé à l'Asnerie, qui serait certainement soulagé de l'apprendre aux bons soins des sœurs. Sans cela, il risquait de s'inquiéter de sa disparition. Marie la rassura, affirmant qu'un des valets porterait la nouvelle. Avant de la quitter, elle lui toucha le front, brûlant de fièvre. Malgré son évident épuisement, elle toussait tellement qu'elle n'avait guère de chances de s'endormir de sitôt. Marie l'indiqua à la sœur responsable de sa rue, qui s'occuperait de demander un traitement au médecin lors de sa visite. Il fallait avant tout que la malade soit en mesure de se confesser au plus vite, surtout si son état était grave. Le péril de son âme était bien plus urgent que celui de son corps, par essence mortel.

Comme elle avait achevé ses tâches matinales, on lui confia plusieurs messages à aller porter du côté des hommes. Les patientes ne pouvaient recevoir leurs époux, fils ou parents et d'incessants échanges se faisaient par des commissionnaires afin de transmettre les nouvelles, les demandes ou les questions. Marie saisissait toujours cette occasion de franchir la clôture pour tenter de s'esquiver au-dehors du quartier. Les accès du couvent féminin étaient généralement gardés par des cerbères peu aimables et guère enclins à la bienveillance, mais celles de la partie masculine étaient sous la surveillance de valets bien moins regardants, surtout si on leur adressait un charmant sourire en guise de salut.

Elle vint donc transmettre les messages au frère à l'entrée de la vaste salle, qui les nota sur une tablette, puis prit congé en prenant garde à ne pas se diriger devant lui vers la grande porte ouvrant sur le Saint-Sépulcre. Elle avait l'habitude d'emprunter une autre sortie, moins visible, qui permettait, par une placette et une ruelle adjacente, de se retrouver dans la cité. Alors qu'elle descendait l'escalier menant dehors, elle croisa Joris qui revenait apparemment de la chapelle ardente, portant sur un brancard avec un compère un corps enveloppé d'un linceul. Elle lui accorda un sourire, qu'elle accompagna d'un geste amical de la main. Elle appréciait assez le valet, qui ne l'avait jamais traitée en fillette. Elle avait subi jusqu'à l'écœurement les bons soins et la prévenance des sœurs et lui était reconnaissante de n'avoir pas vu en elle qu'une pauvre petite chose à protéger.

Tandis qu'elle le regardait s'éloigner avec son lourd chargement, elle lui trouva une certaine allure. Il s'était visiblement fait raser récemment et ses cheveux n'étaient pas trop mal peignés. Ses vêtements propres indiquaient qu'il était également soucieux de son apparence et attentif à ses affaires, ce qui n'était pas une vertu si répandue chez les jeunes hommes. Ayant pris ses renseignements, elle savait qu'il n'était pas marié ni promis. S'il n'était pas trop porté sur le vin ou les dés, il pourrait faire un bon époux, estima-t-elle. Elle était consciente de la précarité de sa situation et avait apprécié qu'il lui adresse un regard intéressé qu'elle ne jugeait pas déshonnête. Depuis qu'elle arborait des formes féminines, elle avait bien vu qu'on ne la jaugeait plus de la même façon, et elle ne goûtait pas toujours les frottements ou les contacts plus ou moins fortuits que de nombreux hommes se permettaient. Au moins Joris se contentait-il de ses yeux. Et il avait un sourire vraiment charmant...

### Jérusalem, Malquisinat, soirée du mercredi 27 mars 1157

En dépit de Carême et durant toute la Semaine sainte, les boutiques du quartier au centre de la ville ne chômaient pas, avec l'afflux de voyageurs et de pèlerins venus assister aux festivités. Les commerces de bouche bénéficiaient d'exemptions, les établissements religieux ne pouvant à eux seuls nourrir la myriade de croyants. Malgré la presse, Joris réussit à obtenir les rissoles à jour de poisson[^121] qu'il affectionnait tant, mais pour la boisson, il avait renoncé au vin de sa taverne préférée pour se rabattre sur une petite bière qu'il estimait correcte. Il rejoignit Marie, qui l'attendait assise sur le rebord d'une citerne publique. Ils avaient profité de l'agitation de la Semaine sainte pour s'esquiver chacun de son côté afin de prendre un repas ensemble. Il y avait également souvent des spectacles à thème religieux ou plus simplement de jongleurs, d'acrobates ou de montreurs d'animaux et ils escomptaient bien une très agréable veillée. La nuit était belle et, malgré les rumeurs qui parlaient depuis quelques jours d'un démon tueur de pèlerines, ils avaient prévu de s'amuser.

Si Joris habitait en dehors de la clôture et faisait ce qu'il souhaitait de son temps libre les soirs et les jours fériés, Marie avait dû s'organiser avec ses compagnes de chambrée pour cacher son absence et franchir le mur. Avec la cohue des Pâques, il y avait tant à faire que les sœurs n'exerçaient plus une discipline aussi ferme qu'habituellement. Ce n'était pas la première fois que la jeune fille quittait ainsi le couvent, mais jusque là cela n'avait jamais été pour rejoindre un homme. Ils avaient combiné cette escapade comme deux écoliers facétieux, mais ne prenaient que graduellement conscience de ce que cela impliquait. Leurs sourires s'emplissaient d'une gravité croissante tandis qu'ils goûtaient ensemble le plaisir des festivités.

Joris avait croisé quelques amis, mais il avait repoussé toutes les invitations, ayant promis, annonçait-il, de faire les honneurs de la veillée à Marie. Il obtenait souvent en réponse des regards entendus et des sarcasmes à peine subtils. Mais il gardait une attitude polie envers la jeune fille, n'ayant que peu d'idée sur la manière on faisait la cour de façon correcte à une future épouse. Il n'avait en tête que les modèles des grands chevaliers dont on lui avait narré les aventures depuis son enfance. Et c'était là rarement une part importante de leurs exploits. Il attendait donc simplement le bon moment pour évoquer avec elle les perspectives d'union, Marie n'ayant nul père ou frère à qui poser légitimement la question. Il avait longuement récapitulé tous les avantages dont ils bénéficieraient en se mariant, le principal étant selon lui la possibilité de s'établir en leur propre demeure.

Pour l'instant, Marie ne pensait à rien de tout cela. Elle s'amusait beaucoup, prenant prétexte du moindre spectacle, de la plus petite manifestation pour exprimer sa joie et applaudir à tout rompre. Elle n'avait que peu d'occasions d'ainsi se divertir. Elle avait l'habitude de passer ses jours de repos à coudre son trousseau ou à des jeux de table sous la surveillance d'une sœur, dans une des cours intérieures du couvent. Elle n'en sortait que rarement et lorsque c'était avec une permission, c'était chaque fois en groupe pour aller se recueillir en un édifice saint ou fleurir la tombe d'une importante personne, souvent bienfaitrice de leur ordre. Pourtant en elle demeurait un feu ardent qui ne demandait qu'à déborder de ces lieux étroits où la vie l'avait confinée depuis la mort de ses parents. Par son baptême, elle avait échappé à une existence de misère en un casal gris, mais elle regrettait amèrement ses jeunes années, dont elle ne gardait que des bribes de souvenirs, des images de courses dans les collines et de jeux dans les rues ensablées, parmi les poules et les chèvres.

Ils revenaient d'un spectacle de montreur de singes, en direction du nord de la cité où d'autres festivités les attendaient, poussés par le flot humain chantant psaumes, cantiques et rengaines paillardes selon l'humeur et, alors qu'ils arrivaient sur une placette ornée d'un tamaris desséché et de quelques débris d'un escalier ne menant plus nulle part, Joris estima que le moment était propice. Ils venaient de se mettre en retrait du passage, assis sur un banc en devanture d'une boutique, fermée pour la nuit, et regardaient défiler une longue troupe de pèlerins alémaniques dont les voix fortes faisaient du *Credo* un grondement de tempête. Il prit la main de Marie, qui sentit instinctivement que l'instant s'emplissait de solennité. Elle réussit malgré tout à lui adresser un timide sourire d'encouragement.

«&nbsp;Voilà, Marie. Je voudrais que tu penses à une idée que je crois pas bête, pour nous deux. On n'a pas tant, ni l'un ni l'autre, mais j'encrois qu'on saurait se dénicher un hostel et le faire prospérer. Je te sais vaillante et adroite, et moi je sais travailler la terre. J'ai bon espoir de pas demeurer valet inféodé. Alors on pourrait se jurer notre foi et...&nbsp;»

Au fur et à mesure qu'il marmonnait le discours tant de fois rabâché et répété, il perdait ses mots, hésitait sur les idées. Il n'osait regarder le visage de la jeune fille, inquiet d'y lire un rejet, voire pire encore, du dédain ou de l'amusement à son encontre. Il savait que les femmes pouvaient être féroces et il n'aimait pas se dévoiler ainsi. Mais s'il voulait avancer dans la vie, il lui fallait prendre des risques. Après tout, il n'y avait là aucun témoin qui pourrait se gausser d'un éventuel refus. Sa voix finit par mourir avant d'achever sa dernière phrase. Il leva les yeux vers Marie, pour la découvrir le regard modestement baissé, une ombre de sourire sur les lèvres, sans une once de moquerie. Elle lui serra la main, de façon à peine perceptible et hocha la tête doucement. Puis elle tourna son visage vers lui.

«&nbsp;Si je dois me promettre à toi, j'aimerais que cela soit en tant que femme...&nbsp;»

Elle n'osa rien dire de plus, attendant une réaction de Joris. Il sourit à son tour, un peu timidement, et approcha ses lèvres.

### Clepsta, demeure de Girout la Cirière, fin d'après-midi du mercredi 12 février 1158

La porte largement ouverte laissait pénétrer la lumière, ainsi que le froid, dans la pièce principale du manse, encombrée d'une vaste table. Un soleil timide apportait une rare chaleur là où ses rayons s'avançaient et Joris en goûtait les effets sur son dos tout en dégustant un fond de vin miellé. Face à lui, sa sœur Girout s'employait à l'achèvement d'une structure en paille de seigle. Si son époux ne confiait jamais ses abeilles, ses «&nbsp;mouches à miel&nbsp;» à quiconque, Girout estimait qu'elle était la seule à faire des ruches de qualité. Elle s'occupait de l'extraction de la cire, qu'elle vendait en larges plaques circulaires à des artisans en ville ou à des communautés religieuses.

L'endroit fleurait bon l'opulence paysanne. Plusieurs coffres à pentures longeaient les murs, de nombreuses céramiques dont certaines, de service, décorées, ornaient les étagères et plusieurs chaudrons de fer et un de cuivre pendaient au-dessus du petit foyer d'angle où un feu moribond était maintenu. En plus d'un sol en carreaux de terre cuite, l'hôtel avait une citerne et un beau jardin où les enfants s'amusaient avec les volailles de la basse-cour. À cela s'ajoutaient des champs en suffisance, sans compter un droit de pâture dans les zones en jachère et quelques bois. Les emplacements des ruches y étaient âprement discutés chaque année avec les voisins, qui disposaient de privilèges similaires.

Même la tenue de Girout indiquait leur relative opulence. Sa robe était de laine épaisse, d'un orange profond, et son voile était de soie, certes un peu passée, mais avec quelques décors brodés en bordure. Joris n'était plus venu depuis des mois et pouvait constater l'enrichissement de sa sœur. Il y voyait un présage de ce qu'il pourrait obtenir une fois établi. Reposant son gobelet, il se décida à annoncer la nouvelle qui l'avait mené jusque là.

«&nbsp;Je suis venu te voir pour t'annoncer que je vais quitter le service des frères.&nbsp;»

Girout le dévisagea de ses grands yeux, trop écartés dans son visage plat, la faisant ressembler à une chouette. Elle pinça les lèvres, attendant de savoir ce qu'annonçait cette entrée en matière.

«&nbsp;Ils ont obtenu gain de cause pour des terres à Mirabel, dans le comté de Jaffa. Ils offrent de bonnes conditions pour qui veut s'y établir.

--- Il y a de l'ouvrage pour simple manouvrier&nbsp;?

--- Si j'y vais, ce sera en chef de famille. J'ai une bonne amie, les épousailles se feront avant de partir.&nbsp;»

De surprise, Girout posa son ouvrage. Elle avait toujours pensé que Joris, benjamin de la fratrie, resterait valet et célibataire. Sans avoir, il n'avait aucune chance de trouver une femme qui accepte de s'installer avec lui. Elle craignait qu'il ne se soit résolu à une mésalliance. Son frère vit bien son embarras, même s'il avait espéré qu'elle manifesterait un peu plus de joie à cette annonce.

«&nbsp;C'est une fille de Saint-Jean, orpheline. Pour elle aussi, le mariage, ça signifie quitter la servitude.

--- Tu sais que le père t'a rien laissé, à sa mort&nbsp;? Tout est allé au Pierre quand il s'est installé.&nbsp;»

Joris hocha la tête. Leur aîné avait obtenu la quasi-totalité des biens de leurs parents, ayant peu à peu récupéré tous les outils pour sa propre maison. Il était établi dans un casal voisin, et s'il avait conservé auprès de lui un frère et deux sœurs, il n'avait guère plus de relation avec Joris ou Girout.

--- Le père avait gardé tout de même quelques affaires, et c'est de ça que je voulais t'entretenir. J'aimerais porter sa belle cotte, celle en soie bleue, pour mes épousailles.&nbsp;»

Le rictus de désappointement de Girout fut si éloquent que Joris n'eut pas à attendre sa réponse. Sa voix monta d'un cran tandis qu'il se penchait en avant.

«&nbsp;Je sais qu'il l'avait gardée, il la tenait de l'ancêtre et lui accordait grande valeur. Une relique de la prise de la Cité. Elle était trop petite pour le Pierre.

--- Elle n'est plus ici. Quand il a fallu vider le manse, on a fait au mieux.&nbsp;»

Joris fronça les sourcils.

«&nbsp;Elle est passée où&nbsp;?&nbsp;»

Girout haussa les épaules, soupira.

«&nbsp;Elle était bien abîmée, surtout depuis que le chien l'avait mordue au sortir de la messe. Je craignais qu'elle ne perde tout son lustre. C'était du souci, de garder si belle toile. Et puis elle était trop menue pour le Clarin. Tu n'avais pas donné de nouvelles depuis long moment... On ne t'a même pas vu à la mort du père&nbsp;!

--- Hé quoi&nbsp;? Je l'ai su après l'enterrement&nbsp;! Si tu crois que c'est aisé d'obtenir quelques jours pour venir ici&nbsp;! Je ne suis pas maître de mes journées&nbsp;!&nbsp;»

Il grogna encore, avala une maigre rasade, finissant son verre, qu'il reposa un peu violemment, s'attirant un regard de reproche de son aînée. D'instinct, il adoucit sa voix.

«&nbsp;T'en as fait quoi&nbsp;?

--- Elle intéressait un fripier, avec quelques autres affaires dont je souhaitais me défaire.

--- Il me restera donc vraiment rien des vieux&nbsp;?&nbsp;»

Désireuse de ne pas laisser entendre qu'elle pourrait offrir une compensation, ce que son époux n'accepterait que difficilement, Girout fit mine de plonger le nez dans ses brins de paille. Joris s'agita un temps sur ton escabeau, maugréant sans rien dire d'intelligible, puis lâcha finalement, d'un ton plus conciliant.

«&nbsp;J'espère au moins que Clarin et toi vous serez là pour ma bénédiction.&nbsp;»

### Jérusalem, grand hôpital de Saint-Jean, soirée du lundi 3 mars 1158

Sœur Mabile était une grande femme au physique élancé, dont la longue tenue de laine sombre rendait le teint cireux et le visage triste. Un nez discret aux fines narines surplombait une bouche nerveuse, aux lèvres minces, en perpétuelle recherche d'un sourire de circonstance. Ses yeux, fatigués et un peu myopes, glissaient dans une langueur permanente d'un point à l'autre, ne se fixant jamais nulle part et ne se risquant à dévisager quiconque que par le travers ou à la dérobée. Elle avait pris son rôle de marraine très à cœur depuis le baptême de Marie, lui répétant au départ avec patience qu'elle ne se prénommait plus Jamila et l'édifiant avec grandiloquence sur le modèle parfait que constituait la mère de Dieu pour toute femme.

Pour le moment, elle n'était pour une fois guère loquace, lançant des regards inquiets vers sa filleule. Marie était venue la voir dans sa cellule après le souper pour lui annoncer son intention d'épouser Joris, et donc de quitter le couvent. Elle voulait également savoir comment se déroulait la remise de sa dot. Si la moniale n'était pas en soi opposée à toute vie hors de la clôture, elle ressentait ce souhait comme une trahison de la part de la petite fille à qui elle avait consacré tant d'années. Elle avait toujours espéré, et même cru, que Marie finirait comme elle par prendre le voile et prononcer des vœux monastiques.

«&nbsp;Es-tu bien certaine de vouloir tomber ainsi dans le siècle&nbsp;? Tu ne le connais guère, c'est un endroit de périls et de tentations&nbsp;!&nbsp;»

Marie se retint de répondre que, donnée à l'ordre alors qu'elle n'était qu'une enfant, sœur Mabile n'en savait pas plus. Et si elle y voyait un endroit plein de dangers, Marie y espérait un espace du possible, où ses rêves pourraient prendre place. Joris lui faisait bon effet et, s'ils avaient déjà partagé une couche à plusieurs reprises, elle ne l'avait pas trouvé irrespectueux ni violent. Parfois un peu naïf, mais elle accordait à cela un certain charme. Et, surtout, il l'avait traitée dès l'abord comme une adulte, la partenaire avec laquelle il pourrait enfin œuvrer à l'accomplissement de ses aspirations. C'était le premier à la considérer comme une vraie personne et cela l'avait séduite.

«&nbsp;C'est pour cela que je souhaite y cheminer avec un époux à mes côtés. C'est un homme de bien, au service de Saint-Jean lui aussi.&nbsp;»

Un éclair de reproche, vite maîtrisé, brilla dans les prunelles de la religieuse. Un valet ne pouvait avoir grandes qualités, étant donné que la place que chacun s'était vu fixée par Dieu était à proportion de ses vertus.

«&nbsp;J'ose encroire que vous ne souillez pas ces lieux saints par quelque débauche&nbsp;! L'on peut s'accorder à certaines pratiques, une fois mariés, afin de peupler la terre du Seigneur de bons chrétiens, mais je ne saurais entendre parler de toi ainsi que d'une souillon.&nbsp;»

Marie pinça les lèvres. Elle savait que sa marraine avait les plus grandes répugnances à parler de tels sujets, et elle préféra faire porter la discussion sur l'aspect religieux, qui lui semblait terrain plus assuré.

«&nbsp;Nous avons eu l'accord du chapelain pour nous bénir dès après les Pâques. Comme Joris peut avoir son congé avant la Saint-Jean d'été, nous pourrions nous installer avant le plus fort des chaleurs.&nbsp;»

Sœur Mabile se passa une main angoissée sur le visage. Ce n'était pas la première fois qu'une de ses filleules ne répondait pas à ses aspirations. Au moins Marie avait-elle le bon sens de faire les choses de façon plus ou moins convenable.

«&nbsp;Je sais que ce n'est pas de mon rôle de marraine, mais j'aimerais assez voir ton futur espousé avant cela. Que je puisse t'en dire ma pensée.

--- Ce serait fort amiable à vous, mère. Je lui ai déjà parlé souventes fois de vous et de toutes vos bontés, et il serait de certes heureux de vous rassurer quant à ses projets.&nbsp;»

Sœur Mabile acquiesça, rassérénée par ce peu discret hommage à sa sollicitude. Elle se fit également forte de se renseigner auprès du chapelain sur ce valet. Elle estimait que cela demeurait de son devoir de tout mettre en œuvre pour garantir le Salut de Marie. Si la jeune fille renonçait au parcours idéal de la vie monacale, il fallait s'assurer qu'elle aurait de quoi protéger son âme en tant que bonne épouse et mère. Un homme sobre, travailleur et pieux pouvait être toléré, à défaut de se consacrer au Christ.

«&nbsp;Ce mercredi, le prêtre nous bénira de la croix de cendre. Profite de ce Carême pour bien pourpenser cela et prendre la bonne décision. Tu as encore temps de faire bon choix d'ici la fin des Pâques.&nbsp;»

### Mirabel, manse des Terres d'Épines, fin d'après-midi du mardi 24 juin 1158

La porte s'ouvrit en grinçant après que Joris en eût déverrouillé la serrure. Il brandit la clef avec un sourire et la noua à sa ceinture, tout en invitant Marie à entrer. Il n'y avait qu'une pièce, en terre battue, aux murs fraîchement chaulés. Deux petites fenêtres à simple volet apportaient de la lumière et un peu d'air, des céramiques à large gueule étaient enfouies au fond, coiffées de couvercles de bois. Le seul meuble était un cadre au sol, empli de paille&nbsp;: leur lit. Marie s'avança, les traits illuminés par le plaisir. Elle voyait l'endroit pour la première fois, pressa l'épaule de son compagnon.

«&nbsp;Nous voilà chez nous, ça y est&nbsp;?

--- Oui. Ce ne sera vraiment officiel que lorsque j'aurai juré auprès de l'intendant, mais ce n'est que broutille.&nbsp;»

Il considérait leur royaume intime avec autant d'allégresse qu'elle, même s'il y était déjà venu à plusieurs reprises. Durant les semaines précédentes, il avait nettoyé la citerne, étayé quelques murets autour du jardin, entreposé un peu de bois, monté un abri pour d'éventuelles poules. Il avait voulu que tout soit prêt pour l'arrivée de sa jeune épouse.

Ils avaient loué un âne pour les aider à acheminer les affaires depuis Jérusalem, en plus des lourds paquets dont ils étaient chargés. Ils avaient de la toile pour le couchage, ainsi que quelques chiffons. Ils apportaient quelques ustensiles ménagers, de fer ou de bois, louches et cuillers essentiellement, et des écuelles. Joris avait tenu à s'équiper de quelques outils qu'il estimait d'importance&nbsp;: marteau, clous et lames de scie. Sa sœur et son beau-frère leur avaient fait don d'une belle étoffe, dont ils se feraient des vêtements pour les jours de repos et de fête ainsi que d'un bon sac de grains de froment. Joris n'avait pas de terre où les semer, mais il espérait pouvoir rapidement aider au défrichement des espaces sauvages voisins, où les redevances seraient faibles pour les colons.

Ils avaient encore quelques monnaies, dont ils pourraient se servir pour leurs premiers repas, des pots ou des fournitures lourdes qu'ils n'avaient pu apporter. Joris avait déjà prospecté aux alentours pour se proposer à divers travaux. Il y aurait du labeur prochainement pour certains des bâtiments des frères de Saint-Jean et il avait l'intention de se doter d'une pelle voire d'une pioche pour cela. Il semblait prêt à réquisitionner toute l'énergie qu'il avait préservée dans son emploi de valet.

Ils déchargèrent leurs paquets, libérèrent l'âne qu'ils laissèrent en pâture dans le petit jardin, sa longe fixée à un maigre pistachier en bordure, puis s'assirent à terre, avalant le poisson séché, les lentilles et le pain qui les avait nourris durant tout le voyage. Joris avait tenu à puiser de l'eau dans leur citerne, qu'ils buvaient comme si c'était de l'ambroisie, les yeux brillants et la joie au cœur.

«&nbsp;On prendra les chapeaux neufs que j'ai tressés pour aller aux feux de ce soir, proposa Joris. Nos tenues ne sont pas tant belles qu'on risque de nous croire bien pauvres, mais au moins nous aurons belle allure.

--- Avec les longues soirées, j'aurai tôt fait de nous confectionner de quoi paraître à la messe sans honte. Ne t'ayant pas sous la main pour les essayages, je craignais de couper la belle étoffe. Je n'ai pas usage de tailler si belle toile&nbsp;!&nbsp;»

Joris sourit. Il ne l'avait pas encore sorti, mais il avait aussi prévu d'offrir à Marie un voile de fine étamine, digne d'une riche bourgeoise voire d'une femme de petit baron. Il l'avait trouvé chez un fripier à Jérusalem et l'avait longuement marchandé pour pouvoir l'acquérir. À cause de la dépense, il regrettait un peu de n'avoir pu s'acheter une pioche de fer comme il l'espérait, mais il avait envie que son épouse soit élégante et bien coiffée. Il en allait de leur statut à tous les deux après tout.

Au moment de partir de la maison, Joris s'apprêtait à verrouiller la porte quand Marie retint son geste.

«&nbsp;N'est-ce pas à l'épouse de tenir les clefs de l'hostel&nbsp;?&nbsp;»

Il la lui remit de bonne grâce, avec une courbette outrée, en lui assurant qu'elle verrait bientôt son trousseau s'étoffer de celles de leurs réserves, coffres et celliers. Marie lui fit promesse de le lui rappeler, puis ajouta, d'un air plus solennel&nbsp;:

«&nbsp;Il va nous falloir bonne cave, car il se peut que nous ayons bientôt une bouche en plus à nourrir.

--- Que veux-tu dire&nbsp;?

--- Si je suis arrivée un peu en retard ce matin, ce n'est pas dû à un souci avec mes paquets. Cela fait un moment que les aliments ne me tiennent pas au matin. Ça et puis d'autres choses...&nbsp;» ❧

### Notes

Il m'a fallu un long moment pour me décider sur le titre et l'approche à donner à ce Qit'a. Mes difficultés étaient issues du fait qu'il s'agit là d'un sujet au cœur du large projet qu'est Hexagora. Ce n'est certes qu'une histoire ordinaire de personnes simples, dont la vie est marquée par leur rencontre et leur union, leur descendance à venir et leur subsistance par un travail manuel possiblement peu créatif. En gros, ce que j'imagine être l'aventure humaine vécue par pratiquement tous nos ancêtres depuis des milliers d'années. Nulle tragédie d'ampleur, aucun haut fait d'armes ni complexe projet politique. Des gens comme il s'en trouvait une écrasante majorité et dont nous ne percevons que très peu.

Quand on parle du Moyen Âge, on évoque les trois ordres&nbsp;: clérical, militaire et travailleur/paysan. À ce dernier, on colle parfois abusivement l'étiquette de Tiers-État, pourtant anachronique, dont l'indétermination contient toutefois en elle-même une indication quant à l'estime qui y est portée. Mais ce classement, pour commode qu'il soit afin de comprendre la société médiévale, quoiqu'hérité de l'imaginaire même des penseurs d'alors, fausse notre représentation mentale par l'impression d'égalité quantitative qu'il induit.

Les paysans, manouvriers, petits artisans formaient l'essentiel de la population, dans un ratio bien loin d'un tiers. Leur vie de labeur, simple et possiblement monotone de notre point de vue, constituait la règle. J'avais donc envie de manifester ce *commun* par un texte qui ne respecte pas trop le schéma quinaire avec sa nécessaire progression, ni le besoin de spectaculaire auquel, en tant qu'émetteur de culture, je dois parfois avoir recours pour revivifier des structures usées jusqu'à la trame. Je devais en outre bâtir sur des assises sablonneuses, cette majorité n'ayant que très exceptionnellement fait l'objet des attentions des littérateurs d'alors. Il m'a fallu extrapoler à partir de nombreuses sources matérielles, archéologiques, mais aussi ethnologiques, pour donner à voir quelque chose.

Pour l'anecdote, l'idée de cette histoire est venue de la découverte d'une charte (RRR 611) qui indiquait le règlement d'un conflit qui opposait le seigneur de Ramla aux frères de Saint-Jean de Jérusalem à propos d'une zone située entre les moulins de Mirabel et une terre «&nbsp;aux épines&nbsp;». J'ai estimé que ce serait un bon endroit où de jeunes mariés peu argentés pouvaient bâtir leurs espoirs de lendemains heureux.

### Références

Duby Georges, *Les trois ordres ou l'imaginaire du féodalisme*, Paris, Gallimard, 1978.

Riley-Smith Jonathan (ed.), Ellenblum Ronnie, Kedar Benjamin, Shagrir Iris, Gutgarts Anna, Edbury Peter, Phillips Jonathan et Bom Myra, *Revised Regesta Regni Hierosolymitani Database* http://crusades-regesta.com/ (consulté le 24/01/2018)

Serviens
--------

### Bourg Saint-Martin, parvis de l'église, fin de matinée du vendredi 16 février 1151

Le petit marché aux poissons finissant répandait ses odeurs de marée jusqu'à la porte de l'église devant laquelle Gosbert avait attaché sa monture. Il était trempé d'avoir chevauché dans le crachin et la brume matinale, ses vêtements alourdis de pluie gouttant sur le sol grossièrement empierré. Il regardait la femme face à lui, qui se tenait prudemment de l'autre côté du portail, abritée elle aussi sous les voussures, les bras croisés. Elle n'avait guère changé estima-t-il. Le visage mince était devenu maigre, les joues creusées par l'absence de quelques dents. Peut-être avait-elle un peu de neige dans ses cheveux noirs, mais elle les dissimulait sous un épais voile de chanvre. Elle le dévisageait de ses yeux bruns cernés, les lèvres pincées, les sourcils froncés. Il retint un sourire à la voir arborer ces mimiques familières. Elle ne semblait pourtant guère enchantée de le croiser. Sa voix grinça comme l'acier sur la roche.

«&nbsp;On a porté la nouvelle de ta mort voilà plusieurs Carêmes.&nbsp;»

Gosbert tira la capuche de sa chape de voyage, révélant son visage rond, le poil désormais mâtiné de gris. Il s'était fait coiffer avant de venir, raser quelques jours auparavant. Il s'efforça de rendre sa voix aimable.

«&nbsp;C'était rude voyage. Mais me voilà de retour.

--- Y'a plus rien ici pour toi, Gosbert. On a même dit des messes. C'est les miens qu'ont payé.&nbsp;»

Gosbert hocha la tête. Il avait appris la nouvelle avant même d'arriver au hameau où il était né et avait grandi&nbsp;: toute sa famille avait été emportée par une épidémie et les terres avaient été redistribuées à d'autres par le seigneur local.

«&nbsp;T'as survécu aux fièvres&nbsp;? Et les petiots&nbsp;? La Maheut, Liénart&nbsp;?

--- J'étais plus là-bas. Ton père m'avait chassée quand Liénart a été emporté, un peu avant sa quatrième année. Maheut a pas connu les Pâques après ton départ...&nbsp;»

Gosbert soupira. Son père avait toujours été un homme emporté et autoritaire, qui n'avait pas supporté de le voir partir comme soldat au lieu de travailler la terre comme eux tous. C'était bien de lui de se venger sur son épouse, une fois sans enfant.

Peironelle chercha un moment ses mots puis le fixa avec sévérité, un brin de colère animant sa voix.

«&nbsp;Tu veux quoi, Gosbert&nbsp;? Tu crois pas que t'as assez fait le mal&nbsp;?

--- Dis pas ça, je pensais qu'on serait mieux...

--- T'étais un rêveur, mon vieux m'avait prévenue. La mort te suit, Gosbert. T'as jamais rien apporté de bon dans ma vie. Ou dans celle de personne.&nbsp;»

Le sergent se crispa à entendre son épouse le morigéner ainsi. Il était las de son interminable voyage, de tous ces combats en Terre sainte pour revenir en un comté de Flandre dévasté par la guerre avec le Hainaut. Mois après mois, il avait repoussé l'échéance où il retournerait chez les siens. Il n'avait pas l'auréole de gloire de ses prédécesseurs en Palestine, ni même aucune richesse tangible, ayant tout perdu en beuveries et fêtes grossières, où il tentait d'oublier ses griefs contre la vie. Il avait espéré jouir du prestige de son périple, mais l'accueil était plus que glacial, inquiet. Il cherchait ses mots, gêné, et fut devancé par Peironelle.

«&nbsp;J'ai marié un autre homme, qui m'a fait un beau garçon. Il laboure sa terre pour les siens, au lieu de...&nbsp;»

Gosbert lui lança un regard courroucé, auquel elle répondit d'un air de défi, le menton levé. Elle était toujours aussi vive. Il baissa la tête, fit mine de laisser courir ses yeux au loin. Une nouvelle fois, ce fut elle qui brisa le silence.

«&nbsp;Tu comptes revenir ici&nbsp;?&nbsp;»

Gosbert haussa les épaules. C'était en effet ce qu'il escomptait. Abandonner pour de bon son épée pour la serpe et la houe. Oublier les neiges d'Anatolie et les flèches turques des déserts, les marches et les assauts dans le vent, la poussière. Ne plus subir la chaleur ou le froid, la faim et la soif. Vivre sans la peur au ventre. Troquer tout cela contre des récits pour les veillées, où les plus jeunes s'endormiraient, souriant des exploits de leur aîné. Par bravade, il montra sa sacoche de cuir où étaient lacés son casque et son haubergeon, à l'abri de l'humidité, fit claquer son fourreau contre sa cuisse.

«&nbsp;Y'a guère de labeur pour l'homme d'armes, par ici. Le sire comte va marier sa cadette au jeune Baudoin du Hainaut. La paix va revenir.&nbsp;»

Peironelle hocha la tête. Pour autant, ce n'était pas la réponse qu'elle attendait. Il regarda la petite place où les derniers étals se vidaient de leurs poissons. Quelques chats disputaient les abats à de gros rats juchés sur un tas d'immondices. Un charroi de bois de charpente tiré par deux bœufs passait, les essieux grinçants. Il revint à Peironelle, s'appuya contre le portail de pierre, secoua la tête.

«&nbsp;Je te veux pas le mal, la mère. Je passais juste comme ça. Personne m'a reconnu, t'auras qu'à dire que j'étais un cousin à ton premier mari qu'était avec lui en outremer.&nbsp;»

Elle lui accorda un visage plus amène, clairement soulagée de ne pas avoir à répondre d'une accusation de bigamie qui l'aurait menée droit au couvent, voire pire. Son nouvel époux était certes un homme peu violent, quoique dur à la tâche, mais il avait l'honneur chatouilleux et n'aurait pas apprécié de se voir traité de cocu. Gosbert fouilla dans son sac et en sortit une palme fanée, brisée en plusieurs endroits.

«&nbsp;Tiens, j'étais certes pas croisé de plein droit, mais ça vient quand même de là-bas, béni par les moines du Sépulcre le Christ.&nbsp;»

Elle hésita un moment, peu désireuse de se rapprocher de lui, méfiante jusque dans ses regards. Il osa un sourire qu'il espérait engageant et posa la feuille au sol entre eux deux. Il se secoua, se composa un visage qu'il pensait allègre.

«&nbsp;Bon, c'est pas tout ça, mais si je veux passer les portes de Lille avant la nuit, faut pas que je traîne.

--- Alors tu comptes pas rester dans le coin&nbsp;? Voir les tombes des tiens&nbsp;? Celles des petiots...&nbsp;»

Il secoua la tête, reprit son sac, le noua rapidement à l'arrière de son troussequin puis détacha ses rênes, faisant tourner sa monture. Peironelle ne put retenir un regard d'admiration devant le vaillant roncin à l'œil vif et à la croupe puissante. Il se hissa en selle, mit son cheval en place et le vit volter avec adresse. Qu'elle garde de lui un bon souvenir.

«&nbsp;Allez, que Dieu te garde la mère. On aurait pu...&nbsp;»

Sa voix mourut dans sa gorge, il remonta sa capuche et talonna sa bête, la lançant à petit trot sans se retourner. Derrière lui, Peironnelle se pencha lentement, ramassa la feuille de palme, serrant le poing autour de la tige à en faire blanchir ses phalanges tout en le regardant disparaître, une nouvelle fois.

### Bruges, cellier de l'hôpital de Saint-Jean, fin de matinée du vendredi 28 décembre 1156

Gosbert avait encore la bouche pâteuse et le crâne comme un tambour des excès de la veille, mais il s'efforçait de se composer un regard attentif. Devant lui se tenait Arnoul, le vétéran de la croisade qui lui avait obtenu un poste de sergent au service du seigneur de Bruges. Son visage massif, rond à force d'abus de chère ne laissait que peu de marge quant à l'interprétation de son humeur. Il frottait sa large panse en respirant bruyamment, sans dire un mot, la tête penchée.

«&nbsp;Je peux dire que j'ai eu pleintée de gredins qui ne valaient pas un pet d'âne, mais là...&nbsp;»

Il écarta ses deux larges mains, un air d'incompréhension totale naissant sur ses traits globulaires.

«&nbsp;Il me fait remembrance que tu avais en charge la porte de Gand jusqu'à la saint Thomas, c'est bien ça&nbsp;? Veiller aux hommes du guet, garantir le paiement des droits et coutumes, éventuellement prévenir tout brigandage. C'est bien là mission à toi confiée&nbsp;?&nbsp;»

Gosbert hocha la tête, au prix d'élancements qui se propagèrent de ses tempes jusqu'à ses reins. Arnoul interpella le petit Ligier, compère habituel des débordements, qui s'était pour l'instant prudemment tenu en retrait derrière Gosbert. Jeune sergent, il s'était attaché à son aîné et le servait comme un valet tout en participant à ses frasques les plus diverses.

«&nbsp;Dis-moi, garçon, as-tu connoissance d'autres choses demandées au sergent de guet&nbsp;?&nbsp;»

Ligier, qu'on surnommait Merle en raison de sa formidable aptitude à siffler, n'osa que timidement déclore son bec, et certes pas pour joyeusement s'exprimer. Encore alourdie par l'alcool, sa voix leur fit plutôt l'effet d'un croassement.

«&nbsp;Prendre fripons et menteurs&nbsp;?

--- Ah, oui, c'est vrai. Cela se peut. Vois-tu autre chose&nbsp;?&nbsp;»

Le jeune homme lançait des œillades désespérées à Gosbert, tordant ses doigts en se trémoussant. Il n'appréciait guère d'assister à l'humiliation de celui qu'il estimait comme un maître et ne voulait y participer que de mauvaise grâce. Arnoul lui accorda un sourire grimaçant, peut-être conciliant, puis se rapprocha de Gosbert.

«&nbsp;Rien d'autre que cela, de certes&nbsp;! Rien qui n'indique qu'il faille pisser sur la robe du chapelain de Saint-Jean&nbsp;!&nbsp;»

Voyant la mine déconfite que Gosbert arborait, il baissa d'un ton et une lueur d'amusement pétilla dans son regard.

«&nbsp;D'un autre côté, peut-être espérais-tu rincer les vomissures dont tu as orné l'entrée de leur chapelle&nbsp;? Cela ne saurait être tenu pour mauvais vouloir, en ce cas.&nbsp;»

Un large sourire fendait désormais son visage et, après avoir vérifié qu'ils étaient seuls, il invita ses compagnons à s'asseoir.

«&nbsp;Au rebours de moi, qui n'aime guère ces tonsurés exempts de tout, je ne te cache pas que ça ne fait pas rire au castel. Le chapelain a parlé de signes d'infamie, rien de moins&nbsp;! Poing percé, marque au fer rouge... Tu sais pourtant qu'ils sont fort appréciés ici. Avec leur bel hospice tout neuf, nul n'ira les contrarier&nbsp;! Ne peux-tu modérer tes veillées, compère&nbsp;?&nbsp;»

Il laissa le silence s'installer un moment, se grattant la tête tout en reniflant avec volupté les senteurs de vin et de moisissure.

«&nbsp;Las, il ne m'est plus possible de leur rappeler une nouvelle fois que tu as répondu à l'appel de Bernard[^122]. Ils sont bien trop échauffés. Mais j'ai peut-être ce qu'il te faut.&nbsp;»

Il rapprocha son tabouret, parlant d'une voix de conspirateur, au grand plaisir de Gosbert, dont le mal de crâne commençait à peine à refluer.

«&nbsp;Le sire comte Thierry remet les voiles pour Jérusalem. Il en a appelé à ses châtelains et bonnes cités pour lui fournir sergents et valets en quantité. Demande à te croiser en pénitence, proposant de t'amender en versant ton sang pour Christ.

--- Hum. Je suis trop vieux pour ça, j'en ai fini avec l'outremer.

--- Alors, prépare-toi à te faire marquer la couenne ou couper quelque oreille ou le nez. Je n'ai guère savoir des coutumes de clerc, mais ce me serait surprise s'ils ne te prenaient pas un bout ou l'autre.&nbsp;»

En finissant sa phrase, il eut un regard éloquent pour l'entrejambe de Gosbert. Celui-ci accusa le coup, essayant de penser malgré la mélasse dans laquelle il devait pousser la moindre réflexion.

«&nbsp;Un don à leur autel ne serait pas enclin à leur adoucir le cœur&nbsp;?

--- Ce ne serait que d'eux, je ne sais. Mais le sire châtelain en a plus qu'assez de tes errements, Gosbert. Quand ce n'est pas clerc que tu éclabousses de pissat, c'est un marchand que tu rudoies ou un quartier que tu éveilles avec tes paillardises. Il serait temps pour toi de faire oublier ton nom en cette cité.&nbsp;»

Gosbert opina lentement, accordant une maigre bourrade à son compagnon d'armes. Arnoul avait toujours fait de son mieux pour arrondir les angles à chaque incartade, faisant l'entremetteur pour payer les dégâts, proposer des excuses ou débrouiller une querelle. Mais il avait raison, la coupe était pleine et il n'y aurait pas longtemps avant qu'un notable demande à ce qu'on inflige une correction bien plus radicale qu'une simple marque d'infamie. Il se tourna vers Ligier, un air faussement goguenard peint sur la face.

«&nbsp;Dis donc gamin, tu as ouï plus qu'à ton tour mes aventures en terre du Christ. Te dirais-tu d'y aller écrire ta propre légende&nbsp;? La geste du Merle, que voilà joli conte à chanter aux veillées, qu'en dis-tu&nbsp;?&nbsp;»

L'éclat qui naquit alors dans les prunelles du jeune homme lui rappela douloureusement celui qui avait brillé dans les siennes moins de dix ans plus tôt. Au moins le gamin bénéficierait-il d'un vétéran qui l'affranchirait d'un certain nombre de périls. Il serra la main d'Arnoul avec chaleur.

«&nbsp;Puisse Dieu et tous les saints te bénir d'avoir ainsi croisé mon chemin, compère&nbsp;!

--- Je ne les en mercierai que plus, avec cierges et messes, quand tu auras déguerpi et arrêté de me causer tous ces ennuis&nbsp;» rétorqua l'autre avec un clin d'œil.

### Plaine de la Bocquée, après-midi du dimanche 29 septembre 1157

Accompagné de Merle chargé de sacs et de paniers, Gosbert déambulait dans le marché aux armées, au sein de ce qu'on nommait le quartier de la Madeleine. On trouvait un véritable camp de foire attaché à tous les déplacements, avec de quoi se restaurer, des fournitures diverses et même des commerces que l'Église réprouvait. Officiellement les femmes qui suivaient les troupes se consacraient à l'entretien du linge, mais on y rencontrait surtout des prostituées, plus ou moins organisées ou protégées. Il n'était pas rare que cela se fasse parfois sous l'égide de quelques clercs qui voyaient là un moindre mal, apte à éviter de bien plus terribles agissements quand les soudards en venaient à se soulager par la violence sur d'honnêtes épouses ou jeunes filles. Les soldats avaient l'usage de désigner ce vaste bordel de toile et de cabanes du nom de la réprouvée qui avait lavé les pieds du Christ.

Gosbert avait pris l'habitude de fréquenter un petit groupe de prostituées menées par Coleite, une blonde aux formes généreuses. Il savait que le signal du départ était prévu pour le lendemain et voulait s'assurer qu'elles suivraient. Il avait l'intention de leur proposer de les protéger en échange de tarifs plus accommodants pour ses hommes et lui. Il appréciait fort la routine et estimait que des présences féminines apportaient du calme dans les rangs. Du moins tant qu'il s'en trouvait en suffisance. Il fréquentait les armées depuis assez longtemps pour savoir que les prostituées voyaient également d'un œil favorable la possibilité d'avoir des défenseurs attitrés, pour peu que ceux-ci ne se montrent pas trop gourmands.

Sur ce dernier point, Gosbert était intransigeant, il réprimandait invariablement les hommes qui se laissaient aller à frapper trop souvent ou trop durement et n'avait jamais autorisé un soldat de son groupe à dépouiller une femme du quartier de la Madeleine. Ça lui avait valu quelques bagarres et une dent cassée, mais aussi une assez bonne réputation, qui équilibrait sa notoriété d'ivrogne braillard. Il se rencontrait dans les armées des âmes plus noires que la sienne.

Lorsqu'il arriva à la petite carriole du campement de Coleite, il ne s'y trouvait que Layla, brune maigrichonne au caractère emporté dont il appréciait avec mesure l'effronterie calculée. Elle était en train de nettoyer leurs écuelles du repas de midi en les frottant dans les cendres.

«&nbsp;Le bon jour Layla. Où sont donc tes compagnes parties&nbsp;?&nbsp;»

Elle répondit d'un haussement d'épaules, tempéré d'un sourire. Elle savait reconnaître un bon client.

«&nbsp;Il faudra te contenter de moi...

--- Je suis ici pour causer de la campagne à venir. Vous avez appris le départ&nbsp;? Viendrez-vous&nbsp;?&nbsp;»

Elle abandonna sa tâche, se leva en époussetant sa robe de la poussière grise qui s'y était accumulée. Malgré de beaux cheveux noirs, elle n'était guère attrayante, avec ses formes osseuses et son visage triste. Mais son regard était habité d'une flamme si vive qu'elle réchauffait le cœur des hommes qu'elle fixait.

«&nbsp;Pas toutes. Certaines n'aiment pas le nord et préfèrent rester dans le coin.

--- Et toi, tu en dis quoi&nbsp;?

--- Moi, je préfère être au parmi de soudards armés qui peuvent me garantir des bédouins. J'accompagnerai le chariot de la Coleite.&nbsp;»

Gosbert hocha la tête, satisfait.

«&nbsp;Si vous en êtes d'accord, on pourrait trouver accommodement pour que chacun ait ses aises...

--- Tu penses à quoi&nbsp;?

--- Eh bien, dès le camp à se monter, vous pourriez trouver ma bannière et installer vos affaires parmi nos cordes.

--- Le sire comte n'y trouvera rien à redire, ou son maréchal&nbsp;?

--- Tant que ça tient les hommes calmes, il n'y aura pas de souci.&nbsp;»

Layla opina. Elle n'avait jamais suivi de campagne d'ampleur où se trouvaient des milliers d'hommes comme ici. Des troupes étaient venues de partout&nbsp;: Palestine, Judée, Galilée, Outrejourdain... et même du comté de Tripoli. On disait qu'il s'en ajouterait d'autres d'Antioche voire d'un royaume arménien encore plus au nord. Jamais elle n'avait vu un si grand camp ni autant de soldats, de cavaliers, d'archers. Il y avait des enclos de chameaux bâtés de poutraisons pour du matériel de siège. Elle avait choisi de demeurer parmi eux, car, pour la première fois de sa vie, elle s'y sentait en sécurité. Dormir au sein même de la troupe offrirait encore davantage de garanties.

«&nbsp;Ça sera pas donné, ton offre, j'encrois&nbsp;?

--- Rien de trop gourmand. Vous pourriez rebattre vos prix quelque peu pour les gars. Et vous serez à moi sans débourser.

--- Rien que ça&nbsp;?

--- Ça vaut bien&nbsp;! Si ça se passe bien, il sera même possible de faire pot et feu commun. Ça fera moins de corvées à chacun et on en vivra que mieux. C'est normal d'accorder au baron du lieu les bénéfices de son titre, non&nbsp;?&nbsp;»

Elle lui accorda un rictus amer. Il n'était pas le pire des hommes, mais son cœur n'était guère généreux. Elle lui promit d'en parler aux autres filles et de retourner le voir d'ici la nuit pour s'entendre sur ce qui serait fait.

En repartant, Merle s'approcha de Gosbert. Il ne s'exprimait que peu en public, mais se montrait toujours curieux et empli de questionnements dès qu'ils se retrouvaient seuls. S'il portait l'épée et le casque du soldat, il agissait avant tout en valet, volontaire pour toutes les corvées et désireux de plaire à celui qu'il servait comme son maître. Gosbert avait fini par le considérer comme un animal familier, parfois avide d'attention, mais qui repayait au centuple les faveurs dont il avait été gratifié.

«&nbsp;À voir cette catin, on aurait dit que c'est d'elle que sera la faveur. Elle avait le menton bien fier.

--- Eh quoi, Merle, que lui reste-t-il sinon cela&nbsp;? Ne faisons pas grief à la femme d'avoir désir d'affirmer sa valeur. Je ne suis pas de ces crétins qui les dédaignent tant. Au pays, quand je rentrerai, il me faudra marier mes six filles et rien ne me plairait tant que de les savoir exigeantes sur leurs époux.&nbsp;»

Le gamin acquiesça. Il n'avait jamais eu la chance de rencontrer la famille du capitaine, mais il gardait le secret espoir de lui être présenté un jour. Parfois, les soirs de solitude, il rêvait qu'il finirait valet au service de l'une ou l'autre. Il en avait tant entendu parler au fil des ans qu'il lui semblait les connaître toutes, même s'il s'embrouillait sur les âges et les prénoms.

### Shayzar sur l'Oronte, camp du comte de Flandre, matinée du mardi 15 octobre 1157

La chaleur montait rapidement et les assaillants avaient déjà pas mal sué à préparer leur matériel. Gosbert allait et venait le long des deux énormes échelles dont il avait la responsabilité. Il avait revêtu son haubergeon, son baudrier d'arme et son casque, en plus du bouclier qui l'attendait au sol. Il vérifiait l'ordonnancement des soldats qu'il avait répartis de façon à avoir les hommes les plus intrépides et les plus lourds en tête.

Ils seraient tous suivis en retrait de quelques archers et arbalétriers ainsi que d'une poignée de frondeurs. Abrités derrière des mantelets légers, leur rôle était d'empêcher des tireurs ennemis de prendre place sur les défenses.

«&nbsp;Dès que ça cornera, on assaillira le pan de courtine que je vous ai désigné. Attention à ne pas glisser lors de la montée, on va être fort serrés.&nbsp;»

Il vérifia que ses têtes de colonne étaient correctement équipées. Il avait désigné deux auxiliaires pour chacun, portant des pavois afin de les garantir des flèches et projectiles lors de l'approche vers le mur. Ceux qui escaladeraient les barreaux les premiers étaient chargés de coincer le crochet de fer fixé au moyen d'une chaîne. Rien que le poids rendrait la tâche malaisée à qui voudrait faire tomber l'échelle, mais cela participait aussi à rassurer les assaillants qui devraient monter péniblement les quelques toises qui les mèneraient au chemin de ronde.

«&nbsp;Une fois arrivés, protégez-vous et tenez la place. N'avancez pas sans ordre&nbsp;! Il n'y a guère de soldat là-dedans et une fois regroupés, rien ne nous résistera&nbsp;!&nbsp;»

Un cor résonna depuis le centre du camp, bientôt repris par tous les musiciens qui propagèrent l'ordre bref.

«&nbsp;Formez les rangs&nbsp;!&nbsp;»

Rapidement, chacun trouva sa place, vérifia une dernière fois qu'il avait toutes ses affaires, remua les épaules pour éprouver et répartir le poids de son armure ou s'assura d'un geste que son baudrier était bien positionné. Gosbert attira à lui Merle, qui devait demeurer en arrière, ayant eu le bras brisé par une flèche lors des journées précédentes. Malgré la fièvre, il avait tenu à être avec eux jusqu'au départ de l'assaut. Il avait apporté plusieurs outres et versait de l'eau fraîche dans le gosier de qui demandait.

«&nbsp;Tu demeures ici tant qu'on a pas emporté le morceau, Merle, tu m'as entendu&nbsp;?

--- Oui, mestre Gosbert. Je ne viendrai qu'au cri de *Ville prise&nbsp;!*. Je vous porterai de l'eau.&nbsp;»

Gosbert lui serra l'épaule dans un sourire puis se tourna vers le centre du campement, attendant avec impatience. Il vit la bannière annoncer l'ordre avant même de percevoir les cors et hurla de sa voix la plus forte.

«&nbsp;Mouvez&nbsp;! Prenez l'échelle, les gars&nbsp;!&nbsp;»

Au son des instruments de musique, les deux colonnes s'avancèrent en parallèle, encadrées d'autres unités qui marchaient du même pas lourd sur la pente. Loin à l'ouest, les bricoles[^123] s'activaient, projetant des boulets sur la porte et ses fortifications afin d'y attirer le maximum de défenseurs. Des troupes de Tripoli y étaient disposées, en plus des farouches montagnards de Thoros l'Arménien, au cas où une brèche pourrait être ouverte. Néanmoins les barons comptaient surtout sur la forêt d'échelles confectionnées ces derniers jours pour emporter la victoire. Des informateurs leur avaient indiqué que les habitants avaient été surpris par l'assaut et n'avaient ni ressources militaires ni réserves de vivres pour un long siège.

Hurlant des encouragements sans regarder où il mettait les pieds, Gosbert trébucha à plusieurs reprises, entravé dans sa marche par son encombrant équipement. Il surveillait avec angoisse la section de muraille à laquelle il devait s'attaquer, craignant d'y voir apparaître des archers, voire de simples miliciens avec des paniers de pierres. Tous ses hommes arrivèrent néanmoins sains et saufs au pied des fortifications, au milieu de buissons d'épineux qu'ils piétinèrent avec entrain.

Autour d'eux, les autres groupes avaient aussi bien avancé et seuls ceux les plus à l'est semblaient en retard. Peut-être étaient-ils confrontés à une opposition depuis les hauteurs&nbsp;? Il ordonna le silence, conseillant à chacun de garder son souffle pour ce qui allait venir. Les yeux plissés, il attendait de voir le dernier signal. Il passa la langue sur ses lèvres sèches, regretta de ne pas avoir laissé Merle les suivre jusque là. Il apercevait le gamin en contrebas, à l'ombre du verger d'où ils avaient jailli.

L'appel des cors le fit sursauter et il prit une grande inspiration avant de hurler.

«&nbsp;Dieu le veult&nbsp;!&nbsp;»

L'ordre fut repris par les soldats tandis qu'ils levaient leurs échelles, en un grondement déferlant à l'assaut de l'enceinte. Le bois heurta la pierre et les sergents commencèrent l'ascension. Sans opposition, elle fut relativement rapide même si elle était laborieuse. Chacun devait porter plusieurs dizaines de kilos de matériel, entravé par une hast[^124] ou un écu lacé dans le dos. Il suffisait qu'un pied glissât sur un barreau et la colonne entière pouvait s'effondrer en un amas de corps douloureux. Gosbert avait donc bien insisté pour que chacun se garantisse à chaque instant, ne lâchant une main que lorsque les deux pieds étaient bien en place. Le nez collé au talon du précédent, chaque soldat se hissait peu à eu en une grappe humaine, jusqu'à pouvoir enjamber le parapet.

Gosbert était second sur une de ses échelles et lorsqu'il arriva sur la plateforme, ce fut pour voir qu'ils étaient dans une zone éloignée de tout accès, encadrés par d'autres soldats latins. À leurs pieds, la ville déroulait ses quartiers en un méandre de rues, de venelles et de places. Au nord, l'imposante enceinte du château écrasait la cité étendue à ses pieds, à peine égalée en hauteur par quelques tours de mosquées. À l'ouest, un chevalier avait fait monter sa bannière. Certainement dans l'intention d'aller libérer la porte, il se dirigeait vers des escaliers où se rassemblaient quelques miliciens derrière des boucliers. Voyant que nul secours ne descendait depuis la citadelle fermée, Gosbert sentit son cœur s'emballer. La ville serait tôt prise si les musulmans ne mettaient pas plus de vigueur à la défendre. De la voix et du geste, il encouragea ses hommes à avancer au plus vite. Il ne s'agissait pas de rester en retrait, si quelque pillage pouvait s'offrir à eux.

### Harim, camp du comte de Flandre, fin d'après-midi du mercredi 25 décembre 1157

L'ambiance était morose dans le camp qui se mettait en place péniblement sous les averses éparses. Les hommes n'avaient été guère enchantés de devoir abandonner le siège de la forteresse après avoir pris la ville de Shayzar. De plus, c'était pour aller s'attaquer à une autre citadelle, encore plus près des territoires de l'émir, qui risquait donc d'offrir une résistance bien plus farouche. Gosbert avait été volontaire pour aider à mettre en place la tente chapelle au centre de leur village de toile, afin de pouvoir demander une faveur au prêtre.

Mal soigné après sa blessure au bras, le petit Merle avait vu sa plaie s'infecter et, malgré son courage à se laisser couper par deux fois le membre, il avait finalement succombé après une longue agonie, apaisée du sirop d'opium que le capitaine avait pu dénicher. Il était mort sur le trajet et ils avaient enveloppé son corps dans un drap afin de lui trouver un endroit où l'inhumer correctement. Mais pour cela ils avaient besoin d'un clerc.

Gosbert n'était pas porté sur la religion, mais il estimait qu'il ne faisait pas bon voir sa dépouille disparaître sans qu'un prêtre y ait appliqué les rituels adaptés. C'était en cela qu'on était un humain et pas un animal. Sachant qu'il y avait là de hauts seigneurs ecclésiastiques, il espérait dénicher un chapelain ou un curé de seconde zone qui accepterait de se déplacer pour un simple sergent. Ils étaient bien assez nombreux pour venir leur fustiger les oreilles au moindre écart de conduite&nbsp;!

Lorsqu'il revint dans le périmètre attribué à son groupe, il constata avec soulagement que leurs toiles avaient été tendues entre les arbres et qu'un feu brûlait vivement, le temps de faire les braises pour le souper. Ils avaient encore pas mal de pois secs et du pain, au moins ils auraient le ventre plein d'un repas chaud pour la soirée. Les prostituées avaient leur propre installation, petit auvent agrémenté de branchages, près d'un muret de pierres grises. Il siffla pour attirer l'attention sur lui.

«&nbsp;Le père curé s'en vient pour qu'on mette en terre le Merle. Me faites pas honte et rincez vos faces et vos mains pour venir bénir la tombe du gamin.&nbsp;»

Layla, qui avait pris un peu d'ascendant sur ses compagnes maintenant que Coleite était partie, se rapprocha de lui. Elle avait passé pas mal de temps à s'occuper du blessé et lui avait témoigné une rare douceur, qu'on aurait pu croire étrangère à son caractère. Gosbert en avait peu à peu fait sa favorite, mais même si elle appréciait le changement de statut que cela lui conférait au sein des soldats, elle n'en était pas plus aimable avec lui. Elle n'hésitait guère à lui faire des remarques acerbes qu'aucun homme n'aurait osé concevoir et encore moins exprimer. Étrangement, il ne lui en tenait aucune rigueur.

«&nbsp;On a tressé une petite couronne de feuillages, les fleurs sont fort rares en ces temps.

--- Tu m'étonnes&nbsp;! On est le jour de la Noël&nbsp;!&nbsp;»

Il remarqua à peine que cela n'éveilla en Layla aucun intérêt. Il avait déjà noté à plusieurs reprises son manque de foi, sans qu'il s'en préoccupe. Peut-être était-elle d'une de ces sectes orientales, dont on dit qu'ils sont chrétiens, mais qui ont leurs coutumes à eux. L'essentiel était qu'elle n'était pas musulmane, arborant une croix discrète autour de son cou.

Un clerc habillé sobrement, la tonsure fatiguée et le visage couperosé se présenta à lui, ses vêtements liturgiques autour du bras. Il avait l'haleine avinée et une tenue à peine plus propre que les soldats. Mais il affichait son plus éclatant sourire, amputé de quelques dents quand il s'exprima, la voix pâteuse.

«&nbsp;C'est ici qu'il faut mettre en terre la Corneille&nbsp;?&nbsp;»

### Jérusalem, quartier de l'Ânerie, midi du vendredi 9 mai 1158

Gosbert et Raguenel dégustaient un ragoût de porc aux lentilles, confortablement installés à l'ombre d'un vénérable pistachier. Ils goûtaient avec un réel enthousiasme le plaisir d'une cuisine plus élaborée que celle des camps et n'échangèrent pas deux mots de tout le temps que leurs écuelles étaient pleines. Ils partageaient la table avec des marchands et des voyageurs, servis par une vieille femme et sa fille, aussi rébarbatives l'une que l'autre. Mais elles offraient de solides repas à un bon prix.

Cela faisait plusieurs semaines que la campagne était terminée et chacun était désormais rentré chez lui. Il ne demeurait que les soldats du comte de Flandre à patienter, installés pour le moment dans les faubourgs de la ville. Parmi eux, Gosbert et ses hommes attendaient de savoir ce qu'il allait advenir de leur petit groupe. Raguenel, qui servait un chevalier de Jérusalem, avait été envoyé justement pour transmettre les nouvelles instructions. Une fois le ventre plein, il entreprit de se curer les dents avec des échardes arrachées à un bâton le temps que Gosbert ait fini également.

La journée était ensoleillée et Jérusalem voyait retomber l'agitation des dernières semaines. Les pèlerins venus pour Pâques étaient repartis, ou proches de l'être, et on pouvait de nouveau déambuler sans trop de peine. Les marchés étaient plus calmes et on y mangeait mieux pour moins cher. Ceci durerait jusqu'aux célébrations de l'été, pour marquer l'anniversaire de la prise de la cité. C'était néanmoins un événement qui attirait moins les foules, même s'il était l'occasion de festivités aussi enthousiastes.

Voyant que Gosbert dégustait son vin en guise de digestif, Raguenel se décida à transmettre les ordres.

«&nbsp;Ta bande et toi, vous allez vous rendre à la Tour-Baudoin, au nord de la cité. il s'y trouve petite forteresse où vous pourrez prendre vos quartiers.

--- Ça veut dire qu'on est là pour un autre été, c'est ça&nbsp;?

--- Je ne saurais dire. D'ici la clôture[^125] il peut s'en passer des choses... La maladie du Soudan[^126] pourrait offrir belles occasions dont les barons ne veulent pas se priver.&nbsp;»

Gosbert regarda le fond de son gobelet vide et le reposa, l'air déçu.

«&nbsp;Les gars vont pas aimer s'éloigner de la cité.

--- T'as qu'à leur dire que vous paierez la provende bien moins cher là-bas qu'ici. Et c'est une région à raisin, vous crèverez pas de soif&nbsp;!&nbsp;»

Gosbert acquiesça lourdement, prit le pichet pour demander à ce qu'on le lui remplisse. Il se sentait d'humeur à faire durer le repas, voire à lancer les dés. Le voyant faire, Raguenel chercha autour d'eux, l'air surpris.

«&nbsp;Dis donc, il est où le Merle&nbsp;? À Césarée il ne s'éloignait guère plus que ton ombre.

--- Il est parti...

--- Il a dû être bien marri de devoir te quitter&nbsp;!

--- Pas tant, je l'ai envoyé avec mon butin chez les miens, porter message à mon épouse et mes filles. Elles l'accueilleront comme un prince, chargé comme il l'est de ma picorée&nbsp;!

--- Tu dois avoir grande fiance en lui, adoncques.

--- La meilleure&nbsp;! Jamais il ne m'a déçu. Il fera un bon valet pour mon aînée, j'en ai certeté. Six filles, ça crée du souci.&nbsp;» ❧

### Notes

Il est très difficile de se faire une idée du quotidien des hommes de guerre de l'époque des croisades. Il n'existe pas une façon de faire standard et les coutumes et usages étaient aussi variés que les unités. Beaucoup de choses évoquées dans ce texte sont des reconstructions bâties sur des informations postérieures ou fragmentaires. J'avais réalisé voilà de nombreuses années un petit fascicule qui tentait de regrouper des notes sur le sujet. J'ai eu l'occasion de l'exhumer et de le parcourir de nouveau et cela m'a donné l'envie d'y ajouter un peu de chair, de le rendre plus vivant. J'en ai profité pour en reprendre la présentation afin de le mettre à disposition de ceux qui seraient intéressés, en sachant qu'il n'y a là que quelques notes de lectures compilées.

Le titre de ce Qit'a vient du terme latin par lequel on appelait de façon générale les serviteurs au sein d'une maison, quelle que soit leur fonction&nbsp;: militaire, domestique ou agricole. Il exprime une notion de dépendance à un maître, à rattacher à l'ancien terme *serf* (dont la racine est la même). Cela a donné les termes sergents, servant, service...

### Références

France John, *Western warfare in the Age of the Crusades 1000-1300*, Ithaca, New York&nbsp;: Cornell University Press, 1999

Smail Raymond Charles, *Crusading Warfare, 1097-1193, Second edition with a new Bibliographical introduction by Christopher Marshall*, Cambridge&nbsp;: Cambridge University Press, 1995.

Verbruggen Jan Frans, *The art of warfare in Western Europe during the Middle Ages*, Woodbridge&nbsp;: Boydell press, 1997.

Ingénieuses bricoles
--------------------

### Jérusalem, boutique de Thomas le regrattier, début d'après-midi du vendredi 6 janvier 1139

Jeune homme d'à peine vingt ans, Baset aimait venir dans la boutique de son père lorsque celle-ci était fermée. Il trouvait toujours une excuse, de balayage ou de rangement quelconque, pour se retrouver dans ce petit monde qu'il connaissait si bien. Les jours d'ouverture, il y apportait les mesures, les paniers, rebouchait les jarres et tonneaux contenant le précieux sel gemme, accueillait les clients d'un salut poli. Il triait aussi régulièrement les aulx, oignons et échalotes qu'ils vendaient en grande quantité, pour en ôter les pourris et les desséchés.

Il n'aimait néanmoins guère faire le valet et porter des commandes, ce qui l'obligeait à quitter son nid. Il s'acquittait de toutes les tâches qu'on lui confiait avec enthousiasme, pourvu qu'elles le cantonnent à ces quelques pièces. Il avait appris à différencier les nombreuses variétés de sel à leur odeur, leur couleur, leur brillance. Et il était désormais aussi habile que son père à reconnaître du premier coup d'œil ce qui était de première qualité, la fleur, des fonds de marais. Il aimait par-dessus tout faire des tresses d'oignons, qu'il accrochait fièrement à l'éventaire dès l'ouverture.

Une fois de retour de la messe matinale, il avait rapidement troqué ses beaux vêtements pour sa vieille tenue de travail et avait entrepris de trier de petits oignons sauciers stockés dans des paniers en paille de seigle. Malgré le froid, il avait laissé la porte arrière de la boutique ouverte afin de bénéficier de la chiche lumière hivernale. Un des chats de la maison en avait profité pour venir s'installer vers lui, ondulant de la queue tout en faisant sa toilette. Il goûtait le calme de la présence du jeune homme, bien moins agité que sa sœur cadette Ascelote, toujours à le pourchasser pour lui infliger caresses et câlins qu'il subissait d'un air affligé.

Baset entendit donc ses deux frères entrer dans la cour, bavardant avec animation. Son aîné, Rufin, revenait de sa rencontre avec les jurés du métier de fabricant d'arbalètes. Il avait achevé sa formation depuis un moment et prévoyait de s'établir maître à son tour. Leur père était enthousiaste à l'idée de voir un de ses enfants devenir un artisan dans une profession prestigieuse. Il avait d'ailleurs promis de payer le prix d'entrée du métier, permettant à son fils de s'installer rapidement sans trop s'angoisser pour les premiers temps. Baset dédaigna un instant son travail pour les accueillir, impatient de savoir si les choses allaient se faire.

«&nbsp;Le mestre du métier est fort impatient de voir s'ouvrir nouvel atelier&nbsp;! Il y a tant à faire qu'ils n'arrivent à fournir et le sénéchal le roi s'en plaint fort&nbsp;! s'enthousiasmait Rufin.

--- Il a précisé qu'il avait déjà une commande à laquelle Ruffin pourrait prendre part, s'il avait ses approvisionnements et oustils à temps, précisa Gauguein, plein de ferveur.

--- Le père est allé se reposer un temps, mais a demandé à ce qu'on l'éveille si les nouvelles étaient bonnes, indiqua Baset, tout sourire.

--- Ce dimanche, il y aura quelque pratique au terrain près la porte Jehoshaphat. Tous les jurés y seront, et nous y conviendrons de la date de mon serment au métier&nbsp;!&nbsp;»

Bondissant de joie, Baset courut réveiller leurs parents, allongés dans la grande salle derrière les rideaux de leur lit. Installée à jouer à la poupée près de la table, Ascelote avait entendu le bruit et les avait déjà prévenus. Ils se levèrent rapidement et accueillirent les trois garçons avec enthousiasme. Pour une fois, Thomas se laissait aller à sourire et demanda à sa femme de leur apporter un pichet de vin. Il avait plutôt bien marié deux de ses filles, verrait un de ses fils tenir sa propre échoppe d'ici peu, avec un autre en bonne voie pour devenir un charpentier compétent. Et il ne l'avait pas encore dit, mais un de ses amis s'apprêtait à proposer Baset comme sergent du roi. Il ne lui resterait alors que sa petite dernière dont il faudrait s'occuper. Ascelote, dont les formes se dessinaient plus féminines chaque mois, pourrait trouver un honnête prétendant d'ici deux ou trois ans, malgré la faible dot qu'il avait pu lui constituer.

Parisete, leur mère, sortit quelques fruits secs et la fouace qu'elle avait achetée pour le repas de fête de la veillée, pour l'Épiphanie. Le moment en serait doublement heureux et elle augurait du bon de cette belle coïncidence. Il n'y avait pas eu autant de joie dans la maison depuis le mariage d'Heloys, trois années plus tôt&nbsp;: elle avait épousé un riche propriétaire dont on disait qu'il pourrait devenir intendant seigneurial du casal où ils vivaient désormais.

### Jérusalem, maison de Thomas le regrattier, soirée du mardi 17 avril 1145

La journée avait été longue pour Thomas. Après la frénésie de la Semaine sainte, il avait dû encore faire face à beaucoup de demandes. Beaucoup de pèlerins s'étaient munis de quelques provisions avant de repartir et certains visiteurs des casaux environnants avaient profité de leur venue dans la cité pour Pâques afin de faire des emplettes. Il n'avait donc guère chômé, désormais assisté seulement d'un petit valet qui n'était pas aussi vaillant à la tâche que Baset, malgré les réprimandes et les corrections qu'il lui infligeait.

C'était au moment de fermer qu'il avait vu arriver ses deux fils Rufin et Gauguein, dont il était sans nouvelle depuis plusieurs mois. Ils étaient partis travailler à la citadelle de Kérak, loin dans les déserts du sud, par-delà la rivière du Diable[^127] afin d'en améliorer les défenses. Rufin était entre autres un des responsables de l'approvisionnement des arsenaux et Gauguein l'avait rejoint pour l'assister dans ses réalisations les plus techniques ou les ouvrages les plus lourds. Leur visage avait gagné quelques rides et Rufin arborait une belle barbe brune qui renforçait son faciès d'ours.

Parisete leur fit un accueil chaleureux, même si elle maugréa de n'avoir pas le temps de leur préparer un repas à la hauteur. Elle envoya le valet prévenir Baset, à l'hôtel du roi, qu'il était convié à souper avec eux tous. Elle n'aimait guère sa bru Garsende, l'épouse de Baset, et estima inutile de l'inviter. D'autant qu'avec un bébé en bas âge, elle devait elle aussi assez peu apprécier de voir sa routine perturbée. Selon Parisete l'absence de son fils serait de moindre conséquence, pour peu que celui-ci sache faire montre de l'autorité qu'on attendait d'un chef de famille quand il rentrerait.

Thomas fit le service lui-même, claudicant autour de ses enfants comme une abeille affairée. Puis il avala quelques gorgées, savourant son bonheur.

«&nbsp;Alors, les garçons, quelles nouvelles du désert&nbsp;?

--- La citadelle aura bientôt belle allure, mais il s'en faut de beaucoup qu'elle soit achevée. On a sans cesse manque de bois d'œuvre.

--- On est de passage pour aller récupérer une cargaison venue de Tripoli. Elle nous attend à Jaffa, précisa Gauguein

--- C'est à vous que l'on confie pareille responsabilité&nbsp;?

--- Le mestre des bois est homme de grand savoir, mais il aime guère mulets et chameaux. Le Rufin avait besoin de fournitures, aussi.

--- Oui, on reste pas. Demain matin, dès les portes ouvertes, on reprend la route.&nbsp;»

Le père hocha la tête, satisfait. Il s'étonnait de voir son cadet prendre l'ascendant sur Rufin, qui était pourtant maître juré désormais. Mais tant que les deux frères s'entendaient bien, cela n'était pas de grande importance.

«&nbsp;À vous tanner pareillement les fesses, quand est-ce que vous trouverez femme pour me pondre nichée de bons enfançons&nbsp;? Nulle garcelette de bonne famille n'espère après vous en Outrejourdain&nbsp;?

--- Rien de la sorte&nbsp;! le détrompa Gauguein. D'autant qu'un de mes compères apprentis m'a fait belle proposition pour un autre chantier, ailleurs.

--- Vous ne vous plaisez pas là-bas&nbsp;?

--- On aimerait mieux être plus proche d'ici... Ce serait à Ibelin, la nouvelle forteresse du vieux Barisan, pour protéger des Égyptiens.&nbsp;»

Parisete retint un sourire. Elle avait vu avec regret ses deux fils aînés partir au loin dans des terres inconnues. Même si leur nouvelle situation était un peu plus risquée, de ce qu'elle en savait, au moins aurait-elle plus de chances de les croiser. Elle rêvait du jour où elle pourrait accompagner toute sa famille à une cérémonie d'importance, dans leurs belles tenues, afin de montrer à l\"envie combien ils avaient réussi. Elle prit place à table avec eux, posant un peu de pain et des olives, pensant à la robe qu'elle devrait peut-être se faire tailler pour une telle occasion.

### Ascalon, camp de siège chrétien, fin d'après-midi du jeudi 9 juillet 1153

Gauguein retrouva son frère dans le petit abri de palmes et de toile qu'ils s'étaient arrangé contre un talus sablonneux. Ils étaient dans les cordes du campement des hommes d'Ibelin, où ils participaient à l'entretien des matériels de combat, essentiellement armes de tirs et mantelets de protection. Le travail était harassant, car il fallait faire avec une permanente pénurie de bois. À peine un convoi arrivait-il que chaque atelier s'efforçait de se faire attribuer les plus belles pièces. Retailler depuis les membrures de navires et les charpentes de maisons démontées ne suffisait pas toujours à satisfaire la demande.

Rufin touillait une petite compotée d'oignons aux lentilles avec un air gourmand. Avec les années, sa silhouette s'empâtait de plus en plus. Travaillant la plupart du temps assis à façonner des arcs ou à ajuster des assemblages, il prenait chaque année un peu plus de ventre, contrairement à Gauguein qui déambulait sans cesse sur les chantiers. Même lorsqu'il avait achevé son ouvrage, il trouvait toujours à aller interroger les plus savants, questionner les plus inventifs. Avec le camp de siège, il était à la fête, admirant avec émerveillement les engins qui lançaient toutes sortes de projectiles, pierres et traits, ou les gigantesques tours d'assaut roulantes.

Il suspendit une outre de vin léger à leur cabane et se laissa tomber au sol, dénouant ses souliers pour se masser les pieds.

«&nbsp;je suis fourbu, j'ai couru jusqu'au camp de la porte sud ce jour d'hui.

--- Quelque nouvelle attaque en préparation&nbsp;?

--- Je ne sais. C'était juste pour faire connoissance d'un ermin[^128], fuyard du comté perdu. Fort savant en engins, et capable d'en remontrer en mathématiques à un clerc féru de comput[^129].

--- Tu lui voulais quoi&nbsp;?

--- J'ai ouï dire qu'il acceptait des valets. Je me disais que ce serait bonne occasion d'en apprendre plus.&nbsp;»

Ruffin soupira. Ils avaient une situation tranquille dans l'hôtel d'Ibelin, partageant un manse confortable, et avec la perspective d'avoir chacun le leur dès qu'ils auraient trouvé un parti convenable pour s'établir. Il désespérait parfois de constater combien son frère ne tenait pas en place. Il ne le dissuadait pas pour autant de tous ses projets, reconnaissant qu'il s'agissait souvent de décisions judicieuses. Il fallait juste que quelqu'un s'occupe de l'intendance de façon appropriée. Vu qu'il était l'aîné, il s'en attribuait la responsabilité, car il estimait qu'il était de son devoir de veiller sur son cadet.

«&nbsp;J'ai eu l'occasion de discuter un peu avec lui. Mestre Paylag est quelque peu rude d'abord, mais de savoir que j'ai un frère maître juré facteur d'arbalète l'a beaucoup intéressé. Lui est maçon, mais son père fabriquait des arcs, à la manière des Turcs. Tu devrais venir avec moi le rencontrer. Il te plairait.

--- Qu'as-tu donc en tête&nbsp;?

--- Rien de spécial. Je me disais juste que notre sire Hugues[^130] ne verrait pas d'un mauvais œil que nous sachions établir pierrières et bricoles[^131], mangonneaux et béliers. C'est ce savoir que nous pourrions apprendre de mestre Paylag.

--- Qui sert-il&nbsp;?

--- Il a suivi les Courtenay et appartient à l'hostel le roi désormais.&nbsp;»

Rufin grimaça. Avec la mort du vieux Barisan, la famille d'Ibelin avait été déchirée à cause du conflit entre Baudoin III et sa mère Mélisende. Au service de cette dernière, le connétable du royaume, Manassès de Hierges, avait épousé la veuve d'Ibelin et pris les armes contre le roi. Il avait été exilé l'année précédente, lors de la victoire du jeune prince et les Ibelin commençaient à rentrer un peu en grâce, avec Hugues, qui régnait désormais sur le fief. Mais les rancœurs n'étaient pas toutes éteintes. Il faudrait faire bien attention à ne froisser aucune délicate susceptibilité. Si les hommes de savoir étaient toujours prisés et appréciés, on ne les traitait souvent guère mieux que des destriers au sang vif&nbsp;: rênes courtes et mors serré.

### Jérusalem, citadelle de la tour de David, début d'après-midi du jeudi 29 août 1157

Sous la chaleur écrasante d'un soleil triomphant, hommes et bêtes recherchaient la moindre trace d'ombre. Profitant d'une courte sieste après un dîner frugal, les serviteurs étaient installés à l'abri du mur sud de la cour centrale blanche de poussière. Baset, encore vêtu de sa cotte rembourrée, avait le visage écarlate, trempé de sueur, malgré sa marche lente. Il tenait à la main son casque de cuir de crocodile, moins éprouvant et plus léger que les habituelles protections en métal, et se passait régulièrement un mouchoir sur le front.

Après avoir pris renseignement auprès de valets, il pénétra dans un des vastes dépôts aux puissants piliers, accueillant avec félicité la fraîcheur qui y demeurait tapie, même au plus fort de l'été. C'était là une des zones de l'arsenal, où certains engins de siège étaient entreposés entre deux campagnes. Il entendit et reconnut les voix qu'il cherchait avant de voir les visages de leurs propriétaires.

«&nbsp;La bonne journée, miens frères&nbsp;!

--- Jambe Dieu, c'est le gamin&nbsp;!&nbsp;» répondit d'un air enjoué son aîné Gauguein.

En l'apercevant, Rufin avança d'un pas lourd pour venir l'étreindre à la manière d'un ours, dont il avait désormais tous les attributs physiques. Il devenait aussi de moins en moins loquace, se contentant de hocher ou d'incliner la tête pour souligner les propos de Gauguein d'un grognement inarticulé. Baset leur fit une longue accolade, souriant largement de revoir enfin ses frères.

«&nbsp;Le Guillemot m'a dit que vous étiez là. Quel bon vent vous mène ici&nbsp;?

--- Le comte de Jaffa nous a mandé auprès son frère le roi pour préparer future campagne.

--- Il y a de la prise de cité ou de citadelle dans l'air&nbsp;?

--- Je ne sais au juste, mais on va être nombreux, avec la troupe du comte de Flandre. Peut-être que le connétable a ouï parler que Norradin est trop faible à nouveau pour protéger ses villes...&nbsp;»

Rufin haussa les épaules, frappant du plat de la main sur un lot de poutres qu'il était jusque-là en train d'inspecter.

«&nbsp;Tu vas en être&nbsp;? Tu as ouï parler de quelque chose&nbsp;?

--- Avec les branlements de terre, moult cités sont fragiles. Il y a belle picorée à faire au Nord dit-on...

--- Si seulement le roi des Grifons[^132] pouvait joindre l'ost... On dit que ses engingneurs sont parmi les plus habiles. Ils ont des machines de guerre d'une incroyable puissance, dont le savoir remonte aux Romains. Tu imagines&nbsp;?&nbsp;»

Baset hocha le menton sans conviction. Il n'avait jamais pu s'intéresser à la politique en dehors de la cité de Jérusalem et n'avait qu'une idée très vague de la géographie et des territoires au-delà de la Judée. Il n'aimait guère s'aventurer au loin et avait développé un réel talent pour se faire affecter à des tâches qui ne l'envoyaient que rarement par delà l'enceinte, et presque jamais sur les champs de bataille. Il avait d'ailleurs un peu perdu le contact avec ses parents depuis que ceux-ci avaient décidé de s'installer dans un petit manse des faubourgs d'Acre. Son père avait eu envie de retrouver les lieux de sa jeunesse et de profiter de la mer pour ses vieux jours.

«&nbsp;Vous viendrez prendre un pot de bière à l'hostel à la veillée, voire souper avec les miens&nbsp;? J'ai la place de vous loger si vous voulez. Les gamins seront contents de vous voir.

--- Ce sera avec plaisir, on comptait en fait te demander héberge dans les jours à venir. Ici ce n'est pas tant confortable, sans même parler des talents de cuisinière de ton espousée. Outre, tu as des nouvelles de la famille&nbsp;?

--- Rien de l'Heloys depuis long temps. Par contre, Pierre passe parfois ici, et me porte nouvelles de la Colete. Tout va bien pour eux. Je vois Ascelote qui-cy qui-là, elle vient souventes fois à l'hostel.&nbsp;»

Il baissa la voix, retrouva son habituel air morne.

«&nbsp;Elle vient conter ses malheurs à Garsende. Son époux...

--- Je l'avais dit au père que c'était male union&nbsp;! tonna Gauguein. Il faudrait y mettre bon ordre&nbsp;!

--- Le père veut pas en entendre parler. Il estime que la femme n'a pas à contredire son époux. Et puis ce sont riches négociants, plusieurs fois jurés du roi.

--- Tu parles, elle a eu le cadet, avec presque rien.&nbsp;»

Baset lui lança un regard courroucé. Lui aussi était le plus jeune des frères et il savait qu'il n'avait pas le statut de ses aînés, prestigieux ingénieurs au service du roi. Gauguein lui accorda un sourire désolé et le gratifia d'une bourrade.

«&nbsp;En tout cas, on passera volontiers boire un pichet avant le souper. Peut-être organiser beau repas de fête ce dimanche&nbsp;? On pourra apporter quelque belle volaille à se partager.&nbsp;»

### Acre, manse de Thomas le regrattier, début d'après-midi du mercredi 17 décembre 1158

Lorsque Baset vit le visage de sa mère, il comprit instantanément que les nouvelles étaient vraiment mauvaises. Il s'attendait au pire et entra dans la petite pièce sombre sans un mot. À son grand soulagement, son père était installé en bout de table, de la paille de seigle éparpillée à ses pieds, des corbeilles inachevées et ses outils pour en faire des paniers répandus autour de lui. Malgré son grand âge et ses doigts tordus, il n'avait pas perdu ses habitudes. Son sourire ressemblait à une grimace et sa voix coassante résonna sans joie.

«&nbsp;Prends un siège, Baset. La mère, verse-lui donc à boire, tu vois bien qu'il a chevauché au froid&nbsp;!&nbsp;»

Puis il acheva ce qu'il avait entamé, ne lançant plus un regard vers son fils tandis que ce dernier dégustait son verre de vin en combattant son angoisse. Face à lui, Parisete reprisait des torchons, la tête baissée. Elle aussi semblait avoir subitement vieilli. On ne voyait guère ses cheveux d'argent sous son voile, mais son visage était sillonné de rides, ses traits devenant austères avec le temps. Elle aurait pu être une des sévères sœurs de Saint-Jean, se dit Baset. Il risqua quelques regards autour de lui&nbsp;: un bon lit, deux coffres, une batterie de cuisine de qualité, deux niches au mur avec des plats décorés, un tonneau en perce. Ses parents n'étaient toujours pas aisés, mais ils semblaient bien vivre. Il hésitait chaque fois à proposer de l'aide à son père, trop fier pour accepter que quiconque lui porte secours, même sa propre famille.

Le vieux Thomas repoussa finalement son ouvrage, se débarrassa du linge qui lui protégeait les genoux et s'approcha de la table, indiquant d'un mouvement de tête à son épouse qu'il voulait aussi être servi de vin.

«&nbsp;Bon, je sais pas trop comment le dire, alors ce sera court. J'ai reçu mauvaises nouvelles&nbsp;: tes deux aînés ont connu male mort lors des campagnes le roi. Un de leurs compagnons me l'a fait savoir...

--- Quoi&nbsp;? Mais où ça&nbsp;? Comment&nbsp;?

--- Aucune idée, c'était pendant un assaut. J'ai pas su le détail. Toujours est-il qu'on les reverra pas.&nbsp;»

Lançant des regards de côté à Baset, il finit son verre lentement pour lui laisser le temps de digérer la nouvelle.

«&nbsp;Enfin bon, c'est dit. On y reviendra pas, c'est trop de douleur pour la mère. Tu le feras savoir aux filles.&nbsp;»

Puis il se leva, silhouette courbée par les ans, au pas traînant. Il alla remuer le feu, y déposa une bûchette.

«&nbsp;La mère, tu mettras du lard dans les potherbes[^133] de ce soir. Qu'on soigne notre gamin, avec toute cette route.

--- Tu peux demeurer quelques jours, fils&nbsp;? demanda Parisete, la voix étranglée.

--- Non, je dois repartir au plus tôt. Le vicomte m'a donné court congé. Je dois porter quelques messages ici au matin puis remonter en selle aussi vite que possible. Avec le roi au loin...&nbsp;»

Il n'acheva pas sa phrase, voyant les larmes monter aux yeux de sa mère. Il hésita un instant à lui prendre la main, mais n'avait jamais osé une telle familiarité avec ses parents. Il pinça les lèvres, frappa des phalanges sur la table et se leva, subitement fort empressé.

«&nbsp;Il me faut d'ailleurs panser ma pauvre bête, elle m'attend devant l'hostel.

--- Je vais te montrer où l'installer. Un voisin a de la place dans son courtil, un appentis où il loge son âne. Les bêtes aiment pas ça, demeurer seules.&nbsp;»

### Antioche, quartier de la porte Saint-Georges, soirée du mercredi 15 avril 1159

Stamatès plissa les yeux quand il pénétra dans le petit local au sol de terre battue. Des effluves d'oignons se mêlaient à l'odeur de sueur et de poussière des voyageurs. Il reconnut de nombreuses langues parmi le brouhaha des discussions et alla se renseigner un moment auprès du gros chauve qui s'affairait à activer les feux. Puis il vint se poser devant une table où deux hommes, l'un poilu et hirsute, l'autre mince et vêtu de la meilleure laine, avalaient un épais brouet où nageaient des tartines de pain. À son approche, ils levèrent la tête d'un air interrogateur.

«&nbsp;J'ai nom Stamatès Mpratos. Vous frères Celtes&nbsp;?&nbsp;»

Gauguein hocha la tête, invita le soldat à s'asseoir vers eux et fit un signe pour avoir un gobelet de plus.

«&nbsp;C'est bien nous. C'est toi qui va souventes fois au royaume de Jérusalem&nbsp;?

--- Allé une fois seulement, mais bons amis là-bas. J'ai usage de vos coutumes maintenant.

--- Tu pourrais porter des nouvelles à notre famille&nbsp;? On a fait écrire une lettre pour eux.

--- Sûr. Je demander à ami sergent le roi.

--- Ce serait parfait&nbsp;! Notre frère, Baset, est aussi de l'hostel le roi. Il n'a pas eu de nouvelles depuis fort longtemps. On a demandé à un ami qui retournait à Acre de dire à notre père qu'on allait devenir soldats grifons l'été dernier, mais depuis...&nbsp;»

Stamatès approuva, lui-même était un vétéran des campagnes et n'avait jamais fondé de famille. Il se consacrait à ses neveux et nièces, heureux de n'avoir pas trop d'attaches.

«&nbsp;Vous partir quand&nbsp;?

--- Aucune idée. Mestre Bonifacius a dit que nous ne serions vraiment membres de l'armée du... basileus[^134] qu'une fois que nous serons de votre Église. Nous avons déjà vu par trois fois le prêtre et je ne sais quand nous serons baptisés selon vos rites. D'ici là, nous devons demeurer ici.

--- Maintenant que basileus venu réprimander prince Renaud, plus tranquille ici. Profiter belle cité, ancienne.&nbsp;»

Gauguein acquiesça. Ils avaient passé les derniers mois à s'émerveiller de l'incroyable richesse des territoires septentrionaux et la rencontre avec les troupes de l'empereur byzantin les avait ébahis. Ils avaient découvert de nouveaux engins propulsant des boulets de pierre sans l'usage de cordes et de valet tirant dessus, dont le contrepoids était une huche emplie de terre et de gravats. Sa précision et sa puissance avaient arraché des gloussements de joie à Gauguein. De son côté Rufin avait particulièrement admiré sans un mot les armes à torsion, dont les délicats réglages l'intriguaient au plus haut point.

Les deux frères avaient rapidement sympathisé avec les techniciens malgré la barrière de la langue, mais jamais ils n'avaient pu en apprendre autant qu'ils le souhaitaient. C'était là une partie des nombreux secrets de guerre que les byzantins ne dévoilaient à personne. Du coup, quand Bonifacius, un des responsables, était venu leur dire qu'il était toujours possible de se proposer comme recrue, ils avaient sauté sur l'occasion. Il leur fallait abjurer leur foi catholique et devenir orthodoxes, puis se jurer à l'empereur byzantin, mais ils acceptèrent sans trop de regrets, emportés par l'enthousiasme.

Malgré la bonne entente entre les souverains, Gauguein s'était épuisé en plusieurs mois de tractations avec le maître ingénieur du roi pour obtenir un congé de leur service. Ils avaient réussi à plaider pour la valeur diplomatique de cette affectation et l'intérêt qu'il y aurait à apprendre de leurs alliés byzantins. Néanmoins, au fond de lui, il ne se sentait nullement concerné par les questions de loyauté ou d'appartenance religieuse. Il était en quête de savoir et il suivait les pistes qui lui semblaient les plus prometteuses. Rufin avait manifesté plus de scrupules, mais il avait fini par se laisser tenter par les formidables perspectives que cela lui ouvrait.

Rufin fit glisser la lettre au soldat byzantin et lui proposa de boire une autre tournée, à leur bonne fortune à tous. Quand Gauguein leva son gobelet, il arborait un sourire radieux.

«&nbsp;Si le père pouvait nous voir, il serait drôlement fier&nbsp;!&nbsp;» ❧

### Notes

On ne sait pas grand-chose de la réalité du quotidien des hommes responsables des engins de siège pour le XIIe siècle. Il semblerait que leur statut ait pu être très fluctuant et leur embauche variée. Si on se fie à des sources légèrement plus tardives, il n'y avait pas de corps de métier à proprement parler et les réputations s'attachaient volontiers aux individus. Les ingénieurs arméniens étaient toutefois plébiscités, en particulier pour les techniques de sape. L'art des engins de trait demeurait assez largement répandu, avec des spécificités régionales. Malgré tout, les Byzantins semblent avoir dominé la question, avec leur maîtrise de la pyrotechnie et des systèmes les plus imposants. Il est possible que ce soit dans leurs provinces que le lourd trébuchet à contrepoids ait été conçu, même s'il a été rapidement été copié et adapté par leurs adversaires et alliés.

Depuis quelques années, des chercheurs en histoire des techniques étudient de près l'existence de ces hommes de l'ombre, assaillants ou architectes des défenses. On peut citer le nom de Nicolas Prouteau, dont les travaux sur la question sont toujours du plus haut intérêt. L'auteur espère qu'à la date de sortie de ce *Qit'a* la publication de sa thèse dans une édition accessible ne saurait tarder.

### Références

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas&nbsp;: Institut Français de Damas, 1967.

Faucherre Nicolas, Mesqui Jean, Prouteau Nicolas, *La fortification au temps des croisades*, Rennes&nbsp;: Presses Universitaires de Rennes, 2004.

Rogers Randall, *Latin siege warfare in the twelfth century*, Oxford&nbsp;: Oxford University Press, 1997.

La croix et la bannière
-----------------------

### Vézelay, pâturages communs, après-midi du mardi 17 juin 1152

Quelques nuages cotonneux épars flottaient autour d'un chaud soleil annonçant l'été à venir. Au pied de la colline où le bourg et l'abbaye s'étendaient, assis le long d'une auge de bois où l'on faisait boire les bêtes, une dizaine de garçons se rafraîchissaient, rouges et haletants d'avoir trop couru. En contrebas, près de taillis et de ronciers abritant un ruisseau, tracées de pierres et de brindilles, on voyait encore les lignes qui délimitaient les camps de jeu, brouillées par les nombreuses parties de barres[^135].

Au-dessus des têtes résonna la cloche des moines, qui appelait ces derniers à la prière. Aucun n'y prit garde, indifférents qu'ils étaient au temps religieux, occupés de leurs importantes affaires d'enfants. Parmi les adolescents, un géant dominait tous les autres, avec un corps massif semblable à celui d'un bûcheron ou d'un portefaix. Il n'avait pourtant qu'un visage juvénile où le poil n'était que duvet, et sa voix oscillait malgré lui parfois entre graves et aigus. Sur ses vêtements gris de poussière, une traînée brune coulait depuis son menton, après les gorgées qu'il venait de prendre à même l'abreuvoir. En dépit de son essoufflement et de son teint rouge, son entrain ne paraissait guère entamé et il éclaboussait quiconque s'approchait de lui, le regard invitant à une bagarre amicale.

Ernaut aimait les jeux où sa taille et sa force étaient rudement éprouvés. Rien ne le remplissait plus de joie que de se confronter à plus âgé, plus retors ou plus nombreux que lui. Il lui fallait se mesurer à tous les adversaires et les vaincre avant de s'estimer satisfait. Bien que son père Ermont de la Treille soit un riche vigneron, il était généralement vêtu de vieilles cottes fatiguées, rapiécées et reprisées, sa famille ayant compris qu'il ne servait à rien de l'habiller de belle toile. Jamais il n'avait pris soin de ses affaires, se roulant dans la poussière ou la boue pour peu qu'on lui offre l'occasion d'un difficile affrontement.

Il n'était néanmoins pas belliqueux, simplement désireux de mesurer sa force et d'en garantir la suprématie face à tous. Il n'hésitait d'ailleurs pas à prêter main-forte à quiconque pouvait en avoir besoin, goûtant autant les compliments sur sa puissance que la victoire lors d'un jeu disputé. Il avait prévu pour la fin de journée de retrouver un de ses camarades afin de l'aider à s'acquitter d'une corvée d'épierrage. Malgré l'aisance de son père, Ernaut ne dédaignait pas les amitiés plus modestes comme avec Droin, même s'il se brouillait avec celui-ci aussi souvent qu'il se réconciliait. Il s'humecta une dernière fois la nuque et se rinça les mains avant de s'adresser au groupe.

«&nbsp;Ceux qui se sentent peuvent me compaigner jusque chez le Groin. Je lui ai fait promesse d'assistance, pour son vieux et lui.

--- Nous prends-tu donc pour serfs des bons frères&nbsp;? se railla un grand brun au visage rond.

--- Il s'agit juste d'aider compère à sa corvée...

--- Il suffit d'attendre et les tonsurés ne pourront plus exiger pareille redevance, d'ici peu.&nbsp;»

Arnaud était parmi les plus vieux de leur petit groupe et le compagnon habituel de bêtises d'Ernaut. Lui tirait une grande fierté de la richesse de son père Hugues et s'arrangeait souvent pour diriger les activités. Conscient du naturel dominant d'Ernaut, il usait de paroles astucieuses et de propositions insidieuses, laissant à son cousin annoncer de sa voix autoritaire ce qu'Arnaud lui avait soufflé par d'habiles suggestions. Et s'il aimait aller faire les pires diableries auprès des moines avec Droin, fils d'un des servants porchers, il était moins enthousiaste à l'idée de se fatiguer à accomplir une de ses obligations.

«&nbsp;Le comte Nevers saura bientôt nous libérer du joug des clercs, c'est mon père qui le dit.

--- Vous croyez qu'ils feront comme à l'époque de l'Ancien&nbsp;? Petiot, il me disait qu'ils avaient pendu l'abbé en leur temps&nbsp;! s'amusa un de leurs compagnons.

--- Crois-moi qu'il faudra bien à ces tonsurés demeurer en leur place et reconnaître qu'ils tirent de nous injustes exactions&nbsp;!&nbsp;» confirma Arnaud.

Tous lancèrent un regard vers le chantier des murailles de la ville, sur la partie ouest du relief. Depuis de nombreuses années, les habitants du bourg tentaient de s'affranchir de la tutelle exigeante de l'abbaye de la Madeleine. Ils avaient trouvé une oreille complaisante dans la famille du comte de Nevers, dont les appétits pour cette riche agglomération aiguisaient l'intérêt. Les villageois espéraient assouplir les prétentions fiscales de l'abbé et avoir un recours auprès de qui en appeler lorsqu'ils estimaient les jugements trop partiaux ou les impôts trop pesants, voire injustes. Parmi les nombreuses redevances contestées ou considérées trop lourdes depuis des décennies, le droit d'héberge, qui permettait aux moines de faire loger des pèlerins chez l'habitant était l'occasion de récriminations et de revendications qui dégénéraient parfois en affrontements ouverts.

«&nbsp;Que dirais-tu d'aller plutôt cueillir quelques framboises aux abords de la pâture aux moines&nbsp;? Le pâtre n'y est guère, en ce moment, les bêtes sont plus bas. On y trouve baies grosses comme le pouce&nbsp;!&nbsp;»

Avec une telle proposition, Arnaud était certain de convaincre Ernaut, dont la gourmandise le disputait au bon cœur. Assaisonnée d'un soupçon de licence, l'éventualité d'un pareil goûter était de nature à faire pencher la balance vers plus agréable activité. Il ne fallut guère plus de quelques instants pour que résonne l'appel du géant à sa troupe.

«&nbsp;Allez, compères, allons quérir monnaie de nos impôts&nbsp;! Sus aux framboises des tonsurés&nbsp;!&nbsp;»

### Vézelay, demeure d'Ermont de la Treille, soirée du lundi 28 juillet 1152

Comme chaque soir, le repas était pris en silence, chacun vidant son écuelle sans mot dire une fois le bénédicité récité. Ermont de la Treille mâchait difficilement, handicapé par ses dents usées ou manquantes. Il engloutit rapidement son potage, épaissi de pain, mais eut besoin de plus de temps pour manger les navets qui suivirent, agrémentés de lardons et d'une sauce au vin. Lorsqu'il vint finalement à bout de sa portion, il s'octroya un dernier verre avant de replier le canif qu'il gardait toujours sur lui. C'était le signal tacite du début des échanges que, privilège de l'âge, il entamait en premier. Il s'adressa à Seguin, son aîné, qui s'occupait de la plupart des affaires que le vieil Ermont dédaignait. En dehors de la vigne, il ne prenait guère plaisir à travailler la terre.

«&nbsp;On en est où, du grain&nbsp;?

--- J'ai déjà empli les deux grands silos et la petite réserve. Nous avons encore plusieurs sacs emplis, alors que nous n'avons pas encore fini battage et vannage. Nous aurons large provision, plus que nos besoins.

--- Il faudrait voir avec le vieux Gennon. Il a toujours de la place en ses greniers. Et il n'est point trop gourmand... As-tu prévenu le père Breton, à Autun&nbsp;? Il nous achète toujours bon prix.

--- Pas encore. Je ne sais s'il fera bon voir son grain par vaux et chemins avec les temps qui s'annoncent.&nbsp;»

Ermont opina lentement. La situation était en train de dégénérer entre les habitants du bourg et l'abbaye de la Madeleine, qui restait sourde à leurs demandes d'allégements fiscaux. Le comte de Nevers avait versé de l'huile sur le feu tout en se proposant comme arbitre, espérant par là établir une main-mise sur certains aspects de la vie de la cité. Le vieil homme se tourna vers son cadet, occupé à gober des boulettes de pain à l'autre bout de la table.

«&nbsp;Parlant de ça, Ernaut, j'ai ouï dire que tu avais été de ceux qui aboyaient avec les chiens de Nevers, ce tantôt.&nbsp;»

Le jeune homme acquiesça, circonspect. Son père n'était jamais bien autoritaire avec lui, mais il avait parfois le verbe cinglant. Et subir une brimade devant sa famille et les valets valait bien tous les châtiments corporels en terme de honte.

«&nbsp;Il serait sage de ne point trop te mêler de cela. Nul n'accorde l'eau à un moulin dont il n'espère tirer farine. Le comte est bien loin et la violence qu'il a déchaînée sur les terres de la Madeleine ne nous sera de guère d'utilité, à nous autres. Nous n'avons qu'un seul seigneur et maître et c'est le sire abbé. Nous devons apprendre à lui adresser nos doléances de la bonne façon. Laisse ces importantes questions aux doyens, nous saurons la régler.

--- Avez-vous eu l'occasion d'en parler avec l'oncle Robert, père&nbsp;? demanda Seguin.

--- Oui et non. En tant que curé de la paroisse Saint-Étienne, il est homme du bourg et n'a guère l'oreille du sire abbé Pons. Pour autant, il pense comme moi qu'il faut demeurer fidèles sujets, sans se risquer à outrages et violences, qui n'ont jamais rien engendré de bon.

--- Il n'a pas eu de nouvelles des discussions d'Auxerre, avec l'envoyé du Saint-Père&nbsp;?

--- Pas encore. Mais je n'en espère que peu, vu comme les envoyés de Nevers se comportent. Je crains un terrible été. Puissent nos vignes ne pas en souffrir&nbsp;!&nbsp;»

Il garda le silence un moment et se tourna vers Seguin.

«&nbsp;Fais donc porter le grain en trop ici, nous lui trouverons bien une place où le garder.&nbsp;»

### Vézelay, village de Saint-Pierre, demeure de Hugues, début d'après-midi du samedi 25 avril 1153

Hugues de Saint-Pierre était installé sur son banc préféré, sous un auvent accolé à sa maison, là où il entassait le bois prêt à brûler et où il s'adonnait à ses tâches à l'abri des intempéries, tout en admirant ses propriétés qui s'étiraient jusqu'aux rives de la Cure et même au-delà. Le visage rond, presque poupin malgré sa barbe grise et son teint rouge, il parlait d'une fois forte, où les r grondaient comme des rochers lancés sur des pentes. Ses larges mains posées sur les cuisses, il se voyait assez comme un seigneur en son fief, en dépit de sa roture et de l'aspect comique que le moindre vêtement de valeur lui donnait. Quels que soient ses efforts de toilette, il ressemblait immanquablement à un chapon se prenant pour un paon.

Ermont de la Treille ne venait que rarement lui faire visite, surtout depuis le décès d'Edelote, sœur handicapée de Hugues qu'Ermont avait épousée en secondes noces. Elle n'avait survécu à son unique enfant, Ernaut, que de quelques semaines. La relation entre les deux hommes, déjà peu amicale, n'avait pas été améliorée par leur chagrin.

Hugues se doutait qu'il allait être question de l'abbaye et des dernières nouvelles. Une sentence d'excommunication avait été prononcée par le pape à l'encontre du comte de Nevers et des habitants du bourg. Si le premier, important vassal à l'entregent nombreux, n'y voyait qu'une gêne à combattre avec l'appui de ses alliés ecclésiastiques, l'évêque d'Autun en tête, beaucoup de fidèles avaient été ébranlés par l'annonce, inquiets d'être mis au ban de leurs coreligionnaires. Sans même parler des menaces que cela faisait peser sur eux lors du moindre voyage.

La violence s'était donc déchaînée des deux côtés, les bourgeois répondant aux sentences et vexations légales par des coups de main et des pillages, orchestrés avec l'aide des hommes du comte de Nevers. Et tout le monde savait que leur âme damnée sur place, c'était Hugues de Saint-Pierre, qui soufflait sur les braises de la discorde avec un art consommé.

«&nbsp;Alors, Ermont, tu n'es pas venu jusqu'ici juste pour fleurir la tombe de ma sœur, n'est-ce pas&nbsp;?

--- Non pas. J'ai important sujet à évoquer avec toi.

--- Si tu viens porteur d'un message de l'abbé par ton frère, je suis prêt à l'entendre...

--- Il n'est question ni de l'un ni de l'autre. Je veux te parler d'Ernaut. Arrête de l'entraîner en tes affaires. Je n'ai nulle prétention à te sermonner sur toi et les tiens, mais laisse mon fils en dehors de cela.

--- Hé quoi, il est mien neveu, et seul fils de ma sœur préférée&nbsp;! S'il veut me joindre, je l'accueille avec plaisir, il est homme assez pour faire ses choix&nbsp;!&nbsp;»

Ermont serra les mâchoires et se contint, s'efforçant de tenir la colère au loin. Hugues cherchait la confrontation, comme chaque fois. Il était trop heureux d'avoir sa revanche sur son beau-frère, un de «&nbsp;ceux du bourg&nbsp;», qu'il méprisait si fort en parole tant il avait désir d'être semblable à eux. L'homme fort du moment se tourna vers Ermont, le visage étonnamment amical.

«&nbsp;Je ne veux pas te manquer de respect, Ermont, mais tu ne peux demeurer en retrait et espérer que les tiens fassent de même. Ernaut a compris que nous ne pouvons rester ainsi, à supporter vexation après vexation, sans jamais pouvoir en appeler à quiconque de la féroce autorité du père abbé. Joins-toi à nous.

--- Je ne saurais approuver les pillages et les sacrilèges envers un saint homme. Toutes ces batailles ne font qu'entraver nos chances de lui poser calmement nos demandes.

--- Les a-t-il jamais entendues&nbsp;? Depuis nos pères, qui ont occis le vieil Artaud, qu'avons-nous obtenu, à courber la tête&nbsp;? Les frères te cuisent-ils ton pain malgré l'interdit, avec toute ta déférence&nbsp;? Ils te traitent comme un impie, malgré toute ta bonne volonté.&nbsp;»

Ermont poussa un soupir. Il était las de ces querelles. Il se leva, serra l'avant-bras de Hugues en signe de salut et lui lança un regard désolé.

«&nbsp;Je ne crois pas en ces violences, Hugues. Arrête d'attirer mon fils en ces voies, l'ivraie ne donne jamais bonne moisson.&nbsp;»

Puis il s'éloigna d'un pas décidé, sans un regard en arrière.

### Vézelay, domaine de l'abbaye de la Madeleine, début de soirée du dimanche 10 avril 1155

Ernaut était assis dans un recoin des jardins, contre une pierre servant de base à un cabanon. Le souffle court, il sentait la fatigue et la lassitude gagner tout son corps. Ses doigts et ses bras étaient endoloris d'avoir frappé en tout sens, ses vêtements étaient déchirés, salis de terre et de poussière. Dans sa bouche dominait le goût métallique du sang, qu'il crachait mêlé de salive entre ses pieds. Il fixait devant lui une pierre, dont les reliefs saillants brillaient d'un vif écarlate.

Plus tôt dans la journée, il avait fait le mur de chez son père pour assister à un combat judiciaire. Avant même que la cérémonie ne commence, la foule s'était soulevée en masse contre les représentants de l'abbaye et les avait pourchassés jusqu'à les faire fuir à l'abri des fortifications monastiques. La cohue avait été sans nom, depuis le cimetière où le duel avait été prévu.

Avec son cousin Arnaud et quelques compères, Ernaut avait rejoint une bande qui avait obliqué vers les propriétés des moines, profitant du désordre pour se servir dans les réserves. Ernaut avait enfourné poulets et volailles dans un sac qu'il avait ensuite perdu dans l'excitation. Ils avaient brandi les pauvres animaux sous le nez des religieux, abrités derrière leurs murailles. Puis avaient décidé de s'attaquer à d'autres biens.

Il ne faisait pas bon être pris à partie par les meutes qui s'étaient alors mises à rôder et Ernaut espérait que les siens étaient saufs. On connaissait son père pour sa modération et il craignait qu'on ne lui en tienne rigueur. Au moins Ernaut pourrait faire valoir sa présence aux côtés des insurgés si la situation le demandait. Mais pour l'instant il n'en était plus là. Épuisé, il avait le regard rivé sur la pierre devant lui, incapable de faire un pas de plus, hébété de toute cette rage qui s'était emparée de lui.

Après avoir dévalisé une partie des réserves des moines, certains avaient pris la direction du sanctuaire et de ses objets précieux pour le mettre à sac. Ernaut et quelques autres avaient refusé de se joindre à eux et avaient préféré demeurer dans les celliers. Ils distribuaient tout qu'ils empoignaient, pillant par haine de leur maître plus que par amour des biens. Et lorsqu'ils voyaient quelques fidèles de l'abbé, ils le poursuivaient en vociférant, cherchant à l'acculer pour le bousculer, le passer à tabac, l'invectiver ou lui cracher dessus, selon les méfaits qu'on lui prêtait.

Puis une rumeur avait enflé&nbsp;: depuis leur abri les moines tiraient flèches et carreaux, vidaient leurs pots d'aisance sur les bourgeois à leur portée. Les plus modérés devinrent alors aussi enragés que les autres et les mains empoignèrent tout ce qui pouvait blesser, tuer. Les serpes et les houes se faisaient hasts pour quérir les droits qu'on ne voulait accorder. Des voix de plus en plus nombreuses s'élevaient, en appelant à couper la tête de l'abbé Pons. Et Ernaut avait suivi, les bras emplis de projectiles et le verbe haut, emporté par la conviction qu'ils obtiendraient leur liberté à la force de leurs poings.

Les derniers partisans des moines ayant rejoint l'enceinte fortifiée, il ne resta guère d'adversaires à empoigner et la colère retomba. Les corps fatigués de tant d'agitation, meurtris des affrontements, s'étaient faits lourds. Quelques hommes s'étaient proposés pour monter la garde alors que d'autres s'étaient esquivés pour aller rapiner ce qui pouvait encore être utile. Conscient que son absence avait dû être remarquée, Ernaut avait pris congé de ses compagnons de combat, partageant leurs fanfaronnades outrées. Puis il avait entamé le chemin du retour, traversant les jardins. Avec son groupe, ils étaient passés par là plus tôt dans l'après-midi, tandis qu'ils pourchassaient sans pitié leurs adversaires.

C'était là que l'attendait la pierre couverte de sang. Celle qui avait frappé le vieux Tous-Saint. Un des hommes de l'abbaye, un convers un peu simplet à la langue pendante, qui se signait chaque fois qu'on disait un prénom, en mémoire du saint qu'il associait à chaque personne. Le vieux Tous-Saint, qui faisait rire tous les enfants quand il trébuchait avec les bannières de l'église lors des processions. Le vieux Tous-Saint qu'il avait vu s'effondrer, le crâne défoncé par cette pierre, avant d'être piétiné par ses camarades.

### Vézelay, demeure d'Ermont de la Treille, matin du jeudi 24 novembre 1155

Ermont était en train de brosser un des chiens terriers qu'il employait dans ses vignes pour pourchasser les campagnols lorsque son frère Robert se présenta à la porte. La tonsure cachée par sa chape de pluie, il ne donnait guère l'impression d'être un prêtre. Grand et maigre, il avait l'élocution sifflante à cause de nombreuses dents manquantes et n'y voyait guère, ce qui l'obligeait à constamment plisser les yeux, en recherche des interlocuteurs auxquels il s'adressait.

«&nbsp;Entre donc, frère. Par cette pluie, tu vas attraper la mort. Veux-tu quelque gobelet de vin&nbsp;? J'ai en perce un de mes coteaux ouest...&nbsp;»

N'attendant pas la réponse, Ermont héla un des valets pour qu'on leur porte à boire et s'installa à la grande table qui emplissait la majeure partie de la pièce. Robert s'assit sur le banc, pliant sa chape avec cérémonie. On aurait dit qu'il était constamment en office, accordant soin et mesure à chacun de ses gestes. Une fois servis, ils burent tous deux en échangeant quelques banalités sur le temps et des nouvelles sans importance, avant que Robert ne se décide à aborder ce qui l'amenait là.

«&nbsp;J'ai pu parler de notre affaire avec les moines. Ils veulent faire condamner tous les fauteurs de trouble, sans exception. Le roi leur a donné franches coudées...&nbsp;»

Le visage d'Ermont s'allongea en entendant la mauvaise nouvelle. Il ne savait pas au juste ce qu'Ernaut avait fait, mais il l'avait sévèrement puni et consigné dans l'enceinte de la maison depuis des mois, occupé aux plus ingrates des tâches. Lorsque le roi était finalement intervenu, l'abbé Pons avait obtenu gain de cause, rétabli dans toutes ses prétentions, et il avait obtenu la condamnation de tous ses adversaires.

«&nbsp;Malgré tout, le père abbé n'est pas tant opiniâtre qu'on le dit. Il sait que tu as prêché la mesure et il n'est pas demeuré insensible à cela, je l'ai bien vu. Seulement des bruits courent sur ce qu'aurait fait le gamin...&nbsp;»

Ermont craignait qu'on ne mutile son fils, voire qu'on lui inflige une flétrissure, signe d'infamie&nbsp;: nez, langue ou oreilles tranchées ou visage marqué au fer rouge. Il savait que cela ferait de Ernaut un paria. Il était prêt à tout pour éviter cela, quitte à s'acquitter d'une très lourde amende. Il en avait soufflé l'idée à son frère.

«&nbsp;J'apense que le père abbé souhaite avant tout que le calme revienne. Cela ne se fera pas en accrochant à des fourches patibulaires tous les habitants du bourg. Il s'en prendra aux meneurs, de certes, mais cela n'est point le cas d'Ernaut, juste imbécile gamin. C'est qu'il ne passe guère inaperçu... Il se murmure qu'il a fait bien terribles choses.&nbsp;»

Robert prit la main de son frère et la tapota délicatement.

«&nbsp;Je ferai de mon mieux pour que rien de définitif ne soit entrepris à son encontre, mais je crains que la meilleure des solutions ne soit pour lui un exil volontaire, qu'on l'oublie ici.&nbsp;»

Ermont hocha la tête, abattu. Il lança un regard au fidèle petit chien lové à ses pieds, le désigna du menton à son frère.

«&nbsp;Sais-tu que c'est le dernier fils qui me reste de P'tite Reine, la chienne à Edelote&nbsp;?&nbsp;»

### Vézelay, demeure d'Ermont de la Treille, veillée du dimanche 25 décembre 1155

Malgré les circonstances difficiles, la demeure était animée de vie pour la célébration de Noël. Les enfants jouaient bruyamment, installés devant l'âtre où brûlait une bûche de grande taille, tandis que les plus âgés parlaient du bon temps à venir avec plus d'enthousiasme que les années précédentes. Avec la paix retrouvée, les affaires pourraient reprendre normalement, et nul n'aurait plus à s'inquiéter pour son bien. De nombreuses dissensions demeuraient, prêtes à renaître à la plus petite étincelle, mais pour l'instant, chacun s'accordait sur le plaisir qu'il y avait de jouir du fruit de son travail sans trop craindre du lendemain.

Ernaut était parmi les rares à ne pas partager ces moments de joie. Il était assis dans l'ombre, près de la porte du cellier, jouant sans entrain avec ses cousins qu'il faisait sauter sur ses genoux. Il n'avait pas prêté la moindre attention au babil des filles, au sol non loin de lui. Régulièrement, il lançait des regards vers Ermont, figure patriarcale confortablement installée sur un escabeau garni de coussins, une couverture sur les jambes. Un verre à la main, il hochait parfois le menton en réponse aux questions, mais n'allait guère au-delà des monosyllabes. Lui aussi avait autre chose en tête. Tous les visages convergèrent vers lui lorsqu'il toussa à plusieurs reprises.

«&nbsp;Je voulais profiter que nous soyons tous ici réunis pour vous faire annonce de plusieurs choses. Nous avons eu de longs échanges avec le père abbé, grâce à Robert, et notre maisonnée n'aura pas à souffrir d'infamie. Nul nom d'ici ne sera cité, et si un de mes fils l'est, on n'y lira que le fait qu'il s'agit d'un neveu de Robert, sans préciser outre.&nbsp;»

De nombreux visages exprimèrent leur soulagement en entendant cela, mais la voix en suspens du vieil homme les incitait à la prudence.

«&nbsp;Malgré tout, afin d'être absous de ses péchés, Ernaut devra pèleriner, demandant pardon de ses fautes et méditant sur les moyens de n'y pas retomber.&nbsp;»

Ermont marqua un temps. C'était là le genre de pénitence habituelle, par laquelle les clercs aimaient à mortifier leurs adversaires et personne ne s'en étonna. Il y avait pléthore de saints à qui demander pardon dans les environs, et il n'était jamais inutile d'éloigner les fauteurs de trouble un moment, en attendant que les choses rentrent dans l'ordre.

«&nbsp;Il devra demander rémission pour ses offenses sur le tombeau du Christ, à Jérusalem.&nbsp;»

Sans s'arrêter sur les visages ébahis de l'assemblée, Ermont enchaîna.

«&nbsp;Outre, une fois son vœu accompli, il serait bon qu'il profite des généreuses offres faites là-bas pour qui veut s'y établir. Dans le but d'éviter que des querelles ne puissent naître à l'avenir, le père abbé a demandé à ce qu'Ernaut ait la chance de vivre en la contrée où Christ a vécu.&nbsp;»

En de telles circonstances, une demande de l'abbé Pons équivalait à un ordre. Un silence stupéfié succéda donc à cette dernière phrase. Ce furent les enfants qui, les premiers, le rompirent, s'esclaffant de joie à l'idée que leur parent puisse aller en ce lieu empli de mystères et de légendes, au centre du monde. Seguin ramena le calme et, d'un geste, invita son père à terminer son annonce.

«&nbsp;Nous avons eu nouvelles de Lambert, qui ne trouve pas à s'établir comme tonnelier à Pontoise. Il a désir de compaigner son frère Ernaut jusque là bas et de s'établir lui aussi. Ils pourront ainsi se conforter l'un l'autre pendant le voyage et une fois installés.&nbsp;»

Le vieil homme était visiblement abattu et n'ajouta pas un mot. Il adressa un regard à la fois courroucé et ému vers son plus jeune fils, imposante masse de muscle ramassée sur son banc.

### Route du Vif (Châteauneuf-Val-de-Bargis), après-midi du jeudi 28 juin 1156

Après un rapide repas pris à proximité d'un ruisseau où ils avaient puisé de quoi se désaltérer, Ernaut et Lambert étaient repartis d'un bon pas. Si les deux frères partageaient vigueur et entrain dans leur marche, tout les opposait dans le caractère. Ils avaient quitté Vézelay quelques jours plus tôt et n'avaient cessé de se chamailler depuis. Ernaut avait fini par envisager ce périple comme une invitation à l'aventure, s'efforçant de manifester autant d'enthousiasme qu'il le pouvait, peut-être afin de se rassurer. Il avait l'habitude de voyager avec son père, jusqu'en Normandie parfois, et n'avait pas encore pleinement ressenti l'aspect définitif de ce départ.

Ernaut avait souvenir d'un frère, de plus de dix ans son aîné, assez sentencieux et réservé, mais il avait oublié un élément essentiel&nbsp;: Lambert était sincère dans ses inclinations sérieuses et dévotes. Le pèlerinage ne serait pas simplement de découverte et d'émerveillement, mais se devait d'être empli de haltes pieuses et de pratiques religieuses régulières. Le chant en faisait partie, fortifiant l'âme autant que le souffle selon Lambert, au grand désarroi d'Ernaut qui s'était jusque-là toujours contenté d'ânonner sans chercher à comprendre quoi que ce soit.

«&nbsp;Allons, Ernaut, l'oncle Robert t'a suffisamment répété ce beau psaume des montées, tu dois t'en souvenir&nbsp;!

--- Je n'ai guère l'oreille musicale, peut-être que si tu commençais, je pourrais suivre...&nbsp;»

Interrompu par le cri d'un geai saluant ces intrus pénétrant sur son territoire, Ernaut se contenta de sourire niaisement à son frère en guise de conclusion. Lambert, peu habitué à devoir morigéner et surveiller en permanence un jeune homme au caractère fantasque, soupira. Puis il prit une profonde inspiration, lançant le chant d'une voix claire et assurée, marquée en rythme par le bruit de son bâton au sol.

«&nbsp;*ad Dominum in tribulatione mea clamavi et exaudivit me*[^136]

--- Ah doux minou entre hiboux là scions, mais acclame vite et tout ce qu'on dit vite, mais...

--- *Domine libera animam meam a labio mendacii a lingua dolosa*

--- Deux minets libèrent âne...» ❧

### Notes

Il aura finalement fallu sept ans, depuis novembre 2011 pour que j'explique la raison qui a poussé Ernaut à traverser le monde. Je pense qu'il était assez clair qu'il n'était pas suffisamment dévot pour avoir pris la décision de pèleriner si loin de son propre chef. J'ai parsemé certains textes d'indices sur le fait qu'il avait été contraint à ce départ et avait un secret à l'origine de cela. Les plus curieux pourront s'amuser à faire la chasse ici et là, au détour de certaines descriptions ou remarques.

L'insurrection de Vézelay est la raison qui m'a fait choisir ce lieu comme origine d'Ernaut. Je voulais m'appuyer sur les tristes événements du tournant du siècle pour bâtir la jeunesse de mon héros. Si certains ont accès au *Bulletin de la Société des sciences historiques et naturelles de l'Yonne*, volume 2, de 1848, page 550, ils pourront y trouver la mention du neveu de Robert le chapelain parmi la liste des pilleurs des biens de l'abbaye, et en particulier des volailles. C'est ce genre de document juridique, d'apparence très austère, qui m'invite le plus souvent à échafauder des récits de vie. De leur lecture naît une société surprenante, foisonnante, des liens se tissent, des histoires se déploient.

Si je suis arrivé à vous rendre plus proche le destin de nos pas si lointains prédécesseurs, alimentant votre réflexion par ce rapport subjectif à l'histoire, à la mémoire et à nos constructions mentales, j'estimerai que mon but est atteint. Je dis parfois que j'écris de la science-fiction, car n'en déplaise à certains, l'histoire est une science. Je m'en nourris pour fabuler, mais avec une exigence de vérité dans le propos, dans les situations qui me font penser que mes lectures adolescentes de Isaac Asimov n'ont pas été sans effet sur ma façon d'aborder mon travail d'écrivain. Ayant récemment lu des récits d'Ursula le Guin, je me suis aperçu que je conservais en outre, comme elle, une attention particulière à ce qui constitue le véritable héros d'Hexagora&nbsp;: l'humain.

### Références

Cherest Aimé, *Vézelay, étude historique*, tome 1, Auxerre&nbsp;: Perriquet et Rouillé, 1848.

De Bastard D'estang Léon, «&nbsp;Recherches sur l'insurrection communale de Vézelay, au XIIe siècle&nbsp;», *Bibliothèque de l'École des chartes*, volume 12, Numéro 1, 1851, p. 339-365.

De Bastard D'estang Léon, «&nbsp;De la commune de Vézelay&nbsp;», *Bulletin de la Société des sciences historiques et naturelles de l'Yonne*, volume 2, 1848, page 527-570.

Jonquay Sylvestre, Müllers Fabian, *Les jeux au Moyen Âge*, Auvilliers en Gâtinais&nbsp;: Éditions La Muse, 2016.

```{=html}
<p class="qita_licence">
```
[❖ ❖ ❖]{.qita_scission}
```{=html}
</p>
```
```{=html}
<p class="qita_info_licence2">
```
Plus d'informations sur le cycle de textes courts dans le monde des enquêtes d'Ernaut de Jérusalem sur le site de la série Hexagora :`<br/>`{=html} `<a href="http://www.hexagora.fr">`{=html}http://www.hexagora.fr`</a>`{=html} / `<a href="http://www.ernautdejerusalem.com">`{=html}http://www.ernautdejerusalem.com`</a>`{=html}
```{=html}
</p>
```
```{=html}
<p class="qita_licence">
```
[❖ ❖ ❖]{.qita_scission}
```{=html}
</p>
```
```{=html}
<p class="qita_info_licence2">
```
Texte mis à disposition sous la licence
```{=html}
</p>
```
```{=html}
<p class="qita_info_licence2">
```
`<a href="https://creativecommons.org/licenses/by-sa/3.0/deed.fr">`{=html}Creative Commons BY - SA 3.0`</a>`{=html}
```{=html}
</p>
```
```{=html}
<p class="qita_info_licence2">
```
![](by-sa.png)
```{=html}
</p>
```
```{=html}
<dl class="qita_info_licence2">
```
```{=html}
<dt>
```
Attribution
```{=html}
</dt>
```
```{=html}
<dd>
```
-- Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Œuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Œuvre
```{=html}
</dd>
```
```{=html}
<dt>
```
Partage dans les Mêmes Conditions
```{=html}
</dt>
```
```{=html}
<dd>
```
-- Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Œuvre originale, vous devez diffuser l'Œuvre modifiée dans les même conditions, c'est-à-dire avec la même licence avec laquelle l'Œuvre originale a été diffusée
```{=html}
</dd>
```
```{=html}
</dl>
```

[^1]: Lundi 29 juin 1136.

[^2]: Terme désignant les Européens.

[^3]: Jeudi 28 décembre 1144.

[^4]: Vêtement à ouverture croisée sur le devant.

[^5]: Chapeau de feutre à fronton triangulaire, un des symboles du statut de guerrier chez les Turcs.

[^6]: Terme désignant les Européens.

[^7]: Métropolite Jacobite.

[^8]: Il est fait ici allusion à l'archevêque arménien d'Édesse, Jean.

[^9]: Bande de tissu décorée fixée au niveau du biceps.

[^10]: Communément appelé Zayn al-Din, il était un militaire réputé, commandant militaire zengide de Djéziré et seigneur d'Irbil.

[^11]: Nom arabe de la cité d'Édesse.

[^12]: Samedi 14 septembre 1146.

[^13]: Pâtisserie salée fourrée.

[^14]: Ensemble de mets servis dans un grand nombre de coupelles, offert lors de grandes occasions ou de banquets.

[^15]: Grande robe de dessus.

[^16]: Samedi 4 janvier 1147.

[^17]: Abu Mahmud Jum'a al-Numayri. Soldat et compagnon d'enfance de Usamah Ibn Munqidh.

[^18]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^19]: Nom arabe de Jérusalem.

[^20]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^21]: Ce qui fait environ 27 kg d'or (en ratls damascènes).

[^22]: Cité par Ibn Jobair, cet arbre marquait la frontière traditionnelle séparant les territoires chrétien et musulman, entre Damas et Baniyas/ Panéas.

[^23]: Administration royale des finances, sous l'autorité du sénéchal.

[^24]: Genre de loyer.

[^25]: Renaud de Châtillon (v. 1120-1187), prince d'Antioche de 1153 jusqu'à sa capture en 1160.

[^26]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^27]: Il s'agit de Thierry, comte d'Alsace, qui avait épousé en 1139 la demi-sœur de Baudoin III, Sybille d'Anjou.

[^28]: frontières.

[^29]: Région dominée par Damas.

[^30]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^31]: Terme arabe désignant l'appel à la prière, lancé par le muezzin depuis le minaret.

[^32]: Terme arabe&nbsp;: le lieu du pouvoir. On peut le traduire par cabinet, ministère ou office. Il a donné le mot français douane.

[^33]: Correspondant au 14 février du calendrier chrétien.

[^34]: Samedi 2 février 1157.

[^35]: Pièce de réception.

[^36]: «&nbsp;Aux yeux clairs&nbsp;».

[^37]: Chapeau de feutre à fronton triangulaire, un des symboles du statut de guerrier chez les Turcs.

[^38]: Vêtement à ouverture croisée sur le devant.

[^39]: Institution musulmane qui concède l'impôt d'un territoire à un officier de façon à ce qu'il entretienne ses troupes.

[^40]: intendant de la chancellerie

[^41]: Voir le Qit'a *Basses eaux*

[^42]: Cité par Ibn Jobair, cet arbre marquait la frontière traditionnelle séparant les territoires chrétien et musulman, entre Damas et Baniyas/ Panéas.

[^43]: Samedi 9 mars 1157.

[^44]: La principale porte occidentale de Damas.

[^45]: Bernard Vacher

[^46]: Le Caire, *al-Qahira* (la victorieuse). C'est aussi le nom donné à l'Égypte en général.

[^47]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^48]: Pietro Maureceno, prêtre et notaire du quartier vénitien autour de 1157.

[^49]: On se sert de deux matrices métalliques (une au-dessus, l'autre au dessous) pour frapper les pièces et y imprimer les motifs.

[^50]: Litt. *Mauvais voisin*

[^51]: Soit vers avril 1157.

[^52]: Aujourd'hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^53]: Médecin.

[^54]: Terme désignant l'office des douanes latin. Le terme vient du fait que c'était généralement de lui que dépendait l'abaissement ou la montée de la chaîne fermant le port.

[^55]: Mercredi 7 janvier 1159.

[^56]: Dynastie dirigeante de la cité de Damas dans la première moitié du XIIe siècle.

[^57]: Mu'īn ad-dīn Anur (ou Unur) (&nbsp;? - 1149), principal dirigeant de l'état bouride de Damas de 1135 à sa mort en 1149.

[^58]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^59]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^60]: Cordon servant à attacher les braies, sous vêtements.

[^61]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d'Amaury.

[^62]: Littéralement&nbsp;: *La mer de Loth*, nom arabe de la Mer Morte.

[^63]: Voir Qit'a *Poing de trop*.

[^64]: Aujourd'hui al-Karak, en Jordanie.

[^65]: Tribu et clan bédouins.

[^66]: Littéralement *Vallée de Moïse*. Dorénavant appelée Gaia, en Jordanie, proche du site archéologique de Pétra.

[^67]: Plat traditionnel à base de mouton, de lait de chèvre et de riz.

[^68]: Quartier de Kérak.

[^69]: Littéralement *Le propre*.

[^70]: Césarée, aujourd'hui *Qesarya*, sur la côte méditerranéenne d'Israël entre Tel Aviv et Haifa.

[^71]: Profession libérale jurée ayant pour rôle, contre rétribution, de mesurer les grandes quantité de grains vendues par les marchands.

[^72]: Amaury de Jérusalem, (1136-1174), comte de Jaffa et d'Ascalon puis roi de Jérusalem à partir de 1163.

[^73]: Cf. Qit'a *Épineuse couronne*

[^74]: Jeune chevalier n'ayant pas encore de fief.

[^75]: Aujourd'hui *Hébron*, ville palestinienne de Cisjordanie.

[^76]: Sergent du roi au statut élevé, assistant le vicomte.

[^77]: Fief constitué d'un revenu financier, plus facile à retirer à son bénéficiaire que des terres.

[^78]: Pantalon court de fine toile faisant office de sous-vêtement.

[^79]: Pantalon à large entrejambe, souvent serré au mollet.

[^80]: Grande robe de dessus.

[^81]: Petit bonnet porté sous les autres coiffes.

[^82]: Longue extrémité du turban.

[^83]: Pâtisserie à base de pâte, avec de la crème et des pistaches.

[^84]: Ou Lait de Damas, boisson à base de lait et d'eau de rose.

[^85]: Bagages.

[^86]: Boisson traditionnelle syrienne à base de lait fermenté.

[^87]: Bagages personnels d'un marchand, séparés de ses produits commerciaux scellés pour le voyage.

[^88]: Voir le Qit'a *Mal chancre*.

[^89]: Amaury de Jérusalem, (1136-1174), comte de Jaffa et d'Ascalon puis roi de Jérusalem à partir de 1163.

[^90]: En 1131

[^91]: Négociant en sel.

[^92]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^93]: Mu'īn ad-dīn Anur (ou Unur) (&nbsp;? - 1149), principal dirigeant de l'état bouride de Damas de 1135 à sa mort en 1149.

[^94]: Aujourd'hui *Hébron*, ville palestinienne de Cisjordanie.

[^95]: Charlemagne. Allusion à *La chanson de Roland*, qui célèbre, entre autres, les hauts faits de ce chevalier légendaire de l'empereur.

[^96]: Voir Qit'a *Branleterre*.

[^97]: Ascalon.

[^98]: Aujourd'hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^99]: « Maître », en l'occurrence, responsable d'un des secteurs du marché.

[^100]: Bosra, importante cité du sud de la Syrien, capitale de la région du Hauran.

[^101]: Le Caire, *al-Qahira* (la victorieuse). C'est aussi le nom donné à l'Égypte en général.

[^102]: Terme désignant un jeune homme, utilisé couramment pour désigner les esclaves mâles (souvent des militaires, mais pas seulement).

[^103]: Pièce de réception.

[^104]: Ville de Haute-Égypte par où transite beaucoup du commerce venu de Mer Rouge.

[^105]: *La victorieuse*, nom arabe du Caire.

[^106]: Impropriété dérivé du titre de sultant, que les Latins utilisaient souvent de façon générale pour désigner les princes musulmans.

[^107]: Aujourd'hui al-Wu'Aira, près de Petra en Jordanie.

[^108]: Nom donné à la Mer Morte.

[^109]: Forme médiévale de l'expression *potron-minet*, qui désigne l'aube. Le choix de ce terme ici est en hommage à Robert Merle, qui me le fit découvrir dans son excellente série *Fortune de France*.

[^110]: Arménien.

[^111]: Désigne les zones impropres à l'exploitation agricole.

[^112]: Nom arabo-musulman donné à saint Jean Baptiste.

[^113]: Notable d'origine indigène tenant lieu d'officier pour une agglomération composée de ses semblables.

[^114]: De l'arabe, *maître, vieillard, sage*. Titre honorifique pour un homme respecté pour son grand âge et ses connaissances, souvent religieuses.

[^115]: Toiles de tentes généralement utilisées pour les simples soldats.

[^116]: Formation de combat équestre.

[^117]: Saint Bernard, qui fut un des grands promoteurs de la Seconde croisade.

[^118]: Les Occidentaux avaient souvent une vision fantaisiste des souverains et territoires lointains. Il parle du calife fatimide du Caire.

[^119]: Mélisende de Jérusalem (1101-1161) fille de Baudoin II du Bourcq, second roi de Jérusalem.

[^120]: Désigne une travée par lesquelles les lits sont organisés dans l'Hôpital de Saint-Jean.

[^121]: Désigne une pâtisserie qui peut être mangée les jours maigres, comme durant le Carême.

[^122]: Allusion à la Seconde croisade, en 1148-49, où croisés français et allemands, Latins de Terre sainte, se rejetaient la faute de l'échec du siège de Damas.

[^123]: Engin de siège propulsant des pierres.

[^124]: Bâton, hampe. Désigne plus largement tout ce qui comporte un long manche, arme comme outil.

[^125]: La navigation s'arrêtait de façon traditionnelle l'hiver en Méditerrannée.

[^126]: Nūr ad-Dīn, fils et successeur de Zankī, homme d'état et chef de guerre d'origine turque (vers 1117/8 - 1174).

[^127]: Nom donné à la Mer Morte.

[^128]: Arménien.

[^129]: Calcul du calendrier, généralement religieux.

[^130]: Hugues d'Ibelin (? - 1170), seigneur d'Ibelin et de Rama.

[^131]: Engin de siège propulsant des pierres.

[^132]: Terme usuel pour désigner les Grecs, c'est à dire les Byzantins.

[^133]: Désigne les légumes que l'ont cuit au pot, en potage.

[^134]: Titre donné à l'empereur byzantin.

[^135]: Jeu de course et de capture, avec des camps délimités pour chaque équipe.

[^136]: Début du Psaume 119(120), premier Cantique des degrés, souvent chanté par les pèlerins.

