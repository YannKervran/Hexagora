\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}La main du destin}{1}{chapter.0.1}%
\contentsline {section}{\numberline {}Mossoul, citadelle des bords du Tigre, matin du youm~el~itnine 27~ramadan~530}{1}{section.0.1.1}%
\contentsline {section}{\numberline {}Édesse, grande salle de la citadelle, soir du youm~al~khemis 1~jadjab~539}{4}{section.0.1.2}%
\contentsline {section}{\numberline {}Qal'at Ja'bar, camp de siège, nuit du youm~al~sebt 5~rabi’~al-thani~541}{8}{section.0.1.3}%
\contentsline {section}{\numberline {}Damas, geôles de la citadelle, matin du youm~al~sebt 29~jadjab~541}{11}{section.0.1.4}%
\contentsline {section}{\numberline {}Naplouse, cour des bourgeois, fin de matinée du jeudi~25~août~1149}{14}{section.0.1.5}%
\contentsline {section}{\numberline {}Notes}{16}{section.0.1.6}%
\contentsline {section}{\numberline {}Références}{17}{section.0.1.7}%
\contentsline {chapter}{\numberline {2}Pour une poignée de dinars}{19}{chapter.0.2}%
\contentsline {section}{\numberline {}Damas, citadelle, début d'après-midi du jeudi~10~janvier~1157}{19}{section.0.2.1}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, matinée du lundi~14~janvier~1157}{22}{section.0.2.2}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, après-midi du mercredi~16~janvier~1157}{25}{section.0.2.3}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, soirée du mardi~18~novembre~1158}{28}{section.0.2.4}%
\contentsline {section}{\numberline {}Notes}{33}{section.0.2.5}%
\contentsline {section}{\numberline {}Références}{33}{section.0.2.6}%
\contentsline {chapter}{\numberline {3}Pour quelques dinars de plus}{35}{chapter.0.3}%
\contentsline {section}{\numberline {}Jérusalem, grand hôpital de Saint-Jean, soirée du samedi~19~janvier~1157}{35}{section.0.3.1}%
\contentsline {section}{\numberline {}Damas, demeure de Al-Zarqa', soirée du youm~al~sebt 19~dhu~al-hijjah~551}{39}{section.0.3.2}%
\contentsline {section}{\numberline {}Jérusalem, hôtel de Bernard Vacher, fin d'après-midi du mardi~26~février~1157}{42}{section.0.3.3}%
\contentsline {section}{\numberline {}Damas, demeure de Abu Malik, soirée du youm~al~sebt 25~muharram~552}{46}{section.0.3.4}%
\contentsline {section}{\numberline {}Notes}{49}{section.0.3.5}%
\contentsline {section}{\numberline {}Références}{49}{section.0.3.6}%
\contentsline {chapter}{\numberline {4}Impitoyable}{51}{chapter.0.4}%
\contentsline {section}{\numberline {}Jérusalem, cuisines du palais royal, matin du lundi~27~octobre~1158}{51}{section.0.4.1}%
\contentsline {section}{\numberline {}Tyr, cathédrale Sainte-Croix, après-midi du mardi~4~novembre~1158}{54}{section.0.4.2}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la demeure d'Abu Malik, début de soirée du mardi~18~novembre~1158}{56}{section.0.4.3}%
\contentsline {section}{\numberline {}Damas, citadelle, début d'après-midi du youm~al~arbia 15~dhu~al-hijjah~553}{59}{section.0.4.4}%
\contentsline {section}{\numberline {}Notes}{63}{section.0.4.5}%
\contentsline {section}{\numberline {}Références}{64}{section.0.4.6}%
\contentsline {chapter}{\numberline {5}Désert}{65}{chapter.0.5}%
\contentsline {section}{\numberline {}Sinaï, camp de bédouins, matin du lundi~22~septembre~1158}{65}{section.0.5.1}%
\contentsline {section}{\numberline {}Sinaï, camp de bédouins, après-midi du mercredi~8~octobre~1158}{70}{section.0.5.2}%
\contentsline {section}{\numberline {}Cité antique de Pétra, Wadi Sabra, soir du jeudi~23~octobre~1158}{73}{section.0.5.3}%
\contentsline {section}{\numberline {}Cité de Kérak, zone des jardins occidentaux, début d'après-midi du vendredi~31~octobre~1158}{75}{section.0.5.4}%
\contentsline {section}{\numberline {}Notes}{77}{section.0.5.5}%
\contentsline {section}{\numberline {}Références}{78}{section.0.5.6}%
\contentsline {chapter}{\numberline {6}Semper ad altum}{79}{chapter.0.6}%
\contentsline {section}{\numberline {}Ascalon, camp de siège, après-midi du samedi~15~août~1153}{79}{section.0.6.1}%
\contentsline {section}{\numberline {}Bethléem, parvis de la basilique de la Nativité, veillée du vendredi~24~décembre~1154}{82}{section.0.6.2}%
\contentsline {section}{\numberline {}Jérusalem, demeure d'Ameline d'Abraham, soirée du vendredi~15~juillet~1155}{85}{section.0.6.3}%
\contentsline {section}{\numberline {}Jérusalem, palais royal, début de matinée du mardi~26~mars~1157}{88}{section.0.6.4}%
\contentsline {section}{\numberline {}Notes}{90}{section.0.6.5}%
\contentsline {section}{\numberline {}Références}{91}{section.0.6.6}%
\contentsline {chapter}{\numberline {7}Deux vies}{93}{chapter.0.7}%
\contentsline {section}{\numberline {}Jérusalem, quartier Saint-Étienne, fin de matinée du jeudi~11~septembre~1147}{93}{section.0.7.1}%
\contentsline {section}{\numberline {}Jérusalem, église du Saint-Sépulcre, après-midi du dimanche~21~décembre~1147}{95}{section.0.7.2}%
\contentsline {section}{\numberline {}Jérusalem, demeure de Pierre Salomon, soirée du samedi~5~avril~1152}{98}{section.0.7.3}%
\contentsline {section}{\numberline {}Jérusalem, abords de l'église Saint-Étienne, matin du dimanche~27~septembre~1153}{100}{section.0.7.4}%
\contentsline {section}{\numberline {}Jérusalem, jardins de la porte Saint-Étienne, fin d'après-midi du jeudi~15~juillet~1154}{101}{section.0.7.5}%
\contentsline {section}{\numberline {}Jérusalem, rue de David, midi du vendredi~23~mai~1158}{103}{section.0.7.6}%
\contentsline {section}{\numberline {}Notes}{105}{section.0.7.7}%
\contentsline {section}{\numberline {}Références}{106}{section.0.7.8}%
\contentsline {chapter}{\numberline {8}Traditions}{107}{chapter.0.8}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la porte d'Hérode, matin du mardi~6~mars~1145}{107}{section.0.8.1}%
\contentsline {section}{\numberline {}Jérusalem, porte de David, fin d'après-midi du vendredi~25~mars~1149}{111}{section.0.8.2}%
\contentsline {section}{\numberline {}Jérusalem, Malquisinat, midi du mardi~10~juillet~1156}{114}{section.0.8.3}%
\contentsline {section}{\numberline {}Jérusalem, quartier de la porte d'Hérode, fin d'après-midi du lundi~16~décembre~1157}{116}{section.0.8.4}%
\contentsline {section}{\numberline {}Notes}{119}{section.0.8.5}%
\contentsline {section}{\numberline {}Références}{120}{section.0.8.6}%
\contentsline {chapter}{\numberline {9}En route}{121}{chapter.0.9}%
\contentsline {section}{\numberline {}Tyr, parvis de la cathédrale Sainte-Croix, fin de matinée du dimanche~18~février~1151}{121}{section.0.9.1}%
\contentsline {section}{\numberline {}Jérusalem, échoppe de Margue l'Allemande, matin du mardi~3~novembre~1153}{124}{section.0.9.2}%
\contentsline {section}{\numberline {}Jérusalem, quartier du palais royal, matin du lundi~16~novembre~1153}{127}{section.0.9.3}%
\contentsline {section}{\numberline {}Ascalon, abords de la porte de Jaffa, veillée du dimanche~13~décembre~1153}{129}{section.0.9.4}%
\contentsline {section}{\numberline {}Lydda, quartier de Samarie, matin du lundi~15~décembre~1158}{132}{section.0.9.5}%
\contentsline {section}{\numberline {}Notes}{134}{section.0.9.6}%
\contentsline {section}{\numberline {}Références}{135}{section.0.9.7}%
\contentsline {chapter}{\numberline {10}Semper fidelis}{137}{chapter.0.10}%
\contentsline {section}{\numberline {}Jérusalem, vallée du Hinnom, fin d'après-midi du mercredi~5~février~1130}{137}{section.0.10.1}%
\contentsline {section}{\numberline {}Casal Techua, fin de matinée du vendredi~15~septembre~1139}{140}{section.0.10.2}%
\contentsline {section}{\numberline {}Casal Techua, chemin méridional menant vers Hébron, midi du vendredi~15~septembre~1139}{143}{section.0.10.3}%
\contentsline {section}{\numberline {}Jérusalem, Saint-Sépulcre, matin du lundi~15~septembre~1152}{147}{section.0.10.4}%
\contentsline {section}{\numberline {}Notes}{149}{section.0.10.5}%
\contentsline {section}{\numberline {}Références}{150}{section.0.10.6}%
\contentsline {chapter}{\numberline {11}Forain}{151}{chapter.0.11}%
\contentsline {section}{\numberline {}Mezze, ouest de Damas, fin d'après-midi du lundi~12~août~1157}{151}{section.0.11.1}%
\contentsline {section}{\numberline {}Bait Jann, aube du vendredi~16~août~1157}{155}{section.0.11.2}%
\contentsline {section}{\numberline {}Baniyas, soirée du vendredi~16~août~1157}{158}{section.0.11.3}%
\contentsline {section}{\numberline {}Jérusalem, quartier Saint-Jean, midi du mercredi~4~septembre~1157}{163}{section.0.11.4}%
\contentsline {section}{\numberline {}Notes}{165}{section.0.11.5}%
\contentsline {section}{\numberline {}Références}{165}{section.0.11.6}%
\contentsline {chapter}{\numberline {12}Les trois frères}{167}{chapter.0.12}%
\contentsline {section}{\numberline {}Misr-Fustat, demeure de Ashraf ben Panoub, fin d'après-midi du mercredi~24~octobre~1156}{167}{section.0.12.1}%
\contentsline {section}{\numberline {}Naplouse, palais seigneurial, veillée du mardi~12~août~1158}{172}{section.0.12.2}%
\contentsline {section}{\numberline {}Ascalon, château comtal, matin du mardi~30~septembre~1158}{175}{section.0.12.3}%
\contentsline {section}{\numberline {}Notes}{179}{section.0.12.4}%
\contentsline {section}{\numberline {}Références}{180}{section.0.12.5}%
\contentsline {chapter}{\numberline {13}Caritas}{181}{chapter.0.13}%
\contentsline {section}{\numberline {}Calanson, halte caravanière, après-midi du mardi~13~mai~1158}{181}{section.0.13.1}%
\contentsline {section}{\numberline {}Beith-Gibelin, église paroissiale, fin d'après-midi du mercredi~12~novembre~1158}{185}{section.0.13.2}%
\contentsline {section}{\numberline {}Château Emmaüs, salle capitulaire des frères de Saint-Jean de Jérusalem, après-midi du dimanche~7~juin~1159}{188}{section.0.13.3}%
\contentsline {section}{\numberline {}Calanson, cellier des frères de Saint-Jean de Jérusalem, matin du jeudi~25~juin~1159}{192}{section.0.13.4}%
\contentsline {section}{\numberline {}Aqua-Bella, cellule de père Daniel, veillée du mercredi~18~novembre~1159}{195}{section.0.13.5}%
\contentsline {section}{\numberline {}Notes}{198}{section.0.13.6}%
\contentsline {section}{\numberline {}Références}{199}{section.0.13.7}%
\contentsline {chapter}{\numberline {14}Écueils}{201}{chapter.0.14}%
\contentsline {section}{\numberline {}Jaffa, bassin de la lune, fin de matinée du lundi~24~mars~1158}{201}{section.0.14.1}%
\contentsline {section}{\numberline {}Jaffa, porte orientale sur la route de Rama, début d'après-midi du lundi~24~mars~1158}{205}{section.0.14.2}%
\contentsline {section}{\numberline {}Jaffa, plages septentrionales, après-midi du lundi~24~mars~1158}{208}{section.0.14.3}%
\contentsline {section}{\numberline {}Jaffa, quartier pisan, veillée du lundi~24~mars~1158}{210}{section.0.14.4}%
\contentsline {section}{\numberline {}Notes}{212}{section.0.14.5}%
\contentsline {section}{\numberline {}Références}{213}{section.0.14.6}%
\contentsline {chapter}{\numberline {15}Vie commune}{215}{chapter.0.15}%
\contentsline {section}{\numberline {}Jérusalem, hôpital de Saint-Jean de Jérusalem, après-midi du mardi~18~décembre~1156}{215}{section.0.15.1}%
\contentsline {section}{\numberline {}Jérusalem, hôpital de Saint-Jean de Jérusalem, fin de matinée du vendredi~4~janvier~1157}{218}{section.0.15.2}%
\contentsline {section}{\numberline {}Jérusalem, Malquisinat, soirée du mercredi~27~mars~1157}{221}{section.0.15.3}%
\contentsline {section}{\numberline {}Clepsta, demeure de Girout la Cirière, fin d'après-midi du mercredi~12~février~1158}{224}{section.0.15.4}%
\contentsline {section}{\numberline {}Jérusalem, grand hôpital de Saint-Jean, soirée du lundi~3~mars~1158}{227}{section.0.15.5}%
\contentsline {section}{\numberline {}Mirabel, manse des Terres d'Épines, fin d'après-midi du mardi~24~juin~1158}{229}{section.0.15.6}%
\contentsline {section}{\numberline {}Notes}{232}{section.0.15.7}%
\contentsline {section}{\numberline {}Références}{233}{section.0.15.8}%
\contentsline {chapter}{\numberline {16}Serviens}{235}{chapter.0.16}%
\contentsline {section}{\numberline {}Bourg Saint-Martin, parvis de l'église, fin de matinée du vendredi~16~février~1151}{235}{section.0.16.1}%
\contentsline {section}{\numberline {}Bruges, cellier de l'hôpital de Saint-Jean, fin de matinée du vendredi~28~décembre~1156}{239}{section.0.16.2}%
\contentsline {section}{\numberline {}Plaine de la Bocquée, après-midi du dimanche~29~septembre~1157}{242}{section.0.16.3}%
\contentsline {section}{\numberline {}Shayzar sur l'Oronte, camp du comte de Flandre, matinée du mardi~15~octobre~1157}{246}{section.0.16.4}%
\contentsline {section}{\numberline {}Harim, camp du comte de Flandre, fin d'après-midi du mercredi~25~décembre~1157}{249}{section.0.16.5}%
\contentsline {section}{\numberline {}Jérusalem, quartier de l'Ânerie, midi du vendredi~9~mai~1158}{251}{section.0.16.6}%
\contentsline {section}{\numberline {}Notes}{253}{section.0.16.7}%
\contentsline {section}{\numberline {}Références}{254}{section.0.16.8}%
\contentsline {chapter}{\numberline {17}Ingénieuses bricoles}{255}{chapter.0.17}%
\contentsline {section}{\numberline {}Jérusalem, boutique de Thomas le regrattier, début d'après-midi du vendredi~6~janvier~1139}{255}{section.0.17.1}%
\contentsline {section}{\numberline {}Jérusalem, maison de Thomas le regrattier, soirée du mardi~17~avril~1145}{257}{section.0.17.2}%
\contentsline {section}{\numberline {}Ascalon, camp de siège chrétien, fin d'après-midi du jeudi~9~juillet~1153}{260}{section.0.17.3}%
\contentsline {section}{\numberline {}Jérusalem, citadelle de la tour de David, début d'après-midi du jeudi~29~août~1157}{262}{section.0.17.4}%
\contentsline {section}{\numberline {}Acre, manse de Thomas le regrattier, début d'après-midi du mercredi~17~décembre~1158}{264}{section.0.17.5}%
\contentsline {section}{\numberline {}Antioche, quartier de la porte Saint-Georges, soirée du mercredi~15~avril~1159}{266}{section.0.17.6}%
\contentsline {section}{\numberline {}Notes}{269}{section.0.17.7}%
\contentsline {section}{\numberline {}Références}{269}{section.0.17.8}%
\contentsline {chapter}{\numberline {18}La croix et la bannière}{271}{chapter.0.18}%
\contentsline {section}{\numberline {}Vézelay, pâturages communs, après-midi du mardi~17~juin~1152}{271}{section.0.18.1}%
\contentsline {section}{\numberline {}Vézelay, demeure d'Ermont de la Treille, soirée du lundi~28~juillet~1152}{274}{section.0.18.2}%
\contentsline {section}{\numberline {}Vézelay, village de Saint-Pierre, demeure de Hugues, début d'après-midi du samedi~25~avril~1153}{276}{section.0.18.3}%
\contentsline {section}{\numberline {}Vézelay, domaine de l'abbaye de la Madeleine, début de soirée du dimanche~10~avril~1155}{278}{section.0.18.4}%
\contentsline {section}{\numberline {}Vézelay, demeure d'Ermont de la Treille, matin du jeudi~24~novembre~1155}{280}{section.0.18.5}%
\contentsline {section}{\numberline {}Vézelay, demeure d'Ermont de la Treille, veillée du dimanche~25~décembre~1155}{282}{section.0.18.6}%
\contentsline {section}{\numberline {}Route du Vif (Châteauneuf-Val-de-Bargis), après-midi du jeudi~28~juin~1156}{284}{section.0.18.7}%
\contentsline {section}{\numberline {}Notes}{285}{section.0.18.8}%
\contentsline {section}{\numberline {}Références}{287}{section.0.18.9}%
\contentsfinish 
