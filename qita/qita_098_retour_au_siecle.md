---
date : 2019-12-15
abstract : 1160. Frère cellérier de Saint-Jean, Hémeri découvre qu’il va être père. Contraint de rapidement faire face à ses responsabilités, arrivera-t-il à concilier des engagements moraux contradictoires ?
characters : Hémeri, Tabitha, Frère Guillaume, Garin la Sonnaille
---

# Retour au siècle

## Calanson, cimetière paroissial des frères de Saint-Jean, après-midi du dimanche 3 avril 1160

Moite de la touffeur de cette chaude journée de printemps, Frère Hémeri s’était retiré dans un bosquet de pistachiers et de caroubiers qui avaient peu à peu colonisé l’aire sépulcrale. Au fil des années, les frères de Saint-Jean y avaient disposé quelques sièges, bancs rustiques et rochers plats, pour y profiter de l’air pur tout en goûtant l’ombre bienfaisante en été. Il était même arrivé que des réunions capitulaires s’y déroulent. Hémeri y avait pris ses habitudes après la collation de sixte. Il s’y installait pour une quasi-sieste, bercé par l’agitation du village dont il n’était séparé que par un mur en pierres sèches.

Des pas firent rouler le gravier aux abords de sa retraite, mais il s’abstint d’ouvrir les yeux, estimant qu’il devait s’agir d’une famille souhaitant se recueillir sur la tombe de quelque défunt. Peu après Pâques, il était fréquent qu’on vienne orner les croix de morceaux de rameaux bénis, espérant attirer sur les proches la bienveillance du Seigneur.

Il entendit le frottement des branches et des feuilles sur un vêtement, signe que la personne pénétrait dans le bosquet. Peut-être était-ce finalement un frère qui avait besoin d’accéder à quelque denrée… Il porta machinalement la main à son trousseau de clefs et ouvrit les paupières. Il sursauta car il ne s’attendait pas à découvrir la robuste silhouette de Tabitha, jeune fille des environs, fréquemment employée comme servante au sein du petit hôpital. Un rapide coup d’œil circulaire lui confirma que personne d’autre ne se trouvait près d’eux.

« Que viens-tu faire ici ? On ne doit ja nous voir ensemble en pareil endroit !

— J’en demande pardon, frère, mais… »

Sa voix s’éteignit, et son regard alla voguer au sol. Hémeri se leva, reculant instinctivement afin de conserver entre eux une distance garante de respectabilité. Les mains dans les manches, il se tordait les doigts sans plus parvenir à articuler pendant un long moment. En pleine lumière, habillée de sa plus belle tenue, elle lui apparaissait si féminine, toute en courbes et en vagues, les cheveux sagement maintenus par un foulard lâche qui dévoilaient une nuque sensuelle. Il se morigéna intérieurement de laisser son esprit divaguer à de telles pensées, à quelques mètres à peine du sanctuaire.

« Que me veux-tu ? Reviens me voir demain, au cellier, avec les autres. Il y aura de l’ouvrage. Avec tous les pèlerins de ces dernières semaines, les lessives et le raccommodage ne manquent guère.

— Je ne suis pas là pour obtenir labeur, c’est à propos de… nous. »

Hémeri tressaillit à l’écoute de ce mot terrible. Voilà qu’ils n’étaient plus deux personnes séparées, distinctes et sans rapport, mais une seule entité, une créature nouvelle, dont la simple évocation le faisait trembler d’effroi.

« Il n’y a rien de tel, nous avons juste… Ne te méprends pas, j’ai réelle amitié pour toi, mais il ne peut y avoir plus que ce que je t’offre. »

Tabitha hocha la tête tristement.

« Tout cela était bel et bon, et je ne saurais dire que vous n’avez pas été de grande générosité, mais il se trouve que les choses sont plus compliquées maintenant.

— En quoi ? T’ai-je trahi à un quelconque moment ? Ou te sens-tu flouée de nos accords ? Je ne peux te garantir quoi que ce soit, mais chaque fois que… Eh bien, à chaque fois, il y aura quelque chose en dédommagement. »

La jeune femme acquiesça, de nouveau en silence, cherchant péniblement en elle le courage de s’expliquer.

« C’est que mon ventre s’arrondit de belle façon, je ne saurais le cacher encore bien longtemps. »

L’annonce résonna en Hémeri comme la foudre, brûlant ses entrailles et broyant ses os. Il lança une main vers le tronc le plus proche pour s’y appuyer. Le monde autour de lui manqua de disparaître.

## Aqua Bella, cellier des frères de Saint-Jean, soirée du jeudi 28 avril 1160

La chiche lumière du soir en ce jour nuageux n’éclairait que les abords de la grande porte, laissant les cuves, barils, doliums et caisses dans les ténèbres. Dans une lampe à huile, une minuscule flammèche battue par une main invisible s’efforçait de ponctuer d’ambre les reliefs. Dans un recoin, deux moines s’étaient installés autour de la petite table de travail où s’étalaient échiquiers, jetons, balance et bâtons de taille. Au fatras habituel de frère Guillaume s’ajoutaient deux gobelets de terre exhalant de chaudes senteurs épicées. Les deux hommes goûtaient en silence leurs boissons, absorbés en eux-mêmes. Il fallut l’irruption d’un chat, ondoyant entre leurs jambes pour les ramener à la réalité.

« Tu n’es certes pas venu jusqu’en mes montagnes pour simplement parler de draps et de boisseaux de blé, Hémeri. Je vois bien une ombre gagner en toi. Non que tu aies jamais été fort jovial, mais un poids semble peser en ton âme. »

Hémeri avala longuement une gorgée, cherchant une contenance et du courage dans le fort breuvage. Il reposa le verre lentement, le faisant rouler entre ses doigts avant de se résoudre à répondre.

« J’ai grave souci, en effet. Je ne sais même pas comment en parler à mon confesseur…

— Quelque faute ? Péché véniel j’ose encroire !

— Bien terrible est le fardeau, et chaque jour en accroît le poids sur mes épaules.

— Conte-moi donc ce qu’il en est, nous portons plus à notre aise avec sochon à nos côtés.

— J’ai forte amitié pour vous, frère Guillaume, et j’ai souffrance à vous souiller ainsi de mes errements. Mais je ne vois nul autre qui puisse apporter lumière en cette noirceur. »

Il inspira un grand coup et parla avec vigueur, comme s’il cherchait à se délester au plus vite, s’arrêtant à peine pour reprendre son souffle entre deux phrases.

« Nous avons usage de recourir à quelques lavandières pour nos linges, comme de coutume, et j’ai succombé à la chair en les côtoyant. Il m’est arrivé, certaines fois, d’encontrer l’une d’elles dans nos resserres, et de m’y abandonner à bien terrible péché. Elle y était fort consentante, je ne l’ai jamais forcée, et j’ai veillé à la dédommager de ce bien funeste péché.

— Il n’y en a eu qu’une ?

— Oh oui, je ne saurais dire le pourquoi, si ce n’est qu’elle m’a séduit. De bien innocente façon, la faute est autant mienne que sienne. Nous avons succombé à la violence de la chair.

— Tu n’es par malheur pas le premier clerc à ainsi rompre ses vœux. Au moins n’as-tu pas ajouté à cette violation de ton serment quelque autre péché plus grave. Il va te falloir mettre fin à tout cela et recevoir de ton confesseur le châtiment approprié. Et l’enjoindre elle aussi à se purger de sa faute… »

Hémeri soupira.

« Si je m’en viens jusqu’ici, c’est que je ne le peux guère aisément. Il se trouve que la garcelette est grosse de moi. »

Frère Guillaume écarquilla les yeux, se reculant sur son tabouret, en un repli instinctif face au péché.

« Par la Croix-Dieu, que voilà funeste situation, mon frère…

— Vous êtes le seul en qui j’ai confiance, j’ai besoin de votre aide.

— Comme tu y vas ! Je veux de certes t’assister, mais je ne porterai le poids de tes choix. »

Il se tut un moment, étudiant son compagnon, s’accordant une petite lippée pour s’éclaircir les idées.

« De ce que je vois, tu as enfreint des promesses faites à Dieu et à tes frères, et tu as souillé cette jeune garce par tes manquements. J’ai souvenir que dans mes jouvences, il y avait un clerc, où je faisais mon noviciat, qui avait pareillement fauté. Il a fait en sorte que le fruit de ce manquement soit confié aux moines de Cluny. De ce que j’ai appris, le gamin est désormais prêtre de bonne fame. D’un mal peut naître un bien.

— Me conseillez-vous de faire don de cet enfant à venir à l’Église, en rémission de mes péchés ?

— Certes non, je me garde bien de donner conseil ! N’écoute d’ailleurs que distraitement ceux qui les dispensent de bon gré alors qu’ils ne connaissent pas les affres de tes tourments ! Ce que je veux te dire, c’est qu’il te faut prendre une juste décision pour que de tout cela quelque bien puisse advenir. Dieu a confiance en toi. Veille simplement à ne pas répandre le mal en justifiant un manquement par une nouvelle erreur. Tu as gravement failli, mais c’est là épreuve dont tu peux sortir grandi. »

Hémeri lança un regard perdu vers son compagnon. Il avait beaucoup espéré de cette entrevue, et elle n’apportait que plus de questions. Il sentait pourtant de façon confuse qu’il y voyait une lumière y naître. Avec l’aide de Dieu, de la prière, il arriverait à trouver en lui les ressources pour faire face à son malheur. Il lui fallait l’affronter sans aucun autre recours terrestre. Le premier vrai combat de sa vie.

## Calanson, cellier des frères de Saint-Jean, fin de veillée du jeudi 5 mai 1160

Hémeri sursauta lorsque le petit guichet s’ouvrit en grinçant puis, reconnaissant les formes de Tabitha, il l’invita d’un geste à entrer rapidement. La lanterne à plaques de corne qu’il avait prise avec lui ne permettait guère d’y voir, n’étant prévue que pour se repérer grossièrement quand on déambulait de nuit dans les bâtiments. Plus sûre que les simples lampes à huile, elle était aussi moins efficace.

Le frère de Saint-Jean se sentait mal à l’aise à l’idée de cette rencontre. Ils renouaient avec leurs usages clandestins, de se retrouver nuitamment, mais c’était désormais dans un but bien moins grisant que leurs étreintes fébriles. Depuis son retour d’Aqua Bella, Hémeri avait beaucoup réfléchi et il voulait livrer le fond de sa pensée à celle qui portait le fruit de leur union défendue.

« Personne ne t’a suivie ?

— J’ai été prudente ainsi que les fois précédentes. Le petit portail du verger puis au travers des jardins. Pas même un chat n’aurait pu me pister sans que je le voie.

— Parfait, parfait. Je t’ai demandé de me retrouver, car j’ai pris ma décision. »

À s’entendre aussi pontifiant, Hémeri réalisa qu’il alarmait la jeune fille. Elle le regardait d’un air qu’elle s’employait à rendre détaché, mais la peur se lisait sur son visage.

« Je ne peux te laisser seule affronter cette épreuve, mais je ne peux demeurer en cette maison, ayant profané mon statut de clerc de façon définitive. Je pense que nous pouvons aller dans ma famille, à Césarée. Je vais prendre de quoi faire le voyage aux frères et je les rembourserai après. »

le soulagement transfigura le visage buté de Tabitha, qui s’accorda même un timide sourire, les yeux emplis de reconnaissance.

« Que voulez-vous dire ? Vous allez m’abandonner aux vôtres ?

— Certes pas. Je ne serai pas le premier moine qui perd le froc. Il me semble que ce serait grande honte que de te laisser porter le poids de notre faute tout en continuant à profiter de la vie ici, à l’abri. Dieu m’a envoyé cette épreuve, je dois m’y confronter. »

Tabitha se rapprocha, rassérénée.

« Vous seriez prêt à tout quitter ? Pour moi ?

— Il faut boire le vin tiré. Il y a plus déshonnête labeur que de gagner le pain des siens à la sueur de son front. »

Émue de cette réaction, Tabitha esquissa un mouvement vers lui, mais s’arrêta en le voyant si raide, si guindé. Elle se contenta de lui prendre la main.

« Je vous pensais homme de bien, mais vous êtes encore plus que cela. Merci de…

— Nous savions tous deux que cela pouvait arriver. Dieu nous met à l’épreuve. »

La jeune femme avait espéré un peu plus de chaleur dans leurs échanges. Elle avait tout d’abord monnayé ses charmes contre quelques menus avantages, dont elle faisait profiter sa famille désargentée. Ses parents faisaient comme s’ils ne voyaient rien, trop impatients de partager cette manne pour se risquer à la perdre par des considérations morales. Mais avec le temps, elle avait trouvé Hémeri attachant. Au contraire des quelques autres auxquelles elle s’était vendue, il n’avait jamais été brutal. Empressé et maladroit, certes, mais par inexpérience et impétuosité. Elle s’était imaginé qu’il aurait pu développer des sentiments. Mais il n’agissait qu’en homme de devoir. Elle retint un soupir, trop heureuse déjà de n’avoir pas à gérer la situation seule. Elle estimait s’en sortir à bon compte.

« Mais que vais-je dire aux miens ? Si nous disparaissons de concert, ils vont comprendre. Grande honte va s’abattre sur eux !

— Feindront-ils l’étonnement  ? Alors même que tu leur portais beaux quartiers de viande ou sacs de grain sans commune mesure avec ton labeur en notre couvent ? Nous ne pouvons rester ici… »

Tabitha hocha la tête, elle sentait son cœur se comprimer à l’idée de quitter ce casal à jamais, aux côtés d’un homme qu’elle ne connaissait que fort peu. Elle n’était pas spécialement attachée à sa famille, qui n’avaient jamais cherché à la marier bien qu’elle allât sur ses seize ans. Pas assez jolie et trop pauvre devaient-ils estimer, ils privilégiaient leur fils aîné et ses deux sœurs cadettes. L’alternative à la fuite, finir servante de ses parents puis de son frère ne lui paraissait guère plus enviable. Au moins aurait-elle une chance de vivre quelques belles années.

## Césarée, hôtel de Garin la Sonnaille, veillée du mardi 21 juin 1160

Au travers des gobelets de verre clair, le nectar de Chypre tirait du soleil tardif des éclats rubis qui dansaient sur les murs. Garin jouait avec sa boisson, la humant et la respirant autant qu’il la goûtait. Il n’accordait que peu de valeur au fait qu’il l’avait payé une fortune ; il était vraiment amateur de bons crus et pouvait passer une veillée à savourer les différents arômes d’un cépage de qualité correctement vinifié. Il sentait juste que la soirée ne serait pas aussi agréable qu’escomptée, avec l’arrivée impromptue de son jeune frère Hémeri.

Celui-ci, face à lui, avalait son nectar comme une petite bière, l’air las, misérable et pathétique. S’il était un péché que Garin ne pouvait supporter, c’était bien l’acédie. Il s’adonnait à ses propres  vices avec une désinvolture qui lui rendait incompréhensible qu’on puisse en tirer la moindre tristesse.

« Si tu restes ainsi à te répandre en lamentations, je ne sais si je t’accorderai long temps avant de te retourner à la rue, mon frère !

— Je suis désolé de faire si grise mine alors que tu m’ouvres grandes tes portes en ces temps troublés.

— Que me chantes-tu là ? Tu as trouvé joli pot où tremper ta cuiller et garcelet va en jaillir, rose et poupon, prêt à s’activer pour tes vieux jours. Où vois-tu le mal ?

— J’avais prononcé quelques vœux, si tu en fais souvenance. Père et mère doivent me maudire…

— Ne préjuge donc pas des morts. Dieu seul sait. »

Garin fit une moue appréciatrice en avalant une petite gorgée, se laissant distraire un instant par le délicieux breuvage.

« Père t’a farci le crâne de ses idées, à faire de toi un clerc ! Mais quand il a fui au monastère, il pensait surtout à lui.

— Là, c’est toi qui médis des défunts.

— Certes non. Je souhaite que père soit heureux au Ciel, avec tous les Saints, mais il t’a montré bien lamentable exemple, à s’adonner en oraison à tout moment. Tu ne seras pas malheureux ici, crois-m’en ! »

Hémeri acquiesça, pitoyable. Il était encore couvert de la poussière du voyage, car il avait finalement fait la route à pieds avec Tabitha, n’emportant que quelques hardes qu’il avait prises dans les dons pour les pauvres. Un chaperon usé dissimulait tant bien que mal la tonsure cléricale. En chemin, lors des héberges, ils avaient prétendu être mari et femme, retournant chez eux après une visite faite à la famille. Arrivés à Césarée la veille, ils ne s’étaient présentés à Garin que ce midi.

Content de revoir son frère, celui-ci les avait accueillis sans problème, saluant Hémeri d’un sourire narquois en découvrant la jeune Tabitha, dont il déchiffrait les moindres courbes avec l’assurance d’un expert. Les questions n’étaient venues qu’à la veillée, dans le petit cabinet de l’étage qui ouvrait à l’ouest, laissant entrer les odeurs salines et le bruit des vagues.

Garin picora quelques amandes, chercha une position plus confortable sur ses coussins, puis rompit de nouveau le silence qui s’était installé avec la nuit qui se déversait depuis la rue.

« Il est malgré tout une affaire que tu ne dois pas surseoir. Il te faut l’autorisation de ton père abbé, ou évêque, ou ce genre de chose. Je ne saurais attirer mauvaise fame sur ma maison en donnant héberge à un défroqué en butte aux frères de Saint-Jean. Tu dois obtenir bon et licite consentement.

— Comment veux-tu que je fasse ? Je n’ai nul appui qui pourrait me permettre de demander pareille licence.

— Je vais faire écrire à Elainne. Elle s’y connait bien mieux en bondieuseries que nous autres. Si elle n’était si donzelle, j’aurais même cru à un moment qu’elle avait passé sa main sous la coule de son petit curé…

— Tu ne respectes donc rien, ni les clercs, ni ta propre sœur ?

— Les frères ont la réputation qu’ils méritent, il n’y a qu’à te voir bouffi d’orgueil alors que tu trempes au pot comme tout le monde ! Quant à Élainne, pour te dire ce que j’en pense : tu n’as jamais trouvé qu’elle ne ressemblait guère à père ? » ❧

## Notes

Cela faisait un moment que j’avais envie de parler du possible changement d’état d’un personnage adulte. L’idée en est née voilà des années, à la lecture d’une anecdote de la vie de Guillaume le Maréchal, relevée par Georges Duby dans sa biographie (pages 1081-1083 de son volume *Féodalité*). Mon souhait était à la base de montrer le défrocage, mais, s’il est évoqué dans les textes, il n’est jamais décrit en détail. J’ai donc dû abandonner cet espoir, et j’ai préféré me concentrer sur le cheminement intérieur du clerc devant affronter cette épreuve.

En outre, ce Qit’a est un de ceux  que je désire voir advenir depuis un moment, car il resserre des fils que j’ai placés au fil du temps. Il me fallait avoir un large éventail de personnages, de situations, de récits, pour avoir suffisamment de matière afin de tracer des motifs distants, mettre en valeur de subtiles jonctions esquissées ça et là. La toile de vie s’en trouve densifiée, réalisée au sens premier. J’espère ainsi permettre l’advenue d’un monde véritable, celui que j’ai choisi d’appeler Hexagora et que je vous invite à parcourir depuis quelques années.

Rq : on parle couramment du *siècle* pour désigner le lieu où vivent les laïcs, à opposer à l’espace régulier, *sous la loi d’une règle*, où sont cantonnés les religieux cloitrés.


## Références

Augé Isabelle, *Byzantins, Arméniens et Francs au temps de la croisade. Politique religieuse et reconquête en orient sous la dynastie des Comnène 1081-1185*, Paris : Geuthner, 2007.

Duby Georges, *Féodalité*, Paris : Gallimard, 1996.

Thibodeaux Jennifer D. *Negotiating Clerical Identities. Priests, Monks and Masculinity in the Middle Ages*, New York : Palgrave MacMillan, 2010.
