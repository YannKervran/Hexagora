---
date: 2014-02-15
abstract: 1158. Au centre du monde, Ernaut accomplit son devoir avec zèle et fait une rencontre inattendue. Là, dans un nouveau monde, où chacun espère se construire une nouvelle vie, il renoue avec son passé et envisage son avenir. Pourtant un lourd secret continue de peser sur son âme.
characters: Ernaut, Baset, Eudes Larchier, Stamatès Mpratos
---

# Eau de vie

## Jérusalem, porte de David, fin de matinée du mardi 15 avril 1158

Les éclats de voix faisaient se tourner les têtes. Parmi les voyageurs, on se félicitait de n’avoir pas affaire au géant qui gardait la porte. Les sergents, eux, s’amusaient de ce qu’Ernaut avait encore pris à partie un négociant qui tentait de jouer au plus fin. Peu connaisseur des hommes dont les familles importaient en ville et certainement pas enclin à se laisser adoucir par des menaces ou des promesses, le jeune homme ne laissait passer aucune occasion de faire du zèle.

Il s’entêtait d’autant plus dans ses investigations que l’on s’efforçait d’y résister. Sa dernière victime était un marchand d’allure orientale qui vitupérait, dans des grands froufrous de son ’aba[^aba] de lin coloré. Il ponctuait chaque phrase de grands effets de manche, pathétique papillon autour d’un bousier acharné.

« N’est-ce pas là toile de soie ? lança Ernaut, un grognement narquois s’échappant de sa gorge.

— De la soie ? Non, certes non ! Peut-être juste quelques fils en décor ! Mais assurément pas habits de soie !

— Pourquoi avoir plié ces draps au parmi de grossières étoffes de coton ?

— Je n’ai pas fait les paquets, j’ai valet pour ça.

— Et les valets en font à leur tête en votre négoce ? »

Le marchant se tut, vexé. Il pointait des lèvres comme une mégère ruminant une bonne nouvelle dans le voisinage. À l’écart, deux de ses serviteurs gardaient la tête basse, oscillant entre amusement et crainte des représailles. Ernaut pencha la tête vers une autre des balles, sur le second animal du train.

« Et là-dedans ? Vous allez encore annoncer bouses et étrons, rien de valeur sur lequel payer octroi ?

— Un peu de respect, sergent, vous savez à qui vous parlez ?

— Ouais, à une face de Carême, qui espère tromper la Secrète royale[^secrete], en pleine semaine sainte. J’ai l’air d’un agnel de six semaines ? »

Tout en parlant, il fit quelques pas vers son interlocuteur, gonflant la poitrine comme un taureau dans l’arène. Les veines saillant dans son cou battaient le sang qui empourprait sa face exaspérée. Il semblait à deux doigts d’abattre son poing massif dans le visage flasque face à lui. L’homme le comprit et fit mine de reculer. Il avoua, à contrecœur comme si c’était un secret d’état :

« J’ai là bonnes longueurs de papier d’Alexandrie, du khôl et de l’encens perse ainsi que quelques mesures de poivre fin.

— À la bonne heure ! Allez voir l’office du péage pour payer ce que vous devez, et vous pourrez entrer. Bienvenue en la sainte Cité, voyageur. »

Sa dernière phrase sonna étrangement aux oreilles du négociant, et il crut y percevoir une menace. Si tous les hommes au service du roi de Jérusalem étaient comme ce Samson à face de lion, il n’était pas certain de souhaiter demeurer bien longtemps. Il aurait tendance à toujours regarder par-dessus son épaule, craignant de le voir surveiller la moindre de ses actions, le plus petit de ses négoces. Il obtempéra en silence, trop heureux de s’en sortir sans avoir reçu un coup, et un peu déstabilisé par l’apparente honnêteté de son interlocuteur. La plupart du temps, les fonctionnaires tâtillons se calmaient bien vite avec quelques monnaies, ou un échantillon des marchandises. Mais là, bizarrement, ce sergent se contentait de bien faire son travail. Quelle étrange façon de voir les choses, songea le marchand, inquiet de voir le monde changer de façon si inquiétante.

Le jeune homme sourit d’un air satisfait lorsqu’il vit le fraudeur se diriger vers le bâtiment des douanes. Il prit une grande inspiration et se tourna vers l’entrée, prêt à fondre sur une nouvelle victime. Et se trouva face à un pèlerin, l’air narquois, qui le toisait sans peur, les pieds plantés au sol, une main sur la hanche. Un air de défi flottait sur son regard et aucune crainte ne semblait l’effleurer face à la masse de muscles frémissants. Non content de demeurer là, il lança, guilleret :

« Bah alors, le gros, toujours à chicaner plus modeste que soi ? »

Ernaut écarquilla les yeux, tiraillé entre l’envie de mettre une baffe directement et celle de voir jusqu’où l’inconnu irait. Puis un sourire se traça un chemin dans le visage écarlate.

« Le Groin ! Ça alors ![^voirporon] »

L’autre lui fit une bourrade amicale.

« Ouais, enfin, j’préfère qu’on me connaisse pas sous ce nom ici, gros. J’te demande pas si ça va, on dirait bien que oui ! »

Ernaut demeurait sans voix. Il n’avait pas vu son camarade depuis des mois, des années. La joie de découvrir un visage familier, évocateur de souvenirs d’enfance, camouflait le fait qu’ils avaient passé leur jeunesse à se cogner consciencieusement l’un sur l’autre. Il enlaça le voyageur d’une embrassade d’ours en riant.

« Tu t’es perdu que tu te retrouves ici ? »

La silhouette maigrelette disparut dans l’étreinte amicale. L’infortuné tentait de parler tandis que son torse subissait une accolade apte à étouffer un sanglier.

« Je viens pour les Pâques, comme tous ici ! »

Ernaut reposa son ami d’enfance et le toisa de bas en haut.

« Tu es bien léger pour si long voyage, bien digne pèlerin tu fais là à cheminer en t’en remettant à la fortune du Christ.

— Je ne viens pas de si loin, tu sais, je demeure à Jéricho.

— Au Jourdain ? Et ça fait long temps ?

— Pas tant, je te conterai mon périple si tu en as désir. »

Ernaut acquiesça, enthousiaste.

« Je finis d’œuvrer en cette porterie à la mitan du jour, qui ne saurait tarder. Nous pourrons aller dîner à Malquisinat, en se racontant les nouvelles. Tu es parti depuis quand de Vézelay ?

— J’ai pris route peu après les Mais de l’an passé, avant la saint Jean d’été. »

La silhouette s’était voûtée et le regard endurci, mais Ernaut était heureux de contempler son ami d’enfance, Droin, fils du porchier, avec lequel les bagarres étaient fréquentes. Un des rares à ne pas avoir peur de la vaste carrure d’Ernaut. Il désigna une des rues adjacentes.

« Il y a taverne juste à l’entrée de la rue de David, là. Tu n’as qu’à m’y attendre. Je ne serai pas long. »

Droin opina, un sourire sur les lèvres.

« C’est drôle de te retrouver ici, au centre du monde, portant l’épée à la hanche. J’ai souvenance qu’on se bestaillait hier sur les bords de la Cure.

— J’ai encore pouvoir de te mettre une pignée, à ta convenance » rétorqua Ernaut, amusé.

Droin haussa les épaules, un rictus moqueur sur les lèvres. Il ravala sa répartie, trop heureux de trouver un vieux compagnon pour risquer de se l’aliéner si tôt. Puis il s’éloigna d’un pas léger, son bourdon frappant le sol avec gaieté, après un dernier signe de la main

## Jérusalem, porte de David, midi du mardi 15 avril 1158

Assemblage de recoins et d’escaliers organisés autour d’une cour, la taverne était bondée. En cette période de semaine sainte, de nombreux voyageurs se présentaient aux porte de la cité, certains plus avides de boissons terrestres que de nourriture céleste. Installés à une table où s’époumonaient des allemands au teint cramoisi, Ernaut et Droin dévoraient des beignets à jour de poisson[^rissolepoisson], arrosés d’un vin léger au goût de framboise. Ils n’avaient guère eu la possibilité d’échanger plus de quelques mots, leurs compagnons vociférant à qui mieux-mieux tandis qu’ils encourageaient un jongleur occupé à des acrobaties. Ils en profitèrent donc pour engloutir rapidement leur repas, échangeant des sourires tout en avalant à belles dents les plats qu’on leur apportait. Malgré sa faible carrure Droin ne le cédait que peu en appétit à Ernaut.

Puis la chaleur et la lumière envahirent peu à peu la cour tandis que le soleil tournait et les clients se firent plus rares. Les deux compagnons purent prendre leurs aises sur les bancs et commandèrent un nouveau pichet et des oublies[^oublies] pour finir leur repas. Le saltimbanque eut droit à quelques piécettes de cuivre qu’il se hâta de convertir en vin, s’installant à côté d’eux. Il se mit alors à siroter tranquillement, le visage au soleil, les yeux fermés. Les ronflements ne se firent guère attendre.

« Alors Droin, quelles nouvelles du pays ? Bonnes, j’ai espoir !

— Les tiens prospèrent toujours. Le vieil Ermond conserve bon pied, malgré une fièvre qui l’a fort tenu alité. Il en a perdu large portion de sa panse. »

Ernaut sourit à l’évocation de son vieux père.

« Il s’esbigne toujours avec la Laurette ?

— Certes ! Leurs voix portent dans la rue souventes fois. Il faut dire qu’elle se sent maîtresse du logis, avec ses deux garçons !

— Deux ! Ça alors ! Mon frère doit être sacrément fier.

— Pas qu’un peu. Et puis l’un d’eux semble tirer de toi, aussi gros et grand que braillard. Alors forcément, la Laurette, elle s’y entend pour faire la belle au marché. »

Les nouvelles s’enchaînèrent, à propos de gens auxquels Ernaut n’avait plus pensé dès la butte de Vézelay perdue dans le lointain. Pourtant, il prenait plaisir à entendre les potins, les derniers ragots, le destin de ceux qu’il avait croisés enfant. Sa vie outremer. Il se faisait l’impression d’un vétéran, lui qui n’avait pas encore connu vingt printemps. Le silence s’installa tandis que les nouvelles tarissaient et que la chère pesait sur les estomacs et les esprits.

« Mais dis-moi, Droin, tu as mis sacré long temps pour venir jusqu’ici, tu as pris le chemin de terre ?

— Tout le monde n’a pas la fortune d’un Ermond pour se payer belle nef de voyage ! Je n’ai pu trouver navire qu’entre Byzance et Antioche.

— Byzance, tu es allé à Byzance ?

— Ouais, rétorqua Droin, dans un sourire fier. Enfin, j’ai vu les faubourgs, parce qu’ils m’ont pas laissé entrer. Mais c’est grand, comme, pfff, je sais pas, grand comme tu peux pas imaginer ! Du bateau, en partant, j’ai pu voir la côte, c’est que de la muraille, qu’a l’air toute petite alors que quand t’es aux pieds, t’en vois pas la fin. »

Ernaut acquiesça. Lui-même n’avait jamais vu la capitale du monde grec, là où résidait le basileus Manuel dont l’ombre s’étendait sur tous les royaumes moyen-orientaux. Il donna une tape amicale à Droin, désireux d’en savoir davantage.

« Tu viens ici comme pèlerin de la Foi, tu vas t’en retourner quand ?

— Non, je reste. J’ai bien trop sué pour arriver. J’ai cru jamais voir le clocher du sépulcre. Et puis ton voyage m’a donné des idées, moi aussi j’ai envie d’avoir ma terre. Pas comme le vieux qui garde les cochons des autres.

— Tu t’es installé ?

— Non, pas encore. Pour le moment, je me loue à des frères moines, à Jéricho. L’ouvrage est pas trop dur et la table est bonne. Je veux pas me tromper et choisir le bon endroit, à l’abri de ces maudits païens. De toute façon, je pense chercher à entrer dans la maison d’un noble, comme palefrenier. C’est que je m’y entends en chevaux, je m’occupais un temps de ceux des frères moines, au village. Avec un peu de chance, on me soudoiera et je pourrais gratter quelques rapines pour emplir mon escarcelle, histoire de me payer bel hostel. »

Ernaut se mit à rire, enthousiaste.

« Je vois que t’as pas changé, toujours autant désireux de changement ! »

Il gratifia d’une tape amicale son ami, qui répondit d’un sourire.

« Et toi, ton vieux disait que t’allais faire le colon ici, et je te retrouve armé comme Roland, gardien de la sainte cité.

— Lambert s’est installé, lui. Moi je ne sais pas encore. On m’a proposé de servir le roi, ça ne se refuse pas. Il marqua un temps et une onde de douceur passa sur ses traits. Et puis si je veux me marier et m’établir, je veux le faire bien, alors j’essaie de me faire un pécule. »

Droin s’agita, se pencha en avant.

« Si ça se trouve, on va finir flanc à flanc, l’épée au poing, à trucider du païen ! Ça m’ferait quelque chose, de sûr.

— Le Gros et le Groin, railla Ernaut.

— Ouais, ça serait notre cri de guerre : Gros et Groin, poingnez ! »

Il partit d’un grand rire sonore, avala un peu de vin. Le silence emplit de nouveau l’espace peu à peu tandis qu’ils sirotaient la chaleur et le vin.

« Tu es déjà venu à Jéricho ?

— Certes, Lambert a voulu qu’on fasse visite à tous les lieux que le père curé lui avait énumérés.

— Parfait, alors tu sauras y revenir. Viens-t’en donc me voir un de ces quatre, on se baignera au fleuve comme on faisait dans la Cure.

— Et je te ferai boire la tasse, comme à l’époque ?

— T’en as eu ta part je crois me faire souvenance… »

Ernaut sourit. Il était heureux d’avoir retrouvé une tête connue, quand bien même c’était celle sur laquelle il avait le plus tapé toutes ces années. Il choqua son gobelet contre celui de Droin.

« Je viendrai après les Pâques, on a fort ouvrage pour l’heure à la sergenterie du roi. Mais je viendrai, j’en fais promesse. »

## Bords du Jourdain, dimanche 27 avril 1158

Le vent portait les chants des pèlerins massés sur le chemin entre Saint-Jean et le lieu du baptême du Christ, à quelque centaines de mètres, jusque dans le taillis où Ernaut et ses proches s’étaient installés. Ils avaient bien sûr rendu visite aux lieux saints, mais profitaient désormais de la tiédeur du jour, alanguis parmi les bosquets d’acacias en fleur, baignant dans leur odeur sirupeuse.

Répondant à l’invitation de Droin, ils étaient venus en nombre. Eudes, sa femme et ses enfants, Droart et sa famille au complet étaient là. Mais aussi Sanson, Libourc et Mahaut venus de Mahomeriola et même Lambert, qui avait fait le déplacement depuis la Mahomerie. Ils avaient trouvé un bras du fleuve enserrant une petite clairière parsemée d’ombre, protégée du flot des croyants par un épais amas de roseaux.

Face à eux, sur l’autre rive, le sol rocailleux s’élevait rapidement vers des plateaux arides. Une compagnie de cavaliers soulevait un panache de fumée, des templiers reconnaissables à leur cape éclatante de blancheur. En ces périodes d’afflux de pèlerins d’outremer, ils parcouraient inlassablement les territoires pour en garantir la sécurité.

Ernaut était affalé contre un tronc de palmier, la tête sur une couverture repliée en guise d’oreiller. Il se laissait bercer par le bourdonnement d’abeilles affairées, colonies placées là par des apiculteurs désireux de profiter de l’abondance de fleurs en ce printemps lumineux. Droin s’approcha de lui, semant les gouttes sur son passage. Il avala une gorgée de vin et grapilla quelques raisins secs.

« Tu viens pas à l’eau ?

— J’ai crainte d’empirer les fièvres légères qui m’assaillent depuis quelques jours. Je sens une fièvre humide me gagner et je préfère éviter de lui donner de quoi se renforcer.

— T’étais pas si frileux, gamin ! »

Ernaut haussa les épaules, indolent.

« Les choses changent. »

Au fond de lui-même, il ne sentait nul mal, mais un fond de superstition l’empêchait de se baigner près d’un endroit aussi sacré. Il avait tout de même certains lourds péchés sur la conscience, et n’avait pas encore eu le courage d’aller s’en ouvrir à un confesseur. Bien qu’il n’ait pas été le plus assidu des fidèles, il n’avait guère envie de subir le courroux divin en le défiant ainsi.

Droin acquiesça et retourna vers le groupe qui barbotait le long de la rive. Les personnes âgées se contentaient de tremper les pieds, les mollets éventuellements tandis que les plus jeunes n’hésitaient pas à se baigner plus largement. Les enfants nus sautaient, éclaboussaient et soulevaient des gerbes d’eau, riant et criant. Les adultes se contentaient de garder leur chemise par pudeur, dans un lieu considéré comme sacré par tous, quand bien même ce n’était pas exactement le lieu du baptême.

Lorsque Droin les rejoignit, Ernaut entendit les voix qui s’interrogeaient sur son refus de se joindre aux baigneurs. Son goût pour l’eau était connu de tous, et le fait qu’il se refuse à en profiter étonnait ses proches. Droart lui cria :

« Viens-donc, Ernaut, il n’y a pas tant de fond que tu risques de t’y noyer !

— Penses-tu, il nage mieux que poisson, répliqua Lambert.

— Il craint d’empirer une fièvre qui le gagne » précisa Droin, qui avançait avec précaution dans le lit du cours d’eau.

Eudes, Sanson et Perrotte discutaient avec passion à mi-voix, assis sur un ressaut de terre, battant l’eau tandis qu’ils échangeaient avec sérieux. Ernaut accueillit d’un sourire Libourc qui sortait des flots tout en tordant le bas de sa chemise, dévoilant des jambes d’un blanc laiteux. Le jeune homme s’efforçait de ne pas laisser son regard errer plus qu’il ne fallait sur la jeune femme, qu’il découvrait en sous-vêtements pour la première fois.

La fine étoffe de lin souple lui dévoilait bien plus que ce qu’il avait pu imaginer jusqu’alors et il pouvait deviner les hanches, les cuisses et la poitrine de Libourc tandis qu’elle s’employait à rattacher correctement sa lourde natte. Légèrement échevelée, elle lui adressait un sourire à chaque fois que leurs regards se croisaient. Ce semblant de toilette achevé, elle vint auprès du jeune homme, s’agenouilla à ses côtés, tout en maintenant une prudente distance. Tandis qu’elle tranchait un peu de pain et de fromage, Ernaut voyait le tissu épouser sa peau, l’encolure dévoiler un peu de son épaule, l’ourlet abandonner une cheville fine. Elle finit par s’assoir en tailleur face à lui, dévorant un en-cas avec enthousiasme.

« Quelle magnifique journée, puisse-t-elle ne jamais finir ! »

Pour toute réponse, elle n’obtint d’Ernaut qu’un sourire. Elle plissa les yeux, le visage illuminé de joie.

« Quelque malaise t’a gagné, que tu ne souhaites pas te baigner? Tu m’as pourtant confié aimer fort cela.

— Je… Oui, j’ai crainte que mon mal n’empire. »

Elle prit un air désolé.

« Profitons du soleil ensemble alors…

— Les premières ardeurs sont les plus agréables. D’ici peu, nous devrons le fuir.

— Quel dommage que nous n’ayons pas si belle eau au casal, pouvoir se baigner ainsi est telle bénédiction.

— En ville, j’ai pris usage des bains et je dois avouer que je goûte fort cette invention. »

Certaines pensées de ce qui se passait dans ces établissements vinrent soudain à l’esprit d’Ernaut, qui se mordit la langue tandis que ses joues s’empourpraient. Libourc inclina la tête, les yeux emplis de joie, légèrement taquins. Avait-elle perçu l’hésitation dans la voix du jeune homme ?

« Tu voudrais donc nous voir nous installer en ville, pour continuer à profiter de ce confort ? »

Il n’était pas certain du sens à donner à cette question et remua un peu, gêné. La jeune femme semblait s’amuser de la situation, arborant un air d’une totale innocence qui déstabilisait d’autant plus Ernaut.

« Un cours d’eau proche pourra faire l’affaire, ou un cuvier en mon hostel.

— Messire a goûts de byzantin, à ce que je vois. »

La lueur espiègle qui s’était allumé dans le regard de Libourc arracha un sourire à Ernaut. Il n’était jamais certain de ses intentions, et la femme idéalisée qu’il l’imaginait être était peut-être bien éloignée de la réalité. Elle finit d’avaler quelques miettes et soupira d’aise. Puis tendit la main pour caresser la joue d’Ernaut tandis qu’elle murmurait :

« Je vais encore profiter de ces flots un petit moment, mon beau. Puis je reviens à toi. »

Elle scella sa promesse d’un éphémère baiser sur les lèvres et s’éloigna d’un pas rapide. Tandis qu’elle marchait, elle tenait l’étoffe de sa chemise dans le poing, dénudant ses jambes, faisant voler l’étoffe à chaque pas tandis que le col avait glissé sur une épaule blafarde.

Ernaut goûta sur ses lèvres le souvenir de sa bienaimée, sourit pour lui-même et ferma les yeux sur la vision de la jeune femme. Le moment lui semblait tout indiqué pour une sieste. ❧

## Notes

La cité de Jérusalem, bien que considérée au centre du monde, profitait de son importance religieuse et politique mais, située en dehors des principaux axes, dans une région montagneuse, n’était pas un centre commercial important au Moyen Orient. L’activité marchande y était régulée par l’administration royale, qui établissait les péages et frais de douane qui s’appliquaient aux denrées franchissant ses portes.

La majeure partie des voyageurs qui franchissaient ses muraille venaient en raison de la dévotion envers les lieux saints, surtout aux dates importantes du calendrier chrétien. Bien évidemment Pâques tenait la première place, mais d’autres moments plus laïques, attiraient les foules, comme l’anniversaire de la prise de la ville quelques décennies plus tôt, à la mi-juillet. Les nombreux croyants qui venaient pour visiter les lieux saints le faisaient pour des raisons extrêmement variées, belliqueuses ou pas, par simple foi ou à la recherche d’une nouvelle vie. Il est difficile de se représenter les intentions qui devaient parfois se mêler au sein d’un individu. Toujours est-il que beaucoup de personnes venues d’Europe décidèrent de rester, profitant des opportunités, des offres de peuplement des propriétaires terriens. Ils créérent une société latine originale, en plein Moyen Orient, qui survécut pendant presque deux siècles.

## Références

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.

Boas Adrian J., *Jerusalem in the Time of the Crusades*, Londres et New-York : Routledge, 2001.
