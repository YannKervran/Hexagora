---
date : 2018-01-15
abstract : 1147-1158. Jeune veuve avec deux enfants en bas âge, Élainne tente de reconstruire sa vie tout en s’assurant de ne pas connaître pareilles vicissitudes une nouvelle fois. La fortune récompensera-t-elle ses efforts ?
characters : Élainne, Gaillard le Tailleur, Ganelon, Tyèce, Fulbert, Senoreth, Jehan de Berry, Jacques le Portier, Remigio
---

# Deux vies

## Jérusalem, quartier Saint-Étienne, fin de matinée du jeudi 11 septembre 1147

Un balai de palme à la main, Élainne frottait la calade de la petite cour ombragée d’un amandier. Son fils Fulbert y poussait de menus jouets de bois et de céramique, proposant certains d’entre eux à sa sœur installée près de lui. Tyèce se contentait de sucer et goûter tout ce qui passait à sa portée, gazouillant de satisfaction lorsqu’elle estimait l’objet suffisamment luisant de salive.

Malgré l’agitation du marché voisin, l’endroit était assez calme. Il n’y manquait qu’un jardin potager pour que cela soit parfait. Ils avaient à peine la place d’installer des claies pour y faire sécher amandes, fruits et légumes. Élainne y cultivait quelques maigres plates-bandes, aromatiques et fleurs. Comme il leur fallait acheminer l’eau depuis la fontaine, la récolte demeurait généralement faible. Heureusement que Gaillard, tailleur de pierre sur le chantier du Saint-Sépulcre gagnait suffisamment pour épargner à son épouse la corvée quotidienne. Ils se faisaient livrer à domicile, comme des patriciens plaisantaient-ils.

La jeune femme vérifia que le ragoût diffusant des senteurs de cumin mijotait paisiblement sur le petit feu de cuisine extérieur. Elle poussa quelques braises, ajouta des bûchettes. Elle avait apporté son pain au four tôt le matin et n’aurait plus qu’à aller chercher un pichet de vin à la taverne pour le souper. Son époux était généralement affamé quand il rentrait, le gosier sec de la poussière avalée sur les chantiers et il n’appréciait rien tant qu’un bon verre là-dessus. Elle regrettait de n’avoir plus l’occasion de brasser de la bière, comme sa mère le lui avait appris, mais leur logement, deux petites pièces en plus d’un minuscule cellier, ne lui en laissait pas le loisir. Elle espérait qu’un jour prochain ils s’installeraient dans une maison plus grande, peut-être dans un des villages qui offraient tant d’avantages aux colons. Le savoir-faire d’un maçon tailleur de pierres serait partout apprécié.

Elle allait s’asseoir pour repriser une chemise lorsque des coups résonnèrent sur la porte de la cour. Pensant accueillir les porteurs d’eau, habituels en cette heure, elle fronça les sourcils, inquiète de découvrir Roncin, un ami de son époux. Le visage gris, il trépignait d’un pied sur l’autre, ses grosses mains serrant un linge lui servant à se rafraîchir.

« Le bon jour, mestre Roncin. Gaillard est au chantier…

— le bon jour, Élainne. J’en viens, justement. »

Il força un peu le passage pour entrer, sans se départir de son air embarrassé. Il lança un regard aux deux enfants puis s’approcha de la jeune femme.

« Une potence a lâché tandis qu’on hissait moellons. Le filet s’est déchiré et il a plu des pierres ! Ton époux en a reçu plusieurs. »

Élainne en eut le souffle coupé, elle s’appuya sur le mur. Le solide maçon tenta un sourire réconfortant.

« Ils sont trois à avoir reçu navrures. On les a portés à l’hôpital des frères de Saint-Jean.

— Est-il sévèrement touché ? Comment est-il ?

— Il a eu le flanc durement frappé, mais je ne saurais dire. Les frères ont mandé habile cirurgien… »

Inspirant un grand coup, Élainne laissa Roncin entrer complètement et poussa la porte derrière lui. Elle fut rapidement prête, se contentant d’enlever le vieux surcot qui lui servait de protection pour les travaux ménagers. Elle déposa sa fille dans une hotte avec quelques affaires et prit la main de Fulbert. Pendant ce temps, Roncin s’occupa d’étouffer le petit feu qui serait sans surveillance. Tout en serrant sa poupée de chiffons, le garçon glissa d’une voix inquiète :

« Père va mourir ? »

Les yeux agrandis d’effroi, Élainne se contenta de se mordre la lèvre. Le garçonnet n’eut pour toute réponse qu’un haussement d’épaules désolé du solide artisan.

## Jérusalem, église du Saint-Sépulcre, après-midi du dimanche 21 décembre 1147

La pluie avait repoussé les badauds dans l’édifice, augmentant la cohue, fréquente en ces périodes de fête. De nombreux pèlerins passaient l’hiver en Terre sainte afin de célébrer la naissance du Christ, puis Pâques, avant de rentrer avant les grosses chaleurs estivales. On entendait parler toutes les langues, y compris les plus exotiques et les clercs les plus rigoristes pinçaient le nez en découvrant certaines activités prendre place dans leur église. Il n’était pas rare que des dés roulent, que des amuseurs fassent montre de leur talent, voire que des prostituées monnayent leurs charmes. Il s’était même vu quelques négociants aventureux qui avaient amené leur marchandise pour y commercer à l’abri. C’était là le destin de tous les lieux de culte ouverts aux fidèles, qui n’étaient concurrencés par les cimetières que les jours de beaux temps.

Élainne profitait de son passage pour prier pour le repos de l’âme de son époux. Il avait mis plusieurs semaines avant de mourir, dans les plus grandes souffrances. Elle était désormais isolée, avec un bien maigre pécule cotisé par les compagnons de Gaillard. Il ne faudrait pas plus de quelques mois pour le voir épuisé. Elle avait fait écrire et porter des messages à ses frères et sœurs, dont aucun ne résidait plus à Jérusalem, mais un seul avait répondu, Hemeri, moine à Calanson. Elle avait reçu le pli quelques jours plus tôt et était venue faire lire sa missive par un des clercs du Saint-Sépulcre : il lui envoyait ses meilleures pensées et prierait pour leur Salut à tous. Qu’elle se trouvât dans le besoin ne semblait pas l’avoir effleuré. Benjamine de la fratrie, elle n’avait jamais été proche d’aucun d’eux et sentait désormais tout le poids de sa solitude. Gaillard ne fréquentait que ses confrères et elle n’avait jamais réellement sympathisé avec leurs épouses.

Lorsqu’elle se releva de ses oraisons, elle chercha Fulbert du regard. Le petit avait trouvé des compagnons de jeu et participait à une course-poursuite alternant avec des périodes d’escalade des bases de colonne. Elle assujettit le panier avec Tyèce, endormie, et se mit en quête de son fils. Le jour ne durait guère au plus fort de l’hiver et elle souhaitait être rentrée au plus tôt. Le clerc qui lui avait lu la missive, jeune chanoine aux yeux doux et aux gestes maladroits, s’approcha d’elle avec un homme de taille modeste, claudicant à ses côtés. Il était habillé de belle toile, sans ostentation, mais on sentait qu’il était aisé. Son visage ingrat, un peu bouffi, n’était guère rehaussé par l’énorme nævus poilu qui surplombait sa barbe maigrelette. Son regard démentait pourtant cette allure commune. Il était direct, franc et brillait de détermination. Le religieux adressa à Élainne un sourire et lui indiqua que, voyant son désarroi, il n’avait pu s’empêcher de chercher une façon de l’aider à améliorer son sort.

« Mestre Senoreth est de bonne fame en nostre congrégation. Peut-être aurait-il moyen de vous porter assistance ? »

Le clerc passa les mains dans ses manches, hochant de la tête comme un gallinacé heureux de sa découverte. Il semblait néanmoins décidé à demeurer lors de l’échange, apportant ainsi sa garantie aux propos tenus.

« Lorsque le frère m’a parlé de votre malheur, je lui ai fait part de mon souhait de trouver nouvel apprenti en mon échoppe.

— C’est grande amabilité de votre part, mestre, mais je ne sais guère que m’occuper de mon foyer.

— Il s’agit là d’une tâche qui sied à toute femme : mercière. Je suis mestre passementier et j’ai toujours préféré l’habileté et le soin des dames pour les délicats travaux qu’on me confie. Vous savez de certes tirer l’aiguille ?

— Ma mère y a pourvu, comme de bien entendu. Mais de là à en faire mestier juré, je n’ai pas le talent ni la sapience de telles choses.

— Ne vous souciez point de cela. L’apprentissage y pourvoira. J’ai usage de prendre plus jeunes personnes, mais le frère m’a ému de vos soucis et ce serait de justice que de faire bonnes œuvres en cette période de l’Avent. »

Élainne sourit aimablement, un peu réticente à l’idée de se lier ainsi à un inconnu. Elle savait que bon nombre de maîtres exploitaient les mains qu’ils recrutaient, quand ils n’abusaient pas de leur position éminente pour exiger de plus sournoises tâches.

« Si vous en êtes d’accord, vous n’aurez qu’à vous présenter à ma boutique, sur la voie menant porte Sainte-Étienne. Demandez mestre Senoreth. Mon épouse sera prévenue au cas où je sois absent. Prenez votre temps, je me suis laissé jusqu’aux Pâques pour décider, je n’ai pas d’ouvrage important en souffrance pour le moment. »

Le chanoine aurait convenu à la place, nul doute qu’il y aurait postulé tellement il acquiesçait d’enthousiasme.

« Je peux me porter témoin en votre engagement si nul ne peut le faire. Mandez après moi ici : Jehan de Berry » précisa-t-il, tout sourire.

## Jérusalem, demeure de Pierre Salomon, soirée du samedi 5 avril 1152

Dégoulinant de pluie, Ganelon se frottait les mains auprès du four encore chaud dans la cuisine du riche négociant. Il avait chevauché à bride abattue pour porter un message de la part de son maître, le chevalier Régnier d’Eaucourt, à Pierre Salomon, membre éminent et influent de la Cour des Bourgeois de Jérusalem. À peine avait-il pris la route depuis Mirabel que les nuages lourdement chargés de la côte s’étaient vidé sur lui. Bien que la température fût clémente, il se sentait transi jusqu’aux os. Il était heureux de ne pas avoir à emporter de réponse avant le lendemain.

Le royaume était sur le point d’exploser : Mélisende et son fils Baudoin se disputaient le pouvoir et les affrontements armés n’étaient pas loin. Le maître de Ganelon, fidèle serviteur du connétable de la reine jusque là venait de se rallier au jeune prince après que celui-ci ait capturé le vieux compagnon de route de sa mère. Ganelon avait été désigné pour porter un message afin de préparer une entrevue avec la Cour des Bourgeois. Leur allégeance allait traditionnellement à Mélisende, mais avec l’ascension irrésistible de Baudoin, peut-être auraient-ils le désir de changer de point de vue.

Pour Ganelon, c’était égal. S’il appréciait la compagnie de Régnier d’Eaucourt malgré ses exigences parfois élevées, il ne se mêlait jamais de politique. D’abord il n’y comprenait rien et, ensuite, il estimait que c’était le plus sûr moyen pour se voir le cou suspendu à une branche. Alors que personne n’avait jamais pendu un valet qui faisait correctement son travail. Il regrettait d’avoir décidé de venir en Terre sainte, quelques années plus tôt, attiré par l’aventure, et n’aspirait plus qu’à la tranquillité.

La cuisine était rangée et le feu moribond, mais un des jeunes valets, Jacques, était allé demander du pain et un peu d’accompagnement pour le souper de Ganelon. En l’absence du personnel, les portes du cellier étaient fermées et c’était l’épouse de maître Pierre qui en détenait les clefs. Une petite veilleuse posée sur la table dispensait une faible lumière dans la vaste salle, prévue pour y préparer les banquets et y donner les repas des domestiques.

Lorsque la maîtresse de maison entra, elle ne lança pas un regard à Ganelon. Bourgeoise d’importance, elle n’accordait aucun égard aux gens les plus modestes. Ganelon suivit son avancée d’un œil appréciateur, reconnaissant qu’elle conservait une séduisante allure malgré les années. Il fut surpris de découvrir dans son sillage une femme au beau visage dont l’ovale était souligné par un voile strictement noué. Elle baissait le regard modestement et prit place à table sans mot dire.

De retour du cellier, Jacques apportait une grosse miche, un pichet et déposa un pot dans le four encore tiède tandis que sa maîtresse ressortait en silence. Il disposa écuelles et gobelets, invitant Ganelon à s’asseoir sur le banc.

« Ganelon, je crois que t’as jamais encontré Élainne. Elle œuvre comme passementière chez mestre Senoreth. Vu l’heure tardive, elle va compaigner ton repas. »

Il fit rapidement les présentations puis se servit une solide rasade de vin coupé. Ganelon appréciait fort le jeune homme, dont les traits lui évoquaient ceux d’Alerot, un compagnon mort en chemin pour la Terre sainte. Il était néanmoins bien plus beau et savait tirer avantage de son frais minois. Il ne semblait pourtant guère intéressé par la couturière, qu’il estimait peut-être trop âgée pour lui. Tout au long du repas, il fit la conversation, sur des sujets légers et des ragots locaux. Ganelon en profita pour lancer quelques regards à la dérobée à Élainne, dont il s’aperçut bien vite qu’elle faisait de même. Il bomba imperceptiblement le torse, heureux d’avoir choisi d’enfiler sa plus belle cotte de travail pour venir délivrer ce message.

## Jérusalem, abords de l’église Saint-Étienne, matin du dimanche 27 septembre 1153

Goûtant le beau temps et une température clémente, les fidèles s’étaient amassés après l’office sur le parvis de l’église. C’était l’occasion d’échanger tranquillement des nouvelles de la cité et d’ailleurs. Ganelon était rentré depuis peu de la campagne royale victorieuse qui avait mené à la capture d’Ascalon. Il profitait de son jour de repos pour passer un moment avec Élainne, qu’il avait pris l’habitude de retrouver pour la messe dominicale.

À son grand dam, maître Senoreth estimait qu’il était de ses attributions de veiller aux bonnes mœurs de son apprentie et il ne leur accordait jamais plus de quelques pas de latitude. C’était néanmoins de bien agréables instants partagés. Autour d’eux, les enfants les plus aventureux couraient parmi les jardins et les enclos. D’un tempérament plutôt lymphatique, Fulbert, le fils d’Ellaine, tentait parfois de suivre le rythme endiablé des plus énergiques. Sa mère confia à Ganelon qu’elle ne savait pas ce qu’elle allait en faire. Elle ne pouvait en effet plus s’en occuper et elle craignait de le voir subir de mauvaises influences.

« Pourquoi ne pas le placer comme valet ? proposa Ganelon. Il suffit de lui trouver bon maître et il suivra droit chemin.

— J’y ai pensé, mais je ne sais à qui me fier. Mestre Senoreth n’a nul ami qui le prendrait à son service, je lui en ai déjà parlé.

— Pourquoi pas au service des moines ? Tu es fort proche des chanoines du sépulcre le Christ. Ils ont des terres tout entour le ventre. Ce serait bien le diable s’ils ne trouvaient lieu où faire œuvrer Fulbert. De plus, on se loue d’une année l’autre chez eux, cela permet de changer. »

Élainne accorda un sourire triste à Ganelon. Elle appréciait beaucoup sa présence et ses conseils, mais elle avait espéré que son fils pourrait connaître meilleure vie. Elle ne souhaitait pas froisser son ami, d’autant qu’elle avait pour lui une tendresse qui se renforçait au fil des mois, à la faveur de leurs échanges, tout de délicatesse et de mesure. Mais elle aurait voulu que son enfant soit plus que valet. Elle hocha la tête doucement et garda le silence un moment. Elle aperçut Tyèce, occupée à gratter la terre avec quelques compagnes de bêtises. Elle s’avança pour la tancer, n’appréciant guère de voir sa plus belle tenue ainsi malmenée, mais fut devancée par un parent plus proche.

Au bout d’un moment, Ganelon revint vers elle, un large sourire barrant son visage amical. Elle lui trouva fort correcte allure, dans sa cotte de laine, la barbe taillée et les chausses bien ajustées. Peut-être pourrait-elle lui confectionner un petit parement à fixer sur ses manches, pour une occasion prochaine. Il était aimable, travailleur et courtois. Plus qu’elle n’en demandait à un époux. Elle lui sourit à son tour.

## Jérusalem, jardins de la porte Saint-Étienne, fin d’après-midi du jeudi 15 juillet 1154

Les festivités de commémoration de la prise de la ville battaient leur plein. De nombreuses célébrations religieuses étaient fêtées par des volées de cloches et les farandoles, défilés et joyeux attroupements profanes se répandaient dans tout Jérusalem. Au nord de la cité, des jardins plantés d’arbres offraient des zones d’ombre où s’installer au plus chaud de l’été. Des musiciens marquaient le rythme, incitant les présents à de folles danses tandis que jongleurs et acrobates rivalisaient de prouesses.

Ganelon avait invité Élainne à profiter de ce jour férié. Il avait amplement fait provision en passant par Malquisinat et avait garni sa bourse de suffisamment de mailles et d’oboles pour régaler comme un prince autour de lui. Fulbert et Tyèce couraient de-ci de-là, mordant avec gourmandise brioches et fruits tout en applaudissant les artistes.

Devant eux, un montreur d’animaux racontait l’histoire de la prise de la ville avec des tableaux illustrés par les bêtes. Un âne servait de destrier aux héros représentés par un perroquet qui connaissait de nombreux cris de guerre tandis que les musulmans étaient symbolisés par un chien au vaste panel de compétences. Le récit était avant tout burlesque et le comédien en faisait des tonnes, pour le plus grand bonheur du public. L’ambiance était joyeuse, les nouvelles de l’été étaient bonnes.

Assise sur un muret à distance respectueuse de Ganelon, Élainne grignotait des pistaches tout en profitant du moment. Elle en proposa à son compagnon et s’enquit de ce qui pouvait le chagriner par une si belle journée. Elle avait remarqué son visage fermé et l’entrain forcé dont il faisait montre malgré toute sa bonne volonté.

« J’ai mauvaise nouvelle à te faire assavoir… Mon sire Régnier s’est vu confier importante mission et je dois le suivre.

— Il est en ainsi depuis que nous nous connaissons, mon ami. En quoi est-ce différent ?

— Nous allons outremer, en Provence, Bourgogne, France, Normandie, Angleterre, Flandre… Il y en a pour des mois, si ce n’est des années ! »

Élainne hocha la tête. Elle fréquentait Ganelon depuis bientôt deux ans. Elle avait ainsi un prétendant officiel qui lui permettait de repousser les avances qu’on aurait pu lui faire. Mais elle savait bien que celles-ci seraient de plus en plus rares. Elle n’était plus si loin de ses trente ans et définitivement sans avoir. Quel homme aurait voulu d’elle ? D’un autre côté, jamais elle ne renoncerait à son apprentissage, dont elle n’avait fait qu’un peu plus de la moitié. La mort de Gaillard l’avait laissée démunie et elle avait besoin de jurer un métier, de devenir suffisamment indépendante pour ne pas risquer de se retrouver une nouvelle fois en si misérable posture, réduite à mendier auprès de sa famille, en pure perte. Elle adressa à Ganelon son plus charmant sourire.

« Vous reviendrez bien assez tôt que je ne me sois lassée d’attendre.

— Que veux-tu dire ?

— Je ne ferai rien tant que je n’aurais pas obtenu maîtrise en mon métier. Et cela ne se fera pas avant long temps. Assez pour que tu reviennes et que tu me jures ta foi. »

Troublé par l’adresse directe, Ganelon faillit en bafouiller et se reprit bien vite.

« Tu accepterais de devenir mon espousée ?

— Il me faudra en demander avis à quelque doctes conseils. Peut-être demanderai-je à frère Jehan de Berry s’il te croit homme de bonne foi » le moqua-t-elle.

Lorsque Ganelon mordit dans sa fouace, il lui sembla qu’il n’en avait jamais goûté de meilleure.

## Jérusalem, rue de David, midi du vendredi 23 mai 1158

Se faufilant parmi la foule à travers la rue de David, toujours encombrée, Élainne succombait à une agitation inhabituelle chez elle. Elle avait reçu un message du chanoine Jehan, avec qui elle entretenait des relations presque fraternelles, depuis toutes ces années. Il lui avait fait lecture d’une excellente nouvelle : un des riches notables de la cité l’avait couchée dans son testament. Éminent membre de la Cour des Bourgeois, Tosetus était un bienfaiteur dont on avait vanté la loyauté et la chaleur humaine. Récemment décédé, il avait souhaité faire des donations à des œuvres et Élainne était du nombre. Elle n’avait pas souvenir de l’avoir rencontré, mais le fait était bien là.

Elle frappa avec enthousiasme à la porte de la demeure de Régnier d’Eaucourt. Elle espérait que Ganelon serait présent, impatiente qu’elle était de partager la nouvelle. Ce fut lui qui ouvrit. Il était en chemise, transpirant. Son visage s’éclaira en la voyant.

« Que voilà belle surprise ! Je n’espérais guère après toi avant ce dimanche. »

Une fois entrée dans la cour, Élainne y découvrit tout un fourniment de combat : boucliers, lances, selles, casque et haubert, étalé un peu partout. C’était visiblement le grand déballage. Ganelon intercepta son regard.

« Une campagne se prépare, il me faut fourbir promptement les affaires de mon sire. »

Élainne hocha la tête, peu intéressée par les détails guerriers de cette nouvelle chevauchée. Elle avait toujours la crainte d’apprendre que Ganelon n’en était pas revenu. Ils en étaient encore à préparer leur installation, pour laquelle leurs maigres économies ne suffisaient guère. Elle commençait à peine à avoir suffisamment de travail pour subvenir à ses besoins propres et Ganelon n’avait nulle ressource en dehors de ses compétences comme valet, en plus de quelques besants âprement épargnés. De plus, elle espérait initier sa fille au métier, maintenant qu’elle en avait le droit, pour qu’elle soit indépendante.

Elle invita Ganelon à s’asseoir sur un des bancs.

« Je reviens du Saint-Sépulcre, Jehan m’a fait lecture d’un important document. Il se trouve qu’un riche bourgeois du roi a décidé d’aider quelques malheureux. Il me fait don d’une maison aux abords de la Tour de David. »

Ganelon en resta sans voix, battant des paupières comme si une poussière s’y attachait.

« Je demanderai à mestre Senoreth ce qu’il sait de lui, mais Jehan m’a dit que c’était fort pieu et charitable prud’homme. »

Elle s’enhardit à lui prendre les mains.

« Comprends-tu ce que cela signifie ? Nous allons pouvoir nous jurer fidélité ! » ❧

## Notes

J’en ai déjà parlé à de nombreuses reprises, l’étude du parcours de vie des plus humbles a longtemps été négligée par les historiens en raison du manque de sources et des difficultés d’analyse de celles-ci. Un pointilleux et laborieux travail de recension et collation était nécessaire en préalable à toute construction scientifique critique. Depuis quelques années, la numérisation permet de rendre tout cela bien plus aisé. Tout d’abord en mettant à disposition les textes, parfois anciens, qui font autorité sur le sujet. Que ce soit dans des bases comme Gallica ou Persée, il est désormais bien plus commode de consulter certaines publications que vingt ans plus tôt.

Mais le bénéfice des outils réseau ne s’arrête pas là. Des initiatives de synthèse critique sont élaborées par des instituts de recherche et offertes à la communauté. J’ai déjà cité le projet prosopographique de Charles Crowley (*Medieval lands. A prosopography of medieval European noble and royal families* : http://fmg.ac/Projects/MedLands/ ) et je voulais ici parler d’un autre programme, qui va grandement me faciliter le travail documentaire : *The Revised Regesta Regni Hierosolymitani*, qui propose de traduire en une base de données toutes les chartes patiemment étudiées par Reinhold Röhricht à la fin du XIXe siècle, base incontournable pour qui s’intéresse à l’histoire des croisades. Cela offre l’opportunité de reconstruire plus aisément le parcours de vie de signataires méconnus. Autant d’occasions pour moi d’étayer le destin des protagonistes de *Hexagora*, à charge pour les plus érudits ou curieux de faire les liens avec les sources scientifiques.

## Références

Besson Florian, « Fidélité et fluidité dans l’Orient latin. L’exemple de Rohard de Jérusalem (v.1105- v. 1185) » dans *Bulletin du Centre de recherche français à Jérusalem*, volume 26, 2015.

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972.

Prawer Joshua, *Crusader Institutions*, Oxford University Press, Oxford, New York : 1980.

Riley-Smith Jonathan (ed.), Ellenblum Ronnie, Kedar Benjamin, Shagrir Iris, Gutgarts Anna, Edbury Peter, Phillips Jonathan et Bom Myra, *Revised Regesta Regni Hierosolymitani Database* http://crusades-regesta.com/ (consulté le 24/01/2018)
