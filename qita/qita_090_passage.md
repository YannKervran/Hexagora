---
date : 2019-04-15
abstract : 1158-1159. Vieillissant, Sanson de Brie accomplit ses ultimes devoirs en tant que père et se prépare à entamer la dernière partie de sa vie, qui le mènera à son trépas.
characters : Libourc, Ernaut, Sanson de Brie, Mahaut, Pariset, Plat-Pié, Géraud Le Roi, Huet, Lambert
---

# Passage

## Casal de Mahomeriola, fin d’après-midi du mercredi 29 janvier 1158

Un ciel chargé de houle noire avait chassé le jour encore plus tôt qu’habituellement. Des maisons claquemurées s’élevaient avec difficulté quelques fumerolles stagnantes, alourdissant l’atmosphère. Parti au matin de la Grande Mahomerie sur une monture de location, Lambert arrivait enfin à Mahomeriola alors que des attardés rentraient les poules ou enclosaient leurs bêtes pour la nuit. En ce crépusucule annonciateur de pluie froide, même les chiens errants avaient trouvé un abri. À son grand réconfort, Sanson lui fit bon accueil et l’invita à se reposer tandis que Mahaut et Libourc préparaient le potage du soir. Près du feu, tout en dégustant un vin léger coupé d’eau, ils discutèrent semailles et récoltes, jointure et cheptel, comparant les terroirs de leurs deux manses. Ils avaient fait connaissance lors de leur séjour à l’hôpital de Saint-Jean[^voirt2], mais se découvraient vraiment pour la première fois.

Ce ne fut qu’une fois le repas avalé que Sanson invita son épouse et sa fille à se retirer derrière les courtines du lit pour que les deux hommes puissent aborder ce qui amenait Lambert en ces lieux. Car il n’était pas là pour une simple visite de courtoisie. Empreint de sérieux, Sanson leur servit de la petite bière et posa un bol d’amandes entre eux sur la table.

« Alors, Lambert, que peux-tu me dire des intentions de ton frère ?

— Elles sont limpides : il m’a mandé afin de voir quelles seraient vos prétentions pour son mariage avec Libourc. Il veut que cela soit fait ainsi qu’il sied. »

Sanson fit une grimace. Le mot avait été lâché et l’entendre ainsi prononcé lui meurtrissait le cœur. Il s’y était préparé, le jeune Ernaut l’avait déjà dit précédemment. Mais cette fois, les choses devenaient concrètes, officielles, inévitables. Il se passa une main fatiguée sur le visage, se recomposant un aspect plus engageant. Il aimait bien ce qu’il voyait de Lambert jusqu’à présent. Et il avait de la sympathie pour Ernaut. Sans pour autant que cela soit suffisant pour qu’il lui concède sa fille sans prendre des garanties.

« Avant tout, je dois te dire que je ne pensais pas m’établir ici et Libourc n’a nul désir de s’en retourner vers les miens, à Meaux. Il me faut donc m’assurer qu’elle aura bon parti pour ne pas se trouver seulette et dépourvue.

— C’est ce que j’ai expliqué à mon frère, et il l’a fort bien entendu. Il est prêt à lui accorder douaire de bon aloi. Je me porte garant de ce qu’il promettra. Elle sera de ma famille et pourra compter sur mon aide et ma protection. À Vézelay, notre parole est écoutée et entendue : le père a une voix d’importance au parmi des doyens, y compris près le sire abbé. »

Sanson hocha la tête. Tout devenait compliqué quand on était loin des siens. Comme Lambert, il était un nouvel arrivant dans son casal et nul n’avait tissé de liens assez forts avec lui pour lui offrir la sécurité d’une communauté ancienne et éprouvée comme en Brie. Il avait fait écrire une lettre à des amis pour se renseigner sur la réputation de la famille d’Ernaut, là-bas, en Bourgogne, mais nul n’était jamais sûr de recevoir une réponse.

Prenant son silence pour une invitation à développer, Lambert enchaîna.

« Outre, j’ai projet de faire bon mariage moi aussi, d’ici peu. Une voisine mienne, Osanne fille Godefroy.

— Deux mariages en si peu de temps ?

— Père nous a bien garni, nous avions désir de nous établir ici. Ernaut m’a laissé usage de sa part pour le moment, mais je lui paierais son dû avec la récolte. Cela s’ajoutera outre sa solde de sergent.

— De cela aussi j’ai quelque crainte. Il n’est pas maître de son bien et, surtout, s’expose à grands dangers chaque jour, même s’il sert prestigieux seigneur. »

Lambert retint de justesse un sourire ironique. Ernaut avait dû dresser un tableau héroïque de ses activités dans l’hôtel royal. Il n’était pourtant que simple sergent affecté à des tâches administratives et du labeur routinier. Il n’encourait guère plus de risques qu’un valet de ferme aux prises avec un troupeau de vaches. Jusqu’à présent il avait toujours été tenu en dehors des opérations militaires, même s’il bouillait d’impatience d’y prendre part. Lambert faisait tout son possible pour l’en dissuader, car il n’était pas bon selon lui que le vigneron ou le laboureur se fasse guerrier. Chacun devait demeurer à sa place.

« Il ne s’est pas juré définitivement au roi, mais renouvelle chaque année son serment. Il envisage aussi de faire quelque négoce avec des amis à lui. Il a désir de s’établir en ville, ce qui offre solides garanties de sauveté.

— Et comment donc ? Il n’a pas de métier ! Même si je sais qu’ici il est de bon usage de jouer avec la monnaie comme si c’en était un.

— De nombreux offices sont proposés à ceux qui servent bien. Ernaut a déjà soudée suffisante pour nourrir les siens.

— Oui, cela j’entends bien, mais *amour de seigneur n’est pas fief !* »

À voir la mine renfrognée de Lambert, Sanson comprit qu’il était allé trop loin dans ses récriminations. Il avait parlé un peu trop librement, dévoilant ses sentiments intimes au lieu d’avancer des arguments de négociation. Il ne souhaitait néanmoins pas laisser croire à un refus. Il ne serait pleinement rassuré que lorsque Libourc aurait un époux pour veiller sur elle, et Ernaut semblait un bon parti. Il tenta un sourire plus engageant.

« Foin de tout ceci, parlons clair. Tu me disais qu’Ernaut allait mieux s’établir ? Est-ce logement pour fonder son foyer ?

— Non pas, juste deux pièces près le centre de la Cité. De là, il voudrait chercher meilleure demeure pour les siens et l’acheter. Avec ses compaings en la sergenterie, il ne devrait guère avoir de peine à dénicher bel endroit. Mais en attendant, il épargne un cens inutile. »

Sanson approuva, se demandant si cette attitude réfléchie était bien du fait d’Ernaut. S’il le trouvait très sympathique et appréciait les égards dont il entourait Libourc, il lui semblait voir là plus des traits de Lambert que du jeune homme. Quoiqu’il en soit, c’était néanmoins un signe rassurant qu’il suive les préceptes d’un aîné pétri de bon sens. Sanson lança un regard aux mains de Lambert : tavelées, crevassées, emplies de corne, celles d’un paysan dur à la tâche. Cela lui plaisait. Il ne faudrait plus longtemps avant qu’ils ne parviennent à un accord formel.

## Casal de Mahomeriola, demeure de Pariset, veillée du dimanche 7 septembre 1158

Le manse de Pariset était parmi les plus vastes et les plus prospères du casal. Établi autour d’une cour enclose de murs maçonnés, il rivalisait de dépendances et d’annexes avec le manoir du Saint-Sépulcre. Installée là depuis les origines, sa famille se vantait d’avoir juré serment à Godefroy de Bouillon lui-même avant qu’il ne fasse don du lieu au Saint-Sépulcre. Chaque année pour les Rameaux, leur doyen faisait bénir une branche de buis qu’ils prétendaient venir de cet hommage illustre, brindille rabougrie et jaunie qui trônait derrière un crucifix dans leur grande salle le reste de l’année.

C’était désormais Pariset, vieil homme malingre et maladif, qui s’asseyait au haut bout de la table. Père d’une considérable fratrie, il avait abandonné la gestion quotidienne de l’exploitation à son aîné pour se consacrer à l’administration du bourg. Son élégance et sa beauté, conservées au fil des ans, attiraient le regard et entretenaient les espoirs de nombreuses femmes, sans aucun succès jusqu’à présent. Veuf sans aucun attrait pour le sexe ou la religion, il avait un tempérament calme et méthodique qui faisait merveille pour concilier des voisins fâchés par des querelles de bornage ou d’animal errant.

Au sein du conseil siégeait aussi Géraud, dit *Le roi*. Imposant octogénaire, sourd à en rendre jalouses bien des pierres, il répondait d’une voix grasseyante tonitruante quand on lui posait une question, mais demeurait coi la plupart du temps, perdu dans ses rêveries et son monde de silence. Arrivé là au temps du premier roi Baudoin[^baudoinI], il était la mémoire vivante du casal, ayant gardé en ses méninges le souvenir des bornages, péages, coutumes et loyers de la plus infime parcelle de terre. Il était aussi un conteur enjoué, narrant avec brio, et pas mal d’humour, le demi-siècle auquel il avait assisté dans les lieux. On lui savait gré de ne jamais se risquer à dévoiler de faits trop fâcheux ou d’histoire trop grivoise, sauf en compagnie choisie.

Le troisième homme était Plat-Pié le Carrier. Cet ancien maçon au dos voûté et aux gigantesques mains percluses d’arthrose s’était mis en tête d’aider et de superviser le moindre aménagement dans le périmètre du casal. Apportant plus que volontiers son savoir-faire bénévolement, il s’attirait régulièrement les foudres de son épouse qui l’accusait de négliger son bien jusque dans l’église durant les offices. Il ne semblait guère en tenir compte et parcourait les lieux assis sur son vieil âne d’un air bonasse, accordant à chacun autant de temps qu’il l’estimait nécessaire. Cet empressement à aider autrui se doublait d’une curiosité maladive, qui lui donnait accès à bien des informations utiles pour ensuite délibérer au sein du conseil des jurés.

Sanson était venu les voir afin de discuter avec eux des habitudes de pacage des bêtes dans les communs.  Il était habituel pour les habitants du casal de laisser déambuler leurs troupeaux sur des terres libres, dont la collectivité avait la charge. En tant que nouvel arrivant, Sanson souhaitait découvrir ce à quoi il avait droit, par coutume, et les devoirs qui s’y rattachaient. Il s’était par ailleurs résolu à demander conseil pour le mariage de Libourc, sujet qu’il aborda une fois la question des animaux réglée.

« Si vous le permettez, j’aurais également une demande fort personnelle, vous sachant hommes de ces lieux. Nous avons convenu de marier notre fille Libourc à un jeune sergent le roi, en la Cité. Tout se fait selon les règles et je n’ai nulle raison de m’opposer à cette union. Mais mon épouse Mahaut est fort marrie de le voir porteur de l’épée. Je me disais que vous auriez peut-être possibilité d’apprendre de quoi la rassurer ? »

Pariset inclina son étroit visage avant de prendre la parole.

« Déjà, il me semble que servir le roi est bon gage. Il est au parmi de l’hostel depuis long temps ?

— Certes pas. Fraîchement débarqué avec son aîné Lambert, désormais établi à la Grande Mahomerie, il vient de Bourgogne. »

Le Carrier risqua un regard à son collègue avant de s’exprimer à son tour.

« Nous pouvons poser quelques questions au frère Géraud, le préchantre du Saint-Sépulcre. Nous avons eu maintes fois affaire à lui, avec grand bonheur. Il me serait étonnant qu’il n’ait pas moyen d’en savoir plus sur ce jeune homme, s’il ne le connaît pas déjà. Comment se nomme-t-il ?

— Ernaut. C’est un géant, bâti à chaux et sable, le verbe haut et la poigne ferme. Pourtant gentil garçon, de ce que j’ai pu en juger.

— Je pourrai aussi entreparler avec mestre Sicherius. Juré le roi en la Cour des Bourgeois de la cité, je le tiens pour bon ami. Il aura sûrement quelques savoirs propres à rassurer les vôtres, mestre Sanson. »

Sanson acquiesça. Sous couvert des inquiétudes de Mahaut, qu’elle avait clairement formulées, il n’estimait pas inutile de découvrir ce qu’on disait d’Ernaut autour de lui. Il n’avait guère pu en apprendre auprès de son curé de paroisse, vu que le jeune homme n’était pas là depuis très longtemps. Et Sanson ne connaissait personne dans son entourage qui puisse lui montrer une autre image que celle qu’il percevait de son probable futur beau-fils. L’excellente impression qu’il avait de Lambert, et donc de l’éducation qu’avait reçue Ernaut, ne pouvait suffire à rassurer un vieux père quant au destin de sa benjamine.

## Casal de Mahomeriola, demeure de Sanson de Brie, fin d’après-midi du mardi 28 juillet 1159

Sanson aimait à se rafraîchir après un long labeur. Il venait d’envoyer son valet Huet récupérer au pâturage les vaches pour les traire et s’accordait ce bref moment pour souffler. Torse nu, en braies, il se frottait de linges qu’il trempait dans un cuvier laissé à chauffer dehors toute la journée. Le soleil avait noirci sa peau, traçant sur son front dégarni une marque avec son crâne blanc, préservé sous son bonnet et son chapeau.

Lorsqu’il se pencha pour se rincer le visage dans l’eau claire, il grimaça. Il n’aimait guère s’y confronter avec ce qu’il était devenu avec les ans. Il gardait en tête les traits de sa jeunesse et ne faisait que difficilement le lien avec ce que l’impitoyable reflet lui proposait. Il détailla ses bras maigrelets, son ventre avachi… puis inspira longuement. La vie avait filé si vite ! Quand il regardait Mahaut, il la voyait toujours comme la belle femme qu’elle était au jour de leur mariage, plaquant sur son austère faciès ridé les minauderies de la jouvencelle d’alors. Cela le fit sourire, évoqua en lui le visage de sa petite Libourc.

Il chercha autour du cuvier le pot où était posé le savon et, ne le trouvant pas, se mit à pester contre Huet qui ne rangeait jamais rien. Il se leva, agacé, et alla dans la réserve. En entrant, il se cogna contre un tabouret appliqué là pour maintenir la porte grande ouverte. Il n’en ronchonna qu’avec plus de vigueur, se frottant le tibia. Avant de se prendre les pieds dans un amas de ficelle. Cette fois il ne pouvait s’en plaindre qu’à lui-même, qui l’avait jeté là. Évitant de peu de se retrouver au sol, il se résolut à laisser ses pupilles accommoder un moment, sans grand succès.

Cela faisait maintenant plusieurs mois qu’il avait remarqué qu’il y voyait de moins en moins bien, surtout quand la lumière se faisait rare. Sa main était également plus hésitante, et il montrait une maladresse inhabituelle dans ses gestes, ce que ne manquait pas de relever Mahaut. Il mettait à chaque fois cela sur le compte de la fatigue, mais il commençait à accepter le fait que c’était un problème d’yeux. Il n’était pas le premier de sa famille à qui ça arrivait. Rien n’était aussi net que dans son souvenir et il avait beau se frotter le visage, écarquiller les paupières, loucher… rien n’y faisait, il n’y voyait qu’à travers un voile de plus en plus épais.

Il finit par retrouver le petit pot de savon et retourna à la cuve, l’air renfrogné. Mahaut, qui revenait du jardin avec des légumes dans un panier, en fut frappée et s’inquiéta de ce qui le tourmentait.

« Rien d’importance, juste trop longue journée pour mes vieux os. On louera peut-être un autre valet pour la fin des travaux…

— Dis-tu cela pour quémander meilleure chère au souper ? J’ai prévu un bouillon de viande, mais je peux l’épaissir et y adjoindre une bonne compotée d’oignons frais… »

Devant le sourire complice de son épouse, dont elle n’usait que fort rarement en dehors du cercle intime, il n’eut pas le cœur de maintenir sa mauvaise humeur. Il hocha la tête avec enthousiasme.

« Ma douce, tu es comme la pluie sur une terre desséchée…

— Écoutez-le, beau damoiseau, le voilà à me conter fleurette ! »

Puis elle disparut avec gaieté dans l’escalier, chantonnant pour elle-même. Sanson se redressa, plissa les yeux. Il fallait vraiment qu’il se dépêche de marier sa fille avec un bon parti, en cas de malheur. Ernaut avait récemment prouvé à quel point il était prêt à s’investir pour défendre les siens[^voirt3] et cela confortait Sanson dans son acceptation de son union avec Libourc. Mais il était également conscient que Mahaut pouvait se montrer aussi épineuse qu’une rose. Et elle ne portait guère le jeune garçon dans son cœur, Dieu seul savait pourquoi.

## Mahomeriola, demeure de Sanson de Brie, soir du vendredi 25 décembre 1159

Sanson avait battu en retraite vers le sous-sol, chassé par sa femme durant la préparation du repas. D’humeur particulièrement taquine, il avait fini par s’attirer des reproches de ses agaceries.  Il était resté un temps assis à table, inquiet de provoquer une catastrophe à cause de sa maladresse, avec le peu de lumière. Puis finalement, muni d’une lampe, il était descendu. Il avançait précautionneusement jusqu’à la huche qu’il avait confectionnée pour Libourc. Il y enfermait tous les objets de prix, tissus et vêtements essentiellement, dont il était convenu qu’il les apporterait en dot pour le mariage. Mais il s’y trouvait un autre élément auquel il était tout particulièrement attaché.

Il déverrouilla le coffre avec la clef qui ne le quittait jamais et prit sur le paquet d’étoffes le petit cheval à roulettes qu’il avait sculpté sans le dire à personne. C’était, autant qu’il pouvait s’en souvenir, une copie de celui que ses enfants avaient poussé dans la cour de l’échoppe à Meaux. Libourc le reconnaîtrait sans aucun doute. C’était une façon pour lui de leur souhaiter une famille nombreuse, tout en créant un peu de lien avec ceux qui étaient demeurés en Champagne.

Il sourit en caressant la crinière qu’il avait tracée de son couteau, vérifia le bon fonctionnement des roulettes puis replaça le tout avec soin. Incapable de distinguer les motifs des magnifiques tissus serrés là, parmi les feuilles de menthe, il y laissa flâner la main avant de refermer le lourd couvercle. Il était content de cette huche, avait hésité un temps à faire peindre le devant avec décors locaux, mais la sobriété du meuble lui plaisait davantage.

Il se leva péniblement puis retrouva les escaliers, qu’il monta et se garantissant de la paume le long du mur, jouant de la clef entre ses doigts. Tout le monde l’attendait et il s’installa directement à sa place, heureux de n’avoir rien heurté en chemin. Il récita le bénédicité l’esprit ailleurs puis annonça qu’il était plus que temps selon lui de célébrer ce mariage tant espéré, dès après Pâques. Les deux jeunes gens en furent muets de stupeur et il sourit à la cantonade, satisfait de lui-même.

Il montra la clef et la tendit à Mahaut.

« Cette clef que je remets à ta mère sera la première qui ornera ta ceinture de femme, ma fille. Puisse-t-elle ouvrir la porte vers une vie joyeuse. »

Il se tourna vers Ernaut, le visage plus grave.

« Ce n’est pas en ce coffre que se tient mon plus précieux bien en cette terre, mon garçon. J’ose encroire que tu sais de quoi je parle. Sois bon mari pour elle, c’est tout ce qui importe. »

Inquiet de son ton pontifiant et de l’atmosphère solennelle qu’il venait d’instaurer, il sortit son petit canif et le déplia, l’air soudain exagérément enjoué.

« Mais en attendant, si nous faisions honneur à tous ces bons plats ? » ❧

## Notes

Le rôle des familles dans le mariage médiéval n’était pas forcément aussi prégnant qu’on pourrait le croire de prime abord. L’influence de l’Église, qui insistait sur le consentement des époux comme constituant essentiel de toute union, fut un moteur très important de cette évolution. Il demeurait malgré tout le fait que la création de nouveaux foyers périphériques intervenait dans un réseau social plus ou moins dense, en interaction avec les groupes impliqués. Chaque parent voyait son rôle bouleversé, la relation à son enfant devant être renégocié par le changement de statut que le mariage engendrait. Avec la naissance de la famille parentale au Moyen Âge en occident, cela signifiait aussi une évolution du rapport à la vieillesse, celle-ci ne se déroulant plus dans le cadre d’une famille élargie.

Il m’a donc semblé intéressant de réfléchir à la façon dont on pouvait évoquer cette dialectique entre ce que l’union consacrée entraînait pour le rapport entre l’enfant et le parent en complément de la vision que les fiancés pouvaient en avoir. Cela m’a paru d’autant plus pertinent que le fait d’être colon dans une région éloignée des cercles de relations habituels accentuait certainement les difficultés. Je n’ai malheureusement pas trouvé de travaux universitaires spécifiques sur cette notion de passage dans les âges de la vie pour nourrir plus précisément ce Qit’a au delà de ce que je pouvais extrapoler des recherches collatérales sur le mariage ou sur l’appréhension de la mort.

## Références

Brundage James A., *Law, Sex, and Christian Society in Medieval Europe*, Chicago : The University of Chicago Press, 1987.

d’Avray D. L., *Medieval marriage - Symbolism and Society*, Oxford : Oxford University Press, 2005.

Donahue, Charles Jr, *Law, Marriage, and Society in the Later Middle Ages*, Cambridge : Cambridge University Press, 2007.
