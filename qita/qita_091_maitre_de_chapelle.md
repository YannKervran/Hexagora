---
date : 2019-05-15
abstract : 1159. Gilbert, donné enfant à un monastère, doit quitter son existence routinière au service de l’évêque de Lydda afin de devenir le chapelain d’un puissant baron…
characters : Gilbert, Jaque, Constantin de Lydda, Géraud Granier, Renaud de Sidon, Remigio, Vital
---

# Maître de chapelle

## Lydda, palais de l’évêque, matin du lundi 9 mars 1159

Les yeux à demi ouverts, Gilbert s’étira longuement, emmitouflé sous les couvertures. La lumière du jour entrait à flots dans la petite salle qu’il partageait avec une demi-douzaine d’autres serviteurs de l’évêque. Comme souvent, il attendait pour sortir du lit le claquement de la porte de son dernier compagnon. Il savait pourtant qu’en tardant ainsi, il n’aurait peut-être même pas le temps de grignoter quoi que ce soit avant de se rendre à la messe matinale.

La capacité à se lever à toute heure était la première chose qu’il avait perdue en quittant le monastère clunisien où il avait grandi. À son arrivée à Lydda, il avait malgré tout mis un point d’honneur à ne manquer aucun office et continuait à suivre la stricte discipline qu’il avait toujours connue. Puis il avait vite remarqué que nombre de chanoines étaient souvent absents, surtout pour les heures nocturnes. De plus, étant simple clerc sans charge liturgique, Gilbert ne semblait tenu à aucune exigence sur ce plan. Il en conclut donc qu’il n’avait pas à être plus assidu que ses aînés dont c’était la mission. Il avait ainsi commencé à se relâcher.

Tout en bâillant, il enfila sa longue cotte de laine, ses sandales et se dirigea vers le sanctuaire. De sa voix chantante enrouée de sommeil, il saluait du bout des lèvres tous ceux qu’il croisait. En revenant de l’église, il passa au réfectoire des religieux et demanda pour sa collation du matin un peu de pain et une compotée de fruits divers, avec un fromage sec. Puis il s’installa à table, le regard dans le vague.

Il fixait sans le voir le grand crucifix accroché au bout de la pièce, à côté du lutrin où, certains jours, un des chanoines faisait la lecture. Il aimait à s’imaginer parfois que ce pourrait être lui. C’était là une des fonctions du préchantre, dont on estimait généralement qu’il était le successeur désigné du chantre lorsque celui-ci était rappelé à Dieu. Gilbert raffolait de musique et, par-dessus tout, chantait avec ferveur, de sa voix forte et pure. Oblat formé dès ses plus jeunes années à la complexe liturgie clunisienne, il ne se sentait jamais si bien que quand il laissait ses vocalises s’unir à celles de ses frères, en célébration du Très-Haut. Souvent il fredonnait des hymnes pieux pour lui-même, enchaînant les mélismes avec un ravissement qui se dessinait sur ses traits joufflus.

Lorsqu’il arriva dans sa salle de travail au petit lutrin où il officiait généralement, il inspecta machinalement ses plumes, pots d’encre, réglets et mines de plomb. C’était pour lui un rituel chaque matin, qu’il exécutait avec gravité, tout en saluant ses compagnons. Il avait progressivement pris l’habitude du papier, dont le comportement à l’écriture était différent du vélin. Il avait découvert de nouvelles recettes de couleur, plus adaptées, retenant avec application les dosages. Jaque, le scribe principal de l’évêque était généralement satisfait de son travail, appréciant de voir un élève aussi discipliné, même s’il le trouvait trop souvent perdu dans ses rêveries.

Ce fut d’ailleurs la toux insistante de son maître qui tira Gilbert de ses songes éveillés. Plusieurs rouleaux à la main, Jaque le toisait de ses yeux myopes.

« Sire Constantin te fait mander, Gilbert. »

Puis, voyant que le jeune clerc s’emparait d’une tablette de cire pour prendre des notes, il secoua la tête.

« Non, tu n’as besoin de rien, mais tu dois y aller séance tenante. »

Gilbert acquiesça et se dirigea avec célérité vers la grande salle d’audience. Chemin faisant, il ajusta sa tenue, se recoiffa d’un geste. Il était toujours inquiet de se faire ainsi convoquer, même s’il était rare qu’on l’admonestât pour quoi que ce fût. De toute façon, l’évêque était assez débonnaire avec les gens de son hôtel, et ses exigences étaient bien moins fortes que celles de l’écolâtre qui avait éduqué le jeune novice. La férule qui l’avait dressé tout au long de son enfance se rappelait néanmoins à sa mémoire chaque fois qu’il était mis en présence d’un supérieur d’une telle importance.

Lorsque Gilbert pénétra dans la grande salle, Constantin de Lydda n’était pas sur son trône, mais assis près d’une petite table, sur une chaise curule, occupé à discuter avec un homme aux cheveux gris coupés courts, à l’allure martiale, installé à côté de lui. Ils partageaient une collation de beignets aux fruits dont le parfum vint chatouiller les narines du clerc. Josce, le vervet de l’ecclésiastique, avait pris la place d’honneur sous le dais et s’activait à grignoter des amandes tout en surveillant les alentours. Il ne réagit pas à l’entrée de Gilbert, auquel il était habitué. Le clerc s’approcha sans bruit, puis inclina la tête, attendant respectueusement qu’on s’adresse à lui.

Les deux hommes parlaient de navigation maritime et du passage de certaines nefs espérées d’ici peu. Ils achevèrent rapidement et Constantin indiqua à Gilbert de venir plus près. L’hôte de l’évêque, d’un certain âge, le détailla des pieds à la tête. Il avait le regard perçant, mais dénué de violence et de méchanceté, attentif plus qu’inquisiteur. Et si ses gestes étaient martiaux, il n’avait pas sur le visage la sécheresse et la dureté de traits de certains barons autoritaires. Il arbora même un sourire amical.

« Voici donc notre jeune merle !

— Comme je vous l’ai dit, mon fils, il a l’usage des chants les plus complexes. Comme tout *nutriti* enfant de Cluny. »

Puis Constantin se tourna vers Gilbert.

« Tu as étudié assez pour les ordres mineurs et majeurs, si j’ai bonne souvenance ?

— De certes, mon sire. Mais n’ai pas été ordonné…

— Cela pourrait se faire vite assez. Géraud, que tu encontres ce jour, est seigneur, entre autres de la ville de Sagette, au nord du royaume. Il t’a ouï à vespres hier et désire te prendre à son service. Son chapelain, fort fatigué, a dû revêtir l’habit, et il n’est pas d’usage qu’une chapelle de telle importance demeure vide. Cela serait belle charge pour jeune clerc, qu’en dis-tu ? »

Familier des questions qui n’étaient que des injonctions formulées aimablement, Gilbert hocha la tête sans faire entendre sa voix.

« Je vais rédiger une lettre pour l’évêque Amaury, que tu puisses recevoir les ordres de sa main. D’ici là, tu pourras tout de même célébrer quelques offices, des morts en particulier. Sire Géraud tient à honorer ses ancêtres. Un clunisien tel que toi doit savoir cela parfaitement ! Va prévenir Jaque des tâches que tu avais en cours et sois prêt à partir avec l’hostel du sire Sagette demain dès potron jaquet. »

Tout en signifiant son congé d’une main distraite à Gilbert, l’évêque se tourna vers le seigneur de Sagette et lui sourit aimablement.

« Vous voilà de nouveau garanti pour votre âme, mon fils. Et par bon clunisien dont je ne me sépare qu’à regret… »

Gilbert salua d’une inclinaison du buste et se retira en silence. Puis il dirigea ses pas vers le scriptorium. Il voyait son univers familier se dissoudre autour de lui, ce qui l’angoissait au plus haut point. Il n’était à Lydda que depuis quelques années, mais son tempérament routinier avait vite tracé un sillon dont il ne s’écartait que peu. Devenir chapelain, cela voulait dire vivre sans clôture, dans le monde séculier. Et surtout, sans supérieur direct auquel obéir chaque jour. Rien que d’y penser, il sentait le trouble gagner son cœur, chavirer son estomac. Il lui faudrait visiter les cuisines au plus vite, voir s’il ne se trouvait pas un petit quignon sur lequel passer ses inquiétudes.

## Château de Beaufort, après-midi du dimanche 14 juin 1159

Installé sur un des petits bancs de la chapelle, Gilbert jouait doucement de son psaltérion. La chaude lumière pénétrait par les fines ouvertures, faisant danser la poussière grise soulevée par le balayage récent qu’il avait demandé. L’autel était paré, surmonté d’une jolie fresque un peu naïve représentant un christ en majesté. Il se sentait désormais moins angoissé à l’idée d’être en charge du service divin. En outre, si les célébrations qui allaient commencer au soir étaient d’importance, messe des morts en mémoire d’un ancêtre particulièrement vénéré, Eustache, c’était là un cérémonial que Gilbert connaissait parfaitement. Il regrettait juste que les participants ne fassent qu’ânonner à sa suite, incapables qu’ils étaient d’accompagner ses envolées vocales.

Il aimait bien la petite chapelle du château, profondément enchâssée dans l’énorme massif de pierre. S’il avait appréhendé la montée jusqu’à ce nid d’aigle, il se sentait à l’abri tant qu’il n’approchait pas des murs d’enceinte. Il s’était installé une couche dans le fond de la salle et avait son coffre personnel, qui lui servait souvent de table. Il avait trouvé étrange de disposer ainsi de biens propres en si grande quantité. De plus, il percevait une dotation qui lui permettrait de s’acheter des instruments de musique de qualité. Au début, il avait été horrifié de découvrir qu’on lui donnait de l’argent pour le service de Dieu, puis il avait compris que c’était nécessaire, vu que certaines choses ne lui étaient plus fournies. Il distribuait néanmoins beaucoup en aumônes.

Il avait aussi constaté qu’on lui conférait une importance à laquelle il n’était pas habitué. Il était désormais servi au haut bout de la table, là où une nappe de lin venait habiller le bois ciré. Il était même juste à côté du maître lorsque celui-ci n’avait pas ses fils avec lui. Cela le mettait encore plus mal à l’aise, et il gardait généralement les yeux rivés sur son tranchoir, ne répondant qu’aux questions directes, et toujours avec quelques propos laconiques. Il aimait que Renaud, l’un des cadets, soit présent. Plus âgé que lui d’une dizaine d’années, il était beaucoup moins affecté dans ses façons, sachant qu’il n’avait aucune responsabilité officielle. C’était un homme de guerre, à l’aise avec l’épée au côté, mais il s’était montré également le plus curieux et le plus amical avec Gilbert. Il avait réussi à l’attirer à quelques veillées, lui apprenant des jeux de table.

Par sa fonction, Gilbert célébrait des offices réguliers pour le seigneur Géraut et sa famille et les entendait en confession. Il avait été horriblement gêné de devoir se charger aussi des femmes, et rougissait de les écouter ainsi, obligées de lui dévoiler leur intimité. Heureusement pour lui, aucune faute avouée ne lui avait jusque là posé de problème, rien de grave ou de non répertorié dans les pénitentiels n’ayant été porté à sa connaissance. Il se doutait par ailleurs qu’il lui faudrait gagner la confiance de chacune des personnes avant qu’elles n’osent lui en dire plus. Il allait devoir apprendre à écouter.

Un autre avantage de la forteresse de Beaufort était de se situer loin de tout, avec une faible garnison. Gilbert se sentait souvent perdu dans la grande ville de Sidon, et il n’aimait guère devoir sortir de la Tour-Baudoin, le château seigneurial. En outre, cela signifiait qu’ici il ne partageait le lieu de culte avec personne. En ville, il devait composer avec un prêtre plus âgé que lui, qui semblait n’avoir accueilli un jeune collègue qu’avec réticence. Peut-être avait-il espéré la place, plus prestigieuse et vraisemblablement mieux dotée que sa cure. Gilbert s’efforçait pourtant de bien marquer son respect des formes, allant à confesse chaque semaine et suivant les messes de son confrère avec application.

Lorsque la porte grinça sur ses gonds, il suspendit son plectre, prêt à saluer quiconque se présentait là. En l’absence de paroisse, les permanents de la garnison tombaient sous sa juridiction spirituelle, mais la plupart montraient bien peu d’appétit pour les questions de cet ordre. Ils se contentaient d’être présents au moins le dimanche à la messe, s’abstenant de bavarder uniquement quand les maîtres étaient aux environs.

En voyant Renaud, il se leva et l’accueillit d’un sourire. C’était un homme assez svelte et tout en muscle, le port bien droit né de toutes ses années en selle, avec un visage extraordinairement laid. Gilbert savait que c’était souvent là le signe d’un esprit retors, ou du moins de quelque infirmité d’âme, mais il n’arrivait pas à en faire grief à Renaud. Celui-ci était par ailleurs certainement le plus instruit de la maisonnée.

« Vous allez finir par prendre la poussière à demeurer ainsi à l’ombre, mon père ! lança le jeune chevalier, un sourire aux lèvres.

— Je n’ai pas grand plaisir à aller au soleil, cela échauffe les sangs.

— Surtout ce jourd’hui. La chaleur coule comme plomb fondu. J’ai épuisé trois montures à la chasse ce matin. Mais au moins j’ai ramené tendre gazelle qui égayera nos soupers ! »

Gilbert ne put retenir un sourire gourmand. Il savait qu’il serait parmi les premiers à se servir dans les beaux morceaux et les plats de sauce. Renaud avait vite compris la façon de lui plaire.

« Je venais vous chercher pour vous proposer une partie d’échecs. Je ne désespère pas de vous y faire prendre goût ! Père déteste ça et nul ici n’a votre jugeote.

— Je ne suis pas encore certain que ce soit là jeu bien chrétien…

— Allons donc. On n’y lance nul dé ! Et on dit que le saint prédicateur Bernard lui-même ne les dédaignait guère. N’était-ce pas votre abbé ?

— Il était abbé de Clairvaux et j’ai grandi dans une fille de Cluny !

— Que voilà bien cléricale finasserie ! N’est-ce pas couvent les deux fois ?

— C’est aussi différent pour moi qu’un alesan et un pie pour vous » répliqua Gilbert, tout content de replacer une nuance qu’il n’avait intégrée que depuis peu.

Renaud éclata de son rire joyeux et ouvrit en grand la porte, invitant le jeune chapelain à le suivre. Malgré ses manières familières et amicales, il était bien entendu que ses demandes ne devaient jamais être comprises autrement que comme des ordres.

## Sagette, château de La Tour-Baudoin, veillée du jeudi 10 septembre 1159

La grande salle bruissait des activités joyeuses d’une fin de veillée festive. Géraud Granier, seigneur de Sagette, recevait quelques chevaliers croisés qui allaient reprendre la mer d’ici quelques jours et une magnifique soirée d’adieu leur était offerte. Quelques molles promesses d’union matrimoniale avaient été évoquées, mais sans que personne n’y croie vraiment. Géraud tenait néanmoins à ce que la réputation de la terre de Sagette soit bonne jusqu’au cœur des plus lointaines principautés. Il avait encore des enfants à placer, dont Renaud était le plus bouillonnant exemple.

Gilbert avait eu le plaisir de passer du temps avec des voyageurs venus de Saxe, se replongeant dans son dialecte natal avec bonheur. Il était rare de voir des croisés originaires de cette région, leur suzerain Henri le Lion étant généralement plus favorable à des expéditions contre Niklot, un seigneur païen abodrite établi non loin de ses territoires. C’était aussi pour Gilbert la première fois qu’il assistait à un spectacle de théâtre d’ombres. Le sujet racontait les exploits de Godefroy de Bouillon lors de la première croisade, en un abrégé des Chansons habituelles. L’accent y était mis sur le caractère exceptionnel de ce héros et certaines de ses aventures semblaient proprement incroyables. Gilbert tint donc à s’en entretenir avec les jongleurs, un homme d’une trentaine d’années accompagné d’un petit garçon. Ils rangeaient leurs marionnettes de cuir quand le prêtre vint les chercher.

Il savait qu’il entrait dans un univers inconnu, ayant été prévenu contre les artistes ambulants depuis sa plus tendre enfance. Il était néanmoins intrigué de voir qu’ils étaient capables, malgré toute l’infamie de leur condition, de célébrer des sujets inspirants. S’il était un thème qui rassemblait tout le monde en Terre sainte, c’était bien la figure de Godefroy.

L’homme l’accueillit d’un sourire poli, sans crainte, puis inclina la tête avec respect, le reconnaissant à sa tonsure comme un ecclésiastique. Il donna un coup de coude au gamin pour que celui-ci exécute également un salut dans les formes.

« Pouvons-nous vous aider, mon père ?

— De certes. De qui tenez-vous ces amiables récits ? J’y ai trouvé quelque matière propre à édifier. Et l’exemple venant de si haut, il n’en est que plus motivant.

— La plupart sont des extraits des chansons de celui qu’on nomme Richard, pérégrin des premiers temps. Un certain nombre est né de choses que j’ai entendues de différents jongleurs, quoique je confesse avoir qui-ci qui-là fleuri de menus détails, souventes fois pour la rime.

— Oh, vous versifiez ?

— J’ai ce plaisir, certes. J’ai étudié quelques années, pour savoir mes arts assez. Du moins suffisamment pour tirer de cette pauvre caboche de quoi enjoyer laboureurs et barons. »

Gilbert était tellement ébahi de voir que certains saltimbanques avaient donc quelques lettres qu’il en oublia sa déception à la nouvelle que les exploits de Godefroy étaient en grande partie imaginaires.

« Concevez-vous également les mélodies de vos chants ?

— Certes. J’emprunte ici et là de quoi garnir ma besace, mais ce qui en sort est bien de moi. »

Gilbert sourit de toutes ses dents. C’était la première fois qu’il s’entretenait avec un compositeur qui créait des pièces originales, selon sa propre inspiration. Il crut un instant se trouver au paradis.

« Auriez-vous un peu de temps pour parler musique ? »

## Sagette, château de La Tour-Baudoin, matinée du mardi 15 décembre 1159

Une large table était disposée dans un des coins de la grande salle d’audience lorsque Géraud ne recevait pas. Le personnel de son administration s’y installait pour établir les comptes, rédiger les missives ou recopier les chartes. Gilbert avait pris l’habitude d’y travailler, profitant de la tiédeur de la pièce et de la calme activité qui y régnait. C’était l’endroit qui lui rappelait le plus le monastère. En dehors du seigneur de Sagette et de sa famille, nul n’élevait la voix et bien peu parlaient haut.

Il y préparait avec application ses célébrations sur de petites tablettes de cire et y venait lire les quelques ouvrages dont la chapelle était dotée. Il s’y trouvait un épitomé du pénitentiel de Burchard de Worms, qu’il avait toujours connu. Il regrettait juste que c’en soit une forme abrégée, car il se sentait de temps à autre décontenancé par les péchés auxquels il était confronté. Tout ce qui ressortissait des soucis de conflit armé ou des relations avec la femme lui était difficile à appréhender car il n’avait jamais eu à se frotter à ces domaines jusque là. Il avait donc emprunté à la bibliothèque de l’évêque de Sagette une version plus complète des écrits de Burchard et se confectionnait un addendum pour ces questions.

Il en était à un point qui le tracassait au plus haut point, et qui concernait les interdits alimentaires : *Comedisti de cibo Judaeorum vel aliorum paganorum quem ipsi sibi prapeparverunt*[^burchard190]. Ce *aliorum paganorum* semblait indiquer que chaque fois que Géraud ou, plus souvent Renaud, partageait un repas avec les mahométans en tant qu’émissaire, il devait faire ensuite pénitence dix jours au pain et à l’eau. Ce qui n’était pas sans poser problème, d’autant qu’ils n’avaient ni l’un ni l’autre jamais avoué encore pareil péché. Quand bien même Gilbert savait pertinemment qu’ils s’étaient déjà trouvés dans ce cas. Il nota la question pour l’aborder avec son propre confesseur, voire avec l’évêque.

Estimant qu’il avait assez travaillé pour le matin, il alla marcher un peu dans la grande cour. Malgré le soleil, un vent descendu des montagnes apportait de la fraîcheur et il avait enfilé des chausses de laine épaisses. C’étaient là une des commodités de la vie séculière qu’il aimait assez, de pouvoir se vêtir de façon confortable. Il continuait néanmoins de porter de modestes et informes cottes longues, incapable d’assumer l’indécence de les couper au genou. Il prenait par contre grand soin de sa tonsure, indicatrice de son état de clerc. Au milieu du peuple, il était toujours inquiet qu’on l’aborde comme un simple quidam et pas avec le respect dû à sa fonction. Ce n’était nullement par orgueil, mais par crainte de ne pas réagir correctement au milieu de laïcs méconnaissant son état.

Lorsqu’il revint dans la salle, il vit qu’on commençait à dresser la table pour le repas. Géraud était présent et discutait avec plusieurs de ses chevaliers des campagnes de l’année. Il était à la fois heureux de n’y avoir rien perdu, mais aussi un peu déçu qu’aucune conquête n’ait pu se faire malgré la rencontre avec le basileus byzantin. Il trouvait qu’on faisait bien grand cas d’un souverain qui n’avait pas à cœur les intérêts des territoires latins. Il expliquait que son père, l’éminent Eustache, *l’épée et le bouclier du royaume*, avait toujours eu profonde méfiance de ceux qu’il considérait à peine moins que des hérétiques. Géraud n’était pas loin de partager son avis.

Sans se faire remarquer, Gilbert alla ranger ses affaires, glissant soigneusement les codex dans des étuis de cuir et classant ses feuillets dans un portfolio. Dans un coin du coffre où il laissait tous ses écrits, il aperçut les notes qu’il tenait de Herbelot. Il n’y avait guère travaillé, manquant d’inspiration, trouvant l’entreprise trop ambitieuse pour lui. Il s’en voulait un peu de conserver ainsi un tel projet sans rien en faire. Puis il eut soudain une idée : il pouvait se servir de ces idées édifiantes jetées pêle-mêle pour des conseils à son seigneur. Mais de façon détournée peut-être, en s’adressant justement à ce fameux roi byzantin, ce Manuel Comnène, en le prenant à parti. Ses propos n’en auraient que plus de facilité à se faire accepter par ceux dont il avait la charge d’âme. ❧

## Notes

Le statut des clercs est extrêmement varié et leurs fonctions au sein de la population pouvaient prendre de nombreuses formes. Vu qu’ils constituaient la frange la plus littéraire de la société, ils nous ont laissé, par chance, une documentation assez conséquente sur cette société parallèle dans laquelle ils évoluaient. Cela n’en rend pas forcément la découverte et le parcours aisé pour les néophytes séculiers que nous sommes pour la plupart. On ne peut néanmoins pas se résoudre à accepter comme suffisamment évocatrices les maigres caricatures que l’on nous assène sans discontinuer du moine paillard, du curé inquisiteur ou de l’évêque politicien.

La pléthore de fonctions qu’ils avaient à remplir créait autant de caractères qu’il y avait de situation, et la lecture des sources nous dévoile une infinie variation de vies. Malgré toutes les tentatives de réforme, de contrôle par les hauts dignitaires, il n’existait aucune homogénéité et des traditions dynamiques s’opposaient parfois, régionalement ou culturellement. J’ai trouvé intéressant de montrer le parcours intellectuel que pouvait connaître un jeune oblat dont le destin aurait voulu qu’il soit forcé de sortir de la clôture qui l’avait toujours protégé. Car c’est aussi la période où l’on estime que le service de Dieu ne peut être envisagé que par choix librement consenti.

## Références

Avril Joseph, « En marge du clergé paroissial : les chapelains de chapellenies (fin XIIe-XIIIe siècles) » dans *Actes des congrès de la Société des historiens médiévistes de l’enseignement supérieur public. 22e congrès*, Amiens, 1991. pp. 121-133

Casagrande Caria, Vecchio Silvana, « Clercs et jongleurs dans la société médiévale (XIIe et XIIIe siècles) », dans *Annales*, Année 1979, Volume 34, Numéro 5, p. 913-928

de Miramon Charles, « Embrasser l’état monastique à l’âge adulte (1050-1200). Étude sur la conversion tardive », dans *Annales*, Année 1999, Volume 54, Numéro 4, p. 825-849

Devailly Guy, « Le clergé régulier et le ministère paroissial » dans *Actes des congrès de la Société des historiens médiévistes de l’enseignement supérieur public. 5e congrès*, Saint-Étienne, 1974, p. 151-164.

Gagnon François, *Le Corrector sive Medicus de Burchard de Worms (1000-1025) : présentation, traduction et commentaire ethno-historique*, Mémoire de Master, Université de Montréal, Département d’histoire, Faculté des arts et des sciences, 2010. [En ligne], consulté le 19 mars 2019, Adresse URL : <https://papyrus.bib.umontreal.ca/xmlui/handle/1866/4915>

Hilton Suzanne M., *A Cluniac office of the Dead*, Thèse de Master of Arts, Université du Maryland, College Park, 2005.
