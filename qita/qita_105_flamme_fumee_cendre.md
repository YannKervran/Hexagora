---
date : 2020-07-15
abstract : 1161-1163. Droin, fils de porcher des faubourgs de Vézelay découvre la vie de nomade dans les sables du Sinaï et les vallées du Negev.
characters : Droin, Nijm, al-Saghir, al-Shada’id al-Namari ibn Musa, Sviatoslav Shuba, abu al-Fazari
---

# Flamme, fumée et cendre

## Bilbais, berges du Nil, fin de journée du jeudi 21 septembre 1161

Assis en haut du talus qui plongeait de façon abrupte dans l’eau fangeuse, Droin laissait son regard divaguer sur les voiliers et esquifs à rames, sur les pirogues de roseau et les ambitieux transports au pont encombré de balles de marchandises. Dans l’île face à lui, des chèvres et des moutons broutaient paisiblement à l’ombre de palmiers au pied moussu, indifférents aux crocodiles se gavant de soleil à quelques encablures de là. Une nauséabonde fragrance venait lui titiller les narines, se mêlant aux subtiles odeurs du marché dont il entendait la clameur derrière lui. Enrubanné d’étoffes diverses pour se protéger des ardeurs de l’astre triomphant, il avait terriblement chaud. Il sentait la sueur qui se formait en gouttes sur ses flancs, imprégnant les tissus qui collaient à sa peau.

Il mordit dans le pain plat frotté d’huile que lui avait tendu Nijm puis dans un rude fromage, mâchant lentement, laissant les saveurs exploser en bouche en écho à l’exubérance qui s’offrait à sa vue, son nez, ses oreilles. Pour la première fois de sa vie, il contemplait le Nil.

Cela faisait des mois que ses désormais compagnons de voyage, la tribu des banu Jarm[^voirqitadesertpoing], évoquaient le mystérieux fleuve qui avait donné naissance à Misr et qui lui redonnait un nouveau souffle chaque année. Les Bédouins y venaient de temps à autre afin d’échanger leurs paniers en feuilles de palme, leurs accessoires en cuir de chèvre et de chameau ou le sel de la mer Morte contre des étoffes, du riz ou des roseaux fins. Le sheikh, al-Saghir, avait décidé cette fois de se rendre jusqu’à la cité frontière de Bilbais pour y entendre les dernières nouvelles. De nombreux voyageurs racontaient que de terribles conflits agitaient le pouvoir califal fâtimide. Vivant à la marche des deux royaumes, latin et musulman, il était vital de savoir si des opérations militaires étaient à envisager, pour s’en protéger, y participer du côté le plus apte à remporter la bataille, ou dans l’espoir de se trouver dans les parages lorsque le pillage serait profitable sans risque.

Droin n’était pas à l’aise avec cet opportunisme, mais il se gardait bien de faire le moindre commentaire déplacé, surtout à Nijm qui le traitait tel un frère. Mieux même, vu que les seuls qu’il ait jamais eus, enfant, se comportaient comme son père violent, fainéant et désabusé. Il goûtait juste la compagnie des Bédouins, tolérant leurs façons de faire pour répondre à leurs nécessités en cette terre de sable, de roc et de pierre. Ils étaient la première vraie famille qu’il ait jamais eue.

En contrebas, une longue felouque s’encombrait peu à peu de balles de toile cirée, de paniers et d’outres, de vases et de sacs de grains et de pois. Les senteurs épicées montaient jusqu’à lui, au rythme des rafales qui faisaient claquer le pan de voile mal fixé au mât oblique. Les portefaix s’invectivaient pour emprunter les minces planches d’accès à bord, sous le regard indifférent des matelots, plus concernés par l’assiette du navire et le bon arrimage de la cargaison. En arrière-plan, une fumée grasse s’élevait depuis la palmeraie qui occupait une île proche, peut-être d’une plantation de cannes à sucre ou d’une fabrique de céramiques. Mais elle ne survivait guère aux assauts de la chaleur et du constant zéphyr.

Droin se tourna, flatta le petit chien qui s’était attaché à ses pas depuis quelques semaines, puis lui offrit un croûton. C’était une brave bête, destinée à garder les chèvres et alerter si des prédateurs rôdaient. Droin avait entrepris de le dresser à faire des cabrioles, lui apprenant à sauter pour le plaisir des enfants. C’était une façon pour lui de se faire accepter par ses nouveaux compagnons, encore hésitants sur la manière d’intégrer ce nouveau venu, même après les mois passés ensemble. La plupart se montraient assez amicaux et de bonne volonté, mais aucun n’était aussi ouvert que Nijm et il manquait cette familiarité instinctive née d’une constante proximité pour que Droin se sente parfaitement à l’aise.

Le marché auquel il tournait le dos était un souk de négociants. On y croisait des marchands de retour d’Adan[^adan] ou sur le départ pour al-Sham[^bilad_al_Sham], pour le lointain al-Sūdaan[^alsudaan] ou encore de plus exotiques et distantes contrées dont le nom n’évoquait que villes mystères et territoires inconnus. Droin avait abandonné l’espoir de s’y repérer, profitant de la magie de ces noms pour nourrir ses appétits de merveilleux.

« Le vizir est mort. Les amis d’hier se déchirent pour savoir qui sera le maître à Misr. »

La voix étonnamment jeune du sheikh interrompit Droin dans ses rêveries. Le vieil homme venait de s’accroupir vers eux et s’empara d’autorité de l’outre avant d’en avaler une longue rasade. Puis il cracha un peu d’eau dans sa main afin de s’en rafraichir le visage. Le soleil luisait sur la peau cuivrée de ses pommettes, accentuant la noirceur des fosses plissées qui abritaient ses yeux sombres.

« On m’a recommandé à un marchand désireux de remonter vers le nord, au-delà d’Anṭākiyya[^antakiyya], chez les Rums[^rum]. Il transporte surtout épices, noix de bétel et poivre. Il craint de prendre la mer en cette période. C’est un ami de al-Shada’id al-Namari ibn Musa. Nous pourrons lui faire escorte jusque Gaza, voire al-Khalil[^alkhalil] selon la route qu’il choisira. »

Nijm hochait la tête par pur conformisme, personne n’attendant que quiconque mette en question une décision du sheikh. C’était lui qui statuait sur tout dès l’instant où cela impliquait la tribu en son entier. Son pouvoir s’arrêtait à l’entrée de chaque toile, mais s’appliquait en dehors sans la moindre contestation. Du moins tant qu’il ne se trouvait pas un chef de famille plus apte à attirer des fidèles par ses avis et suggestions. Auquel cas on n’appellerait plus al-Saghir qu’abu Sa’id, nom de son aîné, et il rejoindrait le conseil des anciens, mémoire et conseil de la tribu, vénérables assistants du sheikh, soumis à son autorité.

Al-Saghir embrassa le paysage d’un rapide coup d’œil, puis fit un rictus à Droin, en ce qui tenait chez lui d’un sourire.

« Le fleuve a bien nourri la terre. Nous sommes à plusieurs semaines de son point haut et les îles n’ont pas toutes réapparu. Les mois à venir seront prospères alors que les princes se battent. Ce sera une bonne année pour nous, avec toutes ces richesses amassées par les hommes des villes et aucun bras puissant pour nous empêcher d’y prendre notre dû. »

## Faubourgs de Gaza, camp nomade, matin du vendredi 9 novembre 1162

Encore ensommeillé, Droin s’efforçait de réveiller ses sens par quelques vigoureuses ablutions près du bassin où leur modeste campement était établi. Il avait profité la veille des bains de la ville, luxe auquel il s’adonnait à la moindre occasion. Il sentait avec ravissement les effluves de savon qui s’attachaient à sa peau. Ils étaient quelques hommes de la tribu installés aux abords de la localité franque, espérant quelques menus travaux pour l’hiver afin de gagner de quoi s’offrir les raffinements citadins qu’ils ne pouvaient tirer du désert.

Quelques tentes noires formaient une cour centrale où ils tenaient les chevaux, autour d’une poignée de genêts épineux et d’un vieux pin d’Alep aux branches tortueuses. Droin avait envie de faire bonne impression pour sa prestation auprès du sheikh. Celui-ci le sollicitait chaque fois qu’il fallait s’entretenir avec des Latins. Même si c’était souvent laborieux pour Droin, qui ne comprenait que de loin en loin les idiomes trop différents du bourguignon, il y voyait une opportunité d’apporter sa contribution à la tribu. C’était pour lui une responsabilité dont il s’enorgueillissait.

Il finissait de nouer son turban, à la nomade, avec des pans sous le menton, quand il remarqua les Francs dont Nijm lui avait parlé. Il s’avança vers eux afin de les intercepter le premier. Il identifia un solide gaillard aussi hirsute qu’un ours comme étant le chef. Une tête mangée de barbe, deux énormes oreilles en chou-fleur servant de cadre à un imposant orifice nasal trônaient sur un large torse. Droin les salua en langue d’oïl et d’oc, principaux langages du royaume, tout en s’inclinant poliment. Le petit groupe se figea et le meneur le dévisagea avec circonspection avant de répondre en langue d’oïl avec un fort accent, mais de façon intelligible. Ses trois acolytes se contentèrent de hocher le menton.

La tente d’accueil avait été préparée avec soin, comme chaque fois qu’une négociation allait se dérouler ou que des étrangers entraient en contact. Au centre, le sheikh était assis à la place d’honneur, sur un coussin, avec les hommes les plus importants de leur modeste ambassade autour de lui. À ses genoux, quelques plats cuisinés par les femmes avant leur départ ou achetés à des commerces locaux allaient offrir l’occasion d’échanger des banalités avant d’en venir au fait. Puis des nattes soigneusement balayées avaient été déroulées pour les Francs peu au fait des coutumes et qui n’hésiteraient pas à garder leurs souliers pour s’installer. Droin les invita à prendre place puis se concentra sur son travail d’interprète.

C’était pour une fois une tâche aisée, il s’agissait qu’un petit groupe de soldats de fortune qui avaient appris l’appel aux armes de l’ancien gouverneur de Moyenne Égypte, Šāwar. Arbalétriers expérimentés, ils savaient que leur compétence serait appréciée en ces périodes plutôt calmes dans le royaume chrétien. L’émir d’Alep et principal instigateur des combats contre les Latins, Nūr ad-Dīn était en pèlerinage à la Mecque. De son côté, Baudoin se trouvait trop occupé à gérer les territoires sans dirigeant pour envisager des campagnes d’ampleur nécessitant un recrutement.

Droin remarqua que, comme à son habitude, le sheikh, tout en discutant âprement le montant de leur assistance pour traverser le désert méridional, ne laissait échapper aucun commentaire quant aux chances de succès de ces mercenaires. Ce n’était pas la première fois que des Latins allaient ainsi louer leurs bras et leurs lames à des princes d’autres religions, mais il était rare qu’ils en reviennent plus riches. Difficultés de s’adapter à de nouvelles façons de faire, hostilité plus ou moins manifeste des populations, duplicité des dirigeants, rien n’était épargné à ces hommes capables de verser le sang pour remplir leur ventre. Pour avoir un temps envisagé de vivre comme eux, Droin appréciait de ne pas partager leur sort.

Une fois parvenus à un accord, les soldats ne restèrent que peu, montrant là aussi leur méconnaissance des usages. Ils se comportaient selon leurs habitudes, sans chercher à s’intégrer à ceux qui allaient les guider jusqu’aux rives du Nil. Ils seraient donc traités ainsi que des bêtes qu’on doit mener, avec rigueur et attention, mais sans complaisance. Ils n’étaient qu’une tâche à accomplir, dont la tribu, toujours inquiète de sa réputation et de son honneur, se chargerait avec sérieux et application, mais sans volonté de se lier. Des étrangers partageant le même chemin.

Droin raccompagna les Latins et retourna à la tente. Il était d’usage que chacun fasse ses commentaires sur les gens que le groupe allait escorter. On ne revenait jamais sur la transaction, mais les modalités d’accueil étaient supputées, les sentiments, craintes et espoirs de chacun étaient entendus et soupesés. Au fil des mois et des voyages, Droin avait senti que la confiance, bâtie sur l’acceptation sans réserve de Nijm, se renforçait. Il avait apprécié à sa juste valeur qu’al-Saghir approuve une de ses remarques, quelques mois plus tôt, en lui attribuant de façon solennelle le nom que chacun lui donnait faute de mieux. Et ce n’était pas un moindre honneur que le vieux sage le désigne ainsi, de ce sobriquet initié par plaisanterie par Nijm : *al-Nazif*[^alnazif]. Depuis, il s’en était fait une véritable identité, veillant à sa tenue, à sa propreté comme il ne l’avait jamais fait jusque-là. Il se sentait enfin lavé des sarcasmes infantiles.

Tout en discutant, les hommes finissaient les plats, dont un succulent sumaghiyyeh[^sumaghiyyeh] et du poisson frit, mariné et farci. Mais le principal attrait de la ville demeurait le zibdieh, réchauffé au maigre feu du camp, et les plus gourmands se brulaient les doigts en tirant les crevettes du ragoût fumant.

« Il nous faudrait encore un ou deux commanditaires et nous pourrons prendre la route, indiqua al-Saghir. Mais pas des soldats. Marchands ou voyageurs seulement. Il est toujours mauvais que des lames inconnues soient en trop grand nombre parmi les nôtres.

— Si la guerre se prépare à Misr, les hommes voudront rentrer protéger les leurs. Nous avons plus d’un mois avant le départ prévu. Nous pourrions envoyer quelques frères à Asqalan[^asqalan] battre le rappel.

— J’en ai parlé à quelques-uns de nos familiers ici, attendons de voir. Je n’aime pas savoir nos fils au loin en ces terres de villes malodorantes et de forteresses de pierre. »

Comme tous les nomades, al-Saghir vouait une grande méfiance aux provinces urbanisées, aux territoires aménagés et avait plus confiance dans le lion, le crocodile ou la panthère que dans le citadin dont il n’escomptait guère de bien. Et il était d’une circonspection quasi maternelle envers les gens de la tribu.

« Je pourrais aussi aller traîner auprès des entrepôts génois ou pisans, proposa Droin. Ces hommes ont le commerce dans le sang, ils ne sauraient refuser les perspectives de profit des luttes à venir. »

Alors qu’il allait avaler une bouchée, le sheikh suspendit son geste, visiblement surpris qu’al-Nazif prenne la parole spontanément pour la première fois, et pas en réponse à une question. Puis il hocha le menton, avant d’engloutir la crevette. Face à Droin, Nijm était tout sourire.

## Désert du Negev, abords du canyon du Nahal Zin, nuit du youm al arbia 10 chawwal 558^[Mercredi 11 septembre 1163.]

La lueur de la lune ne pouvait concurrencer les flammes dansantes qui peignaient d’ambre et d’ocre les falaises environnantes, les peupliers et les arroches des abords du camp. Le clan Tha'laba s’était regroupé au fil des jours et les derniers attendus avaient établi leurs tentes la veille. Il était donc plus que largement temps de célébrer l’événement : les sheikhs avaient convenu de se retrouver là pour parler de l’avenir des familles. Des moutons étaient marchandés, des chameaux accouplés, des alliances négociées, des naissances préparées et les troubles en Égypte alimentaient de nombreuses discussions pleines d’espoir.

C’était la première fois qu’al-Nazif participait à de telles réjouissances. Il regardait tout cela comme dans un rêve, aidant les gens de sa tribu à installer le camp, entretenir les feux ou à choisir les bêtes abattues pour les repas. Il habitait toujours la tente de Nijm, désormais marié, et y vivait comme son frère. Les deux hommes partageaient leurs biens et leurs moutons, et le petit Hamid l’appelait « mon oncle » de façon très naturelle.

Al-Nazif et Nijm finissaient de s’habiller pour la fête, ajustant leur coiffure, lissant leurs barbes et nouant avec soin leur ceinture. Ce soir allait se danser le dabkeh, chaque tribu faisant démonstration de ses talents avant que tous se mêlent en une immense célébration. Al-Nazif avait l’habitude de se joindre aux autres, mais il sentait toute l’importance du moment. Bien que tous appartinssent au même clan, il fallait défendre l’honneur de ses proches. Et il savait que les langues allaient bon train sur son compte. Il demeurait un étranger pour beaucoup de ces fils du désert et le plus petit manquement rejaillirait sur les Banu Jarm, qui en lui feraient payer le prix d’une façon ou d’une autre. Il avait veillé à une stricte observation des prières depuis le début du rassemblement et ne relâchait son attention à ses moindres gestes qu’une fois de retour dans sa toile, à l’abri des murs de broussailles, parmi les siens.

« Ne sois pas si inquiet, frère, tu n’es pas, et de loin, le plus mauvais danseur parmi nous. Et le ras^[Meneur de la danse.] de ce soir n’est pas tant aventureux…

— C’est facile pour toi, qui a usage de telles fêtes, mais c’est pour moi prime fois !

— Et je peux t’assurer que tu y tiens ta place. Tu vas te gâcher le plaisir à tant t’inquiéter. Profite un peu, laisse-toi porter. Si tu dois chuter durant le dabkeh, *inch’hallah* !

— Et mon frère pense que ça va me réconforter ? » ricana al-Nazif, trop angoissé pour arriver à rire de la plaisanterie.

Les deux hommes avaient à peine quitté leur tente qu’ils virent venir à eux al-Saghir. Toujours vaillant en dépit d’une boiterie grandissante, il tenait fièrement son rang et marchait à grandes enjambées malgré la souffrance que cela lui infligeait. Il accompagnait un autre vénérable à la barbe grise, sheikh ou ancien d’une des tribus. Ils s’arrêtèrent devant Nijm et al-Nazif, faisant croître l’angoisse dans le cœur de ce dernier. Il avait commis une faute majeure et on venait le lui signifier, il le devinait à l’air grave des deux hommes. Al-Saghir attendit qu’ils saluent dans les formes et prit la parole.

« Voici abu al-Fazari, mon cousin. Nous bavardions des liens entre nos tribus et il se trouve qu’une de ses nièces est bonne à marier. Je lui ai dit qu’al-Nazir vivait encore en célibataire chez son frère et qu’il serait temps pour lui d’avoir sa propre tente. Nous partagerons donc tous trois le repas sous la toile d’abu al-Fazari ce soir, que je puisse en discuter avec lui, avant le dabkeh. Il aimerait en savoir plus sur vos bêtes et vos biens. » ❧

## Notes

La réalité matérielle de la vie quotidienne dans les populations bédouines est difficile à appréhender pour la période médiévale et une bonne dose d’interpolation à base d’études ethnographiques récentes ou de récits de voyage modernes ne permet que des reconstructions hautement hypothétiques. Un des caractères tant vanté de cette vie, célébré à l’envi dans la poésie arabe est par exemple une certaine intangibilité au fil des siècles. Illusion bien pratique dont on affuble souvent les cultures traditionnelles recourant peu ou pas à l’écrit. Tout ce qui concerne donc les aspects les plus intimes et les plus domestiques des aventures de Droin ne demeure que des spéculations de ma part, inférences de lectures transversales de données éparses.

Le titre vient d’un dicton arabe : « la vie est comme un feu, flamme, fumée et cendre ».


## Références

Bianquis Thierry, *La famille arabe médiévale*, Bruxelles : Éditions Complexe, 2005.

Bonnenfant Paul, « L’évolution de la vie bédouine en Arabie centrale. Notes sociologiques », dans *Revue de l’Occident musulman et de la Méditerranée*, n°23, 1977, p. 111-178.

Élisséef Nikita, *Nūr ad-Dīn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Cowan gregory, *Nomadology in architecture. Ephemerality, Movement and Collaboration*, Dissertation for master in Architecture, University of Adelaide, 2002.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I à 6, Berkeley et Los Angeles : University of California Press, 1999.

Goitein Shlomo Dov, Friedman Mordechai Akiva, *India traders of the Middle Ages. Documents from the Cairo Geniza (‘India Book’)*, Leiden - Boston : Brill, 2008.

Mayeux F. J., *Les bédouins ou Arabes du désert*, 3 tomes, Paris : Ferra jeune, 1816.
