---
date : 2017-05-15
abstract : 1158. Inspiré par Aristote et certains récits religieux apocryphes, Herbelot Gonteux travaille à la création d’un corpus de textes afin d’édifier les souverains. Pour cela, il espère pouvoir s’appuyer sur les exploits d’un extraordinaire dirigeant chrétien, loin à l’est.
characters : Herbelot Gonteux, Bernard Vacher, Naudet, Maugier du Toron, Gilbert, Évêque Constantin
---

# Un royaume en oubli

## Caves de Suète, tente chapelle du camp royal, matin du jeudi 10 juillet 1158

Sortant du recoin à usage de sacristie entouré de paravents, Herbelot découvrit qu’il demeurait un chevalier en oraison agenouillé devant l’autel. Il reconnut sans mal l’imposant Bernard Vacher, habillé de son haubert comme à son habitude. Il était rare que quiconque s’éternise une fois la messe terminée, surtout quand le repas du matin avait été corné. S’interrogeant sur les motivations de ce recueillement, le jeune clerc fit mine d’arranger quelques accessoires, se tenant prêt à répondre à toute demande que le soldat pourrait lui faire.

Il n’avait jamais eu l’occasion de vraiment parler avec le porte-bannière royal, mais il en appréciait l’esprit vif et la parole retenue. Il était impressionné par son allure martiale qui évoquait en lui le vaillant Holomé, fidèle compagnon d’Alexandre[^romanalexandre2]. Seulement, ils appartenaient à deux mondes distincts et Herbelot était toujours réticent à sortir de sa réserve toute monastique, fut-ce pour lier connaissance avec des personnes qui l’intéressaient. Ayant terminé de psalmodier, le chevalier se releva lourdement, soufflant sous l’effort. Les années n’avaient pas été tendres avec lui, mille maux s’étaient installés dans son corps robuste. Malgré cela, il ne se plaignait jamais, se contentant de passer une main agacée sur son crâne et sa couronne de cheveux épars lorsque les douleurs se faisaient trop vives.

Il adressa un sourire poli à Herbelot, s’apprêtant à sortir du lieu.

« Le bon jour, sire Bernard, auriez-vous l’heurt de parler à clerc ?

— Nul besoin mon père, je n’ai pas tiré la lame du fourreau depuis bien longtemps.

— Même sans cela, il demeure moult forts périls pour l’âme. Avez-vous besoin d’aller à confesse ? Quelque lourdeur appesantit votre âme ?

— Oh, de certes ! Mais je n’ai l’envie de m’épancher en ce moment. J’avais juste pensée pour un mien neveu qui commence son service près le roi. J’ai espoir que les saints veilleront sur lui.

— La Sainte Mère de Dieu a toujours le cœur amiable pour les plus jeunes. »

La réponse fit sourire le vieux guerrier.

« Je dois dire que je le confiais plutôt à saint Georges et saint Maurice, qu’ils rendent son bras fort et son cœur vaillant, il en est grand besoin en la presse.

— Je ne le savais pas porteur du glaive et de la lance. Était-il de la bataille ?

— Si fait. Du moins était-il présent. Mais lui non plus n’a guère eu l’usage de ses armes. »

Peu désireux de vanter l’usage de la violence, fût-elle au service de Dieu, Herbelot invita de la main à sortir de la tente. L’auvent claquait sous le vent fort, les bannières et gonfanons dansaient furieusement. Autour d’eux, les pavillons des barons les plus notables étaient disposés en cercles concentriques. Le roi avait préféré s’installer dans la forteresse des Caves, laissant l’habitat de campagne à son armée.

« Je pensais rompre le jeûne avec les bannerets du sire Guillaume de Bures. J’y ai bons compagnons. Outre, sa table est toujours fort accueillante, avec mets variés et vins guillerets. »

La perspective d’un bon repas était le plus sûr moyen d’inciter Herbelot à accompagner quiconque, il continua donc de discuter avec Bernard Vacher jusqu’au vaste tref[^tref] qui accueillait la table d’honneur du prince de Galilée. La toile bruissait sous les rafales et l’air charriait les senteurs de poussière et de foin roulé depuis les écuries. L’ombre des falaises reculait rapidement et la chaleur s’intensifiait, célébrée par les cigales et les grillons.

Leur arrivée fut saluée par une demi-douzaine d’hommes occupés à avaler un épais brouet parsemé de fruits secs. Deux coupes proposaient des bananes et des abricots, faisant saliver par avance le jeune clerc. Repoussant le banc, Bernard laissa Herbelot s’installer avant de prendre place à son tour. Il fit le service du vin puis se tourna vers ses pairs.

« J’avais espoir de croiser Naudet, cela fait bien longtemps que je ne l’ai vu !

— Il a quitté le service du sire prince voilà un petit moment. Il parlait de joindre la milice du Temple[^templiers], il doivent en savoir plus à Jérusalem.

— Je ne sais s’ils accepteront un brave au chef si argenté ! se moqua un des jeunes, tout en dévorant une brioche trempée de vin.

— Il y a toujours des béjaunes[^bejaune] comme toi à former ! répliqua sans aménité son aîné. Il était déjà ici à guerroyer avant même que la Milice ne soit créée.

— J’ai souvenance de la puissance de son bras, enchaîna Bernard. Quand je revins de Flandre, voilà plusieurs décennies, je l’ai vu rompre cinq hampes de frêne à l’exercice sans poser pied à terre. Qui peut en dire autant ? »

La remarque attira plusieurs grognements d’acquiescement. Un jeune, enthousiaste, hocha énergiquement la tête et se mit à taper la table de son index.

« Une fois, il m’a conté qu’il avait guerroyé par-delà les sarrasins, chez un grand roi qui avait vaincu toutes les armées turques par chez lui.

— Gardes-en remembrance ! Bien peu sont les braves qui y étaient et encore moins qui peuvent encore en parler.

— De quelle bataille parlez-vous ? s’intéressa soudainement Herbelot.

— Du temps de mes jouvences, quelques bannerets du grand-père le roi sont allés aider un royaume chrétien loin à l’est. Ils y ont défait une puissante armée turque. »

Comme il avalait un peu de vin, Herbelot faillit s’en étouffer de surprise. Il toussa plusieurs fois, but encore un peu avant de reprendre la parole.

« L’apôtre Thomas a donc bien des héritiers ? Je pensais qu’il n’y avait que mahométans vers le Levant.

— Les terres sont bien trop vastes pour s’arrêter à eux. J’ai croisé des marchands qui parlaient de citadelles immenses, de prairies infinies où galopent des armées de preux chevaliers. Des créatures dont nous n’avons même pas idée. »

La conversation bifurqua sur toutes les étranges bêtes que les Latins avaient trouvées en arrivant en Terre sainte, puis sur celles qu’il restait à découvrir au-delà de leurs paysages coutumiers. Mais Herbelot n’écoutait plus. Il rêvassait à ce territoire merveilleux, forteresse de la foi d’où pouvaient s’élancer des milliers de chevaliers, tous frappés aux couleurs de la chrétienté. Reprenant contact avec la réalité, il toucha Bernard du bras.

« Il s’appelle comment, déjà, ce brave qui souhaite se retirer au parmi de la Milice du Christ ? »

## Jérusalem, mont du Temple, après-midi du samedi 25 octobre 1158

Les arbres dansaient au rythme des fortes rafales qui soulevaient la poussière de l’esplanade. Le bruit des travaux dans l’ancien palais royal volait jusqu’aux abords du Temple autour duquel Herbelot déambulait. Bataillant continuellement contre ses vêtements larges, le jeune clerc s’impatientait à l’idée d’enfin trouver Naudet. Il lui avait fallu plusieurs mois pour déterminer ce qu’il était devenu. Par chance, il était encore à Jérusalem, mais il n’avait pas été accepté dans la Milice du Christ comme il l’espérait. Selon un des chapelains, il avait été convaincu de se faire novice chez les cisterciens, un état plus adapté à son grand âge.

Bien que certains chevaliers aient parfois revêtu le blanc manteau sur le tard, il s’agissait en général de réception honorifique ou de personnes ayant une valeur particulière, dont l’ordre souhaitait bénéficier. Herbelot avait cru comprendre que Naudet ne rentrait nullement dans ces cas de figure. Les silences et les regards évasifs de ceux qui lui en avaient parlé étaient pleins d’éloquence pour un homme rompu aux subtils échanges monastiques. Naudet n’était guère apprécié, n’avait aucun haut fait dont se vanter et n’avait connu qu’une carrière en demi-teinte. Son plus grand exploit était d’avoir réussi à se maintenir jusqu’à un âge vénérable, ce qui en disait long sur ses capacités à éviter le danger.

N’ayant que peu l’occasion de déambuler librement en ces lieux, Herbelot goûtait le plaisir de revenir là où le Christ avait mis ses pas. Là où, chaque année, la procession des Rameaux venait s’échouer, en un ultime hommage des pèlerins. L’endroit était occupé par un collège de chanoines sous la supervision du vieillissant abbé Geoffroy et, surtout, du dynamique prieur Hugues. C’était ce dernier qui avait indiqué au secrétaire de l’archevêque que Naudet était leur hôte et qu’il se préparait au noviciat en suivant leur quotidien régulé. Tout en profitant des conditions de vie plus confortables des augustiniens, estimait Herbelot. Bien qu’étant clunisien, il connaissait assez bien les membres de l’ordre de Citeaux, dont il réprouvait la trop grande austérité. Il se demandait si quelqu’un qui avait vécu si longtemps dans le siècle, comme ce Naudet, avait la moindre chance de s’adapter à un tel environnement.

Herbelot trouvait que c’était là une forme d’orgueil de refuser de célébrer la beauté de la création. Malgré son naturel pacifique, il supportait avec difficulté les crtiques acerbes des moines blancs qui moquaient le goût du faste de Cluny. Au moins, remarqua-t-il, les chanoines du Temple profitaient d’un cloître rehaussé de nombreuses sculptures décoratives et historiées. Les scènes servaient d’ailleurs souvent à l’édification des fidèles, une école accueillant quelques enfants assis sur des bancs dans une des galeries.

Il s’adonnait à la contemplation de riches volutes florales et végétales lorsqu’il sursauta en entendant son nom dans son dos. Faisant face, il découvrit un vieil homme voûté, au visage pointu et au cheveu rare. Il souriait de ses quelques dents gâtées tout en frottant ses mains de tendon et d’os. Bien que non tonsuré, il était revêtu d’une robe blanche toute simple, dévoilant des mollets malingres et laiteux.

« Faites excuse de vous avoir ainsi surpris. C’est un tel honneur pour moi d’encontrer un clerc au service de l’archevêque Pierre.

— Je ne suis pas là à sa requête, mais pour simple raison personnelle. Vous êtes bien Naudet, chevalier ?

— J’étais celui-là, certes. La force a depuis long temps quitté mes bras, et ne suis désormais plus qu’un humble serviteur du Très-Haut, impatient de rejoindre des frères de la Foi pour célébrer Sa gloire jusqu’à ce qu’Il me rappelle auprès de Lui. Je n’ai pas encore choisi le nom sous lequel je renaîtrai à son service. »

L’élocution était difficile, l’homme déglutissant régulièrement, comme si sa gorge était continuellement obstruée. Il en profitait pour tendre son cou maigre, dodelinant du chef tel un pigeon ciblant sa pitance. Il commencèrent à déambuler tranquillement sous les voûtes, tout en échangeant à voix basse.

« On m’a rapporté que vous étiez au service de l’hostel le roi.

— J’ai chevauché sous bien des bannières, mais la première que je suivis ici fut celle du roi Baudoin. »

Il se figea, levant un index sentencieux démenti par son regard matois.

« Attention, je parle là du tout premier qui porta la couronne en ces lieux ! Ah, si vous aviez pu vivre ces instants ! Le monde était si jeune, alors. Même le roi conservait sa verdeur malgré les années. »

Un rire rauque monta de sa poitrine tandis que son visage fin s’animait de rides.

« Tout paraissait possible, en ces temps. Nous parcourions la terre comme des seigneurs. Vous imaginez que le vénérable patriarche Arnulf[^arnulfchocques] avait perdu son siège à cause des manigances du roi bigame ? »

La phrase n’amusa nullement Herbelot, peu disposé à entendre des ragots qui pouvaient souiller la mémoire d’un haut prélat ou d’un roi. Et d’autant moins si cela impliquait une femme, a fortiori deux. Il était certains sujets qu’il préférait conserver à l’écart de son monde de recueillement serein, de prière et de dévotion.

« Justement, on m’a conté que vous avez combattu fort loin au levant, au parmi d’une armée d’un grand souverain.

— J’ai connu tant de guerres ! Auriez-vous quelques précisions sur celle qui vous intéresse ?

— C’était au service d’un roi lointain, qui a défait les Turcs.

— Ah oui, je vois. Ce fût terrible bataille. Les têtes tombaient comme blé mûr en été !

— Avez-vous souvenance du roi ? Du lieu ?

— C’était fort loin d’ici, dans les montagnes. Ils parlaient une langue rauque que bien peu comprenaient.

— Mais c’était là bons chrétiens ?

— Sans nul doute. Seulement, ils étaient de la secte des Griffons[^griffon], avec leurs rites bien à eux. Nous avions quelques pères qui célébraient pour nous les mystères selon la vraie Foi, heureusement. »

Herbelot ne cacha pas sa déception, arborant une grimace, ce qui n’arrêta le vieil homme lancé dans ses souvenirs.

« Il me revient que leur roi s’appelait David. Avec quelques compères, nous avions craint un instant qu’il ne soit un de ces maudits juifs. Mais le sire Baudoin ne se serait jamais entremis avec pareille engeance, croyez-m’en !

— Attendez, mais vous parlez du royaume d’Abkhazie[^abkhazie] ?

— Bien sûr. C’est le plus levantin des royaumes chrétiens, par-delà les terres sarrasines.

— J’avais cru un moment qu’il en existait d’autres, vu que l’apôtre Thomas y est parti propager la bonne parole.

— Je n’ai pas la science de cela. Sur ces sujets, vous auriez intérêt à voir l’évêque de Jablé, Hugues de Nevers. »

Herbelot marqua une pause, intrigué par la mention d’un nom qui le ramenait à ses jeunes années. Tout le temps qu’il avait passé dans la clôture de la Charité-sur-Loire, il avait entendu parler de la cité de Nevers, loin au sud. Il n’imaginait guère le monde plus vaste que les quelques poignées de lieues qui s’étendaient jusque là et Bourges.

« Était-il des vôtres ? Un érudit ?

— De certes ! Je l’ai croisé voilà des années, alors qu’il préparait son passage pour en appeler au saint Père Eugène. Il voulait attirer l’attention du saint homme sur l’alliance profitable qu’il y aurait eu à se concilier les bonnes grâces d’un grand roi vainqueur des Perses et des Mèdes, loin à l’est. Il avait conquis de haute lutte leur capitale et obtenu leur allégeance.

— Il ne parlait pas de l’Abkhazie, vous en êtes acertainé ?

— L’évêque était de la princée d’Antioche, il n’aurait pas fait pareille erreur. Il parlait de certes d’un grand roi par-delà les terres soumises au Soudan de Babylone. »

Le cœur d’Herbelot s’était emballé à l’idée qu’il existait un souverain conforme à ses espoirs. Il ne lui restait qu’à obtenir des détails sur les noms, les lieux, et peut-être pourrait-il appuyer ses récits sur de solides fondations. Un victorieux prince chrétien loin à l’est, pour peu qu’il fût héritier des préceptes enseignés par Thomas et fidèle serviteur de l’Église romaine, voilà qui offrait les meilleures bases pour les épîtres qu’il griffonnait lorsqu’il laissait son esprit vagabonder.

## Tyr, palais de l’archevêque, après-midi du jeudi 6 novembre 1158

Par-delà l’épaisse porte de bois, Herbelot entendait la clameur de l’activité du palais. Avec la visite de plusieurs suffragants de l’archevêque, le lieu était empli de cris, de valets et bagages. Profitant d’un moment de calme dans ses propres obligations, il s’était réfugié dans la bibliothèque du prélat. C’était la pièce dans laquelle il se sentait le plus tranquille, avec tout ce savoir à portée de main, tous ces récits merveilleux qui n’attendaient que de renaître à chaque lecture. Il avait reçu le trousseau des clefs avec une sublime délectation qui l’avait inquiété, au point qu’il en avait parlé à son confesseur.

Il compulsait un petit in-quarto à l’écriture dense, qui reprenait un certain nombre de vies d’apôtres tels qu’habituellement dépeints dans les *Virtutes Apostolorum*. Il espérait y trouver des détails sur l’activité de Thomas, surtout d’un point de vue géographique et remplissait des tablettes de cire de sa graphie soignée. Il avait également amassé pas mal de précisions qu’il avait consignées sur des feuilles de papier, dont il avait découvert l’utilisation en Terre sainte. Avec les chaleurs estivales qui faisaient fondre la cire, il en appréciait grandement la souplesse d’usage, à un tarif nettement moindre que tous les autres supports. Il continuait malgré tout à prendre des notes succinctes sur les tablettes, faisant confiance à sa mémoire pour reconstruire a posteriori les données. On n’abandonnait pas une pratique remontant à l’enfance aussi aisément.

La clenche de la porte grinça sur le mentonnet de pierre et lui fit tourner la tête. Il espérait qu’on n’allait pas l’appeler pour une tâche quelconque. Il sourit de soulagement en voyant le visage ami de Gilbert, le jeune chantre de la suite de l’évêque de Lydda.

« Le bon jour, mon père. J’ai ouï dire que vous étiez là, je viens donc à dessein pour demander usage de certains ouvrages du sire archevêque.

— Après quels volumes espères-tu ?

— On m’a parlé d’un antiphonaire[^antiphonaire] dont le propre a été enrichi pour la Terre sainte, que le regretté père Foucher de Jérusalem[^foucherangouleme] avait fait rédiger. »

Herbelot sourit. Il connaissait l’appétence de Gilbert pour les hymnes et les chants. Il en goûtait également le talent et appréciait que des artistes mettent leur savoir-faire au service de la liturgie. De la main, il invita le jeune clerc à le rejoindre à la table tandis qu’il se levait pour aller chercher le lourd volume.

Quand il revint à sa place, il s’aperçut qu’il avait tellement l’habitude de prendre ses aises qu’il avait répandu ses notes sur tout le plateau. Les bras encombrés de l’imposant manuscrit, il demanda à Gilbert de pousser ses affaires.

« Préparez-vous quelque étude de science, à voir toutes ces références ?

— J’ai depuis longtemps abandonné le goût pour ces savoirs profanes. Il n’est nul besoin de comprendre l’œuvre du Seigneur pour en louer la beauté. »

Gilbert hocha poliment sa grosse tête tout en déplaçant avec soin les différents éléments. Il n’avait pas fait les études d’Herbelot, n’était pas prêtre ordonné et se comportait toujours tel l’élève devant son maître bien qu’ils n’aient que quelques années d’écart.

« Je prépare une missive pour le nord, pour l’évêque de Jablé. J’ai espoir de la confier à un clerc de l’hostel de l’évêque de Beyrouth, Sidon voire Tripoli.

— Le sire archevêque compte y faire visitance ?

— Nenni, c’est là courrier personnel. »

Il se rapprocha de Gilbert, adoptant une voix sourde, offrant l’image d’un conspirateur.

« J’ai appris que l’évêque du lieu y avait bonne connoissance d’une grande bataille qui aurait vu la victoire d’un puissant souverain chrétien sur les Mèdes et les Perses, voilà plus d’une dizaine d’années. Il me faut en apprendre davantage pour un ouvrage édifiant que je compose, à destination des princes.

— Vous parlez du roi Jean, le prêtre et roi ?

— Comment ça ? De qui me parles-tu ?

— J’ai entendu parler de ce grand homme, prêtre et roi, qui avait espoir de combattre les infidèles depuis le Levant. Un des chanoines, Régnier, connaît fort bien l’histoire. C’est un des plus érudits de l’hostel du sire évêque Constantin. »

Herbelot écarquillait des yeux de hibou.

« Ce Régnier est-il du voyage ? Puis-je l’encontrer ?

— Non, il ne voyage que peu. C’est lui qui a la charge de définir les cartons pour les imagiers de la basilique. Il préfère ne guère s’éloigner pour s’assurer que ses instructions sont bien entendues. »

Herbelot acquiesça, légèrement refroidi après l’enthousiasme qui l’avait envahi. Néanmoins, il sentait le fougue qui animait ses pensées. Il ne lui serait guère difficile de trouver une occasion de se rendre à Lydda. Il y avait souvent des messages à porter, des biens à escorter ou des visiteurs à accompagner. Il s’arrangeait généralement pour échapper à ce qu’il considérait une corvée. Mais là, il avait la meilleure des motivations pour réchauffer ses ardeurs.

## Lydda, palais de l’évêque Constantin, début d’après-midi du vendredi 5 décembre 1158

Herbelot entendait chacun de ses pas faire un étrange bruit de succion tandis qu’il avançait dans les salles empierrées. Le trajet n’avait pas été long depuis Césarée, mais ils avaient été douchés par une averse subite alors qu’ils dépassaient Jaffa. Il était légèrement penaud de se présenter ainsi devant l’évêque Constantin, mais il n’avait pas d’autres souliers dans ses bagages. Heureusement, d’épaisses chausses de laine lui avaient permis de mettre ses pieds au sec en arrivant. Il avait également avalé un petit bouillon, agrémenté de soupes d’un pain rassis, mais roboratif, le tout accompagné d’un vin léger. Il se sentait donc parfaitement bien lorsqu’il pénétra dans la vaste salle d’audience.

Comme partout dans le lieu, on voyait les réfections récentes. La basilique était encore l’objet de travaux intensifs. Les jours de beau temps, en ouvrant les fenêtres, le bruit de l’activité des tailleurs, forgerons et charpentiers devait d’ailleurs monter par les magnifiques châssis ornés de verre coloré. Au sol, un dallage de pierre agrémenté de carreaux vernissés disparaissait sous les tapis du dais de l’évêque. Aux murs, des motifs géométriques complexes avaient été peints sous un long ensemble narratif racontant la vie de saint Georges.

Insolemment vautré sur le trône, un petit singe festoyait de quelques fruits secs, tout en lançant des regards inquiets vers le nouvel entrant. Assis à une modeste table près d’un brasero, l’imposant Constantin discutait à mi-voix avec un homme arborant une fantastique bedaine. Malgré la fraîcheur, il était en train de s’éponger le front à l’aide d’un impressionnant mouchoir. Circonspects, ils s’interrompirent à l’arrivée d’Herbelot.

Reconnaissant le jeune clerc, l’évêque s’efforça de sourire poliment et, d’un geste, l’invita à s’avancer. Il l’introduisit rapidement à Maugier du Toron, son vicomte.

« J’ai là quelques courriers à votre intention, ainsi que les salutations les plus amicales du sire archevêque Pierre, sire Constantin.

— Je vous en sais gré, mon ami. Vous m’encontrez en pleins soucis. Êtes-vous là un moment ou juste de passage ?

— J’avais espoir de demeurer pour profiter de la science d’un de vos chanoines, si vous m’en donnez permission.

— Bien sûr, bien sûr. Ils ne sont pas moines cloîtrés, et certainement désireux de répondre aux attentes d’un clerc de l’hostel de l’archevêque. »

Voyant que l’évêque était soucieux, Herbelot décida de se retirer au plus vite, impatient de s’entretenir des questions qui l’intéressaient.

« Pourriez-vous juste m’indiquer où je pourrais encontrer le frère en charge des cartons du chantier ? C’est lui que j’ai espoir d’entretenir. »

La mâchoire du prélat béa soudainement, accompagnant le froncement de sourcils du vicomte. Maugier fut le plus prompt à réagir.

« Peut-on assavoir ce qui vous mande après lui, mon frère ?

— Rien qui ne puisse inquiéter un sire vicomte, je vous en donne gage. Juste question érudite sur laquelle on m’a fort vanté ses talents. »

Incapable de déchiffrer l’ambiance lourde qui planait, Herbelot se tourna vers l’évêque. Celui-ci hésita un petit moment, passant une langue épaisse sur ses lèvres fines avant de se résoudre à préciser, d’une voix ennuyée.

« Nous discutions de cela même à votre entrée : on a retrouvé le corps de Régnier de Waulsort ce matin même, brûlé de la plus horrible des façons. » ❧

## Notes

Il ne faut pas concevoir les royaumes médiévaux similaires aux états modernes. Leurs frontières étaient très fluctuantes et les conséquences de leur définition très aléatoires. Une porosité demeurait, même entre des territoires adversaires, et les combats n’entraînaient pas un arrêt des flux migratoires : pèlerins, diplomates, commerçants, nomades continuaient d’aller et venir, sous un contrôle plus ou moins serré. Il arrivait donc que certains voyagent fort loin et des échanges, ponctuels, mais bien attestés, existaient entre des zones très lointaines. Cela alimentait bien sûr tous les fantasmes et les attributions les plus fantaisistes quant à l’origine de certaines denrées.

Pourtant, les dirigeants, les lettrés parvenaient à échanger de temps à autre. Les informations se propageaient dans un second temps de façon très déformée auprès des couches les plus populaires et constituaient un terreau propice à l’extrapolation. De la même façon que le rapport au temps était complètement distinct du nôtre, habitués que nous sommes à une division métrée de ce dernier au fil du jour, le rapport à l’espace conservait une dimension mythique. L’espace géographique intégrait des composantes symboliques superposées à sa réalité matérielle.

## Références

Hamilton Bernard, « The Lands of Prester John. Western Knowledge of Asia and Africa at the Time of the Crusades  » dans *The Haskins Society Journal*, vol. 15, 2004, p.126-142.

Khintibidze Elguja, *The Designations of the Georgians and Their Etymology*, Tbilisi :Tbilisi State University Press, 1998.

Steinová Evina, *Biblical Material in the Latin Apocryphal Acts of the Apostles*, RMA Thesis, Utrecht University, 2011.
