---
date : 2021-06-15
abstract : 1160. D’un porteur à l’autre, un récit se modèle, se transforme et s’adapte. Un des participants à cette longue chaîne peut-il s’en croire l’auteur ou en prédire la postérité ?
characters : Gilbert, Hermann, Renaud de Sidon, Maître Benjamin, Remiggio
---

# La lettre cachée

## Sidon, château seigneurial, veillée du vendredi 12 février 1160

Avec la nuit était venue une tempête plongeant la cité dans les ténèbres. Le vent violent du large hurlait sur les reliefs et fracassait les vagues sur les rochers, ajoutant les embruns salés au déluge déversé par les nuages épais. Bien au chaud près du feu dans la grande salle, Gilbert goûtait le plaisir d’une soirée tranquille, à jouer aux échecs contre Renaud, le cadet du seigneur des lieux. C’était le membre de la famille avec lequel il avait le plus sympathisé. Guère plus âgé que lui, Renaud avait choisi de suivre la voix des armes, mais n’avait pas négligé les études pour autant. Il lisait parfaitement, écrivait superbement, et connaissait beaucoup d’auteurs qu’il pouvait citer de mémoire.

Sa curiosité l’avait poussé à s’intéresser à des sujets que n’aurait pas considéré un clerc, et il interloquait souvent Gilbert par ses remarques, qu’il lançait d’un air narquois. Le jeune prêtre avait été surpris de découvrir que sous des abords assez repoussants, une belle âme pouvait se cacher. Car si le Seigneur l’avait doté d’un esprit brillant, il ne l’avait guère avantagé au niveau physique. Tout en lui était disgracieux, et si ce n’était le soin qu’il portait à sa tenue et à ses attitudes, on l’aurait volontiers classé parmi les manants les plus ordinaires.

Renaud déplaça un de ses évêques et s’empara de son verre, laissant le loisir à Gilbert de réfléchir.

« J’ai lu ce que vous m’avez fait passer tantôt, mon père. »

Gilbert leva les yeux des pièces, avide de savoir ce que le jeune bachelier en aurait à dire. C’était certainement le plus cultivé des gens de son entourage depuis qu’il avait quitté le milieu clérical pour devenir chapelain.

« La forme est en bien tournée et il s’y trouve bien sages conseils, à n’en point douter.

— Grand merci !

— Il y a néanmoins quelques raffinements que j’y grefferai. Si cette missive vient si loin de l’est, il faudrait y ajouter détails précis, que l’esprit soit nourri des merveilles de ces terres, propres à enflammer l’imagination. Il y a là quelques jolies formules, aptes à édifier le plus mécréant des souverains, mais il y manque quelque saveur orientale pour que ce soit achevé. »

Gilbert était un peu mitigé. Il avait espéré des louanges ininterrompues, susceptibles de flatter son ego, mais se sentait aiguillonné par la perspective d’améliorer encore ce qu’il estimait être un travail majeur. Même s’il était nourri avant tout du labeur d’Herbelot Gontheux, c’était lui qui avait trouvé la forme épistolaire adéquate.

« À quoi pensez-vous ?

— Eh bien, quand j’ai appris à lire et écrire, un de mes maîtres m’a beaucoup transmis sur les récits de ces terres : les histoires moins connues de jadis, les légendes qui sont contées lors des veillées chez les fellahs et au cœur des cités musulmanes, ce genre de chose.

— Il est question d’édifier selon de doctes principes chrétiens, ne serait-ce pas y mêler trop de fantasmagories ?

— Ce sera à vous de trancher, c’est votre lettre, mon père. Mais j’encrois qu’il y manque ce qui fait la force des épopées d’ici, dont on sent qu’ils sont nés voilà bien longtemps, ressassés au fil des siècles pour en affiner le poli et le brillant. Si jamais vous en avez le souhait, je vous présenterai à mon vieux précepteur. Il demeure toujours à Sayette[^sayette]. »

Sans être complètement certain de son désir d’orientaliser son récit, Gilbert était curieux de rencontrer la personne qui était à l’origine de ce savoir si peu orthodoxe. Formé de façon très conservatrice dans un monastère bénédictin tout à fait traditionaliste, il avait pris conscience depuis quelques mois qu’il était totalement inculte en dehors de quelques domaines bien circonscrits. Lui qui était persuadé d’être un érudit et un lettré, il était presque vexé d’être ainsi mis en défaut sur ce qui était censé le caractériser. Sachant qu’il s’attaquait à une tâche qui serait très critiquée, il en repoussait sans cesse la formalisation définitive et la diffusion publique, inquiet de se voir moquer en raison d’un insuffisant travail de conception et de réalisation.

Il déplaça sans trop y penser un de ses rocs, amenant un sourire gourmand sur le visage de Renaud. Gilbert ne s’attarda pas à cette manifestation de joie qui était parfois feinte chez son adversaire, redoutable manipulateur dès lors qu’il s’agissait de s’affronter à un jeu ou l’autre. En aucune circonstance Renaud ne reculait devant un stratagème pour remporter la victoire.

« Je vous remercie, ce serait volontiers, j’ai toujours plaisir à encontrer mes aînés, surtout quand ceux-ci sont lettrés. Est-il issu de quelque prestigieuse école ? Laon, Orléans, Paris peut-être ?

— Oh, rien de tout cela, s’amusa Renaud. Il a fréquenté des maîtres, peut-être à l’ombre des cathédrales, protégé par un évêque ou l’autre, mais certainement pas élève de Thierry de Chartres ! Il est juif, un de leurs doctes, nommé rabbin par ceux de sa foi. »

En disant cela, il observa attentivement Gilbert, semblant espérer quelque surprise dont il appréciait toujours la manifestation chez autrui lorsqu’il s’exprimait. Il ne fut pas déçu et osa un sourire satisfait, quoique sans ironie.

« Ne craignez rien, il n’a jamais cherché à me vanter sa pratique. C’est un homme trop sage pour cela, quoique bien trop sentencieux pour moi. À n’en point douter un père insupportable et un guide spirituel certainement fort strict. Mais il n’était que mon professeur pour apprendre la langue des gens d’ici, ainsi qu’améliorer mon écriture. Il m’a ainsi fait découvrir nombre de récits merveilleux dont je n’avais pas idée. »

Gilbert hocha la tête. Il était régulièrement désarçonné par l’apparente insouciance du jeune chevalier, qui confessait en plaisantant avoir été soumis à l’enseignement d’un maître juif. Il y avait certainement là occasion à quelque pénitence, Gilbert n’en doutait pas. Ces gens étaient connus pour leurs pratiques magiques, souvent impies. Ce qui renforçait d’autant plus leur attrait pour les esprits curieux…

## Sidon, demeure de maître Benjamin, après-midi du mercredi 9 mars 1160

Gilbert avait été presque déçu quand il avait finalement décidé de se risquer à rencontrer maître Benjamin, le précepteur de Renaud. C’était un homme entre deux âges, la barbe grise assez soignée, au maintien assez raide, dont l’élocution était rendue difficile avec la perte de nombreuses dents. Sa vision n’était plus si bonne, qu’il ne quittait plus guère son cabinet de travail. Il avait donc reçu le chapelain avec respect dans cette petite pièce sentant la poussière, encombrée de feuillets et de parchemins, à peine éclairée par une fenêtre orientée au nord.

Un jeune apprenti aussi silencieux et discret qu’un novice leur avait servi de quoi boire et avait disparu sans une parole du vieux maître. Celui-ci s’était installé sur un suffah fatigué, après avoir invité son hôte à faire de même à côté de lui. Autour d’eux, sur des étagères, de nombreux volumes se présentaient à portée de main. Gilbert se demandait combien d’entre eux contenaient des savoirs interdits ou blasphématoires. Il était à la fois grisé et inquiet de se trouver là.

« Le jeune Renaud m’a appris que vous étudiez les terres orientales, c’est cela ? Quelque ouvrage érudit en préparation ?

— C’est cela. Il pensait que vous auriez connoissance de ces sujets, propres à compléter mes recherches.

— Si je peux vous être utile, ce sera avec grand plaisir. Avez-vous copie des récits d’Eldad ?

— Ce nom ne me dit rien, non.

— Il me faudrait un jour traduire son périple en latin, que cela soit mieux connu. Il narre le destin des tribus perdues d’Israël, loin au levant. Si vous étudiez ces provinces, cela pourrait vous être de bon usage. Si cela vous convient, je peux vous en faire récit, j’en ai une copie ici même si vous avez besoin de précisions ou de détails. Nous pourrons à tout moment nous y référer. »

Gilbert accepta avec enthousiasme. Si le texte n’en avait pas été établi en latin, il était fort probable que nul parmi ses pairs ne le connaissait. Il allait accéder à une nouvelle source d’information qu’il serait le premier à dévoiler aux savants et lettrés.

« Peut-être pouvons-nous esquisser déjà quelques grandes notions sur ces terres. Peut-être en prenant le Sambatyon comme frontière ?

— C’est là nom inconnu à mes oreilles…

— Ah, dans ce cas, nous allons devoir progresser pas à pas, car c’est à ce fleuve de pierre que se trace la limite avec les provinces orientales. Laissez-moi vous montrer… »

Les heures qui s’ensuivirent furent un véritable ravissement pour Gilbert. Il avait l’impression de fouler de ses pieds des chemins inexplorés, des terres vierges qu’il pourrait faire découvrir à tous. Il y avait là formidable matière pour situer la retraite d’un docte moine, au fait de toutes choses. Il se disait que le placer dans un royaume soumis à l’autorité des Juifs allait mettre en valeur son enseignement, lui qui avait à subir le joug de païens. Il saurait trouver les bons mots et les phrases qui toucheraient le cœur des souverains chrétiens. Et les précisions géographiques nouvelles renforceraient sans nul doute la puissance de ses révélations.

Alors qu’il rentrait au château, indifférent aux badauds qu’il croisait, Gilbert reprenait de tête différentes parts de son récit, réarrangeait certaines formules, embellissait de mille détails sa narration. Il était impatient de coucher tout cela sur papier, d’en faire un miroir des princes qui servirait à édifier les futurs monarques. Renaud avait eu raison, son travail allait grandement s’enrichir des apports de maître Benjamin.

## Sidon, château seigneurial, fin d’après-midi du lundi 27 juin 1160

Lorsque le jongleur Remiggio passait sur la côte, il ne manquait jamais de proposer une veillée aux habitants du château. Il y savait la table excellente et on ne lui avait jamais refusé la possibilité de tendre sa toile pour y présenter quelques spectacles, plus ou moins édifiants selon le public. S’il réservait les récits les plus épiques et religieux à la grande salle, ce qu’il racontait pour la foule des domestiques et des servants était souvent plus licencieux, voire graveleux.

Il avait pourtant veillé à se mettre en bons termes avec le chapelain, Gilbert, comme il le faisait dans chaque maison noble. Il savait combien les ecclésiastiques pouvaient rapidement lui causer des problèmes, voire lui faire interdire l’accès. Il n’hésitait donc jamais à évoquer les quelques années où lui aussi avait porté la tonsure cléricale. Avec Gilbert, cela avait été extrêmement aisé, le jeune homme n’étant guère vindicatif, amoureux de musique et peu sensibilisé aux questions théologiques les plus épineuses.

Il n’était pas rare qu’ils passent du temps tous les deux, à pratiquer sur tous les instruments dont ils disposaient, comparant des accords et des enchaînements. Ils parlaient parfois également poésie, quoique sur ce point, Remiggio fasse attention à ne pas froisser les oreilles, fort chastes, du prêtre. Il se contentait donc souvent de son registre le plus honorable. Il savait aussi que Gilbert appréciait fort tout ce qui concernait les terres lointaines, les animaux fabuleux et les peuplades les plus étranges et ne manquait jamais une occasion de lui rapporter ses moissons de récits merveilleux.

« Ce soleil frappe si fort qu’il ne plairait qu’au fénix !

— Qu’est-ce donc ?

— Nous n’en avons jamais parlé ? C’est incroyable oiseau, dont on dit qu’il n’en existe de plus beau. Après avoir vécu cent années, il vole au plus près de l’astre du jour pour s’enflammer.

— Que voilà triste fin !

— Oh, mais cela ne s’arrête pas ainsi. Il s’empresse alors de rejoindre son nid et tombe en cendres. De là, un ver en naît, qui devient à son tour fénix au bout de quelque temps. »

Les yeux de Gilbert s’écarquillèrent de plaisir.

« C’est assurément fort admirable oisel. Dont on pourrait tirer bien des sermons édifiants !

— De certes !

— En as-tu déjà vu ?

— Un mien compère à moi, Basam, dit qu’ils demeurent loin au Levant. Ils seraient apparentés aux Rokhs selon lui.

— Les Rokhs ? Peux-tu m’en dire plus sur eux aussi, tu les as évoqués ce tantôt, mais sans trop en narrer. »

Remiggio maîtrisait l’art de maintenir son public dans une perpétuelle attente, à dévider ainsi lentement son récit. Il n’ignorait pas qu’en cette occasion, cela lui permettait de profiter du séjour un peu plus longtemps, justifiant sa présence par ses échanges avec le clerc. Il prit donc un air concentré, faisant mine de chercher dans ses souvenirs, lui qui avait une mémoire sans défaut.

« Il y a tant de choses sur eux que je ne saurais par où commencer. Peut-être pourrais-je confier cette tâche au marin qui les a le plus fréquentés ? Si cela te sied, je pourrai narrer à la veillée les incroyables et merveilleux voyages d’as-Sindibādu, un navigateur qui n’a connu son pareil depuis des siècles. »

## Jérusalem, palais royal, fin de matinée du samedi 16 juillet 1160

Les célébrations pour la capture de la ville venaient à peine de s’achever et une foule immense était encore présente dans les rues, s’attardant sur les traces des conquérants autant que sur celles du Christ et des apôtres. Peu à l’aise dans une pareille multitude, Gilbert profitait du moindre prétexte pour se réfugier dans les enclaves tranquilles, jardinets et cours intérieurs, qui fleurissaient dans les plus grandes demeures.

Il avait eu le plaisir de rencontrer Hermann, un clerc formé dans les mêmes couvents que lui. Il était venu pour une mission diplomatique majeure, disait-il, sans s’étendre dessus. Il était néanmoins fréquemment en audience avec le roi et ses proches et logé au palais, signe de son importance. Malgré sa réserve toute bénédictine, il s’était montré assez chaleureux avec Gilbert, heureux de retrouver quelqu’un qui lui rappelait ses jeunes années. Il avait appris au prêtre la récente disparition de leur ancien abbé, Wibald de Stavelot.

En quelques semaines, ils étaient devenus inséparables, prenant plaisir à se remémorer les lieux dont leurs sandales avaient foulé les dalles. Seulement, Gilbert était là pour lui faire ses adieux. Son seigneur repartait dans ses terres et il devait le suivre. Il avait préparé quelques lettres, dont la plus conséquente s’adressait à la famille de sa bienfaitrice, qu’il continuait d’honorer par des messes régulières. Mais, en cette ultime rencontre, il avait surtout espoir d’obtenir le retour d’un véritable érudit sur ses travaux.

« Avez-vous eu le loisir de parcourir mes lignes ? J’aurais grand plaisir à savoir ce que vous en pensez.

— Cela m’a pris quelque temps, mais oui, j’ai tout lu. Je dois dire que j’ai été assurément impressionné par le propos. »

La phrase se déversait dans les oreilles de Gilbert comme de l’ambroisie l’aurait fait dans sa gorge.

« J’ai d’ailleurs une proposition à vous faire, mon ami. Je suis ici mandé par le roi des Romains, mais je suis avant tout un proche du noble évêque de Freising, Othon, dont vous avez sans nul doute entendu parler. Il est fort intime de l’empereur, depuis des années, et c’est un homme aussi savant que sage. »

Hermann s’arrêta un instant, comme hésitant à confier un secret, puis poursuivit.

« De grandes choses peuvent advenir, mon ami. Mais il faut pour cela que les puissants s’accordent, afin de bouter hors les mécréants de ces terres. L’empereur du Saint-Empire, celui des Griffons[^griffon] et Sa Sainteté le pape. Et votre étude peut aider à cela, j’ai ai certeté.

— Mon modeste travail ? Le croyez-vous si bon ?

— Et comment ! Savoir qu’un grand monarque chrétien ne demande qu’à unir ses forces aux nôtres pour broyer entre marteau et enclumes les païens, cela serait considérable nouvelle, propre à soulever l’enthousiasme.

— Mais cela n’est que supposition de ma part, ce sont surtout pieux conseils assemblés pour guider un prince.

— Et quoi donc, ne m’avez-vous pas dit que vous avez consulté les meilleurs érudits d’ici ? Appuyé votre texte sur de hautes autorités ?

— Si, mais… »

Gilbert hésitait. Il avait était enthousiasmé par les ajouts qu’il avait fait, persuadé que cela servait son propos, mais jamais il n’avait envisagé que son récit puisse être pris au pied de la lettre. Il était étonné qu’Hermann puisse le recevoir ainsi. Ce dernier perçut son embarras, sans forcément en comprendre l’origine et réfléchit un moment avant de briser le silence une nouvelle fois.

« Il demeure certains points que nous ne pourrions souffrir, comme ces royaumes juifs, qui ne peuvent être, car cela serait contraire à toute tradition établie. Il faudra donc le corriger, mais en dehors de ce détail d’importance, rien n’interdit d’imaginer que vous décrivez toutes choses existantes.

— Mais jamais le prêtre Jehan n’a écrit cette lettre !

— Qui serait assez fol pour le croire ? C’est là simple mise en forme qui permet de faire comprendre le plus subtil aux plus obtus, artifice de narration, rien de plus. La vérité emprunte parfois de bien tortueux chemins pour se manifester. Je sais qu’un évêque a, par le passé, déjà parlé de ce prêtre à mon sire. Vous avez recueilli tout ce qui se raconte à son propos, peu ou prou réalité, ou du moins ce qui en est parvenu jusqu’à nous. C’est là œuvre capitale, guidée par la volonté de Dieu sans doute aucun. »

Gilbert respira un grand coup. Jamais il n’avait envisagé que son travail puisse ainsi trouver sa place dans le monde, en si magnifique position. Il n’avait écrit tout cela qu’en suivant son inspiration pensait-il, en un hommage à une personne appréciée tout d’abord, puis comme divertissement intellectuel par la suite. Se pouvait-il que, ce faisant, il ait accompli un dessein supérieur ? Était-il vraiment le créateur de tout ceci ? Cette idée lui donnait le vertige : son œuvre allait le dépasser en tout. ❧

## Notes

Ce Qit’a constitue le dernier auquel je me consacre après avoir passé plus de dix ans à rédiger des textes se déroulant dans le XIIe siècle des Croisades, concluant un cycle en un récit qui en dévoile un des arcs narratifs les plus discrets, mais essentiels : la diffusion culturelle. La lettre du prêtre Jean est un des sujets qui m’a toujours fasciné et j’ai eu l’ambition d’en proposer une genèse qui en explique la complexité narrative et référentielle, la polysémie et les lectures variées possibles. Je ne crois pas qu’une pareille création ait pu voir le jour ex nihilo et sa richesse vient à mon avis aussi de l’investissement que certains ont voulu y mettre a posteriori, plaquant ainsi leurs desseins, peut-être parfois contradictoires avec l’intention préalable.

J’ai malheureusement dû brusquer un peu les choses pour ne pas laisser cet arc en suspens. J’aurais préféré pouvoir poursuivre sa lente diffusion au fil des Qit’a, année après année. Seulement la réalité matérielle du travail littéraire fait que je ne peux plus m’offrir le luxe de continuer cette série. Cela me demande trop de temps et d’énergie, tout en n’assurant que bien trop peu de revenus. Je laisse donc à d’autres le soin d’étendre cet univers s’il s’en sentent l’envie. Je souhaitais juste clarifier les choses sur mes aspirations sur le long terme avant de lâcher l’affaire, au moins pour un moment. Chacun en fera ce qu’il veut, ainsi qu’Hermann.

Enfin, car il y a toujours de nombreuses intentions entremêlées dans un écrit, le titre est bien évidemment une référence directe au texte d’Edgar Poe, « La lettre volée », qui était si bien cachée en étant exposée à tous, comme mon ambition de passeur de culture à travers Hexagora. Ceux qui verraient dans ce Qit’a une mise en abyme de ce qui me semble être le travail d’un poète pourraient bien être dans le vrai.

## Références

Bar-Ilan Meir, « Prester John : Fiction and History », History of European Ideas, 1995, XX, num. 1‑3, p. 291‑298.

Blake Denton, « The Quest for Prester John », Vexillum, 2012, vol. 2, p. 203‑219.

Rouxpetel Camille, « La figure du Prêtre Jean : les mutations d’une prophétie. Souverain chrétien idéal, figure providentielle ou paradigme de l’orientalisme médiéval ? », Questes, 2014, vol. 28, p. 99‑120.

Taylor Christopher Eric, « Waiting For Prester John: The Legend, the Fifth Crusade, and Medieval Christian Holy War » Master of Arts, University of Texas, Austin, 2011.
