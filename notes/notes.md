
[^aba]: Large robe aux manches amples.

[^abi_al_asaker]: Iz Al-Deen Abi Al-Asaker Sultan Mrdad, oncle du fameux Usamah ibn Munqidh et émir de Shayzar (…-1154).

[^abi_salamah]: Majd Al-Deen Abi Salamah Murshed (1068- 1137), père d’Usamah ibn Munqidh, qui abdiqua en faveur de son frère Iz Al-Deen Sultan pour se consacrer à la chasse et à l’étude du Coran.

[^abkhazie]: Terme usuellement utilisé pour désigner le royaume de Géorgie.

[^abraham]: Aujourd'hui *Hébron*, ville palestinienne de Cisjordanie.

[^abu]: *Père*, façon polie d'appeler un aîné père d'un garçon.

[^acre]: Saint-Jean d'Acre, aujourd'hui Akko, en Israël.

[^adan]: Aden, au Yémen, port naturel ouvrant sur le golfe éponyme.

[^adhemarmonteil]: Adhémar de Monteil, légat du pape pour la première croisade en en 1096.

[^adrien4]: Adrien IV, pape de 1154 à 1159.

[^adhan]: Terme arabe désignant l'appel à la prière, lancé par le muezzin depuis le minaret.

[^ahdath]: Milice urbaine.

[^aidkabir]: « Fête du sacrifice », appelée aussi Aïd al-Kabïr, très importante fête musulmane qui commémore la soumission d’Abraham à la volonté de Dieu, le 10 de dhou al-hijja. On y sacrifie traditionnellement un ovin en mémoire.

[^akka]: Nom syrien de Saint-Jean d’Acre.

[^alexiscomnene]: Alexis Ier Comnène (v.1058-1118), empereur byzantin (1er avril 1081-15 août 1118).

[^alhakim]: Al-Hākim bi-Amr Allah (19 octobre 996-19 février 1021), calife fātimide du Caire qui persécuta un temps les chrétiens et fit, en particulier, détruire le Saint-Sépulcre de Jérusalem.

[^aliboufier]: *Styrax officinalis*, dont la résine séchée donne le benjoin.

[^alienor]: Allusion aux rumeurs sur la conduite d’Aliénor d’Aquitaine.

[^alnazif]: Littéralement *Le propre*.

[^aljumah]: Le vendredi, principal jour de prière, où l'on se rend volontiers à la grande mosquée de sa ville.

[^alphonsecastille]: Le roi Alfonse VII de Castille (1126-1157) se faisait appeler l’empereur d’Espagne.

[^alqahira]: « La victorieuse », Le Caire.

[^alquds]: Nom arabe de Jérusalem.

[^alruzzayk]: Vizir du calife fatimide.

[^alsudaan]: En arabe, littéralement « *Pays des noirs* », qui désigne alors les terres au sud de l’Égypte.

[^altuntash]: Seigneur musulman de Salkhad et Busra, qui chercha à gagner son indépendance de Damas en 1147 et se rallia à Jérusalem. Ce fut un échec, malgré les tentatives de négociation, menées par Bernard Vacher pour Jérusalem.

[^alzarqa]: « Aux yeux clairs ».

[^aman]: Un sauf-conduit en cas de reddition.

[^amaury]: Amaury de Jérusalem, (1136-1174), comte de Jaffa et d’Ascalon puis roi de Jérusalem à partir de 1163.

[^amaurynesle]: Amaury de Nesle ( ? - 1180), patriarche de 1158 à 1180.

[^amiete]: Petite amie, compagne. L’Église était encore loin d’avoir imposé le célibat à tous ses clercs.

[^amir]: Officier.

[^amreddine]: Jus de pâte d’abricot.

[^antakiyya]: Antioche, en Turquie.

[^antiphonaire]: Recueil de chants religieux pour les offices.

[^anur]: Mu‘īn ad-dīn Anur (ou Unur) ( ? - 1149), principal dirigeant de l’état bouride de Damas de 1135 à sa mort en 1149.

[^apamee]: Apamée des Latins, bourg fortifié aux abords de Qal’at al-Madhīq, cité antique d’Apameia, en Syrie.

[^apocalypse]: Allusion à l’Apocalypse de Jean.

[^arbremesure]: Cité par Ibn Jobair, cet arbre marquait la frontière traditionnelle séparant les territoires chrétien et musulman, entre Damas et Baniyas/ Panéas.

[^ardre]: Brûler.

[^arnulfchocques]: Arnulf de Chocques, surnommé parfois *Malcouronne*, mauvaise tonsure. Patriarche latin de Jérusalem de 1112 à 1118 bien que temporairement démis en 1115-1116.

[^aspergesme]: Antienne accompagnant dans la messe le rite de la bénédiction et de l’aspersion d’eau bénite.

[^asqalan]: Ascalon.

[^atabeg]: Titre nobiliaire d'origine turque : *père du prince*. Il désigne le tuteur d'un jeune prince.

[^avantparlier]: Avocats, juristes spécialisés qui parlent en lieu et place des plaignants ou des accusés.

[^averexnoster]: « Salut, notre roi », antienne de procession.

[^averexnosterb]: « Fils de David », suite de la même antienne.

[^awaimet]: Petits beignets ronds à la fleur d’oranger.

[^ayla]: Aujourd’hui Aqaba, en Jordanie.

[^aynaddawla]: 'Ayn ad-Dawla Tûmân al-Yârûqī, important émir de Nūr ad-Dīn, qui avait contribué à chasser Usamah ibn Munqidh de Damas quelques années auparavant.

[^bacon]: Jambon sec.

[^ban]: Juridiction.

[^bachelier]: Jeune chevalier n'ayant pas encore de fief.

[^bagues]: Bagages.

[^bahrlut]: Littéralement : *La mer de Loth*, nom arabe de la Mer Morte.

[^balak]: Balak ibn Bahram ibn Ortok (? - v.1125). Atabeg d’Alep en 1124-1125, c’était un officier turc de premier plan, de la famille qui a gouverné Jérusalem et la Palestine.

[^baptise]: Terme désignant un converti, baptisé après avoir abjuré une autre religion.

[^barberousse]: Frédéric Ier de Hohenstaufen, dit Frédéric Barberousse (1122 – 10 juin 1190), empereur romain germanique, roi des Romains, roi d'Italie, duc de Souabe et duc d'Alsace, comte palatin de Bourgogne. 

[^barouk]: Mont culminant à 1943 mètres d'altitude.

[^barres]: Jeu de course et de capture, avec des camps délimités pour chaque équipe.

[^barthes]: Terres incultes.

[^basileus]: Titre donné à l’empereur byzantin.

[^baudoinI]: Le roi Baudoin Ier de Jérusalem.

[^baudoinIb]: Le roi Baudoin Ier de Jérusalem (v.1060 - 2 avril 1118), comte d’Édesse puis roi de Jérusalem.

[^baudoinIII]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Foulque et de Mélisende, frère d’Amaury.

[^baudoinIIIb]: Le roi Baudoin III de Jérusalem (1131-1162), qui prit véritablement le contrôle du royaume en 1152.

[^baudoinIIIc]: Le roi Baudoin III de Jérusalem (1131-1162), fils de Mélisende et roi de Jérusalem, alors en conflit plus ou moins larvé avec sa mère pour le contrôle du royaume.

[^bayle]: Régent.

[^bayle2]: La régence.

[^behourd]: Combat, mêlée, avec une dimension ludique souvent.

[^bejaune]: Contraction de *bec-jaune*, très jeune personne.

[^benedicatdeus]: « Que Dieu bénisse vos corps et vos âmes, et répande sur vous sa bénédiction comme il a béni Abraham, Isaac et Jacob. Amen. » 

[^benedicite]: Bénédicité populaire en latin, « Bénissez-nous, Seigneur, ainsi que la nourriture que nous allons prendre, par Jésus-Christ Notre-Seigneur. », les présents répondent alors « Amen », c’est-à-dire « Ainsi soit-il. ».

[^benedicthalamum]: « Bénis, Seigneur, cette chambre. Fais que tous ceux qui l’habitent soient établis dans ta paix, et que dans ta volonté ils demeurent, vieillissent et se multiplient pour une longue suite de jours. »

[^bertrand]: Petit cousin de Raymond II, qui s’est opposé à lui un temps.

[^besant]: Monnaie byzantine, à l’origine, courante au Levant. Sert surtout d’unité de compte, valant un cinquième de livre, 4 sous ou 48 deniers.

[^bestencier]: Se chicaner.

[^bilad_al_Sham]: Région dominée par Damas.

[^bile]: Aujourd’hui Al-Bira, en Turquie.

[^blanc]: Vient du mot arabe qui désigne familièrement le blé et l’orge.

[^blatier]: Marchand de blé en petites quantités, au détail.

[^bliaud]: Longue cotte, décorée et de riche étoffe, signe de notabilité.

[^bohemond3]: Bohémond III d’Antioche (1142/49 - 1201), prince d’Antioche, considéré mineur jusqu’en 1163. Renaud de Châtillon, son beau père, époux de sa mère Constance, assura la régence jusqu’en 1160.

[^boitemessage]: Les messagers portaient généralement les messages dans des boîtes spécialement conçues pour cela.

[^borek]: Pâtisserie salée fourrée.

[^bosra]: Bosra, importante cité du sud de la Syrien, capitale de la région du Hauran.

[^bourdon]: Bâton de marche, généralement orné, servant aux pèlerins.

[^bourides]: Dynastie dirigeante de la cité de Damas dans la première moitié du XIIe siècle.

[^boursescellee]: Les pouvoirs publics scellaient des bourses de monnaies dont le contenu était garanti, permettant d'attester d'une valeur sans peser individuellement chaque pièce.

[^bourgeoisie]: Type de propriété immobilière roturière, se distinguant du fief qui concernait la noblesse.

[^braiel]: Cordon servant à attacher les braies, sous vêtements.

[^bricole]: Engin de siège propulsant des pierres.

[^burchard190]: Question 190 du pénitentiel de Burchard de Worms : « As-tu mangé la nourriture des Juifs ou des autres païens, qu’ils ont préparée pour eux ? Si oui, tu feras pénitence dix jours au pain et à l’eau ».

[^byzantin]: Ce sont les Byzantins qu’on appelle alors Romains, comme ils se nomment eux-mêmes.

[^caffaro]: Célèbre consul génois (v.1080-v.1164), diplomate et guerrier, qui participa à la Première croisade et entreprit d’écrire l’histoire de sa cité.

[^caire]: *La victorieuse*, nom arabe du Caire.

[^camalli]: Docker, portefaix.

[^canivert]: Canif, petit couteau.

[^capitaneus]: Capitaine d'un vaisseau de guerre.

[^capmouton]: Pièce de bois servant à la tension des haubans.

[^casaux]: Villages accueillant des colons venus d’Europe.

[^captureblanquefort]: Bertrand de Blanquefort a été capturé en juin 1157 par Nūr ad-Dīn lors de la bataille aux abords du lac de Houla, voir *Chétif destin*.

[^celte]: Terme utilisé pour désigner les Latins.

[^cens]: Genre de loyer.

[^centredumonde]: Jérusalem est considéré comme le centre du monde par les Européens.

[^cesareeoronte]: Shayzar, sur l’Oronte, Syrie.

[^cesaire]: Césarée, aujourd'hui *Qesarya*, sur la côte méditerranéenne d'Israël entre Tel Aviv et Haifa.

[^chabanne]: Buron, grange et étable parmi les pâtures.

[^chaine]: Terme désignant l’office des douanes latin. Le terme vient du fait que c’était généralement de lui que dépendait l’abaissement ou la montée de la chaîne fermant le port.

[^chalemie]: Instrument à vent, de la famille du hautbois.

[^chandeleur]: Chandeleur, quarante jours après Noël, soit le 2 février.

[^chapuiseur]: Fabricant d’arçon de selle.

[^charlemagne]: Charlemagne était la figure légendaire favorite de beaucoup de traditions, et on lui prêtait des voyages un peu partout.

[^charlemagne2]: Charlemagne était la figure légendaire favorite de beaucoup de traditions, dont celle des hommes barbus.

[^charlemagne3]: Traduction littérale de *Carolus Magnus*, nom latin de Charlemagne

[^charnage]: Période pendant laquelle on a le droit de manger de la *chair*, de la viande.

[^charvin]: Porte manteau de forme cylindrique, que l’on attache généralement à l’arrière de la selle.

[^chat]: Tunnel d’attaque pour un siège.

[^chaudemar]: Le sol est argileux et a servi lors des temps bibliques à des cérémonies, dont le souvenir s’est mêlé à celui de la période où l’endroit a servi de lieu d’inhumation ou de dépotoir, avant de devenir des jardins.

[^ciborium]: Baldaquin surmontant un autel.

[^circumdederunt]: « Les gémissements de la mort m’ont environné », antienne tirée du psaume 17.

[^cisterciens]: Les Cisterciens, connus pour leurs talents de gestionnaires et d’innovateurs techniques.

[^citedavid]: Au XII^e^ siècle, on croyait que la cité de la Bible se trouvait sur le Mont Sion.

[^citevangile1]: « Marie a choisi la meilleure part, elle ne lui sera pas enlevée », évangile selon saint Luc, chapitre X, verset 42, qui fait allusion à Marthe et Marie, où Jésus Christ fait l’apologie de la vie contemplative.

[^citevangile2]: « En vérité je vous le dis, vous me cherchez, non parce que vous avez vu des miracles, mais parce que vous avez mangé des pains et que vous avez été rassasié », extrait de Jean, chap. VI, verset 26.

[^citevangile3]: Début du verset 33, chap.11 de l’Épître de Paul aux Romains : « Ô profondeur de la richesse, de la sagesse et de la science de Dieu ! », que l’on rapproche souvent du Psaume 139, verset 17, faisant référence à l’expression devenue populaire « les voies du Seigneur sont impénétrables ».

[^citevangile4]: Dernier verset du chap. 13 de la Seconde épitre de saint Paul aux Corinthiens : « Que la grâce de notre Seigneur Jésus-Christ, l’amour de Dieu et la communication du Saint-Esprit soient avec vous tous ! »

[^client]: Terme utilisé alors pour désigner ces serviteurs dans la hiérarchie de l’Hôpital.

[^cloturemer]: La navigation s’arrêtait de façon traditionnelle l’hiver en Méditerrannée.

[^coin]: On se sert de deux matrices métalliques (une au-dessus, l’autre au dessous) pour frapper les pièces et y imprimer les motifs.

[^compagna]: La Commune de Gênes.

[^compostelle]: Saint-Jacques de Compostelle.

[^comput]: Calcul du calendrier, généralement religieux.

[^comtour]: Comtour ou comptour, désigne les barons les plus éminents d’Auvergne.

[^conil]: Un lapin.

[^conrad]: Conrad III de Hohenstauffen (1093-1152), roi germanique.

[^conradlouis]: Conrad, roi d’Allemagne et Louis VII, roi de France, venus en Terre sainte pour ce qu’on nomme la Seconde croisade.

[^conroi]: Unité militaire, de cavalerie généralement, mais qui peut avoir un sens général.

[^conserva]: Sorte de convoi, sans qu’il y ait de chef amiral désigné.

[^convers]: Moine se consacrant à des tâches subalternes, souvent manuelles.

[^cotte]: Vêtement long, serré à la taille par la ceinture, dont la qualité de l’étoffe indique souvent le statut du porteur.

[^cotteughes]: Village abandonné des cases de Cotteughes (Cantal).

[^couderc]: Dans le Massif Central, zone dégagée aux abords d’un village, généralement pâturée en commun.

[^courtil]: Jardin.

[^cousinmanuel]: Allusion au mariage du roi Baudoin avec la jeune princesse byzantine Théodora Comnène, nièce de l’empereur Manuel.

[^croisade1a]: Les participants à la première Croisade

[^croisade2a]: Pour la Seconde croisade, en 1148.

[^croisade2b]: Allusion à la Seconde croisade, en 1148-49, où croisés français et allemands, Latins de Terre sainte, se rejetaient la faute de l’échec du siège de Damas.

[^daftar]: Carnet de notes financières.

[^damans]: Damans, petits plantigrades.

[^danishmendide]: Dynastie turque islamisée, régnant alors sur une partie de l’Anatolie.

[^deogratias]: « Rendons grâce à Dieu ».

[^dextresenestre]: Droite ou gauche.

[^dieumande]: « Je te recommande à Dieu ».

[^dienne]: Nom médiéval de la ville de Dienne (Cantal).

[^diete]: *Diaeta imperii* en latin, *Reichstag* en allemand. Assemblée peu formelle des souverains composant l’empire germanique, et qui se réunissait selon les besoins.

[^dimashq]: Damas.

[^anur]: Mu‘īn ad-dīn Anur (ou Unur) ( ? - 1149), principal dirigeant de l’état bouride de Damas de 1135 à sa mort en 1149.

[^diwan]: Terme arabe : le lieu du pouvoir. On peut le traduire par cabinet, ministère ou office. Il a donné le mot français douane.

[^djebel_ansariyeh]: Jabāl Ansariya, massif montagneux prolongeant le mont Liban vers le nord, en suivant le littoral méditerranéen.

[^djebel_zawiya]: Jabāl Zāwiya, région montagneuse prolongeant le plateau d’Alep vers le sud.

[^dogana]: Office des douanes, qui régit importations et exportations.

[^douaire]: Biens appartenant en propre à une femme qui survit à son mari.

[^douqh]: Ou doogh, boisson à base de lait fermenté, d'eau et de menthe séchée.

[^dragons]: Des crocodiles.

[^durraa]: Grande robe de dessus.

[^echecchevalier]: Ancien nom du cavalier.

[^echeccomte]: Ancien nom du fou.

[^echecroc]: Ancien nom de la tour.

[^echelle]: Formation de combat équestre.

[^ecir]: Vent tempétueux d’hiver qui forme les congères.

[^edesse]: Le comté a été détruit par les musulmans dès 1144.

[^embriaco]: Littéralement « Tête de maillet », Guglielmo Embriaco, héros génois de la première Croisade.

[^encharim]: Aujourd’hui Ain Karim, ou Ein Kerem.

[^epilorikion]: Vêtement byzantin à manches ouvertes au niveau des épaules.

[^ermin]: Arménien.

[^escabeau]: Tabourets.

[^espagne1148]: Allusion aux conquêtes génoises en Espagne en 1148, en parallèle de la Seconde croisade.

[^estree]: Voyage.

[^etambo]: « Que tous deux parviennent au repos des élus et au royaume des cieux, et qu’ils voient les enfants de leurs enfants jusqu’à la troisième et quatrième génération. »

[^etegoconjungo]: « Je vous donne l’un à l’autre au nom du père, du Fils et du Saint-Esprit. »

[^etiennebourges]: À Bourges.

[^eudesarpin]: Eudes Arpin ( ? – vers 1130), ancien vicomte de Bourges, devenu moine après son voyage en Terre sainte vers 1100.

[^fame]: Réputation.

[^fetu]: Mettre sur la paille.

[^fiefdebesant]: Fief constitué d’un revenu financier, plus facile à retirer à son bénéficiaire que des terres.

[^filaire]: *Phillyrea angustifolia*, arbuste proche de l’olivier.

[^Flandre]: Il s’agit alors du comté, donc terme au singulier.

[^fondaco]: Ou encore *fondouk*, *funduq*. Caravansérail pour les marchands.

[^fonde]: Bâtiment où l’on règle les taxes douanières.

[^forces]: Outil de coupe des toisons, des poils en général, selon leur taille.

[^fouaces]: Brioches.

[^foucherangouleme]: Foucher d’Angoulême, patriarche de 1146 à 1157.

[^foudrea]: Le 6 janvier 1146, elle a frappé l’église du Saint-Sépulcre et Sainte-Marie du Mont-Sion.

[^foulque]: Foulque d’Anjou, dit le jeune, (v.1092-1144) devenu roi de Jérusalem par son mariage avec Mélisende en 1131.

[^fourches]: Structures de bois auxquelles on accrochait les pendus.

[^fredericroche]: Frédéric de la Roche, alors évêque d’Acre et futur archevêque de Tyr.

[^frerastre]: Beau-frère.

[^fustat]: Alors capitale du pays, désormais intégrée au Caire, dont elle était voisine.

[^futreau]: Embarcation de taille moindre que les grands chalands de portages, souvent en convois.

[^gaillard]: Partie arrière du bateau, généralement à plusieurs étages.

[^galée]: Galère.

[^gambison]: Vêtement rembourré généralement porté sous le haubert de mailles.

[^gamboise]: Se dit d’un vêtement rembourré de façon à s’en servir d’armure.

[^garum]: Le garum est une macération salée d’entrailles de poisson. C’est un remède préconisé par Rhazès, grand médecin et spécialiste de la variole (865-925).

[^gaste]: Désigne les zones impropres à l’exploitation agricole.

[^ghirara]: Mesure de poids pour les grains, qui vaut 3 irdabbs, soit un peu moins de 210 kg.

[^ghavril]: Gabriel de Malatya (1055-1103), gouverneur arménien de Mélitène/Malatya.

[^genuensis]: « Génois, donc marchand. »

[^geoffroyplantagenet]: Geoffroy Plantagenêt (1113-1151), duc de Normandie depuis 1144. Son père était Foulque V d’Anjou (1092-1144), qui épousa Mélisende de Jérusalem en 1129.

[^gerard]: Fondateur de l’Ordre hospitalier de Saint-Jean au début du XII^e^ siècle.

[^ghulam]: Synonyme de mamlūk, désigne en contexte militaire un esclave formé à devenir un guerrier d’élite, souvent utilisé comme officier.

[^ghulam2]: Terme désignant un jeune homme, utilisé couramment pour désigner les esclaves mâles (souvent des militaires, mais pas seulement).

[^gibelet]: Aujourd’hui J’baïl au Liban, Byblos dans l’Antiquité.

[^godefroy]: Godefroy de Bouillon (v.1058-1100), conquérant de Jérusalem, dont le personnage est vite devenu légendaire.

[^gormond]: Gormond de Picquigny ( ? – v.1128).

[^goupil]: Renard.

[^grandlargue]: Vent de trois-quarts arrière.

[^grandsecrete]: Administration royale des finances, sous l’autorité du sénéchal.

[^grebeleures]: Toiles de tentes généralement utilisées pour les simples soldats.

[^griffon]: Terme usuel pour désigner les Grecs, c’est à dire les Byzantins.

[^guillaumebures]: Guillaume II de Bures, prince de Galilée, 1148-1158.

[^guillaumeconq]: Guillaume le Conquérant (v.1027-1087).

[^guillaumemalines]: Guillaume de Malines, ancien archevêque de Tyr puis patriarche de 1130 à 1145.

[^guillaumenevers]: Guillaume II de Nevers, 1083-1148.

[^haljamiyya]: Plat à base de navet.

[^hamadan1]: Importante ville d’iRan occidental, sur les pentes de l’Alvand, à plus de 1800 mètres d’altitude. C’était alors la capitale seldjoukide.

[^hanse]: Association de marchands de la mer du Nord et de la mer Baltique.

[^harenc]: Aujourd’hui Harim, Syrie. 

[^harrir]: Soie de qualité commune.

[^harrod]: Cours d’eau dont la vallée passe juste au nord de Baisan.

[^hast]: Bâton, hampe. Désigne plus largement tout ce qui comporte un long manche, arme comme outil.

[^haver]: Du verbe *haver*, saluer. Signifie aussi faire mat (aux échecs).

[^alkhalil]: Nom arabe de Hébron, en Cisjordanie.

[^henrifrance]: Henri de France (1121-1175), fils du roi Louis VI le Gros, évêque de Beauvais (1149-1162) puis archevêque de Reims (1162-1175).

[^henriv]: Henri V, empereur du Saint-Empire (11 août 1086-23 mai 1125).

[^heures]: Liturgie impliquant des prières à certains moments précis de la journée, et qui concernait alors essentiellement les ordres réguliers.

[^hun]: Hongrois

[^hodierne]: Hodierne (v. 1110-v. 1164), fille du roi Baudoin II de Jérusalem et de Morfia de Malatya. Elle épouse en 1131 Raymond II de Toulouse comte de Tripoli.

[^hopital]: Structure d’accueil pour les voyageurs.

[^hospitaliers]: Les Hospitaliers de Saint-Jean de Jérusalem.

[^hostel]: Le terme peut désigner une famille, une maisonnée, une maison (le bâtiment), avec toutes les polysémies de ces termes.

[^huchier]: Fabricant de coffres, un type de menuisier.

[^huguesibelin]: Hugues d’Ibelin (? - 1170), seigneur d’Ibelin et de Rama.

[^ifranj]: Terme désignant les Européens.

[^ifriqiya]: Afrique du Nord, du Maroc à la Lybie.

[^ifrit]: Génie magicien.

[^innocentII]: Pape de 1130 à 1143.

[^iqta]: Institution musulmane qui concède l'impôt d'un territoire à un officier de façon à ce qu'il entretienne ses troupes.

[^irdabb1]: Mesure pour les grains qui vaut à Damas à cette époque environ soixante-dix kilos.

[^irene]: L’impératrice Irène, épouse du basileus Manuel Ier Comnène était en fait la belle-sœur du roi Conrad. Ce dernier en avait fait sa fille adoptive pour rehausser son statut lors des négociations de mariage avec Byzance.

[^ispahan]: Aujourd’hui Ispahan, en Iran.

[^issir]: Sortir.

[^itemissa]: « Allez, la messe est dite ». Formule chantée qui indique la fin de la messe.

[^job1]: (Job, 42, 1-2).

[^jobedesse]: Savant, philosophe (IXe siècle) et, surtout, traducteur depuis le grec vers le syriaque de *Galien*, médecin grec très renommé en Méditerranée orientale.

[^jongleur]: Le terme désigne alors les amuseurs de façon générique.

[^joscelinII]: Joscelin II, comte d’Édesse (vers 1110-1159).

[^joscelinIII]: Joscelin III d’Édesse (vers 1135-avant 1200), frère d’Agnès de Courtenay, première épouse d’Amaury de Jérusalem.

[^joscelinIIIb]: Joscelin III de Courtenay (1134 - v.1200) ne put jamais récupérer son comté d’Édesse. Sa soeur étant la première épouse d’Amaury de Jérusalem (et mère de Sybille et de Baudoin IV), il fit carrière dans le royaume de Jérusalem.

[^jubba]: Robe large, à manches très larges.

[^juma]: Abu Mahmud Jum'a al-Numayri. Soldat et compagnon d'enfance de Usamah Ibn Munqidh.

[^kala]: équivalent de *tante* en arabe, « sœur de sa mère » précisément.

[^kavourmas]: Plat à base de porc grillé à l'huile d'olive.

[^keftedes]: Plat byzantin à base de boulettes de viande.

[^kerak]: Aujourd’hui al-Karak, en Jordanie.

[^khaff]: Bagages personnels d'un marchand, séparés de ses produits commerciaux scellés pour le voyage.

[^khamaseen]: Très violent vent de poussière venu des terres désertiques.

[^khamsa]: Symbole de protection en forme de main, parfois appelé *main de Fatma* ou *main de Fatima*.

[^kharaj]: Impôt spécial levé sur les juifs et les musulmans, qui n’avaient pas à s’acquitter de la dîme, payée uniquement par les chrétiens.

[^khwarezm]: Région perse située au sud de la mer d’Aral, en Ouzbékistan actuel.

[^khwarizmi]: *al-Khwarizmi* signifie originaire du Khwarezm, région perse située au sud de la mer d’Aral, en Ouzbékistan actuel.

[^kichk]: Céréales concassées à la base de la mouné, les conserves traditionnelles préparées pour l’hiver.

[^klibanion]: Armure lamellaire.

[^kursi]: Petit siège.

[^kyrie]: Chant liturgique, *Kírie eléison*, « Seigneur, prend pitié » en grec, qui marque traditionnellement le début de la messe proprement dite.

[^laetare]: Dit aussi de Laetare, quatrième dimanche du carême, où la liturgie se veut plus joyeuse.

[^laie]: Sentier tracé en forêt pour y établir des coupes.

[^lavash]: Pain plat traditionnel arménien.

[^leben]: Boisson traditionnelle syrienne à base de lait fermenté.

[^lofer]: Manœuvre du navire pour se rapprocher de l’axe du vent.

[^lormier]: Fabricant de rênes, freins, étrivières et de sangles de sellerie, de façon générale.

[^louis6]: Louis VI de France (1081-1137, roi en 1108), surnommé également le Gros, père de Louis VII.

[^louis7]: Le roi de France Louis VII, 1120-1180.

[^louis7b]: Louis VII (1120-1180), roi de France depuis 1137, qui a participé à la Seconde Croisade entre 1147 et 1149.

[^loupchevalier]: Se disait des loups qui attaquaient les chevaux.

[^luc6-20]: Évangile selon Luc, chapitre 6, verset 20 : « Heureux vous qui êtes pauvres, car le royaume de Dieu est à vous ! »

[^maalem]: « Maître », en l’occurrence, responsable d’un des secteurs du marché.

[^maamoul]: Pâtisserie du Levant, à base de pâte farcie généralement de dattes, de pistaches ou de noix. Des versions salées, avec du poisson, existent.

[^madeleinevezelay]: À Vézelay, où l’on disait trouver des reliques de Marie-Madeleine.

[^mahomerie]: Aujourd’hui Al-Bira.

[^maille]: Petite pièce de monnaie, de faible valeur.

[^majlis]: Pièce de réception.

[^makhlamion]: Vêtement byzantin à ouverture frontale boutonné devant, mais pas ouvert jusqu'en bas.

[^malvicino]: Litt. _Mauvais voisin_

[^manasseshierges]: (v.1110-1177), connétable du royaume de Jérusalem de 1144 à 1152.

[^mandator]: Ambassadeur, messager byzantin.

[^manse]: Unité familiale d’exploitation du sol cultivable.

[^manuelcomnene]: Manuel Comnène, empereur de Byzance à partir de 1143 (1118-1180).

[^manusdomini]: « Que la main du Seigneur soit sur vous, et qu’il envoie son saint ange vous protéger tous les jours de votre vie. Amen. »

[^maravedi]: Nom espagnol du dinar, pièce d’or à la base du système monétaire musulman.

[^markouk]: Pain plat très fin des montagnes libanaises.

[^mathessep]: Sergent du roi au statut élevé, assistant le vicomte.

[^mariage1]: *Le mariage est consommé*.

[^matthieuedesse]: Matthieu d’Édesse (seconde moitié du XIe siècle - 1144) était abbé d’un monastère près de Kaysun. Il a écrit une *Chronique* qui constitue une source de repmière importance pour l’histoire de cette région. Il était très critique envers les Byzantins et les Latins.

[^maubec]: _Mauvais bec_ : qui parle mal.

[^maursin]: Pietro Maureceno, prêtre et notaire du quartier vénitien autour de 1157.

[^meleha]: Défaite des latins face aux forces de Nūr ad-Dīn, le 19 juin 1157, qui vit la mort ou la capture de nombreux dignitaires. Voir Qit'a *Chétif destin*.

[^melkite]: Chrétien syriaque soutenant les positions christologiques byzantines, souvent de milieu urbain. Les chrétiens syriaques orthodoxes, appelés jacobites, étaient alors plus nombreux en milieu rural.

[^melisende]: Mélisende de Jérusalem (1101-1161) fille de Baudoin II du Bourcq, second roi de Jérusalem.

[^menefetu]: Mis sur la paille.

[^mensaf]: Plat traditionnel à base de mouton, de lait de chèvre et de riz.

[^mereau]: Jetons, pion, servant parfois de ticket, voire de monnaie.

[^merelles]: Jeu de plateau où l’on cherche à faire des alignements avec des pions sur un cadre dessiné.

[^meshmeshan]: Aujourd’hui Meshmeshan, dans la région d’Idlib, en Syrie.

[^mesureurble]: Profession libérale jurée ayant pour rôle, contre rétribution, de mesurer les grandes quantité de grains vendues par les marchands.

[^mezze]: Ensemble de mets servis dans un grand nombre de coupelles, offert lors de grandes occasions ou de banquets.

[^miladiu]: Expression auvergnate signifiant « Nom de Dieu ».

[^mindil]: Type d’écharpe pouvant servir de couvre-chef ou de ceinture.

[^mire]: Médecin.

[^miserere]: _Aie pitié de moi, ô Dieu, selon ta bonté_ **(Psaume 51:1)**.

[^misericordia]: Introït de la seconde messe après Pâques, Ps. 32, 5-6.

[^misr]: Le Caire, *al-Qahira* (la victorieuse). C’est aussi le nom donné à l’Égypte en général.

[^mongols]: Il s’agit des invasions mongoles au Moyen-Orient, auxquelles les Mamlouks s’opposent.

[^montsaintmichel]: Mont Saint-Michel.

[^moree]: Nom que les Latins donnaient parfois à l’Achaïe, région au nord-ouest du Péloponnèse.

[^mouhalabie]: Ou Lait de Damas, boisson à base de lait et d'eau de rose.

[^nabidh]: Terme par lequel les marchands en terre d'Islam désignent le vin, mais qui peut aussi désigner toute sorte d'autres boissons.

[^nazif]: Le *propre*, souvent entendu au sens figuré le *pur*.

[^murdrier]: Meurtrier.

[^naples]: Nom latin de Naplouse, aujourd’hui Nablous (arabe) ou Chékhem (hébreu), à environ 50 kilomètres au nord de Jérusalem, en Palestine.

[^nard]: Jeu similaire au backgammon, avec des règles et des positionnements un peu différents.

[^navrer]: Blesser.

[^nerses4]: Nersès IV Chnorhali, Catholicos de l’Église apostolique arménienne de 1166 à 1173. Il fut également un grand écrivain et poète.

[^nocher]: Officier de bord responsable de l’itinéraire.

[^nuraldin]: Nūr ad-Dīn, fils et successeur de Zankī, homme d’état et chef de guerre d’origine turque (vers 1117/8 - 1174).

[^nuraldin2]: Partie du nom complet de Nūr ad-Dīn, qui ne reprend aucun de ses titres, pratique insultante.

[^nizarites]: Communauté mystique chiite ismaélienne, dont font partie les célèbres Assassins.

[^oaltitudo]: « Ô profondeur de la richesse, de la sagesse et de la science de Dieu ! ». Extrait de saint Paul, Épître aux Romains 11, 33.

[^oblat]: Enfant donné à un monastère pour en faire un religieux.

[^omphalos]: Pierre marquant le centre supposé du monde.

[^onfroytoron]: Onfroy II de Toron (v.1117-v.1179), connétable du royaume de Jérusalem.

[^orfroi]: Broderie d’or ou d’argent sur un vêtement.

[^oronte]: L’Oronte.

[^ost]: Armée.

[^oublies]: Crêpes.

[^outremer]: Les territoires latins du Moyen-Orient.

[^paneas]: Aujourd’hui Baniyas, sur le Golan, à ne pas confondre avec Baniyas, ville portuaire du nord de la Syrie.

[^paneas2]: La ville avait été remise courant juin par les Damascènes au royaume de Jérusalem, après un siège commun de la ville tenue par un officier sous l’autorité de Zengî, alors en guerre avec Damas.

[^parerlachataigne]: Se faire tromper.

[^pastfelis]: Sucrerie à base de miel et de graines de sésame.

[^patronus]: Commandant d’un navire commercial.

[^pavesade]: Rang de boucliers qui protège les rameurs.

[^pelagonie]: Aujourd’hui Bitola, Macédoine.

[^perron]: Escabeau pour faciliter la monte.

[^pierrelombard]: Pierre Lombard, théologien (v.1100-1160).

[^pierrevenerable]: L’abbé de Cluny, Pierre le Vénérable (v.1094-1156).

[^pierriere]: Type de catapulte.

[^plineancien]: Pline l’Ancien, érudit et naturaliste romain, mort dans l’éruption du Vésuve en 79.

[^pluiecoucou]: Se dit d’une courte averse.

[^poivre]: Certaines marchandises (épices, étoffes) servaient parfois de monnaie d’échange.

[^polis]: Terme désignant *la* ville, à savoir Byzance.

[^pommeparadis]: Bananes.

[^ponant]: Le couchant, l’Ouest.

[^poree]: Purée ou hachis de légume.

[^portiere]: Rideau faisant office de porte.

[^potherbe]: Désigne les légumes que l’ont cuit au pot, en potage.

[^potronjacquet]: Forme médiévale de l’expression *potron-minet*, qui désigne l’aube. Le choix de ce terme ici est en hommage à Robert Merle, qui me le fit découvrir dans son excellente série *Fortune de France*.

[^praecepta]: Plus connu sous le nom de *Praecepta militaria*, attribué au grand général puis empereur byzantin Nicéphore II Phocas (v. 912 - 11 décembre 969).

[^prieres]: Les trois prières de base que tout bon catholique devait savoir : Credo, Pater noster et Ave Maria.

[^principicule]: Prince de petite envergure.

[^psaume119]: Début du Psaume 119(120), premier Cantique des degrés, souvent chanté par les pèlerins.

[^puerihebraerum]: « Les enfants des Hébreux, portant des branches d’olivier », antienne de procession.

[^pyr]: *Pyr*, prononcé [piɾ].

[^pyxide]: Boîte de rangement des hosties.

[^qaa]: Pièce de réception des riches demeures.

[^qaba]: Vêtement à ouverture croisée sur le devant.

[^qach]: Emplacement le long du qata, côté femmes, où sont stockées la plupart des affaires qui ne sont pas utilisées quotidiennement.

[^qata]: Mur de toile qui sépare le *muharram*, partie réservée aux femmes du *raba’a*, destiné aux hommes.

[^qayah]: Petit bonnet porté sous les autres coiffes.

[^qiligarslan]: Qilig Arslan (? – 1192), sultan seldjoukide de Rum à partir de 1156.

[^quadrivium]: Ensemble des quatres sciences dites mathématiques : arithmétique, musique, géométrie et astronomie. Cela fait suite au trivium, consacré aux trois arts : grammaire, dialectique et rhétorique.

[^quidmulier]: « Pourquoi pleures-tu, femme ? Tu es en train de parler à Celui que tu cherchais. Regarde Moi, digne et vivant, mais ne Me touche pas. », renvoie à l’évangile selon saint Jean, chapitre 20, versets 11 à 18.

[^quirou]: Pierre dressée.

[^qus]: Ville de Haute-Égypte par où transite beaucoup du commerce venu de Mer Rouge.

[^radolibos]: Village du thème de Thessalonique.

[^rafraf]: Longue extrémité du turban.

[^rais]: Notable d’origine indigène tenant lieu d’officier pour une agglomération composée de ses semblables.

[^rama]: Aujourd’hui Ramla, dans la plaine de Sharon.

[^rameaux]: Dimanche des Rameaux.

[^ramonberenguer]: Raymond Bérenger IV, comte de Barcelone.

[^ratier]: Rocher réputé dangereux.

[^ratl]: Unité de mesure du poids, variant d'un lieu à l'autre.

[^rawshan]: Balcon en encorbellement qui surplombe la porte.

[^raymondII]: Raymond II de Tripoli.

[^raymondIIb]: Raymond II de Tripoli avait fait alliance un temps avec Nūr ad-Dīn lorsqu'il a dû faire face aux prétentions d'Alphonse-Jourdain sur le comté, en 1149.

[^raymondIII]: Raymond III (vers 1140- 1187), comte de Tripoli de 1152 à 1187, prince de Galilée de 1174 à 1187.

[^raymondpoitiers]: Raymond de Poitiers, prince d’Antioche (v. 1098-1149). Ses relations avec le comte d’Édesse Josselin II furent souvent très tendues, voire carrément hostiles.

[^raymondpuy]: Raymond du Puy (1080-1160), second supérieur de l’ordre de Saint-Jean de Jérusalem, qui fut le grand artisan de son développement au XIIe siècle.

[^renaudchatillon]: Renaud de Châtillon (v. 1120-1187), prince d’Antioche de 1153 jusqu’à sa capture en 1160.

[^ribier]: Vallée.

[^riffardeur]: Voleur, accapareur.

[^ris]: Réduire la voilure à l’aide de petits cordages.

[^rissolepoisson]: Désigne une pâtisserie qui peut être mangée les jours maigres, comme durant le Carême.

[^rivierediable]: Nom donné à la Mer Morte.

[^robert3auvergne]: Robert III d’Auvergne (v.1095-v.1147).

[^roha]: Nom communément donné à la ville d’Édesse.

[^roihenri]: Henri Ier Beauclerc (1068-1135), en 1105, prit la ville de Bayeux et le feu s’y répandit, détruisant entre autres une partie de la cathédrale.

[^romanalexandre]: Avec la matière de Bretagne et celle de France, cela constituait le socle de la littérature courtoise du XIIe siècle.

[^romanalexandre2]: Avec la matière de Bretagne et celle de France, le *Roman d’Alexandre* constituait le socle de la littérature courtoise du XIIe siècle.

[^romain]: Byzantin.

[^romanoi]: Noms que les Byzantins se donnaient à eux-mêmes, les « Romains ».

[^rue]: Désigne une travée par lesquelles les lits sont organisés dans l’Hôpital de Saint-Jean.

[^rum]: Désignes les *Romains*, c’est à dire les Byzantins.

[^sahlab]: Boisson chaude lactée, épaissie de farine ou de gruau selon les régions, parfois arômatisée.

[^saignes]: Marécage.

[^saintbernard]: Saint Bernard, qui fut un des grands promoteurs de la Seconde croisade.

[^saintbouillant]: Saint Martin d’été, dit aussi saint Martin le Bouillant, fêté le 4 juillet.

[^saintejeanete]: Le 24 juin.

[^saintjuste]: Le 2 novembre.

[^saintpancrace]: Monastère Saint-Pancrace, vers Hamersleben près d’Halberstadt, en Allemagne.

[^saintvital]: Le 28 avril.

[^saphet]: Aujourd’hui Safed, ville de Haute Galilée où se trouvait une forteresse franque.

[^save]: Rivière affluent du Danube, qui conflue à Belgrade.

[^sayette]: Aujourd’hui Sidon, au Liban.

[^sebaste]: Titre nobiliaire de la cour byzantine, initié au XIe siècle.

[^secondecroisade]: La seconde croisade, qui a échoué devant Damas en 1148 et n’a abouti à rien de vraiment concret.

[^secondecroisade2]: Pour la Seconde Croisade.

[^secrete]: Administration royale en charge des finances, dirigée par le sénéchal.

[^secretumsecretorum1]: Le *secret des secrets*.

[^senestre]: Gauche

[^serperfer]: Relever l’ancre.

[^sexte]: La mi-journée.

[^sharbush]: Chapeau de feutre à fronton triangulaire, un des symboles du statut de guerrier chez les Turcs.

[^shaykh]: De l’arabe, *maître, vieillard, sage*. Titre honorifique pour un homme respecté pour son grand âge et ses connaissances, souvent religieuses.

[^shayzar]: Shayzar, dans le nord de la Syrie.

[^shirkuh]: Asad al-Dīn Shīrkūh (vers 1120-23 mars 1169), général de Nūr ad-Dīn, oncle de Saladin.

[^sihna]: Gouverneur militaire, rôle récent crée par l’administration seldjoukide.

[^simples]: Plantes médicinales.

[^sirwal]: Pantalon à large entrejambe, souvent serré au mollet.

[^sochon]: Compagnon.

[^sociustractans]: Dans une association marchande, celui qui reçoit le capital et est chargé de le faire fructifier.

[^societasmaris]: Contrat commercial où des investisseurs complètent l’apport d’un entrepreneur souhaitant investir, en échange d’une part des profits.

[^socques]: Patins de bois portés sous les souliers.

[^soudan]: Terme attribué improprement au sultan, titre qu'Ernaut attribue par méconnaissance à Nūr ad-Dīn, seigneur de Damas et d'Alep.

[^soudan2]: Impropriété dérivé du titre de sultan, que les Latins utilisaient souvent de façon générale pour désigner les princes musulmans.

[^soudanbabylone]: Les Occidentaux avaient souvent une vision fantaisiste des souverains et territoires lointains. Il parle du calife fatimide du Caire.

[^soufi]: Mystique musulman.

[^soule]: Sorte d’ancêtre du rugby.

[^sourate64]: Sourate 64, « La grande perte » verset 11.

[^sourateXXIII50]: Sourate XXIII, dite sourate des Croyants, verset 50. Il y est fait allusion à une colline qu’on disait être celle de Damas.

[^suete]: Aujourd’hui ’Ain al-Habis, dans les gorges du Yarmouk.

[^suffah]: Large siège rembourré, parfois à dos et baldaquin, qui a donné le mot sofa.

[^sumaghiyyeh]: Plat traditionnel de la région de Gaza, en Palestine. Il est préparé à base de sumac, de tahin, de pois chiches et de viande, le tout assaisonné d’épice, d’ail et de piments. On peut le manger froid.

[^suri]: De Tyr.

[^tables]: Forme médiévale du backgammon ou jacquet, avec de nombreuses variantes de règles.

[^tahina]: Crème de sésame.

[^tamawhiya]: Conteneur à vin, souvent vendu par lot.

[^targe]: Petit bouclier léger.

[^taranjeh]: Aujourd’hui village des plateaux du Golan entre Liban, territoires occupés par Israël et la Syrie.

[^tawila]: Chapeau en forme de pain de sucre assez haut, qui peut être soutenu par une structure rigide.

[^tayyib]: Litt. *bon*, *généreux*, en contexte religieux : *pieux*.

[^templiers]: Les Templiers.

[^thawb]: Sorte de robe à manches longues, vêtement de base des populations moyen-orientales d’alors.

[^theophanie]: Appelée communément Épiphanie, célébrée différemment selon les rituels catholiques et byzantins.

[^thierryalsace]: Thierry d’Alsace, comte de Flandre.

[^thierryalsace2]: Il s'agit de Thierry, comte d'Alsace, qui avait épousé en 1139 la demi-soeur de Baudoin III, Sybille d'Anjou.

[^tiraz]: Bande de tissu décorée fixée au niveau du biceps.

[^tjvjik]: Le tjvjik, qui comprend aussi les reins, le tout poêlé.

[^tref]: Tente, généralement de qualité et destinée à des nobles.

[^tuban]: Pantalon court de fine toile faisant office de sous-vêtement.

[^turbessel]: Communément appelé tell bashir, c’est aujourd’hui Tilbeşar en Turquie.

[^turbessel2]: Appelé Turbessel par les Latins, c’est aujourd’hui Tilbeşar en Turquie.

[^toaille]: Chiffon, torchon, mouchoir, selon le cas.

[^tonneau]: Unité de poids valant approximativement une tonne, soit 20 quintaux médiévaux, soit 2000 livres de poids.

[^toussaint]: La Toussaint, soit le 1er novembre.

[^travailnoir]: L’expression désigne le travail qui ne se fait pas à la lueur du jour, en public. Il est généralement interdit aux artisans, pour éviter qu’ils ne travaillent mal ou de façon malhonnête. De là découle la notion de travail caché.

[^triple]: Tripoli, actuellement au Liban.

[^tughtekin]: Tughtekin, atabeg de Damas de 1104 à 1128, fondateur de la dynastie Bouride.

[^urfa]: Nom arabe de la cité d'Édesse.

[^vauxmoyse]: Aujourd’hui al-Wu’Aira, près de Petra en Jordanie.

[^vegece]: Publius Flavius Vegetius Renatus est un écrivain romain fameux au Moyen Âge pour ses textes sur la tactique militaire.

[^vendredipassion1157]: Le 22 mars pour l’année 1157.

[^venimer]: Empoisonné.

[^ventaille]: Partie amovible de la coiffe qui protège le bas du visage.

[^vestimenta]: « Les enfants des Hébreux étendaient leurs vêtements sur le chemin », autre antienne de procession.

[^viandes]: On emploie le terme *viandes* pour désigner la nourriture de façon générale.

[^voirqitamaindestin]: Voir Qit'a *La main du destin*.

[^voirqitameilleurfils]: Voir Qit'a *Le meilleur des fils*.

[^voirqitaporon]: Lieu-dit à côté de Vézelay, voir Qit'a *Les croisés du Poron*.

[^voirqitabranleterre]: Voir Qit’a *Branleterre*.

[^voirqitadesertpoing]: Voir les Qit’a *Poing de trop* et *Désert*.

[^voirqitaliaisonchetif]: Voir les Qit’a *Chétif destin* et *Liaisons dangereuses*.

[^voirqitatraditions]: Voir Qit'a *Traditions*.

[^voirqitavacher]: Voir la trilogie de Qit'a *Pour une poignée de dinars*, *Pour quelques dinars de plus* et *Impitoyable*.

[^voirt1]: Voir le premier tome des enquêtes d’Ernaut, *La nef des loups*.

[^voirt2]: Voir le second tome des enquêtes d’Ernaut, *Les Pâques de sang*.

[^voirt3]: Voir le troisième tome des enquêtes d’Ernaut, *La terre des morts*.

[^voirchansondouxmetier]: Voir la chanson « Le plux doux des mestiers ».

[^voirfierarcher]: Voir la chanson *Les traits du fier archer*.

[^voirporon]: Voir Qit’a *Les croisés du Poron*.

[^voirscriptamanent]: Voir Qit’a *Scripta manent*.

[^vulgrin]: De Bourges (1120-1137).

[^wadimusa]: Littéralement *Vallée de Moïse*. Dorénavant appelée Gaia, en Jordanie, proche du site archéologique de Pétra.

[^waqf]: En droit lmusulman, cela désigne une rente perpétuelle fait à une institution charitable, pieuse ou publique.

[^yahya]: Nom arabo-musulman donné à saint Jean Baptiste.

[^zaatar]: Désormais utilisé pour désigner un mélange d’épices, le terme désignait selon les régions une herbe aromatique ou l’autre : l’hysope au Moyen-Orient ou l’origan au Maghreb.

[^zakat]: Aumône obligatoire calculée sur les revenus et le capital des marchandises et métaux précieux conservés au moins un an.

[^zaynaldin]: Communément appelé Zayn al-Din, il était un militaire réputé, commandant militaire zengide de Djéziré et seigneur d'Irbil.

[^zengi]: Imād al-Dīn Zengī, atabeg de Mossoul et Damas (1087-1146).

[^znoutassit]: Pâtisserie à base de pâte, avec de la crème et des pistaches.
